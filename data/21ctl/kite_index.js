use kite;

db.app.ensureIndex({"name":1},{unique:true});
db.app.ensureIndex({"status": 1});
db.app.ensureIndex({"sp-id": 1});

db.sp.ensureIndex({"sp-id" : 1});
db.sp.ensureIndex({"coop-user-id" : 1});
db.sp.ensureIndex({"coop-user-name":1},{unique:true});

db.ncs_sla.ensureIndex({"ncs-type" : 1});
db.ncs_sla.ensureIndex({"status" : 1});
db.ncs_sla.ensureIndex({"operator" : 1});
db.ncs_sla.ensureIndex({"app-id" : 1});

db.routing_keys.ensureIndex({"ncs-type.": 1});
db.routing_keys.ensureIndex({"operator": 1});
db.routing_keys.ensureIndex({"shortcode": 1});
db.routing_keys.ensureIndex({"keyword": 1});
db.routing_keys.ensureIndex({"sp-id": 1});