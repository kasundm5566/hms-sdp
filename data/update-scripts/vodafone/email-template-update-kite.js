//update script for vodafone
//=========================

use kite;

//Update existing email template
//=====================================

db.email_templates.update({"_id":"app-sp-send-password-event"}, { "_id":"app-sp-send-password-event", "cc-address-list":[], "event-name":"app-sp-send-password-event", "subject":"Your password for application $app.(\"app-id\")$ on $system.(\"sdp-product-name\")$", "bcc-address-list":[], "email-body":"Dear $sp.(\"first-name\")$,\n\nThe application you provisioned on Vodafone Service Delivery Platform has sent to administrator approval. Please set the following password in application requests to SDP.\n\nApplication ID: $app.(\"app-id\")$\nPassword: $app.password$\n\nPlease save this email for future reference.\n\n$system.(\"sp-email-footer\")$", "to-address-list":[] });
