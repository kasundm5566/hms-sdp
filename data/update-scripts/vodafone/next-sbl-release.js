// add newly provided short codes to routing keys

use kite;

db.routing_keys.insert({ "ncs-type" : "sms", "operator" : "vodafone", "category" : ["sdp"], "shortcode" : "8888", "keyword" : "", "exclusive" : false, "sp-id" : ""})
db.routing_keys.insert({ "ncs-type" : "sms", "operator" : "vodafone", "category" : ["sdp"], "shortcode" : "8889", "keyword" : "", "exclusive" : false, "sp-id" : ""})

db.routing_keys.insert({ "ncs-type" : "sms", "operator" : "vodafone", "category" : ["soltura"], "shortcode" : "8888", "keyword" : "", "exclusive" : false, "sp-id" : ""})
db.routing_keys.insert({ "ncs-type" : "sms", "operator" : "vodafone", "category" : ["soltura"], "shortcode" : "8889", "keyword" : "", "exclusive" : false, "sp-id" : ""})

db.routing_keys.insert({ "ncs-type" : "ussd", "operator" : "vodafone", "category" : ["sdp"], "shortcode" : "407", "keyword" : "", "exclusive" : false, "sp-id" : ""})