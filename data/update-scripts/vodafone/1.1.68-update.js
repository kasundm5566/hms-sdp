use kite;

db.ncs_sla.update({"ncs-type" : "cas", "charging.allowed-payment-instruments" : {$in : ["Mpaisa"]}}, { $set : {"charging.payment-instrument-additional-data.Mpaisa.real-time-charging-enabled" : false}}, false, true);