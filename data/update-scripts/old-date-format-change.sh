#!/bin/sh
#2012/02/13

while :
do
app_id=$(echo "db.app.findOne({\"end-date\" : { \$regex : \"^../../....\$\" }})" | mongo 127.0.0.1:27017/kite | grep \"_id\" | cut -c 11-20)
if [ "" != "$app_id" ];then
    echo "processing App with id=$app_id"
    end_date=$(echo "db.app.findOne({\"_id\" : \"$app_id\" })" | mongo 127.0.0.1:27017/kite | grep "end-date" | cut -c 16-25)
    echo $end_date
    dd=$(echo $end_date | cut -c 1-2)
    mm=($(echo $end_date | cut -c 4-5) - 1)
    yy=$(echo $end_date | cut -c 7-10)
    echo $dd $mm $yy

    echo "db.app.update({\"_id\" : \"$app_id\"}, { \$set : {\"end-date\" : new Date($yy,$mm,$dd)}})" | mongo 127.0.0.1:27017/kite
else
    exit
fi

done

while :
do
app_id=$(echo "db.app.findOne({\"active-production-start-date\" : { \$regex : \"^../../....\$\" }})" | mongo 127.0.0.1:27017/kite | grep \"_id\" | cut -c 11-20)
if [ "" != "$app_id" ];then
    echo "processing App with id=$app_id"
    active_date=$(echo "db.app.findOne({\"_id\" : \"$app_id\" })" | mongo 127.0.0.1:27017/kite | grep "active-production-start-date" | cut -c 16-25)
    echo $active_date
    dd=$(echo $active_date | cut -c 1-2)
    mm=($(echo $active_date | cut -c 4-5) - 1)
    yy=$(echo $active_date | cut -c 7-10)
    echo $dd $mm $yy

    echo "db.app.update({\"_id\" : \"$app_id\"}, { \$set : {\"active-production-start-date\" : new Date($yy,$mm,$dd)}})" | mongo 127.0.0.1:27017/kite
else
    exit
fi

done

while :
do
app_id=$(echo "db.app.findOne({\"app-request-date\" : { \$regex : \"^../../....\$\" }})" | mongo 127.0.0.1:27017/kite | grep \"_id\" | cut -c 11-20)
if [ "" != "$app_id" ];then
    echo "processing App with id=$app_id"
    req_date=$(echo "db.app.findOne({\"_id\" : \"$app_id\" })" | mongo 127.0.0.1:27017/kite | grep "app-request-date" | cut -c 16-25)
    echo $req_date
    dd=$(echo $req_date | cut -c 1-2)
    mm=($(echo $req_date | cut -c 4-5) - 1)
    yy=$(echo $req_date | cut -c 7-10)
    echo $dd $mm $yy

    echo "db.app.update({\"_id\" : \"$app_id\"}, { \$set : {\"app-request-date\" : new Date($yy,$mm,$dd)}})" | mongo 127.0.0.1:27017/kite
else
    exit
fi

done