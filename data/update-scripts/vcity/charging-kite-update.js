//update script for vcity
//=========================

use kite;


//Update subscription key changes
//=====================================

db.ncs_sla.update({"ncs-type" : "subscription"},{$rename : {"charging-type" : "charging.type", "frequency" : "charging.frequency", "amount" : "charging.amount"}}, false, true);

db.ncs_sla.update({"ncs-type" : "subscription", "charging.type" : "flat"},{$set : {"charging.allowed-payment-instruments" : ["operator-charging"], "charging.party" : "subscriber"}}, false, true);


//Update downloadable key changes
//=====================================


db.ncs_sla.update({"ncs-type" : "downloadable", "charging-type" : "free"},{$unset : {"charging-method" : 1, "charging-amount" : 1}}, false, true);

db.ncs_sla.update({"ncs-type" : "downloadable"},{$rename : {"charging-type" : "charging.type", "charging-amount" : "charging.amount"}}, false, true);

db.ncs_sla.update({"ncs-type" : "downloadable", "charging.type" : "flat"},{$set : {"charging.allowed-payment-instruments" : ["M-Pesa", "Equity Bank"], "charging.party" : "subscriber"}}, false, true);


//Update caas key changes
//=====================================


db.ncs_sla.update({"ncs-type" : "cas"},{$set : {"charging.allowed-payment-instruments" : ["M-Pesa", "Equity Bank"], "charging.party" : "subscriber"}}, false, true);


//Update sms key changes
//=====================================


db.ncs_sla.update({"ncs-type" : "sms", "mo.charging.method":{$exists:1}},{$unset:{"mo.charging.method":1}}, false, true);

db.ncs_sla.update({"ncs-type" : "sms", "mo.charging.type":"free"},{$unset:{"mo.charging.amount":1, "mo.charging.party":1, "mo.charging.service-code":1}}, false, true);

db.ncs_sla.update({"ncs-type" : "sms", "mo.charging.type":{$exists:1}, "mo.charging.type":{$ne : "free"}},{$set:{"mo.charging.allowed-payment-instruments":["operator-charging"]}}, false, true);

db.ncs_sla.update({"ncs-type" : "sms", "mt.charging.method":{$exists:1}},{$unset:{"mt.charging.method":1}}, false, true);

db.ncs_sla.update({"ncs-type" : "sms", "mt.charging.type":"free"},{$unset:{"mt.charging.amount":1, "mt.charging.party":1, "mt.charging.service-code":1}}, false, true);

db.ncs_sla.update({"ncs-type" : "sms", "mt.charging.type":{$exists:1}, "mt.charging.type":{$ne : "free"}},{$set:{"mt.charging.allowed-payment-instruments":["operator-charging"]}}, false, true);


//Update ussd key changes
//=====================================


db.ncs_sla.update({"ncs-type" : "ussd", "session.mt-allowed":{$exists:1}},{$unset:{"session.mt-allowed":1}}, false, true);

db.ncs_sla.update({"ncs-type" : "ussd", "session.mo-allowed":{$exists:1}},{$unset:{"session.mo-allowed":1}}, false, true);

db.ncs_sla.update({"ncs-type" : "ussd", "mo.charging.method":{$exists:1}},{$unset:{"mo.charging.method":1}}, false, true);

db.ncs_sla.update({"ncs-type" : "ussd", "mo.charging.type":"free"},{$unset:{"mo.charging.amount":1, "mo.charging.party":1, "mo.charging.service-code":1}}, false, true);

db.ncs_sla.update({"ncs-type" : "ussd", "mo.charging.type":{$exists:1}, "mo.charging.type":{$ne : "free"}},{$set:{"mo.charging.allowed-payment-instruments":["operator-charging"], "mo-charging-allowed" : "true", "mt-charging-allowed" : "false", "session-charging-allowed" : "false"}}, false, true);


//Update wap push key changes
//=====================================


db.ncs_sla.update({"ncs-type" : "wap-push", "mt.delivery-report-required":{$exists:1}, "mt.delivery-report-required":false},{$unset:{"mt.delivery-report-required":1, "mt.delivery-report-url":1}}, false, true);

db.ncs_sla.update({"ncs-type" : "wap-push", "mt.delivery-report-required":{$exists:1}, "mt.delivery-report-required":true},{$unset:{"mt.delivery-report-required":1}}, false, true);

db.ncs_sla.update({"ncs-type" : "sms", "mt.charging.type":{$exists:1}, "mt.charging.type":{$ne : "free"}},{$set:{"mt.charging.allowed-payment-instruments":["operator-charging"]}}, false, true);
db.ncs_sla.update({"ncs-type" : "wap-push", "mt.charging.method":{$exists:1}},{$unset:{"mt.charging.method":1}}, false, true);

db.ncs_sla.update({"ncs-type" : "wap-push", "mt.charging.type":"free"},{$unset:{"mt.charging.amount":1, "mt.charging.party":1, "mt.charging.service-code":1}}, false, true);

db.ncs_sla.update({"ncs-type" : "wap-push", "mt.charging.type":{$exists:1}, "mt.charging.type":{$ne : "free"}},{$set:{"mt.charging.allowed-payment-instruments":["operator-charging"]}}, false, true);


//Add safaricom service code related system configuration entries
// !!!!!!!!!! These service codes should be updated according safaricom specification !!!!!!!!
//============================================================================================


db.system_configurations.save({ "_id" : "safaricom-subscription-service-code-charging-amount-mapping", "value" : { "1123" : "5"} });
db.system_configurations.save({ "_id" : "safaricom-subscription-service-code-short-code-mapping", "value" : { "1123" : "789"} });

db.system_configurations.save({ "_id" : "safaricom-wap-push-service-code-charging-amount-mapping", "value" : { "1123" : "5"} });
db.system_configurations.save({ "_id" : "safaricom-wap-push-service-code-short-code-mapping", "value" : { "1123" : "789"} });

db.system_configurations.save({ "_id" : "safaricom-ussd-service-code-charging-amount-mapping", "value" : { "1123" : "5"} });
db.system_configurations.save({ "_id" : "safaricom-ussd-service-code-short-code-mapping", "value" : { "1123" : "789"} });


//!!!!!!!!!!!!!!!!! Please update existing subscription ncses with relevant charging.service-code for charging.amount !!!!!!!!!!!!!!!!


//charging related system configuration entries
//=================================================


db.system_configurations.save({ "_id" : "common-charging-mobile-account-name", "value" : "Mobile Account" });