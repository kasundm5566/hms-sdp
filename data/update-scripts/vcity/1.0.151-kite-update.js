// update mpesa paybill number for already existing ncs with M-Pesa as allowed payment instrument

use kite;

db.ncs_sla.update({"charging.allowed-payment-instruments":"M-Pesa", "charging.meta-data.mpesa-paybill-number": {$not : /.*/}}, {$set : {"charging.meta-data.mpesa-paybill-number":"919900"}}, false, true)


// insert new email templates for mpesa-pay bill number duplicated sp, admin notification email

db.email_templates.insert({ "_id" : "app-registration-duplicate-mpesa-paybill-no-event", "cc-address-list" : [], "event-name" : "app-registration-duplicate-mpesa-paybill-no-event", "subject" : "MPESA Paybill number duplicated", "bcc-address-list" : [], "email-body" : "Dear Admin, \n\n$sp.(\"coop-user-name\")$ has used Paybill number $ncs.(\"mpesa-paybill-number\")$ when provisioning the following \napplication. \nApplication Id: $app.(\"app-id\")$ \nApplication name: $app.name$ \n\nThis Paybill number is already used by $ncs.(\"mpesa-paybill-duplicate-sps\")$. \nPlease resolve this conflict prior to approving $sp.(\"coop-user-name\")$’s application. \n\nPlease login at $system.(\"provisioning-url\")$ \n\nThank you.", "to-address-list" : []})
