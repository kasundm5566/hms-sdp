//remove and update with new keys in sp sla
//==========================================

use kite;

db.sp.update({"dlApp-selected" : "true", "dl-app-mo-tps" : { $exists : true }, "dl-app-mo-tpd" : { $exists : true } }, { $rename : { "dl-app-mo-tps" : "dl-app-mcd", "dl-app-mo-tpd" : "dl-app-mdpd" } }, false, true);

//update with proper values in sp sla
//====================================

db.sp.update({"dlApp-selected" : "true", "dl-app-mcd" : {$gt : "25"}}, { $set : { "dl-app-mcd" : "20" }}, false, true);

//remove and update with new keys in downloadable ncs sla
//========================================================

db.ncs_sla.update({"ncs-type" : "downloadable"}, {$unset:{ "dl-app-mo-tps" : 1, "dl-app-mo-tpd" : 1 }, $set:{"dl-atmpt" : 3, "charging-method" : "financial-instrument-charging", "charging-amount" : "", "charging-type" : "free", "link-exp-time" : "120", "link-exp-time-type" : "minutes", "max-dl-per-day" : "20", "subscription-selected" : false, "max-con-dl" : "5", "dl-repo" : "External"}}, false, true);
