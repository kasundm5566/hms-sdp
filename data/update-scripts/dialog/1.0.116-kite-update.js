//update script for dialog
//=========================

use kite;

//Update downloadable key change
//=====================================

db.ncs_sla.update({"ncs-type" : "downloadable", "subscription-selected" :  { $exists : true }}, { $rename : { "subscription-selected" : "subscription-required" } }, false, true)
