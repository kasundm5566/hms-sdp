//update script for dialog
//==========================================

use kite

//add downloadable ncs to sp sla
//==========================================

db.sp.update({ $and : [{"_id" : {$ne : "SPP_000000"}}, {"_id" : {$ne : "SPP_999990"}}]}, { $set : { "downloadable-mcd" : "10", "downloadable-mdpd" : "100", "downloadable-selected" : "true", "sp-selected-services" : [ "sms", "ussd", "cas", "subscription", "downloadable" ] }}, false, true);

db.system_configurations.update({"_id" : "supported-ncses-and-operators"}, { $set : {"value" : { "sms" : [ "dialog" ], "ussd" : [ "dialog" ], "cas" : [ ], "subscription" : [ ], "downloadable" : [ ] }}}, false, false);
