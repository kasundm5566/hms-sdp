//update script for dialog
//=========================

use cms;

//Update existing download
//=====================================

currentDate = new Date();
newDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() + 3);

db.internal_downloads.update({}, {$set:{"expire-date":newDate, "status":"initial"}}, false, true);