//update script for dialog
//=========================

use kite;

//remove unnecessary ncses from sp sla
//=====================================

db.sp.update({"dlApp-selected" : { $exists : true }}, { $unset : { "dlApp-selected" : 1 }}, false, true);

db.sp.update({"wap-push-selected" : { $exists : true }, "_id":{ $ne : "SPP_000000"}}, { $unset : { "wap-push-selected" : 1 }}, false, true);

//add other ncses to sp sla
//==========================

db.sp.update({ $and : [{"_id" : {$ne : "SPP_000000"}}, {"_id" : {$ne : "SPP_999990"}}]}, { $set : { "sms-mt-tpd" : "30000", "sms-mo-tpd" : "30000", "ussd-mo-tpd" : "10000", "ussd-mo-tps" : "10", "ussd-mt-tpd" : "10000", "ussd-mt-tps" : "10", "ussd-selected" : "true", "cas-mo-tpd" : "100", "cas-mo-tps" : "10", "cas-selected" : "true", "sp-selected-services" : [ "sms", "ussd", "cas", "subscription" ] }}, false, true);

//add new defaults values to sms-ncs in nsc_sla
//==============================================

db.ncs_sla.update({ $and : [{"app-id" : {$ne : "APP_000000"}}, {"app-id" : {$ne : "APP_999990"}}, {"ncs-type" : "sms"}, {"mo" : { $exists : true }}, {"mt" : { $exists : true }}]}, { $set : { "mt.tpd":"10000", "mo.tpd":"10000"}}, false, true);

//update system sp sla
//=====================

db.sp.update({"_id": "SPP_000000"}, { $set : { "sdp-user" : "true", "soltura-user" : "false", "wap-push-mt-tps" : "99", "wap-push-mt-tpd" : "1000", "sp-selected-services" : [ "sms", "ussd", "cas", "subscription", "wap-push" ] }, $unset: { "dl-app-mo-tps":1 , "dl-app-mo-tpd":1, "wap-push-mo-tps" : 1, "wap-push-mo-tpd" : 1 }}, false, false);

//update email templates
//=======================

db.email_templates.save({ "_id" : "app-registration-with-caas-request-event", "cc-address-list" : [], "event-name" : "app-registration-with-caas-request-event", "subject" : "Dialog Application Request with CaaS - $system.(\"current-date-and-time\")$", "bcc-address-list" : [ "sanjeewap@hsenidmobile.com" ], "email-body" : "Dear Administrator,\n\nThe following Application request with CaaS have been received to the $system.(\"sdp-product-name\")$ as at $system.(\"current-date-and-time\")$  and is pending for approval\n\nRequested Date               : $app.(\"app-request-date\")$\nRequesters Name              : $system.(\"sdp-product-name\")$\nDepartment                   : DeveloperZone\nService Phone No             : 077xxxxxxx\nApplication ID               : $app.(\"app-id\")$\nShort Description            : $app.(\"name\")$ $ncs.(\"post-paid-bill-desc-code\")$\nLong Description             : $app.(\"description\")$\nPurpose for Request          : Request for reason code and to register post bill description for the application\nPost Paid Bill Description   : $ncs.(\"post-paid-bill-desc-code\")$\nRequired services            : Debit and Balance Check\n\n\nPlease attend to this request as soon as possible.\n\nThank you,\nDialog\n\nContact Information:\nHotline : 1777\nE-mail : ideamart@dialog.lk\n\n$extra.footer$", "to-address-list" : [] });

//update system configuration
//============================

db.system_configurations.update({"_id" : "supported-ncses-and-operators"}, { $set : {"value" : { "sms" : [ "dialog" ], "ussd" : [ "dialog" ], "cas" : [ ], "subscription" : [ ] }}}, false, false);

db.system_configurations.save({ "_id" : "dialog-sms-service-code-charging-amount-mapping", "value" : { } });
