use kite;

db.system_configurations.save({"_id" : "dialog-lbs-ncs-display-defaults", "value" : {"lbs-freshness-of-location-map" : "low", "lbs-horizontal-accuracy-map" :"0-100 meters", "lbs-response-time-list":"no-delay"}});
db.system_configurations.save({ "_id" : "dialog-lbs-horizontal-accuracy-map", "value" : {"0-100 meters" : "100", "100-500 meters" : "500", "500-1000 meters" : "1000", "Above 1000 meters" : "1500"}});
