//update script for dialog
//=========================

use kite;

//add and update other ncses to sp sla
//=====================================

db.sp.update({ $and : [{"_id" : {$ne : "SPP_000000"}}, {"_id" : {$ne : "SPP_999990"}}]}, { $set : { "cas-mo-tpd" : "5000", "downloadable-mcd" : "10", "downloadable-mdpd" : "100", "downloadable-selected" : "true", "sp-selected-services" : [ "sms", "ussd", "cas", "subscription", "downloadable" ] }}, false, true);

//add new defaults values to caas-ncs in nsc_sla
//==============================================

db.ncs_sla.update({ $and : [{"app-id" : {$ne : "APP_000000"}}, {"app-id" : {$ne : "APP_999990"}}, {"ncs-type" : "cas"}]}, { $set : { "tpd":"5000" , "debit-min-amount" : "5", "debit-max-amount" : "100"}}, false, true);

//enable downloadable in system configuration
//============================================

db.system_configurations.update({"_id" : "supported-ncses-and-operators"}, { $set : {"value" : { "sms" : [ "dialog" ], "ussd" : [ "dialog" ], "cas" : [ ], "subscription" : [ ], "downloadable" : [ ] }}}, false, false);

//add supported platforms for downloadable apps
//==============================================

db.platform.save({"platform-id":"android","platform-name":"Android","suppported-file-types":["apk"],"platform-versions":["2.1","2.2","3.0","3.1","4.0"]});
db.platform.save({"platform-id":"blackberry","platform-name":"Blackberry","suppported-file-types":["cod"],"platform-versions":["3.6","4.5","5.0"]});
db.platform.save({"platform-id":"javame","platform-name":"Java/Java ME","suppported-file-types":["jar"],"platform-versions":["MIDP 1.0","MIDP 2.0","MIDP 3.0"]});
db.platform.save({"platform-id":"symbian","platform-name":"Symbian","suppported-file-types":["sis","sisx"],"platform-versions":["S40","S60","S80"]});
db.platform.save({"platform-id":"windows","platform-name":"Windows Mobile","suppported-file-types":["zip","cab","msi"],"platform-versions":["5.0","6.0","6.5"]});

//add supported devices for downloadable apps
//=============================================

db.device.save({"brand":"Nokia", "model":"Nokia 5800", "platform-id":"symbian", "platform-versions":["S60"]});
db.device.save({"brand":"Nokia", "model":"Nokia 6120", "platform-id":"symbian", "platform-versions":["S60"]});
db.device.save({"brand":"Nokia", "model":"Nokia 6680", "platform-id":"symbian", "platform-versions":["S60"]});
db.device.save({"brand":"Nokia", "model":"Nokia 5300", "platform-id":"symbian", "platform-versions":["S60"]});
db.device.save({"brand":"Samsung", "model":"Galaxy Y", "platform-id":"android", "platform-versions":["2.1", "2.2"]});
db.device.save({"brand":"Samsung", "model":"Galaxy S", "platform-id":"android", "platform-versions":["2.1", "2.2"]});
db.device.save({"brand":"Samsung", "model":"Galaxy S II", "platform-id":"android", "platform-versions":["2.1"]});
db.device.save({"brand":"Samsung", "model":"Galaxy Nexus", "platform-id":"android", "platform-versions":["2.1", "3.1", "4.0"]});
db.device.save({"brand":"HTC", "model":"My Touch 3G", "platform-id":"android", "platform-versions":["2.1", "2.2"]});
db.device.save({"brand":"HTC", "model":"My Touch 4G", "platform-id":"android", "platform-versions":["2.1", "2.3"]});