use kite;

db.system_configurations.save({ "_id" : "common-charging-mobile-account-id", "value" : "Mobile Account" });

db.system_configurations.update({"_id" : "supported-ncses-and-operators"}, { $set : {"value" : { "sms" : [ "dialog" ], "ussd" : [ "dialog" ], "cas" : [ ], "subscription" : [ ], "downloadable" : [ ], "lbs" : [ "dialog" ], "wap-push" : ["dialog"] }}});