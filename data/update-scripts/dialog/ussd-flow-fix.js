// This script adds some missing updates in `charging-kite-update.js`
// Add few missing keys to ncs_sla of ussd app to enable MO and MT flows.
//=======================================================================

use kite;


db.ncs_sla.update({"ncs-type" : "ussd"} , {$set:{"mo-allowed":true, "mt-allowed":true}}, false, true);