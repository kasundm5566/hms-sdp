//update script for dialog LBS NCS support
//========================================


use kite;


//add and update other ncses to sp sla
//=====================================


db.sp.update({ $and : [{"_id" : {$ne : "SPP_000000"}}, {"_id" : {$ne : "SPP_999990"}}]}, { $set : { "lbs-tps" : "10", "lbs-tpd" : "100", "lbs-selected" : "true", "sp-selected-services" : [ "sms", "ussd", "cas", "subscription", "downloadable", "lbs" ] }}, false, true);


//enable lbs in system configuration
//============================================


db.system_configurations.update({"_id" : "supported-ncses-and-operators"}, { $set : {"value" : { "sms" : [ "dialog" ], "ussd" : [ "dialog" ], "cas" : [ ], "subscription" : [ ], "downloadable" : [ ], "lbs" : [ "dialog" ] }}});

db.system_configurations.save({ "_id" : "dialog-lbs-freshness-of-location-map", "value" : { "high" : "high", "high-to-low" : "high-low", "low-to-high" : "low-high", "low" : "low"}});
db.system_configurations.save({ "_id" : "dialog-lbs-horizontal-accuracy-map", "value" : {"0-100" : "100", "100-500" : "500", "500-1000" : "1000", "Above 1000" : "1500"} });
db.system_configurations.save({ "_id" : "dialog-lbs-response-time-list", "value" : [ "no-delay", "low-delay", "delay-tolerance" ] });
db.system_configurations.save({"_id" : "dialog-lbs-ncs-defaults-map", "value" : { "mt": {"charging": {"type": "free"}}}});

db.system_configurations.save({ "_id" : "sbl-lbs-receiver-addresses", "value" : { "dialog" : "http://sbl.sdp:8000/lbs/slir" } })

db.sp.update({"sdp-user" : "true", "sp-selected-services" : {$nin : ["lbs"]}}, {$push : {"sp-selected-services" : "lbs"}}, false, true)
db.sp.update({"sdp-user" : "true", "sp-selected-services" : {$in : ["lbs"]}}, {$set : {"lbs-selected" : "true", "lbs-tpd" : "100", "lbs-tps" : "10"}}, false, true)
