//remove and update with new keys in sp sla
//==========================================

use kite;

db.sp.update({"dlApp-selected" : "true", "dl-app-mcd" : { $exists : true }, "dl-app-mdpd" : { $exists : true } }, { $rename : { "dl-app-mcd" : "downloadable-mcd", "dl-app-mdpd" : "downloadable-mdpd" } }, false, true);

db.sp.update({"dlApp-selected" : { $exists : true }}, { $rename : { "dlApp-selected" : "downloadable-selected" }}, false, true);
