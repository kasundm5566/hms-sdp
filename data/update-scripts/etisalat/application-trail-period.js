
use kite;

// update application trial period related system configurations
db.system_configurations.remove({"_id" : "subscription-ncs-trial-period-unit-map"});
db.system_configurations.remove({"_id" : "subscription-ncs-default-trial-period-map"});
db.system_configurations.insert({ "_id" : "subscription-ncs-trial-period-unit-map", "value" : { "period" : {"days" : "15"}, "max-count" : NumberInt(1) }})
db.system_configurations.insert({ "_id" : "subscription-ncs-default-trial-period-map", "value" : { "unit" : "days", "value" : NumberInt(15) } })