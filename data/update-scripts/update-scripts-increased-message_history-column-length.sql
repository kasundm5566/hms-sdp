
/* To increase the message_history column message length to 2500 */
ALTER TABLE `message_history` MODIFY `message` VARCHAR(2500);