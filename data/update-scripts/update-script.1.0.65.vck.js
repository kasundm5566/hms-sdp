//move kite.build_file collection to cms.build_file
//==================================================

use admin;

db.runCommand({renameCollection:"kite.build_file",to:"cms.build_file"})
db.runCommand({renameCollection:"kite.build_file.files",to:"cms.build_file.files"})
db.runCommand({renameCollection:"kite.build_file.chunks",to:"cms.build_file.chunks"})
