#!/bin/sh

while :
do
app_id=$(echo "db.ncs_sla.findOne({\$and: [{\"subscription-required\" : null}, {\"ncs-type\" :{ \$ne : \"subscription\"}}]}, {\"app-id\" : 1});" | mongo  127.0.0.1:27017/kite | grep \"app-id\" | cut -c 60-71)
echo $app_id

if [ "" != "$app_id" ];then
	correct_app=$(echo "db.ncs_sla.findOne({\"app-id\" : $app_id, \"ncs-type\" : \"subscription\"}, {\"app-id\" : 1});" | mongo  127.0.0.1:27017/kite | grep \"app-id\" | cut -c 60-71)
	echo $correct_app
echo "$app_id"
	if [ "" != "$correct_app" ];then
		echo "db.ncs_sla.update({\$and : [{\"app-id\" : $app_id }, {\"ncs-type\" :{ \$ne : \"subscription\"}},{\"subscription-required\" : null}]}, {\$set : {\"subscription-required\" : true}});" | mongo 127.0.0.1:27017/kite
	else
		echo "db.ncs_sla.update({\$and : [{\"app-id\" : $app_id }, {\"ncs-type\" :{ \$ne : \"subscription\"}},{\"subscription-required\" : null}]}, {\$set : {\"subscription-required\" : false}});" | mongo 127.0.0.1:27017/kite
	fi
else
 	break;
fi

done

echo NCS updated successfully


