#!/bin/sh
DATA_DIR=`dirname $0`

mongoimport --db kite-test --collection email_templates  --file $DATA_DIR/email_templates.json
mongoimport --db kite-test --collection device  --file $DATA_DIR/device.json
mongoimport --db kite-test --collection platform  --file $DATA_DIR/platform.json
mongoimport --db kite-test --collection blacklist  --file $DATA_DIR/blacklist.json
mongoimport --db kite-test --collection service_keywords  --file $DATA_DIR/service_keywords.json
mongoimport --db kite-test --collection system_configurations  --file $DATA_DIR/system_configurations.json
mongoimport --db kite-test --collection routing_keys  --file $DATA_DIR/routing_keys.json
mongoimport --db kite-test --collection sp  --file $DATA_DIR/system_sp.json
mongoimport --db kite-test --collection app  --file $DATA_DIR/system_app.json
mongoimport --db kite-test --collection ncs_sla  --file $DATA_DIR/system_ncs_sla.json

mongo < $DATA_DIR/kite-_index.js >/dev/null 2>&1