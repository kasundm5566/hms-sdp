Collections for each json files should map
==========================================

app-charging.json   > app
app.json            > app
caas_sla.json       > ncs_sla
routing-key-charging.json > routing_keys
sms-charging.json   > ncs_sla
sms_sla.json        > ncs_sla
sp.json             > sp
subscription.json   > ncs_sla
routing-keys.json   > routing_keys