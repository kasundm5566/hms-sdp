use kite;

db.ncs_sla.update({"app-id":"APP_000001","ncs-type":"ussd"},{$set:{"mo-allowed":true}},true,true);
db.throttling.remove();
db.sp.remove({"coop-user-name" : "system-sla"},{$delete:{ "coop-user-name" : "system-sla"}});
db.sp.save ({
    "_id" : "SPP_000001",
    "cas-mo-tpd" : "100",
    "cas-mo-tps" : "99",
    "cas-selected" : "false",
    "coop-user-id" : "20110608130106331",
    "coop-user-name" : "system-sla",
    "created-by" : "corporate",
    "dl-app-mo-tpd" : "5000",
    "dl-app-mo-tps" : "99",
    "dlApp-selected" : "false",
    "max-no-of-bc-msgs-per-day" : "50000",
    "max-no-of-subscribers" : "1",
    "reason" : "reason",
    "sms-mo" : true,
    "sms-mo-tpd" : "50000",
    "sms-mo-tps" : "99",
    "sms-mt" : true,
    "sms-mt-tpd" : "50000",
    "sms-mt-tps" : "99",
    "sms-selected" : "true",
    "sp-id" : "SPP_000001",
    "sp-request-date" : "7/28/11",
    "sp-selected-services" : [
        "sms",
        "subscription",
        "ussd"
    ],
    "status" : "approved",
    "subscription-selected" : "true",
    "ussd-mo-tpd" : "50000",
    "ussd-mo-tps" : "99",
    "ussd-selected" : "false",
    "wap-push-mo-tpd" : "1000",
    "wap-push-mo-tps" : "99",
    "wap-push-selected" : "false"
});
