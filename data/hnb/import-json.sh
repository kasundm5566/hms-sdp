#Use this to populate initial data in kite db or any other. All json files in the running path of this file will be added as a separate collection to db.

#----------settings--------
database=kite
db_ip=192.168.0.189
#----------------------

ls -1 *.json | while read lines
do
collection=`echo $lines | cut -d'.' -f1`
echo "---sourcing $collection----"
mongoimport --host $db_ip --db $database --collection $collection < $lines
done
