use subscription;

db.subscription.ensureIndex({"mandate-id": 1});

db.subscription.ensureIndex({"app-id" : 1});

db.subscription.ensureIndex({"msisdn":1, "app-id" : 1});