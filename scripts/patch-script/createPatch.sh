sourceAdd=$1
livemd5Add=$2

basename=$sourceAdd; file_ext=
while [[ $basename = ?*.* &&
         ( ${basename##*.} = [A-Za-z]* || ${basename##*.} = 7z ) ]]; do
  file_ext=${basename##*.}.$file_ext
  basename=${basename%.*}
done
file_ext=${file_ext%.}

echo $file_ext

if [ -d /tmp/$(date +%Y%m%d) ]; then
rm -r /tmp/$(date +%Y%m%d)
fi
mkdir -p /tmp/$(date +%Y%m%d)
temp_path=/tmp/$(date +%Y%m%d)
echo $temp_path
mkdir -p $temp_path/{patch,need_file,backup}
dos2unix $livemd5Add

if [ -d $sourceAdd ]; then
	
	cd $sourceAdd 
  	 	     
	md5deep -rlx $livemd5Add . > $temp_path/need_file/unmatchLocalfiles.txt 
	md5deep -rlm $livemd5Add . > $temp_path/need_file/matchedLocalfiles.txt
	 
        md5deep -rl . > $temp_path/need_file/localmd5.txt
	sort $temp_path/need_file/localmd5.txt > $temp_path/need_file/sorted_localmd5.txt
	sort $livemd5Add > $temp_path/need_file/sorted_livemd5.txt
	diff $temp_path/need_file/sorted_localmd5.txt $temp_path/need_file/sorted_livemd5.txt | grep '>' > $temp_path/need_file/removable.txt 
	diff $temp_path/need_file/sorted_localmd5.txt $temp_path/need_file/sorted_livemd5.txt | grep '<' > $temp_path/need_file/appliable.txt	
	cut -d " " -f4 $temp_path/need_file/appliable.txt > $temp_path/need_file/appliable_final1.txt	
	cut -d " " -f4 $temp_path/need_file/removable.txt > $temp_path/need_file/removable_final1.txt
        cat $temp_path/need_file/removable_final1.txt | grep -v ROOT > $temp_path/need_file/removable_final.txt
        cat $temp_path/need_file/appliable_final1.txt | grep -v ROOT > $temp_path/need_file/appliable_final.txt

	while read line
	do		
    		cp --parents $line $temp_path/patch		

 
	done < $temp_path/need_file/appliable_final.txt

elif [ $file_ext == tar.gz ]; then 	
				
		
		tar -xvf $sourceAdd -C $temp_path 
		base_name=`basename $sourceAdd .tar.gz`	
		echo $base_name	
		cd $temp_path/$base_name
		md5deep -rlx $livemd5Add . > $temp_path/need_file/unmatchLocalfiles.txt 
		md5deep -rlm $livemd5Add . > $temp_path/need_file/matchedLocalfiles.txt 


		md5deep -rl . > $temp_path/need_file/localmd5.txt
	        sort $temp_path/need_file/localmd5.txt > $temp_path/need_file/sorted_localmd5.txt
        	sort $livemd5Add > $temp_path/need_file/sorted_livemd5.txt
        	diff $temp_path/need_file/sorted_localmd5.txt $temp_path/need_file/sorted_livemd5.txt | grep '>' > $temp_path/need_file/removable.txt
       		diff $temp_path/need_file/sorted_localmd5.txt $temp_path/need_file/sorted_livemd5.txt | grep '<' > $temp_path/need_file/appliable.txt
        	cut -d " " -f4 $temp_path/need_file/appliable.txt > $temp_path/need_file/appliable_final.txt
		cut -d " " -f4 $temp_path/need_file/removable.txt > $temp_path/need_file/removable_final.txt
               # cat $temp_path/need_file/removable_final1.txt | grep -v ROOT > $temp_path/need_file/removable_final.txt
                #cat $temp_path/need_file/appliable_final1.txt | grep -v ROOT > $temp_path/need_file/appliable_final.txt

		while read line
		do		
    			cp --parents $line $temp_path/patch		
 
		done < $temp_path/need_file/appliable_final.txt


		

elif [ $file_ext == zip ]; then 	
				
		cd $temp_path
		unzip $sourceAdd  
		base_name=`basename $sourceAdd .zip`	
		echo $base_name	
		cd $temp_path/$base_name
		md5deep -rlx $livemd5Add . > $temp_path/need_file/unmatchLocalfiles.txt 
		md5deep -rlm $livemd5Add . > $temp_path/need_file/matchedLocalfiles.txt 

		md5deep -rl . > $temp_path/need_file/localmd5.txt
                sort $temp_path/need_file/localmd5.txt > $temp_path/need_file/sorted_localmd5.txt
                sort $livemd5Add > $temp_path/need_file/sorted_livemd5.txt
                diff $temp_path/need_file/sorted_localmd5.txt $temp_path/need_file/sorted_livemd5.txt | grep '>' > $temp_path/need_file/removable.txt
                diff $temp_path/need_file/sorted_localmd5.txt $temp_path/need_file/sorted_livemd5.txt | grep '<' > $temp_path/need_file/appliable.txt
                cut -d " " -f4 $temp_path/need_file/appliable.txt > $temp_path/need_file/appliable_final.txt
		cut -d " " -f4 $temp_path/need_file/removable.txt > $temp_path/need_file/removable_final.txt
			while read line
			do		
    				cp --parents $line $temp_path/patch		
 
			done < $temp_path/need_file/appliable_final.txt


	
fi
