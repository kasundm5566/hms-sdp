Building and running the Simulator
=================================================
1. Build the project using 
	  mvn clean assembly:assembly
2. Go to target folder and extract safaricom-ussd-sim-1.0-SNAPSHOT-bin.tar
3. Go to extracted folder and execute
	  java -jar safaricom-ussd-sim-1.0-SNAPSHOT.jar http://127.0.0.1:7821/ ^#[0-9]+#[0-9#]*$
		  Use given regx to validate first ussd request
	        OR
	  java -jar safaricom-ussd-sim-1.0-SNAPSHOT.jar http://127.0.0.1:7821/



Sample Request from Safaricom Ussd Browser
=================================================

GET /?MSISDN=254702122579&SESSION%5FID=111028090718007&SERVICE%5FCODE=%2A383%23&USSD%5FSTRING= HTTP/1.1
Host: 172.31.224.89:7821
Accept: */*

HTTP/1.1 200 OK
Content-Type: text/plain; charset=UTF-8
Content-Length: 120

CONN VCity AppZone
Type Number to select
1.Latest Apps
2.Most Used
3.Search Apps
4.Alert
5.Business
9.List Others
0.BackGET /?MSISDN=254702122579&SESSION%5FID=111028090718007&SERVICE%5FCODE=%2A383%23&USSD%5FSTRING=2 HTTP/1.1
Host: 172.31.224.89:7821
Accept: */*

HTTP/1.1 200 OK
Content-Type: text/plain; charset=UTF-8
Content-Length: 94

CONN Most Used Apps
1.News Alert
2.Inspire
3.transport
4.Baby Care 
5.Jokes
9.More Apps
0.Back