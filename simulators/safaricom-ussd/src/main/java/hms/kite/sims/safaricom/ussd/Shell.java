/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.sims.safaricom.ussd;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

import jline.ArgumentCompletor;
import jline.ConsoleReader;
import jline.SimpleCompletor;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class Shell {

	private HttpRequestSender httpRequestSender;
	private URI ussdConnectorUri;
	private String[] commandsList;
	private Pattern initialMessageFormat;

	public void init() {
		httpRequestSender = new HttpRequestSender();
		httpRequestSender.init();
		commandsList = new String[] { "help", "start", "endSession", "exit" };
	}

	public void run() throws IOException {
		printWelcomeMessage();
		ConsoleReader reader = new ConsoleReader();
		reader.setBellEnabled(false);
		List completors = new LinkedList();

		completors.add(new SimpleCompletor(commandsList));
		reader.addCompletor(new ArgumentCompletor(completors));

		String line;
		PrintWriter out = new PrintWriter(System.out);

		UssdEnvVariables ussdEnviroment = new UssdEnvVariables();

		while ((line = readLine(reader, "")) != null) {
			if (notControlCommand(line)) {
				String messageText = line;
				if (!ussdEnviroment.isSessionStarted()) {
					System.out.println("No Ussd sessions started, type start and press Enter.");

				} else {
					ussdEnviroment.appendCommand(messageText);
					UssdMessage msg = new UssdMessage(ussdEnviroment.getMsisdn(), ussdEnviroment.getServiceCode(),
							ussdEnviroment.getAccumilatedCommandString(), ussdEnviroment.getSessionId());
					httpRequestSender.sendRequest(ussdConnectorUri, msg);
					waitForResponse(msg);
					processResponse(ussdEnviroment, msg);
				}
			} else {
				handlerControlCommand(line, reader, ussdEnviroment);
			}
			out.flush();

		}
	}

	private void handleInitialRequest(ConsoleReader reader, UssdEnvVariables ussdEnviroment) throws IOException {
		promtForSourceMsisdn(reader, ussdEnviroment);
		String messageText = promtForUssdMessage(reader, ussdEnviroment);
		UssdMessage msg = new UssdMessage(ussdEnviroment.getMsisdn(), ussdEnviroment.getServiceCode(), messageText,
				ussdEnviroment.getSessionId());
		ussdEnviroment.appendCommand(messageText);

		if (messageText.trim().isEmpty()) {
			httpRequestSender.sendRequest(ussdConnectorUri, msg);
			waitForResponse(msg);
			processResponse(ussdEnviroment, msg);
		} else {
			int subMenuLevels = StringUtils.countOccurrencesOf(messageText, "*") + 1;
			// Sending duplicate request to mimic the functionality of safaricom
			// ussd.
			for (int i = 0; i < subMenuLevels; i++) {
				httpRequestSender.sendRequest(ussdConnectorUri, msg);
				waitForResponse(msg);
				processResponseForDuplicate(ussdEnviroment, msg);
			}

			httpRequestSender.sendRequest(ussdConnectorUri, msg);
			waitForResponse(msg);
			processResponse(ussdEnviroment, msg);

		}

	}

	private void promtForSourceMsisdn(ConsoleReader reader, UssdEnvVariables ussdEnviroment) throws IOException {
		String line = readLine(reader, "Enter source msisdn:");
		ussdEnviroment.setMsisdn(line);
	}

	private String promtForUssdMessage(ConsoleReader reader, UssdEnvVariables ussdEnviroment) throws IOException {
		String line = readLine(reader, "Enter Ussd message:");

		Matcher m = initialMessageFormat.matcher(line);

		if (!m.matches()) {
			System.out.println("Invalid message. Should be something like #141*123*1*1#");
			return promtForUssdMessage(reader, ussdEnviroment);
		} else {
			// remove first hash
			line = line.substring(1, line.length());
			String serviceCode = null;
			int separatorIndex = 0;
			int starIndex = line.indexOf("*");
			if (starIndex > 0) {
				serviceCode = line.substring(0, starIndex + 1);
				separatorIndex = starIndex;
			} else {
				int hashIndex = line.indexOf("#");
				serviceCode = line.substring(0, hashIndex + 1);
				separatorIndex = hashIndex;
			}
			ussdEnviroment.setServiceCode("#" + serviceCode);
			if (line.length() > separatorIndex + 1) {
				// Ignore last #
				return line.substring(separatorIndex + 1, line.length() - 1);
			} else {
				return "";
			}
			// return line.substring(hashIndex + 1, line.length());
		}
	}

	private void waitForResponse(UssdMessage msg) {
		synchronized (msg) {
			try {
				msg.wait(2000);
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
				System.exit(0);
			}
		}
	}

	private void processResponse(UssdEnvVariables ussdEnviroment, UssdMessage msg) {
		String response = msg.getResponseMessage();
		if (response == null) {
			ussdEnviroment.endSession();
			System.out.println("No response from ussd connector. Ending session.");
		} else {
			if (msg.isLastMessage()) {
				ussdEnviroment.endSession();
				System.out.println(response);
				System.out.println("-----------Session Ended-----------");
			} else {
				System.out.println(response);
			}
		}
	}

	private void processResponseForDuplicate(UssdEnvVariables ussdEnviroment, UssdMessage msg) {
		String response = msg.getResponseMessage();
		if (response == null) {
			ussdEnviroment.endSession();
			System.out.println("No response from ussd connector. Ending session.");
		} else {
			if (msg.isLastMessage()) {
				ussdEnviroment.endSession();
				System.out.println(response);
				System.out.println("-----------Session Ended-----------");
			} else {
				// Do nothing
			}
		}
	}

	private String readLine(ConsoleReader reader, String promtMessage) throws IOException {
		String line = reader.readLine(promtMessage + "\nsim> ");
		return line.trim();
	}

	private void handlerControlCommand(String line, ConsoleReader reader, UssdEnvVariables ussdEnviroment)
			throws IOException {
		if (line.equalsIgnoreCase("exit")) {
			System.out.println("Exiting Ussd Simulator...");
			System.exit(0);
		} else if (line.equalsIgnoreCase("start")) {
			ussdEnviroment.startSession(String.valueOf(System.currentTimeMillis()));
			handleInitialRequest(reader, ussdEnviroment);
		} else if (line.equalsIgnoreCase("endSession")) {
			ussdEnviroment.endSession();
			System.out.println("-----------Session Ended-----------");
		} else if (line.equalsIgnoreCase("help")) {
			printHelp();
		} else {
			System.out.println("Invalid command.");
		}

	}

	private boolean notControlCommand(String line) {
		for (String command : commandsList) {
			if (command.equalsIgnoreCase(line)) {
				return false;
			}
		}
		return true;
	}

	private void printWelcomeMessage() {
		System.out.println("88        88   ad88888ba    ad88888ba   88888888ba,        ad88888ba   88   ");
		System.out.println("88        88  d8\"     \"8b  d8\"     \"8b  88      `\"8b      d8\"     \"8b  \"\" ");
		System.out.println("88        88  Y8,          Y8,          88        `8b     Y8,                     ");
		System.out
				.println("88        88  `Y8aaaaa,    `Y8aaaaa,    88         88     `Y8aaaaa,    88  88,dPYba,,adPYba,  ");
		System.out
				.println("88        88    `\"\"\"\"\"8b,    `\"\"\"\"\"8b,  88         88       `\"\"\"\"\"8b,  88  88P'   \"88\"    \"8a  ");
		System.out
				.println("88        88          `8b          `8b  88         8P             `8b  88  88      88      88  ");
		System.out
				.println("Y8a.    .a8P  Y8a     a8P  Y8a     a8P  88      .a8P      Y8a     a8P  88  88      88      88  ");
		System.out
				.println(" `\"Y8888Y\"'    \"Y88888P\"    \"Y88888P\"   88888888Y\"'        \"Y88888P\"   88  88      88      88  ");
		System.out.println();
		System.out
				.println("Welcome to HMS Safaricom Ussd Simulator. For assistance press TAB or type \"help\" then hit ENTER.");
	}

	private void printHelp() {
		System.out.println("start       - Start new Ussd Session");
		System.out.println("endSession  - End current Ussd Session");
		System.out.println("exit        - Exit the simulator");

	}

	public static void main(String[] args) throws IOException, URISyntaxException {
		if (args.length < 1 || args.length > 2) {
			System.out
					.println("Usage: java -jar safaricom-ussd-sim-1.0-SNAPSHOT.jar <Ussd Connector URL> <InitialMessageFormatRegX>");
			System.out.println("                                 OR");
			System.out.println("Usage: java -jar safaricom-ussd-sim-1.0-SNAPSHOT.jar <Ussd Connector URL>");
			System.out
					.println("Eg: java -jar safaricom-ussd-sim-1.0-SNAPSHOT.jar http://127.0.0.1:7821/ ^#[0-9]+[\\*]*[0-9\\*]*#$");
			System.out.println("");
		} else {

			Shell shell = new Shell();
			URI ussdConnectorUri = new URI(args[0]);
			if (args.length == 2) {
				shell.initialMessageFormat = Pattern.compile(args[1].trim());
			} else {
				shell.initialMessageFormat = Pattern.compile("^#[0-9]+[\\*]*[0-9\\*]*#$");
				System.out
						.println("Using default initial message format[" + shell.initialMessageFormat.pattern() + "]");
			}

			shell.ussdConnectorUri = ussdConnectorUri;
			shell.init();
			shell.run();
		}
	}
}

class UssdEnvVariables {
	private boolean sessionStarted = false;
	private String sessionId = null;
	private String msisdn = null;
	private String serviceCode = null;
	private String accumilatedCommandString = "";

	public boolean isSessionStarted() {
		return sessionStarted;
	}

	public void startSession(String sessionId) {
		this.sessionStarted = true;
		this.sessionId = sessionId;
	}

	public void endSession() {
		this.sessionStarted = false;
		this.sessionId = null;
		this.msisdn = null;
		this.serviceCode = null;
		this.accumilatedCommandString = "";
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public void appendCommand(String currentCommand) {
		if (!currentCommand.trim().isEmpty()) {
			if (this.accumilatedCommandString.isEmpty()) {
				this.accumilatedCommandString = currentCommand;
			} else {
				this.accumilatedCommandString = accumilatedCommandString.concat("*").concat(currentCommand);
			}
		}
	}

	public String getAccumilatedCommandString() {
		return accumilatedCommandString;
	}

}
