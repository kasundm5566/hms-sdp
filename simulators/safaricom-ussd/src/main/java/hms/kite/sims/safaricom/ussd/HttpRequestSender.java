/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.sims.safaricom.ussd;

import static org.jboss.netty.channel.Channels.pipeline;

import java.net.InetSocketAddress;
import java.net.URI;
import java.util.concurrent.Executors;

import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;
import org.jboss.netty.handler.codec.http.DefaultHttpRequest;
import org.jboss.netty.handler.codec.http.HttpChunkAggregator;
import org.jboss.netty.handler.codec.http.HttpClientCodec;
import org.jboss.netty.handler.codec.http.HttpContentDecompressor;
import org.jboss.netty.handler.codec.http.HttpHeaders;
import org.jboss.netty.handler.codec.http.HttpMethod;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.HttpVersion;
import org.jboss.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class HttpRequestSender {

	private static final Logger LOGGER = LoggerFactory.getLogger(HttpRequestSender.class);
	private ClientBootstrap bootstrap;


	public void init() {
		bootstrap = new ClientBootstrap(new NioClientSocketChannelFactory(Executors.newCachedThreadPool(),
				Executors.newCachedThreadPool()));
		bootstrap.setPipelineFactory(new HttpClientPipelineFactory());

		LOGGER.info("Initialized client bootstap.");
	}

	public void stop() {
		bootstrap.releaseExternalResources();
	}

	public void sendRequest(final URI serverUri, final UssdMessage ussdMessage) {
		ChannelFuture future = bootstrap.connect(new InetSocketAddress(serverUri.getHost(), serverUri.getPort()));
		 final String requestContent = ussdMessage.getMessage();
		future.addListener(new ChannelFutureListener() {

			@Override
			public void operationComplete(ChannelFuture cf) throws Exception {
				if (!cf.isSuccess()) {
					LOGGER.error("Unable to connect to [{}]", serverUri, cf.getCause());
				} else {
					Channel channel = cf.getChannel();
					updateRespHandlerWithDetails(channel, ussdMessage);
					HttpRequest request = createRequest(serverUri, ussdMessage);
					LOGGER.info("[{}] Sending request[{}] to [{}]", new Object[] { requestContent, serverUri });
					channel.write(request);
					channel.getCloseFuture().addListener(ChannelFutureListener.CLOSE);
				}

			}

			private void updateRespHandlerWithDetails(Channel channel, UssdMessage ussdMessage) {
				ChannelHandlerContext context = channel.getPipeline().getContext("handler");
				HttpResponseHandler respHander = (HttpResponseHandler) context.getHandler();
				respHander.setSentTime(System.currentTimeMillis());
				respHander.setUssdMessage(ussdMessage);
			}

		});

	}

	private HttpRequest createRequest(final URI serverUri, final UssdMessage ussdMessage) {
		String requestContent = ussdMessage.createGetRequest();
		HttpRequest request = new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, serverUri.getPath()+ requestContent);
		request.setHeader(HttpHeaders.Names.HOST, serverUri.getHost() + ":" + serverUri.getPort());
		request.setHeader(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.CLOSE);
		return request;
	}

}

class HttpClientPipelineFactory implements ChannelPipelineFactory {

	public ChannelPipeline getPipeline() throws Exception {
		ChannelPipeline pipeline = pipeline();
		pipeline.addLast("codec", new HttpClientCodec());
		pipeline.addLast("inflater", new HttpContentDecompressor());
		pipeline.addLast("aggregator", new HttpChunkAggregator(1048576));
		pipeline.addLast("handler", new HttpResponseHandler());
		return pipeline;
	}
}