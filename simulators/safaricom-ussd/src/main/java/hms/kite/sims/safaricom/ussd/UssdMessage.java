/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.sims.safaricom.ussd;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.text.MessageFormat;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class UssdMessage {

	private static final String requestTemplate = "?MSISDN={0}&SESSION_ID={1}&SERVICE_CODE={2}&USSD_STRING={3}";
	private String sourceMsisdn;
	private String destShortCode;
	private String message;
	private String sessionId;
	private String responseMessage;
	private boolean isLastMessage;

	public UssdMessage(String sourceMsisdn, String destShortCode, String message, String sessionId) {
		super();
		this.sourceMsisdn = sourceMsisdn;
		this.destShortCode = destShortCode;
		this.message = message;
		this.sessionId = sessionId;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		if (responseMessage.startsWith("CON")) {
			isLastMessage = false;
			this.responseMessage = responseMessage.substring(4, responseMessage.length());
		} else if (responseMessage.startsWith("END")) {
			isLastMessage = true;
			this.responseMessage = responseMessage.substring(4, responseMessage.length());
		} else {
			throw new IllegalArgumentException("Invalid response[" + responseMessage + "] from ussd connector");
		}
	}

	public String getMessage() {
		return message;
	}

	public String getSessionId() {
		return sessionId;
	}

	public boolean isLastMessage() {
		return isLastMessage;
	}

	public void setLastMessage(boolean isLastMessage) {
		this.isLastMessage = isLastMessage;
	}

	public String createGetRequest() {
		try {
			return MessageFormat.format(requestTemplate, sourceMsisdn, sessionId, destShortCode, URLEncoder.encode(message, "utf-8"));
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

}
