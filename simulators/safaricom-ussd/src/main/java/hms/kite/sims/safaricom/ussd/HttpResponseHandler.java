/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.sims.safaricom.ussd;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelHandler;
import org.jboss.netty.handler.codec.http.HttpResponse;
import org.jboss.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class HttpResponseHandler extends SimpleChannelHandler {
	private static final Logger LOGGER = LoggerFactory.getLogger(HttpResponseHandler.class);
	private UssdMessage ussdMessage;
	private long sentTime;

	public void setSentTime(long sentTime) {
		this.sentTime = sentTime;
	}


	public void setUssdMessage(UssdMessage ussdMessage) {
		this.ussdMessage = ussdMessage;
	}

	@Override
	public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
		HttpResponse response = (HttpResponse) e.getMessage();
		ChannelBuffer content = response.getContent();
		if (content.readable()) {
			ussdMessage.setResponseMessage(content.toString(CharsetUtil.UTF_8));
			synchronized (ussdMessage) {
				ussdMessage.notify();
			}
		} else {
			LOGGER.info("[{}] Resonse from SDP[{}] took [{}]ms [{}]",
					new Object[] { response.getStatus(), (System.currentTimeMillis() - sentTime),
							"Response content not readable" });
		}

	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SdpResponseHandler [");
		builder.append("]");
		return builder.toString();
	}

}
