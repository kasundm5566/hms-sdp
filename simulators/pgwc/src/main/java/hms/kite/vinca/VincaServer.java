/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.vinca;

import hms.common.rest.util.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import java.util.HashMap;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@Produces({"application/json", "application/xml"})
@Consumes({"application/json", "application/xml"})
@Path("/sms/")
public class VincaServer {
    private static final Logger LOGGER = LoggerFactory.getLogger(VincaServer.class);

    @Context
    private org.apache.cxf.jaxrs.ext.MessageContext mc;
    @Context
    private ServletContext sc;

    private String status;
    private String statusCode;
    private String statusDescription;

    @Produces({"application/json", "application/xml"})
    @Consumes({"application/json", "application/xml"})
    @POST
    @Path("/send")
    public Message sendService(Message msg, @Context javax.servlet.http.HttpServletRequest request) {
        LOGGER.debug("Message received[" + msg.getParams() + "]");
        Map<String, Object> resp = new HashMap<String, Object>();
        resp.put(statusK, status);
        resp.put(statusCodeK, statusCode);
        resp.put(statusDescriptionK, statusDescription);

        Message respMsg = new Message();
        respMsg.putAll((Map) resp);
        return respMsg;
    }

    @Produces({"application/x-www-form-urlencoded", "multipart/form-data"})
    @Consumes({"application/x-www-form-urlencoded", "multipart/form-data"})
    @POST
    @Path("/send")
    public Message sendServiceSimple(Message msg,
                                     @Context javax.servlet.http.HttpServletRequest request) {
        LOGGER.debug("Message received[" + msg.getParams() + "]");
        Map<String, Object> resp = new HashMap<String, Object>();
        resp.put(statusK, status);
        resp.put(statusCodeK, statusCode);
        resp.put(statusDescriptionK, statusDescription);

        Message respMsg = new Message();
        respMsg.putAll((Map) resp);
        return respMsg;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }
}
