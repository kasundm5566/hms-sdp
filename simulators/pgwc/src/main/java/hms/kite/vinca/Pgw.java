/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.vinca;

import hms.pgw.api.common.PayInstrumentInfo;
import hms.pgw.api.request.PayInstrumentQueryMessage;
import hms.pgw.api.response.PayInstrumentResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@Produces({"application/json", "application/xml"})
@Consumes({"application/json", "application/xml"})
@Path("/pgwservice/")
public class Pgw {

    private static final Logger logger = LoggerFactory.getLogger(Pgw.class);

    @POST
    @Path("/payInstrumentQuery")
    public PayInstrumentResponse queryInstruments(PayInstrumentQueryMessage payInstrumentQueryMessage) {
        PayInstrumentResponse payInstrumentResponse = new PayInstrumentResponse();

        payInstrumentResponse.setUserId(payInstrumentQueryMessage.getUserId());
        PayInstrumentInfo[] paymentInstrumentInfo = new PayInstrumentInfo[5];

        PayInstrumentInfo account1 = new PayInstrumentInfo();
        account1.setAccountCurrecy("USD");
        account1.setDefault(false);
        account1.setPayInsAccountId("account1");
        account1.setPayInstrumentId(12345);
        account1.setPayInstrumentName("my-bank-1");

        PayInstrumentInfo account2 = new PayInstrumentInfo();
        account2.setAccountCurrecy("USD");
        account2.setDefault(true);
        account2.setPayInsAccountId("account2");
        account2.setPayInstrumentId(12345);
        account2.setPayInstrumentName("my-bank-1");

        PayInstrumentInfo account3 = new PayInstrumentInfo();
        account3.setAccountCurrecy("USD");
        account3.setDefault(false);
        account3.setPayInsAccountId("account3");
        account3.setPayInstrumentId(12345);
        account3.setPayInstrumentName("my-bank-1");


        PayInstrumentInfo account4 = new PayInstrumentInfo();
        account4.setAccountCurrecy("USD");
        account4.setDefault(false);
        account4.setPayInsAccountId("account4");
        account4.setPayInstrumentId(23456);
        account4.setPayInstrumentName("your-bank-1");

        PayInstrumentInfo account5 = new PayInstrumentInfo();
        account5.setAccountCurrecy("USD");
        account5.setDefault(false);
        account5.setPayInsAccountId("account5");
        account5.setPayInstrumentId(23456);
        account5.setPayInstrumentName("yourbankc-bank-2");


        paymentInstrumentInfo[0] = account1;
        paymentInstrumentInfo[1] = account2;
        paymentInstrumentInfo[2] = account3;
        paymentInstrumentInfo[3] = account4;
        paymentInstrumentInfo[4] = account5;


        payInstrumentResponse.setPayInstrumentInfos(paymentInstrumentInfo);

        return payInstrumentResponse;
    }
}
