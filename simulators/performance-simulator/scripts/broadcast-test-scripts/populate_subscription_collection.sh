#!/bin/bash

#Source the parameters to the script
source parameter_set.txt;

#iterator
i=1;

#Create json file with subscription json objects
while [ $i -le $iterations ]
do

 if [ $i -le 9 ]; then
  echo "{\"status\":\"S1000\",\"frequency\":\"null\",\"current-status\":\"REGISTERED\",\"app-id\":\"$app_id\",\"sp-id\":\"$sp_id\",\"app-name\":\"$app_name\",\"charging-amount\":NumberLong(0),\"msisdn\":\"9477635100$i\",\"mandate-id\":\"-1\",\"keyword\":\"$keyword\",\"shortcode\":\"$short_code\",\"last-modified-date\":{\"\$date\":1334906378947},\"app-status\":\"active-production\",\"coop-user-name\":\"$sp_name\",\"created-date\":{\"\$date\":1334906378947},\"operator\":\"$operator\"}" >> temp.json;
 
 else
  if [ $i -le 99 ]; then
   echo "{\"status\":\"S1000\",\"frequency\":\"null\",\"current-status\":\"REGISTERED\",\"app-id\":\"$app_id\",\"sp-id\":\"$sp_id\",\"app-name\":\"$app_name\",\"charging-amount\":NumberLong(0),\"msisdn\":\"947763510$i\",\"mandate-id\":\"-1\",\"keyword\":\"$keyword\",\"shortcode\":\"$short_code\",\"last-modified-date\":{\"\$date\":1334906378947},\"app-status\":\"active-production\",\"coop-user-name\":\"$sp_name\",\"created-date\":{\"\$date\":1334906378947},\"operator\":\"$operator\"}" >> temp.json;
 
  else
   if [ $i -le 999 ]; then
    echo "{\"status\":\"S1000\",\"frequency\":\"null\",\"current-status\":\"REGISTERED\",\"app-id\":\"$app_id\",\"sp-id\":\"$sp_id\",\"app-name\":\"$app_name\",\"charging-amount\":NumberLong(0),\"msisdn\":\"94776351$i\",\"mandate-id\":\"-1\",\"keyword\":\"$keyword\",\"shortcode\":\"$short_code\",\"last-modified-date\":{\"\$date\":1334906378947},\"app-status\":\"active-production\",\"coop-user-name\":\"$sp_name\",\"created-date\":{\"\$date\":1334906378947},\"operator\":\"$operator\"}" >> temp.json;
  
   else
     echo "Iterator is exceeded";
     i=$iterations;
   fi
  fi
 fi

((i++));

done
echo "Json file was generated...";

#Get mongo export for existing subscription collection
mongoexport -d subscription -c subscription > existing_subscription_collection.json
echo "Existing mongo subscription collection was exported to existing_subscription_collection.json file";


#Import the new subscription values to mongo db
echo "db.subscription.remove()" > tmp.txt;
echo "exit" >> tmp.txt;
mongo subscription < tmp.txt;

mongoimport -d subscription -c subscription --file temp.json;

#Remove temporary files
rm tmp.txt temp.json
