##BroadCast sms traffic

    1. For this, we need valid application in mongo collection with subscription and sms ncs, sdp-server, sbl-server, smpp-sim
    2. Before using scripts, you need to add the parameters to parameter_set.txt file regarding the created application
        #File path
            scripts/broadcast-test/parameter_set.txt
        #Parameter description
            app_id      : <application id, that was given by sdp when application creation>
            app_name    : <application name, that was given by application creator>
            app_password: <application password, that was given by sdp when application creation>
            keyword     : <keyword of the application, that was given by user when application creation>
            short_code  : <short code of the application, that was given by user when application creation>

            sp_id       : <service provider id, that was given by sdp, when the provisioning-user creation>
            sp_name     : <service provider name, that was given by user, when the provisioning-user creation>

            operator    : <operator you using to test - eg: dialog, vcity>

            sdp_url     : <sdp sms-mt receiving url>

    3. Give the executable permissions to script file

        chmod +x <file-name>.sh

    4. Using the populate_subscription_collection.sh script, we can populate subscription collection
        #File path
            scripts/broadcast-test/populate_subscription_collection.sh
        #Using
            Run the script to populate the subscription collection
                ./populate_subscription_collection.sh
        (this add subscription records, that you configured in parameter_set.txt under iterations parameter)
        #Outputs
            This will create a file called existing_subscription_collection.json. This file created for existing mongo subscription
            data, that before we import the created subscription data. (Keep this file in separate location, if you need to restore the old data)

    5. If you need to send sms-mt request from the application one time then run sms-mt.sh script
        #File path
            scripts/broadcast-test/sms-mt.sh
        #Using
            Run the script to send sms-mt request to sdp
                ./sms-mt.sh

    6. If you need to send sms-mt request rapidly from the application then run loop-sms-mt.sh script
        #File path
            scripts/broadcast-test/loop-sms-mt.sh
        #Using
            Run the script to send sms-mt request rapidly to sdp
                ./loop-sms-mt.sh