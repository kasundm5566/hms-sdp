/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hms.kite.sim.perf.server;

import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFactory;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.DefaultChannelGroup;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.jboss.netty.handler.codec.http.HttpChunkAggregator;
import org.jboss.netty.handler.codec.http.HttpRequestDecoder;
import org.jboss.netty.handler.codec.http.HttpResponseEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public class SdpRequestReceivingServer {

	private static final Logger LOGGER = LoggerFactory.getLogger(SdpRequestReceivingServer.class);
	public final static AtomicInteger receivedMessageCount = new AtomicInteger(0);
	public final static ChannelGroup connectedClienChannels = new DefaultChannelGroup();
	private final ExecutorService bossTPool = Executors.newCachedThreadPool();
	private final ExecutorService workerTPool = Executors.newCachedThreadPool();
	private ServerBootstrap serverBootstrap;
	private Channel channel;
	private ResponseSelector responseSelector;
	private int port;

	public void setResponseSelector(ResponseSelector responseSelector) {
		this.responseSelector = responseSelector;
	}

	public void start() {
		ChannelFactory channelFactory = new NioServerSocketChannelFactory(bossTPool, workerTPool);
		serverBootstrap = new ServerBootstrap(channelFactory);
		serverBootstrap.setPipelineFactory(new HttpServerPipelineFactory());
		serverBootstrap.setOption("child.tcpNoDelay", true);
		serverBootstrap.setOption("child.keepAlive", true);
		channel = serverBootstrap.bind(new InetSocketAddress(port));
		LOGGER.info("Started Server on Port[{}]", port);
	}

	public void stop() {
		channel.disconnect();
		ChannelFuture cf = channel.close();
		try {
			cf.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		SdpRequestReceivingServer.connectedClienChannels.disconnect();

		// serverBootstrap.getFactory().releaseExternalResources();
		bossTPool.shutdownNow();
		workerTPool.shutdownNow();
		LOGGER.info("Stoped server");
	}

	public void setPort(int port) {
		this.port = port;
	}

	class HttpServerPipelineFactory implements ChannelPipelineFactory {

		@Override
		public ChannelPipeline getPipeline() throws Exception {
			ChannelPipeline channelPipeline = Channels.pipeline();

			channelPipeline.addLast("decoder", new HttpRequestDecoder());
			channelPipeline.addLast("encoder", new HttpResponseEncoder());
			channelPipeline.addLast("aggregator", new HttpChunkAggregator(1048576));
			channelPipeline.addLast("handler", new SdpRequestHandler(responseSelector));
			return channelPipeline;
		}

	}
}