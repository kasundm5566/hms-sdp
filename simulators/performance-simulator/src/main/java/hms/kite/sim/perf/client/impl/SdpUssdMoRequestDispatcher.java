package hms.kite.sim.perf.client.impl;

import hms.kite.sim.perf.client.HttpRequestSender;
import hms.kite.sim.perf.client.ReqDispatcher;
import hms.kite.sim.perf.client.RequestDataFileReader;
import hms.kite.sim.perf.util.StatisticsHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 10/5/12
 * Time: 11:47 AM
 * To change this template use File | Settings | File Templates.
 */
public class SdpUssdMoRequestDispatcher implements ReqDispatcher{

    private static final Logger LOGGER  = LoggerFactory.getLogger(SdpUssdMoRequestDispatcher.class);
    private String sampleUssdRequestFile;
    private RequestDataFileReader fileReader;
    private HttpRequestSender httpRequestSender;
    private URI requestSendingUri;
    private String ncs;
    private List<String> sampleRequest;
    private StatisticsHandler statHandler;

    private final AtomicInteger lastSentIndex = new AtomicInteger(-1);
    private String requestSendingUrl;

    public void init() {

        sampleRequest = fileReader.readFile(sampleUssdRequestFile);
        LOGGER.info("Sample ussd Request : [{}]", sampleRequest);
        try{
            requestSendingUri = new URI(requestSendingUrl);
        } catch (URISyntaxException e){
            throw new RuntimeException();
        }

        httpRequestSender = new HttpRequestSender(statHandler);
        httpRequestSender.init();
        LOGGER.info("Initialized sdp ussd request dispatcher");
    }

    @Override
    public void sendNext() {

        int nextIndex = lastSentIndex.incrementAndGet();
        if(nextIndex >= sampleRequest.size()){
            lastSentIndex.set(0);
            nextIndex = 0;
        }
        String requestContent = /*ncs + ">" +*/ sampleRequest.get(nextIndex);
        try{
            httpRequestSender.sendGetRequest(requestSendingUri, requestContent);
            statHandler.moSend(ncs);
        }catch (Exception ex){
            LOGGER.error("Exception while sending [" + ncs + "]mo", ex);
            statHandler.moSendFailed(ncs);
        }
    }

    public void setSampleUssdRequestFile(String sampleUssdRequestFile) {
        this.sampleUssdRequestFile = sampleUssdRequestFile;
    }

    public void setFileReader(RequestDataFileReader fileReader) {
        this.fileReader = fileReader;
    }

    public void setNcs(String ncs) {
        this.ncs = ncs;
    }

    public void setStatHandler(StatisticsHandler statHandler) {
        this.statHandler = statHandler;
    }

    public void setRequestSendingUrl(String requestSendingUrl) {
        this.requestSendingUrl = requestSendingUrl;
    }
}
