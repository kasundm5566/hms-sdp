/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.sim.perf.client;

import hms.kite.sim.perf.util.StatisticsHandler;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelHandler;
import org.jboss.netty.handler.codec.http.HttpResponse;
import org.jboss.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class SdpSmsMtResponseHandler extends SimpleChannelHandler {
	private static final String SMS_NCS = "sms";
	private static final Logger LOGGER = LoggerFactory.getLogger(SdpSmsMtResponseHandler.class);
	private long correlationId;
	private long sentTime;
	private StatisticsHandler statHandler;

	public void setCorrelationId(long correlationId) {
		this.correlationId = correlationId;
	}

	public void setSentTime(long sentTime) {
		this.sentTime = sentTime;
	}

	public void setStatHandler(StatisticsHandler statHandler) {
		this.statHandler = statHandler;
	}

	@Override
	public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
		HttpResponse response = (HttpResponse) e.getMessage();
		ChannelBuffer content = response.getContent();
		if (content.readable()) {
			LOGGER.info("[{}] Resonse from SDP[{}] took [{}]ms [{}]",
					new Object[] { correlationId, response.getStatus(), (System.currentTimeMillis() - sentTime),
							content.toString(CharsetUtil.UTF_8) });
			processResponse(content.toString(CharsetUtil.UTF_8));
		} else {
			LOGGER.info("[{}] Resonse from SDP[{}] took [{}]ms [{}]",
					new Object[] { correlationId, response.getStatus(), (System.currentTimeMillis() - sentTime),
							"Response content not readable" });
		}

	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SdpResponseHandler [");
		builder.append(correlationId);
		builder.append("]");
		return builder.toString();
	}

	private void processResponse(String responseTxt) {
		/*
		 * { "statusCode": "E1319", "statusDescription":
		 * "Transaction per day limit exceeded", "destinationResponses": [ {
		 * "statusCode": "E1319", "statusDescription":
		 * "Transaction per day limit exceeded", "address": "tel:254712323654",
		 * "messageId": "1316399357122" } ], "messageId": "1316399357122",
		 * "version": "1.0" }
		 */

		String[] items = responseTxt.split(",");
		if (items.length > 0) {
			String status = items[0];
			String[] statusItems = status.split(":");
			String statusCode = statusItems[1];
			statHandler.mtRespReceived(SMS_NCS, statusCode);
		}
	}

}