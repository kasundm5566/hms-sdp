/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.sim.perf.control;

import hms.kite.sim.perf.client.FlowController;
import hms.kite.sim.perf.util.StatisticsHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * http://localhost:10500/perfsim/sms/mt/stop
 * http://localhost:10500/perfsim/sms/mt/start/1
 * http://localhost:10500/perfsim/sms/mt/tps/5
 *
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class PerformanceControler {
	private static final Logger LOGGER = LoggerFactory.getLogger(PerformanceControler.class);
	private Map<String, FlowController> mtFlowControllerMap;
	private StatisticsHandler statLogger;

	public String execute(String requestPath) {
		try {
			Command command = new Command(requestPath);
			LOGGER.info("Control command received[{}]", command);
			FlowController flowController = mtFlowControllerMap.get(command.getNcs());
			if (flowController == null) {
				return "FlowControler not specified for ncs[" + command.getNcs() + "]";
			}

            //MT Step
			if ("mt-start".equals(command.getCommand())) {
				if (command.getAdditionalInfo().isEmpty()) {
					return "Invalid Command[" + command.getCommand() + "], tps value should be specified.";
				}
				flowController.start(getTpsValue(command.getAdditionalInfo().get(0)));

			} else if ("mt-stop".equals(command.getCommand())) {
				flowController.stop();

			} else if ("mt-tps".equals(command.getCommand())) {
				if (command.getAdditionalInfo().isEmpty()) {
					return "Invalid Command[" + command.getCommand() + "], tps value should be specified.";
				}
				flowController.changeTps(getTpsValue(command.getAdditionalInfo().get(0)));
			} else if ("mt-next".equals(command.getCommand())) {
				flowController.sendNext();
			} else if ("mt-viewtps".equals(command.getCommand())) {
				return flowController.viewCurrentConfiguration();
			} else if ("mt-stats".equals(command.getCommand())) {
				return statLogger.printStatLog();
			} else if ("mt-reset".equals(command.getCommand())) {
				return statLogger.reset();

            //MO Step
			} else if("mo-start".equals(command.getCommand())){
                if (command.getAdditionalInfo().isEmpty()) {
                    return "Invalid Command[" + command.getCommand() + "], tps value should be specified.";
                }
                flowController.start(getTpsValue(command.getAdditionalInfo().get(0)));
            } else if("mo-stop".equals(command.getCommand())){
                flowController.stop();
            } else if("mo-tps".equals(command.getCommand())){
                if (command.getAdditionalInfo().isEmpty()) {
                    return "Invalid Command[" + command.getCommand() + "], tps value should be specified.";
                }
                flowController.changeTps(getTpsValue(command.getAdditionalInfo().get(0)));
            } else if ("mo-next".equals(command.getCommand())) {
                flowController.sendNext();
            } else if ("mo-viewtps".equals(command.getCommand())) {
                return flowController.viewCurrentConfiguration();
            } else if ("mo-stats".equals(command.getCommand())) {
                return statLogger.printStatLog();
            } else if ("mo-reset".equals(command.getCommand())) {
                return statLogger.reset();

            //Ao Step
            } else if("ddqb-start".equals(command.getCommand())){
                if(command.getAdditionalInfo().isEmpty()){
                    return "Invalid Command[" + command.getCommand() + "], tps value should be specified.";
                }
                flowController.start(getTpsValue(command.getAdditionalInfo().get(0)));
            } else if("ddqb-stop".equals(command.getCommand())){
                flowController.stop();
            } else if("ddqb-tps".equals(command.getCommand())){
                if (command.getAdditionalInfo().isEmpty()) {
                    return "Invalid Command[" + command.getCommand() + "], tps value should be specified.";
                }
                flowController.changeTps(getTpsValue(command.getAdditionalInfo().get(0)));
            } else if ("ddqb-next".equals(command.getCommand())) {
                flowController.sendNext();
            } else if ("ddqb-viewtps".equals(command.getCommand())) {
                return flowController.viewCurrentConfiguration();
            } else if ("dbqb-stats".equals(command.getCommand())) {
                return statLogger.printStatLog();
            } else if ("ddqb-reset".equals(command.getCommand())) {
                return statLogger.reset();
            }
            else {
				return "Invalid Command[" + command.getCommand() + "]";
			}

			return "Success";
		} catch (Exception e) {
			return e.getMessage();
		}
	}

	public void setMtFlowControllerMap(Map<String, FlowController> mtFlowControllerMap) {
		this.mtFlowControllerMap = mtFlowControllerMap;
	}

	public void setStatLogger(StatisticsHandler statLogger) {
		this.statLogger = statLogger;
	}

	private float getTpsValue(String str) {
		float tps = Float.parseFloat(str);
		if (tps > 0) {
			return tps;
		} else {
			throw new IllegalArgumentException("Invalid tps value[" + tps + "], should be a positive");
		}
	}

	private class Command {
		private final String ncs;
		private final String command;
		private List<String> additionalInfo = new ArrayList<String>();

		public Command(String requestPath) {
			String[] requestPathComponents = requestPath.split("/");
			if (requestPathComponents.length < 5) {
				throw new IllegalArgumentException("Invalid command");
			}

			LOGGER.info("Control command[{}][{}]", requestPath, Arrays.toString(requestPathComponents));

			ncs = requestPathComponents[2];
			command = requestPathComponents[3] + "-" + requestPathComponents[4];
			if (requestPathComponents.length > 5) {
				for (int i = 5; i < requestPathComponents.length; i++) {
					additionalInfo.add(requestPathComponents[i]);
				}
			}
		}

		public String getNcs() {
			return ncs;
		}

		public String getCommand() {
			return command;
		}

		public List<String> getAdditionalInfo() {
			return additionalInfo;
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("Command [ncs=");
			builder.append(ncs);
			builder.append(", command=");
			builder.append(command);
			builder.append(", additionalInfo=");
			builder.append(additionalInfo);
			builder.append("]");
			return builder.toString();
		}

	}
}
