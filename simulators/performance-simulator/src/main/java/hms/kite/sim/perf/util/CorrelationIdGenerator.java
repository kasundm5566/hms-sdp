/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.sim.perf.util;

import java.text.DecimalFormat;
import java.util.Date;

import com.ibm.icu.text.SimpleDateFormat;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class CorrelationIdGenerator {
	private static int txIdIncrmentator = 0;
	private static final String DATE_FORMAT_CORRELATION_ID = "yyMMddHHmm";
	private static final String DECIMAL_FORMAT_CORRELATION_ID = "0000";

	/**
	 * Creates a long value prefixed by serverId. Value is unique within a
	 * single jvm.
	 *
	 * @param serverId
	 * @return
	 */
	public static long getCorrelationId() {
		int localIndex;
		synchronized (CorrelationIdGenerator.class) {
			if (txIdIncrmentator >= 9999) {
				txIdIncrmentator = 0;
			}
			localIndex = ++txIdIncrmentator;
		}
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT_CORRELATION_ID);
		Date date = new Date();
		StringBuilder sb = new StringBuilder(50);
		sb.append(1);
		sb.append(formatter.format(date));

		DecimalFormat df = new DecimalFormat(DECIMAL_FORMAT_CORRELATION_ID);
		sb.append(df.format(localIndex));

		return Long.parseLong(sb.toString());
	}

}
