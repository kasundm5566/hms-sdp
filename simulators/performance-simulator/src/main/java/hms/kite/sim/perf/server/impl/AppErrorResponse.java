/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.sim.perf.server.impl;

import java.util.List;
import java.util.Random;

import org.apache.bcel.generic.NEW;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.HttpResponseStatus;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class AppErrorResponse extends AbstractResponse {

	private List<String> errorResponses;

	@Override
	public void execute(MessageEvent e, HttpRequest request) {
		String response = errorResponses.get(randGenerator.nextInt(errorResponses.size()));
		LOGGER.info("Sending App Error Response[{}]", response);
		sendResponse(e, request, response, HttpResponseStatus.OK);
	}

	public void setErrorResponses(List<String> errorResponses) {
		this.errorResponses = errorResponses;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AppErrorResponse [errorResponses=");
		builder.append(errorResponses);
		builder.append("]");
		return builder.toString();
	}

}
