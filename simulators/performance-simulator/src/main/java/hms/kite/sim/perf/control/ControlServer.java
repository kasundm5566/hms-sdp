/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.sim.perf.control;

import hms.kite.sim.perf.server.SdpRequestReceivingServer;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.*;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.DefaultChannelGroup;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.jboss.netty.handler.codec.http.HttpChunkAggregator;
import org.jboss.netty.handler.codec.http.HttpRequestDecoder;
import org.jboss.netty.handler.codec.http.HttpResponseEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class ControlServer {
	private static final Logger LOGGER = LoggerFactory.getLogger(ControlServer.class);
	public final static ChannelGroup connectedClienChannels = new DefaultChannelGroup();
	private final ExecutorService bossTPool = Executors.newCachedThreadPool();
	private final ExecutorService workerTPool = Executors.newCachedThreadPool();
	private ServerBootstrap serverBootstrap;
	private Channel channel;
	private int port;
	private PerformanceControler controler;

	public void start() {
		ChannelFactory channelFactory = new NioServerSocketChannelFactory(bossTPool, workerTPool);
		serverBootstrap = new ServerBootstrap(channelFactory);
		serverBootstrap.setPipelineFactory(new HttpControlServerPipelineFactory());
		serverBootstrap.setOption("child.tcpNoDelay", true);
		serverBootstrap.setOption("child.keepAlive", true);
		channel = serverBootstrap.bind(new InetSocketAddress(port));
		LOGGER.info("Started Control Server on Port[{}]", port);
	}

	public void stop() {
		channel.disconnect();
		ChannelFuture cf = channel.close();
		try {
			cf.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		SdpRequestReceivingServer.connectedClienChannels.disconnect();

		// serverBootstrap.getFactory().releaseExternalResources();
		bossTPool.shutdownNow();
		workerTPool.shutdownNow();
		LOGGER.info("Stoped Control Server");
	}

	public void setPort(int port) {
		this.port = port;
	}

	public void setControler(PerformanceControler controler) {
		this.controler = controler;
	}

	class HttpControlServerPipelineFactory implements ChannelPipelineFactory {

		@Override
		public ChannelPipeline getPipeline() throws Exception {
			ChannelPipeline channelPipeline = Channels.pipeline();

			channelPipeline.addLast("decoder", new HttpRequestDecoder());
			channelPipeline.addLast("encoder", new HttpResponseEncoder());
			channelPipeline.addLast("aggregator", new HttpChunkAggregator(1048576));
			channelPipeline.addLast("handler", new ControlRequestHandler(controler));
			return channelPipeline;
		}

	}
}
