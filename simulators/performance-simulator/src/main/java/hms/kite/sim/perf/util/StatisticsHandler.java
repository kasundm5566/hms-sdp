/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hms.kite.sim.perf.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public class StatisticsHandler {
	private static final Logger LOGGER = LoggerFactory.getLogger("Statistics");
	private static final String TOTAL_SENT = "Sent";
	private static final String SENT_FAILED = "Sending-fail";
	private final Map<String, AtomicInteger> moStatHolder = new HashMap<String, AtomicInteger>();
	private final Map<String, Map<String, AtomicInteger>> mtStatHolder = new HashMap<String, Map<String, AtomicInteger>>();
    private final Map<String, Map<String, AtomicInteger>> ussdMoStatHolder = new HashMap<String, Map<String, AtomicInteger>>();
    private final Map<String, Map<String, AtomicInteger>> caasAoStatHolder = new HashMap<String, Map<String, AtomicInteger>>();
	private int statPrintingInterval = 5000;

	public void init() {
		LOGGER.info("Initialized Stat printing with print interval of[{}]ms", statPrintingInterval);
		Thread t = new Thread(new StatLogger());
		t.start();
	}

	public int moReceived(String ncs) {
		AtomicInteger counter = moStatHolder.get(ncs);
		if (counter != null) {
			return counter.incrementAndGet();
		} else {
			counter = new AtomicInteger(1);
			moStatHolder.put(ncs, counter);
			return counter.get();
		}
	}


    //MO Request
    public int moSend(String ncs){
        Map<String, AtomicInteger> moCounters = initMoStatHolder(ncs);
        return updateMoStat(TOTAL_SENT, moCounters);
    }

    public int moSendFailed(String ncs){
        Map<String, AtomicInteger> moCounters = initMoStatHolder(ncs);
        return updateMoStat(SENT_FAILED, moCounters);
    }

    public int moRespReceive(String ncs, String statusCode){
        Map<String, AtomicInteger> moCounters = initMoStatHolder(ncs);
        return updateMoStat(statusCode, moCounters);
    }

    private int updateMoStat(final String statusCode, Map<String, AtomicInteger> moCounters) {
        AtomicInteger counter = moCounters.get(statusCode);
        if (counter != null) {
            return counter.incrementAndGet();
        } else {
            counter = new AtomicInteger(1);
            moCounters.put(statusCode, counter);
            return counter.get();
        }
    }

    private Map<String, AtomicInteger> initMoStatHolder(String ncs) {
        Map<String, AtomicInteger> moCounters = ussdMoStatHolder.get(ncs);

        if(moCounters == null){
            moCounters = new HashMap<String, AtomicInteger>();
            ussdMoStatHolder.put(ncs, moCounters);
        }
        return moCounters;
    }



    //AO Request
    public int aoSend(String ncs){
        Map<String, AtomicInteger> aoCounters = initAoStatHolder(ncs);
        return updateAoStat(TOTAL_SENT, aoCounters);
    }

    public int aoSendFailed(String ncs){
        Map<String, AtomicInteger> aoCounters = initAoStatHolder(ncs);
        return updateAoStat(SENT_FAILED, aoCounters);
    }

    private int updateAoStat(final String statusCode, Map<String, AtomicInteger> aoCounters) {
        AtomicInteger counter = aoCounters.get(statusCode);
        if (counter != null) {
            return counter.incrementAndGet();
        } else {
            counter = new AtomicInteger(1);
            aoCounters.put(statusCode, counter);
            return counter.get();
        }
    }

    private Map<String, AtomicInteger> initAoStatHolder(String ncs) {
        Map<String, AtomicInteger> aoCounters = caasAoStatHolder.get(ncs);

        if(aoCounters == null){
            aoCounters = new HashMap<String, AtomicInteger>();
            ussdMoStatHolder.put(ncs, aoCounters);
        }
        return aoCounters;
    }




    //MT Request
    public int mtSendFailed(String ncs) {
		Map<String, AtomicInteger> mtCounters = initMtStatHolder(ncs);
		return updateMtStat(SENT_FAILED, mtCounters);
	}

	public int mtSent(String ncs) {
		Map<String, AtomicInteger> mtCounters = initMtStatHolder(ncs);

		return updateMtStat(TOTAL_SENT, mtCounters);
	}

	public int mtRespReceived(String ncs, String statusCode) {
		Map<String, AtomicInteger> mtCounters = initMtStatHolder(ncs);

		return updateMtStat(statusCode, mtCounters);
	}

	public String reset() {
		String stats = printStatLog();
		moStatHolder.clear();
		mtStatHolder.clear();
        ussdMoStatHolder.clear();
		return "Resetted stats, Stats befor resetting \n" + stats;
	}

	public void setStatPrintingInterval(int statPrintingInterval) {
		this.statPrintingInterval = statPrintingInterval;
	}

	private int updateMtStat(final String statusCode, final Map<String, AtomicInteger> mtCounters) {
		AtomicInteger counter = mtCounters.get(statusCode);
		if (counter != null) {
			return counter.incrementAndGet();
		} else {
			counter = new AtomicInteger(1);
			mtCounters.put(statusCode, counter);
			return counter.get();
		}
	}

	private Map<String, AtomicInteger> initMtStatHolder(String ncs) {
		Map<String, AtomicInteger> mtCounters = mtStatHolder.get(ncs);
		if (mtCounters == null) {
			mtCounters = new HashMap<String, AtomicInteger>();
			mtStatHolder.put(ncs, mtCounters);
		}
		return mtCounters;
	}

	public String printStatLog() {
		StringBuilder sb = new StringBuilder(300);
		sb.append("MO[");
		for (Entry<String, AtomicInteger> stat : moStatHolder.entrySet()) {
			sb.append("{").append(stat.getKey()).append("-").append(stat.getValue()).append("}");
		}
		sb.append("] MT[");
		for (Entry<String, Map<String, AtomicInteger>> statsMap : mtStatHolder.entrySet()) {
			sb.append("{").append(statsMap.getKey()).append(":[");

			for (Entry<String, AtomicInteger> stat : statsMap.getValue().entrySet()) {
				sb.append("{").append(stat.getKey()).append("-").append(stat.getValue()).append("}");
			}
			sb.append("]}");
		}
		sb.append("]");

		String logContent = sb.toString();
		LOGGER.info(logContent);
		return logContent;
	}

	class StatLogger implements Runnable {
		private final Object waitLock = new Object();

		@Override
		public void run() {
			LOGGER.info("Started Statistic Logger");
			while (true) {
				synchronized (waitLock) {
					try {
						waitLock.wait(statPrintingInterval);
					} catch (InterruptedException e) {
						return;
					}
				}

				printStatLog();
			}

		}
	}

}
