/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.sim.perf.client;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class RequestDataFileReader {
	private static final Logger LOGGER = LoggerFactory.getLogger(RequestDataFileReader.class);

	public List<String> readFile(String filePath) {
		LOGGER.info("Reading data from file[{}]", filePath);
		Scanner scanner = createScanner(filePath);
		return readFile(scanner);
	}

	private Scanner createScanner(String filePath) {
		Scanner scanner;
		try {
			scanner = new Scanner(new File(filePath));
		} catch (FileNotFoundException e) {
			LOGGER.error("Error while reading file[{}]", filePath, e);
			throw new RuntimeException(e);
		}
		return scanner;
	}

	private List<String> readFile(Scanner scanner) {
		List<String> fileData = new ArrayList<String>();
		try {
			while (scanner.hasNextLine()) {
				fileData.add(scanner.nextLine());
			}
			return fileData;

		} finally {
			scanner.close();
		}
	}
}
