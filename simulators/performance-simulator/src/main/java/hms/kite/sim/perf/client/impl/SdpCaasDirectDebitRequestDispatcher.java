package hms.kite.sim.perf.client.impl;

import hms.kite.sim.perf.client.HttpRequestSender;
import hms.kite.sim.perf.client.ReqDispatcher;
import hms.kite.sim.perf.client.RequestDataFileReader;
import hms.kite.sim.perf.util.StatisticsHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 10/9/12
 * Time: 1:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class SdpCaasDirectDebitRequestDispatcher implements ReqDispatcher {

    private static final Logger LOGGER = LoggerFactory.getLogger(SdpCaasDirectDebitRequestDispatcher.class);
    private String requestSendingUrl;
    private String sampleCaasDirectDebitRequestFile;
    private RequestDataFileReader fileReader;
    private String ncs;
    private StatisticsHandler statHandler;

    private HttpRequestSender httpRequestSender;
    private URI requestSendingUri;
    private List<String> sampleRequests;

    private final AtomicInteger lastSentIndex = new AtomicInteger(-1);

    public void init() {

        sampleRequests = fileReader.readFile(sampleCaasDirectDebitRequestFile);
        LOGGER.info("Sample caas direct debit requests : [{}]", sampleRequests);
        try{
            requestSendingUri = new URI(requestSendingUrl);
        }catch (Exception e){
            throw new RuntimeException();
        }

        httpRequestSender = new HttpRequestSender(statHandler);
        httpRequestSender.init();
        LOGGER.info("Initialized caas direct debit request dispatcher");
    }

    @Override
    public void sendNext() {

        int nextIndex = lastSentIndex.incrementAndGet();
        if(nextIndex >= sampleRequests.size()){
            lastSentIndex.set(0);
            nextIndex = 0;
        }
        String requestContent = /*ncs + ">" +*/ sampleRequests.get(nextIndex);
        try{
            httpRequestSender.sendPostRequest(requestSendingUri, requestContent);
            statHandler.aoSend(ncs);
        } catch (Exception ex){
            LOGGER.error("Exception while sending [" + ncs + "]ao", ex);
            statHandler.aoSendFailed(ncs);
        }
    }

    public void setRequestSendingUrl(String requestSendingUrl) {
        this.requestSendingUrl = requestSendingUrl;
    }

    public void setSampleCaasDirectDebitRequestFile(String sampleCaasDirectDebitRequestFile) {
        this.sampleCaasDirectDebitRequestFile = sampleCaasDirectDebitRequestFile;
    }

    public void setFileReader(RequestDataFileReader fileReader) {
        this.fileReader = fileReader;
    }

    public void setNcs(String ncs) {
        this.ncs = ncs;
    }

    public void setStatHandler(StatisticsHandler statHandler) {
        this.statHandler = statHandler;
    }


}
