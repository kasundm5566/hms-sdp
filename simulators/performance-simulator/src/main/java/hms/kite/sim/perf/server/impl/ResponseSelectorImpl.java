/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.sim.perf.server.impl;

import hms.kite.sim.perf.server.Response;
import hms.kite.sim.perf.server.ResponseSelector;
import hms.kite.sim.perf.util.StatisticsHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class ResponseSelectorImpl implements ResponseSelector {
	private static final Logger LOGGER = LoggerFactory.getLogger(ResponseSelector.class);
	// Error response
	// HTTP Error response
	// HTTP request waiting

	private int sdpErrorRespPercentage = 0;
	private int httpErrorRespPercentage = 0;
	private int httpReqWaitPercentage = 0;
	private Response appSuccessResponse;
	private Response appErrorResponse;
	private Response httpErrorResponse;
	private Response httpWaitResponse;
	private StatisticsHandler statHandler;

	private Map<Integer, Response> specialResponseMap = new HashMap<Integer, Response>();

	public void init() {

		validateTotalPercentage();

		Random rand = new Random(System.currentTimeMillis());
		setSpecialResponses(rand, sdpErrorRespPercentage, appErrorResponse);
		setSpecialResponses(rand, httpErrorRespPercentage, httpErrorResponse);
		setSpecialResponses(rand, httpReqWaitPercentage, httpWaitResponse);

		LOGGER.info("Initilized Special Response Map[{}]", specialResponseMap);
	}

	public Response getResponse(String ncs) {
		int receiveCount = statHandler.moReceived(ncs);
		Response resp = specialResponseMap.get(receiveCount % 100);
		if (resp == null) {
			return appSuccessResponse;
		} else {
			return resp;
		}
	}

	private void validateTotalPercentage() {
		if ((sdpErrorRespPercentage + httpErrorRespPercentage + httpReqWaitPercentage) > 100) {
			throw new IllegalArgumentException("Total of all percentages should be 100 or less.");
		}
	}

	private void setSpecialResponses(Random randGenerator, int percentage, Response response) {
		if (response == null) {
			throw new IllegalArgumentException("Response cannot be null.");
		}
		if (percentage > 0) {
			for (int i = 0; i < percentage; i++) {
				specialResponseMap.put(randGenerator.nextInt(100), response);
			}
		}
	}

	private void validatePercentage(int percentage) {
		if (percentage < 0 || percentage > 100) {
			throw new IllegalArgumentException("Invalid percentage value, should be between 0-100");
		}
	}

	public void setSdpErrorRespPercentage(int sdpErrorRespPercentage) {
		validatePercentage(sdpErrorRespPercentage);
		this.sdpErrorRespPercentage = sdpErrorRespPercentage;
	}

	public void setHttpErrorRespPercentage(int httpErrorRespPercentage) {
		validatePercentage(httpErrorRespPercentage);
		this.httpErrorRespPercentage = httpErrorRespPercentage;
	}

	public void setHttpReqWaitPercentage(int httpReqWaitPercentage) {
		validatePercentage(httpReqWaitPercentage);
		this.httpReqWaitPercentage = httpReqWaitPercentage;
	}

	public void setAppSuccessResponse(Response appSuccessResponse) {
		this.appSuccessResponse = appSuccessResponse;
	}

	public void setAppErrorResponse(Response appErrorResponse) {
		this.appErrorResponse = appErrorResponse;
	}

	public void setHttpErrorResponse(Response httpErrorResponse) {
		this.httpErrorResponse = httpErrorResponse;
	}

	public void setHttpWaitResponse(Response httpWaitResponse) {
		this.httpWaitResponse = httpWaitResponse;
	}

	public void setStatHandler(StatisticsHandler statHandler) {
		this.statHandler = statHandler;
	}

	public void setSpecialResponseMap(Map<Integer, Response> specialResponseMap) {
		this.specialResponseMap = specialResponseMap;
	}
}
