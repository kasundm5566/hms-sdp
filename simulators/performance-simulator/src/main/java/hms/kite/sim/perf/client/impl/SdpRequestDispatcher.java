/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.sim.perf.client.impl;

import hms.kite.sim.perf.client.HttpRequestSender;
import hms.kite.sim.perf.client.ReqDispatcher;
import hms.kite.sim.perf.client.RequestDataFileReader;
import hms.kite.sim.perf.util.StatisticsHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class SdpRequestDispatcher implements ReqDispatcher {

	private static final Logger LOGGER = LoggerFactory.getLogger(SdpRequestDispatcher.class);
	private String sampleRequestFile;
	private String requestSendingUrl;
	private RequestDataFileReader fileReader;
	private HttpRequestSender httpRequestSender;
	private List<String> sampleRequests;
	private URI requestSendingUri;
	private StatisticsHandler statHandler;
	private String ncs;

	private final AtomicInteger lastSentIndex = new AtomicInteger(-1);

	public void init() {
		sampleRequests = fileReader.readFile(sampleRequestFile);
		LOGGER.info("Sample Request[{}]", sampleRequests);
		try {
			requestSendingUri = new URI(requestSendingUrl);
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
		httpRequestSender = new HttpRequestSender(statHandler);
		httpRequestSender.init();
		LOGGER.info("Initialized SdpRequestDispatcher");
		// startSending();
	}

	@Override
	public synchronized void sendNext() {
		int nextIndex = lastSentIndex.incrementAndGet();
		if (nextIndex >= sampleRequests.size()) {
			lastSentIndex.set(0);
			nextIndex = 0;
		}
		String requestContent = /*ncs + ">" +*/ sampleRequests.get(nextIndex);
		try {
			httpRequestSender.sendPostRequest(requestSendingUri, requestContent);
			statHandler.mtSent(ncs);
		} catch (Exception ex) {
			LOGGER.error("Exception while sending [" + ncs + "]mt", ex);
			statHandler.mtSendFailed(ncs);
		}

	}

	public void setSampleRequestFile(String sampleRequestFile) {
		this.sampleRequestFile = sampleRequestFile;
	}

	public void setRequestSendingUrl(String requestSendingUrl) {
		this.requestSendingUrl = requestSendingUrl;
	}

	public void setFileReader(RequestDataFileReader fileReader) {
		this.fileReader = fileReader;
	}

	public void setStatHandler(StatisticsHandler statHandler) {
		this.statHandler = statHandler;
	}

	public void setNcs(String ncs) {
		this.ncs = ncs;
	}

}
