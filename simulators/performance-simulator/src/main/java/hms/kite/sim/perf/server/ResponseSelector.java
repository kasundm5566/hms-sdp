/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hms.kite.sim.perf.server;


/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public interface ResponseSelector {

	/**
	 * Get the response to be sent back.
	 * @param ncs
	 * @return
	 */
	Response getResponse(String ncs);

}
