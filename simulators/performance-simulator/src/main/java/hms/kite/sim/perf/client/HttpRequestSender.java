/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.sim.perf.client;

import hms.kite.sim.perf.util.CorrelationIdGenerator;
import hms.kite.sim.perf.util.StatisticsHandler;
import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.*;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;
import org.jboss.netty.handler.codec.http.*;
import org.jboss.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;

import static org.jboss.netty.channel.Channels.pipeline;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class HttpRequestSender {
	private static final Logger LOGGER = LoggerFactory.getLogger(HttpRequestSender.class);
	private ClientBootstrap bootstrap;
	private StatisticsHandler statHandler;
    private long sentTime;

	public HttpRequestSender(StatisticsHandler statHandler) {
		super();
		this.statHandler = statHandler;
	}

	public void init() {
		bootstrap = new ClientBootstrap(new NioClientSocketChannelFactory(Executors.newCachedThreadPool(),
				Executors.newCachedThreadPool()));
		bootstrap.setPipelineFactory(new HttpClientPipelineFactory());

		LOGGER.info("Initialized client bootstap.");
	}

	public void stop() {
		bootstrap.releaseExternalResources();
	}

	public void sendPostRequest(final URI serverUri, final String requestContent) {
		ChannelFuture future = bootstrap.connect(new InetSocketAddress(serverUri.getHost(), serverUri.getPort()));

		future.addListener(new ChannelFutureListener() {

			@Override
			public void operationComplete(ChannelFuture cf) throws Exception {
				if (!cf.isSuccess()) {
					LOGGER.error("Unable to connect to [{}]", serverUri, cf.getCause());
				} else {
					long correlationId = CorrelationIdGenerator.getCorrelationId();
					Channel channel = cf.getChannel();
					updateRespHandlerWithDetails(correlationId, channel);

                    if(requestContent.equals(null)){
                        LOGGER.info("Request content is not received.");
                    } else {
                        HttpRequest request = createRequest(serverUri, requestContent);
                        LOGGER.info("[{}] Sending request[{}] to [{}]", new Object[] { correlationId, requestContent,
                                serverUri });
                        channel.write(request);
                        channel.getCloseFuture().addListener(ChannelFutureListener.CLOSE);
                    }

				}

			}

		});

	}

    private void updateRespHandlerWithDetails(long correlationId, Channel channel) {
        ChannelHandlerContext context = channel.getPipeline().getContext("handler");
        SdpSmsMtResponseHandler respHandler = (SdpSmsMtResponseHandler) context.getHandler();
        respHandler.setCorrelationId(correlationId);
        respHandler.setSentTime(System.currentTimeMillis());
        respHandler.setStatHandler(statHandler);
    }

    public void sendGetRequest(final URI serverUri, final String requestContent){

        ChannelFuture future = bootstrap.connect(new InetSocketAddress(serverUri.getHost(), serverUri.getPort()));

        future.addListener(new ChannelFutureListener() {

            @Override
            public void operationComplete(ChannelFuture cf) throws Exception{

                if(!cf.isSuccess()){
                    LOGGER.error("Unable to connect to [{}]", serverUri, cf.getCause());
                } else {
                    long correlationId = CorrelationIdGenerator.getCorrelationId();
                    //Channel channel = cf.getChannel();
                    //updateRespHandlerWithDetails(correlationId, channel);

                    if(requestContent.equals(null)){
                        LOGGER.info("Request content is not received.");
                    } else {
                        LOGGER.info("[{}] Sending request[{}] to [{}]", new Object[] { correlationId, requestContent,
                                serverUri });
                        sentTime = System.currentTimeMillis();
                        sendUssdGetRequest(serverUri, requestContent);
                    }
                }
            }


        });
    }


    private void sendUssdGetRequest(URI serverUri, String requestContent) {

        String[] readLine;
        String inputLine;

        try {
            URL url = new URL("http://" + serverUri.getHost() + ":" + serverUri.getPort() + "/dialog-ussd/" + requestContent);
            LOGGER.info("Sending url : [{}]", url);
            URLConnection conn = url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            readLine = bufferedReader.readLine().split(" ",2);
            StringBuilder menuContent = new StringBuilder(readLine[1]);

            while((inputLine = bufferedReader.readLine()) != null){
                menuContent.append(inputLine);
            }
            bufferedReader.close();
            Map<String,Object> menu = new HashMap<String,Object>();
            ArrayList<Map<String, Object>> menuList = new ArrayList<Map<String, Object>>();
            menu.put("content",menuContent.toString()) ;
            menuList.add(menu);

            LOGGER.info("Receive SDP Response : [{}] took [{}]ms", menuList, System.currentTimeMillis() - sentTime);
        } catch (Exception e) {
            LOGGER.info("Error occurred while sending ussd-mo request : ", e);
        }
    }

    private HttpRequest createRequest(final URI serverUri, final String requestContent) {

        HttpRequest request = null;

        request = new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.POST, serverUri.getPath());
        request.setHeader(HttpHeaders.Names.HOST, serverUri.getHost() + ":" + serverUri.getPort());
        request.setHeader(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.CLOSE);
        request.setHeader(HttpHeaders.Names.CONTENT_TYPE, "application/json");
        request.setHeader(HttpHeaders.Names.ACCEPT, "application/json");
        request.setHeader(HttpHeaders.Names.CONTENT_LENGTH, String.valueOf(requestContent.length()));
        ChannelBuffer buf = ChannelBuffers.copiedBuffer(requestContent, CharsetUtil.UTF_8);
        request.setContent(buf);


		return request;
	}

}

class HttpClientPipelineFactory implements ChannelPipelineFactory {

	public ChannelPipeline getPipeline() throws Exception {
		ChannelPipeline pipeline = pipeline();
		pipeline.addLast("codec", new HttpClientCodec());
		pipeline.addLast("inflater", new HttpContentDecompressor());
		pipeline.addLast("aggregator", new HttpChunkAggregator(1048576));
		pipeline.addLast("handler", new SdpSmsMtResponseHandler());
		return pipeline;
	}
}
