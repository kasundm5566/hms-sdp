# SDP Performance Simulator

  From this simulator we can generate sms-mt, ussd-mo, caas-direct-debit, caas-query-balance traffic to sdp
  Generated sms or ussd or caas traffic with different tps and that tps values are always configurable.

  ##SMS-mt traffic

    1. For this, we need valid application in mongo collection with sms-mt ncs, sdp-server, sbl-server and smpp-simulator
    2. Make sure this these parts are up and running
    3. Then you need to add sms-mt contents to conf/sms-mt.txt file as follows
        Eg:
         {"message":"my testing message from app1","destinationAddresses":["tel:94772323654"],"applicationId":"APP_000088","encoding":"0","statusRequest":"0","password":"e198975fe780ef35df19c3eff88dbab5","version":"1.0"}
         {"message":"my testing message from app2","destinationAddresses":["tel:94772323655"],"applicationId":"APP_000089","encoding":"0","statusRequest":"0","password":"e198975fe780ef35df19c3eff88dbab6","version":"1.0"}
        In the example, there are two records.
    4. Then start the simulator
         bin/perf-simulator console
    5. Then you can control sms-mt traffic in the simulator from following commands

       ### Start sms-mt sending

           >	http://localhost:10500/perfsim/sms/mt/start/5

       ### Stop sms-mt sending

           >	http://localhost:10500/perfsim/sms/mt/stop

       ### Change sms-mt TPS

           >	http://localhost:10500/perfsim/sms/mt/tps/5

       ### Send message one by one

           > 	http://localhost:10500/perfsim/sms/mt/next



  ##BroadCast sms traffic

    1. For this, we need valid application in mongo collection with subscription and sms ncs, sdp-server, sbl-server, smpp-sim
    2. Before using scripts, you need to add the parameters to parameter_set.txt file regarding the created application
        #File path
            simulators/performance-simulator/scripts/broadcast-test-scripts/parameter_set.txt
        #Parameter description
            app_id      : <application id, that was given by sdp when application creation>
            app_name    : <application name, that was given by application creator>
            app_password: <application password, that was given by sdp when application creation>
            keyword     : <keyword of the application, that was given by user when application creation>
            short_code  : <short code of the application, that was given by user when application creation>

            sp_id       : <service provider id, that was given by sdp, when the provisioning-user creation>
            sp_name     : <service provider name, that was given by user, when the provisioning-user creation>

            operator    : <operator you using to test - eg: dialog, vcity>

            sdp_url     : <sdp sms-mt receiving url>

    3. Using the populate_subscription_collection.sh script, we can populate subscription collection
        #File path
            simulators/performance-simulator/scripts/broadcast-test-scripts/populate_subscription_collection.sh
        #Using
            Run the script to populate the subscription collection
                ./populate_subscription_collection.sh
        (this add subscription records, that you configured in parameter_set.txt under iterations parameter)
        #Outputs
            This will create a file called existing_subscription_collection.json. This file created for existing mongo subscription
            data, that before we import the created subscription data. (Keep this file in separate location, if you need to restore the old data)

    4. If you need to send sms-mt request from the application one time then run sms-mt.sh script
        #File path
            simulators/performance-simulator/scripts/broadcast-test-scripts/sms-mt.sh
        #Using
            Run the script to send sms-mt request to sdp
                ./sms-mt.sh

    5. If you need to send sms-mt request rapidly from the application then run loop-sms-mt.sh script
        #File path
            simulators/performance-simulator/scripts/broadcast-test-scripts/loop-sms-mt.sh
        #Using
            Run the script to send sms-mt request rapidly to sdp
                ./loop-sms-mt.sh


  ##USSD-mo traffic

     1. For this, we need valid application in mongo collection with ussd-mo ncs, sdp-server, sbl-server and ussd-sample application
     2. Make sure this these parts are up and running
     3. Then you need to add ussd-mo contents to conf/ussd-mo.txt file as follows
          Eg:
             ?MSISDN=94772334476&SC=383&SESSIONID=1209992331266121&DATE=2012-03-20&TIME=11%3A57%3A42&msg=789
             ?MSISDN=94772334477&SC=383&SESSIONID=1209992331266121&DATE=2012-03-20&TIME=11%3A57%3A42&msg=790
             In the example, there are two records.
     4. Then start the simulator
              bin/perf-simulator console
     5. Then you can control sms-mt traffic in the simulator from following commands

        ### Start ussd-mo sending

           >	http://localhost:10500/perfsim/ussd/mo/start/5

        ### Stop ussd-mo sending

           >	http://localhost:10500/perfsim/ussd/mo/stop

        ### Change ussd-mo TPS

           >	http://localhost:10500/perfsim/ussd/mo/tps/1

        ### Send message one by one

           > 	http://localhost:10500/perfsim/ussd/mo/next




  ##CAAS traffic

       1. For this, we need valid application in mongo collection with cas ncs, sdp-server and sbl-server, pgw-server, lbs-simulator
       2. Make sure this these parts are up and running
       3. Then you need to add caas-direct/debit or caas-query/balance contents to conf/caas-dd-qb.txt file as follows
            Eg:
              direct-debit
               {"externalTrxId":"22209","amount":"10","paymentInstrumentName":"Dialog-IN","currency":"LKR","applicationId":"APP_000843","password":"f80bd52c063ceb826093f658495c75f2","subscriberId":"tel:94771234568"}
               {"externalTrxId":"22210","amount":"10","paymentInstrumentName":"Dialog-IN","currency":"LKR","applicationId":"APP_000844","password":"f80bd52c063ceb826093f658495c75y8","subscriberId":"tel:94771234569"}
              query-balance
               {"applicationId":"APP_000843","password":"f80bd52c063ceb826093f658495c75f2","subscriberId":"94771234568","accountId":"12345","currency":"LRK"}
               {"applicationId":"APP_000844","password":"f80bd52c063ceb826093f658495c75y8","subscriberId":"94771234569","accountId":"12345","currency":"LRK"}
              In the example, there are two records.

       4. Then start the simulator
                bin/perf-simulator console
       5. Then you can control sms-mt traffic in the simulator from following commands

          ### Start caas-dd or caas-qb sending

             >	http://localhost:10500/perfsim/caas/ddqb/start/5

          ### Stop caas-dd or caas-qb sending

             >	http://localhost:10500/perfsim/caas/ddqb/stop

          ### Change caas-dd or caas-qb TPS

             >	http://localhost:10500/perfsim/caas/ddqb/tps/1

          ### Send message one by one

             > 	http://localhost:10500/perfsim/caas/ddqb/next


You can't use this functionality if tps based message sending is already running.


### View current TPS configuration

    #sms
        > 	http://localhost:10500/perfsim/sms/mt/viewtps
    #ussd
        > 	http://localhost:10500/perfsim/ussd/mo/viewtps
    #caas
        >   http://localhost:10500/perfsim/caas/ddqb/viewtps

### View current statistics

    #sms
        > 	http://localhost:10500/perfsim/sms/mt/stats
    #ussd
        > 	http://localhost:10500/perfsim/ussd/mo/stats
    #caas
        >   http://localhost:10500/perfsim/caas/ddqb/stats
