# SDP Performance Simulator

## Controlling the simulator

### Start mt sending

>	http://localhost:10500/perfsim/sms/mt/start/5

### Stop mt sending

>	http://localhost:10500/perfsim/sms/mt/stop

### Change mt TPS

>	http://localhost:10500/perfsim/sms/mt/tps/5

### Send message one by one

> 	http://localhost:10500/perfsim/sms/mt/next
You can't use this functionality if tps based message sending is already running.


### View current TPS configuration

> 	http://localhost:10500/perfsim/sms/mt/viewtps

### View current statistics

> 	http://localhost:10500/perfsim/sms/mt/stats
