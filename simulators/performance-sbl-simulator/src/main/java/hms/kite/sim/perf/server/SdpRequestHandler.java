/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hms.kite.sim.perf.server;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelHandler;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
class SdpRequestHandler extends SimpleChannelHandler {
	private static final Logger LOGGER = LoggerFactory.getLogger(SdpRequestHandler.class);
	private final ResponseSelector responseSelector;

	public SdpRequestHandler(ResponseSelector responseSelector) {
		super();
		this.responseSelector = responseSelector;
	}

	@Override
	public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
		SdpRequestReceivingServer.connectedClienChannels.add(ctx.getChannel());
		HttpRequest request = (HttpRequest) e.getMessage();
		String ncs = getNcs(request.getUri());
		ChannelBuffer content = request.getContent();
		LOGGER.info("Recieved Message[{}][{}]", request.getUri(), content.toString("UTF-8"));
		Response response = responseSelector.getResponse(ncs);
		response.execute(e, request);
	}

	private String getNcs(String uri) {
		if (uri.contains("sms/receive")) {
			return "sms.receive";
		} else if (uri.contains("sms/report")) {
			return "sms.report";
		} else if (uri.contains("notify/receive")) {
			return "notify.receive";
		} else {
			throw new IllegalStateException("Unsupported request path [" + uri + "]");
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
		LOGGER.error("Exeption caught", e.getCause());
		e.getChannel().close();
	}

}
