/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hms.kite.sim.perf;

import hms.kite.sim.perf.util.StatisticsHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.tanukisoftware.wrapper.WrapperListener;
import org.tanukisoftware.wrapper.WrapperManager;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public class SystemStarter implements WrapperListener {
	private static final Logger LOGGER = LoggerFactory.getLogger(SystemStarter.class);
	private static AbstractApplicationContext applicationContext;

	@Override
	public void controlEvent(int event) {
		if ((event == WrapperManager.WRAPPER_CTRL_LOGOFF_EVENT) && WrapperManager.isLaunchedAsService()) {
			// Ignore
		} else {
			WrapperManager.stop(0);
			// Will not get here.
		}
	}

	@Override
	public Integer start(String[] arg0) {
		applicationContext = new ClassPathXmlApplicationContext("spring-context.xml");
		applicationContext.registerShutdownHook();
		applicationContext.start();
		LOGGER.info("========================================================");
		LOGGER.info("============ SDP Performance SBL Simulator Started =========");
		LOGGER.info("========================================================");
		return null;
	}

	@Override
	public int stop(int exitCode) {
		LOGGER.info("Request received to stop the system");
		StatisticsHandler statHandler = (StatisticsHandler) applicationContext.getBean("statHander");
		if (statHandler != null) {
			statHandler.printStatLog();
		}

		applicationContext.stop();
		LOGGER.info("========================================================");
		LOGGER.info("=========== SDP Performance SBL Simulator Stopped ==========");
		LOGGER.info("========================================================");
		return exitCode;
	}

	public static void main(String[] args) {
		WrapperManager.start(new SystemStarter(), args);
	}

}
