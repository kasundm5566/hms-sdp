/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.sim.perf.client.impl;

import java.util.concurrent.atomic.AtomicInteger;

import hms.kite.sim.perf.client.FlowController;
import hms.kite.sim.perf.client.MtDispatcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class TpsFlowController implements FlowController {
	private static final Logger LOGGER = LoggerFactory.getLogger(TpsFlowController.class);

	private float currentTps;
	private final Object waitLock = new Object();
	private boolean canSendMsg = false;
	private MtDispatcher mtDispatcher;
	private AtomicInteger timeBetweenMsg = new AtomicInteger();

	public void start(float tps) {
		if (canSendMsg) {
			throw new IllegalStateException("Already running.");
		} else {
			canSendMsg = true;
			currentTps = tps;
			timeBetweenMsg.set((int) (1000 / tps));
			LOGGER.info("Start sending MT at [{}]tps, Time between msg[{}]milles.", tps, timeBetweenMsg.get());
			Thread t = new Thread(this);
			t.start();
		}

	}

	public void stop() {
		canSendMsg = false;
	}

	public void changeTps(float tps) {
		currentTps = tps;
		timeBetweenMsg.set((int) (1000 / tps));
		LOGGER.info("Changed Tps to [{}], Time between msg[{}]milles.", tps, timeBetweenMsg.get());
	}

	@Override
	public void sendNext() {
		if (canSendMsg) {
			throw new IllegalStateException("Already running continues message sending.");
		} else {
			mtDispatcher.sendNext();
		}

	}

	@Override
	public String viewCurrentConfiguration() {
		if (canSendMsg) {
			return "Current TPS:" + String.valueOf(currentTps);
		} else {
			return "Not Running.";
		}
	}

	public float getCurrentTps() {
		return currentTps;
	}

	@Override
	public void run() {
		sendCycle(System.currentTimeMillis());
	}

	private void sendCycle(long lastSentTime) {

		while (canSendMsg) {
			waitBeforeSend(lastSentTime, timeBetweenMsg.get());
			mtDispatcher.sendNext();
			lastSentTime = System.currentTimeMillis();
		}

		LOGGER.info("MT Sending stopped.");

	}

	private void waitBeforeSend(final long lastSentTime, final int timeBetweenMsg) {
		long waitTime = timeBetweenMsg - (System.currentTimeMillis() - lastSentTime);
		if (waitTime > 0) {
			synchronized (waitLock) {
				try {
					waitLock.wait(waitTime);
				} catch (InterruptedException e) {
					canSendMsg = false;
					Thread.currentThread().interrupt();
				}
			}
		}
	}

	public void setMtDispatcher(MtDispatcher mtDispatcher) {
		this.mtDispatcher = mtDispatcher;
	}

}
