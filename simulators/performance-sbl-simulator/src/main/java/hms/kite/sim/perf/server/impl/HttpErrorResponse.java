/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.sim.perf.server.impl;

import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.HttpResponseStatus;

import hms.kite.sim.perf.server.Response;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class HttpErrorResponse extends AbstractResponse {

	@Override
	public void execute(MessageEvent e, HttpRequest request) {
		sendResponse(e, request, "{}", HttpResponseStatus.FORBIDDEN);

	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("HttpErrorResponse []");
		return builder.toString();
	}

}
