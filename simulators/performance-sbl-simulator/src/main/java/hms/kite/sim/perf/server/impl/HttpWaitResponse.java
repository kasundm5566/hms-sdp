/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.sim.perf.server.impl;

import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.HttpResponseStatus;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class HttpWaitResponse extends AbstractResponse {

	private int maxWaitTime;
	private String succesResponse;

	@Override
	public void execute(MessageEvent e, HttpRequest request) {
		int waitTime = randGenerator.nextInt(maxWaitTime);
		LOGGER.info("Waiting [{}]ms before sending response", waitTime);
		if (waitTime > 0) {
			final Object waitLock = new Object();
			synchronized (waitLock) {
				try {
					waitLock.wait(waitTime);
				} catch (InterruptedException e1) {
				}
			}
		}
		sendResponse(e, request, succesResponse, HttpResponseStatus.OK);
	}

	public void setMaxWaitTime(int maxWaitTime) {
		this.maxWaitTime = maxWaitTime;
	}

	public void setSuccesResponse(String succesResponse) {
		this.succesResponse = succesResponse;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("HttpWaitResponse [maxWaitTime=");
		builder.append(maxWaitTime);
		builder.append("]");
		return builder.toString();
	}

}
