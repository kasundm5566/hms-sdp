## This is to Keep Etisalat features and how they were enabled/disabled.

### Provisioning
1.  Disabled Blacklist user filed from provisioning app creation. (05/12/2013)
    Removed ROLE_PROV_APP_READ_BLACK_LIST role from CORPORATE_USER group

2.  Subscription Charging Type, Charging frequency, Charging Amount filed were disabled and set to default values.

3.  SMS Mo Charging Type always Free.
    Removed ROLE_PROV_SMS_MO_CHARGING_WRITE_CHARGING_TYPE from CORPORATE_USER group.

4.