package hms.kite.charging;

import hms.kite.datarepo.RepositoryServiceRegistry;
import hms.kite.datarepo.SystemConfiguration;
import hms.kite.util.KiteKeyBox;
import hms.kite.util.SdpException;
import hms.kite.wfengine.transport.BaseChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.wfengine.impl.ResponseBuilder.generate;
import static hms.kite.wfengine.impl.ResponseBuilder.generateSuccess;

/**
 * (C) Copyright 2011-2012 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 * <p/>
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 * <p/>
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
public class OperatorChargingChannel extends BaseChannel{

    private static final Logger logger = LoggerFactory.getLogger(OperatorChargingChannel.class);

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        try {
            return internalExecute(requestContext);
        } catch (SdpException e){
            throw e;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return generate(temporarySystemErrorCode, false, requestContext);
        }
    }

    private Map<String, Object> internalExecute(Map<String,Map<String, Object>> requestContext) {
        Map<String, Object> request = requestContext.get(requestK);
        Map<String, Object> ncs = requestContext.get(ncsK);
        if(Boolean.TRUE.equals(request.get(operatorChargingK))){
            List operators = (List) RepositoryServiceRegistry.systemConfiguration().find("dr-based-op-charging-operators");
            if(operators != null) {
                if(operators.contains(ncs.get(operatorK))){
                    request.put(srrK, true);
                }
            }
        } else {
            logger.debug("Operator charging is not true");
        }
        return generateSuccess();
    }
}


