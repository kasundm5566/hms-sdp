#============================================================================
#========================== mChoice SDP SDK Readme   ========================
#============================================================================


1). Building SDP SDK
	1). Build the sdk module with profile using mvn clean install -DskipTests -P<profile>

2). Starting SDP SDK
	2.1). Go to sdk-standalone/target folder
	2.2). Unzip the file sdk-standalone-1.0.0-distribution.zip
			unzip sdk-standalone-{version}-distribution.zip
	2.3). Go to folder sdk-standalone-{version}/bin/
	2.4). Start the sdk using
			Linux   - sh sdp-simulator console
			Windows - sdp-simulator.bat console

3). Using SDP SDK
	3.1). SDP SDK can be accessed from URL http://localhost:10001/mchoice-sdp-sdk

	NOTE : As a time consuming, the developers can start the simulator by executing a script
	         --> build the ncs-rest-simulator module
	         --> go to the module sdk-standalone and run the start.sh to start the simulator in one step. (e.g. sh start)
	                Simulator will start in console mode.


#================================Error codes==================================

SDK will send following error codes to the preconfigured phone numbers as given in the list.
For any other number sdk will send success responce.

Phone Number Status Code	Status Discription
--------------------------------------------------
9900000	P1001	Partial Success
9900001	E1300	Default Error
9900002	E1301	App Not Available Error
9900003	E1302	SP Not Available Error
9900004	E1303	Invalid Host IP Error
9900005	E1304	App Not Found Error
9900006	E1305	Invalid App ID Error
9900007	E1306	Invalid Routing key Error
9900008	E1307	SP Not Found Error
9900009	E1308	Charging Error
9900010	E1309	ncs Not Allowed Error
9900011	E1310	MO Not Allowed Error
9900012	E1311	MT Not Allowed Error
9900013	E1312	Invalid Request Error
9900014	E1313	Authentication Failed Error
9900015	E1314	ncs Not Available Error
9900016	E1315	App Connection Refused Error
9900017	E1316	msisdn Not Allowed Error
9900018	E1317	Tps Exceeded Error
9900019	E1318	Tpd Exceeded Error
9900020	E1319	AT Message Failed Error
9900021	E1320	SBL Fail Error Error
9900022	E1321	Sender Not Allowed Error
9900023	E1322	Recipient Not Allowed Error
9900024	E1323	HTTP Request Not Allowed Error
9900025	E1324	Invalid Msisdn Error
9900026	E1325	Insufficient Fund Error
9900027	E1326	Charging Not Allowed Error
9900028	E1327	Charging Operation Not Allowed Error
9900029	E1328	Charging Amount Too High Error
9900030	E1329	Charging Amount Too Low Error
9900031	E1330	Invalid Sender Address Error
9900051	E1350	Subscription Reg Blocked Error
9900052	E1351	Subscription Reg Already Registered Error
9900053	E1352	Subscription Reg SLA Error
9900054	E1353	Subscription Reg Charging Error
9900055	E1354	Subscription Unreg SLA Error
9900056	E1355	Subscription Unreg Blocked Error
9900057	E1356	Subscription Unreg Not Registered Error
9900061	E1360	Internal Error
9900062	E1361	System Error

