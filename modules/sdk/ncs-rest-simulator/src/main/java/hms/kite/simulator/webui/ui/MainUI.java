/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.simulator.webui.ui;

import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.*;
import hms.kite.simulator.webui.FeaturePropertiesService;
import hms.kite.simulator.webui.StaticPropertiesService;
import hms.kite.simulator.webui.ui.WP.WPNcsUIService;
import hms.kite.simulator.webui.ui.caas.CaasNcsUIService;
import hms.kite.simulator.webui.ui.sms.SmsNcsUIService;
import hms.kite.simulator.webui.ui.subscription.SubNcsUIService;
import hms.kite.simulator.webui.ui.tab.impl.*;
import hms.kite.simulator.webui.ui.ussd.UssdNcsUIService;

public class MainUI {

	/**
	 * @return the root layout of the main UI
	 */
	public Component getRootLayout() {

		VerticalLayout rootLayout = new VerticalLayout();
		rootLayout.setStyleName("root-layout");
		Panel mainPanel = new Panel();
		mainPanel.setWidth("800px");
		mainPanel.addComponent(createHeader());
		Component tabSheet = createMainUI().createTabSheetPanel();
		mainPanel.addComponent(tabSheet);

		rootLayout.addComponent(mainPanel);
		rootLayout.setComponentAlignment(mainPanel, Alignment.MIDDLE_CENTER);
		return rootLayout;
	}

	private TabSheetPanel createMainUI() {

		final SmsNcsUIService smsNcsUIService = new SmsNcsUIService();
		final UssdNcsUIService ussdNcsUIService = new UssdNcsUIService();
        final WPNcsUIService wpNcsUIService = new WPNcsUIService();
        final CaasNcsUIService casNcsUIService = new CaasNcsUIService();
        final SubNcsUIService subNcsUIService = new SubNcsUIService();
		smsNcsUIService.init();
        ussdNcsUIService.init();
        wpNcsUIService.init();
        casNcsUIService.init();
        subNcsUIService.init();
		final TabViewSmsImpl smsTabView = new TabViewSmsImpl(smsNcsUIService);
		final TabViewUssdImpl ussdTabView = new TabViewUssdImpl(ussdNcsUIService);
        final TabViewWPImpl wpTabView = new TabViewWPImpl(wpNcsUIService);
        final TabViewCasImpl casTabView = new TabViewCasImpl(casNcsUIService);
        final TabViewSubImpl subTabView = new TabViewSubImpl(subNcsUIService);
        smsTabView.init();
        ussdTabView.init();
        wpTabView.init();
        casTabView.init();

		return new TabSheetPanel(smsTabView, ussdTabView ,wpTabView , casTabView, subTabView);
	}

	private AbstractLayout createHeader() {

		HorizontalLayout header = new HorizontalLayout();
		header.setSizeFull();
		Embedded logo = new Embedded("", new ThemeResource("images/"+ FeaturePropertiesService.getInstance().getMessage("WebUI.UI.MainUI.logo")));
		header.addComponent(logo);
		logo.setStyleName("hsenid-logo");
		header.setComponentAlignment(logo, Alignment.TOP_LEFT);
		Label headerLabel = new Label(StaticPropertiesService.getInstant().getMessage("MainUI.header"));
		headerLabel.setStyleName("simulator-header");
		header.addComponent(headerLabel);
		header.setComponentAlignment(headerLabel, Alignment.MIDDLE_RIGHT);
		return header;
	}

}
