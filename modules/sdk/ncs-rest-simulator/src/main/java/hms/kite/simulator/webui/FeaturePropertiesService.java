package hms.kite.simulator.webui;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * <p>
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 * </p>
 *
 * @author Isuru Dilshan
 */
public class FeaturePropertiesService {
    private static FeaturePropertiesService featureProperties = new FeaturePropertiesService();
        private static ResourceBundle resourceBundle = ResourceBundle.getBundle("ncs-rest-simulator-default-values");

    public static FeaturePropertiesService getInstance() {
        return featureProperties;
    }

    private FeaturePropertiesService() {
    }

    public String getMessage(String key,Object... arguments){
        try {
            if (arguments !=null){
                return MessageFormat.format(resourceBundle.getString(key),arguments);
            }else {
                return resourceBundle.getString(key);
            }

        }catch (MissingResourceException mre){
            System.out.println("Message key not found [" + key + "]");
            return '!' + key;
        }
    }

}
