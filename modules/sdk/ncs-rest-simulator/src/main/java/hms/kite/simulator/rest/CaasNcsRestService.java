/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.simulator.rest;

import hms.kite.simulator.MessageReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;

@Produces({"application/json"})
@Consumes({"application/json"})
@Path("/caas")
public class CaasNcsRestService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CaasNcsRestService.class);

    private MessageReceiver caasAoMessageReceiver;
    private MessageReceiver caasChargingReqReceiver;
    private MessageReceiver paymentInstrumentListReq;

    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    @Path("/credit/reserve")
    public Map<String, Object> sendCreditReserveService(Map<String, Object> creditReserveRequest) {

        Map<String, Object> response = caasAoMessageReceiver.onMessage(creditReserveRequest);
        LOGGER.info("The caas credit reserve request received [{}], send response back to application [{}]", creditReserveRequest, response);
        return response;
    }

//    @POST
//    @Path("/direct/debit")
//    public Map<String, Object> sendDirectDebitService(Map<String, Object> creditDebitRequest) {
//
//        Map<String, Object> response = caasAoMessageReceiver.onMessage(creditDebitRequest);
//        LOGGER.info("The caas direct debit request received [{}], send response back to application [{}]", creditDebitRequest, response);
//        return response;
//    }

    @POST
    @Path("/direct/credit")
    public Map<String, Object> sendDirectCreditService(Map<String, Object> directCreditRequest) {

        Map<String, Object> response = caasAoMessageReceiver.onMessage(directCreditRequest);
        LOGGER.info("The caas direct credit request received [{}], send response back to application [{}]", directCreditRequest, response);
        return response;
    }

    @POST
    @Path("direct/debit")
    public Map<String, Object> sendChargingService(Map<String, Object> chargingRequest) {

        Map<String, Object> response = caasChargingReqReceiver.onMessage(chargingRequest);
        LOGGER.info("The caas charging request received [{}], send response back to application [{}]", chargingRequest, response);
        return response;
    }

    @POST
    @Path("/list/pi")
    public Map<String, Object> sendPaymentInstrumentListService(Map<String, Object> paymentInstrumentReq) {

        Map<String, Object> response = paymentInstrumentListReq.onMessage(paymentInstrumentReq);
        LOGGER.info("The caas payment instrument list request received [{}], send response back to application [{}]", paymentInstrumentReq, response);
        return response;
    }

    public void setCaasAoMessageReceiver(MessageReceiver caasAoMessageReceiver) {
        this.caasAoMessageReceiver = caasAoMessageReceiver;
    }

    public void setCaasChargingReqReceiver(MessageReceiver caasChargingReqReceiver) {
        this.caasChargingReqReceiver = caasChargingReqReceiver;
    }

    public void setPaymentInstrumentListReq(MessageReceiver paymentInstrumentListReq) {
        this.paymentInstrumentListReq = paymentInstrumentListReq;
    }
}
