/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.ussd;

import hms.kite.simulator.MessageSender;
import hms.kite.simulator.RequestMessage;
import hms.kite.simulator.rest.AtRequestSender;
import hms.kite.simulator.sms.SmsRequestMessage;

import java.util.HashMap;
import java.util.Map;

public class UssdMessageSender implements MessageSender {
    	private AtRequestSender messageSender;

	@Override
	public Map<String, Object> sendMessage(RequestMessage requestMessage, String url) {

        UssdRequestMessage ussdRequestMessage = (UssdRequestMessage) requestMessage;
		Map<String, Object> atMessage = new HashMap<String, Object>();
        atMessage.put("version", "1.0");
        atMessage.put("applicationId", ussdRequestMessage.getApplicationId());
        atMessage.put("sessionId", ussdRequestMessage.getSessionId());
        atMessage.put("ussdOperation", ussdRequestMessage.getUssdOperation());
        atMessage.put("sourceAddress", "tel:"+ ussdRequestMessage.getSourceAddress());
        atMessage.put("vlrAddress", "some vlr address");
        atMessage.put("message", ussdRequestMessage.getMessage());
        atMessage.put("encoding", "440");
        atMessage.put("requestId", System.currentTimeMillis());
		return messageSender.sendMessage(atMessage, url);
	}

	public void setMessageSender(AtRequestSender messageSender) {
		this.messageSender = messageSender;
	}
}