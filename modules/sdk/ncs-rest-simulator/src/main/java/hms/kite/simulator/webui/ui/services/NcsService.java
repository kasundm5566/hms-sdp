/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.webui.ui.services;

import com.vaadin.ui.Label;
import hms.kite.simulator.RequestMessage;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface NcsService {

	Map<String, Object> sendMessage(RequestMessage requestMessage, String url) throws IOException;

	List receivedMessages();

	void updatePhoneView(Label phoneNum, Label message, Object val);
}
