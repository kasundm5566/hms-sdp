/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.webui.ui.WP;

import com.vaadin.ui.Button;
import com.vaadin.ui.Table;
import hms.kite.simulator.ServiceLocator;
import hms.kite.simulator.WP.WPMessageReceiver;
import hms.kite.simulator.sms.SmsRequestMessage;
import hms.kite.simulator.webui.StaticPropertiesService;
import hms.kite.simulator.webui.ui.services.NcsService;
import hms.kite.simulator.webui.ui.services.NcsUIService;
import hms.kite.simulator.webui.ui.services.impl.WPNcsService;

import java.text.SimpleDateFormat;
import java.util.Date;

public class WPNcsUIService implements NcsUIService {
    private NcsService wpdNcsService;

    private Table sentMsgTable;
    private int sentRowCount = 1;
    private Table receivedMsgTable;

    @Override
    public void init() {
        sentMsgTable = new Table(StaticPropertiesService.getInstant().getMessage("SMSTab.sentMessageTable.Title"));
        sentMsgTable.setStyleName("sent-message-table");
        receivedMsgTable = new Table(StaticPropertiesService.getInstant().getMessage("SMSTab.ReceivedMessageTable.Title"));
        receivedMsgTable.setStyleName("sent-message-table");
        wpdNcsService = (WPNcsService) ServiceLocator.getBean("wpNcsService");
    }

    @Override
    public Table createSentMessageService() {

        String[] headings = {
                StaticPropertiesService.getInstant().getMessage("SMSTab.sentMessageTable.TimeColumn"),
                StaticPropertiesService.getInstant().getMessage("SMSTab.sentMessageTable.numberColumn"),
                StaticPropertiesService.getInstant().getMessage("SMSTab.sentMessageTable.messageColumn"),
                StaticPropertiesService.getInstant().getMessage("SMSTab.sentMessageTable.statusColumn")
        };
        for (String heading : headings) {
            sentMsgTable.addContainerProperty(heading, String.class, null);
        }
        sentMsgTable.setColumnExpandRatio(headings[0], 0.08f);
        sentMsgTable.setColumnExpandRatio(headings[1], 0.25f);
        sentMsgTable.setColumnExpandRatio(headings[2], 0.57f);
        sentMsgTable.setColumnExpandRatio(headings[3], 0.10f);

        sentMsgTable.setHeight("100%");
        sentMsgTable.setWidth("100%");
        return sentMsgTable;
    }

    @Override
    public Table createReceivedMessageService() {

        String[] headings = {
                StaticPropertiesService.getInstant().getMessage("SMSTab.ReceivedMessageTable.TimeColumn"),
                StaticPropertiesService.getInstant().getMessage("SMSTab.ReceivedMessageTable.numberColumn"),
                StaticPropertiesService.getInstant().getMessage("WPTab.ReceivedMessageTable.TitleColumn"),
                StaticPropertiesService.getInstant().getMessage("WPTab.ReceivedMessageTable.url")
        };
        for (String heading : headings) {
            receivedMsgTable.addContainerProperty(heading, String.class, null);
        }
        receivedMsgTable.setColumnExpandRatio(headings[0], 0.10f);
        receivedMsgTable.setColumnExpandRatio(headings[1], 0.25f);
        receivedMsgTable.setColumnExpandRatio(headings[2], 0.35f);
        receivedMsgTable.setColumnExpandRatio(headings[3], 0.40f);

        receivedMsgTable.setHeight("100%");
        receivedMsgTable.setWidth("100%");
        return receivedMsgTable;
    }

    @Override
    public void addElementToReceiveTable(int objectId, Object object) {

        if (receivedMsgTable.getItem(objectId) == null) {
            SmsRequestMessage smsAoRequestMessage = (SmsRequestMessage) object;
            SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
            receivedMsgTable.addItem(new Object[] { dateFormat.format(new Date()), smsAoRequestMessage.getSourceAddress(),
                    smsAoRequestMessage.getMessage(), }, objectId);
        }

    }

    @Override
    public void addElementToSentTable(String date, String address, String message, String status) {

        sentMsgTable.addItem(new Object[] { date, address, message, status }, sentRowCount);
        sentRowCount++;
    }

    /**
     * this method creates a button to clear the recieved messsages table
     *
     * @return
     */
    public Button createClearReceivedMessagesButton() {

        Button clearButton = new Button(StaticPropertiesService.getInstant().getMessage("SMSTab.ReceivedMessageTable.clearButtonText"), new Button.ClickListener() {

            public void buttonClick(com.vaadin.ui.Button.ClickEvent clickEvent) {

                // clear the wap-push message table
                receivedMsgTable.removeAllItems();

                // clears the wap-push message list.
                WPMessageReceiver.ClearMessageList();
            }
        });
        return clearButton;
    }

    /**
     * this method creates a button to clear the sent sms table meanwhile
     * it sets the row number variable to 1
     *
     * @return
     */
    public Button createClearSentMessagesButton() {

        Button clearButton = new Button(StaticPropertiesService.getInstant().getMessage("SMSTab.sentMessageTable.clearButtonText"), new Button.ClickListener() {

            public void buttonClick(Button.ClickEvent clickEvent) {

                // clear message table
                sentMsgTable.removeAllItems();

                // set the row count.
                sentRowCount = 1;
            }
        });
        return clearButton;
    }

    // @Override
    public NcsService getNcsService() {
        return wpdNcsService;
    }
}