/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.WP;

import hms.kite.simulator.MessageReceiver;
import hms.kite.simulator.WpDeliveryReportSender;
import hms.kite.simulator.common.SmsDeliveryStatus;
import hms.kite.simulator.rest.ResponseBuilder;
import hms.kite.simulator.webui.ui.tab.impl.TabViewSmsImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class WPMessageReceiver implements MessageReceiver {

    private static final Logger logger = LoggerFactory.getLogger(WPMessageReceiver.class);

    private static final List<WPRequestMessage> receivedWp = new ArrayList<WPRequestMessage>();
    private ScheduledThreadPoolExecutor deliveryReportExecutor = new ScheduledThreadPoolExecutor(10);
    private WpDeliveryReportSender wpDeliveryReportSender;

    @Override
    public Map<String, Object> onMessage(Map<String, Object> message) {

        String[] statusReq = getRequestedStatus();
        List destinationRes = createDestinationResponses(message, statusReq);
        WPRequestMessage wpRequestMessage = decodeMessage(message);
        receivedWp.add(wpRequestMessage);
        sendDeliveryReport(message, statusReq[0], wpRequestMessage);
        return ResponseBuilder.generate("0.1", String.valueOf(System.currentTimeMillis()), statusReq[0], statusReq[1], destinationRes);
    }

    private WPRequestMessage decodeMessage(Map<String, Object> message) {
        WPRequestMessage wpReqMessage = new WPRequestMessage();
        String address = String.valueOf(message.get("destinationAddresses"));
        address = address.replace("[", "").replace("]", "");
        wpReqMessage.setDestinationAddresses(address);
        wpReqMessage.setTitle(String.valueOf(message.get("title")));
        wpReqMessage.setWapUrl(String.valueOf(message.get("wapUrl")));
        wpReqMessage.setDeliveryStatusRequest(String.valueOf(message.get("deliveryStatusRequest")));
        return wpReqMessage;
    }

    private void sendDeliveryReport(Map<String, Object> message, String status, WPRequestMessage smsRequestMessage) {
        if (smsRequestMessage.getDeliveryStatusRequest().equals("1")) {
            deliveryReportSender(message, status);
        }
    }

    private void deliveryReportSender(Map<String, Object> message, String status) {
        String addresses = String.valueOf(message.get("destinationAddresses"));
        addresses = addresses.replace("[", "").replace("]", "").replace(" ","");
        String[] addressArray = addresses.split(",");
        String deliveryStatus;
        if (status.equals("S1000")) {
            deliveryStatus = SmsDeliveryStatus.DELIVERED.getStatus();
        } else {
            deliveryStatus = SmsDeliveryStatus.UNDELIVERABLE.getStatus();
        }

        for (String address : addressArray) {
            setAndSendingDeliveryReport(address, deliveryStatus);
        }
    }

    private String[] getRequestedStatus() {
        return TabViewSmsImpl.getSmsErrorCodeRequest().split(":");
    }

    private void setAndSendingDeliveryReport(final String address, final String deliveryStatus) {
        deliveryReportExecutor.schedule(new Runnable() {
            @Override
            public void run() {
                try {
                    WpDeliveryReportRequest deliveryReport = new WpDeliveryReportRequest();
                    deliveryReport.setDestinationAddress(address);
                    deliveryReport.setDeliverStatus(deliveryStatus);

                    wpDeliveryReportSender.sendMessage(deliveryReport, TabViewSmsImpl.deliveryReportUrl());
                } catch (Exception e) {
                    logger.error("Unexpected exception occurred while trying to send delivery report", e);
                }
            }
        }, 5, TimeUnit.SECONDS);
    }

    private List createDestinationResponses(Map<String, Object> message, String[] status) {

        String addressList = String.valueOf(message.get("destinationAddresses"));
        String[] addressArray = addressList.replace("[", "").replace("]", "").split(",");

        List<Map<String, Object>> resList = new ArrayList<Map<String, Object>>();
        for (String address : addressArray) {
            Map<String, Object> entries = new HashMap<String, Object>();

            SimpleDateFormat timeStamp = new SimpleDateFormat("yyyyMMddHHmmss");

            entries.put("address", address);
            entries.put("timeStamp", timeStamp.format(new Date()));
            entries.put("requestId", String.valueOf(System.currentTimeMillis()));
            entries.put("statusCode", status[0]);
            entries.put("statusDetail", status[1]);

            resList.add(entries);

        }
        return resList;
    }

    @Override
    public List<WPRequestMessage> getReceivedMessages() {
        return receivedWp;
    }

    public static void ClearMessageList() {
        receivedWp.clear();
    }

    public void setWpDeliveryReportSender(WpDeliveryReportSender wpDeliveryReportSender) {
        this.wpDeliveryReportSender = wpDeliveryReportSender;
    }
}