/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.webui.ui.subscription;

import com.vaadin.ui.Button;
import com.vaadin.ui.Table;
import hms.kite.simulator.subscription.SubscriptionMessageReceiver;
import hms.kite.simulator.subscription.SubscriptionRequestMessage;
import hms.kite.simulator.webui.StaticPropertiesService;
import hms.kite.simulator.webui.ui.services.NcsService;
import hms.kite.simulator.webui.ui.services.NcsUIService;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SubNcsUIService implements NcsUIService{

    private NcsService messageReceiver;

    private Table sentMsgTable;
    private int sentRowCount = 1;
    private Table receivedMsgTable;
    private Table subResponseTable;

    @Override
    public void init() {
        sentMsgTable = new Table(StaticPropertiesService.getInstant().getMessage("SMSTab.sentMessageTable.Title"));
        sentMsgTable.setStyleName("sent-message-table");
        receivedMsgTable = new Table(StaticPropertiesService.getInstant().getMessage("SubTab.ReceivedMessageTable.Title"));
        receivedMsgTable.setStyleName("sent-message-table");
        subResponseTable = new Table(StaticPropertiesService.getInstant().getMessage(""));
        subResponseTable.setStyleName("sent-message-table");
//        messageReceiver = (SubscriptionNcsService) ServiceLocator.getBean("subscriptionNcsService");
    }


    @Override
    public Table createSentMessageService() {
        return sentMsgTable;
    }



    @Override
    public Table createReceivedMessageService() {

        String[] headings = {
                StaticPropertiesService.getInstant().getMessage("SMSTab.ReceivedMessageTable.TimeColumn"),
                StaticPropertiesService.getInstant().getMessage("SubTab.ReceivedMessageTable.addId"),
                StaticPropertiesService.getInstant().getMessage("SubTab.ReceivedMessageTable.subscriberId"),
                StaticPropertiesService.getInstant().getMessage("SubTab.ReceivedMessageTable.action")
        };
        for (String heading : headings) {
            receivedMsgTable.addContainerProperty(heading, String.class, null);
        }
        receivedMsgTable.setColumnExpandRatio(headings[0], 0.10f);
        receivedMsgTable.setColumnExpandRatio(headings[1], 0.30f);
        receivedMsgTable.setColumnExpandRatio(headings[2], 0.30f);
        receivedMsgTable.setColumnExpandRatio(headings[3], 0.30f);

        receivedMsgTable.setHeight("100%");
        receivedMsgTable.setWidth("100%");
        return receivedMsgTable;
    }

    @Override
    public void addElementToReceiveTable(int objectId, Object object) {

        if (receivedMsgTable.getItem(objectId) == null) {
            SubscriptionRequestMessage subRequestMessage = (SubscriptionRequestMessage) object;
            SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
            receivedMsgTable.addItem(new Object[] { dateFormat.format(new Date()), subRequestMessage.getApplicationId(),
                    subRequestMessage.getSubscriberId(), subRequestMessage.getAction()}, objectId);
        }

    }

    @Override
    public void addElementToSentTable(String date, String address, String message, String status) {

        sentMsgTable.addItem(new Object[] { date, address, message, status }, sentRowCount);
        sentRowCount++;
    }

    /**
     * this method creates a button to clear the recieved messsages table
     *
     * @return
     */
    public Button createClearReceivedMessagesButton() {

        Button clearButton = new Button(StaticPropertiesService.getInstant().getMessage("SMSTab.ReceivedMessageTable.clearButtonText"), new Button.ClickListener() {

            public void buttonClick(com.vaadin.ui.Button.ClickEvent clickEvent) {

                // clear the message table
                receivedMsgTable.removeAllItems();
                SubscriptionMessageReceiver.receivedMessages.clear();
            }
        });
        return clearButton;
    }

    /**
     * this method creates a button to clear the sent sms table meanwhile
     * it sets the row number variable to 1
     *
     * @return
     */
    public Button createClearSentMessagesButton() {

        Button clearButton = new Button(StaticPropertiesService.getInstant().getMessage("SMSTab.sentMessageTable.clearButtonText"), new Button.ClickListener() {

            public void buttonClick(Button.ClickEvent clickEvent) {

                // clear message table
                sentMsgTable.removeAllItems();

                // set the row count.
                sentRowCount = 1;
            }
        });
        return clearButton;
    }

    @Override
    public NcsService getNcsService() {
        return messageReceiver;
    }
}