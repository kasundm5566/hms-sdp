/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.webui.ui.tab.impl;

import com.github.wolfie.refresher.Refresher;
import com.vaadin.data.Property;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.BaseTheme;
import hms.kite.simulator.MessageReceiver;
import hms.kite.simulator.ServiceLocator;
import hms.kite.simulator.caas.CaasDirectDebitMessageReceiver;
import hms.kite.simulator.caas.CaasRequestMessage;
import hms.kite.simulator.caas.ChargingNotificationBuilder;
import hms.kite.simulator.caas.ChargingNotificationMessage;
import hms.kite.simulator.common.ChargingStatus;
import hms.kite.simulator.webui.StaticPropertiesService;
import hms.kite.simulator.webui.ui.services.NcsUIService;
import hms.kite.simulator.webui.ui.tab.TabViewCommon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TabViewCasImpl extends TabViewCommon {

    private static final Logger LOGGER = LoggerFactory.getLogger(TabViewCasImpl.class);

    private static final int REFRESH_INTERVAL = 4000;

    private ScheduledExecutorService executorService;

    private int sentRowCount = 1;
    private Table directDebitRequestTable;
    private Table chargingRequestTable;
    private Table notificationSendingTable;
    private Button receiveDirectDebitMsgClearButton;
    private Button receiveChargingReqMsgClearButton;
    private Button notificationClearButton;
    private Refresher refresher;
    private MessageReceiver caasDirectDebitReceiver;
    private MessageReceiver caasChargingRequestReceiver;

    public static final String PAY_BILL = "M-Pesa-Paybill";
    public static final String BUY_GOODS = "M-Pesa-Buygoods";
    public static final String PAYMENT_ALLOW = "allow";
    public static final String PAYMENT_DISALLOW = "disallow";

    private static List<String> usedPaymentInstruments = new ArrayList<String>();
    private static Map<String, Double> totalPayments = new HashMap<String, Double>();
    private String transactionId = null;
    private String currency = "KES";
    private double amountToPay = 0.0;
    private double remainsToPay = 0.0;
    private TextField accountIdTxt;
    private TextField referenceIdTxt;
    private TextField businessNoTxt;
    private TextField amountTxt;
    private Label amountLbl;
    private TextField notificationUrlTxt;
    public static ComboBox chargStatusComboBox;

    boolean isOverAllowed = false;
    boolean isPartialAllowed = false;

    private Layout paymentInstrumentLayout;
    private VerticalLayout paymentDescriptionContent;
    private VerticalLayout paymentInstrumentContent;

    public TabViewCasImpl(NcsUIService ncsUIService) {
        init();
        chargingRequestTable = new Table(StaticPropertiesService.getInstant().getMessage("CaasTab.ReceivedMessageTable.DirectDebit.Title"));
        notificationSendingTable = new Table(StaticPropertiesService.getInstant().getMessage("CaasTab.Notification.Table.Title"));
        directDebitRequestTable = ncsUIService.createReceivedMessageService();
        chargingRequestTable = createChargingReqMessageTable();
        notificationSendingTable = createResponseTable();
        chargingRequestTable.setStyleName("sent-message-table");
        notificationSendingTable.setStyleName("sent-message-table");
        receiveDirectDebitMsgClearButton = ncsUIService.createClearReceivedMessagesButton();
        receiveChargingReqMsgClearButton = createClearChargingMessageTableButton();
        notificationClearButton = createClearNotificationTableButton();

        refresher = new Refresher();
        refresher.setRefreshInterval(REFRESH_INTERVAL);
    }

    public void init() {
        caasDirectDebitReceiver = (MessageReceiver) ServiceLocator.getBean("caasDirectCreditMessageReceiver");
        caasChargingRequestReceiver = (MessageReceiver) ServiceLocator.getBean("caasDirectDebitMessageReceiver");
        if (executorService == null) {
            executorService = Executors.newScheduledThreadPool(1);
            executorService.scheduleAtFixedRate(new Runnable() {
                public void run() {
                    try {
                        List receivedMessages = caasDirectDebitReceiver.getReceivedMessages();
                        List receiveChargingReq = caasChargingRequestReceiver.getReceivedMessages();
                        for (int i = 0, receivedMessagesSize = receivedMessages.size(); i < receivedMessagesSize; i++) {
                            addElementToDirectCreditReceiveTable(i, receivedMessages.get(i));
                        }
                        for (int i = 0, receivedMessagesSize = receiveChargingReq.size(); i < receivedMessagesSize; i++) {
                            addElementToDirectDebitReceiveTable(i, receiveChargingReq.get(i));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 4, 4, TimeUnit.SECONDS);
        }
    }

    private void addElementToDirectCreditReceiveTable(int objectId, Object object) {

        if (directDebitRequestTable.getItem(objectId) == null) {
            CaasRequestMessage casReqMsg = (CaasRequestMessage) object;
            SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
            directDebitRequestTable.addItem(new Object[]{dateFormat.format(new Date()), casReqMsg.getApplicationId(), casReqMsg.getPaymentInstrumentName(),
                    casReqMsg.getAccountId(), casReqMsg.getAmount()}, objectId);
        }
    }

    private void addElementToDirectDebitReceiveTable(int objectId, Object object) {

        if (chargingRequestTable.getItem(objectId) == null) {
            CaasRequestMessage casReqMsg = (CaasRequestMessage) object;
            chargingRequestTable.addItem(new Object[]{casReqMsg.getExternalTrxId(), casReqMsg.getSubscriberId(),
                    casReqMsg.getApplicationId(), casReqMsg.getAmount(),
                    createLink(casReqMsg)}, objectId);
        }
    }

    private Button createLink(final CaasRequestMessage dataToLink) {
        final Button paymentInstrumentLink = new Button(dataToLink.getPaymentInstrumentName());
        paymentInstrumentLink.setStyleName(BaseTheme.BUTTON_LINK);
        paymentInstrumentLink.setImmediate(true);
        paymentInstrumentLink.setData(dataToLink);
        boolean isAsync = false;
        String[] data;
        String[] paymentInt = StaticPropertiesService.getInstant().getMessage("Asynchronous.payment.Instrument").replace(" ", "").split(",");
        for (String s : paymentInt) {
            data = s.split(":");
            if (dataToLink.getPaymentInstrumentName().equals(data[0])) {
                isAsync = data[1].equals("ASYNC");
                break;
            }
        }
        final boolean finalIsAsync = isAsync;
        paymentInstrumentLink.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                if (finalIsAsync) {
                    isPartialAllowed = false;
                    isOverAllowed = false;
                    CaasRequestMessage content = (CaasRequestMessage) paymentInstrumentLink.getData();

                    transactionId = content.getExternalTrxId();
                    currency = content.getCurrency();

                    if (PAYMENT_ALLOW.startsWith(content.getAllowPartialPayments())) {
                        isPartialAllowed = true;
                    }

                    if (PAYMENT_ALLOW.startsWith(content.getAllowOverPayments())) {
                        isOverAllowed = true;
                    }

                    if (!totalPayments.containsKey(transactionId)) {
                        totalPayments.put(transactionId, 0.0);
                    }
                    resetTextFields();
                    amountLbl.setValue("Amount pay for transaction ID : " + transactionId);
                    double amountReq = Double.parseDouble(content.getAmount());

                    if (PAY_BILL.equals(content.getPaymentInstrumentName())) {
                        amountToPay = amountReq + amountReq * 0.05;
                    } else if (BUY_GOODS.equals(content.getPaymentInstrumentName())) {
                        amountToPay = amountReq;
                    } else {
                        amountToPay = amountReq + amountReq * 0.05;
                    }
                    paymentInstrumentLayout.removeAllComponents();
                    paymentInstrumentLayout.addComponent(createDescription());
                    paymentInstrumentLayout.addComponent(paymentInstrumentContent);
                } else {
                    Label label = new Label();
                    label.setValue(new StringBuilder().append("Payment already done for : ").
                            append(dataToLink.getPaymentInstrumentName()).
                            append(", since it is synchronous").toString());
                    paymentInstrumentLayout.removeAllComponents();
                    paymentInstrumentLayout.addComponent(label);
                }
            }
        });
        return paymentInstrumentLink;
    }

    private void resetTextFields() {
        amountTxt.setValue("0");
        accountIdTxt.setValue("");
        businessNoTxt.setValue("");
        referenceIdTxt.setValue("");
    }

    public void addElementToSentTable(String transactionId, String referenceId, String businessNo, String accountId, String amount, String pending, String total) {

        notificationSendingTable.addItem(new Object[]{transactionId, referenceId, businessNo, accountId, amount, pending, total}, sentRowCount);
        sentRowCount++;
    }

    private Button createClearChargingMessageTableButton() {
        Button clearButton = new Button(StaticPropertiesService.getInstant().getMessage("SMSTab.ReceivedMessageTable.clearButtonText"), new Button.ClickListener() {

            public void buttonClick(com.vaadin.ui.Button.ClickEvent clickEvent) {

                chargingRequestTable.removeAllItems();
                CaasDirectDebitMessageReceiver.receivedMessagesCas.clear();
                paymentInstrumentLayout.removeAllComponents();
            }
        });
        return clearButton;

    }

    private Button createClearNotificationTableButton() {
        Button clearButton = new Button(StaticPropertiesService.getInstant().getMessage("SMSTab.ReceivedMessageTable.clearButtonText"), new Button.ClickListener() {

            public void buttonClick(com.vaadin.ui.Button.ClickEvent clickEvent) {

                notificationSendingTable.removeAllItems();
            }
        });
        return clearButton;

    }

    public Table createChargingReqMessageTable() {

        String[] headings = {
                StaticPropertiesService.getInstant().getMessage("CaasTab.ReceivedMessageTable.TransactionId"),
                StaticPropertiesService.getInstant().getMessage("CaasTab.ReceivedMessageTable.SubscriberId"),
                StaticPropertiesService.getInstant().getMessage("CaasTab.ReceivedMessageTable.ApplicationId"),
                StaticPropertiesService.getInstant().getMessage("CaasTab.ReceivedMessageTable.amount")
        };
        for (String heading : headings) {
            chargingRequestTable.addContainerProperty(heading, String.class, null);
        }
        String name = StaticPropertiesService.getInstant().getMessage("CaasTab.ReceivedMessageTable.instrument");
        chargingRequestTable.addContainerProperty(name, Button.class, null);

        chargingRequestTable.setColumnExpandRatio(headings[0], 0.20f);
        chargingRequestTable.setColumnExpandRatio(headings[1], 0.20f);
        chargingRequestTable.setColumnExpandRatio(headings[2], 0.20f);
        chargingRequestTable.setColumnExpandRatio(headings[3], 0.20f);
        chargingRequestTable.setColumnExpandRatio(name, 0.20f);

        chargingRequestTable.setHeight("100%");
        chargingRequestTable.setWidth("100%");
        return chargingRequestTable;
    }

    public Table createResponseTable() {

        String[] headings = {
                StaticPropertiesService.getInstant().getMessage("CaasTab.Notification.TransactionId"),
                StaticPropertiesService.getInstant().getMessage("CaasTab.Notification.ReferenceId"),
                StaticPropertiesService.getInstant().getMessage("CaasTab.Notification.BusinessNo"),
                StaticPropertiesService.getInstant().getMessage("CaasTab.Notification.AccountId"),
                StaticPropertiesService.getInstant().getMessage("CaasTab.Notification.Amount"),
                StaticPropertiesService.getInstant().getMessage("CaasTab.Notification.Pending"),
                StaticPropertiesService.getInstant().getMessage("CaasTab.Notification.Total"),
        };
        for (String heading : headings) {
            notificationSendingTable.addContainerProperty(heading, String.class, null);
        }
        notificationSendingTable.setColumnExpandRatio(headings[0], 0.15f);
        notificationSendingTable.setColumnExpandRatio(headings[1], 0.15f);
        notificationSendingTable.setColumnExpandRatio(headings[2], 0.15f);
        notificationSendingTable.setColumnExpandRatio(headings[3], 0.15f);
        notificationSendingTable.setColumnExpandRatio(headings[4], 0.10f);
        notificationSendingTable.setColumnExpandRatio(headings[5], 0.15f);
        notificationSendingTable.setColumnExpandRatio(headings[6], 0.15f);

        notificationSendingTable.setHeight("100%");
        notificationSendingTable.setWidth("100%");
        return notificationSendingTable;
    }

    private Component createPaymentInstrumentSet() {

        Panel inputPanel = new Panel(
                StaticPropertiesService.getInstant().getMessage("Tab.InputPanel.ChargingType.Request"));
        inputPanel.setWidth("295px");

        Label paymentInstrumentSetHeading = new Label("Payment Instruments");
        paymentInstrumentSetHeading.setStyleName("input-panel-section-header");

        Label chargingStatusLabel = new Label("Charging Status");
        chargingStatusLabel.setStyleName("input-panel-section-header");


        createChargingSettingsContent();
        createChargingDescriptionContent();
        inputPanel.addComponent(paymentInstrumentSetHeading);
        inputPanel.addComponent(createPaymentInstrumentCheckBoxes());
        inputPanel.addComponent(createNotificationSendingUrlTxtField());
        inputPanel.addComponent(createPayInstLayout());
        inputPanel.addComponent(chargingStatusLabel);
        inputPanel.addComponent(createDDChrgStatusComboBox());

        final AbstractOrderedLayout content = (AbstractOrderedLayout) inputPanel.getContent();
        content.setSpacing(true);

        return inputPanel;
    }

    private VerticalLayout createPaymentInstrumentCheckBoxes() {
        HorizontalLayout checkBoxLayout = new HorizontalLayout();
        VerticalLayout verticalLayout = new VerticalLayout();
        CheckBox checkBox;
        Label checkBoxName;
        String[] paymentInt = StaticPropertiesService.getInstant().getMessage("Asynchronous.payment.Instrument").replace(" ", "").split(",");
        for (String s : paymentInt) {
            checkBox = new CheckBox();
            checkBox.setData(s);
            addListenerForCheckBox(checkBox);
            checkBoxName = new Label(s.split(":")[0]);
            checkBoxLayout.addComponent(checkBox);
            checkBoxLayout.addComponent(checkBoxName);
            verticalLayout.addComponent(checkBoxLayout);
        }
        return verticalLayout;
    }

    private void addListenerForCheckBox(final CheckBox checkBox) {
        checkBox.setImmediate(true);
        checkBox.addListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                try {
                    String data = checkBox.getData().toString();
                    addPaymentInstruments(valueChangeEvent, data);
                } catch (Exception e) {
                    LOGGER.error("Unexpected error occurred while selecting charging method", e);
                }
            }

        });
    }

    private void addPaymentInstruments(Property.ValueChangeEvent valueChangeEvent, String instrument) {
        if ((Boolean) valueChangeEvent.getProperty().getValue()) {
            usedPaymentInstruments.add(instrument);
        } else {
            usedPaymentInstruments.remove(instrument);
        }
    }

    private Component createChargingDescriptionContent() {
        paymentDescriptionContent = new VerticalLayout();
        paymentDescriptionContent.setSpacing(true);
        return paymentDescriptionContent;
    }

    private Component createDescription() {
        Label description = new Label();
        description.setSizeFull();
        description.setHeight("50px");
        description.setStyleName("charging-description");

        Map descriptionForRequest = CaasDirectDebitMessageReceiver.getDescriptionForRequest();
        description.setValue(descriptionForRequest.get(transactionId));
        Label descriptionHeading = new Label(StaticPropertiesService.getInstant().getMessage("Caas.Description.Sent.To.Customer"));
        descriptionHeading.setStyleName("input-panel-section-header");

        paymentDescriptionContent.removeAllComponents();
        paymentDescriptionContent.addComponent(descriptionHeading);
        paymentDescriptionContent.addComponent(description);
        return paymentDescriptionContent;
    }

    private HorizontalLayout setTextField(AbstractTextField txtField, String value) {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setSpacing(true);
        txtField.setWidth("60px");
        Label label = new Label(value);
        label.setWidth("220px");
        horizontalLayout.addComponent(label);
        horizontalLayout.addComponent(txtField);
        return horizontalLayout;
    }

    private Component createChargingSettingsContent() {

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setSpacing(true);
        amountTxt = new TextField();
        amountTxt.setValue("0");
        amountTxt.setWidth("60px");
        referenceIdTxt = new TextField();
        businessNoTxt = new TextField();
        accountIdTxt = new TextField();
        amountLbl = new Label();
        amountLbl.setWidth("220px");

        paymentInstrumentContent = new VerticalLayout();
        paymentInstrumentContent.setSpacing(true);
        horizontalLayout.addComponent(amountLbl);
        horizontalLayout.addComponent(amountTxt);

        Label paymentHeading = new Label("Payments");
        paymentHeading.setStyleName("input-panel-section-header");

        paymentInstrumentContent.addComponent(paymentHeading);
        paymentInstrumentContent.addComponent(horizontalLayout);
        paymentInstrumentContent.addComponent(setTextField(accountIdTxt,
                StaticPropertiesService.getInstant().getMessage("Caas.Payment.AccountNo")));
        paymentInstrumentContent.addComponent(setTextField(businessNoTxt,
                StaticPropertiesService.getInstant().getMessage("Caas.Payment.BusinessNo")));
        paymentInstrumentContent.addComponent(setTextField(referenceIdTxt,
                StaticPropertiesService.getInstant().getMessage("Caas.Payment.ReferenceId")));

        Button paymentButton = new Button("PAY");
        paymentButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                try {
                    ChargingNotificationMessage noteMsg;

                    double paidAmount = Double.parseDouble(amountTxt.getValue().toString());
                    String referenceIdValue = referenceIdTxt.getValue().toString();
                    String businessNoValue = businessNoTxt.getValue().toString();
                    String accountIdValue = accountIdTxt.getValue().toString();


                    DecimalFormat twoDForm = new DecimalFormat("#.##");
                    remainsToPay = Double.parseDouble(twoDForm.format(amountToPay - paidAmount - totalPayments.get(transactionId)));
                    noteMsg = generateAppropriateResponse(paidAmount, referenceIdValue);

                    String url = getNotificationUrlTxt().getValue().toString();
                    Map<String, Object> response = ChargingNotificationBuilder.sendNotification(noteMsg, url);
                    String remainsToPayValue = String.valueOf(amountToPay);
                    if (noteMsg.getStatusCode().equals("S1000")) {
                        remainsToPayValue = String.valueOf(remainsToPay);
                    }

                    LOGGER.info("Send the charging notification: {} to application, and received response : {}", noteMsg, response);
                    addElementToSentTable(transactionId, referenceIdValue, businessNoValue, accountIdValue, String.valueOf(paidAmount),
                            remainsToPayValue, totalPayments.get(transactionId).toString());
                } catch (Exception e) {
                    LOGGER.error("Unexpected exception occurred while sending notification", e);
                }
            }
        });
        paymentInstrumentContent.addComponent(paymentButton);

        return paymentInstrumentContent;
    }

    private ChargingNotificationMessage generateAppropriateResponse(double paidAmount, String referenceIdValue) {
        ChargingNotificationMessage noteMsg;
        double availablePayment = totalPayments.get(transactionId);
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        double total = Double.parseDouble(twoDForm.format(availablePayment + paidAmount));
        String statusCode;
        String statusDetail;
        if (isOverAllowed) {
            if (isPartialAllowed) {
                if (remainsToPay >= 0) {
                    totalPayments.put(transactionId, total);
                    statusCode = "S1000";
                    statusDetail = "Partial payment was successfully processed";
                } else {
                    totalPayments.put(transactionId, total);
                    statusCode = "S1000";
                    statusDetail = "Success";
                }

            } else {
                if (remainsToPay <= 0) {
                    totalPayments.put(transactionId, total);
                    statusCode = "S1000";
                    statusDetail = "Success";
                } else {
                    statusCode = "E1401";
                    statusDetail = "Partial Payment not allowed";
                }
            }
        } else {
            if (isPartialAllowed) {
                if (remainsToPay >= 0) {
                    totalPayments.put(transactionId, total);
                    statusCode = "S1000";
                    statusDetail = "Success";
                } else {
                    statusCode = "E1402";
                    statusDetail = "Over Payment not allowed";
                }
            } else {
                if (remainsToPay == 0) {
                    totalPayments.put(transactionId, total);
                    statusCode = "S1000";
                    statusDetail = "Success";
                } else if (remainsToPay > 0) {
                    statusCode = "E1401";
                    statusDetail = "Partial Payment not allowed";
                } else {
                    statusCode = "E1402";
                    statusDetail = "Over Payment not allowed";
                }
            }
        }
        SimpleDateFormat timeStamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        String internalTrxId = CaasDirectDebitMessageReceiver.getInternalTransactionId().get(transactionId);

        noteMsg = ChargingNotificationBuilder.createNotification(
                transactionId, internalTrxId, referenceIdValue, String.valueOf(paidAmount),
                totalPayments.get(transactionId).toString(), String.valueOf(remainsToPay), currency, statusCode, statusDetail, timeStamp.format(new Date()));
        return noteMsg;
    }

    private HorizontalLayout createNotificationSendingUrlTxtField() {
        HorizontalLayout urlLayout = new HorizontalLayout();
        urlLayout.setSpacing(true);
        Label urlLbl = new Label(StaticPropertiesService.getInstant().getMessage("CaasTab.payment.Instrument.Notification.url"));
        notificationUrlTxt = new TextField();
        urlLbl.setWidth("120px");
        notificationUrlTxt.setWidth("260px");
        notificationUrlTxt.setValue(StaticPropertiesService.getInstant().getMessage("Caas.notification.Url"));
        urlLayout.addComponent(urlLbl);
        urlLayout.addComponent(notificationUrlTxt);
        return urlLayout;
    }

    private Component createPayInstLayout() {
        paymentInstrumentLayout = new VerticalLayout();
        return paymentInstrumentLayout;
    }

    @Override
    public Component getTabLayout() {

        VerticalLayout tabLayout = new VerticalLayout();
        tabLayout.setMargin(true);
        tabLayout.setSpacing(true);

        HorizontalLayout tabUpperLayout = new HorizontalLayout();
        tabUpperLayout.setWidth("100%");
        tabUpperLayout.setMargin(true);

        HorizontalLayout tabMiddleLayout = new HorizontalLayout();
        tabMiddleLayout.setWidth("100%");
        tabMiddleLayout.setMargin(true);

        VerticalLayout tableLayout = new VerticalLayout();
        tableLayout.setSpacing(true);
        tableLayout.setMargin(true);
        tableLayout.setWidth("100%");

        //Uncomment following for add the error code selecting window

/*        Component inputFieldPanel = createInputPanel();
        inputFieldPanel.setWidth("400px");
        tabUpperLayout.addComponent(inputFieldPanel);
        tabUpperLayout.setComponentAlignment(inputFieldPanel, Alignment.TOP_RIGHT);
        tabLayout.addComponent(tabUpperLayout);*/

        createPaymentInstrumentList(tabLayout, tabUpperLayout);

        VerticalLayout receivedDirectDebitTableOuterLayout = createTableLayout(chargingRequestTable);
        createButtonLayout(receivedDirectDebitTableOuterLayout, receiveChargingReqMsgClearButton);

        VerticalLayout sentNotificationOuterLayout = createTableLayout(notificationSendingTable);
        createButtonLayout(sentNotificationOuterLayout, notificationClearButton);

        tableLayout.addComponent(receivedDirectDebitTableOuterLayout);
        tableLayout.addComponent(sentNotificationOuterLayout);
        tableLayout.addComponent(refresher);
        tabLayout.addComponent(tableLayout);

        return tabLayout;
    }

    private void createButtonLayout(VerticalLayout receivedChargingReqTableOuterLayout, Button button) {
        HorizontalLayout chargingReqButtonLayout = new HorizontalLayout();
        chargingReqButtonLayout.setHeight("8px");
        chargingReqButtonLayout.setWidth("100%");
        chargingReqButtonLayout.setStyleName("bottom-layout");
        receivedChargingReqTableOuterLayout.addComponent(chargingReqButtonLayout);

        HorizontalLayout receivedChargingReqClearButtonLayout = new HorizontalLayout();
        receivedChargingReqClearButtonLayout.addComponent(button);
        receivedChargingReqClearButtonLayout.setHeight("30px");
        receivedChargingReqClearButtonLayout.setWidth("100%");
        receivedChargingReqClearButtonLayout.setComponentAlignment(button, Alignment.BOTTOM_RIGHT);
        receivedChargingReqTableOuterLayout.addComponent(receivedChargingReqClearButtonLayout);
    }

    private VerticalLayout createTableLayout(Table table) {
        VerticalLayout receivedChargingReqTableOuterLayout = new VerticalLayout();
        receivedChargingReqTableOuterLayout.setWidth("100%");
        receivedChargingReqTableOuterLayout.setMargin(false);
        receivedChargingReqTableOuterLayout.setSpacing(false);
        receivedChargingReqTableOuterLayout.setStyleName("table-outer-layout");

        HorizontalLayout receivedChargingReqTableLayout = new HorizontalLayout();
        receivedChargingReqTableLayout.setHeight("220px");
        receivedChargingReqTableLayout.setWidth("100%");
        receivedChargingReqTableLayout.setMargin(false);
        receivedChargingReqTableLayout.addComponent(table);
        receivedChargingReqTableOuterLayout.addComponent(receivedChargingReqTableLayout);
        return receivedChargingReqTableOuterLayout;
    }

    private void createPaymentInstrumentList(VerticalLayout tabLayout, HorizontalLayout tabMiddleLayout) {
        Component inputFieldPanel = createPaymentInstrumentSet();
        inputFieldPanel.setWidth("600px");
        tabMiddleLayout.addComponent(inputFieldPanel);
        tabMiddleLayout.setComponentAlignment(inputFieldPanel, Alignment.TOP_CENTER);
        tabLayout.addComponent(tabMiddleLayout);
    }


    private Component createDDChrgStatusComboBox() {
        HorizontalLayout subStatusLayout = new HorizontalLayout();
        subStatusLayout.setSpacing(true);
        Label ddStatusLabel = new Label(StaticPropertiesService.getInstant().getMessage("CaasTab.dd.status"));

        chargStatusComboBox = new ComboBox();
        //chargStatusComboBox.addItem(ChargingStatus.CHARGING_SUCCESS);
        chargStatusComboBox.addItem(ChargingStatus.PENDING);
        chargStatusComboBox.addItem(ChargingStatus.INVALID_REQUEST);
        chargStatusComboBox.setNullSelectionAllowed(false);
        chargStatusComboBox.setValue(ChargingStatus.PENDING);
        ddStatusLabel.setWidth("120px");
        chargStatusComboBox.setWidth("260px");
        subStatusLayout.addComponent(ddStatusLabel);
        subStatusLayout.addComponent(chargStatusComboBox);
        setChargStatusComboBox(chargStatusComboBox);
        return subStatusLayout;
    }

    public TextField getAmountTxt() {
        return amountTxt;
    }

    public TextField getNotificationUrlTxt() {
        return notificationUrlTxt;
    }

    public TextField getAccountIdTxt() {
        return accountIdTxt;
    }

    public TextField getBusinessNoTxt() {
        return businessNoTxt;
    }

    public TextField getReferenceIdTxt() {
        return referenceIdTxt;
    }

    public static List<String> getUsedPaymentInstruments() {
        return usedPaymentInstruments;
    }

    public static ComboBox getChargStatusComboBox() {
        return chargStatusComboBox;
    }

    public static void setChargStatusComboBox(ComboBox chargStatusComboBox) {
        TabViewCasImpl.chargStatusComboBox = chargStatusComboBox;
    }
}