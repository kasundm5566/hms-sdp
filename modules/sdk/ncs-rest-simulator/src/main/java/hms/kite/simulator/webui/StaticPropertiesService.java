/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.simulator.webui;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class StaticPropertiesService {

	private static StaticPropertiesService staticProperties = new StaticPropertiesService();

    private static Locale locale = new Locale("en_US");
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("ncs_rest_sim", locale);

	public static StaticPropertiesService getInstant() {
		return staticProperties;
	}

	private StaticPropertiesService() {
	}

    public String getMessage(String key, Object... arguments){
        try {
            if (arguments != null) {
                return MessageFormat.format(resourceBundle.getString(key), arguments);
            } else {
                return resourceBundle.getString(key);
            }
        } catch (MissingResourceException mre) {
            System.out.println("Message key not found [" + key + "]");
            return '!' + key;
        }
    }
}
