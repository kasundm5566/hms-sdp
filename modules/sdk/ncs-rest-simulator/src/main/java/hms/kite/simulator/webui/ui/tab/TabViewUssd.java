/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.webui.ui.tab;

import com.vaadin.ui.*;
import hms.kite.simulator.ErrorCodeLoader;
import hms.kite.simulator.ussd.OperationType;
import hms.kite.simulator.webui.FeaturePropertiesService;
import hms.kite.simulator.webui.StaticPropertiesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class TabViewUssd {
    private static Logger logger = LoggerFactory.getLogger(TabViewUssd.class);

    private TextField sessionIdTextField;
    private TextField userNumberTextField;
    private TextArea messageField;

    //url & appID fields added
    private TextField urlField;
    private TextField appIdField;
    private TextField passwordField;

    private ComboBox errorCodeComboBox;
    private ComboBox ussdOpTypeComboBox;


    public TabViewUssd() {
        init();
    }

    public void init() {
        sessionIdTextField = new TextField();
        userNumberTextField = new TextField();
        messageField = new TextArea();
        messageField.setHeight("90px");
        urlField = new TextField();
        appIdField = new TextField();
        passwordField = new TextField();
    }

    private Component createErrorCodeField() {
        HorizontalLayout errorCodeLayout = new HorizontalLayout();
        errorCodeLayout.setSpacing(true);
        Label errorCodeLbl = new Label(StaticPropertiesService.getInstant().getMessage("Tab.InputPanel.errorCode"));
        errorCodeComboBox = new ComboBox();
        errorCodeLbl.setWidth("120px");
        errorCodeComboBox.setWidth("220px");

        setSelectData(errorCodeComboBox);
        errorCodeComboBox.setNullSelectionAllowed(false);
        errorCodeComboBox.setValue("S1000:Success");

        errorCodeLayout.addComponent(errorCodeLbl);
        errorCodeLayout.addComponent(errorCodeComboBox);
        return errorCodeLayout;
    }

    private Component createUssdOpTypeComboBox() {
        HorizontalLayout layout = new HorizontalLayout();
        layout.setSpacing(true);
        Label label = new Label(StaticPropertiesService.getInstant().getMessage("Tab.InputPanel.ussdOpType"));
        ussdOpTypeComboBox = new ComboBox();
        label.setWidth("120px");
        ussdOpTypeComboBox.setWidth("220px");

        ussdOpTypeComboBox.addItem(OperationType.MO_INIT.getName());
        ussdOpTypeComboBox.addItem(OperationType.MO_CONT.getName());

        ussdOpTypeComboBox.setNullSelectionAllowed(false);
        ussdOpTypeComboBox.setValue(OperationType.MO_INIT.getName());

        layout.addComponent(label);
        layout.addComponent(ussdOpTypeComboBox);
        return layout;
    }

    /**
     * @return the Panel containing input text fields
     */
    public Component createInputPanel() {

        Panel inputPanel = new Panel(
                StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.Title"));
        inputPanel.setWidth("295px");

        Label applicationDataHeader = new Label(
                StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.ApplicationDataHeader"));
        applicationDataHeader.setStyleName("input-panel-section-header");
        inputPanel.addComponent(applicationDataHeader);

        inputPanel.addComponent(createPanelLayout(
                StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.URL"), urlField, "http://localhost:5555/mo-ussd"));
        inputPanel.addComponent(createPanelLayout(
                StaticPropertiesService.getInstant().getMessage("USSDTab.InputPanel.AppID"), appIdField, "APP_000001"));

        inputPanel.addComponent(createPanelLayout(
                StaticPropertiesService.getInstant().getMessage("USSDTab.InputPanel.password"), passwordField, "password"));

        Label messageDataHeader = new Label(
                StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.MessageDataHeader"));
        messageDataHeader.setStyleName("input-panel-section-header");
        inputPanel.addComponent(messageDataHeader);

        inputPanel.addComponent(createPanelLayout(
                StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.sessionId"), sessionIdTextField, "123"));
        inputPanel.addComponent(createUssdOpTypeComboBox());
        inputPanel.addComponent(createPanelLayout(
                StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.myNumber"), userNumberTextField, FeaturePropertiesService.getInstance().getMessage("USSDTab.InputPanel.customerNumber.defaultValue")));
        inputPanel.addComponent(createPanelLayout(
                StaticPropertiesService.getInstant().getMessage("USSDTab.InputPanel.message"), messageField, "*141#"));

        Label additionalDataLbl = new Label(StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.MessageAdditionalRequests"));
        additionalDataLbl.setStyleName("input-panel-section-header");
        inputPanel.addComponent(additionalDataLbl);

        inputPanel.addComponent(createErrorCodeField());

        setDescriptionsForFields();
        Button sendButton = createSendMsgButton();
        inputPanel.addComponent(sendButton);
        final AbstractOrderedLayout content = (AbstractOrderedLayout) inputPanel.getContent();
        content.setSpacing(true);
        content.setComponentAlignment(sendButton, Alignment.BOTTOM_RIGHT);

        return inputPanel;
    }

    private void setSelectData(ComboBox data) {
        ErrorCodeLoader errorCodeLoader = new ErrorCodeLoader();
        errorCodeLoader.setErrorDataForUssdList(data);
    }

    private HorizontalLayout createPanelLayout(final String msg, final AbstractTextField textField, final String textValue) {
        HorizontalLayout msgLayout = new HorizontalLayout();
        msgLayout.setSpacing(true);
        Label msgLabel = new Label(msg);
        msgLabel.setWidth("120px");
        textField.setWidth("220px");
        msgLayout.addComponent(msgLabel);
        msgLayout.addComponent(textField);
        textField.setValue(textValue);
        return msgLayout;
    }

    private void setDescriptionsForFields() {
        urlField.setDescription(StaticPropertiesService.getInstant().getMessage("URl.TextField.Description"));
        appIdField.setDescription(StaticPropertiesService.getInstant().getMessage("APPID.TextField.Description"));
        passwordField.setDescription(StaticPropertiesService.getInstant().getMessage("Password.TextField"));
        sessionIdTextField.setDescription(StaticPropertiesService.getInstant().getMessage("USSD.SessionID.TextField.Description"));
        userNumberTextField.setDescription(StaticPropertiesService.getInstant().getMessage("USSD.CustomerNumber.TextField.Description"));
        messageField.setDescription(StaticPropertiesService.getInstant().getMessage("USSD.Message.TextArea.Description"));
//        errorCodeComboBox.setDescription(StaticPropertiesService.getInstant().getMessage("USSD.StatusCode.Request.To.Generate.ComboBox"));
    }


    /**
     * @return the Send Message Button
     */
    public abstract Button createSendMsgButton();

    public TextField getSessionIdTextField() {
        return sessionIdTextField;
    }

    public TextField getUserNumberTextField() {
        return userNumberTextField;
    }

    public TextArea getMessageField() {
        return messageField;
    }

    public TextField getUrlField() {
        return urlField;
    }

    public TextField getAppIdField() {
        return appIdField;
    }

    public ComboBox getErrorCodeComboBox() {
        return errorCodeComboBox;
    }

    public ComboBox getUssdOpTypeComboBox() {
        return ussdOpTypeComboBox;
    }

    public TextField getPasswordField() {
        return passwordField;
    }

    /**
     * @return a Vertical Layout containing the contents inside USSD TabView
     */
    public abstract Component getTabLayout();
}