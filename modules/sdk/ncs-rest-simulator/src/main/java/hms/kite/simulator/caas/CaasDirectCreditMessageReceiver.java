/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.caas;

import hms.kite.simulator.MessageReceiver;
import hms.kite.simulator.webui.ui.tab.impl.TabViewSmsImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class CaasDirectCreditMessageReceiver implements MessageReceiver {

    private static Logger logger = LoggerFactory.getLogger(CaasDirectCreditMessageReceiver.class);
    public static List<CaasRequestMessage> receivedMessagesCas = new ArrayList<CaasRequestMessage>();

    @Override
    public Map<String, Object> onMessage(Map<String, Object> message) {
        Map<String, Object> response;

        String[] statusReq = getRequestedStatus();
        CaasRequestMessage caasRequestMessage = decodeMessage(message);
        receivedMessagesCas.add(caasRequestMessage);

        SimpleDateFormat timeStamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        response = CaasResponseBuilder.getDirectDebitResponse("externalTrxId", "internalTrxId", "123", "1252", caasRequestMessage.getAmount()+5, timeStamp.format(new Date()),
                statusReq[0], statusReq[1], "longDescription", "shortDescription");
        return response;
    }

    @Override
    public List getReceivedMessages() {
        return receivedMessagesCas;
    }

    private CaasRequestMessage decodeMessage(Map<String, Object> req) {
        CaasRequestMessage aoMessage = new CaasRequestMessage();
        aoMessage.setApplicationId(String.valueOf(req.get("applicationId")));
        aoMessage.setPassword(String.valueOf(req.get("password")));
        aoMessage.setExternalTrxId(String.valueOf(req.get("externalTrxId")));
        aoMessage.setSubscriberId(String.valueOf(req.get("subscriberId")));
        aoMessage.setPaymentInstrumentName(String.valueOf(req.get("paymentInstrumentName")));
        aoMessage.setAccountId(String.valueOf(req.get("accountId")));
        aoMessage.setAmount(String.valueOf(req.get("amount")));
        aoMessage.setCurrency(String.valueOf(req.get("currency")));
        aoMessage.setInvoiceNo(String.valueOf(req.get("invoiceNo")));
        aoMessage.setOrderNo(String.valueOf(req.get("orderNo")));
        aoMessage.setAllowPartialPayments(String.valueOf(req.get("allowPartialPayments")));
        aoMessage.setAllowOverPayments(String.valueOf(req.get("allowOverPayments")));
        aoMessage.setExtra(String.valueOf(req.get("extra")));
        return aoMessage;
    }

    private String[] getRequestedStatus() {
        return TabViewSmsImpl.getSmsErrorCodeRequest().split(":");
    }

}