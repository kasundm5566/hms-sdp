/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.caas;

import hms.kite.simulator.MessageReceiver;
import hms.kite.simulator.common.ChargingStatus;
import hms.kite.simulator.common.PaymentInstruments;
import hms.kite.simulator.webui.StaticPropertiesService;
import hms.kite.simulator.webui.ui.tab.impl.TabViewCasImpl;
import hms.kite.simulator.webui.ui.tab.impl.TabViewSmsImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CaasDirectDebitMessageReceiver implements MessageReceiver {

    private static Logger logger = LoggerFactory.getLogger(CaasDirectDebitMessageReceiver.class);
    public static List<CaasRequestMessage> receivedMessagesCas = new ArrayList<CaasRequestMessage>();
    public static Map<String ,String> descriptionForRequest = new HashMap<String, String>();
    public static Map<String ,String> internalTransactionId = new HashMap<String, String>();

    public static String PENDING_STATUS = "P1003";
    public static String SUCCESS_STATUS = "S1000";
    public static String ERROR_STATUS = "E1312";

    private String tillNo;

    @Override
    public Map<String, Object> onMessage(Map<String, Object> message) {
        Map<String, Object> response = new HashMap<String, Object>();
        boolean isAsync = true;
        CaasRequestMessage caasRequestMessage = decodeMessage(message);
        logger.info("Generated CaasRequestMessage [{}]", caasRequestMessage.toString());

        DecimalFormat twoDForm = new DecimalFormat("#.##");
        double amountToPay = Double.parseDouble(caasRequestMessage.getAmount());
        String amountDue = String.valueOf(twoDForm.format((amountToPay + amountToPay*(0.05))));

        String[] statusReq = getRequestedStatus();
        receivedMessagesCas.add(caasRequestMessage);

        SimpleDateFormat timeStamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        Random randomNumber = new Random();
        String internalTrx = String.valueOf(randomNumber.nextInt(1000));
        String externalTrx = caasRequestMessage.getExternalTrxId();
        String paymentInstrument = caasRequestMessage.getPaymentInstrumentName();
        String referenceId = String.valueOf(randomNumber.nextInt(100000000));
        String businesNo = String.valueOf(randomNumber.nextInt(1000));
        String shortDescription = null;
        String longDescription = null;

        if (paymentInstrument.equals(PaymentInstruments.EQUITY)) {
            shortDescription = new StringBuilder()
                    .append("Please access Equity Bank Easy Pay; enter following:")
                    .append("  Account no:").append(caasRequestMessage.getAccountId())
                    .append(", Business no:").append(businesNo)
                    .append(", Reference ID:").append(referenceId)
                    .append(", Amount:").append(caasRequestMessage.getCurrency())
                    .append("  ").append(amountDue).append(" .").toString();

            longDescription = new StringBuilder()
                    .append("Please access Equity Bank Easy Pay menu and enter following when prompted: ")
                    .append("  Account no:").append(caasRequestMessage.getAccountId())
                    .append(", Business no:").append(businesNo)
                    .append(", Reference ID:").append(referenceId)
                    .append(", Amount:").append(caasRequestMessage.getCurrency())
                    .append("  ").append(amountDue).append(", Please pay within 7 days.").toString();

        } else if (paymentInstrument.equals(PaymentInstruments.PAY_BILL)) {
            shortDescription = new StringBuilder()
                    .append("Please access Pay Bill in M-PESA menu; enter following:")
                    .append("  Business no: ").append(businesNo)
                    .append(", Account no: ").append(referenceId)
                    .append(", Amount: ").append(caasRequestMessage.getCurrency())
                    .append("  ").append(amountDue).append(" .").toString();

            longDescription = new StringBuilder()
                    .append("Please access Pay Bill in M-PESA menu and enter the following when prompted: ")
                    .append("  Business no: ").append(businesNo)
                    .append(", Account no: ").append(referenceId)
                    .append(", Amount: ").append(caasRequestMessage.getCurrency())
                    .append("  ").append(amountDue).append(", Please pay within 2 days.").toString();
        } else if (paymentInstrument.equals(PaymentInstruments.BUY_GOODS)) {
            businesNo = tillNo;
            amountDue = String.valueOf(twoDForm.format(amountToPay));
            shortDescription = new StringBuilder()
                    .append("Please access Buy Goods M-PESA menu; enter following:")
                    .append("  Till no: ").append(tillNo)
                    .append(", Amount: ").append(caasRequestMessage.getCurrency())
                    .append("  ").append(amountDue).append(" .").toString();

            longDescription = new StringBuilder()
                    .append("Please access Buy Goods M-PESA menu and enter the following when prompted:")
                    .append("  Till no: ").append(tillNo)
                    .append(", Amount: ").append(caasRequestMessage.getCurrency())
                    .append("  ").append(amountDue).append(", Please pay within 30 minutes.").toString();
        }

        descriptionForRequest.put(caasRequestMessage.getExternalTrxId(), shortDescription);
        internalTransactionId.put(caasRequestMessage.getExternalTrxId(), internalTrx);


        String chargingStatus = ChargingStatus.PENDING;

        if (TabViewCasImpl.getChargStatusComboBox() != null ) {
            chargingStatus = TabViewCasImpl.getChargStatusComboBox().getValue().toString();
        }

        String statusCode = chargingStatus.split(":")[0];

        if (PENDING_STATUS.equals(statusCode)) {
            response = CaasResponseBuilder.getDirectDebitResponse(
                    externalTrx,
                    internalTrx,
                    referenceId,
                    businesNo,
                    amountDue,
                    timeStamp.format(new Date()),
                    "P1003",
                    "Request successful. Payment notification pending from payment instrument",
                    longDescription,
                    shortDescription);
        }
//        else if (SUCCESS_STATUS.equals(statusCode)) {
//            response = CaasResponseBuilder.getDirectDebitResponse(externalTrx,
//                    internalTrx,
//                    referenceId,
//                    businesNo,
//                    amountDue,
//                    timeStamp.format(new Date()),
//                    "S1000",
//                    "Success",
//                    longDescription,
//                    shortDescription);
//
//        }
        else if (ERROR_STATUS.equals(statusCode)) {
            response = new HashMap<String, Object>();
            response.put("statusCode",statusCode);
            response.put("statusDetail","Invalid request");
        }

        return response;
    }


    public List<CaasRequestMessage> getReceivedMessages() {
        return receivedMessagesCas;
    }

    private CaasRequestMessage decodeMessage(Map<String, Object> req) {
        CaasRequestMessage aoMessage = new CaasRequestMessage();
        aoMessage.setApplicationId(String.valueOf(req.get("applicationId")));
        aoMessage.setPassword(String.valueOf(req.get("password")));
        aoMessage.setExternalTrxId(String.valueOf(req.get("externalTrxId")));
        aoMessage.setSubscriberId(String.valueOf(req.get("subscriberId")));
        aoMessage.setPaymentInstrumentName(String.valueOf(req.get("paymentInstrumentName")));
        aoMessage.setAccountId(String.valueOf(req.get("accountId")));
        aoMessage.setAmount(String.valueOf(req.get("amount")));
        aoMessage.setCurrency(String.valueOf(req.get("currency")));
        aoMessage.setInvoiceNo(String.valueOf(req.get("invoiceNo")));
        aoMessage.setOrderNo(String.valueOf(req.get("orderNo")));
        if (req.containsKey("allowPartialPayments")){
            aoMessage.setAllowPartialPayments(String.valueOf(req.get("allowPartialPayments")));
        } else {
            aoMessage.setAllowPartialPayments("true");
        }
        if (req.containsKey("allowOverPayments")) {
            aoMessage.setAllowOverPayments(String.valueOf(req.get("allowOverPayments")));
        } else {
            aoMessage.setAllowOverPayments("true");
        }
        aoMessage.setExtra(String.valueOf(req.get("extra")));

        Map<String, Object> extraDetails = (Map)req.get("extra");
        if (extraDetails != null) {
            tillNo = String.valueOf(extraDetails.get("tillNo"));
        }
        return aoMessage;
    }

    private String[] getRequestedStatus() {
        return TabViewSmsImpl.getSmsErrorCodeRequest().split(":");
    }

    public static Map<String, String> getDescriptionForRequest() {
        return descriptionForRequest;
    }

    public static Map<String, String> getInternalTransactionId() {
        return internalTransactionId;
    }

    public String getTillNo() {
        return tillNo;
    }

    public void setTillNo(String tillNo) {
        this.tillNo = tillNo;
    }
}