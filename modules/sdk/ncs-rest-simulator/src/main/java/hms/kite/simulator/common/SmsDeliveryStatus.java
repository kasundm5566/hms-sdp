/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.common;

public enum SmsDeliveryStatus {

    DELIVERED("Delivered"),
    EXPIRED("Expired"),
    DELETED("Deleted"),
    UNDELIVERABLE("Undeliverable"),
    ACCEPTED("Accepted"),
    UNKNOWN("Unknown"),
    REJECTED("Rejected");

    private String status;

    private SmsDeliveryStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}