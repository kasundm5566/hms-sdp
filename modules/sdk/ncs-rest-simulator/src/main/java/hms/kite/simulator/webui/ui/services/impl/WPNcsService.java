/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.webui.ui.services.impl;

import com.vaadin.ui.Label;
import hms.kite.simulator.MessageReceiver;
import hms.kite.simulator.MessageSender;
import hms.kite.simulator.RequestMessage;
import hms.kite.simulator.sms.SmsRequestMessage;
import hms.kite.simulator.webui.ui.services.NcsService;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class WPNcsService implements NcsService {

    private MessageSender messageSender;
    private MessageReceiver messageReceiver;

    @Override
    public Map<String, Object> sendMessage(RequestMessage requestMessage, String url) throws IOException {
        return messageSender.sendMessage(requestMessage, url);
    }

    @Override
    public List receivedMessages() {
        return messageReceiver.getReceivedMessages();
    }

    @Override
    public void updatePhoneView(Label phoneNum, Label message, Object val) {
        SmsRequestMessage smsAoRequestMessage = (SmsRequestMessage) val;
        phoneNum.setValue(smsAoRequestMessage.getSourceAddress());
        message.setValue(smsAoRequestMessage.getMessage());
    }

    public void setMessageSender(MessageSender messageSender) {
        this.messageSender = messageSender;
    }

    public void setMessageReceiver(MessageReceiver messageReceiver) {
        this.messageReceiver = messageReceiver;
    }
}