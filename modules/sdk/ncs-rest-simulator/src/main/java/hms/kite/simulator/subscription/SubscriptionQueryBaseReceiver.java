/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.subscription;

import hms.kite.simulator.MessageReceiver;
import hms.kite.simulator.webui.ui.tab.impl.TabViewSmsImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubscriptionQueryBaseReceiver implements MessageReceiver {

    private static Logger logger = LoggerFactory.getLogger(SubscriptionQueryBaseReceiver.class);
    public static List<SubscriptionQueryBaseRequest> receivedSubQueryBaseReq = new ArrayList<SubscriptionQueryBaseRequest>();
    private static Map<String, Object> response;
    
    @Override
    public Map<String, Object> onMessage(Map<String, Object> message) {
        
        logger.info("Subscription Query Base request received, [{}]", message );

        SubscriptionQueryBaseRequest subQueryBaseReq = decodeMessage(message);
        String[] statusReq = getRequestedStatus();

        receivedSubQueryBaseReq.add(subQueryBaseReq);
        response = responseGenerate(statusReq[0], statusReq[1]);
        logger.info("Sent response back for subscription query request [{}]", response);
        return response;
    }

    private Map<String, Object> responseGenerate(String statusCode, String statusDetail) {

        Map<String, Object> res = new HashMap<String, Object>();
        res.put("version", "0.1");
        res.put("baseSize", "Some Base Size");
        res.put("statusCode", statusCode);
        res.put("statusDetail", statusDetail);

        return res;
    }

    private SubscriptionQueryBaseRequest decodeMessage(Map<String, Object> message) {
        SubscriptionQueryBaseRequest subQueryBaseReq = new SubscriptionQueryBaseRequest();

        subQueryBaseReq.setApplicationId(String.valueOf(message.get("applicationId")));
        subQueryBaseReq.setPassword(String.valueOf(message.get("password")));

        return subQueryBaseReq;
    }

    @Override
    public List getReceivedMessages() {
        return receivedSubQueryBaseReq;
    }

    private String[] getRequestedStatus() {
        return TabViewSmsImpl.getSmsErrorCodeRequest().split(":");
    }
    
    public static void clearSubQueryBaseMessageList() {
        receivedSubQueryBaseReq.clear();
    }
    
    public static String getBaseSize() {
        return (String) response.get("baseSize");
    }
}