/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.simulator.rest;

import hms.kite.simulator.MessageReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;

@Produces({"application/json"})
@Consumes({"application/json"})
@Path("/sms")
public class SmsNcsRestService {
	private static final Logger LOGGER = LoggerFactory.getLogger(SmsNcsRestService.class);
	private MessageReceiver smsAoMessageReceiver;

    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    @Path("/send")
    public Map<String, Object> sendService(Map<String, Object> msg) {
        Map<String, Object> response = smsAoMessageReceiver.onMessage(msg);
        LOGGER.info("Received SMS message to send to customer[{}], sent response back to application[{}]", msg, response);
    	return response;
    }

	public void setSmsAoMessageReceiver(MessageReceiver smsAoMessageReceiver) {
		this.smsAoMessageReceiver = smsAoMessageReceiver;
	}

}
