/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.ussd;

import hms.kite.simulator.MessageReceiver;
import hms.kite.simulator.webui.ui.tab.impl.TabViewUssdImpl;

import java.text.SimpleDateFormat;
import java.util.*;

public class UssdMessageReceiver implements MessageReceiver {

    private static final List<UssdRequestMessage> receivedUssd = new ArrayList<UssdRequestMessage>();

    @Override
    public Map<String, Object> onMessage(Map<String, Object> message) {
        UssdRequestMessage ussdRequestMessage = decodeMessage(message);
        receivedUssd.add(ussdRequestMessage);
        String[] stausReq = getStatusRequest();

        SimpleDateFormat timeStamp = new SimpleDateFormat("yyMMddHHmm");
        return generateResponse("0.1", String.valueOf(System.currentTimeMillis()),
                timeStamp.format(new Date()), stausReq[0], stausReq[1]);
    }

    private Map<String, Object> generateResponse(String version, String requestId, String timeStamp, String statusCode, String statusDetil) {
        Map<String, Object> msg = new HashMap<String ,Object>();
        msg.put("version", version);
        msg.put("requestId", requestId);
        msg.put("timeStamp", timeStamp);
        msg.put("statusCode", statusCode);
        msg.put("statusDetail", statusDetil);
        return msg;
    }

    private UssdRequestMessage decodeMessage(Map<String, Object> req) {
        UssdRequestMessage aoMessage = new UssdRequestMessage();
        aoMessage.setApplicationId(String.valueOf(req.get("applicationId")));
        aoMessage.setPassword(String.valueOf(req.get("password")));
        aoMessage.setSessionId(String.valueOf(req.get("sessionId")));
        aoMessage.setUssdOperation(String.valueOf(req.get("ussdOperation")));
        aoMessage.setDestinationAddress(String.valueOf(req.get("destinationAddress")));
        aoMessage.setMessage(String.valueOf(req.get("message")));
        return aoMessage;
    }

    /**
     * @return the sms list.
     */
    public List<UssdRequestMessage> getReceivedMessages() {
        return receivedUssd;
    }

    /**
     * clears the received sms object.
     */
    public static void ClearMessageList() {
        receivedUssd.clear();
    }

    public String[] getStatusRequest() {
        return TabViewUssdImpl.getUssdErrorCodeRequest().split(":");
    }
}