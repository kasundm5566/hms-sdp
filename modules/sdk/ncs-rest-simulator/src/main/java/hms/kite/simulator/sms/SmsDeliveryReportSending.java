/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.sms;

import hms.kite.simulator.SmsDeliveryReportSender;
import hms.kite.simulator.rest.AtDeliveryReportSender;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class SmsDeliveryReportSending implements SmsDeliveryReportSender {

    private AtDeliveryReportSender deReportSender;

    @Override
    public Map<String, Object> sendMessage(SmsDeliveryReportRequest deReportRequest, String url) {
        Map<String, Object> deReport = new HashMap<String, Object>();

        SimpleDateFormat timeStamp = new SimpleDateFormat("yyMMddHHmm");
        deReport.put("destinationAddress", deReportRequest.getDestinationAddress());
        deReport.put("requestId", String.valueOf(System.currentTimeMillis()));
        deReport.put("timeStamp", String.valueOf(timeStamp.format(new Date())));
        deReport.put("deliverStatus", deReportRequest.getDeliverStatus());

        return deReportSender.sendMessage(deReport, url);
    }

    public void setDeReportSender(AtDeliveryReportSender deReportSender) {
        this.deReportSender = deReportSender;
    }
}