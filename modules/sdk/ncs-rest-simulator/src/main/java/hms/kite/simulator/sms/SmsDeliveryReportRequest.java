/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.sms;

public class SmsDeliveryReportRequest {

    private String destinationAddress;
    private String requestId;
    private String timeStamp;
    private String deliverStatus;

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getDeliverStatus() {
        return deliverStatus;
    }

    public void setDeliverStatus(String deliverStatus) {
        this.deliverStatus = deliverStatus;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("SmsDeliveryReportRequest{")
                .append("deliverStatus='").append(deliverStatus).append('\'')
                .append(", destinationAddress='").append(destinationAddress).append('\'')
                .append(", requestId='").append(requestId).append('\'')
                .append(", timeStamp='").append(timeStamp).append('\'')
                .append('}').toString();
    }
}