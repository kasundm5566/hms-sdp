/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.subscription;

import hms.kite.simulator.MessageReceiver;
import hms.kite.simulator.common.SubscriptionStatus;
import hms.kite.simulator.webui.ui.tab.impl.TabViewSmsImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubscriptionMessageReceiver implements MessageReceiver {

    private static Logger logger = LoggerFactory.getLogger(SubscriptionMessageReceiver.class);
    public static List receivedMessages = new ArrayList();
    public static final List<Map<String, Object>> registeredMSISDN = new ArrayList<Map<String, Object>>();


    @Override
    public Map<String, Object> onMessage(Map<String, Object> message) {
        logger.info("Subscription message received {}", message);
        Map<String, Object> response;

        receivedMessages.add(message);

        response = createResponse(message);
        logger.info("Sent respond back to application : {}", response);
        return response;
    }

    @Override
    public List getReceivedMessages() {
        return receivedMessages;
    }

    private Map<String, Object> createResponse(Map<String, Object> msg) {

        boolean isUserAvailable;
        Map<String, Object> data = new HashMap<String, Object>();
        String msisdn = (String) msg.get("subscriberId");

        data.put("status", SubscriptionStatus.REGISTERED);
        data.put("subscriberId", msisdn);

        isUserAvailable = isMsisdnAlreadyAvailable(msisdn);
        return generateResponse(msg, isUserAvailable, data, msisdn);
    }

    private boolean isMsisdnAlreadyAvailable(String msisdn) {
        boolean isAvailable = false;
        for (Map<String, Object> map : registeredMSISDN) {
            if (msisdn.equals(map.get("subscriberId"))) {
                isAvailable = true;
            }
        }
        return isAvailable;
    }

    private Map<String, Object> generateResponse(Map<String, Object> msg, boolean userAvailable, Map<String, Object> data, String msisdn) {
        Map<String, Object> response;
        String action = msg.get("action").toString();
        String[] statusReq = getRequestedStatus();
        String requestId = String.valueOf(System.currentTimeMillis());

        if (action.equals("1") && !userAvailable && statusReq[0].equals("S1000")) {
            registeredMSISDN.add(data);
            response = generate("0.1,", requestId ,statusReq[0], statusReq[1]);
        } else if (action.equals("0") && userAvailable && statusReq[0].equals("S1000")) {
            registeredMSISDN.remove(data);
            response = generate("0.1,", requestId ,statusReq[0], statusReq[1]);
        } else if (action.equals("0") && !userAvailable) {
            response = generate("0.1,", requestId ,"E1356", "User not registered");
        } else if (action.equals("1") && userAvailable) {
            response = generate("0.1,", requestId ,"E1351", "User already registered");
        } else {
            response = generate("0.1,", requestId , statusReq[0], statusReq[1]);
        }
        return response;
    }

    private Map<String, Object> generate(String version, String reqId, String statusCode, String statusDescription) {
        Map<String, Object> msg = new HashMap<String, Object>();
        msg.put("version", version);
        msg.put("requestId", reqId);
        msg.put("statusCode", statusCode);
        msg.put("statusDetail", statusDescription);

        return msg;
    }

    private String[] getRequestedStatus() {
        return TabViewSmsImpl.getSmsErrorCodeRequest().split(":");
    }
}