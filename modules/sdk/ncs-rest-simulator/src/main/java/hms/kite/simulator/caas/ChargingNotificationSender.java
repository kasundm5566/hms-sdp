/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.caas;

import hms.kite.simulator.rest.AtRequestSender;

import java.util.HashMap;
import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ChargingNotificationSender {

    private AtRequestSender atRequestSender;

    public Map<String, Object> sendMessage(ChargingNotificationMessage notificationMessage, String url) {
        Map<String, Object> message = new HashMap<String, Object>();

        message.put("externalTrxId",notificationMessage.getExternalTrxId());
        message.put("internalTrxId", notificationMessage.getInternalTrxId());
        message.put("referenceId", notificationMessage.getReferenceId());
        message.put("paidAmount", notificationMessage.getPaidAmount());
        message.put("totalAmount", notificationMessage.getTotalAmount());
        message.put("balanceDue", notificationMessage.getBalanceDue());
        message.put("currency", notificationMessage.getCurrency());
        message.put("statusCode", notificationMessage.getStatusCode());
        message.put("statusDetail", notificationMessage.getStatusDetail());
        message.put("timeStamp", notificationMessage.getTimeStamp());
        return atRequestSender.sendMessage(message, url);
    }

    public void setNotificationSender(AtRequestSender atRequestSender) {
        this.atRequestSender = atRequestSender;
    }
}