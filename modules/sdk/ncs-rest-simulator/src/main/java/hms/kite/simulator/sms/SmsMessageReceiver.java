/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.sms;

import hms.kite.simulator.SmsDeliveryReportSender;
import hms.kite.simulator.MessageReceiver;
import hms.kite.simulator.common.SmsDeliveryStatus;
import hms.kite.simulator.rest.ResponseBuilder;
import hms.kite.simulator.webui.ui.tab.impl.TabViewSmsImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class SmsMessageReceiver implements MessageReceiver {

    private static final Logger logger = LoggerFactory.getLogger(SmsMessageReceiver.class);

    private static final List<SmsRequestMessage> receivedSms = new ArrayList<SmsRequestMessage>();
    private ScheduledThreadPoolExecutor deliveryReportExecutor = new ScheduledThreadPoolExecutor(10);
    private Map<String, String> errorCodeMap;
    private SmsDeliveryReportSender deliveryReportSender;

    @Override
    public Map<String, Object> onMessage(Map<String, Object> message) {

        String[] statusReq = getRequestedStatus();
        List destinationRes = createDestinationResponses(message, statusReq);
        SmsRequestMessage smsRequestMessage = decodeMessage(message);
        receivedSms.add(smsRequestMessage);
        String[] addressLst = smsRequestMessage.getSourceAddress().split(",");
        String address;
        if (addressLst.length == 1) {
            address = addressLst[0].replaceAll("tel:", "");
            smsRequestMessage.setSourceAddress(address);
            if (errorCodeMap.containsKey(address)) {
                String[] errorCodeArray = errorCodeMap.get(address).split(":");
                sendDeliveryReport(message, errorCodeArray[0], smsRequestMessage);
                return ResponseBuilder.generate("0.1", String.valueOf(System.currentTimeMillis()), errorCodeArray[0], errorCodeArray[1], destinationRes);
            } else {
                sendDeliveryReport(message, statusReq[0], smsRequestMessage);
                return ResponseBuilder.generate("0.1", String.valueOf(System.currentTimeMillis()), statusReq[0], statusReq[1], destinationRes);
            }
        } else {
            sendDeliveryReport(message, statusReq[0], smsRequestMessage);
            return ResponseBuilder.generate("0.1", String.valueOf(System.currentTimeMillis()), statusReq[0], statusReq[1], destinationRes);
        }
    }

    private void sendDeliveryReport(Map<String, Object> message, String status, SmsRequestMessage smsRequestMessage) {
        if (smsRequestMessage.getDeliveryStatusRequest().equals("1")) {
            deliveryReportSender(message, status);
        }
    }

    private void deliveryReportSender(Map<String, Object> message, String status) {
        String addresses = String.valueOf(message.get("destinationAddresses"));
        addresses = addresses.replace("[", "").replace("]", "").replace(" ","");
        String[] addressArray = addresses.split(",");
        String deliveryStatus;
        if (status.equals("S1000")) {
            deliveryStatus = SmsDeliveryStatus.DELIVERED.getStatus();
        } else {
            deliveryStatus = SmsDeliveryStatus.UNDELIVERABLE.getStatus();
        }

        for (String address : addressArray) {
             setAndSendingDeliveryReport(address, deliveryStatus);
        }
    }
    
    private String[] getRequestedStatus() {
        return TabViewSmsImpl.getSmsErrorCodeRequest().split(":");
    }

    private void setAndSendingDeliveryReport(final String address, final String deliveryStatus) {
        deliveryReportExecutor.schedule(new Runnable() {
            @Override
            public void run() {
                try {
                    SmsDeliveryReportRequest deliveryReport = new SmsDeliveryReportRequest();
                    deliveryReport.setDestinationAddress(address);
                    deliveryReport.setDeliverStatus(deliveryStatus);

                    deliveryReportSender.sendMessage(deliveryReport, TabViewSmsImpl.deliveryReportUrl());
                } catch (Exception e) {
                    logger.error("Unexpected exception occurred while trying to send delivery report", e);
                }
            }
        }, 5, TimeUnit.SECONDS);
    }

    private SmsRequestMessage decodeMessage(Map<String, Object> req) {
        SmsRequestMessage aoMessage = new SmsRequestMessage();
        aoMessage.setApplicationId(String.valueOf(req.get("applicationId")));
        aoMessage.setPassword(String.valueOf(req.get("password")));
        String address = String.valueOf(req.get("destinationAddresses"));
        address = address.replace("[", "").replace("]", "");
        aoMessage.setDestinationAddresses(address);
        aoMessage.setMessage(String.valueOf(req.get("message")));
        aoMessage.setSourceAddress(String.valueOf(req.get("sourceAddress")));
        aoMessage.setDeliveryStatusRequest(String.valueOf(req.get("deliveryStatusRequest")));
        return aoMessage;
    }

    private List createDestinationResponses(Map<String, Object> message, String[] status) {

        String addressList = String.valueOf(message.get("destinationAddresses"));
        String[] addressArray = addressList.replace("[", "").replace("]", "").split(",");

        List<Map<String, Object>> resList = new ArrayList<Map<String, Object>>();
        for (String address : addressArray) {
            Map<String, Object> entries = new HashMap<String, Object>();

            SimpleDateFormat timeStamp = new SimpleDateFormat("yyyyMMddHHmmss");

            entries.put("address", address);
            entries.put("timeStamp", timeStamp.format(new Date()));
            entries.put("requestId", String.valueOf(System.currentTimeMillis()));
            entries.put("statusCode", status[0]);
            entries.put("statusDetail", status[1]);

            resList.add(entries);

        }
        return resList;
    }

    /**
     * @return the sms list.
     */
    public List<SmsRequestMessage> getReceivedMessages() {
        return receivedSms;
    }

    /**
     * clears the received sms object.
     */
    public static void ClearMessageList() {
        receivedSms.clear();
    }

    public void setErrorCodeMap(Map<String, String> errorCodeMap) {
        this.errorCodeMap = errorCodeMap;
    }

    public void setDeliveryReportSender(SmsDeliveryReportSender deliveryReportSender) {
        this.deliveryReportSender = deliveryReportSender;
    }
}
