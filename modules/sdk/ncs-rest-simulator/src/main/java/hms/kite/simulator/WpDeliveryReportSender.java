package hms.kite.simulator;

import hms.kite.simulator.WP.WpDeliveryReportRequest;

import java.util.Map;

public interface WpDeliveryReportSender {

    Map<String, Object> sendMessage(WpDeliveryReportRequest wpDeliveryReportRequest, String url);
}
