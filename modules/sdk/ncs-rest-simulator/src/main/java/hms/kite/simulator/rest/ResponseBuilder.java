/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.simulator.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResponseBuilder {

	public static Map<String, Object> generate(String version, String reqId, String  statusCode,
                                               String statusDescription, List destinationResponses) {
		Map<String, Object> msg = new HashMap<String, Object>();

        msg.put("version", version );
        msg.put("requestId", reqId);
		msg.put("statusCode", statusCode);
		msg.put("statusDetail", statusDescription);
		msg.put("destinationResponses", destinationResponses);

        return msg;
	}

    public static Map<String, Object> generate(String statusCode, String statusDescription) {
        Map<String, Object> msg = new HashMap<String, Object>();

        msg.put("statusCode", statusCode);
        msg.put("statusDetail", statusDescription);

        return msg;
    }
}
