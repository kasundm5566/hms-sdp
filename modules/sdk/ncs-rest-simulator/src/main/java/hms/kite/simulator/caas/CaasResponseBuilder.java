/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.caas;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CaasResponseBuilder {

    public static Map<String ,Object> getPaymentInstrumentResponse(List paymentInstrumentList,
                                                             String statusCode,
                                                             String statusDetail) {
        Map<String, Object> msg = new HashMap<String, Object>();

        msg.put("paymentInstrumentList", paymentInstrumentList);
        msg.put("statusCode", statusCode);
        msg.put("statusDetail", statusDetail);

        return  msg;
    }

    public static Map<String ,Object> getDirectDebitResponse(String externalTrxId,
                                                             String internalTrxId,
                                                             String referenceId,
                                                             String businessNumber,
                                                             String amountDue,
                                                             String timeStamp,
                                                             String statusCode,
                                                             String statusDetail,
                                                             String longDescription,
                                                             String shortDescription) {
        Map<String, Object> msg = new HashMap<String, Object>();
        msg.put("externalTrxId",externalTrxId);
        msg.put("internalTrxId",internalTrxId);
        msg.put("referenceId",referenceId);
        msg.put("businessNumber",businessNumber);
        msg.put("amountDue",amountDue);
        msg.put("timeStamp",timeStamp);
        msg.put("statusCode",statusCode);
        msg.put("statusDetail",statusDetail);
        msg.put("longDescription",longDescription);
        msg.put("shortDescription",shortDescription);
        return msg;
    }
}