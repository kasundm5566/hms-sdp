/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.common;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class PaymentInstruments {

    public static final String EQUITY = "Equity";
    public static final String PAY_BILL = "M-Pesa-Paybill";
    public static final String BUY_GOODS = "M-Pesa-Buygoods";
    public static final String OPERATOR = "Operator";
}