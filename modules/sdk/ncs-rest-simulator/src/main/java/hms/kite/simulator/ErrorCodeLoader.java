/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator;

import com.vaadin.ui.ComboBox;
import hms.kite.simulator.webui.StaticPropertiesService;

import java.util.Map;

public class ErrorCodeLoader {

    private String[] errorCodesSms = StaticPropertiesService.getInstant().getMessage("sms.error.codes").replace(" ","").split(",");
    private String[] errorCodesUssd = StaticPropertiesService.getInstant().getMessage("ussd.error.codes").replace(" ","").split(",");

    Map<String, String> errorCode = (Map<String, String>) ServiceLocator.getBean("errorCodeMap");
//    Object [] errorDescription = errorCode.keySet().toArray();

    public void setErrorDataForSmsList(ComboBox data) {
        for (String value : errorCodesSms ) {
            String errorValue = errorCode.get(value);
            data.addItem(errorValue);
        }
    }

    public void setErrorDataForUssdList(ComboBox data) {
        for (String value : errorCodesUssd ) {
            String errorValue = errorCode.get(value);
            data.addItem(errorValue);
        }
    }
}