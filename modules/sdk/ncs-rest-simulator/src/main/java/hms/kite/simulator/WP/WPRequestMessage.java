/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.WP;

public class WPRequestMessage {

    private String applicationId;
    private String password;
    private String version;
    private String wapPushType;
    private String wapUrl;
    private String destinationAddresses;
    private String title;
    private String sourceAddress;
    private String deliveryStatusRequest;
    private String chargingAmount;

    public String getTitle() {
        return title;
    }

    /**
     * Set the message of mt request.
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public String getApplicationId() {
        return applicationId;
    }

    /**
     * Set the application id
     *
     * @param applicationId
     */
    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getPassword() {
        return password;
    }

    /**
     * Set the password
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    public String getDestinationAddresses() {
        return destinationAddresses;
    }

    /**
     * Set the recipient address list.
     *
     * @param destinationAddresses
     */
    public void setDestinationAddresses(String destinationAddresses) {
        this.destinationAddresses = destinationAddresses;
    }

    public String getChargingAmount() {
        return chargingAmount;
    }

    /**
     * Set the charging amount in sdp system currency. This should be a numeric value.
     *
     * @param chargingAmount
     */
    public void setChargingAmount(String chargingAmount) {
        this.chargingAmount = chargingAmount;
    }

    public String getDeliveryStatusRequest() {
        return deliveryStatusRequest;
    }

    /**
     * Specify status Reports required or not. Values should be true of false.
     *
     * @param deliveryStatusRequest
     */
    public void setDeliveryStatusRequest(String deliveryStatusRequest) {
        this.deliveryStatusRequest = deliveryStatusRequest;
    }

    public String getSourceAddress() {
        return sourceAddress;
    }

    /**
     * Set Source address.
     *
     * @param sourceAddress
     */
    public void setSourceAddress(String sourceAddress) {
        this.sourceAddress = sourceAddress;
    }

    public String getVersion() {
        return version;
    }

    /**
     * Set message version.
     *
     * @return
     */
    public void setVersion(String version) {
        this.version = version;
    }

    public String getWapUrl() {
        return wapUrl;
    }

    public void setWapUrl(String wapUrl) {
        this.wapUrl = wapUrl;
    }

    public String getWapPushType() {
        return wapPushType;
    }

    public void setWapPushType(String wapPushType) {
        this.wapPushType = wapPushType;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("MtWpReq");
        sb.append("{message='").append(title).append('\'');
        sb.append(", wapurl='").append(wapUrl).append('\'');
        sb.append(", applicationId='").append(applicationId).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", address=").append(destinationAddresses);
        sb.append(", chargingAmount='").append(chargingAmount).append('\'');
        sb.append(", deliveryStatusRequest='").append(deliveryStatusRequest).append('\'');
        sb.append(", sourceAddress='").append(sourceAddress).append('\'');
        sb.append(", version='").append(version).append('\'');
        sb.append('}');
        return sb.toString();
    }
}