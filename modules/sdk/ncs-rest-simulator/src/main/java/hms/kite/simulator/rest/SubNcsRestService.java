/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.rest;

import hms.kite.simulator.MessageReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;

@Produces({"application/json"})
@Consumes({"application/json"})
@Path("/subscription")
public class SubNcsRestService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SubNcsRestService.class);
    private MessageReceiver subscriptionMessageReceiver;
    private MessageReceiver subReqStatusMessageReceiver;
    private MessageReceiver subQueryBaseReqMessageReceiver;

    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    @Path("/send")
    public Map<String, Object> sendService(Map<String, Object> msg) {
        return subscriptionMessageReceiver.onMessage(msg);
    }

    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    @Path("/getStatus")
    public Map<String, Object> getStatus(Map<String, Object> msg) {
        return subReqStatusMessageReceiver.onMessage(msg);
    }

    @POST
    @Path("/query-base")
    public Map<String, Object> getQueryBase(Map<String, Object> msg) {
        return subQueryBaseReqMessageReceiver.onMessage(msg);
    }

    public void setSubscriptionMessageReceiver(MessageReceiver subscriptionMessageReceiver) {
        this.subscriptionMessageReceiver = subscriptionMessageReceiver;
    }

    public void setSubReqStatusMessageReceiver(MessageReceiver subReqStatusMessageReceiver) {
        this.subReqStatusMessageReceiver = subReqStatusMessageReceiver;
    }

    public void setSubQueryBaseReqMessageReceiver(MessageReceiver subQueryBaseReqMessageReceiver) {
        this.subQueryBaseReqMessageReceiver = subQueryBaseReqMessageReceiver;
    }
}