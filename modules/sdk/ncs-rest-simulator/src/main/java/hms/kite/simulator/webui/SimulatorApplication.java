/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.webui;

import hms.kite.simulator.webui.ui.MainUI;

import com.vaadin.Application;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.Window;

public class SimulatorApplication extends Application {

    public void init() {

        Window mainWindow = new Window(StaticPropertiesService.getInstant().getMessage("MainUI.title"));
        setTheme("simulator");
        final MainUI mainUI = new MainUI();
        mainWindow.setContent((ComponentContainer) mainUI.getRootLayout());
        setMainWindow(mainWindow);

    }
}
