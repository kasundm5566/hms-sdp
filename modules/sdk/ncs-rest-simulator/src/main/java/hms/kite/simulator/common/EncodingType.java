/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.common;

public enum EncodingType {

    TEXT("0"),
    FLASH("64"),
    BINARY("245");

    private String code;

    private EncodingType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
