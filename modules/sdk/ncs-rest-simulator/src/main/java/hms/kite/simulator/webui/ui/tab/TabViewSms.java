/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package hms.kite.simulator.webui.ui.tab;

import com.vaadin.data.Property;
import com.vaadin.ui.*;
import hms.kite.simulator.ErrorCodeLoader;
import hms.kite.simulator.common.EncodingType;
import hms.kite.simulator.webui.FeaturePropertiesService;
import hms.kite.simulator.webui.StaticPropertiesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.Properties;

public abstract class TabViewSms{

    private static Logger logger = LoggerFactory.getLogger(TabViewSms.class);
    private static Properties defaultValuesProperties;

    private TextField userNumberTextField;
    private TextField toNumberTextField;
    private TextArea messageField;
    private TextField binaryHeaderTextField;

    //url & appID fields added
    private TextField urlTextField;
    private TextField appIdTextField;
    private TextField passwordTextField;
    private CheckBox deReportCheckBox;
    private TextField deReportUrlTextFiled;

    private ComboBox errorCodeComboBox;

    private ComboBox encodingTyComboBox;

    private Layout binaryHeaderLayout;
    private HorizontalLayout binaryHeaderLayoutContent;
    private Layout deReportLayout;
    private HorizontalLayout deReportLayoutContent;

    public TabViewSms() {
        init();
    }

    public void init() {
        userNumberTextField = new TextField();
        userNumberTextField.setWidth("160px");
        toNumberTextField = new TextField();
        toNumberTextField.setWidth("160px");
        messageField = new TextArea();
        messageField.setWidth("160px");
        messageField.setHeight("60px");
        urlTextField = new TextField();
        urlTextField.setWidth("160px");
        appIdTextField = new TextField();
        appIdTextField.setWidth("160px");
        passwordTextField = new TextField();
        passwordTextField.setWidth("160px");
        deReportCheckBox = new CheckBox(StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.deliveryReportRequired"));
        deReportUrlTextFiled = new TextField();
        deReportUrlTextFiled.setWidth("160px");

        binaryHeaderTextField = new TextField();
        binaryHeaderTextField.setWidth("160px");
    }

    private Component createErrorCodeField() {
        HorizontalLayout errorCodeLayout = new HorizontalLayout();
        errorCodeLayout.setSpacing(true);
        Label errorCodeLbl = new Label(StaticPropertiesService.getInstant().getMessage("Tab.InputPanel.errorCode"));
        errorCodeComboBox = new ComboBox();
        errorCodeLbl.setWidth("120px");
        errorCodeComboBox.setWidth("220px");

        setSelectData(errorCodeComboBox);
        errorCodeComboBox.setNullSelectionAllowed(false);
        errorCodeComboBox.setValue("S1000:Success");

        errorCodeLayout.addComponent(errorCodeLbl);
        errorCodeLayout.addComponent(errorCodeComboBox);
        return errorCodeLayout;
    }


    /**
     * @return the Panel containing input text fields
     */
    public Component createInputPanel() {

        Panel inputPanel = new Panel(
                StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.Title"));
        inputPanel.setWidth("295px");

        Label applicationDataHeader = new Label(
                StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.ApplicationDataHeader"));
        applicationDataHeader.setStyleName("input-panel-section-header");
        inputPanel.addComponent(applicationDataHeader);

        inputPanel.addComponent(createPanelLayout(
                StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.URL"), urlTextField, "http://localhost:5555/mo-receiver"));
        inputPanel.addComponent(createPanelLayout(
                StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.AppID"), appIdTextField, "APP_000001"));
        inputPanel.addComponent(createPanelLayout(
                StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.password"), passwordTextField, "password"));


        Label messageDataHeader = new Label(
                StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.MessageDataHeader"));
        messageDataHeader.setStyleName("input-panel-section-header");
        inputPanel.addComponent(messageDataHeader);

        inputPanel.addComponent(createPanelLayout(
                StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.myNumber"), userNumberTextField, FeaturePropertiesService.getInstance().getMessage("SMSTab.InputPanel.userNumber.defaultValue")));
        inputPanel.addComponent(createPanelLayout(
                StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.toNumber"), toNumberTextField, FeaturePropertiesService.getInstance().getMessage("SMSTab.InputPanel.toNumber.defaultValue")));

        inputPanel.addComponent(createEncodingTypeLayout());
        inputPanel.addComponent(createBinaryHeaderLayout());

        inputPanel.addComponent(createPanelLayout(
                StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.message"), messageField, "Test Message"));

        Label additionalDataLbl = new Label(StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.MessageAdditionalRequests"));
        additionalDataLbl.setStyleName("input-panel-section-header");
        inputPanel.addComponent(additionalDataLbl);

        inputPanel.addComponent(createDeliveryReportUrlCheckBox());
        inputPanel.addComponent(createDeliveryReportLayout());

        inputPanel.addComponent(createErrorCodeField());

        createDeliveryReportTextFieldContent();
        createBinaryHeaderContent();
        setDescriptionsForFields();
        Button sendButton = createSendMsgButton();
        inputPanel.addComponent(sendButton);
        final AbstractOrderedLayout content = (AbstractOrderedLayout) inputPanel.getContent();
        content.setSpacing(true);
        content.setComponentAlignment(sendButton, Alignment.BOTTOM_RIGHT);

        return inputPanel;
    }
    
    private void setSelectData(ComboBox data) {
        ErrorCodeLoader errorCodeLoader = new ErrorCodeLoader();
        errorCodeLoader.setErrorDataForSmsList(data);
    }

    private Component createBinaryHeaderLayout() {
        binaryHeaderLayout = new HorizontalLayout();
        return binaryHeaderLayout;
    }
    
    private Component createDeliveryReportLayout() {
        deReportLayout= new HorizontalLayout();
        return deReportLayout;
    }

    private Component createDeliveryReportTextFieldContent() {
        deReportLayoutContent = new HorizontalLayout();
        deReportLayoutContent.setSpacing(true);
        Label deReportLbl = new Label(StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.deliveryReport"));
        deReportUrlTextFiled.setValue(StaticPropertiesService.getInstant().getMessage("SMS.Delivery.Report.Url"));
        deReportLbl.setWidth("120px");
        deReportUrlTextFiled.setWidth("220px");
        deReportLayoutContent.addComponent(deReportLbl);
        deReportLayoutContent.addComponent(deReportUrlTextFiled);
        return deReportLayoutContent;
    }

    private Component createDeliveryReportUrlCheckBox() {
        final HorizontalLayout deReportLayout = new HorizontalLayout();
        deReportLayout.setSpacing(true);
        Label deReportLbl = new Label();
        deReportLbl.setWidth("115px");
        deReportLayout.addComponent(deReportLbl);
        deReportLayout.addComponent(deReportCheckBox);
        deReportCheckBox.setImmediate(true);
        deReportCheckBox.addListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                try {
                    AddDeReportTxtField(valueChangeEvent);
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while selecting delivery report required", e);
                }
            }
        });
        return deReportLayout;
    }

    private void AddDeReportTxtField (Property.ValueChangeEvent valueChangeEvent) {
        if ((Boolean) valueChangeEvent.getProperty().getValue()) {
            deReportLayout.addComponent(deReportLayoutContent);
        } else {
            deReportLayout.removeComponent(deReportLayoutContent);
        }
    }

    private Component createEncodingTypeLayout() {
        HorizontalLayout encodingTypeLayout = new HorizontalLayout();
        encodingTypeLayout.setSpacing(true);
        Label encodingLabel = new Label(StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.encoding.type"));
        encodingTyComboBox = new ComboBox();
        encodingTyComboBox.addItem(EncodingType.TEXT);
        encodingTyComboBox.addItem(EncodingType.BINARY);

        encodingTyComboBox.setItemCaption(EncodingType.TEXT, StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.encoding.type.text"));
        encodingTyComboBox.setItemCaption(EncodingType.FLASH, StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.encoding.type.flash"));
        encodingTyComboBox.setItemCaption(EncodingType.BINARY, StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.encoding.type.binary"));

        encodingTyComboBox.setNullSelectionAllowed(false);
        encodingTyComboBox.setValue(EncodingType.TEXT);
        encodingLabel.setWidth("120px");
        encodingTyComboBox.setWidth("220px");
        encodingTypeLayout.addComponent(encodingLabel);
        encodingTypeLayout.addComponent(encodingTyComboBox);

        encodingTyComboBox.setImmediate(true);
        encodingTyComboBox.addListener(new Property.ValueChangeListener() {

            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                try {
                    onEncodingTypeChanged(((EncodingType) valueChangeEvent.getProperty().getValue()));
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while encoding type changed", e);
                }
            }

        });

        return encodingTypeLayout;
    }

    private void onEncodingTypeChanged(EncodingType encodingType) {
        if (encodingType.equals(EncodingType.BINARY)) {
            binaryHeaderLayout.addComponent(binaryHeaderLayoutContent);
        } else {
            binaryHeaderLayout.removeComponent(binaryHeaderLayoutContent);
        }
    }

    private Component createBinaryHeaderContent() {
        binaryHeaderLayoutContent = new HorizontalLayout();
        binaryHeaderLayoutContent.setSpacing(true);
        Label binaryHeaderLabel = new Label(StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.binary.header"));

        binaryHeaderLabel.setWidth("120px");
        binaryHeaderTextField.setWidth("220px");
        binaryHeaderLayoutContent.addComponent(binaryHeaderLabel);
        binaryHeaderLayoutContent.addComponent(binaryHeaderTextField);
        return binaryHeaderLayoutContent;
    }

    private HorizontalLayout createPanelLayout(final String msg, final AbstractTextField textField, final String textValue) {
        HorizontalLayout msgLayout = new HorizontalLayout();
        msgLayout.setSpacing(true);
        Label msgLabel = new Label(msg);
        msgLabel.setWidth("120px");
        textField.setWidth("220px");
        msgLayout.addComponent(msgLabel);
        msgLayout.addComponent(textField);
        textField.setValue(textValue);
        return msgLayout;
    }

    private void setDescriptionsForFields() {
        urlTextField.setDescription(StaticPropertiesService.getInstant().getMessage("URl.TextField.Description"));
        appIdTextField.setDescription(StaticPropertiesService.getInstant().getMessage("APPID.TextField.Description"));
        passwordTextField.setDescription(StaticPropertiesService.getInstant().getMessage("Password.TextField"));
        userNumberTextField.setDescription(StaticPropertiesService.getInstant().getMessage("SMS.CustomerNumber.TextField.Description"));
        toNumberTextField.setDescription(StaticPropertiesService.getInstant().getMessage("ReceivingNumber.TextField.Description"));
        encodingTyComboBox.setDescription(StaticPropertiesService.getInstant().getMessage("Encoding.ComboBox.Description"));
        messageField.setDescription(StaticPropertiesService.getInstant().getMessage("SMS.Message.TextArea.Description"));
        binaryHeaderTextField.setDescription(StaticPropertiesService.getInstant().getMessage("BinaryHeader.TextField.Description"));
        deReportCheckBox.setDescription(StaticPropertiesService.getInstant().getMessage("SMS.Generate.Delivery.Report.Request"));
        deReportUrlTextFiled.setDescription(StaticPropertiesService.getInstant().getMessage("SMS.Delivery.Report.Url.TextField"));
        errorCodeComboBox.setDescription(StaticPropertiesService.getInstant().getMessage("SMS.StatusCode.Request.To.Generate.ComboBox"));
    }

    /**
     * @return the Send Message Button
     */
    public abstract Button createSendMsgButton();


    public TextField getUserNumberTextField() {
        return userNumberTextField;
    }

    public TextField getToNumberTextField() {
        return toNumberTextField;
    }

    public TextArea getMessageField() {
        return messageField;
    }

    public TextField getUrlTextField() {
        return urlTextField;
    }

    public TextField getAppIdTextField() {
        return appIdTextField;
    }

    public ComboBox getEncodingTyComboBox() {
        return encodingTyComboBox;
    }

    public TextField getBinaryHeaderTextField() {
        return binaryHeaderTextField;
    }

    public Select getErrorCodeComboBox() {
        return errorCodeComboBox;
    }

    public TextField getPasswordTextField() {
        return passwordTextField;
    }

    public CheckBox getDeReportCheckBox() {
        return deReportCheckBox;
    }

    public TextField getDeReportUrlTextFiled() {
        return deReportUrlTextFiled;
    }

    /**
     * @return a Vertical Layout containing the contents inside USSD TabView
     */
    public abstract Component getTabLayout();
}
