/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.simulator.webui.ui;

import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.Component;
import com.vaadin.ui.TabSheet;
import hms.kite.simulator.ServiceLocator;
import hms.kite.simulator.webui.StaticPropertiesService;
import hms.kite.simulator.webui.ui.tab.TabViewSms;
import hms.kite.simulator.webui.ui.tab.TabViewCommon;
import hms.kite.simulator.webui.ui.tab.TabViewUssd;

import java.util.Map;

public class TabSheetPanel {

    private TabViewSms smsTabView;
    private TabViewUssd ussdTabView;
    private TabViewCommon wPTabView;
    private TabViewCommon casTabView;
    private TabViewCommon subTabView;

    public TabSheetPanel(TabViewSms smsTabView, TabViewUssd ussdTabView, TabViewCommon WPTabView, TabViewCommon CasTabView, TabViewCommon tabViewSub) {
        this.smsTabView = smsTabView;
        this.ussdTabView = ussdTabView;
        this.wPTabView = WPTabView;
        this.casTabView = CasTabView;
        this.subTabView = tabViewSub;
    }

    /**
     * @return the tabView sheet
     */
    public Component createTabSheetPanel() {

        TabSheet tabSheet = new TabSheet();
        Map<String, String> tab = (Map<String, String>) ServiceLocator.getBean("UIRequiredConfigurations");

        if (tab.get("sms").equals("true")) {
            tabSheet.addTab(smsTabView.getTabLayout(), StaticPropertiesService.getInstant().getMessage("Tabsheet.SMSTab.Title"), new ThemeResource("images/mobile_phone2.ico"));
        }
        if (tab.get("ussd").equals("true")) {
            tabSheet.addTab(ussdTabView.getTabLayout(), StaticPropertiesService.getInstant().getMessage("Tabsheet.USSDTab.Title"), new ThemeResource("images/mobile_phone2.ico"));
        }
        if (tab.get("wappush").equals("true")) {
            tabSheet.addTab(wPTabView.getTabLayout(), StaticPropertiesService.getInstant().getMessage("Tabsheet.WPTab.Title"), new ThemeResource("images/mobile_phone2.ico"));
        }
        if (tab.get("caas").equals("true")) {
            tabSheet.addTab(casTabView.getTabLayout(), StaticPropertiesService.getInstant().getMessage("Tabsheet.CasTab.Title"), new ThemeResource("images/mobile_phone2.ico"));
        }
        if (tab.get("subscription").equals("true")) {
            tabSheet.addTab(subTabView.getTabLayout(), StaticPropertiesService.getInstant().getMessage("Tabsheet.SubTab.Title"), new ThemeResource("images/mobile_phone2.ico"));
        }

        return tabSheet;
    }

}
