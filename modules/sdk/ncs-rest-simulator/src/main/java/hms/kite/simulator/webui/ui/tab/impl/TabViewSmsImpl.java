/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package hms.kite.simulator.webui.ui.tab.impl;

import com.github.wolfie.refresher.Refresher;
import com.vaadin.ui.*;
import hms.kite.simulator.common.EncodingType;
import hms.kite.simulator.sms.SmsRequestMessage;
import hms.kite.simulator.webui.StaticPropertiesService;
import hms.kite.simulator.webui.ui.services.NcsService;
import hms.kite.simulator.webui.ui.services.NcsUIService;
import hms.kite.simulator.webui.ui.tab.TabViewSms;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TabViewSmsImpl extends TabViewSms {

    private static final Logger LOGGER = LoggerFactory.getLogger(TabViewSmsImpl.class);

    private static final int REFRESH_INTERVAL = 4000;
    private ScheduledExecutorService executorService;
    private static String deliveryReportUrl;
    private static String smsErrorCodeRequest;

    final private Table sentMessageTable;
    final private Table receivedMessageTable;
    final private Button sentMsgClearButton;
    final private Button receiveMsgClearButton;
    final private NcsUIService ncsUIService;
    final private Label phoneImageNumLabel;
    final private Label phoneImageMessageLabel;
    private Refresher refresher;


    public TabViewSmsImpl(NcsUIService ncsUIService) {
        init();
        this.ncsUIService = ncsUIService;
        sentMessageTable = ncsUIService.createSentMessageService();
        receivedMessageTable = ncsUIService.createReceivedMessageService();

        sentMsgClearButton = ncsUIService.createClearSentMessagesButton();
        receiveMsgClearButton = ncsUIService.createClearReceivedMessagesButton();

        phoneImageNumLabel = new Label();
        phoneImageMessageLabel = new Label();
        phoneImageNumLabel.setWidth("98px");
        phoneImageNumLabel.setStyleName("address-display");
        phoneImageMessageLabel.setContentMode(Label.CONTENT_RAW);
        phoneImageMessageLabel.setStyleName("message-display");
        refresher = new Refresher();
        refresher.setRefreshInterval(REFRESH_INTERVAL);
    }

    public void init() {
        super.init();
        if (executorService == null) {
            executorService = Executors.newScheduledThreadPool(1);
            executorService.scheduleAtFixedRate(new Runnable() {
                public void run() {
                    try {
                        final NcsService ncsService = ncsUIService.getNcsService();
                        List receivedMessages = ncsService.receivedMessages();
                        for (int i = 0, receivedMessagesSize = receivedMessages.size(); i < receivedMessagesSize; i++) {
                            ncsUIService.addElementToReceiveTable(i, receivedMessages.get(i));
                        }
                        if (receivedMessages.size() > 0) {
                            ncsService.updatePhoneView(phoneImageNumLabel, phoneImageMessageLabel, receivedMessages.get(receivedMessages.size() - 1));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 4, 4, TimeUnit.SECONDS);
        }
    }

    @Override
    public Button createSendMsgButton() {

        Button sendMsgButton = new Button(StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.sendButtonTitle"));
        sendMsgButton.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                deliveryReportUrl = getDeReportUrlTextFiled().getValue().toString();
                final String address = getToNumberTextField().getValue().toString(); //encryptAddress(getPhoneNoField().getValue().toString());
                final String message = getMessageField().getValue().toString();
                smsErrorCodeRequest = getErrorCodeComboBox().getValue().toString();

                //edited
                final String url = getUrlTextField().getValue().toString();
                final String appId = getAppIdTextField().getValue().toString();
                final String password = getPasswordTextField().getValue().toString();
                // -edited

                String encodingType =  ((EncodingType) getEncodingTyComboBox().getValue()).getCode();
                String binaryHeader = (String) getBinaryHeaderTextField().getValue();

                SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
                try {
                    final String myNumber = getUserNumberTextField().getValue().toString();

                    SmsRequestMessage smsRequest = new SmsRequestMessage();
                    smsRequest.setSourceAddress(address);
                    smsRequest.setSourceAddress(myNumber);
                    smsRequest.setMessage(message);
                    smsRequest.setApplicationId(appId);
                    smsRequest.setEncoding(encodingType);
                    smsRequest.setBinaryHeader(binaryHeader);
                    if ((Boolean) getDeReportCheckBox().getValue()) {
                        smsRequest.setDeliveryStatusRequest("1");
                    }
                    //String url = null;
                    Map<String, Object> response = ncsUIService.getNcsService().sendMessage(smsRequest, url);
                    String status = String.valueOf(response.get("statusDetail"));
                    ncsUIService.addElementToSentTable(dateFormat.format(new Date()), address, message, status);
                    LOGGER.info("SMS sending status : {}.......", status);
                } catch (Exception e) {
                    ncsUIService.addElementToSentTable(dateFormat.format(new Date()), address, message, "Failed");
                    LOGGER.info("SMS sending status : Failed ........");
                }
            }
        });
        return sendMsgButton;
    }

    /**
     *
     * @param phoneNo
     * @return  the MD5 checksum of the phoneNo
     */
    private String encryptAddress(String phoneNo) {
        String encryptedAddress = "";
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            byte[] bytes = messageDigest.digest(phoneNo.getBytes());
            for (int i=0; i < bytes.length; i++) { //converting byte array to hex string
                encryptedAddress += Integer.toString( ( bytes[i] & 0xff ) + 0x100, 16).substring( 1 );
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return encryptedAddress;
    }

    @Override
    public Component getTabLayout() {

        VerticalLayout tabLayout = new VerticalLayout();
        tabLayout.setMargin(true);
        tabLayout.setSpacing(true);

        HorizontalLayout tabUpperLayout = new HorizontalLayout();
        tabUpperLayout.setWidth("100%");
        tabUpperLayout.setMargin(true);

        Component mobilePhoneLayout = createMobilePhone();
        tabUpperLayout.addComponent(mobilePhoneLayout);
        tabUpperLayout.setComponentAlignment(mobilePhoneLayout, Alignment.TOP_LEFT);

        Component inputFieldPanel = createInputPanel();
        tabUpperLayout.addComponent(inputFieldPanel);
        tabUpperLayout.setComponentAlignment(inputFieldPanel, Alignment.TOP_RIGHT);
        inputFieldPanel.setWidth("400px");
        tabLayout.addComponent(tabUpperLayout);

        VerticalLayout tableLayout = new VerticalLayout();
        tableLayout.setSpacing(true);
        tableLayout.setMargin(true);
        tableLayout.setWidth("100%");

        VerticalLayout receivedOuterLayout = new VerticalLayout();
        receivedOuterLayout.setWidth("100%");
        receivedOuterLayout.setMargin(false);
        receivedOuterLayout.setSpacing(false);
        receivedOuterLayout.setStyleName("table-outer-layout");

        HorizontalLayout receivedMessageTableLayout = new HorizontalLayout();
        receivedMessageTableLayout.setHeight("220px");
        receivedMessageTableLayout.setWidth("100%");
        receivedMessageTableLayout.setMargin(false);
        receivedMessageTableLayout.addComponent(receivedMessageTable);
        receivedOuterLayout.addComponent(receivedMessageTableLayout);

        HorizontalLayout receivedBottomLayout = new HorizontalLayout();
        receivedBottomLayout.setHeight("8px");
        receivedBottomLayout.setWidth("100%");
        receivedBottomLayout.setStyleName("bottom-layout");
        receivedOuterLayout.addComponent(receivedBottomLayout);

        HorizontalLayout receivedButtonLayout = new HorizontalLayout();
        receivedButtonLayout.addComponent(receiveMsgClearButton);
        receivedButtonLayout.setHeight("30px");
        receivedButtonLayout.setWidth("100%");
        receivedButtonLayout.setComponentAlignment(receiveMsgClearButton,Alignment.BOTTOM_RIGHT);
        receivedOuterLayout.addComponent(receivedButtonLayout);

        VerticalLayout sentOuterLayout = new VerticalLayout();
        sentOuterLayout.setWidth("100%");
        sentOuterLayout.setMargin(false);
        sentOuterLayout.setSpacing(false);
        sentOuterLayout.setStyleName("table-outer-layout");

        HorizontalLayout sentMessageTableLayout = new HorizontalLayout();
        sentMessageTableLayout.setHeight("220px");
        sentMessageTableLayout.setWidth("100%");
        sentMessageTableLayout.setMargin(false);
        sentMessageTableLayout.addComponent(sentMessageTable);
        sentOuterLayout.addComponent(sentMessageTableLayout);

        HorizontalLayout sentBottomLayout = new HorizontalLayout();
        sentBottomLayout.setHeight("8px");
        sentBottomLayout.setWidth("100%");
        sentBottomLayout.setStyleName("bottom-layout");
        sentOuterLayout.addComponent(sentBottomLayout);

        HorizontalLayout sentButtonLayout = new HorizontalLayout();
        sentButtonLayout.addComponent(sentMsgClearButton);
        sentButtonLayout.setHeight("30px");
        sentButtonLayout.setWidth("100%");
        sentButtonLayout.setComponentAlignment(sentMsgClearButton,Alignment.BOTTOM_RIGHT);
        sentOuterLayout.addComponent(sentButtonLayout);

        tableLayout.addComponent(receivedOuterLayout);
        tableLayout.addComponent(sentOuterLayout);
        tableLayout.addComponent(refresher);
        tabLayout.addComponent(tableLayout);

        return tabLayout;
    }


    /**
     * @return a vertical layout containing mobile phone image
     */
    public Component createMobilePhone() {

        Panel phonePannel = new Panel(StaticPropertiesService.getInstant().getMessage("SMSTab.PhonePanel.Title"));
        phonePannel.setWidth("180px");

        VerticalLayout backgroundLayout = new VerticalLayout();
        backgroundLayout.setWidth("119px");
        backgroundLayout.setHeight("260px");
        backgroundLayout.setStyleName("mobile-phone-background");

        VerticalLayout displayLayout = new VerticalLayout();
        displayLayout.setWidth("98px");
        displayLayout.addComponent(phoneImageNumLabel);

        HorizontalLayout messageLayout = new HorizontalLayout();
        messageLayout.setWidth("98px");
        messageLayout.addComponent(phoneImageMessageLabel);
        messageLayout.setExpandRatio(phoneImageMessageLabel,1);

        displayLayout.addComponent(messageLayout);
        displayLayout.addComponent(refresher);
        backgroundLayout.addComponent(displayLayout);
        phonePannel.addComponent(backgroundLayout);
        return phonePannel;
    }

    public static String deliveryReportUrl() {
        return deliveryReportUrl;
    }

    public static String getSmsErrorCodeRequest() {
        if (smsErrorCodeRequest == null) {
            return "S1000:Success";
        } else {
            return smsErrorCodeRequest;
        }
    }
}
