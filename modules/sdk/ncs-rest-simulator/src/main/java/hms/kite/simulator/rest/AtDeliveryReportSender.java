/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.rest;

import hms.common.rest.util.JsonBodyProvider;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AtDeliveryReportSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(AtDeliveryReportSender.class);

    public Map<String, Object> sendMessage(Map<String, Object> requestMessage, String destinationUrl) {
        try {
            Response response = makeRestCall(destinationUrl, requestMessage);
            if (response.getStatus() == 200) {
                LOGGER.info("Delivery report of the address {} has sent to application", requestMessage.get("destinationAddress"));
                Map<String, Object> tmp = new HashMap<String, Object>();
                tmp.put("statusDetail", "SUCCESS");
                return tmp;
            } else {
                LOGGER.error("HTTP Error while sending delivery report");
                return ResponseBuilder.generate("1", "HTTP-ERROR:" + response.getStatus());
            }
        } catch (Throwable e) {
            LOGGER.error("Error while sending delivery report to the application or application may not available for given port", e);
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * Sending request to the given URL
     *
     * @param restUrl
     * @param parameters
     * @return
     */
    private Response makeRestCall(String restUrl, Map<String, Object> parameters) {
        LOGGER.debug("Sending delivery report [{}] ", parameters.toString());
        List<Object> bodyProviders = new ArrayList<Object>();
        bodyProviders.add(new JsonBodyProvider());
        WebClient webClient = WebClient.create(restUrl, bodyProviders);
        webClient.header("Content-Type", "application/json");
        webClient.accept("application/json");
        return webClient.post(parameters);
    }

}