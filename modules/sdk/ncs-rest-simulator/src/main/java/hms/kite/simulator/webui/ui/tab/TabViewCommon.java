/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.webui.ui.tab;

import com.vaadin.ui.*;
import hms.kite.simulator.ErrorCodeLoader;
import hms.kite.simulator.webui.StaticPropertiesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class TabViewCommon {

    private static Logger logger = LoggerFactory.getLogger(TabViewCommon.class);

    private ComboBox errorCodeComboBox;

    /**
     * @return the Panel containing input text fields
     */
    public Component createInputPanel() {

        Panel inputPanel = new Panel(
                StaticPropertiesService.getInstant().getMessage("Tab.InputPanel.StatusCode.Request"));
        inputPanel.setWidth("295px");
        inputPanel.addComponent(createErrorCodeField());

        final AbstractOrderedLayout content = (AbstractOrderedLayout) inputPanel.getContent();
        content.setSpacing(true);

        return inputPanel;
    }

    private Component createErrorCodeField() {
        HorizontalLayout errorCodeLayout = new HorizontalLayout();
        errorCodeLayout.setSpacing(true);
        Label errorCodeLbl = new Label(StaticPropertiesService.getInstant().getMessage("Tab.InputPanel.errorCode"));
        errorCodeComboBox = new ComboBox();
        errorCodeLbl.setWidth("120px");
        errorCodeComboBox.setWidth("220px");

        setSelectData(errorCodeComboBox);
        errorCodeComboBox.setNullSelectionAllowed(false);
        errorCodeComboBox.setValue("S1000:Success");

        errorCodeLayout.addComponent(errorCodeLbl);
        errorCodeLayout.addComponent(errorCodeComboBox);
        return errorCodeLayout;
    }

    private void setSelectData(ComboBox data) {
        ErrorCodeLoader errorCodeLoader = new ErrorCodeLoader();
        errorCodeLoader.setErrorDataForSmsList(data);
    }

    public Select getErrorCodeComboBox() {
        return errorCodeComboBox;
    }

    /**
     * @return a Vertical Layout containing the contents inside USSD TabView
     */
    public abstract Component getTabLayout();
}