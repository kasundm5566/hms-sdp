/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.webui.ui.tab.impl;

import com.github.wolfie.refresher.Refresher;
import com.vaadin.ui.*;
import hms.kite.simulator.MessageReceiver;
import hms.kite.simulator.ServiceLocator;
import hms.kite.simulator.sms.SmsRequestMessage;
import hms.kite.simulator.ussd.UssdRequestMessage;
import hms.kite.simulator.webui.StaticPropertiesService;
import hms.kite.simulator.webui.ui.services.NcsUIService;
import hms.kite.simulator.webui.ui.tab.TabViewUssd;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TabViewUssdImpl extends TabViewUssd {
    private static final Logger LOGGER = LoggerFactory.getLogger(TabViewUssdImpl.class);

    private static final int REFRESH_INTERVAL = 4000;
    private static String ussdErrorCodeRequest;

    private ScheduledExecutorService executorService;

    final private Table sentMessageTable;
    final private Table receivedMessageTable;
    final private Button sentMsgClearButton;
    final private Button receiveMsgClearButton;
    final private NcsUIService ncsUIService;
    final private Label phoneImageNumLabel;
    final private Label phoneImageMessageLabel;
    private Refresher refresher;
    private MessageReceiver ussdMessageReceiver;


    public TabViewUssdImpl(NcsUIService ncsUIService) {
        init();
        this.ncsUIService = ncsUIService;
        sentMessageTable = ncsUIService.createSentMessageService();
        receivedMessageTable = ncsUIService.createReceivedMessageService();

        sentMsgClearButton = ncsUIService.createClearSentMessagesButton();
        receiveMsgClearButton = ncsUIService.createClearReceivedMessagesButton();

        phoneImageNumLabel = new Label();
        phoneImageMessageLabel = new Label();
        phoneImageNumLabel.setWidth("98px");
        phoneImageNumLabel.setStyleName("address-display");
        phoneImageMessageLabel.setContentMode(Label.CONTENT_RAW);
        phoneImageMessageLabel.setStyleName("message-display");
        refresher = new Refresher();
        refresher.setRefreshInterval(REFRESH_INTERVAL);
    }

    public void init() {
        super.init();
        ussdMessageReceiver = (MessageReceiver) ServiceLocator.getBean("ussdMessageReceiver");
        if (executorService == null) {
            executorService = Executors.newScheduledThreadPool(1);
            executorService.scheduleAtFixedRate(new Runnable() {
                public void run() {
                    try {
//                        final NcsService ncsService = ncsUIService.getNcsService();
                        List receivedMessages = ussdMessageReceiver.getReceivedMessages();
                        for (int i = 0, receivedMessagesSize = receivedMessages.size(); i < receivedMessagesSize; i++) {
                            addElementToReceiveTable(i, receivedMessages.get(i));
                        }
                        if (receivedMessages.size() > 0) {
                            updatePhoneView(phoneImageNumLabel, phoneImageMessageLabel, receivedMessages.get(receivedMessages.size() - 1));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 4, 4, TimeUnit.SECONDS);
        }
    }

    private void addElementToReceiveTable(int objectId, Object object) {

        if (receivedMessageTable.getItem(objectId) == null) {
            UssdRequestMessage ussdReqMessage = (UssdRequestMessage) object;
            SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
            receivedMessageTable.addItem(new Object[]{dateFormat.format(new Date()), ussdReqMessage.getDestinationAddress(),
                    ussdReqMessage.getMessage()}, objectId);
        }
    }

    private void updatePhoneView(Label phoneNum, Label message, Object val) {
        UssdRequestMessage ussdReqMessage = (UssdRequestMessage) val;
        phoneNum.setValue(ussdReqMessage.getDestinationAddress().replace("tel:",""));
        message.setValue(ussdReqMessage.getMessage());
    }

    @Override
    public Button createSendMsgButton() {

        Button sendMsgButton = new Button(StaticPropertiesService.getInstant().getMessage("SMSTab.InputPanel.sendButtonTitle"));
        sendMsgButton.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                final String userAddress = getUserNumberTextField().getValue().toString();
                final String message = getMessageField().getValue().toString();
                ussdErrorCodeRequest = getErrorCodeComboBox().getValue().toString();

                final String ussdOperationType = getUssdOpTypeComboBox().getValue().toString();
                final String url = getUrlField().getValue().toString();
                final String appId = getAppIdField().getValue().toString();
                final String sessionId = getSessionIdTextField().getValue().toString();

                SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
                try {
                    UssdRequestMessage requestMessage = new UssdRequestMessage();
                    requestMessage.setSourceAddress(userAddress);
                    requestMessage.setMessage(message);
                    requestMessage.setApplicationId(appId);
                    requestMessage.setSessionId(sessionId);
                    requestMessage.setUssdOperation(ussdOperationType);
                    Map<String, Object> reponse = ncsUIService.getNcsService().sendMessage(requestMessage, url);
                    String status = String.valueOf(reponse.get("statusDetail"));
                    ncsUIService.addElementToSentTable(dateFormat.format(new Date()), userAddress, message, status);
                    LOGGER.info("USSD Request sending status : {}.......", status);
                } catch (Exception e) {
                    ncsUIService.addElementToSentTable(dateFormat.format(new Date()), userAddress, message, "Failed");
                    LOGGER.info("USSD Request sending status : Failed ........");
                }
            }
        });
        return sendMsgButton;
    }

    @Override
    public Component getTabLayout() {

        VerticalLayout tabLayout = new VerticalLayout();
        tabLayout.setMargin(true);
        tabLayout.setSpacing(true);

        HorizontalLayout tabUpperLayout = new HorizontalLayout();
        tabUpperLayout.setWidth("100%");
        tabUpperLayout.setMargin(true);

        Component mobilePhoneLayout = createMobilePhone();
        tabUpperLayout.addComponent(mobilePhoneLayout);
        tabUpperLayout.setComponentAlignment(mobilePhoneLayout, Alignment.TOP_LEFT);

        Component inputFieldPanel = createInputPanel();
        tabUpperLayout.addComponent(inputFieldPanel);
        inputFieldPanel.setWidth("400px");
        tabUpperLayout.setComponentAlignment(inputFieldPanel, Alignment.TOP_RIGHT);
        tabLayout.addComponent(tabUpperLayout);

        VerticalLayout tableLayout = new VerticalLayout();
        tableLayout.setSpacing(true);
        tableLayout.setMargin(true);
        tableLayout.setWidth("100%");

        VerticalLayout receivedOuterLayout = new VerticalLayout();
        receivedOuterLayout.setWidth("100%");
        receivedOuterLayout.setMargin(false);
        receivedOuterLayout.setSpacing(false);
        receivedOuterLayout.setStyleName("table-outer-layout");

        HorizontalLayout receivedMessageTableLayout = new HorizontalLayout();
        receivedMessageTableLayout.setHeight("220px");
        receivedMessageTableLayout.setWidth("100%");
        receivedMessageTableLayout.setMargin(false);
        receivedMessageTableLayout.addComponent(receivedMessageTable);
        receivedOuterLayout.addComponent(receivedMessageTableLayout);

        HorizontalLayout receivedBottomLayout = new HorizontalLayout();
        receivedBottomLayout.setHeight("8px");
        receivedBottomLayout.setWidth("100%");
        receivedBottomLayout.setStyleName("bottom-layout");
        receivedOuterLayout.addComponent(receivedBottomLayout);

        HorizontalLayout receivedButtonLayout = new HorizontalLayout();
        receivedButtonLayout.addComponent(receiveMsgClearButton);
        receivedButtonLayout.setHeight("30px");
        receivedButtonLayout.setWidth("100%");
        receivedButtonLayout.setComponentAlignment(receiveMsgClearButton,Alignment.BOTTOM_RIGHT);
        receivedOuterLayout.addComponent(receivedButtonLayout);

        VerticalLayout sentOuterLayout = new VerticalLayout();
        sentOuterLayout.setWidth("100%");
        sentOuterLayout.setMargin(false);
        sentOuterLayout.setSpacing(false);
        sentOuterLayout.setStyleName("table-outer-layout");

        HorizontalLayout sentMessageTableLayout = new HorizontalLayout();
        sentMessageTableLayout.setHeight("220px");
        sentMessageTableLayout.setWidth("100%");
        sentMessageTableLayout.setMargin(false);
        sentMessageTableLayout.addComponent(sentMessageTable);
        sentOuterLayout.addComponent(sentMessageTableLayout);

        HorizontalLayout sentBottomLayout = new HorizontalLayout();
        sentBottomLayout.setHeight("8px");
        sentBottomLayout.setWidth("100%");
        sentBottomLayout.setStyleName("bottom-layout");
        sentOuterLayout.addComponent(sentBottomLayout);

        HorizontalLayout sentButtonLayout = new HorizontalLayout();
        sentButtonLayout.addComponent(sentMsgClearButton);
        sentButtonLayout.setHeight("30px");
        sentButtonLayout.setWidth("100%");
        sentButtonLayout.setComponentAlignment(sentMsgClearButton,Alignment.BOTTOM_RIGHT);
        sentOuterLayout.addComponent(sentButtonLayout);

        tableLayout.addComponent(receivedOuterLayout);
        tableLayout.addComponent(sentOuterLayout);
        tableLayout.addComponent(refresher);
        tabLayout.addComponent(tableLayout);

        return tabLayout;
    }


    /**
     * @return a vertical layout containing mobile phone image
     */
    public Component createMobilePhone() {

        Panel phonePannel = new Panel(StaticPropertiesService.getInstant().getMessage("SMSTab.PhonePanel.Title"));
        phonePannel.setWidth("180px");

        VerticalLayout backgroundLayout = new VerticalLayout();
        backgroundLayout.setWidth("119px");
        backgroundLayout.setHeight("260px");
        backgroundLayout.setStyleName("mobile-phone-background");

        VerticalLayout displayLayout = new VerticalLayout();
        displayLayout.setWidth("98px");
        displayLayout.addComponent(phoneImageNumLabel);

        HorizontalLayout messageLayout = new HorizontalLayout();
        messageLayout.setWidth("98px");
        messageLayout.addComponent(phoneImageMessageLabel);
        messageLayout.setExpandRatio(phoneImageMessageLabel,1);

        displayLayout.addComponent(messageLayout);
        displayLayout.addComponent(refresher);
        backgroundLayout.addComponent(displayLayout);
        phonePannel.addComponent(backgroundLayout);
        return phonePannel;
    }

    public static String getUssdErrorCodeRequest() {
        return ussdErrorCodeRequest;
    }
}