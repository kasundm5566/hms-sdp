/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.caas;

import hms.kite.simulator.MessageReceiver;
import hms.kite.simulator.webui.ui.tab.impl.TabViewCasImpl;
import hms.kite.simulator.webui.ui.tab.impl.TabViewSmsImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class PaymentInstrumentListReqReceiver implements MessageReceiver {

    public static List<PaymentInstrumentReqMessage> receivedPayInsListReq = new ArrayList<PaymentInstrumentReqMessage>();

    @Override
    public Map<String, Object> onMessage(Map<String, Object> message) {
        Map<String, Object> response;

        String[] statusReq = getRequestedStatus();
        PaymentInstrumentReqMessage paymentInsReq = decodeMessage(message);
        receivedPayInsListReq.add(paymentInsReq);
        List<Map<String, Object>> paymentInList = createAvailablePaymentInstrumentList();
        response = CaasResponseBuilder.getPaymentInstrumentResponse(paymentInList, statusReq[0], statusReq[1]);
        return response;
    }

    private List<Map<String, Object>> createAvailablePaymentInstrumentList() {
        List<Map<String, Object>> paymentInstruments = new ArrayList<Map<String, Object>>();
        Map<String, Object> instrument;

        List<String> pil = TabViewCasImpl.getUsedPaymentInstruments();
        String[] data;
        if (pil != null) {
            for (String s : pil) {
                instrument = new HashMap<String, Object>();
                data = s.split(":");
                if (data.length == 2) {
                    instrument.put("name", data[0]);
                    instrument.put("type", data[1]);
                    paymentInstruments.add(instrument);
                } else if (data.length == 3){
                    instrument.put("name", data[0]);
                    instrument.put("type", data[1]);
                    instrument.put("accountId", data[2]);
                    paymentInstruments.add(instrument);
                }
            }
        } else {
            instrument = new HashMap<String, Object>();
            instrument.put("name", "Equity");
            instrument.put("type", "async");
            paymentInstruments.add(instrument);
        }
        return paymentInstruments;
    }

    private PaymentInstrumentReqMessage decodeMessage(Map<String, Object> message) {

        PaymentInstrumentReqMessage paymentInstrumentReq = new PaymentInstrumentReqMessage();
        paymentInstrumentReq.setApplicationId(String.valueOf(message.get("applicationId")));
        paymentInstrumentReq.setSubscriberId(String.valueOf(message.get("subscriberId")));
        paymentInstrumentReq.setPassword(String.valueOf(message.get("password")));
        paymentInstrumentReq.setType(String.valueOf(message.get("type")));

        return paymentInstrumentReq;
    }

    @Override
    public List getReceivedMessages() {
        return receivedPayInsListReq;
    }

    private String[] getRequestedStatus() {
        return TabViewSmsImpl.getSmsErrorCodeRequest().split(":");
    }

}