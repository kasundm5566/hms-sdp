/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.sms;

import hms.kite.simulator.MessageSender;
import hms.kite.simulator.RequestMessage;
import hms.kite.simulator.common.EncodingType;
import hms.kite.simulator.rest.AtRequestSender;

import java.util.HashMap;
import java.util.Map;

public class SmsMessageSender implements MessageSender {

	private AtRequestSender messageSender;

	@Override
	public Map<String, Object> sendMessage(RequestMessage requestMessage, String url) {

        SmsRequestMessage smsRequest = (SmsRequestMessage) requestMessage;

		Map<String, Object> atMessage = new HashMap<String, Object>();
		atMessage.put("encoding", smsRequest.getEncoding());
        if(EncodingType.BINARY.getCode().equals(smsRequest.getEncoding())) {
            atMessage.put("binaryHeader", smsRequest.getBinaryHeader());
        }
        atMessage.put("version", "1.0");
        atMessage.put("applicationId", smsRequest.getApplicationId());
        atMessage.put("sourceAddress", "tel:"+smsRequest.getSourceAddress());
        atMessage.put("message", smsRequest.getMessage());
        atMessage.put("requestId", String.valueOf(System.currentTimeMillis()));
        atMessage.put("encoding", smsRequest.getEncoding());
        atMessage.put("deliveryStatusRequest", smsRequest.getDeliveryStatusRequest());
		return messageSender.sendMessage(atMessage, url);
	}

	public void setMessageSender(AtRequestSender messageSender) {
		this.messageSender = messageSender;
	}

}
