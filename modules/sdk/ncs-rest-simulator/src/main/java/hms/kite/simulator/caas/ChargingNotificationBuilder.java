/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.caas;

import hms.kite.simulator.ServiceLocator;

import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ChargingNotificationBuilder {

    public static ChargingNotificationMessage createNotification(String externalTrx,
                                                           String internalTrx,
                                                           String referenceId,
                                                           String paidAmount,
                                                           String totalAmount,
                                                           String balanceDue,
                                                           String currency,
                                                           String statusCode,
                                                           String statusDetail,
                                                           String timeStamp
                                                           ) {
        ChargingNotificationMessage noteMsg = new ChargingNotificationMessage();
        noteMsg.setExternalTrxId(externalTrx);
        noteMsg.setInternalTrxId(internalTrx);
        noteMsg.setReferenceId(referenceId);
        noteMsg.setPaidAmount(paidAmount);
        noteMsg.setTotalAmount(totalAmount);
        noteMsg.setBalanceDue(balanceDue);
        noteMsg.setCurrency(currency);
        noteMsg.setStatusCode(statusCode);
        noteMsg.setStatusDetail(statusDetail);
        noteMsg.setTimeStamp(timeStamp);
        return noteMsg;
    }

    public static Map<String, Object> sendNotification(ChargingNotificationMessage noteMsg, String url) {
        ChargingNotificationSender chargingNotificationSender  = (ChargingNotificationSender) ServiceLocator.getBean("caasAtSender");
        return chargingNotificationSender.sendMessage(noteMsg, url);
    }

}