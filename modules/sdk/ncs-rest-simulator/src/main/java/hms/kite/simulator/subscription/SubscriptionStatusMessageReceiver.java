/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.subscription;

import hms.kite.simulator.MessageReceiver;
import hms.kite.simulator.common.SubscriptionStatus;
import hms.kite.simulator.webui.ui.tab.impl.TabViewSmsImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubscriptionStatusMessageReceiver implements MessageReceiver {

    private static Logger logger = LoggerFactory.getLogger(SubscriptionStatusMessageReceiver.class);
    public static List receivedStatusMessages = new ArrayList();

    @Override
    public Map<String, Object> onMessage(Map<String, Object> message) {
        logger.info("Subscription status message received {}", message);
        Map<String, Object> response;

        receivedStatusMessages.add(message);

        response = createResponse(message);
        logger.info("Sent respond back to application : {}", response);
        return response;
    }

    @Override
    public List getReceivedMessages() {
        return receivedStatusMessages;
    }

    private Map<String, Object> createResponse(Map<String, Object> msg) {

        String[] statusReq = getRequestedStatus();
        Map<String, Object> response;
        String msisdn = (String) msg.get("sourceAddress");
        String regStatus = "";
        /* Finding the registration status of the received MSISDN*/
        for (Map<String, Object> map : SubscriptionMessageReceiver.registeredMSISDN) {
            if (msisdn.equals(map.get("subscriberId"))) {
                regStatus = (String) map.get("status");
            }
        }
        if (regStatus.equals("")) {
            regStatus = SubscriptionStatus.UNREGISTERED;
        }
        response = generate(statusReq[0], statusReq[1], regStatus, "0.1");
        return response;
    }

    private Map<String, Object> generate(String statusCode, String statusDescription,
                                         String subscriptionStatus, String version) {
        Map<String, Object> msg = new HashMap<String, Object>();
        msg.put("statusCode", statusCode);
        msg.put("statusDetail", statusDescription);
        msg.put("subscriptionStatus", subscriptionStatus);
        msg.put("version", version);

        return msg;
    }

    private String[] getRequestedStatus() {
        return TabViewSmsImpl.getSmsErrorCodeRequest().split(":");
    }
}