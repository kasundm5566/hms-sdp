/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.webui.ui.tab.impl;

import com.github.wolfie.refresher.Refresher;
import com.vaadin.ui.*;
import hms.kite.simulator.MessageReceiver;
import hms.kite.simulator.ServiceLocator;
import hms.kite.simulator.WP.WPRequestMessage;
import hms.kite.simulator.webui.ui.services.NcsUIService;
import hms.kite.simulator.webui.ui.tab.TabViewCommon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TabViewWPImpl extends TabViewCommon {
    private static final Logger LOGGER = LoggerFactory.getLogger(TabViewWPImpl.class);

    private static final int REFRESH_INTERVAL = 4000;

    private ScheduledExecutorService executorService;

    final private Table receivedMessageTable;
    final private Button receiveMsgClearButton;
    private Refresher refresher;
    private MessageReceiver wpMessageReceiver;


    public TabViewWPImpl(NcsUIService ncsUIService) {
        init();

        receivedMessageTable = ncsUIService.createReceivedMessageService();
        receiveMsgClearButton = ncsUIService.createClearReceivedMessagesButton();
        refresher = new Refresher();
        refresher.setRefreshInterval(REFRESH_INTERVAL);
    }

    public void init() {
        wpMessageReceiver = (MessageReceiver) ServiceLocator.getBean("wpMessageReceiver");
        if (executorService == null) {
            executorService = Executors.newScheduledThreadPool(1);
            executorService.scheduleAtFixedRate(new Runnable() {
                public void run() {
                    try {
                        List receivedMessages = wpMessageReceiver.getReceivedMessages();
                        for (int i = 0, receivedMessagesSize = receivedMessages.size(); i < receivedMessagesSize; i++) {
                            addElementToReceiveTable(i, receivedMessages.get(i));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 4, 4, TimeUnit.SECONDS);
        }
    }

    private void addElementToReceiveTable(int objectId, Object object) {
        if (receivedMessageTable.getItem(objectId) == null) {
            WPRequestMessage wpReqMsg = (WPRequestMessage) object;
            SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
            receivedMessageTable.addItem(new Object[]{dateFormat.format(new Date()), wpReqMsg.getDestinationAddresses(),
                    wpReqMsg.getTitle(), wpReqMsg.getWapUrl()}, objectId);
        }
    }

    @Override
    public Component getTabLayout() {

        VerticalLayout tabLayout = new VerticalLayout();
        tabLayout.setMargin(true);
        tabLayout.setSpacing(true);

        HorizontalLayout tabUpperLayout = new HorizontalLayout();
        tabUpperLayout.setWidth("100%");
        tabUpperLayout.setMargin(true);

        //Uncomment following for add the error code selecting window

/*        Component inputFieldPanel = createInputPanel();
        tabUpperLayout.addComponent(inputFieldPanel);
        tabUpperLayout.setComponentAlignment(inputFieldPanel, Alignment.TOP_RIGHT);
        inputFieldPanel.setWidth("400px");
        tabLayout.addComponent(tabUpperLayout);*/

        VerticalLayout tableLayout = new VerticalLayout();
        tableLayout.setSpacing(true);
        tableLayout.setMargin(true);
        tableLayout.setWidth("100%");

        VerticalLayout receivedOuterLayout = new VerticalLayout();
        receivedOuterLayout.setWidth("100%");
        receivedOuterLayout.setMargin(false);
        receivedOuterLayout.setSpacing(false);
        receivedOuterLayout.setStyleName("table-outer-layout");

        HorizontalLayout receivedMessageTableLayout = new HorizontalLayout();
        receivedMessageTableLayout.setHeight("220px");
        receivedMessageTableLayout.setWidth("100%");
        receivedMessageTableLayout.setMargin(false);
        receivedMessageTableLayout.addComponent(receivedMessageTable);
        receivedOuterLayout.addComponent(receivedMessageTableLayout);

        HorizontalLayout receivedBottomLayout = new HorizontalLayout();
        receivedBottomLayout.setHeight("8px");
        receivedBottomLayout.setWidth("100%");
        receivedBottomLayout.setStyleName("bottom-layout");
        receivedOuterLayout.addComponent(receivedBottomLayout);

        HorizontalLayout receivedButtonLayout = new HorizontalLayout();
        receivedButtonLayout.addComponent(receiveMsgClearButton);
        receivedButtonLayout.setHeight("30px");
        receivedButtonLayout.setWidth("100%");
        receivedButtonLayout.setComponentAlignment(receiveMsgClearButton, Alignment.BOTTOM_RIGHT);
        receivedOuterLayout.addComponent(receivedButtonLayout);

        tableLayout.addComponent(receivedOuterLayout);
        tableLayout.addComponent(refresher);
        tabLayout.addComponent(tableLayout);

        return tabLayout;
    }
}
