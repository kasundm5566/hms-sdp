/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.webui.ui.tab.impl;

import com.github.wolfie.refresher.Refresher;
import com.vaadin.ui.*;
import hms.kite.simulator.MessageReceiver;
import hms.kite.simulator.ServiceLocator;
import hms.kite.simulator.common.SubscriptionStatus;
import hms.kite.simulator.subscription.SubscriptionMessageReceiver;
import hms.kite.simulator.subscription.SubscriptionQueryBaseReceiver;
import hms.kite.simulator.subscription.SubscriptionQueryBaseRequest;
import hms.kite.simulator.subscription.SubscriptionStatusMessageReceiver;
import hms.kite.simulator.webui.StaticPropertiesService;
import hms.kite.simulator.webui.ui.services.NcsUIService;
import hms.kite.simulator.webui.ui.tab.TabViewCommon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TabViewSubImpl extends TabViewCommon {

    private static final Logger LOGGER = LoggerFactory.getLogger(TabViewSubImpl.class);

    private static final int REFRESH_INTERVAL = 4000;

    private ScheduledExecutorService executorService;

    final private Table subRequestMessagesTable;
    final private Table subStatusRequestMessagesTable;
    final private Table subQueryBaseRequestMessagesTable;
    private Button subTableClearButton;
    private Button subStatusClearButton;
    private Button subQueryBaseClearButton;
    private Refresher refresher;
    private MessageReceiver subReqMessageReceiver;
    private MessageReceiver subReqStatusMessagesReceiver;
    private MessageReceiver subQueyBaseMessagesReceiver;

    public TabViewSubImpl(NcsUIService ncsUIService) {
        init();
        subRequestMessagesTable = ncsUIService.createReceivedMessageService();
        subStatusRequestMessagesTable = createSubscriptionStatusRequestMessagesTable();
        subStatusRequestMessagesTable.setStyleName("sent-message-table");
        subQueryBaseRequestMessagesTable = createSubQueryBaseRequestMessagesTable();
        subQueryBaseRequestMessagesTable.setStyleName("sent-message-table");
        subTableClearButton = createClearReceivedMessagesButton();
        subStatusClearButton = createClearStatusMessagesButton();
        subQueryBaseClearButton = createClearQueryBaseClearButton();

        refresher = new Refresher();
        refresher.setRefreshInterval(REFRESH_INTERVAL);
    }

    public void init() {
        subReqMessageReceiver = (MessageReceiver) ServiceLocator.getBean("subscriptionMessageReceiver");
        subReqStatusMessagesReceiver = (MessageReceiver) ServiceLocator.getBean("subReqStatusMessageReceiver");
        subQueyBaseMessagesReceiver = (MessageReceiver) ServiceLocator.getBean("subQueryBaseReqReceiver");
        if (executorService == null) {
            executorService = Executors.newScheduledThreadPool(1);
            executorService.scheduleAtFixedRate(new Runnable() {
                public void run() {
                    try {
                        List receivedMessages = subReqMessageReceiver.getReceivedMessages();
                        List receivedStatusMessage = subReqStatusMessagesReceiver.getReceivedMessages();
                        List receivedQueryBaseMessage = subQueyBaseMessagesReceiver.getReceivedMessages();
                        for (int i = 0, receivedMessagesSize = receivedMessages.size(); i < receivedMessagesSize; i++) {
                            addReceivedMessageToTable((Map<String, Object>) receivedMessages.get(i), i);
                        }
                        for (int i = 0, receivedMessagesSize = receivedStatusMessage.size(); i < receivedMessagesSize; i++) {
                            addReceivedMessageToStatusTable((Map<String, Object>) receivedStatusMessage.get(i), i);
                        }
                        for (int i = 0, receivedMessagesSize = receivedQueryBaseMessage.size(); i < receivedMessagesSize; i++) {
                            addReceivedMessageToQueryBaseTable(receivedQueryBaseMessage.get(i), i);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 4, 4, TimeUnit.SECONDS);
        }
    }

    private void addReceivedMessageToTable(Map<String, Object> subscriptionMessages, int index) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
        subRequestMessagesTable.addItem(new Object[]{
                dateFormat.format(new Date()), subscriptionMessages.get("applicationId"), subscriptionMessages.get("subscriberId"), subscriptionMessages.get("action")}, index);
    }


    private void addReceivedMessageToStatusTable(Map<String, Object> subscriptionMessages, int index) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
        String msisdn = (String) subscriptionMessages.get("sourceAddress");
        String status = SubscriptionStatus.UNREGISTERED;
        for (Map<String,Object> map : SubscriptionMessageReceiver.registeredMSISDN) {
            if (msisdn.equals(map.get("subscriberId"))) {
                status = (String) map.get("status");
            }
        }
        subStatusRequestMessagesTable.addItem(new Object[]{
                dateFormat.format(new Date()), subscriptionMessages.get("applicationId"), subscriptionMessages.get("sourceAddress"), status}, index);
    }

    private void addReceivedMessageToQueryBaseTable(Object queryBaseReq, int index) {
        SubscriptionQueryBaseRequest subQueryBaseReq = (SubscriptionQueryBaseRequest) queryBaseReq;
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
        subQueryBaseRequestMessagesTable.addItem(new Object[]{
                dateFormat.format(new Date()), subQueryBaseReq.getApplicationId(), SubscriptionQueryBaseReceiver.getBaseSize()}, index);
    }

    public Component getTabLayout() {

        VerticalLayout tabLayout = new VerticalLayout();
        tabLayout.setMargin(true);
        tabLayout.setSpacing(true);

        HorizontalLayout tabUpperLayout = new HorizontalLayout();
        tabUpperLayout.setWidth("100%");
        tabUpperLayout.setMargin(true);

        //Uncomment the following lines for the error code selecting window
//        createErrorCodeLayout(tabLayout, tabUpperLayout);

        VerticalLayout tableLayout = new VerticalLayout();
        tableLayout.setSpacing(true);
        tableLayout.setMargin(true);
        tableLayout.setWidth("100%");

        /*Create Subscription request layout*/
        VerticalLayout subReqOuterLayout = createSubscriptionReqLayout();

        /*Create subscription status message received layout*/
        VerticalLayout subStatusOuterLayout = createSubscriptionStatusReqLayout();

        /*Create subscription query base request layout*/
        VerticalLayout subQueryBaseOuterLayout = createSubQueryBaseReqLayout();

        tableLayout.addComponent(subReqOuterLayout);
        tableLayout.addComponent(subStatusOuterLayout);
        tableLayout.addComponent(subQueryBaseOuterLayout);
        tableLayout.addComponent(refresher);
        tabLayout.addComponent(tableLayout);

        return tabLayout;
    }

    private void createErrorCodeLayout(VerticalLayout tabLayout, HorizontalLayout tabUpperLayout) {
        Component inputFieldPanel = createInputPanel();
        tabUpperLayout.addComponent(inputFieldPanel);
        inputFieldPanel.setWidth("400px");
        tabUpperLayout.setComponentAlignment(inputFieldPanel, Alignment.TOP_RIGHT);
        tabLayout.addComponent(tabUpperLayout);
    }

    private VerticalLayout createSubQueryBaseReqLayout() {
        VerticalLayout subQueryBaseOuterLayout = new VerticalLayout();
        subQueryBaseOuterLayout.setWidth("100%");
        subQueryBaseOuterLayout.setMargin(false);
        subQueryBaseOuterLayout.setSpacing(false);
        subQueryBaseOuterLayout.setStyleName("table-outer-layout");

        HorizontalLayout subQueryBaseTableLayout = new HorizontalLayout();
        subQueryBaseTableLayout.setHeight("220px");
        subQueryBaseTableLayout.setWidth("100%");
        subQueryBaseTableLayout.setMargin(false);
        subQueryBaseTableLayout.addComponent(subQueryBaseRequestMessagesTable);
        subQueryBaseOuterLayout.addComponent(subQueryBaseTableLayout);

        HorizontalLayout queryBaseBottomLayout = new HorizontalLayout();
        queryBaseBottomLayout.setHeight("8px");
        queryBaseBottomLayout.setWidth("100%");
        queryBaseBottomLayout.setStyleName("bottom-layout");
        subQueryBaseOuterLayout.addComponent(queryBaseBottomLayout);

        HorizontalLayout queryBaseButtonLayout = new HorizontalLayout();
        queryBaseButtonLayout.addComponent(subQueryBaseClearButton);
        queryBaseButtonLayout.setHeight("30px");
        queryBaseButtonLayout.setWidth("100%");
        queryBaseButtonLayout.setComponentAlignment(subQueryBaseClearButton, Alignment.BOTTOM_RIGHT);
        subQueryBaseOuterLayout.addComponent(queryBaseButtonLayout);
        return subQueryBaseOuterLayout;
    }

    private VerticalLayout createSubscriptionStatusReqLayout() {
        VerticalLayout subStatusOuterLayout = new VerticalLayout();
        subStatusOuterLayout.setWidth("100%");
        subStatusOuterLayout.setMargin(false);
        subStatusOuterLayout.setSpacing(false);
        subStatusOuterLayout.setStyleName("table-outer-layout");

        HorizontalLayout sentMessageTableLayout = new HorizontalLayout();
        sentMessageTableLayout.setHeight("220px");
        sentMessageTableLayout.setWidth("100%");
        sentMessageTableLayout.setMargin(false);
        sentMessageTableLayout.addComponent(subStatusRequestMessagesTable);
        subStatusOuterLayout.addComponent(sentMessageTableLayout);

        HorizontalLayout sentBottomLayout = new HorizontalLayout();
        sentBottomLayout.setHeight("8px");
        sentBottomLayout.setWidth("100%");
        sentBottomLayout.setStyleName("bottom-layout");
        subStatusOuterLayout.addComponent(sentBottomLayout);

        HorizontalLayout sentButtonLayout = new HorizontalLayout();
        sentButtonLayout.addComponent(subStatusClearButton);
        sentButtonLayout.setHeight("30px");
        sentButtonLayout.setWidth("100%");
        sentButtonLayout.setComponentAlignment(subStatusClearButton, Alignment.BOTTOM_RIGHT);
        subStatusOuterLayout.addComponent(sentButtonLayout);
        return subStatusOuterLayout;
    }

    private VerticalLayout createSubscriptionReqLayout() {
        VerticalLayout subReqOuterLayout = new VerticalLayout();
        subReqOuterLayout.setWidth("100%");
        subReqOuterLayout.setMargin(false);
        subReqOuterLayout.setSpacing(false);
        subReqOuterLayout.setStyleName("table-outer-layout");

        HorizontalLayout receivedMessageTableLayout = new HorizontalLayout();
        receivedMessageTableLayout.setHeight("220px");
        receivedMessageTableLayout.setWidth("100%");
        receivedMessageTableLayout.setMargin(false);
        receivedMessageTableLayout.addComponent(subRequestMessagesTable);
        subReqOuterLayout.addComponent(receivedMessageTableLayout);

        HorizontalLayout receivedBottomLayout = new HorizontalLayout();
        receivedBottomLayout.setHeight("8px");
        receivedBottomLayout.setWidth("100%");
        receivedBottomLayout.setStyleName("bottom-layout");
        subReqOuterLayout.addComponent(receivedBottomLayout);

        HorizontalLayout receivedButtonLayout = new HorizontalLayout();
        receivedButtonLayout.addComponent(subTableClearButton);
        receivedButtonLayout.setHeight("30px");
        receivedButtonLayout.setWidth("100%");
        receivedButtonLayout.setComponentAlignment(subTableClearButton, Alignment.BOTTOM_RIGHT);
        subReqOuterLayout.addComponent(receivedButtonLayout);
        return subReqOuterLayout;
    }


    private Table createSubscriptionStatusRequestMessagesTable() {
        Table statusTable = new Table(StaticPropertiesService.getInstant().getMessage("SubTab.statusReceivedMessageTable.Title"));
        String[] headings = {
                StaticPropertiesService.getInstant().getMessage("SMSTab.ReceivedMessageTable.TimeColumn"),
                StaticPropertiesService.getInstant().getMessage("SubTab.ReceivedMessageTable.addId"),
                StaticPropertiesService.getInstant().getMessage("SubTab.ReceivedMessageTable.subscriberId"),
                StaticPropertiesService.getInstant().getMessage("SubTab.ReceivedMessageTable.Status")
        };
        for (String heading : headings) {
            statusTable.addContainerProperty(heading, String.class, null);
        }
        statusTable.setColumnExpandRatio(headings[0], 0.10f);
        statusTable.setColumnExpandRatio(headings[1], 0.30f);
        statusTable.setColumnExpandRatio(headings[2], 0.30f);
        statusTable.setColumnExpandRatio(headings[3], 0.30f);

        statusTable.setHeight("100%");
        statusTable.setWidth("100%");
        return statusTable;
    }

    private Table createSubQueryBaseRequestMessagesTable() {
        Table statusTable = new Table(StaticPropertiesService.getInstant().getMessage("SubTab.queryBaseReceivedMessageTable.Title"));
        String[] headings = {
                StaticPropertiesService.getInstant().getMessage("SMSTab.ReceivedMessageTable.TimeColumn"),
                StaticPropertiesService.getInstant().getMessage("SubTab.ReceivedMessageTable.addId"),
                StaticPropertiesService.getInstant().getMessage("SubTab.SubQueryBase.size")
        };
        for (String heading : headings) {
            statusTable.addContainerProperty(heading, String.class, null);
        }
        statusTable.setColumnExpandRatio(headings[0], 0.20f);
        statusTable.setColumnExpandRatio(headings[1], 0.30f);
        statusTable.setColumnExpandRatio(headings[2], 0.50f);

        statusTable.setHeight("100%");
        statusTable.setWidth("100%");
        return statusTable;
    }

    private Button createClearStatusMessagesButton() {

        Button clearButton = new Button(StaticPropertiesService.getInstant().getMessage("SMSTab.sentMessageTable.clearButtonText"), new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent clickEvent) {
                subStatusRequestMessagesTable.removeAllItems();
                SubscriptionStatusMessageReceiver.receivedStatusMessages.clear();
            }
        });
        return clearButton;
    }

    private Button createClearReceivedMessagesButton() {
        Button clearButton = new Button(StaticPropertiesService.getInstant().getMessage("SMSTab.ReceivedMessageTable.clearButtonText"), new Button.ClickListener() {

            public void buttonClick(com.vaadin.ui.Button.ClickEvent clickEvent) {
                // clear the message table
                subRequestMessagesTable.removeAllItems();
                SubscriptionMessageReceiver.receivedMessages.clear();
            }
        });
        return clearButton;
    }

    private Button createClearQueryBaseClearButton() {

        Button clearButton = new Button(StaticPropertiesService.getInstant().getMessage("SMSTab.sentMessageTable.clearButtonText"), new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent clickEvent) {

                subQueryBaseRequestMessagesTable.removeAllItems();
                SubscriptionQueryBaseReceiver.clearSubQueryBaseMessageList();
            }
        });
        return clearButton;
    }
}