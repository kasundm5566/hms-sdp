/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.rest;

import hms.kite.simulator.MessageReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;

@Produces({"application/json"})
@Consumes({"application/json"})
@Path("/ussd")
public class UssdNcsRestService {
    	private static final Logger LOGGER = LoggerFactory.getLogger(UssdNcsRestService.class);
	private MessageReceiver ussdAoMessageReceiver;

    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    @Path("/send")
    public Map<String, Object> sendService(Map<String, Object> msg) {
        Map<String, Object> response = ussdAoMessageReceiver.onMessage(msg);
        LOGGER.info("Received USSD message to send to customer[{}], sent response back to application [{}]", msg, response);
    	return response;
    }

	public void setUssdAoMessageReceiver(MessageReceiver ussdAoMessageReceiver) {
		this.ussdAoMessageReceiver = ussdAoMessageReceiver;
	}
}