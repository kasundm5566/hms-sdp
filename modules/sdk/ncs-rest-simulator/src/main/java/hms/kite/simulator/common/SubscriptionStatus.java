/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.common;

public class SubscriptionStatus {

    public static final String REGISTERED = "REGISTERED";
    public static final String UNREGISTERED = "UNREGISTERED";
    public static final String INITIAL = "INITIAL";
    public static final String REG_PENDING = "REGISTER_PENDING";
    public static final String TEMPORARY_BLOCKED = "TEMPORARY_BLOCKED";
    public static final String BLOCKED = "BLOCKED";
}
    