/*
 *   (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.common;


public class ChargingStatus {

    public static final String SUCCESS = "S1000:Request Success";
    public static final String CHARGING_SUCCESS = "S1000:Charging Success";
    public static final String PENDING = "P1003:Payment Pending";
    public static final String REQUEST_FAILED = "E1404:Request Failed";
    public static final String INVALID_REQUEST = "E1312:Invalid Request";
    public static final String PARTIAL_OVER = "P1004:Partial/Over Success";
    public static final String NOT_ALLOWED = "E1401:Not Allowed";
    public static final String REQUEST_TIMEOUT_NO_PAYMENTS = "E1405:Timeout.No Payments";
    public static final String REQUEST_TIMEOUT_PAYMENTS_DONE = "P1005:Timeout.Payments Done";

}
