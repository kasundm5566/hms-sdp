/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.simulator.webui;

import com.vaadin.Application;
import com.vaadin.terminal.gwt.server.AbstractApplicationServlet;
import hms.kite.simulator.ServiceLocator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

public class MchoiceSdpSdkServlet extends AbstractApplicationServlet {

    private static ApplicationContext appContext;

    private static final Logger logger = LoggerFactory.getLogger(MchoiceSdpSdkServlet.class);

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        appContext = WebApplicationContextUtils.getWebApplicationContext(servletConfig.getServletContext());
        ServiceLocator.setApplicationContext(appContext);

    }

    @Override
    protected Application getNewApplication(HttpServletRequest httpServletRequest) throws ServletException {
        logger.debug("web application context " + WebApplicationContextUtils.getWebApplicationContext(getServletContext()));
        return new SimulatorApplication();
    }

    @Override
    protected Class<? extends Application> getApplicationClass() throws ClassNotFoundException {
        return SimulatorApplication.class;
    }

}
