import hms.common.rest.util.JsonBodyProvider;
import hms.common.rest.util.Message;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.apache.cxf.jaxrs.client.WebClient;

/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public class TestReqSender {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Map<String, Object> req = new HashMap<String, Object>();
		req.put("applicationId","App123");
		req.put("password","12345677");
		req.put("version","1.0");
		req.put("address",createAddressList("tel: +94232323232"));
		req.put("message","Test SMS 123");
		req.put("sourceAddress", "7788");

		TestReqSender reqSender = new TestReqSender();
		reqSender.sendMessage(req, "App123");

	}

	private void sendMessage(Map<String, Object> requestMessage, String appId) {
		try {
			WebClient webClient =createWebClient();
			javax.ws.rs.core.Response response = webClient.post(requestMessage);
			if (response.getStatus() == 200) {
				System.out.println("Message Successfully processed for appId[" + appId + "]");
			} else {
				System.out.println("Message Possessing Error appId[" + appId + "]");
			}
		} catch (Throwable e) {
			System.out.println("Error while sending message to SDP. AppId[" + appId + "]");
			e.printStackTrace();

		}
	}


	 private static List<String> createAddressList(String address) {
	        List<String> addressList = new ArrayList<String>();
	        addressList.add(address);
	        return addressList;
	    }

    private static WebClient createWebClient() {
        List<Object> bodyProviders = new ArrayList<Object>();
        bodyProviders.add(new JsonBodyProvider());
        WebClient webClient = WebClient.create("http://localhost:7000/sms/send", bodyProviders);
        webClient.header("Content-Type", "application/json");
        webClient.accept("application/json");
        return webClient;
    }

}
