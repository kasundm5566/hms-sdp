/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hms.kite.sdk;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public class JettyServerStarter implements Runnable {
	private static final Logger LOGGER = LoggerFactory.getLogger(JettyServerStarter.class);
	private Thread jettyServerThread;
	private int serverPort;
	Server server;

	public void start() {
		jettyServerThread = new Thread(this);
		jettyServerThread.start();
	}

	public void stop() {
		try {
			server.stop();
		} catch (Exception e) {
			LOGGER.warn("Error while stopping SDP SDK Server", e);
		}
	}

	@Override
	public void run() {
		try {
			startServer();
		} catch (Exception e) {
			LOGGER.error("Error while starting SDP SDK Server", e);
		}
	}

	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}

	private void startServer() throws Exception {
		server = new Server(serverPort);
		WebAppContext webapp = new WebAppContext();
		webapp.setContextPath("/");
		webapp.setWar("lib/mchoice-sdp-sdk.war");
		server.setHandler(webapp);
		LOGGER.info("Starting mChoice SDP SDK Server on Port[{}]", serverPort);
		server.start();
		server.join();
	}

}
