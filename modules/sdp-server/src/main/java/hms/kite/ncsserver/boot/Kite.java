/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.ncsserver.boot;

import hms.commons.SnmpLogUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.Properties;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class Kite {

    public static void main(String[] args) throws IOException {

        final ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("integration-context.xml");

        final Properties properties = (Properties) context.getBean("snmpMessages");
        SnmpLogUtil.log((String) properties.get("system.sdp.start.snmp.message"));
        context.start();
        context.registerShutdownHook();

        System.out.println("#############################################################");
        System.out.println("##         mChoice Kite System Started Successfully        ##");
        System.out.println("#############################################################");

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                System.out.println("#############################################################");
                System.out.println("##         mChoice Kite System Stopped Successfully        ##");
                System.out.println("#############################################################");
                SnmpLogUtil.log((String) properties.get("system.sdp.stop.snmp.message"));
            }
        });
    }
}
