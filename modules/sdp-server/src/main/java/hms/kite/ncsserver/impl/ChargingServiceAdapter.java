/**
 *   (C) Copyright 2011-2012 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.ncsserver.impl;

import hms.kite.util.KiteKeyBox;
import hms.kite.wf.api.Service;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;


/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ChargingServiceAdapter implements Channel {
    private static final Logger logger = LoggerFactory.getLogger(ChargingServiceAdapter.class);

    private Service chargingService;

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> resp = chargingService.process(requestContext);
        logger.trace("Received charging response [{}] for request context [{}]", resp, requestContext);
        return resp;
    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        return null;
    }

    public void setChargingService(Service chargingService) {
        this.chargingService = chargingService;
    }
}
