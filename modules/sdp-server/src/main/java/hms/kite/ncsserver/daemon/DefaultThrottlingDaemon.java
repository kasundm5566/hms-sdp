package hms.kite.ncsserver.daemon;

import hms.kite.datarepo.ThrottlingRepositoryService;
import hms.kite.util.ThrottlingType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * User: azeem
 * Date: 3/12/12
 * Time: 11:13 AM
 */
public class DefaultThrottlingDaemon implements ThrottlingDaemon {

    private ThrottlingRepositoryService throttlingRepositoryService;
    private ThrottlingType throttlingType;

    @Override
    public void execute(String type) {
        setThrottlingType(type);
        resetTpd();
    }

    private void resetTpd() {
        throttlingRepositoryService.resetThrottling(throttlingType);
    }

    public void setThrottlingRepositoryService(ThrottlingRepositoryService throttlingRepositoryService) {
        this.throttlingRepositoryService = throttlingRepositoryService;
    }

    public void setThrottlingType(String type) {
        this.throttlingType = ThrottlingType.valueOf(type);
    }
}
