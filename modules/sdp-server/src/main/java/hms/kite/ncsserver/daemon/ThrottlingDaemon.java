package hms.kite.ncsserver.daemon;

/**
 * User: azeem
 * Date: 3/12/12
 * Time: 11:06 AM
 */
public interface ThrottlingDaemon {

    void execute(String throttlingType);

}
