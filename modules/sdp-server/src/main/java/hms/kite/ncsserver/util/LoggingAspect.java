package hms.kite.ncsserver.util;

import hms.kite.util.KiteKeyBox;

/**
 *   (C) Copyright 2011-2012 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

import hms.kite.util.logging.service.*;
import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

public class LoggingAspect {

    private static Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

    @Autowired
    private SmsTransLogService smsTransLogService;
    @Autowired
    private UssdTransLogService ussdTransLogService;
    @Autowired
    private CasTransLogService casTransLogService;
    @Autowired
    private SubscriptionTransLogService subscriptionTransLogService;
    @Autowired
    private WapPushTransLogService wapPushTransLogService;
    @Autowired
    private LbsTranslogService lbsTranslogService;
    @Autowired
    private VodafoneApisTranslogService vodafoneApisTranslogService;

    public void log(ProceedingJoinPoint pj, Map<String, Map<String, Object>> requestContext) throws Throwable {
        try {
            pj.proceed();
            log(requestContext);
        } catch (Exception e) {
            logger.error("Unexpected error occurred while trying to log a trans log", e);
        }
    }

    private void log(Map<String, Map<String, Object>> requestContext) {

        String ncsType = (String) requestContext.get(requestK).get(ncsTypeK);

        AbstractTransLogService transLogService = null;
        logger.debug("Looking for the corresponding trans log service for ncs type = [{}]", ncsType);
        // Trans Log service according to NCS
        if (KiteKeyBox.smsK.equals(ncsType) && isMessageForOneRecipient(requestContext)) {
            transLogService = smsTransLogService;
        } else if (KiteKeyBox.casK.equals(ncsType)) {
            transLogService = casTransLogService;
        } else if (KiteKeyBox.subscriptionK.equals(ncsType)) {
            transLogService = subscriptionTransLogService;
        } else if (KiteKeyBox.wapPushK.equals(ncsType)) {
            transLogService = wapPushTransLogService;
        } else if (KiteKeyBox.ussdK.equals(ncsType)) {
            transLogService = ussdTransLogService;
        } else if (KiteKeyBox.lbsK.equals(ncsType)) {
            transLogService = lbsTranslogService;
        } else if (KiteKeyBox.vdfApiK.endsWith(ncsType)) {
            transLogService = vodafoneApisTranslogService;
        } else {
            logger.warn("No trans log service is configured for ncs[{}]", ncsType);
        }

        if(transLogService != null) {
            transLogService.serve(requestContext);
        }
    }

    private boolean isMessageForOneRecipient(Map<String, Map<String, Object>> requestContext) {
        if(requestContext.containsKey(requestK)) {
            Map<String, Object> request = requestContext.get(requestK);
            if(request.containsKey(recipientsK) && (request.get(recipientsK) != null)) {
                ArrayList recipients = (ArrayList) request.get(recipientsK);
                if(recipients.size() > 0) {
                    Map firstRecipient = (Map)recipients.get(0);
                    if(firstRecipient.containsKey(recipientAddressK) &&
                            (firstRecipient.get(recipientAddressK).toString().equals("all"))) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}