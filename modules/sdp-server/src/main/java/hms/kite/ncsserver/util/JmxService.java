package hms.kite.ncsserver.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

import java.util.HashMap;
import java.util.Map;

/**
 * (C) Copyright 2011-2012 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 * <p/>
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 * <p/>
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
@ManagedResource(objectName = "sdp:type=sdp,name=sdp_jmx_service", description = "SDP JMX SERVICE")
public class JmxService {

    private static final Logger logger = LoggerFactory.getLogger(JmxService.class);

    @ManagedOperation(description = "Get server statistics")
    public Map<String, Object> getServerStatistics(){
        logger.info("SDP server statistic request received");
        HashMap<String, Object> response = new HashMap<String, Object>();
        response.put("avg_tps", "100");
        return response;
    }
}
