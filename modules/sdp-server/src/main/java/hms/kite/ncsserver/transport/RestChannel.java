/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.ncsserver.transport;

import hms.common.rest.util.JsonBodyProvider;
import hms.common.rest.util.Message;
import hms.kite.wfengine.transport.BaseChannel;
import org.apache.cxf.jaxrs.client.WebClient;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class RestChannel extends BaseChannel {
    private WebClient webClient = null;

    public RestChannel(String uri) {
        List<Object> providers = new ArrayList<Object>();
        providers.add(new JsonBodyProvider());
        
        webClient = WebClient.create(uri, providers);
        webClient.header("Content-Type", "application/json");
        webClient.accept("application/json");
    }

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        Response response = webClient.post(requestContext.get(requestK));
        return (Map<String, Object>) response.getEntity();
    }

    public Map<String, Object> send(Message request) {
        Response response = webClient.post(request.getParameters());
        return (Map<String, Object>) response.getEntity();
    }
}
