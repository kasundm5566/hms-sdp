/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.ncsserver.workflows;

import hms.common.rest.util.Message;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.RegExpression;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import org.junit.Ignore;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class HostnameVerificationWorkflowTest {
    @Ignore
//    @Test
    public void testHostnameVerificationAsService() {
        //"host", "(172.16.9.25)|(172.16.9.26)|(172.16.0.126)|"
        RegExpression hostMatcher = new RegExpression(requestK, remotehostK, appK, remotehostK);
        Condition hostNameVerification = new Condition(hostMatcher, "ERR_HNV_600");
        ServiceImpl hostVerifiService = new ServiceImpl(hostNameVerification);


        WorkflowImpl workflow = new WorkflowImpl(hostVerifiService);
        List<Message> requests = new ArrayList<Message>();
        requests.add(new Message());
        requests.add(new Message(new HashMap<String, Object>()));
        requests.add(new Message(new HashMap<String, Object>() {{
            put("host", null);
        }}));
        requests.add(new Message(new HashMap<String, Object>() {{
            put("hostname", null);
        }}));
        requests.add(new Message(new HashMap<String, Object>() {{
            put("host", "127.0.0.1");
        }}));
        requests.add(new Message(new HashMap<String, Object>() {{
            put("host", "172.16.9.255");
        }}));

        for (Message request : requests) {
            Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String, Object>>();
            requestContext.put(requestK, request.getParams());
            workflow.executeWorkflow(requestContext);
            Map<String, Object> response = ResponseBuilder.generateResponse(request.getParams());
            Assert.assertEquals(response.get("status-code"), "ERR_HNV_600");
            Assert.assertEquals(response.get("status-description"), "Hostname Validation Failed");
        }
        Message request = new Message();
        request.put("host", "172.16.9.25");
        Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String, Object>>();
        requestContext.put(requestK, request.getParams());
        workflow.executeWorkflow(requestContext);
        Map<String, Object> response = ResponseBuilder.generateResponse(request.getParams());
        Assert.assertEquals(request.get("status-code"), "SUCC_1000");
        Assert.assertEquals(request.get("status-description"), "SUCCESS");

        request.put("host", "172.16.0.126");
        workflow.executeWorkflow(requestContext);
        response = ResponseBuilder.generateResponse(request.getParams());
        Assert.assertEquals(response.get("status-code"), "SUCC_1000");
        Assert.assertEquals(response.get("status-description"), "SUCCESS");

        request.put("host", "172.16.9.26");
        workflow.executeWorkflow(requestContext);
        response = ResponseBuilder.generateResponse(request.getParams());
        Assert.assertEquals(response.get("status-code"), "SUCC_1000");
        Assert.assertEquals(response.get("status-description"), "SUCCESS");
    }
}