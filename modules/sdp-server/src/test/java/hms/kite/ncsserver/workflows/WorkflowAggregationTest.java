/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.ncsserver.workflows;

import hms.common.rest.util.Message;
import hms.kite.wfengine.control.*;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import org.junit.Ignore;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteErrorBox.successCode;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class WorkflowAggregationTest {

    @Ignore
//    @Test
    public void testAggregationTest() {
        //"host", "(172.16.9.25)|(172.16.9.26)|(172.16.0.126)|"
        RegExpression hostMatcher = new RegExpression(requestK, remotehostK, appK, remotehostK);
        Condition hostNameVerification = new Condition(hostMatcher, "ERR_HNV_600");

        InExpression userMatcher = new InExpression(requestK, "userid", appK, "userid");
        InExpression pwdMatcher = new InExpression(requestK, "password", appK, "password");
        AndExpression authLogic = new AndExpression(userMatcher, pwdMatcher);
        Condition auth = new Condition(authLogic, "ERR_AUT_600");

        ServiceImpl slaService = new ServiceImpl(hostNameVerification, auth);

        WorkflowImpl workflow = new WorkflowImpl(slaService);
        List<Message> requests = new ArrayList<Message>();
        requests.add(new Message());
        requests.add(new Message(new HashMap<String, Object>()));
        requests.add(new Message(new HashMap<String, Object>() {{
            put("remote-host", "127.0.0.1");
            put("userid", "jakesully");
            put("password", "$1$21/JGwNC$2IuT1QBC673gtfAftN.wK1");
        }}));
        requests.add(new Message(new HashMap<String, Object>() {{
            put("remote-host", "172.16.9.26");
            put("userid", "jakessully");
            put("password", "$1$21/JGwNC$2IuT1QBC673gtfAftN.wK1");
        }}));
        requests.add(new Message(new HashMap<String, Object>() {{
            put("remote-host", "172.16.0.126");
            put("userid", "jakessully");
            put("password", "$1$21/JGwNCss$2IuT1QBC673gtfAftN.wK1");
        }}));


        for (Message request : requests) {
            Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String, Object>>();
            requestContext.put(requestK, request.getParams());
            workflow.executeWorkflow(requestContext);
            Map<String, Object> response = ResponseBuilder.generateResponse(request.getParams());
            Assert.assertFalse(Boolean.getBoolean(request.get("status")));
        }
        Message request = new Message();
        request.put("remote-host", "172.16.9.25");
        request.put("userid", "jakesully");
        request.put("password", "$1$21/JGwNC$2IuT1QBC673gtfAftN.wK1");
        Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String, Object>>();
        requestContext.put(requestK, request.getParams());
        workflow.executeWorkflow(requestContext);
        Map<String, Object> response = ResponseBuilder.generateResponse(request.getParams());
        Assert.assertEquals(response.get(statusK), "true");
        Assert.assertEquals(response.get(statusCodeK), successCode);
    }
}

