/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.ncsserver.impl;

import hms.kite.wfengine.control.AndExpression;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.InExpression;
import hms.kite.wfengine.control.NotInExpression;
import hms.kite.wfengine.impl.RemoteServiceImpl;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static hms.kite.util.KiteKeyBox.requestK;


/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SmsMoAtServiceImplTest {

    @BeforeTest
    public void setup() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext(new String[]{"beans-test.xml"});
        WorkflowImpl originalWorkFlow = createNcsTypeMoAtWorkflow();

    }

    @AfterMethod
    public void tearDown() {

    }

    @Test
    public void testFindWorkflows() {
        //todo
//        SmsMoServiceEndpoint smsMoServiceEndpoint = new SmsMoServiceEndpoint();
//        smsMoServiceEndpoint.setWorkflowRepository(workflowMongoDbRepository);
//        Message request = new Message();
//        request.put("operator", "safaricom");
//        request.put("shortcode", "1555");
//        request.put("keyword", "JK");
//        Assert.assertNotNull(smsMoServiceEndpoint.findAppWorkflow(request));
//        Assert.assertNotNull(smsMoServiceEndpoint.findNcsWorkflow(request));
    }

    private WorkflowImpl createAppWorkflow() {
        ServiceImpl authentication = new ServiceImpl("authentication", "ERR_AUT_600");

        NotInExpression blacklist = new NotInExpression(requestK, "sender-address", "77777777", "99999999");
        final Condition blCondtion = new Condition(blacklist, "ERR_BL-600");
        ServiceImpl blackListService = new ServiceImpl(blCondtion);
        authentication.setOnSuccess(blackListService);
        //condition for state
        return new WorkflowImpl(authentication);
    }


    private  WorkflowImpl createNcsTypeMoAtWorkflow() {
        RemoteServiceImpl chargingService = createRemoteChargingService();
        ServiceImpl smsSendingService = createSmsMoAtSendingService();
        smsSendingService.setOnSuccess(chargingService);
        ServiceImpl numberCheckingService = createNumberValidationService();
        numberCheckingService.setOnSuccess(smsSendingService);
        return new WorkflowImpl(numberCheckingService);
    }

    private ServiceImpl createNumberValidationService() {
        InExpression subTypeCondition = new InExpression(requestK, "sender-subtype", new Object[]{"PREPAID", "POSTPAID"});
        InExpression opTypeCondition = new InExpression(requestK, "sender-optype", new Object[]{"M1", "Starhub", "Singtel"});
        InExpression subStatusCondition = new InExpression(requestK, "sender-status", new Object[]{"ACTIVE", "PREACTIVE"});
        AndExpression operatorCondition = new AndExpression(subTypeCondition, opTypeCondition, subStatusCondition);

        Condition numberTypeCondition = new Condition(operatorCondition, "ERR_NUM-COND-601");

        ServiceImpl numService = new ServiceImpl("number-checking", "ERR_NUM_600");
        numService.addCondition(numberTypeCondition);
        return numService;
    }

    private RemoteServiceImpl createRemoteChargingService() {
        String remoteServiceUrl = "uri:http://127.0.0.1:8080/ncs-rest-server/ncs/requestCharging/";
        //This is the remote object
        WorkflowImpl remoteChargingWorkflow = new WorkflowImpl();
        return new RemoteServiceImpl(remoteServiceUrl, remoteChargingWorkflow, "ERR_CHG_600", "Subscriber Number Not Allowed");
    }

    private ServiceImpl createSmsMoAtSendingService() {
        String smsSendingServiceName = "uri:http://127.0.0.1:9070/vinca/receiveSms/";
        ServiceImpl smsSendingService = new ServiceImpl(smsSendingServiceName, "ERR_SMS_600");
        return smsSendingService;
    }
}