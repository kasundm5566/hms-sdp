/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.ncsserver.workflows;

import hms.common.rest.util.Message;
import hms.kite.wfengine.control.AndExpression;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.InExpression;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import org.junit.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteErrorBox.successCode;
import static hms.kite.util.KiteKeyBox.requestK;
import static hms.kite.util.KiteKeyBox.statusCodeK;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class AuthenticationWorkflowTest {
    @Test
    public void testAuthentication() {
        InExpression userMatcher = new InExpression(requestK, "userid", new Object[]{"jakesully"});

        InExpression pwdMatcher = new InExpression(requestK, "password", new Object[]{"$1$21/JGwNC$2IuT1QBC673gtfAftN.wK1"});

        AndExpression authLogic = new AndExpression(userMatcher, pwdMatcher);

        Condition auth = new Condition(authLogic, "ERR_AUT_600");
        ServiceImpl authentication = new ServiceImpl(auth);

        WorkflowImpl workflow = new WorkflowImpl(authentication);
        List<Message> requests = new ArrayList<Message>();
        requests.add(new Message());
        requests.add(new Message(new HashMap<String, Object>()));
        requests.add(new Message(new HashMap<String, Object>() {{
            put("userid", null);
        }}));
        requests.add(new Message(new HashMap<String, Object>() {{
            put("password", null);
        }}));
        requests.add(new Message(new HashMap<String, Object>() {{
            put("userid", "jake");
        }}));
        requests.add(new Message(new HashMap<String, Object>() {{
            put("userid", "jakesully");
        }}));
        requests.add(new Message(new HashMap<String, Object>() {{
            put("password", "$1$21/JGwNC$2IuT1QBC673gtfAftN.wK1");
        }}));
        requests.add(new Message(new HashMap<String, Object>() {{
            put("userid", "jakesully");
            put("password", "dsdsd");
        }}));
        requests.add(new Message(new HashMap<String, Object>() {{
            put("userid", "jake");
            put("password", "$1$21/JGwNC$2IuT1QBC673gtfAftN.wK1");
        }}));

//        for (Message request : requests) {
//            workflow.executeWorkflow(request, context);
//            Assert.assertEquals(request.get("status-code"), "ERR_AUT_600");
//            Assert.assertEquals(request.get("status-description"), "Authentication Failed");
//        }
        Message request = new Message();
        request.put("userid", "jakesully");
        request.put("password", "$1$21/JGwNC$2IuT1QBC673gtfAftN.wK1");
        Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String, Object>>();
        requestContext.put(requestK, request.getParams());

        workflow.executeWorkflow(requestContext);
        Map<String, Object> response = ResponseBuilder.generateResponse(requestContext);
        Assert.assertEquals(response.get(statusCodeK), successCode);
    }
}
