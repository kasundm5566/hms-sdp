/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.ncsserver.workflows;

import hms.common.rest.util.Message;
import hms.kite.datarepo.mongodb.BlacklistMongoDbRepository;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import org.junit.Ignore;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.requestK;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class BlacklistWorkflowTest {
    private BlacklistMongoDbRepository blacklistMongoDbRepository;

    @BeforeTest
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext(new String[]{"beans-test.xml"});
        blacklistMongoDbRepository = (BlacklistMongoDbRepository) ctx.getBean("blacklistMongoDbRepository");
        String bl1 = "(11\\d\\d\\d\\d\\d\\d\\d\\d\\d\\d)|" + "(131\\d\\d\\d\\d\\d\\d\\d\\d\\d)|" + "(132\\d\\d\\d\\d\\d\\d\\d\\d\\d)|";
        blacklistMongoDbRepository.createBlacklist("jakesully", bl1);
    }

    @AfterTest
    public void tearDown() {
        blacklistMongoDbRepository.removeAllBlacklists();
    }

    @Ignore
//    @Test
    public void testBlacklist() {
        ServiceImpl blackListService = new ServiceImpl("black-list");


        WorkflowImpl workflow = new WorkflowImpl(blackListService);
        List<Message> requests = new ArrayList<Message>();
        requests.add(new Message());
        requests.add(new Message(new HashMap<String, Object>()));
        requests.add(new Message(new HashMap<String, Object>() {{
            put("sender-address", "118798675645");
        }}));

        requests.add(new Message(new HashMap<String, Object>() {{
            put("recipient-address", "12378674932");
        }}));

        requests.add(new Message(new HashMap<String, Object>() {{
            put("sender-address", "118798675645");
            put("recipient-address", "12378674932");
        }}));

        for (Message request : requests) {
            Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String, Object>>();
            requestContext.put(requestK, request.getParams());
            workflow.executeWorkflow(requestContext);
            Assert.assertEquals(request.get("status-code"), "ERR_BL_600");
            Assert.assertEquals(request.get("status-description"), "Number is black listed");
        }
        Message request = new Message();
        request.put("sender-address", "98006129");
        request.put("recipient-address", "98006122");
        Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String, Object>>();
        requestContext.put(requestK, request.getParams());
        workflow.executeWorkflow(requestContext);
        Assert.assertEquals(request.get("status-code"), "SUCC_1000");
        Assert.assertEquals(request.get("status-description"), "SUCCESS");
    }
}
