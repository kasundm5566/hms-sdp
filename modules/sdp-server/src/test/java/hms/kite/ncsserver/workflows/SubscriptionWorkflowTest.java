/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.ncsserver.workflows;

import hms.common.rest.util.Message;
import hms.kite.util.SdpException;
import hms.kite.wfengine.control.*;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.transport.Channel;
import org.hamcrest.Matcher;
import org.hamcrest.core.AnyOf;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Ignore;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.ncsserver.PropertyValueMatcher.valueOfType;
import static hms.kite.util.KiteErrorBox.successCode;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.wfengine.impl.ResponseBuilder.generate;
import static org.hamcrest.Matchers.hasEntry;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

/*TODO
* 1) How do the MT charging work?
* 2) How do the number checking work?
* 3) How do the ncs choice for MT work?
* 4) Which are the charging types supported?
*
* */
public class SubscriptionWorkflowTest {
    private Mockery mock = new Mockery();

    @BeforeTest
    public void setUp() {

        final Channel numberCheckingChannel = mock.mock(Channel.class, "number-checking");
        final Channel subscriptionChannel = mock.mock(Channel.class, "subscription");
        final Channel successChannel = mock.mock(Channel.class, "success");

        final Map<String, Object> successRes = generate("SUCC_1000", "success", true);
        Map<String, String> parameters = new HashMap<String, String>();
        successRes.putAll(parameters);

        final Map<String, Object> subSuccessRes = generate("SUCC_1000",  "success", true);
        subSuccessRes.put("subscribed", "true");

        final Map<String, Object> subFailRes = generate("SUCC_1000", "success", true);
        subFailRes.put("subscribed", "false");

        Expectations expectations = new Expectations() {
            {
                Map<String, Object> message = generate("SUCC_1000", "success", true);
                message.put("sender-optype", "SAFARI_CON");
                message.put("sender-subtype", "PREPAID");
                message.put("sender-status", "ACTIVE");

                Matcher<Map<String, ?>> mapMatcher = AnyOf.<Map<String, ?>>anyOf((hasEntry("sender-address", "90909090")));
                allowing(numberCheckingChannel).send(with(valueOfType(Map.class).withProperty("parameters", mapMatcher)));
                will(throwException(new SdpException("Number Checking Failed", "ERR_NUM_788")));

                allowing(numberCheckingChannel).send(with(valueOfType(Map.class)));
                will(returnValue(message));

                allowing(subscriptionChannel).send(with(valueOfType(Map.class).withProperty("parameters", AnyOf.<Map<String, ?>>anyOf((hasEntry("sender-address", "77777777"))))));
                will(returnValue(subFailRes));

                allowing(subscriptionChannel).send(with(valueOfType(Map.class)));
                will(returnValue(subSuccessRes));

                allowing(successChannel).send(with(valueOfType(Map.class)));
                will(returnValue(successRes));

            }

        };
        mock.checking(expectations);
    }

     /**/

   @Ignore
//    @Test
    public void testMtSubscriptionTest() {
        WorkflowImpl workflow = createSubscriptionMtFlow();
        Message request = new Message();
        request.put("sender-address", "77777777");

        Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String, Object>>();
        requestContext.put(requestK, request.getParams());

        workflow.executeWorkflow(requestContext);
        Map<String, Object> response = ResponseBuilder.generateResponse(request.getParams());
        Assert.assertEquals(request.get("status"), "false");
        Assert.assertEquals(request.get("status-code"), "ERR_SUB_601");
        Assert.assertEquals(request.get("status-description"), "The Number Is Not Subscribed");

        request = new Message();
        request.put("sender-address", "87878787878");
        workflow.executeWorkflow(requestContext);
        response = ResponseBuilder.generateResponse(request.getParams());
        Assert.assertEquals(request.get(statusK), "true");
        Assert.assertEquals(request.get(statusCodeK), successCode);
    }

    private static WorkflowImpl createSubscriptionMtFlow() {
        ServiceImpl stateValidationService = createStateValidationRule("PRODUCTION");
        ServiceImpl numberChecking = createNumberValidationSla();
        ServiceImpl subService = createSubSlaService();
        ServiceImpl chargingSlaService = createChargingSlaService(true, "Flat", 2.0, 0.0, 0.0);
        ServiceImpl creditReservation = createCreditReservationChargingService();
        ServiceImpl commitReservation = createCreditCommitChargingService();
        ServiceImpl cancleReservation = createCreditCancelChargingService();
        ServiceImpl smsSendingService = createSmsService("888888", false, false, "");

        stateValidationService.setOnSuccess(numberChecking);
        numberChecking.setOnSuccess(subService);
        subService.setOnSuccess(chargingSlaService);
        chargingSlaService.setOnSuccess(creditReservation);
        creditReservation.setOnSuccess(smsSendingService);
        smsSendingService.setOnSuccess(commitReservation);
        smsSendingService.onDefaultError(cancleReservation);

        return new WorkflowImpl(stateValidationService);
    }

    /**
     * This is for reg unreg service
     */
    private static WorkflowImpl createSubscriptionRegUnregMoFlow(String status) {
        ServiceImpl stateValidationService = createStateValidationRule(status);
        ServiceImpl numberChecking = createNumberValidationSla();
        ServiceImpl chargingSlaService = createChargingSlaService(true, "Flat", 2.0, 0.0, 0.0);
        ServiceImpl creditReservation = createCreditReservationChargingService();
        ServiceImpl subService = createRegUnRegSubSlaService();
        ServiceImpl commitReservation = createCreditCommitChargingService();
        ServiceImpl cancleReservation = createCreditCancelChargingService();
        ServiceImpl smsSendingService = createSmsService("888888", false, false, "");

        stateValidationService.setOnSuccess(numberChecking);
        numberChecking.setOnSuccess(chargingSlaService);
        chargingSlaService.setOnSuccess(creditReservation);
        creditReservation.setOnSuccess(subService);
        subService.setOnSuccess(commitReservation);
        subService.onDefaultError(cancleReservation);
        commitReservation.setOnSuccess(smsSendingService);
        cancleReservation.setOnSuccess(smsSendingService);

        return new WorkflowImpl(stateValidationService);
    }

    private static ServiceImpl createStateValidationRule(String status) {
        InExpression statusCond = new InExpression(requestK, "ncs-status", new Object[]{"LIMITED_PRODUCTION", "PRODUCTION"});
        Condition condition = new Condition(statusCond, "ERR_STAT_100");
        ServiceImpl wlService = new ServiceImpl(condition);
        return wlService;
    }

    private static ServiceImpl createNumberValidationSla() {
        InExpression subTypeCondition = new InExpression(requestK, "sender-subtype", new Object[]{"PREPAID", "POSTPAID"});
        InExpression opTypeCondition = new InExpression(requestK, "sender-optype", new Object[]{"SAFARI_CON"});
        InExpression subStatusCondition = new InExpression(requestK, "sender-status", new Object[]{"ACTIVE", "PREACTIVE"});
        AndExpression operatorCondition = new AndExpression(subTypeCondition, opTypeCondition, subStatusCondition);
        Condition numberTypeCondition = new Condition(operatorCondition, "ERR_NUM-COND-601");

        ServiceImpl numService = new ServiceImpl("number-checking", "ERR_NUM-COND-602");
        numService.addCondition(numberTypeCondition);
        return numService;
    }

    private static ServiceImpl createChargingSlaService(boolean allowed, String chargingType, double flatAmount, double min, double max) {
        final ServiceImpl chargingSlaService = new ServiceImpl();
        chargingSlaService.addCondition(new Condition(new InExpression(requestK, "allowed", new Object[]{"true"}), "ERR_CHG_RES_601"));

        if ("Free".equalsIgnoreCase(chargingType)) {
//            addFreeChargingCondition(chargingSlaService);
        } else if ("Flat".equalsIgnoreCase(chargingType)) {
//            addFlatChargingCondition(chargingSlaService, flatAmount);
        } else if ("Variable".equalsIgnoreCase(chargingType)) {
            addVariableChargingCondition(chargingSlaService, min, max);
        }
        return chargingSlaService;
    }

    private static void addVariableChargingCondition(ServiceImpl chargingSlaService, double min, double max) {
        GreaterThanExpression minCondition = new GreaterThanExpression(requestK, "amount", appK, "charging-value");
        LessThanExpression maxCondition = new LessThanExpression(requestK, "amount", appK, "charging-value");
        final AndExpression andLogic = new AndExpression(minCondition, maxCondition);

        final Condition varCondition = new Condition(andLogic, "CHG_ERR_800");
        chargingSlaService.addCondition(varCondition);
    }

    private static ServiceImpl createCreditReservationChargingService() {
        return new ServiceImpl("reserve-credit", "ERR_CHG_RES_600");
    }

    private static ServiceImpl createCreditCommitChargingService() {
        return new ServiceImpl("commit-reservation", "ERR_CHG_RES_600");
    }

    private static ServiceImpl createCreditCancelChargingService() {
        return new ServiceImpl("cancel-reservation", "ERR_CHG_COM_601");
    }

    private static ServiceImpl createSubSlaService() {
        InExpression isSubscripted = new InExpression(requestK, "subscribed", new Object[]{"true"});

        Condition subscription = new Condition(isSubscripted, "ERR_SUB_601");
        final ServiceImpl service = new ServiceImpl("subscription");
        service.addCondition(subscription);
        return service;
    }

    private static ServiceImpl createRegUnRegSubSlaService() {
        final ServiceImpl service = new ServiceImpl("subscription");
        return service;
    }

    private static ServiceImpl createSmsService(String defaultSender, boolean aliasing, boolean isDrRequired, String drUrl) {
        ServiceImpl smsSendingService = new ServiceImpl("sms-mt-channel", "ERR_SMS_600");
        return smsSendingService;
    }

}