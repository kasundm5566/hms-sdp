/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.ncsserver;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.apache.commons.beanutils.PropertyUtils;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;


import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class PropertyValueMatcher<T> extends BaseMatcher<T> {

    private final Class<T> valueClass;
    private final HashMap<String, Matcher<?>> propertyValues;

    public PropertyValueMatcher(Class<T> valueClass) {
        this.valueClass = valueClass;
        propertyValues = new HashMap<String, Matcher<?>>();
    }

    public PropertyValueMatcher(Class<T> valueClass, HashMap<String, Matcher<?>> propertyValues) {
        this.valueClass = valueClass;
        if (propertyValues == null) {
            propertyValues = new HashMap<String, Matcher<?>>();
        }
        this.propertyValues = propertyValues;
    }

    /**
     * Creates a matcher which will check that the value is an instance of the
     * specified class.
     *
     * @param <T>
     * @param valueClass
     * @return matcher, to which one can add validation criteria using the
     *         {@link #withProperty(String, Matcher)}
     */
    public static <T> PropertyValueMatcher<T> valueOfType(Class<T> valueClass) {
        PropertyValueMatcher<T> matcher = new PropertyValueMatcher<T>(valueClass);
        return matcher;
    }

    /**
     * Creates a matcher which will validate target object's property values
     * using the {@link Matchers#equalTo(Object)} matcher and using the sample
     * object as the source for correct values.
     *
     * @param <T>
     * @param sample
     * @return matcher which will validate target object's properties based on
     *         the sample object properties
     */
    @SuppressWarnings("unchecked")
    public static <T> Matcher<T> propertiesEqualTo(T sample) {
        PropertyValueMatcher<T> matcher = new PropertyValueMatcher<T>((Class<T>) sample.getClass());
        try {
            Map<String, Object> describe = PropertyUtils.describe(sample);
            Set<Map.Entry<String, Object>> entrySet = describe.entrySet();
            for (Map.Entry<String, Object> entry : entrySet) {
                Matcher<Object> equalTo = Matchers.equalTo(entry.getValue());
                matcher.withProperty(entry.getKey(), equalTo);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return matcher;
    }

    /**
     * Adds a property/value pair to validate against the target object.
     *
     * @param property
     * @param value
     * @return this
     */
    public PropertyValueMatcher<T> withProperty(String property, Matcher<?> value) {
        propertyValues.put(property, value);
        return this;
    }

    public boolean matches(Object obj) {
        boolean valid = obj.getClass().isAssignableFrom(valueClass);
        if (valid) {
            Set<Map.Entry<String, Matcher<?>>> entrySet = propertyValues.entrySet();
            for (Map.Entry<String, Matcher<?>> entry : entrySet) {
                String key = entry.getKey();
                try {
                    Object propertyValue = PropertyUtils.getProperty(obj, key);
                    if (!entry.getValue().matches(propertyValue)) {
                        valid = false;
                        break;
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        }

        return valid;
    }

    public void describeTo(Description desc) {
        desc.appendText("object of type " + valueClass + " with the following field values: " + propertyValues);
    }

}
