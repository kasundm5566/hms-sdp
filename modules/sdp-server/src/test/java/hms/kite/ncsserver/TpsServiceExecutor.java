package hms.kite.ncsserver;

import hms.common.rest.util.JsonBodyProvider;
import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.jaxrs.client.WebClient;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * User: azeem
 * Date: 3/13/12
 * Time: 8:13 PM
 */
public class TpsServiceExecutor implements Runnable {

    public static final String scriptLocation = "/home/azeem/projects/sdp/modules/sdp-server/src/test/resources/sms-mt.sh";
    public static final String subScriptLocation = "/home/azeem/projects/sdp/modules/sdp-server/src/test/resources/subscription.sh";

    private int count = 100;

    private String createRegCommand(int msisdnSuffix) {
        return "curl -v -H \"Content-Type: application/json\" -X POST -d\" '{\"applicationId\":\"APP_000087\",\"password\":\"cb2ece5d55e9d6f6d12dbea640350c8a\",\"subscriberId\":\"tel:94770052" + msisdnSuffix + "\",\"action\":1}' http://core.sdp:7000/subscription/reg";
    }

    private void doPost() {
        WebClient webClient = createWebClient();
        Map post = webClient.post(createRequest(100), Map.class);
        System.out.println("Response : " + post.toString());
    }

    private void executeShell() throws IOException {
        Process process = Runtime.getRuntime().exec(scriptLocation);
        InputStream inputStream = process.getInputStream();
        System.out.println("Result: " + IOUtils.toString(inputStream, "UTF-8"));
    }

    private void executeReg() throws IOException {
        Process process = Runtime.getRuntime().exec("wget www.google.lk");
        InputStream inputStream = process.getInputStream();
        System.out.println(IOUtils.toString(inputStream, "UTF-8"));
    }

    private WebClient createWebClient() {
        List<Object> bodyProviders = new LinkedList<Object>();
        bodyProviders.add(new JsonBodyProvider());
        WebClient webClient = WebClient.create("http://core.sdp:7000/sms/send", bodyProviders);
        webClient.header("Content-Type", "application/json");
        webClient.accept("application/json");
        return webClient;
    }

    private Map<String, Object> createRequest(int id) {
        Map<String, Object> req = new HashMap<String, Object>();
        req.put("applicationId","APP_000087");
        req.put("password","cb2ece5d55e9d6f6d12dbea640350c8a");
        req.put("destinationAddresses", Arrays.asList("tel:94774545" + id));
        req.put("message","Message id: " + id);
        return req;
    }

    @Override
    public void run() {
        try {
            executeShell();
            Thread.yield();
        } catch (IOException e) {
            System.out.println("Exception Occurred" + e.getMessage());
        }
    }

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        for (int i = 0; i < 150 ; i++) {
            executorService.execute(new TpsServiceExecutor());
        }
    }
}
