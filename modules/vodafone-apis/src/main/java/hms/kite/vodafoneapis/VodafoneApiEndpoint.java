/*
 * (C) Copyright 2010- 2017 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.kite.vodafoneapis;

import hms.commons.IdGenerator;
import hms.kite.util.NblKeyMapper;
import hms.kite.util.SdpException;
import hms.kite.util.SystemUtil;
import hms.kite.wfengine.ServiceEndpoint;
import hms.kite.wfengine.transport.Channel;
import org.apache.log4j.NDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import java.util.HashMap;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.correlationIdK;
import static hms.kite.util.KiteKeyBox.*;

/**
 * Created by sandarenu on 4/28/17.
 */
@Produces({"application/json"})
@Consumes({"application/json"})
public class VodafoneApiEndpoint extends ServiceEndpoint {
    private static final Logger LOGGER = LoggerFactory.getLogger(VodafoneApiEndpoint.class);
    public static final boolean FROM_NBL_PRAM = true;
    public static final boolean TO_NBL_PARAM = false;

    private Channel vodafoneApiWfChannel;
    private Map<String, String> nblKeyMap;
    private NblKeyMapper nblKeyMapper;

    @Context
    private org.apache.cxf.jaxrs.ext.MessageContext mc;
    @Context
    private ServletContext sc;

    public void init() {
        nblKeyMapper = new NblKeyMapper();
        nblKeyMapper.setNewNblApiKeyMapper(nblKeyMap);
        LOGGER.info("VodafoneApi endpoint started.");
    }

    @POST
    @Path("/vodafoneapi/checkmsisdn")
    public Map<String, Object> checkMsisdn(Map<String, Object> msg,
                                           @Context javax.servlet.http.HttpServletRequest request) {
        try {
            msg.put(correlationIdK, IdGenerator.generateId());
            msg.put(requestTypeK, vdfApiCheckMsisdnK);
            NDC.push((String) msg.get(correlationIdK));
            LOGGER.info("CheckMsisdn requrest received [{}]", msg);
            return processRequest(msg, request, vodafoneApiWfChannel);
        } finally {
            NDC.pop();
        }
    }

    @POST
    @Path("/vodafoneapi/sendcharge")
    public Map<String, Object> sendCharge(Map<String, Object> msg,
                                          @Context javax.servlet.http.HttpServletRequest request) {
        try {
            msg.put(correlationIdK, IdGenerator.generateId());
            msg.put(requestTypeK, vdfApiSendChargeK);
            NDC.push((String) msg.get(correlationIdK));
            LOGGER.info("SendCharge requrest received [{}]", msg);
            return processRequest(msg, request, vodafoneApiWfChannel);
        } finally {
            NDC.pop();
        }
    }

    @POST
    @Path("/vodafoneapi/sendsubscription")
    public Map<String, Object> sendSubscription(Map<String, Object> msg,
                                                @Context javax.servlet.http.HttpServletRequest request) {
        try {
            msg.put(correlationIdK, IdGenerator.generateId());
            msg.put(requestTypeK, vdfApiSendSubscriptionK);
            NDC.push((String) msg.get(correlationIdK));
            LOGGER.info("SendSubscription requrest received [{}]", msg);
            return processRequest(msg, request, vodafoneApiWfChannel);
        } finally {
            NDC.pop();
        }
    }

    private Map<String, Object> processRequest(Map<String, Object> msg, HttpServletRequest request,
                                               Channel executionChannel) {
        Map<String, Object> respNbl = null;
        try {

            Map<String, Object> newMsg = nblKeyMapper.convert(msg, FROM_NBL_PRAM);
            LOGGER.debug("Received Converted Request [{}]", newMsg);
            addHostDetails(newMsg, request);
            LOGGER.debug("Received request from host [{}]", newMsg.get(remotehostK));
            Map<String, Object> flowExeResult = executionChannel.send(newMsg);
            LOGGER.info("Response after executing VodafoneApi workflow[{}]", flowExeResult);
            respNbl = nblKeyMapper.convert(flowExeResult, TO_NBL_PARAM);

            LOGGER.debug("Sending Converted response [{}]", respNbl);

        } catch (SdpException ex) {
            LOGGER.error("An exception occurred: [{}]", ex);
            respNbl = new HashMap<String, Object>();
            respNbl.put(NblKeyMapper.statusCode, ex.getErrorCode());
            respNbl.put(NblKeyMapper.statusDetail, ex.getErrorDescription());
        }
        return respNbl;
    }

    private void addHostDetails(Map<String, Object> msg, HttpServletRequest request) {
        if (null == msg.get(remotehostK)) {
            String host = SystemUtil.findHostIp(request);
            if (null != host) {
                msg.put(remotehostK, host);
            }
        }
    }

    public void setNblKeyMap(Map<String, String> nblKeyMap) {
        this.nblKeyMap = nblKeyMap;
    }

    public void setVodafoneApiWfChannel(Channel vodafoneApiWfChannel) {
        this.vodafoneApiWfChannel = vodafoneApiWfChannel;
    }
}
