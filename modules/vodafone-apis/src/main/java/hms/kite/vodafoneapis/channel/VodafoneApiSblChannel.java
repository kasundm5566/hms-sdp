/*
 * (C) Copyright 2010- 2017 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.kite.vodafoneapis.channel;

import hms.common.rest.util.client.AbstractWebClient;
import hms.kite.datarepo.RepositoryServiceRegistry;
import hms.kite.util.KeyNameSpaceResolver;
import hms.kite.util.KiteErrorBox;
import hms.kite.util.SdpException;
import hms.kite.vodafoneapis.channel.sbl.VodafoneApiSblRequestBuilder;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import static hms.kite.util.KiteErrorBox.successCode;
import static hms.kite.util.KiteErrorBox.temporarySystemErrorCode;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.KiteKeyBox.statusDescriptionK;
import static hms.kite.wfengine.impl.ResponseBuilder.generate;

/**
 * Created by sandarenu on 4/28/17.
 */
public class VodafoneApiSblChannel extends AbstractWebClient implements Channel {

    private final static Logger LOGGER = LoggerFactory.getLogger(VodafoneApiSblChannel.class);
    private Map<String, WebClient> vodafoneApiWebClientsMap;
    private VodafoneApiSblRequestBuilder vodafoneApiSblRequestBuilder;

    public void init() {
        this.vodafoneApiWebClientsMap = new HashMap<String, WebClient>();

        super.init();
        Map<String, String> sblUrlMap = (Map<String, String>) RepositoryServiceRegistry.systemConfiguration().find(
                sblVodafoneApiReceiverAddressesK);
        LOGGER.debug("SBL vodafone api url map {}", sblUrlMap);
        if (sblUrlMap == null) {
            throwSblEndpointNotDefinedError();
        } else {
            String endpointUrl = sblUrlMap.get("vodafone");
            if (endpointUrl == null || endpointUrl.trim().isEmpty()) {
                throwSblEndpointNotDefinedError();
            } else {
                this.vodafoneApiWebClientsMap.put(vdfApiCheckMsisdnK, createWebClient(endpointUrl + "/checkmsisdn"));
                this.vodafoneApiWebClientsMap.put(vdfApiSendChargeK, createWebClient(endpointUrl + "/sendcharge"));
                this.vodafoneApiWebClientsMap.put(vdfApiSendSubscriptionK, createWebClient(endpointUrl + "/sendsubscription"));
            }
        }

    }

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        String requestType = (String) KeyNameSpaceResolver.data(requestContext, requestK, requestTypeK);

        WebClient webClient = this.vodafoneApiWebClientsMap.get(requestType);
        if (webClient == null) {
            LOGGER.error("SBL endpoint url is not configured for Vodafone API request [{}]", requestType);
            return ResponseBuilder.generate(KiteErrorBox.ncsNotAvailableErrorCode, String.format("Request type [%s] not supported.", requestType), false);
        } else {
            try {
                Map<String, Object> sblRequest = vodafoneApiSblRequestBuilder.createSblRequest(requestType, requestContext);
                LOGGER.debug("Created SBL request [{}]", sblRequest);
                Response httpResponse = sendRequest(sblRequest, webClient);
                return processResponse(requestContext, httpResponse);
            } catch (SdpException e) {
                throw e;
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                return ResponseBuilder.generate(temporarySystemErrorCode, false, requestContext);
            }
        }
    }


    private Response sendRequest(Map<String, Object> msg, WebClient webClient) {
        try {
            Response response = webClient.post(msg);
            return response;
        } catch (RuntimeException e) {
            LOGGER.error("Couldn't send msg. SBL connection failed", e);
            throw e;
        }
    }

    private Map<String, Object> processResponse(Map<String, Map<String, Object>> requestContext, Response response) throws IOException {
        Map<String, Object> resp = readJsonResponse(response);
        LOGGER.info("Vodafone API response[{}]", resp);
        if (null == resp) {
            return generate(temporarySystemErrorCode, "No response from SBL", false);
        } else if (successCode.equals(resp.get(statusK))) {
            requestContext.put(responseK, resp);
            return ResponseBuilder.generateSuccess();
        } else {
            String sblStatus = (String) resp.get(statusK);
            LOGGER.error("Error response received from SBL [{}]", sblStatus);
            if(sblStatus == null || sblStatus.isEmpty()) {
                return generate(temporarySystemErrorCode, "Error response from SBL", false);
            } else {
                return generate(sblStatus, false, requestContext);
            }
        }
    }

    private Map<String, Object> generateErrorResponse(Map<String, Map<String, Object>> requestContext, Response response) throws IOException {
        Map<String, Object> resp = readJsonResponse(response);
        if (resp != null) {
            String errorCode = temporarySystemErrorCode;
            Map<String, Object> errorResponse = generate(errorCode, false, requestContext);
            errorResponse.put(
                    statusDescriptionK,
                    MessageFormat.format("{0}({1})", errorResponse.get(statusDescriptionK),
                            resp.get(statusDescriptionK)));
            return errorResponse;
        } else {
            return generate(temporarySystemErrorCode, false, requestContext);
        }

    }

    private void throwSblEndpointNotDefinedError() {
        throw new RuntimeException(String.format("Vodafone API SBL connector endpoint[%s] in not defined.", sblVodafoneApiReceiverAddressesK));
    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        return null;
    }

    public void setVodafoneApiSblRequestBuilder(VodafoneApiSblRequestBuilder vodafoneApiSblRequestBuilder) {
        this.vodafoneApiSblRequestBuilder = vodafoneApiSblRequestBuilder;
    }
}
