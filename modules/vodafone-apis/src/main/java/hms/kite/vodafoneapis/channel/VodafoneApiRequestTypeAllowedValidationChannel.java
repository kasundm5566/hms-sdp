/*
 * (C) Copyright 2010- 2017 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.kite.vodafoneapis.channel;

import hms.kite.util.KeyNameSpaceResolver;
import hms.kite.util.KiteErrorBox;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;


/**
 * Created by sandarenu on 4/28/17.
 */
public class VodafoneApiRequestTypeAllowedValidationChannel extends BaseChannel {

    private static final Logger LOGGER = LoggerFactory.getLogger(VodafoneApiRequestTypeAllowedValidationChannel.class);

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        String requestType = (String) KeyNameSpaceResolver.data(requestContext, requestK, requestTypeK);
        LOGGER.debug("Validating request type[{}] is allowed in the NCS", requestType);
        Object flowAllowed = KeyNameSpaceResolver.data(requestContext, ncsK, requestType + "-allowed");
        LOGGER.debug("Flow allowed [{}]", flowAllowed);
        if (Boolean.TRUE.equals(flowAllowed)) {
            LOGGER.info("Request type [{}] is allowed for the app.", requestType);
            return ResponseBuilder.generateSuccess();
        } else {
            return ResponseBuilder.generate(KiteErrorBox.ncsNotAllowedErrorCode, "Request type not allowed", false);
        }
    }
}
