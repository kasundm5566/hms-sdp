/*
 * (C) Copyright 2010- 2017 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.kite.vodafoneapis.channel;

import hms.kite.util.KeyNameSpaceResolver;
import hms.kite.util.KiteErrorBox;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

/**
 * Created by sandarenu on 5/8/17.
 */
public class VodafoneApiSendChargeValidationChannel extends BaseChannel {
    private static final Logger LOGGER = LoggerFactory.getLogger(VodafoneApiSendChargeValidationChannel.class);

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        String requestType = (String) KeyNameSpaceResolver.data(requestContext, requestK, requestTypeK);
        LOGGER.debug("Validating request type[{}] is allowed in the NCS", requestType);

        if (vdfApiSendChargeK.equals(requestType)) {
            LOGGER.debug("Received VDF SendCharge request, validating amounts");
            double minAmount = getDoubleAmount(KeyNameSpaceResolver.data(requestContext, ncsK, vdfApiSendChargeK, sendChargeMinAmountK));
            double maxAmount = getDoubleAmount(KeyNameSpaceResolver.data(requestContext, ncsK, vdfApiSendChargeK, sendChargeMaxAmountK));
            double requestedAmount = Double.parseDouble((String) KeyNameSpaceResolver.data(requestContext, requestK, amountK));
            LOGGER.debug("NCS allowed min[{}] max[{}] requestedAmount[{}]", new Object[]{minAmount, maxAmount, requestedAmount});
            if (requestedAmount > maxAmount) {
                LOGGER.debug("Amount too high");
                return ResponseBuilder.generate(KiteErrorBox.chargingAmountTooHighErrorCode,
                        String.format("Request amount exceeds SLA allowed max amount[%s]", maxAmount), false);
            } else if (requestedAmount < minAmount) {
                LOGGER.debug("Amount too low");
                return ResponseBuilder.generate(KiteErrorBox.chargingAmountTooLowErrorCode,
                        String.format("Request amount is lower than SLA allowed min amount[%s]", minAmount), false);
            } else {
                return ResponseBuilder.generateSuccess();
            }

        } else {
            return ResponseBuilder.generateSuccess();
        }

    }

    private double getDoubleAmount(Object valueObj) {
        if (valueObj == null) {
            return 0;
        } else {
            return (Double) valueObj;
        }
    }
}
