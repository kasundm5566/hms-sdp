/*
 * (C) Copyright 2010- 2017 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.kite.vodafoneapis.channel.sbl;

import hms.kite.util.KeyNameSpaceResolver;
import hms.kite.util.KiteErrorBox;
import hms.kite.util.SdpException;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

/**
 * Created by sandarenu on 5/2/17.
 */
public class SendSubscriptionSblRequestBuilder implements VodafoneApiSblRequestBuilder {
    private VodafoneApiSblRequestBuilder nextBuilder;

    @Override
    public Map<String, Object> createSblRequest(String requestType, Map<String, Map<String, Object>> requestContext) {
        if(vdfApiSendSubscriptionK.equals(requestType)){

            Map<String, Object> sblRequest = new HashMap<String, Object>();
            sblRequest.put("msisdn", KeyNameSpaceResolver.data(requestContext, requestK, msisdnK));
            sblRequest.put("requestId", KeyNameSpaceResolver.data(requestContext, requestK, requestIdK));
            sblRequest.put("subscriptionId", KeyNameSpaceResolver.data(requestContext, requestK, "subscriptionId"));
            sblRequest.put("chargeReason", KeyNameSpaceResolver.data(requestContext, requestK, "chargeReason"));
            return sblRequest;

        } else {
            if(nextBuilder != null){
                return nextBuilder.createSblRequest(requestType, requestContext);
            } else {
                throw new SdpException(KiteErrorBox.ncsNotAvailableErrorCode, String.format("Request type [%s] not supported.", requestType));
            }
        }
    }

    public void setNextBuilder(VodafoneApiSblRequestBuilder nextBuilder) {
        this.nextBuilder = nextBuilder;
    }
}
