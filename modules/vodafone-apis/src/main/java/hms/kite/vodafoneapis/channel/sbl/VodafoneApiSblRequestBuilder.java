/*
 * (C) Copyright 2010- 2017 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.kite.vodafoneapis.channel.sbl;

import java.util.Map;

/**
 * Created by sandarenu on 4/30/17.
 */
public interface VodafoneApiSblRequestBuilder {
    Map<String,Object> createSblRequest(String requestType, Map<String, Map<String,Object>> requestContext);
}
