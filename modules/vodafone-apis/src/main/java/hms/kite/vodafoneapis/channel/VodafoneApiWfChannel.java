/*
 * (C) Copyright 2010- 2017 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.kite.vodafoneapis.channel;

import hms.kite.util.KeyNameSpaceResolver;
import hms.kite.util.SdpException;
import hms.kite.util.SystemUtil;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteErrorBox.successCode;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.logging.StatLogger.printStatLog;

/**
 * Created by sandarenu on 4/28/17.
 */
public class VodafoneApiWfChannel extends BaseChannel {
    private static final Logger LOGGER = LoggerFactory.getLogger(VodafoneApiWfChannel.class);

    private Workflow vodafoneApiWorkflow;
    private List<String> powerOnSubcriberStateList;

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        addMandatoryParams(requestContext);

        createRecipientList(requestContext);

        return executeWorkflow(requestContext);
    }


    private void createRecipientList(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> requestObject = requestContext.get(requestK);
        requestObject.put(recipientsK,
                SystemUtil.convert2NblAddresses(Arrays.asList((String) requestObject.get(msisdnK))));

        String address = (String) requestObject.get(msisdnK);
        if (null != address && address.startsWith(telK) && address.contains(":")) {
            address = address.substring(address.indexOf(":") + 1, address.length()).trim();
            requestObject.put(msisdnK, address);
        }
    }

    private void addMandatoryParams(Map<String, Map<String, Object>> requestContext) {
        requestContext.get(requestK).put(ncsTypeK, vdfApiK);
        requestContext.get(requestK).put(directionK, mtK);
        requestContext.get(requestK).put(receiveTimeK, Long.toString(System.currentTimeMillis()));
    }

    private Map<String, Object> executeWorkflow(Map<String, Map<String, Object>> requestContext) {
        long start = System.currentTimeMillis();
        try {
            vodafoneApiWorkflow.executeWorkflow(requestContext);
            return generateResponseForNbl(requestContext);
        } catch (SdpException e) {
            return convert2NblResponse(ResponseBuilder.generate(e.getErrorCode(), false, requestContext),
                    requestContext);
        } catch (Exception e) {
            LOGGER.error("Exception occurred while executing flow", e);
            return convert2NblResponse(ResponseBuilder.generate(requestContext, e), requestContext);
        } finally {
            printStatLog(start, "VDF-API");
        }
    }

    private Map<String, Object> generateResponseForNbl(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> resp = convert2NblResponse(ResponseBuilder.generateResponse(requestContext), requestContext);
        if (successCode.equals(requestContext.get(requestK).get(statusCodeK))) {
            setVdfApiResponseParameters2NblResp(requestContext, resp);
        }
        return resp;
    }

    private void setVdfApiResponseParameters2NblResp(Map<String, Map<String, Object>> requestContext, Map<String, Object> resp) {
        LOGGER.debug("context =+>> [{}]", requestContext);
        Map<String, Object> resp2SentBack = requestContext.get(responseK);
        resp2SentBack.remove(statusCodeK);
        resp2SentBack.remove(statusDescriptionK);
        resp2SentBack.remove(statusK);
        for (Map.Entry<String, Object> item : resp2SentBack.entrySet()) {
            resp.put(item.getKey(), item.getValue());
        }

    }


    private Map<String, Object> convert2NblResponse(Map<String, Object> response,
                                                    Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> nblResp = ResponseBuilder.convert2NblErrorResponse(response);
        if (requestContext.get(requestK).containsKey(correlationIdK)) {
            nblResp.put(messageIdNblK, requestContext.get(requestK).get(correlationIdK));
        }
        return nblResp;
    }

    public void setVodafoneApiWorkflow(Workflow vodafoneApiWorkflow) {
        this.vodafoneApiWorkflow = vodafoneApiWorkflow;
    }

    public void setPowerOnSubcriberStateList(List<String> powerOnSubcriberStateList) {
        this.powerOnSubcriberStateList = powerOnSubcriberStateList;
    }
}
