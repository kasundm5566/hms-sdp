/*
 * (C) Copyright 2010- 2017 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.kite.vodafoneapis.wf;

import hms.kite.util.KeyNameSpaceResolver;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.*;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;

public class VodafoneApiWorkflow extends WrappedGeneratedWorkflow implements Workflow {
    private final String name = "vdf-api";

    @Autowired
    private Channel provSpSlaChannel;
    @Autowired
    private Channel provAppSlaChannel;
    @Autowired
    private Channel provNcsSlaChannel;
    @Autowired
    private Channel vodafoneApiSblChannel;
    @Autowired
    private Channel numberMaskingChannel;
    @Autowired
    private Channel mnpService;
    @Autowired
    private Channel blacklistingService;
    @Autowired
    private Channel subscriptionCheckChannel;
    @Autowired
    private Channel mainThrottlingChannel;
    @Autowired
    private Channel tpsThrottlingChannel;
    @Autowired
    private Channel tpdThrottlingChannel;
    @Autowired
    private Channel numberStatusValidationService;
    @Autowired
    private Channel whitelistService;
    @Autowired
    private Channel blockedMsisdnValidationService;
    @Autowired
    private Channel vodafoneApiRequestTypeAllowedValidationChannel;
    @Autowired
    private Channel vodafoneApiSendChargeValidationChannel;


    @Override
    public String getName() {
        return name;
    }

    @Override
    public void executeWorkflow(Map<String, Map<String, Object>> requestContext) {
        generateWorkflow().executeWorkflow(requestContext);
    }

    @Override
    protected Workflow generateWorkflow() {

        ServiceImpl appSlaChannel = new ServiceImpl("prov.app.sla.channel").attachChannel(provAppSlaChannel);

        ServiceImpl numberMaskingService = new ServiceImpl("number-masking-channel").attachChannel(numberMaskingChannel).addServiceParameter(unmaskK, true);
        ServiceImpl numberCheckingService = new ServiceImpl("number-checking-service").attachChannel(mnpService);
        ServiceImpl subscriptionCheckService = new ServiceImpl("subscription.check").attachChannel(subscriptionCheckChannel);
        ServiceImpl systemTpsService = new ServiceImpl("main-throttling-channel").attachChannel(mainThrottlingChannel).setServiceParameterKeys("system-tps");
        ServiceImpl numberStatusValidateionService = new ServiceImpl("number-status-validation-channel").attachChannel(numberStatusValidationService);
        ServiceImpl numberWhitelistService = new ServiceImpl("white-list").attachChannel(whitelistService);
        ServiceImpl vdfApiSblService = new ServiceImpl("vdf-api-sbl-channel").attachChannel(vodafoneApiSblChannel);


        appSlaChannel.chain("prov.sp.sla.channel").attachChannel(provSpSlaChannel)
                .chain("password-validation",
                        new Condition(new EqualExpression(requestK, passwordK, appK, passwordK), authenticationFailedErrorCode))
                .chain("sp-status-validation",
                        new Condition(new EqualExpression(spK, statusK, approvedK), spNotAvailableErrorCode))
                .chain("app-status-validation",
                        new Condition(new InExpression(appK, statusK, new Object[]{limitedProductionK, productionK, activeProductionK}), appNotAvailableErrorCode))
                .chainBranch(
                        new Condition(new EqualExpression(appK, maskNumberK, true), unknownErrorCode), numberMaskingService, numberCheckingService)
                .chain("blacklist-validation").attachChannel(blacklistingService)
                .chain("ncs-sla-validation").attachChannel(provNcsSlaChannel)
                .chain("request-type-validation").attachChannel(vodafoneApiRequestTypeAllowedValidationChannel)
                .chain("sender.address.validation", createSenderAddressValidationCondition())
                .chainBranch(new Condition(new EqualExpression(ncsK, subscriptionRequiredK, true), unknownErrorCode), subscriptionCheckService, systemTpsService)
                .chain("sp-tpd-throttling-channel").attachChannel(tpdThrottlingChannel).setServiceParameterKeys("sp-vdf-apis")
                .chain("ncs-tpd-throttling-channel").attachChannel(tpdThrottlingChannel).setServiceParameterKeys("ncs-vdf-apis")
                .chain("sp-tps-throttling-channel").attachChannel(tpsThrottlingChannel).setServiceParameterKeys("sp-vdf-apis")
                .chain("ncs-tps-throttling-channel").attachChannel(tpsThrottlingChannel).setServiceParameterKeys("ncs-vdf-apis")
                .chainBranch(new Condition(new EqualExpression(appK, statusK, limitedProductionK), unknownErrorCode), numberWhitelistService, numberStatusValidateionService)
                .chain("blocked-msisdn-channel").attachChannel(blockedMsisdnValidationService)
                .chain("send-charge-validation-channel").attachChannel(vodafoneApiSendChargeValidationChannel)
                .setOnSuccess(vdfApiSblService);

        return new WorkflowImpl(appSlaChannel);

    }


    private static Condition createSenderAddressValidationCondition() {
        ContainsExpression senderAddressAvailable = new ContainsExpression(requestK, senderAddressK);
        NotExpression not = new NotExpression(senderAddressAvailable);
        InExpression alias = new InExpression(KeyNameSpaceResolver.nameSpacedKey(requestK, senderAddressK),
                KeyNameSpaceResolver.nameSpacedKey(ncsK, mtK, aliasingK));
        EqualExpression defaultSenderAddress = new EqualExpression(KeyNameSpaceResolver.nameSpacedKey(requestK, senderAddressK),
                KeyNameSpaceResolver.nameSpacedKey(ncsK, mtK, defaultSenderAddressK));

        OrExpression or = new OrExpression(not, alias, defaultSenderAddress);

        return new Condition(or, invalidSourceAddressErrorCode);
    }

}