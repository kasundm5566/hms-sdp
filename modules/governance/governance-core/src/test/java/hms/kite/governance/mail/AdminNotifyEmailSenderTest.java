/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.governance.mail;

import java.io.Writer;
import java.util.List;

import hms.kite.governance.repo.mongo.ManualFilterMessageRepository;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;
/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */

@ContextConfiguration(locations={"classpath : beans-test.xml"})
public class AdminNotifyEmailSenderTest {

	private AdminNotifyEmailSender adminNotifyEmailSender;
	private Mockery mockery;
	
	@BeforeTest
	public void setUp() {
		ApplicationContext ac = new ClassPathXmlApplicationContext("beans-test.xml");
		adminNotifyEmailSender = (AdminNotifyEmailSender) ac.getBean("adminNotifyEmailSender");
		mockery = new Mockery(){
			{
				setImposteriser(ClassImposteriser.INSTANCE);
			}
		};
		final VelocityEngine ve = mockery.mock(VelocityEngine.class);
		final ManualFilterMessageRepository mfmr = mockery.mock(ManualFilterMessageRepository.class);
		Expectations expectations = new Expectations() {
			{
				allowing(mfmr).countPending();will(returnValue(new Long(4)));
				allowing(ve).mergeTemplate(with(any(String.class)), with(any(VelocityContext.class)), with(any(Writer.class)));
				will(returnValue(true));
			}
		};
		adminNotifyEmailSender.setManualFilterMessageRepository(mfmr);
		adminNotifyEmailSender.setVelocityEngine(ve);
		adminNotifyEmailSender.setCcList(new String[]{"a@gm.co", "b@gm.co"});
		adminNotifyEmailSender.setToAddress("admin@sdp.com");
		adminNotifyEmailSender.setEmailSubject("xxx");
		adminNotifyEmailSender.setHomePageUrl("http://sdp.com");
		mockery.checking(expectations);
	}
	
	@Test
	public void testCreate() {
		List<EmailMessage> list = adminNotifyEmailSender.create();
		assertEquals(list.size(), 1);
		EmailMessage em = list.get(0);
		assertEquals(em.getCcList(), new String[]{"a@gm.co", "b@gm.co"});
		assertEquals(em.getTo(), "admin@sdp.com");
		assertEquals(em.getSubject(), "xxx");
		mockery.assertIsSatisfied();
	}

}
