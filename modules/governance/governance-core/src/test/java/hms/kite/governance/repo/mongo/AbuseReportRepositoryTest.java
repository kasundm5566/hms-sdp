/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance.repo.mongo;

import static hms.kite.util.KiteKeyBox.appIdK;
import static hms.kite.util.KiteKeyBox.correlationIdK;
import static hms.kite.util.KiteKeyBox.endDateK;
import static hms.kite.util.KiteKeyBox.messageK;
import static hms.kite.util.KiteKeyBox.senderAddressK;
import static hms.kite.util.KiteKeyBox.spIdK;
import static hms.kite.util.KiteKeyBox.startDateK;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;
import hms.kite.governance.domain.AbuseReportStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */

@ContextConfiguration(locations={"classpath : beans-test.xml"})
public class AbuseReportRepositoryTest {

	private AbuseReportRepository repository;
	private MongoTemplate template;
	private String ABUSE_REPORT_COLLECTION = "abuse_report";
	
	@BeforeTest
	public void setUp() {
		ApplicationContext ac = new ClassPathXmlApplicationContext("beans-test.xml");
		repository = (AbuseReportRepository) ac.getBean("abuseReportRepository");
		template = (MongoTemplate) ac.getBean("template");
	}
	
	@Test(priority=1, enabled=true)
	public void testCreate() {
		try {
			Map<String, Object> map = getMap();
			repository.create(map);
			DBObject ob = template.getCollection(ABUSE_REPORT_COLLECTION).findOne(new BasicDBObject(correlationIdK, "12345"));
			assertNotNull(ob);
			assertEquals((String) ob.get(correlationIdK), map.get(correlationIdK));
			assertEquals((String) ob.get(startDateK), map.get(startDateK));
			assertEquals((String) ob.get(endDateK), map.get(endDateK));
			assertEquals((String) ob.get(spIdK), map.get(spIdK));
			assertEquals((String) ob.get(senderAddressK), map.get(senderAddressK));
			assertEquals((String) ob.get(appIdK), map.get(appIdK));
			assertEquals((String) ob.get(messageK), map.get(messageK));
			assertEquals((String) ob.get("notify-sp"), map.get("notify-sp"));
			assertEquals((Boolean) ob.get("sp-notified"), map.get("sp-notified"));
			assertEquals((String) ob.get("source"), map.get("source"));
		} catch(Exception ex) {
			fail(ex.getMessage());
		}
	}
	
	@Test(priority=2, enabled=true)
	public void testUpdate() {
		try {
			Map<String, Object> map = getMap();
			map.put("source", "web");
			map.put("notify-sp", "false");
			repository.update(map);
			DBObject ob = template.getCollection(ABUSE_REPORT_COLLECTION).findOne(new BasicDBObject(correlationIdK, "12345"));
			assertNotNull(ob);
			assertEquals((String) ob.get("notify-sp"), map.get("notify-sp"));
			assertEquals((String) ob.get("source"), map.get("source"));
		} catch(Exception ex) {
			fail(ex.getMessage());
		}
	}
	
	@Test(priority=2, enabled=true)
	public void testFindNotificationList() {
		try {
			Map<String, Object> map = getMap();
			map.put("notify-sp", "false");
			map.put(correlationIdK, "23456");
			template.getCollection(ABUSE_REPORT_COLLECTION).save(new BasicDBObject(map));
			map.put("notify-sp", "true");
			map.put(correlationIdK, "34567");
			template.getCollection(ABUSE_REPORT_COLLECTION).save(new BasicDBObject(map));
			List<Map<String, String>> result = repository.findNotificationList();
			assertEquals(result.size(), 2);
			assertEquals(result.get(0).get("notify-sp"), "true");
			assertEquals(result.get(1).get("notify-sp"), "true");
		} catch(Exception ex) {
			fail(ex.getMessage());
		}
	}
	
	@Test(priority=3, enabled=true)
	public void testFind() {
		try {
			restoreCollection();
			List<Map<String, String>> pendingList = repository.find("app 1", AbuseReportStatus.PENDING);
			assertEquals(pendingList.size(), 3);
			for(Map<String, String> map : pendingList) {
				assertEquals(map.get(appIdK), "app 1");
				assertEquals(map.get("report_status"), AbuseReportStatus.PENDING.name());
			}
		} catch(Exception ex) {
			fail(ex.getMessage());
		}
	}
	
	@Test(priority = 3, enabled = true)
	public void testFindWithAppIdAndLimit() {
		List<Map<String, Object>> list1 = repository.find("app 3", AbuseReportStatus.PENDING, 0, 5);
		assertEquals(list1.size(), 5);
		List<String> ids = new ArrayList<String>();
		for(Map<String, Object> ob : list1) {
			ids.add(ob.get("_id").toString());
		}
		
		List<Map<String, Object>> list2 = repository.find("app 3", AbuseReportStatus.PENDING, 6, 8);
		assertEquals(list2.size(), 8);
		for(Map<String, Object> ob : list2) {
			assertFalse(ids.contains(ob.get("_id").toString()));
			ids.add(ob.get("_id").toString());
		}
		
		List<Map<String, Object>> list3 = repository.find("app 3", AbuseReportStatus.PENDING, 15, 4);
		assertEquals(list3.size(), 1);
		for(Map<String, Object> ob : list3) {
			assertFalse(ids.contains(ob.get("_id").toString()));
			ids.add(ob.get("_id").toString());
		}
	}
	
	@Test(priority = 3, enabled = true)
	public void testFindNoOfReports() {
		assertEquals(repository.noOfReports("app 3"), 15);
		assertEquals(repository.noOfReports("app 4"), 0);
	}
	
	private void restoreCollection() {
		template.getCollection(ABUSE_REPORT_COLLECTION).drop();
		Map<String, Object> map = getMap();
		map.put(appIdK, "app 1");
		map.put("report_status", AbuseReportStatus.PENDING.name());
		template.getCollection(ABUSE_REPORT_COLLECTION).save(new BasicDBObject(map));
		template.getCollection(ABUSE_REPORT_COLLECTION).save(new BasicDBObject(map));
		template.getCollection(ABUSE_REPORT_COLLECTION).save(new BasicDBObject(map));
		map.put("report_status", AbuseReportStatus.VALID.name());
		template.getCollection(ABUSE_REPORT_COLLECTION).save(new BasicDBObject(map));
		template.getCollection(ABUSE_REPORT_COLLECTION).save(new BasicDBObject(map));
		map.put("report_status", AbuseReportStatus.INVALID.name());
		template.getCollection(ABUSE_REPORT_COLLECTION).save(new BasicDBObject(map));
		map.put(appIdK, "app 2");
		map.put("report_status", AbuseReportStatus.PENDING.name());
		template.getCollection(ABUSE_REPORT_COLLECTION).save(new BasicDBObject(map));
		template.getCollection(ABUSE_REPORT_COLLECTION).save(new BasicDBObject(map));
		map.put("report_status", AbuseReportStatus.INVALID.name());
		template.getCollection(ABUSE_REPORT_COLLECTION).save(new BasicDBObject(map));
		
		for(int i = 0; i < 15; i ++) {
			map.put(appIdK, "app 3");
			map.put("report_status", AbuseReportStatus.PENDING.name());
			template.getCollection(ABUSE_REPORT_COLLECTION).save(new BasicDBObject(map));
		}
	}
	
	private Map<String, Object> getMap() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(correlationIdK, "12345");
		map.put(startDateK, "2011-04-20 15:45");
		map.put(endDateK, "2020-05-29 14:42");
		map.put(spIdK, "sp-id");
		map.put(senderAddressK, "07788");
		map.put(appIdK, "app-id");
		map.put(messageK, "message");
		map.put("notify-sp", "true");				
		map.put("source", "sms");
		map.put("sp-notified", false);
		map.put("report_status", "pending");
		return map;
	}
	
	@AfterTest
	public void tearDown() {
		template.dropCollection(ABUSE_REPORT_COLLECTION);
	}
}
