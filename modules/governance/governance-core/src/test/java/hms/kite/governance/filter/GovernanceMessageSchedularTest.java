/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance.filter;

import hms.kite.governance.domain.FilterStatus;
import hms.kite.governance.repo.mongo.ManualFilterMessageRepository;
import hms.kite.sms.channel.SmsAoSdpWfChannel;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.lib.concurrent.DeterministicScheduler;
import org.jmock.lib.legacy.ClassImposteriser;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
/**
 * $LastChangedDate: $$
 * $LastChangedBy: danukas $
 * $LastChangedRevision: $$
 */

@ContextConfiguration(locations={"classpath:beans-test.xml"})
public class GovernanceMessageSchedularTest {

	private GovernanceMessageScheduler governanceMessageScheduler;
	private Mockery mockery;
	private MongoTemplate template;
	private DeterministicScheduler scheduler;
	
	@BeforeMethod
	public void setUp() {
		ApplicationContext ac = new ClassPathXmlApplicationContext("beans-test.xml");
		governanceMessageScheduler = (GovernanceMessageScheduler) ac.getBean("governanceMessageScheduler");
		template = (MongoTemplate) ac.getBean("template");
		mockery = new Mockery() {
			{
				setImposteriser(ClassImposteriser.INSTANCE);
			}
		};
		final SmsAoSdpWfChannel smsAoSdpWfChannel = mockery.mock(SmsAoSdpWfChannel.class);
		final ManualFilterMessageRepository manualFilterMessageRepository = mockery.mock(ManualFilterMessageRepository.class);
		scheduler = new DeterministicScheduler();
		Expectations expectations = new Expectations() {
			{
				exactly(2).of(smsAoSdpWfChannel).send(with(any(Map.class)));
				oneOf(manualFilterMessageRepository).find(FilterStatus.ALLOWED);will(returnValue(getDummyList()));
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("_id", null);
				exactly(2).of(manualFilterMessageRepository).update(map, FilterStatus.SENT);
			}

		};
		governanceMessageScheduler.setSmsChannel(smsAoSdpWfChannel);
		governanceMessageScheduler.setManualFilterMessageRepository(manualFilterMessageRepository);
		governanceMessageScheduler.setInterval(1);
		mockery.checking(expectations);
	}
	
	@Test
	public void testSchedular() throws SecurityException, NoSuchMethodException, 
							IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchFieldException {
		
		Class<?> gms = governanceMessageScheduler.getClass();
		Method executeSchedulerMethod = gms.getDeclaredMethod("executeScheduler", new Class[]{});
		executeSchedulerMethod.setAccessible(true);
		executeSchedulerMethod.invoke(governanceMessageScheduler, new Object[]{});
		scheduler.tick(500, TimeUnit.MILLISECONDS);
		mockery.assertIsSatisfied();
	}
	
	private List<Map<String, Object>> getDummyList() {
		List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
		list.add(new HashMap<String, Object>());
		list.add(new HashMap<String, Object>());
		return list;
	}
	
	@AfterMethod
	public void tearDown() {
		governanceMessageScheduler = null;
	}
}
