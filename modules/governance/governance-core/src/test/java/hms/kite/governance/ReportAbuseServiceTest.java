/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.governance;

import static hms.kite.util.KiteErrorBox.successCode;
import static hms.kite.util.KiteKeyBox.appIdK;
import static hms.kite.util.KiteKeyBox.appK;
import static hms.kite.util.KiteKeyBox.coopUserIdK;
import static hms.kite.util.KiteKeyBox.correlationIdK;
import static hms.kite.util.KiteKeyBox.messageK;
import static hms.kite.util.KiteKeyBox.nameK;
import static hms.kite.util.KiteKeyBox.replyK;
import static hms.kite.util.KiteKeyBox.requestK;
import static hms.kite.util.KiteKeyBox.senderAddressK;
import static hms.kite.util.KiteKeyBox.spIdK;
import static hms.kite.util.KiteKeyBox.spK;
import static hms.kite.util.KiteKeyBox.spNameK;
import static hms.kite.util.KiteKeyBox.statusCodeK;
import static hms.kite.util.KiteKeyBox.statusK;
import static hms.kite.util.KiteKeyBox.successK;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;
import hms.kite.governance.repo.mongo.AbuseReportRepository;
import hms.kite.governance.repo.mongo.AbuseReportSummaryRepository;
import hms.kite.governance.repo.mongo.BlockedMsisdnRepository;
import hms.kite.governance.subsc.SubscriptionClient;

import java.util.HashMap;
import java.util.Map;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */
public class ReportAbuseServiceTest {

	private ReportAbuseService service;
	
	@BeforeMethod(enabled=true)
	public void setUp() {
		ApplicationContext ac = new ClassPathXmlApplicationContext("beans-test.xml");
		service = (ReportAbuseService) ac.getBean("reportAbuseService");
		Mockery mockery = new Mockery() {
			{
				setImposteriser(ClassImposteriser.INSTANCE);
			}
		};
		final AbuseReportRepository arr = mockery.mock(AbuseReportRepository.class);
		final SubscriptionClient sc = mockery.mock(SubscriptionClient.class);
		final AbuseReportSummaryRepository arsr = mockery.mock(AbuseReportSummaryRepository.class);
		final BlockedMsisdnRepository bmr = mockery.mock(BlockedMsisdnRepository.class);
		Expectations expectations = new Expectations(){
			{
				Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String,Object>>();
				requestContext.put(requestK, getMap());
				requestContext.put(appK, getMap());
				requestContext.put(spK, getMap());
				Map<String, Object> repoMap = getMap();
				repoMap.put(spIdK, null);
				repoMap.put(spNameK, null);
				repoMap.put(nameK, null);
				repoMap.put(coopUserIdK, null);
				repoMap.put(messageK, "sd");
				repoMap.put(replyK, null);
				allowing(arr).create(repoMap); 
				allowing(arsr).update(repoMap);
				Map<String, Object> unsubResp = new HashMap<String, Object>();
				unsubResp.put(statusCodeK, successK);
				allowing(sc).unsubscribeUser(requestContext);will(returnValue(unsubResp));
				Map<String, Object> request = getMap();
				request.put(appIdK, "invalid");
				Map<String, Map<String, Object>> requestContext1 = new HashMap<String, Map<String,Object>>();
				requestContext1.put(requestK, request);
				allowing(sc).unsubscribeUser(requestContext1); will(throwException(new IllegalStateException()));
				allowing(bmr).block(with(any(String.class)), with(any(String.class)), with(any(String.class)));
				allowing(bmr).isBlocked("app-id", "07788");will(returnValue(new Boolean(false)));
			}
		};
		service.setReplies(new HashMap<String, String>());
		service.setAbuseReportRepository(arr);
		service.setSubscriptionClient(sc);
		service.setAbuseReportSummaryRepository(arsr);
		service.setBlockedMsisdnRepository(bmr);
		mockery.checking(expectations);
	}

	@Test(enabled=true)
	public void testSendSuccessCase() {
		try {
			Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String,Object>>();
			requestContext.put(requestK, getMap());
			requestContext.put(appK, getMap());
			requestContext.put(spK, getMap());
			Map<String, Object> response = service.execute(requestContext);
			assertNotNull(response);
			assertEquals(response.get(statusK), Boolean.toString(true));
			assertEquals(response.get(statusCodeK), successCode);
		} catch(Exception ex) {
			fail(ex.getMessage());
		}
	}
	
	@Test(enabled=true)
	public void testSendFailureCase() {
		try {
			Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String,Object>>();
			Map<String, Object> request = getMap();
			request.put(appIdK, "invalid");
			requestContext.put(requestK, request);
			Map<String, Object> response = service.execute(requestContext);
			assertEquals(response.get(statusK), Boolean.toString(false));
		} catch(Exception ex) {
			fail(ex.getMessage());
		}
	}
	
	private Map<String, Object> getMap() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(correlationIdK, "123");
		map.put(appIdK, "app-id");
		map.put(senderAddressK, "07788");
		map.put(messageK, "abuse app sd");
		return map;
	}
	
	@AfterTest
	public void tearDown() {
		
	}
}
