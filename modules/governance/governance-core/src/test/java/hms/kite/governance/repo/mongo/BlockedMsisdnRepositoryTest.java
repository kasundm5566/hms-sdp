/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance.repo.mongo;

import static hms.kite.util.KiteKeyBox.appIdK;
import static hms.kite.util.KiteKeyBox.correlationIdK;
import static hms.kite.util.KiteKeyBox.msisdnK;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mongodb.BasicDBObject;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $ $LastChangedRevision: 73804 $
 */
@ContextConfiguration(locations={"classpath : beans-test.xml"})
public class BlockedMsisdnRepositoryTest {

	private BlockedMsisdnRepository repository;
	private MongoTemplate template;
	
	private static final String bLOCKED_MSISDN_COLLECTION = "blocked_msisdn";
	
	@BeforeTest
	public void setUp() {
		ApplicationContext ac = new ClassPathXmlApplicationContext("beans-test.xml");
		repository = (BlockedMsisdnRepository) ac.getBean("blockedMsisdnRepository");
		template = (MongoTemplate) ac.getBean("template");
	}
	
	@Test(enabled = true)
	public void testBlock() {
		repository.block("app1", "7778883", "123413322341");
		repository.block("app1", "7788925", "221432521763");
		repository.block("app2", "7794155", "555568333158");
		assertEquals(template.getCollection(bLOCKED_MSISDN_COLLECTION).count(), 3);
		BasicDBObject document = new BasicDBObject();
		document.put(appIdK, "app1");
		document.put(msisdnK, "7778883");
		document.put(correlationIdK, "123413322341");
		assertNotNull(template.getCollection(bLOCKED_MSISDN_COLLECTION).findOne(document));
		document.put(msisdnK, "7788925");
		document.put(correlationIdK, "221432521763");
		assertNotNull(template.getCollection(bLOCKED_MSISDN_COLLECTION).findOne(document));
		document.put(appIdK, "app2");
		document.put(msisdnK, "7794155");
		document.put(correlationIdK, "555568333158");
		assertNotNull(template.getCollection(bLOCKED_MSISDN_COLLECTION).findOne(document));
	}
	
	@Test(enabled = true, dependsOnMethods = {"testBlock"})
	public void testIsBlocked() {
		assertTrue(repository.isBlocked("app1", "7788925"));
		assertFalse(repository.isBlocked("app2", "7788925"));
	}
	
	@AfterTest
	public void tearDown() {
		template.dropCollection(bLOCKED_MSISDN_COLLECTION);
	}
}
