/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.governance.mail;

import static hms.kite.util.KiteKeyBox.appIdK;
import static hms.kite.util.KiteKeyBox.coopUserIdK;
import static hms.kite.util.KiteKeyBox.correlationIdK;
import static hms.kite.util.KiteKeyBox.messageK;
import static hms.kite.util.KiteKeyBox.nameK;
import static hms.kite.util.KiteKeyBox.spIdK;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;
import hms.common.registration.api.response.BasicUserResponseMessage;
import hms.kite.governance.repo.mongo.AbuseReportRepository;
import hms.kite.util.RegistrationClient;
import static hms.common.registration.api.util.RestApiKeys.EMAIL;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */

@ContextConfiguration(locations={"classpath : beans-test.xml"})
public class SpNotifyEmailSenderTest {

	private SpNotifyEmailSender sender;
	private Mockery mockery;
	
	@BeforeMethod
	public void setUp() {
		ApplicationContext ac = new ClassPathXmlApplicationContext("beans-test.xml");
		sender = (SpNotifyEmailSender) ac.getBean("spNotifyEmailSender");
		mockery = new Mockery(){
			{
				setImposteriser(ClassImposteriser.INSTANCE);
			}
		};
		
	}

	@Test(enabled = true)
	public void testCreate() {
		try {
			final AbuseReportRepository ar = mockery.mock(AbuseReportRepository.class);
			final RegistrationClient rc = mockery.mock(RegistrationClient.class);
			final VelocityEngine ve = mockery.mock(VelocityEngine.class);
			final BasicUserResponseMessage burm1 = new BasicUserResponseMessage();
			burm1.setAdditionalData(EMAIL, "abc@gmail.com");
			final BasicUserResponseMessage burm2 = new BasicUserResponseMessage();
			burm2.setAdditionalData(EMAIL, "pqr@gmail.com");
			Expectations expectations = new Expectations(){
				{
					allowing(ar).findNotificationList();will(returnValue(createDummyEmailMessageList()));
					allowing(rc).getUserInfoByUserId("coo1");will(returnValue(burm1));
					allowing(rc).getUserInfoByUserId("coo2");will(returnValue(burm2));
					allowing(ve).mergeTemplate(with(any(String.class)), with(any(VelocityContext.class)), with(any(Writer.class)));
					will(returnValue(true));
				}
			};
			sender.setAbuseReportRepository(ar);
			sender.setRegistrationClient(rc);
			sender.setVelocityEngine(ve);
			mockery.checking(expectations);
			
			List<EmailMessage> mailList = sender.create();
			assertEquals(mailList.size(), 2);
			EmailMessage em1 = mailList.get(0);
			EmailMessage em2 = mailList.get(1);
			assertNotNull(em1);
			assertNotNull(em2);
			assertTrue(em1.getAppName().equals("app 1") || em1.getAppName().equals("app 2"));
			assertTrue(em2.getAppName().equals("app 1") || em2.getAppName().equals("app 2"));
			assertTrue(em1.getTo().equals("abc@gmail.com") || em1.getTo().equals("pqr@gmail.com"));
			assertTrue(em2.getTo().equals("abc@gmail.com") || em2.getTo().equals("pqr@gmail.com"));
		} catch(Exception ex) {
			ex.printStackTrace();
			fail(ex.getMessage());
		}
	}
	
	@Test(enabled = true)
	public void testUpdate() {
		try {
			final AbuseReportRepository ar = mockery.mock(AbuseReportRepository.class);
			mockery.checking(new Expectations() {
				{
					oneOf(ar).update(createUpdatingMap());
				}
			});
			sender.setAbuseReportRepository(ar);
			EmailMessage emailMessage = new EmailMessage("to", "from", "text");
			emailMessage.setCorrelationId("22222");
			sender.update(emailMessage, true);
			mockery.assertIsSatisfied();
		} catch(Exception ex) {
			fail(ex.getMessage());
		}
	}
	
	private List<Map<String, String>> createDummyEmailMessageList() {
		List<Map<String, String>> list = new ArrayList<Map<String,String>>();
		Map<String,String> map1 = new HashMap<String, String>();
		map1.put(spIdK, "sp0001");
		map1.put(messageK, "dummy message 1");
		map1.put(correlationIdK, "000001");
		map1.put(appIdK, "app0001");
		map1.put(coopUserIdK, "coo1");
		map1.put(nameK, "app 1");
		list.add(map1);
		Map<String,String> map2 = new HashMap<String, String>();
		map2.put(spIdK, "sp0002");
		map2.put(messageK, "dummy message 2");
		map2.put(correlationIdK, "000002");
		map2.put(appIdK, "app0002");
		map2.put(coopUserIdK, "coo2");
		map2.put(nameK, "app 2");
		list.add(map2);
		return list;
	}
	
	private Map<String, Object> createUpdatingMap() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("notify-sp", Boolean.toString(true));
		map.put("sp-notified", true);
		map.put(correlationIdK, "22222");
		return map;
	}
	
	@AfterMethod
	public void tearDown() {
		mockery = null;
	}
}
