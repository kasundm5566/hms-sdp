/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance.repo.mongo;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import hms.kite.governance.domain.FilterStatus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */

@ContextConfiguration(locations={"classpath : beans-test.xml"})
public class ManualFilterMessageRepositoryTest {

	private ManualFilterMessageRepository repository;
	private MongoTemplate template;
	
	private static final String MANUAL_FILTER_MESSAGE_COLLECTION = "manual_filter_message";
	
	@BeforeTest
	public void setUp() {
		ApplicationContext ac = new ClassPathXmlApplicationContext("beans-test.xml");
		repository = (ManualFilterMessageRepository) ac.getBean("manualFilteringMessageRepository");
		template = (MongoTemplate) ac.getBean("template");
	}
	
	@Test
	public void testAdd() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("message", "a message");
		map.put("correlation-id", "2341d");
		repository.add(map);
		DBObject ob = template.getCollection(MANUAL_FILTER_MESSAGE_COLLECTION).findOne();
		assertEquals(ob.get("message"), map.get("message"));
		assertEquals(ob.get("correlation-id"), map.get("correlation-id"));
		assertEquals(ob.get("filter-status"), FilterStatus.PENDING.name());
	}
	
	@Test(enabled = true)
	public void testFindWithStatus() {
		createData();
		List<Map<String, Object>> list = repository.find(FilterStatus.PENDING);
		assertEquals(list.size(), 1);
		assertEquals(list.get(0).get("filter-status"), FilterStatus.PENDING.name());
		
		list = repository.find(FilterStatus.ERROR);
		assertEquals(list.size(), 2);
		assertEquals(list.get(0).get("filter-status"), FilterStatus.ERROR.name());
		assertEquals(list.get(1).get("filter-status"), FilterStatus.ERROR.name());
		
		list = repository.find(FilterStatus.REJECTED);
		assertEquals(list.size(), 0);
	}
	
	@Test
	public void testFind() {
		createData();
		Map<String, Object> res = repository.findByCorrelationId("2261d");
		assertNotNull(res);
		assertEquals(res.get("filter-status"), FilterStatus.ERROR.name());
	}
	
	@Test
	public void testFindWithStatusAndNoData() {
		List<Map<String, Object>> list = repository.find(FilterStatus.ERROR);
		assertEquals(list.size(), 0);
	}
	
	@Test
	public void testFindWithNoData() {
		Map<String, Object> res = repository.findByCorrelationId("2261d");
		assertNull(res);
	}
	
	private void createData() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("message", "a message");
		map.put("correlation-id", "2341d");
		map.put("filter-status", FilterStatus.PENDING.name());
		template.getCollection(MANUAL_FILTER_MESSAGE_COLLECTION).save(new BasicDBObject(map));
		map.put("correlation-id", "2322d");
		map.put("filter-status", FilterStatus.ALLOWED.name());
		template.getCollection(MANUAL_FILTER_MESSAGE_COLLECTION).save(new BasicDBObject(map));
		map.put("correlation-id", "2261d");
		map.put("filter-status", FilterStatus.ERROR.name());
		template.getCollection(MANUAL_FILTER_MESSAGE_COLLECTION).save(new BasicDBObject(map));
		map.put("correlation-id", "2861d");
		template.getCollection(MANUAL_FILTER_MESSAGE_COLLECTION).save(new BasicDBObject(map));
	}

	@AfterMethod
	public void tearDown() {
		template.getCollection(MANUAL_FILTER_MESSAGE_COLLECTION).drop();
	}
}
