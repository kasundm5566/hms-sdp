/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance.mail;

import static org.testng.Assert.fail;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */

@ContextConfiguration(locations={"classpath : beans-test.xml"})
public class EmailClientTest {

	private EmailClient emailClient;
	private Mockery mockery;
	
	@BeforeTest
	public void setUp() {
		ApplicationContext ac = new ClassPathXmlApplicationContext("beans-test.xml");
		emailClient = (EmailClient) ac.getBean("governanceEmailClient");
		mockery = new Mockery();
		final MailSender mailSender = mockery.mock(MailSender.class);
		Expectations expectations = new Expectations() {
			{
				exactly(1).of(mailSender).send(with(any(SimpleMailMessage.class)));
			}
		};
		emailClient.setMailSender(mailSender);
		mockery.checking(expectations);
	}
	
	@Test
	public void testSend() {
		try {
			EmailMessage em = new EmailMessage("", "", "");
			emailClient.send(em);
			mockery.assertIsSatisfied();
		} catch(Exception ex) {
			fail(ex.getMessage());
		}
	}
	
	@AfterTest
	public void tearDown() {
		emailClient = null;
		mockery = null;
	}
}
