/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance;

import static hms.kite.util.KiteKeyBox.appIdK;
import static hms.kite.util.KiteKeyBox.appK;
import static hms.kite.util.KiteKeyBox.broadcastAddressK;
import static hms.kite.util.KiteKeyBox.messageK;
import static hms.kite.util.KiteKeyBox.requestK;
import static org.testng.Assert.assertEquals;
import hms.kite.governance.filter.RestrictedWordService;
import hms.kite.governance.repo.mongo.ManualFilterMessageRepository;
import hms.kite.governance.subsc.SubscriptionClient;

import java.util.HashMap;
import java.util.Map;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */

@ContextConfiguration(locations={"classpath : beans-test.xml"})
public class ContentFilteringServiceTest {

	private ContentFilteringService contentFilteringService;
	private Mockery mockery;
	
	@BeforeTest
	public void setUp() {
		ApplicationContext ac = new ClassPathXmlApplicationContext("beans-test.xml");
		contentFilteringService = (ContentFilteringService) ac.getBean("contentFilteringService");
		mockery = new Mockery() {
			{
				setImposteriser(ClassImposteriser.INSTANCE);
			}
		};
		final RestrictedWordService keyList = mockery.mock(RestrictedWordService.class);
		final SubscriptionClient subscriptionClient = mockery.mock(SubscriptionClient.class);
		final ManualFilterMessageRepository repository = mockery.mock(ManualFilterMessageRepository.class);
		Expectations expectations = new Expectations() {
			{
				allowing(repository).add(with(any(Map.class)));
				allowing(subscriptionClient).getSubscriptionCount("X"); will(returnValue(new Integer(20)));
				allowing(subscriptionClient).getSubscriptionCount("Y"); will(returnValue(new Integer(5)));
				allowing(keyList).isAbuse("abuse message"); will(returnValue(new Boolean(true)));
				allowing(keyList).isAbuse("normal message"); will(returnValue(new Boolean(false)));
			}
		};
		contentFilteringService.setKeyList(keyList);
		contentFilteringService.setSubscriptionClient(subscriptionClient);
		contentFilteringService.setManualFilterMessageRepository(repository);
		mockery.checking(expectations);
	}
	
	@Test
	public void testExecute() {
		Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String, Object>>();
		Map<String, Object>  request = new HashMap<String, Object>();
		request.put(messageK, "normal message");
		request.put(broadcastAddressK, "");
		Map<String, Object>  app = new HashMap<String, Object>();
		app.put(appIdK, "X");
		requestContext.put(appK, app);
		requestContext.put(requestK, request);
		
		Map<String, Object> resp = contentFilteringService.execute(requestContext);
		assertEquals((String) resp.get("status"), Boolean.toString(false));
		
		app.put(appIdK, "Y");
		resp = contentFilteringService.execute(requestContext);
		assertEquals((String) resp.get("status"), Boolean.toString(true));
		
		request.put(messageK, "abuse message");
		resp = contentFilteringService.execute(requestContext);
		assertEquals((String) resp.get("status"), Boolean.toString(false));
		
		app.put(appIdK, "X");
		request.put(messageK, "normal message");
		resp = contentFilteringService.execute(requestContext);
		assertEquals((String) resp.get("status"), Boolean.toString(false));
		
		request.remove(broadcastAddressK);
		resp = contentFilteringService.execute(requestContext);
		assertEquals((String) resp.get("status"), Boolean.toString(true));
		
		request.put(messageK, "abuse message");
		resp = contentFilteringService.execute(requestContext);
		assertEquals((String) resp.get("status"), Boolean.toString(false));
		
		mockery.assertIsSatisfied();
	}
	
	@AfterTest
	public void tearDown() {
		contentFilteringService = null;
	}
}
