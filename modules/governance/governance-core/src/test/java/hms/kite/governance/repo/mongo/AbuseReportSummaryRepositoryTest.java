/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance.repo.mongo;

import static hms.kite.util.KiteKeyBox.appIdK;
import static hms.kite.util.KiteKeyBox.nameK;
import static hms.kite.util.KiteKeyBox.spIdK;
import static hms.kite.util.KiteKeyBox.spNameK;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;
import hms.kite.governance.domain.AbuseReportStatus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */

@ContextConfiguration(locations={"classpath : beans-test.xml"})
public class AbuseReportSummaryRepositoryTest {

	private AbuseReportSummaryRepository repository; 
	private MongoTemplate template;
	
	private static final String ABUSE_REPORT_SUMMARY = "abuse_report_summary";
	private static final String PENDING_COUNT = "pending";
	private static final String VALID_COUNT = "valid";
	private static final String INVALID_COUNT = "invalid";
	private static final String REPORTED_COUNT = "reported";
	private String ABUSE_REPORT_STATUS = "report_status";
	
	@BeforeTest
	public void setUp() {
		ApplicationContext ac = new ClassPathXmlApplicationContext("beans-test.xml");
		repository = (AbuseReportSummaryRepository) ac.getBean("abuseReportSummaryRepository");
		template = (MongoTemplate) ac.getBean("template");
		createDummyData();
	}

	@Test(enabled = true, priority = 1)
	public void testUpdate() {
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(ABUSE_REPORT_STATUS, AbuseReportStatus.PENDING.name());
			map.put(appIdK, "app00001");
			repository.update(map);
			DBObject ob = template.getCollection(ABUSE_REPORT_SUMMARY).findOne(new BasicDBObject(appIdK, "app00001"));
			assertEquals((Integer) ob.get(PENDING_COUNT), new Integer(2));
			assertEquals((Integer) ob.get(VALID_COUNT), new Integer(0));
			assertEquals((Integer) ob.get(INVALID_COUNT), new Integer(0));
			assertEquals((Integer) ob.get(REPORTED_COUNT), new Integer(2));
			assertEquals(template.getCollection(ABUSE_REPORT_SUMMARY).count(), 2);
		} catch(Exception ex) {
			fail(ex.getMessage());
		}
	}

	@Test(enabled = true, priority = 2)
	public void testFindSummary() {
		try {
			List<Map<String, Object>> list = repository.findSummary(0, 5);
			assertEquals(list.size(), 2);
			for(Map<String, Object> map : list) {
				if(((String) map.get(appIdK)).equals("app00001")) {
					assertEquals((Integer) map.get(PENDING_COUNT), new Integer(2));
					assertEquals((Integer) map.get(VALID_COUNT), new Integer(0));
					assertEquals((Integer) map.get(INVALID_COUNT), new Integer(0));
					assertEquals((Integer) map.get(REPORTED_COUNT), new Integer(2));
				} else if(((String) map.get(appIdK)).equals("app00002")) {
					assertEquals((Integer) map.get(PENDING_COUNT), new Integer(0));
					assertEquals((Integer) map.get(VALID_COUNT), new Integer(2));
					assertEquals((Integer) map.get(INVALID_COUNT), new Integer(3));
					assertEquals((Integer) map.get(REPORTED_COUNT), new Integer(5));
				} else {
					fail("shouldn't come here!");
				}
			}
		} catch(Exception ex) {
			fail(ex.getMessage());
		}
	}

	@Test(priority = 3, enabled = true)
	public void testUpdateNonExisting() {
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(ABUSE_REPORT_STATUS, AbuseReportStatus.VALID.name());
			map.put(appIdK, "app00003");
			map.put(spIdK, "233");
			map.put(spNameK, "xxx");
			map.put(nameK, "app name");
			repository.update(map);
			DBObject ob = template.getCollection(ABUSE_REPORT_SUMMARY).findOne(new BasicDBObject(appIdK, "app00003"));
			assertNotNull(ob);
			map.put(ABUSE_REPORT_STATUS, AbuseReportStatus.INVALID.name());
			repository.update(map);
			assertEquals(template.getCollection(ABUSE_REPORT_SUMMARY).count(), 3);
		} catch(Exception ex) {
			fail(ex.getMessage());
		}
	}
	
	@Test(priority = 3, enabled = true)
	public void testNoOfReports() {
		assertEquals(repository.noOfReports(), 3);
	}
	
	private void createDummyData() {
		Map<String, Object> map1 = new HashMap<String, Object>();
		map1.put(appIdK, "app00001");
		map1.put(PENDING_COUNT, 1);
		map1.put(VALID_COUNT, 0);
		map1.put(INVALID_COUNT, 0);
		map1.put(REPORTED_COUNT, 1);
		Map<String, Object> map2 = new HashMap<String, Object>();
		map2.put(appIdK, "app00002");
		map2.put(PENDING_COUNT, 0);
		map2.put(VALID_COUNT, 2);
		map2.put(INVALID_COUNT, 3);
		map2.put(REPORTED_COUNT, 5);
		template.getCollection(ABUSE_REPORT_SUMMARY).save(new BasicDBObject(map1));
		template.getCollection(ABUSE_REPORT_SUMMARY).save(new BasicDBObject(map2));
		
	}
	
	@AfterTest
	public void tearDown() {
		template.dropCollection(ABUSE_REPORT_SUMMARY);
		repository = null;
		template = null;
	}
}
