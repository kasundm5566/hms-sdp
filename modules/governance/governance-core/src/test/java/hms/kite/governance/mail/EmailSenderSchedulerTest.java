/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance.mail;

import static org.testng.Assert.fail;



import hms.kite.util.EmailClient;

import java.util.ArrayList;
import java.util.List;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */

@ContextConfiguration(locations={"classpath : beans-test.xml"})
public class EmailSenderSchedulerTest {

	private EmailSenderScheduler scheduler;
	private Mockery mockery;
	
	@BeforeTest
	public void setUp() {
		ApplicationContext ac = new ClassPathXmlApplicationContext("beans-test.xml");
		scheduler = (EmailSenderScheduler) ac.getBean("emailSenderScheduler");
		mockery = new Mockery(){
			{
				setImposteriser(ClassImposteriser.INSTANCE);
			}
		};
		final AbstractEmailSender emailSender = mockery.mock(AbstractEmailSender.class);
		final EmailClient emailClient = mockery.mock(EmailClient.class);
		Expectations expectations = new Expectations(){
			{
				allowing(emailSender).create();will(returnValue(createDummyMailList()));
				allowing(emailSender).update(with(any(EmailMessage.class)), with(true));
				allowing(emailClient).send(null, null, null, null, null);
			}
		};
		scheduler.setEmailClient(emailClient);
		scheduler.setEmailSender(emailSender);
		mockery.checking(expectations);
	}
	
	@Test
	public void testSuccess() {
		try {
			scheduler.init();
			mockery.assertIsSatisfied();
		} catch(Exception ex) {
			fail(ex.getMessage());
		}
	}
	
	private List<EmailMessage> createDummyMailList() {
		List<EmailMessage> mailList = new ArrayList<EmailMessage>();
		mailList.add(new EmailMessage("", "", ""));
		mailList.add(new EmailMessage("", "", ""));
		mailList.add(new EmailMessage("", "", ""));
		mailList.add(new EmailMessage("", "", ""));
		return mailList;
	}
	
	@AfterTest
	public void tearDown() {
		mockery = null;
		scheduler = null;
	}
}
