/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.governance;

import static hms.kite.util.KiteKeyBox.appIdK;
import static hms.kite.util.KiteKeyBox.appK;
import static hms.kite.util.KiteKeyBox.recipientAddressK;
import static hms.kite.util.KiteKeyBox.recipientAddressStatusK;
import static hms.kite.util.KiteKeyBox.recipientsK;
import static hms.kite.util.KiteKeyBox.requestK;
import static hms.kite.util.KiteKeyBox.statusK;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import hms.kite.governance.repo.mongo.BlockedMsisdnRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hamcrest.text.StringContains;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */

@ContextConfiguration(locations={"classpath : beans-test.xml"})
public class BlockedMsisdnValidationServiceTest {

	private BlockedMsisdnValidationService blockedMsisdnValidationService; // TODO: spring
	private Mockery mockery;
	
	@BeforeTest
	public void setUp() {
		ApplicationContext ac = new ClassPathXmlApplicationContext("beans-test.xml");
		blockedMsisdnValidationService = (BlockedMsisdnValidationService) ac.getBean("blockedMsisdnValidationService");
		mockery = new Mockery() {
			{
				setImposteriser(ClassImposteriser.INSTANCE);
			}
		};
		final BlockedMsisdnRepository bmr = mockery.mock(BlockedMsisdnRepository.class);
		Expectations expectations = new Expectations() {
			{
				allowing(bmr).isBlocked(with(equal("app")), with(new StringContains("44444"))); will(returnValue(new Boolean(false)));
				allowing(bmr).isBlocked(with(equal("app")), with(new StringContains("55555"))); will(returnValue(new Boolean(true)));
			}
		};
		blockedMsisdnValidationService.setBlockedMsisdnRepository(bmr);
		mockery.checking(expectations);
	}
	
	@Test
	public void testSuccessCase() {
		Map<String, Map<String, Object>> requestContext = getRequestContext();
		List<Map<String, String>> recipients = (List<Map<String, String>>) requestContext.get(requestK).get(recipientsK);
		
		Map<String, Object> response1 = blockedMsisdnValidationService.execute(requestContext);
		assertTrue((Boolean.valueOf(response1.get(statusK).toString())));
		 
		Map<String, String> recipient2 = new HashMap<String, String>();
		recipient2.put(recipientAddressK, "444445");
		Map<String, String> recipient3 = new HashMap<String, String>();
		recipient3.put(recipientAddressK, "444446");
		recipients.add(recipient2);
		recipients.add(recipient3);
		
		Map<String, Object> response2 = blockedMsisdnValidationService.execute(requestContext);
		assertTrue(Boolean.valueOf(response2.get(statusK).toString()));
		
		List<Map<String, String>> recList = (List<Map<String, String>>) requestContext.get(requestK).get(recipientsK);
		assertFalse(isAnyRejected(recList));
	}
	
	@Test
	public void testFailureCase() {
		Map<String, Map<String, Object>> requestContext = getRequestContext();
		List<Map<String, String>> recipients = (List<Map<String, String>>) requestContext.get(requestK).get(recipientsK);
		recipients.remove(0);
		Map<String, String> recipient1 = new HashMap<String, String>();
		recipient1.put(recipientAddressK, "555555");
		recipients.add(recipient1);
		
		Map<String, Object> response1 = blockedMsisdnValidationService.execute(requestContext);
		assertFalse((Boolean.valueOf(response1.get(statusK).toString())));
		
		Map<String, String> recipient2 = new HashMap<String, String>();
		recipient2.put(recipientAddressK, "444445");
		Map<String, String> recipient3 = new HashMap<String, String>();
		recipient3.put(recipientAddressK, "444446");
		recipients.add(recipient2);
		recipients.add(recipient3);
		
		Map<String, Object> response2 = blockedMsisdnValidationService.execute(requestContext);
		assertTrue(Boolean.valueOf(response2.get(statusK).toString()));
		
		List<Map<String, String>> recList = (List<Map<String, String>>) requestContext.get(requestK).get(recipientsK);
		assertTrue(isAnyRejected(recList));
	}
	
	private Map<String, Map<String, Object>> getRequestContext() {
		Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String,Object>>();
		List<Map<String, String>> recipients = new ArrayList<Map<String,String>>();
		Map<String, String> recipient1 = new HashMap<String, String>();
		recipient1.put(recipientAddressK, "444444");
		recipients.add(recipient1);
		Map<String, Object> request = new HashMap<String, Object>();
		request.put(recipientsK, recipients);
		Map<String, Object> app = new HashMap<String, Object>();
		app.put(appIdK, "app");
		requestContext.put(requestK, request);
		requestContext.put(appK, app);
		return requestContext;
	}
	
	private boolean isAnyRejected(List<Map<String, String>> recipients) {
		boolean flag = false;
		for(Map<String, String> rec : recipients) {
			String status = rec.get(recipientAddressStatusK);
			if(status != null && !status.isEmpty()) {
				flag = true;
				break;
			}
		}
		return flag;
	}
	
}
