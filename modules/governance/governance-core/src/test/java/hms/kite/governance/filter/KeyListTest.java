/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance.filter;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $ $LastChangedRevision: 73804 $
 */

@ContextConfiguration(locations={"classpath : beans-test.xml"})
public class KeyListTest {

	private RestrictedWordService keyList;
	
	@BeforeTest
	public void setUp() {
		ApplicationContext ac = new ClassPathXmlApplicationContext("beans-test.xml");
		keyList = (RestrictedWordService) ac.getBean("filterKeyList");
	}
	
	@Test
	public void testFind() {
		assertTrue(keyList.find("abc"));
		assertFalse(keyList.find("not"));
	}
	
	@Test
	public void testIsAbuse() {
		assertFalse(keyList.isAbuse("some message"));
		assertTrue(keyList.isAbuse("message with pqr key"));
	}
	
	@AfterTest
	public void tearDown() {
		keyList = null;
	}
}
