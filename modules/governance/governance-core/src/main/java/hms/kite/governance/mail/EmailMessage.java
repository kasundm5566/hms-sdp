/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance.mail;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */
public class EmailMessage {

	private String to;
	private String from;
	private String text;
    private String cc;
	private String appName;
	private String[] ccList;
	private String correlationId;
	private String subject;

	public EmailMessage(String to, String from, String text) {
		this.to = to;
		this.from = from;
		this.text = text;
	}
	
	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String[] getCcList() {
		return ccList;
	}

	public void setCcList(String[] ccList) {
		this.ccList = ccList;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Override
	public String toString() {
		return String.format("message: to[%s], from[%s], , subject[%s], text[%s], correlation-id[%s], app-name[%s]",
				to, from, subject, text, correlationId, appName);
	}

}
