/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance;

import hms.kite.governance.filter.RestrictedWordService;
import hms.kite.governance.repo.mongo.ManualFilterMessageRepository;
import hms.kite.governance.subsc.SubscriptionClient;
import hms.kite.util.SdpException;
import hms.kite.util.SystemUtil;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteErrorBox.messagePendingAdminApproval;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $ $LastChangedRevision: 73804 $
 */
public class ContentFilteringService extends BaseChannel {

	private static final Logger logger = LoggerFactory.getLogger(ContentFilteringService.class);
	private ManualFilterMessageRepository manualFilterMessageRepository;
	private SubscriptionClient subscriptionClient;
	private int govAllowedSubscriptionLimit;
	private RestrictedWordService keyList;

	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
		logger.debug("Executing content filtering service with [{}]", requestContext.get(requestK));
		if (SystemUtil.isBroadcastRequest(requestContext)) {
			if (filterBroadcastMessage(requestContext)) {
				//return ResponseBuilder.generate(messagePendingAdminApproval, false, requestContext);
                throw new SdpException(messagePendingAdminApproval);
			}
		} else {
			if (filterSingleMessage(requestContext)) {
				//return ResponseBuilder.generate(messagePendingAdminApproval, false, requestContext);
                throw new SdpException(messagePendingAdminApproval);
			}
		}
		return ResponseBuilder.generateSuccess();
	}

	private Integer findNoOfRecipients(Map<String, Map<String, Object>> requestContext) {
		List<Map<String, String>> recipients = (List<Map<String, String>>) requestContext.get(requestK)
				.get(recipientsK);
		return recipients.size();
	}

	private boolean filterBroadcastMessage(Map<String, Map<String, Object>> requestContext) {
		boolean filtered = false;
		String appId = (String) requestContext.get(appK).get(appIdK);
		int appSubscriberCount = subscriptionClient.getSubscriptionCount(appId);
		logger.debug("subscriber count for app [{}] is [{}]", appId, appSubscriberCount);

        boolean isGovLimitForSubscriptionExceed = appSubscriberCount > govAllowedSubscriptionLimit;
        Object message = requestContext.get(requestK).get(messageK);
        boolean isAbuseKeyWord = keyList.isAbuse(message.toString());

        if(isGovLimitForSubscriptionExceed) {
            logger.info("Application's subscriber count [{}] exceed the Governance limit [{}], admin approval required to push the message.",
                    appSubscriberCount, govAllowedSubscriptionLimit);
        }

        if(isAbuseKeyWord) {
            logger.info("Message [{}] is marked as containing abuse keywords, admin approval required to push the message.", message);
        }

        if (isGovLimitForSubscriptionExceed || isAbuseKeyWord) {
			storeMessageDetails(requestContext, appSubscriberCount);
			filtered = true;
		}
		return filtered;
	}

	private boolean filterSingleMessage(Map<String, Map<String, Object>> requestContext) {
		boolean filtered = false;
		if (keyList.isAbuse(requestContext.get(requestK).get(messageK).toString())) {
			logger.info("Message contain suspective abusive content, will be stored for admin approval");
			storeMessageDetails(requestContext, findNoOfRecipients(requestContext));
			filtered = true;
		}
		return filtered;
	}

	private void storeMessageDetails(Map<String, Map<String, Object>> requestContext, int recipientCount) {
		requestContext.get(requestK).put(recipientCountK, recipientCount);
		Map<String, Object> sp = requestContext.get(spK);
		if (sp != null) {
			requestContext.get(requestK).put(coopUserIdK, sp.get(coopUserIdK));
		}
		manualFilterMessageRepository.add(requestContext.get(requestK));
	}

	public void setManualFilterMessageRepository(ManualFilterMessageRepository manualFilterMessageRepository) {
		this.manualFilterMessageRepository = manualFilterMessageRepository;
	}

	public void setSubscriptionClient(SubscriptionClient subscriptionClient) {
		this.subscriptionClient = subscriptionClient;
	}

	public void setKeyList(RestrictedWordService keyList) {
		this.keyList = keyList;
	}

	public void setGovAllowedSubscriptionLimit(int govAllowedSubscriptionLimit) {
		this.govAllowedSubscriptionLimit = govAllowedSubscriptionLimit;
	}

}
