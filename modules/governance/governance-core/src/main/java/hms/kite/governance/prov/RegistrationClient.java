/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance.prov;

import hms.common.registration.api.common.RequestType;
import hms.common.registration.api.request.UserDetailByMsisdnRequestMessage;
import hms.common.registration.api.request.UserDetailRequestMessage;
import hms.common.registration.api.response.BasicUserResponseMessage;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;
import java.util.Date;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */
public class RegistrationClient { // TODO: remove non-existing method from test case [ findSp(String) ]
	
	private String commonRegRequestPathForMsisdn; 
	private String commonRegRequestPathForUserId;
	private WebClient webClientForMsisdn;
	private WebClient webClientForUserId;
	private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationClient.class);
	
	public BasicUserResponseMessage getUserInfoByMsisdn(String msisdn) {
		UserDetailByMsisdnRequestMessage reqMessage = new UserDetailByMsisdnRequestMessage();
        reqMessage.setRequestType(RequestType.USER_BASIC_DETAILS);
        reqMessage.setRequestedTimeStamp(new Date());
        reqMessage.setMsisdn(msisdn);
        if(LOGGER.isDebugEnabled()) {
        	LOGGER.debug("Sending Get User Additional Details Request " + reqMessage.toString());
        }
        return generateUserResponseMessage(getWebClientForMsisdn().post(reqMessage));
	}
	
	public BasicUserResponseMessage getUserInfoByUserId(String userId) {
		UserDetailRequestMessage reqMessage = new UserDetailRequestMessage();
        reqMessage.setRequestType(RequestType.USER_BASIC_DETAILS);
        reqMessage.setRequestedTimeStamp(new Date());
        reqMessage.setUserId(userId);
        if(LOGGER.isDebugEnabled()) {
        	LOGGER.debug("Sending Get User Basic Details Request [{}]", reqMessage.toString());
        }
		return generateUserResponseMessage(getWebClientForUserId().post(reqMessage));
	}
	
	private <T> T readXmlCommonResponse(InputStream is, Class<T> aClass) {
		JAXBContext ctx;
		try {
			ctx = JAXBContext.newInstance(aClass);
			Unmarshaller um = ctx.createUnmarshaller();
			return (T) um.unmarshal(is);
		} catch(JAXBException ex) {
			LOGGER.error("Error occurred while reading xml response!", ex);
			return null;
		}
	}

	private WebClient getWebClientForMsisdn() {
		if(webClientForMsisdn == null) {
			webClientForMsisdn = WebClient.create(commonRegRequestPathForMsisdn);
			webClientForMsisdn.header("Content-Type", "application/xml");
			webClientForMsisdn.accept("application/xml");
		}
		return webClientForMsisdn;
	}
	
	private WebClient getWebClientForUserId() {
		if(webClientForUserId == null) {
			webClientForUserId = WebClient.create(commonRegRequestPathForUserId);
			webClientForUserId.header("Content-Type", "application/xml");
			webClientForUserId.accept("application/xml");
		}
		return webClientForUserId;
	}
	
	private BasicUserResponseMessage generateUserResponseMessage(Response response) {
		InputStream resStream = (InputStream) response.getEntity();
		return readXmlCommonResponse(resStream, BasicUserResponseMessage.class);
	}

	public void setCommonRegRequestPathForMsisdn(
			String commonRegRequestPathForMsisdn) {
		this.commonRegRequestPathForMsisdn = commonRegRequestPathForMsisdn;
	}

	public void setCommonRegRequestPathForUserId(
			String commonRegRequestPathForUserId) {
		this.commonRegRequestPathForUserId = commonRegRequestPathForUserId;
	}
	
}
