/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.governance.integration.channel;

import static hms.kite.util.KiteKeyBox.replyK;
import static hms.kite.util.KiteKeyBox.requestK;
import static hms.kite.util.KiteKeyBox.statusCodeK;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hms.kite.sms.channel.SmsMoReplyMapperChannel;
import hms.kite.util.StatusDescriptionBuilder;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class ReportAbuseReplyMapperChannel implements Channel {

	private static final Logger logger = LoggerFactory.getLogger(ReportAbuseReplyMapperChannel.class);

	private StatusDescriptionBuilder descriptionBuilder;

	public void init() {
		descriptionBuilder = new StatusDescriptionBuilder("abuse-mo-reply");
		descriptionBuilder.init();
	}

	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
		String reply = descriptionBuilder.createStatusDescription((String) requestContext.get(requestK)
				.get(statusCodeK), requestContext);

		if (null != reply) {
			requestContext.get(requestK).put(replyK, reply);
		}

		return ResponseBuilder.generateSuccess();
	}

	@Override
	public Map<String, Object> send(Map<String, Object> request) {
		// no impl needed
		return null;
	}

}
