/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance.repo.mongo;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import hms.kite.datarepo.mongodb.MongoDbUtils;
import hms.kite.governance.domain.AbuseReportStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;
/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */
public class AbuseReportSummaryRepository extends MongoDbUtils {

	private static final String ABUSE_REPORT_SUMMARY = "abuse_report_summary";
	private static final String PENDING_COUNT = "pending";
	private static final String VALID_COUNT = "valid";
	private static final String INVALID_COUNT = "invalid";
	private static final String REPORTED_COUNT = "reported";
	private static final String ABUSE_REPORT_STATUS = "report_status";
	
	private DBCollection collection;
	
	public void update(Map<String, Object> map) {
		AbuseReportStatus status = AbuseReportStatus.valueOf((String) map.get(ABUSE_REPORT_STATUS));
		Map<String, Object> queryMap = new HashMap<String, Object>();
		queryMap.put(appIdK, map.get(appIdK));
		queryMap.put(spIdK, map.get(spIdK));
		queryMap.put(spNameK, map.get(spNameK));
		queryMap.put(nameK, map.get(nameK));
		queryMap.put(coopUserIdK, map.get(coopUserIdK));
		BasicDBObject query = new BasicDBObject();
		query.putAll(queryMap);
		BasicDBObject update = new BasicDBObject();
		if(status.equals(AbuseReportStatus.INVALID)) {
			update.append(INVALID_COUNT, 1);
			update.append(PENDING_COUNT, -1);
		} else if(status.equals(AbuseReportStatus.VALID)) {
			update.append(VALID_COUNT, 1);
			update.append(PENDING_COUNT, -1);
		} else if(status.equals(AbuseReportStatus.PENDING)) {
			update.append(PENDING_COUNT, 1);
			update.append(REPORTED_COUNT, 1);
			update.append(VALID_COUNT, 0);
			update.append(INVALID_COUNT, 0);
		}
		getAbuseReportSummaryCollection().update(query, new BasicDBObject("$inc", update), true, true);
	}
	
	public List<Map<String, Object>> findSummary(int lastRow, int batchSize) {
		BasicDBObject keys = new BasicDBObject();
		keys.put(appIdK, 1);
		keys.put(PENDING_COUNT, 1);
		keys.put(INVALID_COUNT, 1);
		keys.put(VALID_COUNT, 1);
		keys.put(REPORTED_COUNT, 1);
		keys.put(spIdK, 1);
		keys.put(spNameK, 1);
		keys.put(nameK, 1);
		keys.put(coopUserIdK, 1);
//		DBCursor cursor = getAbuseReportSummaryCollection().find(new BasicDBObject(), keys, lastRow, batchSize);
		DBCursor cursor = getAbuseReportSummaryCollection().find(new BasicDBObject(), keys, lastRow, batchSize).limit(batchSize);
		List<Map<String, Object>> result = new ArrayList<Map<String,Object>>();
		while(cursor.hasNext()) {
			result.add((Map<String, Object>) cursor.next().toMap());
		}
		return result;
	}
	
	public int noOfReports() {
		return ((int) getAbuseReportSummaryCollection().count());
	}
	
	private DBCollection getAbuseReportSummaryCollection() {
		if(collection == null) {
			collection = mongoTemplate.getCollection(ABUSE_REPORT_SUMMARY);
			collection.ensureIndex(appIdK);
		}
		return collection;
	}
}
