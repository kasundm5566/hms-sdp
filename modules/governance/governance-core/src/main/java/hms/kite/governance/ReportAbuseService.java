package hms.kite.governance;

/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

import hms.kite.governance.repo.mongo.AbuseReportRepository;
import hms.kite.governance.repo.mongo.AbuseReportSummaryRepository;
import hms.kite.governance.repo.mongo.BlockedMsisdnRepository;
import hms.kite.governance.subsc.SubscriptionClient;
import hms.kite.util.SdpException;
import hms.kite.util.SystemUtil;
import hms.kite.wfengine.transport.BaseChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.wfengine.impl.ResponseBuilder.generate;
import static hms.kite.wfengine.impl.ResponseBuilder.generateSuccess;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $ $LastChangedRevision: 73804 $
 */
public class ReportAbuseService extends BaseChannel {

	private AbuseReportRepository abuseReportRepository;
	private AbuseReportSummaryRepository abuseReportSummaryRepository;
	private SubscriptionClient subscriptionClient;
	private BlockedMsisdnRepository blockedMsisdnRepository;
	private int abuseKeywordLength;
	private Map<String, String> replies;

	private static final String USER_BLOCKED_MESSAGE = "user_blocked_message";
	private static final String DUPLICATE_REQUEST_MESSAGE = "duplicate_request_message";
	private static final String REPORTING_SUCCESS_MESSAGE = "reporting_success_message";
	private static final String UNSUBSCRIBE_SUCCESS_MESSAGE = "unsubscribe_success_message";
	private static final String USER_NOT_REGISTERED_MESSAGE = "user_not_registered_message";

	private static final Logger logger = LoggerFactory.getLogger(ReportAbuseService.class);

	/**
	 * @param requestContext
	 *            : holds entries needed to execute the abuse report channel The
	 *            user request is contained in requestContext in the form of a
	 *            map. The request
	 *
	 *            request parameters needed app-id sender-address (from sms mo)
	 *            source (web/ussd/sms - default sms)
	 *
	 *            app parameters app-name sp-id send-abuse-report-notify
	 *
	 *            sp parameters sp-name coopUserIdK
	 *
	 *
	 */
	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
		Map<String, Object> request = requestContext.get(requestK);
		logger.debug("Received report abuse request [{}]", request);
		try {
			if (!isValidSenderAddress(requestContext)) {
				logger.warn("Sender address status is invalid - ignoring report abuse request");
				throw new SdpException((String) request.get(senderAddressStatusK));
			}

			if (isSubscriptionTypeApplication(requestContext)) {
				if (subscriptionClient.isRegisteredUser((String) request.get(appIdK),
						(String) request.get(senderAddressK))) {
					Map<String, Object> nblResp = subscriptionClient.unsubscribeUser(requestContext);
					Map<String, Object> resp = convertToIntenalResp(nblResp);
					handleSubscriptionAbuseReport(requestContext, resp.get(statusCodeK));
					return generate(resp);
				} else {
					saveAbuseReport(requestContext);
					requestContext.get(requestK).put(replyK, replies.get(REPORTING_SUCCESS_MESSAGE));
					return generateSuccess();
				}

			} else {
				if (!blockUser(requestContext)) {
					return generate(onDemandBlockedErrorCode, false, requestContext);
				}
				return generateSuccess(requestContext);
			}
		} catch (SdpException e) {
			throw e;
		} catch (Exception e) {
			logger.error("Failed to save error message", e);
			return generate(systemErrorCode, false, requestContext);
		}
	}

	private Map<String, Object> convertToIntenalResp(Map<String, Object> nblResp) {
		Map<String, Object> resp = new HashMap<String, Object>();
		resp.put(statusCodeK, nblResp.get(statusCodeNblK));
		resp.put(statusDescriptionK, nblResp.get(statusDescriptionNblK));
		return resp;
	}

	private boolean isValidSenderAddress(Map<String, Map<String, Object>> requestContext) {
		return successCode.equals(requestContext.get(requestK).get(senderAddressStatusK));
	}

	private void saveAbuseReport(Map<String, Map<String, Object>> requestContext) {
		Map<String, Map<String, Object>> savingCopy = SystemUtil.copy(requestContext);
		addNcsType(savingCopy);
		addAbuseRequestParameters(savingCopy);
		abuseReportRepository.create(savingCopy.get(requestK));
		abuseReportSummaryRepository.update(savingCopy.get(requestK));
		logger.debug("Report abuse saved successfully");
	}

	private boolean isSubscriptionTypeApplication(Map<String, Map<String, Object>> requestContext) {
		return (requestContext.containsKey(ncsK) && requestContext.get(ncsK) != null && subscriptionK
				.equals(requestContext.get(ncsK).get(ncsTypeK)));
	}

	private void addAbuseRequestParameters(Map<String, Map<String, Object>> requestContext) {
		requestContext.get(requestK).put(nameK, requestContext.get(appK).get(nameK));
		requestContext.get(requestK).put(spIdK, requestContext.get(appK).get(spIdK));
		requestContext.get(requestK).put(spNameK, requestContext.get(spK).get(coopUserNameK));
		requestContext.get(requestK).put(coopUserIdK, requestContext.get(spK).get(coopUserIdK));
		requestContext.get(requestK).put(messageK, getAbuseMessageDescrption(requestContext));
	}

	private String getAbuseMessageDescrption(Map<String, Map<String, Object>> requestContext) {
		String message = (String) requestContext.get(requestK).get(messageK);
        String medium = (String) requestContext.get(requestK).get(mediumK);
		int abuseDescriptionIndex = message.trim().indexOf(" ", abuseKeywordLength);
		String abuseDescription = "";
		if (abuseDescriptionIndex > 0 && !webK.equals(medium)) {
			abuseDescription = message.substring(abuseDescriptionIndex).trim();
		} else if (webK.equals(medium)){
            return message;
        }
		return abuseDescription;
	}

	private boolean blockUser(Map<String, Map<String, Object>> requestContext) {
		String appId = (String) requestContext.get(appK).get(appIdK);
		String msisdn = (String) requestContext.get(requestK).get(senderAddressK);
		if (blockedMsisdnRepository.isBlocked(appId, msisdn)) {
			logger.warn("User[{}] have been blocked for on demand application[{}], so ignoring abuse report", msisdn,
					appId);
			requestContext.get(requestK).put(replyK, replies.get(DUPLICATE_REQUEST_MESSAGE));
			return false;
		}
		logger.info("Blocking user for on demand application");
		blockedMsisdnRepository.block(appId, msisdn, (String) requestContext.get(requestK).get(correlationIdK));
		saveAbuseReport(requestContext);
		requestContext.get(requestK).put(replyK, replies.get(REPORTING_SUCCESS_MESSAGE));
		return true;
	}

	private void handleSubscriptionAbuseReport(Map<String, Map<String, Object>> requestContext, Object statusCode) {
		if (successCode.equals(statusCode)) {
			saveAbuseReport(requestContext);
			requestContext.get(requestK).put(replyK, replies.get(UNSUBSCRIBE_SUCCESS_MESSAGE));
		} else if (subscriptionUnregNotRegErrorCode.equals(statusCode)) {
			requestContext.get(requestK).put(replyK, replies.get(USER_NOT_REGISTERED_MESSAGE));
		} else if (subscriptionUnregBlockedErrorCode.equals(statusCode)) {
			requestContext.get(requestK).put(replyK, replies.get(USER_BLOCKED_MESSAGE));
		}
	}

	private void addNcsType(Map<String, Map<String, Object>> requestContext) {
		if (requestContext.get(requestK).containsKey(mediumK) && requestContext.get(requestK).get(mediumK) != null) {
			requestContext.get(requestK).put(ncsTypeK, requestContext.get(requestK).get(mediumK));
		} else {
			requestContext.get(requestK).put(ncsTypeK, smsK);
		}
	}

	public void setAbuseReportRepository(AbuseReportRepository abuseReportRepository) {
		this.abuseReportRepository = abuseReportRepository;
	}

	public void setSubscriptionClient(SubscriptionClient subscriptionClient) {
		this.subscriptionClient = subscriptionClient;
	}

	public void setAbuseReportSummaryRepository(AbuseReportSummaryRepository abuseReportSummaryRepository) {
		this.abuseReportSummaryRepository = abuseReportSummaryRepository;
	}

	public void setBlockedMsisdnRepository(BlockedMsisdnRepository blockedMsisdnRepository) {
		this.blockedMsisdnRepository = blockedMsisdnRepository;
	}

	public void setReplies(Map<String, String> replies) {
		this.replies = replies;
	}

	public void setAbuseKeywordLength(int abuseKeywordLength) {
		this.abuseKeywordLength = abuseKeywordLength;
	}
}
