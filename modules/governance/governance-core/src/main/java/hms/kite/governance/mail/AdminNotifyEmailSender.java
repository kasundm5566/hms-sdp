/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.governance.mail;

import hms.kite.governance.repo.mongo.ManualFilterMessageRepository;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.velocity.VelocityEngineUtils;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */
public class AdminNotifyEmailSender extends AbstractEmailSender {

	private ManualFilterMessageRepository manualFilterMessageRepository;
	private String toAddress;
	private String[] ccList;
	private VelocityEngine velocityEngine;
	private String templateLocation;
	private String homePageUrl;
	private String emailSubject;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminNotifyEmailSender.class);
	private static final String PENDING_COUNT_KEY = "pending_count";
	private static final String EMAIL_DATE_KEY = "email_date";
	private static final String HOME_PAGE_URL_KEY = "governance_home_page_url";
	
	@Override
	public List<EmailMessage> create() {
		List<EmailMessage> emails = new ArrayList<EmailMessage>();
		Map<String, String> replacements = new HashMap<String, String>();
		replacements.put(PENDING_COUNT_KEY, Long.toString(manualFilterMessageRepository.countPending()));
		replacements.put(EMAIL_DATE_KEY, currentDate());
		replacements.put(HOME_PAGE_URL_KEY, homePageUrl);
		String text = createEmailBody(replacements);
		EmailMessage em = new EmailMessage(toAddress, fromAddress, text);
		em.setSubject(emailSubject);
		em.setCcList(ccList);
		emails.add(em);
		return emails;
	}

	@Override
	public void update(EmailMessage emailMessage, boolean status) {
		//XXX: no updating needed .... 
	}

	@Override
	protected String createEmailBody(Map<String, String> replacements) {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("parameters to be set in the email [{}]", replacements);
		}
		return VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, templateLocation, replacements);
	}

	private String currentDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(new Date());
	}
	
	public void setManualFilterMessageRepository(
			ManualFilterMessageRepository manualFilterMessageRepository) {
		this.manualFilterMessageRepository = manualFilterMessageRepository;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public void setVelocityEngine(VelocityEngine velocityEngine) {
		this.velocityEngine = velocityEngine;
	}

	public void setTemplateLocation(String templateLocation) {
		this.templateLocation = templateLocation;
	}

	public void setCcList(String[] ccList) {
		this.ccList = ccList;
	}

	public void setHomePageUrl(String homePageUrl) {
		this.homePageUrl = homePageUrl;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

}
