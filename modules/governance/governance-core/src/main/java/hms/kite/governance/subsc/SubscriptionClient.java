/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance.subsc;

import hms.kite.subscription.core.domain.SubscriptionNblResponse;
import hms.kite.subscription.core.domain.SubscriptionStatus;
import hms.kite.subscription.integration.SubscriptionServiceEndpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $ $LastChangedRevision: 73804 $
 */
public class SubscriptionClient {

	private SubscriptionServiceEndpoint subscriptionServiceEndpoint;
	public static final Logger logger = LoggerFactory.getLogger(SubscriptionClient.class);

	public Map<String, Object> unsubscribeUser(Map<String, Map<String, Object>> requestContext) {
		logger.info("Sending unregister request to subscription end point");
		Map<String, Object> response = subscriptionServiceEndpoint.unreg(createUnregMessage(requestContext), null);
		logger.info("unreg user response [{}]", response);
		return response;
	}

	public int getSubscriptionCount(String appId) {
		logger.debug("App id to get the subscriber count [{}]", appId);
		Map<String, Object> message = new HashMap<String, Object>();
		message.put(applicationIdK, appId);
		Map<String, Object> resp = subscriptionServiceEndpoint.query(message, null);
		logger.debug("response from endpoint [{}]", resp);
		return Integer.parseInt((String) resp.get(baseSizeNblK));
	}

	public boolean isRegisteredUser(String appId, String msisdn) {
		logger.debug("Checking registration status app-id[{}], msisdn[{}]", appId, msisdn);
		Map<String, Object> request = new HashMap<String, Object>();
		request.put(applicationIdK, appId);
		request.put(subscriberIdNblK, convertToNblMsisdnFormat(msisdn));
		Map<String, Object> subscriptionStatusList = subscriptionServiceEndpoint.subscriptionStatusList(request, null);
		logger.debug("Response from subscription[{}]", subscriptionStatusList);
        try {
            SubscriptionNblResponse status = SubscriptionNblResponse.subscriptionStatusValueToSubscriptionNblResponse((String) subscriptionStatusList.get(subscriptionStatusNblK));
            logger.info("Subscription nbl status for msisdn[{}]/app-id[{}], is = [{}]", new Object[]{msisdn, appId, status});
            if (status != null) {
                if (status == SubscriptionNblResponse.REGISTERED) {
                    logger.info("User[{}] is a registered user for app[{}]", msisdn, appId);
                    return true;
                }
            }
            logger.info("User[{}] is a not a registered user for app[{}]", msisdn, appId);
            return false;
        } catch (Exception e) {
            logger.error("Error occurred while retrieving the subscription status. [{}]", e);
            return false;
        }

	}

    private String convertToNblMsisdnFormat(Object msisdn) {
        return telK + ":" + msisdn;
    }

    private Map<String, Object> createUnregMessage(Map<String, Map<String, Object>> requestContext) {
		Map<String, Object> message = new HashMap<String, Object>();
		Map<String, Object> request = requestContext.get(requestK);
		message.put(subscriberIdNblK, convertToNblMsisdnFormat(request.get(senderAddressK)));
		message.put(applicationIdK, request.get(appIdK));
		message.put(actionK, optOutK);
		message.put(operatorK, getValue(request, operatorK));
		message.put(keywordK, getValue(request, keywordK));
		message.put(shortcodeK, getValue(request, shortcodeK));
		logger.debug("Created unsubscribe request[{}]", message);
		return message;
	}

    private Object getValue(Map<String, Object> request, String key){
        Object str = request.get(key);
        return str == null ? "" : str;
    }

	public void setSubscriptionServiceEndpoint(SubscriptionServiceEndpoint subscriptionServiceEndpoint) {
		this.subscriptionServiceEndpoint = subscriptionServiceEndpoint;
	}

}
