/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance.integration;

import hms.kite.util.SystemUtil;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.remotehostK;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@Produces({"application/json", "application/xml"})
@Consumes({"application/json", "application/xml"})
@Path("/governance/")
public class ReportAbuseServiceEndpoint {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReportAbuseServiceEndpoint.class);
    private Channel abuseReportAoChannel;
    private boolean override;

    @Produces({"application/json", "application/xml"})
    @Consumes({"application/json", "application/xml"})
    @POST
    @Path("/report")
    public Map<String, Object> report(Map<String, Object> msg, @Context javax.servlet.http.HttpServletRequest request) {
        addHostDetails(msg, request);
        Map<String, Object> resp = abuseReportAoChannel.send(msg);
        LOGGER.debug("Sending response[{}] to application", resp);
        return resp;
    }

    private void addHostDetails(Map<String, Object> msg, HttpServletRequest request) {
        if (override || null == msg.get(remotehostK)) {
            String host = SystemUtil.findHostIp(request);
            if (null != host) {
                msg.put(remotehostK, host);
            }
        }
    }
    
    public void setAbuseReportAoChannel(Channel abuseReportAoChannel) {
        this.abuseReportAoChannel = abuseReportAoChannel;
    }
}
