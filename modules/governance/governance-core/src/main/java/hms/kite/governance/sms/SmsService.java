/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.governance.sms;

import hms.kite.sms.channel.SmsMtChannel;
import hms.kite.util.SystemUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */

public class SmsService {

	private String senderAddress;
	private SmsMtChannel smsMtChannel;
	private String unsubscribeMessage;
	private String normalMessage;
	private static final Logger LOGGER = LoggerFactory.getLogger(SmsService.class);
	
	public void sendSms(Map<String, Map<String, Object>> requestContext, boolean reg) {
		LOGGER.info("sending sms ... ");
		Map<String, Object> request = new HashMap<String, Object>();
		request.put(messageK, format(reg, new Object[]{requestContext.get(requestK).get(appIdK)}));// TODO: verify the parameters
		request.put(senderAddressK, senderAddress);
//		request.put(recipientAddressK, requestContext.get(requestK).get(senderAddressK));
		request.put(correlationIdK, Long.toString(SystemUtil.getCorrelationId()));
        request.put(operatorK, requestContext.get(requestK).get(operatorK));
//		request.put(recipientAddressOptypeK, requestContext.get(requestK).get(recipientAddressOptypeK));
		request.put("srr", Boolean.toString(false));
		request.put(recipientsK, addRecipient(requestContext));
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("sms message [{}]", request);
		}
		smsMtChannel.send(request);
	}
	
	private String format(boolean reg, Object ... args) {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("user was registered: {}, parameters [{}]", reg, args);
		}
		String text;
		if(reg) {
			text = MessageFormat.format(unsubscribeMessage, args);
		} else {
			text = MessageFormat.format(normalMessage, args);
		}
		return text;
	}

	private List<Map<String, String>> addRecipient(Map<String, Map<String, Object>> requestContext) {
		List<Map<String, String>> recipients = new ArrayList<Map<String,String>>();
		Map<String, String> recipient = new HashMap<String, String>();
		recipient.put(recipientAddressK, (String) requestContext.get(requestK).get(senderAddressK));
		recipients.add(recipient);
		return recipients;
	}
	
	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}

	public void setSmsMtChannel(SmsMtChannel smsMtChannel) {
		this.smsMtChannel = smsMtChannel;
	}

	public void setUnsubscribeMessage(String unsubscribeMessage) {
		this.unsubscribeMessage = unsubscribeMessage;
	}

	public void setNormalMessage(String normalMessage) {
		this.normalMessage = normalMessage;
	}
	
}
