/*
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance.filter;

import hms.kite.governance.domain.FilterStatus;
import hms.kite.governance.repo.mongo.ManualFilterMessageRepository;

import hms.kite.util.logging.TransLogUtil;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static hms.kite.datarepo.RepositoryServiceRegistry.systemConfiguration;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: $$ $LastChangedBy: $$ $LastChangedRevision: $$
 */
public class GovernanceMessageScheduler {

	private int interval;
	private int initialDelay;
	private Channel smsChannel;
	private ManualFilterMessageRepository manualFilterMessageRepository;
    private Thread daemon;
    private boolean stopping = false;

    private static final Logger LOGGER = LoggerFactory.getLogger(GovernanceMessageScheduler.class);

	public void init() {
		LOGGER.info("Starting governance filtered message sender scheduler ....... ");
		interval = (Integer) systemConfiguration().find(governDaemonIntervalK);
		initialDelay = (Integer) systemConfiguration().find(governDaemonDelayK);
		executeScheduler();
	}

    private void executeScheduler() {
        daemon = new Thread(new Runnable() {
            public void run() {
                sleepFor(initialDelay);
                while (!Thread.interrupted() && !stopping) {
                    try {
                        sendManualFilteredMessages();
                    } finally {
                        sleepFor(interval);
                    }
                }
            }
        });
        daemon.setDaemon(true);
        daemon.start();
    }

    private void sleepFor(long interval) {
        try {
            Thread.sleep(interval * 1000 * 60);
        } catch (InterruptedException e) {
            LOGGER.debug("Thread interrupted", e);
        }
    }

    private void sendManualFilteredMessages() {
        List<Map<String, Object>> messages = manualFilterMessageRepository.find(FilterStatus.ALLOWED);
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("number of messages to be sent: {}", messages.size());
        }
        for (Map<String, Object> msg : messages) {
            putInternalFlowTransLogEnabling(msg);
            Object id = msg.remove(idK);
            smsChannel.send(msg);
            msg.put(idK, id);
            manualFilterMessageRepository.update(msg, FilterStatus.SENT);
        }
    }

    private void putInternalFlowTransLogEnabling(Map<String, Object> msg) {
		if (!isBroadcastRequest(msg)) {
			msg.put(TransLogUtil.logFromInternalWorkFlow, trueK);
			LOGGER.debug("Using internal flow translog printing.");
		} else {
			LOGGER.debug("Not using internal flow translog printing for broadcast messages.");
		}

	}

	private boolean isBroadcastRequest(Map<String, Object> msg) {
		for (Map<String, String> recipient : (List<Map<String, String>>) msg.get(recipientsK)) {
			if (recipient.get(recipientAddressK).trim().equals(allK)) {
				LOGGER.info("Message allowed by govenance is a broadcast message");
				return true;
			}
		}

		return msg.containsKey(broadcastAddressK);
	}

	public void stop() {
		stopping = true;
	}

	public void setManualFilterMessageRepository(ManualFilterMessageRepository manualFilterMessageRepository) {
		this.manualFilterMessageRepository = manualFilterMessageRepository;
	}

	public void setSmsChannel(Channel channel) {
		this.smsChannel = channel;
	}

	public void setInterval(int interval) {
		this.interval = interval;
	}

	public void setInitialDelay(int initialDelay) {
		this.initialDelay = initialDelay;
	}

}
