/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance.integration.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.control.InExpression;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.springframework.beans.factory.annotation.Autowired;

import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */
public class ReportAbuseMoWorkflow extends WrappedGeneratedWorkflow implements Workflow {

    @Autowired private Channel smsMoReplySenderChannel;
    @Autowired private Channel abuseMoReplyMapperChannel;
    @Autowired private Channel reportAbuseService;
    @Autowired private Channel provNcsSlaChannel;
    @Autowired private Channel whitelistService;
    @Autowired private Channel blacklistingService;
    @Autowired private Channel mnpService;
    @Autowired private Channel provSpSlaChannel;
    @Autowired private Channel provAppSlaChannel;
    @Autowired private Channel smsMoRoutingKeyChannel;

    @Override
    protected Workflow generateWorkflow() {
        ServiceImpl replyChannel = new ServiceImpl("sms-mo-reply-channel");
        replyChannel.setChannel(smsMoReplySenderChannel);

        ServiceImpl replyMapperChannel = new ServiceImpl("abuse-mo-reply-mapper-channel", replyChannel);
        replyMapperChannel.setChannel(abuseMoReplyMapperChannel);

        ServiceImpl abuseChannel = new ServiceImpl("abuse-channel");
        abuseChannel.setChannel(reportAbuseService);
        abuseChannel.setOnSuccess(replyMapperChannel);
        abuseChannel.onDefaultError(replyMapperChannel);

        /*
        load subscription ncs if exists - this is to check whether application is subscription type or not
         */
        ServiceImpl ncsSlaChannel = new ServiceImpl("prov.ncs.sla.channel");
        ncsSlaChannel.setChannel(provNcsSlaChannel);
        ncsSlaChannel.addServiceParameter(ncsTypeK, subscriptionK);
        ncsSlaChannel.setOnSuccess(abuseChannel);
        ncsSlaChannel.onDefaultError(abuseChannel);

        ServiceImpl whitelist = new ServiceImpl("white-list");
        whitelist.setChannel(whitelistService);
        whitelist.setOnSuccess(ncsSlaChannel);
        whitelist.onDefaultError(replyMapperChannel);

        EqualExpression appStatus = new EqualExpression(appK, statusK, limitedProductionK);
        Condition condition = new Condition(appStatus, unknownErrorCode);

        ServiceImpl whitelistApplied = new ServiceImpl("whitelist.eligibility.service", condition);
        whitelistApplied.setOnSuccess(whitelist);
        whitelistApplied.onDefaultError(ncsSlaChannel);

        ServiceImpl blacklist = new ServiceImpl("black-list");
        blacklist.setChannel(blacklistingService);
        blacklist.setOnSuccess(whitelistApplied);
        blacklist.onDefaultError(replyMapperChannel);

        ServiceImpl numberChecking = new ServiceImpl("number-checking");
        numberChecking.setChannel(mnpService);
        numberChecking.setOnSuccess(blacklist);
        numberChecking.onDefaultError(replyMapperChannel);

        ServiceImpl appStateValidationService = createAppStateValidationService();
        appStateValidationService.setOnSuccess(numberChecking);
        appStateValidationService.onDefaultError(replyMapperChannel);

        ServiceImpl spSlaValidation = createSpStateValidation();
        spSlaValidation.setOnSuccess(appStateValidationService);
        spSlaValidation.onDefaultError(replyMapperChannel);

        ServiceImpl spChannel = new ServiceImpl("prov.sp.sla.channel", spSlaValidation);
        spChannel.setChannel(provSpSlaChannel);
        spChannel.onDefaultError(replyMapperChannel);

        ServiceImpl appChannel = new ServiceImpl("prov.app.sla.channel", spChannel);
        appChannel.setChannel(provAppSlaChannel);
        appChannel.onDefaultError(replyMapperChannel);

        ServiceImpl routingKeyChannel = new ServiceImpl("sms-mo-routing-key-channel", appChannel);
        routingKeyChannel.setChannel(smsMoRoutingKeyChannel);
        routingKeyChannel.onDefaultError(replyMapperChannel);

        return new WorkflowImpl(routingKeyChannel);
    }

    private static ServiceImpl createSpStateValidation() {
        EqualExpression statusExpression = new EqualExpression(spK, statusK, approvedK);
        Condition statusCondition = new Condition(statusExpression, authenticationFailedErrorCode);

        InExpression ncsAllowedExpression = new InExpression(spK, spSelectedServicesK, new Object[]{smsK});
        Condition ncsAllowedCondition = new Condition(ncsAllowedExpression, ncsNotAllowedErrorCode);

        return new ServiceImpl("sp.state.validation.service", statusCondition, ncsAllowedCondition);
    }

    private static ServiceImpl createAppStateValidationService() {
        InExpression statusExpression = new InExpression(appK, statusK, new Object[]{limitedProductionK,
                activeProductionK});
        Condition statusCondition = new Condition(statusExpression, authenticationFailedErrorCode);

        return new ServiceImpl("app.state.validation.service", statusCondition);
    }

}
