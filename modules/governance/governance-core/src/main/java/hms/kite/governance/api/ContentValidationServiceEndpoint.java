/**
 *   (C) Copyright 2010-2013 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.governance.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.messageK;

@Produces({"application/json"})
@Consumes({"application/json"})
@Path("/governance/api")
public class ContentValidationServiceEndpoint {
    private static final Logger LOGGER = LoggerFactory.getLogger(ContentValidationServiceEndpoint.class);
    private ContentValidationService validationService;

    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    @Path("/validate")
    public Map<String, Object> validate(Map<String, Object> msg, @Context javax.servlet.http.HttpServletRequest request) {
        LOGGER.debug("Message received for content validation[{}]", msg);

        Map<String, Object> resp = validationService.validate((String) msg.get(messageK));

        LOGGER.info("Sending response[{}] to application", resp);
        return resp;
    }

    public void setValidationService(ContentValidationService validationService) {
        this.validationService = validationService;
    }
}
