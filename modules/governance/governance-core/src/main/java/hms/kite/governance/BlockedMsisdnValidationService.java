/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance;

import hms.kite.governance.repo.mongo.BlockedMsisdnRepository;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteErrorBox.recipientNotAllowedErrorCode;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $ $LastChangedRevision: 73804 $
 */
public class BlockedMsisdnValidationService extends BaseChannel { 

	private BlockedMsisdnRepository blockedMsisdnRepository;
	private static final Logger LOGGER = LoggerFactory.getLogger(BlockedMsisdnValidationService.class);
	
	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
		List<Map<String, String>> recipients = (List<Map<String, String>>) requestContext.get(requestK).get(recipientsK);
		if(recipients.size() == 1) {
			if(blockedMsisdnRepository.isBlocked((String) requestContext.get(appK).get(appIdK), 
							recipients.get(0).get(recipientAddressK))) {
				return ResponseBuilder.generate(recipientNotAllowedErrorCode, false, requestContext);
			} else {
				return ResponseBuilder.generateSuccess();
			}
		} else {
			for(Map<String, String> recipient : recipients) {
				if(blockedMsisdnRepository.isBlocked((String) requestContext.get(appK).get(appIdK), 
						recipient.get(recipientAddressK))) {
					recipient.put(recipientAddressStatusK, recipientNotAllowedErrorCode);
				}
			}
		}
		return ResponseBuilder.generateSuccess();
	}

	public void setBlockedMsisdnRepository(BlockedMsisdnRepository blockedMsisdnRepository) {
		this.blockedMsisdnRepository = blockedMsisdnRepository;
	}
	
}
