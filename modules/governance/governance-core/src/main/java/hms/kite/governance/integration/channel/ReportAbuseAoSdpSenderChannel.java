/**
 *   (C) Copyright 2011-2012 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance.integration.channel;

import hms.kite.governance.integration.ReportAbuseServiceEndpoint;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.util.KiteKeyBox.requestK;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ReportAbuseAoSdpSenderChannel implements Channel {

    private ReportAbuseServiceEndpoint serviceEndpoint;
    private static final Logger LOGGER = LoggerFactory.getLogger(ReportAbuseAoSdpSenderChannel.class);

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
    	Map<String, Object> response = serviceEndpoint.report(requestContext.get(requestK), null);
        if (null != response ) {
            return response;
        }
        return ResponseBuilder.generateResponse(requestContext);
    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        //no impl needed
        return null;
    }

    public void setServiceEndpoint(ReportAbuseServiceEndpoint serviceEndpoint) {
        this.serviceEndpoint = serviceEndpoint;
    }
}
