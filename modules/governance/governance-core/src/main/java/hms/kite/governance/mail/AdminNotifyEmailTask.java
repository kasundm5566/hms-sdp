/*
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.governance.mail;

import hms.kite.util.EmailClient;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;

/**
 * $LastChangedDate: 2011-07-20 14:01:33 +0530 (Wed, 20 Jul 2011) $
 * $LastChangedBy: danukas $ $LastChangedRevision: 75106 $
 */
public class AdminNotifyEmailTask {

    private static AbstractEmailSender emailSender;
    private static EmailClient emailClient;
    private String cronExpression;
    private StdSchedulerFactory stdSchedulerFactory;
    private Scheduler scheduler;
    private static final Logger LOGGER = LoggerFactory.getLogger(AdminNotifyEmailTask.class);

    public void init() {
        try {
            LOGGER.info("initializing governance admin notify email sending scheduler.");

            scheduler = stdSchedulerFactory.getScheduler();

            JobDetail job = new JobDetail();
            job.setName("admin-email-job");
            job.setJobClass(EmailJob.class);

            CronTrigger trigger = new CronTrigger();
            trigger.setName("admin-email-job-trigger");
            trigger.setCronExpression(cronExpression);

            Date initialRunDate = scheduler.scheduleJob(job, trigger);
            LOGGER.info("[{}] has been scheduled to run at: [{}] and repeat based on expression: [{}]", new Object[] {
                    job.getKey(), initialRunDate, trigger.getCronExpression() });
            scheduler.start();

        } catch (Exception e) {
            LOGGER.warn("scheduler initialization failed .... ", e);
        }
    }

    public void destroy() {
        try {
            LOGGER.info("interrupting job ... ");
            // scheduler.deleteJob(JobKey.jobKey(JOB_NAME));
        } catch (Exception e) {
            LOGGER.warn("couldn't interrupt job ... ", e);
        }
    }

    public static class EmailJob implements Job {

        @Override
        public void execute(JobExecutionContext context) throws JobExecutionException {

            List<EmailMessage> emails = emailSender.create();
            if (!emails.isEmpty()) {
                EmailMessage msg = emails.get(0);
                LOGGER.info("Sending email to admin .... ");
                if (!emailClient.send(msg.getTo(), msg.getFrom(), msg.getCcList(), msg.getSubject(), msg.getText())) {
                    LOGGER.error("failed to send email to admin ... ");
                }
            }
        }

    }

    public void setEmailSender(AbstractEmailSender emailSender) {
        AdminNotifyEmailTask.emailSender = emailSender;
    }

    public void setEmailClient(EmailClient emailClient) {
        AdminNotifyEmailTask.emailClient = emailClient;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public void setStdSchedulerFactory(StdSchedulerFactory stdSchedulerFactory) {
        this.stdSchedulerFactory = stdSchedulerFactory;
    }

}
