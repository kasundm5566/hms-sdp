/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance.mail;


import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */
public abstract class AbstractEmailSender {

	protected String fromAddress;
	
	public abstract List<EmailMessage> create();
	
	public abstract void update(EmailMessage emailMessage, boolean status);

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}
	
	protected abstract String createEmailBody(Map<String, String> replacements);
}
