/**
 *   (C) Copyright 2010-2013 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.governance.api.impl;

import hms.kite.governance.api.ContentValidationService;
import hms.kite.governance.repo.mongo.AbuseWordRepository;
import hms.kite.wfengine.impl.ResponseBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteErrorBox.messageContainAbusiveContentErrorCode;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.KiteKeyBox.statusDescriptionK;
import static hms.kite.util.KiteKeyBox.statusK;

public class ContentValidationServiceImpl implements ContentValidationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ContentValidationServiceImpl.class);

    private AbuseWordRepository abuseWordRepository;

    public Map<String, Object> validate(String s) {

        if(s == null || s.isEmpty()){
           return  ResponseBuilder.generateSuccess();
        } else {
              String [] words =  s.split("\\s");
            List<String> abusiveWords = abuseWordRepository.filterAbusiveWords(Arrays.asList(words));
            LOGGER.info("Found abusive content[{}]", abusiveWords);
            if(abusiveWords.isEmpty()){
                return ResponseBuilder.generateSuccess();
            } else {
                return generateAbusiveContentResponse(abusiveWords);
            }
        }
    }

    private Map<String, Object> generateAbusiveContentResponse(List<String> abusiveWords) {
        Map<String, Object> resp = new HashMap<String, Object>();
        resp.put(statusK, Boolean.toString(false));
        resp.put(statusCodeK, messageContainAbusiveContentErrorCode);
        resp.put(statusDescriptionK, "Message Contains Abusive Content");
        resp.put(abuseWordsK, abusiveWords.toArray());
        return resp;
    }

    public void setAbuseWordRepository(AbuseWordRepository abuseWordRepository) {
        this.abuseWordRepository = abuseWordRepository;
    }
}
