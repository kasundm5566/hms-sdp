/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.governance.repo.mongo;

import static hms.kite.util.KiteKeyBox.appIdK;
import static hms.kite.util.KiteKeyBox.correlationIdK;
import static hms.kite.util.KiteKeyBox.msisdnK;
import hms.kite.datarepo.mongodb.MongoDbUtils;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $ $LastChangedRevision: 73804 $
 */
public class BlockedMsisdnRepository extends MongoDbUtils {

	private static final String bLOCKED_MSISDN_COLLECTION = "blocked_msisdn";
	
	private DBCollection collection;
	
	public void block(String appId, String msisdn, String correlationId) {
		BasicDBObject document = new BasicDBObject();
		document.put(appIdK, appId);
		document.put(msisdnK, msisdn);
		document.put(correlationIdK, correlationId);
		getBlockedMsisdnCollection().save(document);
	}
	
	public boolean isBlocked(String appId, String msisdn) {
		BasicDBObject document = new BasicDBObject();
		document.put(appIdK, appId);
		document.put(msisdnK, msisdn);
		return (getBlockedMsisdnCollection().count(document) != 0);
	}
	
	private DBCollection getBlockedMsisdnCollection() {
		if(collection == null) {
			collection = mongoTemplate.getCollection(bLOCKED_MSISDN_COLLECTION);
			collection.ensureIndex(appIdK);
		}
		return collection;
	}
}
