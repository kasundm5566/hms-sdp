/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hms.kite.util.EmailClient;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $ $LastChangedRevision: 73804 $
 */
public class EmailSenderScheduler {

	private long interval;
	private long initialDelay;
	private AbstractEmailSender emailSender;
	private EmailClient emailClient;
    private Thread daemon;
    private boolean stopping = false;

	private static final Logger LOGGER = LoggerFactory.getLogger(EmailSenderScheduler.class);

	public void init() {
		try {
			LOGGER.info("starting email schedular ..... ");
			execute();
			LOGGER.info("started email schedular ..... ");
		} catch (Throwable t) {
			LOGGER.error("failed to initiate email scheduler ", t);
		}
	}

    private void execute() {
        daemon = new Thread(new Runnable() {
            public void run() {
                sleepFor(initialDelay);
                while (!Thread.interrupted() && !stopping) {
                    try {
                        sendMails();
                    } finally {
                        sleepFor(interval);
                    }
                }
            }
        });
        daemon.setDaemon(true);
        daemon.start();
    }

    private void sendMails() {
        List<EmailMessage> emailMessages = emailSender.create();
        LOGGER.trace("Number of emails to send [{}]", emailMessages.size());
        for (EmailMessage message : emailMessages) {
            if (emailClient.send(message.getTo(), message.getFrom(), message.getCcList(), message.getSubject(),
                    message.getText())) {
                emailSender.update(message, false);
            }
        }
    }

    private void sleepFor(long interval) {
        try {
            Thread.sleep(interval * 1000 * 60);
        } catch (InterruptedException e) {
            LOGGER.debug("Thread interrupted", e);
        }
    }

	public void stop() {
		stopping = true;
	}

	public void setInterval(long interval) {
		this.interval = interval;
	}

	public void setInitialDelay(long initialDelay) {
		this.initialDelay = initialDelay;
	}

	public void setEmailSender(AbstractEmailSender emailSender) {
		this.emailSender = emailSender;
	}

	public void setEmailClient(EmailClient emailClient) {
		this.emailClient = emailClient;
	}

}
