/**
 *   (C) Copyright 2011-2012 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance.integration.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.springframework.beans.factory.annotation.Autowired;

import static hms.kite.util.KiteErrorBox.unknownErrorCode;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ReportAbuseAoSdpWorkflow  extends WrappedGeneratedWorkflow implements Workflow {

    @Autowired private Channel reportAbuseService;
    @Autowired private Channel provNcsSlaChannel;
    @Autowired private Channel whitelistService;
    @Autowired private Channel blacklistingService;
    @Autowired private Channel mnpService;
    @Autowired private Channel provSpSlaChannel;
    @Autowired private Channel provAppSlaChannel;

    @Override
    protected Workflow generateWorkflow() {
        ServiceImpl abuseChannel = new ServiceImpl("abuse-channel");
        abuseChannel.setChannel(reportAbuseService);

        /*
        load subscription ncs if exists - this is to check whether application is subscription type or not
         */
        ServiceImpl ncsSlaChannel = new ServiceImpl("prov.ncs.sla.channel");
        ncsSlaChannel.setChannel(provNcsSlaChannel);
        ncsSlaChannel.addServiceParameter(ncsTypeK, subscriptionK);
        ncsSlaChannel.setOnSuccess(abuseChannel);
        ncsSlaChannel.onDefaultError(abuseChannel);

        ServiceImpl whitelist = new ServiceImpl("white-list");
        whitelist.setChannel(whitelistService);
        whitelist.setOnSuccess(ncsSlaChannel);

        EqualExpression appStatus = new EqualExpression(appK, statusK, limitedProductionK);
        Condition condition = new Condition(appStatus, unknownErrorCode);

        ServiceImpl whitelistApplied = new ServiceImpl("whitelist.eligibility.service", condition);
        whitelistApplied.setOnSuccess(whitelist);
        whitelistApplied.onDefaultError(ncsSlaChannel);

        ServiceImpl blacklist = new ServiceImpl("black-list");
        blacklist.setChannel(blacklistingService);
        blacklist.setOnSuccess(whitelistApplied);

        ServiceImpl numberChecking = new ServiceImpl("number-checking");
        numberChecking.setChannel(mnpService);
        numberChecking.setOnSuccess(blacklist);

        ServiceImpl spSlaChannel = new ServiceImpl("prov.sp.sla.channel", numberChecking);
        spSlaChannel.setChannel(provSpSlaChannel);

        ServiceImpl appSlaChannel = new ServiceImpl("prov.app.sla.channel", spSlaChannel);
        appSlaChannel.setChannel(provAppSlaChannel);

        return new WorkflowImpl(appSlaChannel);
    }

}
