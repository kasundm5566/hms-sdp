/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.governance.repo.mongo;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import hms.kite.datarepo.mongodb.MongoDbUtils;
import hms.kite.governance.domain.FilterStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.correlationIdK;
import static hms.kite.util.KiteKeyBox.idK;
/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $ $LastChangedRevision: 73804 $
 */

public class ManualFilterMessageRepository extends MongoDbUtils {

	private static final String MANUAL_FILTER_MESSAGE_COLLECTION = "manual_filter_message";
	private static final String FILTER_STATUS = "filter-status";
	private DBCollection collection;

	public void add(Map<String, Object> message) {
		BasicDBObject query = new BasicDBObject(message);
		query.put(FILTER_STATUS, FilterStatus.PENDING.name());
		getManualFilterMessageCollection().save(query);
	}

	public List<Map<String, Object>> find(FilterStatus status) {
		BasicDBObject query = new BasicDBObject(FILTER_STATUS, status.name());
		DBCursor cursor = getManualFilterMessageCollection().find(query);
		List<Map<String, Object>> result = new ArrayList<Map<String,Object>>();
		while(cursor.hasNext()) {
			DBObject record = cursor.next();
			result.add(record.toMap());
		}
		return result;
	}

    public List<Map<String, Object>> find(FilterStatus status, int start, int batchSize) {
		BasicDBObject query = new BasicDBObject(FILTER_STATUS, status.name());
		DBCursor cursor = getManualFilterMessageCollection().find(query).limit(batchSize).skip(start);
		List<Map<String, Object>> result = new ArrayList<Map<String,Object>>();
		while(cursor.hasNext()) {
			DBObject record = cursor.next();
			result.add(record.toMap());
		}
		return result;
	}

	public Map<String, Object> findByCorrelationId(String id) {
		BasicDBObject query = new BasicDBObject(correlationIdK, id);
		DBObject result = getManualFilterMessageCollection().findOne(query);
		return ((result != null) ? result.toMap() : null);
	}

	public void remove(Map<String, Object> message) { // TODO: test case
		BasicDBObject query = new BasicDBObject(message);
		getManualFilterMessageCollection().remove(query);
	}

	public void update(Map<String, Object> message, FilterStatus status) { // TODO: test case
		BasicDBObject query = new BasicDBObject(idK, message.get(idK));
		BasicDBObject update = new BasicDBObject(FILTER_STATUS, status.name());
		getManualFilterMessageCollection().update(query, new BasicDBObject("$set", update));
	}

	public long countPending() {
		BasicDBObject query = new BasicDBObject(FILTER_STATUS, FilterStatus.PENDING.name());
		return getManualFilterMessageCollection().count(query);
	}

	private DBCollection getManualFilterMessageCollection() {
		if(collection == null) {
			collection = mongoTemplate.getCollection(MANUAL_FILTER_MESSAGE_COLLECTION);
			BasicDBObject indices = new BasicDBObject();
			indices.put(correlationIdK, 1);
			indices.put(FILTER_STATUS, 1);
			collection.ensureIndex(indices);
		}
		return collection;
	}
}
