package hms.kite.governance.mail;

import static hms.common.registration.api.util.RestApiKeys.EMAIL;
import static hms.kite.util.KiteKeyBox.coopUserIdK;
import static hms.kite.util.KiteKeyBox.correlationIdK;
import static hms.kite.util.KiteKeyBox.messageK;
import static hms.kite.util.KiteKeyBox.nameK;
import hms.common.registration.api.response.BasicUserResponseMessage;
import hms.kite.governance.repo.mongo.AbuseReportRepository;
import hms.kite.util.RegistrationClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.velocity.VelocityEngineUtils;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $ $LastChangedRevision: 73804 $
 *
 */
public class SpNotifyEmailSender extends AbstractEmailSender {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpNotifyEmailSender.class);
	private static final String APP_PLACE_HOLDER = "app";
	private static final String TEXT_PLACE_HOLDER = "text";
	private static final String SP_NOTIFIED = "sp-notified";
	private static final String NOTIFY_SP = "notify-sp";

	private AbuseReportRepository abuseReportRepository;
	private RegistrationClient registrationClient;
	private VelocityEngine velocityEngine;
	private String emailSubject;
	private String templateLocation;

	@Override
	public List<EmailMessage> create() {
		List<Map<String, String>> messageList = abuseReportRepository.findNotificationList();
		List<EmailMessage> mailList = new ArrayList<EmailMessage>();
		for (Map<String, String> message : messageList) {
			try {
				LOGGER.debug("message pameters to create email [{}]", message);
				BasicUserResponseMessage user = registrationClient.getUserInfoByUserId(message.get(coopUserIdK));
				Map<String, String> replacements = new HashMap<String, String>();
				replacements.put(APP_PLACE_HOLDER, message.get(nameK));
				replacements.put(TEXT_PLACE_HOLDER, message.get(messageK));
				final String emailText = createEmailBody(replacements);
				EmailMessage eMessage = new EmailMessage(user.getAdditionalData(EMAIL), fromAddress, emailText);
				eMessage.setCorrelationId(message.get(correlationIdK));
				eMessage.setAppName(message.get(nameK));
				eMessage.setSubject(emailSubject);
				mailList.add(eMessage);
			} catch (Exception ex) {
				LOGGER.error("Error occured while creating sp email message for manual filtering message[{}]", message);
			}
		}
		LOGGER.trace("mailing list [{}]", mailList);
		return mailList;
	}

	@Override
	public void update(EmailMessage emailMessage, boolean status) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(correlationIdK, emailMessage.getCorrelationId());
		map.put(SP_NOTIFIED, true);
		map.put(NOTIFY_SP, Boolean.toString(status));
		abuseReportRepository.update(map);
	}

	protected String createEmailBody(Map<String, String> replacements) {
		Map<String, String> model = new HashMap<String, String>();
		model.put(APP_PLACE_HOLDER, replacements.get(APP_PLACE_HOLDER));
		model.put(TEXT_PLACE_HOLDER, replacements.get(TEXT_PLACE_HOLDER));
		LOGGER.debug("parameters to be set in the email [{}]", model);
		return VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, templateLocation, model);
	}

	public void setAbuseReportRepository(AbuseReportRepository abuseReportRepository) {
		this.abuseReportRepository = abuseReportRepository;
	}

	public void setRegistrationClient(RegistrationClient registrationClient) {
		this.registrationClient = registrationClient;
	}

	public void setVelocityEngine(VelocityEngine velocityEngine) {
		this.velocityEngine = velocityEngine;
	}

	public void setTemplateLocation(String templateLocation) {
		this.templateLocation = templateLocation;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

}
