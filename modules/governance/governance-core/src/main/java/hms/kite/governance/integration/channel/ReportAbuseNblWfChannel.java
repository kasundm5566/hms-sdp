/*
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance.integration.channel;

import static hms.kite.util.KiteErrorBox.invalidRequestErrorCode;
import static hms.kite.util.KiteKeyBox.appIdK;
import static hms.kite.util.KiteKeyBox.applicationIdK;
import static hms.kite.util.KiteKeyBox.correlationIdK;
import static hms.kite.util.KiteKeyBox.mediumK;
import static hms.kite.util.KiteKeyBox.messageIdNblK;
import static hms.kite.util.KiteKeyBox.messageK;
import static hms.kite.util.KiteKeyBox.requestK;
import static hms.kite.util.KiteKeyBox.senderAddressK;
import static hms.kite.util.KiteKeyBox.senderAddressNblK;
import static hms.kite.util.KiteKeyBox.webK;
import hms.kite.util.IdGenerator;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;

import java.util.Map;

import org.apache.log4j.NDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ReportAbuseNblWfChannel extends BaseChannel {

    private static final Logger logger = LoggerFactory.getLogger(ReportAbuseNblWfChannel.class);

    private Workflow abuseReportNblWf;

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        try {
            if(!validate(requestContext)) {
                logger.warn("Validation failed for the request [{}]", requestContext.get(requestK));
                return convert2NblErrorResponse(ResponseBuilder.generate(invalidRequestErrorCode, false, requestContext));
            }
            convert2NblParameters(requestContext);
            NDC.push((String) requestContext.get(requestK).get(correlationIdK));
            abuseReportNblWf.executeWorkflow(requestContext);
            return generateResponse(requestContext);
        } catch (Exception e) {
            logger.error("Exception occurred while executing flow", e);
            return convert2NblErrorResponse(ResponseBuilder.generate(requestContext, e));
        } finally {
            NDC.pop();
        }
    }

    private Map<String, Object> generateResponse(Map<String, Map<String, Object>> requestContext) {
        return convert2NblErrorResponse(ResponseBuilder.generateResponse(requestContext));
    }

    private Map<String, Object> convert2NblErrorResponse(Map<String, Object> response) {
    	Map<String, Object> nblResp = ResponseBuilder.convert2NblErrorResponse(response);
        nblResp.put(messageIdNblK, response.get(correlationIdK));
        return nblResp;
    }

    private void convert2NblParameters(Map<String, Map<String, Object>> requestContext) {
        requestContext.get(requestK).put(correlationIdK, IdGenerator.generate());
        requestContext.get(requestK).put(appIdK, requestContext.get(requestK).get(applicationIdK));
        requestContext.get(requestK).put(senderAddressK, requestContext.get(requestK).get(senderAddressNblK));
        requestContext.get(requestK).put(mediumK, webK);
    }

    private boolean validate(Map<String, Map<String, Object>> requestContext) {
        return (validAppName(requestContext.get(requestK))
        		&& validSenderAddress(requestContext.get(requestK))
        		&& validMessage(requestContext.get(requestK)));
    }

    private boolean validAppName(Map<String, Object> request) {
    	if(isValid(request, applicationIdK)) {
    		return true;
    	}
    	logger.warn("Invalid application id [{}]", request.get(applicationIdK));
    	return false;
    }

    private boolean validSenderAddress(Map<String, Object> request) {
    	if(isValid(request, senderAddressNblK)) {
    		return true;
    	}
    	logger.warn("Invalid sender address [{}]", request.get(senderAddressNblK));
    	return false;
    }

    private boolean validMessage(Map<String, Object> request) {
    	if(isValid(request, messageK)) {
    		return true;
    	}
    	logger.warn("Invalid message [{}]", request.get(messageK));
    	return false;
    }

    private boolean isValid(Map<String, Object> request, String key) {
    	return (request.containsKey(key)
    			&& request.get(key) != null
    			&& !request.get(key).toString().isEmpty());
    }

    public void setAbuseReportNblWf(Workflow abuseReportNblWf) {
        this.abuseReportNblWf = abuseReportNblWf;
    }
}
