/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance.domain;

/**
 * $LastChangedDate: 2011-05-24 10:33:14 +0530 (Tue, 24 May 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73286 $
 */
public enum AbuseReportStatus {

	VALID, INVALID, PENDING
}
