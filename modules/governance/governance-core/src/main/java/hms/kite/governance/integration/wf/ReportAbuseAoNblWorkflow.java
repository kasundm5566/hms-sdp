/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance.integration.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.control.InExpression;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ReportAbuseAoNblWorkflow extends WrappedGeneratedWorkflow implements Workflow {

    @Autowired private Channel internalHostResolverChannel;

    @Autowired private Channel abuseReportAoSdpSenderChannel;
    @Autowired private Channel provSpSlaChannel;
    @Autowired private Channel provAppSlaChannel;


    @Override
    protected Workflow generateWorkflow() {

        ServiceImpl internalHostResolver = new ServiceImpl("internal-host-resolver-channel");
        internalHostResolver.setChannel(internalHostResolverChannel);
        internalHostResolver.setOnSuccess(generateInternalWf());
        internalHostResolver.onDefaultError(generateExternalWf());

        return new WorkflowImpl(internalHostResolver);
    }

    private ServiceImpl generateInternalWf() {
        ServiceImpl subscriptionChannel = new ServiceImpl("abuse-ao-sdp");
        subscriptionChannel.setChannel(abuseReportAoSdpSenderChannel);

        ServiceImpl appStateValidationService = createAppStateValidationService();
        appStateValidationService.setOnSuccess(subscriptionChannel);

        ServiceImpl spSlaValidation = createSpStateValidation();
        spSlaValidation.setOnSuccess(appStateValidationService);

        ServiceImpl spSlaChannel = new ServiceImpl("prov.sp.sla.channel", spSlaValidation);
        spSlaChannel.setChannel(provSpSlaChannel);

        ServiceImpl appSlaChannel = new ServiceImpl("prov.app.sla.channel", spSlaChannel);
        appSlaChannel.setChannel(provAppSlaChannel);

        return appSlaChannel;
    }

    private ServiceImpl generateExternalWf() {
        ServiceImpl subscriptionChannel = new ServiceImpl("abuse-ao-sdp");
        subscriptionChannel.setChannel(abuseReportAoSdpSenderChannel);

        ServiceImpl appStateValidationService = createAppStateValidationService();
        appStateValidationService.setOnSuccess(subscriptionChannel);

        ServiceImpl spSlaValidation = createSpStateValidation();
        spSlaValidation.setOnSuccess(appStateValidationService);

        ServiceImpl authenticationService = createAuthenticationService();
        authenticationService.setOnSuccess(spSlaValidation);

        ServiceImpl hostValidation = createHostValidationService();
        hostValidation.setOnSuccess(authenticationService);

        ServiceImpl spSlaChannel = new ServiceImpl("prov.sp.sla.channel", hostValidation);
        spSlaChannel.setChannel(provSpSlaChannel);

        ServiceImpl appSlaChannel = new ServiceImpl("prov.app.sla.channel", spSlaChannel);
        appSlaChannel.setChannel(provAppSlaChannel);

        return appSlaChannel;
    }

    private static ServiceImpl createAppStateValidationService() {
        InExpression statusExpression = new InExpression(appK, statusK, new Object[]{limitedProductionK,
                activeProductionK});
        Condition statusCondition = new Condition(statusExpression, authenticationFailedErrorCode);

        return new ServiceImpl("app.state.validation.service", statusCondition);
    }

    private static ServiceImpl createHostValidationService() {
        InExpression hostExpression = new InExpression(requestK, remotehostK, appK, allowedHostsK);
        Condition condition = new Condition(hostExpression, invalidHostIpErrorCode);

        return new ServiceImpl("host.validation.service", condition);
    }

    private static ServiceImpl createAuthenticationService() {
        EqualExpression authenticationExpression = new EqualExpression(requestK, passwordK, appK, passwordK);
        Condition condition = new Condition(authenticationExpression, authenticationFailedErrorCode);

        return new ServiceImpl("authentication.service", condition);
    }

    private static ServiceImpl createSpStateValidation() {
        EqualExpression statusExpression = new EqualExpression(spK, statusK, approvedK);
        Condition statusCondition = new Condition(statusExpression, authenticationFailedErrorCode);

        InExpression ncsAllowedExpression = new InExpression(spK, spSelectedServicesK, new Object[]{smsK});
        Condition ncsAllowedCondition = new Condition(ncsAllowedExpression, ncsNotAllowedErrorCode);

        return new ServiceImpl("sp.state.validation.service", statusCondition, ncsAllowedCondition);
    }

    @Override
    public void executeWorkflow(Map<String, Map<String, Object>> requestContext) {
        super.executeWorkflow(requestContext);
    }
}
