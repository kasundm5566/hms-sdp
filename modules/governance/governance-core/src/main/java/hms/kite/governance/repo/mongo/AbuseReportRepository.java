package hms.kite.governance.repo.mongo;

/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

import com.mongodb.*;

import hms.kite.datarepo.mongodb.MongoDbUtils;
import hms.kite.governance.domain.AbuseReportStatus;

import java.util.*;
import java.util.Map.Entry;

import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $ $LastChangedRevision: 73804 $
 */
public class AbuseReportRepository extends MongoDbUtils {

	private static final String ABUSE_REPORT_COLLECTION = "abuse_report";
	private static final String ABUSE_REPORT_STATUS = "report_status";
	private static final String RECEIVED_TIME = "received_time";
	private static final String NOTIFY_SP  = "notify-sp";
	private static final String SP_NOTIFIED = "sp-notified";

	private DBCollection collection;

	public void create(Map<String, Object> map) {
		map.put(ABUSE_REPORT_STATUS, AbuseReportStatus.PENDING.name());
		BasicDBObject query = new BasicDBObject(map);
		query.put(RECEIVED_TIME, new Date());
        query.put(SP_NOTIFIED, false);
//		query.put(NOTIFY_SP, Boolean.toString(true));    // TODO: remove this line ... added to test email sending
		getAbuseReportCollection().save(query);
	}

	public void update(Map<String, Object> map) {
		BasicDBObject query = new BasicDBObject(correlationIdK, map.get(correlationIdK));
		BasicDBObject update = new BasicDBObject();
		Iterator<Entry<String, Object>> ite = map.entrySet().iterator();
		while (ite.hasNext()) {
			Entry<String, Object> entry = ite.next();
			if (entry.getKey().equals(correlationIdK)) {
				continue;
			}
			update.put(entry.getKey(), entry.getValue());
		}
		getAbuseReportCollection().update(query,
				new BasicDBObject("$set", update), false, true);
	}

	public List<Map<String, String>> findNotificationList() {
		BasicDBObject query = new BasicDBObject();
		query.put(NOTIFY_SP, Boolean.toString(true));  // XXX: This parameter has to be set in the abuse report.
        query.put(SP_NOTIFIED, false);
		DBCursor cursor = getAbuseReportCollection().find(query, resultFields());
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		while (cursor.hasNext()) {
			Map<String, String> map = (Map<String, String>) cursor.next().toMap();
			list.add(map);
		}
		return list;
	}

	public List<Map<String, String>> find(String appId, AbuseReportStatus status) {
		BasicDBObject query = new BasicDBObject();
		query.put(appIdK, appId);
		query.put(ABUSE_REPORT_STATUS, status.name());
		DBCursor cursor = getAbuseReportCollection().find(query);
		List<Map<String, String>> result = new ArrayList<Map<String, String>>();
		while (cursor.hasNext()) {
			result.add((Map<String, String>) cursor.next().toMap());
		}
		return result;
	}

	public int noOfReports(String appId) {
		BasicDBObject query = new BasicDBObject(appIdK, appId);
		return ((int) getAbuseReportCollection().count(query));
	}
	
	public List<Map<String, Object>> find(String appId, AbuseReportStatus status, int last, int batchSize) {
		BasicDBObject query = new BasicDBObject(appIdK, appId);
        query.put(ABUSE_REPORT_STATUS, status.name());
//		DBCursor cursor = getAbuseReportCollection().find(query, resultFields(), last, batchSize).sort(new BasicDBObject("_id", 1));
		DBCursor cursor = getAbuseReportCollection().find(query).sort(new BasicDBObject("_id", 1)).skip(last).limit(batchSize);
		List<Map<String, Object>> result = new ArrayList<Map<String,Object>>();
		while(cursor.hasNext()) {
			result.add(cursor.next().toMap());
		}
		return result;
	}
	
	private DBObject resultFields() {
		BasicDBObject keys = new BasicDBObject();
		keys.put(spIdK, 1);
		keys.put(sourceAddressNblK, 1);
		keys.put(appIdK, 1);
		keys.put(messageK, 1);
		keys.put(correlationIdK, 1);
		keys.put(NOTIFY_SP, 1); 
		keys.put("source", 1); 
		keys.put(spIdK, 1);
		keys.put(spNameK, 1);
		keys.put(nameK, 1);
		keys.put(coopUserIdK, 1);
		return keys;
	}
	
	private DBCollection getAbuseReportCollection() {
		if (collection == null) {
			collection = mongoTemplate.getCollection(ABUSE_REPORT_COLLECTION);
		}
		return collection;
	}
}
