/**
 *   (C) Copyright 2010-2013 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.governance.repo.mongo;

import com.mongodb.*;
import hms.kite.datarepo.mongodb.MongoDbUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AbuseWordRepository extends MongoDbUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbuseWordRepository.class);
    public static final String abuse_values = "value";
    private static final String ABUSE_WORDS = "abuse_words";
    private DBCollection collection;

    private DBCollection getAbuseWordsCollection() {
        if (collection == null) {
            collection = mongoTemplate.getCollection(ABUSE_WORDS);
        }
        return collection;
    }

    public Map<String, Object> findAbuseWords(String abuseWord) {
        BasicDBObject keys = new BasicDBObject();
        keys.put(abuse_values, abuseWord);
        DBCursor cursor = getAbuseWordsCollection().find(keys);
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        Map<String, Object> matchedMap = new HashMap<String, Object>();
        while (cursor.hasNext()) {
            //result.add((Map<String, Object>) cursor.next().toMap());
            matchedMap = (Map<String, Object>) cursor.next().toMap();
        }
        return matchedMap;
    }

    /**
     * Filter out abusive words from a given list of words.
     *
     * @param wordList - List of words
     * @return - List containing abusive words or empty if no abusive words found.
     */
    public List<String> filterAbusiveWords(List<String> wordList) {

        List<DBObject> or = new ArrayList<DBObject>();
        for (String word : wordList) {
            or.add(QueryBuilder.start(abuse_values).is(word).get());
        }

        QueryBuilder qb = new QueryBuilder().or(or.toArray(new DBObject[or.size()]));
        LOGGER.debug("Generated query[{}]", qb.get());

        DBCursor cursor = getAbuseWordsCollection().find(qb.get());
        List<String> abusiveWords = new ArrayList<String>();
        while (cursor.hasNext()) {
            String word = (String) cursor.next().toMap().get(abuse_values);
            if (word != null) {
                abusiveWords.add(word);
            }
        }
        return abusiveWords;
    }

    public List<String> getAbuseWords(List<Map<String, Object>> maps) {
        List<String> words = new ArrayList<String>();
        for (int i = 0; i < maps.size(); i++) {
            words.add((String) maps.get(i).get(abuse_values));
        }
        return words;
    }
}
