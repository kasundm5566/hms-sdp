/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.governance.filter;

import hms.kite.governance.repo.mongo.AbuseWordRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $ $LastChangedRevision: 73804 $
 */
public class RestrictedWordService { 

	//private String[] keyList;
    private AbuseWordRepository abuseWordRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(RestrictedWordService.class);

	/*public void setKeyList(String[] keyList) {
		Arrays.sort(keyList);
		this.keyList = keyList;
	}*/
	
	public boolean find(String key) {
		//return (Arrays.binarySearch(keyList, key) >= 0);
        LOGGER.debug("Keyword to be find from the repo : {}", key);
        Map<String, Object> word = abuseWordRepository.findAbuseWords(key);
        LOGGER.debug("Received object [{}] with word [{}]", word, key);
        if(key.equalsIgnoreCase((String)word.get(AbuseWordRepository.abuse_values)))    {
            return true;
        }

        return false;
	}

	public boolean isAbuse(String message) {
		StringTokenizer st = new StringTokenizer(message, " ");
		while(st.hasMoreElements()) {
			if(find(st.nextToken().toLowerCase())) {
				return true;
			}
		}
		return false;
	}

    public AbuseWordRepository getAbuseWordRepository() {
        return abuseWordRepository;
    }

    public void setAbuseWordRepository(AbuseWordRepository abuseWordRepository) {
        this.abuseWordRepository = abuseWordRepository;
    }
}
