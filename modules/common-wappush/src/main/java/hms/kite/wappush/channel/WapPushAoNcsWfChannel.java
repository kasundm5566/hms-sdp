/*
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.wappush.channel;

import static hms.kite.util.KiteErrorBox.unknownErrorCode;
import static hms.kite.util.KiteErrorBox.systemErrorCode;
import static hms.kite.util.KiteErrorBox.partialSuccessCode;
import static hms.kite.util.KiteErrorBox.successCode;
import static hms.kite.util.KiteKeyBox.chargedAmountK;
import static hms.kite.util.KiteKeyBox.chargedCategoryK;
import static hms.kite.util.KiteKeyBox.chargingK;
import static hms.kite.util.KiteKeyBox.chargingStatusK;
import static hms.kite.util.KiteKeyBox.currencyCodeK;
import static hms.kite.util.KiteKeyBox.directionK;
import static hms.kite.util.KiteKeyBox.externalTransIdK;
import static hms.kite.util.KiteKeyBox.operatorCostK;
import static hms.kite.util.KiteKeyBox.operatorK;
import static hms.kite.util.KiteKeyBox.partyK;
import static hms.kite.util.KiteKeyBox.recipientAddressK;
import static hms.kite.util.KiteKeyBox.recipientAddressOptypeK;
import static hms.kite.util.KiteKeyBox.recipientAddressStatusK;
import static hms.kite.util.KiteKeyBox.recipientsK;
import static hms.kite.util.KiteKeyBox.requestK;
import static hms.kite.util.KiteKeyBox.statusCodeK;
import static hms.kite.util.KiteKeyBox.statusDescriptionK;
import static hms.kite.util.KiteKeyBox.statusK;
import static hms.kite.util.KiteKeyBox.trueK;
import static hms.kite.util.KiteKeyBox.typeK;
import static hms.kite.util.KiteKeyBox.unknownK;
import static hms.kite.util.KiteKeyBox.usedExchangeRatesK;
import hms.kite.util.KiteKeyBox;
import hms.kite.util.SdpException;
import hms.kite.util.SystemUtil;
import hms.kite.util.logging.KiteLog.LogField;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class WapPushAoNcsWfChannel implements Channel {

    private Workflow wapAoNcsWf;

	private static final Logger LOGGER = LoggerFactory.getLogger(WapPushAoNcsWfChannel.class);

	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
			LOGGER.debug("executing workflow with [{}]", requestContext);
			List<String> operatorList = findOperatorList(requestContext.get(requestK));
			LOGGER.info("Sending wappush to operators [{}]", operatorList);
			for(String operator: operatorList) {
				Map<String, Map<String, Object>> operatorContext = filterRecipientList(operator, requestContext);
				try {
                    wapAoNcsWf.executeWorkflow(operatorContext);
					updateContext(requestContext, operatorContext);
				} catch (SdpException e) {
	                LOGGER.error("Sdp exception occurred while executing flow", e);
	                updateContextForError(requestContext, operatorContext, e.getErrorCode());
	            } catch (Exception e) {
	                LOGGER.error("Exception occurred while executing flow", e);
	                updateContextForError(requestContext, operatorContext, systemErrorCode);
	            }

			}
			updateOverallStatusCode(requestContext);
	        return ResponseBuilder.generateResponse(requestContext);
	}

	@Override
	public Map<String, Object> send(Map<String, Object> request) {
		return null;
	}

	private void updateContext(Map<String, Map<String, Object>> requestContext, Map<String, Map<String, Object>> optContext) {
        List<Map<String, String>> optRecipientList = (List<Map<String, String>>) optContext.get(requestK)
                .get(recipientsK);
        List<Map<String, String>> recipientList = (List<Map<String, String>>) requestContext.get(requestK)
                .get(recipientsK);
        Map<String, Object> request = requestContext.get(KiteKeyBox.requestK);
		Map<String, Object> ncs = optContext.get(KiteKeyBox.ncsK);

        if (trueK.equals(optContext.get(requestK).get(statusK))) {
            for (final Map<String, String> optRecipient : optRecipientList) {
                for (Map<String, String> recipient : recipientList) {
                    if (optRecipient.get(recipientAddressK).equals(recipient.get(recipientAddressK))) {
                        recipient.put(recipientAddressStatusK, optRecipient.get(recipientAddressStatusK));
                        updateRecipientChargingData(optRecipient, recipient);
                        putChargingTypeDetailsToRecipients(request, ncs, recipient);
                    }
                }
            }
        } else {
            for (final Map<String, String> optRecipient : optRecipientList) {
                for (Map<String, String> recipient : recipientList) {
                    if (optRecipient.get(recipientAddressK).equals(recipient.get(recipientAddressK))) {
                        recipient.put(recipientAddressStatusK, (String) optContext.get(requestK).get(statusCodeK));
                        updateRecipientChargingData(optRecipient, recipient);
                        putChargingTypeDetailsToRecipients(request, ncs, recipient);
                    }
                }
            }
        }
    }

	private void putChargingTypeDetailsToRecipients(Map<String, Object> request, Map<String, Object> ncs,
			Map<String, String> recipient) {
		if (ncs != null && request != null) {
			Map<String, Object> flowDetails = (Map<String, Object>) ncs.get(request.get(directionK));
			if (flowDetails != null && flowDetails.containsKey(chargingK)) {
				Map<String, String> chargingDetails = (Map<String, String>) flowDetails.get(chargingK);
				if (chargingDetails != null) {
					recipient.put(LogField.CHARGING_TYPE.getParameter(), chargingDetails.get(typeK));
					recipient.put(LogField.CHARGE_PARTY_TYPE.getParameter(), chargingDetails.get(partyK));
				}
			}
		}
	}

	private void updateRecipientChargingData(final Map<String, String> optRecipient, Map<String, String> recipient) {
		recipient.put(currencyCodeK, optRecipient.get(currencyCodeK));
		recipient.put(usedExchangeRatesK, optRecipient.get(usedExchangeRatesK));
		recipient.put(externalTransIdK, optRecipient.get(externalTransIdK));
		recipient.put(LogField.CHARGE_AMOUNT.getParameter(), optRecipient.get(chargedAmountK));
		recipient.put(LogField.CHARGED_CATEGORY.getParameter(), optRecipient.get(chargedCategoryK));
		recipient.put(LogField.OPERATOR_COST.getParameter(), optRecipient.get(operatorCostK));
		recipient.put(LogField.EVENT_TYPE.getParameter(), optRecipient.get(chargingStatusK));
	}

    private void updateContextForError(Map<String, Map<String, Object>> requestContext, Map<String,
            Map<String, Object>> optContext, String errorCode) {
        List<Map<String, String>> optRecipientList = (List<Map<String, String>>) optContext.get(requestK)
                .get(recipientsK);
        List<Map<String, String>> recipientList = (List<Map<String, String>>) requestContext.get(requestK)
                .get(recipientsK);

        for (final Map<String, String> optRecipient : optRecipientList) {
            for (Map<String, String> recipient : recipientList) {
                if (optRecipient.get(recipientAddressK).equals(recipient.get(recipientAddressK))) {
                    recipient.put(recipientAddressStatusK, errorCode);
                }
            }
        }
    }

    private List<String> findOperatorList(Map<String, Object> request) {
        List<String> operatorList = new ArrayList<String>();

        if (request.containsKey(recipientsK)) {

            List<Map<String, String>> recipientList = (List<Map<String, String>>) request.get(recipientsK);

            for (final Map<String, String> recipient : recipientList) {
                if ( null != recipient.get(recipientAddressOptypeK) &&
                        !unknownK.equals(recipient.get(recipientAddressOptypeK)) &&
                        !operatorList.contains(recipient.get(recipientAddressOptypeK))) {
                    operatorList.add(recipient.get(recipientAddressOptypeK));
                }
            }
        }
        return operatorList;
    }

    private Map<String, Map<String, Object>> filterRecipientList(String operator, Map<String,
            Map<String, Object>> requestContext) {
        Map<String, Map<String, Object>> optContext = SystemUtil.copy(requestContext);

        List<Map<String, String>> recipientList = (List<Map<String, String>>) requestContext.get(requestK)
                .get(recipientsK);

        List<Map<String, String>> optRecipientList = new ArrayList<Map<String, String>>();

        for (final Map<String, String> recipient : recipientList) {
            if (recipient.containsKey(recipientAddressOptypeK) &&
                    operator.equals(recipient.get(recipientAddressOptypeK))) {
                optRecipientList.add(recipient);
            }
        }
        optContext.get(requestK).put(recipientsK, optRecipientList);
        optContext.get(requestK).put(operatorK, operator);
        return optContext;
    }

	private void updateOverallStatusCode(Map<String, Map<String, Object>> requestContext) {
        if (isRequestFailed(requestContext)) {
            return;
        }

        String overallStatus = findOverallStatusCodeForDestinations(requestContext);
        requestContext.get(requestK).put(statusCodeK, overallStatus);
        requestContext.get(requestK).put(statusDescriptionK, ResponseBuilder.getErrorDescription(overallStatus,
                requestContext));
    }

    private boolean isRequestFailed(Map<String, Map<String, Object>> requestContext) {
        return !successCode.equals(requestContext.get(requestK).get(statusCodeK)) ;
    }

    private String findOverallStatusCodeForDestinations(Map<String, Map<String, Object>> requestContext) {
        if (isAllDestinationsSuccess(requestContext)) {
            return successCode;
        } else {
            List<String> failureCodes = getAllDestinationErrorCodes(requestContext);
            if (isAllFailed(requestContext, failureCodes)) {
                if (isSameErrorCode(failureCodes)) {
                    return failureCodes.get(0);
                } else {
                    return unknownErrorCode;
                }
            } else {
                return partialSuccessCode;
            }
        }
    }

    private boolean isSameErrorCode(List<String> failureCodes) {
        String previousCode = null;
        for(String errorCode : failureCodes) {
            if (null == previousCode) {
                previousCode = errorCode;
            } else if (!previousCode.equals(errorCode)) {
                return false;
            }
        }
        return true;
    }

    private boolean isAllFailed(Map<String, Map<String, Object>> requestContext, List<String> failureCodes) {
        if (failureCodes.size() == ((List<Map<String, String>>) requestContext.get(requestK).get(recipientsK)).size()) {
            return true;
        }

        return false;
    }

    private List<String> getAllDestinationErrorCodes(Map<String, Map<String, Object>> requestContext) {
        List<String> errorCodes = new ArrayList<String>();

        for (Map<String, String> recipient : (List<Map<String, String>>) requestContext.get(requestK).get(recipientsK)) {
            if(!successCode.equals(recipient.get(recipientAddressStatusK))) {
                errorCodes.add(recipient.get(recipientAddressStatusK));
            }
        }

        return errorCodes;
    }

    private boolean isAllDestinationsSuccess(Map<String, Map<String, Object>> requestContext) {
        for (Map<String, String> recipient : (List<Map<String, String>>) requestContext.get(requestK).get(recipientsK)) {
            if(!successCode.equals(recipient.get(recipientAddressStatusK))) {
                return false;
            }
        }

        return true;
    }

    public void setWapAoNcsWf(Workflow wapAoNcsWf) {
        this.wapAoNcsWf = wapAoNcsWf;
    }
}
