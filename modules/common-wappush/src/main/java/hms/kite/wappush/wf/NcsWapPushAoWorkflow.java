/*
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.wappush.wf;

import hms.kite.util.KeyNameSpaceResolver;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.*;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class NcsWapPushAoWorkflow extends WrappedGeneratedWorkflow implements Workflow {

	private static final Logger LOGGER = LoggerFactory.getLogger(NcsWapPushAoWorkflow.class);

    @Autowired private Channel chargingService;
    @Autowired private Channel wapMtChannel;
    @Autowired private Channel blockedMsisdnValidationService;
    @Autowired private Channel whitelistService;
    @Autowired private Channel tpsThrottlingChannel;
    @Autowired private Channel tpdThrottlingChannel;
    @Autowired private Channel mainThrottlingChannel;
    @Autowired private Channel provNcsSlaChannel;
    @Autowired private Channel subscriptionCheckChannel;

	@Override
	protected Workflow generateWorkflow() {
		ServiceImpl cancel = new ServiceImpl("commit-reservation");
        cancel.addServiceParameter(requestTypeK, rollbackK);
        cancel.setChannel(chargingService);
    	
        ServiceImpl commit = new ServiceImpl("commit-reservation");
        commit.addServiceParameter(requestTypeK, commitK);
        commit.setChannel(chargingService);

        //todo call rollback on error
        ServiceImpl wapMt = new ServiceImpl("wap-mt-channel");
        wapMt.setChannel(wapMtChannel);
        wapMt.setOnSuccess(commit);

        ServiceImpl reserve = new ServiceImpl("reserve-credit");
        reserve.setChannel(chargingService);
        reserve.addServiceParameter("request-type", "reserve-credit");
        reserve.setOnSuccess(wapMt);

        ServiceImpl blockedMsisdnChannel = new ServiceImpl("blocked-msisdn-channel");
        blockedMsisdnChannel.setChannel(blockedMsisdnValidationService);
        blockedMsisdnChannel.setOnSuccess(reserve);

        ServiceImpl whitelist = new ServiceImpl("white-list");
        whitelist.setChannel(whitelistService);
        whitelist.setOnSuccess(blockedMsisdnChannel);

        EqualExpression appStatus = new EqualExpression(appK, statusK, limitedProductionK);
        Condition condition = new Condition(appStatus, unknownErrorCode);

        ServiceImpl whitelistApplied = new ServiceImpl("whitelist.eligibility.service", condition);
        whitelistApplied.setOnSuccess(whitelist);
        whitelistApplied.onDefaultError(blockedMsisdnChannel);

        // FIXME:  ncs_sla - data structure is not supported. (have mt map in mongo collection????)

        ServiceImpl ncsTps = new ServiceImpl("tps-throttling-channel");
        ncsTps.setChannel(tpsThrottlingChannel);
        ncsTps.setOnSuccess(whitelistApplied);
        ncsTps.setServiceParameterKeys("ncs-wap-push-mt");

        ServiceImpl spTps = new ServiceImpl("tps-throttling-channel");
        spTps.setChannel(tpsThrottlingChannel);
        spTps.setOnSuccess(ncsTps);
        spTps.setServiceParameterKeys("sp-wap-push-mt");

        ServiceImpl ncsTpd = new ServiceImpl("tpd-throttling-channel");
        ncsTpd.setChannel(tpdThrottlingChannel);
        ncsTpd.setOnSuccess(spTps);
        ncsTpd.setServiceParameterKeys("ncs-wap-push-mt");

        ServiceImpl spTpd = new ServiceImpl("tpd-throttling-channel");
        spTpd.setChannel(tpdThrottlingChannel);
        spTpd.setOnSuccess(ncsTpd);
        spTpd.setServiceParameterKeys("sp-wap-push-mt");

        ServiceImpl systemTps = new ServiceImpl("main-throttling-channel");
        systemTps.setChannel(mainThrottlingChannel);
        systemTps.setOnSuccess(spTpd);
        systemTps.setServiceParameterKeys("system-tps");

        ServiceImpl subscriptionCheck = new ServiceImpl("subscription.check");
        subscriptionCheck.setChannel(subscriptionCheckChannel);
        subscriptionCheck.setOnSuccess(systemTps);

        ServiceImpl subscriptionRequiredCheck = createSubscriptionRequiredCheckService();
        subscriptionRequiredCheck.setOnSuccess(subscriptionCheck);
        subscriptionRequiredCheck.onDefaultError(systemTps);

        ServiceImpl senderAddressValidationService = createSenderAddressValidationService();
        senderAddressValidationService.setOnSuccess(subscriptionRequiredCheck);

        ServiceImpl ncsValidation = createNcsValidationService();
        ncsValidation.setOnSuccess(senderAddressValidationService);

        ServiceImpl ncsSlaChannel = new ServiceImpl("prov.ncs.sla.channel");
        ncsSlaChannel.setChannel(provNcsSlaChannel);
        ncsSlaChannel.setOnSuccess(ncsValidation);

        return new WorkflowImpl(ncsSlaChannel);
    }

    private ServiceImpl createSubscriptionRequiredCheckService() {
        EqualExpression subscriptionRequired = new EqualExpression(ncsK, subscriptionRequiredK, true);
        Condition subscriptionAppliedCondition = new Condition(subscriptionRequired, unknownErrorCode);
        return new ServiceImpl("subscription.check.validation", subscriptionAppliedCondition);
    }

    private static ServiceImpl createNcsValidationService() {
        ContainsExpression allowedExpression = new ContainsExpression(ncsK, mtK);
        Condition ncsAllowedCondition = new Condition(allowedExpression, mtNotAllowedErrorCode);
        return new ServiceImpl("ncs.state.validation", ncsAllowedCondition);
    }

    private static ServiceImpl createSenderAddressValidationService() {
        ContainsExpression senderAddressAvailable = new ContainsExpression(requestK, senderAddressK);
        NotExpression not = new NotExpression(senderAddressAvailable);
        InExpression alias = new InExpression(KeyNameSpaceResolver.nameSpacedKey(requestK, senderAddressK),
                KeyNameSpaceResolver.nameSpacedKey(ncsK, mtK, aliasingK));
        EqualExpression defaultSenderAddress = new EqualExpression(KeyNameSpaceResolver.nameSpacedKey(requestK, senderAddressK),
                KeyNameSpaceResolver.nameSpacedKey(ncsK, mtK, defaultSenderAddressK));

        OrExpression or = new OrExpression(not, alias, defaultSenderAddress);

        Condition condition = new Condition(or, invalidSourceAddressErrorCode);
        return new ServiceImpl("sender.address.validation", condition);
    }
}
