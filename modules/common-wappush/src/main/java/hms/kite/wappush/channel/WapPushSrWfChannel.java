/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hms.kite.wappush.channel;

import static hms.kite.util.KiteKeyBox.ncsTypeK;
import static hms.kite.util.KiteKeyBox.requestK;
import static hms.kite.util.KiteKeyBox.*;

import java.util.List;
import java.util.Map;

import hms.kite.util.StatusDescriptionBuilder;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hms.kite.util.SystemUtil;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public class WapPushSrWfChannel extends BaseChannel {

    private static final Logger LOGGER = LoggerFactory.getLogger(WapPushSrWfChannel.class);

    private Workflow wapPushSrWf;
    private Map<String, String> srStatusCodeMap;
    private StatusDescriptionBuilder descriptionBuilder;

    public void init(){
        descriptionBuilder = new StatusDescriptionBuilder("status-message");
        descriptionBuilder.init();
    }

	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
		LOGGER.debug("request-context received: [{}]", requestContext);
		addParameters(requestContext);
		return executeWf(requestContext, wapPushSrWf);
	}

	private Map<String, Object> executeWf(Map<String, Map<String, Object>> requestContext, Workflow wf) {
		try {
			wf.executeWorkflow(requestContext);
			return ResponseBuilder.generateResponse(requestContext);
		} catch(Exception e) {
			LOGGER.error("Error occured while executing the workflow!", e);
			return ResponseBuilder.generate(requestContext, e);
		}
	}

	private void addParameters(Map<String, Map<String, Object>> requestContext) {
		requestContext.get(requestK).put(ncsTypeK, wapPushK);
		requestContext.get(requestK).put(directionK, srrK);
        addMappedErrorCode(requestContext);
		List<Map<String, String>> recipientList = SystemUtil.generateRecipientList(requestContext.get(requestK));
		LOGGER.debug("Recipient {}", recipientList);
		requestContext.get(requestK).put(recipientsK, recipientList);
	}

    private void addMappedErrorCode(Map<String, Map<String, Object>> requestContext) {
        String status = (String) requestContext.get(requestK).get(statusK);
        String mappedStatus = srStatusCodeMap.get(status);
        if(mappedStatus == null ){
            mappedStatus = status;
        }
        requestContext.get(requestK).put(deliveryStatusK, mappedStatus);
        requestContext.get(requestK).put(deliveryStatusDescriptionK, descriptionBuilder.createStatusDescription(mappedStatus, requestContext));
    }

    public void setWapPushSrWf(Workflow wapPushSrWf) {
        this.wapPushSrWf = wapPushSrWf;
    }

    public void setSrStatusCodeMap(Map<String, String> srStatusCodeMap) {
        this.srStatusCodeMap = srStatusCodeMap;
    }
}
