/*
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.wappush.wf;

import static hms.kite.util.KiteErrorBox.unknownErrorCode;
import static hms.kite.util.KiteKeyBox.appK;
import static hms.kite.util.KiteKeyBox.maskK;
import static hms.kite.util.KiteKeyBox.maskNumberK;

import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.impl.BranchingService;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

/**
 *
 * The only parameter required to contain in the message passed in to the send(Message) is
 * message-id, which is the correlation-id of the last message passed in by the same subscriber.
 *
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */

public class SdpWapPushAoWorkflow extends WrappedGeneratedWorkflow implements Workflow {

	private final String name = "wap-ao-sdp";
	private static final Logger LOGGER = LoggerFactory.getLogger(SdpWapPushAoWorkflow.class);

    @Autowired private Channel numberMaskingChannel;
    @Autowired private Channel ncsWapAoChannel;
    @Autowired private Channel mnpService;
    @Autowired private Channel blacklistingService;
    @Autowired private Channel provSpSlaChannel;
    @Autowired private Channel provAppSlaChannel;

	@Override
	public String getName() {
		return name;
	}

	@Override
	protected Workflow generateWorkflow() {
		ServiceImpl numberMaskingChannelService = new ServiceImpl("number-masking-channel");
		numberMaskingChannelService.setChannel(numberMaskingChannel);
		numberMaskingChannelService.addServiceParameter(maskK, true);

		Condition maskingCondition = new Condition(new EqualExpression(appK, maskNumberK, true), unknownErrorCode);

		BranchingService numberMaskingOnSubmitRespService = new BranchingService("number-masking-on-submit-resp", numberMaskingChannelService,
				null, maskingCondition);

		ServiceImpl smsAoNcsChannel = new ServiceImpl("wap-ao-ncs");
        smsAoNcsChannel.setChannel(ncsWapAoChannel);
        smsAoNcsChannel.setOnSuccess(numberMaskingOnSubmitRespService);
        smsAoNcsChannel.onDefaultError(numberMaskingOnSubmitRespService);

        ServiceImpl numberChecking = new ServiceImpl("number-checking");
        numberChecking.setChannel(mnpService);
        numberChecking.setOnSuccess(smsAoNcsChannel);

        ServiceImpl blacklist = new ServiceImpl("black-list");
        blacklist.setChannel(blacklistingService);
        blacklist.setOnSuccess(numberChecking);

        ServiceImpl spSlaChannel = new ServiceImpl("prov.sp.sla.channel", blacklist);
        spSlaChannel.setChannel(provSpSlaChannel);

        ServiceImpl appSlaChannel = new ServiceImpl("prov.app.sla.channel", spSlaChannel);
        appSlaChannel.setChannel(provAppSlaChannel);

        return new WorkflowImpl(appSlaChannel);
	}

    @Override
    public void executeWorkflow(Map<String, Map<String, Object>> requestContext) {
        super.executeWorkflow(requestContext);
    }

}

