/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hms.kite.wappush.channel;

import hms.common.rest.util.JsonBodyProvider;
import hms.common.rest.util.client.AbstractWebClient;
import hms.kite.util.DeliveryStatusKeyMapper;
import hms.kite.util.NblKeyMapper;
import hms.kite.util.SdpException;
import hms.kite.util.SystemUtil;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.net.ConnectException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KeyNameSpaceResolver.data;
import static hms.kite.util.KeyNameSpaceResolver.nameSpacedKey;
import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.wfengine.impl.ResponseBuilder.generate;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public class WapPushSrAtChannel extends AbstractWebClient implements Channel {

	private static final Logger logger = LoggerFactory.getLogger(WapPushSrAtChannel.class);

    private Map<String, String> newNblWapPushSrKeyMapper;

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        try {
            logger.debug("RequestContext [{}]", requestContext);
            WebClient webClient = createWebclient(requestContext);
            Map<String, Object> atMessage = createMessage(requestContext);
            logger.info("Sending WapPush SR message [{}] to url[{}]", atMessage, webClient.getBaseURI());
            Response response = webClient.post(atMessage);
            if (isSuccessHttpResponse(response)) {
                truncateResponse(response);
                logger.info("Message sent successfully. response [{}]", response.getEntity());
                return ResponseBuilder.generateSuccess();
            } else {
                logger.info("Message sending failed http status is [{}]", response.getStatus());
                return generate(atMessageFailedErrorCode, false, requestContext);
            }

        } catch (ClientWebApplicationException e) {
            logger.error("Exception occurred while sending wappush sr message", e);
            if (null != e.getCause() && e.getCause() instanceof Fault) {
                Fault f = (Fault) e.getCause();
                if (null != f.getCause() ) {
                    if (f.getCause() instanceof ConnectException) {
                        return generate(appConnectionRefusedErrorCode, false, requestContext);
                    }
                }
            }
            return generate(requestContext, e);
        } catch (SdpException e) {
            throw e;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return generate(requestContext, e);
        }
    }

    private WebClient createWebclient(Map<String, Map<String, Object>> requestContext) {
        String appUrl = findAppUrl(requestContext);
        if (null == appUrl || appUrl.isEmpty()) {
            logger.warn("Delivery Report url is not available");
            throw new SdpException(systemErrorCode);
        }
        return createWebClient(appUrl);
    }

    private Map<String, Object> createMessage(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> params = new HashMap<String, Object>();
		params.put(addressK, SystemUtil.addAddressIdentifier(getRecipientAddress(requestContext)));
        params.put(timeStampK, requestContext.get(requestK).get(doneDateK));
        params.put(messageIdNblK, requestContext.get(requestK).get(correlationIdK));

        String smppStatusCode = (String) requestContext.get(requestK).get(deliveryStatusNblK);
        String mappedSdpStatusCode = DeliveryStatusKeyMapper.getSdpStatusCode(smppStatusCode);
        params.put(deliveryStatusNblK, mappedSdpStatusCode);

        NblKeyMapper nblKeyMapper = new NblKeyMapper();
        nblKeyMapper.setNewNblApiKeyMapper(newNblWapPushSrKeyMapper);
        Map<String, Object> mappedParams = nblKeyMapper.convert(params, false);
        logger.debug("WAP PUSH SR AT Message: [{}]", params);
        logger.debug("WAP PUSH SR AT Converted Message: [{}]", mappedParams);
        return mappedParams;
    }

    private String getRecipientAddress(Map<String, Map<String, Object>> requestContext) {
        List<Map<String, String>> recipients = (List<Map<String, String>>) requestContext.get(requestK).get(recipientsK);
        if (recipients.size() > 0) {
            Map<String, String> recipient = recipients.get(0);
            String recipientAddress = SystemUtil.isMaskedApp (requestContext) ? recipient.get(recipientAddressMaskedK) : recipient.get(recipientAddressK);
            return recipientAddress;
        } else {
            throw new RuntimeException("Invalid number[" + recipients.size() + "] of recipients found for sr request");
        }
    }

    private void truncateResponse(Response response) {
        try {
            readJsonResponse(response);
        } catch (Exception e) {

        }
    }

    private String findAppUrl(Map<String, Map<String, Object>> requestContext) {
        return (String) data((Map)requestContext, nameSpacedKey(ncsK, mtK, deliveryReportUrlK));
    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        return null;
    }

    public void setNewNblWapPushSrKeyMapper(Map<String, String> newNblWapPushSrKeyMapper) {
        this.newNblWapPushSrKeyMapper = newNblWapPushSrKeyMapper;
    }
}
