/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.wappush;

import hms.kite.util.NblKeyMapper;
import hms.kite.util.SdpException;
import hms.kite.util.SystemUtil;
import hms.kite.wfengine.ServiceEndpoint;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.portK;
import static hms.kite.util.KiteKeyBox.remotehostK;


@Produces({"application/x-www-form-urlencoded", "multipart/form-data"})
@Consumes({"application/x-www-form-urlencoded", "multipart/form-data"})
@Path("/wappush")
public class WapPushAoServiceEndpoint extends ServiceEndpoint {

    private static final Logger logger = LoggerFactory.getLogger(WapPushAoServiceEndpoint.class);
    private Channel channel;
    private boolean override;
    private NblKeyMapper nblKeyMapper;
    private Map<String, String> newNblWapPushKeyMapper;
    private List<String> unwantedKeysInAppResponseForWapPush;

    /**
     *
     * @param msg	--> should have the following parameters;
     * 				app-id
     * 				password
     * 				sender-address
     * 				recipient-address/multiple recipient
     * 				type - sl/si
     * 				message
     * 				url
     * 				sr - boolean
     * @param request
     * @return
     */
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    @Path("/send")
    public Map<String, Object> sendService(Map<String, Object> msg, @Context javax.servlet.http.HttpServletRequest request) {
        Map<String, Object> respNbl;
        logger.info("Received Request [{}]", msg);
        try {
            logger.debug("Received request from host " + msg.get(remotehostK) + " port : " + msg.get(portK));
            Map<String, Object> newMsg = nblKeyMapper.convert(msg, true);
            logger.debug("Received Converted Request [{}]", newMsg);
            addHostDetails(newMsg, request);
            Map<String, Object> resp = channel.send(newMsg);
            logger.debug("Response received [{}]", resp);
            respNbl = nblKeyMapper.convert(resp, false);
            logger.debug("Sending response [{}]", resp);
        } catch (SdpException ex) {
            logger.error("An exception occurred: [{}]", ex);
            respNbl = new HashMap<String, Object>();
            respNbl.put(NblKeyMapper.statusCode, ex.getErrorCode());
            respNbl.put(NblKeyMapper.statusDetail, ex.getErrorDescription());
        }
        logger.info("Sending Converted response [{}]", respNbl);
        return respNbl;
    }

    private void addHostDetails(Map<String, Object> msg, HttpServletRequest request) {
        if (override || null == msg.get(remotehostK)) {
            String host = SystemUtil.findHostIp(request);
            if (null != host)
                msg.put(remotehostK, host);
        }
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public void setNewNblWapPushKeyMapper(Map<String, String> newNblWapPushKeyMapper) {
        this.newNblWapPushKeyMapper = newNblWapPushKeyMapper;
    }

    public void setUnwantedKeysInAppResponseForWapPush(List<String> unwantedKeysInAppResponseForWapPush) {
        this.unwantedKeysInAppResponseForWapPush = unwantedKeysInAppResponseForWapPush;
    }

    public void init() {
        nblKeyMapper = new NblKeyMapper();
        nblKeyMapper.setNewNblApiKeyMapper(newNblWapPushKeyMapper);
        nblKeyMapper.setUnwantedKeysInAppResponse(unwantedKeysInAppResponseForWapPush);
    }
}
