/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.wappush.channel;

import hms.kite.util.IdGenerator;
import hms.kite.util.SystemUtil;
import hms.kite.util.TimeStampUtil;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;
import org.apache.log4j.NDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static hms.kite.util.KiteErrorBox.invalidRequestErrorCode;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.NblRequestValidator.*;

/**
 *
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $ $LastChangedRevision: 73804 $
 */

public class WapPushAoNblWfChannel extends BaseChannel {

    private Workflow wapPushAoNblWf;
    private Workflow nblWapPushBroadcastWf;

	private static final Logger logger = LoggerFactory.getLogger(WapPushAoNblWfChannel.class);

	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
		try {

			try {
				validate(requestContext);
			} catch (Exception ex) {
				logger.warn("Validation failed for the request [{}], [{}]", requestContext.get(requestK),
						ex.getMessage());
				requestContext.get(requestK).put("validation-error", ex.getMessage());
				return convert2NblErrorResponse (ResponseBuilder.generate(invalidRequestErrorCode, false, requestContext),
                        requestContext);
			}

			addParameters(requestContext);
			NDC.push((String) requestContext.get(requestK).get(correlationIdK));
			if (SystemUtil.isBroadcastRequest(requestContext)) {
				return executeBroadcastRequestWorkflow(requestContext);
			} else {
				return executeWapWorkflow(requestContext);
			}
		} finally {
			NDC.pop();
		}
	}

	private Map<String, Object> executeWapWorkflow(Map<String, Map<String, Object>> requestContext) {
		try {
			wapPushAoNblWf.executeWorkflow(requestContext);
			return generateResponseWithDestination(requestContext);
		} catch (Exception ex) {
			logger.error("Error occurred while executing the workflow!", ex);
			return convert2NblErrorResponse(ResponseBuilder.generate(requestContext, ex), requestContext);
		}
	}

	private Map<String, Object> executeBroadcastRequestWorkflow(Map<String, Map<String, Object>> requestContext) {
		try {
			nblWapPushBroadcastWf.executeWorkflow(requestContext);
			return generateNormalResponse(requestContext);
		} catch (Exception ex) {
			logger.error("Error occurred while executing the workflow!", ex);
            return convert2NblErrorResponse(ResponseBuilder.generate(requestContext, ex), requestContext);
        }
	}

	private void validate(Map<String, Map<String, Object>> requestContext) {
		validLoginDetails(requestContext.get(requestK));
		isWapPushUrlValid(requestContext.get(requestK).get(wapUrlNblK));
		isMessageValid(requestContext.get(requestK).get(messageK));
		destinationsValid(requestContext.get(requestK).get(addressK));
		isValidStatusRequest(requestContext.get(requestK).get(statusRequestK));
		isWappushTypeValid(requestContext);
	}

	private void addParameters(Map<String, Map<String, Object>> requestContext) {
		requestContext.get(requestK).put(correlationIdK, IdGenerator.generate());
		requestContext.get(requestK).put(receiveTimeK, Long.toString(System.currentTimeMillis()));
		requestContext.get(requestK).put(directionK, mtK);
		requestContext.get(requestK).put(ncsTypeK, wapPushK);
		addRecipients(requestContext.get(requestK));
		requestContext.get(requestK).put(appIdK, requestContext.get(requestK).get(applicationIdK));
		requestContext.get(requestK).put(wapUrlK, requestContext.get(requestK).get(wapUrlNblK));
		requestContext.get(requestK).put(wapPushTypeK, requestContext.get(requestK).get(wapPushTypeNblK));
		addOptionalParameters(requestContext);
	}

	private void addOptionalParameters(Map<String, Map<String, Object>> requestContext) {
		if (requestContext.get(requestK).get(sourceAddressNblK) != null
				&& !requestContext.get(requestK).get(sourceAddressNblK).toString().isEmpty()) {
			requestContext.get(requestK).put(senderAddressK, requestContext.get(requestK).get(sourceAddressNblK));
		}
		if (requestContext.get(requestK).get(versionK) == null) {
			requestContext.get(requestK).put(versionK, latestVersionK);
		}
		if (requestContext.get(requestK).get(wapPushTypeNblK) != null
				&& !requestContext.get(requestK).get(wapPushTypeNblK).toString().isEmpty()) {
			requestContext.get(requestK).put(wapPushTypeK, requestContext.get(requestK).get(wapPushTypeNblK));
		} else {
			requestContext.get(requestK).put(wapPushTypeK, serviceIndicatorK);
		}
		if (requestContext.get(requestK).get(statusRequestK) == null) {
			requestContext.get(requestK).put(statusRequestK, deliveryReportNotRequiredK);
		}
	}

	private void addRecipients(Map<String, Object> msg) {
		if (!msg.containsKey(recipientsK)) {
			msg.put(recipientsK, SystemUtil.convert2NblAddresses((List<String>) msg.get(addressK)));
		}
	}

	private Map<String, Object> generateResponseWithDestination(Map<String, Map<String, Object>> requestContext) {

		Map<String, Object> resp = generateNormalResponse(requestContext);
		List<Map<String, String>> destinations = new ArrayList<Map<String, String>>();
		for (Map<String, String> recipient : (List<Map<String, String>>) requestContext.get(requestK).get(recipientsK)) {
			Map<String, String> recipientResponseMap = new HashMap<String, String>();
            if (recipient.containsKey(recipientAddressMaskedK)) {
                recipientResponseMap.put(responseAddressK, telK + ":" + recipient.get(recipientAddressMaskedK));
            } else {
                recipientResponseMap.put(responseAddressK, telK + ":" + recipient.get(recipientAddressK));
            }
//			recipientResponseMap.put(messageIdNblK, (String) requestContext.get(requestK).get(correlationIdK));
			recipientResponseMap.put(timestampK, TimeStampUtil.formatToNblTimeStamp(new Date()));
			if (recipient.containsKey(recipientAddressStatusK)) {
				recipientResponseMap.put(statusCodeNblK, recipient.get(recipientAddressStatusK));
				recipientResponseMap.put(statusDescriptionNblK,
						ResponseBuilder.getErrorDescription(recipient.get(recipientAddressStatusK), requestContext));
			}
			destinations.add(recipientResponseMap);
		}
		resp.put(destinationResponsesNblK, destinations);

		return resp;
	}

	private Map<String, Object> generateNormalResponse(Map<String, Map<String, Object>> requestContext) {
		Map<String, Object> resp = new HashMap<String, Object>();

		if (requestContext.get(requestK).containsKey(versionK) && null != requestContext.get(requestK).get(versionK)) {
			resp.put(versionK, requestContext.get(requestK).get(versionK));
		} else {
			resp.put(versionK, "1.0");
		}

		resp.put(messageIdNblK, requestContext.get(requestK).get(correlationIdK));
		resp.put(statusCodeNblK, requestContext.get(requestK).get(statusCodeK));
		resp.put(statusDescriptionNblK, requestContext.get(requestK).get(statusDescriptionK));
		resp.put(wapUrlNblK, requestContext.get(requestK).get(wapUrlK));
		resp.put(wapPushTypeNblK, requestContext.get(requestK).get(wapPushTypeK));
		return resp;
	}

    private Map<String, Object> convert2NblErrorResponse(Map<String, Object> response,
                                                         Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> nblResp = ResponseBuilder.convert2NblErrorResponse(response);
        if (requestContext.get(requestK).containsKey(correlationIdK)) {
            nblResp.put(messageIdNblK, requestContext.get(requestK).get(correlationIdK));
        }
        return nblResp;
    }

    public void setWapPushAoNblWf(Workflow wapPushAoNblWf) {
        this.wapPushAoNblWf = wapPushAoNblWf;
    }

    public void setNblWapPushBroadcastWf(Workflow nblWapPushBroadcastWf) {
        this.nblWapPushBroadcastWf = nblWapPushBroadcastWf;
    }

}
