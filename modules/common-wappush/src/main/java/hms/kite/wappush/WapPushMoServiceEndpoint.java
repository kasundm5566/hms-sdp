/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hms.kite.wappush;

import hms.kite.wfengine.ServiceEndpoint;
import hms.kite.wfengine.transport.Channel;

import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Produces({ "application/json", "application/xml" })
@Consumes({ "application/json", "application/xml" })
@Path("/wap/")
public class WapPushMoServiceEndpoint extends ServiceEndpoint {
	private static final Logger LOGGER = LoggerFactory.getLogger(WapPushMoServiceEndpoint.class);
	protected Channel wapSrChannel;

	@Produces({ "application/json" })
	@Consumes({ "application/json" })
	@POST
	@Path("/report")
	public Map<String, Object> onSmsDr(Map<String, Object> msg) {
		LOGGER.info("Received WapPush sr from sbl [" + msg + "]");
        Map<String, Object> response = wapSrChannel.send(msg);
		LOGGER.info("Sending WapPush sr response to sbl [{}]", response);
		return response;
	}

	public void setWapSrChannel(Channel wapSrChannel) {
		this.wapSrChannel = wapSrChannel;
	}
}
