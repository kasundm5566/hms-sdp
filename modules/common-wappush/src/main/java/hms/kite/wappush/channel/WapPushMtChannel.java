/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.wappush.channel;

import hms.common.rest.util.Message;
import hms.common.rest.util.client.AbstractWebClient;
import hms.kite.datarepo.RepositoryServiceRegistry;
import hms.kite.util.GsonUtil;
import hms.kite.util.KeyNameSpaceResolver;
import hms.kite.util.MessageCorrelator;
import hms.kite.util.SdpException;
import hms.kite.exceptions.NoRecipientFoundException;
import hms.kite.wfengine.transport.Channel;
import hsenidmobile.wappush.api.WapPushMessage;
import hsenidmobile.wappush.api.WapPushMessage.ConnectionType;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.*;

import static hms.kite.datarepo.RepositoryServiceRegistry.systemConfiguration;
import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.wfengine.impl.ResponseBuilder.generate;
import static hms.kite.wfengine.impl.ResponseBuilder.generateSuccess;

/**
 * $LastChangedDate: 2011-06-17 11:39:40 +0530 (Fri, 17 Jun 2011) $
 * $LastChangedBy: dimuthu $ $LastChangedRevision: 74156 $
 */
public class WapPushMtChannel extends AbstractWebClient implements Channel {

	private Map<String, WebClient> sblClientMap;
	private MessageCorrelator correlator;
	private final static String ACCEPTED = "accepted";
	private final static String QUEUE_FULL = "queue-full";
	private final static String INVALID_REQUEST = "invalid-request";
	private static final Logger LOGGER = LoggerFactory.getLogger(WapPushMtChannel.class);
    private boolean alwaysSendServiceCode;
    private String defaultServiceCode;
    private boolean useServiceCodeShortCodeConvertion = false;

    public void init() {
        super.init();
        sblClientMap = new HashMap<String, WebClient>();
        Map<String, String> sblUrlMap = (Map<String, String>) RepositoryServiceRegistry.systemConfiguration().find(
                sblSmsReceiverAddressesK);
        for (Map.Entry<String, String> entry : sblUrlMap.entrySet()) {
            WebClient webClient = createWebClient(entry.getValue());
            sblClientMap.put(entry.getKey(), webClient);
        }
    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String, Object>>();
        requestContext.put(requestK, request);

        return execute(requestContext);
    }

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        try {
            WebClient webClient = sblClientMap.get(requestContext.get(requestK).get(operatorK));
            LOGGER.debug("executing mt with [{}]", requestContext);
            Map<String, Object> wap = createWapMessage(requestContext);
            LOGGER.debug("wap message created [{}]", wap);
            correlator.addMessage(wap);
            Response response = webClient.post(wap);
            LOGGER.info("Response received from sbl status[{}] message[{}]", response.getStatus(), response.getEntity());

            if (isStatusSuccess(response)) {
                Map<String, Object> resp = correlator.waitForResponse(wap);
                LOGGER.debug("WAP SubmitResponse received [{}]", resp);
                return processResponse(requestContext, resp);
            } else {
                return generateErrorResponse(requestContext, response);
            }
        } catch (NoRecipientFoundException e) {
            LOGGER.info("No valid recipients found to send wap-push mt message [{}]", requestContext.get(requestK));
            return generateSuccess();
        } catch (SdpException e) {
            throw e;
        } catch (Exception ex) {
            LOGGER.error("MT channel failed.", ex);
            return generate(temporarySystemErrorCode, false, requestContext);
        }
    }

    private boolean isStatusSuccess(Response response) throws IOException {
        if (isSuccessHttpResponse(response)) {
            Map<String, Object> resp = readJsonResponse(response);
            if (resp != null && ACCEPTED.equals(resp.get(statusK))) {
                return true;
            }
        }
        return false;
    }

    private Map<String, Object> generateErrorResponse(Map<String, Map<String, Object>> requestContext, Response response) throws IOException {
        Map<String, Object> resp = readJsonResponse(response);
        if (resp != null) {
            String errorCode = temporarySystemErrorCode;
            if (QUEUE_FULL.equals(resp.get(statusK))) {
                errorCode = temporarySystemErrorCode;
            } else if (INVALID_REQUEST.equals(resp.get(statusK))) {
                errorCode = temporarySystemErrorCode;
            }
            Map<String, Object> errorResponse = generate(errorCode, false, requestContext);
            errorResponse.put(
                    statusDescriptionK,
                    MessageFormat.format("{0}({1})", errorResponse.get(statusDescriptionK),
                            resp.get(statusDescriptionK)));
            return errorResponse;
        } else {
            return generate(temporarySystemErrorCode, false, requestContext);
        }

    }

    private Map<String, Object> processResponse(Map<String, Map<String, Object>> requestContext, Map<String, Object> resp) {
        if (null == resp) {
            return generate(temporarySystemErrorCode, false, requestContext);
        } else if (!(successK).equals(resp.get(statusK))) {
            updateRecipientStatusesAsFailed(requestContext);
            return generate(temporarySystemErrorCode, false, requestContext);
        }

        if (resp.containsKey("unsuccessful-destinations")) {
            List<Map<String, String>> failedAddresses = GsonUtil.fromJson(
                    (String) resp.get("unsuccessful-destinations"),
                    ((new ArrayList<Map<String, String>>())).getClass());
            List<Map<String, String>> recipients = (List<Map<String, String>>) requestContext.get(requestK).get(
                    recipientsK);

            for (Map<String, String> failedAddress : failedAddresses) {
                for (Map<String, String> recipient : recipients) {
                    if (failedAddress.get(addressK).equals(recipient.get(recipientAddressK))) {
                        recipient.put(recipientAddressStatusK, failedAddress.get("errorCode"));
                    }
                }
            }
        }

        return generateSuccess();
    }

    private Map<String, Object> createWapMessage(Map<String, Map<String, Object>> requestContext) throws NoRecipientFoundException {
        Map<String, Object> wap = new HashMap<String, Object>();
        createRecipientAddressList(requestContext, wap);
        addMessageAndHeader(requestContext, wap);
        addEncoding(requestContext, wap);
        addStatusRequest(requestContext, wap);
        addOperatorChargingDetailsIfRequired(requestContext, wap);
        addDefaultServiceCode(requestContext, wap);
        wap.put(senderAddressK, getSenderAddress(requestContext));
        wap.put(correlationIdK, (String) requestContext.get(requestK).get(correlationIdK));
        wap.put(operatorK, (String) requestContext.get(requestK).get(operatorK));
        wap.put(appIdK, (String) requestContext.get(ncsK).get(appIdK));
        wap.put(ncsTypeK, getNcsType(requestContext));
        return wap;
    }

    private void addOperatorChargingDetailsIfRequired(Map<String, Map<String, Object>> requestContext, Map<String, Object> sms) {
        Object requestServiceCode = requestContext.get(requestK).get(serviceCodeK);
        if (null == requestServiceCode) {
            return;
        }

        String chargedAmount = (String) requestContext.get(requestK).get(chargedAmountK);
        if(chargedAmount != null){
            sms.put(createAdditionalParameter(chargedAmountK), chargedAmount);
        }

        sms.put(serviceCodeK, (String) requestServiceCode);

        Boolean srr = (Boolean) requestContext.get(requestK).get(srrK);
        if(srr != null && srr) {
            sms.put(srrK, Boolean.TRUE.toString());
        }

        if (!useServiceCodeShortCodeConvertion) {
            return;
        }

        String operator = (String) requestContext.get(requestK).get(operatorK);
        Map<String, String> serviceCodeShortCodeMap = (Map<String, String>) systemConfiguration().find(operator + "-wappush-service-code-short-code-mapping");

        LOGGER.debug("operator {0}, service-code {1}- map {2}", new Object[]{operator, requestServiceCode,
                serviceCodeShortCodeMap});

        if (serviceCodeShortCodeMap == null) {
            return;
        }

        String shortCode = serviceCodeShortCodeMap.get(requestServiceCode);
        if (shortCode == null) {
            LOGGER.debug("There is no matching short code for the service code [{}]", requestServiceCode);
            return;
        }
        sms.put(shortcodeK, shortCode);
        requestContext.get(requestK).put(senderAddressK, shortCode);
        LOGGER.debug("Short Code [{}] for Service Code [{}] ", sms.get(shortcodeK), sms.get(serviceCodeK));
    }

    private String createAdditionalParameter(String chargingAmount) {
        return "add_" + chargingAmount;
    }

    private void addDefaultServiceCode(Map<String, Map<String, Object>> requestContext, Map<String, Object> sms) {
        boolean aServiceNotCodeAvailable = sms.get(serviceCodeK) == null;
        if (alwaysSendServiceCode && aServiceNotCodeAvailable) {
            boolean defaultServiceCodeAvailable = defaultServiceCode != null && !defaultServiceCode.trim().isEmpty();
            if (defaultServiceCodeAvailable) {
                sms.put(serviceCodeK, defaultServiceCode);
            }
        }
    }

    private String payload(Map<String, Map<String, Object>> requestContext) {
        Object wapPushType = requestContext.get(requestK).get(wapPushTypeK);
        WapPushMessage wpm = new WapPushMessage();
        wpm.setText(requestContext.get(requestK).get(messageK).toString());
        wpm.setUrl(requestContext.get(requestK).get(wapUrlK).toString());
        if (wapPushType != null) {
            if (wapPushType.equals(serviceIndicatorK)) {
                wpm.setConnectionType(ConnectionType.WAPPUSH_SERVICE_INDICATION);
            } else if (wapPushType.equals(serviceLocatorK)) {
                wpm.setConnectionType(ConnectionType.WAPPUSH_SERVICE_LOAD);
            }
            StringBuilder payloadBuilder = new StringBuilder();
            payloadBuilder.append(wpm.createPdu()).append(wpm.createMessage());
            return payloadBuilder.toString();
        }
        throw new SdpException(invalidRequestErrorCode);
    }

    private void createRecipientAddressList(Map<String, Map<String, Object>> requestContext, Map<String, Object> wap)
            throws NoRecipientFoundException {
        List<Map<String, String>> recipients = (List<Map<String, String>>) requestContext.get(requestK)
                .get(recipientsK);
        StringBuilder sb = new StringBuilder();
        for (Map<String, String> recipient : recipients) {
            if (successCode.equals(recipient.get(recipientAddressStatusK))) {
                sb.append(",");
                sb.append(recipient.get(recipientAddressK));
            } else {
                LOGGER.info("Recipient address [{}] is not in valid status so ignoring", recipient);
            }
        }
        String recepientAddressList = sb.toString();
        LOGGER.debug("Created recepient address list[{}]", recepientAddressList);
        if (recepientAddressList.isEmpty()) {
            throw new NoRecipientFoundException();
        }

        if (recepientAddressList.startsWith(",")) {
            recepientAddressList = recepientAddressList.substring(1, recepientAddressList.length());
            LOGGER.debug("Recepient address list after removing (,) [{}]", recepientAddressList);
        }
        wap.put(recipientAddressK, recepientAddressList);
    }

    private void addStatusRequest(Map<String, Map<String, Object>> requestContext, Map<String, Object> sms) {
        if (isSrrRequested(requestContext)) {
            if (isSrrAllowedBySla(requestContext)) {
                sms.put(srrK, Boolean.TRUE.toString());
            } else {
                LOGGER.info("Status report is requested but not allowed by sla. Request will be rejected");
                throw new SdpException(invalidRequestErrorCode);
            }
        }
    }

    private boolean isSrrAllowedBySla(Map<String, Map<String, Object>> requestContext) {
        return ((Map<String, Object>) requestContext.get(ncsK).get(mtK)).containsKey(deliveryReportUrlK)
                && null != ((Map<String, Object>) requestContext.get(ncsK).get(mtK)).get(deliveryReportUrlK);
    }

    private void addEncoding(Map<String, Map<String, Object>> requestContext, Map<String, Object> wap) {
        if (requestContext.get(requestK).containsKey(encodingK)) {
            wap.put(encodingK, (String) requestContext.get(requestK).get(encodingK));
        } else {
            wap.put(encodingK, encodingBinaryK);
        }
    }

    private void updateRecipientStatusesAsFailed(Map<String, Map<String, Object>> requestContext) {
        for (Map<String, Object> recipient : (List<Map<String, Object>>) requestContext.get(requestK).get(recipientsK)) {
            if (!successCode.equals(recipient.get(recipientAddressStatusK))) {
                recipient.put(recipientAddressStatusK, temporarySystemErrorCode);
            }
        }
    }

    private void addMessageAndHeader(Map<String, Map<String, Object>> requestContext, Map<String, Object> wap) {
        String payload = payload(requestContext);
        wap.put(binaryHeaderK, payload.substring(0, 14));
        wap.put(messageK, payload.substring(14));
    }

    private String getSenderAddress(Map<String, Map<String, Object>> requestContext) {
        String senderAddress = (String) requestContext.get(requestK).get(senderAddressK);
        if (isNotEmpty(senderAddress)) {
            return senderAddress;
        } else {
            String defaultSenderAddress = (String) getDefaultSenderAddress(requestContext);
            if (isNotEmpty(defaultSenderAddress)) {
                LOGGER.info("Sender address not provided, using default sender address[{}]", defaultSenderAddress);
                return defaultSenderAddress;
            }
        }

        LOGGER.warn("Unable to send sms mt message. Sender address or default sender address not found");
        throw new SdpException(invalidSourceAddressErrorCode);
    }

    private boolean isNotEmpty(String str) {
        if (str != null && !str.trim().isEmpty()) {
            return true;
        } else {
            return false;
        }

    }

    private Object getDefaultSenderAddress(Map<String, Map<String, Object>> requestContext) {
        return (String) KeyNameSpaceResolver.data((Map) requestContext,
                KeyNameSpaceResolver.nameSpacedKey(ncsK, mtK, defaultSenderAddressK));
    }

    private boolean isSrrRequested(Map<String, Map<String, Object>> requestContext) {
        return requestContext.get(requestK).containsKey(statusRequestK)
                && deliveryReportRequiredK.equals(requestContext.get(requestK).get(statusRequestK));
    }

    private Object getAlias(Map requestContext) {
        try {
            return KeyNameSpaceResolver.data((Map) requestContext,
                    KeyNameSpaceResolver.nameSpacedKey(ncsK, mtK, aliasingK));
        } catch (Exception e) {
            return null;
        }
    }

    private void truncateResponse(Response response) {
        try {
            readJsonResponse(response);
        } catch (Exception e) {

        }
    }

    private String getNcsType(Map<String, Map<String, Object>> requestContext) {
        return (String) requestContext.get(ncsK).get(ncsTypeK);
    }

    public void setCorrelator(MessageCorrelator correlator) {
        this.correlator = correlator;
    }

    public void setDefaultServiceCode(String defaultServiceCode) {
        this.defaultServiceCode = defaultServiceCode;
    }

    public void setUseServiceCodeShortCodeConvertion(boolean useServiceCodeShortCodeConvertion) {
        this.useServiceCodeShortCodeConvertion = useServiceCodeShortCodeConvertion;
    }

    public void setAlwaysSendServiceCode(boolean alwaysSendServiceCode) {
        this.alwaysSendServiceCode = alwaysSendServiceCode;
    }
}
