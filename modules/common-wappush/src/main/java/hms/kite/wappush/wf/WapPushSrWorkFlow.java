/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hms.kite.wappush.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.control.InExpression;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.springframework.beans.factory.annotation.Autowired;

import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public class WapPushSrWorkFlow extends WrappedGeneratedWorkflow {

    @Autowired private Channel wapSrNcsWfChannel;
    @Autowired private Channel provSpSlaChannel;
    @Autowired private Channel provAppSlaChannel;

	@Override
	protected Workflow generateWorkflow() {

		ServiceImpl srNcsChannel = new ServiceImpl("wap-sr-ncs-wf-channel");
		srNcsChannel.setChannel(wapSrNcsWfChannel);

		ServiceImpl appStateValidationService = createAppSlaValidationRule();
		appStateValidationService.setOnSuccess(srNcsChannel);

		ServiceImpl spSlaValidation = createSpSlaValidationService();
		spSlaValidation.setOnSuccess(appStateValidationService);

		ServiceImpl spSlaChannel = new ServiceImpl("prov.sp.sla.channel", spSlaValidation);
		spSlaChannel.setChannel(provSpSlaChannel);

		ServiceImpl appSlaChannel = new ServiceImpl("prov.app.sla.channel", spSlaChannel);
		appSlaChannel.setChannel(provAppSlaChannel);

		return new WorkflowImpl(appSlaChannel);
	}

	private static ServiceImpl createAppSlaValidationRule() {
		InExpression statusExpression = new InExpression(appK, statusK, new Object[] { limitedProductionK,
				activeProductionK });
		Condition statusCondition = new Condition(statusExpression, authenticationFailedErrorCode);

		return new ServiceImpl("app.state.validation.service", statusCondition);
	}

	private static ServiceImpl createSpSlaValidationService() {
		EqualExpression statusExpression = new EqualExpression(spK, statusK, approvedK);
		Condition statusCondition = new Condition(statusExpression, authenticationFailedErrorCode);

		InExpression ncsAllowedExpression = new InExpression(spK, spSelectedServicesK, new Object[] { wapPushK });
		Condition ncsAllowedCondition = new Condition(ncsAllowedExpression, ncsNotAllowedErrorCode);

		return new ServiceImpl("sp.state.validation.service", statusCondition, ncsAllowedCondition);
	}

}
