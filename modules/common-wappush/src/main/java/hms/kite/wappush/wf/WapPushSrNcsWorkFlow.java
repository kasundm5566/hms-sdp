/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hms.kite.wappush.wf;

import static hms.kite.util.KiteErrorBox.unknownErrorCode;
import static hms.kite.util.KiteErrorBox.mtNotAllowedErrorCode;
import static hms.kite.util.KiteKeyBox.appK;
import static hms.kite.util.KiteKeyBox.limitedProductionK;
import static hms.kite.util.KiteKeyBox.maskK;
import static hms.kite.util.KiteKeyBox.maskNumberK;
import static hms.kite.util.KiteKeyBox.mtK;
import static hms.kite.util.KiteKeyBox.ncsK;
import static hms.kite.util.KiteKeyBox.statusK;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.ContainsExpression;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
public class WapPushSrNcsWorkFlow extends WrappedGeneratedWorkflow implements Workflow {

    @Autowired private Channel wapSrAtChannel;
    @Autowired private Channel numberMaskingChannel;
    @Autowired private Channel whitelistService;
    @Autowired private Channel blacklistingService;
    @Autowired private Channel mnpService;
    @Autowired private Channel provNcsSlaChannel;

    @Override
    protected Workflow generateWorkflow() {

        ServiceImpl smsSr = new ServiceImpl("wap-sr-at-channel");
        smsSr.setChannel(wapSrAtChannel);

        ServiceImpl numberMaskingChannelService = new ServiceImpl("number-masking-channel", smsSr);
        numberMaskingChannelService.setChannel(numberMaskingChannel);
        numberMaskingChannelService.addServiceParameter(maskK, true);

        EqualExpression maskingApplied = new EqualExpression(appK, maskNumberK, true);
        Condition maskingCondition = new Condition(maskingApplied, unknownErrorCode);

        ServiceImpl numberMaskingApplied = new ServiceImpl("masking.eligibility.validation", maskingCondition);
        numberMaskingApplied.setOnSuccess(numberMaskingChannelService);
        numberMaskingApplied.onDefaultError(smsSr);

        ServiceImpl whitelist = new ServiceImpl("white-list");
        whitelist.setChannel(whitelistService);
        whitelist.setOnSuccess(numberMaskingApplied);

        EqualExpression appStatus = new EqualExpression(appK, statusK, limitedProductionK);
        Condition condition = new Condition(appStatus, unknownErrorCode);

        ServiceImpl whitelistApplied = new ServiceImpl("whitelist.eligibility.validation", condition);
        whitelistApplied.setOnSuccess(whitelist);
        whitelistApplied.onDefaultError(numberMaskingApplied);

        ServiceImpl blacklist = new ServiceImpl("black-list");
        blacklist.setChannel(blacklistingService);
        blacklist.setOnSuccess(whitelistApplied);

        ServiceImpl numberChecking = new ServiceImpl("number-checking");
        numberChecking.setChannel(mnpService);
        numberChecking.setOnSuccess(blacklist);

        ServiceImpl ncsSla = createNcsSlaValidationService();
        ncsSla.setOnSuccess(numberChecking);

        ServiceImpl ncsSlaChannel = new ServiceImpl("prov.ncs.sla.channel", ncsSla);
        ncsSlaChannel.setChannel(provNcsSlaChannel);

        return new WorkflowImpl(ncsSlaChannel);
    }

    private static ServiceImpl createNcsSlaValidationService() {

        ContainsExpression allowedExpression = new ContainsExpression(ncsK, mtK);
        Condition ncsAllowedCondition = new Condition(allowedExpression, mtNotAllowedErrorCode);

        return new ServiceImpl("ncs.state.validation.service", ncsAllowedCondition);
    }

    @Override
    public void executeWorkflow(Map<String, Map<String, Object>> requestContext) {
        super.executeWorkflow(requestContext);
    }
}
