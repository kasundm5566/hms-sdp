/*
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.wappush.channel;

import static hms.kite.util.KiteKeyBox.addressK;
import static hms.kite.util.KiteKeyBox.recipientsK;
import static hms.kite.util.KiteKeyBox.requestK;
import hms.common.rest.util.Message;
import hms.kite.util.SystemUtil;
import hms.kite.wappush.WapPushAoServiceEndpoint;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;
import hms.kite.wfengine.transport.Channel;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * The only parameter required to contain in the message passed in to the send(Message) is
 * message-id, which is the correlation-id of the last message passed in by the same subscriber. 
 * 
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */
public class WapPushAoSdpWfChannel extends BaseChannel {

    private static final Logger LOGGER = LoggerFactory.getLogger(WapPushAoSdpWfChannel.class);

    private Workflow wapPushAoWf;
    private Workflow broadcastRequestWf;

	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("request-context received: [{}]", requestContext);
		}
		if(!isRecipientListAvailable(requestContext)) {
			createRecipientList(requestContext);
		}
		
		if(SystemUtil.isBroadcastRequest(requestContext)) {
			LOGGER.debug("broadcast message ..... ");
			return executeWf(requestContext, broadcastRequestWf);
		} else {
			LOGGER.debug("single message ..... ");
			return executeWf(requestContext, wapPushAoWf);
		}
	}

	private Map<String, Object> executeWf(Map<String, Map<String, Object>> requestContext, Workflow workflow) {
		try {
            workflow.executeWorkflow(requestContext);
			return ResponseBuilder.generateResponse(requestContext);
		} catch(Exception e) {
			LOGGER.error("Error occured while executing the workflow!", e);
			return ResponseBuilder.generate(requestContext, e);
		}
	}
	
	private boolean isRecipientListAvailable(Map<String, Map<String, Object>> requestContext) {
        return requestContext.get(requestK).containsKey(recipientsK);
    }

    private void createRecipientList(Map<String, Map<String, Object>> requestContext) {
        requestContext.get(requestK).put(recipientsK,
                SystemUtil.convert2NblAddresses((List<String>) requestContext.get(requestK).get(addressK)));
    }

    public void setWapPushAoWf(Workflow wapPushAoWf) {
        this.wapPushAoWf = wapPushAoWf;
    }

    public void setBroadcastRequestWf(Workflow broadcastRequestWf) {
        this.broadcastRequestWf = broadcastRequestWf;
    }
}
