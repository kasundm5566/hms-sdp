/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.wappush.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.control.InExpression;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;
/**
 * 
 * The only parameter required to contain in the message passed in to the send(Message) is
 * message-id, which is the correlation-id of the last message passed in by the same subscriber. 
 * 
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */
public class NblWapPushAoWorkflow extends WrappedGeneratedWorkflow implements Workflow {

	private final String name = "wap-ao-nbl";
	private static final Logger LOGGER = LoggerFactory.getLogger(NblWapPushAoWorkflow.class);

    @Autowired private Channel wapAoSdpChannel;
    @Autowired private Channel contentFilteringService;
    @Autowired private Channel messageHistoryChannel;
    @Autowired private Channel numberMaskingChannel;
    @Autowired private Channel provSpSlaChannel;
    @Autowired private Channel provAppSlaChannel;
    @Autowired private Channel internalHostResolverChannel;

	@Override
	public String getName() {
		return name;
	}

	@Override
	protected Workflow generateWorkflow() {
		ServiceImpl sdpChannel = new ServiceImpl("wap-ao-sdp");
        sdpChannel.setChannel(wapAoSdpChannel);

        ServiceImpl governanceChannel = new ServiceImpl("governance.channel");
        governanceChannel.setChannel(contentFilteringService);
        governanceChannel.setOnSuccess(sdpChannel);

        EqualExpression governanceEnabled = new EqualExpression(appK, governK, true);
        Condition governanceCondition = new Condition(governanceEnabled, unknownErrorCode);

        ServiceImpl governanceApplied = new ServiceImpl("governance.eligibility.validation", governanceCondition);
        governanceApplied.setOnSuccess(governanceChannel);
        governanceApplied.onDefaultError(sdpChannel);

        ServiceImpl messageHistoryService = new ServiceImpl("message-history-channel", governanceApplied);
        messageHistoryService.setChannel(messageHistoryChannel);
        messageHistoryService.setOnSuccess(governanceApplied);
        
        ServiceImpl numberMaskingService = new ServiceImpl("number-masking-channel", messageHistoryService);
        numberMaskingService.setChannel(numberMaskingChannel);
        numberMaskingService.addServiceParameter(unmaskK, true);

        EqualExpression maskingApplied = new EqualExpression(appK, maskNumberK, true);
        Condition maskingCondition = new Condition(maskingApplied, unknownErrorCode);

        ServiceImpl numberMaskingApplied = new ServiceImpl("masking.eligibility.validation", maskingCondition);
        numberMaskingApplied.setOnSuccess(numberMaskingService);
        numberMaskingApplied.onDefaultError(messageHistoryService);

        ServiceImpl appStateValidationService = createAppStateValidationService();
        appStateValidationService.setOnSuccess(numberMaskingApplied);

        ServiceImpl spSlaValidation = createSpStateValidation();
        spSlaValidation.setOnSuccess(appStateValidationService);

        ServiceImpl authenticationService = createAuthenticationService();
        authenticationService.setOnSuccess(spSlaValidation);

        ServiceImpl hostValidation = createHostValidationService();
        hostValidation.setOnSuccess(authenticationService);

        ServiceImpl internalHostService = new ServiceImpl("internal.host.validation.channel");
        internalHostService.setChannel(internalHostResolverChannel);
        // by pass authentication and allowed host validation if request is generated from internal host.
        internalHostService.setOnSuccess(spSlaValidation);
        internalHostService.onDefaultError(hostValidation);

        ServiceImpl spSlaChannel = new ServiceImpl("prov.sp.sla.channel");
        spSlaChannel.setChannel(provSpSlaChannel);
        spSlaChannel.setOnSuccess(internalHostService);

        ServiceImpl appSlaChannel = new ServiceImpl("prov.app.sla.channel");
        appSlaChannel.setChannel(provAppSlaChannel);
        appSlaChannel.setOnSuccess(spSlaChannel);

        return new WorkflowImpl(appSlaChannel);
	}

	private static ServiceImpl createAppStateValidationService() {
        InExpression statusExpression = new InExpression(appK, statusK, new Object[]{limitedProductionK, activeProductionK});
        Condition statusCondition = new Condition(statusExpression, authenticationFailedErrorCode);

        return new ServiceImpl("app.state.validation.service", statusCondition);
    }

    private static ServiceImpl createAuthenticationService() {
        EqualExpression authenticationExpression = new EqualExpression(requestK, passwordK, appK, passwordK);
        Condition condition = new Condition(authenticationExpression, authenticationFailedErrorCode);

        return new ServiceImpl("authentication.service", condition);
    }

    private static ServiceImpl createHostValidationService() {
        InExpression hostExpression = new InExpression(requestK, remotehostK, appK, allowedHostsK);
        Condition condition = new Condition(hostExpression, invalidHostIpErrorCode);

        return new ServiceImpl("host.validation.service", condition);
    }

    private static ServiceImpl createSpStateValidation() {
        EqualExpression statusExpression = new EqualExpression(spK, statusK, approvedK);
        Condition statusCondition = new Condition(statusExpression, authenticationFailedErrorCode);

        InExpression ncsAllowedExpression = new InExpression(spK, spSelectedServicesK, new Object[]{wapPushK});
        Condition ncsAllowedCondition = new Condition(ncsAllowedExpression, ncsNotAllowedErrorCode);

        return new ServiceImpl("sp.state.validation.service", statusCondition, ncsAllowedCondition);
    }

    @Override
    public void executeWorkflow(Map<String, Map<String, Object>> requestContext) {
        super.executeWorkflow(requestContext);
    }
}
