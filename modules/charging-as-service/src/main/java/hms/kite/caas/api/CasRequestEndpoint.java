/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.caas.api;

import hms.kite.caas.Utils.MaskAgentDetails;
import hms.kite.util.SystemUtil;
import hms.kite.wfengine.ServiceEndpoint;
import hms.kite.wfengine.transport.Channel;
import org.apache.log4j.NDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import java.util.HashMap;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

/**
 * Charging as service request endpoint
 */
@Produces({ "application/json" })
@Consumes({ "application/json" })
@Path("/caas")
public class CasRequestEndpoint extends ServiceEndpoint {

	private final static Logger LOGGER = LoggerFactory.getLogger(CasRequestEndpoint.class);
	private Channel accessRequestChannel;
	private Channel creditReserveChennel;
	private Channel creditCommitChannel;
	private Channel creditCancelChannel;
	private Channel directDebitChannel;
	private Channel directCreditChannel;
	private Channel listInstrumentsChannel;
	private Channel queryBalanceChannel;
    private Channel asyncChargingStatusChannel;
    private MaskAgentDetails maskAgentDetails;
    private boolean override = true;


    @Produces({ "application/json" })
	@Consumes({ "application/json" })
	@POST
	@Path("/request/access")
	public Map<String, Object> accessRequestService(Map<String, Object> msg, @Context javax.servlet.http.HttpServletRequest request) {
		Map<String, Object> serviceResponse = processService(msg, request);
		msg.put(requestTypeK, "request-access");
		serviceResponse.putAll(accessRequestChannel.send(msg));
        LOGGER.info("Sending response [{}]", serviceResponse);
		return serviceResponse;
	}

	@Produces({ "application/json" })
	@Consumes({ "application/json" })
	@POST
	@Path("/credit/reserve")
	public Map<String, Object> reserveService(Map<String, Object> msg, @Context javax.servlet.http.HttpServletRequest request) {
		Map<String, Object> serviceResponse = processService(msg, request);
		msg.put(requestTypeK, "credit-reserve");
		serviceResponse.putAll(creditReserveChennel.send(msg));
        LOGGER.info("Sending response [{}]", serviceResponse);
		return serviceResponse;
	}

	@Produces({ "application/json" })
	@Consumes({ "application/json" })
	@POST
	@Path("/credit/commit")
	public Map<String, Object> commitService(Map<String, Object> msg, @Context javax.servlet.http.HttpServletRequest request) {
		Map<String, Object> serviceResponse = processService(msg, request);
		msg.put(requestTypeK, "credit-commit");
		serviceResponse.putAll(creditCommitChannel.send(msg));
        LOGGER.info("Sending response [{}]", serviceResponse);
		return serviceResponse;
	}

	@Produces({ "application/json" })
	@Consumes({ "application/json" })
	@POST
	@Path("/credit/cancel")
	public Map<String, Object> cancelService(Map<String, Object> msg, @Context javax.servlet.http.HttpServletRequest request) {
		Map<String, Object> serviceResponse = processService(msg, request);
		msg.put(requestTypeK, "credit-cancel");
		serviceResponse.putAll(creditCancelChannel.send(msg));
        LOGGER.info("Sending response [{}]", serviceResponse);
		return serviceResponse;
	}

	@Produces({ "application/json" })
	@Consumes({ "application/json" })
	@POST
	@Path("/direct/debit")
	public Map<String, Object> directDebitService(Map<String, Object> msg, @Context javax.servlet.http.HttpServletRequest request) {
        // Mask Agent details for dialog ezcash payment instrument additional parameters.
        Map<String, Object> maskedAgentDetailsRequest = maskAgentDetails.maskEzcashAgentDetails(msg);

        LOGGER.info("Received request [{}]", maskedAgentDetailsRequest);
        msg.put(correlationIdK, Long.toString(SystemUtil.getCorrelationId()));
        addHostDetails(msg, request);

        try {
            NDC.push((String) msg.get(correlationIdK));
            LOGGER.debug("Direct debit request received {}", msg);
            msg.put(requestTypeK, "direct-debit");
            Map<String, Object> response = directDebitChannel.send(msg);
            LOGGER.info("Sending direct debit response {}", response);
            return response;
        } finally {
            NDC.pop();
        }
    }

	@Produces({ "application/json" })
	@Consumes({ "application/json" })
	@POST
	@Path("/direct/credit")
	public Map<String, Object> directCreditService(Map<String, Object> msg, @Context javax.servlet.http.HttpServletRequest request) {
		Map<String, Object> serviceResponse = processService(msg, request);
		msg.put(requestTypeK, "direct-credit");
		serviceResponse.putAll(directCreditChannel.send(msg));
        LOGGER.info("Sending response [{}]", serviceResponse);
		return serviceResponse;
	}

	@Produces({ "application/json" })
	@Consumes({ "application/json" })
	@POST
	@Path("/list/pi")
	public Map<String, Object> listPaymentInstrumentsService(Map<String, Object> msg, @Context javax.servlet.http.HttpServletRequest request) {
		Map<String, Object> serviceResponse = processService(msg, request);
		msg.put(requestTypeK, "list-pi");
		serviceResponse.putAll(listInstrumentsChannel.send(msg));
        LOGGER.info("Sending response [{}]", serviceResponse);
		return serviceResponse;
	}

    @Produces({ "application/json", "application/xml" })
	@Consumes({ "application/json", "application/xml" })
	@POST
	@Path("/request/async/charging/status")
    public Map<String, Object> asyncChargingConfirmation(Map<String, Object> msg, @Context javax.servlet.http.HttpServletRequest request) {
        LOGGER.info("Received Asynchronous Charging Status Request Received: [{}]", msg);
        Map<String, Object> serviceResponse = processService(msg, request);
		msg.put(requestTypeK, "async-charging-status");
        LOGGER.debug("Request is sending to the pgw: [{}]", msg);
		serviceResponse.putAll(asyncChargingStatusChannel.send(msg));
        LOGGER.info("Request is proceeded. Service Response was Received: [{}]", serviceResponse);
		return serviceResponse;
    }

    @Produces({ "application/json" })
    @Consumes({ "application/json" })
    @POST
    @Path("/balance/query")
    public Map<String, Object> queryBalanceService(Map<String, Object> msg, @Context javax.servlet.http.HttpServletRequest request) {
        Map<String, Object> serviceResponse = processService(msg, request);
        msg.put(requestTypeK, queryBalanceK);
        serviceResponse.putAll(queryBalanceChannel.send(msg));
        LOGGER.info("Sending response [{}]", serviceResponse);
        return serviceResponse;
    }

	protected Map<String, Object> processService(Map<String, Object> msg, @Context javax.servlet.http.HttpServletRequest request) {
		LOGGER.info("Request received [{}]", msg);
        msg.put(correlationIdK, Long.toString(SystemUtil.getCorrelationId()));
		addHostDetails(msg, request);
		LOGGER.debug("Received request from host " + msg.get(remotehostK) + " port : " + msg.get(portK));
        return new HashMap<String, Object>();
	}

	private void addHostDetails(Map<String, Object> msg, HttpServletRequest request) {
        if (override || null == msg.get(remotehostK)) {
            String host = SystemUtil.findHostIp(request);
            if (null != host) {
                msg.put(remotehostK, host);
            }
        }
    }

	public void setAccessRequestChannel(Channel accessRequestChannel) {
		this.accessRequestChannel = accessRequestChannel;
	}

	public void setCreditReserveChennel(Channel creditReserveChennel) {
		this.creditReserveChennel = creditReserveChennel;
	}

	public void setCreditCommitChannel(Channel creditCommitChannel) {
		this.creditCommitChannel = creditCommitChannel;
	}

	public void setCreditCancelChannel(Channel creditCancelChannel) {
		this.creditCancelChannel = creditCancelChannel;
	}

	public void setDirectDebitChannel(Channel directDebitChannel) {
		this.directDebitChannel = directDebitChannel;
	}

	public void setDirectCreditChannel(Channel directCreditChannel) {
		this.directCreditChannel = directCreditChannel;
	}

	public void setListInstrumentsChannel(Channel listInstrumentsChannel) {
		this.listInstrumentsChannel = listInstrumentsChannel;
	}

    public void setQueryBalanceChannel(Channel queryBalanceChannel) {
        this.queryBalanceChannel = queryBalanceChannel;
    }

    public void setAsyncChargingStatusChannel(Channel asyncChargingStatusChannel) {
        this.asyncChargingStatusChannel = asyncChargingStatusChannel;
    }

    public MaskAgentDetails getMaskAgentDetails() {
        return maskAgentDetails;
    }

    public void setMaskAgentDetails(MaskAgentDetails maskAgentDetails) {
        this.maskAgentDetails = maskAgentDetails;
    }
}
