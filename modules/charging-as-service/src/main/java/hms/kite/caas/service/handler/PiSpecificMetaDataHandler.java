package hms.kite.caas.service.handler;

import java.util.Map;

public interface PiSpecificMetaDataHandler {

    /**
     * Do some custom validations specific to given payment instrument.
     *
     * @param requestContext
     */
    void handle(Map<String, Map<String, Object>> requestContext);

    /**
     * Name of the validator.
     *
     * @return
     */
    String getName();

}
