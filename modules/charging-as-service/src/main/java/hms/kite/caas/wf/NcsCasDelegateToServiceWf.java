package hms.kite.caas.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.transport.Channel;

/**
 * Simple workflow for Charging as a service which delegates the task to the underlying channel.
 *
 * $LastChangedDate 7/26/11 7:50 AM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
public class NcsCasDelegateToServiceWf extends GenericCasNcsWf implements Workflow{

    /**
     * Service name for Payment Gateway Adapter
     */
//    protected String pgwAdapterServiceChannelName;
    protected Channel pgwAdapterServiceChannel;

    @Override
    protected ServiceImpl generateWorkflowTail() {
        ServiceImpl creditReserve = new ServiceImpl(getName(pgwAdapterServiceChannel));
        creditReserve.setChannel(pgwAdapterServiceChannel);


        return creditReserve;
    }

//    /**
//     * Sets the Service name for Payment Gateway Adapter
//     * @param pgwAdapterServiceChannelName
//     */
//    public void setPgwAdapterServiceChannelName(String pgwAdapterServiceChannelName) {
//        this.pgwAdapterServiceChannelName = pgwAdapterServiceChannelName;
//    }

    public void setPgwAdapterServiceChannel(Channel pgwAdapterServiceChannel) {
        this.pgwAdapterServiceChannel = pgwAdapterServiceChannel;
    }
}
