/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.caas.service;

import hms.kite.CasAdapter;
import hms.kite.caas.api.NblParameter;
import hms.kite.caas.service.handler.PiSpecificMetaDataHandler;
import hms.kite.util.KiteKeyBox;
import hms.kite.util.PgwParam;
import hms.kite.wfengine.impl.ResponseBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KeyNameSpaceResolver.data;
import static hms.kite.util.KiteKeyBox.requestK;

/**
 * Charging as a Service Credit Commit.
 */
public class CasDirectDebitService extends AbstractCasPgwAdapterService {

    private final static Logger logger = LoggerFactory.getLogger(CasDirectDebitService.class);

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, Object> createExecutionResult(Map<String, Object> chargingResponse, Map<String, Map<String, Object>> requestContext) {

        Map<String, Object> result = ResponseBuilder.generate((String) chargingResponse.get(KiteKeyBox.statusCodeK), (String) chargingResponse.get(KiteKeyBox.statusDescriptionK), true);

        result.put(NblParameter.EXTERNAL_TRX_ID.getName(), chargingResponse.get(PgwParam.EXT_TRANSID.getParameter()));
        result.put(NblParameter.INTERNAL_TRX_ID.getName(), chargingResponse.get(PgwParam.INT_TRANSID.getParameter()));
        // result.put(NblParameter.TIMESTAMP.getName(),
        // chargingResponse.get(PgwParam.REQUESTED_TIMESTAMP.getParameter()));
        result.put(NblParameter.TIMESTAMP.getName(), requestContext.get(requestK).get(KiteKeyBox.timeStampK));
        result.put(NblParameter.STATUS_CODE.getName(), chargingResponse.get(PgwParam.STATUS_CODE.getParameter()));
        result.put(NblParameter.STATUS_DETAIL.getName(), chargingResponse.get(PgwParam.STATUS_TEXT.getParameter()));
        //todo payment gateway is not sending null status-text, figure out a way to send the response.
        String chargingInstructions = (String) chargingResponse.get(PgwParam.CHARGING_INSTRUCTION.getParameter());

        if (chargingInstructions != null && !chargingInstructions.trim().isEmpty()) {
            result.put(NblParameter.LONG_DESCRIPTION.getName(), chargingInstructions);
        }
        String chargingShortInstructions = (String) chargingResponse.get(PgwParam.CHARGING_SHORT_INSTRUCTIONS.getParameter());
        if (chargingShortInstructions != null && !chargingShortInstructions.trim().isEmpty()) {
            result.put(NblParameter.SHORT_DESCRIPTION.getName(), chargingShortInstructions);
        }
        result.put(NblParameter.BUSINESS_NO.getName(), chargingResponse.get(PgwParam.BUSINESS_NO.getParameter()));
        result.put(NblParameter.AMOUNT_DUE.getName(), chargingResponse.get(PgwParam.AMOUNT_DUE.getParameter()));
        result.put(NblParameter.REFERENCE_ID.getName(), chargingResponse.get(PgwParam.REFERENCE_ID.getParameter()));
        result.put(PgwParam.FROM_PAYMENT_INS_NAME.getParameter(), chargingResponse.get(PgwParam.FROM_PAYMENT_INS_NAME.getParameter()));
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, Object> createChargingRequest(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> nblRequest = requestContext.get(requestK);
        Map<String, Object> pgRequest = createPgwRequestWithAmount(nblRequest, requestContext);

        return pgRequest;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, Object> call(CasAdapter casAdapter, Map<String, Object> chargingRequest) {
        return casAdapter.directDebit(chargingRequest);
    }
}
