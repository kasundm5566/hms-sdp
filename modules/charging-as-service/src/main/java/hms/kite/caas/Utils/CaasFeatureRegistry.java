/*
 *   (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.caas.Utils;


import hms.kite.util.NullObject;

import java.util.Properties;

public class CaasFeatureRegistry {

    private static Properties caasFeatureProperties;

    static {
        setCaasFeatureProperties(new NullObject<Properties>().get());
    }

    private static void setCaasFeatureProperties(Properties caasFeatureProperties) {
        CaasFeatureRegistry.caasFeatureProperties = caasFeatureProperties;
    }

    public static String getDialoEzcashPayInsName() {
        return String.valueOf(caasFeatureProperties.getProperty("ezcash.payment.ins.name"));
    }
    public static String getEzcashAgentDetailsReqMsg() {
        return String.valueOf(caasFeatureProperties.getProperty("ezcash.agent.details.required.msg"));
    }

    public static String getEzcashAgentAliasReqMsg() {
        return String.valueOf(caasFeatureProperties.getProperty("ezcash.agent.alias.required.msg"));
    }

    public static String getEzcashAgentPinReqMsg() {
        return String.valueOf(caasFeatureProperties.getProperty("ezcash.agent.pin.required.msg"));
    }

}
