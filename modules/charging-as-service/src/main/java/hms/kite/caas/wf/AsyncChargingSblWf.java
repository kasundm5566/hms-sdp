/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.caas.wf;

import static hms.kite.util.KiteErrorBox.appNotAvailableErrorCode;
import static hms.kite.util.KiteErrorBox.ncsNotAllowedErrorCode;
import static hms.kite.util.KiteErrorBox.spNotAvailableErrorCode;
import static hms.kite.util.KiteKeyBox.activeProductionK;
import static hms.kite.util.KiteKeyBox.appK;
import static hms.kite.util.KiteKeyBox.approvedK;
import static hms.kite.util.KiteKeyBox.casK;
import static hms.kite.util.KiteKeyBox.limitedProductionK;
import static hms.kite.util.KiteKeyBox.spK;
import static hms.kite.util.KiteKeyBox.spSelectedServicesK;
import static hms.kite.util.KiteKeyBox.statusK;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.control.InExpression;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class AsyncChargingSblWf extends WrappedGeneratedWorkflow implements Workflow {

     @Autowired private Channel caasAsyncAtChannel;
     @Autowired private Channel provSpSlaChannel;
     @Autowired private Channel provAppSlaChannel;
     @Autowired private Channel provNcsSlaChannel;

	@Override
	protected Workflow generateWorkflow() {

		ServiceImpl atChannel = new ServiceImpl("caas-async-at-channel");
		atChannel.setChannel(caasAsyncAtChannel);

		ServiceImpl appStateValidationService = createAppSlaValidationRule();
		appStateValidationService.setOnSuccess(atChannel);

		ServiceImpl spSlaValidation = createSpSlaValidationService();
		spSlaValidation.setOnSuccess(appStateValidationService);

		ServiceImpl spSlaChannel = new ServiceImpl("prov.sp.sla.channel", spSlaValidation);
		spSlaChannel.setChannel(provSpSlaChannel);

		ServiceImpl ncsSlaChannel = new ServiceImpl("prov.ncs.sla.channel", spSlaChannel);
		ncsSlaChannel.setChannel(provNcsSlaChannel);

		ServiceImpl appSlaChannel = new ServiceImpl("prov.app.sla.channel", ncsSlaChannel);
		appSlaChannel.setChannel(provAppSlaChannel);

		return new WorkflowImpl(appSlaChannel);
	}

	private static ServiceImpl createAppSlaValidationRule() {
		InExpression statusExpression = new InExpression(appK, statusK, new Object[] { limitedProductionK,
				activeProductionK });
		Condition statusCondition = new Condition(statusExpression, appNotAvailableErrorCode);

		return new ServiceImpl("app.state.validation.service", statusCondition);
	}

	private static ServiceImpl createSpSlaValidationService() {
		EqualExpression statusExpression = new EqualExpression(spK, statusK, approvedK);
		Condition statusCondition = new Condition(statusExpression, spNotAvailableErrorCode);

		InExpression ncsAllowedExpression = new InExpression(spK, spSelectedServicesK, new Object[] { casK });
		Condition ncsAllowedCondition = new Condition(ncsAllowedExpression, ncsNotAllowedErrorCode);

		return new ServiceImpl("sp.state.validation.service", statusCondition, ncsAllowedCondition);
	}

    @Override
    public void executeWorkflow(Map<String, Map<String, Object>> requestContext) {
        super.executeWorkflow(requestContext);
    }
}
