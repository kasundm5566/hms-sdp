/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.caas.channel;

import static hms.kite.util.KiteKeyBox.directionK;
import static hms.kite.util.KiteKeyBox.moK;
import static hms.kite.util.KiteKeyBox.ncsTypeK;
import static hms.kite.util.KiteKeyBox.receiveTimeK;
import static hms.kite.util.KiteKeyBox.recipientAddressK;
import static hms.kite.util.KiteKeyBox.recipientsK;
import static hms.kite.util.KiteKeyBox.requestK;
import static hms.kite.util.KiteKeyBox.*;

import hms.kite.util.IdGenerator;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.NDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class AsyncChargingWfChannel extends BaseChannel {
	private static final Logger LOGGER = LoggerFactory.getLogger(AsyncChargingWfChannel.class);
	private Workflow workflow;

	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
		String correlationId = IdGenerator.generate();
		try {
			NDC.push(correlationId);
            //todo generate the correlation id when we receive request, not here.
			requestContext.get(requestK).put(correlationIdK, correlationId);
			LOGGER.debug("request-context received: [{}]", requestContext);
			addParameters(requestContext);
			return executeWf(requestContext, workflow);
		} finally {
			NDC.pop();
		}
	}

	private Map<String, Object> executeWf(Map<String, Map<String, Object>> requestContext, Workflow workFlow) {
		try {
			workFlow.executeWorkflow(requestContext);
			return ResponseBuilder.generateResponse(requestContext);
		} catch (Exception e) {
			LOGGER.error("Error occured while executing the workflow!", e);
			return ResponseBuilder.generate(requestContext, e);
		}
	}

	private void addParameters(Map<String, Map<String, Object>> requestContext) {
		Map<String, Object> request = requestContext.get(requestK);
		request.put(ncsTypeK, casK);
		request.put(directionK, moK);
		request.put(receiveTimeK, Long.toString(System.currentTimeMillis()));

		if (request.containsKey(accountIdK)) {
			Map<String, Object> recipients = new HashMap<String, Object>();
			recipients.put(recipientAddressK, request.get(accountIdK));
			request.put(recipientsK, Arrays.asList(recipients));
		}
	}

    public void setWorkflow(Workflow workflow) {
        this.workflow = workflow;
    }
}
