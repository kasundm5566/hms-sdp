package hms.kite.caas.wf;

import hms.kite.util.KiteErrorBox;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.control.GreaterThanExpression;
import hms.kite.wfengine.control.LessThanExpression;
import hms.kite.wfengine.impl.ServiceImpl;

import static hms.kite.util.KiteKeyBox.*;

/**
 * Direct Debit workflow for Charging as a service.
 *
 * $LastChangedDate 7/26/11 7:50 AM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
public class NcsCasDirectCreditWf extends NcsCasDelegateToServiceWf implements Workflow {

    /**
     * Checks "credit-allowed" : true, "credit-min-amount" : "10.5", "credit-max-amount" : "100.5"
     * @return
     */
    @Override
    protected ServiceImpl generateWorkflowTail() {
        ServiceImpl ncsValidationService = new ServiceImpl("ncs-validation");
        ncsValidationService.chain("credit-allowed", new Condition(
                        new EqualExpression(ncsK, creditAllowedK, true),
                        KiteErrorBox.chargingOperationNotAllowedErrorCode
                )).chain("credit-min-amount", new Condition(
                        new GreaterThanExpression(requestK, amountK, ncsK, creditMinAmountK),
                        KiteErrorBox.chargingAmountTooLowErrorCode
                )).chain("credit-max-amount", new Condition(
                        new LessThanExpression(requestK, amountK, ncsK, creditMaxAmountK),
                         KiteErrorBox.chargingAmountTooHighErrorCode
                )).chain(getName(super.pgwAdapterServiceChannel))
                .attachChannel(super.pgwAdapterServiceChannel);

        return ncsValidationService;
    }
}
