/*
 *   (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.caas.Utils;


import hms.kite.caas.api.NblParameter;
import hms.kite.caas.service.CaasParameterValidationException;
import hms.kite.util.KiteErrorBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MaskAgentDetails {

    private final static Logger logger = LoggerFactory.getLogger(MaskAgentDetails.class);
    private static final String UTF = "UTF8";
    private static final int KEY_LENGTH = 5;
    private static final int CIPHER_INDEX = 0;
    private static final int DECIPHER_INDEX = 1;

    /**
     * This method is use to mask ezcash agent details for dialog ezcash payment instrument.
     * @param request
     * @return
     */
    public Map<String, Object> maskEzcashAgentDetails(Map<String, Object> request) {
        logger.debug(" Receive Request to Mask Details");
        if (CaasFeatureRegistry.getDialoEzcashPayInsName()
                .equals(request.get(NblParameter.PAYMENT_INSTRUMENT_NAME.getName()))) {
            if (request.get(NblParameter.ADDITIONAL_PARAMS.getName()) != null) {
                Map<String,Object> ezcashAgentAdditionParams = (Map<String,Object>)request.get(NblParameter.ADDITIONAL_PARAMS.getName());
                if (ezcashAgentAdditionParams.get(NblParameter.EZCASH_AGENT_ALIAS.getName()) != null) {
                    String ezcashAgentAlias = (String)ezcashAgentAdditionParams.get(NblParameter.EZCASH_AGENT_ALIAS.getName());
                    String encryptedAgentAlias = encrypt(ezcashAgentAlias);
                    ezcashAgentAdditionParams.put(NblParameter.EZCASH_AGENT_ALIAS.getName(), encryptedAgentAlias);
                }
                if (ezcashAgentAdditionParams.get(NblParameter.EZCASH_AGENT_PIN.getName()) != null) {
                    String ezcashAgentPin = (String)ezcashAgentAdditionParams.get(NblParameter.EZCASH_AGENT_PIN.getName());
                    String encryptedAgentPin = encrypt(ezcashAgentPin);
                    ezcashAgentAdditionParams.put(NblParameter.EZCASH_AGENT_PIN.getName(), encryptedAgentPin);
                }
                request.put(NblParameter.ADDITIONAL_PARAMS.getName(), ezcashAgentAdditionParams);
            }
        } else {
            logger.info("Payemnt Instrument is not Dialog-Ezcash. Skip Mask Agent details.");
        }
        logger.debug("Returning Request [{}]", request);
        return request;
    }


    /**
     * Following encryption logic is used to encrypt dialog ezcash agent details.
     * This is used by payment gateway - class : /pgw/dialog/ezcash/connector/util/DecryptAgentDetails.java
     * to decrypt those encrypted details using same key as implemented below.
     * @param value
     * @return
     */
    public String encrypt(String value) {
        Map<String, String> encryptionKeys = new HashMap<String, String>();
        encryptionKeys.put("AZ110", "63pq%n89y(*6HK%$^@JK%^!@");

        Map<String, List<Cipher>>  ciphers = new HashMap<String, List<Cipher>>();
        for (Map.Entry<String, String> entry : encryptionKeys.entrySet()) {
            ciphers.put(entry.getKey(), getCipherList(new SecretKeySpec(entry.getValue().getBytes(), "DESede")));
        }

        if (value != null) {
            StringBuilder encryptStr = appendSecretKey(value);
            try {
                for (Map.Entry<String, List<Cipher>> entry : ciphers.entrySet()) {
                    byte[] enc = entry.getValue().get(CIPHER_INDEX).doFinal(encryptStr.toString().getBytes(UTF));
                    String encryptValue = entry.getKey() + (new BASE64Encoder().encode(enc));
                    return encryptValue.replace("+", "_");
                }
                throw new RuntimeException("No encryption format found..");
            } catch (Exception e) {
                // logger.error("Error encrypting [" + value + "]", e);
                throw new RuntimeException("Error encrypting [" + value + "]", e);
            }
        } else {
            return "";
        }
    }

    private StringBuilder appendSecretKey(String msisdn) {
        StringBuilder encryptStr = new StringBuilder("");
        final String[] key = "yu$*(&u@#$(%76(&#867!~#&JGH*(h}{*".split("|");
        String[] split = msisdn.split("|");

        for (int i = 0; i < split.length; i++) {
            String character = split[i];
            if (key.length - 1 > i) {
                if (i % 3 == 0) {
                    encryptStr.append(key[i]).append(character);
                } else {
                    encryptStr.append(key[i + 2]).append(character);
                }
            } else {
                encryptStr.append("G").append(character);
            }
        }
        return encryptStr;
    }

    public static List<Cipher> getCipherList(SecretKey key) {
        ArrayList<Cipher> list = new ArrayList<Cipher>();
        try {
            Cipher ecipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
            Cipher dcipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
            ecipher.init(Cipher.ENCRYPT_MODE, key);
            dcipher.init(Cipher.DECRYPT_MODE, key);
            list.add(ecipher);
            list.add(dcipher);
        } catch (Exception e) {
            //logger.error("Error in initialising encryption stranded", e);
            throw new RuntimeException("Error in initialising encryption stranded", e);
        }
        return list;
    }
}
