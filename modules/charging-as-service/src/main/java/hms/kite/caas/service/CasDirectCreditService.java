/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.caas.service;

import hms.kite.CasAdapter;
import hms.kite.caas.api.NblParameter;
import hms.kite.util.KiteErrorBox;
import hms.kite.util.PgwParam;
import hms.kite.wfengine.impl.ResponseBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.requestK;

/**
 * 
 * Charging as a Service Credit Commit.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

public class CasDirectCreditService extends CasDirectDebitService {

	private final static Logger logger = LoggerFactory.getLogger(CasDirectCreditService.class);

        /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String,Object> createChargingRequest(Map<String,Map<String, Object>> requestContext) {
        Map<String, Object>  nblRequest = requestContext.get(requestK);
        Map<String, Object>  pgRequest = createPgwRequestWithAmount(nblRequest, requestContext);

        return pgRequest;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, Object> call(CasAdapter casAdapter, Map<String, Object> chargingRequest) {
        return casAdapter.directDebit(chargingRequest);
    }
}
