/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.caas.service;

import hms.kite.CasAdapter;
import hms.kite.caas.api.NblParameter;
import hms.kite.util.KiteKeyBox;
import hms.kite.util.PgwParam;
import hms.kite.wfengine.impl.ResponseBuilder;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.caas.api.NblParameter.EXTERNAL_TRX_ID;
import static hms.kite.caas.api.NblParameter.INTERNAL_TRX_ID;
import static hms.kite.util.KiteKeyBox.requestK;
import static hms.kite.util.KiteKeyBox.statusCodeK;
import static hms.kite.util.KiteKeyBox.statusDescriptionK;
import static hms.kite.util.PgwParam.CLIENT_TRANS_ID_FOR_RESPONSE;
import static hms.kite.util.PgwParam.EXT_TRANSID;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class CasAsyncChargingStatusService extends AbstractCasPgwAdapterService {

    private final static Logger logger = LoggerFactory.getLogger(CasAsyncChargingStatusService.class);

    /*
	    Parse the SDP request and create the CasAdapter request.
	 */
    @Override
    protected Map<String, Object> createChargingRequest(Map<String, Map<String, Object>> requestContext) throws CaasParameterValidationException {
        logger.debug("SDP request is being parsed into CasAdapter compatible requests: [{}]", requestContext);
        //TODO make sure the keys are matching
        //return createPgwTransactionStatusRequest(requestContext.get(requestK), requestContext);
        Map<String, Object> pgRequest = new HashMap<String, Object>();

        pgRequest.put(PgwParam.TRANSACTION_ID.getParameter(), requestContext.get(requestK).get(NblParameter.EXTERNAL_TRX_ID.getName()));
        pgRequest.put(PgwParam.REQUESTED_TIMESTAMP.getParameter(), ISODateTimeFormat.dateTime().print(new DateTime()));

        logger.debug("Parsed SDP request [{}]", pgRequest);
        return pgRequest;
    }

    /*
        Parse the charging response form CasAdapter and create the SDP response map.
     */
    @Override
    protected Map<String, Object> createExecutionResult(Map<String, Object> chargingResponse, Map<String, Map<String, Object>> requestContext) {
        logger.debug("Response received from the CasAdapter: [{}]", chargingResponse);
        logger.debug("SDP request context: [{}]", requestContext);
        Map<String, Object> result = ResponseBuilder.generate((String) chargingResponse.get(statusCodeK),
                (String) chargingResponse.get(statusDescriptionK), true);
        result.put(EXTERNAL_TRX_ID.getName(),
                chargingResponse.get(CLIENT_TRANS_ID_FOR_RESPONSE.getParameter()));
        result.put(INTERNAL_TRX_ID.getName(), chargingResponse.get(EXT_TRANSID.getParameter()));
        result.put(NblParameter.STATUS_CODE.getName(), chargingResponse.get(PgwParam.STATUS_CODE.getParameter()));
        result.put(NblParameter.STATUS_TEXT.getName(), chargingResponse.get(PgwParam.STATUS_TEXT.getParameter()));

        return new HashMap<String, Object>();
    }

    @Override
    protected Map<String, Object> call(CasAdapter casAdapter, Map<String, Object> chargingRequest) {
        logger.debug("Asynchronous Charging Status Request is being initiated: [{}]", chargingRequest);
        return casAdapter.asyncTransactionStatus(chargingRequest);
    }
}
