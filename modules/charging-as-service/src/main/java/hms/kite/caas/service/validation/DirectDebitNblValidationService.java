/**
 *   (C) Copyright 2012-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.caas.service.validation;

import hms.kite.util.SdpException;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.NumberFormat;
import java.util.Map;
import java.util.regex.Pattern;

import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.KiteErrorBox.*;

public class DirectDebitNblValidationService extends BaseChannel {

    private final static Logger LOGGER = LoggerFactory.getLogger(DirectDebitNblValidationService.class);
    private static final String amountValidationPattern = "([1-9][0-9]*(\\.[0-9]{1,2})?)|([0-9]*(\\.([0-9][1-9]|[1-9]|[1-9][0-9])))";

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {

        if (null == requestContext.get(requestK).get(externalTransIdNblK)) {
            LOGGER.warn("Invalid request - {} not available", externalTransIdNblK);
            throw new SdpException(invalidRequestErrorCode);
        }

        if (null == requestContext.get(requestK).get(amountK)) {
            LOGGER.warn("Invalid request - {} not available", amountK);
            throw new SdpException(invalidRequestErrorCode);
        }  else {
            String amount = (String) requestContext.get(requestK).get(amountK);
            if (!validateAmount(amount)) {
                LOGGER.warn("Invalid amount");
                throw new SdpException(invalidRequestErrorCode);
            }
        }

        return ResponseBuilder.generateSuccess();
    }

    private static boolean validateAmount(String amount) {
        return Pattern.matches(amountValidationPattern, amount);
    }
}
