/*
 *   (C) Copyright 2012-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.caas.channel;

import hms.kite.util.KiteKeyBox;

import java.util.Map;

/**
 *
 */
public class QueryBalanceNBLChannel extends GenericCasNBLChannel {

    protected Map<String, Object> generateResponse(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> response = super.generateResponse(requestContext);
        Map<String, Object> originalResponse = requestContext.get(KiteKeyBox.responseK);

        if (originalResponse != null) {
            if (originalResponse.containsKey(KiteKeyBox.accountTypeNblK)) {
                response.put(KiteKeyBox.accountTypeNblK, originalResponse.get(KiteKeyBox.accountTypeNblK));
            }
            if (originalResponse.containsKey(KiteKeyBox.accountStatusNblK)) {
                response.put(KiteKeyBox.accountStatusNblK, originalResponse.get(KiteKeyBox.accountStatusNblK));
            }
            if (originalResponse.containsKey(KiteKeyBox.chargeableBalanceNblK)) {
                response.put(KiteKeyBox.chargeableBalanceNblK, originalResponse.get(KiteKeyBox.chargeableBalanceNblK));
            }
        }
        return response;
    }

}
