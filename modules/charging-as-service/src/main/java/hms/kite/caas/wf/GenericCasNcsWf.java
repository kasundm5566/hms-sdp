/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.caas.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.control.InExpression;
import hms.kite.wfengine.control.OrExpression;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.springframework.beans.factory.annotation.Autowired;

import static hms.kite.util.KiteKeyBox.*;

/**
 * Charging as a service (Generic) Request workflow
 *
 * <p/>
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public abstract class GenericCasNcsWf extends WrappedGeneratedWorkflow implements Workflow {
    
    @Autowired private Channel casNcsSlaChannel;
    @Autowired private Channel tpdThrottlingChannel;
    @Autowired private Channel tpsThrottlingChannel;
    @Autowired private Channel mainThrottlingChannel;
    @Autowired private Channel whitelistService;
    @Autowired private Channel subscriptionCheckChannel;

    @Override
    protected Workflow generateWorkflow() {
        return new WorkflowImpl(generateWorkflowHead());
    }

    /**
     * Generates the service which handles the specific workflow fragment for each CaaS requests.
     *
     * @return ServiceImpl
     */
    protected abstract ServiceImpl generateWorkflowTail();

    /**
     * Generates the head of the workflow which is common for all CaaS requests.
     *
     * loads provisioning sla, tpd apply, tps apply, whitelist check  (i.e. limited production)
     *
     * if limited production, apply the white list.
     * else proceed with normal flow.
     *
     * @return ServiceImpl
     */
    protected ServiceImpl generateWorkflowHead() {

        ServiceImpl ncsSlaChannel = new ServiceImpl("cas-ncs-sla-channel").attachChannel(casNcsSlaChannel);

        ServiceImpl subscriptionRequiredCheck =ncsSlaChannel.chain(new Condition(new EqualExpression(ncsK, subscriptionRequiredK, true)));
        ServiceImpl subscriptionCheck = subscriptionRequiredCheck.chain("subscription-required-check").attachChannel(subscriptionCheckChannel);

        ServiceImpl tpdChannel = new ServiceImpl("tpd-throttling-channel",
                new Condition( new InExpression(ncsK, statusK, new Object[]{limitedProductionK, activeProductionK})))
                .attachChannel(tpdThrottlingChannel);
        tpdChannel.setServiceParameterKeys("ncs-caas");

        subscriptionCheck.setOnSuccess(tpdChannel);
        subscriptionRequiredCheck.onDefaultError(tpdChannel);

        ServiceImpl spTps = tpdChannel.chain("tps-throttling-channel").attachChannel(tpsThrottlingChannel);
        spTps.setServiceParameterKeys("ncs-caas");

        ServiceImpl systemTps = spTps.chain("main-throttling-channel").attachChannel(mainThrottlingChannel);
        systemTps.setServiceParameterKeys("system-tps");

        ServiceImpl whitelistAppliedCheck = systemTps.chain(new Condition(
                new OrExpression(
                      new EqualExpression(appK, statusK, limitedProductionK),
                      new EqualExpression(ncsK, statusK, limitedProductionK)
                )
        ));

        ServiceImpl whitelist = whitelistAppliedCheck.chain("white-list").attachChannel(whitelistService);

//        ServiceImpl blockedMsisdnChannel = whitelist.chain("blocked-msisdn-channel")
//                .attachChannel("blocked-msisdn-channel");
        ServiceImpl tail = generateWorkflowTail();


        whitelistAppliedCheck.onDefaultError(tail);

        whitelist.setOnSuccess(tail);

        return  ncsSlaChannel;
    }

}
