package hms.kite.caas.wf;

import hms.kite.util.KiteErrorBox;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.control.GreaterThanExpression;
import hms.kite.wfengine.control.LessThanExpression;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.transport.Channel;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

/**
 *
 */
public class NcsCasQueryBalanceWf extends NcsCasDelegateToServiceWf implements Workflow {


    @Override
    protected ServiceImpl generateWorkflowTail() {
        ServiceImpl ncsValidationService = new ServiceImpl("ncs-validation");
        ncsValidationService.chain("query-balance-allowed", new Condition(
                new EqualExpression(ncsK, queryBalanceAllowedK, true),
                KiteErrorBox.chargingOperationNotAllowedErrorCode))
                .chain(getName(super.pgwAdapterServiceChannel)).attachChannel(super.pgwAdapterServiceChannel);

        return ncsValidationService;
    }

    @Override
    public void executeWorkflow(Map<String, Map<String, Object>> requestContext) {
        super.executeWorkflow(requestContext);
    }

}
