/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.caas.util;

import hms.common.rest.util.JsonBodyProvider;
import org.apache.cxf.jaxrs.client.WebClient;

import java.util.LinkedList;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class Http {

    public static WebClient createWebClient(String connectionUrl) {
		WebClient webClient = WebClient.create(connectionUrl, getJsonProviders());
		webClient.header("Content-Type", "application/json");
		webClient.accept("application/json");
		return webClient;
	}

	public static LinkedList<Object> getJsonProviders() {
        LinkedList<Object> providers = new LinkedList<Object>();
		providers.add(new JsonBodyProvider());
        return providers;
	}
}
