/*
 *   (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.caas.service.handler;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.util.KeyNameSpaceResolver.data;
import static hms.kite.util.KiteKeyBox.*;

/**
 * Filter  Dialog Mobile Account Meta data.
 */
public class DialogMobileAccountMetaDataHandler implements PiSpecificMetaDataHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(DialogMobileAccountMetaDataHandler.class);

    /**
     * Process filtering mets data from request context.
     * @param requestContext
     */
    @Override
    public void handle(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> request = requestContext.get(requestK);
        Map<String, Object> ncsSla = requestContext.get(ncsK);
        LOGGER.debug("Request received to handle request[{}], ncsSla[{}]", request, ncsSla);

        String authorizationMedium = (String)data(ncsSla, chargingK, metaDataK, mobileAccAuthMediumK);
        String authorizationThreshold = (String)data(ncsSla, chargingK, metaDataK, mobileAccAuthThresholdK);

        Map<String, Object> metaData = (Map<String, Object>) data(ncsSla, chargingK, metaDataK);
        metaData.clear();
        metaData.put(authMediumK, authorizationMedium);
        metaData.put(authThresholdK, authorizationThreshold);
    }

    @Override
    public String getName() {
       return "Dialog-Mobile-Account-MetaData-Handler";
    }
}
