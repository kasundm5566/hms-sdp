/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.caas.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.transport.Channel;

/**
 * Charging as a service Access Request workflow
 *
 * <p/>
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class NblCasAccessRequestWf extends GenericCasNcsWf implements Workflow {

    private Channel casAccessRequestService;

    @Override
    protected ServiceImpl generateWorkflowTail() {
        ServiceImpl creditReserve = new ServiceImpl("cas-access-request-service");
        creditReserve.setChannel(casAccessRequestService);


        return creditReserve;
    }
}
