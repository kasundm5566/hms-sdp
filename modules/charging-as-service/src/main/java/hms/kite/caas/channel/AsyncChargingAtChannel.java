/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.caas.channel;

import static hms.kite.util.KeyNameSpaceResolver.data;
import static hms.kite.util.KiteErrorBox.appConnectionRefusedErrorCode;
import static hms.kite.util.KiteErrorBox.atMessageFailedErrorCode;
import static hms.kite.util.KiteErrorBox.internalErrorCode;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.wfengine.impl.ResponseBuilder.generate;
import hms.common.rest.util.JsonBodyProvider;
import hms.kite.caas.util.AsyncChargingStatusCodeMapper;
import hms.kite.util.SdpException;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;

import java.net.ConnectException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import javax.ws.rs.core.Response;

import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import hms.kite.caas.api.NblParameter;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class AsyncChargingAtChannel extends BaseChannel {
	private static final Logger LOGGER = LoggerFactory.getLogger(AsyncChargingAtChannel.class);
	private LinkedList<Object> providers;
    private AsyncChargingStatusCodeMapper asyncChargingStatusCodeMapper;
    private static Logger logger = LoggerFactory.getLogger(AsyncChargingAtChannel.class);

	public void init() {
		providers = new LinkedList<Object>();
		providers.add(new JsonBodyProvider());
	}

	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
		LOGGER.trace("Received request context[{}]", requestContext);
		try {
			WebClient webClient = createWebclient(requestContext);
			Map<String, Object> atMessage = createMessage(requestContext);
			LOGGER.info("Sending caas async charging response message [{}] to url[{}]", atMessage,
					webClient.getBaseURI());
			Response response = webClient.post(atMessage);

            //todo we can not rely on the http status code, we need to read the response and interpret.

			if (response.getStatus() >= 200 && response.getStatus() <= 205) {
				LOGGER.info("Message sent successfully. response [{}]", response.getEntity());
				return ResponseBuilder.generateSuccess();
			} else {
				LOGGER.info("Message sending failed http status is [{}]", response.getStatus());
				return generate(atMessageFailedErrorCode, false, requestContext);
			}

		} catch (ClientWebApplicationException e) {
			LOGGER.error("Exception occurred while sending caas async charging response message", e);
			if (null != e.getCause() && e.getCause() instanceof Fault) {
				Fault f = (Fault) e.getCause();
				if (null != f.getCause()) {
					if (f.getCause() instanceof ConnectException) {
						return generate(appConnectionRefusedErrorCode, false, requestContext);
					}
				}
			}
			return generate(requestContext, e);
		} catch (SdpException e) {
			throw e;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return generate(requestContext, e);
		}
	}

	private WebClient createWebclient(Map<String, Map<String, Object>> requestContext) {
		String appUrl = findAppUrl(requestContext);
		if (null == appUrl) {
			LOGGER.warn("App connection url is not available");
			throw new SdpException(internalErrorCode);
		}
		WebClient webClient = WebClient.create(appUrl, providers);
		webClient.header("Content-Type", "application/json");
		webClient.accept("application/json");
		return webClient;
	}
         ///hms/apps/samples/sdp.hello.world.ant-1.0-SNAPSHOT/target/stand-alone
	private Map<String, Object> createMessage(Map<String, Map<String, Object>> requestContext) {
		Map<String, Object> params = new HashMap<String, Object>();
        String mappedStatusCode = asyncChargingStatusCodeMapper.getPgwMappedSatusCode((String)requestContext.get(requestK).get(pgwStatusCodeK));
		params.put(versionK, "1.0");
		params.put(NblParameter.TIMESTAMP.getName(), requestContext.get(requestK).get("time-stamp"));
		params.put(NblParameter.STATUS_CODE.getName(), mappedStatusCode);
		params.put(NblParameter.STATUS_DETAIL.getName(), asyncChargingStatusCodeMapper.getStatusDetailForStatusCode(mappedStatusCode));
		params.put(NblParameter.EXTERNAL_TRX_ID.getName(), requestContext.get(requestK).get(externalTransIdK));
        params.put(NblParameter.INTERNAL_TRX_ID.getName(), requestContext.get(requestK).get(internalTransIdK));
		params.put(NblParameter.PAID_AMOUNT.getName(), requestContext.get(requestK).get(paidAmountK));
		params.put(NblParameter.CURRENCY.getName(), requestContext.get(requestK).get(currencyK));
        params.put(NblParameter.REFERENCE_ID.getName(), requestContext.get(requestK).get("reference-id"));
        params.put(NblParameter.TOTAL_AMOUNT.getName(), requestContext.get(requestK).get(totalAmountK));
        if(requestContext.get(requestK).get(balanceDueK) != null)   {
            params.put(NblParameter.BALANCE_DUE.getName(), requestContext.get(requestK).get(balanceDueK));
        }
		return params;
	}

	private String findAppUrl(Map<String, Map<String, Object>> requestContext) {
		return (String) requestContext.get(ncsK).get("async-charging-resp-url");
	}

    public AsyncChargingStatusCodeMapper getAsyncChargingStatusCodeMapper() {
        return asyncChargingStatusCodeMapper;
    }

    public void setAsyncChargingStatusCodeMapper(AsyncChargingStatusCodeMapper asyncChargingStatusCodeMapper) {
        this.asyncChargingStatusCodeMapper = asyncChargingStatusCodeMapper;
    }
}
