package hms.kite.caas.channel;

import hms.kite.caas.api.NblParameter;
import hms.kite.sms.channel.GenericWfChannel;
import hms.kite.wfengine.impl.ResponseBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

/**
 * NBL channel.
 * Translates the NBL request and response to and from SDP formats.
 *
 * $LastChangedDate 7/26/11 3:40 PM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
public class GenericCasNBLChannel extends GenericWfChannel {
    private static final Logger logger = LoggerFactory.getLogger(GenericCasNBLChannel.class);

    @Override
        /**
     * Generates the response from the request context.
     *
     * @param requestContext
     * @return
     */
    protected Map<String, Object> generateResponse(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> response = doGenerate(requestContext);
        return response;
    }

     private static Map<String, Object> doGenerate(Map<String, Map<String, Object>> requestContext) {
        if (requestContext.get(requestK).get(statusCodeK) == null) {
            return generate(successK, (String) requestContext.get(requestK).get(statusDescriptionK));
        } else {
            String code = (String)requestContext.get(requestK).get(statusCodeK);
            String description = (String)requestContext.get(requestK).get(statusDescriptionK);
            if (null == description) {
                description = ResponseBuilder.getErrorDescription(code, requestContext);
                requestContext.get(requestK).put(statusDescriptionK, description);
            }
            return generate(code, description);
        }
    }

    public static Map<String, Object> generate(String code, String description) {
        Map<String, Object> resp = new HashMap<String, Object>();
        resp.put(NblParameter.STATUS_DETAIL.getName(), description);
        resp.put(NblParameter.STATUS_CODE.getName(), code);
        return resp;
    }
}
