package hms.kite.caas.channel;

import hms.kite.caas.api.NblParameter;
import hms.kite.util.KiteKeyBox;

import java.util.Map;

/**
 * NBL channel for transaction Reservation,
 * .
 * Translates the NBL request and response to and from SDP formats.
 *
 * $LastChangedDate 7/26/11 3:40 PM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
public class CreditReserveNBLChannel extends GenericCasNBLChannel {

    @Override
        /**
     * Generates the response from the request context.
     *
     * @param requestContext
     * @return
     */
    protected Map<String, Object> generateResponse(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> response = super.generateResponse(requestContext);
        Map<String, Object> originalResponse = requestContext.get(KiteKeyBox.responseK);
        if (originalResponse != null) {
            response.put(NblParameter.EXTERNAL_TRX_ID.getName(), originalResponse.get(NblParameter.EXTERNAL_TRX_ID.getName()));
            response.put(NblParameter.RESERVATION_ID.getName(), originalResponse.get(NblParameter.RESERVATION_ID.getName()));
            response.put(NblParameter.TIMESTAMP.getName(), originalResponse.get(NblParameter.TIMESTAMP.getName()));
            if(originalResponse.containsKey(NblParameter.LONG_DESCRIPTION.getName())){
            	response.put(NblParameter.LONG_DESCRIPTION.getName(), originalResponse.get(NblParameter.LONG_DESCRIPTION.getName()));
            }
            if(originalResponse.containsKey(NblParameter.SHORT_DESCRIPTION.getName())){
            	response.put(NblParameter.SHORT_DESCRIPTION.getName(), originalResponse.get(NblParameter.SHORT_DESCRIPTION.getName()));
            }
        }
        return response;
    }
}
