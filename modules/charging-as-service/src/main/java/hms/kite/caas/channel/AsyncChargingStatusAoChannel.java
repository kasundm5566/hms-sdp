/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.caas.channel;

import hms.kite.caas.api.NblParameter;
import hms.kite.util.KiteKeyBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class AsyncChargingStatusAoChannel extends GenericCasNBLChannel {

    private static final Logger logger = LoggerFactory.getLogger(AsyncChargingStatusAoChannel.class);

    @Override
    protected Map<String, Object> generateResponse(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> response = super.generateResponse(requestContext);
        Map<String, Object> originalResponse = requestContext.get(KiteKeyBox.responseK);
        if (originalResponse != null) {
            response.put(NblParameter.APPLICATION_ID.getName(), originalResponse.get(NblParameter.APPLICATION_ID.getName()));
            response.put(NblParameter.EXTERNAL_TRX_ID.getName(), originalResponse.get(NblParameter.EXTERNAL_TRX_ID.getName()));
            response.put(NblParameter.INTERNAL_TRX_ID.getName(), originalResponse.get(NblParameter.INTERNAL_TRX_ID.getName()));
        }
        return response;
    }

/* @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        logger.trace("Received async transaction status request context [{}]", requestContext);
        try {
            WebClient webClient = Http.createWebClient(findAppUrl(requestContext));
            Map<String, Object> atMessage = createRequestMessage(requestContext);
            logger.info("Sending caas async transaction status request message [{}] to url [{}]", atMessage, webClient.getBaseURI());
            Response response = webClient.post(atMessage);
            if (response.getStatus() >= 200 && response.getStatus() <= 205) {
                logger.info("Message sent successfully. response [{}]", response.getEntity());
                return createResponseMessage(requestContext, response);
            } else {
                logger.info("Message sending failed http status is [{}]", response.getStatus());
                return generate(atMessageFailedErrorCode, false, requestContext);
            }

        } catch (ClientWebApplicationException e) {
            logger.error("Exception occurred while sending caas async transaction status request message", e);
            if (null != e.getCause() && e.getCause() instanceof Fault) {
                Fault f = (Fault) e.getCause();
                if (null != f.getCause()) {
                    if (f.getCause() instanceof ConnectException) {
                        return generate(appConnectionRefusedErrorCode, false, requestContext);
                    }
                }
            }
            return generate(requestContext, e);
        } catch (SdpException e) {
            throw e;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return generate(requestContext, e);
        }
    }

    private Map<String, Object> createRequestMessage(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(versionK, "1.0");
        params.put(NblParameter.APPLICATION_ID.getName(), requestContext.get(appK).get(NblParameter.APPLICATION_ID.getName()));
        params.put(NblParameter.EXTERNAL_TRX_ID.getName(), requestContext.get(requestK).get(NblParameter.EXTERNAL_TRX_ID.getName()));
        params.put(NblParameter.INTERNAL_TRX_ID.getName(), requestContext.get(requestK).get(NblParameter.INTERNAL_TRX_ID.getName()));
        return params;
    }

    private Map<String, Object> createResponseMessage(Map<String, Map<String, Object>> requestContext, Response response) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(NblParameter.STATUS_CODE.getName(), "S1000");
        params.put(NblParameter.STATUS_TEXT.getName(), "Request Successfully Proceeded");
        params.put(NblParameter.EXTERNAL_TRX_ID.getName(), requestContext.get(requestK).get(NblParameter.EXTERNAL_TRX_ID.getName()));
        params.put(NblParameter.INTERNAL_TRX_ID.getName(), requestContext.get(requestK).get(NblParameter.INTERNAL_TRX_ID.getName()));
        return params;
    }


    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        return request;
    }

    private String findAppUrl(Map<String, Map<String, Object>> requestContext) {
        return (String) requestContext.get(ncsK).get("async-charging-resp-url");
    }*/

}
