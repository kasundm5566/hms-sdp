/*
 *   (C) Copyright 2012-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.caas.service;

import java.util.Map;
import hms.kite.CasAdapter;
import hms.kite.caas.api.NblParameter;
import hms.kite.util.KiteKeyBox;
import hms.kite.util.PgwParam;
import hms.kite.wfengine.impl.ResponseBuilder;

import static hms.kite.util.KiteKeyBox.requestK;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class CasQueryBalanceService extends AbstractCasPgwAdapterService {

    @Override
    protected Map<String, Object> createChargingRequest(Map<String, Map<String, Object>> requestContext) throws CaasParameterValidationException {
        Map<String, Object> nblRequest = requestContext.get(requestK);
        Map<String, Object> pgRequest = createPgwRequestWithAmount(nblRequest, requestContext);
        return pgRequest;
    }

    @Override
    protected Map<String, Object> createExecutionResult(Map<String, Object> chargingResponse, Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> result = ResponseBuilder.generate((String) chargingResponse.get(KiteKeyBox.statusCodeK), (String) chargingResponse.get(KiteKeyBox.statusDescriptionK), true);
       result.put(NblParameter.ACCOUNT_TYPE.getName(), chargingResponse.get(PgwParam.ACCOUNT_TYPE.getParameter()));
        result.put(NblParameter.ACCOUNT_STATUS.getName(), chargingResponse.get(PgwParam.ACCOUNT_STATUS.getParameter()));
        result.put(NblParameter.STATUS_CODE.getName(), chargingResponse.get(PgwParam.STATUS_CODE.getParameter()));
        result.put(NblParameter.STATUS_DETAIL.getName(), chargingResponse.get(PgwParam.STATUS_TEXT.getParameter()));
        Map<String, String> balanceAmount = (Map<String, String>) chargingResponse.get(PgwParam.CHARGEABLE_BALANCE.getParameter());
        result.put(NblParameter.CHARGEABLE_BALANCE.getName(), balanceAmount.get("amount-value"));
        return result;
    }

    @Override
    protected Map<String, Object> call(CasAdapter casAdapter, Map<String, Object> chargingRequest) {
        return casAdapter.queryUserBalance(chargingRequest);
    }
}
