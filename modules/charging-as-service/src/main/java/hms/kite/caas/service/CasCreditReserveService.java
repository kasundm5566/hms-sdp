/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.caas.service;

import hms.kite.CasAdapter;
import hms.kite.caas.api.NblParameter;
import hms.kite.util.KiteErrorBox;
import hms.kite.util.KiteKeyBox;
import hms.kite.util.PgwParam;
import hms.kite.wfengine.impl.ResponseBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

/**
 *
 * Charging as a Service Credit Reserve.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

public class CasCreditReserveService extends AbstractCasPgwAdapterService {

	private final static Logger logger = LoggerFactory.getLogger(CasCreditReserveService.class);

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, Object> createExecutionResult(Map<String, Object> chargingResponse,
                                                        Map<String,Map<String, Object>> requestContext) {

        Map<String, Object> result = ResponseBuilder.generate((String) chargingResponse.get(KiteKeyBox.statusCodeK),
				(String) chargingResponse.get(KiteKeyBox.statusDescriptionK), true);
        result.put(NblParameter.EXTERNAL_TRX_ID.getName(), chargingResponse.get(PgwParam.CLIENT_TRANS_ID_FOR_RESPONSE.getParameter()));
        result.put(NblParameter.RESERVATION_ID.getName(), chargingResponse.get(PgwParam.EXT_TRANSID.getParameter()));
        //result.put(NblParameter.TIMESTAMP.getName(), chargingResponse.get(PgwParam.REQUESTED_TIMESTAMP.getParameter()));
        result.put(NblParameter.TIMESTAMP.getName(), requestContext.get(requestK).get(NblParameter.TIMESTAMP.getName()));
        result.put(NblParameter.STATUS_CODE.getName(), chargingResponse.get(PgwParam.STATUS_CODE.getParameter()));
        result.put(NblParameter.STATUS_TEXT.getName(), chargingResponse.get(PgwParam.STATUS_TEXT.getParameter()));
        String chargingInstructions = (String)chargingResponse.get(PgwParam.CHARGING_INSTRUCTION.getParameter());
        if(chargingInstructions != null && !chargingInstructions.trim().isEmpty()){
        	 result.put(NblParameter.LONG_DESCRIPTION.getName(), chargingInstructions);
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String,Object> createChargingRequest(Map<String,Map<String, Object>> requestContext) {
        Map<String, Object>  nblRequest = requestContext.get(requestK);
        Map<String, Object>  pgRequest = createPgwRequestWithAmount(nblRequest, requestContext);

        return pgRequest;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, Object> call(CasAdapter casAdapter, Map<String, Object> chargingRequest) {
        return casAdapter.reserveCredit(chargingRequest);
    }
}
