package hms.kite.caas.api;

import hms.common.rest.util.Message;
import hms.kite.wfengine.transport.Channel;
import static hms.kite.util.KiteKeyBox.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;

/**
 * (C) Copyright 2011-2012 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 * <p/>
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 * <p/>
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
@Produces({ "application/json" })
@Consumes({ "application/json" })
@Path("/caas/receive")
public class AsyncChargingSblListeningEndPoint {

    private static Logger logger = LoggerFactory.getLogger(AsyncChargingSblListeningEndPoint.class);

    private Channel asyncChargingWfChannel;

    @Produces({ "application/json" })
    @Consumes({ "application/json" })
    @POST
    @Path("/async-resp")
    public Message onAsyncChargingNotification(Message msg) {
        logger.info("Received asynchronous charging notification from pgw [" + msg + "]");
        Message resp = new Message();
        //TODO This is a Hack, handle this properly. Introduced this new pgwStatusCodeK here since status code is overriding in the work flow, which status code should be send to app?
        msg.put(pgwStatusCodeK, msg.get(statusCodeK));
        resp.putAll((Map) asyncChargingWfChannel.send(msg.getParams()));
        logger.info("Sending response to pgw [{}]", msg.getParams());
        return resp;
    }

    public void setAsyncChargingWfChannel(Channel asyncChargingWfChannel) {
        this.asyncChargingWfChannel = asyncChargingWfChannel;
    }
}
