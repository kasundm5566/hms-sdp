package hms.kite.caas.channel;

import static hms.kite.util.KiteKeyBox.appIdK;
import static hms.kite.util.KiteKeyBox.casK;
import static hms.kite.util.KiteKeyBox.directionK;
import static hms.kite.util.KiteKeyBox.msisdnK;
import static hms.kite.util.KiteKeyBox.mtK;
import static hms.kite.util.KiteKeyBox.ncsTypeK;
import static hms.kite.util.KiteKeyBox.passwordK;
import static hms.kite.util.KiteKeyBox.receiveTimeK;
import static hms.kite.util.KiteKeyBox.recipientAddressK;
import static hms.kite.util.KiteKeyBox.recipientsK;
import static hms.kite.util.KiteKeyBox.requestK;
import static hms.kite.util.KiteKeyBox.telK;
import hms.kite.caas.api.NblParameter;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Pre-Condition the request, which places required parameters into the request for subsequent processing
 * $LastChangedDate 7/26/11 3:40 PM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
public class RequestPreConditionChannel implements Channel {

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> request = requestContext.get(requestK);
        request.put(receiveTimeK, Long.toString(System.currentTimeMillis()));
        request.put(ncsTypeK, casK);
        request.put(appIdK, request.get(NblParameter.APPLICATION_ID.getName()));
        request.put(passwordK, request.get(NblParameter.PASSWORD.getName()));

        String subscriberId = (String) request.get(NblParameter.SUBSCRIBER_ID.getName());

        if (subscriberId != null && subscriberId.startsWith(telK) && subscriberId.contains(":")) {
            String msisdn = subscriberId.substring(subscriberId.indexOf(":") + 1, subscriberId.length());
            request.put(msisdnK, msisdn);

            addRecipientsList(request, msisdn);
            request.put(directionK, mtK);
        }



        return ResponseBuilder.generateSuccess();
    }

    /**
     * Add Recipients List and add single address MSISDN  in order to be able to reuse other channels
     * @param request
     * @param msisdn
     */
    private void addRecipientsList(Map<String,Object> request, String msisdn) {
        List<Map<String, String>> recipients = new ArrayList<Map<String, String>>(1);
        request.put(recipientsK, recipients);
        Map<String, String> recipient = new HashMap<String, String>();
        recipient.put(recipientAddressK, msisdn);
        recipients.add(recipient);
    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        return null;  //Nothing to do
    }
}
