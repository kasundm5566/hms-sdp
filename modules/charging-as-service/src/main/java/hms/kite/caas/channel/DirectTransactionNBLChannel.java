package hms.kite.caas.channel;

import hms.kite.caas.api.NblParameter;
import hms.kite.util.KiteKeyBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * NBL channel for direct fund transaction (direct debit and credit),
 * .
 * Translates the NBL request and response to and from SDP formats.
 *
 * $LastChangedDate 7/26/11 3:40 PM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
public class DirectTransactionNBLChannel extends GenericCasNBLChannel {
    private static final Logger logger = LoggerFactory.getLogger(DirectTransactionNBLChannel.class);

    @Override
        /**
     * Generates the response from the request context.
     *
     * @param requestContext
     * @return
     */
    protected Map<String, Object> generateResponse(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> response = super.generateResponse(requestContext);
        Map<String, Object> originalResponse = requestContext.get(KiteKeyBox.responseK);
        if (originalResponse != null) {
            response.put(NblParameter.EXTERNAL_TRX_ID.getName(), originalResponse.get(NblParameter.EXTERNAL_TRX_ID.getName()));
            response.put(NblParameter.INTERNAL_TRX_ID.getName(), originalResponse.get(NblParameter.INTERNAL_TRX_ID.getName()));
            response.put(NblParameter.TIMESTAMP.getName(), originalResponse.get(NblParameter.TIMESTAMP.getName()));
            if(originalResponse.containsKey(NblParameter.LONG_DESCRIPTION.getName())){
            	response.put(NblParameter.LONG_DESCRIPTION.getName(), originalResponse.get(NblParameter.LONG_DESCRIPTION.getName()));
            }
            if(originalResponse.containsKey(NblParameter.SHORT_DESCRIPTION.getName())){
            	response.put(NblParameter.SHORT_DESCRIPTION.getName(), originalResponse.get(NblParameter.SHORT_DESCRIPTION.getName()));
            }
            response.put(NblParameter.REFERENCE_ID.getName(), originalResponse.get(NblParameter.REFERENCE_ID.getName()));
            response.put(NblParameter.BUSINESS_NO.getName(), originalResponse.get(NblParameter.BUSINESS_NO.getName()));
            response.put(NblParameter.AMOUNT_DUE.getName(), originalResponse.get(NblParameter.AMOUNT_DUE.getName()));
        }
        return response;
    }
}
