package hms.kite.caas.channel;

import hms.kite.caas.api.NblParameter;
import hms.kite.util.KiteKeyBox;

import java.util.Map;

/**
 * NBL channel for transaction Reservation,
 * .
 * Translates the NBL request and response to and from SDP formats.
 *
 * $LastChangedDate 7/26/11 3:40 PM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
public class AccessRequestNBLChannel extends GenericCasNBLChannel {

    @Override
        /**
     * Generates the response from the request context.
     *
     * @param requestContext
     * @return
     */
    protected Map<String, Object> generateResponse(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> response = super.generateResponse(requestContext);
        Map<String, Object> originalResponse = requestContext.get(KiteKeyBox.responseK);
        if (originalResponse != null) {
            response.put(NblParameter.SUBSCRIBER_ID.getName(), originalResponse.get(NblParameter.SUBSCRIBER_ID.getName()));
            response.put(NblParameter.INTERNAL_TRX_ID.getName(), originalResponse.get(NblParameter.INTERNAL_TRX_ID.getName()));
            response.put(NblParameter.TIMESTAMP.getName(), originalResponse.get(NblParameter.TIMESTAMP.getName()));
        }
        return response;
    }
}
