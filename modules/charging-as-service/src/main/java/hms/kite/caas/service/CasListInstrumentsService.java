/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.caas.service;

import com.mongodb.BasicDBList;
import hms.kite.CasAdapter;
import hms.kite.caas.api.NblParameter;
import hms.kite.util.KeyNameSpaceResolver;
import hms.kite.util.KiteErrorBox;
import hms.kite.util.PgwParam;
import hms.kite.util.SdpException;
import hms.kite.wfengine.impl.ResponseBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteErrorBox.caasPiNotAllowedErrorCode;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.KiteKeyBox.allowedPaymentInstrumentsK;
import static hms.kite.util.KiteKeyBox.chargingK;

public class CasListInstrumentsService extends AbstractCasPgwAdapterService {

	private final static Logger logger = LoggerFactory.getLogger(CasListInstrumentsService.class);

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, Object> createExecutionResult(Map<String, Object> chargingResponse,
                                                        Map<String,Map<String, Object>> requestContext) {


        Map<String, Object> result = ResponseBuilder.generateSuccess();
        List<Map<String, String>> paymentInstrumentsList = filterAllowedPi(chargingResponse, requestContext);;
        logger.debug("Payment Gateway Response[{}]", chargingResponse);

        List<Map<String, String>> paymentInstrumentsNblList = new ArrayList<Map<String, String>>();

        for(Map<String, String> pi: paymentInstrumentsList) {
            Map<String, String> paymentInstrument = new HashMap<String, String>(2);
            paymentInstrument.put(NblParameter.PAYMENT_INSTRUMENT_PI_RES_NAME.getName(), pi.get(PgwParam.PAYINS_NAME.getParameter()));
            paymentInstrument.put(NblParameter.PAYMENT_INSTRUMENT_TYPE.getName(), pi.get(PgwParam.PAYINS_TYPE.getParameter()));
            paymentInstrumentsNblList.add(paymentInstrument);
        }

        result.put(NblParameter.PAYMENT_INSTRUMENT_LIST.getName(), paymentInstrumentsNblList);

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String,Object> createChargingRequest(Map<String,Map<String, Object>> requestContext) {
        Map<String, Object>  result = new HashMap<String, Object>();
        Map<String, Object>  nblRequest = requestContext.get(requestK);

        String subscriberId = (String) nblRequest.get(NblParameter.SUBSCRIBER_ID.getName());
        String msisdn = getMsisdn(requestContext);
        if (msisdn != null) {
            result.put(PgwParam.MSISDN.getParameter(), msisdn);
        } else if (subscriberId != null && subscriberId.startsWith("id:")) {
            subscriberId = subscriberId.substring(subscriberId.indexOf(":") + 1, subscriberId.length());
            result.put(PgwParam.USER_ID_DASHED.getParameter(), subscriberId);
        }else {
            logger.error("Subscriber ID format is not valid for CaaS [{}]", subscriberId);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, Object> call(CasAdapter casAdapter, Map<String, Object> chargingRequest) {
        return casAdapter.queryPaymentInstruments(chargingRequest);
    }

    private List<Map<String, String>> filterAllowedPi(Map<String, Object> chargingResponse, Map<String, Map<String, Object>> requestContext) {
        BasicDBList allowedPiList = (BasicDBList) KeyNameSpaceResolver.data(requestContext, ncsK, chargingK, allowedPaymentInstrumentsK);
        List<Map<String, String>> paymentInstrumentsList = (List<Map<String, String>>) chargingResponse.get("payins-response");

        List<Map<String, String>> filteredPiList = new ArrayList<Map<String, String>>();

        for(Map<String, String> pi : paymentInstrumentsList){
            for( Object allowedPi : allowedPiList.toArray()){
                String piName = getPiName(pi);
                if( piName.startsWith((String) allowedPi)){
                    filteredPiList.add(pi);
                    break;
                }
            }
        }
        logger.debug("Payment Instrument list after filtering[{}]", filteredPiList);
       return filteredPiList;
    }

    private String getPiName(Map<String, String> pi) {
        String name = pi.get(PgwParam.PAYINS_NAME.getParameter());
        return name != null ? name : "";
    }
}
