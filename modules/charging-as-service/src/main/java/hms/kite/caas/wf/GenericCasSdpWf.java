/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.caas.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.control.InExpression;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.springframework.beans.factory.annotation.Autowired;

import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;

/**
 * Charging as a service (Generic) NBL workflow
 *
 * <p/>
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class GenericCasSdpWf extends WrappedGeneratedWorkflow implements Workflow {

    /**
     * Name of the NCS channel this would forward the request to.
     */
//    private String nscChannelName;

    @Autowired private Channel provAppSlaChannel;
    @Autowired private Channel provSpSlaChannel;
    @Autowired private Channel blacklistingService;
    @Autowired private Channel mnpService;
    @Autowired private Channel numberStatusCheckChannel;

    //This is not auto wired need to set explicitly
    private Channel ncsChannel;

    @Override
    protected Workflow generateWorkflow() {

        ServiceImpl spSlaService = new ServiceImpl("prov.sp.sla.channel");
        spSlaService.setChannel(provSpSlaChannel);
        spSlaService.setOnSuccess(casServiceChannel());

        ServiceImpl appSlaService = new ServiceImpl("prov.app.sla.channel");
        appSlaService.setChannel(provAppSlaChannel);
        appSlaService.setOnSuccess(spSlaService);

        return new WorkflowImpl(appSlaService);
    }

    private ServiceImpl casServiceChannel() {
        return new ServiceImpl("cas.service.channel").chain(new Condition(new EqualExpression(spK, statusK, approvedK), spNotAvailableErrorCode))
                                                     .chain(new Condition(new InExpression(appK, statusK, new Object[]{limitedProductionK, productionK, activeProductionK}), appNotAvailableErrorCode))
                                                     .chain(getName(mnpService)).attachChannel(mnpService)
                                                     .chain(getName(numberStatusCheckChannel)).attachChannel(numberStatusCheckChannel)
                                                     .chain(getName(blacklistingService)).attachChannel(blacklistingService)
                                                     .chain(getName(ncsChannel)).attachChannel(ncsChannel);
    }

//    /**
//     * Sets the NCS channel name
//     * @param nscChannelName
//     */
//    public void setNcsChannelName(String nscChannelName) {
//        this.nscChannelName = nscChannelName;
//    }


    public void setNcsChannel(Channel ncsChannel) {
        this.ncsChannel = ncsChannel;
    }
}
