package hms.kite.caas.service.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.util.KeyNameSpaceResolver.data;
import static hms.kite.util.KiteKeyBox.*;

public class MpesaPayBillNumberMetaDataHandler implements PiSpecificMetaDataHandler {
    private static Logger LOGGER = LoggerFactory.getLogger(MpesaPayBillNumberMetaDataHandler.class);

    public void handle(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> request = requestContext.get(requestK);
        Map<String, Object> ncsSla = requestContext.get(ncsK);
        LOGGER.debug("Request received to handle request[{}], ncsSla[{}]", request, ncsSla);

        String allowedPayBillNumber = (String)data(ncsSla, chargingK, metaDataK, mpesaPayBillNoK);

        Map<String, Object> metaData = (Map<String, Object>) data(ncsSla, chargingK, metaDataK);
        metaData.clear();
        metaData.put(mpesaPayBillNoK, allowedPayBillNumber);
    }

    public String getName(){
        return "Mpesa-PayBill-MetaDataHandler";
    }

}
