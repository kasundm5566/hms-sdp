package hms.kite.caas.service.handler;

import hms.kite.util.KiteErrorBox;
import hms.kite.util.SdpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.KeyNameSpaceResolver.*;

public class MpesaBuyGoodsTillNumberMetaDataHandler implements PiSpecificMetaDataHandler {
    private static Logger LOGGER = LoggerFactory.getLogger(MpesaBuyGoodsTillNumberMetaDataHandler.class);

    public void handle(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> request = requestContext.get(requestK);
        Map<String, Object> ncsSla = requestContext.get(ncsK);
        LOGGER.debug("Request received to handle request[{}], ncsSla[{}]", request, ncsSla);

        List<String> allowedTillNumbers = (List<String>)data(ncsSla, chargingK, metaDataK, mpesaBuyGoodsTillNoK);
        String requestedTillNumber = (String)data(request, "extra", "tillNo");

        String partialChargingAllowed = (String) data(ncsSla, chargingK, metaDataK, partialMsisdnAllowedK);

        LOGGER.debug("Extracted allowedTillNo[{}], requestedTill[{}]", allowedTillNumbers, requestedTillNumber);
        if(allowedTillNumbers != null && allowedTillNumbers.contains(requestedTillNumber)){
            LOGGER.info("Requested tillNo[{}] is a allowed one", requestedTillNumber);
            Map<String, Object>  metaData = (Map<String, Object>) data(ncsSla, chargingK, metaDataK);
            metaData.clear();
            metaData.put(mpesaBuyGoodsTillNoK, requestedTillNumber);
            metaData.put(partialMsisdnAllowedK, partialChargingAllowed);
        }  else {
            throw new SdpException(KiteErrorBox.invalidRequestErrorCode, "Till number not allowed[" + requestedTillNumber +"]");
        }
    }

    public String getName(){
        return "Mpesa-BuyGoods-MetaDataHandler";
    }



}
