package hms.kite.caas.api;

import hms.kite.util.KiteKeyBox;

/**
 * Charging as a service NBL parameter definitions and parameter names.
 *
 * $LastChangedDate 8/1/11 4:54 PM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
public enum NblParameter {

    ACCOUNT_CURRENCY("accountCurrency"),
    ACCOUNT_ID("accountId"),
    APPLICATION_ID("applicationId"),
    PASSWORD(KiteKeyBox.passwordK),
    EXTERNAL_TRX_ID("externalTrxId"),
    INTERNAL_TRX_ID("internalTrxId"),
    PAYMENT_INSTRUMENT_LIST("paymentInstrumentList"),
    PAYMENT_INSTRUMENT_ACCOUNT_ID("paymentInstrumentAccountId"),
    PAYMENT_INSTRUMENT_ID ("paymentInstrumentId"),
    PAYMENT_INSTRUMENT_NAME("paymentInstrumentName"),
    PAYMENT_INSTRUMENT_TYPE("type"),
    PAYMENT_INSTRUMENT_PI_RES_NAME("name"),
    AMOUNT("amount"),
    CURRENCY("currency"),
    INVOICE_NO("invoiceNo"),
    IS_DEFAULT("isDefault"),
    ORDER_NO("orderNo"),
    RESERVATION_ID("reservationId"),
    TIMESTAMP("timeStamp"),
    STATUS_CODE("statusCode"),
    STATUS_TEXT("statusText"),
    SUBSCRIBER_ID("subscriberId"),
    LONG_DESCRIPTION("longDescription"),
    SHORT_DESCRIPTION("shortDescription"),
    REFERENCE_ID("referenceId"),
    BUSINESS_NO("businessNumber"),
    STATUS_DETAIL("statusDetail"),
    AMOUNT_DUE("amountDue"),
    PAID_AMOUNT("paidAmount"),
    TOTAL_AMOUNT("totalAmount"),
    BALANCE_DUE("balanceDue"),
    IS_OVER_PAYMENT_ALLOWED("allowOverPayments"),
    IS_PARTIAL_PAYMENT_ALLOWED("allowPartialPayments"),
    TIMEOUT_PERIOD("timeoutPeriod"),
    ACCOUNT_TYPE("accountType"),
    ACCOUNT_STATUS("accountStatus"),
    CHARGEABLE_BALANCE("chargeableBalance"),
    EZCASH_AGENT_PIN("ezcashAgentPin"),
    EZCASH_AGENT_ALIAS("ezcashAgentAlias"),
    ADDITIONAL_PARAMS("additionalParams");


    private String parameter;
    NblParameter(String value){
        this.parameter = value;
    };

    public String getName() {
        return parameter;
    }
}
