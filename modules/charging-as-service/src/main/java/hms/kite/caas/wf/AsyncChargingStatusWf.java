/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.caas.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.control.InExpression;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.springframework.beans.factory.annotation.Autowired;

import static hms.kite.util.KiteErrorBox.appNotAvailableErrorCode;
import static hms.kite.util.KiteErrorBox.ncsNotAllowedErrorCode;
import static hms.kite.util.KiteErrorBox.spNotAvailableErrorCode;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class AsyncChargingStatusWf extends WrappedGeneratedWorkflow implements Workflow {

    private Channel casAsyncChargingStatusService;
     private Channel asyncChargingStatusChannel;
     private Channel provSpSlaChannel;
     private Channel provAppSlaChannel;

    @Override
    protected Workflow generateWorkflow() {
        ServiceImpl atChannel = new ServiceImpl("async-charging-status-channel");
		atChannel.setChannel(asyncChargingStatusChannel);

		ServiceImpl appStateValidationService = createAppSlaValidationRule();
		appStateValidationService.setOnSuccess(atChannel);

		ServiceImpl spSlaValidation = createSpSlaValidationService();
		spSlaValidation.setOnSuccess(appStateValidationService);

		ServiceImpl spSlaChannel = new ServiceImpl("prov.sp.sla.channel", spSlaValidation);
		spSlaChannel.setChannel(provSpSlaChannel);

		ServiceImpl appSlaChannel = new ServiceImpl("prov.app.sla.channel", spSlaChannel);
		appSlaChannel.setChannel(provAppSlaChannel);

        ServiceImpl asyncServiceChannel = appSlaChannel.chain("casAsyncChargingStatusService")
                .attachChannel(casAsyncChargingStatusService);

		return new WorkflowImpl(asyncServiceChannel);
    }

    private static ServiceImpl createAppSlaValidationRule() {
		InExpression statusExpression = new InExpression(appK, statusK, new Object[] { limitedProductionK, activeProductionK });
		Condition statusCondition = new Condition(statusExpression, appNotAvailableErrorCode);

		return new ServiceImpl("app.state.validation.service", statusCondition);
	}

	private static ServiceImpl createSpSlaValidationService() {
		EqualExpression statusExpression = new EqualExpression(spK, statusK, approvedK);
		Condition statusCondition = new Condition(statusExpression, spNotAvailableErrorCode);

		InExpression ncsAllowedExpression = new InExpression(spK, spSelectedServicesK, new Object[] { casK });
		Condition ncsAllowedCondition = new Condition(ncsAllowedExpression, ncsNotAllowedErrorCode);

		return new ServiceImpl("sp.state.validation.service", statusCondition, ncsAllowedCondition);
	}

}
