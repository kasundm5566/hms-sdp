package hms.kite.caas.service;

import com.google.common.base.Optional;
import com.mongodb.BasicDBList;
import hms.kite.CasAdapter;
import hms.kite.caas.Utils.CaasFeatureRegistry;
import hms.kite.caas.api.NblParameter;
import hms.kite.caas.service.handler.PiSpecificMetaDataHandler;
import hms.kite.util.*;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.*;

import static hms.kite.util.KeyNameSpaceResolver.data;
import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;

/**
 * Abstract class which holds the common logic to call CasAdapter.
 *
 * $LastChangedDate 7/27/11 9:50 AM$ $LastChangedBy ruwan$ $LastChangedRevision$
 */
/* package private */abstract class AbstractCasPgwAdapterService extends BaseChannel {

	private final static Logger logger = LoggerFactory.getLogger(AbstractCasPgwAdapterService.class);

	protected final static String subscriberId_idK = "id";

    private int externalTrxIdMaxLen;
    private Map<String, PiSpecificMetaDataHandler> piSpecificMetaDataHandlerMap;
    private List<String> additionalNblParams = Collections.emptyList();
    private Map<String, String> paymentInsSystemAccountIdMap = Collections.emptyMap();

	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
		logger.debug("Message received [{}] ", requestContext.get(requestK));

		try {
			CasAdapter casAdapter = new CasAdapter();
            preProcessMetaData(requestContext);
            validateRequest(requestContext);
			Map<String, Object> chargingRequest = createChargingRequest(requestContext);
			logger.debug("Created charging request[{}]", chargingRequest);
			Map<String, Object> chargingResponse = call(casAdapter, chargingRequest);
			logger.debug("Charging response[{}]", chargingResponse);

			// Check the response
			if (chargingResponse == null) {
				return ResponseBuilder.generate(KiteErrorBox.internalErrorCode, "Charging response is null", false);
			}

			addRateCardToContext(requestContext, chargingResponse);
			addCurrencyToContext(requestContext, chargingResponse);
            logger.info("Response after added rate card [{}]", chargingResponse);

			String statusCode = (String) chargingResponse.get(KiteKeyBox.statusCodeK);
			if (statusCode == null) {
				return ResponseBuilder.generate(KiteErrorBox.internalErrorCode,
						"Charging response status code is null", false);
			}

			if (! (paymentPendingNotificationCode.equals(statusCode)
                    || pendingPaymentErrorCode.equals(statusCode)
                    || successCode.equals(statusCode))) {

				String statusDescription = (String) chargingResponse.get(KiteKeyBox.statusDescriptionK);
				if (statusDescription == null) {
					statusDescription = (String) chargingResponse.get(PgwParam.STATUS_TEXT.getParameter());
                    //todo pgw will not send a status-text. We need to send proper error code in this case
				}

                Map<String, Object> result = ResponseBuilder.generate(statusCode, statusDescription, false);
                result.put(PgwParam.FROM_PAYMENT_INS_NAME.getParameter(), chargingResponse.get(PgwParam.FROM_PAYMENT_INS_NAME.getParameter()));
                requestContext.put(responseK, result);
                return result;
			}

			Map<String, Object> result = createExecutionResult(chargingResponse, requestContext);
			requestContext.put(responseK, result);
			return result;
		} catch (SdpException ex) {
			logger.debug("failed to call charging gateway: ", ex);
			return ResponseBuilder.generate(ex.getErrorCode(), ex.getErrorDescription(), false);
		} catch (Throwable ex) {
			logger.error("failed to call charging gateway: ", ex);
			return ResponseBuilder.generate(KiteErrorBox.internalErrorCode,
					String.format("failed to call charging gateway: %s", ex.getMessage()), false);
		}
	}

    /**
     * Process and removes unwanted charging metadata
     * Keeps only the meta data that are specific to the payment instrument
     * @param requestContext
     */
    private void preProcessMetaData(Map<String, Map<String, Object>> requestContext) {
        String piName = (String) data(requestContext, requestK, NblParameter.PAYMENT_INSTRUMENT_NAME.getName());
        logger.debug("Request received for Pi[{}]", piName);
        PiSpecificMetaDataHandler metaDatahandler = getMetadataHandler(piName);
        if (metaDatahandler != null) {
            logger.debug("Metadata Handler [{}] selected", metaDatahandler.getName());
            metaDatahandler.handle(requestContext);
        } else {
            logger.debug("No Metadata handler available to handle request.");
        }
    }

    /**
     * Returns the meta data handler specified for the given payment instrument name
     * @param piName
     * @return
     */
    private PiSpecificMetaDataHandler getMetadataHandler(String piName) {
        if (piSpecificMetaDataHandlerMap != null) {
            if(piSpecificMetaDataHandlerMap.containsKey(piName)) {
                return piSpecificMetaDataHandlerMap.get(piName);
            }
            else {
                logger.debug("Metadata Handler not available for payment instrument [{}]", piName);
                return piSpecificMetaDataHandlerMap.get("default");
            }
        } else return null;
    }

    public void setPiSpecificMetaDataHandlerMap(Map<String, PiSpecificMetaDataHandler> piSpecificMetaDataHandlerMap) {
        this.piSpecificMetaDataHandlerMap = piSpecificMetaDataHandlerMap;
    }

	private void addRateCardToContext(Map<String, Map<String, Object>> requestContext,
			Map<String, Object> chargingResponse) {
		Map<String, Object> request = requestContext.get(requestK);
		if (!request.containsKey(usedExchangeRatesK)) {
			if (chargingResponse.containsKey(usedExchangeRatesK)) {
				request.put(usedExchangeRatesK, chargingResponse.get(usedExchangeRatesK));
			}
		}
		logger.trace("Request context after adding rate-card[{}]", requestContext);
	}

    private void addCurrencyToContext(Map<String, Map<String, Object>> requestContext,
                                      Map<String, Object> chargingResponse) {
        Map<String, Object> request = requestContext.get(requestK);
        if (!request.containsKey(currencyK)) {
            if (chargingResponse.containsKey(usedExchangeRatesK)) {
                request.put(currencyK, chargingResponse.get(currencyCodeK));
            }
        }
        logger.trace("Request context after adding rate-card[{}]", requestContext);
    }

	/**
	 * Creates the Payment Gateway request.
	 *
	 * @param request
	 * @return
	 */
	protected Map<String, Object> createPgwRequest(Map<String, Object> request,
			Map<String, Map<String, Object>> requestContex) {
		Map<String, Object> application = requestContex.get(KiteKeyBox.appK);
		Map<String, Object> pgRequest = new HashMap<String, Object>();

		pgRequest.put(PgwParam.CLIENT_TRANS_ID.getParameter(), request.get(NblParameter.EXTERNAL_TRX_ID.getName()));
		pgRequest.put(PgwParam.REQUESTED_TIMESTAMP.getParameter(), ISODateTimeFormat.dateTime().print(new DateTime())); // Timestamp
																														// is
																														// considered
																														// the
																														// current
																														// time

		return pgRequest;
	}

    /**
     * Creates the Payment Gateway request for transaction request status
     * @param request
     * @param requestContex
     * @return
     */
	protected Map<String, Object> createPgwTransactionStatusRequest(Map<String, Object> request,
			Map<String, Map<String, Object>> requestContex) {
		Map<String, Object> pgRequest = new HashMap<String, Object>();

		pgRequest.put(PgwParam.TRANSACTION_ID.getParameter(), request.get(NblParameter.EXTERNAL_TRX_ID.getName()));
		pgRequest.put(PgwParam.REQUESTED_TIMESTAMP.getParameter(), ISODateTimeFormat.dateTime().print(new DateTime()));

		return pgRequest;
	}

	/**
	 * Creates the Payment Gateway request.
	 *
	 * @param request
	 * @return
	 */
	protected Map<String, Object> createPgwRequestWithAmount(Map<String, Object> request,
			Map<String, Map<String, Object>> requestContex) {
		Map<String, Object> application = requestContex.get(KiteKeyBox.appK);
		Map<String, Object> pgRequest = new HashMap<String, Object>();

		processTrustedNCS(pgRequest, requestContex);

		pgRequest.put(PgwParam.MERCHANT_ID.getParameter(), request.get(NblParameter.APPLICATION_ID.getName()));
		pgRequest.put(PgwParam.MERCHANT_NAME.getParameter(), application.get(KiteKeyBox.nameK));

		pgRequest.put(PgwParam.CLIENT_TRANS_ID.getParameter(), request.get(NblParameter.EXTERNAL_TRX_ID.getName()));
		String timestampString = ISODateTimeFormat.dateTime().print(new DateTime());
		pgRequest.put(PgwParam.REQUESTED_TIMESTAMP.getParameter(), timestampString);
		request.put(NblParameter.TIMESTAMP.getName(), timestampString);

		// Amount
        if(request.get(NblParameter.AMOUNT.getName()) != null)  {
		    Map<String, Object> amount = new HashMap<String, Object>();
		    amount.put(PgwParam.AMOUNT_VALUE.getParameter(), request.get(NblParameter.AMOUNT.getName()));
		    amount.put(PgwParam.CURRENCY_CODE.getParameter(), request.get(NblParameter.CURRENCY.getName()));
		    pgRequest.put(PgwParam.AMOUNT.getParameter(), amount);
        }

		// additionalParams
		Map<String, Object> additionalParams = new HashMap<String, Object>();
		additionalParams.put(NblParameter.INVOICE_NO.getName(), request.get(NblParameter.INVOICE_NO.getName()));
		additionalParams.put(NblParameter.ORDER_NO.getName(), request.get(NblParameter.ORDER_NO.getName()));

        if (CaasFeatureRegistry.getDialoEzcashPayInsName()
                .equals(request.get(NblParameter.PAYMENT_INSTRUMENT_NAME.getName()))) {
                if (request.get(NblParameter.ADDITIONAL_PARAMS.getName()) != null) {
                    Map<String,Object> ezcashAgentAdditionParams = (Map<String,Object>)request.get(NblParameter.ADDITIONAL_PARAMS.getName());
                    validateEzcashAgentAlias(ezcashAgentAdditionParams, additionalParams);
                    validateEzcashAgentPin(ezcashAgentAdditionParams, additionalParams);
                } else {
                    logger.error("Ezcash Agent Details Required.");
                    throw new CaasParameterValidationException(KiteErrorBox.invalidRequestErrorCode, CaasFeatureRegistry.getEzcashAgentDetailsReqMsg());
                }
        }

        addNcsMetadata(requestContex, additionalParams);

        addChargingMetadata(requestContex, additionalParams);

        addPaymentInstrumentAdditionalData(requestContex, additionalParams);

        addNblAdditionalParameters(request, additionalParams);

        if (KeyNameSpaceResolver.contains(requestContex, KiteKeyBox.ncsK, KiteKeyBox.serviceChargePercentageK)) {
            additionalParams.put(KiteKeyBox.serviceChargePercentageK,
                    KeyNameSpaceResolver.data(requestContex, KiteKeyBox.ncsK, KiteKeyBox.serviceChargePercentageK));
        }

		additionalParams.put(NblParameter.ORDER_NO.getName(), request.get(NblParameter.ORDER_NO.getName()));
		pgRequest.put(PgwParam.ADDITIONAL_PARAMS.getParameter(), additionalParams);

        if (request.get(NblParameter.IS_OVER_PAYMENT_ALLOWED.getName()) != null) {
            pgRequest.put(PgwParam.IS_OVERPAYMENT_ALLOWED.getParameter(),
                    convertPaymentMethodParameter((String) request.get(NblParameter.IS_OVER_PAYMENT_ALLOWED.getName())));
        }
        if (request.get(NblParameter.IS_PARTIAL_PAYMENT_ALLOWED.getName()) != null) {
            pgRequest.put(PgwParam.IS_PARTIAL_PAYMENT_ALLOWED.getParameter(),
                    convertPaymentMethodParameter((String) request.get(NblParameter.IS_PARTIAL_PAYMENT_ALLOWED.getName())));
        }
        if(request.get(NblParameter.TIMEOUT_PERIOD.getName()) != null)   {
            pgRequest.put(PgwParam.TIMEOUT_PERIOD.getParameter(), request.get(NblParameter.TIMEOUT_PERIOD.getName()));
        }

		pgRequest.put(PgwParam.CHARGED_CATEGORY.getParameter(), casK);
		pgRequest.put(PgwParam.SYSTEM_ID.getParameter(), systemIdSdpK);
		pgRequest.put(PgwParam.REQUIESTED_APP_ID.getParameter(), application.get(appIdK));

		addPayerDetails(request, pgRequest, requestContex);

		return pgRequest;
	}

    /*
    * Addtion payment instrument releated addtional data from cas ncs sla.
    * */
    private void addPaymentInstrumentAdditionalData(Map<String, Map<String, Object>> requestContext, Map<String, Object> additionalParams) {
        try {

            Object payInsAdditionalData = KeyNameSpaceResolver.data(requestContext, ncsK, chargingK, paymentInstrumentAdditionalDataK);
            Object paymentInstrumentName = KeyNameSpaceResolver.data(requestContext, requestK, NblParameter.PAYMENT_INSTRUMENT_NAME.getName());

            if(Optional.fromNullable(payInsAdditionalData).isPresent() && Optional.fromNullable(paymentInstrumentName).isPresent())  {

                logger.debug("Adding payment instrument [{}], additional data [{}] to pgw request.", paymentInstrumentName, payInsAdditionalData);

                Map payInsAdditionalDataAsMap = (Map) payInsAdditionalData;
                Object realTimeCharging = KeyNameSpaceResolver.data(payInsAdditionalDataAsMap, (String) paymentInstrumentName, realTimeChargingEnabledK);

                if(Optional.fromNullable(realTimeCharging).isPresent()) {
                    Object businessId = KeyNameSpaceResolver.data(payInsAdditionalDataAsMap, (String) paymentInstrumentName, businessIdK);
                    Boolean realTimeChargingEnabled = (Boolean) realTimeCharging;

                    Optional paymentInstrumentBusinessIdOpt = Optional.absent();

                    if(realTimeChargingEnabled && Optional.fromNullable(businessId).isPresent()) {
                        paymentInstrumentBusinessIdOpt = Optional.fromNullable(businessId);
                    } else {
                        String systemBusinessId = paymentInsSystemAccountIdMap.get(paymentInstrumentName);
                        if(Optional.fromNullable(systemBusinessId).isPresent()) {
                            paymentInstrumentBusinessIdOpt = Optional.fromNullable(systemBusinessId);
                        }
                    }

                    if(paymentInstrumentBusinessIdOpt.isPresent()) {
                        Object paymentInstrumentBusinessId = paymentInstrumentBusinessIdOpt.get();
                        additionalParams.put(businessIdK, paymentInstrumentBusinessId);
                        requestContext.get(requestK).put(businessIdK, paymentInstrumentBusinessId);
                    }
                }
            }
        } catch (ClassCastException | NullPointerException e) {
            logger.debug("Error occurred while adding payment instrument additional data [{}].", e);
        }
    }

    private void validateEzcashAgentAlias(Map<String, Object> nblRequest, Map<String, Object> additionalParams) {
        if (nblRequest.get(NblParameter.EZCASH_AGENT_ALIAS.getName()) != null) {
            String ezcashAgentAlias = (String)nblRequest.get(NblParameter.EZCASH_AGENT_ALIAS.getName());
            additionalParams.put(NblParameter.EZCASH_AGENT_ALIAS.getName(), ezcashAgentAlias);
        } else {
            logger.error("Ezcash Agent Transaction Alias Required.");
            throw new CaasParameterValidationException(KiteErrorBox.invalidRequestErrorCode, CaasFeatureRegistry.getEzcashAgentAliasReqMsg());
        }
    }

    private void validateEzcashAgentPin(Map<String, Object> nblRequest, Map<String, Object> additionalParams) {
        if (nblRequest.get(NblParameter.EZCASH_AGENT_PIN.getName()) != null) {
            String ezcashAgentPin = (String)nblRequest.get(NblParameter.EZCASH_AGENT_PIN.getName());
            additionalParams.put(NblParameter.EZCASH_AGENT_PIN.getName(), ezcashAgentPin);
        } else {
            logger.error("Ezcash Agent Transaction Pin Required.");
            throw new CaasParameterValidationException(KiteErrorBox.invalidRequestErrorCode, CaasFeatureRegistry.getEzcashAgentPinReqMsg());
        }
    }

    private void addNblAdditionalParameters(Map<String, Object> request, Map<String, Object> additionalParams) {
        final List<String> additionalParametersForNblRequest = getAdditionalParametersFromNblRequest();
        if(additionalParametersForNblRequest.isEmpty()) {
            return;
        } else {
            if(request.get(additionalParamsK) != null) {
                try {
                    final Map<String, String> requestAdditionalParameters =  (Map<String, String>) request.get(additionalParamsK);
                    for(String param : additionalParametersForNblRequest) {
                        additionalParams.put(param, requestAdditionalParameters.get(param));
                    }
                } catch (ClassCastException e) {
                    throw new SdpException(invalidRequestErrorCode);
                }
            } else {
                return;
            }
        }
    }

    private void addChargingMetadata(Map<String, Map<String, Object>> requestContext, Map<String, Object> additionalParams) {
        Map<String, String> chargingMetaData = (Map<String, String>) KeyNameSpaceResolver.data(requestContext, ncsK, chargingK, metaDataK);
        if(chargingMetaData != null){
            for(Map.Entry<String, String> item:  chargingMetaData.entrySet()){
                additionalParams.put(item.getKey(), item.getValue());
            }
        }
    }

    private void addNcsMetadata(Map<String, Map<String, Object>> requestContex, Map<String, Object> additionalParams) {
        Map<String, String> metaData = (Map<String, String>) KeyNameSpaceResolver.data(requestContex, KiteKeyBox.ncsK, KiteKeyBox.metaDataK);
        if(metaData != null){
           for(Map.Entry<String, String> item:  metaData.entrySet()){
               additionalParams.put(item.getKey(), item.getValue());
           }
        }
    }

    private String convertPaymentMethodParameter(String aaa) {
        String status = Boolean.FALSE.toString();
        if("Allow".equalsIgnoreCase(aaa))   {
            status = Boolean.TRUE.toString();
        } else {
            status = Boolean.FALSE.toString();
        }
        return status;
    }

	/**
	 * Adds the payer details
	 *
	 * @param nblRequest
	 * @param pgRequest
	 */
	private void addPayerDetails(Map<String, Object> nblRequest, Map<String, Object> pgRequest,
			Map<String, Map<String, Object>> requestContext) throws CaasParameterValidationException {
		Map<String, Object> payerDetails = new HashMap<String, Object>();
		pgRequest.put(PgwParam.PAYER_DETAILS.getParameter(), payerDetails);

		String subscriberId = (String) nblRequest.get(NblParameter.SUBSCRIBER_ID.getName());

		String msisdn = getMsisdn(requestContext);
		if (msisdn != null) {
			payerDetails.put(PgwParam.MSISDN.getParameter(), msisdn);
		} else if (subscriberId != null && subscriberId.startsWith(subscriberId_idK)) {
			subscriberId = subscriberId.substring(subscriberId.indexOf(":") + 1, subscriberId.length());
			payerDetails.put(PgwParam.USER_ID.getParameter(), subscriberId);
		} else {
			logger.error("Subscriber ID format is not valid for CaaS [{}]", subscriberId);
			throw new CaasParameterValidationException(KiteErrorBox.invalidRequestErrorCode, String.format(
					"Subscriber ID [%s] format is not valid for CaaS [%s]. Expected tel:MSISDN or id:USER_ID",
					NblParameter.SUBSCRIBER_ID.getName(), subscriberId));
		}

		payerDetails.put(PgwParam.PAYMENT_INS_NAME.getParameter(),
				nblRequest.get(NblParameter.PAYMENT_INSTRUMENT_NAME.getName()));
		payerDetails.put(PgwParam.ACCOUNT_NO.getParameter(), nblRequest.get(NblParameter.ACCOUNT_ID.getName()));

	}

	/**
	 * Parse the SDP request and create the CasAdapter request.
	 *
	 * @param requestContext
	 * @return
	 */
	protected abstract Map<String, Object> createChargingRequest(Map<String, Map<String, Object>> requestContext)
			throws CaasParameterValidationException;

	/**
	 * Parse the charging response form CasAdapter and create the SDP response
	 * map.
	 *
	 * @param chargingResponse
	 * @param requestContext
	 *            The original request context
	 * @return
	 */
	protected abstract Map<String, Object> createExecutionResult(Map<String, Object> chargingResponse,
			Map<String, Map<String, Object>> requestContext);

	/**
	 * Delegate to the concrete method of the CasAdapter.
	 *
	 * @param casAdapter
	 * @param chargingRequest
	 * @return
	 */
	protected abstract Map<String, Object> call(CasAdapter casAdapter, Map<String, Object> chargingRequest);

	/**
	 * Checks whether the NCS under requeste context is a trusted one.
	 *
	 * @param requestContext
	 * @return
	 */
	protected boolean isTrustedNcs(Map<String, Map<String, Object>> requestContext) {
		Map<String, Object> ncs = requestContext.get(ncsK);
		if (ncs == null)
			return false;
		boolean trustedNcs = Boolean.parseBoolean(String.valueOf(ncs.get(trustedK)));
		return trustedNcs;
	}

	/**
	 * Retruns the MSISDN from the request in the order of
	 * <ol>
	 * <li>unmasked number list</li>
	 * <li>request msisdnK</li>
	 * <li>Nbl Request subscriber ID</li>
	 * </ol>
	 *
	 * @param requestContext
	 * @return
	 */
	protected String getMsisdn(Map<String, Map<String, Object>> requestContext) {

		Map<String, Object> nblRequest = requestContext.get(requestK);

		List<Map<String, String>> recipients = (List<Map<String, String>>) nblRequest.get(recipientsK);
		if (recipients != null && recipients.size() > 0) {
			Map<String, String> recipient = recipients.get(0);
			return recipient.get(recipientAddressK);
		}

		String msisdn = (String) nblRequest.get(msisdnK);
		if (msisdn != null) {
			return msisdn;
		}

		String subscriberId = (String) nblRequest.get(NblParameter.SUBSCRIBER_ID.getName());
		if (subscriberId != null && subscriberId.startsWith(telK) && subscriberId.contains(":")) {
			msisdn = subscriberId.substring(subscriberId.indexOf(":") + 1, subscriberId.length());
		}
		return msisdn;
	}

	/**
	 * Detects if this is a trusted application. If it is trusted then Payment
	 * gateway calls are set so that no authorization necessary
	 *
	 * @param pgwRequest
	 * @param requestContext
	 */
	private void processTrustedNCS(Map<String, Object> pgwRequest, Map<String, Map<String, Object>> requestContext) {
		Map<String, Object> nblRequest = requestContext.get(requestK);
		if (isTrustedNcs(requestContext)) {
			pgwRequest.put(PgwParam.IS_AUTH_REQUIRED.getParameter(), Boolean.FALSE.toString());
			pgwRequest.put(PgwParam.MERCHANT_SHOULD_BE_ALLOWED.getParameter(), Boolean.TRUE.toString());
		} else {
			pgwRequest.put(PgwParam.IS_AUTH_REQUIRED.getParameter(), Boolean.TRUE.toString());
			pgwRequest.put(PgwParam.MERCHANT_SHOULD_BE_ALLOWED.getParameter(), Boolean.FALSE.toString());
		}
	}

    private void validateRequest(Map<String, Map<String, Object>> requestContext) throws SdpException {
        if (KeyNameSpaceResolver.contains(requestContext, requestK, recipientsK)) {
            List<Map<String, String>> recipients = (List<Map<String, String>>) KeyNameSpaceResolver.data(requestContext, requestK, recipientsK);

            Map<String, Object> ncsSla = requestContext.get(ncsK);
            String partialChargingAllowed = (String)KeyNameSpaceResolver.data(ncsSla, chargingK, metaDataK, partialMsisdnAllowedK);

            if(!Boolean.valueOf(partialChargingAllowed)) {
                for (Map<String, String> r : recipients) {
                    if (r.containsKey(recipientAddressStatusK) && !r.get(recipientAddressStatusK).equals(successCode)) {
                        throw new SdpException(r.get(recipientAddressStatusK));
                    }
                }
            }
        }

        if (KeyNameSpaceResolver.contains(requestContext, requestK, externalTransIdNblK)) {
            final String extTrxId = (String) KeyNameSpaceResolver.data(requestContext, requestK, externalTransIdNblK);
            if (externalTrxIdMaxLen > 0 && externalTrxIdMaxLen < extTrxId.length()) {
                throw new SdpException(invalidRequestErrorCode, MessageFormat.format("{0} is too long", externalTransIdNblK));
            }
        }

        validateRequestedPiWithAllowed(requestContext);
    }

    private void validateRequestedPiWithAllowed(Map<String, Map<String, Object>> requestContext) {
        BasicDBList allowedPiList = (BasicDBList) KeyNameSpaceResolver.data(requestContext, ncsK, chargingK, allowedPaymentInstrumentsK);
        String requestPiName = (String)KeyNameSpaceResolver.data(requestContext, requestK, "paymentInstrumentName");
        if(requestPiName != null){
            if(allowedPiList != null){
                for( Object api : allowedPiList.toArray()){
                    if( requestPiName.startsWith((String)api)){
                        return;
                    }
                }
            }
            throw new SdpException(caasPiNotAllowedErrorCode, "App do not accept payments from given Payment Instrument.");
        }
    }


    public void setExternalTrxIdMaxLen(int externalTrxIdMaxLen) {
        this.externalTrxIdMaxLen = externalTrxIdMaxLen;
    }

    public List<String> getAdditionalParametersFromNblRequest() {
        return additionalNblParams;
    }

    public void setAdditionalNblParams(List<String> nblParams) {
        additionalNblParams = nblParams;
    }

    public Map<String, String> getPaymentInsSystemAccountIdMap() {
        return paymentInsSystemAccountIdMap;
    }

    public void setPaymentInsSystemAccountIdMap(Map<String, String> paymentInsSystemAccountIdMap) {
        this.paymentInsSystemAccountIdMap = paymentInsSystemAccountIdMap;
    }
}