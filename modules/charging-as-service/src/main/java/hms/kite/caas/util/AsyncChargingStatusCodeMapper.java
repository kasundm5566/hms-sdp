/**
 *   (C) Copyright 2012-2013 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.caas.util;

import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class AsyncChargingStatusCodeMapper {
    private Map<String, String> mappedStatusCode;
    private Map<String, String> statusCodeDescription;
    
    public String getPgwMappedSatusCode(String pgwStatusCode)  {
        if(mappedStatusCode.get(pgwStatusCode) != null && !(mappedStatusCode.get(pgwStatusCode).isEmpty())) {
            return mappedStatusCode.get(pgwStatusCode);
        } else {
            throw new RuntimeException("No matching Status code found for pgwStatusCode " + pgwStatusCode);
        }
    }

    public String getStatusDetailForStatusCode(String statusCode)   {
        if(!(statusCodeDescription.get(statusCode)).isEmpty())  {
            return statusCodeDescription.get(statusCode);
        }   else {
            throw new RuntimeException("No matching Status Description found for statusCode " + statusCode);
        }
    }

    public Map<String, String> getMappedStatusCode() {
        return mappedStatusCode;
    }

    public void setMappedStatusCode(Map<String, String> mappedStatusCode) {
        this.mappedStatusCode = mappedStatusCode;
    }

    public Map<String, String> getStatusCodeDescription() {
        return statusCodeDescription;
    }

    public void setStatusCodeDescription(Map<String, String> statusCodeDescription) {
        this.statusCodeDescription = statusCodeDescription;
    }
}
