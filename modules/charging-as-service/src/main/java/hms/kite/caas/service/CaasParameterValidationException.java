package hms.kite.caas.service;

import hms.kite.util.SdpException;

/**
 * Exception for Charging as a service Parameter validations
 * $LastChangedDate 9/2/11 10:16 AM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
public class CaasParameterValidationException extends SdpException {

    public CaasParameterValidationException(String errorCode) {
        super(errorCode);
    }

    public CaasParameterValidationException(String errorCode, String errorDescription) {
        super(errorCode, errorDescription);
    }

    public CaasParameterValidationException(String errorCode, Throwable throwable) {
        super(errorCode, throwable);
    }

    public CaasParameterValidationException(String errorCode, String errorDescription, Throwable throwable) {
        super(errorCode, errorDescription, throwable);
    }
}
