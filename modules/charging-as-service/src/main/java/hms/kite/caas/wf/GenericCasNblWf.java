/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.caas.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.control.InExpression;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteErrorBox.appNotAvailableErrorCode;
import static hms.kite.util.KiteKeyBox.*;

/**
 * Charging as a service (Generic) NBL workflow
 *
 * <p/>
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class GenericCasNblWf extends WrappedGeneratedWorkflow implements Workflow {

    /**
     * Name of the SDP channel this would forward the request to.
     */
//    private String sdpChannelName;

    @Autowired private Channel internalHostResolverChannel;
    @Autowired private Channel numberMaskingChannel;
    @Autowired private Channel requestPreConditionChannel;
    @Autowired private Channel provAppSlaChannel;
    private Channel validationChannel;

    protected Channel sdpChannel;

    @Override
    protected Workflow generateWorkflow() {

        ServiceImpl internalHostResolver = new ServiceImpl("internal-host-resolver-channel");
        internalHostResolver.setChannel(internalHostResolverChannel);
        internalHostResolver.setOnSuccess(generateInternalWorkflow());
        internalHostResolver.onDefaultError(generateExternalWorkflow());

        return new WorkflowImpl(internalHostResolver);
    }

    private ServiceImpl generateExternalWorkflow() {
        ServiceImpl entryChannel = new ServiceImpl("request-precondition-channel")
                .attachChannel(requestPreConditionChannel);
        ServiceImpl appSlaChannel = entryChannel
                .chain("validation.channel").attachChannel(validationChannel)
                .chain("prov.app.sla.channel").attachChannel(provAppSlaChannel);

        ServiceImpl numberMaskingCheck = appSlaChannel       //.chain("prov.sp.sla.channel").attachChannel("prov.sp.sla.channel")
                .chain(new Condition(new InExpression(requestK, remotehostK, appK, allowedHostsK),
                        invalidHostIpErrorCode))
                .chain(new Condition(
                        new EqualExpression(requestK, passwordK, appK, passwordK),
                        authenticationFailedErrorCode))
                .chain(new Condition(new InExpression(appK, statusK, new Object[]{limitedProductionK, productionK, activeProductionK}),
                        appNotAvailableErrorCode))
                .chain("masking.eligibility.validation", new Condition(
                        new EqualExpression(appK, maskNumberK, true), systemErrorCode));

        ServiceImpl numberMaskingChannelService = numberMaskingCheck.chain("number-masking-channel")
                .attachChannel(numberMaskingChannel);
        numberMaskingChannelService.addServiceParameter(unmaskK, true);

        ServiceImpl sdpChannelService = numberMaskingChannelService.chain(getName(sdpChannel)).attachChannel(sdpChannel);

        numberMaskingCheck.onDefaultError(sdpChannelService);


        return entryChannel;
    }

    private ServiceImpl generateInternalWorkflow() {
        ServiceImpl entryChannel = new ServiceImpl("request-precondition-channel").attachChannel(requestPreConditionChannel);

        entryChannel.chain("validation.channel").attachChannel(validationChannel)
                    .chain("prov.app.sla.channel").attachChannel(provAppSlaChannel)
                    .chain(new Condition(new InExpression(appK, statusK, new Object[]{limitedProductionK, productionK, activeProductionK}),
                                                            appNotAvailableErrorCode))
                    .chain(getName(sdpChannel)).attachChannel(sdpChannel);

        return entryChannel;
    }

//    /**
//     * Sets the SDP channel name
//     * @param sdpChannelName
//     */
//    public void setSdpChannelName(String sdpChannelName) {
//        this.sdpChannelName = sdpChannelName;
//    }

    public void setSdpChannel(Channel sdpChannel) {
        this.sdpChannel = sdpChannel;
    }

    @Override
    public void executeWorkflow(Map<String, Map<String, Object>> requestContext) {
        super.executeWorkflow(requestContext);
    }

    public void setValidationChannel(Channel validationChannel) {
        this.validationChannel = validationChannel;
    }
}
