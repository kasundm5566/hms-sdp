package hms.kite.caas.wf;

import hms.kite.util.KiteErrorBox;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.control.GreaterThanExpression;
import hms.kite.wfengine.control.LessThanExpression;
import hms.kite.wfengine.impl.ServiceImpl;

import static hms.kite.util.KiteKeyBox.*;

/**
 * Credit Reservation workflow for Charging as a service.
 *
 * $LastChangedDate 7/26/11 7:50 AM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
public class NcsCasCreditReserveWf extends NcsCasDelegateToServiceWf implements Workflow {

    /**
     * Checks reserve-allowed" : true, "reserve-min-amount" : "10.5", "reserve-max-amount" : "100.5"
     * @return
     */
    @Override
    protected ServiceImpl generateWorkflowTail() {
        ServiceImpl ncsValidationService = new ServiceImpl("ncs-validation");

        ncsValidationService.chain("reserve-allowed", new Condition(
                        new EqualExpression(ncsK, reserveAllowedK, true),
                        KiteErrorBox.chargingOperationNotAllowedErrorCode
                )).chain("reserve-min-amount", new Condition(
                        new GreaterThanExpression(requestK, amountK, ncsK, reserveMinAmountK),
                        KiteErrorBox.chargingAmountTooLowErrorCode
                )).chain("reserve-max-amount", new Condition(
                        new LessThanExpression(requestK, amountK, ncsK, reserveMaxAmountK),
                        KiteErrorBox.chargingAmountTooHighErrorCode
                )).chain(super.pgwAdapterServiceChannel.toString())
                .attachChannel(super.pgwAdapterServiceChannel);

        return ncsValidationService;
    }
}
