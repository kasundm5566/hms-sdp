package hms.kite.caas.wf;

import hms.kite.util.KiteErrorBox;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.control.GreaterThanExpression;
import hms.kite.wfengine.control.LessThanExpression;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.transport.Channel;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

/**
 * Credit workflow for Charging as a service.
 *
 * $LastChangedDate 7/26/11 7:50 AM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
public class NcsCasDirectDebitWf extends NcsCasDelegateToServiceWf implements Workflow {

    /**
     * Checks "debit-allowed" : true, "debit-min-amount" : "10.5", "debit-max-amount" : "100.5"
     * @return
     */
    @Override
    protected ServiceImpl generateWorkflowTail() {
        ServiceImpl ncsValidationService = new ServiceImpl("ncs-validation");
        ncsValidationService.chain("debit-allowed", new Condition(
                        new EqualExpression(ncsK, debitAllowedK, true),
                        KiteErrorBox.chargingOperationNotAllowedErrorCode
                )).chain("debit-min-amount", new Condition(
                        new GreaterThanExpression(requestK, amountK, ncsK, debitMinAmount),
                        KiteErrorBox.chargingAmountTooLowErrorCode
                )).chain("debit-max-amount", new Condition(
                        new LessThanExpression(requestK, amountK, ncsK, debitMaxAmount),
                         KiteErrorBox.chargingAmountTooHighErrorCode
                )).chain(getName(super.pgwAdapterServiceChannel))
                .attachChannel(super.pgwAdapterServiceChannel);

        return ncsValidationService;
    }

    @Override
    public void executeWorkflow(Map<String, Map<String, Object>> requestContext) {
        super.executeWorkflow(requestContext);
    }
}
