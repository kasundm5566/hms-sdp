/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.caas.service;

import hms.kite.CasAdapter;
import hms.kite.caas.api.NblParameter;
import hms.kite.util.KiteErrorBox;
import hms.kite.util.PgwParam;
import hms.kite.wfengine.impl.ResponseBuilder;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

/**
 * 
 * Charging as a Service Credit Reserve.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

public class CasAccessRequestService extends AbstractCasPgwAdapterService {

	private final static Logger logger = LoggerFactory.getLogger(CasAccessRequestService.class);

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
               logger.debug("Message received [{}] ", requestContext.get(requestK));
        /**
         * Handle the trusted CAAS NCS. Access request should not propagate to Payment gateway.
         */
        if(isTrustedNcs(requestContext)) {
            return ResponseBuilder.generate(KiteErrorBox.successCode, "CasS NCS is trusted. Access request is not required."
                    , true);
        }

        return super.execute(requestContext);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, Object> createExecutionResult(Map<String, Object> chargingResponse,
                                                        Map<String,Map<String, Object>> requestContext) {
        logger.info("created Execution Results [{}]", chargingResponse);
        Map<String, Object> result = ResponseBuilder.generateSuccess();
        result.put(NblParameter.INTERNAL_TRX_ID.getName(), chargingResponse.get(PgwParam.CLIENT_TRANS_ID_FOR_RESPONSE.getParameter()));
        result.put(NblParameter.SUBSCRIBER_ID.getName(),
                requestContext.get(requestK).get(NblParameter.SUBSCRIBER_ID.getName()));
        //result.put(NblParameter.TIMESTAMP.getName(), chargingResponse.get(PgwParam.REQUESTED_TIMESTAMP.getParameter()));
        result.put(NblParameter.TIMESTAMP.getName(), requestContext.get(requestK).get(NblParameter.TIMESTAMP.getName()));
               result.put(NblParameter.STATUS_CODE.getName(), chargingResponse.get(PgwParam.STATUS_CODE.getParameter()));
        Object statusText = chargingResponse.get(PgwParam.STATUS_TEXT.getParameter());
        if(statusText == null) {
            statusText =  chargingResponse.get(PgwParam.STATUS_CODE.getParameter());
        }
        result.put(NblParameter.STATUS_TEXT.getName(), String.valueOf(statusText));

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String,Object> createChargingRequest(Map<String,Map<String, Object>> requestContext)
            throws CaasParameterValidationException {
        Map<String, Object>  result = new HashMap<String, Object>();
        Map<String, Object>  nblRequest = requestContext.get(requestK);
        Map<String, Object>  casNcs = requestContext.get(ncsK);
        Map<String, Object>  app = requestContext.get(appK);

        result.put(PgwParam.MERCHANT_ID.getParameter(), nblRequest.get(NblParameter.APPLICATION_ID.getName()));
        result.put(PgwParam.MERCHANT_NAME.getParameter(), app.get(nameK));

        //Remember Timestamp
        String timestampString = ISODateTimeFormat.dateTime().print(new DateTime());
        nblRequest.put(NblParameter.TIMESTAMP.getName(), timestampString);

        //Use correlation ID in as the transaction id, since we dont get one from NBL
        result.put(PgwParam.CLIENT_TRANS_ID.getParameter(), nblRequest.get(correlationIdK));
        result.put(PgwParam.CHARGED_CATEGORY.getParameter(), casK);

        result.put(PgwParam.SYSTEM_ID.getParameter(), systemIdSdpK);

        String subscriberId = (String) nblRequest.get(NblParameter.SUBSCRIBER_ID.getName());
        String msisdn = getMsisdn(requestContext);
        if (msisdn != null) {
            result.put(PgwParam.MSISDN.getParameter(), msisdn);
        } else if (subscriberId != null && subscriberId.startsWith(subscriberId_idK)) {
            subscriberId = subscriberId.substring(subscriberId.indexOf(":") + 1, subscriberId.length());
            result.put(PgwParam.USER_ID.getParameter(), subscriberId);
        }else {
            logger.error("Subscriber ID format is not valid for CaaS [{}]", subscriberId);
            throw new CaasParameterValidationException(KiteErrorBox.invalidRequestErrorCode,
                    String.format("Subscriber ID [%s] format is not valid for CaaS [%s]. Expected tel:MSISDN or id:USER_ID",
                           NblParameter.SUBSCRIBER_ID.getName(), subscriberId) );
        }

        return result;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, Object> call(CasAdapter casAdapter, Map<String, Object> chargingRequest) {
        return casAdapter.allowMerchant(chargingRequest);
    }
}
