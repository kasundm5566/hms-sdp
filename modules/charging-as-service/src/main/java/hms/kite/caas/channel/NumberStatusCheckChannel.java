/*
 *   (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.caas.channel;


import hms.kite.caas.api.NblParameter;
import hms.kite.util.KiteErrorBox;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.KiteKeyBox.requestK;
import static hms.kite.util.KiteKeyBox.statusDescriptionK;

public class NumberStatusCheckChannel extends BaseChannel {
    private static final Logger logger = LoggerFactory.getLogger(NumberStatusCheckChannel.class);

    /**
     * Generates the response from the request context.
     *
     * @param requestContext
     * @return
     */
    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        logger.debug("Received Request Context [{}]", requestContext);
        return doGenerate(requestContext);
    }

    private static Map<String, Object> doGenerate(Map<String, Map<String, Object>> requestContext) {
        String recipientAddressStatus = getRecipientAddressStatus(requestContext);
        logger.debug("Received Recipient Address Status [{}]", recipientAddressStatus);
        if (recipientAddressStatus != null) {
            return generate(recipientAddressStatus);
        } else {
            logger.debug("Return Invalid Request  Status [{}]", KiteErrorBox.invalidRequestErrorCode);
            return generate(KiteErrorBox.invalidRequestErrorCode);
        }
    }

    public static Map<String, Object> generate(String code) {
        Map<String, Object> resp = new HashMap<String, Object>();
        resp.put(statusCodeK, code);
        return resp;
    }


    private static String getRecipientAddressStatus(Map<String, Map<String, Object>> context) {
        String recipientAddressStatus = null;
        List<Map<String, String>> recipients = (List<Map<String, String>>) context.get(requestK).get(recipientsK);
        if (recipients != null) {
            Map<String, String> recipient = recipients.get(0);
            if (recipient != null) {
                recipientAddressStatus = recipient.get(recipientAddressStatusK);
            }
        }
        return recipientAddressStatus;
    }



}
