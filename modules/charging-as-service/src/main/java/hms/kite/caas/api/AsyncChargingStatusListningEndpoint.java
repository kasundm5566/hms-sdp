/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.caas.api;

import hms.common.rest.util.Message;
import hms.kite.wfengine.transport.Channel;

import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Listener to the asynchronous charing status messages receiving from payment
 * gateway.
 *
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
@Produces({ "application/xml" })
@Consumes({ "application/xml" })
@Path("/caas/receive")
@Deprecated
public class AsyncChargingStatusListningEndpoint {
	private static final Logger logger = LoggerFactory.getLogger(AsyncChargingStatusListningEndpoint.class);

	private Channel asynchronousChargingResponseChannel;

	@Produces({ "application/x-www-form-urlencoded" })
	@Consumes({ "application/x-www-form-urlencoded" })
	@POST
	@Path("/async-resp")
	public Message onAsynchronosChargingResponse(Message msg) {
		logger.info("Received asynchronous charging response from pgw [" + msg + "]");
		Message resp = new Message();
		resp.putAll((Map) asynchronousChargingResponseChannel.send(msg.getParams()));
		logger.info("Sending response to pgw [{}]", msg.getParams());
		return resp;
	}

	public void setAsynchronousChargingResponseChannel(Channel asynchronousChargingResponseChannel) {
		this.asynchronousChargingResponseChannel = asynchronousChargingResponseChannel;
	}

}
