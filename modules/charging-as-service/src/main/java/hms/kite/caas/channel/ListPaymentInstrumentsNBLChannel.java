package hms.kite.caas.channel;

import hms.kite.caas.api.NblParameter;
import hms.kite.util.KiteKeyBox;

import java.util.Map;

/**
 * NBL channel.
 * Translates the NBL request and response to and from SDP formats.
 *
 * $LastChangedDate 7/26/11 3:40 PM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
public class ListPaymentInstrumentsNBLChannel extends GenericCasNBLChannel {

    @Override
        /**
     * Generates the response from the request context.
     *
     * @param requestContext
     * @return
     */
    protected Map<String, Object> generateResponse(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> response = super.generateResponse(requestContext);
        Map<String, Object> originalResponse = requestContext.get(KiteKeyBox.responseK);
        if (originalResponse != null) {
            Object piList = originalResponse.get(NblParameter.PAYMENT_INSTRUMENT_LIST.getName());
            response.put(NblParameter.PAYMENT_INSTRUMENT_LIST.getName(), piList);
        }
        return response;
    }
}
