/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.caas.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.InExpression;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;

/**
 * Charging as a service Access Request workflow
 *
 * <p/>
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SdpCasAccessRequestWf extends WrappedGeneratedWorkflow implements Workflow {

    private static final Logger LOGGER = LoggerFactory.getLogger(SdpCasAccessRequestWf.class);

    @Autowired private Channel provAppSlaChannel;
    @Autowired private Channel provSpSlaChannel;
    @Autowired private Channel casProcessorService;

    @Override
    protected Workflow generateWorkflow() {

        ServiceImpl appSlaChannel = new ServiceImpl("prov.app.sla.channel").attachChannel(provAppSlaChannel);

        appSlaChannel.chain("prov.sp.sla.channel").attachChannel(provSpSlaChannel)
                .chain(new Condition(new InExpression(requestK, remotehostK, appK, allowedHostsK),
                        invalidHostIpErrorCode))
                .chain("cas-processor-service").attachChannel(casProcessorService);

        return new WorkflowImpl(appSlaChannel);
    }
}
