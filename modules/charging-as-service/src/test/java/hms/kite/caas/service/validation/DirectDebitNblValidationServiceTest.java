package hms.kite.caas.service.validation;


import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.testng.Assert.*;

@Test
public class DirectDebitNblValidationServiceTest {

    @Test
    public void testAmountValidation() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        final Method method = DirectDebitNblValidationService.class.getDeclaredMethod("validateAmount", String.class);
        method.setAccessible(true);
        assertTrue((Boolean) method.invoke(null, "1.00"));
        assertTrue((Boolean) method.invoke(null, "1"));
        assertTrue((Boolean) method.invoke(null, ".01"));
        assertTrue((Boolean) method.invoke(null, "1.01"));
        assertTrue((Boolean) method.invoke(null, "10000"));
        assertTrue((Boolean) method.invoke(null, "10000000000.89"));
        assertTrue((Boolean) method.invoke(null, "1000.1"));

        assertFalse((Boolean) method.invoke(null, "1."));
        assertFalse((Boolean) method.invoke(null, "."));
        assertFalse((Boolean) method.invoke(null, ".111"));
        assertFalse((Boolean) method.invoke(null, ".00"));
        assertFalse((Boolean) method.invoke(null, ".0"));
        assertFalse((Boolean) method.invoke(null, "0.00"));
        assertFalse((Boolean) method.invoke(null, "0"));

    }

}
