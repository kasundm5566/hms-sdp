DELIMITER //
-- ===========================================================================================================

/* This procedure use for execute sql queries */
drop procedure if exists queryExecutor //
create procedure queryExecutor(query varchar(4500))

begin

select query;
set @tmp_sql=query;
prepare statement1 from @tmp_sql;
execute statement1;
deallocate prepare statement1;

end//


-- ================================================================================================
-- Stored Procedure to update the transaction_summary
-- ================================================================================================

DELIMITER //

DROP PROCEDURE IF EXISTS populate_transaction_summary//
CREATE PROCEDURE populate_transaction_summary(IN requ_date DATE)

  BEGIN
    DECLARE no_more_records INT DEFAULT 0;
    DECLARE requ_date_id INT;
    DECLARE summary_date_id INT(10);
    DECLARE summary_sp_id VARCHAR(32);
    DECLARE summary_sp_name VARCHAR(100);
    DECLARE summary_app_id VARCHAR(32);
    DECLARE summary_app_name VARCHAR(100);
    DECLARE summary_operator_id INT(1);
    DECLARE summary_ncs_id INT(1);
    DECLARE summary_direction VARCHAR(5);
    DECLARE summary_event_type VARCHAR(32);
    DECLARE summary_payment_instrument VARCHAR(32);
    DECLARE summary_revenue_share_percentage DOUBLE;
    DECLARE summary_sp_revenue DOUBLE;
    DECLARE summary_operator_revenue DOUBLE;
    DECLARE summary_total_revenue DOUBLE;
    DECLARE summary_traffic INT(11);
    DECLARE summary_charged_msg_count INT(11);
    DECLARE summary_unit_charge_price DOUBLE;
    DECLARE summary_charging_type VARCHAR(32);
    DECLARE cur_summary CURSOR FOR
      SELECT
        trx.date_id,
        trx.sp_id,
        trx.sp_name,
        trx.app_id,
        trx.app_name,
        trx.op_id,
        trx.ncs_id,
        trx.direction,
        trx.event_type,
        trx.payment_instrument,
        trx.revenue_share_percentage,
        trx.sp_revenue,
        trx.operator_revenue,
        trx.total_revenue,
        trx.traffic,
        trx.charged_msg_count,
        trx.unit_price,
        trx.charging_type
      FROM
        (SELECT
           dt.date_id AS date_id,
           sdp.sp_id AS sp_id,
           sdp.sp_name AS sp_name,
           sdp.app_id AS app_id,
           sdp.app_name AS app_name,
           op.id AS op_id,
           ncs.id AS ncs_id,
           sdp.direction AS direction,
           sdp.event_type AS event_type,
           sdp.payment_instrument AS payment_instrument,
           sdp.revenue_share_percentage AS revenue_share_percentage,
           SUM(sdp.sys_cur_sp_profit) AS sp_revenue,
           SUM(sdp.sys_cur_vc_profit) AS operator_revenue,
           SUM(sdp.sys_cur_sp_profit + sdp.sys_cur_vc_profit) AS total_revenue,
           COUNT(*) AS traffic,
           (CASE
            WHEN
              charging_type IS NOT NULL
              AND charging_type NOT IN ('free' , '')
            THEN
              COUNT(*)
            ELSE 0
            END) AS charged_msg_count,
           sdp.charge_amount AS unit_price,
           sdp.charging_type AS charging_type
         FROM
           date_info dt, sdp_transaction sdp, operator op, ncs
         WHERE
           dt.full_date = DATE(sdp.time_stamp)
           AND dt.date_id = requ_date_id
           AND ncs.ncs_name = sdp.ncs
           AND sdp.response_code = 'S1000'
           AND sdp.file_type != 'cms'
         GROUP BY date_id , sp_id , app_id , op.id , ncs_id , direction) AS trx,
        system_app AS sys
      WHERE
        trx.app_id != sys.app_id
        OR trx.ncs_id != sys.ncs_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_records = 1;

    SELECT date_id INTO requ_date_id FROM date_info WHERE full_date = requ_date;

    OPEN cur_summary;

    read_loop: LOOP

      FETCH cur_summary INTO summary_date_id, summary_sp_id, summary_sp_name, summary_app_id, summary_app_name, summary_operator_id, summary_ncs_id,
        summary_direction, summary_event_type, summary_payment_instrument, summary_revenue_share_percentage, summary_sp_revenue, summary_operator_revenue,
        summary_total_revenue, summary_traffic, summary_charged_msg_count, summary_unit_charge_price, summary_charging_type;

      IF no_more_records THEN
        LEAVE read_loop;
      END IF;

      INSERT INTO `sdp_transaction_summary` (date_id, sp_id, sp_name, app_id, app_name, revenue_share_percentage, operator_id, ncs_id, direction, sp_revenue,
                                             operator_revenue, total_revenue, traffic, charged_msg_count, event_type, payment_instrument, unit_charge_price, charging_type)
      VALUES (summary_date_id, summary_sp_id, summary_sp_name, summary_app_id, summary_app_name, summary_revenue_share_percentage, summary_operator_id, summary_ncs_id, summary_direction, summary_sp_revenue,
              summary_operator_revenue, summary_total_revenue, summary_traffic, summary_charged_msg_count, summary_event_type, summary_payment_instrument, summary_unit_charge_price, summary_charging_type);

    END LOOP;

    CLOSE cur_summary;
    CALL populate_transaction_summary_downloadable_revenue(requ_date_id);

  END//

DELIMITER ;
-- ===========================================================================================================


-- ================================================================================================
-- Stored Procedure to update the transaction_summary for downloadable transaction revenue
-- ================================================================================================

DELIMITER //

DROP PROCEDURE IF EXISTS populate_transaction_summary_downloadable_revenue//
CREATE PROCEDURE populate_transaction_summary_downloadable_revenue(IN requ_date DATE)

  BEGIN
    DECLARE no_more_records INT DEFAULT 0;
    DECLARE requ_date_id INT;
    DECLARE summary_date_id INT(10);
    DECLARE summary_sp_id VARCHAR(32);
    DECLARE summary_sp_name VARCHAR(100);
    DECLARE summary_app_id VARCHAR(32);
    DECLARE summary_app_name VARCHAR(100);
    DECLARE summary_operator_id INT(1);
    DECLARE summary_ncs_id INT(1);
    DECLARE summary_direction VARCHAR(5);
    DECLARE summary_event_type VARCHAR(32);
    DECLARE summary_payment_instrument VARCHAR(32);
    DECLARE summary_revenue_share_percentage DOUBLE;
    DECLARE summary_sp_revenue DOUBLE;
    DECLARE summary_operator_revenue DOUBLE;
    DECLARE summary_total_revenue DOUBLE;
    DECLARE summary_traffic INT(11);
    DECLARE summary_charged_msg_count INT(11);
    DECLARE summary_unit_charge_price DOUBLE;
    DECLARE summary_charging_type VARCHAR(32);
    DECLARE cur_summary CURSOR FOR
      SELECT
        trx.date_id,
        trx.sp_id,
        trx.sp_name,
        trx.app_id,
        trx.app_name,
        trx.op_id,
        trx.ncs_id,
        trx.direction,
        trx.event_type,
        trx.payment_instrument,
        trx.revenue_share_percentage,
        trx.sp_revenue,
        trx.operator_revenue,
        trx.total_revenue,
        trx.traffic,
        trx.charged_msg_count,
        trx.unit_price,
        trx.charging_type
      FROM
        (SELECT
           dt.date_id AS date_id,
           sdp.sp_id AS sp_id,
           sdp.sp_name AS sp_name,
           sdp.app_id AS app_id,
           sdp.app_name AS app_name,
           op.id AS op_id,
           ncs.id AS ncs_id,
           sdp.direction AS direction,
           sdp.event_type AS event_type,
           sdp.payment_instrument AS payment_instrument,
           sdp.revenue_share_percentage AS revenue_share_percentage,
           SUM(sdp.sys_cur_sp_profit) AS sp_revenue,
           SUM(sdp.sys_cur_vc_profit) AS operator_revenue,
           SUM(sdp.sys_cur_sp_profit + sdp.sys_cur_vc_profit) AS total_revenue,
           COUNT(*) AS traffic,
           (CASE
            WHEN
              charging_type IS NOT NULL
              AND charging_type NOT IN ('free' , '')
            THEN
              COUNT(*)
            ELSE 0
            END) AS charged_msg_count,
           sdp.charge_amount AS unit_price,
           sdp.charging_type AS charging_type
         FROM
           date_info dt, sdp_transaction sdp, operator op, ncs
         WHERE
           dt.full_date = DATE(sdp.time_stamp)
           AND dt.date_id = requ_date_id
           AND ncs.ncs_name = sdp.ncs
           AND sdp.response_description='eligible'
           AND sdp.ncs='downloadable'
           AND sdp.file_type='cms'
         GROUP BY date_id , sp_id , app_id , op.id , ncs_id , direction) AS trx,
        system_app AS sys
      WHERE
        trx.app_id != sys.app_id
        OR trx.ncs_id != sys.ncs_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_records = 1;

    SELECT date_id INTO requ_date_id FROM date_info WHERE full_date = requ_date;

    OPEN cur_summary;

    read_loop: LOOP

      FETCH cur_summary INTO summary_date_id, summary_sp_id, summary_sp_name, summary_app_id, summary_app_name, summary_operator_id, summary_ncs_id,
        summary_direction, summary_event_type, summary_payment_instrument, summary_revenue_share_percentage, summary_sp_revenue, summary_operator_revenue,
        summary_total_revenue, summary_traffic, summary_charged_msg_count, summary_unit_charge_price, summary_charging_type;

      IF no_more_records THEN
        LEAVE read_loop;
      END IF;

      INSERT INTO `sdp_transaction_summary` (date_id, sp_id, sp_name, app_id, app_name, revenue_share_percentage, operator_id, ncs_id, direction, sp_revenue,
                                             operator_revenue, total_revenue, traffic, charged_msg_count, event_type, payment_instrument, unit_charge_price, charging_type)
      VALUES (summary_date_id, summary_sp_id, summary_sp_name, summary_app_id, summary_app_name, summary_revenue_share_percentage, summary_operator_id, summary_ncs_id, summary_direction, summary_sp_revenue,
              summary_operator_revenue, summary_total_revenue, summary_traffic, summary_charged_msg_count, summary_event_type, summary_payment_instrument, summary_unit_charge_price, summary_charging_type)
      ON DUPLICATE KEY UPDATE sp_revenue=summary_sp_revenue, operator_revenue=summary_operator_revenue, total_revenue=summary_total_revenue;
    END LOOP;

    CLOSE cur_summary;
    CALL populate_transaction_summary_downloadable_traffic(requ_date_id);

  END//

DELIMITER ;
-- ===========================================================================================================

-- ================================================================================================
-- Stored Procedure to update the transaction_summary for downloadable transaction traffic
-- ================================================================================================

DELIMITER //

DROP PROCEDURE IF EXISTS populate_transaction_summary_downloadable_traffic//
CREATE PROCEDURE populate_transaction_summary_downloadable_traffic(IN requ_date DATE)

  BEGIN
    DECLARE no_more_records INT DEFAULT 0;
    DECLARE requ_date_id INT;
    DECLARE summary_date_id INT(10);
    DECLARE summary_sp_id VARCHAR(32);
    DECLARE summary_sp_name VARCHAR(100);
    DECLARE summary_app_id VARCHAR(32);
    DECLARE summary_app_name VARCHAR(100);
    DECLARE summary_operator_id INT(1);
    DECLARE summary_ncs_id INT(1);
    DECLARE summary_direction VARCHAR(5);
    DECLARE summary_event_type VARCHAR(32);
    DECLARE summary_payment_instrument VARCHAR(32);
    DECLARE summary_revenue_share_percentage DOUBLE;
    DECLARE summary_sp_revenue DOUBLE;
    DECLARE summary_operator_revenue DOUBLE;
    DECLARE summary_total_revenue DOUBLE;
    DECLARE summary_traffic INT(11);
    DECLARE summary_charged_msg_count INT(11);
    DECLARE summary_unit_charge_price DOUBLE;
    DECLARE summary_charging_type VARCHAR(32);
    DECLARE cur_summary CURSOR FOR
      SELECT
        trx.date_id,
        trx.sp_id,
        trx.sp_name,
        trx.app_id,
        trx.app_name,
        trx.op_id,
        trx.ncs_id,
        trx.direction,
        trx.event_type,
        trx.payment_instrument,
        trx.revenue_share_percentage,
        trx.sp_revenue,
        trx.operator_revenue,
        trx.total_revenue,
        trx.traffic,
        trx.charged_msg_count,
        trx.unit_price,
        trx.charging_type
      FROM
        (SELECT
           dt.date_id AS date_id,
           sdp.sp_id AS sp_id,
           sdp.sp_name AS sp_name,
           sdp.app_id AS app_id,
           sdp.app_name AS app_name,
           op.id AS op_id,
           ncs.id AS ncs_id,
           sdp.direction AS direction,
           sdp.event_type AS event_type,
           sdp.payment_instrument AS payment_instrument,
           sdp.revenue_share_percentage AS revenue_share_percentage,
           SUM(sdp.sys_cur_sp_profit) AS sp_revenue,
           SUM(sdp.sys_cur_vc_profit) AS operator_revenue,
           SUM(sdp.sys_cur_sp_profit + sdp.sys_cur_vc_profit) AS total_revenue,
           COUNT(*) AS traffic,
           (CASE
            WHEN
              charging_type IS NOT NULL
              AND charging_type NOT IN ('free' , '')
            THEN
              COUNT(*)
            ELSE 0
            END) AS charged_msg_count,
           sdp.charge_amount AS unit_price,
           sdp.charging_type AS charging_type
         FROM
           date_info dt, sdp_transaction sdp, operator op, ncs
         WHERE
           dt.full_date = DATE(sdp.time_stamp)
           AND dt.date_id = requ_date_id
           AND ncs.ncs_name = sdp.ncs
           AND sdp.response_description='download-completed'
           AND sdp.ncs='downloadable'
           AND sdp.file_type='cms'
         GROUP BY date_id , sp_id , app_id , op.id , ncs_id , direction) AS trx,
        system_app AS sys
      WHERE
        trx.app_id != sys.app_id
        OR trx.ncs_id != sys.ncs_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_records = 1;

    SELECT date_id INTO requ_date_id FROM date_info WHERE full_date = requ_date;

    OPEN cur_summary;

    read_loop: LOOP

      FETCH cur_summary INTO summary_date_id, summary_sp_id, summary_sp_name, summary_app_id, summary_app_name, summary_operator_id, summary_ncs_id,
        summary_direction, summary_event_type, summary_payment_instrument, summary_revenue_share_percentage, summary_sp_revenue, summary_operator_revenue,
        summary_total_revenue, summary_traffic, summary_charged_msg_count, summary_unit_charge_price, summary_charging_type;

      IF no_more_records THEN
        LEAVE read_loop;
      END IF;

      INSERT INTO `sdp_transaction_summary` (date_id, sp_id, sp_name, app_id, app_name, revenue_share_percentage, operator_id, ncs_id, direction, sp_revenue,
                                             operator_revenue, total_revenue, traffic, charged_msg_count, event_type, payment_instrument, unit_charge_price, charging_type)
      VALUES (summary_date_id, summary_sp_id, summary_sp_name, summary_app_id, summary_app_name, summary_revenue_share_percentage, summary_operator_id, summary_ncs_id, summary_direction, summary_sp_revenue,
              summary_operator_revenue, summary_total_revenue, summary_traffic, summary_charged_msg_count, summary_event_type, summary_payment_instrument, summary_unit_charge_price, summary_charging_type)
      ON DUPLICATE KEY UPDATE traffic=summary_traffic;
    END LOOP;

    CLOSE cur_summary;

  END//

DELIMITER ;
-- ===========================================================================================================


-- ===================================================
-- stored_procudure for update the sdp_subscription_summary
-- ===================================================
DROP PROCEDURE IF EXISTS populate_subscription_summary//
create procedure populate_subscription_summary(IN requ_date date)


begin

declare done int default 0;

declare requ_date_id int;

declare c_date_id int(10);
declare c_sp_id varchar(32);
declare c_sp_name varchar(100);
declare c_app_id varchar(32);
declare c_app_name varchar(100);
declare c_new_reg int(11);
declare c_dreg int(11);


declare cur1 cursor for select din.date_id,t.sp_id,t.sp_name,t.app_id,t.app_name,
(CASE WHEN (t.event_type='freeRegistration' OR t.event_type='registrationCharging') AND t.response_code='S1000' THEN count(*) ELSE 0 END) as new_reg,
(CASE WHEN t.event_type='unregistration' THEN count(*) ELSE 0 END) as dereg
from sdp_transaction as t,date_info as din
where (t.event_type='freeRegistration' OR t.event_type='registrationCharging' OR t.event_type='unregistration') AND
din.date_id=requ_date_id AND din.full_date = DATE(t.time_stamp) group by t.app_id,din.date_id,t.event_type;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

select date_id into requ_date_id from date_info where full_date = requ_date;

open cur1;

read_loop: LOOP
fetch cur1 into c_date_id,c_sp_id,c_sp_name,c_app_id,c_app_name,c_new_reg,c_dreg;

if done then
	leave read_loop;
end if;
insert into `sdp_subscription_summary` (`date_id`,`sp_id`,`sp_name`,`app_id`,`app_name`,`new_reg`,`dereg`)
values(c_date_id,c_sp_id,c_sp_name,c_app_id,c_app_name,c_new_reg,c_dreg)
ON DUPLICATE key UPDATE new_reg = VALUES(new_reg) + new_reg, dereg = VALUES(dereg) + dereg;

end LOOP;

close cur1;
end //

-- ===================================================
-- Stored Procedure to update the failed summary
-- ===================================================

DROP PROCEDURE IF EXISTS populate_failed_summary//
CREATE PROCEDURE populate_failed_summary(IN requested_date DATE)
  BEGIN

    DECLARE no_more_records INT DEFAULT 0;
    DECLARE summary_date_id INT(10);
    DECLARE summary_sp_id VARCHAR(32);
    DECLARE summary_app_id VARCHAR(32);
    DECLARE summary_error_code VARCHAR(10);
    DECLARE summary_ncs_id INT(1);
    DECLARE summary_direction VARCHAR(5);
    DECLARE summary_error_count INT;

    DECLARE  cur_failed_summary CURSOR FOR
      SELECT
        di.`date_id` dt_id,
        sdp.`sp_id`,
        sdp.`app_id`,
        sdp.`response_code`,
        ncs.`id`,
        sdp.`direction`,
        count(sdp.`time_stamp`) frequency
      FROM
        `sdp_transaction` sdp,
        `date_info` di,
        `ncs` ncs
      WHERE
        di.`full_date` = date(sdp.`time_stamp`)
        AND ncs.`ncs_name` = sdp.`ncs`
        AND sdp.`response_code` <> 'S1000'
        AND sdp.`response_code` <> 'P1500'
        AND sdp.`response_code` <> 'P1501'
        AND date(sdp.`time_stamp`) = requested_date
      GROUP BY dt_id , sdp.`sp_id` , sdp.`app_id` , sdp.`response_code` , sdp.`ncs` , sdp.`direction`;

    DECLARE  CONTINUE HANDLER FOR NOT FOUND
    SET  no_more_records = 1;

    OPEN cur_failed_summary;

    read_loop : LOOP
      FETCH  cur_failed_summary INTO summary_date_id, summary_sp_id, summary_app_id, summary_error_code, summary_ncs_id, summary_direction, summary_error_count;

      IF no_more_records THEN
        LEAVE read_loop;
      END IF;
      INSERT INTO `sdp_failed_summary`
      VALUES (summary_date_id, summary_sp_id, summary_app_id, summary_error_code, summary_ncs_id, summary_direction, summary_error_count );
    END LOOP;

    CLOSE  cur_failed_summary;
  END //

-- ===========================================================================================================

--  -- ===================================================
-- stored_procedure to update the add_summary  table
-- =====================================================
DROP PROCEDURE IF EXISTS populate_add_summary//
  CREATE PROCEDURE populate_add_summary(IN requested_date DATE)
    BEGIN
     DECLARE DONE INT DEFAULT 0;
     DECLARE requested_date_id int(10);
     DECLARE sp_date_id INT(10);
     DECLARE sp_sp_id VARCHAR(32);
     DECLARE sp_app_id VARCHAR(32);
     DECLARE sp_add_name VARCHAR(40);
     DECLARE sp_print_count INT;
     DECLARE cur_add_summary CURSOR FOR
      SELECT date_info.date_id, sdp.sp_id, sdp.app_id, sdp.advertisement_name, COUNT(*)
      FROM date_info, sdp_transaction sdp
      WHERE date_info.full_date = DATE(sdp.time_stamp) and date_info.date_id = requested_date_id
      and sdp.response_code = 'S1000' and sdp.advertisement_name !='' GROUP BY date_id, sp_id, app_id, advertisement_name;

     DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
    SELECT date_id INTO requested_date_id FROM date_info WHERE full_date = requested_date;

    OPEN cur_add_summary;
    read_loop: LOOP

    FETCH cur_add_summary INTO sp_date_id, sp_sp_id, sp_app_id, sp_add_name, sp_print_count;

    IF DONE THEN
	  leave read_loop;
    END IF;

    INSERT INTO `add_summary` VALUES (sp_date_id, sp_sp_id, sp_app_id, sp_add_name, sp_print_count);

    END LOOP;

    CLOSE cur_add_summary;

    END //

-- ===========================================================================================================

--  -- ===================================================
-- stored_procedure to populate the sdp_reconciliation_summary from cms transactions in sdp_transaction table
-- =====================================================

DROP PROCEDURE IF EXISTS populate_sdp_reconciliation_summary//
    CREATE PROCEDURE populate_sdp_reconciliation_summary(IN requested_date DATE)
      BEGIN
      DECLARE DONE INT DEFAULT 0;
       DECLARE requested_date_id INT(10);
       DECLARE sp_date_id INT(10);
       DECLARE sp_sp_id VARCHAR(32);
       DECLARE sp_app_id VARCHAR(32);
       DECLARE sp_msg_rev_sp_profit DOUBLE(10,4);
       DECLARE sp_msg_rev_vc_profit DOUBLE(10,4);
       DECLARE sp_msg_rev_op_cost DOUBLE(10,4);
       DECLARE sp_app_total_rev DOUBLE(10,4);
       DECLARE cur_sdp_reconciliation_summary_1 CURSOR FOR
         SELECT date_info.date_id, sdp.sp_id, sdp.app_id, SUM(sdp.sys_cur_sp_profit), SUM(sdp.sys_cur_vc_profit), SUM(sdp.sys_cur_operator_cost)
         FROM date_info, sdp_transaction sdp
         WHERE sdp.file_type = 'cms' AND sdp.response_description = 'eligible' AND date_info.full_date = DATE(sdp.time_stamp) AND
         date_info.date_id = requested_date_id GROUP BY date_id, sp_id, app_id;


       DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

        SELECT date_id INTO requested_date_id FROM date_info WHERE full_date = requested_date;

        OPEN cur_sdp_reconciliation_summary_1;
          read_loop: LOOP

              FETCH cur_sdp_reconciliation_summary_1 INTO sp_date_id, sp_sp_id, sp_app_id, sp_msg_rev_sp_profit, sp_msg_rev_vc_profit, sp_msg_rev_op_cost;

              IF DONE THEN
                leave read_loop;
              END IF;

              INSERT INTO `sdp_reconciliation_summary` (date_id,sp_id,app_id,msg_rev_sp_profit,msg_rev_vc_profit,msg_rev_op_cost,app_total_rev)
              VALUES (sp_date_id, sp_sp_id, sp_app_id, sp_msg_rev_sp_profit, sp_msg_rev_vc_profit, sp_msg_rev_op_cost, sp_msg_rev_sp_profit + sp_msg_rev_vc_profit + sp_msg_rev_op_cost);


          END LOOP;


        CLOSE cur_sdp_reconciliation_summary_1;
        CALL update_sdp_reconciliation_summary(requested_date_id);

        END //



--  -- ===================================================
-- stored_procedure to populate the sdp_reconciliation_summary from sdp transactions in sdp_transaction table
-- =====================================================
DROP PROCEDURE IF EXISTS update_sdp_reconciliation_summary//
    CREATE PROCEDURE update_sdp_reconciliation_summary(IN requested_date_id INT(10))
      BEGIN
      DECLARE DONE INT DEFAULT 0;
       DECLARE sp_date_id INT(10);
       DECLARE sp_sp_id VARCHAR(32);
       DECLARE sp_app_id VARCHAR(32);
       DECLARE sp_msg_rev_sp_profit DOUBLE(10,4);
       DECLARE sp_msg_rev_vc_profit DOUBLE(10,4);
       DECLARE sp_msg_rev_op_cost DOUBLE(10,4);
       DECLARE sp_app_total_rev DOUBLE(10,4);
       DECLARE cur_sdp_reconciliation_summary_1 CURSOR FOR
         SELECT date_info.date_id, sdp.sp_id, sdp.app_id, SUM(sdp.sys_cur_sp_profit), SUM(sdp.sys_cur_vc_profit), SUM(sdp.sys_cur_operator_cost)
         FROM date_info, sdp_transaction sdp
         WHERE sdp.file_type = 'sdp' AND sdp.response_code = 'S1000' AND sdp.payment_instrument NOT IN
         (SELECT payment_instrument FROM ignore_payment_instrument) AND sdp.app_id NOT IN (SELECT app_id FROM system_app)
          AND date_info.full_date = DATE(sdp.time_stamp) AND
         date_info.date_id = requested_date_id GROUP BY date_id, sp_id, app_id;


       DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

        OPEN cur_sdp_reconciliation_summary_1;
          read_loop: LOOP

              FETCH cur_sdp_reconciliation_summary_1 INTO sp_date_id, sp_sp_id, sp_app_id, sp_msg_rev_sp_profit, sp_msg_rev_vc_profit, sp_msg_rev_op_cost;

              IF DONE THEN
                leave read_loop;
              END IF;

              INSERT INTO `sdp_reconciliation_summary` (date_id,sp_id,app_id,msg_rev_sp_profit,msg_rev_vc_profit,msg_rev_op_cost,app_total_rev)
              VALUES (sp_date_id, sp_sp_id, sp_app_id, sp_msg_rev_sp_profit, sp_msg_rev_vc_profit, sp_msg_rev_op_cost,
              sp_msg_rev_sp_profit + sp_msg_rev_vc_profit + sp_msg_rev_op_cost)
              ON DUPLICATE KEY UPDATE msg_rev_sp_profit = VALUES(msg_rev_sp_profit) + sp_msg_rev_sp_profit ,
              msg_rev_vc_profit = VALUES (msg_rev_vc_profit) + sp_msg_rev_vc_profit,
              msg_rev_op_cost = VALUES (msg_rev_op_cost) + sp_msg_rev_op_cost,
              app_total_rev = VALUES (app_total_rev) + (sp_msg_rev_sp_profit + sp_msg_rev_vc_profit + sp_msg_rev_op_cost);




          END LOOP;


        CLOSE cur_sdp_reconciliation_summary_1;

        END //
-- =====================================================================================================================



--  -- ===================================================
-- stored_procedure to update the  'pgw_transaction_summary
--  -- ===================================================

DROP PROCEDURE IF EXISTS populate_pgw_transaction_summary//
CREATE PROCEDURE `populate_pgw_transaction_summary`(IN requ_date date)
BEGIN
       DECLARE no_more_records INT DEFAULT 0;
       DECLARE state VARCHAR(50);
       DECLARE summary_date_id INT(10);
       DECLARE summary_payment_instrument_type_id VARCHAR(32);
       DECLARE summary_tax_type VARCHAR(32);
       DECLARE summary_request_amount DOUBLE ;
       DECLARE summary_paid_amount DOUBLE;
       DECLARE summary_total_transactions INT;
       DECLARE summary_tax_amount FLOAT;
       DECLARE summary_service_charge_amount FLOAT;
       DECLARE cur_summary CURSOR FOR
       SELECT COUNT(*) AS total_transactions,t2.`date_id` AS dt_id,
            t1.`payment_instrument`,t1.`tx_type`,
            SUM(t1.`request_amount`),
            SUM(t1.`paid_amount`),
            SUM(t1.`tax_amount`),
            SUM(t1.`service_charge_amount`)
       FROM pgw_transaction t1, date_info t2
       WHERE date(t1.`timeStamp`)=t2.`full_date`
       AND date(t1.`timeStamp`) = `requ_date`
       AND (t1.`tx_status` = "SUCCESS"
       OR t1.`tx_status` = "AMOUNT_FULLY_PAID"
       OR t1.`tx_status` = "PARTIAL_PAYMENT_SUCCESS"
       OR t1.`tx_status` = "RESERVE_SUCCESS"
       OR t1.`tx_status` = "RESERVE_COMMIT_SUCCESS"
       OR t1.`tx_status` = "PENDING"
       OR t1.`tx_status` = "PARTIAL_PENDING")
       GROUP BY dt_id, t1.`payment_instrument`,t1.`tx_type`;
       DECLARE CONTINUE HANDLER FOR NOT FOUND
       SET no_more_records = 1;
       OPEN cur_summary;
       read_loop : LOOP
       FETCH cur_summary INTO summary_total_transactions,summary_date_id,
        summary_payment_instrument_type_id,
        summary_tax_type,summary_request_amount, summary_paid_amount,summary_tax_amount,
        summary_service_charge_amount;
       IF no_more_records THEN
          LEAVE read_loop;
       END IF;
       INSERT INTO pgw_transaction_summary
       VALUES (summary_date_id,
        summary_request_amount,
        summary_total_transactions,
        summary_paid_amount-summary_tax_amount-summary_service_charge_amount,
        summary_paid_amount,summary_tax_type,
        summary_payment_instrument_type_id,
        summary_service_charge_amount);
       END LOOP;
       CLOSE cur_summary;
END//

DELIMITER ;
