insert into ncs values (1, 'sms');
insert into ncs values (2, 'ussd');
insert into ncs values (3, 'wap-push');
insert into ncs values (4, 'cas');
insert into ncs values (5, 'lbs');
insert into ncs values (6, 'subscription');

insert into operator values (1, 'etisalat');
-- insert into operator values (2, 'orange');
-- insert into operator values (3, 'airtel');

INSERT INTO `summary_data_status` VALUES (1,'populate_transaction_summary','2011-07-31','2011-07-31 10:00:00',NULL,1),
(2,'populate_transaction_summary','2011-07-31',NULL,'2011-07-31 10:00:00',2),
(3,'populate_failed_summary','2011-07-31','2011-07-31 10:00:00',NULL,1),
(4,'populate_failed_summary','2011-07-31',NULL,'2011-07-31 10:00:00',2),
(5,'populate_add_summary','2011-07-31','2011-07-31 10:00:00',NULL,1),
(6,'populate_add_summary','2011-07-31',NULL,'2011-07-31 10:00:00',2),
(7,'populate_sdp_reconciliation_summary','2011-07-31','2011-07-31 10:00:00',NULL,1),
(8,'populate_sdp_reconciliation_summary','2011-07-31',NULL,'2011-07-31 10:00:00',2),
(11,'populate_pgw_transaction_summary','2011-07-31','2011-07-31 10:00:00',NULL,1),
(12,'populate_pgw_transaction_summary','2011-07-31',NULL,'2011-07-31 10:00:00',2),
(13,'populate_subscription_summary','2011-07-31','2011-07-31 10:00:00',NULL,1),
(14,'populate_subscription_summary','2011-07-31',NULL,'2011-07-31 10:00:00',2);


INSERT INTO `payment_instruments` VALUES (1,'Mobile account');

INSERT INTO `system_app` VALUES ('app_id',0);




