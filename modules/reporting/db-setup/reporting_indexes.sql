-- Create indexes
create unique index ix_operator_name on operator (operator_name);
create unique index ix_ncs_name on ncs (ncs_name);
create index ix_summary_data_status on summary_data_status (procedure_name, status, requested_date);
create index ix_populate_sdp_trans_summary on sdp_transaction (time_stamp, operator, ncs, response_code, sp_id, app_id, direction);
create index ix_populate_add_summary on sdp_transaction (time_stamp, response_code, advertisement_name, sp_id, app_id);
create index ix_populate_pgw_income_summary on pgw_transaction (timeStamp);

create index ix_sdp_failed_summary_report on sdp_failed_summary (sp_id, app_id, ncs_id, date_id);
create index ix_add_summary on add_summary (app_id);
alter table sdp_failed_summary ADD PRIMARY KEY (date_id, sp_id, app_id, error_code, ncs_id, direction);
alter table add_summary ADD PRIMARY KEY (date_id, sp_id, app_id, add_name);
alter table pgw_transaction_summary DROP PRIMARY KEY, ADD PRIMARY KEY (date_id, payment_instrument);
alter table sdp_reconciliation_summary ADD PRIMARY KEY (date_id, sp_id, app_id);
create index ix_add_summary_app_wise_add on add_summary (date_id, app_id);
create index ix_add_summary_add_wise on add_summary (date_id, add_name);

create index ix_sdp_trans_summary_date_app on sdp_transaction_summary (date_id, app_id);
create index ix_sdp_trans_summary_date_op_sp on sdp_transaction_summary (date_id, operator_id, sp_id);
create index ix_sdp_trans_summary_sp_date on sdp_transaction_summary (sp_id, date_id);
create index ix_sdp_trans_summary_op_date on sdp_transaction_summary (operator_id, date_id);

create index ix_sdp_cc_report_new on sdp_transaction(sp_id, app_id,time_stamp);
create index ix_sdp_cc_report_source_address on sdp_transaction(source_address,time_stamp);
create index ix_sdp_cc_report_destination_address on sdp_transaction(destination_address, time_stamp);
