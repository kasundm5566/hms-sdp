ALTER TABLE `pgw_transaction` MODIFY `client_tx_id` varchar(50) DEFAULT NULL;

DELIMITER //

--  -- ===================================================
-- stored_procedure to populate the sdp_reconciliation_summary
-- =====================================================
    DROP PROCEDURE IF EXISTS populate_sdp_reconciliation_summary//
    CREATE PROCEDURE populate_sdp_reconciliation_summary(IN requested_date DATE)
      BEGIN
      DECLARE DONE INT DEFAULT 0;
       DECLARE requested_date_id INT(10);
       DECLARE sp_date_id INT(10);
       DECLARE sp_sp_id VARCHAR(32);
       DECLARE sp_app_id VARCHAR(32);
       DECLARE sp_msg_rev_sp_profit DOUBLE(10,4);
       DECLARE sp_msg_rev_vc_profit DOUBLE(10,4);
       DECLARE sp_msg_rev_op_cost DOUBLE(10,4);
       DECLARE sp_app_total_rev DOUBLE(10,4);
       DECLARE cur_sdp_reconciliation_summary_1 CURSOR FOR
         SELECT date_info.date_id, sdp.sp_id, sdp.app_id, SUM(sdp.sys_cur_sp_profit), SUM(sdp.sys_cur_vc_profit), SUM(sdp.sys_cur_operator_cost)
         FROM date_info, sdp_transaction sdp
         WHERE date_info.full_date = DATE(sdp.time_stamp) AND date_info.date_id = requested_date_id
         AND sdp.response_code = 'S1000'  GROUP BY date_id, sp_id, app_id;


       DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

        SELECT date_id INTO requested_date_id FROM date_info WHERE full_date = requested_date;

        OPEN cur_sdp_reconciliation_summary_1;
          read_loop: LOOP

              FETCH cur_sdp_reconciliation_summary_1 INTO sp_date_id, sp_sp_id, sp_app_id, sp_msg_rev_sp_profit, sp_msg_rev_vc_profit, sp_msg_rev_op_cost;

              IF DONE THEN
                leave read_loop;
              END IF;

              INSERT INTO `sdp_reconciliation_summary` (date_id,sp_id,app_id,msg_rev_sp_profit,msg_rev_vc_profit,msg_rev_op_cost,app_total_rev)
              VALUES (sp_date_id, sp_sp_id, sp_app_id, sp_msg_rev_sp_profit, sp_msg_rev_vc_profit, sp_msg_rev_op_cost, sp_msg_rev_sp_profit + sp_msg_rev_vc_profit + sp_msg_rev_op_cost);


          END LOOP;


        CLOSE cur_sdp_reconciliation_summary_1;

        CALL update_sdp_reconciliation_summary(requested_date_id);

        END //

-- ===========================================================================================================

--  -- ===================================================
-- stored_procedure to update the  'sdp_reconciliation_summary.cas_rev_sp_profit' column
--  -- ===================================================
   DROP PROCEDURE IF EXISTS update_sdp_reconciliation_summary//
    CREATE PROCEDURE update_sdp_reconciliation_summary(IN requested_date_id INT(10))
      BEGIN
      DECLARE DONE INT DEFAULT 0;

      DECLARE ckh_cnt INT;

       DECLARE sp_date_id2 INT(10);
       DECLARE sp_sp_id2 VARCHAR(32);
       DECLARE sp_app_id2 VARCHAR(32);
       DECLARE sp_cas_rev_sp_profit DOUBLE(10,4);

        DECLARE cur_sdp_reconciliation_summary_2 CURSOR FOR
         SELECT date_info.date_id, sdp.sp_id, sdp.app_id, SUM(sdp.sys_cur_charge_amount)
         FROM sdp_transaction sdp, date_info
         WHERE date_info.full_date = DATE(sdp.time_stamp) AND date_info.date_id = requested_date_id
         AND sdp.response_code = 'S1000' GROUP BY date_id, sp_id, app_id;

          DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

          OPEN cur_sdp_reconciliation_summary_2;
          read_loop: LOOP
              FETCH cur_sdp_reconciliation_summary_2 INTO sp_date_id2, sp_sp_id2, sp_app_id2, sp_cas_rev_sp_profit;

              IF DONE THEN
                leave read_loop;
              END IF;


              IF (SELECT COUNT(*) FROM `sdp_reconciliation_summary` WHERE date_id=sp_date_id2 AND sp_id=sp_sp_id2 AND app_id=sp_app_id2)=0 THEN
                INSERT INTO `sdp_reconciliation_summary` (date_id, sp_id, app_id, cas_rev_sp_profit, app_total_rev)
                VALUES (sp_date_id2,sp_sp_id2, sp_app_id2,sp_cas_rev_sp_profit,sp_cas_rev_sp_profit);
              ELSE
                UPDATE `sdp_reconciliation_summary` SET cas_rev_sp_profit = sp_cas_rev_sp_profit , app_total_rev=app_total_rev + sp_cas_rev_sp_profit
                WHERE date_id=sp_date_id2 AND sp_id=sp_sp_id2 AND app_id=sp_app_id2;
              END IF;
          END LOOP;
        CLOSE cur_sdp_reconciliation_summary_2;
        END //

-- ==========================================================================================================

DELIMITER ;


-- initial data for ignore_payment_instrument table.
INSERT INTO  ignore_payment_instrument VALUES (1,'Dialog-Ezcash');



-- Change the stored procedure to take only direct debit transactions since others have no revenue

DELIMITER //

DROP PROCEDURE IF EXISTS populate_pgw_transaction_summary//
CREATE PROCEDURE `populate_pgw_transaction_summary`(IN requ_date date)
BEGIN
       DECLARE no_more_records INT DEFAULT 0;
       DECLARE state VARCHAR(50);
       DECLARE summary_date_id INT(10);
       DECLARE summary_payment_instrument_type_id VARCHAR(32);
       DECLARE summary_tax_type VARCHAR(32);
       DECLARE summary_request_amount DOUBLE ;
       DECLARE summary_paid_amount DOUBLE;
       DECLARE summary_total_transactions INT;
       DECLARE summary_tax_amount FLOAT;
       DECLARE summary_service_charge_amount FLOAT;
       DECLARE cur_summary CURSOR FOR
       SELECT COUNT(*) AS total_transactions,t2.`date_id` AS dt_id,
            t1.`payment_instrument`,t1.`tx_type`,
            SUM(t1.`request_amount`),
            SUM(t1.`paid_amount`),
            SUM(t1.`tax_amount`),
            SUM(t1.`service_charge_amount`)
       FROM pgw_transaction t1, date_info t2
       WHERE date(t1.`timeStamp`)=t2.`full_date`
       AND date(t1.`timeStamp`) = `requ_date`
       AND t1.`tx_type` = "DIRECT_DEBIT"
       AND (t1.`tx_status` = "SUCCESS"
       OR t1.`tx_status` = "AMOUNT_FULLY_PAID"
       OR t1.`tx_status` = "PARTIAL_PAYMENT_SUCCESS"
       OR t1.`tx_status` = "RESERVE_SUCCESS"
       OR t1.`tx_status` = "RESERVE_COMMIT_SUCCESS"
       OR t1.`tx_status` = "PENDING"
       OR t1.`tx_status` = "PARTIAL_PENDING")
       GROUP BY dt_id, t1.`payment_instrument`,t1.`tx_type`;
       DECLARE CONTINUE HANDLER FOR NOT FOUND
       SET no_more_records = 1;
       OPEN cur_summary;
       read_loop : LOOP
       FETCH cur_summary INTO summary_total_transactions,summary_date_id,
        summary_payment_instrument_type_id,
        summary_tax_type,summary_request_amount, summary_paid_amount,summary_tax_amount,
        summary_service_charge_amount;
       IF no_more_records THEN
          LEAVE read_loop;
       END IF;
       INSERT INTO pgw_transaction_summary
       VALUES (summary_date_id,
        summary_request_amount,
        summary_total_transactions,
        summary_paid_amount-summary_tax_amount-summary_service_charge_amount,
        summary_paid_amount,summary_tax_type,
        summary_payment_instrument_type_id,
        summary_service_charge_amount);
       END LOOP;
       CLOSE cur_summary;
END//

DELIMITER ;

-- Add new column paybill no to pgw_transaction table
ALTER TABLE `pgw_transaction` ADD `paybill_no` VARCHAR(10);

-- Add new table to keep payment instrument related records
DROP TABLE IF EXISTS `sp_payment_instrument_info`;

CREATE TABLE IF NOT EXISTS `sp_payment_instrument_info` (
`sp_id` VARCHAR(32) DEFAULT NULL,
`app_id` VARCHAR(32) DEFAULT NULL,
`ncs` VARCHAR(20) DEFAULT NULL,
`payment_instrument` VARCHAR(20) DEFAULT NULL,
`pi_additional_info` VARCHAR(20) DEFAULT NULL
) engine=innodb;
