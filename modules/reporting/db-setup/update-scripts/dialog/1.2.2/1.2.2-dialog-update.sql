DELIMITER //

--  -- ===================================================
-- stored_procedure to populate the sdp_reconciliation_summary from cms transactions in sdp_transaction table
-- =====================================================
-- when calculate reconciliation revenue for downloadable cms apps consider if charging successfull(not download successfull)
DROP PROCEDURE IF EXISTS populate_sdp_reconciliation_summary//
    CREATE PROCEDURE populate_sdp_reconciliation_summary(IN requested_date DATE)
      BEGIN
      DECLARE DONE INT DEFAULT 0;
       DECLARE requested_date_id INT(10);
       DECLARE sp_date_id INT(10);
       DECLARE sp_sp_id VARCHAR(32);
       DECLARE sp_app_id VARCHAR(32);
       DECLARE sp_msg_rev_sp_profit DOUBLE(10,4);
       DECLARE sp_msg_rev_vc_profit DOUBLE(10,4);
       DECLARE sp_msg_rev_op_cost DOUBLE(10,4);
       DECLARE sp_app_total_rev DOUBLE(10,4);
       DECLARE cur_sdp_reconciliation_summary_1 CURSOR FOR
         SELECT date_info.date_id, sdp.sp_id, sdp.app_id, SUM(sdp.sys_cur_sp_profit), SUM(sdp.sys_cur_vc_profit), SUM(sdp.sys_cur_operator_cost)
         FROM date_info, sdp_transaction sdp
         WHERE sdp.file_type = 'cms' AND sdp.response_description = 'eligible' AND date_info.full_date = DATE(sdp.time_stamp) AND
         date_info.date_id = requested_date_id GROUP BY date_id, sp_id, app_id;


       DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

        SELECT date_id INTO requested_date_id FROM date_info WHERE full_date = requested_date;

        OPEN cur_sdp_reconciliation_summary_1;
          read_loop: LOOP

              FETCH cur_sdp_reconciliation_summary_1 INTO sp_date_id, sp_sp_id, sp_app_id, sp_msg_rev_sp_profit, sp_msg_rev_vc_profit, sp_msg_rev_op_cost;

              IF DONE THEN
                leave read_loop;
              END IF;

              INSERT INTO `sdp_reconciliation_summary` (date_id,sp_id,app_id,msg_rev_sp_profit,msg_rev_vc_profit,msg_rev_op_cost,app_total_rev)
              VALUES (sp_date_id, sp_sp_id, sp_app_id, sp_msg_rev_sp_profit, sp_msg_rev_vc_profit, sp_msg_rev_op_cost, sp_msg_rev_sp_profit + sp_msg_rev_vc_profit + sp_msg_rev_op_cost);


          END LOOP;


        CLOSE cur_sdp_reconciliation_summary_1;
        CALL update_sdp_reconciliation_summary(requested_date_id);

        END //


--  -- ===================================================
-- stored_procedure to populate the sdp_reconciliation_summary from sdp transactions in sdp_transaction table
-- =====================================================
DROP PROCEDURE IF EXISTS update_sdp_reconciliation_summary//
    CREATE PROCEDURE update_sdp_reconciliation_summary(IN requested_date_id INT(10))
      BEGIN
      DECLARE DONE INT DEFAULT 0;
       DECLARE sp_date_id INT(10);
       DECLARE sp_sp_id VARCHAR(32);
       DECLARE sp_app_id VARCHAR(32);
       DECLARE sp_msg_rev_sp_profit DOUBLE(10,4);
       DECLARE sp_msg_rev_vc_profit DOUBLE(10,4);
       DECLARE sp_msg_rev_op_cost DOUBLE(10,4);
       DECLARE sp_app_total_rev DOUBLE(10,4);
       DECLARE cur_sdp_reconciliation_summary_1 CURSOR FOR
         SELECT date_info.date_id, sdp.sp_id, sdp.app_id, SUM(sdp.sys_cur_sp_profit), SUM(sdp.sys_cur_vc_profit), SUM(sdp.sys_cur_operator_cost)
         FROM date_info, sdp_transaction sdp
         WHERE sdp.file_type = 'sdp' AND sdp.response_code = 'S1000' AND sdp.payment_instrument NOT IN
         (SELECT payment_instrument FROM ignore_payment_instrument) AND sdp.app_id NOT IN (SELECT app_id FROM system_app)
          AND date_info.full_date = DATE(sdp.time_stamp) AND
         date_info.date_id = requested_date_id GROUP BY date_id, sp_id, app_id;


       DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

        OPEN cur_sdp_reconciliation_summary_1;
          read_loop: LOOP

              FETCH cur_sdp_reconciliation_summary_1 INTO sp_date_id, sp_sp_id, sp_app_id, sp_msg_rev_sp_profit, sp_msg_rev_vc_profit, sp_msg_rev_op_cost;

              IF DONE THEN
                leave read_loop;
              END IF;

              INSERT INTO `sdp_reconciliation_summary` (date_id,sp_id,app_id,msg_rev_sp_profit,msg_rev_vc_profit,msg_rev_op_cost,app_total_rev)
              VALUES (sp_date_id, sp_sp_id, sp_app_id, sp_msg_rev_sp_profit, sp_msg_rev_vc_profit, sp_msg_rev_op_cost,
              sp_msg_rev_sp_profit + sp_msg_rev_vc_profit + sp_msg_rev_op_cost)
              ON DUPLICATE KEY UPDATE msg_rev_sp_profit = VALUES(msg_rev_sp_profit) + sp_msg_rev_sp_profit ,
              msg_rev_vc_profit = VALUES (msg_rev_vc_profit) + sp_msg_rev_vc_profit,
              msg_rev_op_cost = VALUES (msg_rev_op_cost) + sp_msg_rev_op_cost,
              app_total_rev = VALUES (app_total_rev) + (sp_msg_rev_sp_profit + sp_msg_rev_vc_profit + sp_msg_rev_op_cost);




          END LOOP;


        CLOSE cur_sdp_reconciliation_summary_1;

        END //

DELIMITER ;

-- index for populate_sdp_reconciliation_summary procedure
CREATE index ix_cms_reconciliation_summary ON sdp_transaction(file_type, response_code);


insert into ncs values (7, 'downloadable');

DELIMITER //

-- ===================================================
-- stored_procudure for update the transaction_summary
-- ===================================================

-- cms translog data summarization move into seperate two procedures.
-- populate_transaction_summary_downloadable_revenue & populate_transaction_summary_downloadable_traffic
-- populate_transaction_summary_downloadable_revenue procedure calculate the revenue for successful downloadable charging
-- populate_transaction_summary_downloadable_traffic procedure calculate the traffic for successful downloads.

DROP PROCEDURE IF EXISTS populate_transaction_summary//
CREATE PROCEDURE populate_transaction_summary(IN requ_date date)

begin

declare done int default 0;

declare requ_date_id int;

declare c_date_id int(10);
declare c_sp_id varchar(32);
declare c_sp_name varchar(100);
declare c_app_id varchar(32);
declare c_app_name varchar(100);
declare c_operator_id int(1);
declare c_ncs_id int(1);
declare c_direction varchar(32);
declare c_revenue_share_percentage double;
declare c_sp_revenue double;
declare c_operator_revenue double;
declare c_total_revenue double;
declare c_traffic int(11);
declare c_charged_msg_count int(11);

declare cur1 cursor for select trx.date_id, trx.sp_id, trx.sp_name, trx.app_id, trx.app_name, trx.op_id, trx.ncs_id,
    trx.direction, trx.revenue_share_percentage,
    trx.sp_revenue,trx.operator_revenue,
    trx.total_revenue,trx.Traffic,
    trx.charged_msg_count from (select din.date_id as date_id, t.sp_id as sp_id, t.sp_name as sp_name, t.app_id as app_id,
     t.app_name as app_name, op.id as op_id, ncs.id as ncs_id, t.direction as direction, t.revenue_share_percentage as revenue_share_percentage,
    sum(t.sys_cur_sp_profit) as sp_revenue,sum(t.sys_cur_vc_profit) as operator_revenue,
    sum(t.sys_cur_sp_profit + t.sys_cur_vc_profit) as total_revenue,count(*) as Traffic,
    (CASE WHEN charging_type IS NOT NULL AND charging_type NOT IN('free','') THEN count(*) ELSE 0 END) as charged_msg_count
from date_info din, sdp_transaction t, operator op,ncs
where din.full_date = DATE(t.time_stamp) and din.date_id = requ_date_id and ncs.ncs_name = t.ncs and t.response_code='S1000' and t.file_type != 'cms'
group by date_id, sp_id, app_id, op.id, ncs_id, direction) as trx, system_app as s where trx.app_id != s.app_id OR trx.ncs_id != s.ncs_id;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

select date_id into requ_date_id from date_info where full_date = requ_date;

open cur1;

read_loop: LOOP
fetch cur1 into c_date_id,c_sp_id,c_sp_name,c_app_id,c_app_name,c_operator_id,c_ncs_id,c_direction,c_revenue_share_percentage,c_sp_revenue,
c_operator_revenue,c_total_revenue,c_traffic,c_charged_msg_count;

if done then
	leave read_loop;
end if;
insert into `sdp_transaction_summary` (date_id,sp_id,sp_name,app_id,app_name,revenue_share_percentage,operator_id,ncs_id,direction,sp_revenue,operator_revenue,total_revenue,traffic, charged_msg_count)
values(c_date_id,c_sp_id,c_sp_name,c_app_id,c_app_name,c_revenue_share_percentage,c_operator_id,c_ncs_id,c_direction,c_sp_revenue,c_operator_revenue,c_total_revenue,c_traffic,c_charged_msg_count);

end LOOP;

close cur1;
call populate_transaction_summary_downloadable_revenue(requ_date_id);
end//

-- ===========================================================================================================


-- ========================================================================================
-- stored_procudure for update the transaction_summary for donwloadable transaction revenue
-- ========================================================================================
DROP PROCEDURE IF EXISTS populate_transaction_summary_downloadable_revenue//
CREATE PROCEDURE populate_transaction_summary_downloadable_revenue(IN requ_date INT(10))

begin

declare done int default 0;

declare c_date_id int(10);
declare c_sp_id varchar(32);
declare c_sp_name varchar(100);
declare c_app_id varchar(32);
declare c_app_name varchar(100);
declare c_operator_id int(1);
declare c_ncs_id int(1);
declare c_direction varchar(32);
declare c_revenue_share_percentage double;
declare c_sp_revenue double;
declare c_operator_revenue double;
declare c_total_revenue double;
declare c_traffic int(11);
declare c_charged_msg_count int(11);

declare cur1 cursor for select trx.date_id, trx.sp_id, trx.sp_name, trx.app_id, trx.app_name, trx.op_id, trx.ncs_id,
    trx.direction, trx.revenue_share_percentage,
    trx.sp_revenue,trx.operator_revenue,
    trx.total_revenue,trx.Traffic,
    trx.charged_msg_count from (select din.date_id as date_id, t.sp_id as sp_id, t.sp_name as sp_name, t.app_id as app_id,
     t.app_name as app_name, op.id as op_id, ncs.id as ncs_id, t.direction as direction, t.revenue_share_percentage as revenue_share_percentage,
    sum(t.sys_cur_sp_profit) as sp_revenue,sum(t.sys_cur_vc_profit) as operator_revenue,
    sum(t.sys_cur_sp_profit + t.sys_cur_vc_profit) as total_revenue,count(*) as Traffic,
    (CASE WHEN charging_type IS NOT NULL AND charging_type NOT IN('free','') THEN count(*) ELSE 0 END) as charged_msg_count
from date_info din, sdp_transaction t, operator op,ncs
where din.full_date = DATE(t.time_stamp) and din.date_id = requ_date and ncs.ncs_name = t.ncs and t.response_description='eligible' and t.ncs='downloadable' and t.file_type='cms'
group by date_id, sp_id, app_id, op.id, ncs_id, direction) as trx, system_app as s where trx.app_id != s.app_id OR trx.ncs_id != s.ncs_id;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

open cur1;

read_loop: LOOP
fetch cur1 into c_date_id,c_sp_id,c_sp_name,c_app_id,c_app_name,c_operator_id,c_ncs_id,c_direction,c_revenue_share_percentage,c_sp_revenue,
c_operator_revenue,c_total_revenue,c_traffic,c_charged_msg_count;

if done then
	leave read_loop;
end if;
insert into `sdp_transaction_summary` (date_id,sp_id,sp_name,app_id,app_name,revenue_share_percentage,operator_id,ncs_id,direction,sp_revenue,operator_revenue,total_revenue, charged_msg_count)
values(c_date_id,c_sp_id,c_sp_name,c_app_id,c_app_name,c_revenue_share_percentage,c_operator_id,c_ncs_id,c_direction,c_sp_revenue,c_operator_revenue,c_total_revenue,c_charged_msg_count)
ON DUPLICATE key UPDATE sp_revenue=c_sp_revenue, operator_revenue=c_operator_revenue, total_revenue=c_total_revenue;

end LOOP;
close cur1;

call populate_transaction_summary_downloadable_traffic(requ_date);

end//

-- ===========================================================================================================

-- ========================================================================================
-- stored_procudure for update the transaction_summary for donwloadable transaction traffic
-- ========================================================================================

DROP PROCEDURE IF EXISTS populate_transaction_summary_downloadable_traffic//
CREATE PROCEDURE populate_transaction_summary_downloadable_traffic(IN requ_date INT(10))

begin

declare done int default 0;

declare c_date_id int(10);
declare c_sp_id varchar(32);
declare c_sp_name varchar(100);
declare c_app_id varchar(32);
declare c_app_name varchar(100);
declare c_operator_id int(1);
declare c_ncs_id int(1);
declare c_direction varchar(32);
declare c_revenue_share_percentage double;
declare c_sp_revenue double;
declare c_operator_revenue double;
declare c_total_revenue double;
declare c_traffic int(11);
declare c_charged_msg_count int(11);

declare cur1 cursor for select trx.date_id, trx.sp_id, trx.sp_name, trx.app_id, trx.app_name, trx.op_id, trx.ncs_id,
    trx.direction, trx.revenue_share_percentage,
    trx.sp_revenue,trx.operator_revenue,
    trx.total_revenue,trx.Traffic,
    trx.charged_msg_count from (select din.date_id as date_id, t.sp_id as sp_id, t.sp_name as sp_name, t.app_id as app_id,
     t.app_name as app_name, op.id as op_id, ncs.id as ncs_id, t.direction as direction, t.revenue_share_percentage as revenue_share_percentage,
    sum(t.sys_cur_sp_profit) as sp_revenue,sum(t.sys_cur_vc_profit) as operator_revenue,
    sum(t.sys_cur_sp_profit + t.sys_cur_vc_profit) as total_revenue,count(*) as Traffic,
    (CASE WHEN charging_type IS NOT NULL AND charging_type NOT IN('free','') THEN count(*) ELSE 0 END) as charged_msg_count
from date_info din, sdp_transaction t, operator op,ncs
where din.full_date = DATE(t.time_stamp) and din.date_id = requ_date and ncs.ncs_name = t.ncs and t.response_description='download-completed' and t.ncs='downloadable' and t.file_type='cms'
group by date_id, sp_id, app_id, op.id, ncs_id, direction) as trx, system_app as s where trx.app_id != s.app_id OR trx.ncs_id != s.ncs_id;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

open cur1;

read_loop: LOOP
fetch cur1 into c_date_id,c_sp_id,c_sp_name,c_app_id,c_app_name,c_operator_id,c_ncs_id,c_direction,c_revenue_share_percentage,c_sp_revenue,
c_operator_revenue,c_total_revenue,c_traffic,c_charged_msg_count;

if done then
	leave read_loop;
end if;

insert into `sdp_transaction_summary` (date_id,sp_id,sp_name,app_id,app_name,revenue_share_percentage,operator_id,ncs_id,direction,traffic)
values (c_date_id,c_sp_id,c_sp_name,c_app_id,c_app_name,c_revenue_share_percentage,c_operator_id,c_ncs_id,c_direction,c_traffic)
 ON DUPLICATE key UPDATE traffic=c_traffic;

end LOOP;
close cur1;
end//


-- populate_filed_summary procedure consider P1500 status code also as a fail transaction.
-- Therefore change the procedure to omit transaction with response code as P1500.

DROP PROCEDURE IF EXISTS populate_failed_summary//
CREATE PROCEDURE populate_failed_summary(IN requested_date DATE)
       BEGIN

       DECLARE no_more_records INT DEFAULT 0;
       DECLARE summary_date_id INT(10);
       DECLARE summary_sp_id VARCHAR(32);
       DECLARE summary_app_id VARCHAR(32);
       DECLARE summary_error_code VARCHAR(10);
       DECLARE summary_ncs_id INT(1);
       DECLARE summary_direction VARCHAR(2);
       DECLARE summary_error_count INT;

       DECLARE  cur_failed_summary CURSOR FOR
       SELECT  t2.`date_id` dt_id, t1.`sp_id`, t1.`app_id`, t1.`response_code`, t3.`id`, t1.`direction`, count(t1.`time_stamp`) frequency
       FROM `sdp_transaction` t1, `date_info` t2, `ncs` t3
       WHERE t2.`full_date` = date(t1.`time_stamp`)
       AND t3.`ncs_name` = t1.`ncs`
       AND t1.`response_code` <> 'S1000'
       AND t1.`response_code` <> 'P1500'
       AND t1.`response_code` <> 'P1501'
       AND date(t1.`time_stamp`) = requested_date
       GROUP BY dt_id, t1.`sp_id`, t1.`app_id`, t1.`response_code`, t1.`ncs`, t1.`direction`;

       DECLARE  CONTINUE HANDLER FOR NOT FOUND
       SET  no_more_records = 1;

       OPEN cur_failed_summary;

       read_loop : LOOP
       FETCH  cur_failed_summary INTO summary_date_id, summary_sp_id, summary_app_id, summary_error_code, summary_ncs_id, summary_direction, summary_error_count;

       IF no_more_records THEN
          LEAVE read_loop;
       END IF;
       INSERT INTO `sdp_failed_summary`
       VALUES (summary_date_id, summary_sp_id, summary_app_id, summary_error_code, summary_ncs_id, summary_direction, summary_error_count );
       END LOOP;

       CLOSE  cur_failed_summary;
       END //

delimiter ;