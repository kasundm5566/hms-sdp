-- ==================================================================================
-- Add new column to sdp_transaction_summary table to include event type of each transaction
-- ==================================================================================

ALTER TABLE `sdp_transaction_summary` ADD COLUMN `event_type` VARCHAR(32);

-- ==================================================================================
-- Add new column to sdp_transaction_summary table to include payment instrument of each transaction
-- ==================================================================================

ALTER TABLE `sdp_transaction_summary` ADD COLUMN `payment_instrument` VARCHAR(32);

-- ==================================================================================
-- Add new column to sdp_transaction_summary table to include unit charge amount of each transaction
-- ==================================================================================

ALTER TABLE `sdp_transaction_summary` ADD COLUMN `unit_charge_price` DOUBLE;

-- ==================================================================================
-- Add new column to sdp_transaction_summary table to include charging_type each transaction
-- ==================================================================================

ALTER TABLE `sdp_transaction_summary` ADD COLUMN `charging_type` VARCHAR(32);

-- ===================================================
-- Stored Procedure to update the failed summary
-- ===================================================

DELIMITER //

DROP PROCEDURE IF EXISTS populate_failed_summary//
CREATE PROCEDURE populate_failed_summary(IN requested_date DATE)
       BEGIN

       DECLARE no_more_records INT DEFAULT 0;
       DECLARE summary_date_id INT(10);
       DECLARE summary_sp_id VARCHAR(32);
       DECLARE summary_app_id VARCHAR(32);
       DECLARE summary_error_code VARCHAR(10);
       DECLARE summary_ncs_id INT(1);
       DECLARE summary_direction VARCHAR(32);
       DECLARE summary_error_count INT;

       DECLARE  cur_failed_summary CURSOR FOR
		SELECT
			di.`date_id` dt_id,
			sdp.`sp_id`,
			sdp.`app_id`,
			sdp.`response_code`,
			ncs.`id`,
			sdp.`direction`,
			count(sdp.`time_stamp`) frequency
		FROM
			`sdp_transaction` sdp,
			`date_info` di,
			`ncs` ncs
		WHERE
			di.`full_date` = date(sdp.`time_stamp`)
				AND ncs.`ncs_name` = sdp.`ncs`
				AND sdp.`response_code` <> 'S1000'
				AND sdp.`response_code` <> 'P1500'
				AND sdp.`response_code` <> 'P1501'
				AND date(sdp.`time_stamp`) = requested_date
		GROUP BY dt_id , sdp.`sp_id` , sdp.`app_id` , sdp.`response_code` , sdp.`ncs` , sdp.`direction`;

       DECLARE  CONTINUE HANDLER FOR NOT FOUND
       SET  no_more_records = 1;

       OPEN cur_failed_summary;

       read_loop : LOOP
       FETCH  cur_failed_summary INTO summary_date_id, summary_sp_id, summary_app_id, summary_error_code, summary_ncs_id, summary_direction, summary_error_count;

       IF no_more_records THEN
          LEAVE read_loop;
       END IF;
       INSERT INTO `sdp_failed_summary`
       VALUES (summary_date_id, summary_sp_id, summary_app_id, summary_error_code, summary_ncs_id, summary_direction, summary_error_count);
       END LOOP;

       CLOSE  cur_failed_summary;
       END //
DELIMITER ;
-- ===========================================================================================================


