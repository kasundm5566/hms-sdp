-- ============================================================================================
-- Add new column to sdp_transaction_summary table to include event type of each transaction
-- ============================================================================================

ALTER TABLE `sdp_transaction_summary` ADD COLUMN `event_type` VARCHAR(32);

-- ==================================================================================================
-- Add new column to sdp_transaction_summary table to include payment instrument of each transaction
-- ==================================================================================================

ALTER TABLE `sdp_transaction_summary` ADD COLUMN `payment_instrument` VARCHAR(32);

-- ================================================================================
-- Add new column to payment_instruments table to identify the transaction_type
-- ================================================================================

ALTER TABLE `payment_instruments` ADD COLUMN `transaction_type` VARCHAR(15);

-- =================================================================================
-- Add new column to payment_instruments table to include external_share_percentage
-- =================================================================================

ALTER TABLE `payment_instruments` ADD COLUMN `external_share_percentage` INT(11);

-- ==================================================================================
-- Add new column to pgw_transaction_summary table to include ncs of each transaction
-- ==================================================================================

ALTER TABLE `pgw_transaction_summary` ADD COLUMN `ncs` VARCHAR(12);

-- ==================================================================================
-- Add new field to pgw_transaction_summary composite primary key
-- ==================================================================================

ALTER TABLE pgw_transaction_summary
    DROP PRIMARY KEY,
    ADD PRIMARY KEY (date_id, payment_instrument, tx_type);

-- ==================================================================================
-- Update values in the payment_instrument_summary table
-- ==================================================================================

UPDATE `payment_instruments`
SET `transaction_type`='internal', `external_share_percentage`=0
WHERE id=1;

UPDATE `payment_instruments`
SET `transaction_type`='external', `external_share_percentage`=2
WHERE id=2;

-- ================================================================================================
-- Stored Procedure to update the transaction_summary
-- ================================================================================================

DELIMITER //

DROP PROCEDURE IF EXISTS populate_transaction_summary//
CREATE PROCEDURE populate_transaction_summary(IN requ_date DATE)

  BEGIN
    DECLARE no_more_records INT DEFAULT 0;
    DECLARE requ_date_id INT;
    DECLARE summary_date_id INT(10);
    DECLARE summary_sp_id VARCHAR(32);
    DECLARE summary_sp_name VARCHAR(100);
    DECLARE summary_app_id VARCHAR(32);
    DECLARE summary_app_name VARCHAR(100);
    DECLARE summary_operator_id INT(1);
    DECLARE summary_ncs_id INT(1);
    DECLARE summary_direction VARCHAR(32);
    DECLARE summary_event_type VARCHAR(32);
    DECLARE summary_payment_instrument VARCHAR(32);
    DECLARE summary_revenue_share_percentage DOUBLE;
    DECLARE summary_sp_revenue DOUBLE;
    DECLARE summary_operator_revenue DOUBLE;
    DECLARE summary_total_revenue DOUBLE;
    DECLARE summary_traffic INT(11);
    DECLARE summary_charged_msg_count INT(11);
    DECLARE summary_unit_charge_price DOUBLE;
    DECLARE summary_charging_type VARCHAR(32);
    DECLARE cur_summary CURSOR FOR
      SELECT
        trx.date_id,
        trx.sp_id,
        trx.sp_name,
        trx.app_id,
        trx.app_name,
        trx.op_id,
        trx.ncs_id,
        trx.direction,
        trx.event_type,
        trx.payment_instrument,
        trx.revenue_share_percentage,
        trx.sp_revenue,
        trx.operator_revenue,
        trx.total_revenue,
        trx.traffic,
        trx.charged_msg_count,
        trx.unit_price,
        trx.charging_type
      FROM
        (SELECT
           dt.date_id AS date_id,
           sdp.sp_id AS sp_id,
           sdp.sp_name AS sp_name,
           sdp.app_id AS app_id,
           sdp.app_name AS app_name,
           op.id AS op_id,
           ncs.id AS ncs_id,
           sdp.direction AS direction,
           sdp.event_type AS event_type,
           sdp.payment_instrument AS payment_instrument,
           sdp.revenue_share_percentage AS revenue_share_percentage,
           SUM(sdp.sys_cur_sp_profit) AS sp_revenue,
           SUM(sdp.sys_cur_vc_profit) AS operator_revenue,
           SUM(sdp.sys_cur_sp_profit + sdp.sys_cur_vc_profit) AS total_revenue,
           COUNT(*) AS traffic,
           (CASE
            WHEN
              charging_type IS NOT NULL
              AND charging_type NOT IN ('free' , '')
            THEN
              COUNT(*)
            ELSE 0
            END) AS charged_msg_count,
           sdp.charge_amount AS unit_price,
           sdp.charging_type AS charging_type
         FROM
           date_info dt, sdp_transaction sdp, operator op, ncs
         WHERE
           dt.full_date = DATE(sdp.time_stamp)
           AND dt.date_id = requ_date_id
           AND ncs.ncs_name = sdp.ncs
           AND sdp.response_code = 'S1000'
           AND sdp.file_type != 'cms'
         GROUP BY date_id , sp_id , app_id , op.id , ncs_id , direction) AS trx,
        system_app AS sys
      WHERE
        trx.app_id != sys.app_id
        OR trx.ncs_id != sys.ncs_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_records = 1;

    SELECT date_id INTO requ_date_id FROM date_info WHERE full_date = requ_date;

    OPEN cur_summary;

    read_loop: LOOP

      FETCH cur_summary INTO summary_date_id, summary_sp_id, summary_sp_name, summary_app_id, summary_app_name, summary_operator_id, summary_ncs_id,
        summary_direction, summary_event_type, summary_payment_instrument, summary_revenue_share_percentage, summary_sp_revenue, summary_operator_revenue,
        summary_total_revenue, summary_traffic, summary_charged_msg_count, summary_unit_charge_price, summary_charging_type;

      IF no_more_records THEN
        LEAVE read_loop;
      END IF;

      INSERT INTO `sdp_transaction_summary` (date_id, sp_id, sp_name, app_id, app_name, revenue_share_percentage, operator_id, ncs_id, direction, sp_revenue,
                                             operator_revenue, total_revenue, traffic, charged_msg_count, event_type, payment_instrument, unit_charge_price, charging_type)
      VALUES (summary_date_id, summary_sp_id, summary_sp_name, summary_app_id, summary_app_name, summary_revenue_share_percentage, summary_operator_id, summary_ncs_id, summary_direction, summary_sp_revenue,
              summary_operator_revenue, summary_total_revenue, summary_traffic, summary_charged_msg_count, summary_event_type, summary_payment_instrument, summary_unit_charge_price, summary_charging_type);

    END LOOP;

    CLOSE cur_summary;
    CALL populate_transaction_summary_downloadable_revenue(requ_date);

  END//

DELIMITER ;
-- ===========================================================================================================


-- ================================================================================================
-- Stored Procedure to update the transaction_summary for downloadable transaction revenue
-- ================================================================================================

DELIMITER //

DROP PROCEDURE IF EXISTS populate_transaction_summary_downloadable_revenue//
CREATE PROCEDURE populate_transaction_summary_downloadable_revenue(IN requ_date DATE)

  BEGIN
    DECLARE no_more_records INT DEFAULT 0;
    DECLARE requ_date_id INT;
    DECLARE summary_date_id INT(10);
    DECLARE summary_sp_id VARCHAR(32);
    DECLARE summary_sp_name VARCHAR(100);
    DECLARE summary_app_id VARCHAR(32);
    DECLARE summary_app_name VARCHAR(100);
    DECLARE summary_operator_id INT(1);
    DECLARE summary_ncs_id INT(1);
    DECLARE summary_direction VARCHAR(32);
    DECLARE summary_event_type VARCHAR(32);
    DECLARE summary_payment_instrument VARCHAR(32);
    DECLARE summary_revenue_share_percentage DOUBLE;
    DECLARE summary_sp_revenue DOUBLE;
    DECLARE summary_operator_revenue DOUBLE;
    DECLARE summary_total_revenue DOUBLE;
    DECLARE summary_traffic INT(11);
    DECLARE summary_charged_msg_count INT(11);
    DECLARE summary_unit_charge_price DOUBLE;
    DECLARE summary_charging_type VARCHAR(32);
    DECLARE cur_summary CURSOR FOR
      SELECT
        trx.date_id,
        trx.sp_id,
        trx.sp_name,
        trx.app_id,
        trx.app_name,
        trx.op_id,
        trx.ncs_id,
        trx.direction,
        trx.event_type,
        trx.payment_instrument,
        trx.revenue_share_percentage,
        trx.sp_revenue,
        trx.operator_revenue,
        trx.total_revenue,
        trx.traffic,
        trx.charged_msg_count,
        trx.unit_price,
        trx.charging_type
      FROM
        (SELECT
           dt.date_id AS date_id,
           sdp.sp_id AS sp_id,
           sdp.sp_name AS sp_name,
           sdp.app_id AS app_id,
           sdp.app_name AS app_name,
           op.id AS op_id,
           ncs.id AS ncs_id,
           sdp.direction AS direction,
           sdp.event_type AS event_type,
           sdp.payment_instrument AS payment_instrument,
           sdp.revenue_share_percentage AS revenue_share_percentage,
           SUM(sdp.sys_cur_sp_profit) AS sp_revenue,
           SUM(sdp.sys_cur_vc_profit) AS operator_revenue,
           SUM(sdp.sys_cur_sp_profit + sdp.sys_cur_vc_profit) AS total_revenue,
           COUNT(*) AS traffic,
           (CASE
            WHEN
              charging_type IS NOT NULL
              AND charging_type NOT IN ('free' , '')
            THEN
              COUNT(*)
            ELSE 0
            END) AS charged_msg_count,
           sdp.charge_amount AS unit_price,
           sdp.charging_type AS charging_type
         FROM
           date_info dt, sdp_transaction sdp, operator op, ncs
         WHERE
           dt.full_date = DATE(sdp.time_stamp)
           AND dt.date_id = requ_date_id
           AND ncs.ncs_name = sdp.ncs
           AND sdp.response_description='eligible'
           AND sdp.ncs='downloadable'
           AND sdp.file_type='cms'
         GROUP BY date_id , sp_id , app_id , op.id , ncs_id , direction) AS trx,
        system_app AS sys
      WHERE
        trx.app_id != sys.app_id
        OR trx.ncs_id != sys.ncs_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_records = 1;

    SELECT date_id INTO requ_date_id FROM date_info WHERE full_date = requ_date;

    OPEN cur_summary;

    read_loop: LOOP

      FETCH cur_summary INTO summary_date_id, summary_sp_id, summary_sp_name, summary_app_id, summary_app_name, summary_operator_id, summary_ncs_id,
        summary_direction, summary_event_type, summary_payment_instrument, summary_revenue_share_percentage, summary_sp_revenue, summary_operator_revenue,
        summary_total_revenue, summary_traffic, summary_charged_msg_count, summary_unit_charge_price, summary_charging_type;

      IF no_more_records THEN
        LEAVE read_loop;
      END IF;

      INSERT INTO `sdp_transaction_summary` (date_id, sp_id, sp_name, app_id, app_name, revenue_share_percentage, operator_id, ncs_id, direction, sp_revenue,
                                             operator_revenue, total_revenue, traffic, charged_msg_count, event_type, payment_instrument, unit_charge_price, charging_type)
      VALUES (summary_date_id, summary_sp_id, summary_sp_name, summary_app_id, summary_app_name, summary_revenue_share_percentage, summary_operator_id, summary_ncs_id, summary_direction, summary_sp_revenue,
              summary_operator_revenue, summary_total_revenue, summary_traffic, summary_charged_msg_count, summary_event_type, summary_payment_instrument, summary_unit_charge_price, summary_charging_type)
      ON DUPLICATE KEY UPDATE sp_revenue=summary_sp_revenue, operator_revenue=summary_operator_revenue, total_revenue=summary_total_revenue;
    END LOOP;

    CLOSE cur_summary;
    CALL populate_transaction_summary_downloadable_traffic(requ_date);

  END//

DELIMITER ;
-- ===========================================================================================================

-- ================================================================================================
-- Stored Procedure to update the transaction_summary for downloadable transaction traffic
-- ================================================================================================

DELIMITER //

DROP PROCEDURE IF EXISTS populate_transaction_summary_downloadable_traffic//
CREATE PROCEDURE populate_transaction_summary_downloadable_traffic(IN requ_date DATE)

  BEGIN
    DECLARE no_more_records INT DEFAULT 0;
    DECLARE requ_date_id INT;
    DECLARE summary_date_id INT(10);
    DECLARE summary_sp_id VARCHAR(32);
    DECLARE summary_sp_name VARCHAR(100);
    DECLARE summary_app_id VARCHAR(32);
    DECLARE summary_app_name VARCHAR(100);
    DECLARE summary_operator_id INT(1);
    DECLARE summary_ncs_id INT(1);
    DECLARE summary_direction VARCHAR(32);
    DECLARE summary_event_type VARCHAR(32);
    DECLARE summary_payment_instrument VARCHAR(32);
    DECLARE summary_revenue_share_percentage DOUBLE;
    DECLARE summary_sp_revenue DOUBLE;
    DECLARE summary_operator_revenue DOUBLE;
    DECLARE summary_total_revenue DOUBLE;
    DECLARE summary_traffic INT(11);
    DECLARE summary_charged_msg_count INT(11);
    DECLARE summary_unit_charge_price DOUBLE;
    DECLARE summary_charging_type VARCHAR(32);
    DECLARE cur_summary CURSOR FOR
      SELECT
        trx.date_id,
        trx.sp_id,
        trx.sp_name,
        trx.app_id,
        trx.app_name,
        trx.op_id,
        trx.ncs_id,
        trx.direction,
        trx.event_type,
        trx.payment_instrument,
        trx.revenue_share_percentage,
        trx.sp_revenue,
        trx.operator_revenue,
        trx.total_revenue,
        trx.traffic,
        trx.charged_msg_count,
        trx.unit_price,
        trx.charging_type
      FROM
        (SELECT
           dt.date_id AS date_id,
           sdp.sp_id AS sp_id,
           sdp.sp_name AS sp_name,
           sdp.app_id AS app_id,
           sdp.app_name AS app_name,
           op.id AS op_id,
           ncs.id AS ncs_id,
           sdp.direction AS direction,
           sdp.event_type AS event_type,
           sdp.payment_instrument AS payment_instrument,
           sdp.revenue_share_percentage AS revenue_share_percentage,
           SUM(sdp.sys_cur_sp_profit) AS sp_revenue,
           SUM(sdp.sys_cur_vc_profit) AS operator_revenue,
           SUM(sdp.sys_cur_sp_profit + sdp.sys_cur_vc_profit) AS total_revenue,
           COUNT(*) AS traffic,
           (CASE
            WHEN
              charging_type IS NOT NULL
              AND charging_type NOT IN ('free' , '')
            THEN
              COUNT(*)
            ELSE 0
            END) AS charged_msg_count,
           sdp.charge_amount AS unit_price,
           sdp.charging_type AS charging_type
         FROM
           date_info dt, sdp_transaction sdp, operator op, ncs
         WHERE
           dt.full_date = DATE(sdp.time_stamp)
           AND dt.date_id = requ_date_id
           AND ncs.ncs_name = sdp.ncs
           AND sdp.response_description='download-completed'
           AND sdp.ncs='downloadable'
           AND sdp.file_type='cms'
         GROUP BY date_id , sp_id , app_id , op.id , ncs_id , direction) AS trx,
        system_app AS sys
      WHERE
        trx.app_id != sys.app_id
        OR trx.ncs_id != sys.ncs_id;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_records = 1;

    SELECT date_id INTO requ_date_id FROM date_info WHERE full_date = requ_date;

    OPEN cur_summary;

    read_loop: LOOP

      FETCH cur_summary INTO summary_date_id, summary_sp_id, summary_sp_name, summary_app_id, summary_app_name, summary_operator_id, summary_ncs_id,
        summary_direction, summary_event_type, summary_payment_instrument, summary_revenue_share_percentage, summary_sp_revenue, summary_operator_revenue,
        summary_total_revenue, summary_traffic, summary_charged_msg_count, summary_unit_charge_price, summary_charging_type;

      IF no_more_records THEN
        LEAVE read_loop;
      END IF;

      INSERT INTO `sdp_transaction_summary` (date_id, sp_id, sp_name, app_id, app_name, revenue_share_percentage, operator_id, ncs_id, direction, sp_revenue,
                                             operator_revenue, total_revenue, traffic, charged_msg_count, event_type, payment_instrument, unit_charge_price, charging_type)
      VALUES (summary_date_id, summary_sp_id, summary_sp_name, summary_app_id, summary_app_name, summary_revenue_share_percentage, summary_operator_id, summary_ncs_id, summary_direction, summary_sp_revenue,
              summary_operator_revenue, summary_total_revenue, summary_traffic, summary_charged_msg_count, summary_event_type, summary_payment_instrument, summary_unit_charge_price, summary_charging_type)
      ON DUPLICATE KEY UPDATE traffic=summary_traffic;
    END LOOP;

    CLOSE cur_summary;

  END//

DELIMITER ;


-- ====================================================================================
-- Stored Procedure to populate the pgw_transaction_summary
-- ====================================================================================

DELIMITER //

DROP PROCEDURE IF EXISTS populate_pgw_transaction_summary//
CREATE PROCEDURE `populate_pgw_transaction_summary`(IN requ_date DATE)
BEGIN
       DECLARE no_more_records INT DEFAULT 0;
       DECLARE state VARCHAR(50);
       DECLARE summary_date_id INT(10);
       DECLARE summary_payment_instrument VARCHAR(32);
       DECLARE summary_tax_type VARCHAR(32);
       DECLARE summary_ncs VARCHAR(32);
       DECLARE summary_request_amount DOUBLE ;
       DECLARE summary_paid_amount DOUBLE;
       DECLARE summary_total_transactions INT;
       DECLARE summary_tax_amount FLOAT;
       DECLARE summary_sp_earning DOUBLE ;
       DECLARE summary_sdp_income FLOAT;
       DECLARE cur_summary CURSOR FOR SELECT COUNT(*) AS total_transactions,
                dt.`date_id`,
                sdp.`payment_instrument`,
                sdp.`ncs`,
                SUM(sdp.`charge_amount`),
                CASE WHEN (pi.`transaction_type` = 'external' && sdp.`ncs` = 'cas') THEN (SUM(sdp.`charge_amount`) * (pi.`external_share_percentage`)/100)
                ELSE (SUM(sdp.`charge_amount`) * (100 - sdp.`revenue_share_percentage`)/100) END
           FROM date_info dt, sdp_transaction sdp, payment_instruments pi, system_app
           WHERE DATE(sdp.`time_stamp`) = requ_date AND DATE(sdp.`time_stamp`) = dt.`full_date` AND sdp.response_code = 'S1000'
                AND sdp.`payment_instrument` = pi.`name` AND sdp.`app_id` != system_app.`app_id`
           GROUP BY sdp.ncs, sdp.`payment_instrument`, dt.`date_id`;

       DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_records = 1;

       OPEN cur_summary;

       read_loop : LOOP
           FETCH cur_summary INTO summary_total_transactions, summary_date_id,
                summary_payment_instrument, summary_ncs, summary_paid_amount, summary_sdp_income;
           IF no_more_records THEN
                LEAVE read_loop;
           END IF;
           INSERT INTO pgw_transaction_summary
                VALUES (summary_date_id, NULL, summary_total_transactions,NULL, summary_paid_amount,
                'null', summary_payment_instrument, summary_sdp_income, summary_ncs);
       END LOOP;

       CLOSE cur_summary;
END//

DELIMITER ;
-- ===========================================================================================================
