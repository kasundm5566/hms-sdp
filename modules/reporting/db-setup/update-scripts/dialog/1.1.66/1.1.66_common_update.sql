-- =====================================================
-- Add new column to sdp_transaction table to identify the system_id (sdp/cms)
-- =====================================================

ALTER TABLE `sdp_transaction` ADD COLUMN `file_type` VARCHAR(15);


-- =====================================================================
-- Add new column to sdp_transaction table to include payment_instrument
-- ======================================================================

ALTER TABLE `sdp_transaction` ADD COLUMN `payment_instrument` VARCHAR(15);