-- Drop existing populate_sdp_reconciliation_summary. According to new procedure when calculating sp_revenue
-- sdp logs related ezcash payment amount is not considered while cms logs related ezcash amount is taken into sp_revenue calculation.
DROP PROCEDURE IF EXISTS populate_sdp_reconciliation_summary;
DROP PROCEDURE IF EXISTS update_sdp_reconciliation_summary;

DELIMITER //

--  -- ===================================================
-- stored_procedure to populate the sdp_reconciliation_summary from cms transactions in sdp_transaction table
-- =====================================================

DROP PROCEDURE IF EXISTS populate_sdp_reconciliation_summary//
    CREATE PROCEDURE populate_sdp_reconciliation_summary(IN requested_date DATE)
      BEGIN
      DECLARE DONE INT DEFAULT 0;
       DECLARE requested_date_id INT(10);
       DECLARE sp_date_id INT(10);
       DECLARE sp_sp_id VARCHAR(32);
       DECLARE sp_app_id VARCHAR(32);
       DECLARE sp_msg_rev_sp_profit DOUBLE(10,4);
       DECLARE sp_msg_rev_vc_profit DOUBLE(10,4);
       DECLARE sp_msg_rev_op_cost DOUBLE(10,4);
       DECLARE sp_app_total_rev DOUBLE(10,4);
       DECLARE cur_sdp_reconciliation_summary_1 CURSOR FOR
         SELECT date_info.date_id, sdp.sp_id, sdp.app_id, SUM(sdp.sys_cur_sp_profit), SUM(sdp.sys_cur_vc_profit), SUM(sdp.sys_cur_operator_cost)
         FROM date_info, sdp_transaction sdp
         WHERE sdp.file_type = 'cms' AND sdp.response_code = 'P1500' AND date_info.full_date = DATE(sdp.time_stamp) AND
         date_info.date_id = requested_date_id GROUP BY date_id, sp_id, app_id;


       DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

        SELECT date_id INTO requested_date_id FROM date_info WHERE full_date = requested_date;

        OPEN cur_sdp_reconciliation_summary_1;
          read_loop: LOOP

              FETCH cur_sdp_reconciliation_summary_1 INTO sp_date_id, sp_sp_id, sp_app_id, sp_msg_rev_sp_profit, sp_msg_rev_vc_profit, sp_msg_rev_op_cost;

              IF DONE THEN
                leave read_loop;
              END IF;

              INSERT INTO `sdp_reconciliation_summary` (date_id,sp_id,app_id,msg_rev_sp_profit,msg_rev_vc_profit,msg_rev_op_cost,app_total_rev)
              VALUES (sp_date_id, sp_sp_id, sp_app_id, sp_msg_rev_sp_profit, sp_msg_rev_vc_profit, sp_msg_rev_op_cost, sp_msg_rev_sp_profit + sp_msg_rev_vc_profit + sp_msg_rev_op_cost);


          END LOOP;


        CLOSE cur_sdp_reconciliation_summary_1;
        CALL update_sdp_reconciliation_summary(requested_date_id);

        END //



--  -- ===================================================
-- stored_procedure to populate the sdp_reconciliation_summary from sdp transactions in sdp_transaction table
-- =====================================================
DROP PROCEDURE IF EXISTS update_sdp_reconciliation_summary//
    CREATE PROCEDURE update_sdp_reconciliation_summary(IN requested_date_id INT(10))
      BEGIN
      DECLARE DONE INT DEFAULT 0;
       DECLARE sp_date_id INT(10);
       DECLARE sp_sp_id VARCHAR(32);
       DECLARE sp_app_id VARCHAR(32);
       DECLARE sp_msg_rev_sp_profit DOUBLE(10,4);
       DECLARE sp_msg_rev_vc_profit DOUBLE(10,4);
       DECLARE sp_msg_rev_op_cost DOUBLE(10,4);
       DECLARE sp_app_total_rev DOUBLE(10,4);
       DECLARE cur_sdp_reconciliation_summary_1 CURSOR FOR
         SELECT date_info.date_id, sdp.sp_id, sdp.app_id, SUM(sdp.sys_cur_sp_profit), SUM(sdp.sys_cur_vc_profit), SUM(sdp.sys_cur_operator_cost)
         FROM date_info, sdp_transaction sdp
         WHERE sdp.file_type = 'sdp' AND sdp.response_code = 'S1000' AND sdp.payment_instrument NOT IN
         (SELECT payment_instrument FROM ignore_payment_instrument) AND sdp.app_id NOT IN (SELECT app_id FROM system_app)
          AND date_info.full_date = DATE(sdp.time_stamp) AND
         date_info.date_id = requested_date_id GROUP BY date_id, sp_id, app_id;


       DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

        OPEN cur_sdp_reconciliation_summary_1;
          read_loop: LOOP

              FETCH cur_sdp_reconciliation_summary_1 INTO sp_date_id, sp_sp_id, sp_app_id, sp_msg_rev_sp_profit, sp_msg_rev_vc_profit, sp_msg_rev_op_cost;

              IF DONE THEN
                leave read_loop;
              END IF;

              INSERT INTO `sdp_reconciliation_summary` (date_id,sp_id,app_id,msg_rev_sp_profit,msg_rev_vc_profit,msg_rev_op_cost,app_total_rev)
              VALUES (sp_date_id, sp_sp_id, sp_app_id, sp_msg_rev_sp_profit, sp_msg_rev_vc_profit, sp_msg_rev_op_cost,
              sp_msg_rev_sp_profit + sp_msg_rev_vc_profit + sp_msg_rev_op_cost)
              ON DUPLICATE KEY UPDATE msg_rev_sp_profit = VALUES(msg_rev_sp_profit) + sp_msg_rev_sp_profit ,
              msg_rev_vc_profit = VALUES (msg_rev_vc_profit) + sp_msg_rev_vc_profit,
              msg_rev_op_cost = VALUES (msg_rev_op_cost) + sp_msg_rev_op_cost,
              app_total_rev = VALUES (app_total_rev) + (sp_msg_rev_sp_profit + sp_msg_rev_vc_profit + sp_msg_rev_op_cost);




          END LOOP;


        CLOSE cur_sdp_reconciliation_summary_1;

        END //

DELIMITER ;

-- index for populate_sdp_reconciliation_summary procedure
CREATE index ix_cms_reconciliation_summary ON sdp_transaction(file_type, response_code);

-- initial data for ignore_payment_instrument table.
INSERT INTO  ignore_payment_instrument VALUES (1,'Dialog-Ezcash');
