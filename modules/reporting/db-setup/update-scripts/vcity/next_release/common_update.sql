ALTER TABLE service_provider_info ADD INDEX idx_app_id (app_id);

ALTER TABLE `service_provider_info` ADD COLUMN `app_state` varchar(30);

ALTER TABLE `sdp_subscription_summary` ADD COLUMN `dereg` INT(11) AFTER new_reg;

ALTER TABLE `service_provider_info` ADD COLUMN `created_time` datetime;

-- ==================================================================================
-- Add new column to sdp_transaction_summary table to include event type of each transaction
-- ==================================================================================

ALTER TABLE `sdp_transaction_summary` ADD COLUMN `event_type` VARCHAR(20);

-- ==================================================================================
-- Add new column to sdp_transaction_summary table to include payment instrument of each transaction
-- ==================================================================================

ALTER TABLE `sdp_transaction_summary` ADD COLUMN `payment_instrument` VARCHAR(45);

-- ==================================================================================
-- Add new column to sdp_transaction_summary table to include unit charge amount of each transaction
-- ==================================================================================

ALTER TABLE `sdp_transaction_summary` ADD COLUMN `unit_charge_price` DOUBLE;

-- ==================================================================================
-- Add new column to sdp_transaction_summary table to include charging_type each transaction
-- ==================================================================================

ALTER TABLE `sdp_transaction_summary` ADD COLUMN `charging_type` VARCHAR(30);
