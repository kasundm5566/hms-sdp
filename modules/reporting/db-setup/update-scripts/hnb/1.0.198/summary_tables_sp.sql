DELIMITER //
-- ===========================================================================================================

/* This procedure use for execute sql queries */
drop procedure if exists queryExecutor //
create procedure queryExecutor(query varchar(4500))

begin

select query;
set @tmp_sql=query;
prepare statement1 from @tmp_sql;
execute statement1;
deallocate prepare statement1;

end//

-- ===================================================
-- stored_procudure for update the transaction_summary
-- ===================================================
DROP PROCEDURE IF EXISTS populate_transaction_summary//
CREATE PROCEDURE populate_transaction_summary(IN requ_date date)

begin

declare done int default 0;

declare requ_date_id int;

declare c_date_id int(10);
declare c_sp_id varchar(32);
declare c_sp_name varchar(100);
declare c_app_id varchar(32);
declare c_app_name varchar(100);
declare c_operator_id int(1);
declare c_ncs_id int(1);
declare c_direction varchar(32);
declare c_revenue_share_percentage double;
declare c_sp_revenue double;
declare c_operator_revenue double;
declare c_total_revenue double;
declare c_traffic int(11);
declare c_charged_msg_count int(11);

declare cur1 cursor for select trx.date_id, trx.sp_id, trx.sp_name, trx.app_id, trx.app_name, trx.op_id, trx.ncs_id,
    trx.direction, trx.revenue_share_percentage,
    trx.sp_revenue,trx.operator_revenue,
    trx.total_revenue,trx.Traffic,
    trx.charged_msg_count from (select din.date_id as date_id, t.sp_id as sp_id, t.sp_name as sp_name, t.app_id as app_id,
     t.app_name as app_name, op.id as op_id, ncs.id as ncs_id, t.direction as direction, t.revenue_share_percentage as revenue_share_percentage,
    sum(t.sys_cur_sp_profit) as sp_revenue,sum(t.sys_cur_vc_profit) as operator_revenue,
    sum(t.sys_cur_sp_profit + t.sys_cur_vc_profit) as total_revenue,count(*) as Traffic,
    (CASE WHEN charging_type IS NOT NULL AND charging_type NOT IN('free','') THEN count(*) ELSE 0 END) as charged_msg_count
from date_info din, sdp_transaction t, operator op,ncs
where din.full_date = DATE(t.time_stamp) and din.date_id = requ_date_id and ncs.ncs_name = t.ncs and t.response_code='S1000'
group by date_id, sp_id, app_id, op.id, ncs_id, direction) as trx, system_app as s where trx.app_id != s.app_id OR trx.ncs_id != s.ncs_id;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

select date_id into requ_date_id from date_info where full_date = requ_date;

open cur1;

read_loop: LOOP
fetch cur1 into c_date_id,c_sp_id,c_sp_name,c_app_id,c_app_name,c_operator_id,c_ncs_id,c_direction,c_revenue_share_percentage,c_sp_revenue,
c_operator_revenue,c_total_revenue,c_traffic,c_charged_msg_count;

if done then
	leave read_loop;
end if;
insert into `sdp_transaction_summary` (date_id,sp_id,sp_name,app_id,app_name,revenue_share_percentage,operator_id,ncs_id,direction,sp_revenue,operator_revenue,total_revenue,traffic, charged_msg_count)
values(c_date_id,c_sp_id,c_sp_name,c_app_id,c_app_name,c_revenue_share_percentage,c_operator_id,c_ncs_id,c_direction,c_sp_revenue,c_operator_revenue,c_total_revenue,c_traffic,c_charged_msg_count);

end LOOP;

close cur1;
end//


-- ===================================================
-- stored_procedure to update the failed_summary
-- ===================================================
DROP PROCEDURE IF EXISTS populate_failed_summary//
CREATE PROCEDURE populate_failed_summary(IN requested_date DATE)
       BEGIN

       DECLARE no_more_records INT DEFAULT 0;
       DECLARE summary_date_id INT(10);
       DECLARE summary_sp_id VARCHAR(32);
       DECLARE summary_app_id VARCHAR(32);
       DECLARE summary_error_code VARCHAR(10);
       DECLARE summary_ncs_id INT(1);
       DECLARE summary_direction VARCHAR(2);
       DECLARE summary_error_count INT;

       DECLARE  cur_failed_summary CURSOR FOR
       SELECT  t2.`date_id` dt_id, t1.`sp_id`, t1.`app_id`, t1.`response_code`, t3.`id`, t1.`direction`, count(t1.`time_stamp`) frequency
       FROM `sdp_transaction` t1, `date_info` t2, `ncs` t3
       WHERE t2.`full_date` = date(t1.`time_stamp`)
       AND t3.`ncs_name` = t1.`ncs`
       AND t1.`response_code` <> 'S1000'
       AND date(t1.`time_stamp`) = requested_date
       GROUP BY dt_id, t1.`sp_id`, t1.`app_id`, t1.`response_code`, t1.`ncs`, t1.`direction`;

       DECLARE  CONTINUE HANDLER FOR NOT FOUND
       SET  no_more_records = 1;

       OPEN cur_failed_summary;

       read_loop : LOOP
       FETCH  cur_failed_summary INTO summary_date_id, summary_sp_id, summary_app_id, summary_error_code, summary_ncs_id, summary_direction, summary_error_count;

       IF no_more_records THEN
          LEAVE read_loop;
       END IF;
       INSERT INTO `sdp_failed_summary`
       VALUES (summary_date_id, summary_sp_id, summary_app_id, summary_error_code, summary_ncs_id, summary_direction, summary_error_count );
       END LOOP;

       CLOSE  cur_failed_summary;
       END //
-- ===========================================================================================================

DELIMITER ;
