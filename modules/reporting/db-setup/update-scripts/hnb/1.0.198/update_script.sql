/*new columns added to sdp_transaction table*/
ALTER TABLE sdp_transaction ADD order_no varchar(50);
ALTER TABLE sdp_transaction ADD invoice_no varchar(50);
ALTER TABLE sdp_transaction ADD external_trx_id varchar(50);
ALTER TABLE sdp_transaction ADD session_id varchar(20);
ALTER TABLE sdp_transaction ADD ussd_operation varchar(20);
ALTER TABLE sdp_transaction ADD balance_due double(10,4);
ALTER TABLE sdp_transaction ADD total_amount double(10,4);

/*new column added to service_provider_info*/
ALTER TABLE service_provider_info ADD app_catogory varchar(32) after app_name;

/*new column added to sdp_transaction_summary*/
ALTER TABLE sdp_transaction_summary ADD charged_msg_count int(10);

/*Initial data for ncs table*/
INSERT INTO ncs VALUES (1,'sms');
INSERT INTO ncs VALUES (2,'ussd');

/*initial data for operator table*/
INSERT INTO operator VALUES (1,'dialog');
INSERT INTO operator VALUES (2,'etisalat');
INSERT INTO operator VALUES (3,'hutch');
INSERT INTO operator VALUES (4,'mobitel');
INSERT INTO operator VALUES (5,'airtel');

/* summary_data_status update scripts  */
INSERT INTO summary_data_status VALUES (1, 'populate_transaction_summary', '2013-01-01', '2013-01-01 00:00:00', Null, 1);
INSERT INTO summary_data_status VALUES (2, 'populate_transaction_summary', '2013-01-01', Null, '2013-01-01 00:00:00', 2);
INSERT INTO summary_data_status VALUES (3, 'populate_failed_summary', '2013-01-01', '2013-01-01 00:00:00', Null, 1);
INSERT INTO summary_data_status VALUES (4, 'populate_failed_summary', '2013-01-01', Null, '2013-01-01 00:00:00', 2);

