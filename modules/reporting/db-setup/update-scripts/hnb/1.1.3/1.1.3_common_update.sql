ALTER TABLE service_provider_info ADD INDEX idx_app_id (app_id);

ALTER TABLE `service_provider_info` ADD COLUMN `app_state` varchar(20);

ALTER TABLE `sdp_subscription_summary` ADD COLUMN `dereg` INT(11) AFTER new_reg;