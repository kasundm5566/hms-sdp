use reporting;

-- =====================================================
-- Add new column to sdp_transaction table to identify the system_id (sdp/cms)
-- =====================================================

ALTER TABLE `sdp_transaction` ADD COLUMN `file_type` VARCHAR(15);

-- =====================================================================
-- Add new column to sdp_transaction table to include payment_instrument
-- ======================================================================

ALTER TABLE `sdp_transaction` ADD COLUMN `payment_instrument` VARCHAR(15);

-- ==================================================================================
-- Add new column to sdp_transaction_summary table to include event type of each transaction
-- ==================================================================================

ALTER TABLE `sdp_transaction_summary` ADD COLUMN `event_type` VARCHAR(20);

-- ==================================================================================
-- Add new column to sdp_transaction_summary table to include payment instrument of each transaction
-- ==================================================================================

ALTER TABLE `sdp_transaction_summary` ADD COLUMN `payment_instrument` VARCHAR(45);

-- ==================================================================================
-- Add new column to sdp_transaction_summary table to include unit charge amount of each transaction
-- ==================================================================================

ALTER TABLE `sdp_transaction_summary` ADD COLUMN `unit_charge_price` DOUBLE;

-- ==================================================================================
-- Add new column to sdp_transaction_summary table to include charging_type each transaction
-- ==================================================================================

ALTER TABLE `sdp_transaction_summary` ADD COLUMN `charging_type` VARCHAR(30);



