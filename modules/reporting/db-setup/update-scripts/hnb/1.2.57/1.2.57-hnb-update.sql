use reporting;

-- =====================================================
-- Add new operator 'international' to hnb operator table
-- =====================================================

ALTER TABLE operator MODIFY operator_name VARCHAR(20);

ALTER TABLE sdp_transaction MODIFY operator VARCHAR(20);

INSERT INTO operator VALUES (6,'international');

-- ======================================================

/* stored procedure does not compare the operator with operator talbe. Added that to operator table */

delimiter //

DROP PROCEDURE IF EXISTS populate_transaction_summary//
CREATE PROCEDURE populate_transaction_summary(IN requ_date date)

begin

declare done int default 0;

declare requ_date_id int;

declare c_date_id int(10);
declare c_sp_id varchar(32);
declare c_sp_name varchar(100);
declare c_app_id varchar(32);
declare c_app_name varchar(100);
declare c_operator_id int(1);
declare c_ncs_id int(1);
declare c_direction varchar(32);
declare c_revenue_share_percentage double;
declare c_sp_revenue double;
declare c_operator_revenue double;
declare c_total_revenue double;
declare c_traffic int(11);
declare c_charged_msg_count int(11);

declare cur1 cursor for select din.date_id, t.sp_id, t.sp_name, t.app_id, t.app_name, op.id, ncs.id as ncs_id, t.direction, t.revenue_share_percentage,
    sum(t.sys_cur_sp_profit) as sp_revenue,sum(t.sys_cur_vc_profit) as operator_revenue,
    sum(t.sys_cur_sp_profit + t.sys_cur_vc_profit) as total_revenue,count(*) as Traffic,
    (CASE WHEN charging_type IS NOT NULL AND charging_type NOT IN('free','') THEN count(*) ELSE 0 END) as charged_msg_count
from date_info din, sdp_transaction t, operator op,ncs
where din.full_date = DATE(t.time_stamp) and din.date_id = requ_date_id and ncs.ncs_name = t.ncs and op.operator_name=t.operator and t.response_code='S1000'
group by date_id, sp_id, app_id, op.id, ncs_id, direction;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

select date_id into requ_date_id from date_info where full_date = requ_date;

open cur1;

read_loop: LOOP
fetch cur1 into c_date_id,c_sp_id,c_sp_name,c_app_id,c_app_name,c_operator_id,c_ncs_id,c_direction,c_revenue_share_percentage,c_sp_revenue,
c_operator_revenue,c_total_revenue,c_traffic,c_charged_msg_count;

if done then
        leave read_loop;
end if;
insert into `sdp_transaction_summary` (date_id,sp_id,sp_name,app_id,app_name,revenue_share_percentage,operator_id,ncs_id,direction,sp_revenue,operator_revenue,total_revenue,traffic, charged_msg_count)
values(c_date_id,c_sp_id,c_sp_name,c_app_id,c_app_name,c_revenue_share_percentage,c_operator_id,c_ncs_id,c_direction,c_sp_revenue,c_operator_revenue,c_total_revenue,c_traffic,c_charged_msg_count);

end LOOP;

close cur1;
end//

delimiter ;