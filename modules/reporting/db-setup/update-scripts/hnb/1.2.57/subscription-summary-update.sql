use reporting;

delimiter //

DROP PROCEDURE IF EXISTS populate_subscription_summary//
CREATE PROCEDURE populate_subscription_summary(IN requ_date date)


begin

declare done int default 0;

declare requ_date_id int;

declare c_date_id int(10);
declare c_sp_id varchar(32);
declare c_sp_name varchar(100);
declare c_app_id varchar(32);
declare c_app_name varchar(100);
declare c_new_reg int(11);
declare c_dreg int(11);


declare cur1 cursor for select din.date_id,t.sp_id,t.sp_name,t.app_id,t.app_name,
(CASE WHEN (t.event_type='freeRegistration' OR t.event_type='registrationCharging') AND t.response_code='S1000' THEN count(*) ELSE 0 END) as new_reg,
(CASE WHEN t.event_type='unregistration' THEN count(*) ELSE 0 END) as dereg
from sdp_transaction as t,date_info as din
where (t.event_type='freeRegistration' OR t.event_type='registrationCharging' OR t.event_type='unregistration') AND
din.date_id=requ_date_id AND din.full_date = DATE(t.time_stamp) group by t.app_id,din.date_id,t.event_type;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

select date_id into requ_date_id from date_info where full_date = requ_date;

open cur1;

read_loop: LOOP
fetch cur1 into c_date_id,c_sp_id,c_sp_name,c_app_id,c_app_name,c_new_reg,c_dreg;

if done then
	leave read_loop;
end if;
insert into `sdp_subscription_summary` (`date_id`,`sp_id`,`sp_name`,`app_id`,`app_name`,`new_reg`,`dereg`)
values(c_date_id,c_sp_id,c_sp_name,c_app_id,c_app_name,c_new_reg,c_dreg)
ON DUPLICATE key UPDATE new_reg = VALUES(new_reg) + new_reg, dereg = VALUES(dereg) + dereg;

end LOOP;

close cur1;
end //

delimiter ;

-- These two new indexes added to customercare detail transaction report.
DROP index ix_sdp_cc_report on sdp_transaction;
create index ix_sdp_cc_report_source_address on sdp_transaction(source_address,time_stamp);
create index ix_sdp_cc_report_destincation_address on sdp_transaction(destination_address, time_stamp);

-- New table for keep payment instruments which shoul not included in reconciliation report
DROP TABLE IF EXISTS `ignore_payment_instrument`;

CREATE TABLE IF NOT EXISTS ignore_payment_instrument (
`id` INT(1),
`payment_instrument` VARCHAR(30)) ENGINE= InnoDB;


