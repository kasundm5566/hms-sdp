-- ============================================
-- [12] create table: corporate_user_info_business_no
-- Populated from the CSV logs from common_registration module
-- corporate_user_id --> This should match with the service_provider_info.corporate_user_id and
-- its having 1:1 relationship with service_provider_info and corporate_user_info
-- ============================================
DROP TABLE IF EXISTS `corporate_user_info_business_no` ;

CREATE TABLE IF NOT EXISTS `corporate_user_info_business_no` (
       `corporate_user_id` VARCHAR(30),
       `service_provider_type` VARCHAR(15),
       `tin` VARCHAR(30),
       `business_licence_number` VARCHAR(30),
       `mpaisa_account_number` VARCHAR(15),
       PRIMARY KEY (`corporate_user_id`)
) engine=innodb;
