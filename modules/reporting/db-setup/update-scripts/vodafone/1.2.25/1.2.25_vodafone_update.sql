-- ==================================================================================
-- Add new column to sdp_transaction_summary table to include event type of each transaction
-- ==================================================================================

ALTER TABLE `sdp_transaction_summary` ADD COLUMN `event_type` VARCHAR(20);

-- ==================================================================================
-- Add new column to sdp_transaction_summary table to include payment instrument of each transaction
-- ==================================================================================

ALTER TABLE `sdp_transaction_summary` ADD COLUMN `payment_instrument` VARCHAR(45);

-- ================================================================================================
-- Stored Procedure to update the transaction_summary
-- ================================================================================================

DELIMITER //

DROP PROCEDURE IF EXISTS populate_transaction_summary//
CREATE PROCEDURE populate_transaction_summary(IN requ_date DATE)

BEGIN
      DECLARE done INT DEFAULT 0;
      DECLARE requ_date_id INT;
      DECLARE c_date_id INT(10);
      DECLARE c_sp_id VARCHAR(32);
      DECLARE c_sp_name VARCHAR(100);
      DECLARE c_app_id VARCHAR(32);
      DECLARE c_app_name VARCHAR(100);
      DECLARE c_operator_id INT(1);
      DECLARE c_ncs_id INT(1);
      DECLARE c_direction VARCHAR(32);
      DECLARE c_event_type VARCHAR(32);
      DECLARE c_payment_instrument VARCHAR(32);
      DECLARE c_revenue_share_percentage DOUBLE;
      DECLARE c_sp_revenue DOUBLE;
      DECLARE c_operator_revenue DOUBLE;
      DECLARE c_total_revenue DOUBLE;
      DECLARE c_traffic INT(11);
      DECLARE c_charged_msg_count INT(11);
      DECLARE cur1 CURSOR FOR SELECT trx.date_id, trx.sp_id, trx.sp_name, trx.app_id, trx.app_name,
          trx.op_id, trx.ncs_id, trx.direction, trx.event_type, trx.payment_instrument, trx.revenue_share_percentage,
          trx.sp_revenue, trx.operator_revenue, trx.total_revenue, trx.traffic, trx.charged_msg_count
          FROM (SELECT dt.date_id AS date_id, sdp.sp_id AS sp_id, sdp.sp_name AS sp_name, sdp.app_id AS app_id, sdp.app_name AS app_name,
                  op.id AS op_id, ncs.id AS ncs_id, sdp.direction AS direction, sdp.event_type AS event_type, sdp.payment_instrument AS payment_instrument,
                  sdp.revenue_share_percentage AS revenue_share_percentage, SUM(sdp.sys_cur_sp_profit) AS sp_revenue, SUM(sdp.sys_cur_vc_profit) AS operator_revenue,
                  SUM(sdp.sys_cur_sp_profit + sdp.sys_cur_vc_profit) AS total_revenue, COUNT(*) AS traffic,
                  (CASE WHEN charging_type IS NOT NULL AND charging_type NOT IN('free','') THEN COUNT(*) ELSE 0 END) AS charged_msg_count
                FROM date_info dt, sdp_transaction sdp, operator op, ncs
                WHERE dt.full_date = DATE(sdp.time_stamp) AND dt.date_id = requ_date_id AND ncs.ncs_name = sdp.ncs AND sdp.response_code='S1000' AND sdp.file_type != 'cms'
                GROUP BY date_id, sp_id, app_id, op.id, ncs_id, direction) AS trx, system_app AS sys
          WHERE trx.app_id != sys.app_id OR trx.ncs_id != sys.ncs_id;

      DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

      SELECT date_id INTO requ_date_id FROM date_info WHERE full_date = requ_date;

      OPEN cur1;

      read_loop: LOOP
      FETCH cur1 INTO c_date_id, c_sp_id, c_sp_name, c_app_id, c_app_name, c_operator_id, c_ncs_id, c_direction,
        c_event_type, c_payment_instrument, c_revenue_share_percentage, c_sp_revenue,
        c_operator_revenue, c_total_revenue, c_traffic, c_charged_msg_count;

      if done THEN
        leave read_loop;
      END if;
      INSERT INTO `sdp_transaction_summary` (date_id, sp_id, sp_name, app_id, app_name, revenue_share_percentage,
      operator_id, ncs_id, direction, sp_revenue, operator_revenue, total_revenue, traffic, charged_msg_count, event_type, payment_instrument)
      VALUES(c_date_id, c_sp_id, c_sp_name, c_app_id, c_app_name, c_revenue_share_percentage, c_operator_id, c_ncs_id, c_direction,
      c_sp_revenue, c_operator_revenue, c_total_revenue, c_traffic, c_charged_msg_count, c_event_type, c_payment_instrument);

      END LOOP;

      CLOSE cur1;
      call populate_transaction_summary_downloadable_revenue(requ_date_id);

END//

DELIMITER ;
-- ===========================================================================================================
