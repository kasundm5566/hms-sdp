USE common_admin;

-- =========================================================================
-- Add new roles for new sort options in app revenue report generation page
-- =========================================================================

INSERT INTO `role` VALUES
  (624, NULL, NULL, 0, 'sdp app report sort option registrations', 'ROLE_SDP_RPT_APP_REVENUE_REG', 8),
  (625, NULL, NULL, 0, 'sdp app report sort option de-registrations', 'ROLE_SDP_RPT_APP_REVENUE_DEREG', 8),
  (627, NULL, NULL, 0, 'sdp app report sort option total subscribers', 'ROLE_SDP_RPT_APP_REVENUE_SUBSCRIBERS', 8);

INSERT INTO `user_group_roles` VALUES (1, 624), (1, 625), (1, 627), (3, 624), (3, 625), (3, 627);

