USE common_admin;

-- =========================================================================
-- Remove message history report permissions
-- =========================================================================

DELETE FROM user_group_roles WHERE roles=623;