USE reporting;

-- ==================================================================================
-- Add new column to pgw_transaction_summary table to include ncs of each transaction
-- ==================================================================================

ALTER TABLE `pgw_transaction_summary` ADD COLUMN `ncs` VARCHAR(32);

-- ==================================================================================
-- Add new field to pgw_transaction_summary composite primary key
-- ==================================================================================

ALTER TABLE pgw_transaction_summary
    DROP PRIMARY KEY,
    ADD PRIMARY KEY (date_id, payment_instrument, tx_type);

--  -- ===================================================
-- stored_procedure to update the  'pgw_transaction_summary
--  -- ===================================================

DELIMITER //

DROP PROCEDURE IF EXISTS populate_pgw_transaction_summary//
CREATE PROCEDURE `populate_pgw_transaction_summary`(IN requ_date date)
  BEGIN
    DECLARE no_more_records INT DEFAULT 0;
    DECLARE state VARCHAR(50);
    DECLARE summary_date_id INT(10);
    DECLARE summary_payment_instrument_type_id VARCHAR(32);
    DECLARE summary_tax_type VARCHAR(32);
    DECLARE summary_ncs VARCHAR(32);
    DECLARE summary_request_amount DOUBLE ;
    DECLARE summary_paid_amount DOUBLE;
    DECLARE summary_total_transactions INT;
    DECLARE summary_tax_amount FLOAT;
    DECLARE summary_service_charge_amount FLOAT;
    DECLARE cur_summary CURSOR FOR
      SELECT
        COUNT(*) AS total_transactions,
        t2.`date_id` AS dt_id,
        t1.`payment_instrument`,
        t1.`tx_type`,
        SUM(t1.`request_amount`),
        SUM(t1.`paid_amount`),
        SUM(t1.`tax_amount`),
        SUM(t1.`service_charge_amount`)
      FROM
        pgw_transaction t1,
        date_info t2
      WHERE
        date(t1.`timeStamp`) = t2.`full_date`
        AND date(t1.`timeStamp`) = `requ_date`
        AND (t1.`tx_status` = 'SUCCESS'
             OR t1.`tx_status` = 'AMOUNT_FULLY_PAID'
             OR t1.`tx_status` = 'PARTIAL_PAYMENT_SUCCESS'
             OR t1.`tx_status` = 'RESERVE_SUCCESS'
             OR t1.`tx_status` = 'RESERVE_COMMIT_SUCCESS'
             OR t1.`tx_status` = 'PENDING'
             OR t1.`tx_status` = 'PARTIAL_PENDING')
      GROUP BY dt_id , t1.`payment_instrument` , t1.`tx_type`;
    DECLARE CONTINUE HANDLER FOR NOT FOUND
    SET no_more_records = 1;
    OPEN cur_summary;
    read_loop : LOOP
      FETCH cur_summary INTO summary_total_transactions,summary_date_id,
        summary_payment_instrument_type_id,
        summary_tax_type,summary_request_amount, summary_paid_amount,summary_tax_amount,
        summary_service_charge_amount;
      IF no_more_records THEN
        LEAVE read_loop;
      END IF;
      INSERT INTO pgw_transaction_summary
      VALUES (summary_date_id,
              summary_request_amount,
              summary_total_transactions,
              summary_paid_amount-summary_tax_amount-summary_service_charge_amount,
              summary_paid_amount,summary_tax_type,
              summary_payment_instrument_type_id,
              summary_service_charge_amount,
              'null');
    END LOOP;
    CLOSE cur_summary;
  END//

DELIMITER ;
