DROP TABLE IF EXISTS `pgw_transaction_summary` ;

CREATE TABLE `pgw_transaction_summary` (
  `date_id` int(10) NOT NULL DEFAULT '0',
  `total_rquest_amount` double(10,4) DEFAULT '0.0000',
  `total_transactions` int(11) DEFAULT NULL,
  `total_service_pro_earning` double(10,4) DEFAULT NULL,
  `total_amount_paid` double(10,4) DEFAULT NULL,
  `tx_type` varchar(45) NOT NULL,
  `payment_instrument` varchar(45) NOT NULL,
  `income_received` double(10,4) DEFAULT '0.0000',
  PRIMARY KEY (`date_id`,`tx_type`,`payment_instrument`),
  KEY `ix_pgw_transaction_summary_date_id` (`date_id`)
) ENGINE=InnoDB;


DROP TABLE IF EXISTS `payment_instruments` ;

create table IF NOT EXISTS `payment_instruments` (
       `id` int,
       `name` varchar(100),
       primary key (`id`)
) engine=innodb;

DELIMITER //

DROP PROCEDURE IF EXISTS populate_pgw_transaction_summary//
CREATE PROCEDURE `populate_pgw_transaction_summary`(IN requ_date date)
BEGIN
       DECLARE no_more_records INT DEFAULT 0;
       DECLARE state VARCHAR(50);
       DECLARE summary_date_id INT(10);
       DECLARE summary_payment_instrument_type_id VARCHAR(32);
       DECLARE summary_tax_type VARCHAR(32);
       DECLARE summary_request_amount DOUBLE ;
       DECLARE summary_paid_amount DOUBLE;
       DECLARE summary_total_transactions INT;
       DECLARE summary_tax_amount FLOAT;
       DECLARE summary_service_charge_amount FLOAT;
       DECLARE cur_summary CURSOR FOR
       SELECT COUNT(*) AS total_transactions,t2.`date_id` AS dt_id,
            t1.`payment_instrument`,t1.`tx_type`,
            SUM(t1.`request_amount`),
            SUM(t1.`paid_amount`),
            SUM(t1.`tax_amount`),
            SUM(t1.`service_charge_amount`)
       FROM pgw_transaction t1, date_info t2
       WHERE date(t1.`timeStamp`)=t2.`full_date`
       AND date(t1.`timeStamp`) = `requ_date`
       AND (t1.`tx_status` = "SUCCESS"
       OR t1.`tx_status` = "AMOUNT_FULLY_PAID"
       OR t1.`tx_status` = "PARTIAL_PAYMENT_SUCCESS"
       OR t1.`tx_status` = "RESERVE_SUCCESS"
       OR t1.`tx_status` = "RESERVE_COMMIT_SUCCESS"
       OR t1.`tx_status` = "PENDING"
       OR t1.`tx_status` = "PARTIAL_PENDING")
       GROUP BY dt_id, t1.`payment_instrument`,t1.`tx_type`;
       DECLARE CONTINUE HANDLER FOR NOT FOUND
       SET no_more_records = 1;
       OPEN cur_summary;
       read_loop : LOOP
       FETCH cur_summary INTO summary_total_transactions,summary_date_id,
        summary_payment_instrument_type_id,
        summary_tax_type,summary_request_amount, summary_paid_amount,summary_tax_amount,
        summary_service_charge_amount;
       IF no_more_records THEN
          LEAVE read_loop;
       END IF;
       INSERT INTO pgw_transaction_summary
       VALUES (summary_date_id,
        summary_request_amount,
        summary_total_transactions,
        summary_paid_amount-summary_tax_amount-summary_service_charge_amount,
        summary_paid_amount,summary_tax_type,
        summary_payment_instrument_type_id,
        summary_service_charge_amount);
       END LOOP;
       CLOSE cur_summary;
END//

DELIMITER ;

INSERT INTO `summary_data_status` (`procedure_name`, `requested_date`, `start_date_time`, `end_date_time`, `status`)
 VALUES
('populate_pgw_transaction_summary','2013-08-01','2013-08-01 10:00:00',NULL,1),
('populate_pgw_transaction_summary','2013-08-01',NULL,'2013-08-01 10:00:00',2);


INSERT INTO `payment_instruments` VALUES (1,'Mobile Account'), (2,'M-PAiSA');

alter table pgw_transaction change reference_id reference_id VARCHAR(50);
