USE reporting;

DELIMITER //
DROP PROCEDURE IF EXISTS populate_subscription_summary//
CREATE PROCEDURE populate_subscription_summary(IN requ_date date)


BEGIN

DECLARE done INT default 0;

DECLARE requ_date_id INT;

DECLARE c_date_id INT(10);
DECLARE c_sp_id VARCHAR(32);
DECLARE c_sp_name VARCHAR(100);
DECLARE c_app_id VARCHAR(32);
DECLARE c_app_name VARCHAR(100);
DECLARE c_new_reg INT(11);
DECLARE c_dreg INT(11);


DECLARE cur1 CURSOR FOR SELECT din.date_id,t.sp_id,t.sp_name,t.app_id,t.app_name,
(CASE WHEN (t.event_type='freeRegistration' OR t.event_type='registrationCharging') AND t.response_code='S1000' THEN COUNT(*) ELSE 0 END) AS new_reg,
(CASE WHEN t.event_type='unregistration' AND t.response_code='S1000' THEN COUNT(*) ELSE 0 END) AS dereg
FROM sdp_transaction AS t,date_info AS din
where (t.event_type='freeRegistration' OR t.event_type='registrationCharging' OR t.event_type='unregistration') AND
din.date_id=requ_date_id AND din.full_date = DATE(t.time_stamp) AND t.response_code='S1000' GROUP BY t.app_id,din.date_id,t.event_type;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

SELECT date_id INTO requ_date_id FROM date_info WHERE full_date = requ_date;

OPEN cur1;

read_loop: LOOP
FETCH cur1 INTO c_date_id,c_sp_id,c_sp_name,c_app_id,c_app_name,c_new_reg,c_dreg;

if done THEN
	leave read_loop;
END if;
INSERT INTO `sdp_subscription_summary` (`date_id`,`sp_id`,`sp_name`,`app_id`,`app_name`,`new_reg`,`dereg`)
VALUES(c_date_id,c_sp_id,c_sp_name,c_app_id,c_app_name,c_new_reg,c_dreg)
ON DUPLICATE KEY UPDATE new_reg = VALUES(new_reg) + new_reg, dereg = VALUES(dereg) + dereg;

END LOOP;

CLOSE cur1;
END //

DELIMITER ;