USE reporting;

DROP TABLE IF EXISTS subscription_info;
CREATE TABLE subscription_info(
  time_stamp TIMESTAMP ,
  app_id VARCHAR (15),
  sp_id VARCHAR(15),
  msisdn VARCHAR (15),
  subscription_status VARCHAR (20),
  PRIMARY KEY (app_id, msisdn)
);