delimiter //

-- populate_filed_summary procedure consider P1500 status code also as a fail transaction.
-- Therefore change the procedure to omit transaction with response code as P1500.

DROP PROCEDURE IF EXISTS populate_failed_summary//
CREATE PROCEDURE populate_failed_summary(IN requested_date DATE)
       BEGIN

       DECLARE no_more_records INT DEFAULT 0;
       DECLARE summary_date_id INT(10);
       DECLARE summary_sp_id VARCHAR(32);
       DECLARE summary_app_id VARCHAR(32);
       DECLARE summary_error_code VARCHAR(10);
       DECLARE summary_ncs_id INT(1);
       DECLARE summary_direction VARCHAR(2);
       DECLARE summary_error_count INT;

       DECLARE  cur_failed_summary CURSOR FOR
       SELECT  t2.`date_id` dt_id, t1.`sp_id`, t1.`app_id`, t1.`response_code`, t3.`id`, t1.`direction`, count(t1.`time_stamp`) frequency
       FROM `sdp_transaction` t1, `date_info` t2, `ncs` t3
       WHERE t2.`full_date` = date(t1.`time_stamp`)
       AND t3.`ncs_name` = t1.`ncs`
       AND t1.`response_code` <> 'S1000'
       AND t1.`response_code` <> 'P1500'
       AND date(t1.`time_stamp`) = requested_date
       GROUP BY dt_id, t1.`sp_id`, t1.`app_id`, t1.`response_code`, t1.`ncs`, t1.`direction`;

       DECLARE  CONTINUE HANDLER FOR NOT FOUND
       SET  no_more_records = 1;

       OPEN cur_failed_summary;

       read_loop : LOOP
       FETCH  cur_failed_summary INTO summary_date_id, summary_sp_id, summary_app_id, summary_error_code, summary_ncs_id, summary_direction, summary_error_count;

       IF no_more_records THEN
          LEAVE read_loop;
       END IF;
       INSERT INTO `sdp_failed_summary`
       VALUES (summary_date_id, summary_sp_id, summary_app_id, summary_error_code, summary_ncs_id, summary_direction, summary_error_count );
       END LOOP;

       CLOSE  cur_failed_summary;
       END //

delimiter ;