INSERT INTO ncs VALUES (1, 'sms');
INSERT INTO ncs VALUES (2, 'ussd');
INSERT INTO ncs VALUES (3, 'wap-push');
INSERT INTO ncs VALUES (4, 'cas');
INSERT INTO ncs VALUES (5, 'lbs');
INSERT INTO ncs VALUES (6, 'subscription');
INSERT INTO ncs VALUES (7, 'downloadable');

INSERT INTO operator VALUES (1, 'vodafone');

INSERT INTO `summary_data_status` VALUES (1,'populate_transaction_summary','2011-07-31','2011-07-31 10:00:00',NULL,1),
(2,'populate_transaction_summary','2011-07-31',NULL,'2011-07-31 10:00:00',2),
(3,'populate_failed_summary','2011-07-31','2011-07-31 10:00:00',NULL,1),
(4,'populate_failed_summary','2011-07-31',NULL,'2011-07-31 10:00:00',2),
(5,'populate_add_summary','2011-07-31','2011-07-31 10:00:00',NULL,1),
(6,'populate_add_summary','2011-07-31',NULL,'2011-07-31 10:00:00',2),
(7,'populate_sdp_reconciliation_summary','2011-07-31','2011-07-31 10:00:00',NULL,1),
(8,'populate_sdp_reconciliation_summary','2011-07-31',NULL,'2011-07-31 10:00:00',2),
(11,'populate_pgw_transaction_summary','2011-07-31','2011-07-31 10:00:00',NULL,1),
(12,'populate_pgw_transaction_summary','2011-07-31',NULL,'2011-07-31 10:00:00',2),
(13,'populate_subscription_summary','2011-07-31','2011-07-31 10:00:00',NULL,1),
(14,'populate_subscription_summary','2011-07-31',NULL,'2011-07-31 10:00:00',2);

INSERT INTO `system_app` VALUES ('APP_1000002', 4);

INSERT INTO `payment_instruments` VALUES (1, 'Mobile Account'), (2, 'M-PAiSA');


