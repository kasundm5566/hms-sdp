-- #############################################################
-- ============== table summary ===============

-- ......dimension tables
-- [03] create table: date_info
-- [04] create table: operator
-- [05] create table: ncs
-- [06] create table: service_provider_info
-- [07] create table: summary_data_status
-- [08] create table: payment_instrument_type
-- [09] create table: advertisement
-- [10] create table: governance_detail
-- [11] create table: log_info


-- ......summary tables
-- [01] create table: sdp_transaction_summary
-- [02] create table: sdp_failed_summary
-- [03] create table: add_summary
-- [04] create table: pgw_transaction_summary
-- [05] create table: pgw_income_summary
-- [06] create table: sdp_reconciliation_summary

-- ......fact tables
-- [01] create table: sdp_transaction
-- [02] create table: pgw_transaction

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

-- ============================================
-- [03] create table: date_info
-- ============================================
DROP TABLE IF EXISTS `date_info` ;

CREATE TABLE IF NOT EXISTS `date_info` (
  `date_id` INT(11) NULL DEFAULT NULL,
  `year` INT(5) NULL DEFAULT NULL,
  `month` INT(2) NULL DEFAULT NULL,
  `month_name` VARCHAR(51) NULL DEFAULT NULL,
  `day_of_month` INT(2) NULL DEFAULT NULL,
  `day_of_week` INT(1) NULL DEFAULT NULL,
  `week_of_year` INT(2) NULL DEFAULT NULL,
  `full_date` DATE NULL DEFAULT NULL,
  PRIMARY KEY (date_id),
  UNIQUE KEY `ix_date_info_full_date` (`full_date`)
)  ENGINE=InnoDB;

-- ============================================
-- [04] create table: operator
-- operator_name --> Safaricom/Orange/Airtel
-- ============================================
DROP TABLE IF EXISTS `operator` ;

CREATE TABLE IF NOT EXISTS `operator` (
  `id` int(1),
  `operator_name` varchar(12),
  PRIMARY KEY (`id`)
)  ENGINE=innodb;


-- ============================================
-- [04] create table: operator
-- operator_name --> Safaricom/Orange/Airtel
-- ============================================
DROP TABLE IF EXISTS `system_app` ;
CREATE TABLE IF NOT EXISTS `system_app` (
  `app_id` VARCHAR(32),
  `ncs_id` int(1) NOT NULL DEFAULT 0,
  UNIQUE KEY (`app_id` , `ncs_id`)
)  ENGINE=innodb;

-- ============================================
-- [05] create table: ncs
-- ncs_name --> sms/cas/push/ussd
-- ============================================
DROP TABLE IF EXISTS `ncs` ;

CREATE TABLE IF NOT EXISTS `ncs` (
  `id` int(1),
  `ncs_name` varchar(15),
  PRIMARY KEY (`id`)
)  ENGINE=innodb;


-- ============================================
-- [06] create table: service_provider_info
-- Populated from the CSV logs from provisioning
-- ============================================
DROP TABLE IF EXISTS `service_provider_info` ;

CREATE TABLE IF NOT EXISTS `service_provider_info` (
  `sp_id` varchar(32),
  `sp_name` varchar(100),
  `corporate_user_id` varchar(100),
  `app_id` varchar(32),
  `app_name` varchar(100),
  `app_catogory` varchar(8),
  `revenue_share_type` varchar(100),
  `revenue_share_percentage` double(10 , 4 ),
  `app_state` varchar(30),
  `created_time` datetime,
  UNIQUE KEY (`sp_id` , `app_id`),
  KEY `idx_app_id` (`app_id`)
)  ENGINE=innodb;

-- ============================================
-- [07] create table: summary_data_status
-- status --> 1- STARTED, 2- COMPLETE
-- ============================================
DROP TABLE IF EXISTS `summary_data_status` ;

CREATE TABLE IF NOT EXISTS `summary_data_status` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `procedure_name` VARCHAR(50) NOT NULL,
  `requested_date` DATE NULL,
  `start_date_time` DATETIME NULL,
  `end_date_time` DATETIME NULL,
  `status` INT(1) NULL,
  PRIMARY KEY (`id`)
)  ENGINE=InnoDB;

-- ============================================
-- [08] create table: payment_instruments
-- eg: PesaPal, M-Pesa, Mobile-Wallet, ABC Bank
-- ============================================
DROP TABLE IF EXISTS `payment_instruments` ;

CREATE TABLE IF NOT EXISTS `payment_instruments` (
  `id` int,
  `name` varchar(100),
  PRIMARY KEY (`id`)
)  ENGINE=innodb;

-- ============================================
-- [09] create table: advertisement
-- populated from the CSV file got it from Admin-Advertisement module
-- ============================================
DROP TABLE IF EXISTS `advertisement` ;

CREATE TABLE IF NOT EXISTS `advertisement` (
  `name` varchar(40),
  `content` varchar(100),
  PRIMARY KEY (`name`)
)  ENGINE=innodb;

-- ============================================
-- [10] create table: governance
-- populated from the CSV file
-- ============================================
DROP TABLE IF EXISTS `governance_detail` ;

CREATE TABLE IF NOT EXISTS `governance_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `application_id` VARCHAR(32),
  `application_name` VARCHAR(100),
  `message` VARCHAR(160),
  `requested_date` DATETIME,
  `status` VARCHAR(10),
  `date_of_action` DATETIME,
  `action_by` VARCHAR(30),
  `number_of_recipients` INT,
  PRIMARY KEY (`id`)
)  ENGINE=innodb;

-- ============================================
-- [11] create table: log_info
-- populated when log files inserted into database
-- ============================================
DROP TABLE IF EXISTS `log_info` ;

CREATE TABLE IF NOT EXISTS `log_info` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `file_name` varchar(50) UNIQUE,
  `processed_time` DATETIME,
  `file_type` varchar(10),
  PRIMARY KEY (`id`)
)  ENGINE=InnoDB;

-- ============================================
-- [12] create table: corporate_user_info
-- Populated from the CSV logs from common_registration module
-- corporate_user_id --> This should match with the service_provider_info.corporate_user_id and
-- its having 1:1 relationship with service_provider_info and corporate_user_info
-- ============================================
DROP TABLE IF EXISTS `corporate_user_info` ;

CREATE TABLE IF NOT EXISTS `corporate_user_info` (
  `corporate_user_id` varchar(100),
  `sp_beneficiary` text,
  `sp_beneficiary_name` varchar(100),
  `sp_beneficiary_bank_code` varchar(100),
  `sp_beneficiary_branch_code` varchar(100),
  `sp_beneficiary_branch_name` varchar(100),
  `sp_beneficiary_bank_account_number` varchar(100),
  PRIMARY KEY (`corporate_user_id`)
)  ENGINE=innodb;


-- ============================================
-- [12] create table: corporate_user_info_business_no
-- Populated from the CSV logs from common_registration module
-- corporate_user_id --> This should match with the service_provider_info.corporate_user_id and
-- its having 1:1 relationship with service_provider_info and corporate_user_info
-- ============================================
DROP TABLE IF EXISTS `corporate_user_info_business_no` ;

CREATE TABLE IF NOT EXISTS `corporate_user_info_business_no` (
  `corporate_user_id` VARCHAR(30),
  `service_provider_type` VARCHAR(15),
  `tin` VARCHAR(30),
  `business_licence_number` VARCHAR(30),
  `mpaisa_account_number` VARCHAR(15),
  PRIMARY KEY (`corporate_user_id`)
)  ENGINE=innodb;



-- End of Dimension tables

-- ============================================
-- [01] create table: sdp_transaction_summary
-- date_id = from date_info.date_id where sdp_transaction.time_stamp = date_info.full_date
-- operator_id = from operator.id where sdp_transaction.operator = operator.operator_name
-- ncs_id = from ncs.id where sdp_transaction.ncs = ncs.ncs_name
-- revenue = sum of sys_cur_charge_amount for the given date_id,sp_id, app_id, operator_id, ncs_id, direction where transaction_status = 'success'
-- traffic = count of transactions for the given date_id,sp_id, app_id, operator_id, ncs_id, direction where transaction_status = 'success'
-- ============================================
DROP TABLE IF EXISTS `sdp_transaction_summary` ;

CREATE TABLE IF NOT EXISTS `sdp_transaction_summary` (
  `date_id` int(10),
  `sp_id` varchar(32),
  `sp_name` varchar(100),
  `app_id` varchar(32),
  `app_name` varchar(100),
  `revenue_share_percentage` double(10 , 4 ) default 0.0,
  `operator_id` int(1),
  `ncs_id` int(1),
  `direction` varchar(32),
  `sp_revenue` double(10 , 4 ) default 0.0,
  `operator_revenue` double(10 , 4 ) default 0.0,
  `total_revenue` double(10 , 4 ) default 0.0,
  `traffic` int default 0,
  `charged_msg_count` int(11) DEFAULT 0,
  `event_type` varchar(32),
  `payment_instrument` varchar(45),
  `charging_type` varchar(30),
  `unit_charge_price` double,
  PRIMARY KEY (date_id , sp_id , app_id , operator_id , ncs_id , direction)
)  ENGINE=innodb;


-- ============================================
-- [02] create table: sdp_failed_summary
-- date_id = from date_info.date_id where sdp_transaction.time_stamp = date_info.full_date
-- ncs_id = from ncs.id where sdp_transaction.ncs = ncs.ncs_name
-- error_count = count the number of transactions for the given date_id,sp_id, app_id, ncs_id, direction where transaction_status != 'success'
-- ============================================
DROP TABLE IF EXISTS `sdp_failed_summary` ;

CREATE TABLE IF NOT EXISTS `sdp_failed_summary` (
  `date_id` int(10),
  `sp_id` varchar(32),
  `app_id` varchar(32),
  `error_code` varchar(10),
  `ncs_id` int(1),
  `direction` varchar(32),
  `error_count` int default 0
)  ENGINE=innodb;


-- ============================================
-- [03] create table: add_summary
-- date_id = from date_info.date_id where sdp_transaction.time_stamp = date_info.full_date
-- print_count = count the number of transactions for the given date_id,sp_id, app_id, add_id where transaction_status = 'success'
-- ============================================
DROP TABLE IF EXISTS `add_summary` ;

CREATE TABLE IF NOT EXISTS `add_summary` (
  `date_id` int(10),
  `sp_id` varchar(32),
  `app_id` varchar(32),
  `add_name` varchar(40),
  `print_count` int default 0
)  ENGINE=innodb;

-- ============================================
-- [04] create table: pgw_transaction_summary
-- date_id = from date_info.date_id where pgw_transaction.time_stamp = date_info.full_date
-- total_credit_value =
-- total_credit_transactions =
-- total_debit_value =
-- total_debit_transactions =
-- inflow =
-- ============================================
DROP TABLE IF EXISTS `pgw_transaction_summary` ;

CREATE TABLE `pgw_transaction_summary` (
  `date_id` int(10) NOT NULL DEFAULT '0',
  `total_rquest_amount` double(10 , 4 ) DEFAULT '0.0000',
  `total_transactions` int(11) DEFAULT NULL,
  `total_service_pro_earning` double(10 , 4 ) DEFAULT NULL,
  `total_amount_paid` double(10 , 4 ) DEFAULT NULL,
  `tx_type` varchar(45) NOT NULL,
  `payment_instrument` varchar(45) NOT NULL,
  `income_received` double(10 , 4 ) DEFAULT '0.0000',
  PRIMARY KEY (`date_id` , `tx_type` , `payment_instrument`),
  KEY `ix_pgw_transaction_summary_date_id` (`date_id`)
)  ENGINE=InnoDB;


-- ============================================
-- [05] create TABLE: pgw_income_summary
-- date_id = from date_info.date_id where pgw_transaction.time_stamp = date_info.full_date
-- payment_instrument =
-- amount =
-- ============================================
DROP TABLE IF EXISTS `pgw_income_summary` ;

CREATE TABLE IF NOT EXISTS `pgw_income_summary` (
  `date_id` int(10),
  `payment_instrument` int,
  `amount` double(10 , 4 ) DEFAULT '0.0000',
  PRIMARY KEY (`date_id` , `payment_instrument`)
)  ENGINE=innodb;

-- ============================================
-- [06] create table: sdp_reconciliation_summary
-- date_id = from date_info.date_id where sdp_transaction.time_stamp = date_info.full_date
-- msg_rev_sp_profit = sum of sys_cur_sp_profit for given date_id, sp_id, app_id where transaction_status = 'success' and ncs != 'cas'
-- msg_rev_vc_profit = sum of sys_cur_vc_profit for given date_id, sp_id, app_id where transaction_status = 'success' and ncs != 'cas'
-- msg_rev_op_cost = sum of sys_cur_operator_cost for given date_id, sp_id, app_id where transaction_status = 'success' and ncs != 'cas'
-- cas_rev_sp_profit = sum of sys_cur_charge_amount for given date_id, sp_id, app_id where transaction_status = 'success' and ncs = 'cas'
-- app_total_rev = msg_rev_sp_profit + msg_rev_vc_profit + msg_rev_op_cost + cas_rev_sp_profit
-- ============================================
DROP TABLE IF EXISTS `sdp_reconciliation_summary` ;

CREATE TABLE IF NOT EXISTS `sdp_reconciliation_summary` (
  `date_id` int(10),
  `sp_id` varchar(32),
  `app_id` varchar(32),
  `msg_rev_sp_profit` double(10 , 4 ) default 0.0,
  `msg_rev_vc_profit` double(10 , 4 ) default 0.0,
  `msg_rev_op_cost` double(10 , 4 ) default 0.0,
  `cas_rev_sp_profit` double(10 , 4 ) default 0.0,
  `app_total_rev` double(10 , 4 ) default 0.0
)  ENGINE=innodb;


-- ============================================
-- create table: pgw_repayment_summary
-- date_id = from date_info.date_id where pgw_transaction.time_stamp = date_info.full_date
-- payment_instrument =
-- msisdn =
-- payments=
-- tx_type =
-- frm_payment_instrument =
-- ============================================
DROP TABLE IF EXISTS `pgw_repayment_summary`;

CREATE TABLE `pgw_repayment_summary` (
  `date_id` int(11) NOT NULL,
  `msisdn` varchar(45) DEFAULT NULL,
  `payments` int(11) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `payment_instrument` int(11) DEFAULT NULL,
  PRIMARY KEY (`date_id` , `payment_instrument` , `msisdn`)
)  ENGINE=InnoDB;

-- ============================================
-- create table: pgw_repayment_summary
-- ==========================================
DROP TABLE IF EXISTS `pgw_application_summary`;

CREATE TABLE `pgw_application_summary` (
  `date_id` int(10) NOT NULL DEFAULT '0',
  `total_rquest_amount` double(10 , 4 ) DEFAULT '0.0000',
  `total_transactions` int(11) DEFAULT NULL,
  `total_service_pro_earning` double(10 , 4 ) DEFAULT NULL,
  `total_amount_paid` double(10 , 4 ) DEFAULT NULL,
  `tx_type` varchar(45) NOT NULL,
  `payment_instrument` varchar(45) NOT NULL,
  `app_id` varchar(45) NOT NULL,
  `app_name` varchar(30) DEFAULT NULL,
  `income_received` double(10 , 4 ) DEFAULT '0.0000',
  PRIMARY KEY (`date_id` , `tx_type` , `payment_instrument` , `app_id`),
  KEY `ix_pgw_transaction_summary_date_id` (`date_id`)
)  ENGINE=InnoDB;
-- End of Summary tables
-- ============================================
-- create table: sdp_subscription_summary
-- ==========================================
DROP TABLE IF EXISTS `sdp_subscription_summary`;

CREATE TABLE `sdp_subscription_summary` (
  `date_id` int(10) NOT NULL DEFAULT '0',
  `app_id` varchar(45) NOT NULL,
  `app_name` varchar(30) DEFAULT NULL,
  `sp_id` varchar(32),
  `sp_name` varchar(100),
  `new_reg` int(11),
  `dereg` INT(11),
  `total_subscribers` int(11),
  PRIMARY KEY (`date_id` , `app_id`)
)  ENGINE=InnoDB;

-- End of Summary tables

-- ============================================
-- [02] create table: pgw_transaction
-- sys_cur_request_amount = request_amount * request_amount_buying_rate(of request_currency)
-- request_amount_buying_rate = Extracted from rate_card with respect to request_currency
-- request_amount_selling_rate = Extracted from rate_card with respect to request_currency

-- sys_cur_from_amount = from_amount * from_buying_rate(of from_currency)
-- from_buying_rate = Extracted from rate_card with respect to from_currency
-- from_selling_rate = Extracted from rate_card with respect to from_currency

-- sys_cur_to_amount = to_amount * to_buying_rate(of to_currency)
-- to_buying_rate = Extracted from rate_card with respect to to_currency
-- to_selling_rate = Extracted from rate_card with respect to to_currency

-- sys_cur_service_charge_amount = service_charge_amount * service_charge_amount_buying_rate(of to_currency)
-- service_charge_amount_buying_rate = Extracted from rate_card with respect to service_charge_currency
-- service_charge_amount_selling_rate = Extracted from rate_card with respect to service_charge_currency

-- sys_cur_currency_conversion_amount = currency_conversion_amount * currency_conversion_amount_buying_rate(of to_currency)
-- currency_conversion_amount_buying_rate = Extracted from rate_card with respect to currency_conversion_currency
-- currency_conversion_amount_selling_rate = Extracted from rate_card with respect to currency_conversion_currency
-- ============================================
DROP TABLE IF EXISTS `pgw_transaction` ;

CREATE TABLE IF NOT EXISTS `pgw_transaction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_id` varchar(30) DEFAULT NULL,
  `tx_type` varchar(30) DEFAULT NULL,
  `timeStamp` timestamp NULL DEFAULT NULL,
  `channel` varchar(10) DEFAULT NULL,
  `tx_status` varchar(40) DEFAULT NULL,
  `tx_status_details` varchar(50) DEFAULT NULL,
  `client_tx_id` varchar(50) DEFAULT NULL,
  `request_amount` float DEFAULT NULL,
  `request_currency` varchar(3) DEFAULT NULL,
  `tax_type` varchar(10) DEFAULT NULL,
  `tax_percentage` varchar(10) DEFAULT NULL,
  `tax_amount` float DEFAULT NULL,
  `event_type` varchar(20) DEFAULT NULL,
  `paid_amount` float DEFAULT NULL,
  `pending_amount` float DEFAULT NULL,
  `service_charge_type` varchar(10) DEFAULT NULL,
  `service_charge_percentage` varchar(10) DEFAULT NULL,
  `service_charge_amount` float DEFAULT NULL,
  `merchant_id` varchar(30) DEFAULT NULL,
  `merchant_name` varchar(30) DEFAULT NULL,
  `user_name` varchar(30) DEFAULT NULL,
  `line_items` text,
  `payment_instrument` varchar(45) DEFAULT NULL,
  `payment_instrument_account` varchar(30) DEFAULT NULL,
  `category` varchar(30) DEFAULT NULL,
  `invoice_no` varchar(30) DEFAULT NULL,
  `order_no` varchar(30) DEFAULT NULL,
  `rate_card` text,
  `reference_id` varchar(50) DEFAULT NULL,
  `total_paid_amount` float DEFAULT NULL,
  `msisdn` varchar(30) NOT NULL,
  `paybill_no` varchar(10),
  PRIMARY KEY (`id` , `msisdn`)
)  ENGINE=InnoDB AUTO_INCREMENT=23;

-- ============================================
-- [01] create table: sdp_transaction
-- sys_cur_charge_amount = charge_amount * charge_amount_buying_rate(of charge_amount_currency)
-- charge_amount_buying_rate = Extracted from rate_card with respect to charge_amount_currency
-- charge_amount_selling_rate = Extracted from rate_card with respect to charge_amount_currency

-- sys_cur_sp_profit = sys_cur_charge_amount * revenue_share_percentage where [except for ncs=cas]
-- sys_cur_vc_profit = sys_cur_charge_amount * (100% - revenue_share_percentage) [except for ncs=cas]
-- ============================================
DROP TABLE IF EXISTS `sdp_transaction` ;

CREATE TABLE IF NOT EXISTS `sdp_transaction` (
  `correlation_id` varchar(36) not null,
  `time_stamp` datetime,
  `sp_id` varchar(32),
  `sp_name` varchar(100),
  `app_id` varchar(32),
  `app_name` varchar(100),
  `app_state` varchar(32),
  `source_address` varchar(256),
  `masked_source_address` varchar(256),
  `source_channel_type` varchar(32),
  `source_channel_protocol` varchar(32),
  `destination_address` varchar(256),
  `masked_destination_address` varchar(256),
  `destination_channel_type` varchar(32),
  `destination_channel_protocol` varchar(32),
  `direction` varchar(32),
  `ncs` varchar(12),
  `billing_type` varchar(8),
  `charge_party_type` varchar(32),
  `charge_amount` double(10 , 4 ) default 0.0,
  `sys_cur_charge_amount` double(10 , 4 ) default 0.0,
  `charge_amount_currency` varchar(3),
  `charge_amount_buying_rate` double(10 , 4 ) default 0.0,
  `charge_amount_selling_rate` double(10 , 4 ) default 0.0,
  `rate_card` text,
  `charge_category` varchar(30),
  `charge_address` varchar(256),
  `masked_charge_address` varchar(256),
  `event_type` varchar(33),
  `response_code` varchar(16),
  `response_description` varchar(200),
  `transaction_status` varchar(15),
  `keyword` varchar(20),
  `short_code` varchar(12),
  `operator` varchar(12),
  `sys_cur_operator_cost` double(10 , 4 ) default 0.0,
  `revenue_share_type` varchar(50),
  `revenue_share_percentage` double(10 , 4 ) default 0.0,
  `sys_cur_sp_profit` double(10 , 4 ) default 0.0,
  `sys_cur_vc_profit` double(10 , 4 ) default 0.0,
  `advertisement_name` varchar(40),
  `advertisement_content` varchar(100),
  `is_multiple_recipients` int(1) default null,
  `overall_response_code` varchar(16),
  `overall_response_description` varchar(200),
  `pgw_transaction_id` varchar(36),
  `charging_type` varchar(30),
  `is_number_masked` int(1) default null,
  `order_no` varchar(50),
  `invoice_no` varchar(50),
  `external_trx_id` varchar(50),
  `session_id` varchar(20),
  `ussd_operation` varchar(20),
  `balance_due` double(10 , 4 ),
  `total_amount` double(10 , 4 ),
  `file_type` varchar(10),
  `payment_instrument` varchar(45)
)  ENGINE=innodb
  PARTITION BY RANGE ( TO_DAYS(`time_stamp`) )(
    PARTITION par_dec_2012 VALUES LESS THAN (TO_DAYS('2012-12-31')) ENGINE = InnoDB,
    PARTITION par_jan_2013 VALUES LESS THAN (TO_DAYS('2013-01-31')) ENGINE = InnoDB,
    PARTITION par_feb_2013 VALUES LESS THAN (TO_DAYS('2013-02-28')) ENGINE = InnoDB,
    PARTITION par_mar_2013 VALUES LESS THAN (TO_DAYS('2013-03-31')) ENGINE = InnoDB,
    PARTITION par_apr_2013 VALUES LESS THAN (TO_DAYS('2013-04-30')) ENGINE = InnoDB,
    PARTITION par_may_2013 VALUES LESS THAN (TO_DAYS('2013-05-31')) ENGINE = InnoDB,
    PARTITION par_jun_2013 VALUES LESS THAN (TO_DAYS('2013-06-30')) ENGINE = InnoDB,
    PARTITION par_jul_2013 VALUES LESS THAN (TO_DAYS('2013-07-31')) ENGINE = InnoDB,
    PARTITION par_aug_2013 VALUES LESS THAN (TO_DAYS('2013-08-31')) ENGINE = InnoDB,
    PARTITION par_sep_2013 VALUES LESS THAN (TO_DAYS('2013-09-30')) ENGINE = InnoDB,
    PARTITION par_oct_2013 VALUES LESS THAN (TO_DAYS('2013-10-31')) ENGINE = InnoDB,
    PARTITION par_nov_2013 VALUES LESS THAN (TO_DAYS('2013-11-30')) ENGINE = InnoDB,
    PARTITION par_dec_2013 VALUES LESS THAN (TO_DAYS('2013-12-31')) ENGINE = InnoDB,
    PARTITION par_jan_2014 VALUES LESS THAN (TO_DAYS('2014-01-31')) ENGINE = InnoDB,
    PARTITION par_feb_2014 VALUES LESS THAN (TO_DAYS('2014-02-28')) ENGINE = InnoDB,
    PARTITION par_mar_2014 VALUES LESS THAN (TO_DAYS('2014-03-31')) ENGINE = InnoDB,
    PARTITION par_apr_2014 VALUES LESS THAN (TO_DAYS('2014-04-30')) ENGINE = InnoDB,
    PARTITION par_may_2014 VALUES LESS THAN (TO_DAYS('2014-05-31')) ENGINE = InnoDB,
    PARTITION par_jun_2014 VALUES LESS THAN (TO_DAYS('2014-06-30')) ENGINE = InnoDB,
    PARTITION par_jul_2014 VALUES LESS THAN (TO_DAYS('2014-07-31')) ENGINE = InnoDB,
    PARTITION par_aug_2014 VALUES LESS THAN (TO_DAYS('2014-08-31')) ENGINE = InnoDB,
    PARTITION par_sep_2014 VALUES LESS THAN (TO_DAYS('2014-09-30')) ENGINE = InnoDB,
    PARTITION par_oct_2014 VALUES LESS THAN (TO_DAYS('2014-10-31')) ENGINE = InnoDB,
    PARTITION par_nov_2014 VALUES LESS THAN (TO_DAYS('2014-11-30')) ENGINE = InnoDB,
    PARTITION par_dec_2014 VALUES LESS THAN (TO_DAYS('2014-12-31')) ENGINE = InnoDB,
    PARTITION par_monthly_rest VALUES LESS THAN MAXVALUE ENGINE = InnoDB
  );

-- ============================================
-- [01] create table: sp_pi_info
-- ============================================

DROP TABLE IF EXISTS `sp_payment_instrument_info`;

CREATE TABLE IF NOT EXISTS `sp_payment_instrument_info` (
  `sp_id` VARCHAR(32) DEFAULT NULL,
  `app_id` VARCHAR(32) DEFAULT NULL,
  `ncs` VARCHAR(20) DEFAULT NULL,
  `payment_instrument` VARCHAR(20) DEFAULT NULL,
  `pi_additional_info` VARCHAR(20) DEFAULT NULL
)  ENGINE=innodb;


-- ===================================================
-- [02] create table: ignore_payment_instrument
-- ====================================================

DROP TABLE IF EXISTS `ignore_payment_instrument`;

CREATE TABLE IF NOT EXISTS ignore_payment_instrument (
  `id` INT(1),
  `payment_instrument` VARCHAR(30)
)  ENGINE=InnoDB;


-- End of Fact tables

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- alter statements
-- alter table sdp_transaction alter time_stamp set default '0000-00-00 00:00:00.0'
