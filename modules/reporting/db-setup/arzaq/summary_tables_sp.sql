DELIMITER //
-- ===========================================================================================================

/* This procedure use for execute sql queries */
drop procedure if exists queryExecutor //
create procedure queryExecutor(query varchar(4500))

begin

select query;
set @tmp_sql=query;
prepare statement1 from @tmp_sql;
execute statement1;
deallocate prepare statement1;

end//

-- ===================================================
-- stored_procudure for update the transaction_summary
-- ===================================================
DROP PROCEDURE IF EXISTS populate_transaction_summary//
CREATE PROCEDURE populate_transaction_summary(IN requ_date date)

begin

declare done int default 0;

declare requ_date_id int;

declare c_date_id int(10);
declare c_sp_id varchar(32);
declare c_sp_name varchar(100);
declare c_app_id varchar(32);
declare c_app_name varchar(100);
declare c_operator_id int(1);
declare c_ncs_id int(1);
declare c_direction varchar(32);
declare c_revenue_share_percentage double;
declare c_sp_revenue double;
declare c_operator_revenue double;
declare c_total_revenue double;
declare c_traffic int(11);
declare c_charged_msg_count int(11);

declare cur1 cursor for select trx.date_id, trx.sp_id, trx.sp_name, trx.app_id, trx.app_name, trx.op_id, trx.ncs_id,
    trx.direction, trx.revenue_share_percentage,
    trx.sp_revenue,trx.operator_revenue,
    trx.total_revenue,trx.Traffic,
    trx.charged_msg_count from (select din.date_id as date_id, t.sp_id as sp_id, t.sp_name as sp_name, t.app_id as app_id,
     t.app_name as app_name, op.id as op_id, ncs.id as ncs_id, t.direction as direction, t.revenue_share_percentage as revenue_share_percentage,
    sum(t.sys_cur_sp_profit) as sp_revenue,sum(t.sys_cur_vc_profit) as operator_revenue,
    sum(t.sys_cur_sp_profit + t.sys_cur_vc_profit) as total_revenue,count(*) as Traffic,
    (CASE WHEN charging_type IS NOT NULL AND charging_type NOT IN('free','') THEN count(*) ELSE 0 END) as charged_msg_count
from date_info din, sdp_transaction t, operator op,ncs
where din.full_date = DATE(t.time_stamp) and din.date_id = requ_date_id and ncs.ncs_name = t.ncs and t.response_code='S1000'
group by date_id, sp_id, app_id, op.id, ncs_id, direction) as trx, system_app as s where trx.app_id != s.app_id OR trx.ncs_id != s.ncs_id;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

select date_id into requ_date_id from date_info where full_date = requ_date;

open cur1;

read_loop: LOOP
fetch cur1 into c_date_id,c_sp_id,c_sp_name,c_app_id,c_app_name,c_operator_id,c_ncs_id,c_direction,c_revenue_share_percentage,c_sp_revenue,
c_operator_revenue,c_total_revenue,c_traffic,c_charged_msg_count;

if done then
	leave read_loop;
end if;
insert into `sdp_transaction_summary` (date_id,sp_id,sp_name,app_id,app_name,revenue_share_percentage,operator_id,ncs_id,direction,sp_revenue,operator_revenue,total_revenue,traffic, charged_msg_count)
values(c_date_id,c_sp_id,c_sp_name,c_app_id,c_app_name,c_revenue_share_percentage,c_operator_id,c_ncs_id,c_direction,c_sp_revenue,c_operator_revenue,c_total_revenue,c_traffic,c_charged_msg_count);

end LOOP;

close cur1;
end//

-- ===================================================
-- stored_procudure for update the sdp_subscription_summary
-- ===================================================
DROP PROCEDURE IF EXISTS populate_subscription_summary//
create procedure populate_subscription_summary(IN requ_date date)


begin

declare done int default 0;

declare requ_date_id int;

declare c_date_id int(10);
declare c_sp_id varchar(32);
declare c_sp_name varchar(100);
declare c_app_id varchar(32);
declare c_app_name varchar(100);
declare c_new_reg int(11);
declare c_dreg int(11);


declare cur1 cursor for select din.date_id,t.sp_id,t.sp_name,t.app_id,t.app_name,
(CASE WHEN (t.event_type='freeRegistration' OR t.event_type='registrationCharging') AND t.response_code='S1000' THEN count(*) ELSE 0 END) as new_reg,
(CASE WHEN t.event_type='unregistration' THEN count(*) ELSE 0 END) as dereg
from sdp_transaction as t,date_info as din
where (t.event_type='freeRegistration' OR t.event_type='registrationCharging' OR t.event_type='unregistration') AND
din.date_id=requ_date_id AND din.full_date = DATE(t.time_stamp) group by t.app_id,din.date_id,t.event_type;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

select date_id into requ_date_id from date_info where full_date = requ_date;

open cur1;

read_loop: LOOP
fetch cur1 into c_date_id,c_sp_id,c_sp_name,c_app_id,c_app_name,c_new_reg,c_dreg;

if done then
	leave read_loop;
end if;
insert into `sdp_subscription_summary` (`date_id`,`sp_id`,`sp_name`,`app_id`,`app_name`,`new_reg`,`dereg`)
values(c_date_id,c_sp_id,c_sp_name,c_app_id,c_app_name,c_new_reg,c_dreg)
ON DUPLICATE key UPDATE new_reg = VALUES(new_reg) + new_reg, dereg = VALUES(dereg) + dereg;

end LOOP;

close cur1;
end //


-- ===================================================
-- stored_procedure to update the failed_summary
-- ===================================================
DROP PROCEDURE IF EXISTS populate_failed_summary//
CREATE PROCEDURE populate_failed_summary(IN requested_date DATE)
       BEGIN

       DECLARE no_more_records INT DEFAULT 0;
       DECLARE summary_date_id INT(10);
       DECLARE summary_sp_id VARCHAR(32);
       DECLARE summary_app_id VARCHAR(32);
       DECLARE summary_error_code VARCHAR(10);
       DECLARE summary_ncs_id INT(1);
       DECLARE summary_direction VARCHAR(2);
       DECLARE summary_error_count INT;

       DECLARE  cur_failed_summary CURSOR FOR
       SELECT  t2.`date_id` dt_id, t1.`sp_id`, t1.`app_id`, t1.`response_code`, t3.`id`, t1.`direction`, count(t1.`time_stamp`) frequency
       FROM `sdp_transaction` t1, `date_info` t2, `ncs` t3
       WHERE t2.`full_date` = date(t1.`time_stamp`)
       AND t3.`ncs_name` = t1.`ncs`
       AND t1.`response_code` <> 'S1000'
       AND date(t1.`time_stamp`) = requested_date
       GROUP BY dt_id, t1.`sp_id`, t1.`app_id`, t1.`response_code`, t1.`ncs`, t1.`direction`;

       DECLARE  CONTINUE HANDLER FOR NOT FOUND
       SET  no_more_records = 1;

       OPEN cur_failed_summary;

       read_loop : LOOP
       FETCH  cur_failed_summary INTO summary_date_id, summary_sp_id, summary_app_id, summary_error_code, summary_ncs_id, summary_direction, summary_error_count;

       IF no_more_records THEN
          LEAVE read_loop;
       END IF;
       INSERT INTO `sdp_failed_summary`
       VALUES (summary_date_id, summary_sp_id, summary_app_id, summary_error_code, summary_ncs_id, summary_direction, summary_error_count );
       END LOOP;

       CLOSE  cur_failed_summary;
       END //
-- ===========================================================================================================

--  -- ===================================================
-- stored_procedure to update the add_summary  table
-- =====================================================
DROP PROCEDURE IF EXISTS populate_add_summary//
  CREATE PROCEDURE populate_add_summary(IN requested_date DATE)
    BEGIN
     DECLARE DONE INT DEFAULT 0;
     DECLARE requested_date_id int(10);
     DECLARE sp_date_id INT(10);
     DECLARE sp_sp_id VARCHAR(32);
     DECLARE sp_app_id VARCHAR(32);
     DECLARE sp_add_name VARCHAR(40);
     DECLARE sp_print_count INT;
     DECLARE cur_add_summary CURSOR FOR
      SELECT date_info.date_id, sdp.sp_id, sdp.app_id, sdp.advertisement_name, COUNT(*)
      FROM date_info, sdp_transaction sdp
      WHERE date_info.full_date = DATE(sdp.time_stamp) and date_info.date_id = requested_date_id
      and sdp.response_code = 'S1000' and sdp.advertisement_name !='' GROUP BY date_id, sp_id, app_id, advertisement_name;

     DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
    SELECT date_id INTO requested_date_id FROM date_info WHERE full_date = requested_date;

    OPEN cur_add_summary;
    read_loop: LOOP

    FETCH cur_add_summary INTO sp_date_id, sp_sp_id, sp_app_id, sp_add_name, sp_print_count;

    IF DONE THEN
	  leave read_loop;
    END IF;

    INSERT INTO `add_summary` VALUES (sp_date_id, sp_sp_id, sp_app_id, sp_add_name, sp_print_count);

    END LOOP;

    CLOSE cur_add_summary;

    END //

-- ===========================================================================================================

--  -- ===================================================
-- stored_procedure to populate the sdp_reconciliation_summary
-- =====================================================
    DROP PROCEDURE IF EXISTS populate_sdp_reconciliation_summary//
    CREATE PROCEDURE populate_sdp_reconciliation_summary(IN requested_date DATE)
      BEGIN
      DECLARE DONE INT DEFAULT 0;
       DECLARE requested_date_id INT(10);
       DECLARE sp_date_id INT(10);
       DECLARE sp_sp_id VARCHAR(32);
       DECLARE sp_app_id VARCHAR(32);
       DECLARE sp_msg_rev_sp_profit DOUBLE(10,4);
       DECLARE sp_msg_rev_vc_profit DOUBLE(10,4);
       DECLARE sp_msg_rev_op_cost DOUBLE(10,4);
       DECLARE sp_app_total_rev DOUBLE(10,4);
       DECLARE cur_sdp_reconciliation_summary_1 CURSOR FOR
         SELECT date_info.date_id, sdp.sp_id, sdp.app_id, SUM(sdp.sys_cur_sp_profit), SUM(sdp.sys_cur_vc_profit), SUM(sdp.sys_cur_operator_cost)
         FROM date_info, sdp_transaction sdp
         WHERE date_info.full_date = DATE(sdp.time_stamp) AND date_info.date_id = requested_date_id
         AND sdp.response_code = 'S1000' GROUP BY date_id, sp_id, app_id;


       DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

        SELECT date_id INTO requested_date_id FROM date_info WHERE full_date = requested_date;

        OPEN cur_sdp_reconciliation_summary_1;
          read_loop: LOOP

              FETCH cur_sdp_reconciliation_summary_1 INTO sp_date_id, sp_sp_id, sp_app_id, sp_msg_rev_sp_profit, sp_msg_rev_vc_profit, sp_msg_rev_op_cost;

              IF DONE THEN
                leave read_loop;
              END IF;

              INSERT INTO `sdp_reconciliation_summary` (date_id,sp_id,app_id,msg_rev_sp_profit,msg_rev_vc_profit,msg_rev_op_cost,app_total_rev)
              VALUES (sp_date_id, sp_sp_id, sp_app_id, sp_msg_rev_sp_profit, sp_msg_rev_vc_profit, sp_msg_rev_op_cost, sp_msg_rev_sp_profit + sp_msg_rev_vc_profit + sp_msg_rev_op_cost);


          END LOOP;


        CLOSE cur_sdp_reconciliation_summary_1;

        CALL update_sdp_reconciliation_summary(requested_date_id);

        END //

-- ===========================================================================================================

--  -- ===================================================
-- stored_procedure to update the  'sdp_reconciliation_summary.cas_rev_sp_profit' column
--  -- ===================================================
   DROP PROCEDURE IF EXISTS update_sdp_reconciliation_summary//
    CREATE PROCEDURE update_sdp_reconciliation_summary(IN requested_date_id INT(10))
      BEGIN
      DECLARE DONE INT DEFAULT 0;

      DECLARE ckh_cnt INT;

       DECLARE sp_date_id2 INT(10);
       DECLARE sp_sp_id2 VARCHAR(32);
       DECLARE sp_app_id2 VARCHAR(32);
       DECLARE sp_cas_rev_sp_profit DOUBLE(10,4);

        DECLARE cur_sdp_reconciliation_summary_2 CURSOR FOR
         SELECT date_info.date_id, sdp.sp_id, sdp.app_id, SUM(sdp.sys_cur_charge_amount)
         FROM sdp_transaction sdp, date_info
         WHERE date_info.full_date = DATE(sdp.time_stamp) AND date_info.date_id = requested_date_id
         AND sdp.response_code = 'S1000' GROUP BY date_id, sp_id, app_id;

          DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

          OPEN cur_sdp_reconciliation_summary_2;
          read_loop: LOOP
              FETCH cur_sdp_reconciliation_summary_2 INTO sp_date_id2, sp_sp_id2, sp_app_id2, sp_cas_rev_sp_profit;

              IF DONE THEN
                leave read_loop;
              END IF;


              IF (SELECT COUNT(*) FROM `sdp_reconciliation_summary` WHERE date_id=sp_date_id2 AND sp_id=sp_sp_id2 AND app_id=sp_app_id2)=0 THEN
                INSERT INTO `sdp_reconciliation_summary` (date_id, sp_id, app_id, cas_rev_sp_profit, app_total_rev)
                VALUES (sp_date_id2,sp_sp_id2, sp_app_id2,sp_cas_rev_sp_profit,sp_cas_rev_sp_profit);
              ELSE
                UPDATE `sdp_reconciliation_summary` SET cas_rev_sp_profit = sp_cas_rev_sp_profit , app_total_rev=app_total_rev + sp_cas_rev_sp_profit
                WHERE date_id=sp_date_id2 AND sp_id=sp_sp_id2 AND app_id=sp_app_id2;
              END IF;
          END LOOP;
        CLOSE cur_sdp_reconciliation_summary_2;
        END //

-- ==========================================================================================================


--  -- ===================================================
-- stored_procedure to update the  'pgw_transaction_summary
--  -- ===================================================

DROP PROCEDURE IF EXISTS populate_pgw_transaction_summary//
CREATE PROCEDURE `populate_pgw_transaction_summary`(IN requ_date date)
BEGIN
       DECLARE no_more_records INT DEFAULT 0;
       DECLARE state VARCHAR(50);
       DECLARE summary_date_id INT(10);
       DECLARE summary_payment_instrument_type_id VARCHAR(32);
       DECLARE summary_tax_type VARCHAR(32);
       DECLARE summary_request_amount DOUBLE ;
       DECLARE summary_paid_amount DOUBLE;
       DECLARE summary_total_transactions INT;
       DECLARE summary_tax_amount FLOAT;
       DECLARE summary_service_charge_amount FLOAT;
       DECLARE cur_summary CURSOR FOR
       SELECT COUNT(*) AS total_transactions,t2.`date_id` AS dt_id,
            t1.`payment_instrument`,t1.`tx_type`,
            SUM(t1.`request_amount`),
            SUM(t1.`paid_amount`),
            SUM(t1.`tax_amount`),
            SUM(t1.`service_charge_amount`)
       FROM pgw_transaction t1, date_info t2
       WHERE date(t1.`timeStamp`)=t2.`full_date`
       AND date(t1.`timeStamp`) = `requ_date`
       AND (t1.`tx_status` = "SUCCESS"
       OR t1.`tx_status` = "AMOUNT_FULLY_PAID"
       OR t1.`tx_status` = "PARTIAL_PAYMENT_SUCCESS"
       OR t1.`tx_status` = "RESERVE_SUCCESS"
       OR t1.`tx_status` = "RESERVE_COMMIT_SUCCESS"
       OR t1.`tx_status` = "PENDING"
       OR t1.`tx_status` = "PARTIAL_PENDING")
       GROUP BY dt_id, t1.`payment_instrument`,t1.`tx_type`;
       DECLARE CONTINUE HANDLER FOR NOT FOUND
       SET no_more_records = 1;
       OPEN cur_summary;
       read_loop : LOOP
       FETCH cur_summary INTO summary_total_transactions,summary_date_id,
        summary_payment_instrument_type_id,
        summary_tax_type,summary_request_amount, summary_paid_amount,summary_tax_amount,
        summary_service_charge_amount;
       IF no_more_records THEN
          LEAVE read_loop;
       END IF;
       INSERT INTO pgw_transaction_summary
       VALUES (summary_date_id,
        summary_request_amount,
        summary_total_transactions,
        summary_paid_amount-summary_tax_amount-summary_service_charge_amount,
        summary_paid_amount,summary_tax_type,
        summary_payment_instrument_type_id,
        summary_service_charge_amount);
       END LOOP;
       CLOSE cur_summary;
END//

DELIMITER ;
