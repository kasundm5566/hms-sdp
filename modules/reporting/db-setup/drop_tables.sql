-- ######################################################################################
-- #                        Drop tables                                                 #
-- ######################################################################################
drop table if exists `date_info`;
drop table if exists `sdp_transaction_summary`;
drop table if exists `sdp_failed_summary`;
drop table if exists `add_summary`;
drop table if exists `pgw_transaction_summary`;
drop table if exists `pgw_income_summary`;
drop table if exists `operator`;
drop table if exists `ncs`;
drop table if exists `service_provider_info`;
drop table if exists `sdp_transaction`;
drop table if exists `summary_data_status`;
drop table if exists `payment_instrument_type`;
drop table if exists `pgw_transaction`;