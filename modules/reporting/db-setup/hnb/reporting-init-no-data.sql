-- MySQL dump 10.13  Distrib 5.1.47, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: reporting
-- ------------------------------------------------------
-- Server version	5.5.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `add_summary`
--

DROP TABLE IF EXISTS `add_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `add_summary` (
  `date_id` int(10) NOT NULL DEFAULT '0',
  `sp_id` varchar(32) NOT NULL DEFAULT '',
  `app_id` varchar(32) NOT NULL DEFAULT '',
  `add_name` varchar(40) NOT NULL DEFAULT '',
  `print_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`date_id`,`sp_id`,`app_id`,`add_name`),
  KEY `ix_add_summary` (`app_id`),
  KEY `ix_add_summary_app_wise_add` (`date_id`,`app_id`),
  KEY `ix_add_summary_add_wise` (`date_id`,`add_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `advertisement`
--

DROP TABLE IF EXISTS `advertisement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advertisement` (
  `name` varchar(40) NOT NULL DEFAULT '',
  `content` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `corporate_user_info`
--

DROP TABLE IF EXISTS `corporate_user_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corporate_user_info` (
  `corporate_user_id` varchar(100) NOT NULL DEFAULT '',
  `sp_beneficiary` text,
  `sp_beneficiary_name` varchar(100) DEFAULT NULL,
  `sp_beneficiary_bank_code` varchar(100) DEFAULT NULL,
  `sp_beneficiary_branch_code` varchar(100) DEFAULT NULL,
  `sp_beneficiary_branch_name` varchar(100) DEFAULT NULL,
  `sp_beneficiary_bank_account_number` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`corporate_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `date_info`
--

DROP TABLE IF EXISTS `date_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `date_info` (
  `date_id` int(11) NOT NULL DEFAULT '0',
  `year` int(5) DEFAULT NULL,
  `month` int(2) DEFAULT NULL,
  `month_name` varchar(51) DEFAULT NULL,
  `day_of_month` int(2) DEFAULT NULL,
  `day_of_week` int(1) DEFAULT NULL,
  `week_of_year` int(2) DEFAULT NULL,
  `full_date` date DEFAULT NULL,
  PRIMARY KEY (`date_id`),
  UNIQUE KEY `ix_date_info_full_date` (`full_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `governance_detail`
--

DROP TABLE IF EXISTS `governance_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `governance_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `application_id` varchar(32) DEFAULT NULL,
  `application_name` varchar(100) DEFAULT NULL,
  `message` varchar(160) DEFAULT NULL,
  `requested_date` datetime DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `date_of_action` datetime DEFAULT NULL,
  `action_by` varchar(30) DEFAULT NULL,
  `number_of_recipients` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_sdp_governance_date_app` (`date_of_action`,`application_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `log_info`
--

DROP TABLE IF EXISTS `log_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(50) DEFAULT NULL,
  `processed_time` datetime DEFAULT NULL,
  `file_type` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `file_name` (`file_name`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ncs`
--

DROP TABLE IF EXISTS `ncs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ncs` (
  `id` int(1) NOT NULL DEFAULT '0',
  `ncs_name` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_ncs_name` (`ncs_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `operator`
--

DROP TABLE IF EXISTS `operator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operator` (
  `id` int(1) NOT NULL DEFAULT '0',
  `operator_name` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_operator_name` (`operator_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment_instrument_type`
--

DROP TABLE IF EXISTS `payment_instrument_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_instrument_type` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment_instruments`
--

DROP TABLE IF EXISTS `payment_instruments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_instruments` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pgw_application_summary`
--

DROP TABLE IF EXISTS `pgw_application_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pgw_application_summary` (
  `date_id` int(10) NOT NULL DEFAULT '0',
  `total_rquest_amount` double(10,4) DEFAULT '0.0000',
  `total_transactions` int(11) DEFAULT NULL,
  `total_service_pro_earning` double(10,4) DEFAULT NULL,
  `total_amount_paid` double(10,4) DEFAULT NULL,
  `tx_type` varchar(45) NOT NULL,
  `payment_instrument` varchar(45) NOT NULL,
  `app_id` varchar(45) NOT NULL,
  `app_name` varchar(30) DEFAULT NULL,
  `income_received` double(10,4) DEFAULT '0.0000',
  PRIMARY KEY (`date_id`,`tx_type`,`payment_instrument`,`app_id`),
  KEY `ix_pgw_transaction_summary_date_id` (`date_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pgw_income_summary`
--

DROP TABLE IF EXISTS `pgw_income_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pgw_income_summary` (
  `date_id` int(10) NOT NULL DEFAULT '0',
  `payment_instrument_type_id` int(11) NOT NULL DEFAULT '0',
  `amount` double(10,4) DEFAULT '0.0000',
  PRIMARY KEY (`date_id`,`payment_instrument_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pgw_repayment_summary`
--

DROP TABLE IF EXISTS `pgw_repayment_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pgw_repayment_summary` (
  `date_id` int(11) NOT NULL,
  `msisdn` varchar(45) NOT NULL DEFAULT '',
  `payments` int(11) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `payment_instrument` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`date_id`,`payment_instrument`,`msisdn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pgw_transaction`
--

DROP TABLE IF EXISTS `pgw_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pgw_transaction` (
  `correlation_id` bigint(20) NOT NULL DEFAULT '0',
  `client_trx_id` varchar(36) DEFAULT NULL,
  `original_trx_id` bigint(20) DEFAULT NULL,
  `time_stamp` datetime NOT NULL,
  `transaction_type` varchar(30) DEFAULT NULL,
  `channel` varchar(10) DEFAULT NULL,
  `request_amount` double(10,4) DEFAULT NULL,
  `sys_cur_request_amount` double(10,4) DEFAULT NULL,
  `request_currency` varchar(10) DEFAULT NULL,
  `request_amount_buying_rate` double(10,4) DEFAULT NULL,
  `request_amount_selling_rate` double(10,4) DEFAULT NULL,
  `charged_category` varchar(30) DEFAULT NULL,
  `merchant_id` varchar(30) DEFAULT NULL,
  `from_payment_instrument_id` int(11) DEFAULT NULL,
  `from_payment_instrument` varchar(30) DEFAULT NULL,
  `from_msisdn` varchar(20) DEFAULT NULL,
  `from_amount` double(10,4) DEFAULT NULL,
  `sys_cur_from_amount` double(10,4) DEFAULT NULL,
  `from_currency` varchar(3) DEFAULT NULL,
  `from_buying_rate` double(10,4) DEFAULT NULL,
  `from_selling_rate` double(10,4) DEFAULT NULL,
  `to_payment_instrument_id` int(11) DEFAULT NULL,
  `to_payment_instrument` varchar(30) DEFAULT NULL,
  `to_msisdn` varchar(20) DEFAULT NULL,
  `to_amount` double(10,4) DEFAULT NULL,
  `sys_cur_to_amount` double(10,4) DEFAULT NULL,
  `to_currency` varchar(3) DEFAULT NULL,
  `to_buying_rate` double(10,4) DEFAULT NULL,
  `to_selling_rate` double(10,4) DEFAULT NULL,
  `rate_card` text,
  `order_no` varchar(30) DEFAULT NULL,
  `invoice_no` varchar(30) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `line_items` blob,
  `user_name` varchar(30) DEFAULT NULL,
  `service_charge_amount` double(10,4) DEFAULT NULL,
  `sys_cur_service_charge_amount` double(10,4) DEFAULT NULL,
  `service_charge_currency` varchar(3) DEFAULT NULL,
  `service_charge_amount_buying_rate` double(10,4) DEFAULT NULL,
  `service_charge_amount_selling_rate` double(10,4) DEFAULT NULL,
  `currency_conversion_amount` double(10,4) DEFAULT NULL,
  `sys_cur_currency_conversion_amount` double(10,4) DEFAULT NULL,
  `currency_conversion_currency` varchar(3) DEFAULT NULL,
  `currency_conversion_amount_buying_rate` double(10,4) DEFAULT NULL,
  `currency_conversion_amount_selling_rate` double(10,4) DEFAULT NULL,
  PRIMARY KEY (`correlation_id`),
  KEY `ix_populate_pgw_income_summary` (`time_stamp`,`status`,`from_payment_instrument_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pgw_transaction_summary`
--

DROP TABLE IF EXISTS `pgw_transaction_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pgw_transaction_summary` (
  `date_id` int(10) NOT NULL DEFAULT '0',
  `payment_instrument_type_id` int(11) NOT NULL DEFAULT '0',
  `total_credit_value` double(10,4) DEFAULT '0.0000',
  `total_credit_transactions` int(11) DEFAULT NULL,
  `total_debit_value` double(10,4) DEFAULT '0.0000',
  `total_debit_transactions` int(11) DEFAULT NULL,
  `inflow` double(10,4) DEFAULT NULL,
  PRIMARY KEY (`date_id`,`payment_instrument_type_id`),
  KEY `ix_pgw_transaction_summary_date_id` (`date_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sdp_failed_summary`
--

DROP TABLE IF EXISTS `sdp_failed_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sdp_failed_summary` (
  `date_id` int(10) NOT NULL DEFAULT '0',
  `sp_id` varchar(32) NOT NULL DEFAULT '',
  `app_id` varchar(32) NOT NULL DEFAULT '',
  `error_code` varchar(10) NOT NULL DEFAULT '',
  `ncs_id` int(1) NOT NULL DEFAULT '0',
  `direction` varchar(32) NOT NULL DEFAULT '',
  `error_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`date_id`,`sp_id`,`app_id`,`error_code`,`ncs_id`,`direction`),
  KEY `ix_sdp_failed_summary_report` (`sp_id`,`app_id`,`ncs_id`,`date_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sdp_reconciliation_summary`
--

DROP TABLE IF EXISTS `sdp_reconciliation_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sdp_reconciliation_summary` (
  `date_id` int(10) NOT NULL DEFAULT '0',
  `sp_id` varchar(32) NOT NULL DEFAULT '',
  `app_id` varchar(32) NOT NULL DEFAULT '',
  `msg_rev_sp_profit` double(10,4) DEFAULT '0.0000',
  `msg_rev_vc_profit` double(10,4) DEFAULT '0.0000',
  `app_total_rev` double(10,4) DEFAULT '0.0000',
  PRIMARY KEY (`date_id`,`sp_id`,`app_id`),
  KEY `ix_sdp_recon_summery_sp_date` (`sp_id`,`date_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sdp_transaction`
--

DROP TABLE IF EXISTS `sdp_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sdp_transaction` (
  `correlation_id` varchar(36) NOT NULL,
  `time_stamp` datetime DEFAULT NULL,
  `sp_id` varchar(32) DEFAULT NULL,
  `sp_name` varchar(100) DEFAULT NULL,
  `app_id` varchar(32) DEFAULT NULL,
  `app_name` varchar(100) DEFAULT NULL,
  `app_state` varchar(32) DEFAULT NULL,
  `source_address` varchar(256) DEFAULT NULL,
  `masked_source_address` varchar(256) DEFAULT NULL,
  `source_channel_type` varchar(32) DEFAULT NULL,
  `source_channel_protocol` varchar(32) DEFAULT NULL,
  `destination_address` varchar(256) DEFAULT NULL,
  `masked_destination_address` varchar(256) DEFAULT NULL,
  `destination_channel_type` varchar(32) DEFAULT NULL,
  `destination_channel_protocol` varchar(32) DEFAULT NULL,
  `direction` varchar(32) DEFAULT NULL,
  `ncs` varchar(12) DEFAULT NULL,
  `billing_type` varchar(8) DEFAULT NULL,
  `charge_party_type` varchar(32) DEFAULT NULL,
  `charge_amount` double(10,4) DEFAULT '0.0000',
  `sys_cur_charge_amount` double(10,4) DEFAULT '0.0000',
  `charge_amount_currency` varchar(3) DEFAULT NULL,
  `charge_amount_buying_rate` double(10,4) DEFAULT '0.0000',
  `charge_amount_selling_rate` double(10,4) DEFAULT '0.0000',
  `rate_card` text,
  `charge_category` varchar(30) DEFAULT NULL,
  `charge_address` varchar(256) DEFAULT NULL,
  `masked_charge_address` varchar(256) DEFAULT NULL,
  `event_type` varchar(33) DEFAULT NULL,
  `response_code` varchar(16) DEFAULT NULL,
  `response_description` varchar(200) DEFAULT NULL,
  `transaction_status` varchar(15) DEFAULT NULL,
  `keyword` varchar(100) DEFAULT NULL,
  `short_code` varchar(12) DEFAULT NULL,
  `operator` varchar(12) DEFAULT NULL,
  `sys_cur_operator_cost` double(10,4) DEFAULT '0.0000',
  `revenue_share_type` varchar(50) DEFAULT NULL,
  `revenue_share_percentage` double(10,4) DEFAULT '0.0000',
  `sys_cur_sp_profit` double(10,4) DEFAULT '0.0000',
  `sys_cur_vc_profit` double(10,4) DEFAULT '0.0000',
  `advertisement_name` varchar(40) DEFAULT NULL,
  `advertisement_content` varchar(100) DEFAULT NULL,
  `is_multiple_recipients` int(1) DEFAULT NULL,
  `overall_response_code` varchar(16) DEFAULT NULL,
  `overall_response_description` varchar(200) DEFAULT NULL,
  `pgw_transaction_id` varchar(36) DEFAULT NULL,
  `charging_type` varchar(30) DEFAULT NULL,
  `is_number_masked` int(1) DEFAULT NULL,
  `order_no` varchar(50) DEFAULT NULL,
  `invoice_no` varchar(50) DEFAULT NULL,
  `external_trx_id` varchar(50) DEFAULT NULL,
  `session_id` varchar(20) DEFAULT NULL,
  `ussd_operation` varchar(20) DEFAULT NULL,
  `balance_due` double(10,4) DEFAULT NULL,
  `total_amount` double(10,4) DEFAULT NULL,
  KEY `ix_populate_sdp_trans_summary` (`time_stamp`,`operator`,`ncs`,`response_code`,`sp_id`,`app_id`,`direction`),
  KEY `ix_populate_add_summary` (`time_stamp`,`response_code`,`advertisement_name`,`sp_id`,`app_id`),
  KEY `ix_sdp_cc_report` (`time_stamp`,`sp_id`,`app_id`,`short_code`,`keyword`,`app_state`,`direction`,`source_address`,`destination_address`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
/*!50100 PARTITION BY RANGE ( TO_DAYS(`time_stamp`))
(PARTITION par_nov_2011 VALUES LESS THAN (734836) ENGINE = InnoDB,
 PARTITION par_dec_2011 VALUES LESS THAN (734867) ENGINE = InnoDB,
 PARTITION par_jan_2012 VALUES LESS THAN (734898) ENGINE = InnoDB,
 PARTITION par_feb_2012 VALUES LESS THAN (734927) ENGINE = InnoDB,
 PARTITION par_mar_2012 VALUES LESS THAN (734958) ENGINE = InnoDB,
 PARTITION par_apr_2012 VALUES LESS THAN (734988) ENGINE = InnoDB,
 PARTITION par_may_2012 VALUES LESS THAN (735019) ENGINE = InnoDB,
 PARTITION par_jun_2012 VALUES LESS THAN (735049) ENGINE = InnoDB,
 PARTITION par_jul_2012 VALUES LESS THAN (735080) ENGINE = InnoDB,
 PARTITION par_aug_2012 VALUES LESS THAN (735111) ENGINE = InnoDB,
 PARTITION par_sep_2012 VALUES LESS THAN (735141) ENGINE = InnoDB,
 PARTITION par_oct_2012 VALUES LESS THAN (735172) ENGINE = InnoDB,
 PARTITION par_nov_2012 VALUES LESS THAN (735202) ENGINE = InnoDB,
 PARTITION par_dec_2012 VALUES LESS THAN (735233) ENGINE = InnoDB,
 PARTITION par_monthly_rest VALUES LESS THAN MAXVALUE ENGINE = InnoDB) */;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sdp_transaction_summary`
--

DROP TABLE IF EXISTS `sdp_transaction_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sdp_transaction_summary` (
  `date_id` int(10) NOT NULL DEFAULT '0',
  `sp_id` varchar(32) NOT NULL DEFAULT '',
  `sp_name` varchar(100) DEFAULT NULL,
  `app_id` varchar(32) NOT NULL DEFAULT '',
  `app_name` varchar(100) DEFAULT NULL,
  `revenue_share_percentage` double(10,4) DEFAULT '0.0000',
  `operator_id` int(1) NOT NULL DEFAULT '0',
  `ncs_id` int(1) NOT NULL DEFAULT '0',
  `direction` varchar(32) NOT NULL DEFAULT '',
  `sp_revenue` double(10,4) DEFAULT '0.0000',
  `operator_revenue` double(10,4) DEFAULT '0.0000',
  `total_revenue` double(10,4) DEFAULT '0.0000',
  `traffic` int(11) DEFAULT '0',
  `charged_msg_count` int(10) DEFAULT NULL,
  PRIMARY KEY (`date_id`,`sp_id`,`app_id`,`operator_id`,`ncs_id`,`direction`),
  KEY `ix_sdp_trans_summary_date_app` (`date_id`,`app_id`),
  KEY `ix_sdp_trans_summary_date_op_sp` (`date_id`,`operator_id`,`sp_id`),
  KEY `ix_sdp_trans_summary_sp_date` (`sp_id`,`date_id`),
  KEY `ix_sdp_trans_summary_op_date` (`operator_id`,`date_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `service_provider_info`
--

DROP TABLE IF EXISTS `service_provider_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_provider_info` (
  `sp_id` varchar(32) DEFAULT NULL,
  `sp_name` varchar(100) DEFAULT NULL,
  `corporate_user_id` varchar(100) DEFAULT NULL,
  `app_id` varchar(32) DEFAULT NULL,
  `app_name` varchar(100) DEFAULT NULL,
  `app_catogory` varchar(32) DEFAULT NULL,
  `revenue_share_type` varchar(100) DEFAULT NULL,
  `revenue_share_percentage` double(10,4) DEFAULT '0.0000',
  UNIQUE KEY `sp_id` (`sp_id`,`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sp_payment_instrument_info`
--

DROP TABLE IF EXISTS `sp_payment_instrument_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_payment_instrument_info` (
  `sp_id` varchar(32) DEFAULT NULL,
  `app_id` varchar(32) DEFAULT NULL,
  `ncs` varchar(20) DEFAULT NULL,
  `payment_instrument` varchar(20) DEFAULT NULL,
  `pi_additional_info` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `summary_data_status`
--

DROP TABLE IF EXISTS `summary_data_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `summary_data_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `procedure_name` varchar(50) NOT NULL,
  `requested_date` date DEFAULT NULL,
  `start_date_time` datetime DEFAULT NULL,
  `end_date_time` datetime DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_summary_data_status` (`procedure_name`,`status`,`requested_date`)
) ENGINE=InnoDB AUTO_INCREMENT=6185 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `system_app`
--

DROP TABLE IF EXISTS `system_app`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_app` (
  `app_id` varchar(32) DEFAULT NULL,
  `ncs_id` int(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `app_id` (`app_id`,`ncs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-11-08 11:20:12
