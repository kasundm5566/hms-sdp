/*new columns added to sdp_transaction table*/
ALTER TABLE sdp_transaction ADD order_no varchar(50);
ALTER TABLE sdp_transaction ADD invoice_no varchar(50);
ALTER TABLE sdp_transaction ADD external_trx_id varchar(50);
ALTER TABLE sdp_transaction ADD session_id varchar(20);
ALTER TABLE sdp_transaction ADD ussd_operation varchar(20);
ALTER TABLE sdp_transaction ADD balance_due double(10,4) default 0.0000;
ALTER TABLE sdp_transaction ADD total_amount double(10,4) default 0.0000;

/*new column added to sdp_transaction_summary*/
ALTER TABLE sdp_transaction_summary ADD charged_msg_count int(10) default 0;


