delete from sdp_transaction where app_id is NULL;
update sdp_transaction set app_name='HNB_App' where app_id='APP_000001';
update sdp_transaction set app_name='HNB_Credit_Card_Alerts' where app_id='APP_000007';
update sdp_transaction set app_name='Credit_and_Debit_Card_Alerts' where app_id='APP_000009';

update sdp_transaction_summary set app_name='HNB_App' where app_id='APP_000001';
update sdp_transaction_summary set app_name='HNB_Credit_Card_Alerts' where app_id='APP_000007';
update sdp_transaction_summary set app_name='Credit_and_Debit_Card_Alerts' where app_id='APP_000009';

update sdp_transaction set sp_name='bankappuser' where app_id='APP_000001';
update sdp_transaction set sp_name='hnbsdp' where app_id in ('APP_000005','APP_000006','APP_000007','APP_000009');
update sdp_transaction_summary set sp_name='bankappuser' where app_id='APP_000001';
update sdp_transaction_summary set sp_name='hnbsdp' where app_id in ('APP_000005','APP_000006','APP_000007','APP_000009');

update sdp_transaction set sp_id='SPP_000002' where app_id='APP_000001';
update sdp_transaction set sp_id='SPP_000003' where app_id in ('APP_000005','APP_000006','APP_000007','APP_000009');
update sdp_transaction_summary set sp_id='SPP_000002' where app_id='APP_000001';
update sdp_transaction_summary set sp_id='SPP_000003' where app_id in ('APP_000005','APP_000006','APP_000007','APP_000009');

update sdp_failed_summary set sp_id='SPP_000002' where app_id='APP_000001';
update sdp_failed_summary set sp_id='SPP_000003' where app_id in ('APP_000005','APP_000006','APP_000007','APP_000009');


