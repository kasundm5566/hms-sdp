insert into ncs values (1, 'sms');
insert into ncs values (2, 'ussd');
insert into ncs values (3, 'wap-push');
insert into ncs values (4, 'cas');
insert into ncs values (5, 'mms');
insert into ncs values (6, 'subscription');

insert into operator values (1, 'safaricom');
-- insert into operator values (2, 'orange');
-- insert into operator values (3, 'airtel');

INSERT INTO `summary_data_status` VALUES (1,'populate_transaction_summary','2011-07-31','2011-07-31 10:00:00',NULL,1),
(2,'populate_transaction_summary','2011-07-31',NULL,'2011-07-31 10:00:00',2),
(3,'populate_failed_summary','2011-07-31','2011-07-31 10:00:00',NULL,1),
(4,'populate_failed_summary','2011-07-31',NULL,'2011-07-31 10:00:00',2),
(5,'populate_pgw_income_summary','2011-07-31','2011-07-31 10:00:00',NULL,1),
(6,'populate_pgw_income_summary','2011-07-31',NULL,'2011-07-31 10:00:00',2),
(7,'populate_add_summary','2011-07-31','2011-07-31 10:00:00',NULL,1),
(8,'populate_add_summary','2011-07-31',NULL,'2011-07-31 10:00:00',2),
(9,'populate_sdp_reconciliation_summary','2011-07-31','2011-07-31 10:00:00',NULL,1),
(10,'populate_sdp_reconciliation_summary','2011-07-31',NULL,'2011-07-31 10:00:00',2),
(11,'populate_pgw_transaction_summary','2011-07-31','2011-07-31 10:00:00',NULL,1),
(12,'populate_pgw_transaction_summary','2011-07-31',NULL,'2011-07-31 10:00:00',2),
(13,'populate_pgw_repayment_summary','2011-07-31','2011-07-31 10:00:00',NULL,1),
(14,'populate_pgw_repayment_summary','2011-07-31',NULL,'2011-07-31 10:00:00',2),
(15,'populate_pgw_application_summary','2011-07-31','2011-07-31 10:00:00',NULL,1),
(16,'populate_pgw_application_summary','2011-07-31',NULL,'2011-07-31 10:00:00',2);

-- INSERT INTO `service_provider_info` VALUES ('SPP_000000','corporate','APP_000000','system-app'),
-- ('SPP_000001','corporate','APP_000001','votingtest'),
-- ('SPP_000001','corporate','APP_000002','sandaApp1'),
-- ('SPP_000001','corporate','APP_000003','sandaApp2');

-- INSERT INTO `advertisement` VALUES ('ad-7777777','ssssaaaa');

INSERT INTO `payment_instruments` VALUES (1,'Equity Bank');
INSERT INTO `payment_instruments` VALUES (4,'Mobile Account');
INSERT INTO `payment_instruments` VALUES (2,'M-Pesa');
INSERT INTO `payment_instruments` VALUES (5,'Voucher');


INSERT INTO `system_app` VALUES ('app_id',0);



