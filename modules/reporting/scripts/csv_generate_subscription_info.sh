OIFS=$IFS;
IFS=",";

# fill in your details here
dbname=subscription

# first get all collections in the database
collections=subscription;
collectionArray=($collections);

# for each collection
for ((i=0; i<${#collectionArray[@]}; ++i));
do
    echo 'exporting collection' ${collectionArray[$i]}
    # get comma separated list of keys. do this by peeking into the first document in the collection and get his set of keys

    keys=`mongo subscription --eval "rs.slaveOk();var keys = []; for(var key in db.${collectionArray[$i]}.find().sort({_id: -1}).limit(1)[0]) { keys.push(key); }; keys;" --quiet`;

 # now use mongoexport with the set of keys to export the collection to csv

mongoexport -d subscription -c ${collectionArray[$i]} --query '{"current-status" : "REGISTERED"}' --fields created-date,app-id,sp-id,msisdn,current-status --csv --out /hms/scripts/subscription_info.csv;
done

IFS=$OIFS;