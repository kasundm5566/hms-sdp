#!/usr/bin/perl

use feature "switch";

our @possibleValues=(["DIRECT_DEBIT", "CREDIT", "CREDIT_RESERVE", "TRANSFER"],
["WEB", "USSD"],
["SUCCESS", "RESERVE_SUCCESS", "COMMIT_SUCCESS", "RESERVE_CANCLED", "INSUFFICIENT_CREDIT", "EXTERNAL_ERROR", "INTERNAL_ERROR", "PENDING_AUTH", "AUTH_REJECTED", "AUTH_PENDING", "AUTH_TIMEOUTED", "MAX_NO_TRX_PER_DAY_EXCEEDED", "MAX_NO_TRX_PER_MONTH_EXCEEDED", "MAX_TRX_AMOUNT_PER_DAY_EXCEEDED", "MAX_TRX_AMOUNT_PER_MONTH_EXCEEDED", "PURCHASE_CATEGORY_NOT_DEFINED", "APP_DISALLOWED", "APP_PENDING_AUTH", "PAY_INSTRUMENT_ERROR", "USER_PAY_INS_ERROR", "USER_INACTIVE_ERROR", "INVALID_RESERVATION_ID", "ROLLED_BACK", "ROLL_BACK_ERROR"],
["charity", "sms-mo", "cas"]);

our @currencycode=("USD","KES","EUR","LKR");



sub selectfield{
	my $val = $_[0];
	
	given ($val){
		when(3)		{ push(@datarow,&ran_str_rtn(4)) }
		when(4)		{ push(@datarow,$startseq+123) }
		when(5)		{ &datetime() }
		when(6)		{ push(@datarow,$possibleValues[0][int(rand(4))]) }
	   	when(7) 	{ push(@datarow,$possibleValues[1][int(rand(2))]) }
		when(8)		{ push(@datarow,&amount_rtn(3)) }
		when(9)		{ push(@datarow,$currencycode[int(rand(4))]) }#KES
        when(10)    { push(@datarow,$possibleValues[3][int(rand(3))])}
		when(12)	{ push(@datarow,int(rand(4))+1) }
		when(15)	{ push(@datarow,&amount_rtn(3)) }
		when(16)	{ push(@datarow,$currencycode[int(rand(4))]) }#KES
		when(17)	{ push(@datarow,int(rand(4))+1) }
		when(20)	{ push(@datarow,&amount_rtn(3)) }
		when(21)	{ push(@datarow,$currencycode[int(rand(4))]) }#KES
		when(22)	{ &currencyJson() }#curreenyJson
		when(25)	{ &moreSuccess() }
		when(26)	{ &line_item() }
		default		{ push(@datarow,&ran_str_rtn(4)) }
	}
}


sub currencyJson{
	our @currJson=();
	my @curreJsonField = ("{\"currencyCode\":","\"buyingRate\":","\"sellingRate\":");
	my $curJarrLen = 4;#int(rand(4)+1);
	if($curJarrLen !=1){
		push(@currJson,"[");		
	}
	for($curJar=1;$curJar<=$curJarrLen;$curJar++){
		our $curcodeId=$curJar-1;#int(rand(4));
		for($kk=0;$kk<3;$kk++){
			push(@currJson,$curreJsonField[$kk]);
			&currencyJsonVal($kk);
		}
		if($curJar!=$curJarrLen){
				push(@currJson,",")	
		}
	}
	if($curJarrLen !=1){
		push(@currJson,"]");		
	}

	$"="";
	push(@datarow,"@currJson");
}

sub currencyJsonVal{
	my $arg1 = $_[0];
	my @curcoderate=(["USD","0.011","0.018"],
["EUR","0.0077","0.0097"],
["KES","1","1"],
["LKR","1.2334","1.5534"]);


given ($arg1){
		when(0)		{ push(@currJson,"\"".$curcoderate[$curcodeId][$arg1]."\"",",") }
		when(1)		{ push(@currJson,$curcoderate[$curcodeId][$arg1],",") }
		when(2)		{ push(@currJson,$curcoderate[$curcodeId][$arg1],"}") }
	}



	
}



sub datetime{
	######################
	my $yearRange = 1;
	my $startingYear = 2011;
	######################
	my $yyyy = int(rand($yearRange))+$startingYear;
	my $mm	= int(rand(2))+6; #month june and july
	my $dd = int(rand(28))+1; #days form 1 to 28
	my $HH = int(rand(24));
	my $min = int(rand(60));
	my $ss = int(rand(60));

push(@datarow,"$yyyy-$mm-$dd $HH:$min:$ss" );
}



sub ran_str_rtn{
	my $length_of_randomstring=shift;
	my @chars=('a'..'z','A'..'Z','0'..'9');
	my $random_string;
	
	foreach (1..$length_of_randomstring) {
		$random_string.=$chars[rand @chars];
	}
		
	return $random_string;
}

sub amount_rtn{
	my $length_of_randomstring=shift;
	my @chars=('1'..'9');
	my $ran_amt;
	
	foreach (1..$length_of_randomstring) {
		$ran_amt.=$chars[rand @chars];
	}
		
	return $ran_amt;
}

sub moreSuccess{
	#push(@datarow,$possibleValues[2][int(rand(24))])
	my $suc = int(rand(2));
	if($suc==0){
		push(@datarow,$possibleValues[2][$suc])
	}
	else{
		push(@datarow,$possibleValues[2][int(rand(23))+1])
	}
}


sub line_item{
	our @lineitemrow=();
	our @jsonFieldname=("{\"paymentInstrumentId\":","\"accountId\":","\"operationType\":","\"amount\":","{\"amountValue\":","\"currencyCode\":","\"originatedRuleId\":","\"executeOnSysAccount\":","\"ownerType\":","\"type\":");

	
	push(@lineitemrow,"[");
	our $ruleSelector = int(rand(4));
	for(our $linekey=0;$linekey<2;$linekey++){
		if($linekey!=0){
			push(@lineitemrow,",")	
		}	
		for($k=0;$k<10;$k++){
				push(@lineitemrow,$jsonFieldname[$k]);
				&jsonValue($k);
		}
	}
	push(@lineitemrow,"]");

	$"="";
	push(@datarow,"@lineitemrow");
	
	
}

sub jsonValue{
	my $val = $_[0];
	my @enum=("CREDIT", "DEBIT");
	my @booleanval=("true","false");
	my @ruleId=("1001","1002","1003","1004");
	
	my @ownerType=("CONSUMER", "MERCHANT", "SYSTEM");
	my @lineItemType=("PAYER_ORIGINAL", "PAYEE_ORIGINAL", "SYS_GENERATED");
	given ($val){
		when(0)		{ push(@lineitemrow,int(rand(4))+1,",") }
		when(1)		{ push(@lineitemrow,"\"ACC".int(rand(5))."\"",",") }
		when(2)		{ push(@lineitemrow,"\"".$enum[$linekey]."\"",",") }
		when(4)		{ push(@lineitemrow, int(rand(900)+100),",") }
		when(5)		{ push(@lineitemrow, "\"".$currencycode[int(rand(4))]."\"","},") }#KES
		when(6)		{ push(@lineitemrow,"\"1001\"",",")}#{ push(@lineitemrow, "\"".$ruleId[$ruleSelector]."\"",",") }
		when(7)		{ push(@lineitemrow,$booleanval[int(rand(2))],",") } #
		when(8)		{ push(@lineitemrow,"\"".$ownerType[int(rand(3))]."\"",",") }
		when(9)		{ push(@lineitemrow,"\"".$lineItemType[int(rand(3))]."\"","}" ) }
	}
}














print "The number of records have to be generate: ";
our $numberofdata=<>;
print "Enter the path for the file: ";
our $fullname =<>;


print "Enter the starting sequence number: ";
$startseq=<>;
 

$separator = "|"; ################# change the separator here

$surrounder="";









open FILE, ">$fullname" or die $!; 
for (our $j=0;$j<$numberofdata;$j++){
	our @datarow=();
	push(@datarow,$j+$startseq);
		for($i=3;$i<=27;$i++){
			&selectfield($i);
		}
	$"=$surrounder.$separator.$surrounder;	
	print FILE "$surrounder@datarow$surrounder\n";

	
	#print "\"@datarow\"\n"; 
}
close FILE;
