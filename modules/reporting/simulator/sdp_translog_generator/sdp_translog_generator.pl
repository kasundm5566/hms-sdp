#!/usr/bin/perl

use feature "switch";

our @possibleValues=(["limitedLive","live","intermediate"],
["sms","cas","push","ussd"],
["http","smpp","ussd"],
["sms","cas","push","ussd"],
["http","smpp","ussd"],
["mo","mt"],
["prepaid","postpaid","unknown"],
["application","subscriber"],
["charity", "sms-mo", "cas"],
["debit","creditReservation","commitReservation","creditCancellation","creditAmount","bundleSubscription","authentication","DirectDebit","commitSuccess","commitFail","cancel"],
["success","partialSuccess","failure"],
["safaricom","orange","airtel"],
["percentageFromMonthlyRevenue", "unitCostPerMessage", "flatCostPerMonth"],
["kes","usd"],
["flat", "free", "keywordBased", "variable"]);

sub spNapp{
	my $spid;
	my $appid;

	my $spidindex = int(rand(2))+1;
	my $appidindex = 2*($spidindex - 1) + int(rand(2))+1;

		$spid = "sp".$spidindex;
		push(@datarow,$spid);
		my $temspname = "spname";#generate_random_name(5);
		$spname = $spid."_".$temspname;
		push(@datarow,$spname);


	$appid = "app".$appidindex;
	push(@datarow,$appid);
	my $temappname = "appname";#generate_random_name(5);
	$appname = $appid."_".$temappname;
	push(@datarow,$appname);
}

sub datetime{
	######################
	my $yearRange = 1; #one year
	my $startingYear = 2011; # starting from 2011

	my $monthRange = 12;   # all the 12 months
	my $startingMonth = 1; # string from January

	my $dayRange = 31; # 31 days
	my $startingDay=1; # starting from 1st
	######################
	my $yyyy = int(rand($yearRange))+$startingYear;
	my $mm	= int(rand($monthRange))+$startingMonth;
	my $dd = int(rand($dayRange))+$startingDay;
	my $HH = int(rand(24));
	my $min = int(rand(60));
	my $ss = int(rand(60));
#print "$yyyy-$mm-$dd $HH:$min:$ss";
push(@datarow,"$yyyy-$mm-$dd $HH:$min:$ss" )#"$yyyy$mm$dd$HH$min$ss"
}

sub rateCardJson{
	our @currJson=();
	my @curreJsonField = ("{\"currencyCode\":","\"buyingRate\":","\"sellingRate\":");
	my $curJarrLen =2;#int(rand(2)+1);
	if($curJarrLen !=1){
		push(@currJson,"[");
	}
	for($curJar=1;$curJar<=$curJarrLen;$curJar++){
		our $curcodeId=$curJar-1;#int(rand(4));
		for($kk=0;$kk<3;$kk++){
			push(@currJson,$curreJsonField[$kk]);
			&currencyJsonVal($kk);
		}
		if($curJar!=$curJarrLen){
				push(@currJson,",")
		}
	}
	if($curJarrLen !=1){
		push(@currJson,"]");
	}

	$"="";
	push(@datarow,"@currJson");
}

sub currencyJsonVal{
	my $arg1 = $_[0];
	my @curcoderate=(["USD","2.50","2.75"], #"110.50","112.40"
	                ["KES","1","1"]);


    given ($arg1){
		when(0)		{ push(@currJson,"\"".$curcoderate[$curcodeId][$arg1]."\"",",") }
		when(1)		{ push(@currJson,$curcoderate[$curcodeId][$arg1],",") }
		when(2)		{ push(@currJson,$curcoderate[$curcodeId][$arg1],"}") }
	}




}


sub response_code{
	my $s_f = int(rand(2));
	if($s_f == 0){
		push(@datarow,"S1000");
	}
	else{
	    my $fail_ps = int(rand(2));
	    if($fail_ps==0){
		    push(@datarow,"E".(int(rand(100))+1000));
		}else{
		    push(@datarow,"P".(int(rand(100))+1000));
		}
	}

#push(@datarow,"0"); #all are success
}

sub generate_amount{
	my $length_of_randomstring=3;
	my @chars=('1'..'9');
	my $random_amount;

    for($chAmtCnt=1;$chAmtCnt<=$length_of_randomstring;$chAmtCnt++){
	    if($chAmtCnt==2){
	    $random_amount.=".";
	    }
	    $random_amount.=$chars[rand @chars];
	}

	return $random_amount;
}

sub directionNncs{
	my @possibleNCS=(["sms","ussd"],["sms","push","cas"]);
	our $directionNo = int(rand(2));

	push(@datarow,$possibleValues[5][$directionNo]);

	if($directionNo==0){
		push(@datarow,$possibleNCS[0][int(rand(2))]);
	}
	else{
		push(@datarow,$possibleNCS[1][int(rand(3))]);
	}

}

sub multipleRecipients(){
    if($directionNo==0){
		push(@datarow,0);
    }
    else{
		push(@datarow,int(rand(2)));
    }
}





sub selectfield{
	my $val = $_[0];

	given ($val){
		when(2)		{&datetime() }
		when(3)		{&spNapp() }
		when(4)     { push(@datarow,$possibleValues[0][int(rand(3))])} #app_state
	   	when(7) 	{ push(@datarow,$possibleValues[1][int(rand(4))]) }#source_channel_type
		when(8) 	{ push(@datarow,$possibleValues[2][int(rand(3))]) }#source_channel_protocol
		when(11) 	{ push(@datarow,$possibleValues[3][int(rand(4))]) }#destination_channel_type
		when(12) 	{ push(@datarow,$possibleValues[4][int(rand(3))]) }#destination_channel_protocol
		when(13)	{ &directionNncs() }#MO MT
		when(14) 	{ push(@datarow,$possibleValues[6][int(rand(3))]) }#billing_type
		when(15) 	{ push(@datarow,$possibleValues[7][int(rand(2))]) }#charge_party_type
		when(16) 	{ push(@datarow,&generate_amount()) }  #charge amount
		when(17)	{ push(@datarow,$possibleValues[13][int(rand(2))]) }#KES or USD
		when(18)    { &rateCardJson() }                                 #rate_card
		when(19) 	{ push(@datarow,$possibleValues[8][int(rand(3))]) } #charged_category
		when(22)	{ push(@datarow,$possibleValues[9][int(rand(10))]) }# event_type
		when(23)	{ &response_code() } #response_code
		when(25)	{ push(@datarow,$possibleValues[10][int(rand(3))]) }# transaction_status
		when(28)	{ push(@datarow,$possibleValues[11][int(rand(3))]) }# operator
		when(30)	{ push(@datarow,$possibleValues[12][int(rand(3))]) }# revenue_share_type
		when(32)	{ push(@datarow,"add".int(rand(10)+1)) }#Advertisement_name
		when(34)	{ &multipleRecipients }#  is_multiple_recipients
		when(38)	{ push(@datarow,$possibleValues[14][int(rand(4))]) }# charging_type
		when(39)	{ push(@datarow,int(rand(2))) }# charging_type
		default 	{ push(@datarow,"11") }   # string "11" is a dummy value, just to keep the field as not null
	}
}


print "The number of records have to be generate: ";
our $numberofdata=<>;
print "Enter the path for the file: ";
our $fullname =<>;

print "Enter the starting sequence number: ";
our $startseq=<>;





$separator = "|"; ################# change the separator here

$surrounder="";



open FILE, ">$fullname" or die $!;



for ($j=0;$j<$numberofdata;$j++){
	our @datarow=();
	push(@datarow,$j+$startseq);
		for($i=2;$i<40;$i++){
			&selectfield($i);
		}
    $"=$surrounder.$separator.$surrounder;
	print FILE "$surrounder@datarow$surrounder\n";
	#print "@datarow\n";
}

close FILE;