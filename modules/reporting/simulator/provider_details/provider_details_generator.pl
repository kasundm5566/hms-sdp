use feature ':5.10';

################## data seperating variables############
my $column_seperator = "|";
my $row_breaker = "\n";
########################################################

my $id = 1;

print "Please enter the file path of the csv with file name\n";
my $file_path = <>;

substr($file_path, -1, 1) = '';
unless(substr($file_path, -4, 4) eq ".csv"){
    if(substr($file_path, -1, 1) eq '/'){
	$file_path = $file_path."provider_details";
    }
    my @file_path_parts = ($file_path, 'csv');
    $file_path = join('.', @file_path_parts);
}

open (FH, ">".$file_path) or die "Can't write to ".$file_path."$!";

for $i (1 .. 10){
    for $j (1 .. 5){
	print FH "sp".$i.$column_seperator;
	print FH "sp".$i."_spname".$column_seperator;
	print FH "app".$id.$column_seperator;
	print FH "app".$id."_appname".$row_breaker;
	$id++;
    }
}

close(FH);

say "file added successfully here ".$file_path;

