<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%--
  ~ (C) Copyright 2012-2014 hSenid Software International (Pvt) Limited.
  ~   All Rights Reserved.
  ~
  ~   These materials are unpublished, proprietary, confidential source code of
  ~   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
  ~   of hSenid Software International (Pvt) Limited.
  ~
  ~   hSenid Software International (Pvt) Limited retains all title to and intellectual
  ~   property rights in these materials.
  --%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><fmt:message key="title.sdp.yearly.app.operator.traffic"/></title>
    <link href="../css/dialog.sdp.css" rel="stylesheet" type="text/css"/>
    <link href="../css/jquery_chosen/chosen.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../javaScript/jquery/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../javaScript/jquery_chosen/chosen.jquery.js"></script>
    <script type="text/javascript" src="../javaScript/ddaccordion.js"></script>
    <script type="text/javascript" src="../javaScript/JavaScript.js"></script>
    <script type="text/javascript" src="../javaScript/javaScriptInSdpJSP.js"></script>
    <script type="text/javascript">
        var errmaxapp = "<fmt:message key="content.main.content.alert.max.applications"/>";
        var errnoapp = "<fmt:message key="content.main.content.alert.select.applications"/>";
        var maxApp = "<fmt:message key="maximum.app"/>";
        var spNotSelected = "<fmt:message key="content.main.content.criteria.not.added"/>";
    </script>
</head>
<body>
<div id="wrap">

<div>
    <jsp:include page="../header.jsp"/>
</div>
<div id="content">
<div class="urbangreymenu">
    <jsp:include page="../sideMenu.jsp"/>
</div>
<div id="main_content">
<div id="heading_top">
    <div class="heading_name"><fmt:message key="content.main.content.heading.sdp.yearly.app.operator.wise"/></div>
</div>
<div id="heading_middle">
<form id="form1" name="form1" method="get" action="<%= request.getContextPath( ) + "/frameset" %>"
      target="_blank"
      onSubmit="return appOperatorAppSelection(this, ${type}, errmaxapp, errnoapp, maxApp, spNotSelected);">
<table width="670" border="0" align="center" cellspacing="6">
<tr>
    <td><input type='hidden' name='__report'
               value="report/sdp/sdp_yearly_app_operator_traffic_report.rptdesign"></td>
    <td></td>
    <td><input type='hidden' name='RP_app_op_years' id="RP_app_op_years" value=""></td>
    <c:choose>
        <c:when test="${type == 1}">
            <td><input type='hidden' name='RP_app_op_sp_id' value=""></td>
            <td><input type='hidden' name='RP_app_op_sp_name' value=""></td>
        </c:when>
        <c:otherwise>
            <td><input type='hidden' name='RP_app_op_sp_id' value="${spId}"></td>
            <td><input type='hidden' name='RP_app_op_sp_name' value="${spName}"></td>
        </c:otherwise>
    </c:choose>

    <td><input type='hidden' name='RP_app_op_select_all_apps' id="RP_app_op_select_all_apps" value=""></td>
    <td><input type='hidden' name='RP_app_op_app_id' id="RP_app_op_app_id" value=""></td>
    <td><input type='hidden' name='RP_app_op_app_name' id="RP_app_op_app_name" value=""></td>
    <td><input type='hidden' name='RP_app_op_ncs' id="RP_app_op_ncs" value=""></td>
    <td><input type='hidden' name='RP_app_op_dir' id="RP_app_op_dir" value=""></td>
    <!--<td><input type='hidden' name='__overwrite' value="true"></td>
    <td><input type='hidden' name='__locale' value="en_US"></td>
    <td><input type='hidden' name='__masterpage' value="true"></td>-->
</tr>
<tr>
    <td>&nbsp;</td>
    <td id="yearAlert" class="alert">
        <fmt:message key="content.main.content.alert.select.years"/>
    </td>
</tr>
<tr>
<tr>
    <td width="150"><fmt:message key="content.main.content.year(s)"/><a class="star"> *</a></td>
    <td><label>
        <select name="years" id="years" multiple="multiple" size="2" class="select-box" style="height: 35px;"
                onchange="setSortingType('year')">
            <option value="<c:out value="${previousYear}"/>"
                    >${previousYear}</option>
            <option value="<c:out value="${currentYear}"/>" selected="selected">${currentYear}</option>
        </select>
    </label></td>
</tr>
<c:if test="${type == 1}">
    <tr>
        <td>&nbsp;</td>
        <td id="spAlert" class="alert">
            <fmt:message key="content.main.content.alert.select.a.corporate.user"/>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td width="150"><fmt:message key="content.main.content.corporate.user"/>
        </td>
        <td><label>

            <select name="providers" id="providers"
                    data-placeholder='<fmt:message key="content.main.content.select.item"/>'
                    class="chosen-select select-box"
                    onchange="setApplicationsList()"
                    tabindex="2">
                <option value=""><fmt:message key="content.main.content.select.item"/></option>
                <c:forEach items="${spDetails}" var="details">
                    <option value="<c:out value="${details.sp_id}+${details.sp_name}"/>">
                            ${details.sp_name} (${details.sp_id})
                    </option>
                </c:forEach>
            </select>

            <br/>
        </label></td>
    </tr>
</c:if>
<tr>
    <td>&nbsp;</td>
    <td id="appAlert" class="alert">
        <fmt:message key="content.main.content.alert.select.applications"/>
    </td>
    <td>&nbsp;</td>
</tr>
<c:choose>
    <c:when test="${type == 2}">
        <tr>
            <td width="150"><fmt:message key="content.main.content.application(s)"/><a
                    class="star">
                *</a>
            </td>
            <td>
                <input type="checkbox" id="selectAll" name="selectAll" onchange="disableAppsDropDown()"/>

                <label>

                    <fmt:message key="content.main.content.selectAll"/>
                </label>
            </td>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <td width="150">&nbsp;
            </td>
            <td><label>
                <select name="apps" id="apps" data-placeholder='<fmt:message key="content.main.content.select.item"/>'
                        class="chosen-select select-box" multiple tabindex="4"
                        onchange="appAlertNeutralize()">
                    <c:forEach items="${apps}" var="app">
                        <option value="<c:out value="${app.app_id}+${app.app_name}"/>">
                                ${app.app_id} / ${app.app_name}
                        </option>
                    </c:forEach>
                </select>
            </label></td>
        </tr>
    </c:when>
    <c:otherwise>
        <tr>
            <td width="150"><fmt:message key="content.main.content.application(s)"/><a
                    class="star">
                *</a>
            </td>
            <td>
                <input type="checkbox" id="selectAll" name="selectAll" onchange="disableAppsDropDown()"/>

                <label>

                    <fmt:message key="content.main.content.selectAll"/>
                </label>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td width="150">&nbsp;
            </td>
            <td><label>
                <select name="apps" id="apps" data-placeholder='<fmt:message key="content.main.content.select.item"/>'
                        class="chosen-select select-box" multiple tabindex="4" onchange="appAlertNeutralize()">
                    <c:forEach items="${apps}" var="app">
                        <option value="<c:out value="${app.app_id}+${app.app_name}"/>">
                                ${app.app_id} / ${app.app_name}
                        </option>
                    </c:forEach>
                </select>
            </label></td>
        </tr>
    </c:otherwise>
</c:choose>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td width="150"><fmt:message key="content.main.content.ncs.type"/></td>
        <td><label>
            <select name="ncsTypes" id="ncsTypes"
                    data-placeholder='<fmt:message key="content.main.content.select.item"/>'
                    class="chosen-select select-box"
                    multiple  tabindex="4">
                <c:forEach items="${ncsTypes}" var="ncsType">
                    <option value="${ncsType.key}">${ncsType.value}</option>
                </c:forEach>
            </select>
            <br/>
        </label></td>
    </tr>
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td width="150">&nbsp;</td>
    <td><input type="image" src="../images/button.gif" width="63" height="25"
               border="0" alt="submit"/></td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
</table>
</form>
</div>
<div id="heading_bottom"></div>
</div>
</div>
<div id="footer">
    <jsp:include page="../footer.jsp"/>
</div>
</div>
<script type="text/javascript">
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
</body>
</html>
