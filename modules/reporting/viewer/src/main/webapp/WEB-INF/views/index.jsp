<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><fmt:message key="title.index"/></title>
    <!--shortcut icon -->
    <link rel="shortcut icon" href="../images/favicon.ico" />
    <link rel="icon" type="image/ico" href="../images/favicon.ico" />

    <link href="../css/dialog.sdp.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../javaScript/jquery.min.js"></script>
    <script type="text/javascript" src="../javaScript/ddaccordion.js"></script>
    <script type="text/javascript" src="../javaScript/JavaScript.js"></script>
</head>
<body>


    <div>
        <jsp:include page="header.jsp"/>
    </div>
<div id="wrap">
    <div id="content">
        <div class="urbangreymenu">
            <jsp:include page="sideMenu.jsp"/>
        </div>
        <div id="main_content"><img src="../images/main_banner_1.jpg" width="688" height="312"/></div>
    </div>
    <div id="footer">
        <jsp:include page="footer.jsp"/>
    </div>
</div>
</body>
</html>
