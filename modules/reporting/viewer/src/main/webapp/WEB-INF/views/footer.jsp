<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head></head>
<body>
<div align="left">
    <div class="footer_heading" align="center" style="float:none;margin-left: 250px;"><fmt:message
            key="footer.heading.copyright.statement"/> | <a href='<fmt:message key="footer.tc.tag.url"/>'><fmt:message key='footer.tc.tag'/> </a> </div>
</div>
</body>
</html>