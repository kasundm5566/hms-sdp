<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><fmt:message key="title.sdp.daily.app.operator.traffic"/></title>
    <link href="../css/jquery.ui.all.css" rel="stylesheet" type="text/css"/>
    <link href="../css/dialog.sdp.css" rel="stylesheet" type="text/css"/>
    <link href="../css/jquery_chosen/chosen.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../javaScript/jquery/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../javaScript/jquery_chosen/chosen.jquery.js"></script>
    <script type="text/javascript" src="../javaScript/ddaccordion.js"></script>
    <script type="text/javascript" src="../javaScript/JavaScript.js"></script>
    <script type="text/javascript" src="../javaScript/javaScriptInSdpJSP.js"></script>
    <script type="text/javascript" src="../javaScript/jquery.ui.core.js"></script>
    <script type="text/javascript" src="../javaScript/jquery.ui.datepicker.js"></script>
    <script type="text/javascript">
        var err1 = "<fmt:message key="content.main.content.alert.empty.date"/>";
        var err2 = "<fmt:message key="content.main.content.alert.invalid.date"/>";
        var err3 = "<fmt:message key="content.main.content.alert.future.date"/>";
        var err4 = "<fmt:message key="content.main.content.alert.to.date"/>";
        var err5 = "<fmt:message key="content.main.content.alert.invalid.date.range"/>";
        var err6 = "<fmt:message key ="content.main.content.alert.future.to.date"/>";
        var errmaxapp = "<fmt:message key="content.main.content.alert.max.applications"/>";
        var errnoapp = "<fmt:message key="content.main.content.alert.select.applications"/>";
        var maxApp = "<fmt:message key="maximum.app"/>";
        var spNotSelected = "<fmt:message key="content.main.content.criteria.not.added"/>";
    </script>
</head>
<body>
<div id="wrap">

    <div>
        <jsp:include page="../header.jsp"/>
    </div>
    <div id="content">
        <div class="urbangreymenu">
            <jsp:include page="../sideMenu.jsp"/>
        </div>
        <div id="main_content">
            <div id="heading_top">
                <div class="heading_name"><fmt:message
                        key="content.main.content.heading.sdp.daily.app.operator.wise"/></div>
            </div>
            <div id="heading_middle">
                <form name="dailyIncomeReportName" method="get" action="<%= request.getContextPath( ) + "/frameset" %>"
                      target="_blank"
                      onsubmit="return appOperatorDailyAppSelection(this, ${type}, err1, err2, err3, err4, err5, errmaxapp, errnoapp, maxApp, err6, spNotSelected);">
                    <table width="670" border="0" align="center" cellspacing="6">
                        <tr>
                            <td><input type='hidden' name='__report'
                                       value="report/sdp/sdp_daily_app_operator_traffic_report.rptdesign"></td>
                            <td><input type='hidden' name='RP_app_op_daily_start_date' id="RP_app_op_daily_start_date"
                                       value=""></td>
                            <td><input type='hidden' name='RP_app_op_daily_end_date' id="RP_app_op_daily_end_date"
                                       value="">
                            </td>
                            <c:choose>
                                <c:when test="${type == 1}">
                                    <td><input type='hidden' name='RP_app_op_daily_sp_id' value=""></td>
                                    <td><input type='hidden' name='RP_app_op_daily_sp_name' value=""></td>
                                </c:when>
                                <c:otherwise>
                                    <td><input type='hidden' name='RP_app_op_daily_sp_id' value="${spId}"></td>
                                    <td><input type='hidden' name='RP_app_op_daily_sp_name' value="${spName}"></td>
                                </c:otherwise>
                            </c:choose>
                            <td><input type='hidden' name='RP_app_op_daily_app_name' id="RP_app_op_daily_app_name"
                                       value="">
                            </td>
                            <td><input type='hidden' name='RP_app_op_daily_app_id' id="RP_app_op_daily_app_id" value="">
                            </td>
                            <td><input type='hidden' name='RP_app_op_daily_select_all_apps'
                                       id="RP_app_op_daily_select_all_apps" value=""></td>
                            <td><input type='hidden' name='RP_app_op_daily_ncs' id="RP_app_op_daily_ncs" value=""></td>
                            <td><input type='hidden' name='RP_app_op_daily_dir' id="RP_app_op_daily_dir" value=""></td>
                            <%--<td><input type='hidden' name='__overwrite' value="true"></td>

                            <td><input type='hidden' name='__locale' value="en_US"></td>

                            <td><input type='hidden' name='__masterpage' value="true"></td>--%>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td id="dateAlert" class="alert">
                                <fmt:message key="content.main.content.alert.invalid.date"/>
                            </td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.from.date"/><a class="star"> *</a>
                            </td>
                            <td><input type="text" name="fromDate" id="datepicker1" class="input-box"
                                       onchange="datePickAlertNeutralize()"></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td width="150"><fmt:message key="content.main.content.to.date"/><a class="star"> *</a></td>
                            <td><input type="text" name="toDate" id="datepicker2" class="input-box"
                                       onchange="datePickAlertNeutralize()"></td>
                            <td>&nbsp;</td>
                        </tr>
                        <c:if test="${type == 1}">
                            <tr>
                                <td>&nbsp;</td>
                                <td id="spAlert" class="alert">
                                    <fmt:message key="content.main.content.alert.select.a.corporate.user"/>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="150"><fmt:message key="content.main.content.corporate.user"/>
                                </td>
                                <td>
                                    <label>
                                        <select name="providers" id="providers"
                                                data-placeholder='<fmt:message key="content.main.content.select.item"/>'
                                                class="chosen-select select-box"
                                                onchange="setApplications()" tabindex="2">
                                            <option value=""><fmt:message
                                                    key="content.main.content.select.item"/></option>
                                            <c:forEach items="${spDetails}" var="details">
                                                <option value="<c:out value="${details.sp_id}+${details.sp_name}"/>">
                                                        ${details.sp_name} (${details.sp_id})
                                                </option>
                                            </c:forEach>
                                        </select>
                                    </label>
                                </td>
                            </tr>
                        </c:if>
                        <tr>
                            <td>&nbsp;</td>
                            <td id="appAlert" class="alert">
                                <fmt:message key="content.main.content.alert.select.applications"/>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <c:choose>
                            <c:when test="${type == 2}">
                                <tr>
                                    <td width="150"><fmt:message key="content.main.content.application(s)"/><a
                                            class="star">
                                        *</a>
                                    </td>
                                    <td>
                                        <input type="checkbox" id="selectAll" name="selectAll"
                                               onchange="disableAppsDropDown()"/>
                                        <label>
                                            <fmt:message key="content.main.content.selectAll"/>
                                        </label>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="150">&nbsp;
                                    </td>
                                    <td><label>
                                        <select name="apps" id="apps"
                                                data-placeholder='<fmt:message key="content.main.content.select.item"/>'
                                                class="chosen-select select-box" multiple tabindex="4"
                                                onchange="appAlertNeutralize()">
                                            <c:forEach items="${apps}" var="app">
                                                <option value="<c:out value="${app.app_id}+${app.app_name}"/>">
                                                        ${app.app_id}/${app.app_name}
                                                </option>
                                            </c:forEach>
                                        </select>
                                    </label></td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <tr>
                                    <td width="150"><fmt:message key="content.main.content.application(s)"/><a
                                            class="star">
                                        *</a>
                                    </td>
                                    <td>
                                        <input type="checkbox" id="selectAll" name="selectAll"
                                               onchange="disableAppsDropDown()"/>
                                        <label>
                                            <fmt:message key="content.main.content.selectAll"/>
                                        </label>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="150">&nbsp;
                                    </td>
                                    <td><label>
                                        <select name="apps" id="apps"
                                                data-placeholder='<fmt:message key="content.main.content.select.item"/>'
                                                class="chosen-select select-box" multiple tabindex="4"
                                                onchange="appAlertNeutralize()">
                                            <c:forEach items="${apps}" var="app">
                                                <option value="<c:out value="${app.app_id}+${app.app_name}"/>">
                                                        ${app.app_id} / ${app.app_name}
                                                </option>
                                            </c:forEach>
                                        </select>
                                    </label></td>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.ncs.type"/></td>
                            <td><label>
                                <select name="ncsTypes" id="ncsTypes"
                                        data-placeholder='<fmt:message key="content.main.content.select.item"/>'
                                        class="chosen-select select-box"
                                        multiple>
                                    <c:forEach items="${ncsTypes}" var="ncsType">
                                        <option value="${ncsType.key}">${ncsType.value}</option>
                                    </c:forEach>
                                </select>
                                <br/>
                            </label></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150">&nbsp;</td>
                            <td><input type="image" src="../images/button.gif" width="63" height="25"
                                       border="0" alt="submit"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </form>
            </div>
            <div id="heading_bottom"></div>
        </div>
    </div>
    <div id="footer">
        <jsp:include page="../footer.jsp"/>
    </div>
</div>
</body>
<script type="text/javascript">
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
</html>
