<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title></title>
</head>
<body>
<div class="urbangreymenu">
    <%--<h3 class="headerbar"><a href=""><fmt:message key="content.left.pgw.reports"/></a></h3>
    <ul class="submenu">
        <li><a href="#"><fmt:message key="content.left.pgw.reports.yearly.income"/></a></li>
        <li><a href="#"><fmt:message key="content.left.pgw.reports.monthly.income"/></a></li>
        <li><a href="#"><fmt:message key="content.left.pgw.reports.daily.income"/></a></li>
        <li><a href="#"><fmt:message key="content.left.pgw.reports.detailed.income"/></a></li>
        <li><a href="#"><fmt:message key="content.left.pgw.reports.yearly.transactions"/></a></li>
        <li><a href="#"><fmt:message key="content.left.pgw.reports.monthly.transactions"/></a></li>
        <li><a href="#"><fmt:message key="content.left.pgw.reports.daily.transactions"/></a></li>
        <li><a href="#"><fmt:message key="content.left.pgw.reports.detailed.transactions"/></a></li>
        <li><a href="#"><fmt:message key="content.left.pgw.reports.dashboard.reports"/></a></li>
    </ul>--%>
    <sec:authorize ifAnyGranted="ROLE_PGW_RPT_PAYMENT_INSTRUMENT_REVENUE">
        <h3 class="headerbar"><a href=""><fmt:message key="content.left.pgw.payement.instrument.basedreports"/></a></h3>
        <ul class="submenu">
            <li><a href="pgwPIYearlyTransactions.jsp"><fmt:message
                key="content.left.pgw.reports.payment.instrument.based.yearly"/></a></li>
            <li><a href="pgwPIMonthlyTransactions.jsp"><fmt:message
                key="content.left.pgw.reports.payment.instrument.based.monthly"/></a></li>
            <li><a href="pgwPaymentInstrumentBasedDaily.jsp"><fmt:message
                key="content.left.pgw.reports.payment.instrument.based.daily"/></a></li>
        </ul>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_SDP_RPT_OVERALL_REV_TRAFF">
        <h3 class="headerbar">
            <a href=""><fmt:message key="content.left.sdp.reports.overall"/></a>
        </h3>
        <ul class="submenu">
            <li><a href="sdpYearlyRevenue.jsp"><fmt:message
                    key="content.left.sdp.reports.yearly.revenue.traffic"/></a></li>
            <li><a href="sdpMonthlyRevenue.jsp"><fmt:message
                    key="content.left.sdp.reports.monthly.revenue.traffic"/></a></li>
            <li><a href="sdpDailyRevenue.jsp"><fmt:message
                    key="content.left.sdp.reports.daily.revenue.traffic"/></a></li>
        </ul>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_SDP_RPT_SOLTURA">
        <h3 class="headerbar">
            <a href=""><fmt:message key="content.left.sdp.reports.soltura"/></a>
        </h3>
        <ul class="submenu">
            <li><a href="sdpYearlySoltura.jsp"><fmt:message
                    key="content.left.sdp.reports.yearly.soltura"/></a></li>
            <li><a href="sdpMonthlySoltura.jsp"><fmt:message
                    key="content.left.sdp.reports.monthly.soltura"/></a></li>
            <li><a href="sdpDailySoltura.jsp"><fmt:message
                    key="content.left.sdp.reports.daily.soltura"/></a></li>
        </ul>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_SDP_RPT_DOWNLOADABLE">
        <h3 class="headerbar">
            <a href=""><fmt:message key="content.left.sdp.reports.downloadable"/></a>
        </h3>
        <ul class="submenu">
            <li><a href="sdpYearlyDownloadable.jsp"><fmt:message
                    key="content.left.sdp.reports.yearly.downloadable"/></a></li>
            <li><a href="sdpMonthlyDownloadable.jsp"><fmt:message
                    key="content.left.sdp.reports.monthly.downloadable"/></a></li>
            <li><a href="sdpDailyDownloadable.jsp"><fmt:message
                    key="content.left.sdp.reports.daily.downloadable"/></a></li>
        </ul>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_SDP_RPT_CORPORATE_REV_TRAFF">
        <h3 class="headerbar">
            <a href=""><fmt:message key="content.left.sdp.reports.corporate"/></a>
        </h3>
        <ul class="submenu">
            <li><a href="sdpYearlyCorporate.jsp"><fmt:message
                    key="content.left.sdp.reports.yearly.corporate.revenue"/></a></li>
            <li><a href="sdpMonthlyCorporate.jsp"><fmt:message
                    key="content.left.sdp.reports.monthly.corporate.revenue"/></a></li>
            <li><a href="sdpDailyCorporate.jsp"><fmt:message
                    key="content.left.sdp.reports.daily.corporate.revenue"/></a></li>
        </ul>
    </sec:authorize>

    <sec:authorize ifAnyGranted="ROLE_SDP_RPT_APP_REV_TRAFF">
        <h3 class="headerbar">
            <a href=""><fmt:message key="content.left.sdp.reports.app"/></a>
        </h3>
        <ul class="submenu">
            <li><a href="sdpYearlyAppRevenue.jsp"><fmt:message
                    key="content.left.sdp.reports.yearly.app.revenue"/></a></li>
            <li><a href="sdpMonthlyAppRevenue.jsp"><fmt:message
                    key="content.left.sdp.reports.monthly.app.revenue"/></a></li>
            <li><a href="sdpDailyAppRevenue.jsp"><fmt:message
                    key="content.left.sdp.reports.daily.app.revenue"/></a></li>
        </ul>
    </sec:authorize>

    <sec:authorize ifAnyGranted="ROLE_SDP_RPT_OPERATOR_REV_TRAFF">
        <h3 class="headerbar">
            <a href=""><fmt:message key="content.left.sdp.reports.operator"/></a>
        </h3>
        <ul class="submenu">
            <li><a href="sdpYearlyOperatorRevenue.jsp"><fmt:message
                    key="content.left.sdp.reports.yearly.operator.revenue"/></a></li>
            <li><a href="sdpMonthlyOperatorRevenue.jsp"><fmt:message
                    key="content.left.sdp.reports.monthly.operator.revenue"/></a></li>
            <li><a href="sdpDailyOperatorRevenue.jsp"><fmt:message
                    key="content.left.sdp.reports.daily.operator.revenue"/></a></li>
        </ul>
    </sec:authorize>

    <sec:authorize ifAnyGranted="ROLE_SDP_RPT_APP_OPERATOR_TRAFF">
        <h3 class="headerbar">
            <a href=""><fmt:message key="content.left.sdp.reports.app.operator"/></a>
        </h3>
        <ul class="submenu">
            <li><a href="sdpYearlyAppOperatorTraffic.jsp"><fmt:message
                    key="content.left.sdp.reports.yearly.app.operator.wise"/></a></li>
            <li><a href="sdpMonthlyAppOperatorTraffic.jsp"><fmt:message
                    key="content.left.sdp.reports.monthly.app.operator.wise"/></a></li>
            <li><a href="sdpDailyAppOperatorTraffic.jsp"><fmt:message
                    key="content.left.sdp.reports.daily.app.operator.wise"/></a></li>
        </ul>
    </sec:authorize>

    <sec:authorize ifAnyGranted="ROLE_SDP_RPT_NCS_REV_TRAFF">
        <h3 class="headerbar">
            <a href=""><fmt:message key="content.left.sdp.reports.ncs"/></a>
        </h3>
        <ul class="submenu">
            <li><a href="sdpYearlyNCSRevenue.jsp"><fmt:message
                    key="content.left.sdp.reports.yearly.ncs.revenue"/></a></li>
            <li><a href="sdpMonthlyNCSRevenue.jsp"><fmt:message
                    key="content.left.sdp.reports.monthly.ncs.revenue"/></a></li>
            <li><a href="sdpDailyNCSRevenue.jsp"><fmt:message
                    key="content.left.sdp.reports.daily.ncs.revenue"/></a></li>
        </ul>
    </sec:authorize>

    <sec:authorize ifAnyGranted="ROLE_SDP_RPT_FAILED_TRANS">
        <h3 class="headerbar">
            <a href=""><fmt:message key="content.left.sdp.reports.failed"/></a>
        </h3>
        <ul class="submenu">
            <li><a href="sdpFailedTransaction.jsp"><fmt:message
                    key="content.left.sdp.reports.failed.transaction"/></a></li>
        </ul>
    </sec:authorize>

    <sec:authorize ifAnyGranted="ROLE_SDP_RPT_ADV_AD_WISE">
        <h3 class="headerbar">
            <a href=""><fmt:message key="content.left.sdp.reports.add"/></a>
        </h3>
        <ul class="submenu">
            <li><a href="sdpYearlyAddWise.jsp"><fmt:message
                    key="content.left.sdp.reports.yearly.addwise"/></a></li>
            <li><a href="sdpMonthlyAddWise.jsp"><fmt:message
                    key="content.left.sdp.reports.monthly.addwise"/></a></li>
        </ul>
    </sec:authorize>

    <sec:authorize ifAnyGranted="ROLE_SDP_RPT_APP_WISE">
        <h3 class="headerbar">
            <a href=""><fmt:message
                    key="content.left.sdp.reports.app.wise.add"/></a>
        </h3>
        <ul class="submenu">
            <li><a href="sdpYearlyAppWiseAdd.jsp"><fmt:message
                    key="content.left.sdp.reports.yearly.appwise.add"/></a></li>
            <li><a href="sdpMonthlyAppWiseAdd.jsp"><fmt:message
                    key="content.left.sdp.reports.monthly.appwise.add"/></a></li>
        </ul>
    </sec:authorize>

    <sec:authorize ifAnyGranted="ROLE_SDP_RPT_CC_CRE_REP,ROLE_SDP_RPT_CC_DETAILED_TRANS,ROLE_SDP_RPT_CC_FAILED_TRANS">
        <h3 class="headerbar">
            <a href=""><fmt:message key="content.left.customer.care.reports"/></a>
        </h3>
        <ul class="submenu">
            <sec:authorize ifAnyGranted="ROLE_SDP_RPT_CC_DETAILED_TRANS">
                <li><a href="customerCareDetailedTransaction.jsp"><fmt:message
                        key="content.left.customer.care.reports.detailed.transactions"/></a></li>
            </sec:authorize>
            <sec:authorize ifAnyGranted="ROLE_SDP_RPT_CC_FAILED_TRANS">
                <li><a href="customerCareFailedTransaction.jsp"><fmt:message
                        key="content.left.customer.care.reports.failed.transactions"/></a></li>
            </sec:authorize>
            <sec:authorize ifAnyGranted="ROLE_PGW_RPT_DETAIL__TRANS">
                <li><a href="pgwDetailedTransaction.jsp"><fmt:message
                        key="content.left.pgw.reports.detailed.transactions"/></a></li>
            </sec:authorize>
            <sec:authorize ifAnyGranted="ROLE_SDP_RPT_CC_CRE_REP">
                <li><a href="https://dev.sdp.hsenidmobile.com/admin#cc.search"><fmt:message
                        key="content.left.customer.care.reports.admin.report"/></a></li>
            </sec:authorize>
            <sec:authorize ifAnyGranted="ROLE_APP_WISE_SUBSCRIBED_SUBSCRIBER">
                <li><a href="appWiseSubscribedSubscriberReport.jsp"><fmt:message
                        key="content.left.customer.care.reports.app.wise.subscribed.subscriber"/></a></li>
            </sec:authorize>
        </ul>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_SDP_RPT_REC_REP">
        <h3 class="headerbar">
            <a href=""><fmt:message
                    key="content.left.reconciliation.reports"/></a>
        </h3>
        <ul class="submenu">
            <li><a href="sdpReconciliation.jsp"><fmt:message
                    key="content.left.sdp.reports.sdp.reconciliation"/></a></li>
        </ul>
    </sec:authorize>

    <sec:authorize ifAnyGranted="ROLE_SDP_RPT_GOV_REP">
        <h3 class="headerbar">
            <a href=""><fmt:message
                    key="content.left.sdp.reports.sdp.governance"/></a>
        </h3>
        <ul class="submenu">
            <li><a href="sdpGovernance.jsp"><fmt:message
                    key="content.left.governance.report"/></a></li>
        </ul>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_SDP_RPT_MESSAGE_HISTORY">
        <h3 class="headerbar">
            <a href=""><fmt:message key="content.left.sdp.reports.message.history"/></a>
        </h3>
        <ul class="submenu">
            <li><a href="sdpMessageHistory.jsp"><fmt:message
                    key="content.left.sdp.reports.daily.message.history"/></a></li>
        </ul>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_SDP_RPT_SUBMISSION_REP">
        <h3 class="headerbar">
            <a href=""><fmt:message key="content.left.sdp.reports.sdp.sumbissions"/></a>
        </h3>
        <ul class="submenu">
            <li><a href="sdpYearlySubmissions.jsp"><fmt:message key="content.left.yearly.submissions.report"/></a></li>
            <li><a href="sdpMonthlySubmissions.jsp"><fmt:message key="content.left.monthly.submissions.report"/></a></li>
        </ul>
    </sec:authorize>

</div>
</body>
</html>