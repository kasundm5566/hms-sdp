<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><fmt:message key="title.sdp.yearly.addwise"/></title>
    <link href="../css/dialog.sdp.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../javaScript/jquery.min.js"></script>
    <script type="text/javascript" src="../javaScript/ddaccordion.js"></script>
    <script type="text/javascript" src="../javaScript/JavaScript.js"></script>
    <script type="text/javascript" src="../javaScript/javaScriptInSdpJSP.js"></script>
</head>
<body>
<div id="wrap">

    <div>
        <jsp:include page="../header.jsp"/>
    </div>
    <div id="content">
        <div class="urbangreymenu">
            <jsp:include page="../sideMenu.jsp"/>
        </div>
        <div id="main_content">
            <div id="heading_top">
                <div class="heading_name"><fmt:message key="content.main.content.heading.sdp.yearly.addwise"/></div>
            </div>
            <div id="heading_middle">
                <form id="form1" name="form1" method="get" action="<%= request.getContextPath( ) + "/frameset" %>"
                      target="_blank"
                      onSubmit="return yearlyAdd();">
                    <table width="670" border="0" align="center" cellspacing="6">
                        <tr>
                            <td><input type='hidden' name='__report'
                                       value="report/sdp/yearly_ad_wise_report.rptdesign"></td>
                            <td></td>
                            <td><input type='hidden' name='RP_yearly_add_year' id="RP_yearly_add_year" value=""></td>
                            <td><input type='hidden' name='RP_yearly_add_addname' id="RP_yearly_add_addname" value="">
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.year"/><a class="star"> *</a></td>
                            <td><label>
                                <select name="years" id="years" class="list">
                                    <option value="<c:out value="${previousYear}"/>">${previousYear}</option>
                                    <option value="<c:out value="${currentYear}"/>" selected="selected">${currentYear}</option>
                                </select>
                            </label></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td id="addAlert" class="alert">
                                <fmt:message key="content.main.content.alert.select.advertisements"/>
                            </td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.advertisement(s)"/><a class="star">
                                *</a></td>
                            <td><label>
                                <select name="addNames" id="addNames" class="item3list" multiple="multiple"
                                        onchange="addAlertNeutralize()">
                                    <c:forEach items="${aDDDetail}" var="addDetail">
                                        <option value="<c:out value="${addDetail.add_name}"/>"
                                                selected="selected">${addDetail.add_name}</option>
                                    </c:forEach>
                                </select>

                            </label></td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150">&nbsp;</td>
                            <td><input type="image" src="../images/button.gif" width="63" height="25"
                                       border="0" alt="submit"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </form>
            </div>
            <div id="heading_bottom"></div>
        </div>
    </div>
    <div id="footer">
        <jsp:include page="../footer.jsp"/>
    </div>
</div>
</body>
</html>
