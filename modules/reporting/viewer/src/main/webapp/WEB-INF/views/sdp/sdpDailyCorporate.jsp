<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><fmt:message key="title.sdp.daily.corporate.revenue"/></title>
    <link href="../css/jquery.ui.all.css" rel="stylesheet" type="text/css"/>
    <link href="../css/dialog.sdp.css" rel="stylesheet" type="text/css"/>
    <link href="../css/jquery_chosen/chosen.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../javaScript/jquery/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../javaScript/jquery_chosen/chosen.jquery.js"></script>
    <script type="text/javascript" src="../javaScript/ddaccordion.js"></script>
    <script type="text/javascript" src="../javaScript/JavaScript.js"></script>
    <script type="text/javascript" src="../javaScript/javaScriptInSdpJSP.js"></script>
    <script type="text/javascript" src="../javaScript/jquery.ui.core.js"></script>
    <script type="text/javascript" src="../javaScript/jquery.ui.datepicker.js"></script>
    <script type="text/javascript">
        var err1 = "<fmt:message key="content.main.content.alert.empty.date"/>";
        var err2 = "<fmt:message key="content.main.content.alert.invalid.date"/>";
        var err3 = "<fmt:message key="content.main.content.alert.future.date"/>";
        var err4 = "<fmt:message key="content.main.content.alert.to.date"/>";
        var err5 = "<fmt:message key="content.main.content.alert.invalid.date.range"/>";
        var err6 = "<fmt:message key ="content.main.content.alert.future.to.date"/>";
        var errmaxsp = "<fmt:message key="content.main.content.alert.select.max.user"/>";
        var errnosp = "<fmt:message key="content.main.content.alert.select.a.corporate.user"/>";
        var maxSp = "<fmt:message key="maximum.sp"/>";
    </script>

</head>
<body>
<div id="wrap">

    <div>
        <jsp:include page="../header.jsp"/>
    </div>
    <div id="content">
        <div class="urbangreymenu">
            <jsp:include page="../sideMenu.jsp"/>
        </div>
        <div id="main_content">
            <div id="heading_top">
                <div class="heading_name"><fmt:message
                        key="content.main.content.heading.sdp.daily.corporate.revenue"/></div>
            </div>
            <div id="heading_middle">
                <form name="dailyIncomeReportName" method="get" action="<%= request.getContextPath( ) + "/frameset" %>"
                      target="_blank"
                      onsubmit="return dailyCorporate(this,err1,err2,err3,err4,err5,errmaxsp,errnosp,maxSp,err6);">
                    <table width="670" border="0" align="center" cellspacing="6">
                        <tr>
                            <td><input type='hidden' name='__report'
                                       value="report/sdp/sdp_daily_corporateuser_revenue_and_traffic_report.rptdesign">
                            </td>
                            <td><input type='hidden' name='cop_user_based_daily_rp_from_date' value=""></td>
                            <td><input type='hidden' name='cop_user_based_daily_rp_to_date' value=""></td>
                            <td><input type='hidden' name='cop_user_based_daily_rp_sp_ids' value=""></td>
                            <td><input type='hidden' name='cop_user_based_daily_rp_sp_names'
                                       value=""></td>
                            <td><input type='hidden' name='cop_user_based_daily_rp_show_report' value=""></td>

                            <%--<td><input type='hidden' name='__overwrite' value="true"></td>

                            <td><input type='hidden' name='__locale' value="en_US"></td>

                            <td><input type='hidden' name='__masterpage' value="true"></td>--%>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td id="dateAlert" class="alert">
                                <fmt:message key="content.main.content.alert.invalid.date"/>
                            </td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.from.date"/><a class="star"> *</a>
                            </td>
                            <td><input type="text" name="fromDate" id="datepicker1" class="input-box"
                                       onchange="datePickAlertNeutralize()"></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.to.date"/><a class="star"> *</a></td>
                            <td><input type="text" name="toDate" id="datepicker2" class="input-box"
                                       onchange="datePickAlertNeutralize()"></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td id="spAlert" class="alert">
                                <fmt:message key="content.main.content.alert.select.corporate.users"/>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.corporate.user(s)"/><a class="star">
                                *</a></td>
                            <td><label>
                                <select id="providers" class="chosen-select select-box" multiple="multiple"
                                        onchange="spAlertNeutralize()">
                                    <c:forEach items="${spDetails}" var="details">
                                        <option value="<c:out value="${details.sp_id}+${details.sp_name}"/>"
                                                >${details.sp_name} (${details.sp_id})
                                        </option>
                                    </c:forEach>
                                </select>
                            </label></td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td width="150"><fmt:message key="content.main.content.show.report.chart"/></td>
                            <td><label>
                                <input type="checkbox" name="chkShowChart" checked=checked value="1"/>
                            </label></td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td width="150">&nbsp;</td>
                            <td><input type="image" src="../images/button.gif" width="63" height="25"
                                       border="0" alt="submit"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </form>
            </div>
            <div id="heading_bottom"></div>
        </div>
    </div>
    <div id="footer">
        <jsp:include page="../footer.jsp"/>
    </div>
</div>
</body>
<script type="text/javascript">
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
</html>
