<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><fmt:message key="title.appwise.subscribed.subscriber.report"/></title>
    <link href="../css/jquery.ui.all.css" rel="stylesheet" type="text/css"/>
    <link href="../css/dialog.sdp.css" rel="stylesheet" type="text/css"/>
    <link href="../css/jquery_chosen/chosen.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../javaScript/jquery/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../javaScript/jquery_chosen/chosen.jquery.js"></script>
    <script type="text/javascript" src="../javaScript/ddaccordion.js"></script>
    <script type="text/javascript" src="../javaScript/javaScriptInCCJSP-vdf-1.1.87.js"></script>
    <script type="text/javascript" src="../javaScript/javaScriptInSdpJSP.js"></script>
    <script type="text/javascript" src="../javaScript/JavaScript.js"></script>
    <script type="text/javascript" src="../javaScript/jquery.ui.core.js"></script>
    <script type="text/javascript" src="../javaScript/jquery.ui.datepicker.js"></script>
    <script type="text/javascript">
        var errnoapp = "<fmt:message key="content.main.content.alert.select.applications"/>";
        var maxApp = "<fmt:message key="maximum.app"/>";
        var spNotSelected = "<fmt:message key="content.main.content.criteria.not.added"/>";
    </script>
</head>
<body>
<div id="wrap">

    <div>
        <jsp:include page="../header.jsp"/>
    </div>
    <div id="content">
        <div class="urbangreymenu">
            <jsp:include page="../sideMenu.jsp"/>
        </div>
        <div id="main_content">
            <div id="heading_top">
                <div class="heading_name"><fmt:message
                        key="content.main.content.heading.appwise.subscribed.subscriber"/></div>
            </div>
            <div id="heading_middle">
                <form name="dailyIncomeReportName" method="get" action="<%= request.getContextPath( ) + "/frameset" %>"
                      target="_blank"
                      onsubmit="return appwiseSubscriberSubscribeReport(this, ${type}, spNotSelected, errnoapp);">
                    <table width="670" border="0" align="center" cellspacing="6">
                        <tr>
                            <td><input type='hidden' name='__report'
                                       value="report/customercare/appwise_subscribed_subscriber_report.rptdesign"></td>
                            <c:choose>
                                <c:when test="${type == 1}">
                                    <td><input type='hidden' name='rptSpId' value=""></td>
                                    <td><input type='hidden' name='rptSpName' value=""></td>
                                </c:when>
                                <c:otherwise>
                                    <td><input type='hidden' name='rptSpId' value="${spId}"></td>
                                    <td><input type='hidden' name='rptSpName' value="${spName}"></td>
                                </c:otherwise>
                            </c:choose>
                            <td><input type='hidden' name='rptAppName' id="rptAppName" value=""></td>
                            <td><input type='hidden' name='rptAppId' id="rptAppId" value=""></td>

                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <c:if test="${type == 1}">
                            <tr>
                                <td>&nbsp;</td>
                                <td id="spAlert" class="alert">
                                    <fmt:message key="content.main.content.alert.select.a.corporate.user"/>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="150"><fmt:message key="content.main.content.corporate.user"/>
                                </td>
                                <td>
                                    <label>
                                        <select name="providers" id="providers"
                                                data-placeholder='<fmt:message key="content.main.content.select.item"/>'
                                                class="chosen-select select-box"
                                                onchange="setApplications()" tabindex="2">
                                            <option value=""><fmt:message
                                                    key="content.main.content.select.item"/></option>
                                            <c:forEach items="${spDetails}" var="details">
                                                <option value="<c:out value="${details.sp_id}+${details.sp_name}"/>">
                                                        ${details.sp_name} (${details.sp_id})
                                                </option>
                                            </c:forEach>
                                        </select>
                                    </label>
                                </td>
                            </tr>
                        </c:if>
                        <tr>
                            <td>&nbsp;</td>
                            <td id="appAlert" class="alert">
                                <fmt:message key="content.main.content.alert.select.applications"/>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <c:choose>
                            <c:when test="${type == 2}">

                                <tr>

                                    <td width="150"><fmt:message key="content.main.content.application"/><a
                                            class="star">
                                        *</a>
                                    </td>
                                    <td><label>
                                        <select name="apps" id="apps" class="chosen-select select-box"
                                                data-placeholder='<fmt:message key="content.main.content.select.item"/>'
                                                onchange="appAlertNeutralize()">
                                            <option value=""><fmt:message
                                                    key="content.main.content.select.item"/></option>
                                            <c:forEach items="${apps}" var="app">
                                                <option value="<c:out value="${app.app_id}+${app.app_name}"/>">
                                                        ${app.app_id}/${app.app_name}
                                                </option>
                                            </c:forEach>
                                        </select>
                                    </label></td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <tr>

                                    <td width="150"><fmt:message key="content.main.content.application"/><a
                                            class="star">
                                        *</a>
                                    </td>
                                    <td><label>
                                        <select name="apps" id="apps"
                                                data-placeholder='<fmt:message key="content.main.content.select.item"/>'
                                                class="chosen-select select-box"
                                                onchange="appAlertNeutralize()">
                                            <option value=""><fmt:message
                                                    key="content.main.content.select.item"/></option>
                                            <c:forEach items="${apps}" var="app">
                                                <option value="<c:out value="${app.app_id}+${app.app_name}"/>">
                                                        ${app.app_id} / ${app.app_name}
                                                </option>
                                            </c:forEach>
                                        </select>
                                    </label></td>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td width="150">&nbsp;</td>
                            <td><input type="image" src="../images/button.gif" width="63" height="25"
                                       border="0" alt="submit"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </form>
            </div>
            <div id="heading_bottom"></div>
        </div>
    </div>
    <div id="footer">
        <jsp:include page="../footer.jsp"/>
    </div>
</div>
</body>
<script type="text/javascript">
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
</html>
