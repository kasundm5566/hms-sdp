<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><fmt:message key="title.sdp.monthly.appwise.add"/></title>
    <link href="../css/dialog.sdp.css" rel="stylesheet" type="text/css"/>
    <link href="../css/jquery_chosen/chosen.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../javaScript/jquery/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../javaScript/jquery_chosen/chosen.jquery.js"></script>
    <script type="text/javascript" src="../javaScript/ddaccordion.js"></script>
    <script type="text/javascript" src="../javaScript/JavaScript.js"></script>
    <script type="text/javascript" src="../javaScript/javaScriptInSdpJSP.js"></script>
    <script type="text/javascript">
        var errmaxapp = "<fmt:message key="content.main.content.alert.max.applications"/>";
        var errnoapp = "<fmt:message key="content.main.content.alert.select.applications"/>";
        var maxApp = "<fmt:message key="maximum.app"/>";
        var spNotSelected = "<fmt:message key="content.main.content.criteria.not.added"/>";
    </script>
</head>
<body>
<div id="wrap">

    <div>
        <jsp:include page="../header.jsp"/>
    </div>
    <div id="content">
        <div class="urbangreymenu">
            <jsp:include page="../sideMenu.jsp"/>
        </div>
        <div id="main_content">
            <div id="heading_top">
                <div class="heading_name"><fmt:message
                        key="content.main.content.heading.sdp.monthly.appwise.add"/></div>
            </div>
            <div id="heading_middle">
                <form id="form1" name="form1" method="get" action="<%= request.getContextPath( ) + "/frameset" %>"
                      target="_blank"
                      onSubmit="return monthlyAppAdd(${type},errmaxapp,errnoapp,maxApp,spNotSelected);">
                    <table width="670" border="0" align="center" cellspacing="6">
                        <tr>
                            <td><input type='hidden' name='__report'
                                       value="report/sdp/sdp_monthly_app_wise_add_report.rptdesign"></td>
                            <td><input type='hidden' name='RP_monthly_app_add_year' id="RP_monthly_app_add_year"
                                       value=""></td>
                            <td><input type='hidden' name='RP_monthly_app_add_month' id="RP_monthly_app_add_month"
                                       value=""></td>
                            <c:choose>
                                <c:when test="${type == 1}">
                                    <td><input type='hidden' name='RP_monthly_app_add_sp_id'
                                               id="RP_monthly_app_add_sp_id" value=""></td>
                                    <td><input type='hidden' name='RP_monthly_app_add_sp_name'
                                               id="RP_monthly_app_add_sp_name" value=""></td>
                                </c:when>
                                <c:otherwise>
                                    <td><input type='hidden' name='RP_monthly_app_add_sp_id'
                                               id="RP_monthly_app_add_sp_id" value="${spId}"></td>
                                    <td><input type='hidden' name='RP_monthly_app_add_sp_name'
                                               id="RP_monthly_app_add_sp_name" value="${spName}"></td>
                                </c:otherwise>
                            </c:choose>
                            <td><input type='hidden' name='RP_monthly_app_add_app_id' id="RP_monthly_app_add_app_id"
                                       value=""></td>
                            <td><input type='hidden' name='RP_monthly_app_add_app_name' id="RP_monthly_app_add_app_name"
                                       value=""></td>
                            <!--<td><input type='hidden' name='__overwrite' value="true"></td>

                         <td><input type='hidden' name='__locale' value="en_US"></td>

                         <td><input type='hidden' name='__masterpage' value="true"></td>-->
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.year"/><a class="star"> *</a></td>
                            <td><label>
                                <select name="years" id="years" class="select-box" style="height: 27px;">
                                    <option value="<c:out value="${previousYear}"/>">${previousYear}</option>
                                    <option value="<c:out value="${currentYear}"/>" selected="selected">${currentYear}</option>
                                </select>
                            </label></td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td width="150"><fmt:message key="content.main.content.month"/><a class="star"> *</a></td>
                            <td><label>
                                <select name="months" id="months" class="select-box" style="height: 27px;">
                                    <option value="January" selected="selected"><fmt:message
                                            key="content.month.name.january"/></option>
                                    <option value="February"><fmt:message key="content.month.name.february"/></option>
                                    <option value="March"><fmt:message key="content.month.name.march"/></option>
                                    <option value="April"><fmt:message key="content.month.name.april"/></option>
                                    <option value="May"><fmt:message key="content.month.name.may"/></option>
                                    <option value="June"><fmt:message key="content.month.name.june"/></option>
                                    <option value="July"><fmt:message key="content.month.name.july"/></option>
                                    <option value="August"><fmt:message key="content.month.name.august"/></option>
                                    <option value="September"><fmt:message key="content.month.name.september"/></option>
                                    <option value="October"><fmt:message key="content.month.name.october"/></option>
                                    <option value="November"><fmt:message key="content.month.name.november"/></option>
                                    <option value="December"><fmt:message key="content.month.name.december"/></option>
                                </select>
                            </label></td>
                        </tr>
                        <c:if test="${type == 1}">
                            <tr>
                                <td>&nbsp;</td>
                                <td id="spAlert" class="alert">
                                    <fmt:message key="content.main.content.alert.select.a.corporate.user"/>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="150"><fmt:message key="content.main.content.corporate.user"/></td>
                                <td><label>
                                    <select name="providers" id="providers" class="chosen-select select-box"
                                            onchange="setApplications(this.options[this.selectedIndex].value.split('+')[0], '<fmt:message key="content.main.content.select.item"/>')" tabindex="2">
                                        <option value="" selected="selected"><fmt:message key="content.main.content.select.item"/></option>
                                        <c:forEach items="${spDetails}" var="details">
                                            <option value="<c:out value="${details.sp_id}+${details.sp_name}"/>">
                                                    ${details.sp_name} (${details.sp_id})
                                            </option>
                                        </c:forEach>
                                    </select>
                                </label></td>
                            </tr>
                        </c:if>
                        <tr>
                            <td>&nbsp;</td>
                            <td id="appAlert" class="alert">
                                <fmt:message key="content.main.content.alert.select.applications"/>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <c:choose>
                            <c:when test="${type == 2}">
                                <tr>
                                    <td width="150"><fmt:message key="content.main.content.application(s)"/><a
                                            class="star">
                                        *</a>
                                    </td>
                                    <td><label>
                                        <select name="apps" id="apps" class="item3list" multiple="multiple"
                                                onchange="appAlertNeutralize()">
                                            <c:forEach items="${apps}" var="app">
                                                <option value="<c:out value="${app.app_id}+${app.app_name}"/>">
                                                        ${app.app_id}/${app.app_name}
                                                </option>
                                            </c:forEach>
                                        </select>
                                    </label></td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <tr>
                                    <td width="150"><fmt:message key="content.main.content.application(s)"/><a
                                            class="star">
                                        *</a>
                                    </td>
                                    <td><label>
                                        <select name="apps" id="apps" data-placeholder='<fmt:message key="content.main.content.select.item"/>'
                                                class="chosen-select select-box" multiple tabindex="4" onchange="appAlertNeutralize()">
                                            <c:forEach items="${apps}" var="app">
                                                <option value="<c:out value="${app.app_id}+${app.app_name}"/>">
                                                        ${app.app_id} / ${app.app_name}
                                                </option>
                                            </c:forEach>
                                        </select>
                                    </label></td>
                                </tr>
                            </c:otherwise>
                        </c:choose>

                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150">&nbsp;</td>
                            <td><input type="image" src="../images/button.gif" width="63" height="25"
                                       border="0" alt="submit"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </form>
            </div>
            <div id="heading_bottom"></div>
        </div>
    </div>
    <div id="footer">
        <jsp:include page="../footer.jsp"/>
    </div>
</div>
</body>
<script type="text/javascript">
    selectCurrentMonth();
    var config = {
        '.chosen-select'           : {},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
        '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
</html>
