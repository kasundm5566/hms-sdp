<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="static hms.kite.rpt.util.ReportingKeyBox.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><fmt:message key="title.pi.detailed.transaction"/></title>
    <link href="../css/jquery.ui.all.css" rel="stylesheet" type="text/css"/>
    <link href="../css/dialog.sdp.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../javaScript/jquery.min.js"></script>
    <script type="text/javascript" src="../javaScript/ddaccordion.js"></script>
    <script type="text/javascript" src="../javaScript/JavaScript.js"></script>
    <script type="text/javascript" src="../javaScript/javaScriptInSdpJSP.js"></script>
    <script type="text/javascript" src="../javaScript/jquery.ui.core.js"></script>
    <script type="text/javascript" src="../javaScript/jquery.ui.datepicker.js"></script>
    <script type="text/javascript" src="../javaScript/javaScriptInPgwJSP.js"></script>
    <script type="text/javascript">
        var err1 = "<fmt:message key="content.main.content.alert.empty.date"/>"  ;
        var err2 = "<fmt:message key="content.main.content.alert.invalid.date"/>"  ;
        var err3 = "<fmt:message key="content.main.content.alert.future.date"/>" ;
        var err4 = "<fmt:message key="content.main.content.alert.to.date"/>"  ;
        var err5 = "<fmt:message key="content.main.content.alert.invalid.date.range"/>";
        var err6 = "<fmt:message key="content.main.content.alert.future.to.date"/>";
        var val1 = "${dateRange}";
        var SpID = "${SpID}";
        var userType = "${userType}";
        var paymentInstrumentlist = "${paymentInstruments}";
    </script>
</head>
<body>
<div id="wrap">
    <div><jsp:include page="../header.jsp" />
    </div>
    <div id="content">
        <div class="urbangreymenu">
            <jsp:include page="../sideMenu.jsp" />
        </div>
        <div id="main_content">
            <div id="heading_top">
                <div class="heading_name">
                    <fmt:message key="title.pgw.detailed.transaction" />
                </div>
            </div>
            <div id="heading_middle">
                <form name="pgwDetailTransactionForm" method="get" action="<%= request.getContextPath( ) + "/frameset" %>"
                      target="_blank" onsubmit="return detailTransactionValidationCustomCare(this, err1, err2, err3, err4, err5, val1, SpID, userType, paymentInstrumentlist, err6);">
                    <table width="670" border="0" align="center" cellspacing="6">
                        <tr>
                            <td><input type='hidden' name='__report'
                                       value="<%=pgwDetailedTransactionsReport%>"></td>
                            <td><input type='hidden' name='<%=rpFromDate%>'  id ='<%=rpFromDate%>' value=""></td>
                            <td><input type='hidden' name='<%=rpToDate%>'  id='<%=rpToDate%>' value="">
                            <td><input type='hidden' name="<%=rpPaymentInstrument%>" id ="<%=rpPaymentInstrument%>" value="">
                            <td><input type='hidden' name="<%=rpPaybillno%>" id="<%=rpPaybillno%>" value=""></td>
                            <td><input type='hidden' name="<%=rpTillno%>" id="<%=rpTillno%>" value=""></td>
                            <td><input type='hidden' name="<%=rpReferenceId%>"  value="">
                            <td><input type='hidden' name="<%=rpEventType%>" id="<%=rpEventType%>"  value="">
                            <td><input type='hidden' name="<%=rpStatus%>"  id="<%=rpStatus%>" value="">
                            <td><input type='hidden' name="<%=rpServiceProviderId%>"  id="<%=rpServiceProviderId%>" value="">
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td id="dateAlert" class="alert">
                                <fmt:message key="content.main.content.alert.invalid.date"/>
                            </td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message
                                    key="content.main.content.from.date" /><a class="star"> *</a></td>
                            </td>
                            <td><label class="demo"><input type="text"
                                                           name="fromDate" id="datepicker1"
                                                           onchange="datePickAlertNeutralize()" class="input-box"></label></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message
                                    key="content.main.content.to.date" /><a class="star"> *</a></td>
                            </td>
                            <td><label class="demo"><input type="text"
                                                           name="toDate" id="datepicker2"
                                                           onchange="datePickAlertNeutralize()" class="input-box"></label></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message
                                    key="pgw.content.payment.instrument" />
                            </td>

                            <td><div id= "paymentInstrumentList">
                                <c:forEach var = "paymentInstrument" items = "${paymentInstruments}" >
                                    <input type = "checkbox" name = "paymentInstrument" id = "${paymentInstrument}" value = "${paymentInstrument}"  onchange="validate(this,this.value,'<fmt:message key="content.main.content.select.item"/>')"  />
                                    <c:out value = "${paymentInstrument}" />

                                    <c:if test="${paymentInstrument=='M-Pesa'}">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <fmt:message key="pgw.content.payment.instrument.paybillno" /><%--</td>--%>
                                        <label> <select id="paybillNo" name="paybillNo" class="item2list" multiple="multiple" >
                                            <c:forEach items="${paybillNo}" var="paybillNo1">
                                                <option value="<c:out value="${paybillNo1}"/>">
                                                        ${paybillNo1}
                                                </option>
                                            </c:forEach>
                                        </select>
                                        </label>
                                        <br/>
                                        <br/>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <fmt:message key="pgw.content.payment.instrument.tillno"/>
                                        <label>
                                            <select id="tillNos" name="tillNos" class="item2list" multiple="multiple">
                                                <c:forEach items="${tillNos}" var="buyGoodsTillNo">
                                                    <option value="<c:out value="${buyGoodsTillNo}"/>">
                                                            ${buyGoodsTillNo}
                                                    </option>
                                                </c:forEach>
                                            </select>
                                        </label>
                                    </c:if>
                                    <br>
                                </c:forEach>
                            </div>
                            </td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="pgw.content.reference.id" /></td>
                            <td><label><input type="text" name="referenceId"
                                              id="referenceId" class="input-box"/></label></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="pgw.content.event.type" />
                            <td><label> <select id="eventType" class="input-box" name="eventType">
                                <option value="" selected="selected"> <fmt:message key="content.main.content.select.item" /></option>
                                <option value="DIRECT_DEBIT">	<fmt:message key="pgw.content.transaction.type.direct.debit.request" /></option>
                                <option value="CREDIT_RESERVE" ><fmt:message key="pgw.content.transaction.type.direct.creadit.reserve.request" /> </option>
                                <option value="CREDIT_COMMIT" ><fmt:message key="pgw.content.transaction.type.direct.creadit.commit.request" /> </option>
                                <option value="PAYMENTS" ><fmt:message key="pgw.content.transaction.type.payment" /> </option>
                            </select>
                            </label></td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message
                                    key="pgw.content.transaction.status" />
                            <td><label> <select id="tranactionStatus"
                                                class="input-box" name="status">
                                <option value="" selected="selected"> <fmt:message key="content.main.content.select.item" /></option>
                                <option value="success"><fmt:message key="pgw.content.transaction.status.successful" />	</option>
                                <option value="failed" ><fmt:message key="pgw.content.transaction.status.failed" /> </option>
                                <option value="pending" ><fmt:message key="pgw.content.transaction.status.successful.payment.pending" /> </option>
                                </option>
                            </select>
                            </label></td>
                        </tr>
                        <tr>
                            <td width="150">&nbsp;</td>
                            <td><input type="image" src="../images/button.gif" width="63" height="25"
                                       border="0" alt="submit"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </form>
            </div>
            <div id="heading_bottom"></div>
        </div>
    </div>
</div>
</body>
</html>