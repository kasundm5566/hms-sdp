<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><fmt:message key="title.sdp.yearly.corporate.revenue"/></title>
    <link href="../css/dialog.sdp.css" rel="stylesheet" type="text/css"/>
    <link href="../css/jquery_chosen/chosen.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../javaScript/jquery/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../javaScript/jquery_chosen/chosen.jquery.js"></script>
    <script type="text/javascript" src="../javaScript/ddaccordion.js"></script>
    <script type="text/javascript" src="../javaScript/JavaScript.js"></script>
    <script type="text/javascript" src="../javaScript/javaScriptInSdpJSP.js"></script>
    <script type="text/javascript">
        var errmaxsp = "<fmt:message key="content.main.content.alert.select.max.user"/>";
        var errnosp = "<fmt:message key="content.main.content.alert.select.a.corporate.user"/>";
        var maxSp = "<fmt:message key="maximum.sp"/>";
    </script>
</head>
<body>
<div id="wrap">

    <div>
        <jsp:include page="../header.jsp"/>
    </div>
    <div id="content">
        <div class="urbangreymenu">
            <jsp:include page="../sideMenu.jsp"/>
        </div>
        <div id="main_content">
            <div id="heading_top">
                <div class="heading_name"><fmt:message
                        key="content.main.content.heading.sdp.yearly.corporate.revenue"/></div>
            </div>
            <div id="heading_middle">
                <form id="form1" name="form1" method="get" action="<%= request.getContextPath( ) + "/frameset" %>"
                      target="_blank"
                      onSubmit="return spSelection(this,errmaxsp,errnosp,maxSp);">
                    <table width="670" border="0" align="center" cellspacing="6">
                        <tr>
                            <td><input type='hidden' name='__report'
                                       value="report/sdp/sdp_yearly_corporateuser_revenue_report.rptdesign"></td>
                            <td></td>
                            <td><input type='hidden' name='RP_yearly_co_user_year' id="RP_yearly_co_user_year" value="">
                            </td>
                            <td><input type='hidden' name='RP_yearly_co_user_name' id="RP_yearly_co_user_name" value="">
                            </td>
                            <td><input type='hidden' name='RP_yearly_co_user_id' id="RP_yearly_co_user_id" value="">
                            </td>

                            <td><input type='hidden' name='RP_show_graphs' id="RP_show_graphs" value=""></td>
                            <!--<td><input type='hidden' name='__overwrite' value="true"></td>

                         <td><input type='hidden' name='__locale' value="en_US"></td>

                         <td><input type='hidden' name='__masterpage' value="true"></td>-->
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td id="yearAlert" class="alert">
                                <fmt:message key="content.main.content.alert.select.years"/>
                            </td>
                        </tr>
                        <tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.year(s)"/><a class="star"> *</a></td>
                            <td><label>
                                <select name="years" id="years" multiple="multiple" class="year_list"
                                        onchange="yearAlertNeutralize()">
                                    <option value="<c:out value="${previousYear}"/>"
                                            >${previousYear}</option>
                                    <option value="<c:out value="${currentYear}"/>" selected="selected">${currentYear}</option>
                                </select>
                            </label></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td id="spAlert" class="alert">
                                <fmt:message key="content.main.content.alert.select.corporate.users"/>
                            </td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.corporate.user(s)"/><a class="star">
                                *</a></td>
                            <td><label>
                                <select name="spNames" id="providers" class="chosen-select select-box"
                                        data-placeholder='<fmt:message key="content.main.content.select.item"/>'
                                        multiple onchange="spAlertNeutralize()" tabindex="2">
                                    <c:forEach items="${sPNames}" var="spName">
                                        <option value="<c:out value="${spName.sp_name}(${spName.sp_id})"/>"
                                                >${spName.sp_name} (${spName.sp_id})
                                        </option>
                                    </c:forEach>
                                </select>
                            </label></td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td width="150"><fmt:message key="content.main.content.show.report.chart"/></td>
                            <td><label>
                                <input type="checkbox" name="chkShowChart" checked=checked value="1"/>
                            </label></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150">&nbsp;</td>
                            <td><input type="image" src="../images/button.gif" width="63" height="25"
                                       border="0" alt="submit"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </form>
            </div>
            <div id="heading_bottom"></div>
        </div>
    </div>
    <div id="footer">
        <jsp:include page="../footer.jsp"/>
    </div>
</div>
</body>
<script type="text/javascript">
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
</html>
