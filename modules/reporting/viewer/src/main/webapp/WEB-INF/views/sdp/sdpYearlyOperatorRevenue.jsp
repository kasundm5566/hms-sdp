<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><fmt:message key="title.sdp.yearly.operator.revenue"/></title>
    <link href="../css/dialog.sdp.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../javaScript/jquery.min.js"></script>
    <script type="text/javascript" src="../javaScript/ddaccordion.js"></script>
    <script type="text/javascript" src="../javaScript/JavaScript.js"></script>
    <script type="text/javascript" src="../javaScript/javaScriptInSdpJSP.js"></script>
</head>
<body>
<div id="wrap">

    <div>
        <jsp:include page="../header.jsp"/>
    </div>
    <div id="content">
        <div class="urbangreymenu">
            <jsp:include page="../sideMenu.jsp"/>
        </div>
        <div id="main_content">
            <div id="heading_top">
                <div class="heading_name"><fmt:message
                        key="content.main.content.heading.sdp.yearly.operator.revenue"/></div>
            </div>
            <div id="heading_middle">
                <form id="form1" name="form1" method="get" action="<%= request.getContextPath( ) + "/frameset" %>"
                      target="_blank"
                      onSubmit="return yearlyOPSelection();">
                    <table width="670" border="0" align="center" cellspacing="6">
                        <tr>
                            <td><input type='hidden' name='__report'
                                       value="report/sdp/sdp_yearly_operator_report.rptdesign"></td>
                            <td></td>
                            <td><input type='hidden' name='RP_yearly_ope_opid' id="RP_yearly_ope_opid" value=""></td>
                            <td><input type='hidden' name='RP_yearly_ope_opname' id="RP_yearly_ope_opname" value="">
                            </td>
                            <td><input type='hidden' name='RP_yearly_ope_year' id="RP_yearly_ope_year" value=""></td>
                            <td><input type='hidden' name='RP_yearly_ope_showGraph' id="RP_yearly_ope_showGraph"
                                       value=""></td>
                            <!--<td><input type='hidden' name='__overwrite' value="true"></td>

                         <td><input type='hidden' name='__locale' value="en_US"></td>

                         <td><input type='hidden' name='__masterpage' value="true"></td>-->
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td id="yearAlert" class="alert">
                                <fmt:message key="content.main.content.alert.select.years"/>
                            </td>
                        </tr>
                        <tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.year(s)"/><a class="star"> *</a></td>
                            <td><label>
                                <select name="years" id="years" multiple="multiple" class="item2list"
                                        onchange="yearAlertNeutralize()">
                                    <option value="<c:out value="${previousYear}"/>"
                                            selected="selected">${previousYear}</option>
                                    <option value="<c:out value="${currentYear}"/>">${currentYear}</option>
                                </select>
                            </label></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td id="operatorAlert" class="alert">
                                <fmt:message key="content.main.content.alert.select.operators"/>
                            </td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.operator(s)"/><a class="star"> *</a>
                            </td>
                            <td><label>
                                <select name="opNames" id="opNames" class="item2list" multiple="multiple"
                                        onchange="operatorAlertNeutralize()">
                                    <c:forEach items="${oPDetail}" var="opDetail">
                                        <option value="<c:out value="${opDetail.operator_id}+${opDetail.operator_name}"/>"
                                                selected="selected">${opDetail.operator_name}</option>
                                    </c:forEach>
                                </select>

                            </label></td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td width="150"><fmt:message key="content.main.content.show.report.chart"/></td>
                            <td><label>
                                <input type="checkbox" name="chkShowChart" id="chkShowChart" checked=checked
                                       value="True"/>
                            </label></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150">&nbsp;</td>
                            <td><input type="image" src="../images/button.gif" width="63" height="25"
                                       border="0" alt="submit"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </form>
            </div>
            <div id="heading_bottom"></div>
        </div>
    </div>
    <div id="footer">
        <jsp:include page="../footer.jsp"/>
    </div>
</div>
</body>
</html>
