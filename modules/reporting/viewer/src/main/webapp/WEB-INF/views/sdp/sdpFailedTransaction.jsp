<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><fmt:message key="title.sdp.failed.revenue"/></title>
    <link href="../css/jquery.ui.all.css" rel="stylesheet" type="text/css"/>
    <link href="../css/demos.css" rel="stylesheet" type="text/css"/>
    <link href="../css/dialog.sdp.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../javaScript/jquery.min.js"></script>
    <script type="text/javascript" src="../javaScript/ddaccordion.js"></script>
    <script type="text/javascript" src="../javaScript/JavaScript.js"></script>
    <script type="text/javascript" src="../javaScript/javaScriptInSdpJSP.js"></script>
    <script type="text/javascript" src="../javaScript/jquery.ui.core.js"></script>
    <script type="text/javascript" src="../javaScript/jquery.ui.widget.js"></script>
    <script type="text/javascript" src="../javaScript/jquery.ui.datepicker.js"></script>
    <script type="text/javascript">
        var err1 = "<fmt:message key="content.main.content.alert.empty.date"/>";
        var err2 = "<fmt:message key="content.main.content.alert.invalid.date"/>";
        var err3 = "<fmt:message key="content.main.content.alert.future.date"/>";
        var err4 = "<fmt:message key="content.main.content.alert.to.date"/>";
        var err5 = "<fmt:message key="content.main.content.alert.invalid.date.range"/>";
        var err6 = "<fmt:message key ="content.main.content.alert.future.to.date"/>"
    </script>
</head>
<body>
<div id="wrap">

    <div>
        <jsp:include page="../header.jsp"/>
    </div>
    <div id="content">
        <div class="urbangreymenu">
            <jsp:include page="../sideMenu.jsp"/>
        </div>
        <div id="main_content">
            <div id="heading_top">
                <div class="heading_name"><fmt:message key="content.main.content.heading.sdp.failed.transaction"/></div>
            </div>
            <div id="heading_middle">
                <form name="dailyIncomeReportName" method="get" action="<%= request.getContextPath( ) + "/frameset" %>"
                      target="_blank"
                      onsubmit="return failedReport(err1,err2,err3,err4,err5,err6);">
                    <table width="670" border="0" align="center" cellspacing="6">
                        <tr>
                            <td><input type='hidden' name='__report'
                                       value="report/sdp/failed_transaction_report.rptdesign"></td>
                            <td><input type='hidden' name='RP_failed_trans_start_date' id="RP_failed_trans_start_date"
                                       value=""></td>
                            <td><input type='hidden' name='RP_failed_trans_end_date' id="RP_failed_trans_end_date"
                                       value=""></td>

                            <%--<td><input type='hidden' name='__overwrite' value="true"></td>

                            <td><input type='hidden' name='__locale' value="en_US"></td>

                            <td><input type='hidden' name='__masterpage' value="true"></td>--%>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td id="dateAlert" class="alert">
                                <fmt:message key="content.main.content.alert.invalid.date"/>
                            </td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.from.date"/><a class="star"> *</a>
                            </td>
                            <td><label class="demo"><input type="text" name="fromDate" id="datepicker1"
                                                           onchange="datePickAlertNeutralize()"></label></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.to.date"/><a class="star"> *</a></td>
                            <td><label class="demo"><input type="text" name="toDate"
                                                           id="datepicker2"
                                                           onchange="datePickAlertNeutralize()"></label></td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150">&nbsp;</td>
                            <td><input type="image" src="../images/button.gif" width="63" height="25"
                                       border="0" alt="submit"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </form>
            </div>
            <div id="heading_bottom"></div>
        </div>
    </div>
    <div id="footer">
        <jsp:include page="../footer.jsp"/>
    </div>
</div>
</body>
</html>
