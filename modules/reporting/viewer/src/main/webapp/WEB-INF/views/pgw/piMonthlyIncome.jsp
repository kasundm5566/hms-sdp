<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><fmt:message key="title.pi.monthly.income"/></title>
    <link href="../css/dialog.sdp.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../javaScript/jquery.min.js"></script>
    <script type="text/javascript" src="../javaScript/ddaccordion.js"></script>
    <script type="text/javascript" src="../javaScript/JavaScript.js"></script>
    <script type="text/javascript" src="../javaScript/javaScriptInPiJSP.js"></script>
</head>
<body>
<div id="wrap">

    <div>
        <jsp:include page="../header.jsp"/>
    </div>
    <div id="content">
        <div class="urbangreymenu">
            <jsp:include page="../sideMenu.jsp"/>
        </div>
        <div id="main_content">
            <div id="heading_top">
                <div class="heading_name"><fmt:message key="content.main.content.title"/></div>
            </div>
            <div id="heading_middle">
                <form name="monthlyIncomeReportName" method="get" action="" target="_blank"
                      onsubmit="return monthDataValidation(this);">
                    <table width="670" border="0" align="center" cellspacing="6">
                        <tr>
                            <td><input type='hidden' name='__report'
                                       value="report/pgw/payment_instrument_monthly_income_report.rptdesign"></td>
                            <td><input type='hidden' name='monthly_rp_year' value="2011"></td>
                            <td><input type='hidden' name='monthly_rp_months'
                                       value="January,February,March,April,May,June,July,August,September,October,November,December,">
                            </td>
                            <td><input type='hidden' name='monthly_rp_display_chart' value="true"></td>

                            <%--<td><input type='hidden' name='__overwrite' value="true"></td>

                            <td><input type='hidden' name='__locale' value="en_US"></td>

                            <td><input type='hidden' name='__masterpage' value="true"></td>--%>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.year(s)"/></td>
                            <td><label>
                                <select name="years" class="list">
                                    <option value="1" selected="selected">${previousYear}</option>
                                    <option value="2">${currentYear}</option>
                                </select>
                            </label></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.month(s)"/></td>
                            <td><label>
                                <select name="months" class="dropdown" multiple="multiple" size="5">
                                    <option value="1" selected="selected"><fmt:message
                                            key="content.month.name.january"/></option>
                                    <option value="2"><fmt:message key="content.month.name.february"/></option>
                                    <option value="3"><fmt:message key="content.month.name.march"/></option>
                                    <option value="4"><fmt:message key="content.month.name.april"/></option>
                                    <option value="5"><fmt:message key="content.month.name.may"/></option>
                                    <option value="6"><fmt:message key="content.month.name.june"/></option>
                                    <option value="7"><fmt:message key="content.month.name.july"/></option>
                                    <option value="8"><fmt:message key="content.month.name.august"/></option>
                                    <option value="9"><fmt:message key="content.month.name.september"/></option>
                                    <option value="10"><fmt:message key="content.month.name.october"/></option>
                                    <option value="11"><fmt:message key="content.month.name.november"/></option>
                                    <option value="12"><fmt:message key="content.month.name.december"/></option>
                                </select>
                            </label></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.show.report.chart"/></td>
                            <td><label>
                                <input type="checkbox" name="chkShowChart" checked=checked value="1"/>
                            </label></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150">&nbsp;</td>
                            <td><input type="image" src="../images/button.gif" width="63" height="25"
                                       border="0" alt="submit"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </form>
            </div>
            <div id="heading_bottom"></div>
        </div>
    </div>
    <div id="footer">
        <jsp:include page="../footer.jsp"/>
    </div>
</div>
</body>
</html>
