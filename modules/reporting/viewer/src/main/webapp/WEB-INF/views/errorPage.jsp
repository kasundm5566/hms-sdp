<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page isErrorPage="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><fmt:message key="title.error.page"/></title>
    <link href="../css/dialog.sdp.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../javaScript/jquery.min.js"></script>
    <script type="text/javascript" src="../javaScript/ddaccordion.js"></script>
    <script type="text/javascript" src="../javaScript/JavaScript.js"></script>
</head>
<body>
<div id="wrap">

    <div>
        <jsp:include page="header.jsp"/>
    </div>
    <div id="content">
        <div class="urbangreymenu">
            <jsp:include page="sideMenu.jsp"/>
        </div>
        <div id="main_content">
            <div id="heading_top">
                <div class="heading_name"><fmt:message key="error.page.error.heading"/></div>
            </div>
            <div id="heading_middle">
                <table>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td class="alert" style="visibility:visible;"><fmt:message key="error.page.error.message"/></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </div>
            <div id="heading_bottom"></div>
        </div>
    </div>
    <div id="footer">
        <jsp:include page="footer.jsp"/>
    </div>
</div>
</body>
</html>