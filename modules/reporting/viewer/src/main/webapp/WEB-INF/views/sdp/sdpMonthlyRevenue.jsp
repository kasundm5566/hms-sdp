<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><fmt:message key="title.sdp.monthly.revenue"/></title>
    <link href="../css/dialog.sdp.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../javaScript/jquery.min.js"></script>
    <script type="text/javascript" src="../javaScript/ddaccordion.js"></script>
    <script type="text/javascript" src="../javaScript/JavaScript.js"></script>
    <script type="text/javascript" src="../javaScript/javaScriptInSdpJSP.js"></script>
</head>
<body>
<div id="wrap">

    <div>
        <jsp:include page="../header.jsp"/>
    </div>
    <div id="content">
        <div class="urbangreymenu">
            <jsp:include page="../sideMenu.jsp"/>
        </div>
        <div id="main_content">
            <div id="heading_top">
                <div class="heading_name"><fmt:message key="content.main.content.heading.sdp.monthly.revenue"/></div>
            </div>
            <div id="heading_middle">
                <form name="monthlyIncomeReportName" method="get"
                      action="<%= request.getContextPath( ) + "/frameset" %>" target="_blank"
                      onsubmit="return monthlyRevenue(this);">
                    <table width="670" border="0" align="center" cellspacing="6">
                        <tr>
                            <td><input type='hidden' name='__report'
                                       value="report/sdp/sdp_monthly_revenue_and_traffic_report.rptdesign"></td>
                            <td><input type='hidden' name='monthly_rp_year' value=""></td>
                            <td><input type='hidden' name='monthly_rp_months' value=""></td>
                            <td><input type='hidden' name='monthly_rp_show_graph' value=""></td>
                            <td><input type='hidden' name='monthly_rp_sp_id' value="${spId}"></td>
                            <td><input type='hidden' name='monthly_rp_operator_id' value="${operatorId}"></td>

                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.year"/><a class="star"> *</a></td>
                            <td><label>
                                <select name="years" class="list">
                                    <option value="${previousYear}">${previousYear}</option>
                                    <option value="${currentYear}" selected="selected">${currentYear}</option>
                                </select>
                            </label></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td id="monthAlert" class="alert">
                                <fmt:message key="content.main.content.alert.select.months"/>
                            </td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.month(s)"/><a class="star"> *</a>
                            </td>
                            <td><label>
                                <select id="months" name="months" class="dropdown" multiple="multiple" size="5"
                                        onchange="monthAlertNeutralize()">
                                    <option value="January"><fmt:message
                                            key="content.month.name.january"/></option>
                                    <option value="February"><fmt:message key="content.month.name.february"/></option>
                                    <option value="March"><fmt:message key="content.month.name.march"/></option>
                                    <option value="April"><fmt:message key="content.month.name.april"/></option>
                                    <option value="May"><fmt:message key="content.month.name.may"/></option>
                                    <option value="June"><fmt:message key="content.month.name.june"/></option>
                                    <option value="July"><fmt:message key="content.month.name.july"/></option>
                                    <option value="August"><fmt:message key="content.month.name.august"/></option>
                                    <option value="September"><fmt:message key="content.month.name.september"/></option>
                                    <option value="October"><fmt:message key="content.month.name.october"/></option>
                                    <option value="November"><fmt:message key="content.month.name.november"/></option>
                                    <option value="December"><fmt:message key="content.month.name.december"/></option>
                                </select>
                            </label></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.show.report.chart"/></td>
                            <td><label>
                                <input type="checkbox" name="chkShowChart" id="chkShowChart" checked=checked value="1"/>
                            </label></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150">&nbsp;</td>
                            <td><input type="image" src="../images/button.gif" width="63" height="25"
                                       border="0" alt="submit"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </form>
            </div>
            <div id="heading_bottom"></div>
        </div>
    </div>
    <div id="footer">
        <jsp:include page="../footer.jsp"/>
    </div>
</div>
</body>
<script type="text/javascript">
    selectCurrentMonth();
</script>
</html>
