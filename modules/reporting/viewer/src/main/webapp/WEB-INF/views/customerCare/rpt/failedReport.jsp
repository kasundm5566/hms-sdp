<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><fmt:message key="title.customer.care.failed.transation"/></title>
    <link href="../css/dialog.sdp.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../javaScript/javaScriptInCCJSP-vdf-1.1.87.js"></script>
</head>
<body>
<div id="wrap">

    <div id="top_banner"></div>

    <div id="report_heading rpt_report_heading">
        <div class="heading_name rpt_heading_name"><fmt:message
                key="content.main.content.heading.customer.care.failed.transaction"/></div>
    </div>

    <div id="content">
        <div class="content_param">
            <table>
                <tr>
                    <td width="100px" class="field_header"><fmt:message key="content.main.content.from.date"/></td>
                    <td class="field_value">${fromDate}</td>
                </tr>
                <tr>
                    <td class="field_header"><fmt:message key="content.main.content.to.date"/></td>
                    <td class="field_value">${toDate}</td>
                </tr>
                <tr>
                    <c:if test="${spList != ' ()'}">
                        <td class="field_header"><fmt:message key="content.main.content.corporate.user"/></td>
                        <td class="field_value">${spList}</td>
                    </c:if>
                </tr>
                <tr>
                    <td class="field_header"><fmt:message key="content.main.content.application(s)"/></td>
                    <td class="field_value">${appList}</td>
                </tr>
            </table>
        </div>

        <div>
            <c:choose>
                <c:when test="${transactions.pagesAvailable > 0}">
                    <table>
                        <tr>
                            <td width="15">
                                <p title='<fmt:message key="tool.tip.care.reports.pdf"/>'><a
                                        href="failedPDFHandler?sp_id=${spId}&app_ids=${appIds}&sp_name=${spName}&app_names=${appNames}&fromDate=${fromDate}&toDate=${toDate}"><img
                                        src="../images/pdf_icon32x32.gif" style="height:26px; width:26px" alt=""></a>
                                </p>
                            </td>
                            <td width="15">&nbsp;</td>
                            <td width="15">
                                <p title='<fmt:message key="tool.tip.care.reports.csv"/>'><a
                                        href="failedCSVHandler?sp_id=${spId}&app_ids=${appIds}&sp_name=${spName}&app_names=${appNames}&fromDate=${fromDate}&toDate=${toDate}"><img
                                        src="../images/csv_icon32x32.png" style="height:26px; width:26px" alt=""></a>
                                </p>
                            </td>
                            <td width="15">&nbsp;</td>
                            <td width="15">
                                <p title='<fmt:message key="tool.tip.care.reports.xls"/>'><a
                                        href="failedXLSHandler?sp_id=${spId}&app_ids=${appIds}&sp_name=${spName}&app_names=${appNames}&fromDate=${fromDate}&toDate=${toDate}"><img
                                        src="../images/excel_icon32x32.png" style="height:26px; width:26px" alt=""></a>
                                </p>
                            </td>
                        </tr>
                    </table>

                    <table width="900" border="0" align="center" cellspacing="3" class="tableborder rpt_table_border">
                        <tr>
                            <td width="60" height="59" class="rowtop-color rpt_row_top-color"><fmt:message
                                    key="content.table.heading.care.reports.message.id"/></td>
                            <td width="55" class="rowtop-color rpt_row_top-color"><fmt:message
                                    key="content.table.heading.care.reports.sp.id.name"/></td>
                            <td width="48" class="rowtop-color rpt_row_top-color"><fmt:message
                                    key="content.table.heading.care.reports.app.id.name"/></td>
                            <td width="48" class="rowtop-color rpt_row_top-color"><fmt:message
                                    key="content.table.heading.care.reports.error.code"/></td>
                            <td width="36" class="rowtop-color rpt_row_top-color"><fmt:message
                                    key="content.table.heading.care.reports.error.description"/></td>
                            <td width="36" class="rowtop-color rpt_row_top-color"><fmt:message
                                    key="content.table.heading.care.reports.message.type"/></td>
                            <td width="39" class="rowtop-color rpt_row_top-color"><fmt:message
                                    key="content.table.heading.care.reports.direction"/></td>
                            <td width="39" class="rowtop-color rpt_row_top-color"><fmt:message
                                    key="content.table.heading.care.reports.msisdn"/></td>
                        </tr>
                        <c:forEach var="transaction" items="${transactions.pageItems}" varStatus="status" begin="0"
                                   step="1">
                            <c:choose>
                                <c:when test="${status.count%2 == 0}">
                                    <tr class="rowmiddle-color rpt_row_middle-color">
                                </c:when>
                                <c:otherwise>
                                    <tr class="rowsub-color rpt_row_sub-color">
                                </c:otherwise>
                            </c:choose>
                            <td>${transaction.messageId}</td>
                            <td>${transaction.spId} / ${transaction.spName}</td>
                            <td>${transaction.appId} / ${transaction.appName}</td>
                            <td>${transaction.responseCode}</td>
                            <td>${transaction.responseDescription}</td>
                            <td>${transaction.ncs}</td>
                            <td>${transaction.direction}</td>
                            <td>${transaction.address}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:when>
                <c:otherwise>
                    <table align="center" width="100%">
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="rowmiddle-color rpt_row_middle-color"><fmt:message
                                    key="no.records.available"/></td>
                        </tr>
                    </table>
                </c:otherwise>
            </c:choose>

            <table align="center">
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="pagination_section">
                        <c:if test="${transactions.pagesAvailable > 1}">
                            <c:choose>
                                <c:when test="${transactions.pageNumber == 1}">
                                    Prev&nbsp;
                                </c:when>
                                <c:otherwise>
                                    <a href="failedReport.jsp?sp_id=${spId}&app_ids=${appIds}&sp_name=${spName}&app_names=${appNames}&pageNo=${transactions.pageNumber-1}&pageSize=${pageSize}&fromDate=${fromDate}&toDate=${toDate}">Prev</a>&nbsp;
                                </c:otherwise>
                            </c:choose>
                            <c:forEach var="status" begin="${transactions.pageListStart}"
                                       end="${transactions.pageListEnd}"
                                       step="1">
                                <c:choose>
                                    <c:when test="${status == transactions.pageNumber}">
                                        <span>${status}&nbsp;</span>
                                    </c:when>
                                    <c:otherwise>
                                        <a href="failedReport.jsp?sp_id=${spId}&app_ids=${appIds}&sp_name=${spName}&app_names=${appNames}&pageNo=${status}&pageSize=${pageSize}&fromDate=${fromDate}&toDate=${toDate}">${status}</a>&nbsp;
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                            <c:choose>
                                <c:when test="${transactions.pageNumber == transactions.pagesAvailable}">
                                    Next
                                </c:when>
                                <c:otherwise>
                                    <a href="failedReport.jsp?sp_id=${spId}&app_ids=${appIds}&sp_name=${spName}&app_names=${appNames}&&pageNo=${transactions.pageNumber+1}&pageSize=${pageSize}&fromDate=${fromDate}&toDate=${toDate}">Next</a>&nbsp;
                                </c:otherwise>
                            </c:choose>
                        </c:if>
                    </td>
                </tr>
            </table>
        </div>

    </div>

    <div class="footer rpt_footer">
        <div class="footer_heading rpt_footer_heading"><fmt:message key="footer.heading.copyright.statement"/></div>
    </div>

</div>
</body>
</html>
