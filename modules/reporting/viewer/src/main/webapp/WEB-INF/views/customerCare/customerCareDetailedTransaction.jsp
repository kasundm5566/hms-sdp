<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><fmt:message key="title.customer.care.detailed.transation"/></title>
    <link href="../css/jquery.ui.all.css" rel="stylesheet" type="text/css"/>
    <link href="../css/dialog.sdp.css" rel="stylesheet" type="text/css"/>
    <link href="../css/jquery_chosen/chosen.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../javaScript/jquery/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../javaScript/jquery_chosen/chosen.jquery.js"></script>
    <script type="text/javascript" src="../javaScript/ddaccordion.js"></script>
    <script type="text/javascript" src="../javaScript/JavaScript.js"></script>
    <script type="text/javascript" src="../javaScript/javaScriptInSdpJSP.js"></script>
    <script type="text/javascript" src="../javaScript/javaScriptInCCJSP-vdf-1.1.87.js"></script>
    <script type="text/javascript" src="../javaScript/jquery.ui.core.js"></script>
    <script type="text/javascript" src="../javaScript/jquery.ui.datepicker.js"></script>
    <script type="text/javascript">
        var err1 = "<fmt:message key="content.main.content.alert.empty.date"/>";
        var err2 = "<fmt:message key="content.main.content.alert.invalid.date"/>";
        var err3 = "<fmt:message key="content.main.content.alert.future.date"/>";
        var err4 = "<fmt:message key="content.main.content.alert.to.date"/>";
        var err5 = "<fmt:message key="content.main.content.alert.invalid.date.range.30"/>";
        var err6 = "<fmt:message key ="content.main.content.alert.future.to.date"/>";
        var err7 = "<fmt:message key="content.main.content.alert.invalid.subscriber.msisdn"/>";
        var err8 = "<fmt:message key="content.main.content.alert.enter.subscriber.msisdn"/>";
        var msisdnPattern = "<fmt:message key="content.main.content.msisdn.reg"/>";
        var msisdnLength = "<fmt:message key="content.main.content.msisdn.length"/>";
        var err9 = "<fmt:message key="content.main.content.alert.max.application"/>";
    </script>
</head>
<body>
<div id="wrap">

    <div>
        <jsp:include page="../header.jsp"/>
    </div>
    <div id="content">
        <div class="urbangreymenu">
            <jsp:include page="../sideMenu.jsp"/>
        </div>
        <div id="main_content">
            <div id="heading_top">
                <div class="heading_name"><fmt:message
                        key="content.main.content.heading.customer.care.detailed.transaction"/></div>
            </div>
            <div id="heading_middle">
                <form name="dailyIncomeReportName" method="get" action="detailedReport.jsp"
                      target="_blank"
                      onsubmit="return validateDetailedTransaction(this, ${type}, err1, err2, err3, err4, err5, err6, err7, err8, msisdnPattern, msisdnLength, err9);">
                    <table width="670" border="0" align="center" cellspacing="6">
                        <tr>
                            <c:choose>
                                <c:when test="${type == 1}">
                                    <td><input type='hidden' name='sp_id' value=""></td>
                                    <td><input type='hidden' name='sp_name' value=""></td>
                                </c:when>
                                <c:otherwise>
                                    <td><input type='hidden' name='sp_id' value="${spId}"></td>
                                    <td><input type='hidden' name='sp_name' value="${spName}"></td>
                                </c:otherwise>
                            </c:choose>
                            <td><input type='hidden' name='app_ids' value=""></td>
                            <td><input type='hidden' name='app_names' value=""></td>
                            <td><input type='hidden' name='pageNo' value="1"></td>
                            <td><input type='hidden' name='pageSize' value="20"></td>
                            <td><input type="hidden" name="userType" value="${type}"></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td id="dateAlert" class="alert">
                                <fmt:message key="content.main.content.alert.invalid.date"/>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.from.date"/><a class="star"> *</a>
                            </td>
                            <td><input type="text" name="fromDate" id="datepicker1" class="input-box"
                                       onchange="datePickAlertNeutralize()"></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.to.date"/><a class="star"> *</a></td>
                            <td><input type="text" name="toDate" id="datepicker2" class="input-box"
                                       onchange="datePickAlertNeutralize()"></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td id="msisdnAlert" class="alert">
                                <fmt:message key="content.main.content.alert.enter.subscriber.msisdn"/>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.subscriber.msisdn"/><a class="star"> *</a></td>
                            <td><label><input type="text" name="msisdn" id="txtMSISDN" class="select-box" onchange="msisdnAlertNeutralize()"/></label></td>
                            <td>&nbsp;</td>
                        </tr>
                        <c:if test="${type == 1}">
                            <tr>
                                <td>&nbsp;</td>
                                <td id="spAlert" class="alert">
                                    <fmt:message key="content.main.content.alert.select.a.corporate.user"/>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="150"><fmt:message key="content.main.content.corporate.user"/>
                                </td>
                                <td><label>
                                    <select name="providers" id="providers" data-placeholder='<fmt:message key="content.main.content.select.item"/>' class="chosen-select select-box"
                                            onchange="setApplications(this.options[this.selectedIndex].value.split('+')[0], '<fmt:message key="content.main.content.select.item"/>')" tabindex="2">
                                        <option value=""><fmt:message key="content.main.content.select.item"/></option>
                                        <c:forEach items="${spDetails}" var="details">
                                            <option value="<c:out value="${details.sp_id}+${details.sp_name}"/>">
                                                    ${details.sp_name} (${details.sp_id})
                                            </option>
                                        </c:forEach>
                                    </select>
                                </label></td>
                            </tr>
                        </c:if>
                        <tr>
                            <td>&nbsp;</td>
                            <td id="appAlert" class="alert">
                                <fmt:message key="content.main.content.alert.select.applications"/>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <c:choose>
                            <c:when test="${type == 2}">
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.application(s)"/>
                            </td>
                            <td><label>
                                <select name="apps" id="apps" class="chosen-select select-box" data-placeholder='<fmt:message key="content.main.content.select.item"/>'
                                        multiple onchange="appAlertNeutralize()">
                                    <c:forEach items="${apps}" var="app">
                                        <option value="<c:out value="${app.app_id}+${app.app_name}"/>">
                                                ${app.app_id}/${app.app_name}
                                        </option>
                                    </c:forEach>
                                </select>
                            </label></td>
                        </tr>
                        </c:when>
                        <c:otherwise>
                            <tr>
                                <td width="150"><fmt:message key="content.main.content.application(s)"/>
                                </td>
                                <td><label>
                                    <select name="apps" id="apps" data-placeholder='<fmt:message key="content.main.content.select.item"/>'
                                            class="chosen-select select-box" multiple tabindex="4" onchange="appAlertNeutralize()">
                                        <c:forEach items="${apps}" var="app">
                                            <option value="<c:out value="${app.app_id}+${app.app_name}"/>">
                                                    ${app.app_id} / ${app.app_name}
                                            </option>
                                        </c:forEach>
                                    </select>
                                </label></td>
                            </tr>
                        </c:otherwise>
                        </c:choose>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.short.code"/></td>
                            <td><label><input type="text" name="shortCode" id="txtShrtCode" class="select-box"/></label></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.key.word"/></td>
                            <td><label><input type="text" name="keyWord" id="txtKeyWord" class="select-box"/></label></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.application.status"/></td>
                            <td><label><select name="appStatus" class="select-box">
                                <option value=""><fmt:message key="content.main.content.select.item"/></option>
                                <option value="1"><fmt:message key="content.main.content.limited.live"/></option>
                                <option value="2"><fmt:message key="content.main.content.live"/></option>
                                <option value="3"><fmt:message key="content.main.content.intermediate"/></option>
                            </select>
                            </label></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.transaction.status"/></td>
                            <td><label><select name="transactionStatus" class="select-box">
                                <option value="1"><fmt:message key="content.main.content.all"/></option>
                                <option value="2"><fmt:message key="content.main.content.success"/></option>
                                <option value="3"><fmt:message key="content.main.content.failure"/></option>
                            </select>
                            </label></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150"><fmt:message key="content.main.content.sort.by"/></td>
                            <td><label><select name="sort" class="select-box">
                                <option value=""><fmt:message key="content.main.content.select.item"/></option>
                                <option value="time_stamp"><fmt:message key="content.main.content.created.time"/></option>
                            </select>
                            </label></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="150">&nbsp;</td>
                            <td><input type="image" src="../images/button.gif" width="63" height="25"
                                       border="0" alt="submit"/></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </form>
            </div>
            <div id="heading_bottom"></div>
        </div>
    </div>
    <div id="footer">
        <jsp:include page="../footer.jsp"/>
    </div>
</div>
</body>
<script type="text/javascript">
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
</html>
