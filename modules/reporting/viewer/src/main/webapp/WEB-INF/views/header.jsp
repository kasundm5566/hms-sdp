<%@ page import="org.springframework.security.core.userdetails.UserDetails" %>
<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head></head>
<body>
<div id="brand-logo"></div>
<div id="wrap">
<div id="navigation">
    <ul>
        <div class="home"></div>
        <li><a href='<fmt:message key="url.home"/>'><fmt:message key="navigation.home"/></a></li>
        <sec:authorize ifAnyGranted="ROLE_SDP_RPT_HEADER_MENU_PROVISIONING"><li><a href='<fmt:message key="url.provisioning"/>'><fmt:message key="navigation.provisioning"/></a></li></sec:authorize>
        <sec:authorize ifAnyGranted="ROLE_SDP_RPT_HEADER_MENU_REGISTRATION"><li><a href='<fmt:message key="url.registration"/>'><fmt:message key="navigation.registration"/></a></li></sec:authorize>
        <sec:authorize ifAnyGranted="ROLE_SDP_RPT_HEADER_MENU_APPSTORE"><li><a href='<fmt:message key="url.appstore"/>'><fmt:message key="navigation.appstore"/></a></li></sec:authorize>
        <sec:authorize ifAnyGranted="ROLE_SDP_RPT_HEADER_MENU_ADMIN"><li><a href='<fmt:message key="url.sdp.admin"/>'><fmt:message key="navigation.sdp.admin"/></a></li></sec:authorize>
        <sec:authorize ifAnyGranted="ROLE_SDP_RPT_HEADER_MENU_SOLTURA"><li><a href='<fmt:message key="url.soltura"/>'><fmt:message key="navigation.soltura"/></a></li></sec:authorize>

        <%--<li><a href='<fmt:message key="url.pgw.selfcare"/>'><fmt:message key="navigation.pgw.selfcare"/></a></li>--%>
        <%--<li><a href='<fmt:message key="url.pgw.admincare"/>'><fmt:message key="navigation.pgw.admincare"/></a></li>--%>
        <div class="home"></div>
    </ul>
</div>
<div id="header_right">
    <fmt:message
        key="header.right.welcome"/> <%=((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()%>
<%--    <label>|</label><a href='<fmt:message key="url.home"/>'><fmt:message key="navigation.home"/></a>--%>
    <label>|</label><a href="/viewer/j_spring_security_logout"><fmt:message key="header.right.logout"/></a>
</div>
<div id="top_banner"></div>
</div>
</body>
</html>