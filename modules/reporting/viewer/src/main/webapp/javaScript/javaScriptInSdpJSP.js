/*
 * (C) Copyright 2012-2014 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

function dateValidator(stringDate, errEmptyDate, errInvalidDate, errFutureDate) {
    if (stringDate == "") {
        showDateError(errEmptyDate);
        return false;
    }
    var leapYearDays = new Array(31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
    var normalYearDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

    var dateRegEx = "\\d{4}-\\d{2}-\\d{2}";

    if (stringDate.match(dateRegEx)) {
        var dateArray = stringDate.split("-");
        var year = parseInt(dateArray[0], 10);
        var month = parseInt(dateArray[1], 10);
        var day = parseInt(dateArray[2], 10);
        if ((year > 0) && (month > 0) && (month <= 12) && (day > 0)) {
            if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
                if (day <= leapYearDays[month - 1]) {
                    if (!pastDatesOnly(stringDate)) {
                        showDateError(errFutureDate);
                        return false;
                    }
                } else {
                    showDateError(errInvalidDate);
                    return false;
                }
            } else {
                if (day <= normalYearDays[month - 1]) {
                    if (!pastDatesOnly(stringDate)) {
                        showDateError(errFutureDate);
                        return false;
                    }
                } else {
                    showDateError(errInvalidDate);
                    return false;
                }
            }
        } else {
            showDateError(errInvalidDate);
            return false;
        }
    } else {
        showDateError(errInvalidDate);
        return false;
    }
    return true;
}

function showDateError(errID) {
    document.getElementById("dateAlert").innerHTML = errID;
    document.getElementById("dateAlert").style.visibility = "visible";
}

function pastDatesOnly(stringDate) {
    var dateArray = stringDate.split("-");

    var today = new Date();
    var givenDate = new Date();
    givenDate.setFullYear(parseInt(dateArray[0], 10), parseInt(dateArray[1], 10) - 1, parseInt(dateArray[2], 10));

    return givenDate < today;
}

function datePickAlertNeutralize() {
    document.getElementById("dateAlert").style.visibility = "hidden";
}

function spAlertNeutralize() {
    if (document.getElementById("spAlert") != null) {
        document.getElementById("spAlert").style.visibility = "hidden";
    }
}

function appAlertNeutralize() {
    document.getElementById("appAlert").style.visibility = "hidden";
}

function appTypeNeutralize() {
    document.getElementById("appTypeAlert").style.visibility = "hidden";
}

function msisdnAlertNeutralize() {
    document.getElementById("msisdnAlert").style.visibility = "hidden";
    document.getElementById("invalidMsisdnAlert").style.visibility = "hidden";
}

function operatorAlertNeutralize() {
    document.getElementById("operatorAlert").style.visibility = "hidden";
}

function monthAlertNeutralize() {
    document.getElementById("monthAlert").style.visibility = "hidden";
}

function addAlertNeutralize() {
    document.getElementById("addAlert").style.visibility = "hidden";
}

function yearAlertNeutralize() {
    document.getElementById("yearAlert").style.visibility = "hidden";
}

function selectCurrentMonth() {
    document.getElementById("months").selectedIndex = new Date().getMonth();
}

function setSortingType(type) {
    if (type == "year") {
        var time = document.getElementById("years");
    } else if (type == "month") {
        var time = document.getElementById("months");
    }

    var count = 0;
    for (var i = 0; i < time.options.length; i++) {
        if (time.options[i].selected) {
            count++;
        }
    }

    if (count != 1) {
        document.getElementById("sortWay").disabled = true;
        document.getElementById("orderWay").disabled = true;
    } else {
        document.getElementById("sortWay").disabled = false;
        document.getElementById("orderWay").disabled = false;
    }
}

function disableAppsDropDown() {
    if (document.getElementById("selectAll").checked == true) {
        appAlertNeutralize();
        document.getElementById('apps').options.length = 0;
        $("#apps").trigger("chosen:updated");
    } else {
        setApplicationsList();
    }
}

function setApplicationsList() {
    var elemSelectAll = document.getElementById("selectAll");

    spAlertNeutralize();
    appAlertNeutralize();

    if (elemSelectAll == null || elemSelectAll.checked != true) {
        setApplications();
    } else {
        disableAppsDropDown();
    }
}

function setApplications() {
    spAlertNeutralize();
    appAlertNeutralize();
    var elemSp = document.getElementById("providers");
    var elemReportType = document.getElementById("report_type");
    var elemAppType = document.getElementById("applicationTypes");
    var elemChargingType = document.getElementById("chargingTypes");
    var selectedSp = "";
    var applicationType = "";
    var chargingType = "";
    var reportType = "";
    if (elemSp != null) {
        selectedSp = elemSp.options[elemSp.selectedIndex].value.split('+')[0];
    } else {
        selectedSp = document.getElementById('current_sp_id').value;
    }
    if (elemReportType != null) {
        reportType = elemReportType.value;
    }
    if (elemAppType != null) {
        applicationType = elemAppType.options[elemAppType.selectedIndex].value;
    }
    if (elemChargingType != null) {
        chargingType = elemChargingType.options[elemChargingType.selectedIndex].value;
    }
    var redirection = "getApplications?reportType=" + reportType + "&appType=" + applicationType + "&spId=" + selectedSp + "&chargingType=" + chargingType;

    $.get(redirection, function (data) {
        var appsListElement = document.getElementById('apps');
        appsListElement.options.length = 0;
        if (data.length == 2) {
            $("#apps").trigger("chosen:updated");
        } else {
            var appList = data.split("&&");
            for (var i = 0; i < appList.length; i++) {
                var appId = appList[i].split("++")[0];
                var appName = appList[i].split("++")[1];
                var opt = document.createElement('option');
                opt.value = appId + "+" + appName;
                opt.innerText = appId + " / " + appName;
                appsListElement.appendChild(opt);
            }
            $("#apps").trigger("chosen:updated");
        }
    });
}

function monthlyRevenue(current_form) {
    var yearsSelector = current_form["years"];
    var year = yearsSelector.options[yearsSelector.selectedIndex].value;
    var monthsSelector = current_form["months"];
    if (monthsSelector.selectedIndex == -1) {
        document.getElementById("monthAlert").style.visibility = "visible";
        return false;
    } else {
        var selectedArray = new Array();
        var count = 0;
        for (var i = 0; i < monthsSelector.options.length; i++) {
            if (monthsSelector.options[i].selected) {
                selectedArray[count] = monthsSelector.options[i].value;
                count++;
            }
        }
        current_form["monthly_rp_year"].value = year;
        current_form["monthly_rp_months"].value = selectedArray.toString();
        current_form["monthly_rp_show_graph"].value = current_form["chkShowChart"].checked;
        return true;
    }
}

function dailyRevenue(current_form, err1, err2, err3, err4, err5, err6) {
    var fromDateString = current_form["fromDate"].value;
    var toDateString = current_form["toDate"].value;
    if (dateValidator(fromDateString, err1, err2, err3) && dateValidator(toDateString, err1, err2, err6)) {
        var fromDateArray = fromDateString.split("-");
        var toDateArray = toDateString.split("-");
        var fromDate = new Date();
        var toDate = new Date();
        fromDate.setFullYear(parseInt(fromDateArray[0], 10), parseInt(fromDateArray[1], 10) - 1, parseInt(fromDateArray[2], 10));
        toDate.setFullYear(parseInt(toDateArray[0], 10), parseInt(toDateArray[1], 10) - 1, parseInt(toDateArray[2], 10));
        if (toDate < fromDate) {
            showDateError(err4);
            return false;
        } else if (toDate - fromDate > 2592000000) {
            showDateError(err5);
            return false;
        }
        current_form["daily_rp_from_date"].value = current_form["fromDate"].value;
        current_form["daily_rp_to_date"].value = current_form["toDate"].value;
        current_form["daily_rp_show_graph"].value = current_form["chkShowChart"].checked;
        return true;
    } else {
        return false;
    }
}

function monthlyCorporate(current_form, errmaxsp, errnosp, maxSp) {
    current_form["cop_user_based_monthly_rp_show_report"].value = current_form["chkShowChart"].checked;

    var yearsSelector = current_form["years"];
    current_form["cop_user_based_monthly_rp_year"].value = yearsSelector.options[yearsSelector.selectedIndex].value;

    var selectedArray = new Array();
    var selObj = current_form["months"];
    if (selObj.selectedIndex == -1) {
        document.getElementById("monthAlert").style.visibility = "visible";
        return false;
    } else {
        var i;
        var count = 0;
        for (i = 0; i < selObj.options.length; i++) {
            if (selObj.options[i].selected) {
                selectedArray[count] = selObj.options[i].value;
                count++;
            }
        }
        current_form["cop_user_based_monthly_rp_months"].value = selectedArray.toString();
    }

    //====================================================================================
    var spid = new Array();
    var spname = new Array();
    var selObj2 = current_form["providers"];
    if (selObj2.selectedIndex == -1) {
        document.getElementById("spAlert").innerText = errnosp;
        document.getElementById("spAlert").style.visibility = "visible";
        return false;
    } else {
        count = 0;
        for (i = 0; i < selObj2.options.length; i++) {
            if (selObj2.options[i].selected) {
                spid[count] = selObj2.options[i].value.split("+")[0];
                spname[count] = selObj2.options[i].value.split("+")[1];
                count++;
            }
        }
        if (count > maxSp) {
            document.getElementById("spAlert").innerText = errmaxsp;
            document.getElementById("spAlert").style.visibility = "visible";
            return false;
        }

        current_form["cop_user_based_monthly_rp_sp_ids"].value = spid.toString();
        current_form["cop_user_based_monthly_rp_sp_names"].value = spname.toString();

        return true;
    }
}

function dailyCorporate(current_form, err1, err2, err3, err4, err5, errmaxsp, errnosp, maxSp, err6) {
    var fromDateString = current_form["fromDate"].value;
    var toDateString = current_form["toDate"].value;
    var selObj2 = current_form["providers"];


    if (dateValidator(fromDateString, err1, err2, err3) && dateValidator(toDateString, err1, err2, err6)) {
        var fromDateArray = fromDateString.split("-");
        var toDateArray = toDateString.split("-");
        var fromDate = new Date();
        var toDate = new Date();
        fromDate.setFullYear(parseInt(fromDateArray[0], 10), parseInt(fromDateArray[1], 10) - 1, parseInt(fromDateArray[2], 10));
        toDate.setFullYear(parseInt(toDateArray[0], 10), parseInt(toDateArray[1], 10) - 1, parseInt(toDateArray[2], 10));
        if (toDate < fromDate) {
            showDateError(err4);
            return false;
        } else if (toDate - fromDate > 2592000000) {
            showDateError(err5);
            return false;
        }
        current_form["cop_user_based_daily_rp_from_date"].value = fromDateString;
        current_form["cop_user_based_daily_rp_to_date"].value = toDateString;
        if (selObj2.selectedIndex == -1) {
            document.getElementById("spAlert").innerText = errnosp;
            document.getElementById("spAlert").style.visibility = "visible";
            return false;
        } else {
            var spid = new Array();
            var spname = new Array();
            var count = 0;
            for (var i = 0; i < selObj2.options.length; i++) {
                if (selObj2.options[i].selected) {
                    spid[count] = selObj2.options[i].value.split("+")[0];
                    spname[count] = selObj2.options[i].value.split("+")[1];
                    count++;
                }
            }
            if (count > maxSp) {
                document.getElementById("spAlert").innerText = errmaxsp;
                document.getElementById("spAlert").style.visibility = "visible";
                return false;
            }
            current_form["cop_user_based_daily_rp_sp_ids"].value = spid.toString();
            current_form["cop_user_based_daily_rp_sp_names"].value = spname.toString();
            current_form["cop_user_based_daily_rp_show_report"].value = current_form["chkShowChart"].checked;
            return true;

        }
    } else {
        return false;
    }
}

function yearlyNCS(current_form, type, hideSP, hideApplications, hideApplicationType, hideOperator, errMaxApp, errNoApp, maxApp, spNotSelected) {
    var yearArray = new Array();
    var appIdArray = new Array();
    var appNameArray = new Array();
    var operatorIdArray = new Array();
    var operatorNameArray = new Array();

    var appSelector = current_form["apps"];
    var yearSelector = current_form["years"];
    var spSelector = current_form["providers"];
    var appType = current_form["applicationTypes"];
    var operatorSelector = current_form["operators"];

    var yearCount = 0;
    var opCount = 0;
    var appCount = 0;

    if (yearSelector.selectedIndex == -1) {
        document.getElementById("yearAlert").style.visibility = "visible";
        return false;
    } else {
        for (i = 0; i < yearSelector.options.length; i++) {
            if (yearSelector.options[i].selected) {
                yearArray[yearCount] = yearSelector.options[i].value;
                yearCount++;
            }
        }
        current_form["ncs_yearly_rp_years"].value = yearArray.toString();
    }

    if (type == 1) {
        if (hideOperator != true) {
            if (operatorSelector.selectedIndex == -1) {
                document.getElementById("operatorAlert").style.visibility = "visible";
                return false;
            } else {
                for (var i = 0; i < operatorSelector.options.length; i++) {
                    if (operatorSelector.options[i].selected) {
                        operatorIdArray[opCount] = operatorSelector.options[i].value.split("+")[0];
                        operatorNameArray[opCount] = operatorSelector.options[i].value.split("+")[1];
                        opCount++;
                    }
                }
                current_form["ncs_yearly_rp_operator_ids"].value = operatorIdArray.toString();
                current_form["ncs_yearly_rp_operator_names"].value = operatorNameArray.toString();
            }
        }
        if (hideSP != true) {
            var spSelectedString = spSelector.options[spSelector.selectedIndex].value;
            if (spSelectedString == "") {
                current_form["ncs_yearly_rp_sp_id"].value = spNotSelected;
                current_form["ncs_yearly_rp_sp_name"].value = spNotSelected;
            } else {
                current_form["ncs_yearly_rp_sp_id"].value = spSelectedString.split("+")[0];
                current_form["ncs_yearly_rp_sp_name"].value = spSelectedString.split("+")[1];
            }
        }

        if (current_form["chkShowChart"] != null) {
            current_form["ncs_yearly_rp_show_report"].value = current_form["chkShowChart"].checked;
            current_form["ncs_yearly_rp_show_graphs"].value = current_form["chkShowChart"].checked;
        } else {
            current_form["ncs_yearly_rp_show_report"].value = false;
            current_form["ncs_yearly_rp_show_graphs"].value = false;
        }
    }

    if (hideApplicationType != true) {
        current_form["ncs_yearly_rp_app_type"].value = appType.options[appType.selectedIndex].value;
    }

    if (hideApplications != true) {
        current_form["ncs_yearly_rp_select_all_apps"].value = current_form["selectAll"].checked;

        if (appSelector.selectedIndex == -1 && (current_form["selectAll"].checked == false)) {
            document.getElementById("appAlert").innerText = errNoApp;
            document.getElementById("appAlert").style.visibility = "visible";
            return false;
        } else {
            for (var j = 0; j < appSelector.options.length; j++) {
                if (appSelector.options[j].selected) {
                    appIdArray[appCount] = appSelector.options[j].value.split("+")[0];
                    appNameArray[appCount] = appSelector.options[j].value.split("+")[1];
                    appCount++;
                }
            }
            if (appCount > maxApp) {
                document.getElementById("appAlert").innerText = errMaxApp;
                document.getElementById("appAlert").style.visibility = "visible";
                return false;
            }
            current_form['ncs_yearly_rp_app_name'].value = appNameArray.toString();
            current_form['ncs_yearly_rp_app_id'].value = appIdArray.toString();
        }
    }
    return true;
}

function monthlyNCS(current_form, type, hideSP, hideApplications, hideApplicationType, hideOperator, errMaxApp, errNoApp, maxApp, spNotSelected) {
    var monthArray = new Array();
    var appIdArray = new Array();
    var appNameArray = new Array();
    var operatorIdArray = new Array();
    var operatorNameArray = new Array();

    var appSelector = current_form["apps"];
    var yearSelector = current_form["year"];
    var monthSelector = current_form["months"];
    var spSelector = current_form["providers"];
    var appType = current_form["applicationTypes"];
    var operatorSelector = current_form["operators"];

    var opCount = 0;
    var appCount = 0;
    var monthCount = 0;

    if (monthSelector.selectedIndex == -1) {
        document.getElementById("monthAlert").style.visibility = "visible";
        return false;
    } else {
        for (i = 0; i < monthSelector.options.length; i++) {
            if (monthSelector.options[i].selected) {
                monthArray[monthCount] = monthSelector.options[i].value;
                monthCount++;
            }
        }
        current_form["ncs_monthly_rp_months"].value = monthArray.toString();
    }

    current_form["ncs_monthly_rp_year"].value = yearSelector.options[yearSelector.selectedIndex].value;

    if (type == 1) {
        if (hideOperator != true) {
            if (operatorSelector.selectedIndex == -1) {
                document.getElementById("operatorAlert").style.visibility = "visible";
                return false;
            } else {
                for (var i = 0; i < operatorSelector.options.length; i++) {
                    if (operatorSelector.options[i].selected) {
                        operatorIdArray[opCount] = operatorSelector.options[i].value.split("+")[0];
                        operatorNameArray[opCount] = operatorSelector.options[i].value.split("+")[1];
                        opCount++;
                    }
                }
                current_form["ncs_monthly_rp_operator_ids"].value = operatorIdArray.toString();
                current_form["ncs_monthly_rp_operator_names"].value = operatorNameArray.toString();
            }
        }
        if (hideSP != true) {
            var spSelectedString = spSelector.options[spSelector.selectedIndex].value;
            if (spSelectedString == "") {
                current_form["ncs_monthly_rp_sp_id"].value = spNotSelected;
                current_form["ncs_monthly_rp_sp_name"].value = spNotSelected;
            } else {
                current_form["ncs_monthly_rp_sp_id"].value = spSelectedString.split("+")[0];
                current_form["ncs_monthly_rp_sp_name"].value = spSelectedString.split("+")[1];
            }
        }

        if (current_form["chkShowChart"] != null) {
            current_form["ncs_monthly_rp_show_report"].value = current_form["chkShowChart"].checked;
            current_form["ncs_monthly_rp_show_graphs"].value = current_form["chkShowChart"].checked;
        } else {
            current_form["ncs_monthly_rp_show_report"].value = false;
            current_form["ncs_monthly_rp_show_graphs"].value = false;
        }

    }

    if (hideApplicationType != true) {
        current_form["ncs_monthly_rp_app_type"].value = appType.options[appType.selectedIndex].value;
    }

    if (hideApplications != true) {
        current_form["ncs_monthly_rp_select_all_apps"].value = current_form["selectAll"].checked;

        if (appSelector.selectedIndex == -1 && (current_form["selectAll"].checked == false)) {
            document.getElementById("appAlert").innerText = errNoApp;
            document.getElementById("appAlert").style.visibility = "visible";
            return false;
        } else {
            for (var j = 0; j < appSelector.options.length; j++) {
                if (appSelector.options[j].selected) {
                    appIdArray[appCount] = appSelector.options[j].value.split("+")[0];
                    appNameArray[appCount] = appSelector.options[j].value.split("+")[1];
                    appCount++;
                }
            }
            if (appCount > maxApp) {
                document.getElementById("appAlert").innerText = errMaxApp;
                document.getElementById("appAlert").style.visibility = "visible";
                return false;
            }
            current_form['ncs_monthly_rp_app_name'].value = appNameArray.toString();
            current_form['ncs_monthly_rp_app_id'].value = appIdArray.toString();
        }
    }
    return true;
}

function dailyNCS(current_form, type, errEmptyDate, errInvalidDate, errFutureDate, errToDate, errDateRange, errFutureToDate, hideSP, hideApplications, hideApplicationType, hideOperator, errMaxApp, errNoApp, maxApp, spNotSelected) {
    var appIdArray = new Array();
    var appNameArray = new Array();
    var operatorIdArray = new Array();
    var operatorNameArray = new Array();

    var appSelector = current_form["apps"];
    var spSelector = current_form["providers"];
    var appType = current_form["applicationTypes"];
    var operatorSelector = current_form["operators"];
    var toDateString = current_form["toDate"].value;
    var fromDateString = current_form["fromDate"].value;

    var opCount = 0;
    var appCount = 0;

    if (dateValidator(fromDateString, errEmptyDate, errInvalidDate, errFutureDate) && dateValidator(toDateString, errEmptyDate, errInvalidDate, errFutureToDate)) {
        var fromDateArray = fromDateString.split("-");
        var toDateArray = toDateString.split("-");
        var fromDate = new Date();
        var toDate = new Date();
        fromDate.setFullYear(parseInt(fromDateArray[0], 10), parseInt(fromDateArray[1], 10) - 1, parseInt(fromDateArray[2], 10));
        toDate.setFullYear(parseInt(toDateArray[0], 10), parseInt(toDateArray[1], 10) - 1, parseInt(toDateArray[2], 10));
        if (toDate < fromDate) {
            showDateError(errToDate);
            return false;
        } else if (toDate - fromDate > 2592000000) {
            showDateError(errDateRange);
            return false;
        }
        current_form["ncs_daily_rp_from_date"].value = fromDateString;
        current_form["ncs_daily_rp_to_date"].value = toDateString;

        if (type == 1) {
            if (hideOperator != true) {
                if (operatorSelector.selectedIndex == -1) {
                    document.getElementById("operatorAlert").style.visibility = "visible";
                    return false;
                } else {
                    for (var i = 0; i < operatorSelector.options.length; i++) {
                        if (operatorSelector.options[i].selected) {
                            operatorIdArray[opCount] = operatorSelector.options[i].value.split("+")[0];
                            operatorNameArray[opCount] = operatorSelector.options[i].value.split("+")[1];
                            opCount++;
                        }
                    }
                    current_form["ncs_daily_rp_operator_ids"].value = operatorIdArray.toString();
                    current_form["ncs_daily_rp_operator_names"].value = operatorNameArray.toString();
                }
            }
            if (hideSP != true) {
                var spSelectedString = spSelector.options[spSelector.selectedIndex].value;
                if (spSelectedString == "") {
                    current_form["ncs_daily_rp_sp_id"].value = spNotSelected;
                    current_form["ncs_daily_rp_sp_name"].value = spNotSelected;
                } else {
                    current_form["ncs_daily_rp_sp_id"].value = spSelectedString.split("+")[0];
                    current_form["ncs_daily_rp_sp_name"].value = spSelectedString.split("+")[1];
                }
            }

            if (current_form["chkShowChart"] != null) {
                current_form["ncs_daily_rp_show_report"].value = current_form["chkShowChart"].checked;
                current_form["ncs_daily_rp_show_graphs"].value = current_form["chkShowChart"].checked;
            } else {
                current_form["ncs_daily_rp_show_report"].value = false;
                current_form["ncs_daily_rp_show_graphs"].value = false;
            }

        }

        if (hideApplicationType != true) {
            current_form["ncs_daily_rp_app_type"].value = appType.options[appType.selectedIndex].value;
        }

        if (hideApplications != true) {
            current_form["ncs_daily_rp_select_all_apps"].value = current_form["selectAll"].checked;

            if (appSelector.selectedIndex == -1 && (current_form["selectAll"].checked == false)) {
                document.getElementById("appAlert").innerText = errNoApp;
                document.getElementById("appAlert").style.visibility = "visible";
                return false;
            } else {
                for (var j = 0; j < appSelector.options.length; j++) {
                    if (appSelector.options[j].selected) {
                        appIdArray[appCount] = appSelector.options[j].value.split("+")[0];
                        appNameArray[appCount] = appSelector.options[j].value.split("+")[1];
                        appCount++;
                    }
                }
                if (appCount > maxApp) {
                    document.getElementById("appAlert").innerText = errMaxApp;
                    document.getElementById("appAlert").style.visibility = "visible";
                    return false;
                }
                current_form['ncs_daily_rp_app_name'].value = appNameArray.toString();
                current_form['ncs_daily_rp_app_id'].value = appIdArray.toString();
            }
        }
        return true;
    } else {
        return false;
    }
}

$(function () {
    $("#datepicker1").datepicker({showOtherMonths: true, selectOtherMonths: true});
    $("#datepicker1").datepicker("option", "dateFormat", "yy-mm-dd");
});

$(function () {
    $("#datepicker2").datepicker({showOtherMonths: true, selectOtherMonths: true});
    $("#datepicker2").datepicker("option", "dateFormat", "yy-mm-dd");
});

function loopSelected(current_form) {
    var selectedArray = new Array();
    var selObj = current_form["years"];
    if (selObj.selectedIndex == -1) {
        document.getElementById("yearAlert").style.visibility = "visible";
        return false;
    } else {

        var i;
        var count = 0;
        for (i = 0; i < selObj.options.length; i++) {
            if (selObj.options[i].selected) {
                selectedArray[count] = selObj.options[i].value;
                count++;
            }
        }
        document.getElementById('RP_yearly_revenue_year').value = selectedArray.toString();

        document.getElementById("RP_show_graphs").value = current_form["chkShowChart"].checked;
        return true;
    }

}

function submissionYearSelected(current_form) {
    var selectedArray = new Array();
    var selObj = current_form["years"];
    if (selObj.selectedIndex == -1) {
        document.getElementById("yearAlert").style.visibility = "visible";
        return false;
    } else {

        var i;
        var count = 0;
        for (i = 0; i < selObj.options.length; i++) {
            if (selObj.options[i].selected) {
                selectedArray[count] = selObj.options[i].value;
                count++;
            }
        }
        document.getElementById('RP_year').value = selectedArray.toString();

        document.getElementById("RP_show_graphs").value = current_form["chkShowChart"].checked;
        return true;
    }

}

function submissionMonthSelect(current_form) {
    var yearsSelector = current_form["years"];
    var year = yearsSelector.options[yearsSelector.selectedIndex].value;
    var monthsSelector = current_form["months"];
    if (monthsSelector.selectedIndex == -1) {
        document.getElementById("monthAlert").style.visibility = "visible";
        return false;
    } else {
        var selectedArray = new Array();
        var count = 0;
        for (var i = 0; i < monthsSelector.options.length; i++) {
            if (monthsSelector.options[i].selected) {
                selectedArray[count] = monthsSelector.options[i].value;
                count++;
            }
        }
        current_form["RP_year"].value = year;
        current_form["RP_month"].value = selectedArray.toString();
        current_form["RP_show_graphs"].value = current_form["chkShowChart"].checked;
        return true;
    }
}

function spSelection(current_form, errmaxsp, errnosp, maxSp) {
    var selectedArray = new Array();
    var selObj = current_form["years"];
    if (selObj.selectedIndex == -1) {
        document.getElementById("yearAlert").style.visibility = "visible";
        return false;
    } else {
        var i;
        var count = 0;
        for (i = 0; i < selObj.options.length; i++) {
            if (selObj.options[i].selected) {
                selectedArray[count] = selObj.options[i].value;
                count++;
            }
        }
        document.getElementById('RP_yearly_co_user_year').value = selectedArray.toString();
    }

    document.getElementById("RP_show_graphs").value = current_form["chkShowChart"].checked;


    var selectedArrayNames = new Array();
    var selObj2 = current_form["spNames"];
    if (selObj2.selectedIndex == -1) {
        document.getElementById("spAlert").innerText = errnosp;
        document.getElementById("spAlert").style.visibility = "visible";
        return false;
    } else {
        var j;
        var count2 = 0;
        for (j = 0; j < selObj2.options.length; j++) {
            if (selObj2.options[j].selected) {
                selectedArrayNames[count2] = selObj2.options[j].value;
                count2++;
            }
        }
        if (count2 > maxSp) {
            document.getElementById("spAlert").innerText = errmaxsp;
            document.getElementById("spAlert").style.visibility = "visible";
            return false;
        }
        var spid = new Array();
        var spname = new Array();
        for (var jj = 0; jj < selectedArrayNames.length; jj++) {
            spname[jj] = selectedArrayNames[jj].slice(0, selectedArrayNames[jj].indexOf("(", 0));
            spid[jj] = selectedArrayNames[jj].slice(selectedArrayNames[jj].indexOf("(", 0) + 1, selectedArrayNames[jj].indexOf(")", 0));
        }
        document.getElementById('RP_yearly_co_user_name').value = spname.toString();
        document.getElementById('RP_yearly_co_user_id').value = spid.toString();

        return true;
    }
}

function reconciliationSelection(current_form, navigationReport) {
    current_form["__report"].value = navigationReport;
    var yearsSelector = current_form["years"];
    current_form["RP_reconciliation_year"].value = yearsSelector.options[yearsSelector.selectedIndex].value;
    var selectAllSPs = current_form["selectAll"].checked;
    current_form["RP_select_all"].value = selectAllSPs;
    var monthSelector = current_form["months"];
    current_form["RP_reconciliation_month"].value = monthSelector.options[monthSelector.selectedIndex].value;

    var spIdArray = new Array();
    var spNameArray = new Array();
    var coopIdArray = new Array();
    var spSelector = current_form["providers"];
    if (spSelector.selectedIndex == -1 && selectAllSPs == false) {
        document.getElementById("spAlert").style.visibility = "visible";
        return false;
    } else {
        if (spSelector.selectedIndex != -1 && selectAllSPs == false) {
            var i;
            var count = 0;
            for (i = 0; i < spSelector.options.length; i++) {
                if (spSelector.options[i].selected) {
                    spIdArray[count] = spSelector.options[i].value.split("+")[0];
                    spNameArray[count] = spSelector.options[i].value.split("+")[1];
                    coopIdArray[count] = spSelector.options[i].value.split("+")[2];
                    count++;
                }
            }
            document.getElementById('RP_reconciliation_sp').value = spIdArray.toString();
            document.getElementById('RP_reconciliation_sp_name').value = spNameArray.toString();
            document.getElementById('RP_reconciliation_coopId').value = coopIdArray.toString();
            return true;
        }
        else if (selectAllSPs == true) {
            for (i = 0; i < spSelector.options.length; i++) {
                spNameArray[i] = spSelector.options[i].value.split("+")[1];
            }
            document.getElementById('RP_reconciliation_sp').value = null;
            document.getElementById('RP_reconciliation_sp_name').value = spNameArray.toString();
            document.getElementById('RP_reconciliation_coopId').value = null;
            return true;
        }
    }
}

function reconciliationSelectionWithSpLimit(current_form, errmaxsp, errnosp, maxSp) {
    var yearsSelector = current_form["years"];
    current_form["RP_reconciliation_year"].value = yearsSelector.options[yearsSelector.selectedIndex].value;
    var selectAllSPs = current_form["selectAll"].checked;
    current_form["RP_select_all"].value = selectAllSPs;
    var monthSelector = current_form["months"];
    current_form["RP_reconciliation_month"].value = monthSelector.options[monthSelector.selectedIndex].value;

    var spIdArray = new Array();
    var coopIdArray = new Array();
    var spSelector = current_form["providers"];
    if (spSelector.selectedIndex == -1 && selectAllSPs == false) {
        document.getElementById("spAlert").innerText = errnosp;
        document.getElementById("spAlert").style.visibility = "visible";
        return false;
    } else {
        if (spSelector.selectedIndex != -1 && selectAllSPs == false) {
            var i;
            var count = 0;
            for (i = 0; i < spSelector.options.length; i++) {
                if (spSelector.options[i].selected) {
                    spIdArray[count] = spSelector.options[i].value.split("+")[0];
                    coopIdArray[count] = spSelector.options[i].value.split("+")[2];
                    count++;
                }
            }
            if (count > maxSp) {
                document.getElementById("spAlert").innerText = errmaxsp;
                document.getElementById("spAlert").style.visibility = "visible";
                return false;
            }
            document.getElementById('RP_reconciliation_sp').value = spIdArray.toString();
            document.getElementById('RP_reconciliation_coopId').value = coopIdArray.toString();
            return true;
        }
        else if (selectAllSPs == true) {
            document.getElementById('RP_reconciliation_sp').value = null;
            document.getElementById('RP_reconciliation_coopId').value = null;
            return true;
        }
    }
}

function appSelection(current_form, type, errmaxapp, errnoapp, maxApp, spNotSelected) {
    var selectedArray = new Array();
    var appId = new Array();
    var appName = new Array();

    var yearSelector = current_form["years"];
    var appSelector = current_form["apps"];
    var sort = current_form["sortWay"];
    var order = current_form["orderWay"];
    var appType = current_form["applicationTypes"];

    var yearCount = 0;
    var appCount = 0;

    if (yearSelector.selectedIndex == -1) {
        document.getElementById("yearAlert").style.visibility = "visible";
        return false;
    } else {
        var i;
        for (i = 0; i < yearSelector.options.length; i++) {
            if (yearSelector.options[i].selected) {
                selectedArray[yearCount] = yearSelector.options[i].value;
                yearCount++;
            }
        }
        document.getElementById('RP_app_year').value = selectedArray.toString();
    }

    document.getElementById("RP_select_all_apps").value = current_form["selectAll"].checked;
    document.getElementById("RP_show_graphs").value = current_form["chkShowChart"].checked;

    if (type == 1) {
        var spSelector = current_form["providers"];
        var spSelectedString = spSelector.options[spSelector.selectedIndex].value;
        if (spSelectedString == "") {
            current_form["RP_sp_id"].value = spNotSelected;
            current_form["RP_sp_name"].value = spNotSelected;
        } else {
            current_form["RP_sp_id"].value = spSelectedString.split("+")[0];
            current_form["RP_sp_name"].value = spSelectedString.split("+")[1];
        }
    }

    var selectedAppType = appType.options[appType.selectedIndex].value;
    current_form["RP_app_type"].value = selectedAppType;

    if (appSelector.selectedIndex == -1 && (current_form["selectAll"].checked == false)) {
        document.getElementById("appAlert").innerText = errnoapp;
        document.getElementById("appAlert").style.visibility = "visible";
        return false;
    } else {
        for (var j = 0; j < appSelector.options.length; j++) {
            if (appSelector.options[j].selected) {
                appId[appCount] = appSelector.options[j].value.split("+")[0];
                appName[appCount] = appSelector.options[j].value.split("+")[1];
                appCount++;
            }
        }
        if (appCount > maxApp) {
            document.getElementById("appAlert").innerText = errmaxapp;
            document.getElementById("appAlert").style.visibility = "visible";
            return false;
        }
        document.getElementById('RP_app_name').value = appName.toString();
        document.getElementById('RP_app_id').value = appId.toString();

        if (yearCount == 1) {
            if (sort.selectedIndex != -1) {
                for (var k = 0; k < sort.options.length; k++) {
                    if (sort.options[k].selected) {
                        document.getElementById('RP_sort').value = sort.options[k].value;
                    }
                }
            }

            if (order.selectedIndex != -1) {
                for (var m = 0; m < sort.options.length; m++) {
                    if (order.options[m].selected) {
                        document.getElementById('RP_order').value = order.options[m].value;
                    }
                }
            }
        }

        return true;
    }
}

function monthlyAppSelection(current_form, type, errmaxapp, errnoapp, maxApp, spNotSelected) {
    var sort = current_form["sortWay"];
    var order = current_form["orderWay"];
    var spSelector = current_form["providers"];
    var appSelector = current_form["apps"];
    var appType = current_form["applicationTypes"];

    var appId = new Array();
    var appName = new Array();
    var selectedArray = new Array();

    var monthCount = 0;
    var appCount = 0;

    document.getElementById("RP_show_graphs").value = current_form["chkShowChart"].checked;
    document.getElementById("RP_select_all_apps").value = current_form["selectAll"].checked;

    var yearsSelector = current_form["years"];
    current_form["RP_app_monthly_year"].value = yearsSelector.options[yearsSelector.selectedIndex].value;

    var monthSelector = current_form["months"];
    if (monthSelector.selectedIndex == -1) {
        document.getElementById("monthAlert").style.visibility = "visible";
        return false;
    } else {
        var i;
        for (i = 0; i < monthSelector.options.length; i++) {
            if (monthSelector.options[i].selected) {
                selectedArray[monthCount] = monthSelector.options[i].value;
                monthCount++;
            }
        }
        document.getElementById('RP_app_monthly_month').value = selectedArray.toString();
    }

    if (type == 1) {
        var spSelectedString = spSelector.options[spSelector.selectedIndex].value;
        if (spSelectedString == "") {
            /*document.getElementById("spAlert").style.visibility = "visible";
             return false;*/
            current_form["RP_app_monthly_sp_id"].value = spNotSelected;
            current_form["RP_app_monthly_sp_name"].value = spNotSelected;
        } else {
            current_form["RP_app_monthly_sp_id"].value = spSelectedString.split("+")[0];
            current_form["RP_app_monthly_sp_name"].value = spSelectedString.split("+")[1];
        }
    }

    var selectedAppType = appType.options[appType.selectedIndex].value;
    current_form["RP_app_type"].value = selectedAppType;

    if (appSelector.selectedIndex == -1 && (current_form["selectAll"].checked == false)) {
        document.getElementById("appAlert").innerText = errnoapp;
        document.getElementById("appAlert").style.visibility = "visible";
        return false;
    } else {

        for (var j = 0; j < appSelector.options.length; j++) {
            if (appSelector.options[j].selected) {
                appId[appCount] = appSelector.options[j].value.split("+")[0];
                appName[appCount] = appSelector.options[j].value.split("+")[1];
                appCount++;
            }
        }
        if (appCount > maxApp) {
            document.getElementById("appAlert").innerText = errmaxapp;
            document.getElementById("appAlert").style.visibility = "visible";
            return false;
        }

        document.getElementById('RP_app_monthly_appname').value = appName.toString();
        document.getElementById('RP_app_monthly_appid').value = appId.toString();

        if (appCount == 1) {
            if (sort.selectedIndex != -1) {
                for (var k = 0; k < sort.options.length; k++) {
                    if (sort.options[k].selected) {
                        document.getElementById('RP_sort').value = sort.options[k].value;
                    }
                }
            }

            if (order.selectedIndex != -1) {
                for (var m = 0; m < sort.options.length; m++) {
                    if (order.options[m].selected) {
                        document.getElementById('RP_order').value = order.options[m].value;
                    }
                }
            }
        }
        return true;
    }
}

function dailyAppSelection(current_form, type, err1, err2, err3, err4, err5, maxAppErr, noAppErr, maxApp, err6, spNotSelected) {
    var fromDateString = current_form["fromDate"].value;
    var toDateString = current_form["toDate"].value
    var spSelector = current_form["providers"];
    var appType = current_form["applicationTypes"];
    var appSelector = current_form["apps"];

    var appId = new Array();
    var appName = new Array();

    if (dateValidator(fromDateString, err1, err2, err3) && dateValidator(toDateString, err1, err2, err6)) {
        var fromDateArray = fromDateString.split("-");
        var toDateArray = toDateString.split("-");
        var fromDate = new Date();
        var toDate = new Date();
        fromDate.setFullYear(parseInt(fromDateArray[0], 10), parseInt(fromDateArray[1], 10) - 1, parseInt(fromDateArray[2], 10));
        toDate.setFullYear(parseInt(toDateArray[0], 10), parseInt(toDateArray[1], 10) - 1, parseInt(toDateArray[2], 10));
        if (toDate < fromDate) {
            showDateError(err4);
            return false;
        }
        else if (toDate - fromDate > 2592000000) {
            showDateError(err5);
            return false;
        }
        document.getElementById('RP_app_daily_start_date').value = fromDateString;
        document.getElementById('RP_app_daily_end_date').value = toDateString;

        document.getElementById("RP_show_graphs").value = current_form["chkShowChart"].checked;
        document.getElementById("RP_select_all_apps").value = current_form["selectAll"].checked;

        if (type == 1) {
            var spSelectedString = spSelector.options[spSelector.selectedIndex].value;
            if (spSelectedString == "") {
                /*document.getElementById("spAlert").style.visibility = "visible";
                 return false;*/
                current_form["RP_app_daily_sp_id"].value = spNotSelected;
                current_form["RP_app_daily_sp_name"].value = spNotSelected;
            } else {
                current_form["RP_app_daily_sp_id"].value = spSelectedString.split("+")[0];
                current_form["RP_app_daily_sp_name"].value = spSelectedString.split("+")[1];
            }
        }

        var selectedAppType = appType.options[appType.selectedIndex].value;
        current_form["RP_app_daily_type"].value = selectedAppType;

        if (appSelector.selectedIndex == -1 && (current_form["selectAll"].checked == false)) {
            document.getElementById("appAlert").innerText = noAppErr;
            document.getElementById("appAlert").style.visibility = "visible";
            return false;
        }
        else {
            var appCount = 0;
            for (var j = 0; j < appSelector.options.length; j++) {
                if (appSelector.options[j].selected) {
                    appId[appCount] = appSelector.options[j].value.split("+")[0];
                    appName[appCount] = appSelector.options[j].value.split("+")[1];
                    appCount++;
                }
            }
            if (appCount > maxApp) {
                document.getElementById("appAlert").innerText = maxAppErr;
                document.getElementById("appAlert").style.visibility = "visible";
                return false;
            }

            document.getElementById('RP_app_daily_appname').value = appName.toString();
            document.getElementById('RP_app_daily_appid').value = appId.toString();
        }

        return true;
    }
    return false;

}

function governanceSelection(current_form, err1, err2, err3, err4, err5, noAppErr, err6) {
    var fromDateString = current_form["fromDate"].value;
    var toDateString = current_form["toDate"].value;

    if (dateValidator(fromDateString, err1, err2, err3) && dateValidator(toDateString, err1, err2, err6)) {
        var fromDateArray = fromDateString.split("-");
        var toDateArray = toDateString.split("-");
        var fromDate = new Date();
        var toDate = new Date();
        fromDate.setFullYear(parseInt(fromDateArray[0], 10), parseInt(fromDateArray[1], 10) - 1, parseInt(fromDateArray[2], 10));
        toDate.setFullYear(parseInt(toDateArray[0], 10), parseInt(toDateArray[1], 10) - 1, parseInt(toDateArray[2], 10));
        if (toDate < fromDate) {
            showDateError(err4);
            return false;
        }
        else if (toDate - fromDate > 2592000000) {
            showDateError(err5);
            return false;
        }
        document.getElementById('RP_governance_start_date').value = fromDateString;
        document.getElementById('RP_governance_end_date').value = toDateString;

        var appIdArray = new Array();
        var appSelector = current_form["appDetails"];
        if (appSelector.selectedIndex == -1) {
            document.getElementById("appAlert").style.visibility = "visible";
            return false;
        } else {
            var i;
            var count = 0;
            for (i = 0; i < appSelector.options.length; i++) {
                if (appSelector.options[i].selected) {
                    appIdArray[count] = appSelector.options[i].value.split("+")[0];
                    count++;
                }
            }
            document.getElementById('RP_governance_app_id').value = appIdArray.toString();
            return true;
        }
    }
    return false;
}

function yearlyOPSelection() {
    var selectedArray = new Array();
    var selObj = document.getElementById("years");
    if (selObj.selectedIndex == -1) {
        document.getElementById("yearAlert").style.visibility = "visible";
        return false;
    } else {
        var i;
        var count = 0;
        for (i = 0; i < selObj.options.length; i++) {
            if (selObj.options[i].selected) {
                selectedArray[count] = selObj.options[i].value;
                count++;
            }
        }
        document.getElementById('RP_yearly_ope_year').value = selectedArray.toString();
    }

    document.getElementById("RP_yearly_ope_showGraph").value = document.getElementById("chkShowChart").checked;

    var selectedArrayOp = new Array();
    var selectedArrayOpName = new Array();
    var selObjOp = document.getElementById("opNames");
    if (selObjOp.selectedIndex == -1) {
        document.getElementById("operatorAlert").style.visibility = "visible";
        return false;
    } else {
        var count2 = 0;
        for (var j = 0; j < selObjOp.options.length; j++) {
            if (selObjOp.options[j].selected) {
                selectedArrayOp[count2] = selObjOp.options[j].value.split("+")[0];
                selectedArrayOpName[count2] = selObjOp.options[j].value.split("+")[1];
                count2++;
            }
        }
        document.getElementById('RP_yearly_ope_opid').value = selectedArrayOp.toString();
        document.getElementById('RP_yearly_ope_opname').value = selectedArrayOpName.toString();

        return true;
    }

}

function monthlyOpSelection() {
    document.getElementById("RP_monthly_op_showGraphs").value = document.getElementById("chkShowChart").checked;

    var yearsSelector = document.getElementById("years");
    document.getElementById("RP_monthly_op_year").value = yearsSelector.options[yearsSelector.selectedIndex].value;

    var selectedArray = new Array();
    var selObj = document.getElementById("months");
    if (selObj.selectedIndex == -1) {
        document.getElementById("monthAlert").style.visibility = "visible";
        return false;
    } else {
        var i;
        var count = 0;
        for (i = 0; i < selObj.options.length; i++) {
            if (selObj.options[i].selected) {
                selectedArray[count] = selObj.options[i].value;
                count++;
            }
        }
        document.getElementById('RP_monthly_op_month').value = selectedArray.toString();
    }

    //====================================================================================
    var selectedArrayNames = new Array();
    var selectedArrayId = new Array();
    var selObj2 = document.getElementById("opNames");
    if (selObj2.selectedIndex == -1) {
        document.getElementById("operatorAlert").style.visibility = "visible";
        return false;
    } else {
        var j;
        var count2 = 0;
        for (j = 0; j < selObj2.options.length; j++) {
            if (selObj2.options[j].selected) {
                selectedArrayId[count2] = selObj2.options[j].value.split("+")[0];
                selectedArrayNames[count2] = selObj2.options[j].value.split("+")[1];
                count2++;
            }
        }
        document.getElementById('RP_monthly_op_opname').value = selectedArrayNames.toString();
        document.getElementById('RP_monthly_op_opid').value = selectedArrayId.toString();

        return true;
    }

}

function dailyOPSelection(err1, err2, err3, err4, err5) {

    var fromDateString = document.getElementById('datepicker1').value;
    var toDateString = document.getElementById('datepicker2').value;

    if (dateValidator(fromDateString, err1, err2, err3) && dateValidator(toDateString, err1, err2, err3)) {
        var fromDateArray = fromDateString.split("-");
        var toDateArray = toDateString.split("-");
        var fromDate = new Date();
        var toDate = new Date();
        fromDate.setFullYear(parseInt(fromDateArray[0], 10), parseInt(fromDateArray[1], 10) - 1, parseInt(fromDateArray[2], 10));
        toDate.setFullYear(parseInt(toDateArray[0], 10), parseInt(toDateArray[1], 10) - 1, parseInt(toDateArray[2], 10));
        if (toDate < fromDate) {
            showDateError(err4);
            return false;
        } else if (toDate - fromDate > 2592000000) {
            showDateError(err5);
            return false;
        }
        document.getElementById('RP_daily_op_start_date').value = fromDateString;
        document.getElementById('RP_daily_op_end_date').value = toDateString;

        if (document.getElementById("chkShowChart").checked) {
            document.getElementById("RP_daily_op_showGraphs").value = "True";
        }
        else {
            document.getElementById("RP_daily_op_showGraphs").value = "False";
        }


        var selectedArrayNames = new Array();
        var selectedArrayId = new Array();
        var selObj2 = document.getElementById("opNames");
        if (selObj2.selectedIndex == -1) {
            document.getElementById("operatorAlert").style.visibility = "visible";
            return false;
        } else {
            var j;
            var count2 = 0;
            for (j = 0; j < selObj2.options.length; j++) {
                if (selObj2.options[j].selected) {
                    selectedArrayId[count2] = selObj2.options[j].value.split("+")[0];
                    selectedArrayNames[count2] = selObj2.options[j].value.split("+")[1];
                    count2++;
                }
            }
            document.getElementById('RP_daily_op_opid').value = selectedArrayId.toString();
            document.getElementById('RP_daily_op_opname').value = selectedArrayNames.toString();

            return true;
        }
    } else {
        return false;
    }
}

function failedReport(err1, err2, err3, err4, err5, err6) {
    var fromDateString = document.getElementById('datepicker1').value;
    var toDateString = document.getElementById('datepicker2').value;

    if (dateValidator(fromDateString, err1, err2, err3) && dateValidator(toDateString, err1, err2, err6)) {
        var fromDateArray = fromDateString.split("-");
        var toDateArray = toDateString.split("-");
        var fromDate = new Date();
        var toDate = new Date();
        fromDate.setFullYear(parseInt(fromDateArray[0], 10), parseInt(fromDateArray[1], 10) - 1, parseInt(fromDateArray[2], 10));
        toDate.setFullYear(parseInt(toDateArray[0], 10), parseInt(toDateArray[1], 10) - 1, parseInt(toDateArray[2], 10));
        if (toDate < fromDate) {
            showDateError(err4);
            return false;
        } else if (toDate - fromDate > 2592000000) {
            showDateError(err5);
            return false;
        }
        document.getElementById('RP_failed_trans_start_date').value = fromDateString;
        document.getElementById('RP_failed_trans_end_date').value = toDateString;
        return true;
    }
    else {
        return false;
    }


}

function yearlyAdd() {
    var yearsSelector = document.getElementById("years");
    document.getElementById("RP_yearly_add_year").value = yearsSelector.options[yearsSelector.selectedIndex].value;

    var selectedArrayNames = new Array();
    var selObj2 = document.getElementById("addNames");
    if (selObj2.selectedIndex == -1) {
        document.getElementById("addAlert").style.visibility = "visible";
        return false;
    } else {
        var j;
        var count2 = 0;
        for (j = 0; j < selObj2.options.length; j++) {
            if (selObj2.options[j].selected) {
                selectedArrayNames[count2] = selObj2.options[j].value;
                count2++;
            }
        }
        document.getElementById('RP_yearly_add_addname').value = selectedArrayNames.toString();

        return true;
    }

}

function monthlyAdd() {
    var yearsSelector = document.getElementById("years");
    document.getElementById("RP_monthly_add_year").value = yearsSelector.options[yearsSelector.selectedIndex].value;

    var selectedMonth = document.getElementById("months");
    document.getElementById("RP_monthly_add_month").value = selectedMonth.options[selectedMonth.selectedIndex].value;

    var selectedArrayNames = new Array();
    var selObj2 = document.getElementById("addNames");
    if (selObj2.selectedIndex == -1) {
        document.getElementById("addAlert").style.visibility = "visible";
        return false;
    } else {
        var j;
        var count2 = 0;
        for (j = 0; j < selObj2.options.length; j++) {
            if (selObj2.options[j].selected) {
                selectedArrayNames[count2] = selObj2.options[j].value;
                count2++;
            }
        }
        document.getElementById('RP_monthly_add_addname').value = selectedArrayNames.toString();

        return true;
    }

}

function yearlyAppAdd(type, errmaxapp, errnoapp, maxApp, spNotSelected) {
    var yearsSelector = document.getElementById("years");
    var yearSelector = document.getElementById("years");
    var appSelector = document.getElementById("apps");
    document.getElementById("RP_yearly_app_add_year").value = yearsSelector.options[yearsSelector.selectedIndex].value;

    if (type == 1) {
        var spSelector = document.getElementById("providers");
        var spSelectedString = spSelector.options[spSelector.selectedIndex].value;
        if (spSelectedString == "") {
            document.getElementById("RP_yearly_app_add_sp_id").value = spNotSelected;
            document.getElementById("RP_yearly_app_add_sp_name").value = spNotSelected;
        } else {
            document.getElementById("RP_yearly_app_add_sp_id").value = spSelectedString.split("+")[0];
            document.getElementById("RP_yearly_app_add_sp_name").value = spSelectedString.split("+")[1];
        }
    }

    if (appSelector.selectedIndex == -1) {
        document.getElementById("appAlert").innerText = errnoapp;
        document.getElementById("appAlert").style.visibility = "visible";
        return false;
    } else {
        var count2 = 0;
        var appId = new Array();
        var appName = new Array();
        for (var j = 0; j < appSelector.options.length; j++) {
            if (appSelector.options[j].selected) {
                appId[count2] = appSelector.options[j].value.split("+")[0];
                appName[count2] = appSelector.options[j].value.split("+")[1];
                count2++;
            }
        }
        if (count2 > maxApp) {
            document.getElementById("appAlert").innerText = errmaxapp;
            document.getElementById("appAlert").style.visibility = "visible";
            return false;
        }
        document.getElementById('RP_yearly_app_add_app_name').value = appName.toString();
        document.getElementById('RP_yearly_app_add_app_id').value = appId.toString();
        return true;
    }

}

function monthlyAppAdd(type, errmaxapp, errnoapp, maxApp, spNotSelected) {
    var yearsSelector = document.getElementById("years");
    document.getElementById("RP_monthly_app_add_year").value = yearsSelector.options[yearsSelector.selectedIndex].value;

    var selectedMonth = document.getElementById("months");
    document.getElementById("RP_monthly_app_add_month").value = selectedMonth.options[selectedMonth.selectedIndex].value;

    if (type == 1) {
        var spSelector = document.getElementById("providers");
        var spSelectedString = spSelector.options[spSelector.selectedIndex].value;
        if (spSelectedString == "") {
            document.getElementById("RP_monthly_app_add_sp_id").value = spNotSelected;
            document.getElementById("RP_monthly_app_add_sp_name").value = spNotSelected;
        } else {
            document.getElementById("RP_monthly_app_add_sp_id").value = spSelectedString.split("+")[0];
            document.getElementById("RP_monthly_app_add_sp_name").value = spSelectedString.split("+")[1];
        }
    }

    //====================================================================================
    var appSelector = document.getElementById("apps");
    if (appSelector.selectedIndex == -1) {
        document.getElementById("appAlert").innerText = errnoapp;
        document.getElementById("appAlert").style.visibility = "visible";
        return false;
    } else {
        var appId = new Array();
        var appName = new Array();
        var count2 = 0;
        for (var j = 0; j < appSelector.options.length; j++) {
            if (appSelector.options[j].selected) {
                appId[count2] = appSelector.options[j].value.split("+")[0];
                appName[count2] = appSelector.options[j].value.split("+")[1];
                count2++;
            }
        }
        if (count2 > maxApp) {
            document.getElementById("appAlert").innerText = errmaxapp;
            document.getElementById("appAlert").style.visibility = "visible";
            return false;
        }

        document.getElementById('RP_monthly_app_add_app_name').value = appName.toString();
        document.getElementById('RP_monthly_app_add_app_id').value = appId.toString();

        return true;
    }

}


function yearlySolturaAppSelection(current_form, type, errmaxapp, errnoapp, maxApp, spNotSelected) {
    var selectedArray = new Array();
    var appId = new Array();
    var appName = new Array();
    var yearSelector = current_form["years"];
    var appSelector = current_form["apps"];
    var yearCount = 0;
    var appCount = 0;

    if (yearSelector.selectedIndex == -1) {
        document.getElementById("yearAlert").style.visibility = "visible";
        return false;
    } else {
        for (var i = 0; i < yearSelector.options.length; i++) {
            if (yearSelector.options[i].selected) {
                selectedArray[yearCount] = yearSelector.options[i].value;
                yearCount++;
            }
        }
        document.getElementById('soltura_yearly_rp_years').value = selectedArray.toString();
    }

    document.getElementById("soltura_yearly_rp_select_all_apps").value = current_form["selectAll"].checked;

    if (type == 1) {
        var spSelector = current_form["providers"];
        var spSelectedString = spSelector.options[spSelector.selectedIndex].value;
        if (spSelectedString == "") {
            current_form["soltura_yearly_rp_sp_id"].value = spNotSelected;
            current_form["soltura_yearly_rp_sp_name"].value = spNotSelected;
        } else {
            current_form["soltura_yearly_rp_sp_id"].value = spSelectedString.split("+")[0];
            current_form["soltura_yearly_rp_sp_name"].value = spSelectedString.split("+")[1];
        }
    }

    if (appSelector.selectedIndex == -1 && (current_form["selectAll"].checked == false)) {
        document.getElementById("appAlert").innerText = errnoapp;
        document.getElementById("appAlert").style.visibility = "visible";
        return false;
    } else {

        for (var j = 0; j < appSelector.options.length; j++) {
            if (appSelector.options[j].selected) {
                appId[appCount] = appSelector.options[j].value.split("+")[0];
                appName[appCount] = appSelector.options[j].value.split("+")[1];
                appCount++;
            }
        }
        if (appCount > maxApp) {
            document.getElementById("appAlert").innerText = errmaxapp;
            document.getElementById("appAlert").style.visibility = "visible";
            return false;
        }
        document.getElementById('soltura_yearly_rp_app_name').value = appName.toString();
        document.getElementById('soltura_yearly_rp_app_id').value = appId.toString();

        return true;
    }
}

function yearlyDownloadableAppSelection(current_form, type, errmaxapp, errnoapp, maxApp, spNotSelected) {
    var appId = new Array();
    var appName = new Array();
    var selectedArray = new Array();
    var appSelector = current_form["apps"];
    var yearSelector = current_form["years"];
    var spSelector = current_form["providers"];
    var chargingTypeSelector = document.getElementById("chargingTypes");
    var yearCount = 0;
    var appCount = 0;

    if (yearSelector.selectedIndex == -1) {
        document.getElementById("yearAlert").style.visibility = "visible";
        return false;
    } else {
        for (var i = 0; i < yearSelector.options.length; i++) {
            if (yearSelector.options[i].selected) {
                selectedArray[yearCount] = yearSelector.options[i].value;
                yearCount++;
            }
        }
        current_form['downloadable_yearly_rp_years'].value = selectedArray.toString();
    }

    if (type == 1) {
        var spSelectedString = spSelector.options[spSelector.selectedIndex].value;
        if (spSelectedString == "") {
            current_form["downloadable_yearly_rp_sp_id"].value = spNotSelected;
            current_form["downloadable_yearly_rp_sp_name"].value = spNotSelected;
        } else {
            current_form["downloadable_yearly_rp_sp_id"].value = spSelectedString.split("+")[0];
            current_form["downloadable_yearly_rp_sp_name"].value = spSelectedString.split("+")[1];
        }
    }

    document.getElementById("downloadable_yearly_rp_charging_type").value = chargingTypeSelector.options[chargingTypeSelector.selectedIndex].value;
    document.getElementById("downloadable_yearly_rp_select_all_apps").value = current_form["selectAll"].checked;

    if (appSelector.selectedIndex == -1 && (current_form["selectAll"].checked == false)) {
        document.getElementById("appAlert").innerText = errnoapp;
        document.getElementById("appAlert").style.visibility = "visible";
        return false;
    } else {
        for (var j = 0; j < appSelector.options.length; j++) {
            if (appSelector.options[j].selected) {
                appId[appCount] = appSelector.options[j].value.split("+")[0];
                appName[appCount] = appSelector.options[j].value.split("+")[1];
                appCount++;
            }
        }
        if (appCount > maxApp) {
            document.getElementById("appAlert").innerText = errmaxapp;
            document.getElementById("appAlert").style.visibility = "visible";
            return false;
        }
        document.getElementById('downloadable_yearly_rp_app_name').value = appName.toString();
        document.getElementById('downloadable_yearly_rp_app_id').value = appId.toString();
        return true;
    }
}

function monthlySolturaAppSelection(current_form, type, errmaxapp, errnoapp, maxApp, spNotSelected) {

    var yearCount = 0;
    var appId = new Array();
    var appName = new Array();
    var appCount = 0;
    document.getElementById("soltura_monthly_rp_show_graphs").value = current_form["chkShowChart"].checked;
    document.getElementById("soltura_monthly_rp_select_all_apps").value = current_form["selectAll"].checked;

    var yearsSelector = current_form["years"];
    current_form["soltura_monthly_rp_app_monthly_year"].value = yearsSelector.options[yearsSelector.selectedIndex].value;

    //soltura_monthly_rp_app_monthly_appname
    var selectedArray = new Array();
    var monthSelector = current_form["months"];
    if (monthSelector.selectedIndex == -1) {
        document.getElementById("monthAlert").style.visibility = "visible";
        return false;
    } else {
        for (var i = 0; i < monthSelector.options.length; i++) {
            if (monthSelector.options[i].selected) {
                selectedArray[yearCount] = monthSelector.options[i].value;
                yearCount++;
            }
        }
        document.getElementById('soltura_monthly_rp_month').value = selectedArray.toString();
    }

    if (type == 1) {
        var spSelector = current_form["providers"];
        var spSelectedString = spSelector.options[spSelector.selectedIndex].value;
        if (spSelectedString == "") {
            /*document.getElementById("spAlert").style.visibility = "visible";
             return false;*/
            current_form["soltura_monthly_rp_sp_id"].value = spNotSelected;
            current_form["soltura_monthly_rp_sp_name"].value = spNotSelected;
        } else {
            current_form["soltura_monthly_rp_sp_id"].value = spSelectedString.split("+")[0];
            current_form["soltura_monthly_rp_sp_name"].value = spSelectedString.split("+")[1];
        }
    }

    var appSelector = current_form["apps"];
    if (appSelector.selectedIndex == -1 && (current_form["selectAll"].checked == false)) {
        document.getElementById("appAlert").innerText = errnoapp;
        document.getElementById("appAlert").style.visibility = "visible";
        return false;
    } else {
        for (var j = 0; j < appSelector.options.length; j++) {
            if (appSelector.options[j].selected) {
                appId[appCount] = appSelector.options[j].value.split("+")[0];
                appName[appCount] = appSelector.options[j].value.split("+")[1];
                appCount++;
            }
        }
        if (appCount > maxApp) {
            document.getElementById("appAlert").innerText = errmaxapp;
            document.getElementById("appAlert").style.visibility = "visible";
            return false;
        }

        document.getElementById('soltura_monthly_rp_app_name').value = appName.toString();
        document.getElementById('soltura_monthly_rp_app_id').value = appId.toString();
        return true;
    }
}

function monthlyDownloadableAppSelection(current_form, type, errmaxapp, errnoapp, maxApp, spNotSelected) {

    var appId = new Array();
    var appName = new Array();
    var selectedArray = new Array();
    var appSelector = current_form["apps"];
    var yearsSelector = current_form["year"];
    var monthSelector = current_form["months"];
    var spSelector = current_form["providers"];
    var chargingTypeSelector = document.getElementById("chargingTypes");
    var monthCount = 0;
    var appCount = 0;

    current_form["downloadable_monthly_rp_year"].value = yearsSelector.options[yearsSelector.selectedIndex].value;

    if (monthSelector.selectedIndex == -1) {
        document.getElementById("monthAlert").style.visibility = "visible";
        return false;
    } else {
        for (var i = 0; i < monthSelector.options.length; i++) {
            if (monthSelector.options[i].selected) {
                selectedArray[monthCount] = monthSelector.options[i].value;
                monthCount++;
            }
        }
        current_form['downloadable_monthly_rp_months'].value = selectedArray.toString();
    }

    if (type == 1) {
        var spSelectedString = spSelector.options[spSelector.selectedIndex].value;
        if (spSelectedString == "") {
            current_form["downloadable_monthly_rp_sp_id"].value = spNotSelected;
            current_form["downloadable_monthly_rp_sp_name"].value = spNotSelected;
        } else {
            current_form["downloadable_monthly_rp_sp_id"].value = spSelectedString.split("+")[0];
            current_form["downloadable_monthly_rp_sp_name"].value = spSelectedString.split("+")[1];
        }
    }

    document.getElementById("downloadable_monthly_rp_charging_type").value = chargingTypeSelector.options[chargingTypeSelector.selectedIndex].value;
    current_form["downloadable_monthly_rp_select_all_apps"].value = current_form["selectAll"].checked;

    if (appSelector.selectedIndex == -1 && (current_form["selectAll"].checked == false)) {
        document.getElementById("appAlert").innerText = errnoapp;
        document.getElementById("appAlert").style.visibility = "visible";
        return false;
    } else {
        for (var j = 0; j < appSelector.options.length; j++) {
            if (appSelector.options[j].selected) {
                appId[appCount] = appSelector.options[j].value.split("+")[0];
                appName[appCount] = appSelector.options[j].value.split("+")[1];
                appCount++;
            }
        }
        if (appCount > maxApp) {
            document.getElementById("appAlert").innerText = errmaxapp;
            document.getElementById("appAlert").style.visibility = "visible";
            return false;
        }

        document.getElementById('downloadable_monthly_rp_app_name').value = appName.toString();
        document.getElementById('downloadable_monthly_rp_app_id').value = appId.toString();
        return true;
    }
}

function dailySolturaAppSelection(current_form, type, err1, err2, err3, err4, err5, maxAppErr, noAppErr, maxApp, err6, spNotSelected) {
    var fromDateString = current_form["fromDate"].value;
    var toDateString = current_form["toDate"].value;

    if (dateValidator(fromDateString, err1, err2, err3) && dateValidator(toDateString, err1, err2, err6)) {
        var fromDateArray = fromDateString.split("-");
        var toDateArray = toDateString.split("-");
        var fromDate = new Date();
        var toDate = new Date();
        fromDate.setFullYear(parseInt(fromDateArray[0], 10), parseInt(fromDateArray[1], 10) - 1, parseInt(fromDateArray[2], 10));
        toDate.setFullYear(parseInt(toDateArray[0], 10), parseInt(toDateArray[1], 10) - 1, parseInt(toDateArray[2], 10));
        if (toDate < fromDate) {
            showDateError(err4);
            return false;
        }
        else if (toDate - fromDate > 2592000000) {
            showDateError(err5);
            return false;
        }
        document.getElementById('soltura_daily_rp_start_date').value = fromDateString;
        document.getElementById('soltura_daily_rp_end_date').value = toDateString;

        document.getElementById("RP_show_graphs").value = current_form["chkShowChart"].checked;

        if (type == 1) {
            var spSelector = current_form["providers"];
            var spSelectedString = spSelector.options[spSelector.selectedIndex].value;
            if (spSelectedString == "") {
                /*document.getElementById("spAlert").style.visibility = "visible";
                 return false;*/
                current_form["soltura_daily_rp_sp_id"].value = spNotSelected;
                current_form["soltura_daily_rp_sp_name"].value = spNotSelected;
            } else {
                current_form["soltura_daily_rp_sp_id"].value = spSelectedString.split("+")[0];
                current_form["soltura_daily_rp_sp_name"].value = spSelectedString.split("+")[1];
            }
        }

        var appId = new Array();
        var appName = new Array();
        var appSelector = current_form["apps"];
        if (appSelector.selectedIndex == -1) {
            document.getElementById("appAlert").innerText = noAppErr;
            document.getElementById("appAlert").style.visibility = "visible";
            return false;
        }
        else {
            var appCount = 0;
            for (var j = 0; j < appSelector.options.length; j++) {
                if (appSelector.options[j].selected) {
                    appId[appCount] = appSelector.options[j].value.split("+")[0];
                    appName[appCount] = appSelector.options[j].value.split("+")[1];
                    appCount++;
                }
            }
            if (appCount > maxApp) {
                document.getElementById("appAlert").innerText = maxAppErr;
                document.getElementById("appAlert").style.visibility = "visible";
                return false;
            }

            document.getElementById('soltura_daily_rp_app_name').value = appName.toString();
            document.getElementById('soltura_daily_rp_app_id').value = appId.toString();
        }

        return true;
    }
    return false;

}

function dailyDownloadableAppSelection(current_form, type, err1, err2, errFutureDate, err4, err5, errFutureToDate,maxAppErr, noAppErr, maxApp, spNotSelected)
{
    var appId = new Array();
    var appName = new Array();
    var chargingTypeSelector = document.getElementById("chargingTypes");
    var fromDateString = current_form["fromDate"].value;
    var toDateString = current_form["toDate"].value;
    var spSelector = current_form["providers"];
    var appSelector = current_form["apps"];

    if (dateValidator(fromDateString, err1, err2, errFutureDate) && dateValidator(toDateString, err1, err2, errFutureToDate)) {
        var fromDateArray = fromDateString.split("-");
        var toDateArray = toDateString.split("-");
        var fromDate = new Date();
        var toDate = new Date();
        fromDate.setFullYear(parseInt(fromDateArray[0], 10), parseInt(fromDateArray[1], 10) - 1, parseInt(fromDateArray[2], 10));
        toDate.setFullYear(parseInt(toDateArray[0], 10), parseInt(toDateArray[1], 10) - 1, parseInt(toDateArray[2], 10));
        if (toDate < fromDate) {
            showDateError(err4);
            return false;
        }
        else if (toDate - fromDate > 2592000000) {
            showDateError(err5);
            return false;
        }
        current_form['downloadable_daily_rp_from_date'].value = fromDateString;
        current_form['downloadable_daily_rp_to_date'].value = toDateString;

        if (type == 1) {
            var spSelectedString = spSelector.options[spSelector.selectedIndex].value;
            if (spSelectedString == "") {
                current_form["downloadable_daily_rp_sp_id"].value = spNotSelected;
                current_form["downloadable_daily_rp_sp_name"].value = spNotSelected;
            } else {
                current_form["downloadable_daily_rp_sp_id"].value = spSelectedString.split("+")[0];
                current_form["downloadable_daily_rp_sp_name"].value = spSelectedString.split("+")[1];
            }
        }

        document.getElementById("downloadable_daily_rp_charging_type").value = chargingTypeSelector.options[chargingTypeSelector.selectedIndex].value;
        current_form["downloadable_daily_rp_select_all_apps"].value = current_form["selectAll"].checked;

        if (appSelector.selectedIndex == -1 && (current_form["selectAll"].checked == false)) {
            document.getElementById("appAlert").innerText = noAppErr;
            document.getElementById("appAlert").style.visibility = "visible";
            return false;
        }
        else {
            var appCount = 0;
            for (var j = 0; j < appSelector.options.length; j++) {
                if (appSelector.options[j].selected) {
                    appId[appCount] = appSelector.options[j].value.split("+")[0];
                    appName[appCount] = appSelector.options[j].value.split("+")[1];
                    appCount++;
                }
            }
            if (appCount > maxApp) {
                document.getElementById("appAlert").innerText = maxAppErr;
                document.getElementById("appAlert").style.visibility = "visible";
                return false;
            }

            document.getElementById('downloadable_daily_rp_app_name').value = appName.toString();
            document.getElementById('downloadable_daily_rp_app_id').value = appId.toString();
        }
        return true;
    }
    return false;
}


function messageHistoryAppSelection(current_form, type, err1, err2, err3, err4, err5, maxAppErr, noAppErr, maxApp, err6, spNotSelected) {
    var fromDateString = current_form["fromDate"].value;
    var toDateString = current_form["toDate"].value;

    if (dateValidator(fromDateString, err1, err2, err3) && dateValidator(toDateString, err1, err2, err6)) {
        var fromDateArray = fromDateString.split("-");
        var toDateArray = toDateString.split("-");
        var fromDate = new Date();
        var toDate = new Date();
        fromDate.setFullYear(parseInt(fromDateArray[0], 10), parseInt(fromDateArray[1], 10) - 1, parseInt(fromDateArray[2], 10));
        toDate.setFullYear(parseInt(toDateArray[0], 10), parseInt(toDateArray[1], 10) - 1, parseInt(toDateArray[2], 10));
        if (toDate < fromDate) {
            showDateError(err4);
            return false;
        }
        else if (toDate - fromDate > 2592000000) {
            showDateError(err5);
            return false;
        }
        document.getElementById('RP_message_history_start_date').value = fromDateString;
        document.getElementById('RP_message_history_end_date').value = toDateString;

        if (type == 1) {
            var spSelector = current_form["providers"];
            var spSelectedString = spSelector.options[spSelector.selectedIndex].value;
            if (spSelectedString == "") {
                current_form["RP_message_history_sp_id"].value = spNotSelected;
                current_form["RP_message_history_sp_name"].value = spNotSelected;
            } else {
                current_form["RP_message_history_sp_id"].value = spSelectedString.split("+")[0];
                current_form["RP_message_history_sp_name"].value = spSelectedString.split("+")[1];
            }
        }

        var appId = new Array();
        var appName = new Array();
        var appSelector = current_form["apps"];
        if (appSelector.selectedIndex == -1) {
            document.getElementById("appAlert").innerText = noAppErr;
            document.getElementById("appAlert").style.visibility = "visible";
            return false;
        }
        else {
            var count2 = 0;
            for (var j = 0; j < appSelector.options.length; j++) {
                if (appSelector.options[j].selected) {
                    appId[count2] = appSelector.options[j].value.split("+")[0];
                    appName[count2] = appSelector.options[j].value.split("+")[1];
                    count2++;
                }
            }
            if (count2 > maxApp) {
                document.getElementById("appAlert").innerText = maxAppErr;
                document.getElementById("appAlert").style.visibility = "visible";
                return false;
            }

            document.getElementById('RP_message_history_app_name').value = appName.toString();
            document.getElementById('RP_message_history_app_id').value = appId.toString();
        }

        var order = current_form["orderWay"];

        if (order.selectedIndex != -1) {
            for (var m = 0; m < sort.options.length; m++) {
                if (order.options[m].selected) {
                    document.getElementById('RP_message_history_order').value = order.options[m].value;
                }
            }
        }

        return true;
    }
    return false;

}


function appOperatorAppSelection(current_form, type, errmaxapp, errnoapp, maxApp, spNotSelected) {
    var selectedArray = new Array();
    var appId = new Array();
    var appName = new Array();
    var ncs = new Array();
    var directions = new Array();

    var yearSelector = current_form["years"];
    var appSelector = current_form["apps"];
    var ncsSelector = current_form["ncsTypes"];

    var yearCount = 0;
    var ncsTypeCount = 0;
    var appCount = 0;
    var dirCount = 0;
    var ncsCount = 0;

    if (yearSelector.selectedIndex == -1) {
        document.getElementById("yearAlert").style.visibility = "visible";
        return false;
    } else {
        var i;
        for (i = 0; i < yearSelector.options.length; i++) {
            if (yearSelector.options[i].selected) {
                selectedArray[yearCount] = yearSelector.options[i].value;
                yearCount++;
            }
        }
        document.getElementById('RP_app_op_years').value = selectedArray.toString();
    }

    if (type == 1) {
        var spSelector = current_form["providers"];
        var spSelectedString = spSelector.options[spSelector.selectedIndex].value;
        if (spSelectedString == "") {
            current_form["RP_app_op_sp_id"].value = spNotSelected;
            current_form["RP_app_op_sp_name"].value = spNotSelected;
        } else {
            current_form["RP_app_op_sp_id"].value = spSelectedString.split("+")[0];
            current_form["RP_app_op_sp_name"].value = spSelectedString.split("+")[1];
        }
    }

    document.getElementById("RP_app_op_select_all_apps").value = current_form["selectAll"].checked;

    if (appSelector.selectedIndex == -1 && (current_form["selectAll"].checked == false)) {
        document.getElementById("appAlert").innerText = errnoapp;
        document.getElementById("appAlert").style.visibility = "visible";
        return false;
    } else {
        for (var j = 0; j < appSelector.options.length; j++) {
            if (appSelector.options[j].selected) {
                appId[appCount] = appSelector.options[j].value.split("+")[0];
                appName[appCount] = appSelector.options[j].value.split("+")[1];
                appCount++;
            }
        }
        if (appCount > maxApp) {
            document.getElementById("appAlert").innerText = errmaxapp;
            document.getElementById("appAlert").style.visibility = "visible";
            return false;
        }
        document.getElementById('RP_app_op_app_name').value = appName.toString();
        document.getElementById('RP_app_op_app_id').value = appId.toString();

        for (var k = 0; k < ncsSelector.options.length; k++) {
            if (ncsSelector.options[k].selected) {
                if (ncsSelector.options[k].value == 'ussd') {
                    ncs[ncsCount] = ncsSelector.options[k].value;
                    ncsCount++;
                } else {
                    if (ncs[ncsCount - 1] != 'sms') {
                        ncs[ncsCount] = ncsSelector.options[k].value.split("+")[0];
                        ncsCount++;
                    }
                    directions[dirCount] = ncsSelector.options[k].value.split("+")[1];
                    dirCount++;
                }
                ncsTypeCount++;
            }
        }
        document.getElementById('RP_app_op_ncs').value = ncs.toString();
        document.getElementById('RP_app_op_dir').value = directions.toString();
        return true;
    }
}

function appOperatorMonthlyAppSelection(current_form, type, errmaxapp, errnoapp, maxApp, spNotSelected) {
    var spSelector = current_form["providers"];
    var appSelector = current_form["apps"];
    var ncsSelector = current_form["ncsTypes"];

    var appId = new Array();
    var appName = new Array();
    var selectedArray = new Array();
    var ncs = new Array();
    var directions = new Array();

    var monthCount = 0;
    var ncsTypeCount = 0;
    var appCount = 0;
    var dirCount = 0;
    var ncsCount = 0;

    var yearsSelector = current_form["years"];
    current_form["RP_app_op_monthly_year"].value = yearsSelector.options[yearsSelector.selectedIndex].value;

    var monthSelector = current_form["months"];
    if (monthSelector.selectedIndex == -1) {
        document.getElementById("monthAlert").style.visibility = "visible";
        return false;
    } else {
        var i;
        for (i = 0; i < monthSelector.options.length; i++) {
            if (monthSelector.options[i].selected) {
                selectedArray[monthCount] = monthSelector.options[i].value;
                monthCount++;
            }
        }
        document.getElementById('RP_app_op_monthly_months').value = selectedArray.toString();
    }

    if (type == 1) {
        var spSelectedString = spSelector.options[spSelector.selectedIndex].value;
        if (spSelectedString == "") {
            current_form["RP_app_op_monthly_sp_id"].value = spNotSelected;
            current_form["RP_app_op_monthly_sp_name"].value = spNotSelected;
        } else {
            current_form["RP_app_op_monthly_sp_id"].value = spSelectedString.split("+")[0];
            current_form["RP_app_op_monthly_sp_name"].value = spSelectedString.split("+")[1];
        }
    }

    document.getElementById("RP_app_op_monthly_select_all_apps").value = current_form["selectAll"].checked;

    if (appSelector.selectedIndex == -1 && (current_form["selectAll"].checked == false)) {
        document.getElementById("appAlert").innerText = errnoapp;
        document.getElementById("appAlert").style.visibility = "visible";
        return false;
    } else {

        for (var j = 0; j < appSelector.options.length; j++) {
            if (appSelector.options[j].selected) {
                appId[appCount] = appSelector.options[j].value.split("+")[0];
                appName[appCount] = appSelector.options[j].value.split("+")[1];
                appCount++;
            }
        }
        if (appCount > maxApp) {
            document.getElementById("appAlert").innerText = errmaxapp;
            document.getElementById("appAlert").style.visibility = "visible";
            return false;
        }

        document.getElementById('RP_app_op_monthly_app_name').value = appName.toString();
        document.getElementById('RP_app_op_monthly_app_id').value = appId.toString();

        for (var k = 0; k < ncsSelector.options.length; k++) {
            if (ncsSelector.options[k].selected) {
                if (ncsSelector.options[k].value == 'ussd') {
                    ncs[ncsCount] = ncsSelector.options[k].value;
                    ncsCount++;
                } else {
                    if (ncs[ncsCount - 1] != 'sms') {
                        ncs[ncsCount] = ncsSelector.options[k].value.split("+")[0];
                        ncsCount++;
                    }
                    directions[dirCount] = ncsSelector.options[k].value.split("+")[1];
                    dirCount++;
                }
                ncsTypeCount++;
            }
        }
        document.getElementById('RP_app_op_monthly_ncs').value = ncs.toString();
        document.getElementById('RP_app_op_monthly_dir').value = directions.toString();
        return true;
    }
}

function appOperatorDailyAppSelection(current_form, type, err1, err2, err3, err4, err5, maxAppErr, noAppErr, maxApp, err6, spNotSelected) {
    var fromDateString = current_form["fromDate"].value;
    var toDateString = current_form["toDate"].value
    var spSelector = current_form["providers"];
    var appSelector = current_form["apps"];
    var ncsSelector = current_form["ncsTypes"];

    var appId = new Array();
    var appName = new Array();
    var ncs = new Array();
    var directions = new Array();

    var dirCount = 0;
    var ncsCount = 0;
    var ncsTypeCount = 0;

    if (dateValidator(fromDateString, err1, err2, err3) && dateValidator(toDateString, err1, err2, err6)) {
        var fromDateArray = fromDateString.split("-");
        var toDateArray = toDateString.split("-");
        var fromDate = new Date();
        var toDate = new Date();
        fromDate.setFullYear(parseInt(fromDateArray[0], 10), parseInt(fromDateArray[1], 10) - 1, parseInt(fromDateArray[2], 10));
        toDate.setFullYear(parseInt(toDateArray[0], 10), parseInt(toDateArray[1], 10) - 1, parseInt(toDateArray[2], 10));
        if (toDate < fromDate) {
            showDateError(err4);
            return false;
        }
        else if (toDate - fromDate > 2592000000) {
            showDateError(err5);
            return false;
        }
        document.getElementById('RP_app_op_daily_start_date').value = fromDateString;
        document.getElementById('RP_app_op_daily_end_date').value = toDateString;

        if (type == 1) {
            var spSelectedString = spSelector.options[spSelector.selectedIndex].value;
            if (spSelectedString == "") {
                current_form["RP_app_op_daily_sp_id"].value = spNotSelected;
                current_form["RP_app_op_daily_sp_name"].value = spNotSelected;
            } else {
                current_form["RP_app_op_daily_sp_id"].value = spSelectedString.split("+")[0];
                current_form["RP_app_op_daily_sp_name"].value = spSelectedString.split("+")[1];
            }
        }

        document.getElementById("RP_app_op_daily_select_all_apps").value = current_form["selectAll"].checked;

        if (appSelector.selectedIndex == -1 && (current_form["selectAll"].checked == false)) {
            document.getElementById("appAlert").innerText = noAppErr;
            document.getElementById("appAlert").style.visibility = "visible";
            return false;
        } else {
            var appCount = 0;
            for (var j = 0; j < appSelector.options.length; j++) {
                if (appSelector.options[j].selected) {
                    appId[appCount] = appSelector.options[j].value.split("+")[0];
                    appName[appCount] = appSelector.options[j].value.split("+")[1];
                    appCount++;
                }
            }
            if (appCount > maxApp) {
                document.getElementById("appAlert").innerText = maxAppErr;
                document.getElementById("appAlert").style.visibility = "visible";
                return false;
            }

            document.getElementById('RP_app_op_daily_app_name').value = appName.toString();
            document.getElementById('RP_app_op_daily_app_id').value = appId.toString();


            for (var k = 0; k < ncsSelector.options.length; k++) {
                if (ncsSelector.options[k].selected) {
                    if (ncsSelector.options[k].value == 'ussd') {
                        ncs[ncsCount] = ncsSelector.options[k].value;
                        ncsCount++;
                    } else {
                        if (ncs[ncsCount - 1] != 'sms') {
                            ncs[ncsCount] = ncsSelector.options[k].value.split("+")[0];
                            ncsCount++;
                        }
                        directions[dirCount] = ncsSelector.options[k].value.split("+")[1];
                        dirCount++;
                    }
                    ncsTypeCount++;
                }
            }
            document.getElementById('RP_app_op_daily_ncs').value = ncs.toString();
            document.getElementById('RP_app_op_daily_dir').value = directions.toString();

            return true;
        }
    }
    return false;
}