/*var head= document.getElementsByTagName('head')[0];
var script= document.createElement('script');
script.type= 'text/javascript';
script.src= 'reportingKeyBox.js';
head.appendChild(script);*/

var RpYear                     =  "RP_Year";
var RpServiceProviderId        =  "RP_Sp_Id";
var RpShowGraphs               =  "RP_Show_Graphs";
var RpMonth                    =  "RP_Month";
var RpFromDate                 =  "RP_From_Date";
var RpToDate                   =  "RP_To_Date";
var RpApplicationIds           =  "RP_Application_Ids";
var RpPaymentInstrument        =  "RP_Payment_Instrument";
var RpPaybillNo                =  "RP_Paybill_No";
var RpTillNo                   =  "RP_Till_No";
var RpPaymentInstrumentId      =  "RP_Payment_Instrument_Id";
var RpReferenceId              =  "RP_Reference_Id";
var RpEventType                =  "RP_Event_Type";
var RpStatus                   =  "RP_Status";
var RpTransactionId            =  "RP_Trx_Id";

function detailTransactionValidationCustomCare(current_form, err1, err2, err3, err4, err5, val1, SpID, userType, pi, err6) {

    var fromDateString = current_form["fromDate"].value;
    var toDateString = current_form["toDate"].value;
    if(dateValidator(fromDateString,err1,err2,err3) && dateValidator(toDateString,err1,err2,err6)){
        var fromDateArray = fromDateString.split("-");
        var toDateArray = toDateString.split("-");
        var fromDate = new Date();
        var toDate = new Date();
        var maxNumDays = daysToMilliSecs(val1);
        fromDate.setFullYear(parseInt(fromDateArray[0], 10), parseInt(fromDateArray[1], 10) - 1, parseInt(fromDateArray[2], 10));
        toDate.setFullYear(parseInt(toDateArray[0], 10), parseInt(toDateArray[1], 10) - 1, parseInt(toDateArray[2], 10));
        if (toDate < fromDate) {
            showDateError(err4);
            return false;
        } else if (toDate - fromDate > maxNumDays) {
            showDateError(err5);
            return false;
        }
        document.getElementById(RpFromDate).value = fromDateArray;
        document.getElementById(RpToDate).value = toDateArray;


        current_form[RpReferenceId].value = current_form["referenceId"].value;

        var eventTypes = document.getElementById("eventType");
        document.getElementById(RpEventType).value = eventTypes.options[eventTypes.selectedIndex].value;

        var status = document.getElementById("tranactionStatus");
        document.getElementById(RpStatus).value = status.options[status.selectedIndex].value;
        setPaymentINsturmentsCC(pi);
        setPaybill();
        setBuyGoodTillno();
        if (userType == 'CORPORATE') {
            current_form[RpServiceProviderId].value = SpID;
        }
        else {
            current_form[RpServiceProviderId].value = '';
        }
        return true;
    }
    return false;
}

function detailTransactionValidation(current_form, err1, err2, err3, err4, err5, val1, SpID, userType) {
    var fromDateString = current_form["fromDate"].value;
    var toDateString = current_form["toDate"].valu
    if(dateValidator(fromDateString,err1,err2,err3) && dateValidator(toDateString,err1,err2,err3)){
        var fromDateArray = fromDateString.split("-");
        var toDateArray = toDateString.split("-");
        var fromDate = new Date();
        var toDate = new Date();
        var maxNumDays = daysToMilliSecs(val1);
        fromDate.setFullYear(parseInt(fromDateArray[0], 10), parseInt(fromDateArray[1], 10) - 1, parseInt(fromDateArray[2], 10));
        toDate.setFullYear(parseInt(toDateArray[0], 10), parseInt(toDateArray[1], 10) - 1, parseInt(toDateArray[2], 10));
        if (toDate < fromDate) {
            showDateError(err4);
            return false;
        } else if (toDate - fromDate > maxNumDays) {
            showDateError(err5);
            return false;
        }
        document.getElementById(RpFromDate).value = fromDateArray;
        document.getElementById(RpToDate).value = toDateArray;


        current_form[RpReferenceId].value = current_form["referenceId"].value;

        var eventTypes = document.getElementById("eventType");
        document.getElementById(RpEventType).value = eventTypes.options[eventTypes.selectedIndex].value;

        var status = document.getElementById("tranactionStatus");
        document.getElementById(RpStatus).value = status.options[status.selectedIndex].value;
        setPaymentINsturments();
        setPaybill();
        if (userType == 'CORPORATE') {
            current_form[RpServiceProviderId].value = SpID;
        }
        else {
            current_form[RpServiceProviderId].value = '';
        }
        return true;
    }
        return false;
}

function setDates(fromDateString, toDateString, err4, err5) {
    var fromDateArray = fromDateString.split("-");
    var toDateArray = toDateString.split("-");
    var fromDate = new Date();
    var toDate = new Date();
    var numOfDays = daysToMilliSecs(val1);

    fromDate.setFullYear(parseInt(fromDateArray[0], 10), parseInt(fromDateArray[1], 10) - 1, parseInt(fromDateArray[2], 10));
    toDate.setFullYear(parseInt(toDateArray[0], 10), parseInt(toDateArray[1], 10) - 1, parseInt(toDateArray[2], 10));
    if (toDate < fromDate) {
        showDateError(err4);
        return false;
    } else if (toDate-fromDate > numOfDays) {
        showDateError(err5);
        return false;
    } else {
    document.getElementById(RpFromDate).value = fromDateArray;
    document.getElementById(RpToDate).value = toDateArray;
    alert( toDateArray );
    }
}

function appBasedDetailRevenueValidation(current_form, err1, err2, err3, err4, err5, val1,errmaxapp,errnoapp,maxApp) {
    var appSelector = current_form["paymentApplications"];
    var fromDateString = current_form["fromDate"].value;
    var toDateString = current_form["toDate"].value;
    if(dateValidator(fromDateString,err1,err2,err3) && dateValidator(toDateString,err1,err2,err3)){
        var fromDateArray = fromDateString.split("-");
        var toDateArray = toDateString.split("-");
        var fromDate = new Date();
        var toDate = new Date();

        var numOfDays = daysToMilliSecs(val1);

        fromDate.setFullYear(parseInt(fromDateArray[0], 10), parseInt(fromDateArray[1], 10) - 1, parseInt(fromDateArray[2], 10));
        toDate.setFullYear(parseInt(toDateArray[0], 10), parseInt(toDateArray[1], 10) - 1, parseInt(toDateArray[2], 10));
        if(toDate < fromDate){
            showDateError(err4);
            return false;
        }else if(toDate-fromDate > numOfDays){
            showDateError(err5);
            return false;
        }
        current_form[RpFromDate].value = current_form["fromDate"].value;
        current_form[RpToDate].value = current_form["toDate"].value;
    }else{
        return false;
    }

    if  (appSelector.selectedIndex == -1) {
        document.getElementById("appAlert").style.visibility = "visible";
        return false;
    }
    else {
            var count2 = 0;
            var appId = new Array();

            for (var j = 0; j < appSelector.options.length; j++) {
                if (appSelector.options[j].selected) {
                    appId[count2] = appSelector.options[j].value.split("+")[0];
                    count2++;
                }
            }
            if (count2 > maxApp) {
                document.getElementById("appAlert").innerText = errmaxapp;
                document.getElementById("appAlert").style.visibility = "visible";
                return false;
            }
            document.getElementById(RpApplicationIds).value = appId.toString();

            if ( current_form[RpApplicationIds].value == '' ) {
                document.getElementById("appAlertNull").style.visibility = "visible";
                return false;

            } else {
                return true;
            }
        }

    }

function appBasedRevenueValidation(current_form, err1, err2, err3, err4, err5, errmaxapp, errnoapp, maxApp) {
    var appSelector = current_form["paymentApplications"];

    var fromDateString = current_form["fromDate"].value;
    var toDateString = current_form["toDate"].value;
    if(dateValidator(fromDateString,err1,err2,err3) && dateValidator(toDateString,err1,err2,err3)){
        var fromDateArray = fromDateString.split("-");
        var toDateArray = toDateString.split("-");
        var fromDate = new Date();
        var toDate = new Date();
        fromDate.setFullYear(parseInt(fromDateArray[0], 10), parseInt(fromDateArray[1], 10) - 1, parseInt(fromDateArray[2], 10));
        toDate.setFullYear(parseInt(toDateArray[0], 10), parseInt(toDateArray[1], 10) - 1, parseInt(toDateArray[2], 10));
        if(toDate < fromDate){
            showDateError(err4);
            return false;
        }else if(toDate-fromDate > 2592000000){
            showDateError(err5);
            return false;
        }
        current_form[RpFromDate].value = current_form["fromDate"].value;
        current_form[RpToDate].value = current_form["toDate"].value;
    }else{
        return false;
    }

    if(appSelector.selectedIndex == -1) {
        document.getElementById("appAlert").innerText = errnoapp;
        document.getElementById("appAlert").style.visibility="visible";
        return false;
    }
    else {
        var count2 = 0;
        var appId = new Array();

        for (var j = 0; j < appSelector.options.length; j++) {
            if (appSelector.options[j].selected) {
                appId[count2] = appSelector.options[j].value.split("+")[0];
                count2++;
            }
        }
        if (count2 > maxApp) {
            document.getElementById("appAlert").innerText = errmaxapp;
            document.getElementById("appAlert").style.visibility = "visible";
            return false;
        }
        document.getElementById(RpApplicationIds).value = appId.toString();

        if ( current_form[RpApplicationIds].value == '' ) {
            document.getElementById("appAlertNull").style.visibility = "visible";
            return false;

        } else {
            return true;
        }
    }
}



function pgwAppBaseMonthlyIncomeValidation(current_form, errmaxapp,errnoapp,maxApp) {
    var yearsSelector = current_form["years"];
    var year = yearsSelector.options[yearsSelector.selectedIndex].value;
    var appSelector = current_form["paymentApplications"];

    var monthsSelector = current_form["months"];
    if (monthsSelector.selectedIndex == -1) {
        document.getElementById("monthAlert").style.visibility = "visible";
        return false;
    } else {
        var selectedArray1 = new Array();
        var count = 0;
        for (var i = 0; i < monthsSelector.options.length; i++) {
            if (monthsSelector.options[i].selected) {
                selectedArray1[count] = monthsSelector.options[i].value;
                count++;
            }
        }

    current_form[RpYear].value = year;
    current_form[RpMonth].value = selectedArray1.toString();


    }
    if(appSelector.selectedIndex == -1) {
        document.getElementById("appAlert").innerText = errnoapp;
        document.getElementById("appAlert").style.visibility="visible";
        return false;
    } else {
        var count2 = 0;
        var appId = new Array();

        for (var j = 0; j < appSelector.options.length; j++) {
            if (appSelector.options[j].selected) {
                appId[count2] = appSelector.options[j].value.split("+")[0];
                count2++;
            }
        }
        if (count2 > maxApp) {
            document.getElementById("appAlert").innerText = errmaxapp;
            document.getElementById("appAlert").style.visibility = "visible";
            return false;
        }
        document.getElementById('RP_Application_Ids').value = appId.toString();

        current_form[RpShowGraphs].value = selectedArray2.toString();
        document.getElementById(RpShowGraphs).value = current_form["chkShowChart"].checked;
    }

    if ( current_form[RpApplicationIds].value == '' ) {
        document.getElementById("appAlertNull").style.visibility = "visible";
        return false;

    } else {
        return true;

    }

}

function paymentInstrumentBasedTransactionValidation(current_form, err1, err2, err3, err4, err5, spID, userType, err6 ) {
    var fromDateString = current_form["fromDate"].value;
    var toDateString = current_form["toDate"].value;
    if(dateValidator(fromDateString,err1,err2,err3) && dateValidator(toDateString,err1,err2,err6)){
        var fromDateArray = fromDateString.split("-");
        var toDateArray = toDateString.split("-");
        var fromDate = new Date();
        var toDate = new Date();
        fromDate.setFullYear(parseInt(fromDateArray[0], 10), parseInt(fromDateArray[1], 10) - 1, parseInt(fromDateArray[2], 10));
        toDate.setFullYear(parseInt(toDateArray[0], 10), parseInt(toDateArray[1], 10) - 1, parseInt(toDateArray[2], 10));
        if(toDate < fromDate){
            showDateError(err4);
            return false;
        }else if(toDate-fromDate > 2592000000){
            showDateError(err5);
            return false;
        }
        current_form[RpFromDate].value = current_form["fromDate"].value;
        current_form[RpToDate].value = current_form["toDate"].value;
        document.getElementById(RpShowGraphs).value = current_form["chkShowChart"].checked;
        if (userType == "CORPORATE") {
            current_form[RpServiceProviderId].value = spID;
        }
        else {
            current_form[RpServiceProviderId].value = '';
        }

        return true;
    }else{
        return false;
    }


}


function subscriptionRepaymentDaily(current_form, err1, err2, err3, err4, err5, val1) {
    var fromDateString = current_form["fromDate"].value;
    var toDateString = current_form["toDate"].value;
    var fromDateArray = fromDateString.split("-");
    var toDateArray = toDateString.split("-");
    var fromDate = new Date();
    var toDate = new Date();
    var maxNumDays = daysToMilliSecs(val1);
    fromDate.setFullYear(parseInt(fromDateArray[0], 10), parseInt(fromDateArray[1], 10) - 1, parseInt(fromDateArray[2], 10));
    toDate.setFullYear(parseInt(toDateArray[0], 10), parseInt(toDateArray[1], 10) - 1, parseInt(toDateArray[2], 10));
    if (toDate < fromDate) {
        showDateError(err4);
        return false;
    } else if (toDate - fromDate > maxNumDays) {
        showDateError(err5);
        return false;
    }

    document.getElementById(RpFromDate).value = fromDateArray;
    document.getElementById(RpToDate).value = toDateArray;

    var paymentInstrumentSelector = current_form["paymentInstruments"];


    if (paymentInstrumentSelector.selectedIndex == 0) {

        document.getElementById("instrumentAlert").style.visibility = "visible";
        return false;
    } else {
        current_form[RpPaymentInstrumentId].value= paymentInstrumentSelector.options[paymentInstrumentSelector.selectedIndex].value;
        current_form[RpPaymentInstrument].value= paymentInstrumentSelector.options[paymentInstrumentSelector.selectedIndex].text;
        return true;
    }

}

function subscriptionRepaymentDetail(current_form, err1, err2, err3, err4, err5,val1) {
    var fromDateString = current_form["fromDate"].value;
    var toDateString = current_form["toDate"].value;
    var fromDateArray = fromDateString.split("-");
    var toDateArray = toDateString.split("-");
    var fromDate = new Date();
    var toDate = new Date();
    var maxNumDays = daysToMilliSecs(val1);
    fromDate.setFullYear(parseInt(fromDateArray[0], 10), parseInt(fromDateArray[1], 10) - 1, parseInt(fromDateArray[2], 10));
    toDate.setFullYear(parseInt(toDateArray[0], 10), parseInt(toDateArray[1], 10) - 1, parseInt(toDateArray[2], 10));
    if (toDate < fromDate) {
        showDateError(err4);
        return false;
    } else if (toDate - fromDate > maxNumDays) {
        showDateError(err5);
        return false;
    }

    document.getElementById(RpFromDate).value = fromDateArray;
    document.getElementById(RpToDate).value = toDateArray;

    var paymentInstrumentSelector = current_form["paymentInstruments"];


    if (paymentInstrumentSelector.selectedIndex == 0) {

        document.getElementById("instrumentAlert").style.visibility = "visible";
        return false;
    } else {
        current_form[RpPaymentInstrumentId].value= paymentInstrumentSelector.options[paymentInstrumentSelector.selectedIndex].value;
        current_form[RpPaymentInstrument].value= paymentInstrumentSelector.options[paymentInstrumentSelector.selectedIndex].text;
        return true;
    }

}



function setPaymentINsturments() {
    var selectedArray = new Array();
    var selObj = document.getElementById("paymentInstrument");
    var i;
    var count = 0;
    if (0 < selObj.options.length) {
        for (i = 0; i < selObj.options.length; i++) {
            if (selObj.options[i].selected) {
                selectedArray[count] = selObj.options[i].value;
                count++;
            }
        }
        document.getElementById(RpPaymentInstrument).value = selectedArray.toString();

    } else {
        document.getElementById(RpPaymentInstrument).value = "";
    }
}


function setPaymentINsturmentsCC(pai) {
    var str = new Array();
    var selectedpi = new Array();
    var count = 0;

    str = convertToArray(pai);

    for(var i=0;i<str.length;i++) {
        var checkbox = document.getElementById(str[i]);
        if(checkbox.checked){
            selectedpi[count] = checkbox.value.toString();
            count++;
        }
    }

    if(selectedpi.length > 0) {
        document.getElementById(RpPaymentInstrument).value = selectedpi.toString();
    } else {
        document.getElementById(RpPaymentInstrument).value = "";
    }
}

function convertToArray(pi) {
    var str = pi;
    str = str.toString();
    str = str.replace("[","");
    str = str.replace("]","");
    var str1 = new Array();
    str1 = str.split(",");

    for(var i = 0; i < str1.length; i++) {
       str1[i] = str1[i].trim();
    }
    return str1;
}

function setPaybill() {
    var selectedPaybill = new Array();
    var paybilllist = document.getElementById("paybillNo");
    var j;
    var count = 0;
    if(0 < paybilllist.options.length) {
        for(j = 0; j < paybilllist.options.length; j++) {
            if (paybilllist.options[j].selected) {
                selectedPaybill[count] = paybilllist.options[j].value;
                count ++;
            }
        }
        document.getElementById(RpPaybillNo).value = selectedPaybill.toString();
    } else {
        document.getElementById(RpPaybillNo).value = "";
    }
}

function setBuyGoodTillno() {
    var selectedTillNo = new Array();
    var tillNoList = document.getElementById("tillNos");
    var k;
    var selectCount = 0;
    if(0 < tillNoList.options.length) {
        for(k = 0; k< tillNoList.options.length; k++) {
            if (tillNoList.options[k].selected) {
                selectedTillNo[selectCount] = tillNoList.options[k].value;
                selectCount ++;
            }
        }
        document.getElementById(RpTillNo).value = selectedTillNo.toString();
    } else {
        document.getElementById(RpTillNo).value = "";
    }
}

function dateValidator(stringDate, err1, err2, err3) {
    if (stringDate == "") {
        showDateError(err1);
        return false;
    }
    var leapYearDays = new Array(31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
    var normalYearDays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

    var dateRegEx = "\\d{4}-\\d{2}-\\d{2}";

    if (stringDate.match(dateRegEx)) {
        var dateArray = stringDate.split("-");
        var year = parseInt(dateArray[0], 10);
        var month = parseInt(dateArray[1], 10);
        var day = parseInt(dateArray[2], 10);
        if ((year > 0) && (month > 0) && (month <= 12) && (day > 0)) {
            if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
                if (day <= leapYearDays[month - 1]) {
                    if (!pastDatesOnly(stringDate)) {
                        showDateError(err3);
                        return false;
                    }
                } else {
                    showDateError(err2);
                    return false;
                }
            } else {
                if (day <= normalYearDays[month - 1]) {
                    if (!pastDatesOnly(stringDate)) {
                        showDateError(err3);
                        return false;
                    }
                } else {
                    showDateError(err2);
                    return false;
                }
            }
        } else {
            showDateError(err2);
            return false;
        }
    } else {
        showDateError(err2);
        return false;
    }
    return true;

}

function showDateError(errID) {
    document.getElementById("dateAlert").innerText = errID;
    document.getElementById("dateAlert").style.visibility = "visible";
    return false;
}

function yearlyEarning(current_form,errmaxapp,errnoapp,maxApp) {

    var appSelector = current_form["paymentApplications"];
    var yearSelector = current_form["years"];

    var selectedArray2 = new Array();

    var count2 = 0;

    if(appSelector.selectedIndex == -1) {
        document.getElementById("appAlert").innerText = errnoapp;
        document.getElementById("appAlert").style.visibility="visible";
        return false;
    } else {
        var count2 = 0;
        var appId = new Array();

        for (var j = 0; j < appSelector.options.length; j++) {
            if (appSelector.options[j].selected) {
                appId[count2] = appSelector.options[j].value.split("+")[0];
                count2++;
            }
        }
        if (count2 > maxApp) {
            document.getElementById("appAlert").innerText = errmaxapp;
            document.getElementById("appAlert").style.visibility = "visible";
            return false;
        }
        document.getElementById(RpApplicationIds).value = appId.toString();


        for (var i = 0; i < yearSelector.options.length; i++) {
            if (yearSelector.options[i].selected) {
                selectedArray2[count2] = yearSelector.options[i].value;
                count2++;
            }
        }
        current_form[RpYear].value = selectedArray2.toString();
        document.getElementById(RpShowGraphs).value = current_form["chkShowChart"].checked;


        if ( current_form[RpApplicationIds].value == '' ) {
            document.getElementById("appAlertNull").style.visibility = "visible";
            return false;

        } else {
                return true;
        }
    }

}

function piMonthlyTransactions(current_form, spID, userType ) {

    var yearsSelector = current_form["years"];
    var year = yearsSelector.options[yearsSelector.selectedIndex].value;

    var monthsSelector = current_form["months"];
    if (monthsSelector.selectedIndex == -1) {
        document.getElementById("monthAlert").style.visibility = "visible";
        return false;
    }
    else {
        var selectedArray = new Array();
        var count = 0;
        for (var i = 0; i < monthsSelector.options.length; i++) {
            if (monthsSelector.options[i].selected) {
                selectedArray[count] = monthsSelector.options[i].value;
                count++;
            }
        }
        if (userType == "CORPORATE") {
            current_form[RpServiceProviderId].value = spID;
        }
        else {
            current_form[RpServiceProviderId].value = '';
        }
        current_form[RpYear].value = year;
        current_form[RpMonth].value = selectedArray.toString();
        document.getElementById(RpShowGraphs).value = current_form["chkShowChart"].checked;

        return true;
    }
}

function piYearlyTransactions(current_form, userType , spID) {

    var yearsSelector = current_form["years"];

    var selectedArray = new Array();
    var count = 0;
    if(yearsSelector.selectedIndex == -1) {
        document.getElementById("yearAlert").style.visibility = "visible";
        return false;
    } else {
        for (var i = 0; i < yearsSelector.options.length; i++) {
            if (yearsSelector.options[i].selected) {
                selectedArray[count] = yearsSelector.options[i].value;
                count++;
            }
        }
    }

    if (userType == "CORPORATE") {
        current_form[RpServiceProviderId].value = spID;
    }
    else {
        current_form[RpServiceProviderId].value = '';
    }

    current_form[RpYear].value = selectedArray.toString();
    document.getElementById(RpShowGraphs).value = current_form["chkShowChart"].checked;
    return true;

}


function subscriberRepaymentMonthly(current_form) {
    var yearsSelector = current_form["years"];
    var year = yearsSelector.options[yearsSelector.selectedIndex].value;
    current_form[RpYear].value = year;

    var selectedArray = new Array();
    var monthsSelector = current_form["months"];

    var count = 0;
    for (var i = 0; i < monthsSelector.options.length; i++) {
        if (monthsSelector.options[i].selected) {
            selectedArray[count] = monthsSelector.options[i].value;
            count++;
        }
    }
    current_form[RpMonth].value = selectedArray.toString();

    var paymentInstrumentSelector = current_form["paymentInstruments"];
    if (paymentInstrumentSelector.selectedIndex == 0 || paymentInstrumentSelector.selectedIndex == -1) {
        document.getElementById("instrumentAlert").style.visibility = "visible";
        return false;

    } else {
        current_form[RpPaymentInstrumentId].value= paymentInstrumentSelector.options[paymentInstrumentSelector.selectedIndex].value;
        current_form[RpPaymentInstrument].value= paymentInstrumentSelector.options[paymentInstrumentSelector.selectedIndex].text;
        document.getElementById(RpShowGraphs).value = current_form["chkShowChart"].checked;
        return true;
    }




}


function pgwSubscriberRepaymentYearlyValidation(current_form) {


    var paymentInstrumentSelector = current_form["paymentInstruments"];


    if (paymentInstrumentSelector.selectedIndex == 0) {

        document.getElementById("instrumentAlert").style.visibility = "visible";
        return false;
    } else {
        current_form[RpPaymentInstrumentId].value= paymentInstrumentSelector.options[paymentInstrumentSelector.selectedIndex].value;
        current_form[RpPaymentInstrument].value= paymentInstrumentSelector.options[paymentInstrumentSelector.selectedIndex].text;
        document.getElementById(RpShowGraphs).value = current_form["chkShowChart"].checked;
        return true;
    }

}

function daysToMilliSecs (numOfDays) {
    var milliSecsPerDay = 86400000;
    var numOfDays;
    var numOfMilliSecs = milliSecsPerDay * numOfDays;
    return numOfMilliSecs;
}








