/*
 * (C) Copyright 2012-2014 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.rpt.sdp;

import hms.kite.repository.sdp.ServiceProviderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */


@Controller
public class SDPMonthlyDownloadableController {

    private final static Logger LOGGER = LoggerFactory.getLogger(SDPMonthlyDownloadableController.class);

    @Autowired
    ServiceProviderRepository serviceProviderRepository;

    @Resource(name = "sdpChargingTypes")
    private Map<String, String> sdpChargingTypes;

    @RequestMapping(value = "/sdpMonthlyDownloadable.jsp", method = RequestMethod.GET)
    public String redirect(Model model, HttpServletRequest request) {
        String userName = (String) request.getSession().getAttribute("userName");
        LOGGER.info("User [{}] has requested SDP Monthly Downloadable Report", userName);
        try {
            setAttributes(model, request);
        } catch (Exception e) {
            LOGGER.error("Unexpected exception thrown while generating Monthly Downloadable Report", e);
            return "errorPage";
        }
        return "sdp/sdpMonthlyDownloadable";
    }

    private void setAttributes(Model model, HttpServletRequest request) throws Exception {
        int userType = (Integer) request.getSession().getAttribute("uType");
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);


        if (userType == 1) {
            model.addAttribute("spDetails", serviceProviderRepository.getAllServiceProviders());
            model.addAttribute("apps", serviceProviderRepository.getAllDownloadableApps());
        } else if (userType == 2) {
            String spId = (String) request.getSession().getAttribute("spID");
            model.addAttribute("spId", spId);
            model.addAttribute("spName", serviceProviderRepository.getSPNameById(spId));
            model.addAttribute("apps", serviceProviderRepository.getDownloadableAppsBySPId(spId));
        }

        model.addAttribute("chargingTypes", sdpChargingTypes);
        model.addAttribute("type", userType);
        model.addAttribute("previousYear", calendar.get(Calendar.YEAR) - 1);
        model.addAttribute("currentYear", calendar.get(Calendar.YEAR));
    }

}
