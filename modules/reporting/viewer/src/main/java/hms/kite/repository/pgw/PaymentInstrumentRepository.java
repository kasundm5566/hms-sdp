/*
 * (C) Copyright 1996-2012 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.repository.pgw;

import hms.kite.domain.pgw.PaymentInstrument;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class PaymentInstrumentRepository {

    private JdbcTemplate jdbcTemplate;

    public List< PaymentInstrument> getAllPaymentInstruments() throws Exception {
        return jdbcTemplate.query("Select id,name FROM `payment_instruments`", new RowMapper<PaymentInstrument>() {
            @Override
            public PaymentInstrument mapRow(ResultSet resultSet, int i) throws SQLException {
                PaymentInstrument paymentInstrument = new PaymentInstrument();
                paymentInstrument.setId(resultSet.getString("id"));
                paymentInstrument.setName(resultSet.getString("name"));
                return paymentInstrument;
            }
        });


    }

    public List<String> getAllTransactionTypes()throws Exception{
        return jdbcTemplate.queryForList("Select DISTINCT tx_type FROM `pgw_transaction`", String.class);
    }

    public List<String> getPaymentInstrument() throws Exception {
        return jdbcTemplate.queryForList("Select DISTINCT `name` FROM `payment_instruments`", String.class);
    }

    public List<String> getPaybillNo() throws Exception {
        return jdbcTemplate.queryForList("Select DISTINCT `pi_additional_info` FROM `sp_payment_instrument_info` WHERE `payment_instrument`='M-Pesa'", String.class);
    }
    public List<String> getTillNos() throws Exception {
        return jdbcTemplate.queryForList("Select DISTINCT `paybill_no` FROM `pgw_transaction` WHERE `payment_instrument`='M-Pesa-Buygoods'", String.class);
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}
