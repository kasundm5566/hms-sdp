/*
 * (C) Copyright 1996-2012 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.rpt.pgw;

import hms.common.registration.api.common.UserType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

import static hms.kite.rpt.util.ReportingKeyBox.errorPageJsp;
import static hms.kite.rpt.util.ReportingKeyBox.pgwPIDailyTransactionsJsp;


@Controller
public class PGWPaymentInstrumentDailyController {

    private static final Logger logger = LoggerFactory.getLogger(PGWPaymentInstrumentDailyController.class);

    @RequestMapping(value = "/pgwPaymentInstrumentBasedDaily.jsp", method = RequestMethod.GET)
	public String redirect(Model model, HttpServletRequest request) {
        final String userName = (String)request.getSession().getAttribute("userName");
        logger.info("User [{}] has requested Payment Instruments Daily Transactions Report", userName);

        try {
            setAttributesOfPIDailyReport(model, request);
            return pgwPIDailyTransactionsJsp;
        } catch (Exception e) {
            logger.error( "Error [{}] occurred while trying to set attributes to the model of Payment Instruments Daily Transactions Report", e);
            return errorPageJsp ;

        }
	}

    private void setAttributesOfPIDailyReport(Model model, HttpServletRequest request) {
        UserType userType = (UserType) request.getSession().getAttribute("userType");

        if (userType == UserType.CORPORATE) {
            final String spID = (String)request.getSession().getAttribute("spID");
            model.addAttribute("SpID", spID );
            logger.info("Payment Instruments Daily Transactions Report requested SP : [{}] ", spID);

        }
        model.addAttribute("userType", userType);

    }

}
