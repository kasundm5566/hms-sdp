/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.rpt.pgw;

import hms.common.registration.api.common.UserType;
import hms.kite.repository.pgw.PaymentInstrumentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

import static hms.kite.rpt.util.ReportingKeyBox.errorPageJsp;
import static hms.kite.rpt.util.ReportingKeyBox.pgwDetailedTransactionJsp;

@Controller
public class PGWDetailedTransactionController {
    private static final Logger logger = LoggerFactory.getLogger(PGWDetailedTransactionController.class);
    private @Value("${max.date.range.allowed}") int maxDateRangeAllowed;
    @Autowired
    PaymentInstrumentRepository paymentInstrumentRepository;

    @RequestMapping(value = "/pgwDetailedTransaction.jsp", method = RequestMethod.GET)
    public String redirect(Model model, HttpServletRequest request) {
        final String userName = (String)request.getSession().getAttribute("userName");
        logger.info("User [{}] has requested PGW Detailed Transactions Report", userName);
        try{
            setAttributesOfCustomerCareDetailReport(model, request);
            return pgwDetailedTransactionJsp;
        }catch (Exception e){
            logger.error( "Error occurred while trying to set attributes to the model of PGW Detailed Transactions Report", e);
            return errorPageJsp;
        }
    }

    private void setAttributesOfCustomerCareDetailReport(Model model, HttpServletRequest request) throws Exception{
        final UserType userType = (UserType) request.getSession().getAttribute("userType");

        if(userType == UserType.CORPORATE) {
            final String SpID = (String)request.getSession().getAttribute("spID");
            model.addAttribute("SpID", SpID );
            logger.info("spID passed from controler : [{}]", SpID);
        }

        model.addAttribute("userType", userType);
        //model.addAttribute("paymentInstruments", paymentInstrumentRepository.getAllPaymentInstruments());
        model.addAttribute("paymentInstruments", paymentInstrumentRepository.getPaymentInstrument());
        //model.addAttribute("paybillNo", paymentInstrumentRepository.getPaybillNo());
        model.addAttribute("transactionTypes", paymentInstrumentRepository.getAllTransactionTypes());
        model.addAttribute("dateRange", maxDateRangeAllowed);
    }
}
