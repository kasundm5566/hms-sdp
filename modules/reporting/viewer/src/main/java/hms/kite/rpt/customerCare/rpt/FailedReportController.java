/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.rpt.customerCare.rpt;

import hms.kite.domain.customerCare.FailedTransaction;
import hms.kite.domain.customerCare.Page;
import hms.kite.repository.customerCare.SDPTransactionRepository;
import hms.kite.repository.sdp.ServiceProviderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

@Controller
public class FailedReportController {

    private static final Logger logger = LoggerFactory.getLogger(FailedReportController.class);

    private static final String COUNT_ROW_QUERY = "select count(*) from sdp_transaction";
    private static final String GET_ROW_QUERY = "select correlation_id, sp_id, sp_name, app_id, app_name, " +
            "response_code, response_description, (case when ncs='vdf-apis' then event_type else ncs end) as ncs, direction, transaction_status, charge_address, source_address, destination_address from sdp_transaction";

    @Autowired
    SDPTransactionRepository sdpTransactionRepository;

    @Autowired
    ServiceProviderRepository serviceProviderRepository;

    /**
     * Selects the home page and populates the model with a message
     *
     * @param fromDate
     * @param toDate
     * @param spId
     * @param appIds
     * @param model
     * @return
     */

    @RequestMapping(value = "/failedReport.jsp", method = RequestMethod.GET)
    public String redirect(@RequestParam("fromDate") String fromDate, @RequestParam("toDate") String toDate,
                           @RequestParam("sp_id") String spId, @RequestParam("app_ids") String appIds,
                           @RequestParam("sp_name") String spName, @RequestParam("app_names") String appNames,
                           @RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize, Model model, HttpServletRequest request) {

        String userName = (String) request.getSession().getAttribute("userName");
        logger.info("User [{}] has requested failed transaction report", userName);

        Page<FailedTransaction> transactions = new Page<FailedTransaction>();
        String[] queries = getQueries(fromDate, toDate, spId, appIds);

        logger.info("row count query [{}]", queries[0]);
        logger.info("get row query [{}]", queries[1]);
        logger.info("parameter [{}]", queries[2]);

        transactions = sdpTransactionRepository.getFailedTransactionPage(queries[0], queries[1], queries[2], pageNo, pageSize);

        String[] list = getSP_AppList(spId, spName, appIds, appNames);

        model.addAttribute("transactions", transactions);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("spId", spId);
        model.addAttribute("appIds", appIds);
        model.addAttribute("spName", spName);
        model.addAttribute("appNames", appNames);
        model.addAttribute("spList", list[0]);
        model.addAttribute("appList", list[1]);
        model.addAttribute("pageNo", pageNo);
        model.addAttribute("pageSize", pageSize);

        return "customerCare/rpt/failedReport";
    }


    public String[] getQueries(String fromDate, String toDate, String spId, String appIds) {
        String queryCondition = "";
        String fromDateTimeStamp = fromDate + " " + "00:00:00";
        String toDateTimeStamp =  toDate + " " + "23:59:59";

        String queryParam = "S1000,P1500,P1501,"+fromDateTimeStamp+","+toDateTimeStamp;
        queryCondition = " where response_code != ? and response_code != ? and response_code != ? and time_stamp >= ?  and time_stamp <= ?";

        if(!spId.equals("")) {
            queryParam += ","+spId;
            queryCondition += " and sp_id = ?";
        }
        String temp = "";
        for (String appId : appIds.split(",")) {
            temp += " app_id = ? or";
            queryParam += "," + appId;
        }
        temp = temp.substring(0, temp.length() - 2);
        queryCondition += " and (" + temp + ")";

        return new String[]{COUNT_ROW_QUERY + queryCondition, GET_ROW_QUERY + queryCondition, queryParam};
    }

    public String[] getSP_AppList(String spId, String spName, String appIds, String appNames) {

        String spList = spName + " (" + spId + ")";
        String appList = "";

        String[] appIdArray = appIds.split(",");
        String[] appNameArray = appNames.split(",");
        for (int i = 0; i < appIdArray.length; i++) {
            appList += appNameArray[i] + " (" + appIdArray[i] + "), ";
        }
        appList = appList.substring(0, appList.length() - 2);
        return new String[]{spList, appList};
    }

}
