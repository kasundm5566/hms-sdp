/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.rpt.util;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ReportingKeyBox {

    //Report Parameters
    public static final String rpServiceProviderId = "RP_Sp_Id";
    public static final String rpServiceProviderName = "RP_Sp_Name";
    public static final String rpApplicationIds = "RP_Application_Ids";
    public static final String rpApplicationNames = "RP_Application_Names";
    public static final String rpShowGraphs = "RP_Show_Graphs";
    public static final String rpYear = "RP_Year";
    public static final String rpMonth = "RP_Month";
    public static final String rpFromDate = "RP_From_Date";
    public static final String rpToDate = "RP_To_Date";
    public static final String rpPaymentInstrument = "RP_Payment_Instrument";
    public static final String rpPaybillno = "RP_Paybill_No";
    public static final String rpTillno = "RP_Till_No";
    public static final String rpPaymentInstrumentId = "RP_Payment_Instrument_Id";
    public static final String rpOperatorId = "RP_Operator_Id";
    public static final String rpOperatorName = "RP_Operator_Name";
    public static final String rpReferenceId = "RP_Reference_Id";
    public static final String rpEventType = "RP_Event_Type";
    public static final String rpStatus = "RP_Status";
    public static final String rpAddName = "RP_Add_Name";



    //JSP PGW Pages
    public static final String pgwPIYearlyTransactionsJsp             = "pgw/pgwPIYearlyTransactions";
    public static final String pgwPIMonthlyTransactionsJsp            = "pgw/pgwPIMonthlyTransactions";
    public static final String pgwPIDailyTransactionsJsp              = "pgw/pgwPaymentInstrumentBasedDaily";

    public static final String pgwYearlySubsriberRepaymentJsp         = "pgw/pgwSubscriberRepaymentYearly";
    public static final String pgwMonthlySubsriberRepaymentJsp        = "pgw/pgwSubscriberRepaymentMonthly";
    public static final String pgwDailySubsriberRepaymentJsp          = "pgw/pgwSubscriptionRepaymentDaily";
    public static final String pgwDetailedSubsriberRepaymentJsp       = "pgw/pgwSubscriptionRepaymentDetailed";

    public static final String pgwAppBasedIncomeYearlyJsp             = "pgw/pgwYearlyApplicationEarnings";
    public static final String pgwAppBasedIncomeMonthlyJsp            = "pgw/pgwAppBasedIncomeMonthly";
    public static final String pgwAppBasedIncomeDailyJsp              = "pgw/pgwAppBasedIncomeDaily";
    public static final String pgwAppBasedIncomeDetailJsp             = "pgw/pgwAppBasedIncomeDetail";

    //JSP SDP Pages
    public static final String sdpYearlyAppRevenueJsp                 = "sdp/sdpYearlyAppRevenue";
    public static final String sdpMonthlyAppRevenueJsp                = "sdp/sdpMonthlyAppRevenue";
    public static final String sdpDailyAppRevenueJsp                  = "sdp/sdpDailyAppRevenue";

    public static final String sdpCorporateYearlyRevenueJsp           = "sdp/sdpYearlyCorporate";
    public static final String sdpCorporateMonthlyRevenueJsp          = "sdp/sdpMonthlyCorporate";
    public static final String sdpCorporateDailyRevenueJsp            = "sdp/sdpDailyCorporate";

    public static final String sdpYearlyNCSRevenueJsp                 = "sdp/sdpYearlyNCSRevenue";
    public static final String sdpMonthlyNCSRevenueJsp                = "sdp/sdpMonthlyNCSRevenue";
    public static final String sdpDailyNCSRevenueJsp                  = "sdp/sdpDailyNCSRevenue";

    public static final String sdpYearlyOperatorRevenueJsp            = "sdp/sdpYearlyOperatorRevenue";
    public static final String sdpMonthlyOperatorRevenueJsp           = "sdp/sdpMonthlyOperatorRevenue";
    public static final String sdpDailyOperatorRevenueJsp             = "sdp/sdpDailyOperatorRevenue";

    public static final String sdpYearlyRevenueJsp                    = "sdp/sdpYearlyRevenue";
    public static final String sdpMonthlyRevenueJsp                   = "sdp/sdpMonthlyRevenue";
    public static final String sdpDailyRevenueJsp                     = "sdp/sdpDailyRevenue";

    public static final String sdpFailedTransactionJsp                = "sdp/sdpFailedTransaction";

    public static final String sdpYearlyAddWiseJsp                    = "sdp/sdpYearlyAddWise";
    public static final String sdpMonthlyAddWiseJsp                   = "sdp/sdpMonthlyAddWise";

    public static final String sdpYearlyAppWiseAddJsp                 = "sdp/sdpYearlyAppWiseAdd";
    public static final String sdpMonthlyAppWiseAddJsp                = "sdp/sdpMonthlyAppWiseAdd";


    public static final String unavailablePageJsp                     = "unavailablePage";
    public static final String unauthorizedPageJsp                    = "unauthorizedPage";
    public static final String blockedPageJsp                         = "blockedPage";
    public static final String indexJsp                               = "index";
    public static final String errorPageJsp                           = "errorPage";

    public static final String customerCareDetailedTransactionJsp     = "customerCare/customerCareDetailedTransaction";
    public static final String customerCareFailedTransactionJsp       = "customerCare/customerCareFailedTransaction";
    public static final String pgwDetailedTransactionJsp              = "pgw/pgwDetailedTransaction";

    public static final String customerCareDetailedReportJsp          = "customerCare/rpt/detailedReport";
    public static final String customerCareFailedReportJsp            = "customerCare/rpt/failedReport";



    //Report Names
    public static final String pgwPIYearlyTransactionsReport          =  "report/pgw/pgw_payment_instruments_yearly_transactions.rptdesign";
    public static final String pgwPIMonthlyTransactionsReport         =  "report/pgw/pgw_pi_monthly_transactions.rptdesign";
    public static final String pgwPIDailyTransactionsReport           =  "report/pgw/pgw_payment_instruments_daily_transactions.rptdesign";

    public static final String pgwYearlyApplicationEarningsJsp        =  "report/pgw/pgw_total_yearly_earnings.rptdesign";

    public static final String pgwSubscriberYearlyReport              =  "report/pgw/pgw_subscriber_repayment_yearly.rptdesign";
    public static final String pgwSubscriberMonthlyReport             =  "report/pgw/pgw_subscriber_repayment_monthly.rptdesign";
    public static final String pgwSubscriberDailyReport               =  "report/pgw/pgw_subscriber_repayment_daily.rptdesign";
    public static final String pgwSubscriberDetailedReport            =  "report/pgw/pgw_subscriber_repayment_detailed.rptdesign";

    public static final String sdpYearlyRevenueReport                 =  "report/sdp/sdp_yearly_revenue_report.rptdesign";
    public static final String sdpMonthlyRevenueReport                =  "report/sdp/sdp_monthly_revenue_and_traffic_report.rptdesign";
    public static final String sdpDailyRevenueReport                  =  "report/sdp/sdp_daily_revenue_and_traffic_report.rptdesign";

    public static final String sdpYearlyCorporateRevenueReport        =  "report/sdp/sdp_yearly_corporateuser_revenue_report.rptdesign";
    public static final String sdpMonthlyCorporateRevenueReport       =  "report/sdp/sdp_monthly_corporateuser_revenue_and_traffic_report.rptdesign";
    public static final String sdpDailyCorporateRevenueReport         =  "report/sdp/sdp_daily_corporateuser_revenue_and_traffic_report.rptdesign";

    public static final String sdpYearlyApplicationsRevenueReport     =  "report/sdp/sdp_yearly_app_revenue_report.rptdesign";
    public static final String sdpMonthlyApplicationsRevenueReport    =  "report/sdp/sdp_monthly_app_revenue_report.rptdesign";
    public static final String sdpDailyApplicationsRevenueReport      =  "report/sdp/sdp_daily_app_revenue_report.rptdesign";

    public static final String sdpYearlyOperatorRevenueReport         =  "report/sdp/sdp_yearly_operator_report.rptdesign";
    public static final String sdpMonthlyOperatorRevenueReport        =  "report/sdp/sdp_monthly_operator_revenue_report.rptdesign";
    public static final String sdpDailyOperatorRevenueReport          =  "report/sdp/sdp_daily_operator_revenue_report.rptdesign";

    public static final String sdpYearlyNcsWiseReport                 =  "report/sdp/sdp_yearly_ncs_revenue_report.rptdesign";
    public static final String sdpMonthlyNcsWiseReport                =  "report/sdp/sdp_monthly_ncs_revenue_report.rptdesign";
    public static final String sdpDailyNcsWiseReport                  =  "report/sdp/sdp_daily_ncs_revenue_report.rptdesign";

    public static final String sdpFailedTransactionsReport            =  "report/sdp/failed_transaction_report.rptdesign";

    public static final String sdpYearlyAddWiseReport                 =  "report/sdp/yearly_ad_wise_report.rptdesign";
    public static final String sdpMonthlyAddWiseReport                =  "report/sdp/monthly_ad_wise_report.rptdesign";

    public static final String sdpYearyAppWiseAddReport               =  "report/sdp/sdp_yearly_app_wise_add_report.rptdesign";
    public static final String sdpMonthlyAppWiseAddReport             =  "report/sdp/sdp_monthly_app_wise_add_report.rptdesign";



    public static final String pgwDetailedTransactionsReport          =  "report/pgw/pgw_detailed_transaction_report.rptdesign";


}
