/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.rpt.pgw;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

@Controller
public class PIDailyIncomeController {

    private static final Logger logger = LoggerFactory
            .getLogger(PIDailyIncomeController.class);

    /**
     * Selects the home page and populates the model with a message
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/piDailyIncome.jsp", method = RequestMethod.GET)
    public String home(Model model) {
        logger.info("Welcome Payment instrument daily income!");
        model.addAttribute("controllerMessage",
                "This is the message from the controller!");
        return "pgw/piDailyIncome";
    }

}
