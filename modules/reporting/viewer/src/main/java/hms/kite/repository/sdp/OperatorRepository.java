package hms.kite.repository.sdp;

import hms.kite.domain.sdp.OperatorDomain;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
public class OperatorRepository {
    JdbcTemplate jdbcTemplate;

    public List<OperatorDomain> getAllOperators() throws Exception {
        return jdbcTemplate.query("SELECT id,operator_name FROM `operator`", new RowMapper<OperatorDomain>() {
            @Override
            public OperatorDomain mapRow(ResultSet resultSet, int i) throws SQLException {
                OperatorDomain operatorDomain = new OperatorDomain();
                operatorDomain.setOperator_id(resultSet.getInt("id"));
                operatorDomain.setOperator_name(resultSet.getString("operator_name"));
                return operatorDomain;
            }
        });
    }


    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}
