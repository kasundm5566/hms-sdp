package hms.kite.rpt.sdp;

import hms.kite.domain.sdp.GovernanceDomain;
import hms.kite.domain.sdp.ServiceProviderAPPDomain;
import hms.kite.repository.sdp.GovernanceRepository;
import hms.kite.repository.sdp.ServiceProviderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Controller
public class SDPGovernanceController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SDPGovernanceController.class);

    @Autowired
    GovernanceRepository governanceRepository;

    @RequestMapping(value = "/sdpGovernance.jsp", method = RequestMethod.GET)
    public String redirect(Model model, HttpServletRequest request) {
        String userName = (String) request.getSession().getAttribute("userName");
        LOGGER.info("User [{}] has requested SDP Governance Report", userName);
        List<GovernanceDomain> governanceDetails = new ArrayList<GovernanceDomain>();
        try {
            governanceDetails = governanceRepository.getApps();
        } catch (Exception e) {
            LOGGER.error("Unexpected exception thrown while generating the Report", e);
            return "errorPage";
        }
        model.addAttribute("governanceDetails", governanceDetails);
        return "sdp/sdpGovernance";
    }
}
