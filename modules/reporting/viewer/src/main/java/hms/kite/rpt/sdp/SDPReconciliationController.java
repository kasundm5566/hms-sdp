package hms.kite.rpt.sdp;

import hms.kite.domain.sdp.ServiceProviderCoopUserDomain;
import hms.kite.repository.sdp.ServiceProviderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Controller
public class SDPReconciliationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SDPReconciliationController.class);

    @Autowired
    ServiceProviderRepository serviceProviderRepository;

    @RequestMapping(value = "/sdpReconciliation.jsp", method = RequestMethod.GET)
    public String redirect(Model model, HttpServletRequest request) {
        String userName = (String) request.getSession().getAttribute("userName");
        LOGGER.info("User [{}] has requested SDP Reconciliation Report", userName);
        List<ServiceProviderCoopUserDomain> spDetails = new ArrayList();
        Calendar calendar = Calendar.getInstance();
        Date date = new Date();
        calendar.setTime(date);
        try {
            spDetails = serviceProviderRepository.getAllSPCoopUserIDs();
        } catch (Exception e) {
            LOGGER.error("Unexpected exception thrown while generating the Report", e);
            return "errorPage";
        }
        model.addAttribute("spDetails", spDetails);
        model.addAttribute("previousYear", calendar.get(Calendar.YEAR) - 1);
        model.addAttribute("currentYear", calendar.get(Calendar.YEAR));
        return "sdp/sdpReconciliation";
    }
}
