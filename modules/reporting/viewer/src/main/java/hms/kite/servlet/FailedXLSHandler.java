package hms.kite.servlet;

import hms.kite.domain.customerCare.FailedTransaction;
import hms.kite.repository.customerCare.ExportRepository;
import hms.kite.rpt.customerCare.rpt.FailedReportController;
import hms.kite.service.ExportXLSService;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * $LastChangedDate:  10/25/11
 * $LastChangedBy:  senduran
 * $LastChangedRevision:  $()
 */
public class FailedXLSHandler extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String fromDate = req.getParameter("fromDate");
        String toDate = req.getParameter("toDate");
        String spId = req.getParameter("sp_id");
        String spName = req.getParameter("sp_name");
        String appIds = req.getParameter("app_ids");
        String appNames = req.getParameter("app_names");

        ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
        ExportXLSService exportXLSService = (ExportXLSService) context.getBean("exportXLSService");
        ExportRepository exportRepository = (ExportRepository) context.getBean("exportRepository");

        String[] queryDetail = (new FailedReportController()).getQueries(fromDate, toDate, spId, appIds);

        List<FailedTransaction> transactionList = exportRepository.getFailedTransactions(queryDetail[1], queryDetail[2].split(","));

        byte[] fileContent = exportXLSService.failedXLSCreator(transactionList);

        Format formatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        resp.setContentType("application/xls");
        resp.setHeader("Content-Disposition", "attachment; filename=\"FailedData_" + formatter.format(System.currentTimeMillis()) + ".xls\"");
        resp.getOutputStream().write(fileContent);


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);    //To change body of overridden methods use File | Settings | File Templates.
    }
}
