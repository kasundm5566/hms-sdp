package hms.kite.repository.customerCare;

import hms.kite.domain.customerCare.DetailedTransaction;
import hms.kite.domain.customerCare.FailedTransaction;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * $LastChangedDate:  10/14/11
 * $LastChangedBy:  senduran
 * $LastChangedRevision:  $()
 */
public class ExportRepository {

    JdbcTemplate jdbcTemplate;

    public List<DetailedTransaction> getDetailedTransactions(String query, Object[] args) {
        return jdbcTemplate.query(query, args, new RowMapper<DetailedTransaction>() {
            @Override
            public DetailedTransaction mapRow(ResultSet rs, int rowNum) throws SQLException {
                DetailedTransaction detailedTransaction = new DetailedTransaction();
                detailedTransaction.setMessageId(rs.getString("correlation_id"));
                detailedTransaction.setDateTime(rs.getTimestamp("time_stamp"));
                detailedTransaction.setSpId(rs.getString("sp_id"));
                detailedTransaction.setSpName(rs.getString("sp_name"));
                detailedTransaction.setAppId(rs.getString("app_id"));
                detailedTransaction.setAppName(rs.getString("app_name"));
                detailedTransaction.setAppStatus(rs.getString("app_state"));
                detailedTransaction.setMessageType(rs.getString("ncs"));
                detailedTransaction.setDirection(rs.getString("direction"));
                detailedTransaction.setSourceAddress(rs.getString("source_address"));
                detailedTransaction.setDestinationAddress(rs.getString("destination_address"));
                detailedTransaction.setChargeAmount(rs.getDouble("sys_cur_charge_amount"));
                detailedTransaction.setTransactionStatus(rs.getString("transaction_status"));
                return detailedTransaction;
            }
        });
    }

    public List<FailedTransaction> getFailedTransactions(String query, Object[] args) {
        return jdbcTemplate.query(query, args, new RowMapper<FailedTransaction>() {
            @Override
            public FailedTransaction mapRow(ResultSet rs, int rowNum) throws SQLException {
                FailedTransaction failedTransaction = new FailedTransaction();
                failedTransaction.setMessageId(rs.getString("correlation_id"));
                failedTransaction.setSpId(rs.getString("sp_id"));
                failedTransaction.setSpName(rs.getString("sp_name"));
                failedTransaction.setAppId(rs.getString("app_id"));
                failedTransaction.setAppName(rs.getString("app_name"));
                failedTransaction.setResponseCode(rs.getString("response_code"));
                failedTransaction.setResponseDescription(rs.getString("response_description"));
                failedTransaction.setNcs(rs.getString("ncs"));
                failedTransaction.setDirection(rs.getString("direction"));
                failedTransaction.setTransactionStatus(rs.getString("transaction_status"));
                return failedTransaction;
            }
        });
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}
