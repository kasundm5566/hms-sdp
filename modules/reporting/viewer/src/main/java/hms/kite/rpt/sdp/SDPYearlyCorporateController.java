package hms.kite.rpt.sdp;

import hms.kite.domain.sdp.ServiceProviderSPDomain;
import hms.kite.repository.sdp.ServiceProviderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
@Controller
public class SDPYearlyCorporateController {

    private final static Logger LOGGER = LoggerFactory.getLogger(SDPYearlyCorporateController.class);

    @Autowired
    ServiceProviderRepository serviceProviderRepository;

    @RequestMapping(value = "/sdpYearlyCorporate.jsp", method = RequestMethod.GET)
    public String redirect(Model model, HttpServletRequest request) {
        String userName = (String) request.getSession().getAttribute("userName");
        LOGGER.info("User [{}] has requested SDP Yearly corporate user based Report", userName);
        List<ServiceProviderSPDomain> providerNameList = new ArrayList<ServiceProviderSPDomain>();
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        try {
            providerNameList = serviceProviderRepository.getAllServiceProviders();
        } catch (Exception e) {
            LOGGER.error("Unexpected exception thrown while generating the Report", e);
            return "errorPage";
        }
        model.addAttribute("sPNames", providerNameList);
        model.addAttribute("previousYear", calendar.get(Calendar.YEAR) - 1);
        model.addAttribute("currentYear", calendar.get(Calendar.YEAR));
        return "sdp/sdpYearlyCorporate";
    }


}

