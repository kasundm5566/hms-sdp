package hms.kite.rpt.customerCare;

import hms.kite.repository.sdp.ServiceProviderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by peshala on 7/25/18.
 */
@Controller
public class AppwiseSubscribedSubscriberReport {
    private final static Logger logger = LoggerFactory.getLogger(AppwiseSubscribedSubscriberReport.class);

    @Autowired
    ServiceProviderRepository serviceProviderRepository;

    @RequestMapping(value = "/appWiseSubscribedSubscriberReport.jsp", method = RequestMethod.GET)
    public String redirect(Model model, HttpServletRequest request) {
        String userName = (String) request.getSession().getAttribute("userName");
        logger.info("User [{}] has requested Appwise Subscribed Subscriber Report", userName);
        try {
            setAttributes(model, request);
        } catch (Exception e) {
            logger.error("Unexpected exception thrown while generating Appwise Subscribed Subscriber Report", e);
            return "errorPage";
        }
        return "customerCare/appWiseSubscribedSubscriberReport";
    }

    private void setAttributes(Model model, HttpServletRequest request) throws Exception {
        int userType = (Integer) request.getSession().getAttribute("uType");

        if (userType == 1) {
            model.addAttribute("spDetails", serviceProviderRepository.getAllServiceProviders());
            model.addAttribute("apps", serviceProviderRepository.getAllApps());
        } else if (userType == 2) {
            String spId = (String) request.getSession().getAttribute("spID");
            model.addAttribute("spId", spId);
            model.addAttribute("spName", serviceProviderRepository.getSPNameById(spId));
            model.addAttribute("apps", serviceProviderRepository.getAppsBySPId(spId));
        }
        model.addAttribute("type", userType);
    }
}
