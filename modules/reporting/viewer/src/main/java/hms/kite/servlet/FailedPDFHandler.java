package hms.kite.servlet;

import hms.kite.service.ExportPDFService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;

/**
 * $LastChangedDate:  10/19/11
 * $LastChangedBy:  senduran
 * $LastChangedRevision:  $()
 */
public class FailedPDFHandler extends HttpServlet {
    private final static Logger LOGGER = LoggerFactory.getLogger(FailedPDFHandler.class);


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String fromDate = req.getParameter("fromDate");
        String toDate = req.getParameter("toDate");
        String spId = req.getParameter("sp_id");
        String spName = req.getParameter("sp_name");
        String appIds = req.getParameter("app_ids");
        String appNames = req.getParameter("app_names");

        ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
        ExportPDFService exportPDFService = (ExportPDFService) context.getBean("exportPDFServices");
        byte[] fileContent = null;

        try {
            fileContent = exportPDFService.failedPdfCreator(fromDate, toDate, spId, appIds, spName, appNames);
        } catch (Exception e) {
            LOGGER.error("Error while exporting the Detailed report as PDF", e);
        }

        Format formatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");

        resp.setContentType("application/pdf");
        resp.setHeader("Content-Disposition", "attachment; filename=\"FailedReport_" + formatter.format(System.currentTimeMillis()) + ".pdf\"");
        resp.getOutputStream().write(fileContent);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);    //To change body of overridden methods use File | Settings | File Templates.
    }
}
