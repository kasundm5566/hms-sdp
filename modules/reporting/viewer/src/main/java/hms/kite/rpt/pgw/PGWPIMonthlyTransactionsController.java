/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.rpt.pgw;

import hms.common.registration.api.common.UserType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Date;

import static hms.kite.rpt.util.ReportingKeyBox.errorPageJsp;
import static hms.kite.rpt.util.ReportingKeyBox.pgwPIMonthlyTransactionsJsp;

@Controller
public class PGWPIMonthlyTransactionsController {

    private static final Logger logger = LoggerFactory.getLogger(PGWPIMonthlyTransactionsController.class);

	@RequestMapping(value = "/pgwPIMonthlyTransactions.jsp", method = RequestMethod.GET)
	public String redirect(Model model, HttpServletRequest request) {
        final String userName = (String)request.getSession().getAttribute("userName");
        logger.info("User [{}] has requested Payment Gateway Payment Instrument Monthly Report", userName);

        try {
            setAttributesOfPIMonthlyReport(model, request);
            return pgwPIMonthlyTransactionsJsp;
        } catch (Exception e) {
            logger.error( "Error occurred while trying to set attributes to the model of Payment Gateway Payment Instrument Monthly Report", e);
            return errorPageJsp;
        }

	}

    private void setAttributesOfPIMonthlyReport(Model model, HttpServletRequest request) throws Exception{
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        UserType userType = (UserType) request.getSession().getAttribute("userType");

        if (userType == UserType.CORPORATE) {
            final String spID = (String)request.getSession().getAttribute("spID");
            logger.info("Payment Gateway Payment Instrument Monthly Report requested SP : [{}] ", spID);
            model.addAttribute("spID", spID);
        }
        model.addAttribute("previousYear", calendar.get(Calendar.YEAR) - 1);
        model.addAttribute("currentYear", calendar.get(Calendar.YEAR));
        model.addAttribute("userType", userType);
    }


}
