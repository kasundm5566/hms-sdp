package hms.kite.repository.sdp;

import hms.kite.domain.sdp.GovernanceDomain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class GovernanceRepository {

    JdbcTemplate jdbcTemplate;

    public List<GovernanceDomain> getApps() throws Exception {
        return jdbcTemplate.query("SELECT DISTINCT application_id, application_name " +
                "FROM `governance_detail`", new RowMapper<GovernanceDomain>() {
            @Override
            public GovernanceDomain mapRow(ResultSet resultSet, int i) throws SQLException {
                GovernanceDomain governanceDomain = new GovernanceDomain();
                governanceDomain.setApplication_id(resultSet.getString("application_id"));
                governanceDomain.setApplication_name(resultSet.getString("application_name"));
                return governanceDomain;
            }
        });
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}
