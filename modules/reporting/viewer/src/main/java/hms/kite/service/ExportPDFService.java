package hms.kite.service;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import hms.kite.domain.customerCare.DetailedTransaction;
import hms.kite.domain.customerCare.FailedTransaction;
import hms.kite.repository.customerCare.ExportRepository;
import hms.kite.rpt.customerCare.rpt.DetailedReportController;
import hms.kite.rpt.customerCare.rpt.FailedReportController;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * $LastChangedDate:  10/14/11
 * $LastChangedBy:  senduran
 * $LastChangedRevision:  $()
 */
public class ExportPDFService {

    public final static String RESULT = "/home/senduran/Desktop/tblDetails_a4LS.pdf";

    ExportRepository exportRepository;
    private String chargeAmount;

    public byte[] detailedPdfCreator(String fromDate, String toDate, String spId, String appIds, String shortCode,
                                     String keyWord, String msisdn, String appStatus, String transactionStatus,
                                     int userType, String spName, String appNames, String sort) throws DocumentException, IOException {

        Document document = new Document(PageSize.A4.rotate(), 36f, 36f, 36f, 36f);

        DetailedReportController reportController = new DetailedReportController();
        String[] queries = reportController.getQueries(fromDate, toDate, spId, appIds, shortCode, keyWord, msisdn,
                appStatus, transactionStatus, userType, sort, 1, 0);
        String[] reportDetails = reportController.getSP_AppList(spId, spName, appIds, appNames);
        List<DetailedTransaction> detailedTransactionList = exportRepository.getDetailedTransactions(queries[1], queries[2].split(","));

        Paragraph heading = new Paragraph("Customer Care Detailed Transaction Report", new Font(Font.FontFamily.TIMES_ROMAN, 20, Font.BOLD));
        heading.setAlignment(Element.ALIGN_CENTER);

        PdfPTable detailTbl = new PdfPTable(new float[]{40f, 60f});
        detailTbl.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        detailTbl.setWidthPercentage(40f);
        detailTbl.setHorizontalAlignment(Element.ALIGN_LEFT);
        detailTbl.addCell("From Date");
        detailTbl.addCell(fromDate);
        detailTbl.addCell("To Date");
        detailTbl.addCell(toDate);
        detailTbl.addCell("Subscriber MSISDN");
        detailTbl.addCell(msisdn);

        if (!reportDetails[0].equals(" ()")) {
            detailTbl.addCell("Corporate User");
            detailTbl.addCell("["+reportDetails[0]+"]");
        }

        if (!reportDetails[1].equals(" ()")) {
            detailTbl.addCell("Application(s)");
            detailTbl.addCell("["+reportDetails[1]+"]");
        }

        if (!shortCode.equalsIgnoreCase("")) {
            detailTbl.addCell("Short Code");
            detailTbl.addCell(shortCode);
        }
        if (!keyWord.equalsIgnoreCase("")) {
            detailTbl.addCell("Key Word");
            detailTbl.addCell(keyWord);
        }

        if (!appStatus.equals("")) {
            String value = "";
            switch (Integer.parseInt(appStatus)) {
                case 1:
                    value = "Limited Live";
                    break;
                case 2:
                    value = "Live";
                    break;
                case 3:
                    value = "Intermediate";
            }
            detailTbl.addCell("Application Status");
            detailTbl.addCell(value);
        }


        float[] fieldsSize = {56f, 85f, 113f, 105f, 85f, 56f, 42f, 70f, 70f, 56f, 90f};
        PdfPTable table = new PdfPTable(fieldsSize);
        table.setWidthPercentage(100f);


        PdfPCell pCell = new PdfPCell(new Phrase("Message ID", new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));
        pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(pCell);
        pCell = new PdfPCell(new Phrase("Date Time", new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));
        pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(pCell);
        pCell = new PdfPCell(new Phrase("SP: ID / Name", new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));
        pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(pCell);
        pCell = new PdfPCell(new Phrase("APP: ID / Name", new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));
        pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(pCell);
        pCell = new PdfPCell(new Phrase("APP: State", new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));
        pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(pCell);
        pCell = new PdfPCell(new Phrase("Message Type", new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));
        pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(pCell);
        pCell = new PdfPCell(new Phrase("Direction", new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));
        pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(pCell);
        pCell = new PdfPCell(new Phrase("Source Address", new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));
        pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(pCell);
        pCell = new PdfPCell(new Phrase("Target Address", new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));
        pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(pCell);
        pCell = new PdfPCell(new Phrase(chargeAmount, new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));
        pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(pCell);
        pCell = new PdfPCell(new Phrase("Transaction Status", new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));
        pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(pCell);
        table.setHeaderRows(1);

        for (DetailedTransaction transaction : detailedTransactionList) {
            table.addCell(transaction.getMessageId());
            Format formatter = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
            table.addCell(formatter.format(transaction.getDateTime()));
            table.addCell(transaction.getSpId() + " / " + transaction.getSpName());
            table.addCell(transaction.getAppId() + " / " + transaction.getAppName());
            table.addCell(transaction.getAppStatus());
            table.addCell(transaction.getMessageType());
            table.addCell(transaction.getDirection());
            table.addCell(transaction.getSourceAddress());
            table.addCell(transaction.getDestinationAddress());
            PdfPCell amtCell = new PdfPCell(new Phrase(transaction.getChargeAmount()));
            amtCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(amtCell);
            table.addCell(transaction.getTransactionStatus());
        }

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        PdfWriter.getInstance(document, byteArrayOutputStream);
        document.open();
        document.add(heading);
        document.add(new Phrase("\n"));
        document.add(detailTbl);
        document.add(new Phrase("\n"));
        document.add(table);
        document.close();

        return byteArrayOutputStream.toByteArray();
    }


    public byte[] failedPdfCreator(String fromDate, String toDate, String spId, String appIds, String spName, String appNames) throws DocumentException, IOException {
        Document document = new Document(PageSize.A4.rotate(), 36f, 36f, 36f, 36f);

        FailedReportController failedReportController = new FailedReportController();
        String[] queries = failedReportController.getQueries(fromDate, toDate, spId, appIds);
        String[] reportDetails = failedReportController.getSP_AppList(spId, spName, appIds, appNames);
        List<FailedTransaction> failedTransactionList = exportRepository.getFailedTransactions(queries[1], queries[2].split(","));

        Paragraph heading = new Paragraph("Customer Care Failed Transaction Report", new Font(Font.FontFamily.TIMES_ROMAN, 20, Font.BOLD));
        heading.setAlignment(Element.ALIGN_CENTER);

        PdfPTable detailTbl = new PdfPTable(new float[]{40f, 60f});
        detailTbl.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        detailTbl.setWidthPercentage(40f);
        detailTbl.setHorizontalAlignment(Element.ALIGN_LEFT);
        detailTbl.addCell("From Date");
        detailTbl.addCell(fromDate);
        detailTbl.addCell("To Date");
        detailTbl.addCell(toDate);
        detailTbl.addCell("Corporate User");
        detailTbl.addCell(reportDetails[0]);
        detailTbl.addCell("Application(s)");
        detailTbl.addCell(reportDetails[1]);


        float[] fieldsSize = {60f, 100f, 110f, 70f, 95f, 45f, 45f, 75f};
        PdfPTable table = new PdfPTable(fieldsSize);
        table.setWidthPercentage(100f);


        PdfPCell pCell = new PdfPCell(new Phrase("Message ID", new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));
        pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(pCell);

        pCell = new PdfPCell(new Phrase("SP: ID / Name", new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));
        pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(pCell);
        pCell = new PdfPCell(new Phrase("APP: ID / Name", new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));
        pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(pCell);
        pCell = new PdfPCell(new Phrase("Error Code", new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));
        pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(pCell);
        pCell = new PdfPCell(new Phrase("Error Description", new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));
        pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(pCell);
        pCell = new PdfPCell(new Phrase("Message Type", new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));
        pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(pCell);
        pCell = new PdfPCell(new Phrase("Direction", new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));
        pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(pCell);
        pCell = new PdfPCell(new Phrase("Status", new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD)));
        pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(pCell);
        table.setHeaderRows(1);

        for (FailedTransaction transaction : failedTransactionList) {
            table.addCell(transaction.getMessageId());
            table.addCell(transaction.getSpId() + " / " + transaction.getSpName());
            table.addCell(transaction.getAppId() + " / " + transaction.getAppName());
            table.addCell(transaction.getResponseCode());
            table.addCell(transaction.getResponseDescription());
            table.addCell(transaction.getNcs());
            table.addCell(transaction.getDirection());
            table.addCell(transaction.getTransactionStatus());
        }

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        PdfWriter.getInstance(document, byteArrayOutputStream);
        document.open();
        document.add(heading);
        document.add(new Phrase("\n"));
        document.add(detailTbl);
        document.add(new Phrase("\n"));
        document.add(table);
        document.close();

        return byteArrayOutputStream.toByteArray();
    }

    public void setExportRepository(ExportRepository exportRepository) {
        this.exportRepository = exportRepository;
    }
    public void setChargeAmount(String chargeAmount) {
        this.chargeAmount = chargeAmount;
    }
}
