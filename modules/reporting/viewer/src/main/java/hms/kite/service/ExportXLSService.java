package hms.kite.service;

import hms.kite.domain.customerCare.DetailedTransaction;
import hms.kite.domain.customerCare.FailedTransaction;
import org.apache.poi.hssf.usermodel.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * $LastChangedDate:  10/25/11
 * $LastChangedBy:  senduran
 * $LastChangedRevision:  $()
 */
public class ExportXLSService {
    private String chargeAmount;
    public byte[] detailedXLSCreator(List<DetailedTransaction> transactionList) throws IOException {

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("DetailedReport");
        HSSFRow row1 = sheet.createRow(0);

        HSSFCellStyle cellStyleTitle = workbook.createCellStyle();
        HSSFFont hssfFontTitle = workbook.createFont();
        hssfFontTitle.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        cellStyleTitle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        cellStyleTitle.setFont(hssfFontTitle);

        HSSFCell cellA1 = row1.createCell(0);
        cellA1.setCellValue(new HSSFRichTextString("Message ID"));
        cellA1.setCellStyle(cellStyleTitle);
        sheet.autoSizeColumn((short) cellA1.getColumnIndex());

        HSSFCell cellA2 = row1.createCell(1);
        cellA2.setCellValue(new HSSFRichTextString("Created Time"));
        cellA2.setCellStyle(cellStyleTitle);
        sheet.autoSizeColumn((short) cellA2.getColumnIndex());

        HSSFCell cellA3 = row1.createCell(2);
        cellA3.setCellValue(new HSSFRichTextString("SP: ID / Name"));
        cellA3.setCellStyle(cellStyleTitle);
        sheet.autoSizeColumn((short) cellA3.getColumnIndex());

        HSSFCell cellA4 = row1.createCell(3);
        cellA4.setCellValue(new HSSFRichTextString("APP: ID / Name"));
        cellA4.setCellStyle(cellStyleTitle);
        sheet.autoSizeColumn((short) cellA4.getColumnIndex());

        HSSFCell cellA5 = row1.createCell(4);
        cellA5.setCellValue(new HSSFRichTextString("App: State"));
        cellA5.setCellStyle(cellStyleTitle);
        sheet.autoSizeColumn((short) cellA5.getColumnIndex());

        HSSFCell cellA6 = row1.createCell(5);
        cellA6.setCellValue(new HSSFRichTextString("Message Type"));
        cellA6.setCellStyle(cellStyleTitle);
        sheet.autoSizeColumn((short) cellA6.getColumnIndex());

        HSSFCell cellA7 = row1.createCell(6);
        cellA7.setCellValue(new HSSFRichTextString("Direction"));
        cellA7.setCellStyle(cellStyleTitle);
        sheet.autoSizeColumn((short) cellA7.getColumnIndex());

        HSSFCell cellA8 = row1.createCell(7);
        cellA8.setCellValue(new HSSFRichTextString("Source Address"));
        cellA8.setCellStyle(cellStyleTitle);
        sheet.autoSizeColumn((short) cellA8.getColumnIndex());

        HSSFCell cellA9 = row1.createCell(8);
        cellA9.setCellValue(new HSSFRichTextString("Target Address"));
        cellA9.setCellStyle(cellStyleTitle);
        sheet.autoSizeColumn((short) cellA9.getColumnIndex());

        HSSFCell cellA10 = row1.createCell(9);
        cellA10.setCellValue(new HSSFRichTextString(chargeAmount));
        cellA10.setCellStyle(cellStyleTitle);
        sheet.autoSizeColumn((short) cellA10.getColumnIndex());

        HSSFCell cellA11 = row1.createCell(10);
        cellA11.setCellValue(new HSSFRichTextString("Transaction Status"));
        cellA11.setCellStyle(cellStyleTitle);
        sheet.autoSizeColumn((short) cellA11.getColumnIndex());

        HSSFCellStyle cellStyleChargeAmount = workbook.createCellStyle();
        cellStyleChargeAmount.setAlignment(HSSFCellStyle.ALIGN_RIGHT);

        int i = 1;
        for (DetailedTransaction transaction : transactionList) {
            HSSFRow row = sheet.createRow(i);
            HSSFCell cell1 = row.createCell(0);
            cell1.setCellValue(new HSSFRichTextString(transaction.getMessageId()));
            sheet.autoSizeColumn((short) cell1.getColumnIndex());

            HSSFCell cell2 = row.createCell(1);
            Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            cell2.setCellValue(new HSSFRichTextString(formatter.format(transaction.getDateTime())));
            sheet.autoSizeColumn((short) cell2.getColumnIndex());

            HSSFCell cell3 = row.createCell(2);
            cell3.setCellValue(new HSSFRichTextString(transaction.getSpId() + " / " + transaction.getSpName()));
            sheet.autoSizeColumn((short) cell3.getColumnIndex());

            HSSFCell cell4 = row.createCell(3);
            cell4.setCellValue(new HSSFRichTextString(transaction.getAppId() + " / " + transaction.getAppName()));
            sheet.autoSizeColumn((short) cell4.getColumnIndex());

            HSSFCell cell5 = row.createCell(4);
            cell5.setCellValue(new HSSFRichTextString(transaction.getAppStatus()));
            sheet.autoSizeColumn((short) cell5.getColumnIndex());

            HSSFCell cell6 = row.createCell(5);
            cell6.setCellValue(new HSSFRichTextString(transaction.getMessageType()));
            sheet.autoSizeColumn((short) cell6.getColumnIndex());

            HSSFCell cell7 = row.createCell(6);
            cell7.setCellValue(new HSSFRichTextString(transaction.getDirection()));
            sheet.autoSizeColumn((short) cell7.getColumnIndex());

            HSSFCell cell8 = row.createCell(7);
            cell8.setCellValue(new HSSFRichTextString(transaction.getSourceAddress()));
            sheet.autoSizeColumn((short) cell8.getColumnIndex());

            HSSFCell cell9 = row.createCell(8);
            cell9.setCellValue(new HSSFRichTextString(transaction.getDestinationAddress()));
            sheet.autoSizeColumn((short) cell9.getColumnIndex());

            HSSFCell cell10 = row.createCell(9);
            cell10.setCellValue(new HSSFRichTextString(transaction.getChargeAmount()));
            cell10.setCellStyle(cellStyleChargeAmount);
            sheet.autoSizeColumn((short) cell10.getColumnIndex());

            HSSFCell cell11 = row.createCell(10);
            cell11.setCellValue(new HSSFRichTextString(transaction.getTransactionStatus()));
            sheet.autoSizeColumn((short) cell11.getColumnIndex());
            i++;
        }

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        workbook.write(byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }


    public byte[] failedXLSCreator(List<FailedTransaction> transactionList) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("FailedReport");
        HSSFRow row1 = sheet.createRow(0);

        HSSFCellStyle cellStyleTitle = workbook.createCellStyle();
        HSSFFont hssfFontTitle = workbook.createFont();
        hssfFontTitle.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        cellStyleTitle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        cellStyleTitle.setFont(hssfFontTitle);

        HSSFCell cellA1 = row1.createCell(0);
        cellA1.setCellValue(new HSSFRichTextString("Message ID"));
        cellA1.setCellStyle(cellStyleTitle);
        sheet.autoSizeColumn((short) cellA1.getColumnIndex());

        HSSFCell cellA2 = row1.createCell(1);
        cellA2.setCellValue(new HSSFRichTextString("SP: ID / Name"));
        cellA2.setCellStyle(cellStyleTitle);
        sheet.autoSizeColumn((short) cellA2.getColumnIndex());

        HSSFCell cellA3 = row1.createCell(2);
        cellA3.setCellValue(new HSSFRichTextString("APP: ID / Name"));
        cellA3.setCellStyle(cellStyleTitle);
        sheet.autoSizeColumn((short) cellA3.getColumnIndex());

        HSSFCell cellA4 = row1.createCell(3);
        cellA4.setCellValue(new HSSFRichTextString("Error code"));
        cellA4.setCellStyle(cellStyleTitle);
        sheet.autoSizeColumn((short) cellA4.getColumnIndex());

        HSSFCell cellA5 = row1.createCell(4);
        cellA5.setCellValue(new HSSFRichTextString("Error Description"));
        cellA5.setCellStyle(cellStyleTitle);
        sheet.autoSizeColumn((short) cellA5.getColumnIndex());

        HSSFCell cellA6 = row1.createCell(5);
        cellA6.setCellValue(new HSSFRichTextString("Message Type"));
        cellA6.setCellStyle(cellStyleTitle);
        sheet.autoSizeColumn((short) cellA6.getColumnIndex());

        HSSFCell cellA7 = row1.createCell(6);
        cellA7.setCellValue(new HSSFRichTextString("Direction"));
        cellA7.setCellStyle(cellStyleTitle);
        sheet.autoSizeColumn((short) cellA7.getColumnIndex());

        HSSFCell cellA8 = row1.createCell(7);
        cellA8.setCellValue(new HSSFRichTextString("Status"));
        cellA8.setCellStyle(cellStyleTitle);
        sheet.autoSizeColumn((short) cellA8.getColumnIndex());

        int i = 1;
        for (FailedTransaction transaction : transactionList) {
            HSSFRow row = sheet.createRow(i);
            HSSFCell cell1 = row.createCell(0);
            cell1.setCellValue(new HSSFRichTextString(transaction.getMessageId()));
            sheet.autoSizeColumn((short) cell1.getColumnIndex());

            HSSFCell cell2 = row.createCell(1);
            cell2.setCellValue(new HSSFRichTextString(transaction.getSpId() + " / " + transaction.getSpName()));
            sheet.autoSizeColumn((short) cell2.getColumnIndex());

            HSSFCell cell3 = row.createCell(2);
            cell3.setCellValue(new HSSFRichTextString(transaction.getAppId() + " / " + transaction.getAppName()));
            sheet.autoSizeColumn((short) cell3.getColumnIndex());

            HSSFCell cell4 = row.createCell(3);
            cell4.setCellValue(new HSSFRichTextString(transaction.getResponseCode()));
            sheet.autoSizeColumn((short) cell4.getColumnIndex());

            HSSFCell cell5 = row.createCell(4);
            cell5.setCellValue(new HSSFRichTextString(transaction.getResponseDescription()));
            sheet.autoSizeColumn((short) cell5.getColumnIndex());

            HSSFCell cell6 = row.createCell(5);
            cell6.setCellValue(new HSSFRichTextString(transaction.getNcs()));
            sheet.autoSizeColumn((short) cell6.getColumnIndex());

            HSSFCell cell7 = row.createCell(6);
            cell7.setCellValue(new HSSFRichTextString(transaction.getDirection()));
            sheet.autoSizeColumn((short) cell7.getColumnIndex());

            HSSFCell cell8 = row.createCell(7);
            cell8.setCellValue(new HSSFRichTextString(transaction.getTransactionStatus()));
            sheet.autoSizeColumn((short) cell8.getColumnIndex());
            i++;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        workbook.write(byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
    public void setChargeAmount(String chargeAmount) {
        this.chargeAmount = chargeAmount;
    }
}
