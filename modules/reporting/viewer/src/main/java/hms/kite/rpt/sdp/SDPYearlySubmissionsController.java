package hms.kite.rpt.sdp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Date;
/**
 * Created with IntelliJ IDEA.
 * User: dulanjalee
 * Date: 11/25/13
 * Time: 11:09 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class SDPYearlySubmissionsController {

    private static final Logger logger = LoggerFactory.getLogger(SDPYearlySubmissionsController.class);

    /**
     * Selects the home page and populates the model with a message
     *
     * @param model
     * @return
     */

    @RequestMapping(value = "/sdpYearlySubmissions.jsp", method = RequestMethod.GET)
    public String redirect(Model model, HttpServletRequest request) {
        String userName = (String) request.getSession().getAttribute("userName");
        logger.info("User [{}] has requested SDP Yearly Submissions Report", userName);
        Calendar calendar = Calendar.getInstance();
        Date date = new Date();
        calendar.setTime(date);
        model.addAttribute("previousYear", calendar.get(Calendar.YEAR) - 1);
        model.addAttribute("currentYear", calendar.get(Calendar.YEAR));
        return "sdp/sdpYearlySubmissions";
    }
}
