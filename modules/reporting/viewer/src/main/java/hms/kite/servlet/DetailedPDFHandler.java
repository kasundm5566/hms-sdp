package hms.kite.servlet;

import hms.kite.service.ExportPDFService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;

/**
 * $LastChangedDate:  10/17/11
 * $LastChangedBy:  senduran
 * $LastChangedRevision:  $()
 */
public class DetailedPDFHandler extends HttpServlet {

    private final static Logger LOGGER = LoggerFactory.getLogger(DetailedPDFHandler.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String fromDate = req.getParameter("fromDate");
        String toDate = req.getParameter("toDate");
        String spId = req.getParameter("sp_id");
        String spName = req.getParameter("sp_name");
        String appIds = req.getParameter("app_ids");
        String appNames = req.getParameter("app_names");
        String shortCode = req.getParameter("shortCode");
        String keyWord = req.getParameter("keyWord");
        String msisdn = req.getParameter("msisdn");
        String appStatus = req.getParameter("appStatus");
        String transactionStatus = req.getParameter("transactionStatus");
        int userType = Integer.parseInt(req.getParameter("userType"));
        String sort = req.getParameter("sort");

        ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
        ExportPDFService exportPDFService = (ExportPDFService) context.getBean("exportPDFServices");
        byte[] fileContent = null;


        try {
            fileContent = exportPDFService.detailedPdfCreator(fromDate, toDate, spId, appIds, shortCode, keyWord, msisdn, appStatus, transactionStatus, userType, spName, appNames, sort);
        } catch (Exception e) {
            LOGGER.error("Error while exporting the Detailed report as PDF", e);
        }

        Format formatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");

        resp.setContentType("application/pdf");
        resp.setHeader("Content-Disposition", "attachment; filename=\"DetailedReport_" + formatter.format(System.currentTimeMillis()) + ".pdf\"");
        resp.getOutputStream().write(fileContent);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);    //To change body of overridden methods use File | Settings | File Templates.
    }
}
