/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.rpt.sdp;

import hms.kite.domain.sdp.ServiceProviderSPDomain;
import hms.kite.repository.sdp.ServiceProviderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

@Controller
public class SDPMonthlyCorporateController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SDPMonthlyCorporateController.class);

    @Autowired
    ServiceProviderRepository serviceProviderRepository;

    /**
     * Selects the home page and populates the model with a message
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/sdpMonthlyCorporate.jsp", method = RequestMethod.GET)
    public String redirect(Model model, HttpServletRequest request) {
        String userName = (String) request.getSession().getAttribute("userName");
        LOGGER.info("User [{}] has requested SDP Monthly corporate user based revenue Report", userName);
        List<ServiceProviderSPDomain> spDetails = new ArrayList<ServiceProviderSPDomain>();
        Calendar calendar = Calendar.getInstance();
        Date date = new Date();
        calendar.setTime(date);
        try {
            spDetails = serviceProviderRepository.getAllServiceProviders();
        } catch (Exception e) {
            LOGGER.error("Unexpected exception thrown while generating the Report", e);
            return "errorPage";
        }
        model.addAttribute("spDetails", spDetails);
        model.addAttribute("previousYear", calendar.get(Calendar.YEAR) - 1);
        model.addAttribute("currentYear", calendar.get(Calendar.YEAR));
        return "sdp/sdpMonthlyCorporate";
    }

}
