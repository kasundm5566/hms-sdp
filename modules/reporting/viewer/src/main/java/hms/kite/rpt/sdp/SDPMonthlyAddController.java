package hms.kite.rpt.sdp;

import hms.kite.domain.sdp.AddvDomain;
import hms.kite.repository.sdp.AddvRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
@Controller
public class SDPMonthlyAddController {

    private final static Logger LOGGER = LoggerFactory.getLogger(SDPMonthlyAddController.class);

    @Autowired
    AddvRepository addvRepository;

    @RequestMapping(value = "/sdpMonthlyAddWise.jsp", method = RequestMethod.GET)
    public String redirect(Model model, HttpServletRequest request) {
        String userName = (String) request.getSession().getAttribute("userName");
        LOGGER.info("User [{}] has requested SDP Monthly advertisement Report", userName);

        List<AddvDomain> addList = new ArrayList<AddvDomain>();
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        try {
            addList = addvRepository.getAdds();
        } catch (Exception e) {
            LOGGER.error("Unexpected exception thrown while generating the Report", e);
            return "errorPage";
        }
        model.addAttribute("aDDDetail", addList);
        model.addAttribute("previousYear", calendar.get(Calendar.YEAR) - 1);
        model.addAttribute("currentYear", calendar.get(Calendar.YEAR));

        return "sdp/sdpMonthlyAddWise";
    }
}
