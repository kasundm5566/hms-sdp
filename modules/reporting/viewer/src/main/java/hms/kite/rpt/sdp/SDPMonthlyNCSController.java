/*
 * (C) Copyright 2012-2014 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.rpt.sdp;

import hms.kite.domain.sdp.OperatorDomain;
import hms.kite.repository.sdp.OperatorRepository;
import hms.kite.repository.sdp.ServiceProviderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */


@Controller
public class SDPMonthlyNCSController {

    private final static Logger LOGGER = LoggerFactory.getLogger(SDPMonthlyNCSController.class);

    @Autowired
    OperatorRepository operatorRepository;

    @Autowired
    ServiceProviderRepository serviceProviderRepository;

    @Resource(name = "sdpMonthlyNCSFeatures")
    private Map<String, String> sdpMonthlyNCSFeatures;

    @Resource(name = "sdpApplicationTypes")
    private Map<String, String> sdpApplicationTypes;

    @RequestMapping(value = "/sdpMonthlyNCSRevenue.jsp", method = RequestMethod.GET)
    public String redirect(Model model, HttpServletRequest request) {
        String userName = (String) request.getSession().getAttribute("userName");
        LOGGER.info("User [{}] has requested SDP Monthly ncs based revenue Report", userName);
        try {
            setFeatureAttributes(model);
            setAttributes(model, request);
        } catch (Exception e) {
            LOGGER.error("Unexpected exception thrown while generating Monthly NCS based Report", e);
            return "errorPage";
        }
        return "sdp/sdpMonthlyNCSRevenue";
    }

    private void setAttributes(Model model, HttpServletRequest request) throws Exception {
        int userType = (Integer) request.getSession().getAttribute("uType");
        List<OperatorDomain> operators = operatorRepository.getAllOperators();
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        if (userType == 1) {
            model.addAttribute("operatorDetails", operators);
            model.addAttribute("spDetails", serviceProviderRepository.getAllServiceProviders());
            model.addAttribute("apps", serviceProviderRepository.getAllApps());
        } else if (userType == 2) {
            String spId = (String) request.getSession().getAttribute("spID");
            model.addAttribute("operatorIds", getOperatorDetailStrings(operators));
            model.addAttribute("spId", spId);
            model.addAttribute("spName", serviceProviderRepository.getSPNameById(spId));
            model.addAttribute("apps", serviceProviderRepository.getAppsBySPId(spId));
        }

        model.addAttribute("appTypes", sdpApplicationTypes);
        model.addAttribute("type", userType);
        model.addAttribute("previousYear", calendar.get(Calendar.YEAR) - 1);
        model.addAttribute("currentYear", calendar.get(Calendar.YEAR));
    }

    private void setFeatureAttributes(Model model) {
        model.addAttribute("hideOperator", sdpMonthlyNCSFeatures.get("hideOperator"));
        model.addAttribute("hideSP", sdpMonthlyNCSFeatures.get("hideSP"));
        model.addAttribute("hideAllApplications", sdpMonthlyNCSFeatures.get("hideAllApplications"));
        model.addAttribute("hideApplications", sdpMonthlyNCSFeatures.get("hideApplications"));
        model.addAttribute("hideApplicationType", sdpMonthlyNCSFeatures.get("hideApplicationType"));
    }

    private String getOperatorDetailStrings(List<OperatorDomain> operators) {
        String operatorIds = "";

        for (OperatorDomain operator : operators) {
            operatorIds += operator.getOperator_id() + ",";
        }
        return operatorIds.substring(0, operatorIds.length() - 1);
    }

}
