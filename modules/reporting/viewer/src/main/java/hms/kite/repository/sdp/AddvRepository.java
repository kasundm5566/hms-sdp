package hms.kite.repository.sdp;

import hms.kite.domain.sdp.AddvDomain;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
public class AddvRepository {
    JdbcTemplate jdbcTemplate;

    public List<AddvDomain> getAdds() throws Exception {
        return jdbcTemplate.query("SELECT `name`, `content` FROM `advertisement`", new RowMapper<AddvDomain>() {
            @Override
            public AddvDomain mapRow(ResultSet resultSet, int i) throws SQLException {
                AddvDomain addvDomain = new AddvDomain();
                addvDomain.setAdd_name(resultSet.getString("name"));
                addvDomain.setAdd_content(resultSet.getString("content"));
                return addvDomain;
            }
        });
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}
