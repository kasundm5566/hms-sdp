/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.service;

import hms.common.registration.api.common.UserType;
import hms.common.registration.api.request.UserDetailByUsernameRequestMessage;
import hms.common.registration.api.response.BasicUserResponseMessage;
import hms.common.rest.util.JsonBodyProvider;
import hms.common.rest.util.client.AbstractWebClient;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import static hms.common.registration.api.common.RequestType.USER_BASIC_DETAILS;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class UserDetailService extends AbstractWebClient {

    private static final Logger logger = LoggerFactory.getLogger(UserDetailService.class);

    private static final String APPLICATION_CONTENT_TYPE = "application/json";

    private static final String COOP_USER_KEY = "coop-user-id";
    private static final String STATUS_CODE = "status-code";
    private static final String STATUS = "status";
    private static final String SP_ID = "sp-id";

    private String commonAdminURL;
    private String provisioningURL;
    private static final String REPORTING_MODULE_NAME = "report-viewer";

    public UserType getUserType(String username) {
        try {
            Response response = getResponse(username);
            BasicUserResponseMessage resMessage = BasicUserResponseMessage.convertFromMap(readJsonResponse(response));
            return resMessage.getType();
        } catch (IOException e) {
            return null;
        } catch (ParseException e) {
            return null;
        }
    }


    public String getServiceProviderId(String username) {
        try {
            Response response = getResponse(username);

            BasicUserResponseMessage userResponseMessage = BasicUserResponseMessage.convertFromMap(readJsonResponse(response));
            String corporateUserId = userResponseMessage.getCorporateUserId();

            logger.debug("corporate user id [{}]", corporateUserId);

            return getSPDetail(corporateUserId)[0];
        } catch (Exception e) {
            logger.error("Error in reading user details service", e);
            return null;
        }
    }

    public String getServiceProviderStatus(String userName) {
        try {
            Response response = getResponse(userName);

            BasicUserResponseMessage userResponseMessage = BasicUserResponseMessage.convertFromMap(readJsonResponse(response));
            String corporateUserId = userResponseMessage.getCorporateUserId();

            logger.debug("corporate user id [{}]", corporateUserId);

            return getSPDetail(corporateUserId)[1];
        } catch (Exception e) {
            logger.error("Error in reading user details service", e);
            return null;
        }
    }

    private String[] getSPDetail(String corporateUserId) {
        Map<String, String> reqMessage2 = new HashMap<String, String>();
        reqMessage2.put(COOP_USER_KEY, corporateUserId);

        LinkedList<Object> providers = new LinkedList<Object>();
        providers.add(new JsonBodyProvider());

        WebClient webClient2 = WebClient.create(provisioningURL, providers);
        logger.debug("provisioning url [{}]", provisioningURL);
        webClient2.header("Content-Type", APPLICATION_CONTENT_TYPE);
        webClient2.accept(APPLICATION_CONTENT_TYPE);

        Map result = webClient2.post(reqMessage2, Map.class);

        String statusCode = (String) result.get(STATUS_CODE);
        String status = (String) result.get(STATUS);
        String spId = (String) result.get(SP_ID);

        logger.debug("sp_id:[{}] status:[{}] status_code:[{}] ", new String[]{spId, status, statusCode});
        return (new String[]{spId, status, statusCode});
    }

    private Response getResponse(String username) {
        logger.debug("common registration url [{}]", commonAdminURL);
        WebClient webClient = createWebClient(commonAdminURL);

        UserDetailByUsernameRequestMessage reqMessage = new UserDetailByUsernameRequestMessage();
        reqMessage.setRequestType(USER_BASIC_DETAILS);
        reqMessage.setRequestedTimeStamp(new Date());
        reqMessage.setUserName(username);
        reqMessage.setModuleName(REPORTING_MODULE_NAME);

        logger.debug("Sending Get User Basic Details By Username [{}]", reqMessage.toString());

        return webClient.post(reqMessage.convertToMap());
    }

//    private static Object readJsonResponse(InputStream is, Class aClass) {
//        JSONProvider jsonProvider = new JSONProvider();
//        try {
//            return jsonProvider.readFrom(aClass, null, null, MediaType.APPLICATION_JSON_TYPE, null, is);
//        } catch (IOException e) {
//            logger.error("can't create the json object", e);
//            return null;
//        }
//    }

    public void setCommonAdminURL(String commonAdminURL) {
        this.commonAdminURL = commonAdminURL;
    }

    public void setProvisioningURL(String provisioningURL) {
        this.provisioningURL = provisioningURL;
    }
}
