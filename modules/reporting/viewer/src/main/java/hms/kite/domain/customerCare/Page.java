/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.domain.customerCare;

import java.util.ArrayList;
import java.util.List;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class Page<E> {

    int pageNumber;
    int pagesAvailable;
    int pageListStart;
    int pageListEnd;
    List<E> pageItems = new ArrayList<E>();

    public int getPageListStart() {
        return pageListStart;
    }

    public void setPageListStart(int pageListStart) {
        this.pageListStart = pageListStart;
    }

    public int getPageListEnd() {
        return pageListEnd;
    }

    public void setPageListEnd(int pageListEnd) {
        this.pageListEnd = pageListEnd;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPagesAvailable() {
        return pagesAvailable;
    }

    public void setPagesAvailable(int pagesAvailable) {
        this.pagesAvailable = pagesAvailable;
    }

    public List<E> getPageItems() {
        return pageItems;
    }

    public void setPageItems(List<E> pageItems) {
        this.pageItems = pageItems;
    }
}
