/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.rpt.sdp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Date;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

@Controller
public class SDPYearlyRevenueController {

    private static final Logger logger = LoggerFactory.getLogger(SDPYearlyRevenueController.class);

    /**
     * Selects the home page and populates the model with a message
     *
     * @param model
     * @return
     */

    @RequestMapping(value = "/sdpYearlyRevenue.jsp", method = RequestMethod.GET)
    public String redirect(Model model, HttpServletRequest request) {
        String userName = (String) request.getSession().getAttribute("userName");
        logger.info("User [{}] has requested SDP Yearly Report", userName);
        Calendar calendar = Calendar.getInstance();
        Date date = new Date();
        calendar.setTime(date);
        String spId = "";
        String operatorId = "";
        if ((Integer) request.getSession().getAttribute("uType") == 2) {
            spId = (String) request.getSession().getAttribute("spID");
        }
        model.addAttribute("spId", spId);
        model.addAttribute("operatorId", operatorId);
        model.addAttribute("previousYear", calendar.get(Calendar.YEAR) - 1);
        model.addAttribute("currentYear", calendar.get(Calendar.YEAR));
        return "sdp/sdpYearlyRevenue";
    }

}
