/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.rpt.customerCare.rpt;

import hms.kite.domain.customerCare.DetailedTransaction;
import hms.kite.domain.customerCare.Page;
import hms.kite.repository.customerCare.SDPTransactionRepository;
import hms.kite.service.ExportPDFService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

@Controller
public class DetailedReportController {

    private static final Logger logger = LoggerFactory.getLogger(DetailedReportController.class);

    private static final String COUNT_ROW_QUERY = "select count(*) from (select correlation_id from sdp_transaction where source_address = ?";
    private static final String GET_ROW_QUERY = "select * from (select correlation_id, time_stamp, sp_id, sp_name, app_id, app_name, app_state, (case when ncs='vdf-apis' then event_type else ncs end) as ncs , direction, source_address, destination_address, sys_cur_charge_amount, transaction_status from  sdp_transaction where source_address = ?";
    private static final String GET_ROW_QUERY_SP = "select * from (select correlation_id, time_stamp, sp_id, sp_name, app_id, app_name, app_state, (case when ncs='vdf-apis' then event_type else ncs end) as ncs, direction, if (is_number_masked='1' or is_number_masked is null, masked_source_address ,source_address) as source_address, if (is_number_masked='1' or is_number_masked is null, masked_destination_address ,destination_address) as destination_address, sys_cur_charge_amount, transaction_status from  sdp_transaction where source_address = ?";
    @Autowired
    SDPTransactionRepository sdpTransactionRepository;

    @Autowired
    ExportPDFService exportPDFService;

    /**
     * Selects the home page and populates the model with a message
     *
     * @param fromDate
     * @param toDate
     * @param spId
     * @param appIds
     * @param shortCode
     * @param keyWord
     * @param msisdn
     * @param appStatus
     * @param model
     * @return
     */

    @RequestMapping(value = "/detailedReport.jsp", method = RequestMethod.GET)
    public String redirect(@RequestParam("fromDate") String fromDate, @RequestParam("toDate") String toDate,
                           @RequestParam("sp_id") String spId, @RequestParam("app_ids") String appIds,
                           @RequestParam("sp_name") String spName, @RequestParam("app_names") String appNames,
                           @RequestParam("shortCode") String shortCode, @RequestParam("keyWord") String keyWord,
                           @RequestParam("msisdn") String msisdn, @RequestParam("appStatus") String appStatus,
                           @RequestParam("transactionStatus") String transactionStatus,
                           @RequestParam("pageNo") int pageNo, @RequestParam("pageSize") int pageSize,
                           @RequestParam("userType") int userType, @RequestParam("sort") String sort,
                           Model model, HttpServletRequest request) {


        String userName = (String) request.getSession().getAttribute("userName");
        logger.info("User [{}] has requested detailed transaction report", userName);

        Page<DetailedTransaction> transactions = new Page<DetailedTransaction>();
        String[] queries = getQueries(fromDate, toDate, spId, appIds, shortCode, keyWord, msisdn, appStatus,
                transactionStatus, userType, sort, pageNo, pageSize);

        logger.info("row count query[{}] get row query[{}] parameter[{}]", queries);

        transactions = sdpTransactionRepository.getDetailedTransactionPage(queries[0], queries[1], queries[2], pageNo, pageSize);

        String[] list = getSP_AppList(spId, spName, appIds, appNames);

        model.addAttribute("transactions", transactions);
        model.addAttribute("fromDate", fromDate);
        model.addAttribute("toDate", toDate);
        model.addAttribute("spId", spId);
        model.addAttribute("appIds", appIds);
        model.addAttribute("spName", spName);
        model.addAttribute("appNames", appNames);
        model.addAttribute("spList", list[0]);
        model.addAttribute("appList", list[1]);
        model.addAttribute("shortCode", shortCode);
        model.addAttribute("keyword", keyWord);
        model.addAttribute("msisdn", msisdn);
        model.addAttribute("appStatus", appStatus);
        model.addAttribute("transactionStatus", transactionStatus);
        model.addAttribute("pageNo", pageNo);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("userType", userType);
        model.addAttribute("sort", sort);


        return "customerCare/rpt/detailedReport";
    }


    public String[] getQueries(String fromDate, String toDate, String spId, String appIds, String shortCode,
                               String keyWord, String msisdn, String appStatus, String transactionStatus, int userType,
                               String sort, int pageNo, int pageSize) {
        int offset = (pageNo -1) * pageSize;
        String queryCondition = "";
        String queryParam = "";
        String fromDateTimeStamp = fromDate + " " + "00:00:00";
        String toDateTimeStamp =  toDate + " " + "23:59:59";


        queryParam = msisdn;

        queryParam += "," +fromDateTimeStamp+","+toDateTimeStamp;
        queryCondition = " and time_stamp >=  ? and time_stamp <= ? ";

        if(!spId.equals("")) {
            queryParam += ","+spId;
            queryCondition += " and sp_id = ?";
        }

        if(!appIds.equals("")){
            String temp = "";
            for (String appId : appIds.split(",")) {
                temp += " app_id = ? or";
                queryParam += "," + appId;
            }
            temp = temp.substring(0, temp.length() - 2);
            queryCondition += " and (" + temp + ")";
        }

        if (!shortCode.equals("")) {
            queryCondition += " and short_code = ?";
            queryParam += "," + shortCode;
        }

        if (!keyWord.equals("")) {
            queryCondition += " and keyword = ?";
            queryParam += "," + keyWord;
        }

        if (!appStatus.equals("")) {
            String value = "";
            queryCondition += " and app_state = ?";
            switch (Integer.parseInt(appStatus)) {
                case 1:
                    value = "limitedLive";
                    break;
                case 2:
                    value = "live";
                    break;
                case 3:
                    value = "intermediate";
                    break;
            }
            queryParam += "," + value;
        }

        if (!transactionStatus.equals("")) {
            if(transactionStatus.equals("2")) {
                queryCondition += " and (response_code='S1000' OR response_code='P1500') " ;
            } else if(transactionStatus.equals("3")) {
                queryCondition += "and (response_code!='S1000' AND response_code!='P1500' AND response_code!='P1501')";
            }
        }

        String countRowQuery  = COUNT_ROW_QUERY + queryCondition + " union all select correlation_id from sdp_transaction where destination_address = ?" +queryCondition + ") as join_table ";
        String queryParameters = queryParam + ',' +queryParam;

        String rowQuery = GET_ROW_QUERY + queryCondition + "union all select correlation_id, time_stamp, sp_id, sp_name, " +
                "app_id, app_name, app_state, ncs, direction, source_address, destination_address, sys_cur_charge_amount, " +
                "transaction_status from  sdp_transaction where destination_address = ?" +queryCondition + ") as join_table ";

        String spRowQuery = GET_ROW_QUERY_SP + queryCondition + " union select correlation_id, time_stamp, sp_id, " +
                "sp_name, app_id, app_name, app_state, ncs, direction, " +
                "if (is_number_masked='1' or is_number_masked is null, masked_source_address ,source_address) as source_address, " +
                "if (is_number_masked='1' or is_number_masked is null, masked_destination_address ,destination_address) as destination_address," +
                " sys_cur_charge_amount, transaction_status from  sdp_transaction where destination_address = ? " +
                queryCondition + ") as join_table ";

        String orderQuery = "";
        if(!sort.equals("") ) {
           orderQuery = " order by time_stamp ";

        }

        String limitQuery = "";
        if(pageSize != 0) {
            limitQuery = " limit " + pageSize + " offset "+ offset ;
        }

        rowQuery = rowQuery + orderQuery + limitQuery;
        spRowQuery = spRowQuery + orderQuery + limitQuery;

        if (userType == 1) {
            /*return new String[]{COUNT_ROW_QUERY + queryCondition, GET_ROW_QUERY + queryCondition + limitQuery, queryParam};*/
            return new String[]{countRowQuery, rowQuery, queryParameters};
        } else {
            /*return new String[]{COUNT_ROW_QUERY + queryCondition, GET_ROW_QUERY_SP + queryCondition+ limitQuery, queryParam};*/
            return new String[]{countRowQuery, spRowQuery, queryParameters};
        }
    }

    public String[] getSP_AppList(String spId, String spName, String appIds, String appNames) {

        String spList = spName + " (" + spId + ")";
        String appList = "";

        String[] appIdArray = appIds.split(",");
        String[] appNameArray = appNames.split(",");
        for (int i = 0; i < appIdArray.length; i++) {
            appList += appNameArray[i] + " (" + appIdArray[i] + "), ";
        }
        appList = appList.substring(0, appList.length() - 2);
        return new String[]{spList, appList};
    }

}
