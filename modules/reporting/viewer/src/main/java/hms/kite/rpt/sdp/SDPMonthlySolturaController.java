/*
 * (C) Copyright 2012-2014 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.rpt.sdp;

import hms.kite.repository.sdp.ServiceProviderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Date;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */


@Controller
public class SDPMonthlySolturaController {

    private final static Logger LOGGER = LoggerFactory.getLogger(SDPMonthlySolturaController.class);

    @Autowired
    ServiceProviderRepository serviceProviderRepository;

    @RequestMapping(value = "/sdpMonthlySoltura.jsp", method = RequestMethod.GET)
    public String redirect(Model model, HttpServletRequest request) {
        String userName = (String) request.getSession().getAttribute("userName");
        LOGGER.info("User [{}] has requested SDP Monthly Soltura Report", userName);
        try {
            setAttributes(model, request);
        } catch (Exception e) {
            LOGGER.error("Unexpected exception thrown while generating Monthly Soltura Report", e);
            return "errorPage";
        }
        return "sdp/sdpMonthlySoltura";
    }

    private void setAttributes(Model model, HttpServletRequest request) throws Exception {
        int userType = (Integer) request.getSession().getAttribute("uType");
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        if (userType == 1) {
            model.addAttribute("spDetails", serviceProviderRepository.getAllServiceProviders());
            model.addAttribute("apps", serviceProviderRepository.getAllApps());
        } else if (userType == 2) {
            String spId = (String) request.getSession().getAttribute("spID");
            model.addAttribute("spId", spId);
        }

        model.addAttribute("type", userType);
        model.addAttribute("previousYear", calendar.get(Calendar.YEAR) - 1);
        model.addAttribute("currentYear", calendar.get(Calendar.YEAR));
    }

}
