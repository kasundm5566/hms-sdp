/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.service;

import hms.kite.domain.customerCare.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class PagingHelper<E> {

    private static final Logger logger = LoggerFactory
            .getLogger(PagingHelper.class);

    private static final int PAGE_LIST_LENGTH = 9;

    public Page<E> fetchPage(
            final JdbcTemplate jdbcTemplate,
            final String sqlCountRows,
            final String sqlFetchRows,
            final Object args[],
            final int pageNo,
            final int pageSize,
            final ParameterizedRowMapper<E> rowMapper) {

        // determine how many rows are available
        final int rowCount = jdbcTemplate.queryForInt(sqlCountRows, args);

        logger.info("row count [{}]", rowCount);

        // calculate the number of pages
        int pageCount = rowCount / pageSize;
        if (rowCount > pageSize * pageCount) {
            pageCount++;
        }
        logger.info("page no [{}]", pageNo);
        logger.info("page count [{}]", pageCount);

        // create the page object
        final Page<E> page = new Page<E>();
        page.setPageNumber(pageNo);
        page.setPagesAvailable(pageCount);
        int[] pageLimits = getPageListLimits(pageNo, pageCount);
        page.setPageListStart(pageLimits[0]);
        page.setPageListEnd(pageLimits[1]);

        logger.info("page list starts at [{}] and ends at [{}]", pageLimits[0], pageLimits[1]);

        // fetch a single page of results
        /*final int startRow = (pageNo - 1) * pageSize;*/
        final int startRow = 0;

        logger.debug("start row no [{}]", startRow);

        jdbcTemplate.query(
                sqlFetchRows,
                args,
                new ResultSetExtractor() {
                    public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
                        final List<E> pageItems = page.getPageItems();
                        int currentRow = 0;
                        while (rs.next() && currentRow < startRow + pageSize) {
                            if (currentRow >= startRow) {
                                pageItems.add(rowMapper.mapRow(rs, currentRow));
                            }
                            currentRow++;
                        }
                        return page;
                    }
                });
        return page;
    }

    private int[] getPageListLimits(int pageNo, int pagesAvailable) {

        int pageListStart = pageNo - 4;
        int pageListEnd = pageNo + 4;

        if (pageListStart > 1) {
            if (pageListEnd > pagesAvailable) {
                pageListEnd = pagesAvailable;
                if(pagesAvailable > 8) {
                    pageListStart = pagesAvailable - (PAGE_LIST_LENGTH - 1);
                } else {
                    pageListStart = 1;
                }

            }
        } else {
            pageListStart = 1;
            if (pagesAvailable < PAGE_LIST_LENGTH) {
                pageListEnd = pagesAvailable;
            } else {
                pageListEnd = PAGE_LIST_LENGTH;
            }
        }

        return new int[]{pageListStart, pageListEnd};

    }
}
