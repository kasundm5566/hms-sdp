/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.repository.customerCare;

import hms.kite.domain.customerCare.DetailedTransaction;
import hms.kite.domain.customerCare.FailedTransaction;
import hms.kite.domain.customerCare.Page;
import hms.kite.service.PagingHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SDPTransactionRepository {

    private static final Logger logger = LoggerFactory
            .getLogger(SDPTransactionRepository.class);


    JdbcTemplate jdbcTemplate;

    public Page<DetailedTransaction> getDetailedTransactionPage(String queryCountRows, String queryGetRows,
                                                                String parameters, int pageNo, int pageSize) {
        PagingHelper<DetailedTransaction> pagingHelper = new PagingHelper<DetailedTransaction>();
        return pagingHelper.fetchPage(jdbcTemplate, queryCountRows, queryGetRows, parameters.split(","),
                pageNo, pageSize,
                new ParameterizedRowMapper<DetailedTransaction>() {
                    public DetailedTransaction mapRow(ResultSet rs, int i) throws SQLException {
                        DetailedTransaction detailedTransaction = new DetailedTransaction();
                        detailedTransaction.setMessageId(rs.getString("correlation_id"));
                        detailedTransaction.setDateTime(rs.getTimestamp("time_stamp"));
                        detailedTransaction.setSpId(rs.getString("sp_id"));
                        detailedTransaction.setSpName(rs.getString("sp_name"));
                        detailedTransaction.setAppId(rs.getString("app_id"));
                        detailedTransaction.setAppName(rs.getString("app_name"));
                        detailedTransaction.setAppStatus(rs.getString("app_state"));
                        detailedTransaction.setMessageType(rs.getString("ncs"));
                        detailedTransaction.setDirection(rs.getString("direction"));
                        detailedTransaction.setSourceAddress(rs.getString("source_address"));
                        detailedTransaction.setDestinationAddress(rs.getString("destination_address"));
                        detailedTransaction.setChargeAmount(rs.getDouble("sys_cur_charge_amount"));
                        detailedTransaction.setTransactionStatus(rs.getString("transaction_status"));
                        return detailedTransaction;
                    }
                }
        );
    }

    public Page<FailedTransaction> getFailedTransactionPage(String queryCountRows, String queryGetRows,
                                                            String parameters, int pageNo, int pageSize) {
        PagingHelper<FailedTransaction> pagingHelper = new PagingHelper<FailedTransaction>();
        return pagingHelper.fetchPage(jdbcTemplate, queryCountRows, queryGetRows, parameters.split(","),
                pageNo, pageSize, new ParameterizedRowMapper<FailedTransaction>() {
            public FailedTransaction mapRow(ResultSet rs, int i) throws SQLException {
                FailedTransaction failedTransaction = new FailedTransaction();
                failedTransaction.setMessageId(rs.getString("correlation_id"));
                failedTransaction.setSpId(rs.getString("sp_id"));
                failedTransaction.setSpName(rs.getString("sp_name"));
                failedTransaction.setAppId(rs.getString("app_id"));
                failedTransaction.setAppName(rs.getString("app_name"));
                failedTransaction.setResponseCode(rs.getString("response_code"));
                failedTransaction.setResponseDescription(rs.getString("response_description"));
                failedTransaction.setNcs(rs.getString("ncs"));
                failedTransaction.setDirection(rs.getString("direction"));
                failedTransaction.setTransactionStatus(rs.getString("transaction_status"));

                if (rs.getString("direction").equals("ao")) {
                    failedTransaction.setAddress(rs.getString("destination_address"));
                } else if (rs.getString("direction").equals("mt")) {
                    failedTransaction.setAddress(rs.getString("destination_address"));
                } else if (rs.getString("direction").equals("mo")) {
                    failedTransaction.setAddress(rs.getString("source_address"));
                }

                return failedTransaction;
            }
        }
        );
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}
