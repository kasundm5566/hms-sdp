/*
 * (C) Copyright 2012-2014 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.rpt.sdp;

import hms.kite.repository.sdp.ServiceProviderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */

@Controller
public class SDPMessageHistoryController {

    private final static Logger logger = LoggerFactory.getLogger(SDPDailyAppController.class);

    @Autowired
    ServiceProviderRepository serviceProviderRepository;

    @RequestMapping(value = "/sdpMessageHistory.jsp", method = RequestMethod.GET)
    public String redirect(Model model, HttpServletRequest request) {
        String userName = (String) request.getSession().getAttribute("userName");
        logger.info("User [{}] has requested SDP Message History Report", userName);
        try {
            setAttributes(model, request);
        } catch (Exception e) {
            logger.error("Unexpected exception thrown while generating Message History Report", e);
            return "errorPage";
        }
        return "sdp/sdpMessageHistory";
    }

    private void setAttributes(Model model, HttpServletRequest request) throws Exception {
        int userType = (Integer) request.getSession().getAttribute("uType");

        if (userType == 1) {
            model.addAttribute("spDetails", serviceProviderRepository.getAllServiceProviders());
            model.addAttribute("apps", serviceProviderRepository.getAllApps());
        } else if (userType == 2) {
            String spId = (String) request.getSession().getAttribute("spID");
            model.addAttribute("spId", spId);
            model.addAttribute("spName", serviceProviderRepository.getSPNameById(spId));
            model.addAttribute("apps", serviceProviderRepository.getAppsBySPId(spId));
        }
        model.addAttribute("type", userType);
    }
}
