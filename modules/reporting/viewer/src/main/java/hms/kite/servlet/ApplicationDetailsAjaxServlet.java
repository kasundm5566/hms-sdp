/*
 * (C) Copyright 2012-2014 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.servlet;

import hms.kite.domain.sdp.ServiceProviderAPPDomain;
import hms.kite.repository.sdp.ServiceProviderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ApplicationDetailsAjaxServlet extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationDetailsAjaxServlet.class);

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String spId = request.getParameter("spId");
        String appType = request.getParameter("appType");
        String reportType = request.getParameter("reportType");
        String chargingType = request.getParameter("chargingType");
        logger.debug("request for applications of sp : " + spId + ", application type : " + appType + ", charging type : " + chargingType + ", report type : " + reportType);

        ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
        ServiceProviderRepository serviceProviderRepository = (ServiceProviderRepository) context.getBean("serviceProviderRepository");

        List<ServiceProviderAPPDomain> applications = new ArrayList<ServiceProviderAPPDomain>();
        try {
            if (reportType.equals("downloadable")) {
                if (!chargingType.isEmpty() || !chargingType.equals("")) {
                    if (spId.isEmpty() || spId.equals("")) {
                        applications = serviceProviderRepository.getDownloadableAppsByChargingType(chargingType);
                    } else {
                        applications = serviceProviderRepository.getDownloadableAppsBySPAndChargingType(spId, chargingType);
                    }
                } else {
                    if (spId.isEmpty() || spId.equals("")) {
                        applications = serviceProviderRepository.getAllDownloadableApps();
                    } else {
                        applications = serviceProviderRepository.getDownloadableAppsBySPId(spId);
                    }
                }
            } else if (reportType.equals("soltura")) {
                if (!chargingType.isEmpty() || !chargingType.equals("")) {
                    if (spId.isEmpty() || spId.equals("")) {
                        applications = serviceProviderRepository.getSolturaAppsByChargingType(chargingType);
                    } else {
                        applications = serviceProviderRepository.getSolturaAppsBySPId(spId);
                    }
                } else {
                    if (spId.isEmpty() || spId.equals("")) {
                        applications = serviceProviderRepository.getAllSolturaApps();
                    } else {
                        applications = serviceProviderRepository.getSolturaAppsBySPId(spId);
                    }
                }
            } else {
                if (!appType.isEmpty() || !appType.equals("")) {
                    if (spId.isEmpty() || spId.equals("")) {
                        applications = serviceProviderRepository.getAppsByAppType(appType);
                    } else {
                        applications = serviceProviderRepository.getAppsBySPAndAppType(spId, appType);
                    }
                } else {
                    if (spId.isEmpty() || spId.equals("")) {
                        applications = serviceProviderRepository.getAllApps();
                    } else {
                        applications = serviceProviderRepository.getAppsBySPId(spId);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Unexpected exception thrown while generating the Report", e);
        }

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println(getAppList(applications));
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        doPost(request, response);
    }

    public String getAppList(List<ServiceProviderAPPDomain> applications) {
        String text = "";
        for (int i = 0; i < applications.size(); i++) {
            text += applications.get(i).getApp_id() + "++" + applications.get(i).getApp_name();
            if (i != applications.size() - 1) {
                text += "&&";
            }
        }
        return text;
    }

}
