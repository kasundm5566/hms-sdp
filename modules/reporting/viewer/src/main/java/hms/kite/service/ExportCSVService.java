package hms.kite.service;

import hms.kite.domain.customerCare.DetailedTransaction;
import hms.kite.domain.customerCare.FailedTransaction;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * $LastChangedDate:  10/18/11
 * $LastChangedBy:  senduran
 * $LastChangedRevision:  $()
 */
public class ExportCSVService {

    private String exportCSVSeparator;
    private String exportCSVNewLine;
    private String chargeAmount;

    public byte[] detailCSVCreator(List<DetailedTransaction> transactionList) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("MessageID");
        stringBuilder.append(exportCSVSeparator);
        stringBuilder.append("CreatedTime");
        stringBuilder.append(exportCSVSeparator);
        stringBuilder.append("SpID");
        stringBuilder.append(exportCSVSeparator);
        stringBuilder.append("SpName");
        stringBuilder.append(exportCSVSeparator);
        stringBuilder.append("AppID");
        stringBuilder.append(exportCSVSeparator);
        stringBuilder.append("AppName");
        stringBuilder.append(exportCSVSeparator);
        stringBuilder.append("AppState");
        stringBuilder.append(exportCSVSeparator);
        stringBuilder.append("MessageType");
        stringBuilder.append(exportCSVSeparator);
        stringBuilder.append("Direction");
        stringBuilder.append(exportCSVSeparator);
        stringBuilder.append("SourceAddress");
        stringBuilder.append(exportCSVSeparator);
        stringBuilder.append("TargetAddress");
        stringBuilder.append(exportCSVSeparator);
        stringBuilder.append(chargeAmount);
        stringBuilder.append(exportCSVSeparator);
        stringBuilder.append("TransactionStatus");
        stringBuilder.append(exportCSVNewLine);
        for (DetailedTransaction transaction : transactionList) {
            stringBuilder.append(transaction.getMessageId());
            stringBuilder.append(exportCSVSeparator);
            Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            stringBuilder.append(formatter.format(transaction.getDateTime()));
            stringBuilder.append(exportCSVSeparator);
            stringBuilder.append(transaction.getSpId());
            stringBuilder.append(exportCSVSeparator);
            stringBuilder.append(transaction.getSpName());
            stringBuilder.append(exportCSVSeparator);
            stringBuilder.append(transaction.getAppId());
            stringBuilder.append(exportCSVSeparator);
            stringBuilder.append(transaction.getAppName());
            stringBuilder.append(exportCSVSeparator);
            stringBuilder.append(transaction.getAppStatus());
            stringBuilder.append(exportCSVSeparator);
            stringBuilder.append(transaction.getMessageType());
            stringBuilder.append(exportCSVSeparator);
            stringBuilder.append(transaction.getDirection());
            stringBuilder.append(exportCSVSeparator);
            stringBuilder.append(transaction.getSourceAddress());
            stringBuilder.append(exportCSVSeparator);
            stringBuilder.append(transaction.getDestinationAddress());
            stringBuilder.append(exportCSVSeparator);
            stringBuilder.append(transaction.getChargeAmount());
            stringBuilder.append(exportCSVSeparator);
            stringBuilder.append(transaction.getTransactionStatus());
            stringBuilder.append(exportCSVNewLine);
        }
        return stringBuilder.toString().getBytes();

    }

    public byte[] failCSVCreator(List<FailedTransaction> transactionList) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("MessageID");
        stringBuilder.append(exportCSVSeparator);
        stringBuilder.append("SpID");
        stringBuilder.append(exportCSVSeparator);
        stringBuilder.append("SpName");
        stringBuilder.append(exportCSVSeparator);
        stringBuilder.append("AppID");
        stringBuilder.append(exportCSVSeparator);
        stringBuilder.append("AppName");
        stringBuilder.append(exportCSVSeparator);
        stringBuilder.append("ErrorCode");
        stringBuilder.append(exportCSVSeparator);
        stringBuilder.append("ErrorDescription");
        stringBuilder.append(exportCSVSeparator);
        stringBuilder.append("MessageType");
        stringBuilder.append(exportCSVSeparator);
        stringBuilder.append("Direction");
        stringBuilder.append(exportCSVSeparator);
        stringBuilder.append("Status");
        stringBuilder.append(exportCSVNewLine);
        for (FailedTransaction transaction : transactionList) {
            stringBuilder.append(transaction.getMessageId());
            stringBuilder.append(exportCSVSeparator);
            stringBuilder.append(transaction.getSpId());
            stringBuilder.append(exportCSVSeparator);
            stringBuilder.append(transaction.getSpName());
            stringBuilder.append(exportCSVSeparator);
            stringBuilder.append(transaction.getAppId());
            stringBuilder.append(exportCSVSeparator);
            stringBuilder.append(transaction.getAppName());
            stringBuilder.append(exportCSVSeparator);
            stringBuilder.append(transaction.getResponseCode());
            stringBuilder.append(exportCSVSeparator);
            stringBuilder.append(transaction.getResponseDescription());
            stringBuilder.append(exportCSVSeparator);
            stringBuilder.append(transaction.getNcs());
            stringBuilder.append(exportCSVSeparator);
            stringBuilder.append(transaction.getDirection());
            stringBuilder.append(exportCSVSeparator);
            stringBuilder.append(transaction.getTransactionStatus());
            stringBuilder.append(exportCSVNewLine);
        }
        return stringBuilder.toString().getBytes();
    }

    public void setExportCSVSeparator(String exportCSVSeparator) {
        this.exportCSVSeparator = exportCSVSeparator;
    }

    public void setExportCSVNewLine(String exportCSVNewLine) {
        this.exportCSVNewLine = exportCSVNewLine;
    }
    public void setChargeAmount(String chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

}
