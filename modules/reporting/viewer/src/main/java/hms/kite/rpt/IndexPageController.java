package hms.kite.rpt;

import hms.common.registration.api.common.UserType;
import hms.kite.service.UserDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
public class IndexPageController {

    @Autowired
    UserDetailService userDetailService;

    private static final Logger logger = LoggerFactory.getLogger(IndexPageController.class);

    /**
     * Selects the home page and populates the model with a message
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String redirect(Model model, HttpServletRequest request) {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String userName = userDetails.getUsername();
        logger.info("User [{}] has requested Report index page", userName);

        UserType userType = userDetailService.getUserType(userName);

        int userTypeID = 0;
        if (userType.equals(UserType.ADMIN_USER)) {
            userTypeID = 1;
        } else if (userType.equals(UserType.CORPORATE) || userType.equals(UserType.SUB_CORPORATE)) {
            userTypeID = 2;
            request.getSession().setAttribute("spID", userDetailService.getServiceProviderId(userName));
        }

        if (userTypeID == 2 && !userDetailService.getServiceProviderStatus(userName).equalsIgnoreCase("allowed")) {
            logger.info("User [{}] is not approved to view the report", userName);
            return "blockedPage";
        }
        request.getSession().setAttribute("userName", userName);
        request.getSession().setAttribute("uType", userTypeID);
        return "index";
    }

}
