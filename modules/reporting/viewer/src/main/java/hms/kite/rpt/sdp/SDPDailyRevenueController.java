/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.rpt.sdp;

import hms.kite.service.UserDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

@Controller
public class SDPDailyRevenueController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SDPDailyRevenueController.class);

    @Autowired
    UserDetailService userDetailService;

    /**
     * Selects the home page and populates the model with a message
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/sdpDailyRevenue.jsp", method = RequestMethod.GET)
    public String redirect(Model model, HttpServletRequest request) {
        String userName = (String) request.getSession().getAttribute("userName");
        LOGGER.info("User [{}] has requested SDP Daily revenue Report", userName);
        String spId = "";
        String operatorId = "";
        if ((Integer) request.getSession().getAttribute("uType") == 2) {
            spId = (String) request.getSession().getAttribute("spID");
        }
        model.addAttribute("spId", spId);
        model.addAttribute("operatorId", operatorId);

        return "sdp/sdpDailyRevenue";
    }

}
