package hms.kite.servlet;

import hms.kite.domain.customerCare.FailedTransaction;
import hms.kite.repository.customerCare.ExportRepository;
import hms.kite.rpt.customerCare.rpt.FailedReportController;
import hms.kite.service.ExportCSVService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * $LastChangedDate:  10/19/11
 * $LastChangedBy:  senduran
 * $LastChangedRevision:  $()
 */
public class FailedCSVHandler extends HttpServlet {

    private final static Logger LOGGER = LoggerFactory.getLogger(FailedCSVHandler.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String fromDate = req.getParameter("fromDate");
        String toDate = req.getParameter("toDate");
        String spId = req.getParameter("sp_id");
        String spName = req.getParameter("sp_name");
        String appIds = req.getParameter("app_ids");
        String appNames = req.getParameter("app_names");

        ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
        ExportCSVService exportCSVService = (ExportCSVService) context.getBean("exportCSVService");
        ExportRepository exportRepository = (ExportRepository) context.getBean("exportRepository");

        String[] queryDetail = (new FailedReportController()).getQueries(fromDate, toDate, spId, appIds);

        List<FailedTransaction> transactionList = exportRepository.getFailedTransactions(queryDetail[1], queryDetail[2].split(","));

        byte[] fileContent = exportCSVService.failCSVCreator(transactionList);
        Format formatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        resp.setContentType("application/csv");
        resp.setHeader("Content-Disposition", "attachment; filename=\"FailedData_" + formatter.format(System.currentTimeMillis()) + ".csv\"");
        resp.getOutputStream().write(fileContent);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);    //To change body of overridden methods use File | Settings | File Templates.
    }
}
