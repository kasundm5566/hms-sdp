package hms.kite.domain.sdp;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
public class AddvDomain {
    String add_name;
    String add_content;

    public String getAdd_name() {
        return add_name;
    }

    public void setAdd_name(String add_name) {
        this.add_name = add_name;
    }

    public String getAdd_content() {
        return add_content;
    }

    public void setAdd_content(String add_content) {
        this.add_content = add_content;
    }
}
