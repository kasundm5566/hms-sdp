package hms.kite.rpt.sdp;

import hms.kite.domain.sdp.OperatorDomain;
import hms.kite.repository.sdp.OperatorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
@Controller
public class SDPDailyOperatorController {

    private final static Logger LOGGER = LoggerFactory.getLogger(SDPDailyOperatorController.class);

    @Autowired
    OperatorRepository operatorRepository;

    @RequestMapping(value = "/sdpDailyOperatorRevenue.jsp", method = RequestMethod.GET)
    public String redirect(Model model, HttpServletRequest request) {
        String userName = (String) request.getSession().getAttribute("userName");
        LOGGER.info("User [{}] has requested SDP Daily operator based revenue Report", userName);
        List<OperatorDomain> operatorList = new ArrayList<OperatorDomain>();
        try {
            operatorList = operatorRepository.getAllOperators();
        } catch (Exception e) {
            LOGGER.error("Unexpected exception thrown while generating the Report", e);
            return "errorPage";
        }
        model.addAttribute("oPDetail", operatorList);

        return "sdp/sdpDailyOperatorRevenue";
    }
}
