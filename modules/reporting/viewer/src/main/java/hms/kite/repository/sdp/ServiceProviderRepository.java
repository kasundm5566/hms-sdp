/*
 * (C) Copyright 2012-2014 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.repository.sdp;

import hms.kite.domain.sdp.ServiceProviderAPPDomain;
import hms.kite.domain.sdp.ServiceProviderCoopUserDomain;
import hms.kite.domain.sdp.ServiceProviderSPDomain;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
public class ServiceProviderRepository {

    JdbcTemplate jdbcTemplate;

    public List<ServiceProviderSPDomain> getAllServiceProviders() throws Exception {
        return jdbcTemplate.query("SELECT sp_id, sp_name FROM `service_provider_info` WHERE (app_id IS NOT NULL OR app_id!='') GROUP BY sp_id ORDER BY sp_name", new RowMapper<ServiceProviderSPDomain>() {
            @Override
            public ServiceProviderSPDomain mapRow(ResultSet resultSet, int i) throws SQLException {
                ServiceProviderSPDomain serviceProviderSPDomain = new ServiceProviderSPDomain();
                serviceProviderSPDomain.setSp_id(resultSet.getString("sp_id"));
                serviceProviderSPDomain.setSp_name(resultSet.getString("sp_name"));
                return serviceProviderSPDomain;
            }
        });
    }

    public List<ServiceProviderCoopUserDomain> getAllSPCoopUserIDs() throws Exception {
        return jdbcTemplate.query("SELECT sp_id, sp_name, corporate_user_id FROM `service_provider_info` WHERE (app_id IS NOT NULL OR app_id!='') GROUP BY sp_id ORDER BY sp_name", new RowMapper<ServiceProviderCoopUserDomain>() {
            @Override
            public ServiceProviderCoopUserDomain mapRow(ResultSet resultSet, int i) throws SQLException {
                ServiceProviderCoopUserDomain serviceProviderCoopUserDomain = new ServiceProviderCoopUserDomain();
                serviceProviderCoopUserDomain.setSp_id(resultSet.getString("sp_id"));
                serviceProviderCoopUserDomain.setSp_name(resultSet.getString("sp_name"));
                serviceProviderCoopUserDomain.setCoop_user_id(resultSet.getString("corporate_user_id"));
                return serviceProviderCoopUserDomain;
            }
        });
    }

    public List<ServiceProviderAPPDomain> getAllApps() throws Exception {
        List<ServiceProviderAPPDomain> appDomainList = jdbcTemplate.query("SELECT app_id, app_name FROM `service_provider_info` WHERE (app_id IS NOT NULL OR app_id!='') GROUP BY app_id ORDER BY app_name", new RowMapper<ServiceProviderAPPDomain>() {
            @Override
            public ServiceProviderAPPDomain mapRow(ResultSet resultSet, int i) throws SQLException {
                ServiceProviderAPPDomain serviceProviderAPPDomain = new ServiceProviderAPPDomain();
                serviceProviderAPPDomain.setApp_id(resultSet.getString("app_id"));
                serviceProviderAPPDomain.setApp_name(resultSet.getString("app_name"));
                return serviceProviderAPPDomain;
            }
        });
        return getValidApps(appDomainList);
    }

    public List<ServiceProviderAPPDomain> getAppsBySPId(String spId) throws Exception {
        List<ServiceProviderAPPDomain> appDomainList = jdbcTemplate.query("SELECT app_id, app_name FROM `service_provider_info` WHERE sp_id=? AND (app_id IS NOT NULL OR app_id!='') GROUP BY app_id ORDER BY app_name", new RowMapper<ServiceProviderAPPDomain>() {
            @Override
            public ServiceProviderAPPDomain mapRow(ResultSet resultSet, int i) throws SQLException {
                ServiceProviderAPPDomain serviceProviderAPPDomain = new ServiceProviderAPPDomain();
                serviceProviderAPPDomain.setApp_id(resultSet.getString("app_id"));
                serviceProviderAPPDomain.setApp_name(resultSet.getString("app_name"));
                return serviceProviderAPPDomain;
            }
        }, spId);
        return getValidApps(appDomainList);
    }

    public List<ServiceProviderAPPDomain> getAppsByAppType(String appType) throws Exception {
        List<ServiceProviderAPPDomain> appDomainList = jdbcTemplate.query("SELECT app_id, app_name FROM `service_provider_info` WHERE app_catogory=? AND (app_id IS NOT NULL OR app_id!='') GROUP BY app_id ORDER BY app_name", new RowMapper<ServiceProviderAPPDomain>() {
            @Override
            public ServiceProviderAPPDomain mapRow(ResultSet resultSet, int i) throws SQLException {
                ServiceProviderAPPDomain serviceProviderAPPDomain = new ServiceProviderAPPDomain();
                serviceProviderAPPDomain.setApp_id(resultSet.getString("app_id"));
                serviceProviderAPPDomain.setApp_name(resultSet.getString("app_name"));
                return serviceProviderAPPDomain;
            }
        }, appType);
        return getValidApps(appDomainList);
    }

    public List<ServiceProviderAPPDomain> getAppsBySPAndAppType(String spId, String appType) throws Exception {
        List<ServiceProviderAPPDomain> appDomainList = jdbcTemplate.query("SELECT app_id, app_name FROM `service_provider_info` WHERE sp_id=? AND app_catogory=? AND (app_id IS NOT NULL OR app_id!='') GROUP BY app_id ORDER BY app_name", new RowMapper<ServiceProviderAPPDomain>() {
            @Override
            public ServiceProviderAPPDomain mapRow(ResultSet resultSet, int i) throws SQLException {
                ServiceProviderAPPDomain serviceProviderAPPDomain = new ServiceProviderAPPDomain();
                serviceProviderAPPDomain.setApp_id(resultSet.getString("app_id"));
                serviceProviderAPPDomain.setApp_name(resultSet.getString("app_name"));
                return serviceProviderAPPDomain;
            }
        }, spId, appType);
        return getValidApps(appDomainList);
    }

    public List<ServiceProviderAPPDomain> getAllSolturaApps() throws Exception {
        List<ServiceProviderAPPDomain> appDomainList = jdbcTemplate.query("SELECT app_id, app_name FROM `service_provider_info` WHERE app_catogory='soltura' AND (app_id IS NOT NULL OR app_id!='') GROUP BY app_id ORDER BY app_name", new RowMapper<ServiceProviderAPPDomain>() {
            @Override
            public ServiceProviderAPPDomain mapRow(ResultSet resultSet, int i) throws SQLException {
                ServiceProviderAPPDomain serviceProviderAPPDomain = new ServiceProviderAPPDomain();
                serviceProviderAPPDomain.setApp_id(resultSet.getString("app_id"));
                serviceProviderAPPDomain.setApp_name(resultSet.getString("app_name"));
                return serviceProviderAPPDomain;
            }
        });
        return getValidApps(appDomainList);
    }

    public List<ServiceProviderAPPDomain> getSolturaAppsBySPId(String spId) throws Exception {
        List<ServiceProviderAPPDomain> appDomainList = jdbcTemplate.query("SELECT app_id, app_name FROM `service_provider_info` WHERE sp_id = ? AND  app_catogory='soltura' AND (app_id IS NOT NULL OR app_id!='') GROUP BY app_id ORDER BY app_name", new RowMapper<ServiceProviderAPPDomain>() {
            @Override
            public ServiceProviderAPPDomain mapRow(ResultSet resultSet, int i) throws SQLException {
                ServiceProviderAPPDomain serviceProviderAPPDomain = new ServiceProviderAPPDomain();
                serviceProviderAPPDomain.setApp_id(resultSet.getString("app_id"));
                serviceProviderAPPDomain.setApp_name(resultSet.getString("app_name"));
                return serviceProviderAPPDomain;
            }
        }, spId);
        return getValidApps(appDomainList);
    }

    public List<ServiceProviderAPPDomain> getSolturaAppsByChargingType(String chargingType) throws Exception {
        List<ServiceProviderAPPDomain> appDomainList = jdbcTemplate.query("SELECT app_id, app_name FROM `service_provider_info` WHERE app_catogory='soltura' AND (app_id IS NOT NULL OR app_id!='') GROUP BY app_id ORDER BY app_name", new RowMapper<ServiceProviderAPPDomain>() {
            @Override
            public ServiceProviderAPPDomain mapRow(ResultSet resultSet, int i) throws SQLException {
                ServiceProviderAPPDomain serviceProviderAPPDomain = new ServiceProviderAPPDomain();
                serviceProviderAPPDomain.setApp_id(resultSet.getString("app_id"));
                serviceProviderAPPDomain.setApp_name(resultSet.getString("app_name"));
                return serviceProviderAPPDomain;
            }
        });
        return getValidApps(appDomainList);
    }

    public List<ServiceProviderAPPDomain> getSolturaAppsBySPAndChargingType(String spId, String chargingType) throws Exception {
        List<ServiceProviderAPPDomain> appDomainList = jdbcTemplate.query("SELECT app_id, app_name FROM `service_provider_info` WHERE app_catogory='soltura' AND (app_id IS NOT NULL OR app_id!='') GROUP BY app_id ORDER BY app_name", new RowMapper<ServiceProviderAPPDomain>() {
            @Override
            public ServiceProviderAPPDomain mapRow(ResultSet resultSet, int i) throws SQLException {
                ServiceProviderAPPDomain serviceProviderAPPDomain = new ServiceProviderAPPDomain();
                serviceProviderAPPDomain.setApp_id(resultSet.getString("app_id"));
                serviceProviderAPPDomain.setApp_name(resultSet.getString("app_name"));
                return serviceProviderAPPDomain;
            }
        });
        return getValidApps(appDomainList);
    }

    public List<ServiceProviderAPPDomain> getAllDownloadableApps() throws Exception {
        List<ServiceProviderAPPDomain> appDomainList = jdbcTemplate.query("SELECT DISTINCT app_id, app_name FROM `sdp_transaction_summary` WHERE  ncs_id=7 GROUP BY app_id ORDER BY app_name", new RowMapper<ServiceProviderAPPDomain>() {
            @Override
            public ServiceProviderAPPDomain mapRow(ResultSet resultSet, int i) throws SQLException {
                ServiceProviderAPPDomain serviceProviderAPPDomain = new ServiceProviderAPPDomain();
                serviceProviderAPPDomain.setApp_id(resultSet.getString("app_id"));
                serviceProviderAPPDomain.setApp_name(resultSet.getString("app_name"));
                return serviceProviderAPPDomain;
            }
        });
        return getValidApps(appDomainList);
    }

    public List<ServiceProviderAPPDomain> getDownloadableAppsBySPId(String spId) throws Exception {
        List<ServiceProviderAPPDomain> appDomainList = jdbcTemplate.query("SELECT app_id, app_name FROM `sdp_transaction_summary` WHERE  ncs_id=7 AND sp_id=? GROUP BY app_id ORDER BY app_name", new RowMapper<ServiceProviderAPPDomain>() {
            @Override
            public ServiceProviderAPPDomain mapRow(ResultSet resultSet, int i) throws SQLException {
                ServiceProviderAPPDomain serviceProviderAPPDomain = new ServiceProviderAPPDomain();
                serviceProviderAPPDomain.setApp_id(resultSet.getString("app_id"));
                serviceProviderAPPDomain.setApp_name(resultSet.getString("app_name"));
                return serviceProviderAPPDomain;
            }
        }, spId);
        return getValidApps(appDomainList);
    }

    public List<ServiceProviderAPPDomain> getDownloadableAppsByChargingType(String chargingType) throws Exception {
        List<ServiceProviderAPPDomain> appDomainList = jdbcTemplate.query("SELECT DISTINCT app_id, app_name FROM `sdp_transaction_summary` WHERE  ncs_id=7 AND charging_type=? GROUP BY app_id ORDER BY app_name", new RowMapper<ServiceProviderAPPDomain>() {
            @Override
            public ServiceProviderAPPDomain mapRow(ResultSet resultSet, int i) throws SQLException {
                ServiceProviderAPPDomain serviceProviderAPPDomain = new ServiceProviderAPPDomain();
                serviceProviderAPPDomain.setApp_id(resultSet.getString("app_id"));
                serviceProviderAPPDomain.setApp_name(resultSet.getString("app_name"));
                return serviceProviderAPPDomain;
            }
        }, chargingType);
        return getValidApps(appDomainList);
    }

    public List<ServiceProviderAPPDomain> getDownloadableAppsBySPAndChargingType(String spId, String chargingType) throws Exception {
        List<ServiceProviderAPPDomain> appDomainList = jdbcTemplate.query("SELECT DISTINCT app_id, app_name FROM `sdp_transaction_summary` WHERE ncs_id=7 AND sp_id=? AND charging_type=? GROUP BY app_id ORDER BY app_name", new RowMapper<ServiceProviderAPPDomain>() {
            @Override
            public ServiceProviderAPPDomain mapRow(ResultSet resultSet, int i) throws SQLException {
                ServiceProviderAPPDomain serviceProviderAPPDomain = new ServiceProviderAPPDomain();
                serviceProviderAPPDomain.setApp_id(resultSet.getString("app_id"));
                serviceProviderAPPDomain.setApp_name(resultSet.getString("app_name"));
                return serviceProviderAPPDomain;
            }
        }, spId, chargingType);
        return getValidApps(appDomainList);
    }

    private List<ServiceProviderAPPDomain> getValidApps(List<ServiceProviderAPPDomain> appList) {
        List<ServiceProviderAPPDomain> appDomainList = appList;
        for (int i = 0; i < appDomainList.size(); i++) {
            if (appDomainList.get(i).getApp_id().equalsIgnoreCase("")) {
                appDomainList.remove(i);
            }
        }
        return appDomainList;
    }

    public String getSPNameById(String spId) throws Exception {
        return jdbcTemplate.queryForObject("SELECT DISTINCT sp_name FROM `service_provider_info` WHERE sp_id = ?", String.class, spId);
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

}
