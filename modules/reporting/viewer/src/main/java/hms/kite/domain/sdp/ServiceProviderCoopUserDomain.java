package hms.kite.domain.sdp;

public class ServiceProviderCoopUserDomain {

    private String sp_id;
    private String sp_name;
    private String coop_user_id;

    public String getSp_id() {
        return sp_id;
    }

    public void setSp_id(String sp_id) {
        this.sp_id = sp_id;
    }

    public String getSp_name() {
        return sp_name;
    }

    public void setSp_name(String sp_name) {
        this.sp_name = sp_name;
    }

    public String getCoop_user_id() {
        return coop_user_id;
    }

    public void setCoop_user_id(String coop_user_id) {
        this.coop_user_id = coop_user_id;
    }
}
