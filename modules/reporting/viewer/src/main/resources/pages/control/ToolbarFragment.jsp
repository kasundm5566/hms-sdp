<%-----------------------------------------------------------------------------
	Copyright (c) 2004 Actuate Corporation and others.
	All rights reserved. This program and the accompanying materials
	are made available under the terms of the Eclipse Public License v1.0
	which accompanies this distribution, and is available at
	http://www.eclipse.org/legal/epl-v10.html

	Contributors:
		Actuate Corporation - Initial implementation.
-----------------------------------------------------------------------------%>
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page session="false" buffer="none" %>
<%@ page import="org.eclipse.birt.report.presentation.aggregation.IFragment,
                 org.eclipse.birt.report.resource.BirtResources,
                 org.eclipse.birt.report.utility.ParameterAccessor,
                 org.eclipse.birt.report.servlet.ViewerServlet" %>

<%-----------------------------------------------------------------------------
	Expected java beans
-----------------------------------------------------------------------------%>
<jsp:useBean id="fragment" type="org.eclipse.birt.report.presentation.aggregation.IFragment" scope="request"/>
<jsp:useBean id="attributeBean" type="org.eclipse.birt.report.context.BaseAttributeBean" scope="request"/>

<%-----------------------------------------------------------------------------
	Toolbar fragment
-----------------------------------------------------------------------------%>
<link href="birt/styles/bootstrap/css/bootstrap.css" rel="stylesheet" type='text/css'>
<link href="birt/styles/custom.css" rel="stylesheet" type='text/css'>


<div class="navbar">
    <div class="navbar-inner">
        <a id="productHomeLink" class="brand" href="http://www.ideamart.lk/" style="padding-left: 36px;">
            <img src="../images/main_logo.png" />
        </a>
        <label class="brand" style="padding-top: 16px;">Reporting Module</label>
        <ul class="nav pull-right" style="padding-right: 40px">
            <div style="padding-top: 10px;"><img src="../images/sub_logo.png"/></div>
        </ul>

    </div>
</div>