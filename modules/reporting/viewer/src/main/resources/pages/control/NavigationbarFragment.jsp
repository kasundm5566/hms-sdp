<%-----------------------------------------------------------------------------
	Copyright (c) 2004 Actuate Corporation and others.
	All rights reserved. This program and the accompanying materials 
	are made available under the terms of the Eclipse Public License v1.0
	which accompanies this distribution, and is available at
	http://www.eclipse.org/legal/epl-v10.html
	
	Contributors:
		Actuate Corporation - Initial implementation.
-----------------------------------------------------------------------------%>
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page session="false" buffer="none" %>
<%@ page import="org.eclipse.birt.report.presentation.aggregation.IFragment,
				 org.eclipse.birt.report.context.BaseAttributeBean,
				 org.eclipse.birt.report.utility.ParameterAccessor,
				 org.eclipse.birt.report.servlet.ViewerServlet,
				 org.eclipse.birt.report.resource.BirtResources" %>

<%-----------------------------------------------------------------------------
	Expected java beans
-----------------------------------------------------------------------------%>
<jsp:useBean id="fragment" type="org.eclipse.birt.report.presentation.aggregation.IFragment" scope="request" />
<jsp:useBean id="attributeBean" type="org.eclipse.birt.report.context.BaseAttributeBean" scope="request" />

<%-----------------------------------------------------------------------------
	Navigation bar fragment
-----------------------------------------------------------------------------%>

<script type="text/javascript">

    function exportReport(format) {
        var requestMethod = ""
        if(<%= request.getParameterMap().containsKey("__request_method")%>) {
            requestMethod = "<%= request.getParameter("__request_method") %>";
        }

        if(requestMethod == "post") {
            exportReportForPost(format);
        } else {
            exportReportForGet(format);
        }
    }

    function exportReportForPost(format) {
        <%
            String jsArray = "[]";
            if(request.getParameterMap().containsKey("__report_params")) {
               String[] paramListArray = request.getParameter("__report_params").toString().split(",");
                String jsArrayContent = "";
                for(int i=0; i< paramListArray.length; i++) {
                    if(i != 0) {
                        jsArrayContent = jsArrayContent + ",";
                    }
                    jsArrayContent = jsArrayContent + "['" + paramListArray[i] + "','" + request.getParameter(paramListArray[i]).toString() + "']";
                }
                jsArray = "[" + jsArrayContent + "]";
            }
        %>

        var reportParams = <%= jsArray %>;

        var form = document.createElement("form");
        form.method = "POST";
        form.action = document.URL;
        form.target = "_blank";

        for (i = 0; i < reportParams.length; i++) {
            var element = document.createElement("input");
            element.name = "" + reportParams[i][0];
            element.value = "" + reportParams[i][1];
            form.appendChild(element);
        }

        var elementFormat = document.createElement("input");
        elementFormat.name = "__format";
        elementFormat.value = format;
        form.appendChild(elementFormat);

        document.body.appendChild(form);
        form.submit();
    }

    function exportReportForGet(format) {
        var action = document.URL
        if (action.indexOf("&__format=html") < 0) {
            action = action + "&__format=" + format;
        } else {
            action = action.replace("&__format=html", ("&__format=" + format));
        }

        window.open(action, "_target");
    }
</script>

<TR 
	<%	
		String imagesPath = "birt/images/";
	
		if( attributeBean.isShowNavigationbar( ) )
		{
	%>
		HEIGHT="25px"
	<%
		}
		else
		{
	%>
		style="display:none"
	<%
		}
	%>	
>
	<TD>
		<DIV id="navigationBar">
			<TABLE CELLSPACING="0" CELLPADDING="0" WIDTH="100%" HEIGHT="25px" CLASS="birtviewer_navbar" style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
				<TR>
                   <TD>
                    <TABLE>
                        <TR
                                <%
                                    if (attributeBean.isShowToolbar()) {
                                %>
                                HEIGHT="20px"
                                <%
                                } else {
                                %>
                                style="display:none"
                                <%
                                    }
                                %>
                                >
                            <TD COLSPAN='2'>
                                <DIV ID="toolbar">
                                    <TABLE CELLSPACING="1px" CELLPADDING="1px" WIDTH="100%" CLASS="birtviewer_toolbar">
                                        <TR>
                                            <TD></TD>
                                        </TR>
                                        <TR>
                                            <TD WIDTH="6px" style="padding-left: 35px;"/>
                                            <TD WIDTH="15px">
                                                <INPUT TYPE="image" NAME='export' SRC="../images/excel_icon32x32.png"
                                                       TITLE="Export Excel Document"
                                                       ALT="<%= BirtResources.getHtmlMessage( "birt.viewer.toolbar.export" )%>"
                                                       CLASS="birtviewer_clickable" onclick="exportReport('xls')" style="width: 26px;">
                                            </TD>
                                            <TD WIDTH="6px"/>
                                            <TD WIDTH="15px" style="padding-left: 5px">
                                                <INPUT TYPE="image" NAME='exportReport' SRC="../images/pdf_icon32x32.gif"
                                                       TITLE="View PDF"
                                                       ALT="<%= BirtResources.getHtmlMessage( "birt.viewer.toolbar.exportreport" )%>"
                                                       CLASS="birtviewer_clickable" onclick="exportReport('pdf')" style="width: 26px;">
                                            </TD>
                                            <TD WIDTH="6px"/>
                                            <TD WIDTH="15px" style="padding-left: 10px">
                                                <INPUT TYPE="image" NAME='print' SRC="../images/print_icon32x32.png"
                                                       TITLE="Print Report"
                                                       ALT="<%= BirtResources.getHtmlMessage( "birt.viewer.toolbar.print" )%>"
                                                       CLASS="birtviewer_clickable" style="width: 26px;">
                                            </TD>
                                            <%
                                                if (ParameterAccessor.isSupportedPrintOnServer) {
                                            %>
                                            <TD WIDTH="6px"/>
                                            <TD WIDTH="15px">
                                                <INPUT TYPE="image" NAME='printServer' SRC="birt/images/PrintServer.gif"
                                                       TITLE="<%= BirtResources.getHtmlMessage( "birt.viewer.toolbar.printserver" )%>"
                                                       ALT="<%= BirtResources.getHtmlMessage( "birt.viewer.toolbar.printserver" )%>"
                                                       CLASS="birtviewer_clickable" style="display: none;">
                                            </TD>
                                            <%
                                                }
                                            %>
                                            <TD ALIGN='right'>
                                            </TD>
                                            <TD WIDTH="6px"/>
                                        </TR>
                                    </TABLE>
                                </DIV>
                            </TD>
                        </TR>
                    </TABLE>
                   </TD>
                   <TD>
                       <TABLE>
                           <TR>
                               <TD WIDTH="6px" style="padding-left: 20px;">&nbsp;</TD>

                               <TD WIDTH="100%" NOWRAP style="font-size: 12px">
                                   <B>
                                       <%
                                           if ( attributeBean.getBookmark( ) != null )
                                           {
                                       %>
                                       <%=
                                       BirtResources.getMessage( "birt.viewer.navbar.prompt.one" )
                                       %>&nbsp;
                                       <SPAN ID='pageNumber'></SPAN>&nbsp;
                                       <%= BirtResources.getMessage( "birt.viewer.navbar.prompt.two" )%>&nbsp;
                                       <SPAN ID='totalPage'></SPAN>
                                       <%
                                       }
                                       else
                                       {
                                       %>
                                       <%= BirtResources.getMessage( "birt.viewer.navbar.prompt.one" )%>&nbsp;
                                       <SPAN ID='pageNumber'><%= ""+attributeBean.getReportPage( ) %></SPAN>&nbsp;
                                       <%= BirtResources.getMessage( "birt.viewer.navbar.prompt.two" )%>&nbsp;
                                       <SPAN ID='totalPage'></SPAN>
                                       <%
                                           }
                                       %>
                                   </B>
                               </TD>
                               <TD WIDTH="15px">
                                   <INPUT TYPE="image" SRC="<%= imagesPath + (attributeBean.isRtl()?"LastPage":"FirstPage") + "_disabled.gif" %>" NAME='first'
                                          ALT="<%= BirtResources.getHtmlMessage( "birt.viewer.navbar.first" )%>"
                                          TITLE="<%= BirtResources.getHtmlMessage( "birt.viewer.navbar.first" )%>" CLASS="birtviewer_clickable nav_item">
                               </TD>
                               <TD WIDTH="2px">&nbsp;</TD>
                               <TD WIDTH="15px">
                                   <INPUT TYPE="image" SRC="<%= imagesPath + (attributeBean.isRtl()?"NextPage":"PreviousPage") + "_disabled.gif" %>" NAME='previous'
                                          ALT="<%= BirtResources.getHtmlMessage( "birt.viewer.navbar.previous" )%>"
                                          TITLE="<%= BirtResources.getHtmlMessage( "birt.viewer.navbar.previous" )%>" CLASS="birtviewer_clickable nav_item">
                               </TD>
                               <TD WIDTH="2px">&nbsp;</TD>
                               <TD WIDTH="15px">
                                   <INPUT TYPE="image" SRC="<%= imagesPath + (attributeBean.isRtl()?"PreviousPage":"NextPage") + "_disabled.gif" %>" NAME='next'
                                          ALT="<%= BirtResources.getHtmlMessage( "birt.viewer.navbar.next" )%>"
                                          TITLE="<%= BirtResources.getHtmlMessage( "birt.viewer.navbar.next" )%>" CLASS="birtviewer_clickable nav_item">
                               </TD>
                               <TD WIDTH="2px">&nbsp;</TD>
                               <TD WIDTH="15px">
                                   <INPUT TYPE="image" SRC="<%= imagesPath + (attributeBean.isRtl()?"FirstPage":"LastPage") + "_disabled.gif" %>" NAME='last'
                                          ALT="<%= BirtResources.getHtmlMessage( "birt.viewer.navbar.last" )%>"
                                          TITLE="<%= BirtResources.getHtmlMessage( "birt.viewer.navbar.last" )%>" CLASS="birtviewer_clickable nav_item">
                               </TD>

                               <TD WIDTH="8px">&nbsp;&nbsp;</TD>

                               <TD ALIGN="right" NOWRAP style="padding-top: 10px;"><LABEL for="gotoPage" style="font-size: 12px;"><b><%= BirtResources.getMessage( "birt.viewer.navbar.lable.goto" )%></b></LABEL></TD>
                               <TD WIDTH="2px">&nbsp;</TD>
                               <TD ALIGN="right" WIDTH="50px"  style="padding-top: 10px;">
                                   <INPUT ID='gotoPage' TYPE='text' VALUE='' MAXLENGTH="8" SIZE='5' CLASS="birtviewer_navbar_input">
                               </TD>
                               <TD WIDTH="4px">&nbsp;</TD>
                               <TD ALIGN="right" WIDTH="10px">
                                   <INPUT TYPE="image" SRC="<%= imagesPath + (attributeBean.isRtl()?"Go_rtl.gif":"Go.gif") %>" NAME='goto'
                                          ALT="<%= BirtResources.getHtmlMessage( "birt.viewer.navbar.goto" )%>"
                                          TITLE="<%= BirtResources.getHtmlMessage( "birt.viewer.navbar.goto" )%>" CLASS="birtviewer_clickable nav_item">
                               </TD>
                               <TD WIDTH="6px" style="padding-right: 60px;">&nbsp;</TD>
                           </TR>
                       </TABLE>
                   </TD>
				</TR>
			</TABLE>
		</DIV>
	</TD>
</TR>

<TR>
    <TD>
        <hr style="border-bottom:1px solid #d4d4d4; margin: 0px;"/>
    </TD>
</TR>


