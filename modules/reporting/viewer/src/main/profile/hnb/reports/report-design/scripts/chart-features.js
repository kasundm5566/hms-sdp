/*HNB Based Chart Palettes */

function getPaletteSet1() {
    var colors = new Array();
    colors[0] = [[18,114,224],[109,184,227]];   /*eca842  fef3af*/
    colors[1] = [[236,168,66],[236,128,66]];   /*08b8e0  a2d8e3*/
    colors[2] = [[134,18,224],[194,135,203]];  /*1272e0  6db8e3*/
    colors[3] = [[224,8,69],[227,190,165]];    /*e00845  e3bea5*/
    colors[4] = [[23,23,224],[124,121,227]];   /*1717e0  7c79e3*/
    colors[5] = [[41,224,102],[175,224,174]];  /*29e066  29e066*/
    colors[6] = [[89,13,224],[160,136,203]];   /*590de0  a088cb*/
    colors[7] = [[134,18,224],[194,135,203]];  /*8612e0  c287cb*/
    colors[8] = [[176,15,224],[203,146,202]];  /*b00fe0  cb92ca*/
    colors[9] = [[224,15,204],[203,166,203]];  /*e00fcc  cba6cb*/
    return colors;
}

function getPaletteSet2() {
    var colors = new Array();
    colors[0] = [[8,184,224],[162,216,227]];   /*08b8e0  a2d8e3*/
    colors[1] = [[224,8,69],[227,190,165]];    /*e00845  e3bea5*/
    colors[2] = [[18,114,224],[109,184,227]];  /*1272e0  6db8e3*/
    colors[3] = [[236,168,66],[254,243,175]];   /*eca842  fef3af*/
    colors[4] = [[23,23,224],[124,121,227]];   /*1717e0  7c79e3*/
    colors[5] = [[41,224,102],[175,224,174]];  /*29e066  29e066*/
    colors[6] = [[89,13,224],[160,136,203]];   /*590de0  a088cb*/
    colors[7] = [[134,18,224],[194,135,203]];  /*8612e0  c287cb*/
    colors[8] = [[176,15,224],[203,146,202]];  /*b00fe0  cb92ca*/
    colors[9] = [[224,15,204],[203,166,203]];  /*e00fcc  cba6cb*/
    return colors;
}

/*Available NCS are -
[0] SMS MO
[1] SMS MT
[2] USSD
[3] WAP Push
[4] CAS
[5] Subscription
[6] LBS
*/

function ncsChartVisibility() {
    var visibleNcs = [true, true, true, false, false, false, false];
    return visibleNcs;
}


/*Available Chart Series are -
 [0] - Total Revenue
 [1] - Total Traffic
 */

function revenueAndTrafficChartVisibility() {
    var visibleSeries = [false, true];
    return visibleSeries;
}