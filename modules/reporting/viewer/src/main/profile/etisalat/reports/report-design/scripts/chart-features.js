/*HNB Based Chart Palettes */

function getPaletteSet1() {
    var colors = new Array();
    colors[0] = [[224,140,50],[227,196,118]];  /*e08d32  e3c576*/
    colors[1] = [[239,72,122],[250,141,135]];  /*ef487b  fa8c87*/
    colors[2] = [[154,50,102],[187,126,157]];  /*9a3266  bb7e9c*/
    colors[3] = [[154,50,0],[202,167,87]];    /*9a3200  caa757*/
    colors[4] = [[233,15,80],[255,106,98]];    /*e90f52  ff6962*/
    colors[5] = [[128,128,192],[159,189,203]]; /*8080c0  9fbdcb*/
    colors[6] = [[170,85,85],[209,161,99]];    /*aa5555  d1a163*/
    colors[7] = [[128,128,0],[167,202,13]];    /*808000  a6ca0d*/
    colors[8] = [[192,192,192],[206,220,197]]; /*c0c0c0  cedcc5*/
    colors[9] = [[225,196,117],[255,255,128]]; /*e1c475  ffff80*/
    return colors;
}

function getPaletteSet2() {
    var colors = new Array();
    colors[0] = [[154,50,0],[202,167,87]];    /*9a3200  caa757*/
    colors[1] = [[239,72,122],[250,141,135]];  /*ef487b  fa8c87*/
    colors[2] = [[154,50,102],[187,126,157]];  /*9a3266  bb7e9c*/
    colors[3] = [[224,140,50],[227,196,118]];  /*e08d32  e3c576*/
    colors[4] = [[233,15,80],[255,106,98]];    /*e90f52  ff6962*/
    colors[5] = [[128,128,192],[159,189,203]]; /*8080c0  9fbdcb*/
    colors[6] = [[170,85,85],[209,161,99]];    /*aa5555  d1a163*/
    colors[7] = [[128,128,0],[167,202,13]];    /*808000  a6ca0d*/
    colors[8] = [[192,192,192],[206,220,197]]; /*c0c0c0  cedcc5*/
    colors[9] = [[225,196,117],[255,255,128]]; /*e1c475  ffff80*/
    return colors;
}

/*Available NCS are -
[0] SMS MO
[1] SMS MT
[2] USSD
[3] WAP Push
[4] CAS
[5] Subscription
[6] LBS
*/

function ncsChartVisibility() {
    var visibleNcs = [true, true, true, true, true, true, true];
    return visibleNcs;
}


/*Available Chart Series are -
 [0] - Total Revenue
 [1] - Total Traffic
 */

function revenueAndTrafficChartVisibility() {
    var visibleSeries = [true, true];
    return visibleSeries;
}