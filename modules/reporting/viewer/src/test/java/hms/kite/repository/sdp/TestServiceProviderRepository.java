package hms.kite.repository.sdp;

import hms.kite.domain.sdp.ServiceProviderAPPDomain;
import hms.kite.domain.sdp.ServiceProviderSPDomain;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */

@ContextConfiguration(locations = "classpath*:test-db.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class TestServiceProviderRepository {
    @Autowired
    ServiceProviderRepository serviceProviderRepository;

    @Test
    public void testGetAllServiceProvider() throws Exception {
        List<ServiceProviderSPDomain> listString = new ArrayList<ServiceProviderSPDomain>();
        listString = serviceProviderRepository.getAllServiceProviders();

//        for (String s : listString) {
//            System.out.println(s);
//        }
    }

    @Test
    public void testGetAllApps() throws Exception {
        List<ServiceProviderAPPDomain> stringList = new ArrayList<ServiceProviderAPPDomain>();
        stringList = serviceProviderRepository.getAllApps();

//        for (String s : stringList) {
//            System.out.println(s);
//        }
    }

//    @Test
//    public void testGetSpNamebySpId() {
//        String spName = serviceProviderRepository.getSPNameById("sp1");
//        System.out.println("======================");
//        System.out.println(spName);
//        System.out.println("======================");
//    }
}
