USE common_admin;

INSERT INTO `role` (id,description,name,module) VALUES (180,'Detailed transaction report (Customer Care)', 'ROLE_SDP_RPT_CC_DETAILED_TRANS',8);
INSERT INTO `role` (id,description,name,module) VALUES (181,'Failed Transaction report (Customer Care)', 'ROLE_SDP_RPT_CC_FAILED_TRANS',8);
INSERT INTO `role` (id,description,name,module) VALUES (200,'Operator Reports', 'ROLE_SDP_RPT_OPERATOR_REV_TRAFF',8);


INSERT INTO `user_group_roles` VALUES (1,180);
INSERT INTO `user_group_roles` VALUES (1,181);
INSERT INTO `user_group_roles` VALUES (3,180);
INSERT INTO `user_group_roles` VALUES (3,181);
INSERT INTO `user_group_roles` VALUES (5,180);
INSERT INTO `user_group_roles` VALUES (5,181);
INSERT INTO `user_group_roles` VALUES (1,200);