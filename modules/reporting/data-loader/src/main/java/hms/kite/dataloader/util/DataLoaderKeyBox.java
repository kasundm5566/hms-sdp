/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.util;

public class DataLoaderKeyBox {
    public static final String currentStatusK        = "current-status";
    public static final String appIdK                = "app-id";
    public static final String appNameK              = "app-name";
    public static final String spIdK                 = "sp-id";
    public static final String spNameK               = "coop-user-name";
    public static final String idK                   = "_id";
    public static final String totalSubscribersK     = "total-subscribers";
    public static final String dateK                 = "date";
    public static final String dateFormatK           = "yyyy-MM-dd";
    public static final String translogDateFormat    = "yyyy-MM-dd-hh-mm-ss";

    public static final String DateAppMK             = "dateId_app";
    public static final String spIdMK                = "sp_id";
    public static final String spNameMK              = "sp_name";
    public static final String appIdMK               = "app_id";
    public static final String appNameMK             = "app_name";
    public static final String newRegMK              = "new_reg";
    public static final String totalSubscribersMK    = "total_subscribers";


    public static final String currentStatus         = "REGISTERED";
    public static final String subscriptionCollectionName = "subscription";
    public static final String newLineCharactor      = "\n";
    public static final String subscriptionTranslog  = "subscription-translog.log.";

}