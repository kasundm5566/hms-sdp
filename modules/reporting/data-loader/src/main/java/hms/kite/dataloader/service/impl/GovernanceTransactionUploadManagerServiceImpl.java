/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.service.impl;

import hms.kite.dataloader.repository.impl.GovernanceTransactionRepositoryImpl;
import hms.kite.dataloader.repository.impl.LogInfoRepositoryImpl;
import hms.kite.dataloader.service.TransactionUploadManagerService;
import hms.kite.dataloader.util.FileHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.SQLException;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
public class GovernanceTransactionUploadManagerServiceImpl implements TransactionUploadManagerService {
    private String sourceDirectoryPath;
    private String fileNamePattern;
    private String archiveDirectoryPath;
    private String columnSeparator;
    private String rowBreaker;
    private String enclosedBy;
    private String columnAppender;
    private GovernanceTransactionRepositoryImpl governanceTransactionRepository;
    private LogInfoRepositoryImpl logInfoRepository;
    private static final String FILE_TYPE="governance";

    private FileHandler fileHandler;
    private final static Logger logger = LoggerFactory.getLogger(GovernanceTransactionUploadManagerServiceImpl.class);

    @Override
    public void upload() {
        logger.info("Governance messages upload scheduler started");
        fileHandler = new FileHandler();
        String[] files = fileHandler.getMatchingFiles(sourceDirectoryPath, fileNamePattern);
        if(files != null) {
            logger.info("Total of [{}] governance messages files found", files.length);
            for (String file : files) {
                String fullPath = sourceDirectoryPath+file;
                logger.info("Uploading governance messages file [{}]", file);
                try {
                    if(!logInfoRepository.isInsertedFile(file)){
                        insertIntoDataBase(fullPath);
                        logInfoRepository.insertFileInfo(file,FILE_TYPE);
                        logger.info("Successfully inserted the records to database in the file [{}]", fullPath);
                    }
                    else{
                        String processedTime =logInfoRepository.getProcessedTime(file);
                        logger.info("The file [{}] is already inserted into respective database on [{}]", file, processedTime);
                        archiveTheFile(file);
                        continue;
                    }
                    archiveTheFile(file);                    
                } catch (SQLException e) {
                    logger.error("DB Exception thrown while processing governance message files", e);
                    //todo:send SNMP trap
                } catch (Exception e) {
                    logger.error("Unexpected error occurred when inserting data from file [" + sourceDirectoryPath + file + "]", e);
                }
            }
            logger.info("Governance files upload scheduler completed");
        }
    }

    private void insertIntoDataBase(String filePath) throws SQLException, IOException {
        String fileContent=null;
        fileContent = fileHandler.getDataOfFile(filePath);
        if(fileContent != null) {
            String[] rows = new String[0];
            rows = fileContent.split(rowBreaker);
            for(String row : rows){
                try{
                    governanceTransactionRepository.save(row, columnSeparator);
                } catch (Exception e) {
                    logger.error("Couldn't save the record ["+row+"] Error while saving the record", e);
                }
            }
        }
    }

    private void archiveTheFile(String file) {
        logger.info("Archiving the file [ {}]", file);
        fileHandler.moveDataFile(sourceDirectoryPath + file, archiveDirectoryPath);
    }

    public void setSourceDirectoryPath(String sourceDirectoryPath) {
        if(!sourceDirectoryPath.endsWith("/")) {
            sourceDirectoryPath = sourceDirectoryPath + "/";
        }
        this.sourceDirectoryPath = sourceDirectoryPath;
    }

    public void setFileNamePattern(String fileNamePattern) {
        this.fileNamePattern = fileNamePattern;
    }

    public void setArchiveDirectoryPath(String archiveDirectoryPath) {
        this.archiveDirectoryPath = archiveDirectoryPath;
    }

    public void setColumnSeparator(String columnSeparator) {
        this.columnSeparator = columnSeparator;
    }

    public void setRowBreaker(String rowBreaker) {
        this.rowBreaker = rowBreaker;
    }

    public void setEnclosedBy(String enclosedBy) {
        this.enclosedBy = enclosedBy;
    }

    public void setGovernanceTransactionRepository(GovernanceTransactionRepositoryImpl governanceTransactionRepository) {
        this.governanceTransactionRepository = governanceTransactionRepository;
    }

    public void setColumnAppender(String columnAppender) {
        this.columnAppender = columnAppender;
    }

    public void setLogInfoRepository(LogInfoRepositoryImpl logInfoRepository) {
        this.logInfoRepository = logInfoRepository;
    }
}
