/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */

package hms.kite.dataloader.service.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import hms.kite.dataloader.repository.SubscriptionMongoDataRetrieve;
import hms.kite.dataloader.repository.impl.ServiceProviderInfoRepositoryImpl;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.io.*;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;

import static hms.kite.dataloader.util.DataLoaderKeyBox.*;

public class GenerateTotalSubscriptionTranslog implements SubscriptionMongoDataRetrieve {
    private MongoTemplate mongoTemplate;
    private final static Logger logger = LoggerFactory.getLogger(GenerateTotalSubscriptionTranslog.class);
    private String columnAppendar;
    private String translogDirectory;
    private ServiceProviderInfoRepositoryImpl serviceProviderInfoRepository;
    private String collectionName;

    @Override
    public void findRegisteredApps(String currentStatus) {
        logger.info("findRegisteredApps method invoked");
        HashMap<String, Object> queryCriteriaForAll = new HashMap<String, Object>();
        queryCriteriaForAll.put(currentStatusK, currentStatus);
        setDBojectsForRegisteredApps(queryCriteriaForAll);
    }

    private void setDBojectsForRegisteredApps(Map<String, Object> queryCriteriaForAll) {
        BasicDBObject queryForAll = new BasicDBObject();
        queryForAll.putAll(queryCriteriaForAll);
        generateTranslog(queryForAll);
    }

    private void generateTranslog(DBObject queryForAll) {
        try {
            List<String> terminatedApps = serviceProviderInfoRepository.getTerminatedApps();
            logger.info("Terminated app list [" + terminatedApps.toString() + "]");
            DBCollection appCollection = mongoTemplate.getCollection(getCollectionName());
            HashMap<String, Object> queryCriteriaByApp = new HashMap<String, Object>();
            List<String> distinctAllApps = appCollection.distinct(appIdK, queryForAll);
            String date = getDate();
            String translogFileName = createTranslogFilename();
            logger.info("Total subscription details write into file : " + translogFileName);
            for (String app : distinctAllApps) {
                if (!terminatedApps.contains(app)) {
                    queryCriteriaByApp.put(appIdK, app);
                    queryForAll.putAll(queryCriteriaByApp);
                    DBObject fields = getFeilds();
                    DBObject appDetails = appCollection.findOne(queryForAll, fields);
                    appDetails.put(totalSubscribersK, (int) appCollection.count(queryForAll));
                    appDetails.put(dateK, date);
                    appDetails.removeField(idK);
                    String appRow = setTranslogString(appDetails);
                    writeIntoTranslog(appRow, translogFileName);
                } else {
                    logger.info("Selected Application [" + app + "] is a terminated application.");
                }
            }
            logger.info("Finished writing total subscription details into file : " + translogFileName);
        } catch (Exception e) {
            logger.info("Could not receive the app details due to: [{}]", e);
        }
    }

    private void writeIntoTranslog(String appRow, String filename) throws IOException {

        File translog = new File(translogDirectory + filename);

        if (!translog.exists()) {
            translog.createNewFile();
        }
        if (!translog.canWrite()) {
            translog.setWritable(true);
        }
        BufferedWriter translogWriter = new BufferedWriter(new FileWriter(translog.getAbsolutePath(), true));
        translogWriter.write(appRow + newLineCharactor);
        translogWriter.flush();
    }

    private String createTranslogFilename() {
        Date date = new DateTime().toDate();
        Format simpleDateFormat = new SimpleDateFormat(translogDateFormat);
        String fileDate = simpleDateFormat.format(date);
        return subscriptionTranslog + fileDate;
    }

    private DBObject getFeilds() {
        BasicDBObject fields = new BasicDBObject();
        fields.put(appIdK, 1);
        fields.put(appNameK, 1);
        fields.put(spIdK, 1);
        fields.put(spNameK, 1);
        return fields;
    }

    private String getDate() {
        Date dateUnformatted = new DateTime().minusDays(1).toDateTime().toDate();

        Format formatter = new SimpleDateFormat(dateFormatK);

        return formatter.format(dateUnformatted);
    }

    private String setTranslogString(DBObject appDetails) {
        String appRow = appDetails.get(dateK) + columnAppendar + appDetails.get(spIdK) + columnAppendar + appDetails.get(spNameK) + columnAppendar + appDetails.get(appIdK) +
                columnAppendar + appDetails.get(appNameK) + columnAppendar + appDetails.get(totalSubscribersK) + columnAppendar;
        return appRow;
    }


    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public void setColumnAppendar(String columnAppendar) {
        this.columnAppendar = columnAppendar;
    }

    public void setTranslogDirectory(String translogDirectory) {
        if (!translogDirectory.endsWith("/")) {
            translogDirectory = translogDirectory + "/";
            this.translogDirectory = translogDirectory;
        } else
            this.translogDirectory = translogDirectory;
    }

    public void setServiceProviderInfoRepository(ServiceProviderInfoRepositoryImpl serviceProviderInfoRepository) {
        this.serviceProviderInfoRepository = serviceProviderInfoRepository;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }
}
