/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.util;

import hms.kite.dataloader.domain.Amount;
import hms.kite.dataloader.domain.Currency;
import hms.kite.dataloader.domain.LineItem;
import hms.kite.dataloader.domain.PGWTranslogEntry;
import hms.kite.dataloader.repository.TransactionRepository;
import hms.kite.dataloader.service.JsonDecoderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PgwTransactionRecordProcessorThread implements Runnable{

    private static final String NEWLINE_CHARACTER = "\n";

    private String record;
    private String columnSeparator;
    private String columnSeparatorAppender;
    private List<String> lineItemKeys;
    private List<Integer> currencyKeyPositions;
    private int lineItemIndex;
    private int currencyJsonIndex;
    private JsonDecoderService jsonDecoderService;
    private TransactionRepository transactionRepository;
    private PGWTranslogEntry pgwTranslogEntry;

    private static final Logger logger = LoggerFactory.getLogger(PgwTransactionRecordProcessorThread.class);

    public PgwTransactionRecordProcessorThread(String record, String columnSeparator,
                                               String columnSeparatorAppender, List lineItemKeys,
                                               int lineItemIndex, int currencyJsonIndex,
                                               JsonDecoderService jsonDecoderService,
                                               TransactionRepository transactionRepository) {
        if (record.endsWith(NEWLINE_CHARACTER)) {
            record = record.substring(0, record.length() - 1);
        }
        this.record = record;
        this.columnSeparator = columnSeparator;
        this.columnSeparatorAppender = columnSeparatorAppender;
        this.lineItemKeys = lineItemKeys;
        this.lineItemIndex = lineItemIndex;
        this.currencyJsonIndex = currencyJsonIndex;
        this.jsonDecoderService = jsonDecoderService;
        this.transactionRepository = transactionRepository;
        currencyKeyPositions = new ArrayList<Integer>();
        currencyKeyPositions.clear();
        currencyKeyPositions.add(8);    // requestCurrency
        currencyKeyPositions.add(15);   // fromCurrency
        currencyKeyPositions.add(20);   // toCurrency
        currencyKeyPositions.add(28);   // lineItem CurrencyCode for first Key
        currencyKeyPositions.add(30);   // lineItem CurrencyCode for second Key
    }

    public void run() {
        //record = record + "|*";
        String[]   columns =  record.split(columnSeparator,1000);
//        Map<String, LineItem> lineItemMap = jsonDecoderService.decodeLineItem( columns[23]);
//        Map<String, Currency> currencyMap = jsonDecoderService.decodeCurrency( getCurrencyJsonObj(record, columnSeparator, currencyJsonIndex));
//        Map<String, Currency> currencyMap = jsonDecoderService.decodeCurrency(columns[23]);

        pgwTranslogEntry = new PGWTranslogEntry(columns);
//        appendLineItemToRecord(lineItemMap);
//        appendCurrencyToRecord(currencyMap);
        try {
            transactionRepository.save(pgwTranslogEntry);
        } catch (Exception e) {
            //todo: Send SNMP trap
            logger.error("Couldn't save the record [" + record + "] Error while saving the record", e);
        }
    }

    private void appendLineItemToRecord(Map<String, LineItem> lineItemMap) {
        for (String lineItemKey : lineItemKeys) {
            if (lineItemMap == null) {
                appendStringToRecord("0");
                appendStringToRecord("");
            } else {
                if (lineItemMap.containsKey(lineItemKey)) {
                    Amount amount = lineItemMap.get(lineItemKey).getAmount();
                    appendStringToRecord(String.valueOf(amount.getAmountValue()));
                    appendStringToRecord(amount.getCurrencyCode());
                } else {
                    appendStringToRecord("0");
                    appendStringToRecord("");   //line item contains different key
                }
            }
        }
    }

    private void appendCurrencyToRecord(Map<String, Currency> currencyMap) {
        String[] columnValues = record.split(columnSeparator);
        for (int currencyKeyPosition : currencyKeyPositions) {
            if (currencyMap == null) {
                appendStringToRecord("0");
                appendStringToRecord("0");
            } else {
                if (currencyMap.containsKey(columnValues[currencyKeyPosition - 1])) {
                    Currency currency = currencyMap.get(columnValues[currencyKeyPosition - 1]);
                    appendStringToRecord(String.valueOf(currency.getBuyingRate()));
                    appendStringToRecord(String.valueOf(currency.getSellingRate()));
                } else {
                    appendStringToRecord("0");
                    appendStringToRecord("0");
                }
            }
        }
    }

    private void appendStringToRecord(String appendValue) {
        record = record + columnSeparatorAppender + appendValue;
    }

    public String getCurrencyJsonObj(String record, String separator, int currencyJsonIndex) {
        String[] arrString = record.split(separator);
        return arrString[currencyJsonIndex - 1];
    }
}
