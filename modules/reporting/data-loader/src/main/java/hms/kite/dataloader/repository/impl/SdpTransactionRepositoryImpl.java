/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.repository.impl;

import hms.kite.dataloader.domain.SdpTranslogEntry;
import hms.kite.dataloader.domain.TranslogEntry;
import hms.kite.dataloader.repository.TransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.SQLException;

public class SdpTransactionRepositoryImpl implements TransactionRepository {

    private JdbcTemplate jdbcTemplate;

    private static final Logger logger = LoggerFactory.getLogger(SdpTransactionRepositoryImpl.class);


    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void save(String record, String columnSeparator) throws SQLException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void save(TranslogEntry translogEntry) throws SQLException {
        SdpTranslogEntry e = (SdpTranslogEntry) translogEntry;
        jdbcTemplate.update("insert into `sdp_transaction` (" +
                "correlation_id, time_stamp, sp_id, sp_name, app_id, app_name, app_state, source_address, " +
                "masked_source_address, source_channel_type, source_channel_protocol, destination_address, masked_destination_address, destination_channel_type," +
                " destination_channel_protocol, direction, ncs, billing_type, charge_party_type, charge_amount, sys_cur_charge_amount, charge_amount_currency," +
                " charge_amount_buying_rate, charge_amount_selling_rate, rate_card, charge_category, charge_address, masked_charge_address, event_type," +
                " response_code, response_description, transaction_status, keyword, short_code, operator, sys_cur_operator_cost, revenue_share_type, " +
                "revenue_share_percentage, sys_cur_sp_profit, sys_cur_vc_profit, advertisement_name, advertisement_content, is_multiple_recipients, overall_response_code, " +
                "overall_response_description, pgw_transaction_id, charging_type, is_number_masked, order_no, invoice_no," +
                " external_trx_id, session_id, ussd_operation, balance_due," +
                " total_amount, file_type, payment_instrument)values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
                "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
                "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                e.correlationId, e.timeStamp, e.spId, e.spName, e.appId,
                e.appName, e.appState, e.sourceAddress, e.maskedSourceAddress, e.sourceChannelType,
                e.sourceChannelProtocol, e.destinationAddress, e.maskedDestinationAddress, e.destinationChannelType, e.destinationChannelProtocol,
                e.direction, e.ncs, e.billingType, e.chargePartyType, e.chargeAmount, e.sysCurChargeAmount, e.chargeAmountCurrency,
                e.chargeAmountBuyingRate, e.chargeAmountSellingRate, e.rateCard, e.chargeCategory, e.chargeAddress,
                e.maskedChargeAddress, e.eventType, e.responseCode, e.responseDescription, e.transactionStatus,
                e.keyword, e.shortCode, e.operator, e.sysCurOperatorCost, e.revenueShareType, e.revenueSharePercentage,
                e.sysCurSpProfit, e.sysCurVcProfit, e.advertisementName, e.advertisementContent, e.isMultipleRecipients,
                e.responseCode, e.overallResponseDescription, e.pgwTransactionId, e.chargingType, e.isNumberMasked,
                e.orderNo, e.invoiceNo, e.externalTrxId, e.sessionId, e.ussdOperation,
                e.balanceDue, e.totalAmount, e.fileType, e.paymentInstrument);

        if (e.ncs.equals("subscription")){
            if(e.direction.equals("mo")){
                if(e.eventType.equals("freeRegistration")||e.eventType.equals("registrationCharging")){
                    jdbcTemplate.update("INSERT INTO subscription_info (time_stamp, app_id, sp_id, msisdn, subscription_status)"+
                            "VALUES (?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE subscription_status = ? ",e.timeStamp, e.appId, e.spId, e.sourceAddress, "REGISTERED", "REGISTERED" );
                }else if (e.eventType.equals("unregistration")){
                    jdbcTemplate.update("INSERT INTO subscription_info (time_stamp, app_id, sp_id, msisdn, subscription_status)"+
                            "VALUES (?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE subscription_status = ? ",e.timeStamp, e.appId, e.spId, e.sourceAddress, "UNREGISTERED", "UNREGISTERED" );
                }

            }else{
                if(e.eventType.equals("freeRegistration")||e.eventType.equals("registrationCharging")){
                    jdbcTemplate.update("INSERT INTO subscription_info (time_stamp, app_id, sp_id, msisdn, subscription_status)"+
                            "VALUES (?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE subscription_status = ? ",e.timeStamp, e.appId, e.spId, e.destinationAddress, "REGISTERED", "REGISTERED" );
                }else if (e.eventType.equals("unregistration")){
                    jdbcTemplate.update("INSERT INTO subscription_info (time_stamp, app_id, sp_id, msisdn, subscription_status)"+
                            "VALUES (?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE subscription_status = ? ",e.timeStamp, e.appId, e.spId, e.destinationAddress, "UNREGISTERED", "UNREGISTERED" );
                }
            }

        }
    }
}
