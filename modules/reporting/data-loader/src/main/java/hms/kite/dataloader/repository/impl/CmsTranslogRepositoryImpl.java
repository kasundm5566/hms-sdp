/*
 * (C) Copyright 2012-2014 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.dataloader.repository.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.MongoException;
import hms.kite.dataloader.domain.CmsTranslogEntry;
import hms.kite.dataloader.domain.ServiceProviderInfo;
import hms.kite.dataloader.domain.TranslogEntry;
import hms.kite.dataloader.repository.TransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CmsTranslogRepositoryImpl implements TransactionRepository {

    private JdbcTemplate jdbcTemplate;
    private MongoTemplate mongoTemplate;
    private String collectionName;
    private String responseDescriptionFromLog;
    private static final Logger logger = LoggerFactory.getLogger(CmsTranslogRepositoryImpl.class);

    @Override
    public void save(String record, String columnSeparator) throws SQLException {
    }

    @Override
    public void save(TranslogEntry translogEntry) throws SQLException {
        CmsTranslogEntry e = (CmsTranslogEntry) translogEntry;
        jdbcTemplate.update("INSERT INTO `sdp_transaction` (correlation_id, time_stamp, " +
                        "sp_id, sp_name, app_id, app_name, direction, ncs, charge_amount, sys_cur_charge_amount, " +
                        "charge_amount_currency, charging_type, response_code, response_description, transaction_status, " +
                        "revenue_share_percentage, sys_cur_sp_profit, sys_cur_vc_profit, file_type, payment_instrument, destination_address) " +
                        "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                e.correlationId, e.timeStamp, e.spId, e.spName, e.appId, e.appName, e.direction, e.ncs,
                e.chargeAmount, e.sysCurChargeAmount, e.chargeAmountCurrency, e.chargingType, e.responseCode,
                e.responseDescription, e.transactionStatus, e.revenueSharePercentage, e.sysCurSpProfit,
                e.sysCurVcProfit, e.fileType, e.paymentInstrument, e.msisdn);
    }

    public ServiceProviderInfo getSpDetail(String appId, String spId) {

        return jdbcTemplate.queryForObject("SELECT `sp_name`, `app_name`, `revenue_share_percentage` FROM `service_provider_info` WHERE app_id = ? AND sp_id = ?",
                new Object[]{appId, spId}, new RowMapper<ServiceProviderInfo>() {
                    public ServiceProviderInfo mapRow(ResultSet resultSet, int i) throws SQLException {
                        ServiceProviderInfo serviceProviderInfo = new ServiceProviderInfo();
                        serviceProviderInfo.setAppName(resultSet.getString("app_name"));
                        serviceProviderInfo.setSpName(resultSet.getString("sp_name"));
                        serviceProviderInfo.setRevenueSharePercentage(resultSet.getString("revenue_share_percentage"));
                        return serviceProviderInfo;
                    }
                });
    }

    public void saveMongo(TranslogEntry translogEntry) throws MongoException {

        CmsTranslogEntry e = (CmsTranslogEntry) translogEntry;
        DBCollection downloadApps = mongoTemplate.getCollection(collectionName);
        if (e.responseDescription.equals(responseDescriptionFromLog)) {
            String id = e.msisdn + "_" + e.appId;
            BasicDBObject doc = new BasicDBObject("title", "MongoDB").
                    append("_id", id).
                    append("appId", e.appId).
                    append("msisdn", e.msisdn).
                    append("spId", e.spId).
                    append("requestedDateTime", e.timeStamp).
                    append("responseDescription", e.responseDescription);
            downloadApps.save(doc);
        }

    }

    public void setResponseDescriptionFromLog(String responseDescriptionFromLog) {
        this.responseDescriptionFromLog = responseDescriptionFromLog;
    }

    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }


}
