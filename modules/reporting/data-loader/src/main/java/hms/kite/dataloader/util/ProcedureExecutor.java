/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.util;

import hms.kite.dataloader.domain.SummaryDataStatus;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import javax.sql.DataSource;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
public class ProcedureExecutor extends StoredProcedure {

    public ProcedureExecutor(DataSource dataSource, String storedProcedureName) {
        super(dataSource, storedProcedureName);
        declareParameter(new SqlParameter(SummaryDataStatus.REQUESTED_DATE, Types.DATE));
        //declareParameter(new SqlParameter(SummaryDataStatus.PROCEDURE_NAME, Types.VARCHAR));
        compile();
    }

    public void executeSp(HashMap map) {
        Map outParams = super.execute(map);
    }
}
