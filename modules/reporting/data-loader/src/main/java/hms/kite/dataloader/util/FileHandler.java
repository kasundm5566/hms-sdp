/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.dataloader.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;


public class FileHandler {

    private static final Logger logger = LoggerFactory.getLogger(FileHandler.class);
    public String[] getMatchingFiles(String filePath, final String fileNamePattern) {
        File dir = new File(filePath);
        FilenameFilter filter = new FilenameFilter() {

            public boolean accept(File dir, String name) {
                return name.matches(fileNamePattern);

            }
        };

        return dir.list(filter);
    }

    public boolean moveDataFile(String filePath, String archiveDirectoryPath) {
        File file = new File(filePath);

        File dir = new File(archiveDirectoryPath);
        return file.renameTo(new File(dir, file.getName()));
    }


    private static final String NEW_LINE_CHARACTER = "\n";

    public String getDataOfFile(String filePath) throws IOException {
        String data = "";
        FileInputStream fileInputStream = new FileInputStream(new File(filePath));
        DataInputStream dataInputStream = new DataInputStream(fileInputStream);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(dataInputStream));
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            data = data + line + NEW_LINE_CHARACTER;
        }
        if (data.equalsIgnoreCase("")) {
            return null;
        }
        return data;
    }

}
