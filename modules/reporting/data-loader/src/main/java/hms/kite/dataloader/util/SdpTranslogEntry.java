/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.domain;

public class SdpTranslogEntry extends TranslogEntry {

    public String correlationId;
    public String timeStamp;
    public String spId;
    public String spName;
    public String appId;
    public String appName;
    public String appState;
    public String sourceAddress;
    public String maskedSourceAddress;
    public String sourceChannelType;
    public String sourceChannelProtocol;
    public String destinationAddress;
    public String maskedDestinationAddress;
    public String destinationChannelType;
    public String destinationChannelProtocol;
    public String direction;
    public String ncs;
    public String billingType;
    public String chargePartyType;
    public double chargeAmount;
    public double sysCurChargeAmount;
    public String chargeAmountCurrency;
    public double chargeAmountBuyingRate;
    public double chargeAmountSellingRate;
    public String rateCard;
    public String chargeCategory;
    public String chargeAddress;
    public String maskedChargeAddress;
    public String eventType;
    public String responseCode;
    public String responseDescription;
    public String transactionStatus;
    public String keyword;
    public String shortCode;
    public String operator;
    public double sysCurOperatorCost;
    public String revenueShareType;
    public double revenueSharePercentage;
    public String advertisementName;
    public String advertisementContent;
    public String isMultipleRecipients;
    public String overallResponseCode;
    public String overallResponseDescription;
    public String pgwTransactionId;
    public String chargingType;
    public String isNumberMasked;
    public String orderNo;
    public String invoiceNo;
    public String externalTrxId;
    public String sessionId;
    public String ussdOperation;
    public double balanceDue;
    public double totalAmount;
    public double buyingRate;
    public double sellingRate;
    public double sysCurSpProfit;
    public double sysCurVcProfit;
    public String fileType;
    public String paymentInstrument;

    public SdpTranslogEntry(String[] columns) {
        if (columns.length > 0) {
            correlationId = columns[0];
            timeStamp = filterTimeStamp(columns[1]);
            spId = columns[2];
            spName = columns[3];
            appId = columns[4];
            appName = columns[5];
            appState = columns[6];
            sourceAddress = columns[7];
            maskedSourceAddress = columns[8];
            sourceChannelType = columns[9];
            sourceChannelProtocol = columns[10];
            destinationAddress = columns[11];
            maskedDestinationAddress = columns[12];
            destinationChannelType = columns[13];
            destinationChannelProtocol = columns[14];
            direction = columns[15];
            ncs = columns[16];
            billingType = columns[17];
            chargePartyType = columns[18];
            chargeAmount =  convertToDouble(columns[19]);
            chargeAmountCurrency = columns[20];
            rateCard = columns[21];
            chargeCategory = columns[22];
            chargeAddress = columns[23];
            maskedChargeAddress = columns[24];
            eventType = columns[25];
            responseCode = columns[26];
            responseDescription = columns[27];
            transactionStatus = columns[28];
            keyword = columns[29];
            shortCode = columns[30];
            operator = columns[31];
            sysCurOperatorCost =convertToDouble(columns[32]);
            revenueShareType = columns[33];
            revenueSharePercentage = convertToDouble(columns[34]);
            advertisementName = columns[35];
            advertisementContent = columns[36];
            isMultipleRecipients = checkNull(columns[37]);
            overallResponseCode = columns[38];
            overallResponseDescription = columns[39];
            pgwTransactionId = columns[40];
            chargingType = columns[41];
            isNumberMasked = checkNull(columns[42]);
            orderNo = columns[43];
            invoiceNo = columns[44];
            externalTrxId = columns[45];
            sessionId = columns[46];
            ussdOperation = columns[47];
            balanceDue = convertToDouble(columns[48]);
            totalAmount = convertToDouble(columns[49]);
            paymentInstrument = columns[57];
        }
    }

    private String checkNull(String var) {
        return (var.equals("")) ? null : var;
    }

    private String filterTimeStamp(String timeStamp) {
        if (timeStamp != null && timeStamp.length() > 19) {
            return timeStamp.substring(0, 19);
        } else {
            return null;
        }
    }

    private double convertToDouble(String val) {
        return (!val.equals("")) ? Double.parseDouble(val) : 0.0;
    }

    public String getSourceAddress() {
        return sourceAddress;
    }

    public void setSourceAddress(String sourceAddress) {
        this.sourceAddress = sourceAddress;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public double getChargeAmount() {
        return chargeAmount;
    }

    public void setSysCurChargeAmount(double sysCurChargeAmount) {
        this.sysCurChargeAmount = sysCurChargeAmount;
    }

    public double getSysCurOperatorCost() {
        return sysCurOperatorCost;
    }

    public void setSysCurOperatorCost(double sysCurOperatorCost) {
        this.sysCurOperatorCost = sysCurOperatorCost;
    }

    public String getRevenueShareType() {
        return revenueShareType;
    }

    public void setRevenueShareType(String revenueShareType) {
        this.revenueShareType = revenueShareType;
    }

    public double getRevenueSharePercentage() {
        return revenueSharePercentage;
    }

    public void setRevenueSharePercentage(double revenueSharePercentage) {
        this.revenueSharePercentage = revenueSharePercentage;
    }

    public double getSysCurSpProfit() {
        return sysCurSpProfit;
    }

    public void setSysCurSpProfit(double sysCurSpProfit) {
        this.sysCurSpProfit = sysCurSpProfit;
    }

    public double getSysCurVcProfit() {
        return sysCurVcProfit;
    }

    public void setSysCurVcProfit(double sysCurVcProfit) {
        this.sysCurVcProfit = sysCurVcProfit;
    }

    public String getAdvertisementName() {
        return advertisementName;
    }

    public void setAdvertisementName(String advertisementName) {
        this.advertisementName = advertisementName;
    }

    public String getAdvertisementContent() {
        return advertisementContent;
    }

    public void setAdvertisementContent(String advertisementContent) {
        this.advertisementContent = advertisementContent;
    }

    public String getMultipleRecipients() {
        return isMultipleRecipients;
    }

    public void setMultipleRecipients(String multipleRecipients) {
        isMultipleRecipients = multipleRecipients;
    }

    public String getOverallResponseCode() {
        return overallResponseCode;
    }

    public void setOverallResponseCode(String overallResponseCode) {
        this.overallResponseCode = overallResponseCode;
    }

    public String getOverallResponseDescription() {
        return overallResponseDescription;
    }

    public void setOverallResponseDescription(String overallResponseDescription) {
        this.overallResponseDescription = overallResponseDescription;
    }

    public String getPgwTransactionId() {
        return pgwTransactionId;
    }

    public void setPgwTransactionId(String pgwTransactionId) {
        this.pgwTransactionId = pgwTransactionId;
    }

    public String getChargingType() {
        return chargingType;
    }

    public void setChargingType(String chargingType) {
        this.chargingType = chargingType;
    }

    public String getNumberMasked() {
        return isNumberMasked;
    }

    public void setNumberMasked(String numberMasked) {
        isNumberMasked = numberMasked;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getExternalTrxId() {
        return externalTrxId;
    }

    public void setExternalTrxId(String externalTrxId) {
        this.externalTrxId = externalTrxId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUssdOperation() {
        return ussdOperation;
    }

    public void setUssdOperation(String ussdOperation) {
        this.ussdOperation = ussdOperation;
    }

    public double getBuyingRate() {
        return buyingRate;
    }

    public void setBuyingRate(double buyingRate) {
        this.buyingRate = buyingRate;
    }

    public double getSellingRate() {
        return sellingRate;
    }

    public void setSellingRate(double sellingRate) {
        this.sellingRate = sellingRate;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
}
