/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.domain;


public class LineItem {

    public static final String PAYMENT_INSTRUMENT_ID = "paymentInstrumentId";
    public static final String ACCOUNT_ID = "accountId";
    public static final String OPERATION_TYPE = "operationType";
    public static final String AMOUNT = "amount";
    public static final String AMOUNT_VALUE = "amountValue";
    public static final String CURRENCY_CODE = "currencyCode";
    public static final String ORIGINATED_RULE_ID = "originatedRuleId";
    public static final String EXECUTE_ON_SYS_ACCOUNT = "executeOnSysAccount";

    private int paymentInstrumentId;
    private String accountId;
    private String operationType;
    private Amount amount;
    private String originatedRuleId;
    private boolean executeOnSysAccount;
    private String ownerType;
    private String type;

    public int getPaymentInstrumentId() {
        return paymentInstrumentId;
    }

    public String getAccountId() {
        return accountId;
    }

    public String getOperationType() {
        return operationType;
    }

    public Amount getAmount() {
        return amount;
    }

    public String getOriginatedRuleId() {
        return originatedRuleId;
    }

    public boolean isExecuteOnSysAccount() {
        return executeOnSysAccount;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public String getType() {
        return type;
    }
}