/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.dataloader.boot;

import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.slf4j.Logger;

import java.io.IOException;

public class DataLoaderServer {

    private final static Logger logger = LoggerFactory.getLogger(DataLoaderServer.class);

     public static void main(String[] args) throws IOException {
        ApplicationContext context = new ClassPathXmlApplicationContext("data-loader-context.xml");
        logger.info("####################################################################");
        logger.info("##    mChoice Kite Data Loader System Started Successfully        ##");
        logger.info("####################################################################");
    }
}
