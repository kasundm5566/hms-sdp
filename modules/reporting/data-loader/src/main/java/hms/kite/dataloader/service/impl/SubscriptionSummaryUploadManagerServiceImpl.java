/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */

package hms.kite.dataloader.service.impl;

import hms.kite.dataloader.repository.DateInfoRepository;
import hms.kite.dataloader.repository.TransactionRepository;
import hms.kite.dataloader.service.SubscriptionSummaryUploadManagerService;
import hms.kite.dataloader.util.FileHandler;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import static hms.kite.dataloader.util.DataLoaderKeyBox.*;

public class SubscriptionSummaryUploadManagerServiceImpl implements SubscriptionSummaryUploadManagerService {

    private static final Logger logger  = LoggerFactory.getLogger(SubscriptionSummaryUploadManagerServiceImpl.class);

    private String rowBreaker;
    private String dateSeparator;
    private String columnSeparator;
    private String columnAppender;
    private String keyAppender;

    private TransactionRepository transactionRepository;
    private DateInfoRepository dateInfoRepository;

    private FileHandler fileHandler;

    public void insertNewSubscriptionsCount(String filePath) throws SQLException, IOException{
        fileHandler = new FileHandler();
        String fileContent = fileHandler.getDataOfFile(filePath);
        String[] rows = new String[0];

        if(fileContent != null) {
            rows = fileContent.split(rowBreaker);
            List<Map<String, Object>> preparedApps = prepareAppDetails(rows);
            Set <Map <String, Object>> newlySubscribedApps = findNewSubscribers(preparedApps);

            for (Map<String, Object> newlysubscribedApp : newlySubscribedApps  ) {
                String[] filterKey = newlysubscribedApp.get(DateAppMK).toString().split("/");

                String appRow = filterKey[0] + columnAppender + newlysubscribedApp.get(spIdMK) + columnAppender+ newlysubscribedApp.get(spNameMK) + columnAppender + filterKey[1] +
                        columnAppender + newlysubscribedApp.get(appNameMK) + columnAppender + newlysubscribedApp.get(newRegMK) + columnAppender + "0" + columnAppender;
                try {
                    transactionRepository.save(appRow, columnAppender);
                    logger.info("Successfully inserted the records of new subscribers from file to database [{}]", filePath);

                }
                catch (Exception e) {
                    logger.error("Couldn't save the record ["+appRow+"] Error while saving the record", e);
                }
            }

        }
    }
    private List<Map<String, Object>> prepareAppDetails(String[] rows) throws SQLException {
        List<Map<String, Object>> apps = new ArrayList<Map<String, Object>>();
        Map<String,Object> appPerDate;
        String[] columnValues;
        for(String row : rows) {
            appPerDate = new HashMap<String, Object>();
            columnValues = row.split(columnSeparator);
            String eventType = columnValues[25];
            if (eventType.matches("registrationCharging") ||eventType.matches("freeRegistration")) {
                String appId = columnValues[4];
                String dateId = getDateId((columnValues[1].toString().split(" "))[0]);
                appPerDate.put(DateAppMK,dateId+keyAppender+ appId);
                appPerDate.put(spIdMK,columnValues[2]);
                appPerDate.put(spNameMK,columnValues[3]);
                appPerDate.put(appNameMK,columnValues[5]);
                apps.add(appPerDate);
            }
        }
        return apps;

    }

    private String getDateId(String dateStr) throws SQLException {
        String[] date = dateStr.split(dateSeparator);
        DateTime dateTime = new DateTime(Integer.parseInt(date[0]), Integer.parseInt(date[1]), Integer.parseInt(date[2]),0, 0, 0, 0);
        return dateInfoRepository.getDateId(dateTime);
    }

    private Set<Map<String, Object>> findNewSubscribers(List<Map<String, Object>> preparedApps) {
        Set<Map<String, Object>> subscribedApps = new HashSet();
        Map<String, Object> subscribedApp;
        for(Map<String, Object> preparedApp : preparedApps) {
            subscribedApp = new HashMap<String, Object>();
            int newSubscriptions = Collections.frequency(preparedApps , preparedApp);
            subscribedApp.putAll(preparedApp);
            subscribedApp.put(newRegMK, newSubscriptions);
            subscribedApps.add(subscribedApp);
        }
        return subscribedApps;
    }

    public void setRowBreaker(String rowBreaker) {
        this.rowBreaker = rowBreaker;
    }

    public void setColumnSeparator(String columnSeparator) {
        this.columnSeparator = columnSeparator;
    }
    public void setTransactionRepository(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public void setDateInfoRepository(DateInfoRepository dateInfoRepository) {
        this.dateInfoRepository = dateInfoRepository;
    }

    public void setDateSeparator(String dateSeparator) {
        this.dateSeparator = dateSeparator;
    }
    public void setColumnAppender(String columnAppender) {
        this.columnAppender = columnAppender;
    }

    public void setKeyAppender(String keyAppender) {
        this.keyAppender = keyAppender;
    }

}
