/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.repository.impl;

import hms.kite.dataloader.domain.PGWTranslogEntry;
import hms.kite.dataloader.domain.TranslogEntry;
import hms.kite.dataloader.repository.TransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.SQLException;

public class PgwTransactionRepositoryImpl implements TransactionRepository {

    private JdbcTemplate jdbcTemplate;

    private static final Logger logger = LoggerFactory.getLogger(PgwTransactionRepositoryImpl.class);

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void save(String record, String columnSeparator) throws SQLException {
    }

    @Override
    public void save(TranslogEntry entry) throws SQLException {
        PGWTranslogEntry e= (PGWTranslogEntry) entry;
        jdbcTemplate.update("insert into `pgw_transaction`(" +
                "tx_id, tx_type, timeStamp, channel, tx_status, tx_status_details, client_tx_id, request_amount, request_currency," +
                "tax_type, tax_percentage, tax_amount, event_type, paid_amount, pending_amount," +
                " service_charge_type, service_charge_percentage, service_charge_amount, merchant_id, merchant_name, user_name, line_items," +
                "payment_instrument, payment_instrument_account, category, invoice_no, order_no, rate_card, reference_id,total_paid_amount, msisdn, paybill_no)" +
                " values (" +
                "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
                "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                e.transactionId,e.transactionType,e.timeStamp,e.channel,e.transactionStatus,
                e.transactionStatusDetails,e.clientTrxId,e.requestAmount,e.requsetCurrency,e.taxType,
                e.taxPercentage,e.taxAmount,e.eventType,
                e.paidAmount,e.pendingAmount,e.serviceChargeType,e.serviceChargePercentage,e.serviceChargeAmount,
                e.merchantId,e.merchantName,e.userName,e.lineItems,e.fromPaymentInstrument,
                e.fromPaymentInstrumentAccount,e.catergory,e.invoiceNo,e.orderNo,e.rateCard,
                e.referenceId,e.totalPaidAmount,e.msisdn,e.paybillNo);
    }
}
