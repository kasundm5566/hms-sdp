/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.service.impl;

import hms.kite.dataloader.domain.SummaryDataStatus;
import hms.kite.dataloader.repository.SummaryDataRepository;
import hms.kite.dataloader.repository.SummaryDataStatusRepository;
import hms.kite.dataloader.service.DataLoaderService;
import hms.kite.dataloader.service.ProcedureSchedulerManagerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
public class ProcedureSchedulerManagerServiceImpl implements ProcedureSchedulerManagerService {

    private static final Logger logger = LoggerFactory.getLogger(ProcedureSchedulerManagerServiceImpl.class);
    public String procedureName;
    public SummaryDataStatusRepository summaryDataStatusRepository;
    public SummaryDataRepository summaryDataRepository;
    public DataLoaderService dataLoaderService;

    public void execute() {
        try {
            logger.info("Stored Procedure execution scheduler started.");
            Date lastExecutedDate = summaryDataStatusRepository.getLastExecutedDate(procedureName);
            logger.info("Last executed date of the procedure [{}] is [{}]", procedureName, lastExecutedDate);
            SummaryDataStatus completeState = summaryDataStatusRepository.getStatus(lastExecutedDate,
                    procedureName, SummaryDataStatus.COMPLETE);
            if (completeState == null) {
                logger.info("Stored procedure [{}] identified as failed in the previous cycle and executing the started state again", procedureName);
                SummaryDataStatus startedState = summaryDataStatusRepository.getStatus(lastExecutedDate,
                        procedureName, SummaryDataStatus.STARTED);
                logger.info("Started date of procedure [{}] is [{}]", procedureName, startedState);
                executeStartedState(startedState);
            } else {
                logger.info("Executing the complete state for [{}]", procedureName);
                executeCompleteState(completeState);
            }
        } catch (SQLException e) {
            logger.error("Exception thrown while executing the stored procedures", e);
            //todo: Send an SNMP trap
        } catch (Exception e) {
            logger.error("Unexpected Exception thrown while executing the stored procedure [" + procedureName + "]", e);
        }
        logger.info("Stored Procedure execution scheduler completed.");
    }

    private void executeCompleteState(SummaryDataStatus completeState) throws SQLException {
        logger.info("Started running the executeCompleteState method.");
        Calendar lastExecutedDate = Calendar.getInstance();
        lastExecutedDate.setTime(completeState.getRequestedDate());

        logger.debug("Last Requested date [" + completeState.getRequestedDate() + "]");

        int dateDiff = getDateDifference(completeState.getRequestedDate());
        logger.debug("Date difference is [" + dateDiff + "]");
        for(int i = 0; i < dateDiff; i++) {
            lastExecutedDate.add(Calendar.DATE, 1);
            logger.info("Setting the status as started for [{}]", procedureName);
            summaryDataStatusRepository.save(new SummaryDataStatus(procedureName,
                    SummaryDataStatus.STARTED, new Date(), null, lastExecutedDate.getTime()));

            logger.info("Executing [{}] for [{}]", procedureName, lastExecutedDate.getTime());
            dataLoaderService.executeProcedure(procedureName, lastExecutedDate.getTime());
            
            logger.info("Setting the status as completed for [{}]", procedureName);
            summaryDataStatusRepository.save(new SummaryDataStatus(procedureName,
                    SummaryDataStatus.COMPLETE, null, new Date(), lastExecutedDate.getTime()));
        }
        logger.info("Completed running the executeCompleteState method.");
    }

    public int getDateDifference(Date requestedDateTime) {
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        calendar1.setTime(requestedDateTime);
        long time1 = calendar1.getTimeInMillis();
        long time2 = calendar2.getTimeInMillis();
        long time_diff = time2 - time1;
        return (int) (time_diff / (24 * 3600 * 1000))-1;
    }

    private void executeStartedState(SummaryDataStatus startedState) throws SQLException {
        logger.info("Started running executeStarted State for [{}].", procedureName);
        if (startedState != null) {
            logger.info("Removing partially generated data from status summary table for procedure [{}]", procedureName);
            summaryDataStatusRepository.delete(startedState);

            logger.info("Removing partially generated data from summary data table for procedure [{}]", procedureName);

            summaryDataRepository.delete(startedState.getRequestedDate());

            Calendar lastExecutedDate = Calendar.getInstance();
            lastExecutedDate.setTime(startedState.getRequestedDate());

            lastExecutedDate.setTime(startedState.getRequestedDate());

            logger.debug("Last Requested date [{}]", startedState.getRequestedDate());

            int dateDiff = getDateDifference(startedState.getRequestedDate());
            logger.debug("Date difference is [{}]", dateDiff);


            for (int i = 0; i <= dateDiff; i++) {

                if (i != 0) {
                    lastExecutedDate.add(Calendar.DATE, 1);
                }
                logger.info("Setting the status as started for [{}]", procedureName);
                summaryDataStatusRepository.save(new SummaryDataStatus(procedureName,
                        SummaryDataStatus.STARTED, new Date(), null, lastExecutedDate.getTime()));

                logger.info("Executing the stored procedure [{}] on [{}]", procedureName, lastExecutedDate.getTime());
                dataLoaderService.executeProcedure(procedureName, lastExecutedDate.getTime());

                logger.info("Setting the status as completed for [{}]", procedureName);
                summaryDataStatusRepository.save(new SummaryDataStatus(procedureName,
                        SummaryDataStatus.COMPLETE, null, new Date(), lastExecutedDate.getTime()));
            }
        }
        logger.info("Completed running executeStarted State method.");
    }

    public void setSummaryDataStatusRepository(SummaryDataStatusRepository summaryDataStatusRepository) {
        this.summaryDataStatusRepository = summaryDataStatusRepository;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public void setSummaryDataRepository(SummaryDataRepository summaryDataRepository) {
        this.summaryDataRepository = summaryDataRepository;
    }

    public void setDataLoaderService(DataLoaderService dataLoaderService) {
        this.dataLoaderService = dataLoaderService;
    }

}
