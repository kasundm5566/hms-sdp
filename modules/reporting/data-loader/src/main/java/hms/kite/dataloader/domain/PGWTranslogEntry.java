/*
 * (C) Copyright 1996-2012 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.dataloader.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PGWTranslogEntry extends TranslogEntry {
    public String transactionId;
    public String transactionType;
    public String timeStamp;
    public String channel;
    public String transactionStatus;
    public String transactionStatusDetails;
    public String clientTrxId;
    public double requestAmount;
    public String requsetCurrency;
    public String taxType;
    public String taxPercentage;
    public double taxAmount;
    public String eventType;
    public double paidAmount;
    public double pendingAmount;
    public String serviceChargeType;
    public String serviceChargePercentage;
    public double serviceChargeAmount;
    public String merchantId;
    public String merchantName;
    public String userName;
    public String lineItems;
    public String fromPaymentInstrument;
    public String fromPaymentInstrumentAccount;
    public String catergory;
    public String invoiceNo;
    public String orderNo;
    public String rateCard;
    public String referenceId;
    public double totalPaidAmount;
    public String msisdn;
    public String paybillNo;


    public PGWTranslogEntry(String[] record) {
        transactionId = record[0];
        transactionType = record[1];
        timeStamp = convertTimeStamp(record[2]);
        channel = record[3];
        transactionStatus = record[4];
        transactionStatusDetails = record[5];
        clientTrxId = record[6];
        requestAmount = convertToDouble(record[7]);
        requsetCurrency = record[8];
        taxType = record[9];
        taxPercentage = record[10];
        taxAmount = convertToDouble(record[11]);
        eventType = record[12];
        paidAmount = convertToDouble(record[13]);
        pendingAmount = convertToDouble(record[14]);
        serviceChargeType = record[15];
        serviceChargePercentage = record[16];
        serviceChargeAmount = convertToDouble(record[17]);
        merchantId = record[18];
        merchantName = record[19];
        userName = record[20];
        lineItems = record[21];
        fromPaymentInstrument = record[22];
        fromPaymentInstrumentAccount = record[23];
        catergory = record[24];
        invoiceNo = record[25];
        orderNo = record[26];
        rateCard = record[27];
        referenceId = record[28];
        msisdn = record[29];
        this.totalPaidAmount = requestAmount - pendingAmount;
        paybillNo = record[30];
    }

    private String checkNull(String var) {
        return (var.equals("")) ? null : var;
    }

    private String filterTimeStamp(String timeStamp) {
        if (timeStamp != null && timeStamp.length() > 19) {
            return timeStamp.substring(0, 19);
        } else {
            return null;
        }
    }

    private double convertToDouble(String val) {
        return (!val.equals("")) ? Double.parseDouble(val) : 0.0;
    }

    private String convertTimeStamp(String inDate) {
        SimpleDateFormat oldFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date strDate = null;
        try {
            strDate = oldFormat.parse(inDate.substring(0, 23));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newFormat.format(strDate);
    }
}
