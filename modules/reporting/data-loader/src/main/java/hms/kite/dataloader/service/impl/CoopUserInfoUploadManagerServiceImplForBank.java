/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.service.impl;

import hms.kite.dataloader.domain.SpBeneficiary;
import hms.kite.dataloader.repository.TransactionRepository;
import hms.kite.dataloader.service.CoopUserInfoUploadManagerService;
import hms.kite.dataloader.service.JsonDecoderService;
import hms.kite.dataloader.util.FileHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.SQLException;

public class CoopUserInfoUploadManagerServiceImplForBank implements CoopUserInfoUploadManagerService {

    private static final Logger logger = LoggerFactory.getLogger(CoopUserInfoUploadManagerServiceImplForBank.class);

    private String sourceDirectoryPath;
    private String fileNamePattern;
    private String archiveDirectoryPath;
    private String columnSeparator;
    private String enclosedBy;
    private String rowBreaker;
    private String columnAppender;
    private String record;
    private int spBeneficiaryIndex;

    private JsonDecoderService jsonDecoderService;
    private TransactionRepository transactionRepository;
    private FileHandler fileHandler;

    public void upload() {
        logger.info("Cooperate User Detail info upload scheduler started");
        fileHandler = new FileHandler();
        String[] matchingFiles = fileHandler.getMatchingFiles(sourceDirectoryPath, fileNamePattern);
        String fullPath;
        if (matchingFiles != null) {
            logger.info("Total of [{}] provider-info files found", matchingFiles.length);
            for (String matchingFile : matchingFiles) {
                fullPath = sourceDirectoryPath + "/" + matchingFile;
                logger.info("Uploading Coorporate User info file [{}]", matchingFile);
                try {
                    insertDataOfFile(fullPath);
                    logger.info("Successfully inserted the records to database in the file [{}]", fullPath);
                    logger.info("Archiving the file [{}]", fullPath);
                    fileHandler.moveDataFile(fullPath, archiveDirectoryPath);
                } catch (SQLException e) {
                    logger.error("DB Exception thrown while processing provider info log files", e);
                } catch (Exception e) {
                    logger.error("Exception thrown while uploading the service provider info file [" + fullPath + "]", e);
                }
            }
            logger.info("Cooperate User Details upload scheduler completed");
        }
    }

    private void insertDataOfFile(String filePath) throws SQLException, IOException {
        String fileContent = null;
        fileContent = fileHandler.getDataOfFile(filePath);
        String[] rows = new String[0];
        if (fileContent != null) {
            rows = fileContent.split(rowBreaker);
            for (String row : rows) {
                SpBeneficiary spBeneficiary = jsonDecoderService.decodeSpBeneficiary(row, columnSeparator, spBeneficiaryIndex);
                appendSpBeneficiaryToRecord(spBeneficiary, row);
                transactionRepository.save(record, columnSeparator);
            }
        }
    }

    private void appendSpBeneficiaryToRecord(SpBeneficiary spBeneficiary, String record) {
        this.record = record;
        appendStringToRecord(spBeneficiary.getName());
        appendStringToRecord(spBeneficiary.getBankCode());
        appendStringToRecord(spBeneficiary.getBranchCode());
        appendStringToRecord(spBeneficiary.getBranchName());
        appendStringToRecord(spBeneficiary.getBankAccountNumber());
    }


    public void setSourceDirectoryPath(String sourceDirectoryPath) {
        this.sourceDirectoryPath = sourceDirectoryPath;
    }

    public void setFileNamePattern(String fileNamePattern) {
        this.fileNamePattern = fileNamePattern;
    }

    public void setArchiveDirectoryPath(String archiveDirectoryPath) {
        this.archiveDirectoryPath = archiveDirectoryPath;
    }

    public void setColumnSeparator(String columnSeparator) {
        this.columnSeparator = columnSeparator;
    }

    public void setEnclosedBy(String enclosedBy) {
        this.enclosedBy = enclosedBy;
    }

    public void setRowBreaker(String rowBreaker) {
        this.rowBreaker = rowBreaker;
    }

    public void setColumnAppender(String columnAppender) {
        this.columnAppender = columnAppender;
    }

    public void setSpBeneficiaryIndex(int spBeneficiaryIndex) {
        this.spBeneficiaryIndex = spBeneficiaryIndex;
    }

    public void setJsonDecoderService(JsonDecoderService jsonDecoderService) {
        this.jsonDecoderService = jsonDecoderService;
    }

    public void setTransactionRepository(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    private void appendStringToRecord(String value) {
        record = record + columnAppender + value;
    }
}
