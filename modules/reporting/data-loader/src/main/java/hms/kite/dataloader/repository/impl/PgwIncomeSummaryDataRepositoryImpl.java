/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.repository.impl;

import hms.kite.dataloader.repository.DateInfoRepository;
import hms.kite.dataloader.repository.SummaryDataRepository;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.SQLException;
import java.util.Date;

public class PgwIncomeSummaryDataRepositoryImpl implements SummaryDataRepository {
    JdbcTemplate jdbcTemplate;
    DateInfoRepository dateInfoRepository;

    private static final String deleteSqlStatement = "delete from `pgw_income_summary` where `date_id` = ?";

    @Override
    public void delete(Date date) throws SQLException {
        int date_id = dateInfoRepository.getDateId(date);
        jdbcTemplate.update(deleteSqlStatement, date_id);
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void setDateInfoRepository(DateInfoRepository dateInfoRepository) {
        this.dateInfoRepository = dateInfoRepository;
    }
}
