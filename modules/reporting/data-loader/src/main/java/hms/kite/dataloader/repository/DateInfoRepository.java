/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.repository;

import org.joda.time.DateTime;

import java.sql.SQLException;
import java.util.Date;

/**
 * Interface for the DateInformation
 */
public interface DateInfoRepository {
    /**
     * getDateId get the date id from date_info table for the given date
     * @param date full_date
     * @return dateId corresponding to the given full_date
     * @throws SQLException if the given date is not available in the date_info table
     */
    public int getDateId(Date date) throws SQLException;
    public String getDateId(DateTime dateTime) throws SQLException;

}
