/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.service.impl;

import hms.kite.dataloader.repository.TransactionRepository;
import hms.kite.dataloader.repository.impl.LogInfoRepositoryImpl;
import hms.kite.dataloader.service.JsonDecoderService;
import hms.kite.dataloader.service.TransactionUploadManagerService;
import hms.kite.dataloader.util.FileHandler;
import hms.kite.dataloader.util.PgwTransactionRecordProcessorThread;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
public class PgwTransactionUploadManagerServiceImpl implements TransactionUploadManagerService {

    private static final CharsetDecoder decoder = Charset.forName("ISO-8859-1").newDecoder();
    private static final Logger logger = LoggerFactory.getLogger(PgwTransactionUploadManagerServiceImpl.class);

    private String sourceDirectoryPath;
    private String fileNamePattern;
    private String archiveDirectoryPath;
    private String columnSeparator;
    private String columnSeparatorAppender;
    private String rowBreaker;
    private int poolSize;
    private int lineItemIndex;
    private int currencyJsonIndex;
    private List list;

    private JsonDecoderService jsonDecoderService;
    private TransactionRepository transactionRepository;
    private FileHandler fileHandler;
    private LogInfoRepositoryImpl logInfoRepository;
    private static final String FILE_TYPE="pgw";

    private final ExecutorService pool;

    public PgwTransactionUploadManagerServiceImpl(int poolSize) {
        this.poolSize = poolSize;
        pool = Executors.newFixedThreadPool(this.poolSize);
    }

    public void upload() {
        logger.info("PGW Trans log upload scheduler started");
        fileHandler = new FileHandler();
        String[] files = getFiles();
        logger.info("Total of [{}] PGW translog files found", files.length);
        for(String file : files){
            try{
                logger.info("Started to process [{}] file", file);
                if(!logInfoRepository.isInsertedFile(file)){
                    readFile(file);
                    logInfoRepository.insertFileInfo(file,FILE_TYPE);
                    logger.info("Successfully inserted the records to database in the file [{}]", file);
                }
                else{
                    String processedTime =logInfoRepository.getProcessedTime(file);
                    logger.error("The file [{}] is already inserted into database on [{}]", file, processedTime);
                    archiveTheFile(file);
                    continue;
                }
                archiveTheFile(file);                
            } catch (SQLException e) {
                logger.error("DB Exception thrown while processing pgw trans log files", e);
                //todo:send SNMP trap
            } catch (Exception e) {
                logger.error("Unexpected error while reading the file ["+ file +"]", e);
            }
        }
        logger.info("PGW Trans log upload scheduler completed");
    }

    private String[] getFiles(){
        return fileHandler.getMatchingFiles(sourceDirectoryPath, fileNamePattern);
    }

    private void readFile(String file) throws IOException {
        FileChannel channel = null;
        FileInputStream inFile = null;
        String fullPath = sourceDirectoryPath + file;
        try{
            logger.info("Reading the file [ {} ] started", file);
            StringBuilder builder = new StringBuilder();
            inFile = new FileInputStream(fullPath);
            channel = inFile.getChannel();
            MappedByteBuffer buffer = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
            CharBuffer charBuffer = decoder.decode(buffer);
            char character;
            while (charBuffer.hasRemaining()) {
                character = charBuffer.get();
                builder.append(character);
                if (character == rowBreaker.charAt(0)) {
                    pool.execute(new PgwTransactionRecordProcessorThread(builder.toString(),
                            columnSeparator, columnSeparatorAppender, list, lineItemIndex,
                            currencyJsonIndex, jsonDecoderService, transactionRepository));
                    builder = new StringBuilder();
                }
            }
            logger.info("Reading the file [{}] completed", file);
        } finally{
            try{
                if(channel != null){
                    channel.close();
                }
                if(inFile != null){
                    inFile.close();
                }
            } catch (Exception ignored){
            }
        }
    }

    private void archiveTheFile(String file) {
        logger.info("Archiving the file [{}]", file);
        fileHandler.moveDataFile(sourceDirectoryPath + file, archiveDirectoryPath);
    }

    public void setSourceDirectoryPath(String sourceDirectoryPath) {
        if(!sourceDirectoryPath.endsWith("/")) {
            sourceDirectoryPath = sourceDirectoryPath + "/";
        }
        this.sourceDirectoryPath = sourceDirectoryPath;
    }

    public void setFileNamePattern(String fileNamePattern) {
        this.fileNamePattern = fileNamePattern;
    }

    public void setArchiveDirectoryPath(String archiveDirectoryPath) {
        if(!archiveDirectoryPath.endsWith("/")) {
            archiveDirectoryPath = archiveDirectoryPath + "/";
        }
        this.archiveDirectoryPath = archiveDirectoryPath;
    }

    public void setColumnSeparator(String columnSeparator) {
        this.columnSeparator = columnSeparator;
    }

    public void setColumnSeparatorAppender(String columnSeparatorAppender) {
        this.columnSeparatorAppender = columnSeparatorAppender;
    }

    public void setRowBreaker(String rowBreaker) {
        this.rowBreaker = rowBreaker;
    }

    public void setPoolSize(int poolSize) {
        this.poolSize = poolSize;
    }

    public void setLineItemIndex(int lineItemIndex) {
        this.lineItemIndex = lineItemIndex;
    }

    public void setCurrencyJsonIndex(int currencyJsonIndex) {
        this.currencyJsonIndex = currencyJsonIndex;
    }

    public void setList(List<String> list) {
        this.list = list;
    }

    public void setJsonDecoderService(JsonDecoderService jsonDecoderService) {
        this.jsonDecoderService = jsonDecoderService;
    }

    public void setTransactionRepository(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public void setLogInfoRepository(LogInfoRepositoryImpl logInfoRepository) {
        this.logInfoRepository = logInfoRepository;
    }

}
