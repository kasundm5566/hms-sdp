/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.service.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import hms.kite.dataloader.domain.Currency;
import hms.kite.dataloader.domain.LineItem;
import hms.kite.dataloader.domain.SpBeneficiary;
import hms.kite.dataloader.service.JsonDecoderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
public class JsonDecoderServiceImpl implements JsonDecoderService {

    private static final Logger logger = LoggerFactory.getLogger(JsonDecoderServiceImpl.class);

    private static Type typeCurrency = new TypeToken<List<Currency>>() {}.getType();
    private static Type typeLineItem = new TypeToken<List<LineItem>>() {}.getType();
    private static Type typePaymentInstrument = new TypeToken<Map<String,List<String>>>() {}.getType();
    private static Gson gson = new Gson();


    public Map<String, LineItem> decodeLineItem(String  lineItemJSonString) {
        logger.debug("Decoding LineItem json object [{}]", lineItemJSonString);
        if(lineItemJSonString.equalsIgnoreCase("")) {
            return null;
        }
        List<LineItem> lineItemList = gson.fromJson(lineItemJSonString, typeLineItem);
        Map<String, LineItem> lineItemMap = new HashMap<String, LineItem>();
        for (LineItem lineItem : lineItemList) {
            lineItemMap.put(lineItem.getOriginatedRuleId() + ":" + lineItem.getOperationType(), lineItem);
        }
        return lineItemMap;
    }

    public Map<String, List<String>> decodePaymentInstrument(String paymentInstrumentJsonString) {
         logger.debug("Payment Instrument jason object string [{}]", paymentInstrumentJsonString);
        if (paymentInstrumentJsonString.equalsIgnoreCase("")) {
            return null;
        }
        Map<String, List<String>> paymentInstrumentMap= gson.fromJson(paymentInstrumentJsonString, typePaymentInstrument);
        return paymentInstrumentMap;
    }


    @Override
    public Map<String, LineItem> decodeLineItem(String record, String separator, int lineItemIndex) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Map<String, Currency> decodeCurrency(String currencyJsonString) {
        logger.debug("Rate card jason object string [{}]", currencyJsonString);
        if (currencyJsonString.equalsIgnoreCase("")) {
            return null;
        }
        //===============================
        if (!currencyJsonString.startsWith("[")) {
            currencyJsonString = "[" + currencyJsonString + "]";
        }
        //===============================

        List<Currency> currencyList = gson.fromJson(currencyJsonString, typeCurrency);
        Map<String, Currency> currencyMap = new HashMap<String, Currency>();
        for (Currency currency : currencyList) {
            currencyMap.put(currency.getCurrencyCode(), currency);
        }
        return currencyMap;
    }

    public SpBeneficiary decodeSpBeneficiary(String record, String separator, int spBeneficiaryIndex) {
        String spBeneficiaryString = getSpBeneficiaryObj(record, separator, spBeneficiaryIndex);
        logger.debug("SP Beneficiary jason object string [{}]", spBeneficiaryString);
        //todo handle null
        SpBeneficiary spBeneficiary = new Gson().fromJson(spBeneficiaryString, SpBeneficiary.class);
        return spBeneficiary;
    }

    public String getLineItemObj(String record, String separator, int lineItemIndex) {
        String[] arrString = record.split(separator, -1);
        return arrString[lineItemIndex - 1];
    }

    public String getCurrencyJsonObj(String record, String separator, int currencyJsonIndex) {
        String[] arrString = record.split(separator);
        return arrString[currencyJsonIndex - 1];
    }

    public String getSpBeneficiaryObj(String record, String separator, int spBeneficiaryIndex) {
        String[] arrString = record.split(separator, -1);
        return arrString[spBeneficiaryIndex - 1];

    }

}
