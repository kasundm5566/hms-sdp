/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.service.impl;

import hms.kite.dataloader.domain.SummaryDataStatus;
import hms.kite.dataloader.service.DataLoaderService;
import hms.kite.dataloader.util.ProcedureExecutor;

import javax.sql.DataSource;
import java.util.Date;
import java.util.HashMap;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */
public class DataLoaderServiceImpl implements DataLoaderService {

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void executeProcedure(String procedureName, Date requestedDate) {
        ProcedureExecutor procedureExecutor = new ProcedureExecutor(dataSource, procedureName);

        java.sql.Date sqlRequestedDate = new java.sql.Date(requestedDate.getTime());

        HashMap parameters = new HashMap();
        parameters.put(SummaryDataStatus.REQUESTED_DATE, sqlRequestedDate);
        //parameters.put(SummaryDataStatus.PROCEDURE_NAME, procedureName);
        procedureExecutor.executeSp(parameters);
    }
}
