/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.service.impl;

import hms.kite.dataloader.repository.impl.ServiceProviderInfoRepositoryImpl;
import hms.kite.dataloader.service.JsonDecoderService;
import hms.kite.dataloader.service.ServiceProviderInfoUploadManagerService;
import hms.kite.dataloader.util.FileHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate:  $
 * $LastChangedBy:  $
 * $LastChangedRevision:  $
 */

public class ServiceProviderInfoUploadManagerServiceImpl implements ServiceProviderInfoUploadManagerService {

    private static final Logger logger = LoggerFactory.getLogger(ServiceProviderInfoUploadManagerServiceImpl.class);

    private String sourceDirectoryPath;
    private String fileNamePattern;
    private String archiveDirectoryPath;
    private String columnSeparator;
    private String enclosedBy;
    private String rowBreaker;
    private String columnAppender;
    private ServiceProviderInfoRepositoryImpl transactionRepository;
    private FileHandler fileHandler;
    private JsonDecoderService jsonDecoderService;
    private String paymentInstrument;

    public void upload() {
        logger.info("Service Provider info upload scheduler started");
        fileHandler = new FileHandler();
        String[] matchingFiles = fileHandler.getMatchingFiles(sourceDirectoryPath, fileNamePattern);
        String fullPath;
        if (matchingFiles != null) {
            logger.info("Total of [{}] provider-info files found", matchingFiles.length);
            for (String matchingFile : matchingFiles) {
                fullPath = sourceDirectoryPath + matchingFile;
                logger.info("Uploading Service Provider info file [{}]", matchingFile);
                try {
                    insertDataOfFile(fullPath);
                    logger.info("Successfully inserted the records to database in the file [{}]", fullPath);
                    logger.info("Archiving the file [{}]", fullPath);
                    fileHandler.moveDataFile(fullPath, archiveDirectoryPath);
                } catch (SQLException e) {
                    logger.error("DB Exception thrown while processing provider info log files", e);
                    //todo:send SNMP trap
                } catch (Exception e) {
                    logger.error("Exception thrown while uploading the service provider info file [" + fullPath + "]", e);
                }
            }
            logger.info("Service Provider info upload scheduler completed");
        }
    }

    private void insertDataOfFile(String filePath) throws SQLException, IOException {
        String fileContent = null;
        fileContent = fileHandler.getDataOfFile(filePath);
        String[] rows = new String[0];
        if (fileContent != null) {
            rows = fileContent.split(rowBreaker);
            String[] data;

            for (String row : rows) {
                try {
                    String[] columnValues = row.split(columnSeparator, -1);
                    if ((!columnValues[8].isEmpty()) && (!columnValues[8].equals("{}"))) {
                        logger.info("Column value 8 [" + columnValues[8] + "]");
                        Map<String, List<String>> paymentInstrumentMap = jsonDecoderService.decodePaymentInstrument(columnValues[8]);

                        for (Map.Entry<String, List<String>> entry : paymentInstrumentMap.entrySet()) {
                            for (String pi : entry.getValue()) {
                                String ncs = entry.getKey();
                                String paymentInstrument1;
                                String paybillno;
                                if (pi.contains(paymentInstrument)) {
                                    String[] arr = pi.split(":");
                                    paymentInstrument1 = arr[0];
                                    paybillno = arr[1];
                                } else {
                                    paymentInstrument1 = pi;
                                    paybillno = "";
                                }
                                transactionRepository.saveSpPaymentInstrumentInfo(columnValues[0], columnValues[3],
                                        ncs, paymentInstrument1, paybillno);
                            }
                        }
                    }
                    transactionRepository.save(row, columnSeparator);
                } catch (Exception e) {
                    logger.error("Couldn't save the record [" + row + "] Error while saving the record", e);
                }
                logger.debug("Saving Service Provider info [{ " + row + " }]");
            }
        }
    }

    public void setSourceDirectoryPath(String sourceDirectoryPath) {
        if (!sourceDirectoryPath.endsWith("/")) {
            sourceDirectoryPath = sourceDirectoryPath + "/";
        }
        this.sourceDirectoryPath = sourceDirectoryPath;
    }

    public void setFileNamePattern(String fileNamePattern) {
        this.fileNamePattern = fileNamePattern;
    }

    public void setArchiveDirectoryPath(String archiveDirectoryPath) {
        this.archiveDirectoryPath = archiveDirectoryPath;
    }

    public void setTransactionRepository(ServiceProviderInfoRepositoryImpl transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public void setColumnSeparator(String columnSeparator) {
        this.columnSeparator = columnSeparator;
    }

    public void setEnclosedBy(String enclosedBy) {
        this.enclosedBy = enclosedBy;
    }

    public void setRowBreaker(String rowBreaker) {
        this.rowBreaker = rowBreaker;
    }

    public void setColumnAppender(String columnAppender) {
        this.columnAppender = columnAppender;
    }

    public void setJsonDecoderService(JsonDecoderService jsonDecoderService) {
        this.jsonDecoderService = jsonDecoderService;
    }

    public String getPaymentInstrument() {
        return paymentInstrument;
    }

    public void setPaymentInstrument(String paymentInstrument) {
        this.paymentInstrument = paymentInstrument;
    }
}