/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.domain;

import java.util.Date;

public class SummaryDataStatus {

    public static final int STARTED = 1;
    public static final int COMPLETE = 2;

    public static final String REQUESTED_DATE = "requested_date";
    public static final String PROCEDURE_NAME = "procedure_name";

    private long ID;
    private String procedureName;
    private int status;
    private Date startDateTime;
    private Date endDateTime;
    private Date requestedDate;

    public SummaryDataStatus(String procedureName, int status, Date startDateTime, Date endDateTime, Date requestedDate) {
        this.procedureName = procedureName;
        this.status = status;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.requestedDate = requestedDate;
    }

    public SummaryDataStatus() {
    }

    public Date getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(Date endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public Date getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(Date requestedDate) {
        this.requestedDate = requestedDate;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Date startDateTime) {
        this.startDateTime = startDateTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
