/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.repository.impl;

import hms.kite.dataloader.repository.DateInfoRepository;
import hms.kite.dataloader.repository.SummaryDataRepository;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.SQLException;
import java.util.Date;

public class TransactionSummaryDataRepositoryImpl implements SummaryDataRepository {

    private JdbcTemplate jdbcTemplate;

    private DateInfoRepository dateInfoRepository;

    public void setDateInfoRepository(DateInfoRepository dateInfoRepository) {
        this.dateInfoRepository = dateInfoRepository;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String DELETE_SQL_STATEMENT =
            "delete from `sdp_transaction_summary` where `date_id` = ?";

    public void delete(Date date) throws SQLException{
        int date_id = dateInfoRepository.getDateId(date);
        this.jdbcTemplate.update(DELETE_SQL_STATEMENT, date_id);
    }
}
