/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.repository.impl;

import hms.kite.dataloader.domain.SummaryDataStatus;
import hms.kite.dataloader.repository.SummaryDataStatusRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SummaryDataStatusRepositoryImpl implements SummaryDataStatusRepository {

    private JdbcTemplate jdbcTemplate;
    private final static Logger logger = LoggerFactory.getLogger(SummaryDataStatusRepositoryImpl.class);

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    private static final String MAX_DATE_QUERY = "select max(`requested_date`) " +
            "as max_req_date from `summary_data_status` where `procedure_name` =  ?";

    public Date getLastExecutedDate(String procedureName) throws SQLException{
        return jdbcTemplate.queryForObject(MAX_DATE_QUERY,new Object[]{procedureName},Date.class);
    }


    private static final String GET_STATUS_QUERY = "select `start_date_time`, " +
                "`end_date_time` from `summary_data_status` where `procedure_name` = ? and " +
                "`status` = ? and `requested_date` = ?";

    public SummaryDataStatus getStatus(Date date, String procedureName, int status) throws SQLException{
        final Date finalDate = date;
        final String finalProcedureName = procedureName;
        final int finalStatus = status;
        if(getNoOfRecords(date, procedureName, status) == 1){
            return this.jdbcTemplate.queryForObject(GET_STATUS_QUERY,
                    new Object[]{procedureName, status, getStringDate(date)}, new RowMapper<SummaryDataStatus>() {
                        public SummaryDataStatus mapRow(ResultSet resultSet, int rowNum) throws SQLException {
                            SummaryDataStatus summaryDataStatus = new SummaryDataStatus();
                            summaryDataStatus.setProcedureName(finalProcedureName);
                            summaryDataStatus.setStatus(finalStatus);
                            summaryDataStatus.setRequestedDate(finalDate);
                            summaryDataStatus.setStartDateTime(resultSet.getTimestamp("start_date_time"));
                            summaryDataStatus.setEndDateTime(resultSet.getTimestamp("end_date_time"));
//                            logger.debug("["+finalProcedureName+"]["+finalStatus+"]["+finalDate+"]["+resultSet.getTimestamp("start_date_time")+"]["+resultSet.getTimestamp("end_date_time")+"]");
                            return summaryDataStatus;
                        }
                    });
        } else {
            return null;
        }
    }


    private static final String SAVE_SQL_STATEMENT = "insert into `summary_data_status` (" +
            "`procedure_name`, `requested_date`, `start_date_time`, `end_date_time`," +
            " `status`) values (?, ?, ?, ?, ?)";

    public void save(SummaryDataStatus summaryDataStatus) throws SQLException{
        this.jdbcTemplate.update(SAVE_SQL_STATEMENT, summaryDataStatus.getProcedureName(),
                getStringDate(summaryDataStatus.getRequestedDate()), getStringDateTime(
                summaryDataStatus.getStartDateTime()), getStringDateTime(
                summaryDataStatus.getEndDateTime()), summaryDataStatus.getStatus());
    }


    private static final String DELETE_SQL_STATEMENT = "delete from `summary_data_status` where " +
            "`procedure_name` = ?and `requested_date` = ? and `start_date_time` = ? and " +
            "`status` = ?";

    public void delete(SummaryDataStatus summaryDataStatus) throws SQLException{
//        logger.debug("["+summaryDataStatus.getProcedureName()+"]["+getStringDate(summaryDataStatus.getRequestedDate())+"]["+getStringDateTime(summaryDataStatus.getStartDateTime())+"]["+getStringDateTime(summaryDataStatus.getEndDateTime())+"]["+summaryDataStatus.getStatus()+"]");
//        logger.debug("https://jira.springsource.org/browse/SPR-4809");
        this.jdbcTemplate.update(DELETE_SQL_STATEMENT, summaryDataStatus.getProcedureName(),
                getStringDate(summaryDataStatus.getRequestedDate()), getStringDateTime(
                summaryDataStatus.getStartDateTime()), summaryDataStatus.getStatus());
    }


    private static final String GET_NO_OF_RECORDS_QUERY = "select count(*) from " +
            "`summary_data_status` where `procedure_name` = ? and " +
                "`status` = ? and `requested_date` = ?";

    private int getNoOfRecords(Date date, String procedureName, int status) throws SQLException{
        return this.jdbcTemplate.queryForInt(GET_NO_OF_RECORDS_QUERY, procedureName,
                status, getStringDate(date));
    }

    private static final String DATE_FORMAT = "yyyy-MM-dd";

    private String getStringDate(Date date) {
        Format formatter = new SimpleDateFormat(DATE_FORMAT);
        if(date == null) {
            return null;
        } else {
            return formatter.format(date);
        }
    }


    private static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    private String getStringDateTime(Date date) {
        Format formatter = new SimpleDateFormat(DATE_TIME_FORMAT);
        if(date == null) {
            return null;
        } else {
            return formatter.format(date);
        }
    }
}
