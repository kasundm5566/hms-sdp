/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.domain;

public class ServiceProviderInfo {

    public int id;
    public String spId;
    public String spName;
    public String appId;
    public String appName;
    public String revenueSharePercentage;

    public ServiceProviderInfo(String spId, String spName, String appId, String appName, String revenueSharePercentage) {
        this.spId = spId;
        this.spName = spName;
        this.appId = appId;
        this.appName = appName;
        this.revenueSharePercentage = revenueSharePercentage;
    }

    public ServiceProviderInfo() { }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSpId() {
        return spId;
    }

    public void setSpId(String spId) {
        this.spId = spId;
    }

    public String getSpName() {
        return spName;
    }

    public void setSpName(String spName) {
        this.spName = spName;
    }

    public String getRevenueSharePercentage() {
        return revenueSharePercentage;
    }

    public void setRevenueSharePercentage(String revenueSharePercentage) {
        this.revenueSharePercentage = revenueSharePercentage;
    }

    @Override
    public String toString() {
        String colSep=",";
        return spId+colSep+spName+colSep+appId+colSep+appName+colSep+revenueSharePercentage;
    }
}



