/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */

package hms.kite.dataloader.service.impl;

import hms.kite.dataloader.repository.DateInfoRepository;
import hms.kite.dataloader.repository.TransactionRepository;
import hms.kite.dataloader.repository.impl.LogInfoRepositoryImpl;
import hms.kite.dataloader.service.TotalSubscriptionsUploadManagerService;
import hms.kite.dataloader.util.FileHandler;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.SQLException;

public class TotalSubscriptionsUploadManagerServiceImpl implements TotalSubscriptionsUploadManagerService{

    private static final Logger logger = LoggerFactory.getLogger(TotalSubscriptionsUploadManagerServiceImpl.class);

    private TransactionRepository transactionRepository;

    private String columnSeparator;

    private String rowBreaker;

    private String archiveDirectoryPath;

    private String sourceDirectoryPath;

    private String fileNamePattern;

    FileHandler fileHandler;

    private String dateSeparator;

    private String columnAppender;

    private DateInfoRepository dateInfoRepository;

    private LogInfoRepositoryImpl logInfoRepository;

    private final String FILE_TYPE = "subs";


    @Override
    public void upload() {
        logger.info("upload method invoked");
        fileHandler = new FileHandler();
        String[] matchingFiles = fileHandler.getMatchingFiles(sourceDirectoryPath, fileNamePattern);
        logger.info("matchingFiles [{}]",matchingFiles);
        String fullPath;
        if (matchingFiles != null) {
            logger.info("Total of [{}] subscription translog files found", matchingFiles.length);
            for (String matchingFile : matchingFiles) {
                fullPath = sourceDirectoryPath + matchingFile;
                try {
                    if (!logInfoRepository.isInsertedFile(matchingFile)) {
                        logger.info("Uploading subscription file [{}]", matchingFile);
                        insertDataOfFile(fullPath);
                        logInfoRepository.insertFileInfo(matchingFile, FILE_TYPE);
                        logger.info("Successfully inserted the records of total subscribers from file to database [{}]", fullPath);
                    } else {
                        String processedTime =logInfoRepository.getProcessedTime(matchingFile);
                        logger.info("The file [{}] is already inserted into the database on [{}]", matchingFile, processedTime);
                        archiveTheFile(fullPath);
                        continue;
                    }
                    archiveTheFile(fullPath);
                } catch (IOException e) {
                    logger.error("DB Exception thrown while processing total subscription log files", e);
                } catch (SQLException e) {
                    logger.error("DB Exception thrown while receiving mysql data", e);
                }
            }
            logger.info("total subscriptions upload scheduler completed");
        }


    }

    private void archiveTheFile(String fullPath) {
        fileHandler.moveDataFile(fullPath, archiveDirectoryPath);
        logger.info("Successfully archived the file [{}]", fullPath);
    }


    private void insertDataOfFile(String fullPath) throws IOException {
        String fileContent = fileHandler.getDataOfFile(fullPath);
        String[] rows;
        String[] receivedAppRow;
        String appRow = "";
        int newSubscriptions = 0;
        int deRegSubscription = 0;

        if(fileContent != null) {
            rows = fileContent.split(rowBreaker);
            for (String row : rows) {
                receivedAppRow = row.split(columnSeparator);
                try {
                    appRow = getDateId(receivedAppRow[0]) + columnAppender + receivedAppRow[1] + columnAppender + receivedAppRow[2] + columnAppender +
                            receivedAppRow[3] + columnAppender + receivedAppRow[4] + columnAppender + newSubscriptions + columnAppender + deRegSubscription + columnAppender + receivedAppRow[5] + columnAppender;
                    transactionRepository.save(appRow, columnSeparator);
                }
                catch (Exception e) {
                    logger.error("Couldn't save the record ["+row+"] Error while saving the record", e);
                }
            }
        }
    }

    private String getDateId(String dateStr) throws SQLException {
        String[] date = dateStr.split(dateSeparator);
        DateTime dateTime = new DateTime(Integer.parseInt(date[0]), Integer.parseInt(date[1]), Integer.parseInt(date[2]),0, 0, 0, 0);
        return dateInfoRepository.getDateId(dateTime);
    }

    public void setSourceDirectoryPath(String sourceDirectoryPath) {
        if (!sourceDirectoryPath.endsWith("/")) {
            sourceDirectoryPath = sourceDirectoryPath + "/";
            this.sourceDirectoryPath = sourceDirectoryPath;
        }
        else
            this.sourceDirectoryPath = sourceDirectoryPath;
    }

    public void setFileNamePattern(String fileNamePattern) {
        this.fileNamePattern = fileNamePattern;
    }

    public void setArchiveDirectoryPath(String archiveDirectoryPath) {
        this.archiveDirectoryPath = archiveDirectoryPath;
    }

    public void setRowBreaker(String rowBreaker) {
        this.rowBreaker = rowBreaker;
    }

    public void setColumnSeparator(String columnSeparator) {
        this.columnSeparator = columnSeparator;
    }

    public void setTransactionRepository(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public void setDateSeparator(String dateSeparator) {
        this.dateSeparator = dateSeparator;
    }

    public void setDateInfoRepository(DateInfoRepository dateInfoRepository) {
        this.dateInfoRepository = dateInfoRepository;
    }

    public void setColumnAppender(String columnAppender) {
        this.columnAppender = columnAppender;
    }

    public void setLogInfoRepository(LogInfoRepositoryImpl logInfoRepository) {
        this.logInfoRepository = logInfoRepository;
    }

}
