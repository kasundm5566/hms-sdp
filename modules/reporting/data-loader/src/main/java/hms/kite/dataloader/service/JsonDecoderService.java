/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.service;

import hms.kite.dataloader.domain.Currency;
import hms.kite.dataloader.domain.LineItem;
import hms.kite.dataloader.domain.SpBeneficiary;

import java.lang.String;
import java.util.List;
import java.util.Map;

public interface JsonDecoderService {

    /**
     * Decode the LineItem Json string
     * @param record
     * @return a Map with 'originatedRuleId:OperationType' as the Key
     */
    public Map<String, LineItem> decodeLineItem(String record, String separator, int lineItemIndex);

    /**
     * Decode the Currency Json string
     * @param record
     * @return a map with currencyCode as a key
     */
    public Map<String, Currency> decodeCurrency(String currencyJsonString);

    /**
     * Decode the LineItem Json string
     * @param record
     * @return a Map with 'originatedRuleId:OperationType' as the Key
     */
    public SpBeneficiary decodeSpBeneficiary(String record, String separator, int spBeneficiaryIndex);


    public Map<String, LineItem> decodeLineItem(String lineItemJeson);

    public Map<String, List<String>> decodePaymentInstrument(String paymentInstrumentJsonString);
}
