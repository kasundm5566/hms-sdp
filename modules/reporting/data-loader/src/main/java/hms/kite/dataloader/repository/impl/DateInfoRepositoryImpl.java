/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.repository.impl;

import hms.kite.dataloader.repository.DateInfoRepository;
import org.joda.time.DateTime;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateInfoRepositoryImpl implements DateInfoRepository{
    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private static final String QUERY_FOR_INT = "select `date_id` from `date_info` where `full_date` = ?";

    public int getDateId(Date date) throws SQLException {
        Format formatter = new SimpleDateFormat(DATE_FORMAT);
        String fullDate = formatter.format(date);
        return this.jdbcTemplate.queryForInt(QUERY_FOR_INT, fullDate);
    }

    public String getDateId(DateTime dateTime) throws SQLException {
        Date fullDateUnformatted = dateTime.toDate();
        Format formatter = new SimpleDateFormat(DATE_FORMAT);
        Object[] fullDate = new Object[1];
        fullDate[0] = formatter.format(fullDateUnformatted);

        try {
            Object o = (String) jdbcTemplate.queryForObject(QUERY_FOR_INT, fullDate, String.class);
            return (String) o;
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

}
