package hms.kite.dataloader.domain;

/**
 * Created with IntelliJ IDEA.
 * User: dulanjaleen
 * Date: 2/15/13
 * Time: 4:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class CmsTranslogEntry extends TranslogEntry{

    public String correlationId;
    public String timeStamp;
    public String spId;
    public String spName;
    public String appId;
    public String appName;
    public String chargingType;
    public double chargeAmount;
    public String chargeAmountCurrency;
    public String ncs;
    public String direction;
    public double revenueSharePercentage;
    public double sysCurChargeAmount;
    public double sysCurSpProfit;
    public double sysCurVcProfit;
    public String responseCode;
    public String transactionStatus;
    public String fileType;
    public String paymentInstrument;
    public String msisdn;
    public String responseDescription;

    public CmsTranslogEntry(String[] columns) {
        if (columns.length > 0) {
            correlationId = columns[0];
            timeStamp = filterTimeStamp(columns[1]);
            appId = columns[2];
            spId = columns[3];
            ncs = columns[4];
            direction = columns[8];
            responseDescription = columns[9];
            chargeAmount =  convertToDouble(columns[12]);
            chargeAmountCurrency = columns[13];
            msisdn = columns[6];
            chargingType = columns[10];
            responseCode = columns[14];
            transactionStatus = columns[15];
            paymentInstrument = columns[11];

        }
    }

    private double convertToDouble(String val) {
        return (!val.equals("")) ? Double.parseDouble(val) : 0.0;
    }

    private String filterTimeStamp(String timeStamp) {
        if (timeStamp != null && timeStamp.length() > 19) {
            return timeStamp.substring(0, 19);
        } else {
            return null;
        }
    }

    public double getSysCurSpProfit() {
        return sysCurSpProfit;
    }

    public void setSysCurSpProfit(double sysCurSpProfit) {
        this.sysCurSpProfit = sysCurSpProfit;
    }

    public double getSysCurChargeAmount() {
        return sysCurChargeAmount;
    }

    public void setSysCurChargeAmount(double sysCurChargeAmount) {
        this.sysCurChargeAmount = sysCurChargeAmount;
    }

    public double getSysCurVcProfit() {
        return sysCurVcProfit;
    }

    public void setSysCurVcProfit(double sysCurVcProfit) {
        this.sysCurVcProfit = sysCurVcProfit;
    }

    public double getChargeAmount() {
        return chargeAmount;
    }

    public void setRevenueSharePercentage(double revenueSharePercentage) {
        this.revenueSharePercentage = revenueSharePercentage;
    }

    public double getRevenueSharePercentage() {
        return revenueSharePercentage;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getSpName() {
        return spName;
    }

    public void setSpName(String spName) {
        this.spName = spName;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }
}
