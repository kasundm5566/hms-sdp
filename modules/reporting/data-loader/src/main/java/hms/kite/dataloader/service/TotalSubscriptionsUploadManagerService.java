/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */

package hms.kite.dataloader.service;

/**
 * Created with IntelliJ IDEA.
 * User: Upeka
 * Date: 1/11/13
 * Time: 6:14 PM
 * To change this template use File | Settings | File Templates.
 */
public interface TotalSubscriptionsUploadManagerService {
    public void upload();
}
