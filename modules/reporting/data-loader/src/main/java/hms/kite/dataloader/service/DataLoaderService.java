/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.service;

import java.util.Date;

public interface DataLoaderService {
    /**
     * Call the stored procedure
     * @param procedureName Stored procedure name
     * @param requestedDate For when the summary is to be populate
     */
    public void executeProcedure(String procedureName, Date requestedDate);

}
