/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */

package hms.kite.dataloader.service.impl;

import hms.kite.dataloader.service.SubscriptionLogGenerater;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import static hms.kite.dataloader.util.DataLoaderKeyBox.*;

public class SubscriptionLogGeneraterImpl implements SubscriptionLogGenerater {
    private static final Logger logger = LoggerFactory.getLogger(SubscriptionLogGeneraterImpl.class);
    private GenerateTotalSubscriptionTranslog generateTotalSubscriptionTranslog;

    @Override
    public void generate() {
        logger.info("SubscriptionLogGeneraterImpl invoked");
        generateTotalSubscriptionTranslog.findRegisteredApps(currentStatus);
    }

    public void setGenerateTotalSubscriptionTranslog(GenerateTotalSubscriptionTranslog generateTotalSubscriptionTranslog) {
        this.generateTotalSubscriptionTranslog = generateTotalSubscriptionTranslog;
    }
}
