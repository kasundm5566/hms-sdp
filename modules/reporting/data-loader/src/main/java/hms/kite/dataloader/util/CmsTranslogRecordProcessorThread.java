package hms.kite.dataloader.util;

/**
 * Created with IntelliJ IDEA.
 * User: dulanjaleen
 * Date: 2/15/13
 * Time: 5:34 PM
 * To change this template use File | Settings | File Templates.
 */
import hms.kite.dataloader.domain.CmsTranslogEntry;
import hms.kite.dataloader.domain.ServiceProviderInfo;
import hms.kite.dataloader.repository.impl.CmsTranslogRepositoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CmsTranslogRecordProcessorThread implements Runnable {

    private static final String NEWLINE_CHARACTER = "\n";

    private String record;
    private String columnSeparator;
    private String columnSeparatorAppender;
    private CmsTranslogEntry cmsTranslogEntry;
    private CmsTranslogRepositoryImpl cmsTranslogRepository;
    private String fileType;


    private static final Logger logger = LoggerFactory.getLogger(CmsTranslogRecordProcessorThread.class);

    public CmsTranslogRecordProcessorThread(String record, String columnSeparator,
                                               String columnSeparatorAppender,
                                               CmsTranslogRepositoryImpl cmsTranslogRepository,
                                               String fileType) {
        if (record.endsWith(NEWLINE_CHARACTER)) {
            record = record.substring(0, record.length() - 1);
        }
        this.record = record;
        this.columnSeparator = columnSeparator;
        this.columnSeparatorAppender = columnSeparatorAppender;
        this.cmsTranslogRepository = cmsTranslogRepository;
        this.fileType = fileType;
    }

    @Override
    public void run() {
        double revenueSharePercentage = 0.0;
        double chargeAmount = 0.0;

        String[] columnValues = record.split("\\|", 1000);

        cmsTranslogEntry = new CmsTranslogEntry(columnValues);
        cmsTranslogEntry.setFileType(fileType);
        chargeAmount = Double.valueOf(cmsTranslogEntry.getChargeAmount());
        cmsTranslogEntry.setSysCurChargeAmount(chargeAmount);

        ServiceProviderInfo serviceProviderInfo = cmsTranslogRepository.getSpDetail(columnValues[2],columnValues[3]);
        cmsTranslogEntry.setRevenueSharePercentage(Double.valueOf(serviceProviderInfo.getRevenueSharePercentage()));
        cmsTranslogEntry.setSpName(serviceProviderInfo.getSpName());
        cmsTranslogEntry.setAppName(serviceProviderInfo.getAppName());
        revenueSharePercentage = cmsTranslogEntry.getRevenueSharePercentage();
        logger.info("revenue Share percentage for app [" + cmsTranslogEntry.getAppName() + "] is [{}]", revenueSharePercentage);

        double systemCurrencySPProfit = TaxUtil.calculateSpProfit(chargeAmount, revenueSharePercentage);
        logger.info("spProfit value [{}]", systemCurrencySPProfit) ;
        cmsTranslogEntry.setSysCurSpProfit(systemCurrencySPProfit);

        cmsTranslogEntry.setSysCurVcProfit(chargeAmount - systemCurrencySPProfit);
        try {
            cmsTranslogRepository.save(cmsTranslogEntry);
            cmsTranslogRepository.saveMongo(cmsTranslogEntry);
        } catch (Exception e) {
            //todo: Send SNMP trap
            logger.error("Couldn't save the record [" + record + "] Unexpected exception thrown while saving the record", e);
        }


    }

}
