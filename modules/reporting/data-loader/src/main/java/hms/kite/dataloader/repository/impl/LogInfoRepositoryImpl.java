/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.repository.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.SQLException;


public class LogInfoRepositoryImpl {
    JdbcTemplate jdbcTemplate;
    private final static Logger logger = LoggerFactory.getLogger(LogInfoRepositoryImpl.class);

    public boolean isInsertedFile(String  fileName) throws SQLException {
        logger.info("Checking the file [{}] whether its already processed", fileName);
        try{
            jdbcTemplate.queryForObject("SELECT `file_name` FROM `log_info` WHERE file_name=?",new Object[]{fileName},String.class) ;
        }catch (EmptyResultDataAccessException e){
            return false;
        }
        return true;
    }

    public String getProcessedTime(String fileName) throws SQLException{
        return jdbcTemplate.queryForObject("SELECT  `processed_time` FROM `log_info` WHERE `file_name`=?",new Object[]{fileName},String.class);
    }

    public void insertFileInfo(String fileName,String fileType) throws SQLException{
        jdbcTemplate.update("INSERT INTO `log_info`(`file_name`, `processed_time`,`file_type`) VALUES (?, NOW(),?)",fileName,fileType);
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}
