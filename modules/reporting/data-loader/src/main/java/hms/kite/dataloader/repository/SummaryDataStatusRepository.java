/*
 * (C) Copyright 1996-2012 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.dataloader.repository;

import hms.kite.dataloader.domain.SummaryDataStatus;

import java.sql.SQLException;
import java.util.Date;


public interface SummaryDataStatusRepository {

    /**
     *
     * @param procedureName Stored procedure name
     * @return return date when the stored procedure is executed last
     * @throws SQLException
     */
    public Date getLastExecutedDate(String procedureName) throws SQLException;

    /**
     *
     * @param date last executed date
     * @param procedureName stored procedure name
     * @param status completed or started state
     * @return return summaryDataStatus. If the specified status is not available then returns null
     * @throws SQLException
     */
    public SummaryDataStatus getStatus(Date date, String procedureName, int status) throws SQLException;

    /**
     * Inserts the stored procedure started and completed state into summary_data_status
     * @param summaryDataStatus status to be insert
     * @throws SQLException
     */
    public void save(SummaryDataStatus summaryDataStatus) throws SQLException;

    /**
     * Deletes the started state summary data from the summary_data_status when executing the stated state execution
     * @param summaryDataStatus
     * @throws SQLException
     */
    public void delete(SummaryDataStatus summaryDataStatus) throws SQLException;

}
