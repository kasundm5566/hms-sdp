package hms.kite.dataloader.util;

public class TaxUtil {

    public static double calculateSpProfit(double chargeAmount,
                                           double revenueSharePercentage) {

        double taxPercentage = Double.parseDouble(
                System.getProperty("data-loader.tax.percentage", "0.0"));
        return (chargeAmount * (1 - taxPercentage / 100)) * revenueSharePercentage / 100;
    }
}