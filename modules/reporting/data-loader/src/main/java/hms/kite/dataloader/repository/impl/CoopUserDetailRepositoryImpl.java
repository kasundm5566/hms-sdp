/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.repository.impl;

import hms.kite.dataloader.repository.TransactionRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import hms.kite.dataloader.domain.TranslogEntry;

import java.sql.SQLException;

public class CoopUserDetailRepositoryImpl implements TransactionRepository {

    private JdbcTemplate jdbcTemplate;

    private static final String SAVE_SQL_STATEMENT_BANK = " insert into `corporate_user_info` values (?, ?, ?, ?, ?, ?, ?) " +
            "on duplicate key update `sp_beneficiary`= ?,`sp_beneficiary_name` = ?,  `sp_beneficiary_bank_code` = ?," +
            " `sp_beneficiary_branch_code` = ?, `sp_beneficiary_branch_name` = ?," +
            " `sp_beneficiary_bank_account_number` = ?  ;";

    private static final String SAVE_SQL_STATEMENT_BUSINESS_ID = "insert into `corporate_user_info_business_no` values " +
            "(?,?,?,?,?) on duplicate key update `service_provider_type`=?, `tin`=?, " +
            "`business_licence_number`=?, `mpaisa_account_number`=?";

      public void save(String record, String columnSeparator) throws SQLException {
        String[] columnValues = record.split(columnSeparator, -1);
        if(columnValues[1] != null && columnValues[1].startsWith("{")){
            jdbcTemplate.update(SAVE_SQL_STATEMENT_BANK, columnValues[0], columnValues[1], columnValues[2], columnValues[3],
                    columnValues[4], columnValues[5], columnValues[6], columnValues[1], columnValues[2], columnValues[3], columnValues[4],
                    columnValues[5], columnValues[6]);

        } else if(columnValues != null && columnValues[1]!="") {
            jdbcTemplate.update(SAVE_SQL_STATEMENT_BUSINESS_ID, columnValues[0], columnValues[1], columnValues[2],
                    columnValues[3], columnValues[4], columnValues[1], columnValues[2],
                    columnValues[3], columnValues[4]);
        }
    }

    @Override
    public void save(TranslogEntry entry) throws SQLException {
        //To change body of implemented methods use File | Settings | File Templates.
    }


    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}
