/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.dataloader.util;

import hms.kite.dataloader.domain.Currency;
import hms.kite.dataloader.domain.SdpTranslogEntry;
import hms.kite.dataloader.repository.TransactionRepository;
import hms.kite.dataloader.service.JsonDecoderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class SdpTransactionRecordProcessorThread implements Runnable {

    private static final String NEWLINE_CHARACTER = "\n";

    private String record;
    private String columnSeparator;
    private String columnSeparatorAppender;
    private int currencyKeyPosition;
    private int currencyJsonIndex;
    private JsonDecoderService jsonDecoderService;
    private TransactionRepository transactionRepository;
    SdpTranslogEntry sdpTransalogEntry;
    private String fileType;

    private static final Logger logger = LoggerFactory.getLogger(SdpTransactionRecordProcessorThread.class);

    public SdpTransactionRecordProcessorThread(String record, String columnSeparator,
                                               String columnSeparatorAppender,
                                               int currencyJsonIndex,
                                               JsonDecoderService jsonDecoderService,
                                               TransactionRepository transactionRepository,
                                               String fileType) {
        if (record.endsWith(NEWLINE_CHARACTER)) {
            record = record.substring(0, record.length() - 1);
        }
        this.record = record;
        this.columnSeparator = columnSeparator;
        this.columnSeparatorAppender = columnSeparatorAppender;
        this.currencyJsonIndex = currencyJsonIndex;
        this.jsonDecoderService = jsonDecoderService;
        this.transactionRepository = transactionRepository;
        this.fileType = fileType;
        currencyKeyPosition = 21;
    }

    @Override
    public void run() {
        double revenueSharePercentage = 0.0;
        double systemCurrencyChargeAmount = 0.0;
        double chargeAmount = 0.0;

        String[] columnValues = record.split("\\|", 1000);
        Map<String, Currency> currencyMap = jsonDecoderService.decodeCurrency(columnValues[currencyJsonIndex - 1]);

        sdpTransalogEntry = new SdpTranslogEntry(columnValues);
        sdpTransalogEntry.setFileType(fileType);
        if (currencyMap != null && currencyMap.containsKey(columnValues[currencyKeyPosition - 1])) {
            Currency currency = currencyMap.get(columnValues[currencyKeyPosition - 1]);
            sdpTransalogEntry.setBuyingRate(currency.getBuyingRate());
            sdpTransalogEntry.setSellingRate(currency.getSellingRate());
        }

        chargeAmount = Double.valueOf(sdpTransalogEntry.getChargeAmount());

        systemCurrencyChargeAmount = chargeAmount * sdpTransalogEntry.getBuyingRate();
        sdpTransalogEntry.setSysCurChargeAmount(systemCurrencyChargeAmount);
        revenueSharePercentage = Double.valueOf(sdpTransalogEntry.getRevenueSharePercentage());
        double systemCurrencySPProfit = TaxUtil.calculateSpProfit(systemCurrencyChargeAmount, revenueSharePercentage);
        sdpTransalogEntry.setSysCurSpProfit(systemCurrencySPProfit);
        sdpTransalogEntry.setSysCurVcProfit(systemCurrencyChargeAmount - systemCurrencySPProfit);
        try {
            transactionRepository.save(sdpTransalogEntry);
        } catch (Exception e) {
            //todo: Send SNMP trap
            logger.error("Couldn't save the record [" + record + "] Unexpected exception thrown while saving the record", e);
        }
    }
}
