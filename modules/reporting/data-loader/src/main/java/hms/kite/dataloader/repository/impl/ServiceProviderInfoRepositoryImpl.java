/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.repository.impl;

import hms.kite.dataloader.domain.TranslogEntry;
import hms.kite.dataloader.repository.TransactionRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import java.sql.SQLException;
import java.util.List;

public class ServiceProviderInfoRepositoryImpl implements TransactionRepository {
    private JdbcTemplate jdbcTemplate;

    private static final String SAVE_SQL_STATEMENT = "insert into `service_provider_info` " +
            "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)  on duplicate key update `sp_name`=?, `app_name`=?, `app_state`=?;";

    private static final String TERMINATED_APP_SQL_STATEMENT = "select `app_id` from `service_provider_info` " +
            "where app_state='terminate'";

    private static final String SAVE_PI_SQL_STATEMENT = "insert into `sp_payment_instrument_info` " +
            "values (?, ?, ?, ?, ?);";

    @Override
    public void save(String record, String columnSeparator) throws SQLException {
        String[] columnValues = record.split(columnSeparator, -1);
        if (columnValues[7].trim().equals("")) {
            columnValues[7] = String.valueOf(0);
        }
        jdbcTemplate.update(SAVE_SQL_STATEMENT, columnValues[0], columnValues[1],
                columnValues[2], columnValues[3], columnValues[4], columnValues[5], columnValues[6],
                columnValues[7], columnValues[9], columnValues[10], columnValues[1], columnValues[4], columnValues[9]);
    }

    @Override
    public void save(TranslogEntry entry) throws SQLException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public List<String> getTerminatedApps() {
        return jdbcTemplate.queryForList(TERMINATED_APP_SQL_STATEMENT, String.class);
    }

    public void saveSpPaymentInstrumentInfo(String spId, String appId, String ncs, String paymentInstrument,
                                            String additionalInfo) {
        jdbcTemplate.update(SAVE_PI_SQL_STATEMENT, spId, appId, ncs, paymentInstrument, additionalInfo);
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}