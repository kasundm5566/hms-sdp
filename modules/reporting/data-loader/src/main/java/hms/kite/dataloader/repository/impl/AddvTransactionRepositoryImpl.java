/*
 *
 *  *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *  *   All Rights Reserved.
 *  *
 *  *   These materials are unpublished, proprietary, confidential source code of
 *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *   of hSenid Software International (Pvt) Limited.
 *  *
 *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *   property rights in these materials.
 *  *
 *
 */
package hms.kite.dataloader.repository.impl;

import hms.kite.dataloader.domain.TranslogEntry;
import hms.kite.dataloader.repository.TransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.SQLException;

public class AddvTransactionRepositoryImpl implements TransactionRepository {
    private JdbcTemplate jdbcTemplate;
    private final static Logger logger = LoggerFactory.getLogger(AddvTransactionRepositoryImpl.class);
    private final static String SAVE_SQL_STATEMENT = "INSERT INTO `advertisement` VALUES (?, ?) on duplicate key update `content`=? ";

    @Override
    public void save(String record, String columnSeparator) throws SQLException {
        String [] columnValues = record.split(columnSeparator, -1);
        jdbcTemplate.update(SAVE_SQL_STATEMENT,columnValues[0],columnValues[1],columnValues[1]);
    }

    @Override
    public void save(TranslogEntry entry) throws SQLException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}
