/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.dataloader.repository.impl;

import hms.kite.dataloader.domain.SummaryDataStatus;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

@ContextConfiguration(locations = "classpath*:test-data-loader-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class SummaryDataStatusRepositoryImplTest {

    @Autowired
    SummaryDataStatusRepositoryImpl summaryDataStatusRepository;

    @Autowired
    JdbcTemplate jdbcTemplate;


    final int requestedDateYear = 2061;
    final int requestedDateMonth = 11;
    final int requestedDateDate = 16;

    final int startDateTimeYear = 2010;
    final int startDateTimeMonth = 3;
    final int startDateTimeDate = 12;
    final int startDateTimeHours = 23;
    final int startDateTimeMinutes = 15;
    final int startDateTimeSeconds = 19;

    final int endDateTimeYear = 2010;
    final int endDateTimeMonth = 3;
    final int endDateTimeDate = 12;
    final int endDateTimeHours = 23;
    final int endDateTimeMinutes = 15;
    final int endDateTimeSeconds = 20;

    final String storedProcedureName = "stored_procedure";
    final int storedProcStatus = 15;

    String stringRequestedDate, stringStartDateTime, stringEndDateTime;

    Date requestedDate, startDateTime, endDateTime;
    Calendar calendar = Calendar.getInstance();

    @Before
    public void setUp() throws Exception {
        setDateVariables();
        if (getNumberOfRecords() != 0) {
            deleteRecords();
        }
    }

    @After
    public void tearDown() throws Exception {
        if (getNumberOfRecords() != 0) {
            deleteRecords();
        }
    }

    @Test
    public void testSave() throws Exception {
        SummaryDataStatus summaryDataStatus = new SummaryDataStatus(storedProcedureName,
                storedProcStatus, startDateTime, endDateTime, requestedDate);
        summaryDataStatusRepository.save(summaryDataStatus);
        assertEquals(1, getNumberOfRecords());
    }

    @Test
    public void testGetLastExecutedDate() throws Exception {
        if (getNumberOfRecords() == 0) {
            insertRecords();
        }
        Date date = summaryDataStatusRepository.getLastExecutedDate(storedProcedureName);
        String fullDate = getStringDate(date);
        assertEquals(stringRequestedDate, fullDate);
    }

    @Test
    public void testGetStatus() throws Exception {
        SummaryDataStatus summaryDataStatus = new SummaryDataStatus(storedProcedureName,
                storedProcStatus, startDateTime, endDateTime, requestedDate);
        if (getNumberOfRecords() == 0) {
            insertRecords();
        }
        SummaryDataStatus newSummaryDataStatus = summaryDataStatusRepository.getStatus(
                requestedDate, storedProcedureName, storedProcStatus);
        checkEqualitySummaryDataStatus(summaryDataStatus, newSummaryDataStatus);
    }

    @Test
    public void testDelete() throws Exception {
        if (getNumberOfRecords() == 0) {
            insertRecords();
        }
        SummaryDataStatus summaryDataStatus = new SummaryDataStatus(storedProcedureName,
                storedProcStatus, startDateTime, endDateTime, requestedDate);
        summaryDataStatusRepository.delete(summaryDataStatus);
        assertEquals(0, getNumberOfRecords());
    }

    private void setDateVariables() {
        calendar.set(requestedDateYear, requestedDateMonth - 1, requestedDateDate);
        requestedDate = calendar.getTime();
        calendar.set(startDateTimeYear, startDateTimeMonth - 1, startDateTimeDate,
                startDateTimeHours, startDateTimeMinutes, startDateTimeSeconds);
        startDateTime = calendar.getTime();
        calendar.set(endDateTimeYear, endDateTimeMonth - 1, endDateTimeDate,
                endDateTimeHours, endDateTimeMinutes, endDateTimeSeconds);
        endDateTime = null;// calendar.getTime();
        stringRequestedDate = getStringDate(requestedDate);
        stringStartDateTime = getStringDateTime(startDateTime);
        stringEndDateTime = getStringDateTime(endDateTime);
    }

    private void checkEqualitySummaryDataStatus(SummaryDataStatus expected,
                                                SummaryDataStatus actual) {
        assertEquals(expected.getStatus(), actual.getStatus());
        assertEquals(expected.getProcedureName(), actual.getProcedureName());
        assertEquals(expected.getRequestedDate(), actual.getRequestedDate());
        assertEquals(getStringDateTime(expected.getStartDateTime()),
                getStringDateTime(actual.getStartDateTime()));
        assertEquals(getStringDateTime(expected.getEndDateTime()),
                getStringDateTime(actual.getEndDateTime()));
    }

    private String getStringDate(Date date) {
        if (date == null)
            return null;
        Format formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(date);
    }

    private String getStringDateTime(Date date) {
        if (date == null)
            return null;
        Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return formatter.format(date);
    }

    private int getNumberOfRecords() {
        return jdbcTemplate.queryForInt("select count(*) from `summary_data_status` where " +
                "`procedure_name` = ? and `requested_date` = ?", storedProcedureName,
                stringRequestedDate);
    }

    private void deleteRecords() {
        jdbcTemplate.update("delete from `summary_data_status` where `procedure_name` = ? " +
                "and `requested_date` = ?", storedProcedureName, stringRequestedDate);
    }

    private void insertRecords() {
        jdbcTemplate.update("insert into `summary_data_status` (`procedure_name`, `requested_date`, " +
                "`start_date_time`, `end_date_time`, `status`) values (?, ?, ?, ?, ?)",
                storedProcedureName, stringRequestedDate, stringStartDateTime, stringEndDateTime,
                storedProcStatus);
    }
}
