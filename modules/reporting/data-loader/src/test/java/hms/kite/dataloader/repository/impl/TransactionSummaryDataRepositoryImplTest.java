/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.dataloader.repository.impl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

@ContextConfiguration(locations = "classpath*:test-data-loader-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class TransactionSummaryDataRepositoryImplTest {


    @Autowired
    TransactionSummaryDataRepositoryImpl transactionSummaryDataRepository;

    @Autowired
    JdbcTemplate jdbcTemplate;

    Calendar calendar = Calendar.getInstance();


    @Before
    public void setUp() throws Exception {
        int value = getSpId();
        if(value == 0){
            Integer val = -1;
            jdbcTemplate.update("insert into `sdp_transaction_summary` values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", val,
                    "spId", "sp_name", "appId", "app_name", val, val, "dr", val, val);
        }
        calendar.set(2000, 0, 1);
        jdbcTemplate.update("insert into `date_info` (`date_id`, `full_date`) values (?, ?)",
                new Integer(-1), getStringDate(calendar.getTime()));
    }

    @After
    public void tearDown() throws Exception {
        int value = getSpId();
        if(value != 0){
            jdbcTemplate.update("delete from `sdp_transaction_summary` where `date_id` = ?", new Integer(-1));
        }
        jdbcTemplate.update("delete from `date_info` where `date_id` = ?", new Integer(-1));
    }

    @Test
    public void testDelete() throws Exception {
        calendar.set(2000, 0, 1);
        transactionSummaryDataRepository.delete(calendar.getTime());
        assertEquals(0, getSpId());
    }

    private String getStringDate(Date date) {
        Format formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(date);
    }

    private int getSpId(){
        return jdbcTemplate.queryForInt("select count(*) from `sdp_transaction_summary` where" +
                "`date_id` = ?", new Integer(-1));
    }

}
