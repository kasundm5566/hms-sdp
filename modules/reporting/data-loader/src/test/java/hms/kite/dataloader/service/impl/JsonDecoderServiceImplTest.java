package hms.kite.dataloader.service.impl;

import hms.kite.dataloader.domain.Currency;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: hms
 * Date: 6/27/11
 * Time: 3:34 PM
 * To change this template use File | Settings | File Templates.
 */

@ContextConfiguration(locations = "classpath*:test-data-loader-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class JsonDecoderServiceImplTest {
    @Autowired
    JsonDecoderServiceImpl jsonDecoderService;

    private static final String record ="1317352403170|2011-09-30 08:43:23|SPP_000001|corporate|APP_000002|sandaApp1|live|254701234562|sms|smpp||sms|http|mo|sms|unknown|application|12|KES|{\"currencyCode\":\"KES\", \"buyingRate\": 1, \"sellingRate\": 1}|sms|SPP_000001||S1000|Request was successfully processed|success|key002|1234|safaricom||PERCENTAGE_FROM_MONTHLY_REVENUE|10||||||111093008430254|flat";
    private static final String record2 ="1|2011-8-3 4:11:42|sp1|sp1_spname|app2|app2_appname|limitedLive|11|cas|ussd|11|push|http|mo|sms|postpaid|subscriber|1.61|USD|[{\"currencyCode\":\"USD\",\"buyingRate\":2.50,\"sellingRate\":2.75},{\"currencyCode\":\"KES\",\"buyingRate\":1,\"sellingRate\":1}]|sms-mo|11|authentication|5|11|error|11|11|Airtel|11|PERCENTAGE_FROM_MONTHLY_REVENUE|11|add2|11|0|11|11|11|FLAT";

    @Test
    public void testDecode() {
//        Map<String, LineItem> lineItemMap = jsonDecoderService.decodeLineItem(record);
//
//        for (String keyVal : lineItemMap.keySet()) {
//            System.out.println(keyVal + "\t" + lineItemMap.get(keyVal).getAccountId() + "\t" + lineItemMap.get(keyVal).getAmount().getCurrencyCode());
//            System.out.println();
//        }
//

    }

    @Test
    public void testGetJsonString() {
//        String jstr = jsonDecoderService.getLineItemObj(record);
//        System.out.println(jstr);
    }

    @Test
    public void testDecodeCurrency() {
//        Map<String, Currency> currencyMap = jsonDecoderService.decodeCurrency(record,"\\|",20);
//        for(String key:currencyMap.keySet()){
//            System.out.println(key+"\t"+currencyMap.get(key).getBuyingRate()+"\t"+currencyMap.get(key).getSellingRate());
        }
    }

    /*@Before
    public void setVals() {
        jsonDecoderService.setSeparator("\\|");
        jsonDecoderService.setLineItemIndex(13);
    }*/
//}
