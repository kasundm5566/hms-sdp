/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.dataloader.repository.impl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

@ContextConfiguration(locations = "classpath*:test-data-loader-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class DateInfoRepositoryImplTest {

    @Autowired
    DateInfoRepositoryImpl dateInfoRepository;

    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final Integer dateId = -1;
    private static final int intYear = 2001;
    private static final int intMonth = 3;
    private static final int intDate = 14;

    private static final String dateFormat = "yyyy-MM-dd";

    private String stringDate;
    private Date date;

    @Before
    public void setUp() throws Exception {
        initializeVariables();
        if(getNumberOfRecords() != 0) {
            deleteFromDatabase();
        }
    }

    @After
    public void tearDown() throws Exception {
        if(getNumberOfRecords() != 0) {
            deleteFromDatabase();
        }
    }

    @Test
    public void testGetDateId() throws Exception {
        insertIntoDatabase();
        assertEquals(-1, dateInfoRepository.getDateId(date));
    }

    private void insertIntoDatabase() {
        this.jdbcTemplate.update("insert into `date_info` (`date_id`, `full_date`) " +
                "values (?, ?)", dateId, stringDate);
    }

    private int getNumberOfRecords() {
        return this.jdbcTemplate.queryForInt("select count(*) from `date_info` " +
                "where `date_id` = ? and `full_date` = ?", dateId, stringDate);
    }

    private void deleteFromDatabase() {
        this.jdbcTemplate.update("delete from `date_info` where `date_id` = ? " +
                "and `full_date` = ?", dateId, stringDate);
    }

    private void initializeVariables() {
        Format formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.set(intYear, intMonth, intDate);
        date = calendar.getTime();
        stringDate = formatter.format(date);
    }

}
