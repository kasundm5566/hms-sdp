/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.dataloader.domain;

import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SummaryDataStatusTest {

    SummaryDataStatus summaryDataStatus = new SummaryDataStatus();
    Calendar calendar = Calendar.getInstance();

    @Test
    public void testGetEndDateTime() throws Exception {
        calendar.set(2011, 5, 23, 15, 19, 42);
        summaryDataStatus.setEndDateTime(calendar.getTime());
        assertEquals(calendar.getTime(), summaryDataStatus.getEndDateTime());
    }

    @Test
    public void testGetProcedureName() throws Exception {
        String procedureName = "stored procedure";
        summaryDataStatus.setProcedureName(procedureName);
        assertEquals(procedureName, summaryDataStatus.getProcedureName());
    }

    @Test
    public void testGetRequestedDate() throws Exception {
        calendar.set(2011, 10, 5, 25, 52, 31);
        summaryDataStatus.setEndDateTime(calendar.getTime());
        assertEquals(calendar.getTime(), summaryDataStatus.getEndDateTime());
    }

    @Test
    public void testGetStartDateTime() throws Exception {
        calendar.set(2014, 9, 26, 23, 56, 41);
        summaryDataStatus.setEndDateTime(calendar.getTime());
        assertEquals(calendar.getTime(), summaryDataStatus.getEndDateTime());
    }

    @Test
    public void testGetStatus() throws Exception {
        int status = 12;
        summaryDataStatus.setStatus(status);
        assertEquals(status, summaryDataStatus.getStatus());
    }
}
