/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.dataloader.util;

import org.junit.Test;

import java.io.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class FileHandlerTest {

    FileHandler fileHandler = new FileHandler();

    //for IDE relative path     data-loader/src/test/test_data
    //for terminal relative path     src/test/test_data

    private String filePath = "src/test/test_data";
    private String fileNamePattern = ".{0,}\\.csv";

    //for IDE relative path     data-loader/src/test/test_data
    //for terminal relative path     src/test/test_data
    private String fileName = "data.csv";
    private String destinationPath = "src/test/test_data";

    //for IDE relative path     data-loader/src/test/test_data/abc123.txt
    //for terminal relative path     src/test/test_data/abc123.txt
    private String writeFile = "src/test/test_data/abc123.txt";
    private String writeText = "Hello World\n";

    @Test
    public void testGetMatchingFiles() throws Exception {
        String[] files = fileHandler.getMatchingFiles(filePath, fileNamePattern);
        if(files != null) {
            for(String file : files) {
                assertTrue(file.matches(fileNamePattern));
            }
        }
    }

    @Test
    public void testMoveDataFile() throws Exception {
        boolean result = fileHandler.moveDataFile(filePath + fileName, destinationPath);
        if(result) {
            assertEquals(1, fileHandler.getMatchingFiles(destinationPath, fileName).length);
            fileHandler.moveDataFile(destinationPath + fileName, filePath);
        }
    }

    @Test
    public void testGetDataOfFile() throws Exception {
        writeIntoFile();
        String text = fileHandler.getDataOfFile(writeFile);
        assertEquals(writeText, text);
        deleteFile();
    }

    private void writeIntoFile() {
        File file = new File(writeFile);
        try {
            Writer writer = new BufferedWriter(new FileWriter(file));
            writer.write(writeText);
            writer.close();
        } catch (IOException ignored) { }
    }

    private boolean deleteFile() {
        File file = new File(writeFile);
        boolean status = true;
        if(file.exists()) {
            status = file.delete();
        }
        return status;
    }

}
