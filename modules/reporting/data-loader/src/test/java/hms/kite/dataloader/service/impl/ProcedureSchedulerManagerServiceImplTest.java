/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.dataloader.service.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

@ContextConfiguration(locations = "classpath*:test-data-loader-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class ProcedureSchedulerManagerServiceImplTest {

    @Autowired
    ProcedureSchedulerManagerServiceImpl procedureSchedulerManagerService;

    @Test
    public void testGetDateDifference() throws Exception {
        int difference = procedureSchedulerManagerService.getDateDifference(new Date());
        assertEquals(-1, difference);
    }

    @Test
    public void testSetSummaryDataStatusRepository() throws Exception {

    }

    @Test
    public void testSetProcedureName() throws Exception {

    }

    @Test
    public void testSetSummaryDataRepository() throws Exception {

    }

    @Test
    public void testSetDataLoaderService() throws Exception {

    }

    @Test
    public void testExecute() throws Exception {

    }
}
