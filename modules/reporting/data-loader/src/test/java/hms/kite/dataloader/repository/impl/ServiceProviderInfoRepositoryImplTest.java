/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.dataloader.repository.impl;

import hms.kite.dataloader.domain.ServiceProviderInfo;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

@ContextConfiguration(locations = "classpath*:test-data-loader-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class ServiceProviderInfoRepositoryImplTest {

    @Autowired
    ServiceProviderInfoRepositoryImpl serviceProviderInfoRepository;

    @Autowired
    JdbcTemplate jdbcTemplate;

    final String spId = "spId";
    final String appId = "appId";
    final String spName1 = "spName1";
    final String spName2 = "spName2";
    final String appName1 = "appName1";
    final String appName2 = "appName2";

    @Before
    public void setUp() throws Exception {
        if(numberOfRecords() != 0) {
            deleteTheRecord();
        }
    }

    @After
    public void tearDown() throws Exception {
        if(numberOfRecords() != 0) {
            deleteTheRecord();
        }
    }

    @Test
    public void testSave() throws Exception {
        ServiceProviderInfo serviceProviderInfo1 = new ServiceProviderInfo(spId,
                spName1, appId, appName1, "70");
        serviceProviderInfoRepository.save(serviceProviderInfo1.toString(),",");
        assertEquals(1, numberOfRecords());
        checkServiceProviderInfoEquality(serviceProviderInfo1, getServiceProviderInfo());
        ServiceProviderInfo serviceProviderInfo2 = new ServiceProviderInfo(spId,
                spName2, appId, appName2, "70");
        serviceProviderInfoRepository.save(serviceProviderInfo2.toString(),",");
        assertEquals(1, numberOfRecords());
        checkServiceProviderInfoEquality(serviceProviderInfo2, getServiceProviderInfo());
    }

    private int numberOfRecords() {
        return jdbcTemplate.queryForInt("select count(*) from `service_provider_info` " +
                "where `sp_id` = ? and `app_id` = ?", spId, appId);
    }

    private void deleteTheRecord() {
        jdbcTemplate.update("delete from `service_provider_info` where `sp_id` = ? " +
                "and `app_id` = ?", spId, appId);
    }

    private ServiceProviderInfo getServiceProviderInfo() {
        return jdbcTemplate.queryForObject("select * from `service_provider_info` where `sp_id` = ? " +
                "and `app_id` = ?", new Object[] {spId, appId},
                new RowMapper<ServiceProviderInfo>() {
                    public ServiceProviderInfo mapRow(ResultSet resultSet, int i) throws SQLException {
                        ServiceProviderInfo serviceProviderInfo = new ServiceProviderInfo();
                        serviceProviderInfo.setSpId(spId);
                        serviceProviderInfo.setAppId(appId);
                        serviceProviderInfo.setSpName(resultSet.getString("sp_name"));
                        serviceProviderInfo.setAppName(resultSet.getString("app_name"));
                        return serviceProviderInfo;
                    }
                });
    }

    private void checkServiceProviderInfoEquality(ServiceProviderInfo expected, ServiceProviderInfo actual) {
        assertEquals(expected.getSpId(), actual.getSpId());
        assertEquals(expected.getSpName(), actual.getSpName());
        assertEquals(expected.getAppId(), actual.getAppId());
        assertEquals(expected.getAppName(), actual.getAppName());
    }
}
