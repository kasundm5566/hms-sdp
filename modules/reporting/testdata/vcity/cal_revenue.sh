#!/bin/bash

cd sdp_translog
printf "Overall revenue  for year 2011 \n "
grep -r 'S1000' .|grep '|2011-' | grep 'KES'| cut -d "|" -f 20 > overallrev.txt

#sum=0;
#while read num;
#do
#sum=$(($sum + $num));
#done < overallrev.txt; 
#echo $sum

awk '{ SUM += $1} END { printf "%.2f \n",  SUM }' overallrev.txt
#-----------------------------------------------------------------------------------------------------------------------
printf "Overall revenue  for year 2012 \n "
grep -r 'S1000' .|grep '|2012-' | grep 'KES'| cut -d "|" -f 20 > overallrev2012.txt

awk '{ SUM += $1} END { printf "%.2f \n",  SUM }' overallrev2012.txt
#------------------------------------------------------------------------------------------------------------------------

printf "Overall revenue  for USSD NCS for year  2012 \n "
grep -r 'S1000' .|grep '|2012-' | grep 'KES'|grep 'ussd'| cut -d "|" -f 20 > rev2012ussd.txt

awk '{ SUM += $1} END { printf "%.2f \n",  SUM }' rev2012ussd.txt

#-------------------------------------------------------------------------------------------------------------------------

printf "Overall revenue  for USSD NCS for year  2011 \n "
grep -r 'S1000' .|grep '|2011-' | grep 'KES'|grep 'ussd'| cut -d "|" -f 20 > rev2011ussd.txt

awk '{ SUM += $1} END { printf "%.2f \n",  SUM }' rev2011ussd.txt

#-------------------------------------------------------------------------------------------------------------------------

printf "Overall revenue  for CAAS NCS for year  2011 \n "
grep -r 'S1000' .|grep '|2011-' | grep 'KES'|grep 'cas'| cut -d "|" -f 20 > rev2011cas.txt

awk '{ SUM += $1} END { printf "%.2f \n",  SUM }' rev2011cas.txt

#--------------------------------------------------------------------------------------------------------------------------

printf "Overall revenue  for CAAS NCS for year  2012 \n "
grep -r 'S1000' .|grep '|2012-' | grep 'KES'|grep 'cas'| cut -d "|" -f 20 > rev2012cas.txt

awk '{ SUM += $1} END { printf "%.2f \n",  SUM }' rev2012cas.txt

#--------------------------------------------------------------------------------------------------------------------------

printf "Overall revenue  for SMS MO NCS for year  2012 \n "
grep -r 'S1000' .|grep '|2012-' | grep 'KES'|grep 'sms'| grep 'mo'|cut -d "|" -f 20 > rev2012smsmo.txt

awk '{ SUM += $1} END { printf "%.2f \n",  SUM }' rev2012smsmo.txt

#--------------------------------------------------------------------------------------------------------------------------

printf "Overall revenue  for SMS MO NCS for year  2011 \n "
grep -r 'S1000' .|grep '|2011-' | grep 'KES'|grep 'sms'| grep 'mo'|cut -d "|" -f 20 > rev2011smsmo.txt

awk '{ SUM += $1} END { printf "%.2f \n",  SUM }' rev2011smsmo.txt

#---------------------------------------------------------------------------------------------------------------------------

printf "Overall revenue  for SMS MT NCS for year  2011 \n "
grep -r 'S1000' .|grep '|2011-' | grep 'KES'|grep 'sms'| grep 'mt'|cut -d "|" -f 20 > rev2011smsmt.txt

awk '{ SUM += $1} END { printf "%.2f \n",  SUM }' rev2011smsmt.txt

#---------------------------------------------------------------------------------------------------------------------------

printf "Overall revenue  for SMS MT NCS for year  2012 \n "
grep -r 'S1000' .|grep '|2012-' | grep 'KES'|grep 'sms'| grep 'mt'|cut -d "|" -f 20 > rev2012smsmt.txt

awk '{ SUM += $1} END { printf "%.2f \n",  SUM }' rev2012smsmt.txt

#-----------------------------------------------------------------------------------------------------------------------------

printf "Overall revenue  for anjulaw sp user for year  2011 \n "
grep -r 'S1000' .|grep '|2011-' | grep 'KES'|grep 'SPP_000059'|cut -d "|" -f 20 > rev2011anjulaw.txt

awk '{ SUM += $1} END { printf "%.2f \n",  SUM }' rev2011anjulaw.txt

#-----------------------------------------------------------------------------------------------------------------------------

printf "Overall revenue  for testuser sp user for year  2011 \n "
grep -r 'S1000' .|grep '|2011-' | grep 'KES'|grep 'SPP_000008'|cut -d "|" -f 20 > rev2011testuser.txt

awk '{ SUM += $1} END { printf "%.2f \n",  SUM }' rev2011testuser.txt

#-----------------------------------------------------------------------------------------------------------------------------


rm *.txt ;







