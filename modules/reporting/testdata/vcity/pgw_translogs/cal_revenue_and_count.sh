#!/bin/bash

printf "total transaction count in the data set \n"
cat pgw-core-trans* | wc -l;
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------
printf "total success transaction1 count in M-Pesa in year 2011 \n"
grep -r 'M-Pesa' pgw-core-trans* | grep '2011'| grep 'AMOUNT_FULLY_PAID\|PARTIAL_PAYMENT_SUCCESS' |wc -l;

#--------------------------------------------------------------------------------------------------------------------------------------------------------------------
printf "total success transaction count in equity bank in year 2011\n" 
grep -r 'Equity Bank' pgw-core-trans* | grep '2011'| grep 'AMOUNT_FULLY_PAID\|PARTIAL_PAYMENT_SUCCESS' |wc -l;

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------
printf "total success transaction count in Mobile Account in year 2011\n"
grep -r 'Mobile Account' pgw-core-trans* | grep '2011' |grep 'AMOUNT_FULLY_PAID\|PARTIAL_PAYMENT_SUCCESS'|wc -l;

#------------------------------------------------------------------------------------------------------------------------------------------------------------------
printf "total success transaction count in Voucher in year 2011\n"
grep -r 'Voucher' pgw-core-trans* | grep '2011' |grep 'AMOUNT_FULLY_PAID\|PARTIAL_PAYMENT_SUCCESS'|wc -l;

#--------------------------------------------------------------------------------------------------------------------------------------------------------------------
printf "total revenue in M-Pesa in Year 2011 \n"
grep -r 'M-Pesa' pgw-core-trans* | grep '2011'| grep 'AMOUNT_FULLY_PAID\|PARTIAL_PAYMENT_SUCCESS'| cut -d "|" -f 14 > M-Pesa.txt

awk '{ SUM += $1} END { printf "%.2f \n",  SUM }' M-Pesa.txt

#--------------------------------------------------------------------------------------------------------------------------------------------------------------------

printf "total revenue in Equity Bank in Year 2011 \n"
grep -r 'Equity Bank' pgw-core-trans* | grep '2011'| grep 'AMOUNT_FULLY_PAID\|PARTIAL_PAYMENT_SUCCESS'| cut -d "|" -f 14 > Equity.txt

awk '{ SUM += $1} END { printf "%.2f \n",  SUM }' Equity.txt

#--------------------------------------------------------------------------------------------------------------------------------------------------------------------

printf "total revenue in Mobile Account in Year 2011 \n"
grep -r 'Mobile Account' pgw-core-trans* | grep '2011'| grep 'AMOUNT_FULLY_PAID\|PARTIAL_PAYMENT_SUCCESS'| cut -d "|" -f 14 > Mobile_Account.txt

awk '{ SUM += $1} END { printf "%.2f \n",  SUM }' Mobile_Account.txt

#--------------------------------------------------------------------------------------------------------------------------------------------------------------------

printf "total revenue in Voucher in Year 2011 \n"
grep -r 'Voucher' pgw-core-trans* | grep '2011'| grep 'AMOUNT_FULLY_PAID'| cut -d "|" -f 14 > voucher.txt

awk '{ SUM += $1} END { printf "%.2f \n",  SUM }' voucher.txt

#--------------------------------------------------------------------------------------------------------------------------------------------------------------------
printf "total vcity income from M-Pesa in Year 2011 \n"
grep -r 'M-Pesa' pgw-core-trans* | grep '2011'| grep 'AMOUNT_FULLY_PAID\|PARTIAL_PAYMENT_SUCCESS'| cut -d "|" -f 18 > vcity-mpesa.txt

awk '{ SUM += $1} END { printf "%.2f \n",  SUM }' vcity-mpesa.txt

#---------------------------------------------------------------------------------------------------------------------------------------------------------------------

printf "total vcity income from Equity Bank in Year 2011 \n"
grep -r 'Equity Bank' pgw-core-trans* | grep '2011'| grep 'AMOUNT_FULLY_PAID\|PARTIAL_PAYMENT_SUCCESS'| cut -d "|" -f 18 > vcity-equity.txt

awk '{ SUM += $1} END { printf "%.2f \n",  SUM }' vcity-equity.txt

#--------------------------------------------------------------------------------------------------------------------------------------------------------------------

printf "total revenue for the Sample_Caas_Application in year 2011 \n"
grep -r 'APP_300102' pgw-core-trans* | grep '2011' | grep 'AMOUNT_FULLY_PAID\|PARTIAL_PAYMENT_SUCCESS'| cut -d "|" -f 14 > sample-cas-app.txt

revenue_value=`awk '{ revenue += $1} END { print revenue}' sample-cas-app.txt`

grep -r 'APP_300102' pgw-core-trans* | grep '2011' | grep 'AMOUNT_FULLY_PAID\|PARTIAL_PAYMENT_SUCCESS'| cut -d "|" -f 18 > sample-cas-app-sp.txt

service_charge=`awk '{ service_charge += $1} END {print service_charge}' sample-cas-app-sp.txt`

final=`expr $revenue_value - $service_charge`

echo $final;
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------

printf "total subscriber repayment from M-Pesa in year 2011 \n"
grep -r 'M-Pesa' pgw-core-trans* | grep '2011'| grep 'PARTIAL_OR_OVER_PAYMENT_NOT_ALLOWED'|cut -d "|" -f 14 > repayment-mpesa.txt
awk '{ SUM += $1} END { printf "%.2f \n",  SUM }' repayment-mpesa.txt

#--------------------------------------------------------------------------------------------------------------------------------------------------------------------

printf "total subscriber repayment from Equity Bank in year 2011 \n"
grep -r 'Equity Bank' pgw-core-trans* | grep '2011'| grep 'PARTIAL_OR_OVER_PAYMENT_NOT_ALLOWED'|cut -d "|" -f 14 > repayment-equity.txt
awk '{ SUM += $1} END { printf "%.2f \n",  SUM }' repayment-equity.txt

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------

rm -rf *.txt;
