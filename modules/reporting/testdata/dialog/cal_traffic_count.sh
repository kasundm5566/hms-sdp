#!/bin/bash

cd sdp_translogs/
printf "total sdp trans logs count \n"
cat sdp-translog.log* | wc -l ;

printf "total success traffic count \n"
egrep "\|S1000\|" ./* | wc -l ;

printf "total success traffic count for year 2012 \n"
egrep "\|S1000\|" ./* | grep '|2012-'| wc -l ;

printf "total success traffic count for year 2011 \n"
egrep "\|S1000\|" ./* | grep '|2011-'| wc -l ;

printf "total success traffic count for testuser for year 2011 \n"
egrep "\|S1000\|" ./* | grep '|2011-'| grep 'SPP_000008' |wc -l ;

printf "total success traffic count for testuser for year 2012 \n"
egrep "\|S1000\|" ./* | grep '|2012-'| grep 'SPP_000008' |wc -l ;

printf "total success traffic count for anjulaw for year 2011 \n"
egrep "\|S1000\|" ./* | grep '|2011-'| grep 'SPP_000059' |wc -l ;

printf "total success traffic count for anjulaw for year 2012 \n"
egrep "\|S1000\|" ./* | grep '|2012-'| grep 'SPP_000059' |wc -l ;

printf "total success traffic count for Success App for year 2011 \n"
egrep "\|S1000\|" ./* | grep '|2011-'| grep 'APP_000070' |wc -l ;

printf "total success traffic count for Success App for year 2012 \n"
egrep "\|S1000\|" ./* | grep '|2012-'| grep 'APP_000070' |wc -l ;

printf "total success traffic count for USSD NCS for year 2012 \n"
egrep "\|S1000\|" ./* | grep '|2012-'| grep 'ussd' |wc -l ;

printf "total success traffic count for CAAS NCS for year 2012 \n"
egrep "\|S1000\|" ./* | grep '|2012-'| grep 'cas' |wc -l ;

printf "total success traffic count for SMS MT NCS for year 2012 \n"
egrep "\|S1000\|" ./* | grep '|2012-'| grep 'sms' |grep 'mt' | wc -l

printf "total success traffic count for SMS MO NCS for year 2012 \n"
egrep "\|S1000\|" ./* | grep '|2012-'| grep 'sms' |grep 'mo' | wc -l 


printf "total success traffic count for USSD NCS for year 2011 \n"
egrep "\|S1000\|" ./* | grep '|2011-'| grep 'ussd' |wc -l ;

printf "total success traffic count for CAAS NCS for year 2011 \n"
egrep "\|S1000\|" ./* | grep '|2011-'| grep 'cas' |wc -l ;

printf "total success traffic count for SMS MT NCS for year 2011 \n"
egrep "\|S1000\|" ./* | grep '|2011-'| grep 'sms' |grep 'mt' | wc -l

printf "total success traffic count for SMS MO NCS for year 2011 \n"
egrep "\|S1000\|" ./* | grep '|2011-'| grep 'sms' |grep 'mo' | wc -l



