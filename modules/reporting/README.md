TAP Reporting Module
====================

- data loader
- report viewer

Required Software
====================

Java 1.7.x
For dialog, ndb, seylan, cargills-bank - Tomcat 8.x
Other profiles - Tomcat 6.x
Maven 3.3.x
MySQL 5.6.x
Apache-Server
birt-runtime-3.7.2

Note: copy Java ,Tomcat  and apache2 into /hms/installs location

Prerequisites
====================

common_admin database should be there with initial data.

cas.war should have copied into apache-tomcat-6.x/webapps/


birt-runtime installation
====================

extract smb://shared.hsenidmobile.com/softwares/Linux/LATEST_SOFTWARE_ALL_IN_ONE/birt-runtime-3_7_2.tar.gz and copy into /hms/dev/deps/


DNS Settings
====================

/etc/hosts

127.0.0.1 dev.sdp.hsenidmobile.com
127.0.0.1 apps.sdp.hsenidmobile.com
127.0.0.1 db.mysql.reports
127.0.0.1 db.mongo.sdp


DB Setup
====================

create user
------------

CREATE USER 'user'@'localhost' IDENTIFIED BY 'password';

GRANT ALL PRIVILEGES ON *.* TO 'user'@'localhost';  WITH GRANT OPTION

create db
------------

log into mysql again by 'user' which has previously created.

create a database named 'reporting'.

Follow the instructions given in the readme in /data/profile

run the update scripts in the data/update-scripts/profile 
(earlier version to latest version)

Note : To find the correct order of the update scripts use this command
 find "$(pwd)" -name '*.sql' | sort -t. -k 1,1n -k 2,2n -k 3,3n -k 4,4n | awk '{print "source `" $0 "`"}'


How to Build
====================

mvn clean install -Pprofile_name


How to Deploy
====================

1. data loader
extract reporting/module/dataloader/target/dataloader.zip and copy into /hms/apps

Create following directories 

/hms/logs/ftp/sp/source
/hms/logs/ftp/sp/archive
/hms/logs/ftp/sdp/source
/hms/logs/ftp/sdp/archive
/hms/logs/ftp/cms/source
/hms/logs/ftp/cms/archive
/hms/logs/ftp/pgw/source
/hms/logs/ftp/pgw/archive
/hms/logs/ftp/add/source
/hms/logs/ftp/add/archive
/hms/logs/ftp/governance/source
/hms/logs/ftp/governance/archive
/hms/logs/ftp/cooperate/source
/hms/logs/ftp/cooperate/archive
/hms/logs/ftp/subscription/source
/hms/logs/ftp/subscription/archive


Copy provisioning-provider-info.log logs from /hms/logs/prov/prov into /hms/logs/ftp/sp/source
Copy sdp-translog.log logs from /hms/logs/sdp-server into /hms/logs/ftp/sdp/source
Copy cms-translog.log logs from /hms/logs/cms into /hms/logs/ftp/cms/source
Copy pgw-core-trans.log logs from /hms/logs/pgw into /hms/logs/ftp/pgw/source
Copy admin-advertisement.log logs from /hms/logs/admin into /hms/logs/ftp/add/source
Copy admin-governance-message.log logs from /hms/logs/admin into /hms/logs/ftp/governance/source
Copy cooperate-user-info.log logs from /hms/logs/cur into /hms/logs/ftp/cooperate/source

Set crons as below 

service.provider.info.upload.cron.expression=0 0/10 * * * ?
sdp.trans.upload.cron.expression=0 0/10 * * * ?
cms.trans.upload.cron.expression=0 0/10 * * * ?
pgw.trans.upload.cron.expression=0 0/10 * * * ?
advertisement.upload.cron.expression=0 0/10 * * * ?
governance.upload.cron.expression=0 0/10 * * * ?
cooperate.user.detail.upload.cron.expression=0 0/10 * * * ?
subscription.log.generator.cron.expression=0 0 0 * * ?
total.subscription.summary.cron.expression=0 0 30 * * ?

Note : In DA or DEV environment total.subscription.summary.cron.expression should run after running subscription.log.generator.cron.expression.

#stored procedure calling  cron expression
daily.sp.main.cron.expression=0 1 0 * * ?

2. report viewer
copy reporting/module/viewer/target/viewer.war into /hms/installs/apache-tomcat-6.0.39/webapps

(Inorder to view )

(Inorder to validate the built viewer.war follow the steps in profile validation)


Tomcat JNDI Setting
====================
** Copy following configurations into tomcat/conf/context.xml

<?xml version='1.0' encoding='utf-8'?>
<Context path="/viewer/spring/" docBase="viewer" debug="5" reloadable="true" crossContext="true">
    <Resource name="jdbc/vcReportingDB"
              auth="Container"
              type="javax.sql.DataSource"
              initialSize="0"
              maxActive="40"
              maxIdle="20"
              minIdle="10"
              maxWait="15000"
              validationQuery="select 1"
              testWhileIdle="true"
              username="user"
              password="password"
              driverClassName="com.mysql.jdbc.Driver"
              url="jdbc:mysql://db.mysql.reports:3306/reporting?autoReconnect=true"
              description="MySQL Sfdata DB"/>
</Context>


Profile Validation.
===================

* Insert/Delete the keywords relavant to each profile seperated by " | " in profile_validate.sh script .

* Comment out the profile you build for. 

* Copy the script in to the deployed ../apache-tomcat/webapps/viewer dir.

* Run the Script by 
  $ ./profile_validate.sh

Eg. If you bulid the dialog profile and want to check other profiles keywords are included in the deployed viewer dir,
Comment out Dialog grep line and run the script in deployed viewer folder.

Note : If you know exact capitalization for given keywords u can ommit "i" flag in grep.

To access common admin module
=============================

Install apache-server

* Get source from smb://shared.hsenidmobile.com/softwares/Linux/hmsvmsetup/hms/installs/apache-built-for-ubutu
* Copy apach2 directory into the hms/installs location
* Go to the /bin then execute sudo ./apachctl start to start | stop for stop apache server

Note: Your /hms folder should have the following directories apps,logs, scripts, installs

Login to registration module

start cas: https://dev.sdp.hsenidmobile.com/cas/login
Access the url https://dev.sdp.hsenidmobile.com/cas/admin then login with

userName : sdpadmin
password: test123#


DB-Scripts
==========
Refer to data/profile/README 

Find all the update scripts from /reporting/data/update-scripts/profile


