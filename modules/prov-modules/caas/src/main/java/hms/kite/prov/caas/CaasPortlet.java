/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.caas;

import com.google.common.base.Optional;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.terminal.gwt.server.PortletApplicationContext2;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import hms.kite.datarepo.audit.AuditTrail;
import hms.kite.prov.caas.ui.CaasSlaBasicLayout;
import hms.kite.prov.caas.ui.CaasSlaMainLayout;
import hms.kite.prov.caas.ui.common.ViewMode;
import hms.kite.prov.caas.util.CaasIdGenerator;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.LiferayUtil;
import hms.kite.provisioning.commons.ui.UiManagerImpl;
import hms.kite.provisioning.commons.util.InAppServiceAdapter;
import hms.kite.util.KiteKeyBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.ncsRepositoryService;
import static hms.kite.prov.caas.CaasFeatureRegistry.isCaasIdGenerationEnable;
import static hms.kite.prov.caas.ui.common.ViewMode.EDIT;
import static hms.kite.prov.caas.ui.common.ViewMode.VIEW;
import static hms.kite.provisioning.commons.util.ProvCommonUtil.getNcsDataFromSessionByAppId;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CaasPortlet extends BaseApplication {

    private static final Logger logger = LoggerFactory.getLogger(CaasPortlet.class);

    private static final String PORTLET_USER_ID = "portlet_user_id";

    private CaasSlaMainLayout caasSlaMainLayout;
    private ThemeDisplay themeDisplay;
    private String corporateUserId;
    private Panel mainPanel;
    private Panel innerPanel;

    private void handleCrEditNcsEvent(Map<String, Object> eventData) {
        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        innerPanel.removeAllComponents();
        String appId = (String) eventData.get(appIdK);
        Map<String, Object> sessionCasSlaData = getCaasSlaData(appId);
        final String spId = (String) eventData.get(spIdK);
        caasSlaMainLayout = new CaasSlaMainLayout(EDIT, this, appId, corporateUserId, (List<Map>) eventData.get(ncsesK));
        innerPanel.addComponent(caasSlaMainLayout);
        if (sessionCasSlaData != null) {
            caasSlaMainLayout.reloadData(sessionCasSlaData);
        } else {
            reloadDataFromRepository(eventData);
        }
        caasSlaMainLayout.reloadComponents(EDIT);
    }

    private void setCorporateUserId(EventRequest request) {
        Map<String, Object> eventData = (Map<String, Object>) request.getEvent().getValue();
        this.corporateUserId = (String) eventData.get(coopUserIdK);
    }

    private void setPortletUserId(Long userId) {
        if (getPortletUserId() == null) {
            addToSession(PORTLET_USER_ID, userId);
        }
    }

    private Long getPortletUserId() {
        return (Long) getFromSession(PORTLET_USER_ID);
    }

    private void handleCreateNcsRequest(Map<String, Object> eventData) {
        logger.debug("CAAS NCS SLA Create request received...");

        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        innerPanel.removeAllComponents();
        caasSlaMainLayout = new CaasSlaMainLayout(EDIT, this, (String) eventData.get(appIdK), corporateUserId, (List<Map>) eventData.get(ncsesK));
        innerPanel.addComponent(caasSlaMainLayout);
        caasSlaMainLayout.reloadComponents(EDIT);
    }

    private void handleReconfigureNcsRequest(Map<String, Object> eventData) {
        logger.debug("CAAS NCS SLA Re-Configure request received...");

        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        innerPanel.removeAllComponents();
        String appId = (String) eventData.get(appIdK);
        Map<String, Object> sessionCasSlaData = getCaasSlaData(appId);
        final String spId = (String) eventData.get(spIdK);
        final String appName = (String) eventData.get(nameK);
        caasSlaMainLayout = new CaasSlaMainLayout(EDIT, this, appId, corporateUserId, (List<Map>) eventData.get(ncsesK));
        innerPanel.addComponent(caasSlaMainLayout);
        if (sessionCasSlaData != null) {
            caasSlaMainLayout.reloadData(sessionCasSlaData);
        } else {
            reloadDataFromRepository(eventData);
        }
        caasSlaMainLayout.reloadComponents(EDIT);
    }

    private void handleViewNcsRequest(Map<String, Object> eventData) {
        logger.debug("CAAS NCS SLA View request received...");

        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        reloadNcsData(VIEW, eventData);
    }

    private void handleSaveNcsRequest(Map<String, Object> eventData) {
        logger.debug("CAAS NCS SLA Save request received...");

        String operator = "";
        String ncsType = casK;
        String appId = (String) eventData.get(appIdK);
        String status = (String) eventData.get(statusK);

        Map<String, Object> sessionCasSlaData = getCaasSlaData(appId);
        if (deleteK.equals(status)) {
            ncsRepositoryService().update(appId, ncsType, operator, status);
        } else if (sessionCasSlaData != null) {
            if (isCaasIdGenerationEnable() && sessionCasSlaData.get(postPaidBillDescCodeK) == null) {
                sessionCasSlaData.put(postPaidBillDescCodeK, CaasIdGenerator.generateBillDescriptionCode());
            }
            sessionCasSlaData.put(statusK, eventData.get(statusK));
            final String spId = (String) eventData.get(spIdK);
            ncsRepositoryService().createNcs(sessionCasSlaData, spId);

        } else {
            ncsRepositoryService().update(appId, ncsType, operator, status);
        }
        removeFromSession(casSlaDataSessionK);
        closePortlet();

        logger.debug("CAAS SLA Saving request processed successfully");
    }

    private void reloadNcsData(ViewMode viewMode, Map<String, Object> eventData) {
        innerPanel.removeAllComponents();
        caasSlaMainLayout = new CaasSlaMainLayout(viewMode, this, (String) eventData.get(appIdK), corporateUserId, (List<Map>) eventData.get(ncsesK));
        innerPanel.addComponent(caasSlaMainLayout);
        reloadDataFromRepository(eventData);
        caasSlaMainLayout.reloadComponents(viewMode);
    }

    private void reloadDataFromRepository(Map<String, Object> eventData) {
        String appId = (String) eventData.get(appIdK);
        List<Map<String, Object>> ncsList = ncsRepositoryService().findByAppIdNcsType(appId, casK);

        if (ncsList != null && ncsList.size() > 0) {
            Map<String, Object> data = ncsList.get(0);

            caasSlaMainLayout.reloadData(data);
            setCaasSlaData(data);
        }
    }

    public void closePortlet() {
        if (caasSlaMainLayout != null) {
            caasSlaMainLayout.removeAllComponents();
        }
        themeDisplay.getLayoutTypePortlet().removePortletId(getPortletUserId(), LiferayUtil.createPortletId(casK));
    }

    @Override
    protected String getApplicationName() {
        return "caas";
    }

    @Override
    public void init() {
        setResourceBundle("US");
        Window main = new Window(getMsg("cas.sla.window.title"));
        setMainWindow(main);
        setUiManager(new UiManagerImpl(this));
        String caption = this.getMsg("cas.sla.window.title");

        VerticalLayout mainLayout = new VerticalLayout();

        mainPanel = new Panel();
        mainLayout.addComponent(mainPanel);

        mainLayout.addStyleName("container-panel");
        mainLayout.setWidth("762px");
        mainLayout.setMargin(true);

        innerPanel = new Panel(caption);
        mainPanel.addComponent(innerPanel);
        innerPanel.addStyleName("child-panel");

        setPortletContext((PortletApplicationContext2) getContext());
        getPortletContext().addPortletListener(this, this);

        getUiManager().clearAll();
        getUiManager().pushScreen(CaasSlaBasicLayout.class.getName(), mainLayout);
    }

    @Override
    public void handleEventRequest(EventRequest request, EventResponse response, Window window) {
        try {
            super.handleEventRequest(request, response, window);
            setCorporateUserId(request);

            String eventName = request.getEvent().getName();
            themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
            Long userId = themeDisplay.getUserId();
            setPortletUserId(userId);
            Map<String, Object> eventData = (Map<String, Object>) request.getEvent().getValue();

            logger.debug("Received Event Data is [{}]", eventData);

            AuditTrail.setCurrentUser((String) eventData.get(currentUsernameK), (String) eventData.get(currentUserTypeK));
            addToSession(currentUserTypeK, eventData.get(currentUserTypeK));
            String appId = (String) eventData.get(appIdK);
            String ncsType = (String) eventData.get(ncsTypeK);

            if (appId == null || ncsType == null || !ncsType.equals(KiteKeyBox.casK)) {
                logger.debug("Event Request ignored, since it is not belong to me");
                return;
            }
            if (createNcsK.equals(eventName)) {
                handleCreateNcsRequest(eventData);
            } else if (saveNcsK.equals(eventName)) {
                handleSaveNcsRequest(eventData);
            } else if (viewNcsK.equals(eventName)) {
                handleViewNcsRequest(eventData);
            } else if (reconfigureNcsK.equals(eventName)) {
                handleReconfigureNcsRequest(eventData);
            } else if (crEditNcsEvent.equals(eventName)) {
                handleCrEditNcsEvent(eventData);
            } else {
                logger.error("Unexpected event [{}], Can't handle", eventName);
            }

        } catch (Exception e) {
            logger.error("Unexpected error occurred while handling event ", e);
        }
    }

    public Map<String, Object> getCaasSlaData(String appId) {
        return getNcsDataFromSessionByAppId(appId, casSlaDataSessionK, this);
    }

    public void setCaasSlaData(Map<String, Object> casSlaData) {
        addToSession(casSlaDataSessionK, casSlaData);
    }
}
