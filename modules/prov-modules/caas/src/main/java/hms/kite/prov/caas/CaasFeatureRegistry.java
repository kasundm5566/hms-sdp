/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.caas;

import hms.kite.util.NullObject;

import java.util.Properties;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CaasFeatureRegistry {

    private static Properties caasFeatureProperties;

    static {
        setCaasFeatureProperties(new NullObject<Properties>().get());
    }

    private static void setCaasFeatureProperties(Properties caasFeatureProperties) {
        CaasFeatureRegistry.caasFeatureProperties = caasFeatureProperties;
    }

    public static Boolean isCaasIdGenerationEnable() {
        return Boolean.valueOf(caasFeatureProperties.getProperty("caas.id.generation.enable"));
    }

    public static boolean isHelpButtonLinkEnable() {
        return Boolean.parseBoolean(caasFeatureProperties.getProperty("caas.help.link.enable"));
    }
}
