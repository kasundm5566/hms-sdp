/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.prov.caas.util;

import java.text.DecimalFormat;

import static hms.kite.datarepo.RepositoryServiceRegistry.systemConfiguration;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class CaasIdGenerator {

    private static String billDescriptionCodeFormat;
    private static String billDescriptionCodePrefix;

    static {
        setBillDescriptionCodeFormat("0000");
        setBillDescriptionCodePrefix("D");
    }

    /**
     * Generate bill description code.
     *
     * @return - generated id like D1234
     */
    public static String generateBillDescriptionCode() {
        return billDescriptionCodePrefix + new DecimalFormat(billDescriptionCodeFormat).format(systemConfiguration().incrementAndGetNextId("bill-desc-code"));
    }

    private static void setBillDescriptionCodeFormat(String billDescriptionCodeFormat) {
        CaasIdGenerator.billDescriptionCodeFormat = billDescriptionCodeFormat;
    }

    private static void setBillDescriptionCodePrefix(String billDescriptionCodePrefix) {
        CaasIdGenerator.billDescriptionCodePrefix = billDescriptionCodePrefix;
    }
}
