/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.caas.util;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CaasSlaRoles {

    public static final String CAAS_PERMISSION_ROLE_PREFIX = "ROLE_PROV_CAAS";

    public static final String POST_PAID_BILL_DESC_CODE = "POST_PAID_BILL_DESC_CODE";
    public static final String SERVICE_CHARGE_PERCENTAGE = "SERVICE_CHARGE_PERCENTAGE";
    public static final String REASON_CODE = "REASON_CODE";

    public static final String ASYNC_CHARGING_NOTIFICATION = "ASYNC_CHARGING_NOTIFICATION";

    public static final String MAX_TRANSACTIONS_PER_SECOND = "MAX_TRANSACTIONS_PER_SECOND";
    public static final String MAX_TRANSACTIONS_PER_DAY = "MAX_TRANSACTIONS_PER_DAY";

    public static final String ALLOW_QUERY_BALANCE = "ALLOW_QUERY_BALANCE";

    public static final String ALLOW_FUND_TRANSFER = "ALLOW_FUND_TRANSFER";
    public static final String MIN_FUND_TRANSFER_AMOUNT = "MIN_FUND_TRANSFER_AMOUNT";
    public static final String MAX_FUND_TRANSFER_AMOUNT = "MAX_FUND_TRANSFER_AMOUNT";

    public static final String ALLOW_CREDIT_RESERVE = "ALLOW_CREDIT_RESERVE";
    public static final String MIN_CREDIT_RESERVE_AMOUNT = "MIN_CREDIT_RESERVE_AMOUNT";
    public static final String MAX_CREDIT_RESERVE_AMOUNT = "MAX_CREDIT_RESERVE_AMOUNT";

    public static final String ALLOW_DEBIT = "ALLOW_DEBIT";
    public static final String MIN_DEBIT_AMOUNT = "MIN_DEBIT_AMOUNT";
    public static final String MAX_DEBIT_AMOUNT = "MAX_DEBIT_AMOUNT";

    public static final String ALLOW_CREDIT = "ALLOW_CREDIT";
    public static final String MIN_CREDIT_AMOUNT = "MIN_CREDIT_AMOUNT";
    public static final String MAX_CREDIT_AMOUNT = "MAX_CREDIT_AMOUNT";

    public static final String SUBSCRIPTION_REQUIRED = "SUBSCRIPTION_REQUIRED";
    public static final String TRUSTED = "TRUSTED";

    public static final String ALLOW_AUTH_MEDIUM = "ALLOW_AUTH_MEDIUM";
    public static final String ALLOW_AUTH_THRESHOLD = "ALLOW_AUTH_THRESHOLD";

    public static final String IN_APP_ALLOWED = "IN_APP_ALLOWED";
}
