/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.caas.ui.validator;

import com.vaadin.data.Validator;
import com.vaadin.ui.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CaasMinMaxValidator implements Validator {

    private static final Logger logger = LoggerFactory.getLogger(CaasMinMaxValidator.class);

    private String customErrorKey;
    private TextField minTxtFld;

    public CaasMinMaxValidator(String customErrorKey, TextField minTxtFld) {
        this.customErrorKey = customErrorKey;
        this.minTxtFld = minTxtFld;
    }

    @Override
    public void validate(Object value) throws Validator.InvalidValueException {
        if (!isValid(value)) {
            throw new Validator.InvalidValueException(customErrorKey);
        }
    }

    @Override
    public boolean isValid(Object value) {
        try {
            double max = Double.parseDouble(value.toString());
            double min = Double.parseDouble(minTxtFld.getValue().toString());
            if (max > min) {
                return true;
            }
        } catch (NumberFormatException ignored) {
        } catch (Exception e) {
            logger.error("Unexpected error occurred while validating", e);
        }
        return false;
    }
}
