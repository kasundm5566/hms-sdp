/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.caas;

import hms.kite.provisioning.commons.ValidationRegistry;
import hms.kite.util.NullObject;

import java.util.Properties;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CaasServiceRegistry {

    private static ValidationRegistry validationRegistry;
    private static Properties caasProperties;

    static {
        setValidationRegistry(new NullObject<ValidationRegistry>().get());
        setCaasProperties(new NullObject<Properties>().get());
    }

    public static ValidationRegistry validationRegistry() {
        return validationRegistry;
    }

    private static void setValidationRegistry(ValidationRegistry validationRegistry) {
        CaasServiceRegistry.validationRegistry = validationRegistry;
    }

    private static void setCaasProperties(Properties caasProperties) {
        CaasServiceRegistry.caasProperties = caasProperties;
    }

    public static boolean isDefaultServiceChargePercentageAllow() {
        return Boolean.parseBoolean(caasProperties.getProperty("caas.default.service.charge.percentage.allow"));
    }

    public static String getDefaultServiceChargePercentage() {
        return caasProperties.getProperty("caas.default.service.charge.percentage");
    }

    public static boolean isDefaultMaxTpsAllow() {
        return Boolean.parseBoolean(caasProperties.getProperty("caas.default.max.tps.allow"));
    }

    public static String getDefaultMaxTps() {
        return caasProperties.getProperty("caas.default.max.tps");
    }

    public static boolean isDefaultMaxTpdAllow() {
        return Boolean.parseBoolean(caasProperties.getProperty("caas.default.max.tpd.allow"));
    }

    public static String getDefaultMaxTpd() {
        return caasProperties.getProperty("caas.default.max.tpd");
    }

    public static boolean isDefaultMinDebitAmountAllow() {
        return Boolean.parseBoolean(caasProperties.getProperty("caas.default.min.debit.amount.allow"));
    }

    public static String getDefaultMinDebitAmount() {
        return caasProperties.getProperty("caas.default.min.debit.amount");
    }

    public static boolean isDefaultMaxDebitAmountAllow() {
        return Boolean.parseBoolean(caasProperties.getProperty("caas.default.max.debit.amount.allow"));
    }

    public static String getDefaultMaxDebitAmount() {
        return caasProperties.getProperty("caas.default.max.debit.amount");
    }

    public static String getDefaultAuthThresholdValue() {
        return caasProperties.getProperty("cas.sla.auth.threshold.default.value");
    }
}
