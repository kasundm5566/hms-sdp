/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.caas.ui;

import com.google.common.base.Optional;
import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.ui.*;
import hms.kite.prov.caas.ui.validator.CaasMinMaxValidator;
import hms.kite.provisioning.commons.ui.AbstractBasicDetailsLayout;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.component.charging.ChargingLayout;
import hms.kite.provisioning.commons.ui.component.popup.HelpDetails;
import hms.kite.provisioning.commons.ui.validator.SpLimitValidator;
import hms.kite.provisioning.commons.ui.validator.SubscriptionRequiredValidator;
import hms.kite.provisioning.commons.util.InAppServiceAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.prov.caas.CaasServiceRegistry.*;
import static hms.kite.prov.caas.CaasFeatureRegistry.*;
import static hms.kite.prov.caas.util.CaasSlaRoles.*;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CaasSlaBasicLayout extends AbstractBasicDetailsLayout {

    private static final Logger logger = LoggerFactory.getLogger(CaasSlaBasicLayout.class);

    private static final String WIDTH = "220px";
    private static final String USSD_COMBO_ITEM = "Ussd";
    private static final String SMS_COMBO_ITEM = "Sms";
    public static final String HELP_IMG_WIDTH = "790px";
    public static final String HELP_IMG_HEIGHT = "320px";
    private static final String HELP_IMAGE = "help.jpg";
    public static final String HELP_IMG_STYLE_NAME = "link";
    public static final String HELP_BUTTON_NAME = "Help";

    private String corporateUserId;

    private Form casSlaForm;

    private TextField postPaidBillDescCodeTxtFld;
    private TextField serviceChargePercentageTxtFld;
    private TextField reasonCodeTxtFld;

    private TextField asyncChargingNotificationUrlTxtFld;

    private TextField maxTransPerSecTxtFld;
    private TextField maxTransPerDayTxtFld;

    private CheckBox allowQueryBalanceChkBox;

    private ChargingLayout chargingLayout;

    private CheckBox allowFundTransferChkBox;
    private TextField minTransferAmountTxtFld;
    private TextField maxTransferAmountTxtFld;

    private CheckBox allowCreditReserveChkBox;
    private TextField minReserveAmountTxtFld;
    private TextField maxReserveAmountTxtFld;

    private CheckBox allowDebitChkBox;
    private TextField minDebitAmountTxtFld;
    private TextField maxDebitAmountTxtFld;

    private CheckBox allowCreditChkBox;
    private TextField minCreditAmountTxtFld;
    private TextField maxCreditAmountTxtFld;

    private CheckBox subscriptionRequiredChkBox;
    private CheckBox trustedChkBox;

    private ComboBox authorizationMediumCombo;
    private TextField authorizationThresholdField;

    private List<Map> selectedNcses;
    private String appId;

    private PopupView popupView;

    private CheckBox inAppCheckBox;

    /**
     * Initializes the caas sla configuration UI
     */
    private void init() {
        logger.debug("Initializing the CAAS SLA configuration page started..");

        casSlaForm = new Form();
        casSlaForm.setImmediate(true);
        casSlaForm.addStyleName("cas-ncs-form");

        int spTpd;
        int spTps;

        try {
            Map<String, Object> sp = spRepositoryService().findSpByCoopUserId(corporateUserId);
            if (sp != null) {
                logger.debug("Returned SP Details [{}]", sp.toString());

                spTpd = Integer.parseInt(sp.get(casTpdK).toString());
                spTps = Integer.parseInt(sp.get(casTpsK).toString());

                createMainLayout(spTpd, spTps);
            } else {
                logger.error("No Sp Details found with the given corporate user id [{}]", corporateUserId);
            }
        } catch (Exception e) {
            logger.error("Error occurred while searching SP details for corporate user id [{}] ", corporateUserId, e);
        }
    }

    private void createMainLayout(int spTpd, int spTps) {

        if(isHelpButtonLinkEnable()) {
            createHelperLink();
        }
        createSystemOwnerRelatedFieldsView();
        createServiceChargePercentageFld();
        createCaasAsyncNotificationView();
        createTransactionView(spTpd, spTps);
        createQueryBalanceRequestView();
        createChargingLayoutView();
        createFundTransferRequestView();
        createCreditReserveRequestView();
        createDebitRequestView();
        createCreditRequestView();
        createTrustedSelectionView();
        createSubscriptionRequiredView();
        createAuthMediumView();
        createAuthThresholdView();
        createInAppPurchaseView();
        addComponent(casSlaForm);
    }

    private void createHelperLink() {
        Button helpButtonLink = createHelpButtonLink();
        addComponent(helpButtonLink);
        setComponentAlignment(helpButtonLink, Alignment.TOP_RIGHT);
    }

    private Button createHelpButtonLink() {
        final HelpDetails helpDetails = new HelpDetails(application, HELP_IMAGE);
        final Embedded image = helpDetails.getHelpImage();
        Button helpButtonLink = new Button(HELP_BUTTON_NAME);
        helpButtonLink.setStyleName(HELP_IMG_STYLE_NAME);
        helpButtonLink.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent clickEvent) {
                getWindow().addWindow(helpDetails.generateHelpImageSubWindow(image, HELP_IMG_WIDTH, HELP_IMG_HEIGHT));
            }
        });
        return helpButtonLink;
    }

    private void createSystemOwnerRelatedFieldsView() {
        postPaidBillDescCodeTxtFld = getFieldWithPermission(POST_PAID_BILL_DESC_CODE, new TextField());
        if (isFieldNotNull(postPaidBillDescCodeTxtFld)) {
            postPaidBillDescCodeTxtFld.setCaption(application.getMsg("cas.sla.post.paid.bill.description.code"));
            postPaidBillDescCodeTxtFld.setWidth(WIDTH);
            casSlaForm.addField("postPaidBillDescCode", postPaidBillDescCodeTxtFld);
        }

        reasonCodeTxtFld = getFieldWithPermission(REASON_CODE, new TextField());
        if (isFieldNotNull(reasonCodeTxtFld)) {
            reasonCodeTxtFld.setCaption(application.getMsg("cas.sla.reason.code"));
            reasonCodeTxtFld.setWidth(WIDTH);
            reasonCodeTxtFld.setRequired(true);
            reasonCodeTxtFld.setRequiredError(application.getMsg("cas.sla.reason.code.required.error"));
            reasonCodeTxtFld.setDescription(application.getMsg("cas.sla.reason.code.tooltip"));
            casSlaForm.addField("reasonCode", reasonCodeTxtFld);
        }
    }

    private void createServiceChargePercentageFld() {
        serviceChargePercentageTxtFld = getFieldWithPermission(SERVICE_CHARGE_PERCENTAGE, new TextField());
        if (isFieldNotNull(serviceChargePercentageTxtFld)) {
            serviceChargePercentageTxtFld.setCaption(application.getMsg("cas.sla.service.charge.percentage"));
            serviceChargePercentageTxtFld.setWidth(WIDTH);
            serviceChargePercentageTxtFld.setRequired(true);
            serviceChargePercentageTxtFld.setRequiredError(application.getMsg("cas.sla.service.charge.percentage.required.error"));
            serviceChargePercentageTxtFld.setDescription(application.getMsg("cas.sla.service.charge.percentage.tooltip"));
            if (isDefaultServiceChargePercentageAllow()) {
                serviceChargePercentageTxtFld.setValue(getDefaultServiceChargePercentage());
            }
            casSlaForm.addField("serviceChargePercentage", serviceChargePercentageTxtFld);
        }
    }

    private void createCaasAsyncNotificationView() {
        asyncChargingNotificationUrlTxtFld = getFieldWithPermission(ASYNC_CHARGING_NOTIFICATION, new TextField());
        if (isFieldNotNull(asyncChargingNotificationUrlTxtFld)) {
            asyncChargingNotificationUrlTxtFld.setCaption(application.getMsg("cas.sla.async.charging.notification.url.caption"));
            asyncChargingNotificationUrlTxtFld.setWidth(WIDTH);
            asyncChargingNotificationUrlTxtFld.setRequired(true);
            asyncChargingNotificationUrlTxtFld.setRequiredError(application.getMsg("cas.sla.async.charging.notification.url.required.error"));
            asyncChargingNotificationUrlTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("notificationUrlValidation"),
                    application.getMsg("cas.sla.async.charging.notification.url.validation.error")));
            casSlaForm.addField("asyncChargingNotificationUrl", asyncChargingNotificationUrlTxtFld);
        }
    }

    /**
     * Initializes caas transaction view
     */
    private void createTransactionView(int spTpd, int spTps) {
        maxTransPerSecTxtFld = getFieldWithPermission(MAX_TRANSACTIONS_PER_SECOND, new TextField());
        if (isFieldNotNull(maxTransPerSecTxtFld)) {
            maxTransPerSecTxtFld.setCaption(application.getMsg("cas.sla.max.trans.per.sec.caption"));
            maxTransPerSecTxtFld.setWidth(WIDTH);
            maxTransPerSecTxtFld.setRequired(true);
            maxTransPerSecTxtFld.setRequiredError(application.getMsg("cas.sla.max.trans.per.sec.required.error"));
            maxTransPerSecTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("ncsCasTpsRegex"),
                    application.getMsg("cas.sla.max.trans.per.sec.sp.number.validation.error")));
            maxTransPerSecTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("sp.cas.zero.validation"),
                    application.getMsg("cas.sla.zeroValue.validation.error.message")));
            maxTransPerSecTxtFld.addValidator(new SpLimitValidator(spTps, MessageFormat.format(application.getMsg("cas.sla.max.trans.per.sec.sp.limit.error"), spTps)));
            if (isDefaultMaxTpsAllow()) {
                maxTransPerSecTxtFld.setValue(getDefaultMaxTps());
            }
            casSlaForm.addField("maxTransPerSec", maxTransPerSecTxtFld);
        }

        maxTransPerDayTxtFld = getFieldWithPermission(MAX_TRANSACTIONS_PER_DAY, new TextField());
        if (isFieldNotNull(maxTransPerDayTxtFld)) {
            maxTransPerDayTxtFld.setCaption(application.getMsg("cas.sla.max.trans.per.day.caption"));
            maxTransPerDayTxtFld.setWidth(WIDTH);
            maxTransPerDayTxtFld.setRequired(true);
            maxTransPerDayTxtFld.setRequiredError(application.getMsg("cas.sla.max.trans.per.day.required.error"));
            maxTransPerDayTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("ncsCasTpdRegex"),
                    application.getMsg("cas.sla.max.trans.per.day.sp.number.validation.error")));
            maxTransPerDayTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("sp.cas.zero.validation"),
                    application.getMsg("cas.sla.zeroValue.validation.error.message")));
            maxTransPerDayTxtFld.addValidator(new SpLimitValidator(spTpd, MessageFormat.format(application.getMsg("cas.sla.max.trans.per.day.sp.limit.error"), spTpd)));
            maxTransPerDayTxtFld.addValidator(new CaasMinMaxValidator(application.
                    getMsg("cas.sla.max.trans.per.sec.greater.max.trans.per.sec"), maxTransPerSecTxtFld));
            if (isDefaultMaxTpdAllow()) {
                maxTransPerDayTxtFld.setValue(getDefaultMaxTpd());
            }
            casSlaForm.addField("maxTransPerDay", maxTransPerDayTxtFld);
        }
    }

    /**
     * Initializes query balance request view
     */
    private void createQueryBalanceRequestView() {
        allowQueryBalanceChkBox = getFieldWithPermission(ALLOW_QUERY_BALANCE, new CheckBox());
        if (isFieldNotNull(allowQueryBalanceChkBox)) {
            allowQueryBalanceChkBox.setCaption(application.getMsg("cas.sla.allow.query.balance.request"));
            casSlaForm.addField("allowQueryBalanceRequest", allowQueryBalanceChkBox);
        }
    }

    /**
     * Initializes charging layout view
     */
    private void createChargingLayoutView() {
        chargingLayout = ChargingLayout.caasChargingLayout(casSlaForm.getLayout(), application, corporateUserId, CAAS_PERMISSION_ROLE_PREFIX, WIDTH, 3, 17);
    }

    /**
     * Initializes fund transfer request view
     */
    private void createFundTransferRequestView() {
        allowFundTransferChkBox = getFieldWithPermission(ALLOW_FUND_TRANSFER, new CheckBox());
        if (isFieldNotNull(allowFundTransferChkBox)) {
            allowFundTransferChkBox.setCaption(application.getMsg("cas.sla.allow.fund.transfer.caption"));
            allowFundTransferChkBox.addListener(new Property.ValueChangeListener() {

                @Override
                public void valueChange(Property.ValueChangeEvent event) {
                    Boolean enabled = (Boolean) event.getProperty().getValue();
                    if (enabled) {
                        setCommonConfigurations(minTransferAmountTxtFld, "cas.sla.min.transfer.amount.required.error",
                                "ncsCasCurrencyRegex", "cas.sla.min.transfer.amount.numeric.error");
                        setCommonConfigurations(maxTransferAmountTxtFld, "cas.sla.max.transfer.amount.required.error",
                                "ncsCasCurrencyRegex", "cas.sla.max.transfer.amount.numeric.error");
                        addAmountFieldValidators(maxTransferAmountTxtFld, minTransferAmountTxtFld,
                                "cas.sla.min.transfer.amount.greater.max.transfer.amount");
                    } else {
                        resetAndDisableField(minTransferAmountTxtFld);
                        resetAndDisableField(maxTransferAmountTxtFld);
                    }
                }
            });
            casSlaForm.addField("allowFundTransfer", allowFundTransferChkBox);
        }

        minTransferAmountTxtFld = getFieldWithPermission(MIN_FUND_TRANSFER_AMOUNT, new TextField());
        if (isFieldNotNull(minTransferAmountTxtFld)) {
            minTransferAmountTxtFld.setCaption(application.getMsg("cas.sla.min.transfer.amount.caption"));
            minTransferAmountTxtFld.setWidth(WIDTH);
            minTransferAmountTxtFld.setEnabled(false);
            casSlaForm.addField("minTransferAmount", minTransferAmountTxtFld);
        }

        maxTransferAmountTxtFld = getFieldWithPermission(MAX_FUND_TRANSFER_AMOUNT, new TextField());
        if (isFieldNotNull(maxTransferAmountTxtFld)) {
            maxTransferAmountTxtFld.setCaption(application.getMsg("cas.sla.max.transfer.amount.caption"));
            maxTransferAmountTxtFld.setWidth(WIDTH);
            maxTransferAmountTxtFld.setEnabled(false);
            casSlaForm.addField("maxTransferAmount", maxTransferAmountTxtFld);
        }
    }

    /**
     * Initializes credit reserve request view
     */
    private void createCreditReserveRequestView() {
        allowCreditReserveChkBox = getFieldWithPermission(ALLOW_CREDIT_RESERVE, new CheckBox());
        if (isFieldNotNull(allowCreditReserveChkBox)) {
            allowCreditReserveChkBox.setCaption(application.getMsg("cas.sla.allow.credit.reserve.caption"));
            allowCreditReserveChkBox.addListener(new Property.ValueChangeListener() {
                @Override
                public void valueChange(Property.ValueChangeEvent event) {
                    Boolean enabled = (Boolean) event.getProperty().getValue();
                    if (enabled) {
                        setCommonConfigurations(minReserveAmountTxtFld, "cas.sla.min.reserve.amount.required.error",
                                "ncsCasCurrencyRegex", "cas.sla.min.reserve.amount.numeric.error");
                        setCommonConfigurations(maxReserveAmountTxtFld, "cas.sla.max.reserve.amount.required.error",
                                "ncsCasCurrencyRegex", "cas.sla.max.reserve.amount.numeric.error");
                        addAmountFieldValidators(maxReserveAmountTxtFld, minReserveAmountTxtFld,
                                "cas.sla.min.reserve.amount.greater.max.reserve.amount");
                    } else {
                        resetAndDisableField(minReserveAmountTxtFld);
                        resetAndDisableField(maxReserveAmountTxtFld);
                    }
                }
            });
            casSlaForm.addField("allowCreditReserve", allowCreditReserveChkBox);
        }

        minReserveAmountTxtFld = getFieldWithPermission(MIN_CREDIT_RESERVE_AMOUNT, new TextField());
        if (isFieldNotNull(minReserveAmountTxtFld)) {
            minReserveAmountTxtFld.setCaption(application.getMsg("cas.sla.min.reserve.amount.caption"));
            minReserveAmountTxtFld.setWidth(WIDTH);
            minReserveAmountTxtFld.setEnabled(false);
            casSlaForm.addField("minReserveAmount", minReserveAmountTxtFld);
        }

        maxReserveAmountTxtFld = getFieldWithPermission(MAX_CREDIT_RESERVE_AMOUNT, new TextField());
        if (isFieldNotNull(maxReserveAmountTxtFld)) {
            maxReserveAmountTxtFld.setCaption(application.getMsg("cas.sla.max.reserve.amount.caption"));
            maxReserveAmountTxtFld.setWidth(WIDTH);
            maxReserveAmountTxtFld.setEnabled(false);
            casSlaForm.addField("maxReserveAmount", maxReserveAmountTxtFld);
        }
    }

    /**
     * Initializes debit request view
     */
    private void createDebitRequestView() {
        allowDebitChkBox = getFieldWithPermission(ALLOW_DEBIT, new CheckBox());
        if (isFieldNotNull(allowDebitChkBox)) {
            allowDebitChkBox.setCaption(application.getMsg("cas.sla.allow.debit.caption"));
            allowDebitChkBox.addListener(new Property.ValueChangeListener() {

                @Override
                public void valueChange(Property.ValueChangeEvent event) {
                    Boolean enabled = (Boolean) event.getProperty().getValue();
                    if (enabled) {
                        setCommonConfigurations(minDebitAmountTxtFld, "cas.sla.min.debit.amount.required.error",
                                "ncsCasCurrencyRegex", "cas.sla.min.debit.amount.numeric.error");
                        setCommonConfigurations(maxDebitAmountTxtFld, "cas.sla.max.debit.amount.required.error",
                                "ncsCasCurrencyRegex", "cas.sla.max.debit.amount.numeric.error");
                        addAmountFieldValidators(maxDebitAmountTxtFld, minDebitAmountTxtFld,
                                "cas.sla.min.debit.amount.greater.max.debit.amount");
                    } else {
                        resetAndDisableField(minDebitAmountTxtFld);
                        resetAndDisableField(maxDebitAmountTxtFld);
                    }
                }
            });
            casSlaForm.addField("allowDebit", allowDebitChkBox);
        }

        minDebitAmountTxtFld = getFieldWithPermission(MIN_DEBIT_AMOUNT, new TextField());
        if (isFieldNotNull(minDebitAmountTxtFld)) {
            minDebitAmountTxtFld.setCaption(application.getMsg("cas.sla.min.debit.amount.caption"));
            minDebitAmountTxtFld.setWidth(WIDTH);
            minDebitAmountTxtFld.setEnabled(false);
            if (isDefaultMinDebitAmountAllow()) {
                minDebitAmountTxtFld.setValue(getDefaultMinDebitAmount());
            }
            casSlaForm.addField("minDebitAmount", minDebitAmountTxtFld);
        }

        maxDebitAmountTxtFld = getFieldWithPermission(MAX_DEBIT_AMOUNT, new TextField());
        if (isFieldNotNull(maxDebitAmountTxtFld)) {
            maxDebitAmountTxtFld.setCaption(application.getMsg("cas.sla.max.debit.amount.caption"));
            maxDebitAmountTxtFld.setWidth(WIDTH);
            maxDebitAmountTxtFld.setEnabled(false);
            if (isDefaultMaxDebitAmountAllow()) {
                maxDebitAmountTxtFld.setValue(getDefaultMaxDebitAmount());
            }
            casSlaForm.addField("maxDebitAmount", maxDebitAmountTxtFld);
        }
    }

    /**
     * Initializes credit request view
     */
    private void createCreditRequestView() {
        allowCreditChkBox = getFieldWithPermission(ALLOW_CREDIT, new CheckBox());
        if (isFieldNotNull(allowCreditChkBox)) {
            allowCreditChkBox.setCaption(application.getMsg("cas.sla.allow.credit.caption"));
            allowCreditChkBox.addListener(new Property.ValueChangeListener() {

                @Override
                public void valueChange(Property.ValueChangeEvent event) {
                    Boolean enabled = (Boolean) event.getProperty().getValue();
                    if (enabled) {
                        setCommonConfigurations(minCreditAmountTxtFld, "cas.sla.min.credit.amount.required.error",
                                "ncsCasCurrencyRegex", "cas.sla.min.credit.amount.numeric.error");
                        setCommonConfigurations(maxCreditAmountTxtFld, "cas.sla.max.credit.amount.required.error",
                                "ncsCasCurrencyRegex", "cas.sla.max.credit.amount.numeric.error");
                        addAmountFieldValidators(maxCreditAmountTxtFld, minCreditAmountTxtFld,
                                "cas.sla.min.credit.amount.greater.max.credit.amount");
                    } else {
                        resetAndDisableField(minCreditAmountTxtFld);
                        resetAndDisableField(maxCreditAmountTxtFld);
                    }
                }
            });
            casSlaForm.addField("allowCredit", allowCreditChkBox);
        }

        minCreditAmountTxtFld = getFieldWithPermission(MIN_CREDIT_AMOUNT, new TextField());
        if (isFieldNotNull(minCreditAmountTxtFld)) {
            minCreditAmountTxtFld.setCaption(application.getMsg("cas.sla.min.credit.amount.caption"));
            minCreditAmountTxtFld.setWidth(WIDTH);
            minCreditAmountTxtFld.setEnabled(false);
            casSlaForm.addField("minCreditAmount", minCreditAmountTxtFld);
        }

        maxCreditAmountTxtFld = getFieldWithPermission(MAX_CREDIT_AMOUNT, new TextField());
        if (isFieldNotNull(maxCreditAmountTxtFld)) {
            maxCreditAmountTxtFld.setCaption(application.getMsg("cas.sla.max.credit.amount.caption"));
            maxCreditAmountTxtFld.setWidth(WIDTH);
            maxCreditAmountTxtFld.setEnabled(false);
            casSlaForm.addField("maxCreditAmount", maxCreditAmountTxtFld);
        }
    }

    private void createSubscriptionRequiredView() {
        subscriptionRequiredChkBox = getFieldWithPermission(SUBSCRIPTION_REQUIRED, new CheckBox());
        if (isFieldNotNull(subscriptionRequiredChkBox)) {
            subscriptionRequiredChkBox.setCaption(application.getMsg("cas.sla.subscription.required"));
            subscriptionRequiredChkBox.addValidator(new SubscriptionRequiredValidator(selectedNcses,
                    application.getMsg("cas.sla.subscription.required.error")));
            casSlaForm.addField("subscriptionRequired", subscriptionRequiredChkBox);
        }
    }

    private void createTrustedSelectionView() {
        trustedChkBox = getFieldWithPermission(TRUSTED, new CheckBox());
        if (isFieldNotNull(trustedChkBox)) {
            trustedChkBox.setCaption(application.getMsg("cas.sla.trusted.application"));
            trustedChkBox.addStyleName("red-label");
            casSlaForm.addField("trusted", trustedChkBox);
        }
    }

    /**
     * Create Authorization Medium view
     */
    private void createAuthMediumView() {
        authorizationMediumCombo = getFieldWithPermission(ALLOW_AUTH_MEDIUM, new ComboBox());
        if (isFieldNotNull(authorizationMediumCombo)) {
            authorizationMediumCombo.setCaption(application.getMsg("cas.sla.auth.medium"));
            authorizationMediumCombo.setWidth(WIDTH);
            authorizationMediumCombo.addItem(SMS_COMBO_ITEM);
            authorizationMediumCombo.addItem(USSD_COMBO_ITEM);
            authorizationMediumCombo.setValue(SMS_COMBO_ITEM);
            authorizationMediumCombo.setRequired(true);
            authorizationMediumCombo.setRequiredError(application.getMsg("cas.sla.auth.threshold.required.error"));
            casSlaForm.addField("authMedium", authorizationMediumCombo);
        }
    }

    /**
     * Create Authorization Threshold View
     */
    private void createAuthThresholdView() {
        authorizationThresholdField = getFieldWithPermission(ALLOW_AUTH_THRESHOLD, new TextField());
        if (isFieldNotNull(authorizationThresholdField)) {
            authorizationThresholdField.setCaption(application.getMsg("cas.sla.auth.threshold"));
            authorizationThresholdField.setWidth(WIDTH);
            authorizationThresholdField.setEnabled(true);
            authorizationThresholdField.setRequired(true);
            authorizationThresholdField.setRequiredError(application.getMsg("cas.sla.auth.medium.required.error"));
            authorizationThresholdField.setValue(getDefaultAuthThresholdValue());
            casSlaForm.addField("authThreshold", authorizationThresholdField);
        }
//      TODO - Review when enabling authorization threshold field to "Corporate users group".
//      else {
//            // Setting Default value for authorization threshold.
//            authorizationThresholdField = new TextField();
//            authorizationThresholdField.setValue(getDefaultAuthThresholdValue());
//        }
    }

    private void createInAppPurchaseView() {
        inAppCheckBox = getFieldWithPermission(IN_APP_ALLOWED, new CheckBox());
        if(isFieldNotNull(inAppCheckBox)) {
            inAppCheckBox.setCaption(application.getMsg("cas.sla.in.app.api.allow.caption"));
            inAppCheckBox.addListener(new Property.ValueChangeListener() {
                @Override
                public void valueChange(Property.ValueChangeEvent event) {
                    if (Boolean.TRUE.equals(event.getProperty().getValue())) {
                        final Optional<String> stringOptional = InAppServiceAdapter.queryKey(appId);
                        if(stringOptional.isPresent()) {
                            final TextArea inAppApiKeyTextArea = new TextArea(application.getMsg("cas.sla.in.app.api.key.caption"));
                            inAppApiKeyTextArea.setSizeFull();
                            casSlaForm.addField("inAppTextArea", inAppApiKeyTextArea);
                            inAppApiKeyTextArea.setValue(stringOptional.get());
                            inAppApiKeyTextArea.setReadOnly(true);
                        }
                    } else {
                        casSlaForm.removeItemProperty("inAppTextArea");
                    }
                }
            });
            casSlaForm.addField("inAppCheckBox", inAppCheckBox);
        }
    }

    private void resetAndDisableField(TextField textField) {
        if (isFieldNotNull(textField)) {
            if (!textField.isReadOnly()) {
                textField.setValue("");
            }
            textField.setRequired(false);
            textField.setEnabled(false);
        }
    }

    private void setCommonConfigurations(TextField textField,
                                         String requiredErrorKey,
                                         String regexKey,
                                         String nonNumericErrorKey) {
        if (isFieldNotNull(textField)) {
            textField.setEnabled(true);
            textField.setRequired(true);
            textField.setRequiredError(application.getMsg(requiredErrorKey));
            textField.addValidator(createValidator(regexKey, nonNumericErrorKey));
        }
    }

    private void addAmountFieldValidators(TextField maxAmountTxtFld, TextField minAmountTxtFld, String minMaxErrorMsgKey) {
        if (isFieldNotNull(maxAmountTxtFld)) {
            maxAmountTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("ncsCasMinMaxValidationRegex"),
                    application.getMsg("cas.sla.maximum.amount.validation.error.message")));
            if (isFieldNotNull(minAmountTxtFld)) {
                maxAmountTxtFld.addValidator(new CaasMinMaxValidator(application.getMsg(minMaxErrorMsgKey), minAmountTxtFld));
            }
        }
    }

    protected Validator createValidator(String regexKey, String errorMsgKey) {
        logger.debug("Creating a regex validator with regex [{}] and  error message [{}]", regexKey, errorMsgKey);

        String regex = validationRegistry().regex(regexKey);
        return new RegexpValidator(regex, application.getMsg(errorMsgKey));
    }

    /**
     * @param application
     * @param corporateUserId
     * @param selectedNcses
     */
    public CaasSlaBasicLayout(BaseApplication application,
                              String corporateUserId,
                              List<Map> selectedNcses,
                              String appId) {

        super(application, CAAS_PERMISSION_ROLE_PREFIX);
        this.corporateUserId = corporateUserId;
        this.selectedNcses = selectedNcses;
        this.appId = appId;

        init();
    }

    @Override
    public void reset() {
        resetFields(false, allowFundTransferChkBox, allowCreditReserveChkBox, allowDebitChkBox,
                allowCreditChkBox, allowQueryBalanceChkBox, subscriptionRequiredChkBox, inAppCheckBox);

        resetFields("", postPaidBillDescCodeTxtFld, serviceChargePercentageTxtFld, reasonCodeTxtFld,
                maxTransPerSecTxtFld, maxTransPerDayTxtFld, asyncChargingNotificationUrlTxtFld,
                maxTransferAmountTxtFld, minTransferAmountTxtFld, maxReserveAmountTxtFld,
                minReserveAmountTxtFld, minDebitAmountTxtFld, maxDebitAmountTxtFld,
                minCreditAmountTxtFld, maxCreditAmountTxtFld);
    }

    @Override
    public void setPermissions() {
        setReadWritePermissionToComponent(allowFundTransferChkBox, allowCreditReserveChkBox, allowDebitChkBox,
                allowCreditChkBox, allowQueryBalanceChkBox, subscriptionRequiredChkBox,
                postPaidBillDescCodeTxtFld, serviceChargePercentageTxtFld, reasonCodeTxtFld,
                maxTransPerSecTxtFld, maxTransPerDayTxtFld, asyncChargingNotificationUrlTxtFld,
                maxTransferAmountTxtFld, minTransferAmountTxtFld, maxReserveAmountTxtFld,
                minReserveAmountTxtFld, minDebitAmountTxtFld, maxDebitAmountTxtFld,
                minCreditAmountTxtFld, maxCreditAmountTxtFld);
        chargingLayout.setPermissions();
    }

    @Override
    public void setData(Map<String, Object> data) {
        setValueToField(trustedChkBox, data.get(trustedK));
        setValueToField(maxTransPerSecTxtFld, data.get(maxTransPerSec));
        setValueToField(maxTransPerDayTxtFld, data.get(maxTransPerDay));
        setValueToField(allowFundTransferChkBox, data.get(fundTransferReqAllowed));
        setValueToField(minTransferAmountTxtFld, data.get(minTransferAmount));
        setValueToField(maxTransferAmountTxtFld, data.get(maxTransferAmount));
        setValueToField(allowCreditReserveChkBox, data.get(creditReserveReqAllowed));
        setValueToField(minReserveAmountTxtFld, data.get(minReserveAmount));
        setValueToField(maxReserveAmountTxtFld, data.get(maxReserveAmount));
        setValueToField(allowDebitChkBox, data.get(debitReqAllowed));
        setValueToField(minDebitAmountTxtFld, data.get(minDebitAmount));
        setValueToField(maxDebitAmountTxtFld, data.get(maxDebitAmount));
        setValueToField(allowCreditChkBox, data.get(creditReqAllowed));
        setValueToField(minCreditAmountTxtFld, data.get(minCreditAmount));
        setValueToField(maxCreditAmountTxtFld, data.get(maxCreditAmount));
        setValueToField(asyncChargingNotificationUrlTxtFld, data.get(asyncChargingNotificationUrl));
        setValueToField(subscriptionRequiredChkBox, data.get(subscriptionRequiredK));
        setValueToField(allowQueryBalanceChkBox, data.get(queryBalanceAllowedK));
        setValueToField(postPaidBillDescCodeTxtFld, data.get(postPaidBillDescCodeK));
        setValueToField(serviceChargePercentageTxtFld, data.get(serviceChargePercentageK));
        setValueToField(chargingLayout, data.get(chargingK));
        setValueToField(inAppCheckBox, data.get(inAppPurchasingAllowedK));

        Map chLayout = (Map) data.get(chargingK);

        if(chLayout != null) {
            Map chargingMetaData =  (Map)chLayout.get(metaDataK);
            if (chargingMetaData != null) {
                setValueToField(authorizationMediumCombo, chargingMetaData.get(mobileAccAuthMediumK));
                setValueToField(authorizationThresholdField, chargingMetaData.get(mobileAccAuthThresholdK));
            }
        }

        Map metaData = (Map) data.get(metaDataK);
        if (metaData != null) {
            setValueToField(reasonCodeTxtFld, metaData.get(reasonCodeK));
        }
    }

    @Override
    public void getData(Map<String, Object> data) {
        getValueFromField(trustedChkBox, data, trustedK);
        getValueFromField(maxTransPerSecTxtFld, data, maxTransPerSec);
        getValueFromField(maxTransPerDayTxtFld, data, maxTransPerDay);
        getValueFromField(allowFundTransferChkBox, data, fundTransferReqAllowed);
        getValueFromField(minTransferAmountTxtFld, data, minTransferAmount);
        getValueFromField(maxTransferAmountTxtFld, data, maxTransferAmount);
        getValueFromField(allowCreditReserveChkBox, data, creditReserveReqAllowed);
        getValueFromField(minReserveAmountTxtFld, data, minReserveAmount);
        getValueFromField(maxReserveAmountTxtFld, data, maxReserveAmount);
        getValueFromField(allowDebitChkBox, data, debitReqAllowed);
        getValueFromField(minDebitAmountTxtFld, data, minDebitAmount);
        getValueFromField(maxDebitAmountTxtFld, data, maxDebitAmount);
        getValueFromField(allowCreditChkBox, data, creditReqAllowed);
        getValueFromField(minCreditAmountTxtFld, data, minCreditAmount);
        getValueFromField(maxCreditAmountTxtFld, data, maxCreditAmount);
        getValueFromField(asyncChargingNotificationUrlTxtFld, data, asyncChargingNotificationUrl);
        getValueFromField(subscriptionRequiredChkBox, data, subscriptionRequiredK);
        getValueFromField(allowQueryBalanceChkBox, data, queryBalanceAllowedK);
        getValueFromField(serviceChargePercentageTxtFld, data, serviceChargePercentageK);
        getValueFromField(chargingLayout, data, chargingK);
        getValueFromField(inAppCheckBox, data, inAppPurchasingAllowedK);

        Map value = (Map)data.get(chargingK);
        if (value != null) {
            Map chargingMetaData =  (Map)value.get(metaDataK);
            if(chargingMetaData != null) {
                getValueFromField(authorizationMediumCombo, chargingMetaData, mobileAccAuthMediumK);
                getValueFromField(authorizationThresholdField, chargingMetaData, mobileAccAuthThresholdK);
            } else {
                Map<String, Object> chMetaData = new HashMap<String, Object>();
                value.put(metaDataK, chMetaData);
                getValueFromField(authorizationMediumCombo, chMetaData, mobileAccAuthMediumK);
                getValueFromField(authorizationThresholdField, chMetaData, mobileAccAuthThresholdK);
            }
        }


        Map<String, Object> metaData = new HashMap<String, Object>();
        data.put(metaDataK, metaData);
        getValueFromField(reasonCodeTxtFld, metaData, reasonCodeK);

        setMandatoryValues(data);
    }

    /*
    * Check fields with mandatory values are set by the ui, if not set their default value.
    */
    private void setMandatoryValues(Map<String, Object> data) {
        if(!Optional.fromNullable(data.get(serviceChargePercentageK)).isPresent()) {
            data.put(serviceChargePercentageK, getDefaultServiceChargePercentage());
        }
    }

    @Override
    public void validate() {
        try {
            casSlaForm.validate();
            chargingLayout.validate();
            casSlaForm.setComponentError(null);
        } catch (Validator.InvalidValueException e) {
            casSlaForm.setComponentError(e);
            throw e;
        }
    }
}