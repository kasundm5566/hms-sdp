/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/
package hms.kit.prov.simulator;

import com.vaadin.Application;
import com.vaadin.terminal.gwt.server.PortletApplicationContext2;
import com.vaadin.ui.*;
import hms.kit.prov.simulator.ui.event.PortletEventSender;
import hms.kit.prov.simulator.ui.event.impl.PortletEventSenderImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.portlet.*;
import java.util.HashMap;
import java.util.Map;

/**
* $LastChangedDate$
* $LastChangedBy$
* $LastChangedRevision$
*/
public class ProvSimulator extends Application implements PortletApplicationContext2.PortletListener {

    private static final Logger logger = LoggerFactory.getLogger(ProvSimulator.class);

    @Override
    public void init() {

        final Label currentDataLabel;
        final Map<String, String> data  = new HashMap<String, String>();

        GridLayout gridLayout;

        setMainWindow(new Window("EventSimulator"));

        gridLayout = new GridLayout(1, 3);

        currentDataLabel = new Label();

        /* create first raw with keyword, value and add button. */
        gridLayout.addComponent(createFirstRaw(currentDataLabel, data));
        /* create second raw with data and clear button */
        gridLayout.addComponent(createSecondRaw(currentDataLabel, data));
        /*crete third raw with event name and event fire button */
        gridLayout.addComponent(createThirdRaw(data));

        getMainWindow().setContent(gridLayout);
    }


    private HorizontalLayout createFirstRaw(final Label currentDataLabel, final Map<String, String> data) {
        final Select keySelect;
        final TextField valueTextField;
        Button addButton;
        HorizontalLayout firstRaw;
        keySelect = new Select("Enter key");
        keySelect.setNewItemsAllowed(true);
        keySelect.setImmediate(true);

        valueTextField = new TextField("Enter Value");
        valueTextField.setImmediate(true);

        addButton = new Button("Add");
        addButton.setImmediate(true);

        addButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                String key = keySelect.getValue().toString();
                String value = valueTextField.getValue().toString();
                data.put(key, value);
                currentDataLabel.setValue("Current data : " + data);
            }
        });
        firstRaw = new HorizontalLayout();
        firstRaw.addComponent(keySelect);
        firstRaw.addComponent(valueTextField);
        firstRaw.addComponent(addButton);
        return firstRaw;
    }

    private HorizontalLayout createSecondRaw(final Label currentDataLabel, final Map<String, String> data) {

        HorizontalLayout secondRaw;
        secondRaw = new HorizontalLayout();

        Button clearButton = new Button("Clear");
        clearButton.setImmediate(true);
        clearButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                data.clear();
                currentDataLabel.setValue("Current data : " + data);
            }
        });

        secondRaw.addComponent(currentDataLabel);
        secondRaw.addComponent(clearButton);
        return secondRaw;
    }

    private HorizontalLayout createThirdRaw(final Map<String, String> data) {
        HorizontalLayout thirdRaw;
        final TextField eventNameTextField = new TextField("Event Name");
        eventNameTextField.setImmediate(true);

        final Button fireEventButton = new Button("Fire Event");
        final Application app = this;
        fireEventButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                PortletEventSender eventSender = new PortletEventSenderImpl();
                eventSender.send(app, data, (String) eventNameTextField.getValue());
            }
        });

        thirdRaw = new HorizontalLayout();
        thirdRaw.addComponent(eventNameTextField);
        thirdRaw.addComponent(fireEventButton);
        return thirdRaw;
    }

    public void handleRenderRequest(RenderRequest request, RenderResponse response, Window window) {
        logger.debug("Event Simulator portlet recevied render request");
    }

    public void handleActionRequest(ActionRequest request, ActionResponse response, Window window) {
        logger.debug("Event Simulator portlet recevied action request");
    }

    public void handleEventRequest(EventRequest request, EventResponse response, Window window) {
        logger.debug("Event Simulator portlet recevied event request");
    }

    public void handleResourceRequest(ResourceRequest request, ResourceResponse response, Window window) {
        logger.debug("Event Simulator portlet recevied resource request");

    }


}
