/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/
package hms.kit.prov.simulator.ui.event;

import com.vaadin.Application;

import java.util.Map;

/**
* $LastChangedDate$
* $LastChangedBy$
* $LastChangedRevision$
*/
public interface PortletEventSender {

    void send(Application application, Map keyValuMap, String event);

}
