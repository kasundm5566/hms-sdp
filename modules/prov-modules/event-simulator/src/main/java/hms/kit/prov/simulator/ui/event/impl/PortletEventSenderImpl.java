/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/
package hms.kit.prov.simulator.ui.event.impl;

import com.vaadin.Application;
import com.vaadin.terminal.gwt.server.PortletApplicationContext2;
import hms.kit.prov.simulator.ui.event.PortletEventSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.namespace.QName;
import java.io.Serializable;
import java.util.Map;

/**
* $LastChangedDate$
* $LastChangedBy$
* $LastChangedRevision$
*/
public class PortletEventSenderImpl implements PortletEventSender {

    private static final Logger logger = LoggerFactory.getLogger(PortletEventSenderImpl.class);

    public void send(final Application application, Map keyValueMap, String event) {

        ((PortletApplicationContext2) application.getContext())
                .sendPortletEvent(application.getMainWindow(),
                        new QName("http://vaadin.com/liferay", event), (Serializable) keyValueMap);

        logger.debug("Portlet event data [{}] , name [{}] sent", keyValueMap, event);
    }

}
