#!/bin/sh

server=localhost
user=bmp
LIFERAY_HOME=/hms/installs/liferay-portal-6.0.6/
tomcat_webapps=$LIFERAY_HOME/tomcat-6.0.29/webapps

export LIFERAY_DEPLOY_DIR=$LIFERAY_HOME/deploy

if [ $server = "localhost" ]
then
{
	cp login-hook/target/login-hook.war theme/target/prov-theme.war prov/target/prov.war menu/target/prov-menu.war sms/operator-sms/target/*-sms.war ussd/operator-ussd/target/*-ussd.war $LIFERAY_DEPLOY_DIR
	cp rest/target/prov-api.war $tomcat_webapps
}
else
{
	scp login-hook/target/login-hook.war theme/target/prov-theme.war prov/target/prov.war menu/target/prov-menu.war sms/operator-sms/target/*-sms.war ussd/operator-ussd/target/*-ussd.war $user@$server:$LIFERAY_DEPLOY_DIR
	scp rest/target/prov-api.war $user@$server:$tomcat_webapps
}
fi
