/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.util;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class StringConverterTest {

    private List<String> sampleList;
    private String sampleString;

    @BeforeClass
    public void setup() {
        sampleList = new ArrayList<String>();
        sampleList.add("st1");
        sampleList.add("st2");
        sampleList.add("st3");
        sampleList.add("st4");
        sampleList.add("st5");
        sampleList.add("st6");

        sampleString = "aa,bb,pp,dd,ee,ff,mm";
    }

    @Test
    public void testConvertToStringSuccess() {
        String result = StringConverter.convertToString(sampleList, ", ");

        Assert.assertEquals("st1, st2, st3, st4, st5, st6", result);
    }

    @Test
    public void testConvertToListSuccess() {
        List<String> result = StringConverter.convertToList(sampleString, "\\,");

        Assert.assertEquals("aa", result.get(0));
        Assert.assertEquals("pp", result.get(2));
        Assert.assertEquals("ee", result.get(4));
        Assert.assertEquals("mm", result.get(6));
    }
}
