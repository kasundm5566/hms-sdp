/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.kite.provisioning.commons.ui;

import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.ui.Field;
import com.vaadin.ui.TextField;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static hms.kite.provisioning.commons.ui.VaadinUtil.removeComponentValidators;

public class VaadinUtilTest {
    @Test
    public void testRemoveComponentValidators() throws Exception {
        Field field = new TextField("hello text field");
        field.addValidator(new RegexpValidator("^1234$", "error1"));
        field.addValidator(new RegexpValidator("^2345$", "error2"));
        field.addValidator(new RegexpValidator("^3456$", "error3"));

        removeComponentValidators(field);

        System.out.println("validators " + field.getValidators());
        assertEquals(field.getValidators() == null, true);
    }
}
