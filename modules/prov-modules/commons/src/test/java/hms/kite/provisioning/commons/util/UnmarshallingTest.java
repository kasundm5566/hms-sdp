package hms.kite.provisioning.commons.util;

import hms.pgw.api.response.PayInstrumentResponse;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import static org.testng.Assert.assertEquals;


@Test
public class UnmarshallingTest {
    JAXBContext context;

    @BeforeClass
    public void setUp() throws JAXBException {
    }

    /**
     * <payins-infos payins_name="Equity Bank" payins_id="0" is-default="false" payins_type="ASYNC"/><payins-infos payins_name="M-Pesa" payins_id="0" is-default="false" payins_type="ASYNC"/><payins-infos payins_name="Mobile Account" payins_id="0" is-default="false" payins_type="ASYNC"/></payins-response>
     */
    @Test
    public void testUnmarshalling() throws JAXBException {
        context = JAXBContext.newInstance(PayInstrumentResponse.class);

        String xmlString = "<payins-response status=\"SUCCESS\">" +
                            "<payins-infos payins_name=\"Equity Bank\" payins_id=\"1\" payins-acc-id=\"Equity Bank\" is-default=\"false\" " +"payins_type=\"ASYNC\"/>" +
                            "<payins-infos payins_name=\"M-Pesa\" payins_id=\"2\" payins-acc-id=\"M-Pesa\" is-default=\"false\" payins_type=\"ASYNC\"/>" +
                            "<payins-infos payins_name=\"Mobile Account\" payins_id=\"4\" payins-acc-id=\"Mobile Account\" is-default=\"false\" payins_type=\"ASYNC\"/>" +
                            "<payins-infos payins_name=\"Voucher/Dormans Coffee\" payins_id=\"5\" payins-acc-id=\"Voucher/null\" is-default=\"false\" payins_type=\"SYNC\"/>" +
                            "<payins-infos payins_name=\"Voucher/Steers\" payins_id=\"5\" payins-acc-id=\"Voucher/null\" is-default=\"false\" payins_type=\"SYNC\"/>" +
                            "<payins-infos payins_name=\"Voucher/Kenchic LTD\" payins_id=\"5\" payins-acc-id=\"Voucher/null\" is-default=\"false\" payins_type=\"SYNC\"/>" +
                            "<payins-infos payins_name=\"Voucher/Deacons Kenya Ltd\" payins_id=\"5\" payins-acc-id=\"Voucher/null\" is-default=\"false\" payins_type=\"SYNC\"/>" +
                            "<payins-infos payins_name=\"Voucher/Uchumi Ltd\" payins_id=\"5\" payins-acc-id=\"Voucher/null\" is-default=\"false\" payins_type=\"SYNC\"/>" +
                            "<payins-infos payins_name=\"Voucher/Nakumatt Holding Ltd\" payins_id=\"5\" payins-acc-id=\"Voucher/null\" is-default=\"false\" payins_type=\"SYNC\"/>" +
                            "</payins-response>";

        PayInstrumentResponse response = (PayInstrumentResponse) context.createUnmarshaller().unmarshal(new StringReader(xmlString));
        response.getPayInstrumentInfos();
        assertEquals(response.getPayInstrumentInfos().length, 3);
        assertEquals(response.getPayInstrumentInfos()[0].getPayInstrumentName(), "Equity Bank");
        assertEquals(response.getPayInstrumentInfos()[1].getPayInstrumentName(), "M-Pesa");
    }
}
