/*
*   (C) Copyright 2009-2012 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/
package hms.kite.provisioning.commons.util;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.regex.Pattern;

public class RegExpTest {

    @Test
    public void testChargingAmount() {
        Pattern p = Pattern.compile("([0].[1-9])|([0].[0][1-9])|([0].[1-9][0-9])|([1-9][0-9]*(\\.[0-9]{1,2})?)");

        Assert.assertTrue(p.matcher("1").matches());
        Assert.assertTrue(p.matcher("1.0").matches());
        Assert.assertTrue(p.matcher("0.01").matches());
        Assert.assertTrue(p.matcher("0.50").matches());
        Assert.assertTrue(p.matcher("0.25").matches());
        Assert.assertTrue(p.matcher("2").matches());
        Assert.assertTrue(p.matcher("2.5").matches());
        Assert.assertTrue(p.matcher("2.50").matches());
        Assert.assertTrue(p.matcher("2.55").matches());
        Assert.assertTrue(p.matcher("20").matches());
        Assert.assertTrue(p.matcher("20.5").matches());
        Assert.assertTrue(p.matcher("2000.5").matches());

        Assert.assertFalse(p.matcher("0").matches());
        Assert.assertFalse(p.matcher("0.0").matches());
        Assert.assertFalse(p.matcher("0.00").matches());
        Assert.assertFalse(p.matcher("0.555").matches());
        Assert.assertFalse(p.matcher("-0.55").matches());
        Assert.assertFalse(p.matcher("2.555").matches());
        Assert.assertFalse(p.matcher("02.55").matches());
        Assert.assertFalse(p.matcher("20.555").matches());
        Assert.assertFalse(p.matcher("200000.555").matches());
        Assert.assertFalse(p.matcher("-20").matches());
        Assert.assertFalse(p.matcher("-02.55").matches());
        Assert.assertFalse(p.matcher("-2.55").matches());
    }

    @Test
    public void testMaxChargingAmount() {
        Pattern p = Pattern.compile("([0].[1-9])|([0].[0][1-9])|([0].[1-9][0-9])|([1-9](([0-9]{1,3})?)(\\.[0-9]{1,2})?)|(10000(\\.[0]{1,2})?)");

        Assert.assertTrue(p.matcher("1").matches());
        Assert.assertTrue(p.matcher("10000").matches());
        Assert.assertTrue(p.matcher("10000.00").matches());
        Assert.assertTrue(p.matcher("999.99").matches());
        Assert.assertTrue(p.matcher("0.50").matches());
        Assert.assertTrue(p.matcher("0.01").matches());
        Assert.assertTrue(p.matcher("0.25").matches());
        Assert.assertTrue(p.matcher("2").matches());
        Assert.assertTrue(p.matcher("2.5").matches());
        Assert.assertTrue(p.matcher("2.50").matches());
        Assert.assertTrue(p.matcher("2.55").matches());
        Assert.assertTrue(p.matcher("20").matches());
        Assert.assertTrue(p.matcher("20.5").matches());
        Assert.assertTrue(p.matcher("2000.5").matches());

        Assert.assertFalse(p.matcher("0").matches());
        Assert.assertFalse(p.matcher("0.0").matches());
        Assert.assertFalse(p.matcher("0.00").matches());
        Assert.assertFalse(p.matcher("0.555").matches());
        Assert.assertFalse(p.matcher("-0.55").matches());
        Assert.assertFalse(p.matcher("2.555").matches());
        Assert.assertFalse(p.matcher("02.55").matches());
        Assert.assertFalse(p.matcher("20.555").matches());
        Assert.assertFalse(p.matcher("10000.01").matches());
        Assert.assertFalse(p.matcher("99999.50").matches());
        Assert.assertFalse(p.matcher("-100.50").matches());
        Assert.assertFalse(p.matcher("-20").matches());
        Assert.assertFalse(p.matcher("-02.55").matches());
        Assert.assertFalse(p.matcher("-2.55").matches());
    }

    @Test
    public void testKeywordWithoutPremium() {
        Pattern p = Pattern.compile("(?!000|111|222|333|444|555|666|777|771|888|999)([0-9]{3})");

        Assert.assertTrue(p.matcher("001").matches());
        Assert.assertTrue(p.matcher("021").matches());
        Assert.assertTrue(p.matcher("123").matches());
        Assert.assertTrue(p.matcher("451").matches());
        Assert.assertTrue(p.matcher("456").matches());
        Assert.assertTrue(p.matcher("998").matches());

        //Premium set
        Assert.assertFalse(p.matcher("000").matches());
        Assert.assertFalse(p.matcher("111").matches());
        Assert.assertFalse(p.matcher("222").matches());
        Assert.assertFalse(p.matcher("333").matches());
        Assert.assertFalse(p.matcher("444").matches());
        Assert.assertFalse(p.matcher("555").matches());
        Assert.assertFalse(p.matcher("666").matches());
        Assert.assertFalse(p.matcher("777").matches());
        Assert.assertFalse(p.matcher("771").matches());
        Assert.assertFalse(p.matcher("888").matches());
        Assert.assertFalse(p.matcher("999").matches());

        //Other invalid
        Assert.assertFalse(p.matcher("1").matches());
        Assert.assertFalse(p.matcher("90").matches());
        Assert.assertFalse(p.matcher("4546").matches());
    }
}
