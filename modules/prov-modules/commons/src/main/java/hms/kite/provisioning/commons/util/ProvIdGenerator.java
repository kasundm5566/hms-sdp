/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.provisioning.commons.util;

import java.text.DecimalFormat;

import static hms.kite.datarepo.RepositoryServiceRegistry.systemConfiguration;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ProvIdGenerator {

    private static final String ID_FORMAT = "000000";

    protected ProvIdGenerator() {

    }

    /**
     * Generate app id.
     *
     * @return - generated id like APP_00001
     */
    public static String generateAppId() {
        return "APP_" + new DecimalFormat(ID_FORMAT).format(systemConfiguration().incrementAndGetNextId("app"));
    }

    /**
     * Generate sp id by appending _id.
     *
     * @return - generated id like SPP_00001
     */
    public static String generateSpid() {
        return "SPP_" + new DecimalFormat(ID_FORMAT).format(systemConfiguration().incrementAndGetNextId("sp"));
    }

    /**
     * Generate ncs id.
     *
     * @return - generated id like SPP_00001
     */
    public static String generateNcsId() {
        return "NCS_" + new DecimalFormat(ID_FORMAT).format(systemConfiguration().incrementAndGetNextId("ncs"));
    }

    /**
     * Generate routing-key id.
     *
     * @return - generated id like SPP_00001
     */
    public static String generateRoutingKeyId() {
        return "RK_" + new DecimalFormat(ID_FORMAT).format(systemConfiguration().incrementAndGetNextId("rk"));
    }

    /**
     * Generate cms id.
     *
     * @return - generated id like CMS_00001
     */
    public static String generateCmsAppId() {
        return "CMS_" + new DecimalFormat(ID_FORMAT).format(systemConfiguration().incrementAndGetNextId("cms"));
    }
}
