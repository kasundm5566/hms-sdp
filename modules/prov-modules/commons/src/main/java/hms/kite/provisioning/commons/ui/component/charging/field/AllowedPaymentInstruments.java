/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.ui.component.charging.field;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.Maps;
import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.ui.*;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.validator.TillNumberExistsValidator;
import hms.kite.provisioning.commons.ui.validator.TillNumberListValidator;
import hms.kite.util.SdpException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.addon.customfield.CustomField;

import java.util.*;

import static hms.kite.provisioning.commons.ui.component.charging.field.PaymentInstrumentCustomFieldDataRegistry.getCustomFieldByPaymentInstrumentName;
import static hms.kite.provisioning.commons.util.ProvChargingConfiguration.*;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

public class AllowedPaymentInstruments extends ChargingField {

    private static final Logger logger = LoggerFactory.getLogger(AllowedPaymentInstruments.class);

    private static final String WIDTH = "220px";
    private static final String TILL_NO_SEPERATOR = ",";

    private final boolean mobileAccountAllow;
    private final boolean otherPaymentInstrumentAllow;
    private final Set<Object> allowedList;
    private final Set<Object> notAllowedList;
    private final String corporateUserId;
    private final String width;
    private final int rows;
    private final int columns;
    private final Map<String, CheckBox> paymentInstruments = new HashMap<String, CheckBox>();
    private final Map<String, String> paymentInsNameIdMap = new HashMap<String, String>();

    //private String appStatus;

    private Map<String, Field> additionalFields = new HashMap<String, Field>();

    private Map<String, Field> paymentInstrumentAdditionalData =  new HashMap<String, Field>();

    private HashSet<Object> defaultPaymentInstrument;

    private String currentChargingType;

    private VerticalLayout verticalLayout;

    private PaymentInstrumentComponent paymentInstrumentComponent;

    private boolean isChargingType(String chargingType) {
        return chargingType.equals(currentChargingType);
    }

    private boolean isNoOtherPaymentInstrumentAllowed() {
        return !otherPaymentInstrumentAllow && allowedList.isEmpty();
    }

    private boolean isNoPaymentInstrumentAllowed() {
        return !mobileAccountAllow && isNoOtherPaymentInstrumentAllowed();
    }

    private void populatePaymentInstruments(Layout layout) {

        try {
            Map<String, Map<String, Object>> paymentInsListByPGW =
                    paymentGatewayAdaptor().paymentInstruments(corporateUserId);

            logger.debug("Payment instrument list for user [{}] received [{}].", corporateUserId, paymentInsListByPGW);

            for (Map<String, Object> instrument : paymentInsListByPGW.values()) {
                String paymentInsName = (String)instrument.get(nameK);
                Map<String, Map<String, String>> accounts =
                        (Map<String, Map<String, String>>)instrument.get(accountsK);
                String accountId = accounts.values().iterator().next().get(accountIdK);

                if ((otherPaymentInstrumentAllow && !notAllowedList.contains(paymentInsName))
                        || (!otherPaymentInstrumentAllow && allowedList.contains(paymentInsName))) {

                    CheckBox checkBox = new CheckBox(paymentInsName);
                    checkBox.setImmediate(true);

                    paymentInstruments.put(paymentInsName, checkBox);
                    paymentInsNameIdMap.put(paymentInsName, accountId);
                }
            }

//            if (!mobileAccountAllow) {
//                paymentInstruments.remove(getMobileAccountName());
//                paymentInsNameIdMap.remove(getMobileAccountName());
//            }

            // add label to payment instrument grid verticalLayout
            String mpesaPaymentInsName = application.getMsg("common.charging.allowed.payment.instrument.mpesa.name");
            String buygoodsPaymentInsName = application.getMsg("common.charging.allowed.payment.instrument.mpesa.buygoods.name");

            //remove mpesa buy goods from pi list (ncs-wise)
            if (!isMpesaBuyGoodsPiAllow()) {
               paymentInstruments.remove(buygoodsPaymentInsName);
               paymentInsNameIdMap.remove(buygoodsPaymentInsName);
            }

            for (final String checkBoxKey : paymentInstruments.keySet()) {
                CheckBox checkBox = paymentInstruments.get(checkBoxKey);
                verticalLayout.addComponent(checkBox);

                checkBox.addListener(new Property.ValueChangeListener() {
                    @Override
                    public void valueChange(Property.ValueChangeEvent event) {
                        if(Boolean.TRUE.equals(event.getProperty().getValue())) {
                            Optional<Field> paymentInstrumentCustomDataFieldOpt = getCustomFieldByPaymentInstrumentName(checkBoxKey, application);
                            if(paymentInstrumentCustomDataFieldOpt.isPresent()) {
                                addAdditionalPaymentInstrumentAdditionalDataFiled(checkBoxKey, paymentInstrumentCustomDataFieldOpt);
                            }
                        } else {
                            removePaymentInstrumentAdditionalDataField(checkBoxKey);
                        }
                    }
                });

                if(checkBoxKey.equals(mpesaPaymentInsName)) {
                    verticalLayout.addComponent(generateMpesaPayBillNoField(mpesaPaymentInsName));
                } else if (checkBoxKey.equals(buygoodsPaymentInsName)) {
                    verticalLayout.addComponent(generateBuyGoodsTillNoField(buygoodsPaymentInsName));
                    verticalLayout.addComponent(generateBuyGoodsTillNoTip(buygoodsPaymentInsName));
                }
            }

            // If only one payment instrument is listed, then select it by default
            if(paymentInstruments.size() == 1) {
                CheckBox paymentIns = paymentInstruments.values().iterator().next();
                paymentIns.setEnabled(true);
                paymentIns.setReadOnly(true);
            }

            // adding custom component for payment instruments
            layout.addComponent(paymentInstrumentComponent);

        } catch (SdpException e) {
            logger.error(e.getMessage());
        } catch (Exception e) {
            logger.error("Error occurred while loading payment instrument list", e);
        }
    }

    private void addAdditionalPaymentInstrumentAdditionalDataFiled(String checkBoxKey, Optional<Field> paymentInstrumentCustomDataFieldOpt) {
        Field paymentInstrumentsCustomDataField = paymentInstrumentCustomDataFieldOpt.get();
        verticalLayout.addComponent(paymentInstrumentsCustomDataField);
        paymentInstrumentAdditionalData.put(checkBoxKey, paymentInstrumentsCustomDataField);
    }

    private void removePaymentInstrumentAdditionalDataField(String key) {
        Field field = paymentInstrumentAdditionalData.get(key);
        if(Optional.fromNullable(field).isPresent()) {
            verticalLayout.removeComponent(field);
            paymentInstrumentAdditionalData.remove(key);
        }
    }

    private AllowedPaymentInstruments(boolean mobileAccountAllow, boolean otherPaymentInstrumentAllow,
                                      Set<Object> allowedList, Set<Object> notAllowedList, String corporateUserId,
                                      BaseApplication application, String moduleRolePrefix,
                                      String width, int rows, int columns) {
        super(application, moduleRolePrefix);

        this.mobileAccountAllow = mobileAccountAllow;
        this.otherPaymentInstrumentAllow = otherPaymentInstrumentAllow;
        this.allowedList = allowedList;
        this.notAllowedList = notAllowedList;
        this.corporateUserId = corporateUserId;
        this.width = width;
        this.rows = rows;
        this.columns = columns;

        verticalLayout = new VerticalLayout();
        paymentInstrumentComponent = getFieldWithPermission(ALLOWED_PAYMENT_INSTRUMENT, new PaymentInstrumentComponent(verticalLayout));
    }

    @Override
    public void init(Layout layout) {
        if (isNoPaymentInstrumentAllowed()) {
            defaultPaymentInstrument = new HashSet<Object>();
            defaultPaymentInstrument.add(getDefaultAllowedPaymentInstrument());
            return;
        }

        if(!isNoOtherPaymentInstrumentAllowed()) {
            populatePaymentInstruments(layout);
        }

        if(isChargingType(freeK)) {
            hidePaymentInstruments();
        }
    }

    private Field generateMpesaPayBillNoField(String mpesaPaymentInsName) {

        final TextField payBillNoTextField = new TextField();
        additionalFields.put(mpesaPayBillNoK, payBillNoTextField);

        payBillNoTextField.setCaption(
                application.getMsg("common.charging.allowed.payment.instrument.mpesa.paybill.no.caption"));

        payBillNoTextField.setWidth(WIDTH);
        payBillNoTextField.setVisible(false);
        payBillNoTextField.setRequired(true);
        payBillNoTextField.setImmediate(true);

        payBillNoTextField.setRequiredError(
                application.getMsg("common.charging.allowed.payment.instrument.mpesa.paybill.no.required.error"));

        payBillNoTextField.addValidator(new RegexpValidator(getMpesaPayBillNoRegex(),
                application.getMsg("common.charging.allowed.payment.instrument.mpesa.paybill.no.length.validation.error")));

        CheckBox checkBox = paymentInstruments.get(mpesaPaymentInsName);

        checkBox.addListener(new ComboBox.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                if(valueChangeEvent.getProperty().getValue().equals(true)) {
                    payBillNoTextField.setVisible(true);
                }
                else payBillNoTextField.setVisible(false);
            }
        });

        return payBillNoTextField;
    }

    private Field generateBuyGoodsTillNoField(String buyGoodsPaymentInsName) {
        final TextArea tillNoTextField = new TextArea();
        additionalFields.put(mpesaBuyGoodsTillNoK, tillNoTextField);

        tillNoTextField.setCaption(application.getMsg("common.charging.allowed.payment.instrument.mpesa.buygoods.till.no.caption"));
        tillNoTextField.setDescription(application.getMsg("common.charging.allowed.payment.instrument.mpesa.buygoods.till.no.tip"));

        tillNoTextField.setWidth(WIDTH);
        tillNoTextField.setVisible(false);
        tillNoTextField.setRequired(true);
        tillNoTextField.setImmediate(true);

        tillNoTextField.setRequiredError(
                application.getMsg("common.charging.allowed.payment.instrument.mpesa.buygoods.till.no.required.error"));

        tillNoTextField.addValidator(new TillNumberListValidator(TILL_NO_SEPERATOR,
                application.getMsg("common.charging.allowed.payment.instrument.mpesa.buygoods.till.no.length.validation.error")));

        //todo when in view mode, validation should not occur
        tillNoTextField.addValidator(new TillNumberExistsValidator(TILL_NO_SEPERATOR,
                application.getMsg("common.charging.allowed.payment.instrument.mpesa.buygoods.till.no.already.exists")));


        CheckBox checkBox = paymentInstruments.get(buyGoodsPaymentInsName);

        checkBox.addListener(new ComboBox.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                if(valueChangeEvent.getProperty().getValue().equals(true)) {
                    tillNoTextField.setVisible(true);
                }
                else tillNoTextField.setVisible(false);
            }
        });


        return tillNoTextField;
    }

    private Label generateBuyGoodsTillNoTip(String buyGoodsPaymentInsName) {
        final Label tillNoTip = new Label();
        tillNoTip.setValue(application.getMsg("common.charging.allowed.payment.instrument.mpesa.buygoods.till.no.tip"));

        tillNoTip.setWidth(WIDTH);
        tillNoTip.setVisible(false);
        tillNoTip.setStyleName("textfield-tip");

        CheckBox checkBox = paymentInstruments.get(buyGoodsPaymentInsName);

        checkBox.addListener(new ComboBox.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                if(valueChangeEvent.getProperty().getValue().equals(true)) {
                    tillNoTip.setVisible(true);
                }
                else tillNoTip.setVisible(false);
            }
        });

        return tillNoTip;
    }

    @Override
    public void chargingValueChange(Map<String, Object> data) {
        final Object chargingType = data.get(typeK);
        currentChargingType = Optional.fromNullable(chargingType).isPresent() ? (String) chargingType : freeK;

        if (paymentInstruments.isEmpty() || isNoPaymentInstrumentAllowed()) {
            return;
        }

        if (isChargingType(freeK)) {
            hidePaymentInstruments();
        } else {
            showPaymentInstruments();
        }
    }

    private void hidePaymentInstruments() {
        paymentInstrumentComponent.setVisible(false);
    }

    private void showPaymentInstruments() {
        paymentInstrumentComponent.setVisible(true);
    }

    @Override
    public void setData(Map<String, Object> data) {
        if (!isChargingType(freeK)) {
            if (isNoPaymentInstrumentAllowed()) {
                Collection<?> allowedPaymentInstruments = (Collection<?>) data.get(allowedPaymentInstrumentsK);
                if (allowedPaymentInstruments != null) {
                    defaultPaymentInstrument = new HashSet<Object>();
                    defaultPaymentInstrument.addAll(allowedPaymentInstruments);
                }
            } else {
                List<String> allowedPayIns = (List<String>) data.get(allowedPaymentInstrumentsK);
                if(allowedPayIns != null) {
                    for (String payIns : paymentInstruments.keySet()) {
                        if(allowedPayIns.contains(paymentInsNameIdMap.get(payIns))) {
                            setValueToField(paymentInstruments.get(payIns), true);
                        }
                    }
                }

                Map<String, Object> metaData = (Map<String, Object>) data.get(metaDataK);

                if(metaData != null) {
                    setMetaData(metaData, mpesaPayBillNoK);
                    setMetaData(metaData, mpesaBuyGoodsTillNoK);
                }

                if(Optional.fromNullable(data.get(paymentInstrumentAdditionalDataK)).isPresent()) {
                    Map<String, Object> paymentInstrumentData = (Map<String, Object>) data.get(paymentInstrumentAdditionalDataK);
                    for (String paymentInsName : paymentInstrumentData.keySet()) {
                        Field field = paymentInstrumentAdditionalData.get(paymentInsName);
                        if(Optional.fromNullable(field).isPresent()) {
                            field.setValue(paymentInstrumentData.get(paymentInsName));
                        }
                    }
                }
            }
        }
    }

    private void setMetaData(Map<String, Object> metaData, String key){
        if(metaData.containsKey(key) && additionalFields.get(key) != null) {
            additionalFields.get(key).setVisible(true);
            if(key.equals(mpesaPayBillNoK)) {
                setValueToField(additionalFields.get(key), metaData.get(key));
            } else if (key.equals(mpesaBuyGoodsTillNoK)) {
                setValueToField(additionalFields.get(key),
                        StringUtils.join((ArrayList<String>) metaData.get(key), TILL_NO_SEPERATOR));
            }
        }
    }

    private boolean isFreeCharging() {
        if(Optional.fromNullable(currentChargingType).isPresent()) {
            return isChargingType(freeK);
        } else {
            return isNoPaymentInstrumentAllowed();
        }
    }

    @Override
    public void getData(Map<String, Object> data) {
        if (!isFreeCharging()) {
            if (defaultPaymentInstrument != null && isNoPaymentInstrumentAllowed()) {
                data.put(allowedPaymentInstrumentsK, defaultPaymentInstrument);
            } else if(mobileAccountAllow) {
                data.put(allowedPaymentInstrumentsK, Arrays.asList(getMobileAccountId()));
            } else {
                List<String> allowedPayIns = new ArrayList<String>();
                for (String paymentIns : paymentInstruments.keySet()) {
                    if(paymentInstruments.get(paymentIns).getValue() == Boolean.TRUE) {
                        allowedPayIns.add(paymentInsNameIdMap.get(paymentIns));
                    }
                }
                data.put(allowedPaymentInstrumentsK, allowedPayIns);

                Map<String, Object> metadataMap = new HashMap<String, Object>();
                metadataMap = getMetaData(metadataMap, mpesaPayBillNoK);
                metadataMap = getMetaData(metadataMap, mpesaBuyGoodsTillNoK);

                data.put(metaDataK, metadataMap);

                /*
                * Add values from additional data fields for payment instruments
                */
                Map<String, Object> additionalDataMap = new HashMap<>();
                for (String paymentInsKey : paymentInstrumentAdditionalData.keySet()) {
                    additionalDataMap.put(paymentInsKey, paymentInstrumentAdditionalData.get(paymentInsKey).getValue());
                }

                data.put(paymentInstrumentAdditionalDataK, additionalDataMap);
            }
        }
    }

    private Map<String, Object> getMetaData(Map<String, Object> metadataMap, String key) {
        if(additionalFields.get(key) != null && additionalFields.get(key).isVisible()) {
            if(key.equals(mpesaPayBillNoK)){
                metadataMap.put(key, additionalFields.get(key).getValue());
            } else if(key.equals(mpesaBuyGoodsTillNoK)) {
                String[] tillNoArray = String.valueOf(additionalFields.get(key).getValue()).split(TILL_NO_SEPERATOR);
                List<String> tillNoList = new ArrayList<String>();
                for(String tillNo : tillNoArray) {
                    if(!tillNo.trim().isEmpty() && !tillNoList.contains(tillNo)) {
                        tillNoList.add(tillNo.trim());
                    }
                }
                metadataMap.put(key, tillNoList);
                metadataMap.put(partialMsisdnAllowedK, "true");
            }
        }
        return metadataMap;
    }

    @Override
    public void validate() {
        if (isChargingType(freeK) || isNoOtherPaymentInstrumentAllowed()) {
            return;
        }
        boolean noInstrumentSelected = true;
        for (CheckBox checkBox : paymentInstruments.values()) {
            if(checkBox.getValue().equals(true)) {
                noInstrumentSelected = false;
            }
        }
        if(noInstrumentSelected) {
            throw new Validator.InvalidValueException(
                    application.getMsg("common.charging.payment.instrument.not.selected.error"));
        }

        validateAdditionalFields(mpesaPayBillNoK);
        validateAdditionalFields(mpesaBuyGoodsTillNoK);

        for (Field field : paymentInstrumentAdditionalData.values()) {
            field.validate();
        }
    }

    private void validateAdditionalFields(String key) {
        if(additionalFields.get(key) != null && additionalFields.get(key).isVisible()) {
            validateField(additionalFields.get(key));
        }
    }

    @Override
    public void reset() {
    }

    @Override
    public void setPermissions() {
        setReadWritePermissionToComponent(paymentInstrumentComponent);
    }

    public static class Builder {

        private final String corporateUserId;
        private final BaseApplication application;
        private final String permissionRolePrefix;
        private final String width;
        private final int rows;
        private final int columns;

        private boolean otherPaymentInstrumentAllow = false;
        private boolean mobileAccountAllow = false;
        private Set<Object> allowedList;
        private Set<Object> notAllowedList;

        public Builder(String corporateUserId, BaseApplication application, String permissionRolePrefix, String width,
                       int rows, int columns) {
            this.corporateUserId = corporateUserId;
            this.application = application;
            this.permissionRolePrefix = permissionRolePrefix;
            this.width = width;
            this.rows = rows;
            this.columns = columns;

            this.allowedList = new HashSet<Object>();
            this.notAllowedList = new HashSet<Object>();
        }

        public Builder allow(Set<Object> allowedList) {
            this.allowedList = allowedList;
            return this;
        }

        public Builder notAllow(Set<Object> notAllowedList) {
            this.notAllowedList = notAllowedList;
            return this;
        }

        public Builder mobileAccount() {
            mobileAccountAllow = isMobileAccountPaymentInstrumentAllow();
            return this;
        }

        public Builder otherPaymentInstrument() {
            otherPaymentInstrumentAllow = isOtherPaymentInstrumentAllow();
            return this;
        }

        public AllowedPaymentInstruments build() {
            return new AllowedPaymentInstruments(mobileAccountAllow, otherPaymentInstrumentAllow,
                    allowedList, notAllowedList, corporateUserId, application,
                    permissionRolePrefix, width, rows, columns);
        }

    }

    private class PaymentInstrumentComponent extends CustomField {

        public PaymentInstrumentComponent(VerticalLayout verticalLayout) {
            this.setCaption(application.getMsg("common.charging.allowed.payment.instrument.caption"));

            HorizontalLayout layout = new HorizontalLayout();
            setRequired(true);
            setRequiredError(application.getMsg("common.charging.allowed.payment.instrument.required.error"));
            layout.setSpacing(true);

            layout.addComponent(verticalLayout);
            setCompositionRoot(layout);
        }


        @Override
        public Class<?> getType() {
            return PaymentInstrumentComponent.class;
        }
    }

}