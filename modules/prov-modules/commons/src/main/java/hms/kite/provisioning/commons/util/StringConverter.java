/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.util;


import java.util.ArrayList;
import java.util.List;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class StringConverter {

    public static final String SEPARATOR = "\\,";
    public static final String JOIN_SEPARATOR = ", ";

    public String convertToString(List<String> stringList) {
        StringBuilder concatenatedString = new StringBuilder();
        for (String string : stringList) {
            concatenatedString.append(string);
            concatenatedString.append(",");
        }
        String commaSeparatedString;
        try {
            commaSeparatedString = concatenatedString.toString().substring(0,
                    concatenatedString.toString().length() - 1);
        } catch (Exception e) {
            commaSeparatedString = "";
        }
        return commaSeparatedString;
    }

    public List<String> convertToSet(String commaSeparatedList) {
        String[] stringArray = commaSeparatedList.split(",");
        List<String> stringList = new ArrayList<String>();
        for (String aStringArray : stringArray) {
            stringList.add(aStringArray.trim());
        }
        return stringList;
    }

    public static List<String> convertToList(String value, String separator) {
        List<String> result = new ArrayList<String>();
        String[] entryList = value.trim().split(separator);
        for (String entry : entryList) {
            String trimmedEntry = entry.trim();
            if (!trimmedEntry.equals("")) {
                result.add(trimmedEntry);
            }
        }
        return result;
    }

    public static String convertToString(List<String> valueList, String separator) {
        int loop = 0;
        StringBuilder result = new StringBuilder();
        for (String value : valueList) {
            value = value.trim();
            if (value.equals("")) {
                continue;
            }
            if (loop == 0) {
                result.append(value);
            } else {
                result.append(separator).append(value);
            }
            loop++;
        }
        return result.toString();
    }
}
