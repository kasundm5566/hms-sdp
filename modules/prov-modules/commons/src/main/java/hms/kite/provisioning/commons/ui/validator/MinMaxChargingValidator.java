/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.ui.validator;

import com.vaadin.data.Validator;
import com.vaadin.ui.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class MinMaxChargingValidator implements Validator {

    private static Logger logger = LoggerFactory.getLogger(MinMaxChargingValidator.class);

    private String message;
    private TextField minTxtFld;

    public MinMaxChargingValidator(String message, TextField minTxtFld) {
        this.message = message;
        this.minTxtFld = minTxtFld;
    }

    @Override
    public void validate(Object value) throws InvalidValueException {
        if (!isValid(value)) {
            logger.debug("Min value [{}] is greater than the Max value [{}]", minTxtFld.getValue(), value);
            throw new InvalidValueException(message);
        }
    }

    @Override
    public boolean isValid(Object value) {
        try {
            double max = Double.parseDouble(value.toString());
            double min = Double.parseDouble(minTxtFld.getValue().toString());
            if (min < max) {
                return true;
            }
        } catch (Exception e) {
            logger.error("Unexpected error occurred while validating", e);
        }
        return false;
    }
}
