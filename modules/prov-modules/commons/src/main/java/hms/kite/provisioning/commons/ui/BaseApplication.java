/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.provisioning.commons.ui;

import com.vaadin.Application;
import com.vaadin.terminal.gwt.server.PortletApplicationContext2;
import com.vaadin.ui.Window;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.portlet.*;
import java.text.MessageFormat;
import java.util.*;

import static hms.kite.provisioning.commons.util.ProvKeyBox.kiteUserRolesK;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public abstract class BaseApplication extends Application implements I18nProvider, PortletApplicationContext2.PortletListener {

    private static Logger logger = LoggerFactory.getLogger(BaseApplication.class);

    private transient UiManager uiManager;
    private ResourceBundle i18nBundle;
    private PortletApplicationContext2 portletContext;
    private Map<String, Object> applicationSession;
    private ResourceBundle resourceBundle;


    protected BaseApplication() {
        this.applicationSession = new HashMap<String, Object>();

    }

    protected void setUiManager(UiManager uiManager) {
        this.uiManager = uiManager;
    }


    public UiManager getUiManager() {
        return uiManager;
    }

    /**
     * Returns the bundle for the current locale.
     *
     * @return resourceBundle
     */
    @Override
    public ResourceBundle getI18nBundle() {
        return i18nBundle;
    }

    @Override
    public void setLocale(Locale locale) {
        super.setLocale(locale);
        this.i18nBundle = ResourceBundle.getBundle(getApplicationName(), locale);
    }

    /**
     * Returns a localized message from the resource bundle
     * with the current application locale.
     *
     * @param key keyword
     * @return value
     * @deprecated
     */
    @Override
    @Deprecated
    public String getMessage(String key) {
        return "?? " + key + " ??";
    }

    public void setResourceBundle(String locale) {
        resourceBundle = ResourceBundle.getBundle(getApplicationName(), new Locale(locale));
    }

    public String getMsg(String key, String... arguments) {
        try {
            if (arguments != null) {
                return MessageFormat.format(resourceBundle.getString(key), arguments);
            } else {
                return resourceBundle.getString(key);
            }
        } catch (MissingResourceException mre) {
            logger.error("Message key not found [" + key + "]");
            return '!' + key;
        }

    }

    public void showErrorNotification(String caption, String message) {
        Window.Notification notification = new Window.Notification(caption, message, Window.Notification.TYPE_ERROR_MESSAGE);
        getMainWindow().showNotification(notification);
    }

    protected void setPortletContext(PortletApplicationContext2 portletContext) {
        this.portletContext = portletContext;
    }

    public PortletApplicationContext2 getPortletContext() {
        return portletContext;
    }

    public void addToSession(String key, Object object) {
        this.applicationSession.put(key, object);
    }

    public Object removeFromSession(String key) {
        return this.applicationSession.remove(key);
    }

    public Object getFromSession(String key) {
        return this.applicationSession.get(key);
    }

    protected abstract String getApplicationName();

    public void handleRenderRequest(RenderRequest request, RenderResponse response, Window window) {
        logger.debug("{} portlet received a render request", getApplicationName());
    }

    public void handleActionRequest(ActionRequest request, ActionResponse response, Window window) {
        logger.debug("{} portlet received an action request", getApplicationName());
    }

    public void handleEventRequest(EventRequest request, EventResponse response, Window window) {
        logger.debug("{} portlet received a event request", getApplicationName());

        setUserRoles(request);
    }

    public void handleResourceRequest(ResourceRequest request, ResourceResponse response, Window window) {
        logger.debug("{} portlet received a resource request ", getApplicationName());

        setUserRoles(request);
    }

    private void setUserRoles(PortletRequest request) {
        if (getFromSession(kiteUserRolesK) == null) {
            logger.debug("Adding user roles to the application session");

            addToSession(kiteUserRolesK, request.getPortletSession().getAttribute(kiteUserRolesK, PortletSession.APPLICATION_SCOPE));
        }
    }

    public boolean hasAnyRole(String... roles) {
        HashSet<String> userRoles = (HashSet) getFromSession(kiteUserRolesK);
        if (userRoles == null) {
            return false;
        }
        for (String role : roles) {
            if (userRoles.contains(role)) {
                return true;
            }
        }
        return false;
    }

}
