package hms.kite.provisioning.commons.service.in.app.connector;

public class QueryAppKeyResponse {

    private String statusCode;
    private String statusDescription;
    private String result;

    public QueryAppKeyResponse() {
    }

    public QueryAppKeyResponse(String statusCode, String statusDescription, String result) {
        this.statusCode = statusCode;
        this.statusDescription = statusDescription;
        this.result = result;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("RegisterAppResponse{");
        sb.append("statusCode='").append(statusCode).append('\'');
        sb.append(", statusDescription='").append(statusDescription).append('\'');
        sb.append(", result=").append(result);
        sb.append('}');
        return sb.toString();
    }

}
