/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.ui.component.popup;

import com.vaadin.ui.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 * <p/>
 * This will display user to confirm the action which user did
 */
public class ConfirmationBox {

    private Window window;
    private String title;
    private String yesCaption;
    private String noCaption;
    private String content;
    private ConfirmationListener listener;
    private Window parentWindow;

    private void init() {
        createWindow();
        createLayout();
    }

    private void createWindow() {
        window = new Window(title);
        window.setWidth("300px");
        window.setHeight("100px");
        window.setResizable(false);
    }

    private void createLayout() {
        VerticalLayout layout = (VerticalLayout) window.getContent();
        layout.setMargin(true);
        layout.setSpacing(true);
        layout.setSizeFull();

        Label message = new Label(content);
        window.addComponent(message);

        Button yes = createYesButton();
        Button no = createNoButton();

        HorizontalLayout option = new HorizontalLayout();
        option.setSpacing(true);
        option.addComponent(yes);
        option.addComponent(no);

        layout.addComponent(option);
        layout.setComponentAlignment(option, Alignment.BOTTOM_CENTER);
    }

    private Button createYesButton() {
        Button yes = new Button(yesCaption);
        yes.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                listener.onYes();
                (window.getParent()).removeWindow(window);
            }
        });
        return yes;
    }

    private Button createNoButton() {
        Button no = new Button(noCaption);
        no.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                listener.onNo();
                (window.getParent()).removeWindow(window);
            }
        });
        return no;
    }

    public ConfirmationBox(String title,
                           String yesCaption,
                           String noCaption,
                           String content,
                           ConfirmationListener listener,
                           Window parentWindow) {
        this.title = title;
        this.yesCaption = yesCaption;
        this.noCaption = noCaption;
        this.content = content;
        this.listener = listener;
        this.parentWindow = parentWindow;

        init();
    }

    public void viewConfirmation() {
        if (window.getParent() == null) {
            parentWindow.addWindow(window);
        }
        window.center();
    }

    public interface ConfirmationListener {

        public void onYes();

        public void onNo();
    }
}