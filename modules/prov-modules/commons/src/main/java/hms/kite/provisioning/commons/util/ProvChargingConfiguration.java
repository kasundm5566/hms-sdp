/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.util;

import com.google.common.base.Optional;
import hms.kite.provisioning.commons.service.PaymentGatewayAdaptor;
import hms.kite.provisioning.commons.service.impl.PaymentGatewayAdaptorImpl;
import hms.kite.util.NullObject;

import java.util.Properties;

import static hms.kite.datarepo.RepositoryServiceRegistry.systemConfiguration;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ProvChargingConfiguration {

    private static PaymentGatewayAdaptor paymentGatewayAdaptor;
    private static Properties configurationProperties;

    static {
        setPaymentGatewayAdaptor(new NullObject<PaymentGatewayAdaptorImpl>().get());
        setConfigurationProperties(new NullObject<Properties>().get());
    }

    public static PaymentGatewayAdaptor paymentGatewayAdaptor() {
        return paymentGatewayAdaptor;
    }

    private static void setPaymentGatewayAdaptor(PaymentGatewayAdaptor paymentGatewayAdaptor) {
        ProvChargingConfiguration.paymentGatewayAdaptor = paymentGatewayAdaptor;
    }

    private static void setConfigurationProperties(Properties configurationProperties) {
        ProvChargingConfiguration.configurationProperties = configurationProperties;
    }

    public static String getMobileAccountName() {
        return (String) systemConfiguration().find("common-charging-mobile-account-name");
    }

    public static String getMobileAccountId() {
        return (String) systemConfiguration().find("common-charging-mobile-account-id");
    }

    public static String getChargingAmountRegExp() {
        return configurationProperties.getProperty("common.charging.charging.amount.regex");
    }

    public static boolean isSubscriberChargingPartyAllow() {
        return Boolean.parseBoolean(configurationProperties.getProperty("common.charging.subscriber.charging.party.allow"));
    }

    public static boolean isServiceProviderChargingPartyAllow() {
        return Boolean.parseBoolean(configurationProperties.getProperty("common.charging.service.provider.charging.party.allow"));
    }

    public static boolean isMpesaBuyGoodsPiAllow() {
        return Boolean.parseBoolean(configurationProperties.getProperty("common.charging.mpesa.buygoods.pi.allow"));
    }

    public static String getDefaultChargingParty() {
        return configurationProperties.getProperty("common.charging.default.charging.party");
    }

    public static boolean isOperatorChargingMethodAllow() {
        return Boolean.parseBoolean(configurationProperties.getProperty("common.charging.operator.charging.method.allow"));
    }

    public static boolean isPaymentInstrumentChargingMethodAllow() {
        return Boolean.parseBoolean(configurationProperties.getProperty("common.charging.payment.instrument.charging.method.allow"));
    }

    public static String getDefaultChargingMethod() {
        return configurationProperties.getProperty("common.charging.default.charging.method");
    }

    public static boolean isOtherPaymentInstrumentAllow() {
        return Boolean.parseBoolean(configurationProperties.getProperty("common.charging.other.payment.instruments.allow"));
    }

    public static boolean isMobileAccountPaymentInstrumentAllow() {
        return Boolean.parseBoolean(configurationProperties.getProperty("common.charging.mobile.account.payment.instrument.allow"));
    }

    public static String getDefaultAllowedPaymentInstrument() {
        return configurationProperties.getProperty("common.charging.default.allowed.payment.instrument");
    }

    public static boolean isFreeChargingTypeAllow() {
        return Boolean.parseBoolean(configurationProperties.getProperty("common.charging.free.charging.type.allow"));
    }

    public static boolean isFlatChargingTypeAllow() {
        return Boolean.parseBoolean(configurationProperties.getProperty("common.charging.flat.charging.type.allow"));
    }

    public static boolean isKeywordChargingTypeAllow() {
        return Boolean.parseBoolean(configurationProperties.getProperty("common.charging.keyword.charging.type.allow"));
    }

    public static boolean isVariableChargingTypeAllow() {
        return Boolean.parseBoolean(configurationProperties.getProperty("common.charging.variable.charging.type.allow"));
    }

    public static String getDefaultChargingType() {
        return configurationProperties.getProperty("common.charging.default.charging.type");
    }

    public static boolean isDailyChargingFrequencyAllow() {
        return Boolean.valueOf(configurationProperties.getProperty("common.charging.frequency.daily.allow"));
    }

    public static boolean isWeeklyChargingFrequencyAllow() {
        return Boolean.valueOf(configurationProperties.getProperty("common.charging.frequency.weekly.allow"));
    }

    public static boolean isMonthlyChargingFrequencyAllow() {
        return Boolean.valueOf(configurationProperties.getProperty("common.charging.frequency.monthly.allow"));
    }

    public static boolean isHalfMonthlyChargingFrequencyAllow() {
        return Boolean.valueOf(configurationProperties.getProperty("common.charging.frequency.half.monthly.allow"));
    }

    public static String getDefaultChargingFrequency() {
        return configurationProperties.getProperty("common.default.charging.frequency");
    }

    public static boolean isServiceCodeChargingAllow() {
        return Boolean.valueOf(configurationProperties.getProperty("common.charging.service.code.charging.allow"));
    }

    public static String getMpesaPayBillNoRegex() {
        return configurationProperties.getProperty("common.charging.mpesa.paybill.no.regex");
    }

    public static String getBuyGoodsTillNoRegex() {
        return configurationProperties.getProperty("common.charging.mpesa.buygoods.till.no.regex");
    }

    public static Optional<String> getPropertyByKey(String key) {
        return Optional.fromNullable(configurationProperties.getProperty(key));
    }
}
