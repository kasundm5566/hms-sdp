package hms.kite.provisioning.commons.ui.component.popup;

import com.vaadin.Application;
import com.vaadin.terminal.FileResource;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.Reindeer;

import java.io.File;


public class HelpDetails extends AbstractComponent {

    public static final String HELP_IMG_URL = "/VAADIN/img/";
    public static final String HELP_BUTTON_NAME = "Help";
    public static final String HELP_SUB_WINDOW_STYLE_NAME = "helpWindow";

    private String imageName;
    Application application;

    public HelpDetails(Application application, String imgName) {
        this.application = application;
        this.imageName = imgName;
    }

    public Embedded getHelpImage() {
        String basePath = application.getContext().getBaseDirectory().getAbsolutePath();
        return new Embedded("", new FileResource(new File(basePath + HELP_IMG_URL + imageName), application));
    }

    public Window generateHelpImageSubWindow(Component image, String width, String height) {
        //Window termsAndConditionWindow = new Window("");
        final Window helpViewSubWindow = new Window("");
        helpViewSubWindow.setModal(true);
        helpViewSubWindow.setWidth(width);
        helpViewSubWindow.setHeight(height);
        helpViewSubWindow.setStyleName(HELP_SUB_WINDOW_STYLE_NAME);
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.addComponent(image);
        helpViewSubWindow.addComponent(verticalLayout);
        return helpViewSubWindow;
    }

}
