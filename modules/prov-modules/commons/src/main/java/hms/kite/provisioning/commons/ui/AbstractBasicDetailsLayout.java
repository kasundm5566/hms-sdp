/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.ui;

import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Field;
import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.provisioning.commons.ui.VaadinUtil.setAllReadonly;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public abstract class AbstractBasicDetailsLayout extends VerticalLayout implements DataComponent {

    private static final String READ = "_READ_";
    private static final String WRITE = "_WRITE_";

    private final String moduleRolePrefix;

    private Map<String, Object> setApplicationData(boolean read, boolean write) {
        Map<String, Object> applicationData = new HashMap<String, Object>();

        applicationData.put(READ, read);
        applicationData.put(WRITE, write);

        return applicationData;
    }

    private boolean getComponentPermission(AbstractComponent component, String permission) {
        if (component == null) {
            return false;
        }

        Map applicationData = (Map) component.getData();
        if (applicationData == null) {
            return false;
        }

        Object value = applicationData.get(permission);
        if (value == null) {
            return false;
        }

        return (Boolean) value;
    }

    private void setReadOnlyValue(AbstractComponent component, boolean readOnly) {
        if (component instanceof Layout) {
            setAllReadonly(readOnly, (Layout) component);
        }
        component.setReadOnly(readOnly);
    }

    private boolean isWritable(AbstractComponent component) {
        return getComponentPermission(component, WRITE);
    }

    private boolean isReadable(AbstractComponent component) {
        return getComponentPermission(component, READ);
    }

    private void setAsWritable(AbstractComponent component) {
        setReadOnlyValue(component, false);
    }

    private void setAsReadable(AbstractComponent component) {
        setReadOnlyValue(component, true);
    }

    protected final BaseApplication application;

    protected AbstractBasicDetailsLayout(BaseApplication application, String moduleRolePrefix) {
        this.application = application;
        this.moduleRolePrefix = moduleRolePrefix;
    }

    /**
     * Check whether the field is not null
     *
     * @param field
     * @return true if the field is not null else return false
     */
    protected boolean isFieldNotNull(Field field) {
        return field != null;
    }

    /**
     * Reset the value of given field set with given value
     *
     * @param value  New value of the Property.
     * @param fields
     */
    protected void resetFields(Object value, Field... fields) {
        for (Field field : fields) {
            if (isFieldNotNull(field) && !field.isReadOnly()) {
                field.setValue(value);
            }
        }
    }

    /**
     * Set the field value with given value
     *
     * @param field
     * @param value
     */
    protected void setValueToField(Field field, Object value) {
        if (isFieldNotNull(field) && value != null && !value.equals("")) {
            if (!field.isReadOnly()) {
                field.setValue(value);
            } else {
                field.setReadOnly(false);
                field.setValue(value);
                field.setReadOnly(true);
            }
        }
    }

    /**
     * Get the value from field and put it into the data map with given key
     *
     * @param field
     * @param data
     * @param key
     */
    protected void getValueFromField(Field field, Map<String, Object> data, String key) {
        if (isFieldNotNull(field)) {
            data.put(key, field.getValue());
        }
    }

    /**
     * Put the given value into the data map if the field is not null
     *
     * @param field
     * @param data
     * @param key
     * @param value
     */
    protected void getValueFromField(Field field, Map<String, Object> data, String key, Object value) {
        if (isFieldNotNull(field)) {
            data.put(key, value);
        }
    }

    /**
     * Check whether user has given component related read and write permissions, then set the permission as
     * component data
     *
     * @param componentRolePostFix
     * @param component
     * @param <T>
     * @return
     */
    protected <T extends AbstractComponent> T getFieldWithPermission(String componentRolePostFix, T component) {
        Map<String, Object> applicationData;

        if (component == null) {
            return null;
        }

        if (application.hasAnyRole(moduleRolePrefix + READ + componentRolePostFix)
                && application.hasAnyRole(moduleRolePrefix + WRITE + componentRolePostFix)) {
            applicationData = setApplicationData(true, true);
            component.setData(applicationData);

            return component;
        } else if (application.hasAnyRole(moduleRolePrefix + READ + componentRolePostFix)) {
            applicationData = setApplicationData(true, false);
            component.setData(applicationData);

            return component;
        }

        return null;
    }

    /**
     * Set the read write permission to the component
     *
     * @param components
     */
    protected void setReadWritePermissionToComponent(AbstractComponent... components) {
        for (AbstractComponent component : components) {
            if (component != null) {
                Boolean read = isReadable(component);
                Boolean write = isWritable(component);

                if (read && !write) {
                    setAsReadable(component);
                } else if (read) {
                    setAsWritable(component);
                }
            }
        }
    }

    /**
     * Execute validate function for given field
     *
     * @param field
     */
    protected void validateField(Field field) {
        if (isFieldNotNull(field)) {
            field.validate();
        }
    }

    public abstract void reset();

    public abstract void setPermissions();
}
