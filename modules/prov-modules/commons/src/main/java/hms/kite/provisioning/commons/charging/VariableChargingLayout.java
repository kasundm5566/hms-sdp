/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.charging;

import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.DoubleValidator;
import com.vaadin.data.validator.NullValidator;
import com.vaadin.ui.*;
import hms.kite.provisioning.commons.ui.DataComponent;
import hms.kite.provisioning.commons.ui.I18nProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.provisioning.commons.util.ProvChargingConfiguration.*;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
@Deprecated
public class VariableChargingLayout extends VerticalLayout implements DataComponent {

    private static final Logger logger = LoggerFactory.getLogger(VariableChargingLayout.class);

    private I18nProvider i18n;
    private OptionGroup chargeParty;
    private OptionGroup method;
    private TextField maxCharge;
    private TextField minCharge;
    private TextField maxChargeSub;
    private TextField minChargeSub;
    private FormLayout chargingMethodLayout;
    private FlatChargingLayout.SpChargingLayout spChargingLayout;

    private TextField[] fields;
    private TextField variableCharing;

    public VariableChargingLayout(Map<String, Map<String, Object>> instruments, I18nProvider i18n, String corpUserId) {
        this.i18n = i18n;
        addComponent(loadComponents(instruments, corpUserId));
    }

    private VerticalLayout loadComponents(Map<String, Map<String, Object>> instruments, String corpUserId) {
        VerticalLayout layout = new VerticalLayout();
        maxCharge = new TextField(i18n.getMsg("create.mt.maximum.charging.amount"));
        maxCharge.setWidth("280px");
        maxCharge.addValidator(new NullValidator(i18n.getMsg("create.mt.maximum.charging.amount.null.error"), false));
        maxCharge.addValidator(new DoubleValidator(i18n.getMsg("create.mt.maximum.charging.amount.numeric.error")));


        minCharge = new TextField(i18n.getMsg("create.mt.minimum.charging.amount"));
        minCharge.setWidth("280px");
        minCharge.addValidator(new NullValidator(i18n.getMsg("create.mt.minimum.charging.amount.null.error"), false));
        minCharge.addValidator(new DoubleValidator(i18n.getMsg("create.mt.minimum.charging.amount.numeric.error")));

        maxCharge.addValidator(new MinMaxChargingValidator(
                i18n.getMsg("create.mt.min.max.charging.validation.error.message"), minCharge));

        maxChargeSub = new TextField(i18n.getMsg("create.mt.maximum.charging.amount"));
        maxChargeSub.setWidth("280px");
        maxChargeSub.addValidator(new NullValidator(i18n.getMsg("create.mt.maximum.charging.amount.null.error"), false));
        maxChargeSub.addValidator(new DoubleValidator(i18n.getMsg("create.mt.maximum.charging.amount.numeric.error")));
        maxChargeSub.addValidator(new NegativeValidator(i18n.getMsg("create.mt.maximum.charging.amount.numeric.negative.error")));

        minChargeSub = new TextField(i18n.getMsg("create.mt.minimum.charging.amount"));
        minChargeSub.setWidth("280px");
        minChargeSub.addValidator(new NullValidator(i18n.getMsg("create.mt.minimum.charging.amount.null.error"), false));
        minChargeSub.addValidator(new DoubleValidator(i18n.getMsg("create.mt.minimum.charging.amount.numeric.error")));
        minChargeSub.addValidator(new NegativeValidator(i18n.getMsg("create.mt.minimum.charging.amount.numeric.negative.error")));

        variableCharing = new TextField(i18n.getMsg("create.mt.variable.charging.amount"));
        variableCharing.setWidth("280px");
        variableCharing.setEnabled(false);
        variableCharing.setValue(i18n.getMsg("create.mt.variable.charging.amount.values"));

        maxChargeSub.addValidator(new MinMaxChargingValidator(
                i18n.getMsg("create.mt.min.max.charging.validation.error.message"), minChargeSub));

        fields = new TextField[]{maxCharge, minCharge, maxChargeSub, minChargeSub};
        for (TextField field : fields) {
            field.setImmediate(true);
            field.setRequired(true);
        }
        layout.addComponent(getChargeTypeLayout());
        layout.addComponent(getPaymentMethodsLayout());
        layout.addComponent(getChargingInstrumentLayout(instruments, corpUserId));

        chargeParty.select(getDefaultChargingParty());
        method.select(getDefaultChargingMethod());
        return layout;
    }

    private void negativeValidate(Field... fields) {
        for (Field field : fields) {
            int amount = Integer.parseInt(field.getValue().toString());
            if (amount > 0) {
                logger.error(i18n.getMsg("create.mt.minimum.charging.amount.numeric.negative.error"));
                throw new Validator.InvalidValueException(i18n.getMsg("create.mt.minimum.charging.amount.numeric.negative.error"));
            }
        }
    }

    private FormLayout getChargeTypeLayout() {
        FormLayout formLayout = new FormLayout();
        formLayout.addStyleName("charging-form");
        chargeParty = new OptionGroup(i18n.getMsg("create.charge.party"));
        if (isSubscriberChargingPartyAllow()) {
            chargeParty.addItem(subscriberK);
            chargeParty.setItemCaption(subscriberK, i18n.getMsg("create.charge.party.subscriber"));
        }
        if (isServiceProviderChargingPartyAllow()) {
            chargeParty.addItem(spK);
            chargeParty.setItemCaption(spK, i18n.getMsg("create.charge.party.sp"));
        }
        chargeParty.setImmediate(true);
        chargeParty.setRequired(true);
        chargeParty.addListener(new Property.ValueChangeListener() {

            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                if (subscriberK.equals(chargeParty.getValue())) {
                    chargingMethodLayout.setVisible(true);
                    spChargingLayout.setVisible(false);
                } else {
                    chargingMethodLayout.setVisible(false);
                    spChargingLayout.setVisible(true);
                }
            }
        });
        formLayout.addComponent(chargeParty);
        return formLayout;
    }

    private FormLayout getPaymentMethodsLayout() {
        chargingMethodLayout = new FormLayout();
        chargingMethodLayout.addStyleName("charging-form");
        method = new OptionGroup(i18n.getMsg("create.mo.flat.charging.method"));
        if (isPaymentInstrumentChargingMethodAllow()) {
            method.addItem(defaultPaymentInstrumentK);
            method.setItemCaption(defaultPaymentInstrumentK,
                    i18n.getMsg("create.mo.flat.charging.method.default.payment.instrument"));
        }
        if (isOperatorChargingMethodAllow()) {
            method.addItem(operatorChargingK);
            method.setItemCaption(operatorChargingK, i18n.getMsg("create.mo.flat.charging.method.operator"));
        }
        method.setImmediate(true);
        method.setRequired(true);
        chargingMethodLayout.addComponent(method);

        method.addListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                if (defaultPaymentInstrumentK.equals(method.getValue())) {
                    chargingMethodLayout.addComponent(minChargeSub);
                    chargingMethodLayout.addComponent(maxChargeSub);
                    chargingMethodLayout.removeComponent(variableCharing);
                } else if (operatorChargingK.equals(method.getValue())) {
                    chargingMethodLayout.removeComponent(minChargeSub);
                    chargingMethodLayout.removeComponent(maxChargeSub);
                    chargingMethodLayout.addComponent(variableCharing);
                }
            }
        });

        return chargingMethodLayout;
    }

    private FormLayout getChargingInstrumentLayout(Map<String, Map<String, Object>> instruments, String corpUserId) {
        FlatChargingLayout.SpChargingLayout spChargingLayout = new FlatChargingLayout.SpChargingLayout(instruments, i18n, corpUserId);
        spChargingLayout.removeComponent(spChargingLayout.getAmountTxtFld());
        spChargingLayout.addComponent(maxCharge);
        spChargingLayout.addComponent(minCharge);
        this.spChargingLayout = spChargingLayout;
        return spChargingLayout;
    }

    @Override
    public void setData(Map<String, Object> data) {
        Map<String, Object> chargingMap = (Map<String, Object>) data.get(chargingK);
        String value = (String) chargingMap.get(partyK);
        chargeParty.select(value);
        if (subscriberK.equals(value)) {
            method.select(chargingMap.get(methodK));
            if (chargingMap.get(methodK).equals(defaultPaymentInstrumentK)) {
                minChargeSub.setValue(chargingMap.get(minAmountK));
                maxChargeSub.setValue(chargingMap.get(maxAmountK));
            } else if (chargingMap.get(methodK).equals(operatorChargingK)) {
                variableCharing.setValue(i18n.getMsg("create.mt.variable.charging.amount.values"));
            }
        } else if (spK.equals(value)) {
            spChargingLayout.setData(chargingMap);
            minCharge.setValue(chargingMap.get(minAmountK));
            maxCharge.setValue(chargingMap.get(maxAmountK));

        }
    }

    @Override
    public void getData(Map<String, Object> data) {
        Map<String, Object> chargingMap = new HashMap<String, Object>();
        chargingMap.put(typeK, variableK);
        String value = chargeParty.getValue().toString();
        chargingMap.put(partyK, value);
        if (subscriberK.equals(value)) {
            chargingMap.put(methodK, method.getValue().toString());
            if (method.getValue().toString().equals(defaultPaymentInstrumentK)) {
                chargingMap.put(minAmountK, minChargeSub.getValue().toString());
                chargingMap.put(maxAmountK, maxChargeSub.getValue().toString());
            }
        } else if (spK.equals(value)) {
            spChargingLayout.getData(chargingMap);
            chargingMap.remove(amountK);
            chargingMap.put(minAmountK, minCharge.getValue().toString());
            chargingMap.put(maxAmountK, maxCharge.getValue().toString());
        }

        data.put(chargingK, chargingMap);
    }

    @Override
    public void validate() {
        String value = (String) chargeParty.getValue();
        if (subscriberK.equals(value)) {
            method.validate();
            if (defaultPaymentInstrumentK.equals(method.getValue())) {
                TextField[] fields = new TextField[]{maxChargeSub, minChargeSub};
                for (TextField field : fields) {
                    try {
                        field.validate();
                    } catch (Validator.InvalidValueException ex) {
                        logger.error(ex.getMessage());
                        throw new IllegalStateException(ex.getMessage());
                    }
                }
            }
        } else if (spK.equals(value)) {
            TextField[] fields = new TextField[]{maxCharge, minCharge};
            for (TextField field : fields) {
                try {
                    field.validate();
                } catch (Validator.InvalidValueException ex) {
                    logger.error(ex.getMessage());
                    throw new IllegalStateException(ex.getMessage());
                }
            }
            spChargingLayout.validate();
            spChargingLayout.validate(variableK);
        }
    }

    class NegativeValidator implements Validator {

        private String message;

        NegativeValidator(String errorMessage) {
            this.message = errorMessage;
        }

        @Override
        public void validate(Object value) throws InvalidValueException {
            if (!isValid(value)) {
                logger.debug("Value not allowed " + value);
                throw new InvalidValueException(message);
            }
        }

        @Override
        public boolean isValid(Object value) {
            if (value.equals("") || value == null) {
                return false;
            }
            try {
                double amount = Double.parseDouble(value.toString());
                if (amount > 0) {
                    return true;
                }
            } catch (NumberFormatException e) {
                logger.error("Number format exception occurred while validating for value " + value);
            } catch (Exception e) {
                logger.error("Unexpected error occurred while validating", e);
            }
            return false;
        }

    }

    class MinMaxChargingValidator implements Validator {

        private String customErrorKey;
        private TextField minTxtFld;

        MinMaxChargingValidator(String customErrorKey, TextField minTxtFld) {
            this.customErrorKey = customErrorKey;
            this.minTxtFld = minTxtFld;
        }

        @Override
        public void validate(Object o) throws InvalidValueException {
            if (!isValid(o)) {
                logger.debug("Max value is greater than the Min value ", o);
                throw new InvalidValueException(customErrorKey);
            }
        }

        @Override
        public boolean isValid(Object o) {

            try {
                boolean isValid = true;
                double max = Double.parseDouble(o.toString());
                double min = Double.parseDouble(minTxtFld.getValue().toString());
                if (max > 0 && min > max) {
                    isValid = false;
                }
                return isValid;
            } catch (NumberFormatException e) {
                logger.error("Unexpected error occurred while validating", e);
            } catch (Exception e) {
                logger.error("Unexpected error occurred while validating", e);
            }
            return false;
        }
    }
}
