/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.ui.validator;

import com.vaadin.data.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class SpLimitValidator implements Validator {

    private static Logger logger = LoggerFactory.getLogger(SpLimitValidator.class);

    private Integer limit;
    private String errorMessage;

    public SpLimitValidator(Integer limit, String errorMessage) {
        this.limit = limit;
        this.errorMessage = errorMessage;
    }

    @Override
    public void validate(Object value) throws InvalidValueException {
        if (!isValid(value)) {
            throw new InvalidValueException(errorMessage);
        }
    }

    @Override
    public boolean isValid(Object value) {
        try {
            int userValue = Integer.parseInt((String) value);
            if (userValue <= limit) {
                return true;
            }
        } catch (Exception e) {
            logger.error("Unexpected error occurred while validating", e);
        }
        return false;
    }
}
