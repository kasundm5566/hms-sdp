/*
 *   (C) Copyright 2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.provisioning.commons.ui.validator;

import com.vaadin.data.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static hms.kite.provisioning.commons.util.ProvChargingConfiguration.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class TillNumberListValidator implements Validator {

    private String seperator;
    private String errorMessage;

    private static final Logger logger = LoggerFactory.getLogger(TillNumberListValidator.class);

    public TillNumberListValidator(String seperator, String errorMessage) {
        this.seperator = seperator;
        this.errorMessage = errorMessage;
    }

    @Override
    public void validate(Object value) throws InvalidValueException {
        if (!isValid(value)) {
            logger.info("invalid till numbers {}", new Object[]{ value});
            throw new InvalidValueException(errorMessage);
        }
    }

    @Override
    public boolean isValid(Object value) {
        boolean isValidNumber=true;
        String[] tillNumbers = String.valueOf(value).split(seperator);
        for(int i=0; i<tillNumbers.length; i++) {
            if(tillNumbers[i].trim().matches(getBuyGoodsTillNoRegex())) {
                isValidNumber = true;
            } else return false;
        }
        return isValidNumber;
    }


}
