/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.commons.event;

import java.util.Map;

/**
 * $LastChangedDate $
 * $LastChangedBy $
 * $LastChangedRevision $
 */
public interface EventRouter {

    void start();

    void shutdown();

    /**
     * TODO: Documentation
     * @param data
     */
    void fire (Map<String, Object> data);
}
