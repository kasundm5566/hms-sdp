/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.ui.component.table;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.themes.BaseTheme;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class Paging extends HorizontalLayout {

    private static final Logger logger = LoggerFactory.getLogger(Paging.class);

    private final PagingListener pagingListener;
    private final HorizontalLayout pageIndexLayout;

    private int recordsPerPage;
    private int totalNumberOfPages;
    private int displayLastIndex;
    private int displayStartIndex = 1;
    private int displaySelectedIndex = 1;
    private int maxPagesDisplayed = 5;

    private String nextButtonText = "Next >>";
    private String prevButtonText = "<< Prev";

    private void initializeData() {
        if (totalNumberOfPages < maxPagesDisplayed) {
            maxPagesDisplayed = totalNumberOfPages;
        }
        displayLastIndex = maxPagesDisplayed;
    }

    private void reloadIndexBar() {
        logger.trace("Reloading page index bar");

        pagingListener.onReload((displaySelectedIndex - 1) * recordsPerPage, recordsPerPage);

        pageIndexLayout.removeAllComponents();
        if (totalNumberOfPages <= 1) {
            logger.trace("No more than one pages exists. Index bar is not showing");
            return;
        }
        Button previousBtn = new Button(prevButtonText);
        previousBtn.setStyleName(BaseTheme.BUTTON_LINK);
        previousBtn.addStyleName("page-link");

        previousBtn.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                if (displayStartIndex <= 1) {
                    if (displaySelectedIndex > displayStartIndex) {
                        displaySelectedIndex--;
                        reloadIndexBar();
                    }
                    return;
                }
                displayStartIndex--;
                displayLastIndex--;
                displaySelectedIndex--;
                reloadIndexBar();
            }
        });
        pageIndexLayout.addComponent(previousBtn);

        reloadPageIndexes();

        Button nextBtn = new Button(nextButtonText);
        nextBtn.setStyleName(BaseTheme.BUTTON_LINK);
        nextBtn.addStyleName("page-link");
        nextBtn.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                if (displayLastIndex >= totalNumberOfPages) {
                    if (displaySelectedIndex < displayLastIndex) {
                        displaySelectedIndex++;
                        reloadIndexBar();
                    }
                    return;
                }
                displayStartIndex++;
                displayLastIndex++;
                displaySelectedIndex++;
                reloadIndexBar();
            }
        });
        pageIndexLayout.addComponent(nextBtn);
    }

    private void reloadPageIndexes() {
        for (int i = displayStartIndex; i <= displayLastIndex; i++) {
            Button indexBtn = new Button("" + i);
            indexBtn.setData(new Integer(i));
            indexBtn.setStyleName(BaseTheme.BUTTON_LINK);
            if (i == displaySelectedIndex) {
                indexBtn.addStyleName("page-selected-link");
            } else {
                indexBtn.addStyleName("page-link");
            }
            indexBtn.addListener(new Button.ClickListener() {

                @Override
                public void buttonClick(Button.ClickEvent clickEvent) {
                    Integer pageIndex = (Integer) clickEvent.getButton().getData() - 1;
                    displaySelectedIndex = pageIndex + 1;
                    reloadIndexBar();
                }
            });
            pageIndexLayout.addComponent(indexBtn);
        }
    }

    public Paging(PagingListener pagingListener, int totalRecordCount, int recordsPerPage) {
        this.pagingListener = pagingListener;
        this.recordsPerPage = recordsPerPage;

        totalNumberOfPages = (int) Math.ceil((double) totalRecordCount / recordsPerPage);

        pageIndexLayout = new HorizontalLayout();
        addComponent(pageIndexLayout);
    }

    public void init() {
        initializeData();
        reloadIndexBar();
    }
}