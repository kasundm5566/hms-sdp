/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.commons.ui.validator;

import com.vaadin.data.Validator;
import com.vaadin.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static hms.kite.datarepo.RepositoryServiceRegistry.ussdRoutingKeyRepositoryService;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class KeywordValidator implements Validator {

    private String spId;
    private String operatorName;
    private Select shortCodeSelect;
    private String errorMessage;
    private String appId;

    private static final Logger logger = LoggerFactory.getLogger(KeywordValidator.class);

    public KeywordValidator(String spId, String appId, String operatorName, Select shortCodeSelect, String errorMessage) {
        this.spId = spId;
        this.operatorName = operatorName;
        this.shortCodeSelect = shortCodeSelect;
        this.errorMessage = errorMessage;
        this.appId = appId;
    }

    @Override
    public void validate(Object value) throws InvalidValueException {
        String keyword = (String) value;
        String shortCode = (String) shortCodeSelect.getValue();

        if (!isValid(value)) {
            logger.info("keyword {} for shortcode {} of operator {} already taken.",
                    new Object[]{ keyword, shortCode, operatorName});

            throw new InvalidValueException(errorMessage);
        }
    }

    @Override
    public boolean isValid(Object value) {

        Boolean available = false;

        String keyword = (String) value;
        String shortCode = (String) shortCodeSelect.getValue();

        boolean keywordEntered = keyword != null && keyword.trim().length() != 0;
        boolean shortcodeSelected = shortCode != null && shortCode.trim().length() != 0;

        if (keywordEntered && shortcodeSelected) {

            boolean availableForSp = ussdRoutingKeyRepositoryService()
                    .isRoutingKeyAvailableForSp(operatorName, shortCode, keyword);

            boolean availableForApp = ussdRoutingKeyRepositoryService()
                    .isRoutingKeyAvailableForApp(spId, appId, operatorName, shortCode, keyword);

            available =  availableForSp && availableForApp;

            logger.debug("RoutingKey available for sp {}, RoutingKey available for app {} ",
                    availableForSp, availableForApp);
        }
        return available;
    }
}
