/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.provisioning.commons.service;

import hms.kite.util.SdpException;

import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public interface PaymentGatewayAdaptor {

    /**
     * Find the payment instruments for the corporate-user-id.
     *
     * @param corporateUserId -
     * @return ex : - {
     *           payment-instrument-id1 : {instrument-id : payment-instrument-id1,
     *                                     name : my-bank,
     *                                     accounts : {
     *                                                      account-id1 : { account-id : account-id1, currency : USD},
     *                                                      account-id2 : { account-id : account-id2, currency : USD},
     *                                                }
     *                                    }
     *          payment-instrument-id2 : {instrument-id : payment-instrument-id2,
     *                                     name : your-bank,
     *                                     accounts : { account-id1 : { account-id : account-id1, currency}}
     *                                    }
     * }
     * @throws hms.kite.util.SdpException - for any Exception occurred withing query of gateway.
     */
    Map<String, Map<String, Object>> paymentInstruments(String corporateUserId) throws SdpException;
}
