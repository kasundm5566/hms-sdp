/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.ui.component.table;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 * <p/>
 * Paging component listener class. Use this to listen page change events.
 */
public interface PagingListener {

    /**
     * This Method will be executed when page is changed by clicking a page index
     * or next, previous button
     *
     * @param start     start of the batch need to be loaded
     * @param batchSize Size of batch need to be loaded
     */
    public void onReload(int start, int batchSize);
}