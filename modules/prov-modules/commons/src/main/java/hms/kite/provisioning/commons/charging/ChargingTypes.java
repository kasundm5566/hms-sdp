/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.charging;

import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.terminal.UserError;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Select;
import com.vaadin.ui.VerticalLayout;
import hms.kite.provisioning.commons.ui.DataComponent;
import hms.kite.provisioning.commons.ui.I18nProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.provisioning.commons.ui.VaadinUtil.setAllReadonly;
import static hms.kite.provisioning.commons.util.ProvChargingConfiguration.*;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */

@Deprecated
/**
 * Use ChargingLayout
 */
public class ChargingTypes extends VerticalLayout implements DataComponent {

    private static final Logger logger = LoggerFactory.getLogger(ChargingTypes.class);

    private Select chargingTypeSelect;
    private Map<String, Object> map;

    private final I18nProvider i18n;
    private KeywordChargingLayout keywordChargingLayout;
    private FlatChargingLayout flatChargingLayout;
    private VariableChargingLayout variableChargingLayout;

    private ChargingTypes(Builder builder) {
        chargingTypeSelect = builder.chargingTypeSelect;
        chargingTypeSelect.setWidth("280px");
        i18n = builder.i18n;
        FormLayout formLayout = builder.formLayout;
        formLayout.addStyleName("charging-form");
        map = builder.map;

        flatChargingLayout = new FlatChargingLayout(builder.operatorId, builder.instruments, i18n, builder.corpUserId);
        keywordChargingLayout = new KeywordChargingLayout(builder.instruments, i18n, builder.corpUserId);
        variableChargingLayout = new VariableChargingLayout(builder.instruments, i18n, builder.corpUserId);

        formLayout.addComponent(chargingTypeSelect);
        addComponent(flatChargingLayout);
        addComponent(keywordChargingLayout);
        addComponent(variableChargingLayout);

        populateChargingTypes();
    }

    public void alterChargingTypes(Builder builder) {
        map = builder.map;
        this.chargingTypeSelect.removeAllItems();
        populate();
    }

    private void populateChargingTypes() {
        populate();
        chargingTypeSelect.setImmediate(true);
        chargingTypeSelect.setNullSelectionAllowed(false);
        chargingTypeSelect.setRequired(true);
        chargingTypeSelect.setRequiredError(i18n.getMsg("create.charging.type.required.error"));
        chargingTypeSelect.addListener(new Property.ValueChangeListener() {

            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                String value = (String) chargingTypeSelect.getValue();
                if (flatK.equals(value)) {
                    changeVisibility(true, false, false);
                } else if (keywordK.equals(value)) {
                    changeVisibility(false, true, false);
                } else if (freeK.equals(value)) {
                    changeVisibility(false, false, false);
                } else if (variableK.equals(value)) {
                    changeVisibility(false, false, true);
                }
            }
        });
        chargingTypeSelect.select(getDefaultChargingType());
    }

    public Map<String, Object> getItems() {
        return map;
    }

    public void resetItems(Map<String, Object> newItems) {
        for (String s : newItems.keySet()) {
            chargingTypeSelect.removeItem(s);
        }
        map = newItems;
        populate();
    }

    private void populate() {
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            chargingTypeSelect.addItem(entry.getKey());
            chargingTypeSelect.setItemCaption(entry.getKey(), (String) entry.getValue());
        }
    }

    private void changeVisibility(boolean isFlat, boolean isKeyword, boolean isVariable) {
        flatChargingLayout.setVisible(isFlat);
        keywordChargingLayout.setVisible(isKeyword);
        variableChargingLayout.setVisible(isVariable);
    }

    @Override
    public void setData(Map<String, Object> data) {
        Map<String, Object> chargingMap = (HashMap<String, Object>) data.get(chargingK);
        String value = (String) chargingMap.get(typeK);
        if (flatK.equalsIgnoreCase(value)) {
            chargingTypeSelect.select(flatK);
            flatChargingLayout.setData(data);
        } else if (keywordK.equalsIgnoreCase(value)) {
            chargingTypeSelect.select(keywordK);
            keywordChargingLayout.setData(data);
        } else if (variableK.equalsIgnoreCase(value)) {
            chargingTypeSelect.select(variableK);
            variableChargingLayout.setData(data);
        } else {
            chargingTypeSelect.select(freeK);
        }
    }

    @Override
    public void getData(Map<String, Object> data) {
        String value = (String) chargingTypeSelect.getValue();
        setComponentError(null);
        if (flatK.equals(value)) {
            flatChargingLayout.getData(data);
        } else if (keywordK.equals(value)) {
            keywordChargingLayout.getData(data);
        } else if (variableK.equals(value)) {
            variableChargingLayout.getData(data);
        } else if (freeK.equals(value)) {
            Map<String, Object> chargingMap = new HashMap<String, Object>();
            chargingMap.put(typeK, freeK);
            data.put(chargingK, chargingMap);
        } else {
            String errorMessage = i18n.getMsg("create.charging.type.required.error");
            chargingTypeSelect.setComponentError(new UserError(errorMessage));
            throw new Validator.InvalidValueException(errorMessage);
        }
    }

    @Override
    public void validate() {
        String value = (String) chargingTypeSelect.getValue();
        logger.debug("Selected Charging Type: [" + value + "]");
        setComponentError(null);
        if (flatK.equals(value)) {
            flatChargingLayout.validate();
        } else if (keywordK.equals(value)) {
            keywordChargingLayout.validate();
        } else if (variableK.equals(value)) {
            variableChargingLayout.validate();
        } else if (!freeK.equals(value)) {
            String errorMessage = i18n.getMsg("create.charging.type.required.error");
            chargingTypeSelect.setComponentError(new UserError(errorMessage));
            throw new Validator.InvalidValueException(errorMessage);
        }

    }

    @Override
    public void setReadOnly(boolean readOnly) {
        chargingTypeSelect.setReadOnly(readOnly);

        setAllReadonly(readOnly, flatChargingLayout);
        setAllReadonly(readOnly, keywordChargingLayout);
        setAllReadonly(readOnly, variableChargingLayout);
    }

    public static class Builder {

        private Select chargingTypeSelect;
        private Map<String, Object> map;
        private I18nProvider i18n;
        private FormLayout formLayout;
        private Map<String, Map<String, Object>> instruments;
        private String operatorId;
        private String corpUserId;


        /**
         * Use this constructor only to alter the existing ChargingType object
         *
         * @param i18n
         */
        public Builder(I18nProvider i18n) {
            map = new HashMap<String, Object>();
            this.i18n = i18n;
        }

        public Builder(String operatorId,
                       String caption,
                       Map<String, Map<String, Object>> instruments,
                       I18nProvider i18n,
                       FormLayout formLayout,
                       String corpUserId) {
            this.operatorId = operatorId;
            this.i18n = i18n;
            this.formLayout = formLayout;
            this.instruments = instruments;
            map = new HashMap<String, Object>();
            chargingTypeSelect = new Select(caption);
            this.corpUserId = corpUserId;
        }

        public Builder free() {
            if (isFreeChargingTypeAllow()) {
                map.put(freeK, i18n.getMsg("create.charging.type.free"));
            }
            return this;
        }

        public Builder flat() {
            if (isFlatChargingTypeAllow()) {
                map.put(flatK, i18n.getMsg("create.charging.type.flat"));
            }
            return this;
        }

        public Builder keyword() {
            if (isKeywordChargingTypeAllow()) {
                map.put(keywordK, i18n.getMsg("create.charging.type.keyword.select"));
            }
            return this;
        }

        public Builder variable() {
            if (isVariableChargingTypeAllow()) {
                map.put(variableK, i18n.getMsg("create.charging.type.variable"));
            }
            return this;
        }

        public Map<String, Object> getBuildItems() {
            return map;
        }

        public ChargingTypes build() {
            return new ChargingTypes(this);
        }
    }
}
