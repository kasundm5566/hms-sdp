/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.charging;

import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.DoubleValidator;
import com.vaadin.ui.*;
import hms.kite.provisioning.commons.ui.DataComponent;
import hms.kite.provisioning.commons.ui.I18nProvider;
import hms.kite.provisioning.commons.ui.VaadinUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.provisioning.commons.util.ProvChargingConfiguration.*;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
@Deprecated
public class KeywordChargingLayout extends VerticalLayout implements DataComponent {

    private static final Logger logger = LoggerFactory.getLogger(KeywordChargingLayout.class);

    private final OptionGroup chargePartyOptionGroup;
    private final Button button = new Button("+");

    private Table table;
    private I18nProvider i18n;
    private FlatChargingLayout.SpChargingLayout spChargingLayout;

    private OptionGroup method;
    private FormLayout chargingMethodLayout;

    private int rowNumber = 0;

    public KeywordChargingLayout(Map<String, Map<String, Object>> instruments, I18nProvider i18n, String corpuserId) {

        this.i18n = i18n;
        final FormLayout bodyLayout = new FormLayout();
        bodyLayout.addStyleName("charging-form");

        chargePartyOptionGroup = new OptionGroup(i18n.getMsg("create.charge.party"));
        if (isSubscriberChargingPartyAllow()) {
            chargePartyOptionGroup.addItem(subscriberK);
            chargePartyOptionGroup.setItemCaption(subscriberK, i18n.getMsg("create.charge.party.subscriber"));
        }
        if (isServiceProviderChargingPartyAllow()) {
            chargePartyOptionGroup.addItem(spK);
            chargePartyOptionGroup.setItemCaption(spK, i18n.getMsg("create.charge.party.sp"));
        }
        chargePartyOptionGroup.setImmediate(true);
        chargePartyOptionGroup.setRequired(true);
        bodyLayout.addComponent(chargePartyOptionGroup);

        table = getTable(i18n);
        FormLayout formLayout = new FormLayout();
        formLayout.addStyleName("charging-form");
        formLayout.addComponent(table);
        addComponent(bodyLayout);
        addComponent(getChargingInstrumentLayout(instruments, corpuserId));
        addComponent(getPaymentMethodsLayout());
        addComponent(formLayout);
        chargePartyOptionGroup.addListener(new Property.ValueChangeListener() {

            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                if (subscriberK.equals(chargePartyOptionGroup.getValue())) {
                    chargingMethodLayout.setVisible(true);
                    spChargingLayout.setVisible(false);
                } else {
                    chargingMethodLayout.setVisible(false);
                    spChargingLayout.setVisible(true);
                }
            }
        });
        chargePartyOptionGroup.select(getDefaultChargingParty());
    }

    private FormLayout getPaymentMethodsLayout() {
        FormLayout formLayout = new FormLayout();
        formLayout.addStyleName("charging-form");
        method = new OptionGroup(i18n.getMsg("create.mo.flat.charging.method"));
        if (isPaymentInstrumentChargingMethodAllow()) {
            method.addItem(defaultPaymentInstrumentK);
            method.setItemCaption(defaultPaymentInstrumentK,
                    i18n.getMsg("create.mo.flat.charging.method.default.payment.instrument"));
        }
        method.setImmediate(true);
        method.setRequired(true);
        formLayout.addComponent(method);
        chargingMethodLayout = formLayout;
        return formLayout;
    }

    private FormLayout getChargingInstrumentLayout(Map<String, Map<String, Object>> instruments, String corporateUserId) {
        FlatChargingLayout.SpChargingLayout spChargingLayout
                = new FlatChargingLayout.SpChargingLayout(instruments, i18n, corporateUserId);
        spChargingLayout.removeComponent(spChargingLayout.getAmountTxtFld());
        this.spChargingLayout = spChargingLayout;
        spChargingLayout.addStyleName("charging-form");
        return spChargingLayout;
    }

    public Table getTable(I18nProvider i18n) {

        final Table table = new Table("Amount");
        table.setWidth("280px");
        table.addContainerProperty(i18n.getMsg("create.charging.type.keyword"), TextField.class, null);
        table.addContainerProperty(i18n.getMsg("create.mo.flat.charging.amount"), TextField.class, null);
        table.addContainerProperty("Remove", Button.class, null);
        table.addContainerProperty("Add", Button.class, null);

        table.setColumnWidth(i18n.getMsg("create.charging.type.keyword"), 60);
        table.setColumnWidth(i18n.getMsg("create.mo.flat.charging.amount"), 60);
        table.setColumnWidth("Remove", 55);
//        table.setColumnWidth("Add", 40);

        table.setImmediate(true);

        table.setColumnReorderingAllowed(false);
        table.setColumnCollapsingAllowed(false);
        table.setStyleName("striped");
        table.setPageLength(5);


        button.setImmediate(true);
        button.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                int nextRowNumber = getNextRowNumber();
                table.addItem(createField(button, "", "", nextRowNumber, table), nextRowNumber);
            }
        });

        int nextRowNumber = getNextRowNumber();
        table.addItem(createField(button, "", "", nextRowNumber, table), null);
        return table;
    }

    private Object[] createField(Button button, String tf1Value, String tf2Value, final int nextRowNumber, final Table table) {
        Object[] objects = new Object[4];
        TextField tf1 = new TextField();
        tf1.setWidth("59px");
        TextField tf2 = new TextField();
        tf2.setWidth("59px");
        tf1.setValue(tf1Value);
        tf2.setValue(tf2Value);
        tf1.setRequired(true);
        tf1.setRequiredError((i18n.getMsg("create.keyword.null.error")));
        tf2.setRequired(true);
        tf2.setRequiredError((i18n.getMsg("create.mo.flat.charging.amount.null.error")));
        tf2.addValidator(new DoubleValidator(i18n.getMsg("create.keyword.base.charge.number.error")));
        tf2.addValidator(new NegativeValidator(i18n.getMsg("create.keyword.base.charge.number.negative.error")));


        Button removeButton = new Button("-");
        removeButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    table.removeItem(nextRowNumber);
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while removing table item", e);
                }
            }
        });

        objects[0] = tf1;
        objects[1] = tf2;
        objects[2] = removeButton;
        objects[3] = button;

        return objects;
    }

    @Override
    public void setData(Map<String, Object> data) {
        Map<String, Object> chargingMap = (Map<String, Object>) data.get(chargingK);
        setTableValues(chargingMap);
        String value = (String) chargingMap.get(partyK);
        chargePartyOptionGroup.select(value);
        if (subscriberK.equals(value)) {
            method.select(chargingMap.get(methodK));
        } else if (spK.equals(value)) {
            spChargingLayout.setData(chargingMap);
        }
    }

    @Override
    public void getData(Map<String, Object> data) {
        Map<String, Object> chargingMap = new HashMap<String, Object>();
        chargingMap.put(typeK, keywordK);
        chargingMap.put(keywordChargingAmountK, getTableValues());
        String value = (String) chargePartyOptionGroup.getValue();
        chargingMap.put(partyK, value);
        if (subscriberK.equals(value)) {
            chargingMap.put(methodK, method.getValue().toString());
        } else if (spK.equals(value)) {
            spChargingLayout.getData(chargingMap);
        }
        data.put(chargingK, chargingMap);
    }

    @Override
    public void validate() throws Validator.InvalidValueException {

        for (Object itemId : table.getItemIds()) {
            Item row = table.getItem(itemId);
            TextField field1 = (TextField) row.getItemProperty(i18n.getMsg("create.charging.type.keyword")).getValue();
            TextField field2 = (TextField) row.getItemProperty(i18n.getMsg("create.mo.flat.charging.amount")).getValue();

            if (!VaadinUtil.validate(field1)) {
                if (!VaadinUtil.validate(field2)) {
                    logger.debug("Enter correct details.................");
                } else {
                    throw new Validator.InvalidValueException(i18n.getMsg("create.mo.flat.charging.amount.null.error"));
                }
            } else {
                throw new Validator.InvalidValueException(i18n.getMsg("create.keyword.null.error"));
            }
        }

        String value = (String) chargePartyOptionGroup.getValue();
        if (subscriberK.equals(value)) {
//            try {
//                amount.validate();
//            } catch (Validator.InvalidValueException ex) {
//                logger.error(ex.getMessage());
//                throw new IllegalStateException(ex.getMessage());
//            }
        } else if (spK.equals(value)) {
            spChargingLayout.validate();
        }

    }

    public Map<String, Object> getTableValues() {
        Map<String, Object> map = new HashMap<String, Object>();
        for (Object itemId : table.getItemIds()) {
            Item item = table.getItem(itemId);
            String keyword = (String) ((TextField) item.getItemProperty(i18n.getMsg("create.charging.type.keyword")).getValue()).getValue();
            String value = (String) ((TextField) item.getItemProperty(i18n.getMsg("create.mo.flat.charging.amount")).getValue()).getValue();
            map.put(keyword, value);
        }
        return map;
    }

    public void setTableValues(Map<String, Object> data) {
        Map<String, Object> map = (HashMap<String, Object>) data.get(keywordChargingAmountK);
        table.removeAllItems();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            table.addItem(createField(button, entry.getKey(), (String) entry.getValue(), getNextRowNumber(), table), null);
        }
    }

    private int getNextRowNumber() {
        return rowNumber++;
    }

    private class NegativeValidator implements Validator {

        private String message;

        NegativeValidator(String errorMessage) {
            this.message = errorMessage;
        }

        @Override
        public void validate(Object value) throws InvalidValueException {
            if (!isValid(value)) {
                logger.debug("Value not allowed " + value);
                throw new InvalidValueException(message);
            }
        }

        @Override
        public boolean isValid(Object value) {
            if (value.equals("") || value.equals(null)) {
                return false;
            }
            try {
                double amount = Double.parseDouble(value.toString());
                if (amount > 0) {
                    return true;
                }
            } catch (NumberFormatException e) {
                logger.debug("Number format exception occurred while validating for value" + value);
            } catch (Exception e) {
                logger.error("Unexpected error occurred while validating", e);
            }
            return false;
        }
    }
}

