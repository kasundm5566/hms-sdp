/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.ui.component.charging.field;

import com.vaadin.data.Property;
import com.vaadin.ui.Field;
import com.vaadin.ui.Layout;
import hms.kite.provisioning.commons.ui.AbstractBasicDetailsLayout;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.component.charging.ChargingValueChangeListener;

import java.util.HashMap;

import static com.vaadin.data.Property.ValueChangeListener;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public abstract class ChargingField extends AbstractBasicDetailsLayout implements ChargingValueChangeListener {

    private ValueChangeListener valueChangeListener;

    private void setFieldVisible(Field field, boolean newStatus) {
        if (isFieldNotNull(field)) {
            field.setRequired(newStatus);
            field.setVisible(newStatus);
        }
    }

    protected static final String CHARGING_TYPE = "CHARGING_TYPE";
    protected static final String CHARGING_PARTY = "CHARGING_PARTY";
    protected static final String CHARGING_FREQUENCY = "CHARGING_FREQUENCY";
    protected static final String ALLOWED_PAYMENT_INSTRUMENT = "ALLOWED_PAYMENT_INSTRUMENT";
    protected static final String CHARGING_AMOUNT = "CHARGING_AMOUNT";

    protected ChargingField(BaseApplication application, String moduleRolePrefix) {
        super(application, moduleRolePrefix);
    }

    protected Property.ValueChangeListener getValueChangeListener() {
        return valueChangeListener;
    }

    protected void hideField(Field field) {
        setFieldVisible(field, false);
    }

    protected void showField(Field field) {
        setFieldVisible(field, true);
    }

    protected abstract void init(Layout layout);

    public void initField(Layout layout, final ChargingValueChangeListener chargingValueChangeListener) {
        this.valueChangeListener = new ValueChangeListener() {

            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                chargingValueChangeListener.chargingValueChange(new HashMap<String, Object>());
            }
        };
        init(layout);
    }
}
