/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.ui.component.charging.field;

import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.ui.Field;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Select;
import com.vaadin.ui.TextField;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.validator.MinMaxChargingValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.util.AbstractMap;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.systemConfiguration;
import static hms.kite.provisioning.commons.util.ProvChargingConfiguration.getChargingAmountRegExp;
import static hms.kite.provisioning.commons.util.ProvChargingConfiguration.isServiceCodeChargingAllow;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.util.KeyNameSpaceResolver.key;
import static hms.kite.util.KiteKeyBox.amountK;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ChargingAmount extends ChargingField {

    private static final Logger logger = LoggerFactory.getLogger(ChargingAmount.class);

    private static final String DECIMAL_FORMAT = "#.00";
    private final boolean isServiceCodeChargingAllow;
    private final String width;
    private final String operator;
    private final String ncsType;

    private TextField flatChargingAmountTxtFld;
    private TextField variableMinChargingAmountTxtFld;
    private TextField variableMaxChargingAmountTxtFld;

    private Select flatServiceCodeChargingAmountSelect;

    private String currentChargingType;
    private DecimalFormat decimalFormat = new DecimalFormat(DECIMAL_FORMAT);

    private boolean isChargingType(String chargingType) {
        return chargingType.equals(currentChargingType);
    }

    private void initPaymentInstrumentFlatChargingLayout() {
        flatChargingAmountTxtFld = getFieldWithPermission(CHARGING_AMOUNT, new TextField());
        if (isFieldNotNull(flatChargingAmountTxtFld)) {
            flatChargingAmountTxtFld.setCaption(application.getMsg("common.flat.charging.amount.caption"));
            flatChargingAmountTxtFld.setDescription(application.getMsg("common.flat.charging.amount.tooltip"));
            flatChargingAmountTxtFld.setRequired(true);
            flatChargingAmountTxtFld.setImmediate(true);
            flatChargingAmountTxtFld.setRequiredError(application.getMsg("common.flat.charging.amount.required.error"));
            flatChargingAmountTxtFld.addValidator(new RegexpValidator(getChargingAmountRegExp(),
                    application.getMsg("common.charging.amount.regex.error")));
            flatChargingAmountTxtFld.setWidth(width);
        }

        if (isServiceCodeChargingAllow) {
            flatServiceCodeChargingAmountSelect = getFieldWithPermission(CHARGING_AMOUNT, new Select());
            if (isFieldNotNull(flatServiceCodeChargingAmountSelect)) {
                flatServiceCodeChargingAmountSelect.setCaption(application.getMsg("common.flat.charging.amount.caption"));
                flatServiceCodeChargingAmountSelect.setDescription(application.getMsg("common.flat.charging.amount.tooltip"));
                flatServiceCodeChargingAmountSelect.setRequired(true);
                flatServiceCodeChargingAmountSelect.setNullSelectionAllowed(false);
                flatServiceCodeChargingAmountSelect.setRequiredError(application.getMsg("common.flat.charging.amount.required.error"));
                flatServiceCodeChargingAmountSelect.setWidth(width);
                loadServiceCodes();
            }
        }

        variableMinChargingAmountTxtFld = getFieldWithPermission(CHARGING_AMOUNT, new TextField());
        if (isFieldNotNull(variableMinChargingAmountTxtFld)) {
            variableMinChargingAmountTxtFld.setCaption(application.getMsg("common.variable.min.charging.amount.caption"));
            variableMinChargingAmountTxtFld.setDescription(application.getMsg("common.variable.min.charging.amount.tooltip"));
            variableMinChargingAmountTxtFld.setRequired(true);
            variableMinChargingAmountTxtFld.setImmediate(true);
            variableMinChargingAmountTxtFld.setRequiredError(application.getMsg("common.variable.min.charging.amount.required.error"));
            variableMinChargingAmountTxtFld.addValidator(new RegexpValidator(getChargingAmountRegExp(),
                    application.getMsg("common.charging.amount.regex.error")));
            variableMinChargingAmountTxtFld.setWidth(width);
        }

        variableMaxChargingAmountTxtFld = getFieldWithPermission(CHARGING_AMOUNT, new TextField());
        if (isFieldNotNull(variableMaxChargingAmountTxtFld)) {
            variableMaxChargingAmountTxtFld.setCaption(application.getMsg("common.variable.max.charging.amount.caption"));
            variableMaxChargingAmountTxtFld.setDescription(application.getMsg("common.variable.max.charging.amount.tooltip"));
            variableMaxChargingAmountTxtFld.setRequired(true);
            variableMaxChargingAmountTxtFld.setImmediate(true);
            variableMaxChargingAmountTxtFld.setRequiredError(application.getMsg("common.variable.max.charging.amount.required.error"));
            variableMaxChargingAmountTxtFld.addValidator(new RegexpValidator(getChargingAmountRegExp(),
                    application.getMsg("common.charging.amount.regex.error")));
            variableMaxChargingAmountTxtFld.addValidator(new MinMaxChargingValidator(
                    application.getMsg("common.variable.max.charging.amount.min.max.error"), variableMinChargingAmountTxtFld));
            variableMaxChargingAmountTxtFld.setWidth(width);
        }
    }

    private void addFieldToLayout(Layout layout, Field field) {
        if (isFieldNotNull(field)) {
            layout.addComponent(field);
        }
    }

    private void hideFlatChargingAmount() {
        hideField(flatChargingAmountTxtFld);
        hideField(flatServiceCodeChargingAmountSelect);
    }

    private void showFlatChargingAmount() {
        showField(flatChargingAmountTxtFld);
        showField(flatServiceCodeChargingAmountSelect);
    }

    private void hideVariableChargingAmount() {
        hideField(variableMinChargingAmountTxtFld);
        hideField(variableMaxChargingAmountTxtFld);
    }

    private void showVariableChargingAmount() {
        showField(variableMinChargingAmountTxtFld);
        showField(variableMaxChargingAmountTxtFld);
    }

    private void loadServiceCodes() {
        try {
            //################### Please correct this in future (This is only for vcity) #####################

            String operator = this.operator;

            if (subscriptionK.equals(ncsType)) {
                operator = "safaricom";
            }

            //################################################################################################

            String key = key(operator, ncsType, serviceCodeChargingAmountMappingK);

            Map<String, String> serviceCodeAmountMap = (Map<String, String>) systemConfiguration().find(key);

            if (serviceCodeAmountMap == null) {
                throw new IllegalStateException("Can not find system configurations for key [" + key + "]");
            }

            for (Map.Entry<String, String> serviceCodeAmount : serviceCodeAmountMap.entrySet()) {
                flatServiceCodeChargingAmountSelect.addItem(serviceCodeAmount);
                flatServiceCodeChargingAmountSelect.setItemCaption(serviceCodeAmount, serviceCodeAmount.getValue());
            }

            String defaultValueKey = key(operator, ncsType, serviceCodeChargingAmountDefaultK);

            String defaultValue = (String) systemConfiguration().find(defaultValueKey);

            flatServiceCodeChargingAmountSelect.select(defaultValue);
        } catch (Exception e) {
            logger.error("Error occurred while loading service codes", e);
        }
    }

    private ChargingAmount(boolean isServiceCodeChargingAllow, String operator, String ncsType,
                           BaseApplication application, String moduleRolePrefix, String width) {
        super(application, moduleRolePrefix);

        this.isServiceCodeChargingAllow = isServiceCodeChargingAllow;
        this.operator = operator;
        this.ncsType = ncsType;
        this.width = width;

        initPaymentInstrumentFlatChargingLayout();
    }

    @Override
    public void init(Layout layout) {
        if (isServiceCodeChargingAllow) {
            addFieldToLayout(layout, flatServiceCodeChargingAmountSelect);
        } else {
            addFieldToLayout(layout, flatChargingAmountTxtFld);
        }
        addFieldToLayout(layout, variableMinChargingAmountTxtFld);
        addFieldToLayout(layout, variableMaxChargingAmountTxtFld);

        hideVariableChargingAmount();
    }

    @Override
    public void chargingValueChange(Map<String, Object> data) {
        currentChargingType = (String) data.get(typeK);

        if (isChargingType(freeK)) {
            hideFlatChargingAmount();
            hideVariableChargingAmount();
        } else if (isChargingType(flatK)) {
            hideVariableChargingAmount();
            showFlatChargingAmount();
        } else if (isChargingType(variableK)) {
            hideFlatChargingAmount();
            showVariableChargingAmount();
        }
    }

    @Override
    public void setData(Map<String, Object> data) {
        if (isChargingType(flatK)) {
            if (isServiceCodeChargingAllow) {
                if (isFieldNotNull(flatServiceCodeChargingAmountSelect)
                        && data.containsKey(serviceCodeK) && data.containsKey(amountK)) {
                    Map.Entry<String, String> serviceCodeAmount =
                            new AbstractMap.SimpleEntry<String, String>((String) data.get(serviceCodeK),
                                    (String) data.get(amountK));
                    setValueToField(flatServiceCodeChargingAmountSelect, serviceCodeAmount);
                }
            } else {
                setValueToField(flatChargingAmountTxtFld, data.get(amountK));
            }
        } else if (isChargingType(variableK)) {
            setValueToField(variableMinChargingAmountTxtFld, data.get(minAmountK));
            setValueToField(variableMaxChargingAmountTxtFld, data.get(maxAmountK));
        }
    }

    @Override
    public void getData(Map<String, Object> data) {
        if (isChargingType(flatK)) {
            if (isServiceCodeChargingAllow) {
                if (isFieldNotNull(flatServiceCodeChargingAmountSelect)) {
                    Map.Entry<String, String> chargingSelectValue =
                            (Map.Entry<String, String>) flatServiceCodeChargingAmountSelect.getValue();
                    if (chargingSelectValue != null) {
                       String chargingSelectAmount = chargingSelectValue.getValue();
                        if (!"".equals(chargingSelectAmount.trim())) {
                            data.put(amountK, formatAmount(chargingSelectAmount));
                        }
                        data.put(serviceCodeK, chargingSelectValue.getKey());
                    }
                }
            } else {
                String flatChargingAmount = (String)flatChargingAmountTxtFld.getValue();
                 if (!"".equals(flatChargingAmount.trim())) {
                     data.put(amountK, formatAmount(flatChargingAmount));
                 }
                //getValueFromField(flatChargingAmountTxtFld, data, amountK);
            }
        } else if (isChargingType(variableK)) {
            getValueFromField(variableMinChargingAmountTxtFld, data, minAmountK);
            getValueFromField(variableMaxChargingAmountTxtFld, data, maxAmountK);
        }
    }

    @Override
    public void validate() {
        if (isChargingType(flatK)) {
            if (isServiceCodeChargingAllow) {
                validateField(flatServiceCodeChargingAmountSelect);
            } else {
                validateField(flatChargingAmountTxtFld);
            }
        } else if (isChargingType(variableK)) {
            validateField(variableMinChargingAmountTxtFld);
            validateField(variableMaxChargingAmountTxtFld);
        }
    }

    @Override
    public void reset() {
        resetFields("", flatChargingAmountTxtFld, variableMinChargingAmountTxtFld, variableMaxChargingAmountTxtFld);
    }

    @Override
    public void setPermissions() {
        setReadWritePermissionToComponent(flatChargingAmountTxtFld, flatServiceCodeChargingAmountSelect,
                variableMinChargingAmountTxtFld, variableMaxChargingAmountTxtFld);
    }

    public static class Builder {

        private final String operator;
        private final String ncsType;
        private final BaseApplication application;
        private final String permissionRolePrefix;
        private final String width;

        private boolean serviceCodeChargingAllow = false;

        public Builder(String operator, String ncsType, BaseApplication application,
                       String permissionRolePrefix, String width) {
            this.operator = operator;
            this.ncsType = ncsType;
            this.application = application;
            this.permissionRolePrefix = permissionRolePrefix;
            this.width = width;
        }

        public Builder serviceCodeCharging() {
            serviceCodeChargingAllow = isServiceCodeChargingAllow();
            return this;
        }

        public ChargingAmount build() {
            return new ChargingAmount(serviceCodeChargingAllow, operator, ncsType, application, permissionRolePrefix, width);
        }
    }

    private String formatAmount(String value) {
        Double amount = Double.parseDouble(value);
        logger.debug("Format Amount - Entered Value :[{}] to [{}]", value, decimalFormat.format(amount));
        return decimalFormat.format(amount);
    }

}
