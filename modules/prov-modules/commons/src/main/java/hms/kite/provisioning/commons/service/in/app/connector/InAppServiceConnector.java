package hms.kite.provisioning.commons.service.in.app.connector;


import hms.kite.util.SdpException;

public interface InAppServiceConnector {

    QueryAppKeyResponse queryKey(String appId) throws SdpException;

    RegisterAppResponse registerApp(RegisterAppRequest request) throws SdpException;
}
