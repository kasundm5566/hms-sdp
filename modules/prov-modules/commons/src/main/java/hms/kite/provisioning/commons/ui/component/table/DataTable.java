/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.ui.component.table;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.BaseTheme;
import hms.kite.provisioning.commons.ui.Action;
import hms.kite.provisioning.commons.ui.BaseApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.provisioning.commons.util.ProvKeyBox.actionK;
import static hms.kite.provisioning.commons.util.ProvKeyBox.dataK;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public abstract class DataTable extends VerticalLayout {

    private static final Logger logger = LoggerFactory.getLogger(DataTable.class);

    private Table table;

    private final String actionRolePrefix;

    private boolean hasPermission(Action action) {
        return application.hasAnyRole(actionRolePrefix + action.name());
    }

    private Map<String, Object> createAction(Action action, Map<String, Object> app) {
        Map<String, Object> actionData = new HashMap<String, Object>();
        actionData.put(actionK, action);
        actionData.put(dataK, app);
        return actionData;
    }

    protected final BaseApplication application;
    protected final DataManagementPanel dataManagementPanel;

    protected DataTable(BaseApplication application, DataManagementPanel dataManagementPanel, String actionRolePrefix) {
        this.application = application;
        this.dataManagementPanel = dataManagementPanel;
        this.actionRolePrefix = actionRolePrefix;
    }

    protected void addActionButton(Action action, String caption, Map<String, Object> data, HorizontalLayout buttonLayout) {
        if (hasPermission(action)) {
            buttonLayout.addComponent(createLink(action, caption, data));
        }
    }

    protected Button createLink(Action action, String caption, Map<String, Object> data) {
        final Button button = new Button(caption);
        button.setStyleName(BaseTheme.BUTTON_LINK);
        button.setImmediate(true);
        button.setData(createAction(action, data));
        button.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    processAction((Map<String, Object>) button.getData());
                } catch (Exception e) {
                    logger.error("Unexpected error occurred", e);
                }
            }
        });
        return button;
    }

    protected void addContainerProperty(String msg, Class itemClass, Object defaultValue) {
        table.addContainerProperty(msg, itemClass, defaultValue);
    }

    protected void setColumnWidth(String itemName, int size) {
        table.setColumnWidth(itemName, size);
    }

    protected void addItem(Object[] objects, int rawNumber) {
        table.addItem(objects, rawNumber);
    }

    protected void setStatusMessage(String message, String style) {
        dataManagementPanel.setStatusMessage(message, style);
    }

    protected abstract void initialize();

    protected abstract void processAction(Map<String, Object> actionData);

    public void reloadTable(List<Map<String, Object>> records) {
        removeAllComponents();

        table = new Table();
        table.addStyleName("striped");
        table.setSizeFull();
        addComponent(table);

        initialize();
    }
}
