/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.util;

import hms.kite.provisioning.commons.ui.BaseApplication;

import java.util.List;

import static hms.kite.provisioning.commons.util.ProvCommonRoles.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ProvShortCodeConfiguration {

    public static void addPermittedCategories(BaseApplication application, List<String> supportedCategories) {
        if (application.hasAnyRole(SDP_SHORT_CODE)) {
            supportedCategories.add("sdp");
        }
        if (application.hasAnyRole(SOLTURA_SHORT_CODE)) {
            supportedCategories.add("soltura");
        }
        if (application.hasAnyRole(PREMIUM_SHORT_CODE)) {
            supportedCategories.add("premium");
        }
    }
}
