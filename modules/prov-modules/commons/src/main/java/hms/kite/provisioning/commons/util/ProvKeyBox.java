/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.util;

import hms.kite.util.KiteKeyBox;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ProvKeyBox extends KiteKeyBox {


    protected ProvKeyBox() {
    }

    public static final String actionK = "action";
    public static final String accountsK = "accounts";
    public static final String amountK = "amount";
    public static final String minAmountK = "min-amount";
    public static final String maxAmountK = "max-amount";
    public static final String allowedPaymentInstrumentsK = "allowed-payment-instruments";
    public static final String adminK = "admin";
    public static final String portletIdK = "portlet-id";

    public static final String portletIdSetK = "portlet-id-set";
    public static final String applyTaxK = "apply-tax";
    public static final String cancelAppK = "cancel-app";
    public static final String createNcsK = "create-ncs";
    public static final String createNcsProvK = "create-ncs-prov";
    public static final String currentSpDataK = "current-sp-data";
    public static final String spActionStatusK = "sp-action-status";
    public static final String defaultOperatorK = "default-operator";
    public static final String descriptionK = "description";
    public static final String defaultK = "default";
    public static final String defaultKeywordChargeAmountK = "default-keyword-charge-amount";
    public static final String defaultPaymentInstrumentK = "default-payment-instrument";
    public static final String deliveryReportRequiredK = "delivery-report-required";
    public static final String deliveryReportUrlK = "delivery-report-url";
    public static final String kiteUserRolesK = "kite-user-roles";
    public static final String coopUserIdK = "coop-user-id";

    public static final String enableSrK = "enable-sr";
    public static final String frequencyK = "frequency";
    public static final String hostingReqK = "hosting-required";
    public static final String keywordChargingMapK = "keyword-charging-map";
    public static final String ncsConfiguredK = "ncs-configured";
    public static final String ncsProvisionedK = "ncs-provisioned";
    public static final String ncsViewedK = "ncs-viewed";
    public static final String provisionNcsK = "provision-ncs";
    public static final String reconfigureNcsK = "reconfigure-ncs";
    public static final String revenueShareK = "revenue-share";
    public static final String routingKeysK = "routing-keys";

    public static final String saveAppAsDraftK = "save-app-as-draft";
    public static final String saveAppK = "save-app";
    public static final String saveK = "save";
    public static final String saveNcsK = "save-ncs";
    public static final String spPaymentInstrumentK = "sp-payment-instrument";
    public static final String spRequestDateK = "sp-request-date";

    public static final String selectedK = "selected";

    public static final String smsSelectedK = "sms-selected";
    public static final String smsConfiguredK = "sms-configured";
    public static final String smsStatusK = "sms-status";
    public static final String smsMoK = "sms-mo";
    public static final String smsMoTpsK = "sms-mo-tps";
    public static final String smsMoTpdK = "sms-mo-tpd";
    public static final String smsMtK = "sms-mt";
    public static final String smsMtTpsK = "sms-mt-tps";
    public static final String smsMtTpdK = "sms-mt-tpd";
    public static final String smsSlaDataK = "sms-sla-data";

    public static final String ussdSelectedK = "ussd-selected";
    public static final String ussdConfiguredK = "ussd-configured";
    public static final String ussdStatusK = "ussd-status";
    public static final String ussdMoK = "ussd-mo";
    public static final String ussdMoTpsK = "ussd-mo-tps";
    public static final String ussdMoTpdK = "ussd-mo-tpd";
    public static final String ussdMtTpsK = "ussd-mt-tps";
    public static final String ussdMtTpdK = "ussd-mt-tpd";
    public static final String ussdSlaDataK = "ussd-sla-data";

    public static final String wapPushSelectedK = "wap-push-selected";
    public static final String wapPushMtTpsK = "wap-push-mt-tps";
    public static final String wapPushMtTpdK = "wap-push-mt-tpd";
    public static final String wapPushSlaDataK = "wap-push-sla-data";

    public static final String casSelectedK = "cas-selected";
    public static final String casTpsK = "cas-mo-tps";
    public static final String casTpdK = "cas-mo-tpd";
    public static final String casSlaDataSessionK = "cas-sla-data";

    public static final String aasSelectedK = "aas-selected";
    public static final String aasAttachDetach = "aas-attach-detach";
    public static final String aasAuthenticate = "aas-authenticate";

    public static final String lbsSelectedK = "lbs-selected";
    public static final String lbsTpsK = "lbs-tps";
    public static final String lbsTpdK = "lbs-tpd";
    public static final String lbsSlaDataK = "lbs-sla-data";

    /* ^ Downloadable Application ^ */
    public static final String downloadableSelectedK = "downloadable-selected";
    public static final String platformSupportedFileTypesK = "suppported-file-types";
    public static final String downloadableSlaDataSessionK = "downloadable-sla-data";
    public static final String downloadableSlaStatusSessionK = "downloadable-sla-status";
    public static final String buildFileDataSessionK = "build-file-data";
    public static final String buildFilesDataListSessionK = "build-file-data-list";
    public static final String selectedPlatformVersions = "selected-platform-versions";
    /* ^ Downloadable Application ^ */

    public static final String subscriptionSelectedK = "subscription-selected";
    public static final String subscriptionDataK = "subscription-data";

    public static final String solturaSelectedK = "soltura-selected";

    public static final String viewNcsK = "view-ncs";

    public static final String ncsTypeK = "ncs-type";
    public static final String ncsPortletTitle = "ncs-portlet-title";
    public static final String notAvailableK = "not-available";

    public static final String requestedK = "requested";
    public static final String rejectK = "reject";
    public static final String reservedKeywordK = "reserved-keyword";
    public static final String limitedProductionK = "limited-production";
    public static final String productionK = "production";
    public static final String ncsesK = "ncses";
    public static final String appRequestDateK = "app-request-date";
    public static final String ncsConfigurationK = "ncs-configuration";
    public static final String ncsDataK = "ncs-data";

    public static final String adminUserTypeK = "ADMIN-USER-TYPE";
    public static final String spUserTypeK = "SP-USER-TYPE";
    public static final String userTypeK = "user-type";

    //event name
    public static final String SaveAppAsDraftEvent = "draft-app";
    public static final String SaveAppAsPendingApproveEvent = "save-app";
    public static final String ncsViewedEvent = "ncs-viewed";

    //CAS SLA configuration parameters
    public static final String maxTransPerSec = "tps";
    public static final String maxTransPerDay = "tpd";
    public static final String fundTransferReqAllowed = "transfer-allowed";
    public static final String minTransferAmount = "transfer-min-amount";
    public static final String maxTransferAmount = "transfer-max-amount";
    public static final String creditReserveReqAllowed = "reserve-allowed";
    public static final String minReserveAmount = "reserve-min-amount";
    public static final String maxReserveAmount = "reserve-max-amount";
    public static final String debitReqAllowed = "debit-allowed";
    public static final String minDebitAmount = "debit-min-amount";
    public static final String maxDebitAmount = "debit-max-amount";
    public static final String creditReqAllowed = "credit-allowed";
    public static final String minCreditAmount = "credit-min-amount";
    public static final String maxCreditAmount = "credit-max-amount";
    public static final String asyncChargingNotificationUrl = "async-charging-resp-url";
    public static final String postPaidBillDescCodeK = "post-paid-bill-desc-code";

    public static final String spCreateSpEvent = "sp-create-sp";
    public static final String spViewSpEvent = "sp-view-sp";
    public static final String spCancelSpEvent = "sp-cancel-sp";
    public static final String spEditSpEvent = "sp-edit-sp";

    public static final String spCreateAppEvent = "sp-create-app";
    public static final String allowspCreateAppEvent = "sp-create-app";
    public static final String spViewAppEvent = "sp-view-app";
    public static final String spCreateCrEvent = "sp-create-cr";


    public static final String adminNewSpRequestsEvent = "admin-new-sp-requests";
    public static final String adminChangeSpRequestsEvent = "admin-change-sp-requests";
    public static final String adminManageSpRequestsEvent = "admin-manage-sps";

    public static final String adminNewAppRequestsEvent = "admin-new-app-requests";
    public static final String adminChangeAppsEvent = "admin-change-apps";
    public static final String adminManageAppsEvent = "admin-manage-apps";
    public static final String adminManageBuildFilesEventK = "admin-view-build-file";


    public static final String provCreateNcsEvent = "prov-create-ncs";
    public static final String provSaveNcsEvent = "prov-save-ncs";
    public static final String provReConfigureNcsEvent = "prov-reconfigure-ncs";
    public static final String provChangeNcsEvent = "prov-change-ncs";
    public static final String provDeleteNcsEvent = "prov-change-ncs";
    public static final String provViewNcsEvent = "prov-view-ncs";
    public static final String provRefreshEvent = "prov-refresh";

    public static final String createNcsEvent = "create-ncs";
    public static final String reConfigureNcsEvent = "reconfigure-ncs";
    public static final String changeNcsEvent = "change-ncs";

    public static final String deleteNcsEvent = "delete-ncs";
    public static final String saveNcsEvent = "save-ncs";
    public static final String viewNcsEvent = "view-ncs";
    public static final String refreshProvEvent = "refresh-provision";
    public static final String refreshMenuEvent = "menu-refresh";

    /* v For Downloadable portlet only v*/
    public static final String eventOriginK = "event-origin";
    public static final String currentEventOriginK = "current-event-origin";

    public static final String provCreateBuildFileEventK = "prov-create-buildfile";
    public static final String provSaveBuildFileEventK = "prov-save-buildfile";
    public static final String provReConfigureBuildFileEventK = "prov-reconfigure-buildfile";
    public static final String provViewBuildFileEventK = "prov-view-buildfile";

    public static final String createBuildFileEventK = "create-buildfile";
    public static final String reConfigureBuildFileEventK = "reconfigure-buildfile";
    public static final String saveBuildFileEventK = "save-buildfile";
    public static final String viewBuildFileEventK = "view-buildfile";
    /* ^ For Downloadable portlet only ^ */

    public static final String ncsConfiguredEventHandler = "ncs-configured-event-handler";

    public static final String provViewK = "provView";
    public static final String spHomeK = "spHome";
    public static final String adminHomeK = "adminHome";


    public static final String sendActiveProductionImmediatelyK = "send-active-production-immediately";
    public static final String sendActiveProductionAtRequestedTimeK = "send-active-production-at-requested-time";
    public static final String sendActiveProductionAtScheduledTimeK = "send-active-production-at-scheduled-time";

    public static final String trustedK = "trusted";

    public static final String dataK = "data";
    public static final String originalSpK = "original-sp";
    public static final String removedNcsesK = "removed-ncses";


    public static final String systemK = "system";
    public static final String sdpProductNameK = "sdp-product-name";
    public static final String currentDateAndTimeK = "current-date-and-time";
    public static final String sdpAdminEmailAddressK = "sdp-admin-email-address";
    public static final String provisioningUrlK = "provisioning-url";
    public static final String spEmailFooterK = "sp-email-footer";

    public static final String spViewUploadBuildFilesK = "sp-upload-build-files";

    public static final String trialK  = "trials";
    public static final String trialAllowedK  = "allowed";
    public static final String trialPeriodK  = "period";
    public static final String maxTrialCountK  = "max-count";
    public static final String trialPeriodUnitK  = "unit";
    public static final String trialPeriodValueK  = "value";

    public static final String provEventAppNameK = "app-name";
    public static final String provEventAdditionalDataK = "prov-event-additional-data";

    /*
    Email templates keys
    */
    public static final String emailAdditionalDataK = "emailAdditionalData";
    public static final String emailAttributeInAppApiKeyK = "inAppApiKey";

    public static final String vdfApiSlaDataK = "vdf-apis-sla-data";
    public static final String vdfApisSelected = "vdf-apis-selected";
}
