package hms.kite.provisioning.commons;

import hms.kite.datarepo.audit.AuditTrail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class AuditFilter implements Filter {

    private static final Logger logger = LoggerFactory.getLogger(AuditFilter.class);

    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
            throws ServletException, IOException {

        String userName = (String) ((HttpServletRequest) req).getSession().getAttribute("kite-user-name");
        String userType = (String) ((HttpServletRequest) req).getSession().getAttribute("kite-user-type");
        logger.debug("Set current user as {} inside {}", userName, ((HttpServletRequest) req).getContextPath());
        if (userName != null) {
            AuditTrail.setCurrentUser(userName, userType);
        }
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
