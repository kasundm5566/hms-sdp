/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.ui;


import com.vaadin.data.Item;
import com.vaadin.data.Validator;
import com.vaadin.ui.*;
import org.vaadin.addon.customfield.CustomField;

import java.util.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class VaadinUtil {

    public static void setAllReadonly(boolean readonly, Layout layout) {
        layout.setReadOnly(readonly);

        Iterator<Component> componentIterator = layout.getComponentIterator();

        while (componentIterator.hasNext()) {
            Component next = componentIterator.next();
            next.setReadOnly(readonly);

            /*if (readonly && next instanceof Field) {
                removeComponentValidators((Field) next);
            }*/

            if (next instanceof Form) {
                setAllReadonly(readonly, ((Form) next).getLayout());
            } else if (next instanceof Layout) {
                setAllReadonly(readonly, (Layout) next);
            } else if (next instanceof CustomField) {
                setAllReadOnly(readonly, (CustomField) next);
            } else if (next instanceof Table) {
                Table table = (Table) next;
                for (Object id : table.getItemIds()) {
                    Item row = table.getItem(id);
                    Collection tc = table.getContainerPropertyIds();
                    for (Object o : tc) {
                        Field field = (Field) row.getItemProperty(o).getValue();
                        field.setReadOnly(readonly);
                    }
                }
            }
        }
    }

    public static void setAllReadonly(Form form, boolean readOnly) {
        setAllReadonly(readOnly, form.getLayout());
    }

    /**
     * @param field
     * @return - true if validation failed
     */
    public static boolean validate(AbstractField field) {
        boolean validationFailed = true;
        try {
            field.validate();
            validationFailed = false;
        } catch (Validator.EmptyValueException e) {
            //validation failed
        } catch (Validator.InvalidValueException e) {
            field.setComponentError(e);
        }
        return validationFailed;
    }

    /**
     * Validate all the AbstractFields in layouts. Deep validation take place if there are inner layouts.
     *
     * @param layout
     * @return - true if there is at least one error. If no error return false.
     */
    public static boolean validate(Layout layout) {
        boolean validationFailed = false;
        Iterator<Component> componentIterator = layout.getComponentIterator();
        while (componentIterator.hasNext()) {
            Component component = componentIterator.next();
            if (validationFailed) {
                validateComponent(component);
            } else {
                validationFailed = validateComponentWithResult(component);
            }
        }
        return validationFailed;
    }

    private static boolean validateComponentWithResult(Component component) {
        boolean validationFailed = false;
        if (component instanceof Layout) {
            validationFailed = validate((Layout) component);
        } else if (component instanceof AbstractField) {
            validationFailed = validate((AbstractField) component);
        }
        return validationFailed;
    }

    private static void validateComponent(Component component) {
        if (component instanceof Layout) {
            validate((Layout) component);
        } else if (component instanceof AbstractField) {
            validate((AbstractField) component);
        }
    }

    public static boolean validate(Form form) {
        return validate(form.getLayout());
    }

    public static void cleanErrorComponents(AbstractField field) {
        field.setComponentError(null);
    }

    /**
     * Set error component null.
     *
     * @param layout -
     */
    public static void cleanErrorComponents(Layout layout) {
        Iterator<Component> componentIterator = layout.getComponentIterator();
        while (componentIterator.hasNext()) {
            Component component = componentIterator.next();
            if (component instanceof Layout) {
                cleanErrorComponents((Layout) component);
            } else if (component instanceof AbstractField) {
                cleanErrorComponents((AbstractField) component);
            }
        }
    }

    public static void setAllReadOnly(boolean readOnly, CustomField customField) {
        Iterator<Component> componentIterator = customField.getComponentIterator();
        while (componentIterator.hasNext()) {
            Component next = componentIterator.next();
            if (next instanceof Layout) {
                setAllReadonly(readOnly, (Layout) next);
            } else if (next instanceof Field) {
                next.setReadOnly(readOnly);
            }
        }
    }

    public static void removeComponentValidators(Field field) {

        Collection<Validator> validators = field.getValidators();
        if(validators == null || validators.isEmpty()) {
            return;
        }

        List<Validator> validatorList = new ArrayList<Validator>();
        validatorList.addAll(validators);

        for (Validator validator : validatorList) {
            field.removeValidator(validator);
        }
    }

    public static void setEnabled(boolean enabled, Field... fields) {
        for (Field field : fields) {
            field.setEnabled(enabled);
        }
    }
}
