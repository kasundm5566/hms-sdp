/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.util;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ProvCommonRoles {

    public static final String COMMON_PERMISSION_ROLE_PREFIX = "ROLE_PROV_COMMON";

    public static final String SDP_SHORT_CODE = COMMON_PERMISSION_ROLE_PREFIX + "_SDP_SHORT_CODE";
    public static final String SOLTURA_SHORT_CODE = COMMON_PERMISSION_ROLE_PREFIX + "_SOLTURA_SHORT_CODE";
    public static final String PREMIUM_SHORT_CODE = COMMON_PERMISSION_ROLE_PREFIX + "_PREMIUM_SHORT_CODE";
}
