/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.commons.event;

import hms.kite.util.SdpException;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import org.slf4j.Logger;

import static hms.kite.util.KiteErrorBox.systemErrorCode;
import static hms.kite.util.KiteKeyBox.eventNameK;
/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class DefaultEventRouter implements EventRouter {

    private List<EventListener> listeners;
    private Map<String, List<EventListener>> mappedListeners;
    private ExecutorService executorService;
    private LinkedBlockingQueue<Map<String, Object>> linkedBlockingQueue;

    private int numberOfThreads;
    private int queueWarnLimit;

    private static final Logger logger = LoggerFactory.getLogger(DefaultEventRouter.class);

    public DefaultEventRouter() {
        mappedListeners = new HashMap<String, List<EventListener>>();
        linkedBlockingQueue = new LinkedBlockingQueue<Map<String, Object>>();
    }

    @Override
    public void start() {
        logger.info("Executor Service is being initialized with FixedThreadPool [NumberOfThread: {}]", numberOfThreads);
        //TODO: Use better executor by considering concurrency
        executorService = Executors.newFixedThreadPool(numberOfThreads);
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    try {
                        executorService.submit(newRunnable(linkedBlockingQueue.take()));
                    } catch (InterruptedException e) {
                        String error = "Interrupted while waiting to be executed. Unfinished tasks are cancelled. Error: " + e;
                        logger.error(error);
                        throw new SdpException(systemErrorCode, error);
                    }
                }
            }
        });
    }

    @Override
    public void shutdown() {
        logger.info("ExecutorService is being shutdown");
        executorService.shutdown();
    }

    @Override
    public void fire(final Map<String, Object> data) {
        logger.debug("Blocking Queue Size: {}, Data {}", linkedBlockingQueue.size(), data);
        linkedBlockingQueue.add(data);
        if(linkedBlockingQueue.size() >= queueWarnLimit) {
            logger.warn("The number of elements in the blocking queue exceeded, {}", queueWarnLimit);
        }
    }

    private Runnable newRunnable(final Map<String, Object> data) {
        final String eventName = (String) data.get(eventNameK);
        return new Runnable() {
            @Override
            public void run() {
                logger.info("Event: {} is being handled [Data: {}", eventName, data);
                List<EventListener> eventListeners = mappedListeners.get(eventName);
                for (EventListener eventListener : eventListeners) {
                    eventListener.handle(data);
                }
            }
        };
    }

    public void setEventListeners(List<EventListener> listeners) {
        this.listeners = listeners;
        for (EventListener listener : listeners) {
            for (String eventName : listener.supportedEvents()) {
                index(eventName, listener);
            }
        }
    }

    private void index(String eventName, EventListener listener) {
        List<EventListener> eventListeners = mappedListeners.get(eventName);
        if (eventListeners == null) {
            eventListeners = new ArrayList<EventListener>();
            mappedListeners.put(eventName, eventListeners);
        }
        eventListeners.add(listener);
    }

    public void setNumberOfThreads(int numberOfThreads) {
        this.numberOfThreads = numberOfThreads;
    }

    public void setQueueWarnLimit(int queueWarnLimit) {
        this.queueWarnLimit = queueWarnLimit;
    }
}
