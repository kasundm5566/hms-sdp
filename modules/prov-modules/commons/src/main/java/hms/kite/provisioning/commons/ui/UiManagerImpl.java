/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.provisioning.commons.ui;

import com.vaadin.Application;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Panel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class UiManagerImpl implements UiManager {

    private static final Logger logger = LoggerFactory.getLogger(UiManagerImpl.class);

    private Map<String, Layout> views = new HashMap<String, Layout>();
    private Stack<Layout> screenStack = new Stack<Layout>();
    private Panel window;

    public UiManagerImpl(Application application) {
        this.window = application.getMainWindow();
    }

    public void init() {
    }

    /**
     * Switches current screen to the given screen.
     *
     * @param viewName The screen to switch to.
     * @param newView  If not null, the existing screen is replaced with the given
     *                 screen.
     */
    public void switchScreen(String viewName, Layout newView) {
        Layout view;
        if (newView != null) {
            view = newView;
            views.put(viewName, newView);
        } else {
            view = views.get("viewname");
        }
        window.setContent(view);
    }

    /**
     * Switches to the given screen and pushes the current screen to stack. The
     * pushed screen can be switched back to by calling popScreen().
     *
     * @param viewName Screen to switch to.
     */
    public void pushScreen(String viewName, Layout newView) {
        screenStack.push((Layout) window.getContent());
        switchScreen(viewName, newView);
    }

    /**
     * Switches back to the topmost screen in the screen stack.
     */
    public void popScreen() {
        if (!screenStack.isEmpty()) {
            Layout previousScreen = screenStack.pop();
            logger.debug("Previous Screen found {}", previousScreen);
            if (previousScreen instanceof UiPopListener) {
                UiPopListener uiPopListener = (UiPopListener) previousScreen;
                uiPopListener.onUiPop();
            }
            window.setContent(previousScreen);
        }

    }

    @Override
    public void pushCurrentScreen() {
        screenStack.push((Layout) window.getContent());
    }

    @Override
    public void clearAll() {
        views.clear();
        screenStack.clear();
    }

    public Panel getWindow() {
        return window;
    }
}
