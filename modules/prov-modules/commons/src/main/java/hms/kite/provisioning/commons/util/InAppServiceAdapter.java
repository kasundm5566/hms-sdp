package hms.kite.provisioning.commons.util;


import com.google.common.base.Optional;
import com.google.common.collect.ImmutableBiMap;
import hms.kite.provisioning.commons.service.in.app.connector.*;
import hms.kite.util.NullObject;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InAppServiceAdapter {

    private static InAppServiceConnector inAppServiceConnector;
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyyMMddHHmmssSSS");
    private static final String requestingSystemId = "Provisioning";
    private static final DateTimeFormatter isoDateFormat = ISODateTimeFormat.dateTime();

    private static final Logger logger = LoggerFactory.getLogger(InAppServiceAdapter.class);

    static {
        setInAppServiceConnector(new NullObject<InAppServiceConnectorImpl>().get());
    }

    private static void setInAppServiceConnector(InAppServiceConnector inAppServiceConnector) {
        InAppServiceAdapter.inAppServiceConnector = inAppServiceConnector;
    }

    public static Optional<String> createKey(String appId, String appName, String spId) {
        final DateTime date = DateTime.now();
        final String requestId = dateTimeFormatter.print(date.getMillis());
        final RegisterAppRequest request = new RegisterAppRequest(requestId, isoDateFormat.print(date), requestingSystemId, appId, appName, spId);
        logger.debug("Dispatching In-App api key creation request = [{}].", request);

        try {
            RegisterAppResponse registerAppResponse = inAppServiceConnector.registerApp(request);

            if(Optional.fromNullable(registerAppResponse).isPresent()) {
                logger.debug("Create In-App api key response = [{}] received, for application with id = [{}].", registerAppResponse, appId);
                return Optional.fromNullable(registerAppResponse.getResult());
            }

            return Optional.absent();
        } catch (Exception e) {
            logger.debug("Create In-App api key, error occurred while processing the request [{}].", e);
            return Optional.absent();
        }
    }

    public static Optional<String> queryKey(String appId) {
        logger.debug("Querying In-App api key for application id = [{}].", appId);
        try {
            final QueryAppKeyResponse queryAppKeyResponse = inAppServiceConnector.queryKey(appId);
            return Optional.fromNullable(queryAppKeyResponse.getResult());
        } catch (Exception e) {
            logger.debug("Query In-App api key, error occurred while processing the request [{}].", e);
            return Optional.absent();
        }
    }

    public static Optional<String> getKey(String appId, String appName, String spId) {

        try {
            logger.debug("Querying In-App api key for application id = [{}].", appId);
            final QueryAppKeyResponse queryAppKeyResponse = inAppServiceConnector.queryKey(appId);
            if(Optional.fromNullable(queryAppKeyResponse).isPresent()) {
                logger.debug("Query response = [{}] received from In-Api for application with id = [{}].", queryAppKeyResponse, appId);
                return Optional.fromNullable(queryAppKeyResponse.getResult());
            } else {
                final DateTime date = DateTime.now();
                final String requestId = dateTimeFormatter.print(date.getMillis());
                final RegisterAppRequest request = new RegisterAppRequest(requestId, isoDateFormat.print(date), requestingSystemId, appId, appName, spId);
                logger.debug("Dispatching In-App api key creation request = [{}].", request);
                RegisterAppResponse registerAppResponse = inAppServiceConnector.registerApp(request);

                if(Optional.fromNullable(registerAppResponse.getResult()).isPresent()) {
                    logger.debug("Create In-App api key response = [{}] received, for application with id = [{}].", registerAppResponse, appId);
                    return Optional.fromNullable(registerAppResponse.getResult());
                }

                return Optional.absent();
            }
        } catch (Exception e) {
            logger.debug("Get In-App api key, error occurred while processing the request [{}].", e);
            return Optional.absent();
        }
    }

    public enum InAppCreateKeyStatusCode {
        S1000("S1000"),
        S1003("S1003");

        private String statusCode;

        final static ImmutableBiMap<String, InAppCreateKeyStatusCode> statusCodeMap;

        static {
            statusCodeMap = new ImmutableBiMap.Builder<String, InAppCreateKeyStatusCode>().
                    put("S1000", S1000).
                    put("S1003", S1003).build();
        }

        InAppCreateKeyStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        public static Optional<InAppCreateKeyStatusCode> getStatusByCode(String statusCode) {
            return Optional.fromNullable(statusCodeMap.get(statusCode));
        }
    }

    public enum InAppQueryKeyStatusCode {
        S1000("S1000");

        private String statusCode;

        final static ImmutableBiMap<String, InAppQueryKeyStatusCode> statusCodeMap;

        static {
            statusCodeMap = new ImmutableBiMap.Builder<String, InAppQueryKeyStatusCode>().
                    put("S1000", S1000).build();
        }

        InAppQueryKeyStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        public static Optional<InAppQueryKeyStatusCode> getStatusByCode(String statusCode) {
            return Optional.fromNullable(statusCodeMap.get(statusCode));
        }
    }
}
