package hms.kite.provisioning.commons.service.in.app.connector;

import org.joda.time.DateTime;

public class RegisterAppRequest {

    private String requestId;
    private String requestDateTime;
    private String requestingSystem;
    private String appId;
    private String appName;
    private String spId;

    public RegisterAppRequest() {
    }

    public RegisterAppRequest(String requestId, String requestDateTime, String requestingSystem, String appId, String appName, String spId) {
        this.requestId = requestId;
        this.requestDateTime = requestDateTime;
        this.requestingSystem = requestingSystem;
        this.appId = appId;
        this.appName = appName;
        this.spId = spId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestDateTime() {
        return requestDateTime;
    }

    public void setRequestDateTime(String requestDateTime) {
        this.requestDateTime = requestDateTime;
    }

    public String getRequestingSystem() {
        return requestingSystem;
    }

    public void setRequestingSystem(String requestingSystem) {
        this.requestingSystem = requestingSystem;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getSpId() {
        return spId;
    }

    public void setSpId(String spId) {
        this.spId = spId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("RegisterAppRequest{");
        sb.append("requestId='").append(requestId).append('\'');
        sb.append(", requestDateTime='").append(requestDateTime).append('\'');
        sb.append(", requestingSystem='").append(requestingSystem).append('\'');
        sb.append(", appId='").append(appId).append('\'');
        sb.append(", appName='").append(appName).append('\'');
        sb.append(", spId='").append(spId).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
