/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.provisioning.commons.ui;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class LiferayUtil {

    public static String createPortletId(String ncsType, String operator) {
        if (ncsType != null) {
            ncsType = ncsType.replace("-", "");
        }
        if (operator != null) {
            operator = operator.replace("-", "");
        }

        if (operator == null) {
            return createPortletId(ncsType);
        }
        final StringBuilder portletName = new StringBuilder();
        portletName.append(operator).append(ncsType).append("provisioning_WAR_").append(operator).append(ncsType);
        return portletName.toString();
    }

    public static String createPortletId(String ncsType) {
        return createPortletId(ncsType, "");
    }
}
