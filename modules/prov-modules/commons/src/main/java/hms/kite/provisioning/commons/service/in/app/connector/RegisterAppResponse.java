package hms.kite.provisioning.commons.service.in.app.connector;

public class RegisterAppResponse {

    private String statusCode;
    private String statusDescription;
    private String requestId;
    private String result;

    public RegisterAppResponse() {
    }

    public RegisterAppResponse(String statusCode, String statusDescription, String requestId, String result) {
        this.statusCode = statusCode;
        this.statusDescription = statusDescription;
        this.requestId = requestId;
        this.result = result;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("RegisterAppResponse{");
        sb.append("statusCode='").append(statusCode).append('\'');
        sb.append(", statusDescription='").append(statusDescription).append('\'');
        sb.append(", requestId='").append(requestId).append('\'');
        sb.append(", result=").append(result);
        sb.append('}');
        return sb.toString();
    }
}
