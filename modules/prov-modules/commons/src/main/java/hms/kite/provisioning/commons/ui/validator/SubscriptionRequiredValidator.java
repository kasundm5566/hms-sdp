/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.ui.validator;

import com.vaadin.data.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import static hms.kite.provisioning.commons.util.ProvKeyBox.ncsTypeK;
import static hms.kite.util.KiteKeyBox.subscriptionK;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class SubscriptionRequiredValidator implements Validator {

    private static Logger logger = LoggerFactory.getLogger(SubscriptionRequiredValidator.class);

    private List<Map> selectedNcses;
    private String errorMessage;

    public SubscriptionRequiredValidator(List<Map> selectedNcses, String errorMessage) {
        this.selectedNcses = selectedNcses;
        this.errorMessage = errorMessage;
    }

    @Override
    public void validate(Object value) throws InvalidValueException {
        if (!isValid(value)) {
            throw new InvalidValueException(errorMessage);
        }
    }

    @Override
    public boolean isValid(Object value) {
        try {
            if ((Boolean) value) {
                for (Map selectedNcs : selectedNcses) {
                    if (subscriptionK.equals(selectedNcs.get(ncsTypeK))) {
                        return true;
                    }
                }
            } else {
                return true;
            }
        } catch (Exception e) {
            logger.error("Unexpected error occurred while validating", e);
        }
        return false;
    }
}
