/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.commons.ui.validator;

import com.vaadin.data.Validator;
import hms.kite.datarepo.RepositoryServiceRegistry;
import hms.kite.util.KeyNameSpaceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * $LastChangedDate: 2011-08-08 12:36:24 +0530 (Mon, 08 Aug 2011) $
 * $LastChangedBy: mohameda $
 * $LastChangedRevision: 75733 $
 */
public class MessageLimitValidator implements Validator {

    private static Logger logger = LoggerFactory.getLogger(MessageLimitValidator.class);

    private String errorMessage;
    private String coopUserId;
    private String ncsType;
    private String direction;
    private String limitType;
    private int limit;

    public MessageLimitValidator(String errorMessage, String coopUserId, String ncsType, String direction, String limitType) {
        this.coopUserId = coopUserId;
        this.errorMessage = errorMessage;
        this.ncsType = ncsType;
        this.direction = direction;
        this.limitType = limitType;
    }

    @Override
    public void validate(Object value) throws InvalidValueException {
        if (!isValid(value)) {
            throw new InvalidValueException(errorMessage + " " + limit);
        }
    }

    @Override
    public boolean isValid(Object value) {
        try {
            Map<String, Object> sp = RepositoryServiceRegistry.spRepositoryService().findSpByCoopUserId(coopUserId);
            limit = Integer.parseInt((String) sp.get(KeyNameSpaceResolver.key(ncsType, direction, limitType)));
            int s = Integer.parseInt((String) value);
            return s <= limit;
        } catch (NumberFormatException e) {
            logger.debug("Number format is not in integer format in tps or tpd filed");
            return false;
        } catch (Exception e) {
            logger.error("Unexpected error occurred while validating", e);
            return false;
        }
    }
}
