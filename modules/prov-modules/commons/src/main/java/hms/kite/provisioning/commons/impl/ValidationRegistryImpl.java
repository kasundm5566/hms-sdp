/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.provisioning.commons.impl;

import hms.kite.provisioning.commons.ValidationRegistry;

import java.util.HashMap;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ValidationRegistryImpl implements ValidationRegistry {

    private Map<String, String> validators = new HashMap<String, String>();

    @Override
    public String regex(String key) {
        String value = validators.get(key);
        if (value == null) {
            throw new RuntimeException("Regular expression for the key [" + key + "] is not defined in Validation Registry");
        }
        return value;
    }

    public void setValidators(Map<String, String> validators) {
        this.validators = validators;
    }
}
