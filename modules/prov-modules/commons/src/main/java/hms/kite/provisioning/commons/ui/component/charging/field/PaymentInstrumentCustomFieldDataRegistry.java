package hms.kite.provisioning.commons.ui.component.charging.field;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import com.vaadin.ui.Field;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.component.charging.field.custom.MpaisaPaymentInstrumentAdditionalDataField;

import java.util.Map;

public class PaymentInstrumentCustomFieldDataRegistry {


    public enum PaymentInstrumentWithAdditionalData {
        MPAISA("Mpaisa", getMpaisaAdditionalDataField());
        private String name;
        private Function<BaseApplication, Field> function;

        PaymentInstrumentWithAdditionalData(String name, Function<BaseApplication, Field> function) {
            this.name = name;
            this.function = function;
        }

        public String getName() {
            return name;
        }

        public Function<BaseApplication, Field> getFunction() {
            return function;
        }
    }

    private static final Map<String, PaymentInstrumentWithAdditionalData> paymentInstrumentWithAdditionalDataMap;

    static {
        paymentInstrumentWithAdditionalDataMap = ImmutableMap.<String, PaymentInstrumentWithAdditionalData>builder().
                                                                    put("Mpaisa", PaymentInstrumentWithAdditionalData.MPAISA).
                                                                    build();
    }

    public static Optional<Field> getCustomFieldByPaymentInstrumentName(String checkBoxKey, BaseApplication application) {
        PaymentInstrumentWithAdditionalData paymentInstrumentWithAdditionalData = paymentInstrumentWithAdditionalDataMap.get(checkBoxKey);
        if(!Optional.fromNullable(paymentInstrumentWithAdditionalData).isPresent()) {
            return Optional.absent();
        } else {
            return Optional.fromNullable(paymentInstrumentWithAdditionalData.getFunction().apply(application));
        }

    }

    public static Function<BaseApplication, Field> getMpaisaAdditionalDataField() {
        return new Function<BaseApplication, Field>() {
            @Override
            public Field apply(BaseApplication input) {
                return new MpaisaPaymentInstrumentAdditionalDataField(input);
            }
        };
    }
}
