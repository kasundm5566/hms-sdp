package hms.kite.provisioning.commons.service.in.app.connector;

import hms.kite.util.GsonUtil;
import hms.kite.util.SdpException;
import org.apache.cxf.jaxrs.client.WebClient;

import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.io.InputStreamReader;

import static hms.commons.SnmpLogUtil.clearTrap;
import static hms.commons.SnmpLogUtil.trap;
import static hms.kite.util.KiteErrorBox.systemErrorCode;

public class InAppServiceConnectorImpl implements InAppServiceConnector {

    private String registerApplicationUrl;
    private String queryApplicationKeyUrl;
    private String successKey;
    private String successMessage;
    private String failedKey;
    private String failedMessage;

    @Override
    public QueryAppKeyResponse queryKey(String appId) throws SdpException {

        try {
            WebClient webClient = WebClient.create(queryApplicationKeyUrl + "/" + appId);
            webClient.header("Content-Type", "application/json");
            webClient.accept("application/json");

            final Response response = webClient.get();

            QueryAppKeyResponse queryAppKeyResponse = readResponse((InputStream) response.getEntity(), QueryAppKeyResponse.class);

            clearTrap(successKey, successMessage);

            return queryAppKeyResponse;
        } catch (Exception e) {
            trap(failedKey, failedMessage);
            throw new SdpException(systemErrorCode, "Error while querying for in-app api key.", e);
        }
    }

    private <T> T readResponse(InputStream is, Class<T> t) {
        return GsonUtil.getGson().fromJson(new InputStreamReader(is), t);
    }

    @Override
    public RegisterAppResponse registerApp(RegisterAppRequest request) {
        try {
            WebClient webClient = WebClient.create(registerApplicationUrl);
            webClient.header("Content-Type", "application/json");
            webClient.accept("application/json");

            final Response response = webClient.post(GsonUtil.toJson(request));

            RegisterAppResponse registerAppResponse = readResponse((InputStream) response.getEntity(), RegisterAppResponse.class);

            clearTrap(successKey, successMessage);

            return registerAppResponse;
        } catch (Exception e) {
            trap(failedKey, failedMessage);
            throw new SdpException(systemErrorCode, "Error while registering in in-app server.", e);
        }

    }

    public void setRegisterApplicationUrl(String registerApplicationUrl) {
        this.registerApplicationUrl = registerApplicationUrl;
    }

    public void setQueryApplicationKeyUrl(String queryApplicationKeyUrl) {
        this.queryApplicationKeyUrl = queryApplicationKeyUrl;
    }

    public void setSuccessKey(String successKey) {
        this.successKey = successKey;
    }

    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }

    public void setFailedKey(String failedKey) {
        this.failedKey = failedKey;
    }

    public void setFailedMessage(String failedMessage) {
        this.failedMessage = failedMessage;
    }
}
