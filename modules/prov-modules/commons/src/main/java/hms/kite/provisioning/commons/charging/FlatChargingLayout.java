/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.charging;

import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.terminal.ExternalResource;
import com.vaadin.terminal.SystemError;
import com.vaadin.ui.*;
import hms.kite.provisioning.commons.ui.DataComponent;
import hms.kite.provisioning.commons.ui.DataComponentValidator;
import hms.kite.provisioning.commons.ui.I18nProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.systemConfiguration;
import static hms.kite.provisioning.commons.util.ProvChargingConfiguration.*;
import static hms.kite.util.KeyNameSpaceResolver.key;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
@Deprecated
public class FlatChargingLayout extends VerticalLayout implements DataComponent {

    //todo select the key of i18n messages based on the direction.
    private OptionGroup chargePartyOptionGroup;
    private SubscriberChargingLayout subscriberChargingLayout;
    private SpChargingLayout spChargingLayout;
    private static final Logger logger = LoggerFactory.getLogger(FlatChargingLayout.class);
    private I18nProvider i18n;

    public FlatChargingLayout(String operatorId, Map<String, Map<String, Object>> instruments,
                              I18nProvider i18n, String corporateUserId) {

        this.i18n = i18n;
        final FormLayout bodyLayout = new FormLayout();
        bodyLayout.addStyleName("charging-form");
        chargePartyOptionGroup = new OptionGroup(i18n.getMsg("create.charge.party"));
        if (isSubscriberChargingPartyAllow()) {
            chargePartyOptionGroup.addItem(subscriberK);
            chargePartyOptionGroup.setItemCaption(subscriberK, i18n.getMsg("create.charge.party.subscriber"));
        }
        if (isServiceProviderChargingPartyAllow()) {
            chargePartyOptionGroup.addItem(spK);
            chargePartyOptionGroup.setItemCaption(spK, i18n.getMsg("create.charge.party.sp"));
        }
        chargePartyOptionGroup.setImmediate(true);
        chargePartyOptionGroup.setRequired(true);
        chargePartyOptionGroup.addListener(new Property.ValueChangeListener() {

            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                if (subscriberK.equals(chargePartyOptionGroup.getValue())) {
                    subscriberChargingLayout.setVisible(true);
                    spChargingLayout.setVisible(false);
                } else {
                    subscriberChargingLayout.setVisible(false);
                    spChargingLayout.setVisible(true);
                }
            }
        });

        subscriberChargingLayout = new SubscriberChargingLayout(operatorId, i18n);
        subscriberChargingLayout.setVisible(false);
        subscriberChargingLayout.addStyleName("charging-form");

        spChargingLayout = new SpChargingLayout(instruments, i18n, corporateUserId);
        spChargingLayout.setVisible(false);
        spChargingLayout.addStyleName("charging-form");

        bodyLayout.addComponent(chargePartyOptionGroup);

        addComponent(bodyLayout);
        addComponent(subscriberChargingLayout);
        addComponent(spChargingLayout);

        chargePartyOptionGroup.select(getDefaultChargingParty());
    }

    @Override
    public void setData(Map<String, Object> data) {
        Map<String, Object> chargingMap = (Map<String, Object>) data.get(chargingK);
        String value = (String) chargingMap.get(partyK);
        chargePartyOptionGroup.select(value);
        if (subscriberK.equals(value)) {
            subscriberChargingLayout.setData(chargingMap);
        } else if (spK.equals(value)) {
            spChargingLayout.setData(chargingMap);
        }
    }

    @Override
    public void getData(Map<String, Object> data) {
        Map<String, Object> chargingMap = new HashMap<String, Object>();
        chargingMap.put(typeK, flatK);
        String value = (String) chargePartyOptionGroup.getValue();
        chargingMap.put(partyK, value);
        if (subscriberK.equals(value)) {
            subscriberChargingLayout.getData(chargingMap);
        } else if (spK.equals(value)) {
            spChargingLayout.getData(chargingMap);
        }
        data.put(chargingK, chargingMap);
    }

    @Override
    public void validate() {
        String value = (String) chargePartyOptionGroup.getValue();
        if (subscriberK.equals(value)) {
            subscriberChargingLayout.validate();
        } else if (spK.equals(value)) {
            spChargingLayout.validate();
            spChargingLayout.validate(flatK);
        }
    }

    public void setDefaultFlatChargingAmount(String chargingAmount) {
        //To change body of created methods use File | Settings | File Templates.
    }

    private static class SubscriberChargingLayout extends FormLayout implements DataComponent {

        private OptionGroup chargingMethodOptionGroup;
        private I18nProvider i18nProvider;
        private TextField chargingAmountTxtFld;
        private Select operatorChargingSelect;

        private SubscriberChargingLayout(String operatorId, I18nProvider i18n) {
            this.i18nProvider = i18n;
            chargingMethodOptionGroup = new OptionGroup(i18n.getMsg("create.mo.flat.charging.method"));
            if (isPaymentInstrumentChargingMethodAllow()) {
                chargingMethodOptionGroup.addItem(defaultPaymentInstrumentK);
                chargingMethodOptionGroup.setItemCaption(defaultPaymentInstrumentK,
                        i18n.getMsg("create.mo.flat.charging.method.default.payment.instrument"));
            }
            if (isOperatorChargingMethodAllow()) {
                chargingMethodOptionGroup.addItem(operatorChargingK);
                chargingMethodOptionGroup.setItemCaption(operatorChargingK, i18n.getMsg("create.mo.flat.charging.method.operator"));
            }
            chargingMethodOptionGroup.setImmediate(true);
            chargingMethodOptionGroup.setRequired(true);
            chargingMethodOptionGroup.addListener(new Property.ValueChangeListener() {

                @Override
                public void valueChange(Property.ValueChangeEvent event) {
                    if (defaultPaymentInstrumentK.equals(chargingMethodOptionGroup.getValue())) {
                        removeComponent(operatorChargingSelect);
                        addComponent(chargingAmountTxtFld);
                    } else {
                        removeComponent(chargingAmountTxtFld);
                        addComponent(operatorChargingSelect);
                    }
                }
            });

            chargingAmountTxtFld = new TextField(i18n.getMsg("create.mo.flat.charging.amount"));
            chargingAmountTxtFld.setWidth("280px");
            chargingAmountTxtFld.setImmediate(true);
            chargingAmountTxtFld.setRequiredError(i18n.getMsg("create.mo.flat.charging.amount.null.error"));
            chargingAmountTxtFld.addValidator(new RegexpValidator(getChargingAmountRegExp(), i18nProvider.getMsg("create.mo.flat.charging.amount.numeric.error")));
            chargingAmountTxtFld.setRequired(true);

            operatorChargingSelect = new Select(i18n.getMsg("create.mo.flat.operator.charging.code.selection"));
            operatorChargingSelect.setWidth("280px");
            operatorChargingSelect.setRequired(true);
            operatorChargingSelect.setNullSelectionAllowed(false);
            loadServiceCodes(operatorId, operatorChargingSelect);
            operatorChargingSelect.setImmediate(true);

            addComponent(chargingMethodOptionGroup);

            chargingMethodOptionGroup.select(getDefaultChargingMethod());
        }

        private void loadServiceCodes(String operatorId, Select operatorChargingSelect) {
            operatorChargingSelect.setComponentError(null);
            try {
                String key = key(operatorId, smsK, serviceCodeChargingAmountMappingK);

                Map<String, String> serviceCodeAmountMap = (Map<String, String>) systemConfiguration().find(key);

                if (serviceCodeAmountMap == null) {
                    throw new IllegalStateException("Can not find system configurations for key [" + key + "]");
                }

                for (Map.Entry<String, String> serviceCodeAmount : serviceCodeAmountMap.entrySet()) {
                    operatorChargingSelect.addItem(serviceCodeAmount);
                    operatorChargingSelect.setItemCaption(serviceCodeAmount, serviceCodeAmount.getValue());
                }

                String defaultValueKey = key(operatorId, smsK, serviceCodeChargingAmountDefaultK);

                String defaultValue = (String) systemConfiguration().find(defaultValueKey);

                operatorChargingSelect.setValue(defaultValue);

            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                operatorChargingSelect.setComponentError(new SystemError(i18nProvider.getMsg("system.error.message")));
            }
        }

        @Override
        public void setData(Map<String, Object> data) {
            String value = (String) data.get(methodK);
            chargingMethodOptionGroup.select(value);
            if (operatorChargingK.equals(value)) {
                Map.Entry<String, String> serviceCodeAmount =
                        new AbstractMap.SimpleEntry<String, String>((String) data.get(serviceCodeK), (String) data.get(amountK));
                operatorChargingSelect.select(serviceCodeAmount);
            } else {
                chargingAmountTxtFld.setValue(data.get(amountK));
            }
        }

        @Override
        public void getData(Map<String, Object> data) {
            String chargingMethod = chargingMethodOptionGroup.getValue().toString();
            data.put(methodK, chargingMethod);
            if (operatorChargingK.equals(chargingMethod)) {
                Map.Entry<String, String> chargingSelectValue = (Map.Entry<String, String>) operatorChargingSelect.getValue();
                data.put(amountK, chargingSelectValue.getValue());
                data.put(serviceCodeK, chargingSelectValue.getKey());
            } else {
                data.put(amountK, chargingAmountTxtFld.getValue());
            }
        }

        @Override
        public void validate() {
            String value = (String) chargingMethodOptionGroup.getValue();
            if (defaultPaymentInstrumentK.equals(value)) {
                chargingAmountTxtFld.validate();
            } else if (operatorChargingK.equals(value)) {
                if (operatorChargingSelect.getValue() == null || operatorChargingSelect.getValue() == "") {
                    logger.error(i18nProvider.getMsg("create.mo.operator.charging.null.error"));
                    throw new Validator.InvalidValueException(i18nProvider.getMsg("create.mo.operator.charging.null.error"));
                }
            }
        }
    }

    public static class SpChargingLayout extends FormLayout implements DataComponent, DataComponentValidator {

        private Select paymentInstrumentSelect;

        private TextField amountTxtFld;
        private Select paymentAccountSelect;
        /**
         *
         */
        private Map<String, Map<String, Object>> instruments;

        private Link pgUrl;

        private I18nProvider i18n;

        public SpChargingLayout(Map<String, Map<String, Object>> instruments,
                                I18nProvider i18nProvider,
                                String corporateUserId) {
            this.i18n = i18nProvider;
            paymentInstrumentSelect = new Select(i18nProvider.getMsg("create.mo.flat.payment.instrument"));
            paymentInstrumentSelect.setWidth("280px");
            paymentInstrumentSelect.setRequired(true);
            paymentInstrumentSelect.setNullSelectionAllowed(false);

            amountTxtFld = new TextField(i18nProvider.getMsg("create.mo.flat.charging.amount"));
            amountTxtFld.setRequired(true);
            amountTxtFld.setWidth("280px");
            amountTxtFld.addValidator(new RegexpValidator(getChargingAmountRegExp(), i18nProvider.getMsg("create.mo.flat.charging.amount.numeric.error")));
            amountTxtFld.setRequiredError(i18nProvider.getMsg("create.mo.flat.charging.amount.null.error"));

            loadInstrumentDetails(instruments, corporateUserId);

            //todo show the payment gateway url to configure payment instruments if there isn't payment instrument.

            Map<String, String> paymentInstruments = findPaymentInstruments();

            String defaultInstrument = null;

            for (Map.Entry<String, String> instrument : paymentInstruments.entrySet()) {
                paymentInstrumentSelect.addItem(instrument.getKey());
                paymentInstrumentSelect.setItemCaption(instrument.getKey(), instrument.getValue());
                defaultInstrument = instrument.getKey();
            }

            paymentInstrumentSelect.setImmediate(true);
            paymentInstrumentSelect.addListener(new Property.ValueChangeListener() {

                @Override
                public void valueChange(Property.ValueChangeEvent event) {
                    loadAccounts((String) event.getProperty().getValue());
                }
            });

            paymentAccountSelect = new Select(i18nProvider.getMsg("create.mo.flat.payment.account"));
            paymentAccountSelect.setRequired(true);
            paymentAccountSelect.setImmediate(true);
            paymentAccountSelect.setNullSelectionAllowed(false);
            loadAccounts(defaultInstrument);

            if (paymentInstruments.size() == 0) {
                initPaymentGatewayUrlButton();
                addComponent(pgUrl);
            } else {
                addComponent(paymentInstrumentSelect);
                addComponent(paymentAccountSelect);
            }
            addComponent(amountTxtFld);

            paymentInstrumentSelect.select(defaultInstrument);
        }

        private void initPaymentGatewayUrlButton() {
            pgUrl = new Link(i18n.getMsg("create.configure.payment.instruments.link.message"),
                    new ExternalResource(i18n.getMsg("create.configure.payment.instruments.link")));
        }

        private void loadAccounts(String instrumentId) {
            if (instrumentId == null) {
                logger.debug("Instrument id is null.");
                return;
            }
            Map<String, String> accounts = findAccounts(instrumentId);
            paymentAccountSelect.removeAllItems();
            for (Map.Entry<String, String> accountEntry : accounts.entrySet()) {
                paymentAccountSelect.addItem(accountEntry.getKey());
                paymentAccountSelect.setItemCaption(accountEntry.getKey(), accountEntry.getValue());
                paymentAccountSelect.select(accountEntry.getKey());
            }
        }

        private Map<String, String> findAccounts(String instrumentId) {
            Map<String, Map<String, String>> accountsEntries = (Map<String, Map<String, String>>) instruments.get(instrumentId).get("accounts");

            Map<String, String> accounts = new HashMap<String, String>();

            for (Map<String, String> account : accountsEntries.values()) {
                accounts.put(account.get("account-id"), account.get("account-id"));
            }

            for (String accountId : accountsEntries.keySet()) {
                accounts.put(accountId, accountId);
            }

            return accounts;
        }

        private void loadInstrumentDetails(Map<String, Map<String, Object>> instruments, String corporateUserId) {
//            this.instruments = OperatorServiceRegistry.paymentGatewayAdaptor().paymentInstruments(corporateUserId);
            this.instruments = instruments;
        }

        private Map<String, String> findPaymentInstruments() {
            Map<String, String> instruments = new HashMap<String, String>();

            for (Map.Entry<String, Map<String, Object>> instrument : this.instruments.entrySet()) {

                String instrumentId = (String) instrument.getValue().get("instrument-id");
                String name = (String) instrument.getValue().get("name");

                instruments.put(instrumentId, name);

            }
            return instruments;
        }

        public TextField getAmountTxtFld() {
            return amountTxtFld;
        }

        @Override
        public void setData(Map<String, Object> data) {
            paymentInstrumentSelect.select(data.get(paymentInstrumentIdK));
            paymentAccountSelect.select(data.get(paymentAccountK));
            amountTxtFld.setValue(data.get(amountK));
        }

        @Override
        public void getData(Map<String, Object> data) {
            String instrumentId = paymentInstrumentSelect.getValue().toString();
            data.put(paymentInstrumentIdK, instrumentId);
            data.put(paymentInstrumentNameK, instruments.get(instrumentId).get(nameK));
            data.put(paymentAccountK, paymentAccountSelect.getValue());
            data.put(amountK, amountTxtFld.getValue());
        }

        @Override
        public void validate() {
            Map<String, String> paymentInstruments = findPaymentInstruments();

            if (paymentInstruments.size() == 0) {
                logger.error("No payment instruments configured for sp");
                throw new Validator.InvalidValueException(i18n.getMsg("create.flat.sp.payment.instrument.not.configured.error"));
            }
            if (paymentInstrumentSelect.getValue() == null || paymentAccountSelect.getValue() == "") {
                logger.error(i18n.getMsg("create.mo.flat.payment.instrument.null.error"));
                throw new Validator.InvalidValueException(i18n.getMsg("create.mo.flat.payment.instrument.null.error"));
            }
            if (paymentAccountSelect.getValue() == null || paymentAccountSelect.getValue() == "") {
                logger.error(i18n.getMsg("create.mo.flat.payment.account.null.error"));
                throw new Validator.InvalidValueException(i18n.getMsg("create.mo.flat.payment.account.null.error"));
            }
        }

        @Override
        public void validate(String chargingType) {
            if (keywordK.equals(chargingType)) {
                logger.debug("not need to validate amount");
            } else if (flatK.equals(chargingType) || variableK.equals(chargingType)) {
                amountTxtFld.validate();
            }
        }
    }
}
