/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.provisioning.commons.ui;


import hms.kite.util.SdpException;

import java.util.Map;

/**
* $LastChangedDate$
* $LastChangedBy$
* $LastChangedRevision$
 */
public class DefaultErrorCodeResolver implements ErrorCodeResolver {

    private Map<String, String> errorCodeKeyMapping;
    private String defaultKey;

    @Override
    public String findMessageKey(SdpException exception){

        String value = errorCodeKeyMapping.get(exception.getErrorCode());
        if (value == null) {
            value = errorCodeKeyMapping.get(defaultKey);
        }
        return value;
    }

    public void setErrorCodeKeyMapping(Map<String, String> errorCodeKeyMapping) {
        this.errorCodeKeyMapping = errorCodeKeyMapping;
    }

    public void setDefaultKey(String defaultKey) {
        this.defaultKey = defaultKey;
    }
}
