/*
 *   (C) Copyright 2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.provisioning.commons.ui.validator;

import com.vaadin.data.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static hms.kite.datarepo.RepositoryServiceRegistry.ncsRepositoryService;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class TillNumberExistsValidator implements Validator {

    private String seperator;
    private String errorMessage;

    private static final Logger logger = LoggerFactory.getLogger(TillNumberExistsValidator.class);

    public TillNumberExistsValidator(String seperator, String errorMessage) {
        this.seperator = seperator;
        this.errorMessage = errorMessage;
    }

    @Override
    public void validate(Object value) throws InvalidValueException {
        if (!isValid(value)) {
            logger.info("till number already exists for till numbers {}", new Object[]{ value});
            throw new InvalidValueException(errorMessage);
        }
    }

    @Override
    public boolean isValid(Object value) {
        try {
            String[] tillNumbers = String.valueOf(value).split(seperator);
            for(String tillNo: tillNumbers) {
                if(!tillNo.trim().isEmpty()) {
                    if (ncsRepositoryService().isTillNumberExists(tillNo.trim())) return false;
                }
            }
        } catch (Exception e) {
            logger.error("Unexpected error occurred while validating", e);
            return false;
        }
        return true;
    }


}
