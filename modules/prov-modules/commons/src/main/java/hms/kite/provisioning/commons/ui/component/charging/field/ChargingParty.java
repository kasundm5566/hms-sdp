/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.ui.component.charging.field;

import com.vaadin.ui.Layout;
import com.vaadin.ui.OptionGroup;
import hms.kite.provisioning.commons.ui.BaseApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.provisioning.commons.util.ProvChargingConfiguration.*;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ChargingParty extends ChargingField {

    private static final Logger logger = LoggerFactory.getLogger(ChargingParty.class);

    private final Map<String, Object> chargingParties;
    private final String width;

    private OptionGroup chargingPartyOptGrp;

    private String defaultChargingParty;

    private String currentChargingType;

    private boolean isChargingType(String chargingType) {
        return chargingType.equals(currentChargingType);
    }

    private boolean isNoChargingPartyAllow() {
        return chargingParties.isEmpty();
    }

    private void initChargingPartyOptGrp() {
        chargingPartyOptGrp = getFieldWithPermission(CHARGING_PARTY, new OptionGroup());
        if (isFieldNotNull(chargingPartyOptGrp)) {
            chargingPartyOptGrp.setCaption(application.getMsg("common.charging.party.caption"));
            chargingPartyOptGrp.setDescription(application.getMsg("common.charging.party.tooltip"));
            chargingPartyOptGrp.setRequired(true);
            chargingPartyOptGrp.setRequiredError(application.getMsg("common.charging.party.required.error"));
            chargingPartyOptGrp.setImmediate(true);
            chargingPartyOptGrp.setWidth(width);
            chargingPartyOptGrp.setNullSelectionAllowed(false);
        }
    }

    private void populateChargingParties() {
        for (Map.Entry<String, Object> entry : chargingParties.entrySet()) {
            chargingPartyOptGrp.addItem(entry.getKey());
            chargingPartyOptGrp.setItemCaption(entry.getKey(), (String) entry.getValue());
        }
    }

    private ChargingParty(Map<String, Object> chargingParties, BaseApplication application, String moduleRolePrefix, String width) {
        super(application, moduleRolePrefix);

        this.chargingParties = chargingParties;
        this.width = width;

        initChargingPartyOptGrp();
    }

    @Override
    public void init(Layout layout) {
        if (!isFieldNotNull(chargingPartyOptGrp) || isNoChargingPartyAllow()) {
            defaultChargingParty = getDefaultChargingParty();
            return;
        }

        populateChargingParties();

        chargingPartyOptGrp.addListener(getValueChangeListener());
        chargingPartyOptGrp.select(getDefaultChargingParty());
        layout.addComponent(chargingPartyOptGrp);
    }

    @Override
    public void chargingValueChange(Map<String, Object> data) {
        currentChargingType = (String) data.get(typeK);

        if (!isFieldNotNull(chargingPartyOptGrp) || isNoChargingPartyAllow()) {
            return;
        }

        if (isChargingType(freeK)) {
            hideField(chargingPartyOptGrp);
        } else {
            showField(chargingPartyOptGrp);
        }
    }

    @Override
    public void setData(Map<String, Object> data) {
        if (!isChargingType(freeK)) {
            if (!isFieldNotNull(chargingPartyOptGrp) || isNoChargingPartyAllow()) {
                defaultChargingParty = (String) data.get(partyK);
            } else {
                setValueToField(chargingPartyOptGrp, data.get(partyK));
            }
        }
    }

    @Override
    public void getData(Map<String, Object> data) {
        if (!isChargingType(freeK)) {
            if (defaultChargingParty != null && isNoChargingPartyAllow()) {
                data.put(partyK, defaultChargingParty);
            } else {
                getValueFromField(chargingPartyOptGrp, data, partyK);
            }
        }
    }

    @Override
    public void validate() {
        if (!isChargingType(freeK) && !isNoChargingPartyAllow()) {
            validateField(chargingPartyOptGrp);
        }
    }

    @Override
    public void reset() {
    }

    @Override
    public void setPermissions() {
        setReadWritePermissionToComponent(chargingPartyOptGrp);
    }

    public static class Builder {

        private final BaseApplication application;
        private final String permissionRolePrefix;
        private final String width;
        private final Map<String, Object> chargingParties;

        private Builder addChargingParty(boolean isChargingPartyAllow, String type, String caption) {
            if (isChargingPartyAllow) {
                chargingParties.put(type, caption);
            }
            return this;
        }

        public Builder(BaseApplication application, String permissionRolePrefix, String width) {
            this.application = application;
            this.permissionRolePrefix = permissionRolePrefix;
            this.width = width;
            this.chargingParties = new HashMap<String, Object>();
        }

        public Builder subscriber() {
            return addChargingParty(isSubscriberChargingPartyAllow(), subscriberK,
                    application.getMsg("common.charging.party.subscriber.caption"));
        }

        public Builder sp() {
            return addChargingParty(isServiceProviderChargingPartyAllow(), spK,
                    application.getMsg("common.charging.party.service.provider.caption"));
        }

        public ChargingParty build() {
            return new ChargingParty(chargingParties, application, permissionRolePrefix, width);
        }
    }
}
