/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.ui.component.table;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.UiPopListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public abstract class AbstractDataManagementPanel extends VerticalLayout implements DataManagementPanel, UiPopListener {

    private static final Logger logger = LoggerFactory.getLogger(AbstractDataManagementPanel.class);

    private SearchLayout searchLayout;
    private DataTable dataTable;
    private Paging pagingComponent;

    private VerticalLayout searchResultLayout;
    private Panel mainPanel;
    private Label statusLbl;

    private boolean status = false;

    private String noRecordsFoundError = "No record(s) found";
    private boolean showSearchLayout = true;
    private int recordsPerPage = 15;

    private void init() {
        statusLbl = new Label();

        searchResultLayout = new VerticalLayout();

        mainPanel = new Panel();
        addComponent(mainPanel);
    }

    protected final BaseApplication application;

    protected abstract SearchLayout getSearchLayout();

    protected abstract DataTable getDataTable();

    protected abstract void finalizeQueryCriteria(Map<String, Object> queryCriteria);

    protected abstract int getTotalRecords(Map<String, Object> queryCriteria);

    protected abstract List<Map<String, Object>> getRecords(int start, int batchSize, Map<String, Object> queryCriteria);

    @Override
    public void search() {
        final Map<String, Object> queryCriteria = searchLayout.getQueryCriteria();

        finalizeQueryCriteria(queryCriteria);

        pagingComponent = new Paging(
                new PagingListener() {

                    @Override
                    public void onReload(int start, int batchSize) {
                        try {
                            List<Map<String, Object>> records = getRecords(start, batchSize, queryCriteria);
                            status = records.isEmpty();
                            dataTable.reloadTable(records);
                        } catch (Exception e) {
                            logger.error("Error occurred while reloading the table", e);
                        }
                    }
                }, getTotalRecords(queryCriteria), recordsPerPage);
        pagingComponent.init();

        setStatusMessage(status ? noRecordsFoundError : null, "sp-request-fail-notification");

        searchResultLayout.removeAllComponents();
        searchResultLayout.addComponent(statusLbl);
        searchResultLayout.addComponent(dataTable);
        searchResultLayout.addComponent(pagingComponent);
        searchResultLayout.setComponentAlignment(pagingComponent, Alignment.MIDDLE_CENTER);
    }

    @Override
    public void setStatusMessage(String message, String style) {
        statusLbl.setVisible(message != null);
        statusLbl.setValue(message);
        statusLbl.setStyleName(style);
    }

    @Override
    public void setCaption(String caption) {
        mainPanel.setCaption(caption);
    }

    @Override
    public void onUiPop() {
        loadComponents();
    }

    public AbstractDataManagementPanel(BaseApplication application) {
        this.application = application;

        init();
    }

    public void loadComponents() {
        mainPanel.removeAllComponents();
        searchResultLayout.removeAllComponents();
        if (showSearchLayout) {
            searchLayout = getSearchLayout();
            mainPanel.addComponent(searchLayout);
        }
        dataTable = getDataTable();
        mainPanel.addComponent(searchResultLayout);
        search();
    }

    public void setNoRecordsFoundError(String error) {
        this.noRecordsFoundError = error;
    }

    public void setShowSearchLayout(boolean showSearchLayout) {
        this.showSearchLayout = showSearchLayout;
    }

    public void setRecordsPerPage(int recordsPerPage) {
        this.recordsPerPage = recordsPerPage;
    }
}