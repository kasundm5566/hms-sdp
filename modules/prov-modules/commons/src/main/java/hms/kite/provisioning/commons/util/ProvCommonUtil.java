/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.util;

import hms.kite.provisioning.commons.ui.BaseApplication;

import java.util.Map;

import static hms.kite.util.KiteKeyBox.appIdK;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ProvCommonUtil {

    public static Map<String, Object> getNcsDataFromSessionByAppId(String appId, String sessionKey, BaseApplication application) {
        return getNcsDataFromSessionByKey(appIdK, appId, sessionKey, application);
    }

    public static Map<String, Object> getNcsDataFromSessionByKey(String mapKey, String value, String sessionKey, BaseApplication application) {
        Map<String, Object> result = (Map<String, Object>) application.getFromSession(sessionKey);
        if (result != null && value != null && value.equals(result.get(mapKey))) {
            return result;
        }
        return null;
    }
}
