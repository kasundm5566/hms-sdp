/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.ui.component.table;

import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import hms.kite.provisioning.commons.ui.BaseApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public abstract class AbstractStandardSearchLayout extends SearchLayout {

    private static final Logger logger = LoggerFactory.getLogger(AbstractStandardSearchLayout.class);

    private final DataManagementPanel dataManagementPanel;

    private String height = "60px";

    private Button searchBtn;

    private void init() {
        searchBtn = new Button();
        searchBtn.setCaption("Search");
        searchBtn.setHeight("25px");
        searchBtn.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    dataManagementPanel.search();
                } catch (Exception e) {
                    logger.error("Unexpected error occurred", e);
                }
            }
        });
    }

    private HorizontalLayout createButtonBar() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setHeight(height);
        horizontalLayout.setSpacing(true);

        addSearchFields(horizontalLayout);

        addToHorizontalLayout(true, horizontalLayout, searchBtn);

        return horizontalLayout;
    }

    protected final BaseApplication application;

    protected void addToHorizontalLayout(boolean available, HorizontalLayout horizontalLayout, Component component) {
        if (available) {
            FormLayout formLayout = new FormLayout();
            formLayout.addComponent(component);
            horizontalLayout.addComponent(formLayout);
        }
    }

    protected void setStatusMessage(String message, String style) {
        dataManagementPanel.setStatusMessage(message, style);
    }

    protected abstract void addSearchFields(HorizontalLayout horizontalLayout);

    @Override
    public void setCaption(String caption) {
        searchBtn.setCaption(caption);
    }

    public AbstractStandardSearchLayout(BaseApplication application, DataManagementPanel dataManagementPanel) {
        this.application = application;
        this.dataManagementPanel = dataManagementPanel;

        init();
    }

    public void loadComponents() {
        removeAllComponents();
        addComponent(createButtonBar());
    }

    public void setSearchBarHeight(String height) {
        this.height = height;
    }
}
