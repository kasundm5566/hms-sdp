/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.ui.component.charging.field;

import com.vaadin.ui.Layout;
import com.vaadin.ui.Select;
import hms.kite.provisioning.commons.ui.BaseApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.provisioning.commons.util.ProvChargingConfiguration.*;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ChargingType extends ChargingField {

    private static final Logger logger = LoggerFactory.getLogger(ChargingType.class);

    private final Map<String, Object> chargingTypes;
    private final String width;

    private Select chargingTypeSelect;

    private void initChargingType() {
        chargingTypeSelect = getFieldWithPermission(CHARGING_TYPE, new Select());
        if (isFieldNotNull(chargingTypeSelect)) {
            chargingTypeSelect.setCaption(application.getMsg("common.charging.type.caption"));
            chargingTypeSelect.setDescription(application.getMsg("common.charging.type.tooltip"));
            chargingTypeSelect.setRequired(true);
            chargingTypeSelect.setRequiredError(application.getMsg("common.charging.type.required.error"));
            chargingTypeSelect.setNullSelectionAllowed(false);
            chargingTypeSelect.setImmediate(true);
            chargingTypeSelect.setWidth(width);
        }
    }

    private void populateChargingTypes() {
        for (Map.Entry<String, Object> entry : chargingTypes.entrySet()) {
            chargingTypeSelect.addItem(entry.getKey());
            chargingTypeSelect.setItemCaption(entry.getKey(), (String) entry.getValue());
        }
    }

    private ChargingType(Map<String, Object> chargingTypes, BaseApplication application, String moduleRolePrefix,
                         String width) {
        super(application, moduleRolePrefix);

        this.chargingTypes = chargingTypes;
        this.width = width;

        initChargingType();
    }

    @Override
    public void init(Layout layout) {
        if (isFieldNotNull(chargingTypeSelect)) {
            populateChargingTypes();

            chargingTypeSelect.addListener(getValueChangeListener());
            chargingTypeSelect.setValue(getDefaultChargingType());
            layout.addComponent(chargingTypeSelect);
        }
    }

    @Override
    public void chargingValueChange(Map<String, Object> data) {
    }

    @Override
    public void setData(Map<String, Object> data) {
        setValueToField(chargingTypeSelect, data.get(typeK));
    }

    @Override
    public void getData(Map<String, Object> data) {
        // set default value for charging type
        data.put(typeK, freeK);
        getValueFromField(chargingTypeSelect, data, typeK);
    }

    @Override
    public void validate() {
        validateField(chargingTypeSelect);
    }

    @Override
    public void reset() {
    }

    @Override
    public void setPermissions() {
        setReadWritePermissionToComponent(chargingTypeSelect);
    }

    public static class Builder {

        private final BaseApplication application;
        private final String permissionRolePrefix;
        private final String width;
        private final Map<String, Object> chargingTypes;

        private Builder addChargingType(boolean isChargingTypeAllow, String type, String caption) {
            if (isChargingTypeAllow) {
                chargingTypes.put(type, caption);
            }
            return this;
        }

        public Builder(BaseApplication application, String permissionRolePrefix, String width) {
            this.application = application;
            this.permissionRolePrefix = permissionRolePrefix;
            this.width = width;
            this.chargingTypes = new HashMap<String, Object>();
        }

        public Builder free() {
            return addChargingType(isFreeChargingTypeAllow(), freeK,
                    application.getMsg("common.charging.type.free.caption"));
        }

        public Builder flat() {
            return addChargingType(isFlatChargingTypeAllow(), flatK,
                    application.getMsg("common.charging.type.flat.caption"));
        }

        public Builder keyword() {
            return addChargingType(isKeywordChargingTypeAllow(), keywordK,
                    application.getMsg("common.charging.type.keyword.caption"));
        }

        public Builder variable() {
            return addChargingType(isVariableChargingTypeAllow(), variableK,
                    application.getMsg("common.charging.type.variable.caption"));
        }

        public ChargingType build() {
            return new ChargingType(chargingTypes, application, permissionRolePrefix, width);
        }
    }
}
