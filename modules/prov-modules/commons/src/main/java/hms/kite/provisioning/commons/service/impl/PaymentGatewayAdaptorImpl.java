/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.service.impl;

import hms.kite.provisioning.commons.service.PaymentGatewayAdaptor;
import hms.kite.util.KiteErrorBox;
import hms.kite.util.KiteKeyBox;
import hms.kite.util.SdpException;
import hms.pgw.api.common.PayInstrumentInfo;
import hms.pgw.api.request.PayInstrumentQueryMessage;
import hms.pgw.api.response.PayInstrumentResponse;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class PaymentGatewayAdaptorImpl implements PaymentGatewayAdaptor {

    private static final Logger logger = LoggerFactory.getLogger(PaymentGatewayAdaptorImpl.class);

    private String instrumentQueryApiUrl;
    private String instrumentQueryFailed;

    private PayInstrumentResponse readResponse(InputStream is) {
        JAXBContext ctx;
        PayInstrumentResponse res;
        try {
            ctx = JAXBContext.newInstance(PayInstrumentResponse.class);
            res = (PayInstrumentResponse) ctx.createUnmarshaller().unmarshal(new InputStreamReader(is));
        } catch (Exception e) {
            throw new SdpException(KiteErrorBox.systemErrorCode, e);
        }
        return res;
    }

    private void addInstrument(PayInstrumentInfo inst, Map<String, Map<String, Object>> result) {

        String payInstrumentId = inst.getPayInsAccountId();

        Map<String, Object> paymentInstrument = result.get(payInstrumentId);

        if (paymentInstrument == null) {
            paymentInstrument = new HashMap<String, Object>();
            paymentInstrument.put("instrument-id", Long.toString(inst.getPayInstrumentId()));
            paymentInstrument.put("name", inst.getPayInstrumentName());
            paymentInstrument.put("accounts", new HashMap<String, Map<String, String>>());
            result.put(payInstrumentId, paymentInstrument);
        }
        Map<String, Map<String, String>> accounts = (Map<String, Map<String, String>>) paymentInstrument.get("accounts");


        HashMap<String, String> accountDetails;

        if (accounts.get(inst.getPayInsAccountId()) == null) {

            accountDetails = new HashMap<String, String>();

            accounts.put(inst.getPayInsAccountId(), accountDetails);

            accountDetails.put("account-id", inst.getPayInsAccountId());
            accountDetails.put("currency", inst.getAccountCurrecy());
            accountDetails.put("default", Boolean.toString(inst.isDefault()));
        }
    }

    @Override
    public Map<String, Map<String, Object>> paymentInstruments(String corporateUserId) throws SdpException {

        Map<String, Map<String, Object>> result;

        try {
            logger.debug("Query the payment gateway with url [{}] to find payment instruments for [{}]", instrumentQueryApiUrl, corporateUserId);

            WebClient webClient;

            webClient = WebClient.create(instrumentQueryApiUrl);
            webClient.header("Content-Type", "application/xml");
            webClient.accept("application/xml");

            PayInstrumentQueryMessage msg = new PayInstrumentQueryMessage();
            msg.setUserId(corporateUserId);

            Response response = webClient.post(msg);
            InputStream is = (InputStream) response.getEntity();

            PayInstrumentResponse payInstrumentResponse = readResponse(is);

            result = new HashMap<String, Map<String, Object>>();

            PayInstrumentInfo[] payInstrumentInfoList = payInstrumentResponse.getPayInstrumentInfos();
            if (payInstrumentInfoList != null) {
                for (PayInstrumentInfo inst : payInstrumentInfoList) {
                    if (KiteKeyBox.syncK.equals(inst.getPayInsType())) {
                        addInstrument(inst, result);
                    }
                }
            }

            logger.debug("Response from payment-gateway instruments for user [{}] is [{}]", corporateUserId, result);

            return result;
        } catch (Exception e) {
            logger.error("Error while getting payment instruments {}", e);
            throw new SdpException("Error while getting payment instruments", e);
        }
    }

    public void setInstrumentQueryApiUrl(String instrumentQueryApiUrl) {
        this.instrumentQueryApiUrl = instrumentQueryApiUrl;
    }

    public void setInstrumentQueryFailed(String instrumentQueryFailed) {
        this.instrumentQueryFailed = instrumentQueryFailed;
    }
}
