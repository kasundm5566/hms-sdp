package hms.kite.provisioning.commons.ui.component.charging.field.custom;

import com.google.common.base.Optional;
import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.util.ProvChargingConfiguration;
import hms.kite.util.KiteKeyBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.addon.customfield.CustomField;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.businessIdK;
import static hms.kite.util.KiteKeyBox.realTimeChargingEnabledK;

public class MpaisaPaymentInstrumentAdditionalDataField extends CustomField {

    private static final Logger logger = LoggerFactory.getLogger(MpaisaPaymentInstrumentAdditionalDataField.class);

    private final BaseApplication application;

    private final CheckBox realTimeChargingCheckBox;
    private final TextField mpaisaBusinessIdTextField;

    public MpaisaPaymentInstrumentAdditionalDataField(BaseApplication application) {
        this.application = application;

        final VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setWidth(this.getWidth(), this.getWidthUnits());

        realTimeChargingCheckBox = new CheckBox(application.getMsg("common.charging.allowed.payment.instrument.mpaisa.real.time.charging.enable.checkbox.caption"));
        mpaisaBusinessIdTextField = new TextField(application.getMsg("common.charging.allowed.payment.instrument.mpaisa.business.id.caption"));

        mpaisaBusinessIdTextField.setRequired(true);
        mpaisaBusinessIdTextField.setRequiredError(application.getMsg("common.charging.allowed.payment.instrument.mpaisa.business.id.required.error"));

        realTimeChargingCheckBox.setImmediate(true);
        mpaisaBusinessIdTextField.setImmediate(true);

        Optional<String> businessIdValidationPatternOpt = ProvChargingConfiguration.getPropertyByKey("common.charging.mpaisa.business.id.pattern");

        if(businessIdValidationPatternOpt.isPresent()) {
            mpaisaBusinessIdTextField.addValidator(new RegexpValidator(businessIdValidationPatternOpt.get(), application.getMsg("common.charging.allowed.payment.instrument.mpaisa.business.id.validation.error")));
        }

        verticalLayout.addComponent(realTimeChargingCheckBox);

        realTimeChargingCheckBox.addListener(new ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                if(Boolean.TRUE.equals(event.getProperty().getValue())) {
                    verticalLayout.addComponent(mpaisaBusinessIdTextField);
                } else {
                    verticalLayout.removeComponent(mpaisaBusinessIdTextField);
                }
            }
        });

        realTimeChargingCheckBox.setValue(false);

        setCompositionRoot(verticalLayout);
    }

    @Override
    public Class<?> getType() {
        return MpaisaPaymentInstrumentAdditionalDataField.class;
    }

    @Override
    public void setValue(Object newValue) throws ReadOnlyException, ConversionException {
        try {
            Map values = (Map<String, Object>) newValue;
            if(Optional.fromNullable(values.get(realTimeChargingEnabledK)).isPresent()) {
                try {
                    Boolean realTimeChargingEnabled = (Boolean) values.get(realTimeChargingEnabledK);
                    if(realTimeChargingEnabled) {
                        realTimeChargingCheckBox.setValue(realTimeChargingEnabled);
                        Object businessId = values.get(KiteKeyBox.businessIdK);
                        if(Optional.fromNullable(businessId).isPresent()) {
                            mpaisaBusinessIdTextField.setValue(businessId);
                        }
                    }
                } catch (ClassCastException | NullPointerException e) {
                    logger.error("Error while setting data to the mpaisa charging additional data component [{}].", e);
                }
            }
        } catch (ClassCastException | NullPointerException e) {
            logger.error("Error while setting data to the mpaisa charging additional data component [{}].", e);
        }
    }

    @Override
    public Object getValue() {
        Map<String,Object> result = new HashMap<>();

        result.put(realTimeChargingEnabledK, realTimeChargingCheckBox.booleanValue());

        if(realTimeChargingCheckBox.booleanValue() && Optional.fromNullable(mpaisaBusinessIdTextField.getValue()).isPresent()) {
            result.put(businessIdK, mpaisaBusinessIdTextField.getValue().toString());
        }

        return result;
    }

    @Override
    public void validate() throws Validator.InvalidValueException {
        if(realTimeChargingCheckBox.booleanValue()) {
            mpaisaBusinessIdTextField.validate();
        }
    }
}
