/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.provisioning.commons.ui.validator;

import com.vaadin.data.Validator;
import com.vaadin.ui.Field;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class TpsTpdValidator implements Validator {

    private  static Logger logger = LoggerFactory.getLogger(TpsTpdValidator.class);

    private Field tpsField;
    private Field tpdField;
    private String validationFailedMessage;

    public TpsTpdValidator(Field tpsField, Field tpdField, String validationFailedMessage) {
        this.tpsField = tpsField;
        this.tpdField = tpdField;
        this.validationFailedMessage = validationFailedMessage;
    }

    @Override
    public void validate(Object value) throws InvalidValueException {
       if(!isValid(value)) {
           throw new InvalidValueException(validationFailedMessage);
       }
    }

    @Override
    public boolean isValid(Object value) {
        try {
            int tps = Integer.parseInt((String) tpsField.getValue());
            int tpd = Integer.parseInt((String) tpdField.getValue());
            if (tps < tpd) {
                return true;
            }
            return false;
        } catch (NumberFormatException e) {
            logger.debug("Number format is not in integer format in tps or tpd filed");
            return false;
        }catch (Exception e) {
            logger.error("Unexpected error occurred while validating", e);
            return false;
        }
    }
}
