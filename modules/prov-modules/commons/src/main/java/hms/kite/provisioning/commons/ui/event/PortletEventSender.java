/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/

package hms.kite.provisioning.commons.ui.event;

import com.vaadin.Application;
import com.vaadin.terminal.gwt.server.PortletApplicationContext2;
import hms.kite.datarepo.audit.AuditTrail;
import hms.kite.util.KiteKeyBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.namespace.QName;
import java.util.HashMap;
import java.util.Map;

/*
* $LastChangedDate: 2011-05-12 17:46:05 +0530 (Thu, 12 May 2011) $
* $LastChangedBy: dimuthu $
* $LastChangedRevision: 73118 $
*/

public class PortletEventSender {

    private static final Logger logger = LoggerFactory.getLogger(PortletEventSender.class);


    public static void send(Map<String, Object> valueMap, String eventName, Application application) {
        if (valueMap != null) {
            valueMap.put(KiteKeyBox.currentUsernameK, AuditTrail.getCurrentUsername());
            valueMap.put(KiteKeyBox.currentUserTypeK, AuditTrail.getCurrentUserType());
        }
        PortletApplicationContext2 ctx = (PortletApplicationContext2) application.getContext();
        ctx.sendPortletEvent(application.getMainWindow(), new QName(
                "http://vaadin.com/liferay", eventName), (HashMap) valueMap);
        logger.debug("Portlet event [{}] , type [{}] sent", valueMap, eventName);

    }

//    public void send(SendingEvent sendingEvent) {
//        Gson gson = new Gson();
//        String message = gson.toJson(sendingEvent);
//
//        String eventName = "provisioning";
//        PortletApplicationContext2 ctx = (PortletApplicationContext2) application.getContext();
//        ctx.sendPortletEvent(application.getMainWindow(), new QName(
//                "http://vaadin.com/liferay", eventName), message);
//        logger.debug("Portlet event [{}] , type [{}] sent", message, eventName);
//    }

//    @Override
//    public void send(OperatorNewAppRequest request) {
//        Gson gson = new Gson();
//        String message = gson.toJson(request);
//
//        String eventName = "sms-operator-provisioning";
//        PortletApplicationContext2 ctx = (PortletApplicationContext2) application.getContext();
//        ctx.sendPortletEvent(application.getMainWindow(), new QName(
//                "http://vaadin.com/liferay", eventName), message);
//        logger.debug("Portlet event [{}] , type [{}] sent", message, eventName);
//
//
//    }
}
