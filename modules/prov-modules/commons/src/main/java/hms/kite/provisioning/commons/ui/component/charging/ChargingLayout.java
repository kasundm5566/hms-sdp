/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.ui.component.charging;

import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.DataComponent;
import hms.kite.provisioning.commons.ui.component.charging.field.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.addon.customfield.CustomField;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ChargingLayout extends CustomField implements ChargingValueChangeListener, DataComponent {

    private static final Logger logger = LoggerFactory.getLogger(ChargingLayout.class);

    private static final String NO_OPERATOR = "";

    private final List<ChargingField> chargingFields = new ArrayList<ChargingField>();

    private Layout layout;

    private Map<String, Object> currentFieldsData;

    private boolean isAlreadyChanging = false;
    private boolean isSettingData = false;

    private ChargingLayout(Layout layout) {
        this.layout = layout;
    }

    private void addChargingField(ChargingField field) {
        chargingFields.add(field);
    }

    private void initFields() {
        for (ChargingField chargingField : chargingFields) {
            chargingField.initField(layout, this);
        }
    }

    private void setFieldsData(Map<String, Object> data) {
        currentFieldsData = data;
        isSettingData = true;

        for (ChargingField chargingField : chargingFields) {
            chargingField.setData(currentFieldsData);
        }

        isSettingData = false;
    }

    private void getFieldsData(Map<String, Object> data) {
        for (ChargingField chargingField : chargingFields) {
            chargingField.getData(data);
        }
    }

    @Override
    public void setValue(Object value) {
        setFieldsData((Map<String, Object>) value);
    }

    @Override
    public Object getValue() {
        Map<String, Object> data = new HashMap<String, Object>();
        getFieldsData(data);
        return data;
    }

    @Override
    public void validate() {
        for (ChargingField chargingField : chargingFields) {
            chargingField.validate();
        }
    }

    @Override
    public void setReadOnly(boolean newStatus) {
        for (ChargingField chargingField : chargingFields) {
            chargingField.setReadOnly(newStatus);
        }
    }

    @Override
    public Class<?> getType() {
        return VerticalLayout.class;
    }

    @Override
    public void chargingValueChange(Map<String, Object> data) {
        if (!isAlreadyChanging) {
            isAlreadyChanging = true;

            int noOfChargingFields = chargingFields.size();

            for (int i = 0; i < noOfChargingFields; i++) {
                if (!isSettingData) {
                    currentFieldsData = new HashMap<String, Object>();
                    getFieldsData(currentFieldsData);
                } else {
                    i = noOfChargingFields;
                }

                for (ChargingField chargingField : chargingFields) {
                    chargingField.chargingValueChange(currentFieldsData);
                }
            }

            logger.debug("Charging value updated to [{}]", currentFieldsData);

            isAlreadyChanging = false;
        }
    }

    @Override
    public void setData(Map<String, Object> data) {
        setFieldsData(data);
    }

    @Override
    public void getData(Map<String, Object> data) {
        getFieldsData(data);
    }

    public void setPermissions() {
        for (ChargingField chargingField : chargingFields) {
            chargingField.setPermissions();
        }
    }

    public void reset() {
        for (ChargingField chargingField : chargingFields) {
            chargingField.reset();
        }
    }

    public static ChargingLayout downloadableChargingLayout(Layout layout, BaseApplication application,
                                                            String corporateUserId, String permissionRolePrefix,
                                                            String width, int rows, int columns) {
        ChargingLayout chargingLayout = new ChargingLayout(layout);
        chargingLayout.addChargingField(new ChargingType.Builder(application, permissionRolePrefix, width).
                free().flat().build());
        chargingLayout.addChargingField(new ChargingParty.Builder(application, permissionRolePrefix, width).build());
        chargingLayout.addChargingField(new AllowedPaymentInstruments.Builder(corporateUserId, application,
                permissionRolePrefix, width, rows, columns).otherPaymentInstrument().mobileAccount().build());
        chargingLayout.addChargingField(new ChargingAmount.Builder(NO_OPERATOR, downloadableK, application,
                permissionRolePrefix, width).build());
        chargingLayout.initFields();

        return chargingLayout;
    }

    public static ChargingLayout caasChargingLayout(Layout layout, BaseApplication application,
                                                    String corporateUserId, String permissionRolePrefix,
                                                    String width, int rows, int columns) {
        ChargingLayout chargingLayout = new ChargingLayout(layout);
        chargingLayout.addChargingField(new ChargingParty.Builder(application, permissionRolePrefix, width).build());
        chargingLayout.addChargingField(new AllowedPaymentInstruments.Builder(corporateUserId, application,
                permissionRolePrefix, width, rows, columns).otherPaymentInstrument().mobileAccount().build());
        chargingLayout.initFields();

        return chargingLayout;
    }

    public static ChargingLayout subscriptionChargingLayout(Layout layout, BaseApplication application,
                                                            String corporateUserId, String permissionRolePrefix,
                                                            String width, int rows, int columns) {
        ChargingLayout chargingLayout = new ChargingLayout(layout);
        chargingLayout.addChargingField(new ChargingType.Builder(application, permissionRolePrefix, width).
                free().flat().build());
        chargingLayout.addChargingField(new ChargingParty.Builder(application, permissionRolePrefix, width).build());
        chargingLayout.addChargingField(new ChargingFrequency.Builder(application, permissionRolePrefix, width).
                daily().weekly().halfMonthly().monthly().build());
        chargingLayout.addChargingField(new AllowedPaymentInstruments.Builder(corporateUserId, application,
                permissionRolePrefix, width, rows, columns).mobileAccount().build());
        chargingLayout.addChargingField(new ChargingAmount.Builder(NO_OPERATOR, subscriptionK, application,
                permissionRolePrefix, width).serviceCodeCharging().build());
        chargingLayout.initFields();

        return chargingLayout;
    }

    public static ChargingLayout smsMoChargingLayout(Layout layout, String operator, BaseApplication application,
                                                     String corporateUserId, String permissionRolePrefix,
                                                     String width, int rows, int columns) {
        ChargingLayout chargingLayout = new ChargingLayout(layout);
        chargingLayout.addChargingField(new ChargingType.Builder(application, permissionRolePrefix, width).
                free().flat().build());
        chargingLayout.addChargingField(new ChargingParty.Builder(application, permissionRolePrefix, width).
                subscriber().build());
        chargingLayout.addChargingField(new AllowedPaymentInstruments.Builder(corporateUserId, application,
                permissionRolePrefix, width, rows, columns).mobileAccount().build());
        chargingLayout.addChargingField(new ChargingAmount.Builder(operator, smsK, application,
                permissionRolePrefix, width).serviceCodeCharging().build());
        chargingLayout.initFields();

        return chargingLayout;
    }

    public static ChargingLayout smsMtChargingLayout(Layout layout, String operator, BaseApplication application,
                                                     String corporateUserId, String permissionRolePrefix,
                                                     String width, int rows, int columns) {
        ChargingLayout chargingLayout = new ChargingLayout(layout);
        chargingLayout.addChargingField(new ChargingType.Builder(application, permissionRolePrefix, width).
                free().flat().variable().build());
        chargingLayout.addChargingField(new ChargingParty.Builder(application, permissionRolePrefix, width).
                subscriber().build());
        chargingLayout.addChargingField(new AllowedPaymentInstruments.Builder(corporateUserId, application,
                permissionRolePrefix, width, rows, columns).mobileAccount().build());
        chargingLayout.addChargingField(new ChargingAmount.Builder(operator, smsK, application,
                permissionRolePrefix, width).serviceCodeCharging().build());
        chargingLayout.initFields();

        return chargingLayout;
    }

    public static ChargingLayout ussdMoChargingLayout(Layout layout, String operator, BaseApplication application,
                                                      String corporateUserId, String permissionRolePrefix,
                                                      String width, int rows, int columns) {
        ChargingLayout chargingLayout = new ChargingLayout(layout);
        chargingLayout.addChargingField(new ChargingType.Builder(application, permissionRolePrefix, width).
                flat().build());
        chargingLayout.addChargingField(new ChargingParty.Builder(application, permissionRolePrefix, width).
                subscriber().build());
        chargingLayout.addChargingField(new AllowedPaymentInstruments.Builder(corporateUserId, application,
                permissionRolePrefix, width, rows, columns).mobileAccount().build());
        chargingLayout.addChargingField(new ChargingAmount.Builder(operator, ussdK, application,
                permissionRolePrefix, width).serviceCodeCharging().build());
        chargingLayout.initFields();

        return chargingLayout;
    }

    public static ChargingLayout ussdStandardMoChargingLayout(Layout layout, String operator, BaseApplication application,
                                                      String corporateUserId, String permissionRolePrefix,
                                                      String width, int rows, int columns) {
        ChargingLayout chargingLayout = new ChargingLayout(layout);
        chargingLayout.addChargingField(new ChargingType.Builder(application, permissionRolePrefix, width).
                free().flat().build());
        chargingLayout.addChargingField(new ChargingParty.Builder(application, permissionRolePrefix, width).
                subscriber().build());
        chargingLayout.addChargingField(new AllowedPaymentInstruments.Builder(corporateUserId, application,
                permissionRolePrefix, width, rows, columns).mobileAccount().build());
        chargingLayout.addChargingField(new ChargingAmount.Builder(operator, ussdK, application,
                permissionRolePrefix, width).serviceCodeCharging().build());
        chargingLayout.initFields();

        return chargingLayout;
    }

    public static ChargingLayout ussdMtChargingLayout(Layout layout, String operator, BaseApplication application,
                                                      String corporateUserId, String permissionRolePrefix,
                                                      String width, int rows, int columns) {
        ChargingLayout chargingLayout = new ChargingLayout(layout);
        chargingLayout.addChargingField(new ChargingType.Builder(application, permissionRolePrefix, width).
                flat().variable().build());
        chargingLayout.addChargingField(new ChargingParty.Builder(application, permissionRolePrefix, width).
                subscriber().build());
        chargingLayout.addChargingField(new AllowedPaymentInstruments.Builder(corporateUserId, application,
                permissionRolePrefix, width, rows, columns).mobileAccount().build());
        chargingLayout.addChargingField(new ChargingAmount.Builder(operator, ussdK, application,
                permissionRolePrefix, width).serviceCodeCharging().build());
        chargingLayout.initFields();

        return chargingLayout;
    }

    public static ChargingLayout ussdSessionChargingLayout(Layout layout, String operator, BaseApplication application,
                                                           String corporateUserId, String permissionRolePrefix,
                                                           String width, int rows, int columns) {
        ChargingLayout chargingLayout = new ChargingLayout(layout);
        chargingLayout.addChargingField(new ChargingType.Builder(application, permissionRolePrefix, width).
                flat().build());
        chargingLayout.addChargingField(new ChargingParty.Builder(application, permissionRolePrefix, width).
                subscriber().build());
        chargingLayout.addChargingField(new AllowedPaymentInstruments.Builder(corporateUserId, application,
                permissionRolePrefix, width, rows, columns).mobileAccount().build());
        chargingLayout.addChargingField(new ChargingAmount.Builder(operator, ussdK, application,
                permissionRolePrefix, width).serviceCodeCharging().build());
        chargingLayout.initFields();

        return chargingLayout;
    }

    public static ChargingLayout wapPushMtChargingLayout(Layout layout, String operator, BaseApplication application,
                                                         String corporateUserId, String permissionRolePrefix,
                                                         String width, int rows, int columns) {
        ChargingLayout chargingLayout = new ChargingLayout(layout);
        chargingLayout.addChargingField(new ChargingType.Builder(application, permissionRolePrefix, width).
                free().flat().variable().build());
        chargingLayout.addChargingField(new ChargingParty.Builder(application, permissionRolePrefix, width).
                subscriber().build());
        chargingLayout.addChargingField(new AllowedPaymentInstruments.Builder(corporateUserId, application,
                permissionRolePrefix, width, rows, columns).mobileAccount().build());
        chargingLayout.addChargingField(new ChargingAmount.Builder(operator, wapPushK, application,
                permissionRolePrefix, width).serviceCodeCharging().build());
        chargingLayout.initFields();

        return chargingLayout;
    }
}
