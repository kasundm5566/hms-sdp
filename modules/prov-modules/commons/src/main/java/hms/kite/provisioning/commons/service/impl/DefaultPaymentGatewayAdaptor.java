/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.service.impl;

import hms.kite.provisioning.commons.service.PaymentGatewayAdaptor;
import hms.kite.util.SdpException;
import hms.pgw.api.common.PayInstrumentInfo;
import hms.pgw.api.common.PayInstrumentQueryType;
import hms.pgw.api.request.PayInstrumentQueryMessage;
import hms.pgw.api.response.PayInstrumentResponse;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import static hms.commons.SnmpLogUtil.clearTrap;
import static hms.commons.SnmpLogUtil.trap;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.util.KiteErrorBox.systemErrorCode;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class DefaultPaymentGatewayAdaptor implements PaymentGatewayAdaptor {

    private static final Logger logger = LoggerFactory.getLogger(DefaultPaymentGatewayAdaptor.class);

    private String instrumentQueryApiUrl;
    private String instrumentQueryFailed;
    private String successKey;
    private String successMessage;
    private String failedKey;
    private String failedMessage;

    private PayInstrumentResponse readResponse(InputStream is) {
        JAXBContext ctx;
        PayInstrumentResponse response;
        try {
            ctx = JAXBContext.newInstance(PayInstrumentResponse.class);
            response = (PayInstrumentResponse) ctx.createUnmarshaller().unmarshal(new InputStreamReader(is));
        } catch (Exception e) {
            throw new SdpException(systemErrorCode, e);
        }
        return response;
    }

    private void addInstrument(PayInstrumentInfo instrument, Map<String, Map<String, Object>> result) {
        Long payInstrumentId = instrument.getPayInstrumentId();

        Map<String, Object> paymentInstrument = result.get(payInstrumentId.toString());

        if (paymentInstrument == null) {
            paymentInstrument = new HashMap<String, Object>();
            paymentInstrument.put(paymentInstrumentIdK, Long.toString(instrument.getPayInstrumentId()));
            paymentInstrument.put(nameK, instrument.getPayInstrumentName());
            paymentInstrument.put(accountsK, new HashMap<String, Map<String, String>>());
            String key = payInstrumentId  + ( instrument.getPayInsAccountId() != null ? ("/" + instrument.getPayInsAccountId()) : "");
            result.put(key, paymentInstrument);
        }

        Map<String, Map<String, String>> accounts = (Map<String, Map<String, String>>) paymentInstrument.get(accountsK);

        if (accounts.get(instrument.getPayInsAccountId()) == null) {
            Map<String, String> accountDetails = new HashMap<String, String>();
            accountDetails.put(accountIdK, instrument.getPayInsAccountId());
            accountDetails.put(currencyK, instrument.getAccountCurrecy());
            accountDetails.put(defaultK, Boolean.toString(instrument.isDefault()));

            accounts.put(instrument.getPayInsAccountId(), accountDetails);
        }
    }

    @Override
    public Map<String, Map<String, Object>> paymentInstruments(String corporateUserId) throws SdpException {
        Map<String, Map<String, Object>> result;

        try {
            logger.debug("Query the payment gateway with url [{}] to find payment instruments for [{}]", instrumentQueryApiUrl, corporateUserId);

            WebClient webClient = WebClient.create(instrumentQueryApiUrl);
            webClient.header("Content-Type", "application/xml");
            webClient.accept("application/xml");

            PayInstrumentQueryMessage msg = new PayInstrumentQueryMessage();
            msg.setUserId(corporateUserId);
            msg.setQueryType(PayInstrumentQueryType.ALL);
            logger.debug("Sending QueryPI request[{}]", msg);
            Response response = webClient.post(msg);
            PayInstrumentResponse payInstrumentResponse = readResponse((InputStream) response.getEntity());

            result = new HashMap<String, Map<String, Object>>();

            PayInstrumentInfo[] payInstrumentInfoList = payInstrumentResponse.getPayInstrumentInfos();

            if (payInstrumentInfoList != null) {
                for (PayInstrumentInfo inst : payInstrumentInfoList) {
                    addInstrument(inst, result);
                }
            }

            logger.debug("Response from payment-gateway instruments for user [{}] is [{}]", corporateUserId, result);

            clearTrap(successKey, successMessage);

            return result;
        } catch (Exception e) {
            trap(failedKey, failedMessage);

            logger.error("Error while getting payment instruments {}", e);
            throw new SdpException(systemErrorCode, "Error while getting payment instruments", e);
        }
    }

    public void setInstrumentQueryApiUrl(String instrumentQueryApiUrl) {
        this.instrumentQueryApiUrl = instrumentQueryApiUrl;
    }

    public void setInstrumentQueryFailed(String instrumentQueryFailed) {
        this.instrumentQueryFailed = instrumentQueryFailed;
    }

    public void setSuccessKey(String successKey) {
        this.successKey = successKey;
    }

    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }

    public void setFailedKey(String failedKey) {
        this.failedKey = failedKey;
    }

    public void setFailedMessage(String failedMessage) {
        this.failedMessage = failedMessage;
    }
}
