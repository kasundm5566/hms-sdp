/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.commons.ui.component.charging.field;

import com.vaadin.ui.Layout;
import com.vaadin.ui.Select;
import hms.kite.provisioning.commons.ui.BaseApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashMap;
import java.util.Map;

import static hms.kite.provisioning.commons.util.ProvChargingConfiguration.*;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ChargingFrequency extends ChargingField {

    private static final Logger logger = LoggerFactory.getLogger(ChargingFrequency.class);

    private final Map<String, Object> chargingFrequencies;
    private final String width;

    private Select chargingFrequencySelect;

    private String defaultChargingFrequency;

    private String currentChargingType;

    private boolean isChargingType(String chargingType) {
        return chargingType.equals(currentChargingType);
    }

    private boolean isNoChargingFrequencyAllowed() {
        return chargingFrequencies.isEmpty();
    }

    private void initPaymentInstrumentList() {
        chargingFrequencySelect = getFieldWithPermission(CHARGING_FREQUENCY, new Select());
        if (isFieldNotNull(chargingFrequencySelect)) {
            chargingFrequencySelect.setCaption(application.getMsg("common.charging.frequency.caption"));
            chargingFrequencySelect.setDescription(application.getMsg("common.charging.frequency.tooltip"));
            chargingFrequencySelect.setRequired(true);
            chargingFrequencySelect.setRequiredError(application.getMsg("common.charging.frequency.required.error"));
            chargingFrequencySelect.setImmediate(true);
            chargingFrequencySelect.setWidth(width);
            chargingFrequencySelect.setNullSelectionAllowed(false);
        }
    }

    private void populateChargingFrequencies() {
        for (Map.Entry<String, Object> entry : chargingFrequencies.entrySet()) {
            chargingFrequencySelect.addItem(entry.getKey());
            chargingFrequencySelect.setItemCaption(entry.getKey(), (String) entry.getValue());
        }
    }

    private ChargingFrequency(Map<String, Object> chargingFrequencies, BaseApplication application, String moduleRolePrefix, String width) {
        super(application, moduleRolePrefix);

        this.chargingFrequencies = chargingFrequencies;
        this.width = width;

        initPaymentInstrumentList();
    }

    @Override
    public void init(Layout layout) {
        if (!isFieldNotNull(chargingFrequencySelect) || isNoChargingFrequencyAllowed()) {
            defaultChargingFrequency = getDefaultChargingFrequency();
            return;
        }

        populateChargingFrequencies();

        chargingFrequencySelect.addListener(getValueChangeListener());
        chargingFrequencySelect.select(getDefaultChargingFrequency());
        layout.addComponent(chargingFrequencySelect);
    }

    @Override
    public void chargingValueChange(Map<String, Object> data) {
        currentChargingType = (String) data.get(typeK);

        if (!isFieldNotNull(chargingFrequencySelect) || isNoChargingFrequencyAllowed()) {
            return;
        }

        if (isChargingType(freeK)) {
            hideField(chargingFrequencySelect);
        } else {
            showField(chargingFrequencySelect);
        }
    }

    @Override
    public void setData(Map<String, Object> data) {
        if (!isChargingType(freeK)) {
            if (!isFieldNotNull(chargingFrequencySelect) || isNoChargingFrequencyAllowed()) {
                defaultChargingFrequency = getDefaultChargingFrequency();
            } else {
                setValueToField(chargingFrequencySelect, data.get(frequencyK));
            }
        }
    }

    @Override
    public void getData(Map<String, Object> data) {
        if (!isChargingType(freeK)) {
            if (defaultChargingFrequency != null && isNoChargingFrequencyAllowed()) {
                data.put(frequencyK, defaultChargingFrequency);
            } else {
                getValueFromField(chargingFrequencySelect, data, frequencyK);
            }
        }
    }

    @Override
    public void validate() {
        if (!isChargingType(freeK) && !isNoChargingFrequencyAllowed()) {
            validateField(chargingFrequencySelect);
        }
    }

    @Override
    public void reset() {
    }

    @Override
    public void setPermissions() {
        setReadWritePermissionToComponent(chargingFrequencySelect);
    }

    public static class Builder {

        private BaseApplication application;
        private String permissionRolePrefix;
        private Map<String, Object> chargingFrequencies;
        private String width;

        private Builder addChargingFrequency(boolean isChargingTypeAllow, String type, String caption) {
            if (isChargingTypeAllow) {
                chargingFrequencies.put(type, caption);
            }
            return this;
        }

        public Builder(BaseApplication application, String permissionRolePrefix, String width) {
            this.application = application;
            this.permissionRolePrefix = permissionRolePrefix;
            this.chargingFrequencies = new LinkedHashMap<String, Object>();
            this.width = width;
        }

        public Builder daily() {
            return addChargingFrequency(isDailyChargingFrequencyAllow(), dailyK,
                    application.getMsg("common.charging.frequency.daily.caption"));
        }

        public Builder weekly() {
            return addChargingFrequency(isWeeklyChargingFrequencyAllow(), weeklyK,
                    application.getMsg("common.charging.frequency.weekly.caption"));
        }

        public Builder halfMonthly() {
            return addChargingFrequency(isHalfMonthlyChargingFrequencyAllow(), halfMonthlyK,
                    application.getMsg("common.charging.frequency.half.monthly.caption"));
        }

        public Builder monthly() {
            return addChargingFrequency(isMonthlyChargingFrequencyAllow(), monthlyK,
                    application.getMsg("common.charging.frequency.monthly.caption"));
        }

        public ChargingFrequency build() {
            return new ChargingFrequency(chargingFrequencies, application, permissionRolePrefix, width);
        }
    }
}
