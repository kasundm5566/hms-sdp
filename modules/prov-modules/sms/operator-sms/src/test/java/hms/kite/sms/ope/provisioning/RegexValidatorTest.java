/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sms.ope.provisioning;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class RegexValidatorTest {

    @Test
    public void testDefaultSenderAddress() {
        List<String> successStrings = new ArrayList<String>();
        successStrings.add("test");
        successStrings.add("t");
        successStrings.add("t-t");
        successStrings.add("t9t");
        successStrings.add("7744");
        successStrings.add("1321231233");
        successStrings.add("testtesttt");

        for (String successString : successStrings) {
            assert successString.matches("^[A-Za-z0-9-]{1,10}$") : "validation filed for string [" + successString + "]";
        }

        List<String> failureStrings = new ArrayList<String>();
        failureStrings.add("");
        failureStrings.add("qwerrqewrrr");
        failureStrings.add("_");
        failureStrings.add("testtes ttt");

        for (String failedString : failureStrings) {
            assert !failedString.matches("^[A-Za-z0-9-]{1,10}$") : "validation filed for string [" + failedString + "]";
        }
    }

    @Test
    public void testAlias() {

        assert ("15456".matches("([0-9]{0,5})?(100000)?"));
//        assert "this".matches("^(([A-Za-z0-9-]{1,10})(,)*){1,10}$");
//        assert !("this,this,this,this,this," +
//                "this,this,this,this,this,this,").matches("^(([A-Za-z0-9-]{1,10})(,)*){1,10}$");
    }
}
