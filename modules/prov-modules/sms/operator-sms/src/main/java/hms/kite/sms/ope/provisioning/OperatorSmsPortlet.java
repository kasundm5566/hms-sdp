/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.sms.ope.provisioning;

import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.terminal.gwt.server.PortletApplicationContext2;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import hms.kite.datarepo.RepositoryServiceRegistry;
import hms.kite.datarepo.audit.AuditTrail;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.UiManagerImpl;
import hms.kite.provisioning.commons.util.ProvKeyBox;
import hms.kite.sms.ope.provisioning.ui.SmsSlaForm;
import hms.kite.sms.ope.provisioning.ui.SmsSlaFormBodyLayout;
import hms.kite.util.SdpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.ncsRepositoryService;
import static hms.kite.provisioning.commons.ui.LiferayUtil.createPortletId;
import static hms.kite.provisioning.commons.util.ProvCommonUtil.getNcsDataFromSessionByAppId;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.sms.ope.provisioning.OperatorSmsServiceRegistry.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class OperatorSmsPortlet extends BaseApplication {

    private static final Logger logger = LoggerFactory.getLogger(OperatorSmsPortlet.class);

    private static final String PORTLET_USER_ID = "portlet_user_id";

    private ThemeDisplay themeDisplay;
    private Panel mainPanel;
    private Panel innerPanel;
    private SmsSlaForm form;

    private void handleCreateNcsRequest(String eventName, Map<String, Object> eventData, String corpUserId, List<Map> selectedNcses) {
        String appId = (String) eventData.get(appIdK);
        String operator = (String) eventData.get(operatorK);
        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        innerPanel.removeAllComponents();
        logger.info("Start creating ncs for app-id [{}] ncs-type [sms] operator [{}]", appId, operator);
        Map<String, Object> smsSla = RepositoryServiceRegistry.ncsRepositoryService().findNcs(appId, smsK, operator);
        form = new SmsSlaForm(eventName, this, smsSla, corpUserId, appId, selectedNcses);
        innerPanel.addComponent(form);
    }

    private void handleSaveNcsRequest(Map<String, Object> data) {
        String operator = operatorId();
        String ncsType = smsK;
        String appId = (String) data.get(appIdK);
        String status = (String) data.get(statusK);

        Map<String, Object> smsSlaFromSession = getNcsDataFromSessionByAppId(appId, smsSlaDataK, this);

        if (deleteK.equals(status)) {
            ncsRepositoryService().update(appId, ncsType, operator, status);
        } else if (smsSlaFromSession != null) {
            smsSlaFromSession.put(operatorK, operator);
            smsSlaFromSession.put(ncsTypeK, ncsType);
            smsSlaFromSession.put(appIdK, appId);
            smsSlaFromSession.put(statusK, status);
            ncsRepositoryService().createNcs(smsSlaFromSession, (String) data.get(spIdK));
        } else {
            ncsRepositoryService().update(appId, ncsType, operator, status);
        }
        removeFromSession(smsSlaDataK);
        closePortlet();
    }

    private void handleViewNcsRequest(String eventName, Map<String, Object> value, String corpUserId, List<Map> selectedNcses) {
        logger.info("Received view-ncs request for {} ", value);

        Map<String, Object> ncsActions = new HashMap<String, Object>();
        ncsActions.put(operatorK, operatorId());
        ncsActions.put(ncsTypeK, smsK);
        ncsActions.put(appIdK, value.get(appIdK));

        Map<String, Object> smsSla;
        try {
            smsSla = operatorSmsSlaService().viewOperatorSmsSla(ncsActions);
        } catch (SdpException e) {
            logger.debug("Error looking for operator sla for " + ncsActions, e);
            smsSla = new HashMap<String, Object>();
        }
        form = new SmsSlaForm(eventName, this, smsSla, corpUserId, (String) value.get(appIdK), selectedNcses);
        mainPanel.setCaption((String) value.get(ncsPortletTitle));
        innerPanel.removeAllComponents();
        innerPanel.addComponent(form);
    }

    private void showErrorNotification(String message) {
        Window.Notification notification = new Window.Notification(message, Window.Notification.TYPE_ERROR_MESSAGE);
        notification.setDelayMsec(-1);
        getMainWindow().showNotification(notification);
    }

    private String operatorPresentationName() {
        return operatorId().substring(0, 1).toUpperCase() + operatorId().substring(1);
    }

    private Long getPortletUserId() {
        return (Long) getFromSession(PORTLET_USER_ID);
    }

    private void setPortletUserId(Long userId) {
        if (getPortletUserId() == null) {
            addToSession(PORTLET_USER_ID, userId);
        }
    }

    @Override
    protected String getApplicationName() {
        return "sms-prov";
    }

    @Override
    public void init() {
        setResourceBundle(getLocale().getCountry());
        Window main = new Window(getMsg("sms.sla.window.title"));
        setMainWindow(main);
        setUiManager(new UiManagerImpl(this));
        VerticalLayout mainLayout = new VerticalLayout();
        String caption = this.getMsg("sms.sla.configuration.panel.header", operatorPresentationName());
        mainPanel = new Panel("");
        mainLayout.addComponent(mainPanel);
        mainLayout.addStyleName("container-panel");
        mainLayout.setWidth("762px");
        mainLayout.setMargin(true);
        innerPanel = new Panel(caption);
        mainPanel.addComponent(innerPanel);
        innerPanel.addStyleName("child-panel");

        setPortletContext((PortletApplicationContext2) getContext());
        getPortletContext().addPortletListener(this, this);

        getUiManager().clearAll();
        getUiManager().pushScreen(SmsSlaFormBodyLayout.class.getName(), mainLayout);
    }

    public void handleEventRequest(EventRequest request, EventResponse response, Window window) {
        super.handleEventRequest(request, response, window);

        String eventName = request.getEvent().getName();
        Map<String, Object> data = (Map<String, Object>) request.getEvent().getValue();
        try {
            AuditTrail.setCurrentUser((String) data.get(currentUsernameK), (String) data.get(currentUserTypeK));

            themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
            Long userId = themeDisplay.getUserId();

            setPortletUserId(userId);

            String corpUserId = (String) data.get(coopUserIdK);

            logger.info("Received event - {}, with data {}", eventName, data);

            if (!smsK.equals(data.get(ProvKeyBox.ncsTypeK))) {
                logger.debug("Received [{}] event is not a [{}] NCS type", eventName, smsK);
                return;
            }

            if (!operatorId().equals(data.get(operatorK))) {
                logger.debug("Received [{}] event is not for me({}-sms). It is for [{}-sms]",
                        new Object[]{eventName, operatorId(), data.get(operatorK)});
                return;
            }

            List<Map> selectedNcses = (List<Map>) data.get(ncsesK);

            if (createNcsK.equals(eventName)) {
                handleCreateNcsRequest(eventName, data, corpUserId, selectedNcses);
            } else if (saveNcsK.equals(eventName)) {
                handleSaveNcsRequest(data);
            } else if (viewNcsK.equals(eventName)) {
                handleViewNcsRequest(eventName, data, corpUserId, selectedNcses);
            } else if (reconfigureNcsK.equals(eventName)) {
                handleCreateNcsRequest(eventName, data, corpUserId, selectedNcses);
            } else {
                logger.warn("Unexpected event {} i think", eventName);
            }
        } catch (SdpException e) {
            logger.error("Sdp exception occurred while processing event [" + request.getEvent().getValue(), e);
            String message = getMsg(errorCodeResolver().findMessageKey(e), operatorId(), (String) data.get(appIdK));
            showErrorNotification(message);
            closePortlet();
        } catch (Exception e) {
            logger.error("Unexpected error occurred while processing event [" + request.getEvent().getValue() + "]", e);
            String message = getMsg("default.system.error");
            showErrorNotification(message);
            closePortlet();
        }
    }

    public ThemeDisplay getThemeDisplay() {
        return themeDisplay;
    }

    public void closePortlet() {
        themeDisplay.getLayoutTypePortlet().removePortletId(getPortletUserId(), createPortletId(smsK, operatorId()));
    }
}