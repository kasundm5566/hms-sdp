/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.sms.ope.provisioning.ui;

import com.vaadin.Application;
import com.vaadin.data.Validator;
import com.vaadin.ui.*;
import hms.kite.provisioning.commons.ui.event.PortletEventSender;
import hms.kite.provisioning.commons.util.ProvKeyBox;
import hms.kite.sms.ope.provisioning.OperatorSmsPortlet;
import hms.kite.sms.ope.provisioning.OperatorSmsServiceRegistry;
import hms.kite.util.KiteKeyBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.provisioning.commons.util.ProvCommonUtil.getNcsDataFromSessionByAppId;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class SmsSlaForm extends Form {

    private static final Logger logger = LoggerFactory.getLogger(SmsSlaForm.class);

    private final SmsSlaFormBodyLayout smsSlaFormBodyLayout;

    private OperatorSmsPortlet application;

    private Button submitButton;
    private Button backButton;
    private Button confirmButton;

    private VerticalLayout buttonLayout;
    private State currentState = State.INITIAL;

    private void createButtons() {
        buttonLayout = new VerticalLayout();
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        buttonLayout.addComponent(horizontalLayout);
        buttonLayout.setComponentAlignment(horizontalLayout, Alignment.MIDDLE_CENTER);

        submitButton = new Button(application.getMsg("sms.sla.create.submit"));
        backButton = new Button(application.getMsg("sms.sla.create.back"));
        confirmButton = new Button(application.getMsg("sms.sla.create.confirm"));

        backButton.setImmediate(true);
        submitButton.setImmediate(true);

        horizontalLayout.setSpacing(true);
        horizontalLayout.addComponent(backButton);
        horizontalLayout.addComponent(submitButton);
        horizontalLayout.addComponent(confirmButton);
    }

    private void addButtonListeners(final OperatorSmsPortlet application, final Map<String, Object> currentData, final String appId) {
        final SmsSlaForm form = this;

        submitButton.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                try {
                    currentState.onSubmit(event, form, currentData);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
        });

        backButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                currentState.onBack(event, application, form);
            }
        });
        confirmButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                currentData.put(appIdK, appId);
                currentState.onConfirm(event, application, currentData);
            }
        });
    }

    private enum State {
        INITIAL {
            @Override
            void onSubmit(Button.ClickEvent event, SmsSlaForm form, Map<String, Object> currentData) {
                try {
                    form.setComponentError(null);
                    form.smsSlaFormBodyLayout.validate();
                    form.smsSlaFormBodyLayout.getData(currentData);
                    form.smsSlaFormBodyLayout.readOnly(true);
                    form.setButtonVisibility(true, false, true);
                    form.currentState = State.CONFIRM;
                } catch (Validator.InvalidValueException ex) {
                    logger.info("Validator Error occurred: " + ex);
                    form.setComponentError(ex);
                } catch (Exception e) {
                    logger.error("Unexpected error occurred", e);
                    form.showErrorNotification(form.application, form.application.getMsg("default.system.error"));
                }
            }
        }, VIEW {

        }, RECONFIGURE {
            @Override
            void onSubmit(Button.ClickEvent event, SmsSlaForm form, Map<String, Object> currentData) {
                State.INITIAL.onSubmit(event, form, currentData);
            }
        }, CONFIRM {
            @Override
            void onConfirm(Button.ClickEvent event, OperatorSmsPortlet application, Map<String, Object> currentData) {
                logger.info("NCS Configuration is prepared to save: [{}]", currentData);
                application.addToSession(smsSlaDataK, currentData);
                Map<String, Object> dataMap = new HashMap<String, Object>();
                dataMap.put(KiteKeyBox.ncsTypeK, KiteKeyBox.smsK);
                dataMap.put(KiteKeyBox.statusK, KiteKeyBox.successK);
                String operator = OperatorSmsServiceRegistry.operatorId();
                dataMap.put(KiteKeyBox.operatorK, operator);
                PortletEventSender.send(dataMap, ProvKeyBox.ncsConfiguredK, application);
                application.closePortlet();
            }

            @Override
            void onBack(Button.ClickEvent event, OperatorSmsPortlet application, SmsSlaForm form) {
                form.smsSlaFormBodyLayout.readOnly(false);
                form.setButtonVisibility(true, true, false);
                form.currentState = State.INITIAL;
            }
        };

        void onBack(Button.ClickEvent event, OperatorSmsPortlet application, SmsSlaForm form) {
            PortletEventSender.send(new HashMap<String, Object>(), provRefreshEvent, application);
            application.closePortlet();
        }

        void onSubmit(Button.ClickEvent event, SmsSlaForm form, Map<String, Object> currentData) {
            throw new IllegalStateException("Unexpected submit event at state [" + this + "]");
        }

        void onConfirm(Button.ClickEvent event, OperatorSmsPortlet application, Map<String, Object> currentData) {
            throw new IllegalStateException("Unexpected confirm event at state [" + this + "]");
        }
    }

    private void setButtonVisibility(boolean backVisibility, boolean submitVisibility, boolean confirmVisibility) {
        backButton.setVisible(backVisibility);
        submitButton.setVisible(submitVisibility);
        confirmButton.setVisible(confirmVisibility);
    }

    private void showErrorNotification(Application application, String message) {
        showNotification(application, message, Window.Notification.TYPE_ERROR_MESSAGE);
    }

    private void showWarningNotification(Application application, String message) {
        showNotification(application, message, Window.Notification.TYPE_WARNING_MESSAGE);
    }

    private void showHumanizedNotification(Application application, String message) {
        showNotification(application, message, Window.Notification.TYPE_HUMANIZED_MESSAGE);
    }

    private void showTrayNotification(Application application, String message) {
        showNotification(application, message, Window.Notification.TYPE_TRAY_NOTIFICATION);
    }

    private void showNotification(Application application, String message, int type) {
        application.getMainWindow().showNotification(message, type);
    }

    public SmsSlaForm(final String eventName,
                      final OperatorSmsPortlet application,
                      final Map<String, Object> smsSla,
                      final String corporateUserId,
                      final String appId,
                      List<Map> selectedNcses) {

        this.application = application;

        smsSlaFormBodyLayout = new SmsSlaFormBodyLayout(application, corporateUserId, appId, selectedNcses);
        smsSlaFormBodyLayout.setWidth("90%");

        setLayout(new VerticalLayout());
        getLayout().addComponent(smsSlaFormBodyLayout);
        ((VerticalLayout) getLayout()).setComponentAlignment(smsSlaFormBodyLayout, Alignment.MIDDLE_CENTER);
        createButtons();

        final Map<String, Object> currentSla;

        if (reconfigureNcsK.equals(eventName)) {
            currentState = State.RECONFIGURE;
            Map<String, Object> sessionSla = getNcsDataFromSessionByAppId(appId, smsSlaDataK, application);
            currentSla = (sessionSla == null ? smsSla : sessionSla);
            smsSlaFormBodyLayout.setData(currentSla);
            setButtonVisibility(true, true, false);
        } else if (viewNcsK.equals(eventName)) {
            currentState = State.VIEW;
            currentSla = smsSla;
            smsSlaFormBodyLayout.setData(currentSla);
            smsSlaFormBodyLayout.readOnly(true);
            setButtonVisibility(true, false, false);
        } else {
            currentSla = new HashMap<String, Object>();
            setButtonVisibility(true, true, false);
        }

        addButtonListeners(application, currentSla, appId);
        setFooter(buttonLayout);
    }
}
