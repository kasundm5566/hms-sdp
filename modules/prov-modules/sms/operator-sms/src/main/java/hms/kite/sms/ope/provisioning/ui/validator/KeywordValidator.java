/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.sms.ope.provisioning.ui.validator;

import com.vaadin.data.Validator;
import com.vaadin.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static hms.kite.datarepo.RepositoryServiceRegistry.smsRoutingKeyRepositoryService;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class KeywordValidator implements Validator {

    private String spId;
    private String operatorName;
    private Select shortcodeSelect;
    private String errorMessage;
    private String appId;

    private static final Logger logger = LoggerFactory.getLogger(KeywordValidator.class);

    public KeywordValidator(String spId, String appId, String operatorName, Select shortcodeSelect, String errorMessage) {
        this.spId = spId;
        this.operatorName = operatorName;
        this.shortcodeSelect = shortcodeSelect;
        this.errorMessage = errorMessage;
        this.appId = appId;
    }

    @Override
    public void validate(Object value) throws InvalidValueException {
        String keyword = ((String) value).toLowerCase();
        String shortCode = (String) shortcodeSelect.getValue();

        if (!isValid(value)) {
            logger.info("keyword {} for shortcode {} of operator {} already taken.",
                    new Object[]{keyword, shortCode, operatorName});

            throw new InvalidValueException(errorMessage);
        }
    }

    @Override
    public boolean isValid(Object value) {
        Boolean available = false;

        String keyword = ((String) value).toLowerCase();
        String shortCode = (String) shortcodeSelect.getValue();

        boolean keywordEntered = keyword != null && keyword.trim().length() != 0;
        boolean shortcodeSelected = shortCode != null && shortCode.trim().length() != 0;

        if (keywordEntered && shortcodeSelected) {
            available = smsRoutingKeyRepositoryService().isRoutingKeyAvailableForApp(spId, appId, operatorName, shortCode, keyword);

            logger.debug("RoutingKey {}/{}/{} available for app {}/{} = {} ",
                    new Object[]{operatorName, shortCode, keyword, spId, appId, available});
        }
        return available;
    }
}
