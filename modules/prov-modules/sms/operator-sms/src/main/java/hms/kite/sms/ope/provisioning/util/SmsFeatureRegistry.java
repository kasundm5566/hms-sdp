package hms.kite.sms.ope.provisioning.util;


import hms.kite.util.NullObject;

import java.util.Properties;

public class SmsFeatureRegistry {

    private static Properties featureProperties;

    static {
        setFeatureProperties(new NullObject<Properties>().get());
    }

    private static void setFeatureProperties(Properties properties) {
        SmsFeatureRegistry.featureProperties = properties;
    }

    public static boolean getSmsMoDefaultActive() {
        return Boolean.valueOf(featureProperties.getProperty("prov.sms.mo.default.active"));
    }

    public static boolean getSmsMtDefaultActive() {
        return Boolean.valueOf(featureProperties.getProperty("prov.sms.mt.default.active"));
    }

    public static boolean isHelpButtonLinkEnable() {
        return Boolean.parseBoolean(featureProperties.getProperty("sms.help.link.enable"));
    }

}
