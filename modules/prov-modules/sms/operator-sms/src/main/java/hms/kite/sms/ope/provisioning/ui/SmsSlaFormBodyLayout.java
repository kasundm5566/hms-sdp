/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.sms.ope.provisioning.ui;

import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.ui.*;
import hms.kite.provisioning.commons.ui.AbstractBasicDetailsLayout;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.component.popup.HelpDetails;
import hms.kite.provisioning.commons.ui.validator.SubscriptionRequiredValidator;
import hms.kite.sms.ope.provisioning.util.SmsSlaRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.provisioning.commons.ui.VaadinUtil.cleanErrorComponents;
import static hms.kite.provisioning.commons.ui.VaadinUtil.setAllReadonly;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.provisioning.commons.util.ProvKeyBox.spIdK;
import static hms.kite.sms.ope.provisioning.util.SmsFeatureRegistry.getSmsMoDefaultActive;
import static hms.kite.sms.ope.provisioning.util.SmsFeatureRegistry.getSmsMtDefaultActive;
import static hms.kite.sms.ope.provisioning.util.SmsSlaRoles.SUBSCRIPTION_REQUIRED;
import static hms.kite.util.KiteKeyBox.moAllowedK;
import static hms.kite.util.KiteKeyBox.moK;
import static hms.kite.util.KiteKeyBox.mtAllowedK;
import static hms.kite.util.KiteKeyBox.mtK;
import static hms.kite.util.KiteKeyBox.subscriptionRequiredK;
import static hms.kite.sms.ope.provisioning.util.SmsFeatureRegistry.*;


public class SmsSlaFormBodyLayout extends AbstractBasicDetailsLayout {

    private static final Logger logger = LoggerFactory.getLogger(SmsSlaFormBodyLayout.class);
    private static final String HELP_IMAGE = "help.jpg";
    public static final String HELP_IMG_WIDTH = "880px";
    public static final String HELP_IMG_HEIGHT = "880px";
    public static final String HELP_IMG_STYLE_NAME = "link";
    public static final String HELP_BUTTON_NAME = "Help";

    private SmsMoLayout smsMoLayout;
    private SmsMtLayout smsMtLayout;

    private CheckBox moAllowedChkBox;
    private CheckBox mtAllowedChkBox;
    private CheckBox subscriptionReqChkBox;

    private String appId;
    private List<Map> selectedNcses;
    private String corporateUserId;
    private Map<String, Object> sp;

    private void init() {
        logger.debug("Initializing the Sms SLA configuration page started..");

        try {
            sp = spRepositoryService().findSpByCoopUserId(corporateUserId);
            if (sp != null) {
                logger.debug("Returned SP Details [{}]", sp.toString());

                createMainLayout();
            } else {
                logger.error("No Sp Details found with the given corporate user id [{}]", corporateUserId);
            }
        } catch (Exception e) {
            logger.error("Error occurred while searching SP details for corporate user id [{}] ", corporateUserId, e);
        }
    }

    private void createMainLayout() {
        if(isHelpButtonLinkEnable()) {
            createHelperLink();
        }
        createSmsMoView();
        createSmsMtView();
        createSubscriptionRequiredChkBox();

    }



    private void createSmsMoView() {
        moAllowedChkBox = new CheckBox(application.getMsg("sms.sla.create.mo.layout"));
        moAllowedChkBox.setImmediate(true);
        moAllowedChkBox.addListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                Boolean value = (Boolean) event.getProperty().getValue();
                if (smsMoLayout != null) {
                    if (value) {
                        setAllReadonly(false, smsMoLayout);
                        smsMoLayout.setPermissions();
                    } else {
                        // setting readonly should come after resetting.
                        smsMoLayout.reset();
                        setAllReadonly(true, smsMoLayout);
                    }
                }
            }
        });
        moAllowedChkBox.addStyleName("highlight-label");

        smsMoLayout = new SmsMoLayout(application, corporateUserId, (String) sp.get(spIdK), appId);
        smsMoLayout.setVisible(true);
        smsMoLayout.setSizeFull();
        smsMoLayout.setWidth("92%");

        if (sp.get(smsMoK) != null && sp.get(smsMoK).equals(true)) {
            addComponent(moAllowedChkBox);
            addComponent(smsMoLayout);
            setComponentAlignment(smsMoLayout, Alignment.MIDDLE_CENTER);
            setAllReadonly(false, smsMoLayout);
        }

        moAllowedChkBox.setValue(getSmsMoDefaultActive());
        // make all fields readonly - if default mo not allowed
        if(!getSmsMoDefaultActive()) {
            setAllReadonly(true, smsMoLayout);
        }
    }

    private void createSmsMtView() {
        mtAllowedChkBox = new CheckBox(application.getMsg("sms.sla.create.mt.layout"));
        mtAllowedChkBox.setImmediate(true);
        mtAllowedChkBox.addListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                //isSmsMtDefaultVlueChanged = true;
                Boolean value = (Boolean) event.getProperty().getValue();
                if (smsMtLayout != null) {
                    if (value) {
                        setAllReadonly(false, smsMtLayout);
                        smsMtLayout.setPermissions();
                    } else {
                        // setting readonly should come after resetting.
                        smsMtLayout.reset();
                        setAllReadonly(true, smsMtLayout);
                    }
                }
            }
        });
        mtAllowedChkBox.addStyleName("highlight-label");

        smsMtLayout = new SmsMtLayout(application, corporateUserId);
        smsMtLayout.setVisible(true);
        smsMtLayout.setSizeFull();
        smsMtLayout.setWidth("92%");

        if (sp.get(smsMtK) != null && sp.get(smsMtK).equals(true)) {
            addComponent(mtAllowedChkBox);
            addComponent(smsMtLayout);
            setComponentAlignment(smsMtLayout, Alignment.MIDDLE_CENTER);
            setAllReadonly(false, smsMtLayout);
        }
        mtAllowedChkBox.setValue(getSmsMtDefaultActive());
        if(!getSmsMtDefaultActive()) {
            setAllReadonly(true, smsMtLayout);
        }
    }

    private void createSubscriptionRequiredChkBox() {
        subscriptionReqChkBox = getFieldWithPermission(SUBSCRIPTION_REQUIRED, new CheckBox());
        if (isFieldNotNull(subscriptionReqChkBox)) {
            subscriptionReqChkBox.setCaption(application.getMsg("sms.sla.create.subscription.required"));
            subscriptionReqChkBox.addStyleName("highlight-label");
            subscriptionReqChkBox.addValidator(new SubscriptionRequiredValidator(selectedNcses,
                    application.getMsg("sms.sla.create.subscription.required.error")));
            addComponent(subscriptionReqChkBox);
        }
    }

    private void createHelperLink() {
        Button helpButtonLink = createHelpButtonLink();
        addComponent(helpButtonLink);
        setComponentAlignment(helpButtonLink, Alignment.TOP_RIGHT);
    }

    private Button createHelpButtonLink() {
        final HelpDetails helpDetails = new HelpDetails(application, HELP_IMAGE);
        final Embedded image = helpDetails.getHelpImage();
        Button helpButtonLink = new Button(HELP_BUTTON_NAME);
        helpButtonLink.setStyleName(HELP_IMG_STYLE_NAME);
        helpButtonLink.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent clickEvent) {
                getWindow().addWindow(helpDetails.generateHelpImageSubWindow(image, HELP_IMG_WIDTH, HELP_IMG_HEIGHT));
            }
        });
        return helpButtonLink;
    }

    protected void readOnly(boolean readOnly) {
        moAllowedChkBox.setReadOnly(readOnly);
        setAllReadonly(readOnly, smsMoLayout);

        mtAllowedChkBox.setReadOnly(readOnly);
        setAllReadonly(readOnly, smsMtLayout);

        if (isFieldNotNull(subscriptionReqChkBox)) {
            subscriptionReqChkBox.setReadOnly(readOnly);
        }

        if (!readOnly) {
            smsMoLayout.setPermissions();
            smsMtLayout.setPermissions();
            setReadWritePermissionToComponent(subscriptionReqChkBox);
        }
    }

    public SmsSlaFormBodyLayout(BaseApplication application, String corporateUserId,
                                String appId, List<Map> selectedNcses) {
        super(application, SmsSlaRoles.SMS_PERMISSION_ROLE_PREFIX);

        this.corporateUserId = corporateUserId;
        this.appId = appId;
        this.selectedNcses = selectedNcses;

        init();
    }

    @Override
    public void setData(Map<String, Object> data) {
        if (data.get(moK) != null) {
            moAllowedChkBox.setValue(true);
            smsMoLayout.setData(data);
        }
        if (data.get(mtK) != null) {
            mtAllowedChkBox.setValue(true);
            smsMtLayout.setData(data);
        }
        setValueToField(subscriptionReqChkBox, data.get(subscriptionRequiredK));
    }

    @Override
    public void getData(Map<String, Object> data) {
        if (moAllowedChkBox.booleanValue()) {
            data.put(moAllowedK, true);
            smsMoLayout.getData(data);
        }
        if (mtAllowedChkBox.booleanValue()) {
            data.put(mtAllowedK, true);
            smsMtLayout.getData(data);
        }
        getValueFromField(subscriptionReqChkBox, data, subscriptionRequiredK);
    }

    @Override
    public void validate() {
        cleanErrorComponents(this);

        boolean neitherOfMoMtSlaConfigured = !mtAllowedChkBox.booleanValue() && !moAllowedChkBox.booleanValue();
        if (neitherOfMoMtSlaConfigured) {
            throw new Validator.EmptyValueException(application.getMsg("sms.mo.or.mt.have.not.selected"));
        }

        if (moAllowedChkBox.booleanValue()) smsMoLayout.validate();
        if (mtAllowedChkBox.booleanValue()) smsMtLayout.validate();

        if (isFieldNotNull(subscriptionReqChkBox)) {
            subscriptionReqChkBox.validate();
        }
    }

    @Override
    public void reset() {
        throw new UnsupportedOperationException("This function is not implemented");
    }

    @Override
    public void setPermissions() {
        throw new UnsupportedOperationException("This function is not implemented");
    }
}
