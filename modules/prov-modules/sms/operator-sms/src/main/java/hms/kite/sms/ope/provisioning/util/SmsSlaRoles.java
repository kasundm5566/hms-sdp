/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.sms.ope.provisioning.util;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class SmsSlaRoles {

    public static final String SMS_PERMISSION_ROLE_PREFIX = "ROLE_PROV_SMS";

    public static final String SHORT_CODE_SELECT = "SHORT_CODE_SELECT";
    public static final String KEYWORD = "KEYWORD";
    public static final String CONNECTION_URL = "CONNECTION_URL";
    public static final String MO_TPS = "MO_TPS";
    public static final String MO_TPD = "MO_TPD";
    public static final String MO_CHARGING = SMS_PERMISSION_ROLE_PREFIX + "_MO_CHARGING";

    public static final String DEFAULT_SENDER_ADDRESS = "DEFAULT_SENDER_ADDRESS";
    public static final String ALIASING = "ALIASING";
    public static final String MT_TPS = "MT_TPS";
    public static final String MT_TPD = "MT_TPD";
    public static final String DELIVERY_REPORT = "DELIVERY_REPORT";
    public static final String DELIVERY_REPORT_URL = "DELIVERY_REPORT_URL";
    public static final String MT_CHARGING = SMS_PERMISSION_ROLE_PREFIX + "_MT_CHARGING";

    public static final String SUBSCRIPTION_REQUIRED = "SUBSCRIPTION_REQUIRED";
}
