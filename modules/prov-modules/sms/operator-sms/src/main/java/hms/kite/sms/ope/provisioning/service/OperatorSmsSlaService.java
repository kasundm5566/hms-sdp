/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.sms.ope.provisioning.service;

import com.vaadin.Application;
import com.vaadin.ui.Component;

import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public interface OperatorSmsSlaService {

    void addPanel(Application application, Component component, String caption);

    Map<String, Object> viewOperatorSmsSla(Map<String, Object> operatorSmsSla);
}
