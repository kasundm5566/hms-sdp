/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.sms.ope.provisioning.ui;

import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.IntegerValidator;
import com.vaadin.data.validator.NullValidator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Select;
import com.vaadin.ui.TextField;
import hms.kite.provisioning.commons.ui.AbstractBasicDetailsLayout;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.component.charging.ChargingLayout;
import hms.kite.provisioning.commons.ui.validator.MessageLimitValidator;
import hms.kite.provisioning.commons.ui.validator.TpsTpdValidator;
import hms.kite.sms.ope.provisioning.OperatorSmsServiceRegistry;
import hms.kite.sms.ope.provisioning.ui.validator.KeywordValidator;
import hms.kite.util.SdpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.smsRoutingKeyRepositoryService;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.provisioning.commons.util.ProvShortCodeConfiguration.addPermittedCategories;
import static hms.kite.sms.ope.provisioning.OperatorSmsServiceRegistry.*;
import static hms.kite.sms.ope.provisioning.util.SmsSlaRoles.*;
import static hms.kite.util.KiteKeyBox.routingKeysK;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class SmsMoLayout extends AbstractBasicDetailsLayout {

    private static final Logger logger = LoggerFactory.getLogger(SmsMoLayout.class);

    private static final String WIDTH = "280px";

    private String operatorId;
    private String corporateUserId;

    private FormLayout bodyLayout;

    private Select shortCodeSelect;

    private TextField keywordTxtFld;
    private TextField moTpsTxtFld;
    private TextField moTpdTxtFld;
    private TextField connectionUrlTxtFld;

    private ChargingLayout chargingLayout;

    private Map<String, Object> findRoutingKey(Map<String, Object> smsSla) {
        List<Map<String, Object>> routingKeys = (List<Map<String, Object>>) smsSla.get(routingKeysK);
        if (routingKeys == null || routingKeys.size() != 1)
            throw new IllegalStateException("Found more than one  or no rouging key. Currently Only one routing key will be supported. Routing Keys = " + routingKeys);

        return routingKeys.get(0);
    }

    private List<Map<String, Object>> createRoutingKeys(String shortcode, String keyword, boolean exclusiveShortcode) {
        List<Map<String, Object>> routingKeys = new ArrayList<Map<String, Object>>();
        HashMap<String, Object> routingKey = new HashMap<String, Object>();
        routingKey.put(shortcodeK, shortcode);
        routingKey.put(keywordK, keyword);
        routingKey.put(exclusiveK, exclusiveShortcode);
        routingKeys.add(routingKey);
        return routingKeys;
    }

    private void init(String spId, String appId) {
        bodyLayout = new FormLayout();
        bodyLayout.addStyleName("sms-ncs-form");
        addComponent(bodyLayout);

        createShortCodeSelect();
        createKeywordTxtFld(spId, appId);
        createTpsTxtFld();
        createTpdTxtFld();
        createConnectionUrlTxtFld();
        createChargingTypeView();
    }

    private void createShortCodeSelect() {
        shortCodeSelect = getFieldWithPermission(SHORT_CODE_SELECT, new Select());
        if (isFieldNotNull(shortCodeSelect)) {
            shortCodeSelect.setCaption(application.getMsg("sms.sla.create.shortcode"));
            shortCodeSelect.setWidth(WIDTH);
            shortCodeSelect.setRequired(true);
            shortCodeSelect.setRequiredError(application.getMsg("sms.mo.shortcode.is.null"));
            shortCodeSelect.setImmediate(true);
            shortCodeSelect.setNullSelectionAllowed(false);

            List<String> supportedCategories = new ArrayList<String>();
            addPermittedCategories(application, supportedCategories);

            Map<String, Object> categorySelectQuery = new HashMap<String, Object>();
            categorySelectQuery.put("$in", supportedCategories);

            Map<String, Object> query = new HashMap<String, Object>();
            query.put(operatorK, operatorId);
            query.put(categoryK, categorySelectQuery);
            List<Map<String, Object>> rks = smsRoutingKeyRepositoryService().findAllRoutingKeysForOperatorWithCategories(query);

            for (Map<String, Object> rk : rks) {
                shortCodeSelect.addItem(rk.get(shortcodeK));
                shortCodeSelect.setItemCaption(rk.get(shortcodeK), (String) rk.get(shortcodeK));
            }

            shortCodeSelect.addListener(new Property.ValueChangeListener() {

                @Override
                public void valueChange(Property.ValueChangeEvent event) {
                    String shortcode = (String) shortCodeSelect.getValue();
                    boolean exclusive = smsRoutingKeyRepositoryService().isExclusive(smsK, operatorId, shortcode);

                    if (isFieldNotNull(keywordTxtFld)) {
                        if (exclusive) {
                            keywordTxtFld.setVisible(false);
                            keywordTxtFld.setRequired(false);
                            keywordTxtFld.setValue("");
                        } else {
                            keywordTxtFld.setVisible(true);
                            keywordTxtFld.setRequired(true);
                        }
                    }
                }
            });

            bodyLayout.addComponent(shortCodeSelect);
        }
    }

    private void createKeywordTxtFld(String spId, String appId) {
        keywordTxtFld = getFieldWithPermission(KEYWORD, new TextField());
        if (isFieldNotNull(keywordTxtFld)) {
            keywordTxtFld.setCaption(application.getMsg("sms.sla.create.keyword"));
            keywordTxtFld.setWidth(WIDTH);
            keywordTxtFld.setRequired(true);
            keywordTxtFld.setRequiredError(application.getMsg("sms.sla.create.keyword.null.error"));
            keywordTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("sms.mo.keyword.validation"),
                    application.getMsg("sms.sla.keyword.validation.error")));
            keywordTxtFld.addValidator(new KeywordValidator(spId, appId, operatorId, shortCodeSelect,
                    application.getMsg("sms.sla.create.keyword.already.exist.error")));
            bodyLayout.addComponent(keywordTxtFld);
        }
    }

    private void createTpsTxtFld() {
        moTpsTxtFld = getFieldWithPermission(MO_TPS, new TextField());
        if (isFieldNotNull(moTpsTxtFld)) {
            moTpsTxtFld.setCaption(application.getMsg("sms.sla.create.tps"));
            moTpsTxtFld.setWidth(WIDTH);
            moTpsTxtFld.setRequired(true);
            moTpsTxtFld.setRequiredError(application.getMsg("sms.sla.create.tps.null.error"));
            moTpsTxtFld.addValidator(new IntegerValidator(application.getMsg("sms.sla.create.tps.numeric.error")));
            moTpsTxtFld.addValidator(new MessageLimitValidator(application.getMsg("sms.sla.create.mo.maximum.message.per.second.limit.error"),
                    corporateUserId, smsK, moK, tpsK));
            moTpsTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("positiveNumberValidation"),
                    application.getMsg("sms.sla.create.tps.negative.error")));
            moTpsTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("sp.sms.zero.validation"),
                    application.getMsg("sms.sla.zeroValue.validation.error.message")));
            if (isDefaultMoTpsAllow()) {
                moTpsTxtFld.setValue(getDefaultMoTps());
            }
            bodyLayout.addComponent(moTpsTxtFld);
        }
    }

    private void createTpdTxtFld() {
        moTpdTxtFld = getFieldWithPermission(MO_TPD, new TextField());
        if (isFieldNotNull(moTpdTxtFld)) {
            moTpdTxtFld.setCaption(application.getMsg("sms.sla.create.tpd"));
            moTpdTxtFld.setWidth(WIDTH);
            moTpdTxtFld.setRequired(true);
            moTpdTxtFld.setRequiredError(application.getMsg("sms.sla.create.tpd.null.error"));
            moTpdTxtFld.addValidator(new IntegerValidator(application.getMsg("sms.sla.create.tpd.numeric.error")));
            moTpdTxtFld.addValidator(new MessageLimitValidator(application.getMsg("sms.sla.create.mo.maximum.message.per.day.limit.error"),
                    corporateUserId, smsK, moK, tpdK));
            moTpdTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("positiveNumberValidation"),
                    application.getMsg("sms.sla.create.tpd.negative.error")));
            moTpdTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("sp.sms.zero.validation"),
                    application.getMsg("sms.sla.zeroValue.validation.error.message")));
            moTpdTxtFld.addValidator(new TpsTpdValidator(moTpsTxtFld, moTpdTxtFld, application.getMsg("sms.sla.create.tps.tpd.validation")));
            if (isDefaultMoTpdAllow()) {
                moTpdTxtFld.setValue(getDefaultMoTpd());
            }
            bodyLayout.addComponent(moTpdTxtFld);
        }
    }

    private void createConnectionUrlTxtFld() {
        connectionUrlTxtFld = getFieldWithPermission(CONNECTION_URL, new TextField());
        if (isFieldNotNull(connectionUrlTxtFld)) {
            connectionUrlTxtFld.setCaption(application.getMsg("sms.sla.create.connection.url"));
            connectionUrlTxtFld.setWidth(WIDTH);
            connectionUrlTxtFld.setRequired(true);
            connectionUrlTxtFld.setRequiredError(application.getMsg("sms.sla.create.connection.url.empty.error"));
            connectionUrlTxtFld.addValidator(new NullValidator(application.getMsg("sms.sla.create.connection.url.null.error"), false));
            connectionUrlTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("urlValidation")
                    , application.getMsg("sms.sla.create.connection.url.malformed.error")));
            bodyLayout.addComponent(connectionUrlTxtFld);
        }
    }

    private void createChargingTypeView() {
        chargingLayout = ChargingLayout.smsMoChargingLayout(bodyLayout, operatorId, application,
                corporateUserId, MO_CHARGING, WIDTH, 3, 21);
        if (isDefaultMoChargingAllow()) {
            setValueToField(chargingLayout, getDefaultMoCharging());
        }
    }

    public SmsMoLayout(BaseApplication application, String corporateUserId, String spId, String appId) {
        super(application, SMS_PERMISSION_ROLE_PREFIX);

        this.corporateUserId = corporateUserId;
        this.operatorId = operatorId();

        init(spId, appId);
    }

    @Override
    public void setData(Map<String, Object> data) {
        Map<String, Object> moSla = (Map<String, Object>) data.get(moK);

        if (moSla == null) {
            return;
        }

        Map<String, Object> routingKey = findRoutingKey(data);

        if (isFieldNotNull(shortCodeSelect)) {
            String shortcode = (String) routingKey.get(shortcodeK);
            if (!shortCodeSelect.getItemIds().contains(shortcode)) {
                shortCodeSelect.addItem(shortcode);
            }
            setValueToField(shortCodeSelect, shortcode);
        }

        if (isFieldNotNull(keywordTxtFld)) {
            Boolean exclusive = (Boolean) routingKey.get(exclusiveK);
            if (!exclusive) {
                setValueToField(keywordTxtFld, routingKey.get(keywordK));
                keywordTxtFld.setVisible(true);
            } else {
                keywordTxtFld.setVisible(false);
            }
        }

        setValueToField(connectionUrlTxtFld, moSla.get(connectionUrlK));
        setValueToField(moTpsTxtFld, moSla.get(tpsK));
        setValueToField(moTpdTxtFld, moSla.get(tpdK));
        setValueToField(chargingLayout, moSla.get(chargingK));
    }

    @Override
    public void getData(Map<String, Object> data) {
        Map<String, Object> moSla = new HashMap<String, Object>();

        if (isFieldNotNull(shortCodeSelect) && isFieldNotNull(keywordTxtFld)) {
            String shortcode = shortCodeSelect.getValue().toString();
            String keyword = keywordTxtFld.isVisible() ? keywordTxtFld.getValue().toString().toLowerCase() : "";
            boolean exclusiveShortcode = !keywordTxtFld.isVisible();
            List<Map<String, Object>> routingKeys = createRoutingKeys(shortcode, keyword, exclusiveShortcode);

            getValueFromField(shortCodeSelect, data, routingKeysK, routingKeys);
        }

        getValueFromField(connectionUrlTxtFld, moSla, connectionUrlK);
        getValueFromField(moTpsTxtFld, moSla, tpsK);
        getValueFromField(moTpdTxtFld, moSla, tpdK);
        getValueFromField(chargingLayout, moSla, chargingK);

        data.put(moK, moSla);
    }

    @Override
    public void validate() {
        try {
            validateField(shortCodeSelect);
            if (isFieldNotNull(keywordTxtFld) && keywordTxtFld.isVisible()) {
                keywordTxtFld.validate();
            }
        } catch (SdpException ex) {
            String messageKey = OperatorSmsServiceRegistry.errorCodeResolver().findMessageKey(ex);
            throw new Validator.InvalidValueException(application.getMsg(messageKey));
        }

        validateField(moTpsTxtFld);
        validateField(moTpdTxtFld);
        validateField(connectionUrlTxtFld);
        validateField(chargingLayout);
    }

    @Override
    public void reset() {
        resetFields("", keywordTxtFld, connectionUrlTxtFld);
        chargingLayout.reset();
        if (isDefaultMoTpsAllow()) {
            resetFields(getDefaultMoTps(), moTpsTxtFld);
        } else {
            resetFields("", moTpsTxtFld);
        }

        if(isDefaultMoTpdAllow()) {
            resetFields(getDefaultMoTpd(), moTpdTxtFld);
        } else {
            resetFields("", moTpdTxtFld);
        }
    }

    @Override
    public void setPermissions() {
        setReadWritePermissionToComponent(shortCodeSelect, keywordTxtFld, moTpsTxtFld,
                moTpdTxtFld, connectionUrlTxtFld);
        chargingLayout.setPermissions();
    }
}
