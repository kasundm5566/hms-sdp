/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.sms.ope.provisioning;

import hms.kite.provisioning.commons.ValidationRegistry;
import hms.kite.provisioning.commons.impl.ValidationRegistryImpl;
import hms.kite.provisioning.commons.ui.DefaultErrorCodeResolver;
import hms.kite.provisioning.commons.ui.ErrorCodeResolver;
import hms.kite.sms.ope.provisioning.service.OperatorSmsSlaService;
import hms.kite.sms.ope.provisioning.service.impl.OperatorSmsSlaServiceImpl;
import hms.kite.util.NullObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class OperatorSmsServiceRegistry {

    private static OperatorSmsSlaService operatorSmsSlaService;
    private static String operatorId;
    private static ErrorCodeResolver errorCodeResolver;
    private static ValidationRegistry validationRegistry;
    private static Properties smsDefaultProperties;
    private static Map<String, Object> defaultMoCharging;
    private static Map<String, Object> defaultMtCharging;

    static {
        setOperatorSmsSlaService(new NullObject<OperatorSmsSlaServiceImpl>().get());
        setOperatorId("");
        setErrorCodeResolver(new NullObject<DefaultErrorCodeResolver>().get());
        setValidationRegistry(new NullObject<ValidationRegistryImpl>().get());
        setSmsDefaultProperties(new NullObject<Properties>().get());
        setDefaultMoCharging(new HashMap<String, Object>());
        setDefaultMtCharging(new HashMap<String, Object>());
    }

    public static OperatorSmsSlaService operatorSmsSlaService() {
        return operatorSmsSlaService;
    }

    private static void setOperatorSmsSlaService(OperatorSmsSlaService operatorSmsSlaService) {
        OperatorSmsServiceRegistry.operatorSmsSlaService = operatorSmsSlaService;
    }

    public static String operatorId() {
        return operatorId;
    }

    private static void setOperatorId(String operatorId) {
        OperatorSmsServiceRegistry.operatorId = operatorId;
    }

    public static ErrorCodeResolver errorCodeResolver() {
        return errorCodeResolver;
    }

    private static void setErrorCodeResolver(ErrorCodeResolver errorCodeResolver) {
        OperatorSmsServiceRegistry.errorCodeResolver = errorCodeResolver;
    }

    public static ValidationRegistry validationRegistry() {
        return validationRegistry;
    }

    private static void setValidationRegistry(ValidationRegistry validationRegistry) {
        OperatorSmsServiceRegistry.validationRegistry = validationRegistry;
    }

    private static void setSmsDefaultProperties(Properties smsDefaultProperties) {
        OperatorSmsServiceRegistry.smsDefaultProperties = smsDefaultProperties;
    }

    public static boolean isDefaultMoTpsAllow() {
        return Boolean.parseBoolean(smsDefaultProperties.getProperty("sms.default.mo.tps.allow"));
    }

    public static String getDefaultMoTps() {
        return smsDefaultProperties.getProperty("sms.default.mo.tps");
    }

    public static boolean isDefaultMoTpdAllow() {
        return Boolean.parseBoolean(smsDefaultProperties.getProperty("sms.default.mo.tpd.allow"));
    }

    public static String getDefaultMoTpd() {
        return smsDefaultProperties.getProperty("sms.default.mo.tpd");
    }

    public static boolean isDefaultMtTpsAllow() {
        return Boolean.parseBoolean(smsDefaultProperties.getProperty("sms.default.mt.tps.allow"));
    }

    public static String getDefaultMtTps() {
        return smsDefaultProperties.getProperty("sms.default.mt.tps");
    }

    public static boolean isDefaultMtTpdAllow() {
        return Boolean.parseBoolean(smsDefaultProperties.getProperty("sms.default.mt.tpd.allow"));
    }

    public static String getDefaultMtTpd() {
        return smsDefaultProperties.getProperty("sms.default.mt.tpd");
    }

    public static boolean getDefaultDeliveryReport() {
        return Boolean.parseBoolean(smsDefaultProperties.getProperty("sms.default.delivery.report.enable"));
    }

    public static boolean isDefaultMoChargingAllow() {
        return Boolean.parseBoolean(smsDefaultProperties.getProperty("sms.default.mo.charging.allow"));
    }

    private static void setDefaultMoCharging(Map<String, Object> defaultMoCharging) {
        OperatorSmsServiceRegistry.defaultMoCharging = defaultMoCharging;
    }

    public static Map<String, Object> getDefaultMoCharging() {
        return defaultMoCharging;
    }

    public static boolean isDefaultMtChargingAllow() {
        return Boolean.parseBoolean(smsDefaultProperties.getProperty("sms.default.mt.charging.allow"));
    }

    private static void setDefaultMtCharging(Map<String, Object> defaultMtCharging) {
        OperatorSmsServiceRegistry.defaultMtCharging = defaultMtCharging;
    }

    public static Map<String, Object> getDefaultMtCharging() {
        return defaultMtCharging;
    }
}
