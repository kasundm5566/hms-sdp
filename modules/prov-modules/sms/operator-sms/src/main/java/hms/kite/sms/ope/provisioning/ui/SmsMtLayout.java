/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.sms.ope.provisioning.ui;

import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.IntegerValidator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import hms.kite.provisioning.commons.ui.AbstractBasicDetailsLayout;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.component.charging.ChargingLayout;
import hms.kite.provisioning.commons.ui.validator.MessageLimitValidator;
import hms.kite.provisioning.commons.ui.validator.TpsTpdValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.provisioning.commons.util.StringConverter.*;
import static hms.kite.sms.ope.provisioning.OperatorSmsServiceRegistry.*;
//import static hms.kite.sms.ope.provisioning.util.SmsFeatureRegistry.getSmsMoDefaultActive;
//import static hms.kite.sms.ope.provisioning.util.SmsFeatureRegistry.getSmsMtDefaultDeliveryReportActive;
import static hms.kite.sms.ope.provisioning.util.SmsSlaRoles.*;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class SmsMtLayout extends AbstractBasicDetailsLayout {

    private static final Logger logger = LoggerFactory.getLogger(SmsMtLayout.class);

    private static final String WIDTH = "280px";

    private String corporateUserId;

    private FormLayout bodyLayout;

    private TextField defaultSenderAddressTxtFld;
    private TextField aliasingTxtFld;
    private TextField mtTpsTxtFld;
    private TextField mtTpdTxtFld;
    private TextField deliveryReportUrlTxtFld;

    private CheckBox deliveryReportChkBox;

    private ChargingLayout chargingLayout;

    private void validateDefaultSenderAddressOrAliasingForNonEmpty() {
        boolean defaultSenderAddressEmpty = isFieldEmpty(defaultSenderAddressTxtFld);
        boolean aliasingEmpty = isFieldEmpty(aliasingTxtFld);
        if (defaultSenderAddressEmpty && aliasingEmpty) {
            throw new Validator.InvalidValueException(application.getMsg("sms.sla.create.mt.both.default.sender.address." +
                    "and.aliasing.field.are.empty"));
        }
    }

    private boolean isFieldEmpty(Field field) {
        return isFieldNotNull(field) && (field.getValue() == null || field.getValue().toString().trim().length() == 0);
    }

    private void init() {
        bodyLayout = new FormLayout();
        bodyLayout.addStyleName("sms-ncs-form");
        addComponent(bodyLayout);

        createDefaultSenderAddressFld();
        createAliasingFld();
        createMaxTpsFld();
        createMaxTpdFld();
        createDeliveryReportView();
        createChargingView();
    }

    private void createDefaultSenderAddressFld() {
        defaultSenderAddressTxtFld = getFieldWithPermission(DEFAULT_SENDER_ADDRESS, new TextField());
        if (isFieldNotNull(defaultSenderAddressTxtFld)) {
            defaultSenderAddressTxtFld.setCaption(application.getMsg("sms.sla.create.mt.default.sender.address"));
            defaultSenderAddressTxtFld.setWidth(WIDTH);
            defaultSenderAddressTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("default.sender.address"),
                    application.getMsg("sms.sla.create.mt.default.sender.address.format.error")));
            defaultSenderAddressTxtFld.setRequired(false);
            bodyLayout.addComponent(defaultSenderAddressTxtFld);
        }
    }

    private void createAliasingFld() {
        aliasingTxtFld = getFieldWithPermission(ALIASING, new TextField());
        if (isFieldNotNull(aliasingTxtFld)) {
            aliasingTxtFld.setCaption(application.getMsg("sms.sla.create.mt.aliasing"));
            aliasingTxtFld.setWidth(WIDTH);
            aliasingTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("alias"),
                    application.getMsg("sms.sla.create.mt.aliasing.format.error")));
            aliasingTxtFld.setRequired(false);
            bodyLayout.addComponent(aliasingTxtFld);
        }
    }

    private void createMaxTpsFld() {
        mtTpsTxtFld = getFieldWithPermission(MT_TPS, new TextField());
        if (isFieldNotNull(mtTpsTxtFld)) {
            mtTpsTxtFld.setCaption(application.getMsg("sms.sla.create.mt.maximum.message.per.second"));
            mtTpsTxtFld.setWidth(WIDTH);
            mtTpsTxtFld.setRequired(true);
            mtTpsTxtFld.setRequiredError(application.getMsg("sms.sla.create.mt.maximum.message.per.second.null.error"));
            mtTpsTxtFld.addValidator(new IntegerValidator(application.getMsg("sms.sla.create.mt.maximum.message.per.second.numeric.error")));
            mtTpsTxtFld.addValidator(new MessageLimitValidator(application.getMsg("sms.sla.create.mt.maximum.message.per.second.limit.error"),
                    corporateUserId, smsK, mtK, tpsK));
            mtTpsTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("positiveNumberValidation"),
                    application.getMsg("sms.sla.create.mt.maximum.message.per.second.negative.error")));
            mtTpsTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("sp.sms.zero.validation"),
                    application.getMsg("sms.sla.zeroValue.validation.error.message")));
            if (isDefaultMtTpsAllow()) {
                mtTpsTxtFld.setValue(getDefaultMtTps());
            }
            bodyLayout.addComponent(mtTpsTxtFld);
        }
    }

    private void createMaxTpdFld() {
        mtTpdTxtFld = getFieldWithPermission(MT_TPD, new TextField());
        if (isFieldNotNull(mtTpdTxtFld)) {
            mtTpdTxtFld.setCaption(application.getMsg("sms.sla.create.mt.maximum.message.per.day"));
            mtTpdTxtFld.setWidth(WIDTH);
            mtTpdTxtFld.setRequired(true);
            mtTpdTxtFld.setRequiredError(application.getMsg("sms.sla.create.mt.maximum.message.per.day.null.error"));
            mtTpdTxtFld.addValidator(new IntegerValidator(application.getMsg("sms.sla.create.mt.maximum.message.per.day.numeric.error")));
            mtTpdTxtFld.addValidator(new MessageLimitValidator(application.getMsg("sms.sla.create.mt.maximum.message.per.day.limit.error"),
                    corporateUserId, smsK, mtK, tpdK));
            mtTpdTxtFld.addValidator(new TpsTpdValidator(mtTpsTxtFld, mtTpdTxtFld, application.getMsg("sms.sla.create.tps.tpd.validation")));
            mtTpdTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("positiveNumberValidation"),
                    application.getMsg("sms.sla.create.mt.maximum.message.per.day.negative.error")));
            mtTpdTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("sp.sms.zero.validation"),
                    application.getMsg("sms.sla.zeroValue.validation.error.message")));
            if (isDefaultMtTpdAllow()) {
                mtTpdTxtFld.setValue(getDefaultMtTpd());
            }
            bodyLayout.addComponent(mtTpdTxtFld);
        }
    }

    private void createDeliveryReportView() {
        deliveryReportChkBox = getFieldWithPermission(DELIVERY_REPORT, new CheckBox());
        if (isFieldNotNull(deliveryReportChkBox)) {
            deliveryReportChkBox.setCaption(application.getMsg("sms.sla.create.mt.delivery.reports.required"));
            deliveryReportChkBox.setImmediate(true);
            deliveryReportChkBox.addListener(new Property.ValueChangeListener() {

                @Override
                public void valueChange(Property.ValueChangeEvent event) {
                    if (isFieldNotNull(deliveryReportUrlTxtFld)) {
                        Boolean value = (Boolean) event.getProperty().getValue();
                        if (isFieldNotNull(deliveryReportUrlTxtFld)) {
                            if (value) {
                                deliveryReportUrlTxtFld.setVisible(true);
                                deliveryReportUrlTxtFld.setRequired(true);
                            } else {
                                deliveryReportUrlTxtFld.setVisible(false);
                                deliveryReportUrlTxtFld.setRequired(false);
                            }
                        }
                    }
                }
            });
            bodyLayout.addComponent(deliveryReportChkBox);

            deliveryReportUrlTxtFld = getFieldWithPermission(DELIVERY_REPORT_URL, new TextField());
            if (isFieldNotNull(deliveryReportUrlTxtFld)) {
                deliveryReportUrlTxtFld.setCaption(application.getMsg("sms.sla.create.mt.delivery.report.url"));
                deliveryReportUrlTxtFld.setWidth(WIDTH);
                deliveryReportUrlTxtFld.setRequiredError(application.getMsg("sms.sla.create.mt.delivery.report.url.null.error"));
                deliveryReportUrlTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("urlValidation"),
                        application.getMsg("sms.sla.create.mt.delivery.report.url.malformed.error")));
                deliveryReportUrlTxtFld.setVisible(false);
                bodyLayout.addComponent(deliveryReportUrlTxtFld);
            }

            deliveryReportChkBox.setValue(getDefaultDeliveryReport());
        }
    }

    private void createChargingView() {
        chargingLayout = ChargingLayout.smsMtChargingLayout(bodyLayout, operatorId(), application, corporateUserId, MT_CHARGING, WIDTH, 3, 21);
        if (isDefaultMtChargingAllow()) {
            setValueToField(chargingLayout, getDefaultMtCharging());
        }
    }

    public SmsMtLayout(BaseApplication application, String corporateUserId) {
        super(application, SMS_PERMISSION_ROLE_PREFIX);

        this.corporateUserId = corporateUserId;

        init();
    }

    @Override
    public void setData(Map<String, Object> data) {
        Map<String, Object> mtMap = (Map<String, Object>) data.get(mtK);

        if (mtMap == null) {
            return;
        }

        setValueToField(defaultSenderAddressTxtFld, mtMap.get(defaultSenderAddressK));
        setValueToField(aliasingTxtFld, convertToString((List<String>) mtMap.get(aliasingK), JOIN_SEPARATOR));
        setValueToField(mtTpsTxtFld, mtMap.get(tpsK));
        setValueToField(mtTpdTxtFld, mtMap.get(tpdK));

        if (mtMap.get(deliveryReportUrlK) != null) {
            setValueToField(deliveryReportChkBox, true);
            setValueToField(deliveryReportUrlTxtFld, mtMap.get(deliveryReportUrlK));
        } else {
            setValueToField(deliveryReportChkBox, false);
        }
        setValueToField(chargingLayout, mtMap.get(chargingK));
    }

    @Override
    public void getData(Map<String, Object> data) {
        Map<String, Object> mtMap = new HashMap<String, Object>();

        getValueFromField(defaultSenderAddressTxtFld, mtMap, defaultSenderAddressK);
        getValueFromField(aliasingTxtFld, mtMap, aliasingK, convertToList(aliasingTxtFld.getValue().toString(), SEPARATOR));
        getValueFromField(mtTpsTxtFld, mtMap, tpsK);
        getValueFromField(mtTpdTxtFld, mtMap, tpdK);

        if (deliveryReportChkBox.booleanValue()) {
            getValueFromField(deliveryReportUrlTxtFld, mtMap, deliveryReportUrlK);
        }
        getValueFromField(chargingLayout, mtMap, chargingK);

        data.put(mtK, mtMap);
    }

    @Override
    public void validate() {
        validateField(defaultSenderAddressTxtFld);
        validateField(aliasingTxtFld);
        validateDefaultSenderAddressOrAliasingForNonEmpty();
        validateField(mtTpsTxtFld);
        validateField(mtTpdTxtFld);
        if (isFieldNotNull(deliveryReportChkBox) && deliveryReportChkBox.booleanValue()) {
            validateField(deliveryReportUrlTxtFld);
        }
        validateField(chargingLayout);
    }

    @Override
    public void reset() {
        resetFields("", defaultSenderAddressTxtFld, aliasingTxtFld, deliveryReportUrlTxtFld);
        chargingLayout.reset();
        resetFields(false, deliveryReportChkBox);
        if (isDefaultMtTpsAllow()) {
            resetFields(getDefaultMtTps(), mtTpsTxtFld);
        } else {
            resetFields("", mtTpsTxtFld);
        }

        if (isDefaultMtTpdAllow()) {
            resetFields(getDefaultMtTpd(), mtTpdTxtFld);
        } else {
            resetFields("", mtTpdTxtFld);
        }
    }

    @Override
    public void setPermissions() {
        setReadWritePermissionToComponent(defaultSenderAddressTxtFld, aliasingTxtFld, mtTpsTxtFld,
                mtTpdTxtFld, deliveryReportChkBox, deliveryReportUrlTxtFld);
        chargingLayout.setPermissions();
    }
}
