/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/

package hms.kite.sms.ope.provisioning.service.impl;

import com.vaadin.Application;
import com.vaadin.ui.Component;
import com.vaadin.ui.Panel;
import hms.kite.sms.ope.provisioning.service.OperatorSmsSlaService;
import hms.kite.util.SdpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.ncsRepositoryService;
import static hms.kite.datarepo.RepositoryServiceRegistry.smsRoutingKeyRepositoryService;

/*
* $LastChangedDate$
* $LastChangedBy$
* $LastChangedRevision$
*/

public class OperatorSmsSlaServiceImpl implements OperatorSmsSlaService {

    private static final Logger logger = LoggerFactory.getLogger(OperatorSmsSlaServiceImpl.class);

    @Override
    public void addPanel(Application application, Component component, String caption) {

        Panel panel = new Panel();
        panel.addComponent(component);
        panel.setCaption(caption);

        application.getMainWindow().removeAllComponents();
        application.getMainWindow().addComponent(panel);
    }

    @Override
    public Map<String, Object> viewOperatorSmsSla(Map<String, Object> operatorSmsSla) {
        Map<String, Object> map = ncsRepositoryService().findOperatorSla(operatorSmsSla);
        if (map != null) {
            return map;
        } else {
            logger.error("No matching operator SMS sla for the query: {}", operatorSmsSla);
            throw new SdpException("No matching operator SMS sla for the query: " + operatorSmsSla);
        }
    }
}
