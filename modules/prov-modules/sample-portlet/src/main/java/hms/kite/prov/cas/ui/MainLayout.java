/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.prov.cas.ui;

import com.vaadin.ui.*;
import hms.kite.prov.cas.SamplePortlet;
import hms.kite.prov.cas.ui.common.ViewMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class MainLayout extends VerticalLayout {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainLayout.class);
    private SamplePortlet application;

    public MainLayout(SamplePortlet application, String appId, String corporateUserId) {
        this.application = application;
    }

    private void loadComponents() {
        addComponent(new Label(application.getMsg("sample.app.label")));
    }

    private void goBack() {

    }

    private void save() {

    }

    private void confirmed() {

    }

    private void reset() {
    }

    public void reloadComponents(ViewMode viewMode) {
        removeAllComponents();
        loadComponents();
    }

    public void reloadData(Map<String, Object> casSlaData) {

    }
}
