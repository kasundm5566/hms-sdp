/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.prov.cas.ui.common;

/**
 * $LastChangedDate:$
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public enum ViewMode {
    VIEW, EDIT, CONFIRM;
}
