/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.prov.cas;

import hms.kite.provisioning.commons.ValidationRegistry;

/**
 * $LastChangedDate: 2011-07-25 13:35:35 +0530 (Mon, 25 Jul 2011) $
 * $LastChangedBy: romith $
 * $LastChangedRevision: 75232 $
 */
public class ServiceRegistryForSample {

    private static ValidationRegistry validationRegistry;

    public static ValidationRegistry validationRegistry() {
        return validationRegistry;
    }

    private static void setValidationRegistry(ValidationRegistry validationRegistry) {
        ServiceRegistryForSample.validationRegistry = validationRegistry;
    }
}
