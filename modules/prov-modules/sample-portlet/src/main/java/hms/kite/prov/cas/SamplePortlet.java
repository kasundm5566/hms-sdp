/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.prov.cas;

import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.terminal.gwt.server.PortletApplicationContext2;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import hms.kite.datarepo.audit.AuditTrail;
import hms.kite.prov.cas.ui.MainLayout;
import hms.kite.prov.cas.ui.common.ViewMode;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.LiferayUtil;
import hms.kite.provisioning.commons.ui.UiManagerImpl;
import hms.kite.provisioning.commons.util.ProvKeyBox;
import hms.kite.util.KiteKeyBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.ncsRepositoryService;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: 2011-07-25 13:35:35 +0530 (Mon, 25 Jul 2011) $
 * $LastChangedBy: romith $
 * $LastChangedRevision: 75232 $
 */
public class SamplePortlet extends BaseApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(SamplePortlet.class);
    private MainLayout casSlaLayout;
    private ThemeDisplay themeDisplay;
    private String corporateUserId;
    private Panel mainPanel;
    private Panel innerPanel;

    @Override
    protected String getApplicationName() {
        return "sample";
    }

    @Override
    public void init() {
        setResourceBundle("US");
        Window main = new Window(getMsg("cas.sla.window.title"));
        setMainWindow(main);
        setUiManager(new UiManagerImpl(this));
        String caption = this.getMsg("cas.sla.window.title");


        VerticalLayout mainLayout = new VerticalLayout();

        //TODO remove hard coded title
        mainPanel = new Panel("");
        mainLayout.addComponent(mainPanel);

        mainLayout.addStyleName("container-panel");
        mainLayout.setWidth("762px");
        mainLayout.setMargin(true);

        innerPanel = new Panel(caption);
        mainPanel.addComponent(innerPanel);
        innerPanel.addStyleName("child-panel");

        setPortletContext((PortletApplicationContext2) getContext());
        getPortletContext().addPortletListener(this, this);

        getUiManager().clearAll();
        getUiManager().pushScreen(MainLayout.class.getName(), mainLayout);
    }

    @Override
    public void handleEventRequest(EventRequest request, EventResponse response, Window window) {
        try {
            super.handleEventRequest(request, response, window);
            setCorporateUserId(request);


            String eventName = request.getEvent().getName();
            themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
            Long userId = themeDisplay.getUserId();
            setPortletUserId(userId);
            Map<String, Object> eventData = (HashMap<String, Object>) request.getEvent().getValue();
            LOGGER.debug("Received Event Data is [{}]", eventData);
            AuditTrail.setCurrentUser((String) eventData.get(currentUsernameK), (String) eventData.get(currentUserTypeK));
            addToSession(ProvKeyBox.currentUserTypeK, eventData.get(currentUserTypeK));
            String appId = (String) eventData.get(ProvKeyBox.appIdK);
            String ncsType = (String) eventData.get(ProvKeyBox.ncsTypeK);
            if (appId == null || ncsType == null || !ncsType.equals(KiteKeyBox.dlAppK)) {
                LOGGER.debug("Event Request ignored, since it is not belong to me");
                return;
            }
            if (createNcsK.equals(eventName)) {
                handleCreateNcsRequest(eventData);
            } else if (saveNcsK.equals(eventName)) {
                handleSaveNcsRequest(eventData);
            } else if (viewNcsK.equals(eventName)) {
                handleViewNcsRequest(eventData);
            } else if (reconfigureNcsK.equals(eventName)) {
                handleReconfigureNcsRequest(eventData);
            } else if (crEditNcsEvent.equals(eventName)) {
                handleCrEditNcsEvent(eventData);
            } else {
                LOGGER.error("Unexpected event [{}], Can't handle", eventName);
            }

        } catch (Exception e) {
            LOGGER.error("Unexpected error occurred while handling event ", e);
        }
    }

    private void handleCrEditNcsEvent(Map<String, Object> eventData) {
        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        innerPanel.removeAllComponents();
        Map<String, Object> sessionCasSlaData = getCasSlaData();
        casSlaLayout = new MainLayout(this, (String) eventData.get(appIdK), corporateUserId);
        innerPanel.addComponent(casSlaLayout);
        if (sessionCasSlaData != null) {
            casSlaLayout.reloadData(sessionCasSlaData);
        } else {
            reloadDataFromRepository(eventData);
        }
        casSlaLayout.reloadComponents(ViewMode.EDIT);
    }

    private void setCorporateUserId(EventRequest request) {
        Map<String, Object> eventData = (HashMap<String, Object>) request.getEvent().getValue();
        this.corporateUserId = (String) eventData.get(coopUserIdK);
    }

    private void setPortletUserId(Long userId) {
        if (getPortletUserId() == null) {
            addToSession("portlet_user_id", userId);
        }
    }

    private Long getPortletUserId() {
        return (Long) getFromSession("portlet_user_id");
    }

    private void handleCreateNcsRequest(Map<String, Object> eventData) {
        LOGGER.debug("CAS NCS SLA Create request received...");
        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        innerPanel.removeAllComponents();
        casSlaLayout = new MainLayout(this, (String) eventData.get(appIdK), corporateUserId);
        innerPanel.addComponent(casSlaLayout);
        casSlaLayout.reloadComponents(ViewMode.EDIT);
    }

    private void handleReconfigureNcsRequest(Map<String, Object> eventData) {
        LOGGER.debug("CAS NCS SLA Re-Configure request received...");
        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        innerPanel.removeAllComponents();
        Map<String, Object> sessionCasSlaData = getCasSlaData();
        casSlaLayout = new MainLayout(this, (String) eventData.get(appIdK), corporateUserId);
        innerPanel.addComponent(casSlaLayout);
        if (sessionCasSlaData != null) {
            casSlaLayout.reloadData(sessionCasSlaData);
        } else {
            reloadDataFromRepository(eventData);
        }
        casSlaLayout.reloadComponents(ViewMode.EDIT);
    }

    private void handleViewNcsRequest(Map<String, Object> data) {
        LOGGER.debug("CAS NCS SLA View request received...");
        mainPanel.setCaption((String) data.get(ncsPortletTitle));
        reloadNcsData(ViewMode.VIEW, data);
    }

    private void handleSaveNcsRequest(Map<String, Object> data) {
        LOGGER.debug("CAS NCS SLA Save request received...");
        Map<String, Object> sessionCasSlaData = getCasSlaData();
        String operator = "";
        String ncsType = casK;
        String appId = (String) data.get(appIdK);
        String status = (String) data.get(statusK);

        if (deleteK.equals(status)) {
            ncsRepositoryService().update(appId, ncsType, operator, status);
            removeFromSession("cas_sla_data");
        } else if (sessionCasSlaData != null) {
            sessionCasSlaData.put(statusK, data.get(statusK));
            ncsRepositoryService().createNcs(sessionCasSlaData, (String) data.get(spIdK));
        } else {
            ncsRepositoryService().update(appId, ncsType, operator, status);
        }
        closePortlet();
        LOGGER.debug("CAS SLA Saving request processed successfully");
    }

    private void reloadNcsData(ViewMode viewMode, Map<String, Object> data) {
        innerPanel.removeAllComponents();
        casSlaLayout = new MainLayout(this, (String) data.get(appIdK), corporateUserId);
        innerPanel.addComponent(casSlaLayout);
        reloadDataFromRepository(data);
        casSlaLayout.reloadComponents(viewMode);
    }

    private void reloadDataFromRepository(Map<String, Object> data) {
        String appId = (String) data.get(KiteKeyBox.appIdK);
        List<Map<String, Object>> casSla = ncsRepositoryService().findByAppIdNcsType(appId, KiteKeyBox.casK);
        if (casSla != null && casSla.size() > 0) {
            casSlaLayout.reloadData(casSla.get(0));
        }
    }

    public void closePortlet() {
        if (casSlaLayout != null) {
            casSlaLayout.removeAllComponents();
        }
        themeDisplay.getLayoutTypePortlet().removePortletId(getPortletUserId(), LiferayUtil.createPortletId(casK));
    }

    public Map<String, Object> getCasSlaData() {
        return (Map<String, Object>) getFromSession("cas_sla_data");
    }
}
