
##DA README

1.  Extract the smb://192.168.0.15/shared/Virtual%20City/prov/build-deps/liferay-portal-6.0.6.zip path/ . It'll create path/liferay-portal-6.0.6

2.  Extract the smb://192.168.0.15/shared/Virtual%20City/prov/build-deps/ROOT-NEW.tar.gz to {LIFERAY_HOME}/tomcat-{version}/webapps

3.  Edit mysql database properties in portal-ext.properties file which can be found in the following location.
            {LIFERAY_HOME}/tomcat-{version}/webapps/ROOT/WEB-INF/classes

4.  Start mysql and create a database named lportal using CREATE DATABASE lportal;

5.  Run sql script in kite/modules/prov-modules/db-scripts to populate lportal database. Use the following command.
            source <path to the script file>/lportal.sql;

6.  Start mongoDB server

7.  Add initial data from mchoice-sdp/kite/data directory. Import data in these files using mongo import command.
            db name is kite and collection name is the file name without extension.

            For example : system_configurations.json can import using
            mongoimport -d kite -c system_configuration -file mchoice-sdp/kite/data/system_configurations.json

    for more details read the section 2.2 in kite/README.txt

8.  Run mvn clean install on top level of kite

9.  Configure following enviromental variable in your ~/.bash_profile
            export 'LIFERAY_HOME=/path/liferay-portal-6.0.6'

10. Run prov-modules/deploy.sh to deploy the built portlets.

11. Add the followings to /etc/hosts
        192.168.0.95       cur.dlg
        192.168.0.95       prov.sdp.dlg
        192.168.0.95       workflowdb.sdp.dlg
        192.168.0.95       pgwc.dlg
        192.168.0.95       pgw.dlg
        192.168.0.95       store.app.dlg

12. Go to {LIFERAY_HOME}/tomcat-{version}/bin and start liferay server using ./catalina.sh run command.

13. Remove the log4j.properties file which created automatically in all web modules/WEB-INF/classes directory for
    proper logging.

14. Change the following DNS according to the user
        - login-hook/src/main/resources/portal.properties
            cur.cas.login.url = https://dev.sdp.hsenidmobile.com/cas/login
        - prov/src/main/webapp/WEB-INF/provisioning.properties
            application.publish.url=https://dev.sdp.hsenidmobile.com/appstore/internal/#publish-application_
        - docs/portal-ext.properties
            cas.login.url=https://dev.sdp.hsenidmobile.com/cas/login
            cas.logout.url=https://dev.sdp.hsenidmobile.com/cas/logout?service=https://dialogsdp.com
            cas.server.url=https://dev.sdp.hsenidmobile.com/cas
            cas.service.url=https://dev.sdp.hsenidmobile.com/c/portal/login
        - cas/src/main/resources/cas_en_US.properties
            ussd/operator-ussd/src/main/resources/ussd-prov.properties
            sms/operator-sms/src/main/resources/sms-prov.properties
            subscription/src/main/resources/subscription_en_US.properties
            wap-push/src/main/resources/wap-push_en_US.properties
        create.configure.payment.instruments.link = https://dev.sdp.hsenidmobile.com/hms-pgw-consumer-selfcare#add-payment

15. If building for 'Vodafone' profile please add the following update to the 'kite' mongo-database.
         
          db.system_configurations.update(
                {"_id":"supported-ncses-and-operators"}, 
                {$set: 
                    {"value":
                        {
                            "sms" : [ "vodafone" ], 
                            "ussd" : [ "vodafone" ], 
                            "cas" : [ ], 
                            "subscription" : [ ], 
                            "downloadable" : [ ], 
                            "lbs" : [ "vodafone" ], 
                            "wap-push" : [ "vodafone" ],
                            "vdf-apis":[] 
                        }
                    }
                }
          );