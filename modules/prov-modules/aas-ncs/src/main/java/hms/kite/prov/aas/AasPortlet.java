package hms.kite.prov.aas;

import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.terminal.gwt.server.PortletApplicationContext2;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import hms.kite.datarepo.audit.AuditTrail;
import hms.kite.prov.aas.ui.AasSlaLayout;
import hms.kite.prov.aas.ui.AasSlaMainLayout;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.UiManagerImpl;
import hms.kite.provisioning.commons.util.ProvKeyBox;
import hms.kite.util.KiteKeyBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class AasPortlet extends BaseApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(AasPortlet.class);
    /*private AasSlaLayout casSlaLayout;
    private ThemeDisplay themeDisplay;
    private String corporateUserId;*/
    private Panel mainPanel;
    private Panel innerPanel;

    @Override
    protected String getApplicationName() {
        return "aas";
    }

    @Override
    public void init() {
        setResourceBundle("US");
        Window main = new Window(getMsg("aas.sla.window.title"));
        setMainWindow(main);
        setUiManager(new UiManagerImpl(this));
        String caption = this.getMsg("aas.sla.window.title");


        VerticalLayout mainLayout = new VerticalLayout();

        //TODO remove hard coded title
        mainPanel = new Panel("");
        mainLayout.addComponent(mainPanel);

        mainLayout.addStyleName("container-panel");
        mainLayout.setWidth("762px");
        mainLayout.setMargin(true);

        innerPanel = new Panel(caption);
        mainPanel.addComponent(innerPanel);
        innerPanel.addStyleName("child-panel");

        setPortletContext((PortletApplicationContext2) getContext());
        getPortletContext().addPortletListener(this, this);

       // getUiManager().clearAll();
        //getUiManager().pushScreen(AasSlaMainLayout.class.getName(), mainLayout);
    }


}
