package hms.kite.prov.aas;


import hms.kite.provisioning.commons.ValidationRegistry;


public class AasServiceRegistry {

    private static ValidationRegistry validationRegistry;

    public static ValidationRegistry validationRegistry() {
        return validationRegistry;
    }

    private static void setValidationRegistry(ValidationRegistry validationRegistry) {
        AasServiceRegistry.validationRegistry = validationRegistry;
    }
}
