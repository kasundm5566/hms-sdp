01. Setup following requirments

	liferay-portal-6.0.6
	mysql.5.x
	mongodb 1.8.x

	-Download the liferay package from shared location
	 smb://192.168.0.15/shared/VirtualCity/prov/build-deps/liferay-portal-6.0.6.zip

02. Start mysql and create a database named lportal using CREATE DATABASE lportal;

03. Run sql script in kite/modules/prov-modules/db-scripts to populate lportal database. Use the following command. source <path to the script file>/lportal.sql;

04. Start mongoDB serve.

05. add initial data from mchoice-sdp/kite/data directory. Import data in these files using mongo import command. db name is kite and collection name is the file name without extension.

	Run the kite/data/test/kite_init_data.sh scrip file to import mongo db.
	important : change the routing_keys.json & system_configurations.json scripts, if any changes to be done according to your environment.

06. Copy all the deployed modules to the liferay tomcat webapp folder from already deployed place $LIFERAY_HOME/tomcat.x.x/webapp (Including with ROOT folder).

07. The following themes are used for provisioning and should be included in proper place as following.

	webapp/ROOT/html/VAADIN/themes/runo
	webapp/ROOT/html/VAADIN/themes/provisioning

08. Change the DNS,CAS sever and mysql properties in portal-ext.properties file in side the ROOT folder.

	webapp/ROOT/WEB-INF/classes/portal-ext.properties

09. Configure following host names in your /etc/hosts as ur deployment environment.

    	192.168.0.95       cur.dlg
    	192.168.0.95       prov.sdp.dlg
    	192.168.0.95       workflowdb.sdp.dlg
    	192.168.0.95       pgwc.dlg
	    192.168.0.95       pgw.dlg
    	192.168.0.95       store.app.dlg

10. -Change the following DNS according to the environment

    - login-hook/WEB-INF/classes/portal.properties
        cur.cas.login.url = https://dev.sdp.hsenidmobile.com/cas/login

    - webapp/prov/WEB-INF/provisioning.properties
        spInformationExternalUrl=http://dev.sdp.hsenidmobile.com/registration/default/#viewuser/
        application.publish.url=https://dev.sdp.hsenidmobile.com/appstore/internal/#publish-application_
        email.provisioning.url=http://dev.sdp.hsenidmobile.com/provisioning/c/portal/login

    - create.configure.payment.instruments.link = https://dev.sdp.hsenidmobile.com/hms-pgw-consumer-selfcare#add-payment --> inside the following files
     	dialog-ussd/WEB-INF/classes/ussd-prov.properties
      	dialog-sms/WEB-INF/classes/sms-prov.properties
      	subscription/WEB-INF/classes/subscription_en_US.properties
     	dialog-wap-push/WEB-INF/classes/wap-push_en_US.properties

11. Set proper paths for log4j.xml in each modules
	Example path : webapp/prov/WEB-INF/classes/log4j.xml
	important : Remove log4j.properties files in each modules.