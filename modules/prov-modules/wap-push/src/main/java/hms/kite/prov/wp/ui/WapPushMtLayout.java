/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.wp.ui;

import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.IntegerValidator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.TextField;
import hms.kite.provisioning.commons.ui.AbstractBasicDetailsLayout;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.component.charging.ChargingLayout;
import hms.kite.provisioning.commons.ui.validator.MessageLimitValidator;
import hms.kite.provisioning.commons.ui.validator.TpsTpdValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.prov.wp.WapPushServiceRegistry.*;
import static hms.kite.prov.wp.ui.util.WapPushSlaRoles.*;
import static hms.kite.provisioning.commons.util.StringConverter.*;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class WapPushMtLayout extends AbstractBasicDetailsLayout {

    private static final Logger logger = LoggerFactory.getLogger(WapPushMtLayout.class);

    private static final String WIDTH = "280px";

    private String corporateUserId;

    private Form form;

    private TextField defaultSenderAddressTxtFld;
    private TextField aliasingTxtFld;
    private TextField mtTpsTxtFld;
    private TextField mtTpdTxtFld;
    private TextField deliveryReportUrlTxtFld;

    private CheckBox deliveryReportChkBox;

    private ChargingLayout chargingLayout;

    private void validateDefaultSenderAddressOrAliasingForNonEmpty() {
        boolean defaultSenderAddressEmpty = isFieldEmpty(defaultSenderAddressTxtFld);
        boolean aliasingEmpty = isFieldEmpty(aliasingTxtFld);
        if (defaultSenderAddressEmpty && aliasingEmpty) {
            throw new Validator.InvalidValueException(
                    application.getMsg("wapPush.default.sender.address.and.aliasing.field.are.empty"));
        }
    }

    private boolean isFieldEmpty(Field field) {
        return isFieldNotNull(field) && (field.getValue() == null || field.getValue().toString().trim().length() == 0);
    }

    private void init() {
        form = new Form();
        form.addStyleName("sms-ncs-form");
        addComponent(form);

        createDefaultSenderAddressFld();
        createAliasingFld();
        createMaxTpsFld();
        createMaxTpdFld();
        createDeliveryReportView();
        createChargingView();
    }

    private void createDefaultSenderAddressFld() {
        defaultSenderAddressTxtFld = getFieldWithPermission(DEFAULT_SENDER_ADDRESS, new TextField());
        if (isFieldNotNull(defaultSenderAddressTxtFld)) {
            defaultSenderAddressTxtFld.setCaption(application.getMsg("wap.push.default.sender.address.caption"));
            defaultSenderAddressTxtFld.setWidth(WIDTH);
            defaultSenderAddressTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("default.sender.address"),
                    application.getMsg("wapPush.default.sender.address.validation")));
            defaultSenderAddressTxtFld.setRequired(false);
            form.addField("defaultSenderAddressTxtFld", defaultSenderAddressTxtFld);
        }
    }

    private void createAliasingFld() {
        aliasingTxtFld = getFieldWithPermission(ALIASING, new TextField());
        if (isFieldNotNull(aliasingTxtFld)) {
            aliasingTxtFld.setCaption(application.getMsg("wap.push.aliasing.caption"));
            aliasingTxtFld.setWidth(WIDTH);
            aliasingTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("alias"),
                    application.getMsg("wapPush.aliasing.validation")));
            aliasingTxtFld.setRequired(false);
            form.addField("aliasingTxtFld", aliasingTxtFld);
        }
    }

    private void createMaxTpsFld() {
        mtTpsTxtFld = getFieldWithPermission(MT_TPS, new TextField());
        if (isFieldNotNull(mtTpsTxtFld)) {
            mtTpsTxtFld.setCaption(application.getMsg("wap.push.max.message.per.second.caption"));
            mtTpsTxtFld.setWidth(WIDTH);
            mtTpsTxtFld.setRequired(true);
            mtTpsTxtFld.setRequiredError(application.getMsg("wapPush.max.messages.per.second.mandatory.validation"));
            mtTpsTxtFld.addValidator(new IntegerValidator(application.getMsg("wapPush.max.messages.per.sec.integer.validation")));
            mtTpsTxtFld.addValidator(new MessageLimitValidator(application.getMsg("wapPush.max.message.per.second.limit.validation"),
                    corporateUserId, smsK, mtK, tpsK));
            mtTpsTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("positiveNumberValidation"),
                    application.getMsg("wapPush.max.messages.per.sec.integer.validation")));
            mtTpsTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("sp.wp.zero.validation"),
                    application.getMsg("wp.sla.zeroValue.validation.error.message")));
            if (isDefaultMtTpsAllow()) {
                mtTpsTxtFld.setValue(getDefaultMtTps());
            }
            form.addField("mtTpsTxtFld", mtTpsTxtFld);
        }
    }

    private void createMaxTpdFld() {
        mtTpdTxtFld = getFieldWithPermission(MT_TPD, new TextField());
        if (isFieldNotNull(mtTpdTxtFld)) {
            mtTpdTxtFld.setCaption(application.getMsg("wap.push.max.messages.per.day.caption"));
            mtTpdTxtFld.setWidth(WIDTH);
            mtTpdTxtFld.setRequired(true);
            mtTpdTxtFld.setRequiredError(application.getMsg("wapPush.max.messages.per.day.mandatory.validation"));
            mtTpdTxtFld.addValidator(new IntegerValidator(application.getMsg("wapPush.max.messages.per.day.integer.validation")));
            mtTpdTxtFld.addValidator(new MessageLimitValidator(application.getMsg("wapPush.max.messages.per.day.limit.validation"),
                    corporateUserId, smsK, mtK, tpdK));
            mtTpdTxtFld.addValidator(new TpsTpdValidator(mtTpsTxtFld, mtTpdTxtFld, application.getMsg("wapPush.tps.tpd.validation")));
            mtTpdTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("positiveNumberValidation"),
                    application.getMsg("wapPush.max.messages.per.day.integer.validation")));
            mtTpdTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("sp.wp.zero.validation"),
                    application.getMsg("wp.sla.zeroValue.validation.error.message")));
            if (isDefaultMtTpdAllow()) {
                mtTpdTxtFld.setValue(getDefaultMtTpd());
            }
            form.addField("mtTpdTxtFld", mtTpdTxtFld);
        }
    }

    private void createDeliveryReportView() {
        deliveryReportChkBox = getFieldWithPermission(DELIVERY_REPORT, new CheckBox());
        if (isFieldNotNull(deliveryReportChkBox)) {
            deliveryReportChkBox.setCaption(application.getMsg("wap.push.dr.requested.caption"));
            deliveryReportChkBox.setImmediate(true);
            deliveryReportChkBox.addListener(new Property.ValueChangeListener() {

                @Override
                public void valueChange(Property.ValueChangeEvent event) {
                    if (isFieldNotNull(deliveryReportUrlTxtFld)) {
                        boolean value = (Boolean) event.getProperty().getValue();

                        deliveryReportUrlTxtFld.setVisible(value);
                        deliveryReportUrlTxtFld.setRequired(value);
                    }
                }
            });
            form.addField("deliveryReportChkBox", deliveryReportChkBox);

            deliveryReportUrlTxtFld = getFieldWithPermission(DELIVERY_REPORT_URL, new TextField());
            if (isFieldNotNull(deliveryReportUrlTxtFld)) {
                deliveryReportUrlTxtFld.setCaption(application.getMsg("wap.push.dr.url.caption"));
                deliveryReportUrlTxtFld.setWidth(WIDTH);
                deliveryReportUrlTxtFld.setRequiredError(application.getMsg("wapPush.drUrl.mandatory.validation"));

                deliveryReportUrlTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("urlValidation"),
                        application.getMsg("wapPush.drUrl.malformed.validation")));
                deliveryReportUrlTxtFld.setVisible(false);
                form.addField("deliveryReportUrlTxtFld", deliveryReportUrlTxtFld);
            }

            deliveryReportChkBox.setValue(getDefaultDeliveryReport());
        }
    }

    private void createChargingView() {
        chargingLayout = ChargingLayout.wapPushMtChargingLayout(form.getLayout(), operatorId(), application, corporateUserId,
                MT_CHARGING, WIDTH, 3, 21);
        if (isDefaultMtChargingAllow()) {
            setValueToField(chargingLayout, getDefaultMtCharging());
        }
    }

    public WapPushMtLayout(BaseApplication application, String corporateUserId) {
        super(application, WAP_PUSH_PERMISSION_ROLE_PREFIX);

        this.corporateUserId = corporateUserId;

        init();
    }

    @Override
    public void setData(Map<String, Object> data) {
        Map<String, Object> mtMap = (Map<String, Object>) data.get(mtK);

        if (mtMap == null) {
            return;
        }

        setValueToField(defaultSenderAddressTxtFld, mtMap.get(defaultSenderAddressK));
        setValueToField(aliasingTxtFld, convertToString((List<String>) mtMap.get(aliasingK), JOIN_SEPARATOR));
        setValueToField(mtTpsTxtFld, mtMap.get(tpsK));
        setValueToField(mtTpdTxtFld, mtMap.get(tpdK));

        if (mtMap.get(deliveryReportUrlK) != null) {
            setValueToField(deliveryReportChkBox, true);
            setValueToField(deliveryReportUrlTxtFld, mtMap.get(deliveryReportUrlK));
        } else {
            setValueToField(deliveryReportChkBox, false);
        }
        setValueToField(chargingLayout, mtMap.get(chargingK));
    }

    @Override
    public void getData(Map<String, Object> data) {
        Map<String, Object> mtMap = new HashMap<String, Object>();

        getValueFromField(defaultSenderAddressTxtFld, mtMap, defaultSenderAddressK);
        getValueFromField(aliasingTxtFld, mtMap, aliasingK, convertToList(aliasingTxtFld.getValue().toString(), SEPARATOR));
        getValueFromField(mtTpsTxtFld, mtMap, tpsK);
        getValueFromField(mtTpdTxtFld, mtMap, tpdK);

        if (deliveryReportChkBox.booleanValue()) {
            getValueFromField(deliveryReportUrlTxtFld, mtMap, deliveryReportUrlK);
        }
        getValueFromField(chargingLayout, mtMap, chargingK);

        data.put(mtK, mtMap);
    }

    @Override
    public void validate() {
        try {
            validateField(defaultSenderAddressTxtFld);
            validateField(aliasingTxtFld);
            validateDefaultSenderAddressOrAliasingForNonEmpty();
            validateField(mtTpsTxtFld);
            validateField(mtTpdTxtFld);
            if (isFieldNotNull(deliveryReportChkBox) && deliveryReportChkBox.booleanValue()) {
                validateField(deliveryReportUrlTxtFld);
            }
            validateField(chargingLayout);
            form.setComponentError(null);
        } catch (Validator.InvalidValueException e) {
            form.setComponentError(e);
            throw e;
        }
    }

    @Override
    public void reset() {
        resetFields("", defaultSenderAddressTxtFld, aliasingTxtFld, mtTpsTxtFld, mtTpdTxtFld, deliveryReportUrlTxtFld);
        if (isDefaultMtChargingAllow()) {
            setValueToField(chargingLayout, getDefaultMtCharging());
        }
    }

    @Override
    public void setPermissions() {
        setReadWritePermissionToComponent(defaultSenderAddressTxtFld, aliasingTxtFld, mtTpsTxtFld,
                mtTpdTxtFld, deliveryReportChkBox, deliveryReportUrlTxtFld);
        chargingLayout.setPermissions();
    }
}
