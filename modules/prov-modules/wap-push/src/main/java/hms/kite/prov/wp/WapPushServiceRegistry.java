/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.wp;

import hms.kite.provisioning.commons.ValidationRegistry;
import hms.kite.provisioning.commons.impl.ValidationRegistryImpl;
import hms.kite.util.NullObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class WapPushServiceRegistry {

    private static String operatorId;
    private static ValidationRegistry validationRegistry;
    private static Properties wapPushDefaultProperties;
    private static Map<String, Object> defaultMtCharging;

    static {
        setOperatorId("");
        setValidationRegistry(new NullObject<ValidationRegistryImpl>().get());
        setWapPushDefaultProperties(new NullObject<Properties>().get());
        setDefaultMtCharging(new HashMap<String, Object>());
    }

    public static String operatorId() {
        return operatorId;
    }

    private static void setOperatorId(String operatorId) {
        WapPushServiceRegistry.operatorId = operatorId;
    }

    public static ValidationRegistry validationRegistry() {
        return validationRegistry;
    }

    private static void setValidationRegistry(ValidationRegistry validationRegistry) {
        WapPushServiceRegistry.validationRegistry = validationRegistry;
    }

    private static void setWapPushDefaultProperties(Properties wapPushDefaultProperties) {
        WapPushServiceRegistry.wapPushDefaultProperties = wapPushDefaultProperties;
    }

    public static boolean isDefaultMtTpsAllow() {
        return Boolean.parseBoolean(wapPushDefaultProperties.getProperty("wap.push.default.mt.tps.allow"));
    }

    public static String getDefaultMtTps() {
        return wapPushDefaultProperties.getProperty("wap.push.default.mt.tps");
    }

    public static boolean isDefaultMtTpdAllow() {
        return Boolean.parseBoolean(wapPushDefaultProperties.getProperty("wap.push.default.mt.tpd.allow"));
    }

    public static String getDefaultMtTpd() {
        return wapPushDefaultProperties.getProperty("wap.push.default.mt.tpd");
    }

    public static boolean getDefaultDeliveryReport() {
        return Boolean.parseBoolean(wapPushDefaultProperties.getProperty("wap.push.default.delivery.report.enable"));
    }

    public static boolean isDefaultMtChargingAllow() {
        return Boolean.parseBoolean(wapPushDefaultProperties.getProperty("wap.push.default.mt.charging.allow"));
    }

    private static void setDefaultMtCharging(Map<String, Object> defaultMtCharging) {
        WapPushServiceRegistry.defaultMtCharging = defaultMtCharging;
    }

    public static Map<String, Object> getDefaultMtCharging() {
        return defaultMtCharging;
    }
}
