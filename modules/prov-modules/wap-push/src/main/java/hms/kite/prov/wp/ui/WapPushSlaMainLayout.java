/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.wp.ui;

import com.vaadin.data.Validator;
import com.vaadin.ui.*;
import hms.kite.prov.wp.WapPushPortlet;
import hms.kite.prov.wp.ui.common.ViewMode;
import hms.kite.provisioning.commons.ui.VaadinUtil;
import hms.kite.provisioning.commons.ui.event.PortletEventSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.prov.wp.WapPushServiceRegistry.operatorId;
import static hms.kite.prov.wp.ui.common.ViewMode.*;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class WapPushSlaMainLayout extends VerticalLayout {

    private static Logger logger = LoggerFactory.getLogger(WapPushSlaMainLayout.class);

    private WapPushPortlet application;
    private WapPushMtLayout mtLayout;
    private ViewMode viewMode = EDIT;
    private String appId;

    private void init(WapPushPortlet application, String coopUserId) {
        mtLayout = new WapPushMtLayout(application, coopUserId);
        mtLayout.setWidth("80%");
    }

    private void loadComponents() {
        addComponent(mtLayout);
        setComponentAlignment(mtLayout, Alignment.MIDDLE_CENTER);

        Component buttonBar = createButtonBar();
        addComponent(buttonBar);
        setComponentAlignment(buttonBar, Alignment.MIDDLE_CENTER);
    }

    private Component createButtonBar() {
        HorizontalLayout buttonBar = new HorizontalLayout();
        buttonBar.setSpacing(true);

        buttonBar.addComponent(createBackButton());

        if (viewMode.equals(EDIT)) {
            buttonBar.addComponent(createSaveButton());
            buttonBar.addComponent(createResetButton());
        } else if (viewMode.equals(CONFIRM)) {
            buttonBar.addComponent(createConfirmButton());
        }

        return buttonBar;
    }

    private Button createResetButton() {
        Button button = new Button(application.getMsg("wap.push.layout.reset"));
        button.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    logger.debug("Resetting form values");
                    reset();
                } catch (Exception e) {
                    logger.error("Unexpected Error occurred while resetting the wap push sla", e);
                }
            }
        });
        return button;
    }


    private Button createSaveButton() {
        Button button = new Button(application.getMsg("wap.push.layout.save"));
        button.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    logger.debug("Validating Wap Push SLA parameters");
                    save();
                } catch (Exception e) {
                    logger.error("Unexpected Error occurred while configuring wap push sla", e);
                }
            }
        });
        return button;
    }

    private Component createConfirmButton() {
        Button button = new Button(application.getMsg("wap.push.layout.confirm"));
        button.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    logger.debug("Saving Wap Push SLA Parameter values in to the session");
                    confirmed();
                } catch (Exception e) {
                    logger.error("Unexpected Error occurred while configuring wap push sla", e);
                }
            }
        });
        return button;
    }


    private Button createBackButton() {
        Button button = new Button(application.getMsg("wap.push.layout.back"));
        button.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    logger.debug("Redirecting to the previous UI");
                    goBack();
                } catch (Exception e) {
                    logger.error("Unexpected Error occurred while going back", e);
                }
            }
        });
        return button;
    }

    private void goBack() {
        if (viewMode == CONFIRM) {
            reloadComponents(EDIT);
        } else if (viewMode == EDIT || viewMode == VIEW) {
            PortletEventSender.send(new HashMap<String, Object>(), provRefreshEvent, application);
            application.closePortlet();
        }
    }

    private void save() {
        try {
            mtLayout.validate();
            reloadComponents(CONFIRM);
        } catch (Validator.InvalidValueException e) {
            logger.error("Validation Failed for App ID [{}] - Cause [{}]  ", appId, e.getMessage());
        }
    }

    private void confirmed() {
        Map<String, Object> wapPushData = application.getWapPushSlaData(appId);

        if (wapPushData == null) {
            wapPushData = new HashMap<String, Object>();
        }

        mtLayout.getData(wapPushData);

        logger.debug("Configured Wap Push SLA [{}] ", wapPushData);

        wapPushData.put(appIdK, appId);
        wapPushData.put(ncsTypeK, wapPushK);
        wapPushData.put(operatorK, operatorId());
        application.setWapPushSlaData(wapPushData);

        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put(ncsTypeK, wapPushK);
        dataMap.put(statusK, successK);
        dataMap.put(operatorK, operatorId());
        dataMap.put(ncsDataK, wapPushData.get(chargingK));

        PortletEventSender.send(dataMap, ncsConfiguredK, application);
        application.closePortlet();
    }

    private void reset() {
        mtLayout.reset();
    }

    public WapPushSlaMainLayout(WapPushPortlet application, String appId, String coopUserId) {
        this.application = application;
        this.appId = appId;

        init(application, coopUserId);
    }

    public void reloadComponents(ViewMode viewMode) {
        this.viewMode = viewMode;

        removeAllComponents();
        loadComponents();

        if (viewMode.equals(VIEW)) {
            VaadinUtil.setAllReadonly(true, mtLayout);
        } else if (viewMode.equals(EDIT)) {
            VaadinUtil.setAllReadonly(false, mtLayout);
            mtLayout.setPermissions();
        } else if (viewMode.equals(CONFIRM)) {
            VaadinUtil.setAllReadonly(true, mtLayout);
        }
    }

    public void reloadData(Map<String, Object> wapPushSla) {
        mtLayout.setData(wapPushSla);
    }
}
