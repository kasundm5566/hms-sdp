/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.wp;

import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.terminal.gwt.server.PortletApplicationContext2;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import hms.kite.datarepo.RepositoryServiceRegistry;
import hms.kite.datarepo.audit.AuditTrail;
import hms.kite.prov.wp.ui.WapPushMtLayout;
import hms.kite.prov.wp.ui.WapPushSlaMainLayout;
import hms.kite.prov.wp.ui.common.ViewMode;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.LiferayUtil;
import hms.kite.provisioning.commons.ui.UiManagerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import java.util.HashMap;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.ncsRepositoryService;
import static hms.kite.prov.wp.WapPushServiceRegistry.operatorId;
import static hms.kite.prov.wp.ui.common.ViewMode.EDIT;
import static hms.kite.prov.wp.ui.common.ViewMode.VIEW;
import static hms.kite.provisioning.commons.util.ProvCommonUtil.getNcsDataFromSessionByAppId;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class WapPushPortlet extends BaseApplication {

    private static final Logger logger = LoggerFactory.getLogger(WapPushPortlet.class);

    private static final String PORTLET_USER_ID = "portlet_user_id";

    private WapPushSlaMainLayout mainLayout;
    private ThemeDisplay themeDisplay;
    private Panel mainPanel;
    private Panel innerPanel;

    private void setPortletUserId(Long userId) {
        addToSession(PORTLET_USER_ID, userId);
    }

    private Long getPortletUserId() {
        return (Long) getFromSession(PORTLET_USER_ID);
    }

    private void handleCreateNcsRequest(Map<String, Object> eventData) {
        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        innerPanel.removeAllComponents();
        mainLayout = new WapPushSlaMainLayout(this, (String) eventData.get(appIdK), (String) eventData.get(coopUserIdK));
        mainLayout.reloadComponents(EDIT);
        innerPanel.addComponent(mainLayout);
    }

    private void handleReconfigureNcsRequest(Map<String, Object> eventData) {
        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        innerPanel.removeAllComponents();
        String appId = (String) eventData.get(appIdK);
        String corporateUserId = (String) eventData.get(coopUserIdK);
        Map<String, Object> sessionWapPushData = getWapPushSlaData(appId);
        mainLayout = new WapPushSlaMainLayout(this, appId, corporateUserId);
        innerPanel.addComponent(mainLayout);
        if (sessionWapPushData != null) {
            mainLayout.reloadData(sessionWapPushData);
        } else {
            reloadDataFromRepository(eventData);
        }
        mainLayout.reloadComponents(EDIT);
    }

    private void handleViewNcsRequest(Map<String, Object> data) {
        mainPanel.setCaption((String) data.get(ncsPortletTitle));
        reloadNcsData(VIEW, data);
    }

    private void handleSaveNcsRequest(Map<String, Object> data) {
        String operator = operatorId();
        String appId = (String) data.get(appIdK);
        String status = (String) data.get(statusK);

        Map<String, Object> sessionWapPushData = getWapPushSlaData(appId);

        if (deleteK.equals(status)) {
            ncsRepositoryService().update(appId, wapPushK, operator, status);
        } else if (sessionWapPushData != null) {
            sessionWapPushData.put(statusK, data.get(statusK));
            ncsRepositoryService().createNcs(sessionWapPushData, (String) data.get(spIdK));
        } else {
            ncsRepositoryService().update(appId, wapPushK, operator, status);
        }
        removeFromSession(wapPushSlaDataK);
        closePortlet();
    }

    private void reloadNcsData(ViewMode viewMode, Map<String, Object> data) {
        mainPanel.setCaption((String) data.get(ncsPortletTitle));
        innerPanel.removeAllComponents();
        mainLayout = new WapPushSlaMainLayout(this, (String) data.get(appIdK), (String) data.get(coopUserIdK));
        innerPanel.addComponent(mainLayout);
        reloadDataFromRepository(data);
        mainLayout.reloadComponents(viewMode);
    }

    private void reloadDataFromRepository(Map<String, Object> data) {
        String appId = (String) data.get(appIdK);
        Map<String, Object> wapPushSla = RepositoryServiceRegistry.ncsRepositoryService().findNcs(appId, wapPushK, operatorId());
        if (wapPushSla != null) {
            mainLayout.reloadData(wapPushSla);
            setWapPushSlaData(wapPushSla);
        }
    }

    private String operatorPresentationName() {
        return operatorId().substring(0, 1).toUpperCase() + operatorId().substring(1);
    }

    @Override
    protected String getApplicationName() {
        return "wap-push";
    }

    @Override
    public void init() {
        setResourceBundle("US");
        Window main = new Window(getMsg("wapPush.sla.window.title"));
        setMainWindow(main);
        setUiManager(new UiManagerImpl(this));
        String caption = this.getMsg("wap.push.title", operatorPresentationName());
        VerticalLayout mainLayout = new VerticalLayout();
        mainPanel = new Panel("");
        mainLayout.addComponent(mainPanel);
        mainLayout.addStyleName("container-panel");
        mainLayout.setWidth("762px");
        mainLayout.setMargin(true);
        innerPanel = new Panel(caption);
        mainPanel.addComponent(innerPanel);
        innerPanel.addStyleName("child-panel");
        setPortletContext((PortletApplicationContext2) getContext());
        getPortletContext().addPortletListener(this, this);

        getUiManager().clearAll();
        getUiManager().pushScreen(WapPushMtLayout.class.getName(), mainLayout);
    }

    public void handleEventRequest(EventRequest request, EventResponse response, Window window) {
        super.handleEventRequest(request, response, window);

        try {

            String eventName = request.getEvent().getName();
            themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
            Long userId = themeDisplay.getUserId();
            setPortletUserId(userId);

            Map<String, Object> eventData = (HashMap<String, Object>) request.getEvent().getValue();
            logger.debug("WapPush {} portlet Received Event Data is [{}]", operatorId(), eventData);
            AuditTrail.setCurrentUser((String) eventData.get(currentUsernameK), (String) eventData.get(currentUserTypeK));

            String appId = (String) eventData.get(appIdK);
            String ncsType = (String) eventData.get(ncsTypeK);
            String operator = (String) eventData.get(operatorK);

            if (!wapPushK.equals(ncsType) && !operatorId().equals(operator)) {
                logger.debug("Event is not for me [{}/{}]", wapPushK, operatorId());
                return;
            }

            if (appId == null || ncsType == null || !ncsType.equals(wapPushK)
                    || operator == null || !operator.equals(operatorId())) {
                logger.debug("Event Request ignored, since it is not belong to me");
                return;
            }

            if (createNcsEvent.equals(eventName)) {
                handleCreateNcsRequest(eventData);
            } else if (reConfigureNcsEvent.equals(eventName)) {
                handleReconfigureNcsRequest(eventData);
            } else if (saveNcsEvent.equals(eventName)) {
                handleSaveNcsRequest(eventData);
            } else if (viewNcsK.equals(eventName)) {
                handleViewNcsRequest(eventData);
            } else {
                logger.error("Unexpected event [{}], Can't handle", eventName);
            }
        } catch (Exception e) {
            logger.error("Unexpected error occurred while handling event ", e);
        }
    }

    public void closePortlet() {
        innerPanel.removeAllComponents();
        String portletId = LiferayUtil.createPortletId(wapPushK, operatorId());
        logger.debug("closing portlet [{}] by user [{}]", portletId, getPortletUserId());
        themeDisplay.getLayoutTypePortlet().removePortletId(getPortletUserId(), portletId);
    }

    public Map<String, Object> getWapPushSlaData(String appId) {
        return getNcsDataFromSessionByAppId(appId, wapPushSlaDataK, this);
    }

    public void setWapPushSlaData(Map<String, Object> casSlaData) {
        addToSession(wapPushSlaDataK, casSlaData);
    }
}
