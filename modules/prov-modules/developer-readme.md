
Developer README
-----------------

Release Procedure
 1) Update the patch with smb://192.168.0.15/shared/Virtual%20City/prov/build-deps/ROOT in shared location

-Download liferay package

-In order to override default properties of liferay, create a properties file named portal-ext.properties inside
    {LIFERAY_HOME}/tomcat-{version}/webapps/ROOT/WEB-INF/classes

-Add the followings to portal-ext.properties file to override database configurations.

    jdbc.default.driverClassName=com.mysql.jdbc.Driver

    jdbc.default.url=jdbc:mysql://localhost/lportal?useUnicode=true&characterEncoding=UTF-8&useFastDateParsing=false

    jdbc.default.username=<mysql_username>

    jdbc.default.password=<mysql_password>

-Apart from ROOT, remove all the other folders in {LIFERAY_HOME}/tomcat-{version}/webapps to remove
 default portlets.

- To disable firefox autostarting
    Open {LIFERAY_HOME}/tomcat-{version}/bin/setenv.sh, Add -Dexternal-properties=portal-developer.properties to JAVA_OPTS

-Download chameleon theme and place it inside {LIFERAY_HOME}/tomcat-{version}/webapps/ROOT/html/VAADIN/themes

-Add the following lines to portal-ext.properties to make chameleon your default vaadin theme

    vaadin.resources.path=/../../html

    vaadin.theme=chameleon

-To set layout pattern and a default portlet for the left menu, add the followings to portal-ext.properties

   layout.static.portlets.start.column-1=provmenu_WAR_provmenu10SNAPSHOT_INSTANCE_abcd

   default.user.layout.template.id=2_columns_ii

-To enable cas add the following lines to portal-ext.properties

   cas.auth.enabled=true

   cas.login.url=http://localhost:8080/cas/login
   cas.logout.url=http://localhost:8080/cas/logout
   cas.server.name=localhost:8080
   cas.server.url=http://localhost:8080/cas
   cas.service.url=

-Start mysql and create a database named lportal using CREATE DATABASE lportal;

-Run sql script in kite/modules/prov-modules/db-scripts to populate lportal database. Use the following command.
   source <path to the script file>/lportal_script.sql;

-Start mongoDB server

-Build prov-modules

- Copy
    kite/modules/prov-modules/provisioning-theme/provisioning-theme/target/provisioning-theme-1.0-SNAPSHOT.war
    kite/modules/prov-modules/prov-menu/target/prov-menu-1.0-SNAPSHOT.war
    kite/modules/prov-modules/sms/operator-sms/target/dialog-sms.war
    kite/modules/prov-modules/prov/target/prov-1.0.0-SNAPSHOT.war
 to {LIFE_RAY_HOME}/deploy

-Add cur.sdp.dialog to /etc/hosts

-Start liferay server

-When the server is started, click on the sign in link and login using following credentials
    user name : supun
    password  : test


Modifications for liferay-impl.jar
----------------------------------

1.) Removed values of auto.login.hooks property which can be found in 'Auto Login' section of
    {portal-impl.jar}/portal.properties

    Reason : To enable the cas integrated liferay to work with kite/modules/prov-modules/login-hook

2.) Removed all unnecessary loggers except ERROR log level for each super type of loggers
    in {portal-impl.jar}/META-INF/portal-log4j.xml.

    E.g.: Replaced following logger types of com.liferay

            <category name="com.liferay.documentlibrary">
                <priority value="ERROR" />
            </category>

            <category name="com.liferay.documentlibrary.util">
                <priority value="ERROR" />
            </category>

            <category name="com.liferay.documentlibrary.util.CMISHook">
                <priority value="INFO" />
            </category>

          with
            <category name="com.liferay">
		        <priority value="ERROR" />
	        </category>

    2.1) Created the extended version of portal-log4j.xml ; portal-log4j-ext.xml in META-INF folder which is
         created inside {LIFE_RAY_HOME}/webapps/ROOT/WEB-INF/classes/ with the same set of loggers defined in
         portal-log4j.xml with the log level INFO.





INFORMATION
------------

After deploying operator provisioning portlets, a session expired exception come if the user tried to view applications
before creating.