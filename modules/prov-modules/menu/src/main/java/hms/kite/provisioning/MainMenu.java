/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.vaadin.terminal.gwt.server.PortletApplicationContext2;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import hms.kite.provisioning.commons.ui.BaseApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.portlet.*;
import java.util.Locale;
import java.util.Set;

import static hms.kite.provisioning.commons.util.ProvKeyBox.refreshMenuEvent;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class MainMenu extends BaseApplication implements PortletApplicationContext2.PortletListener {

    private static final Logger logger = LoggerFactory.getLogger(MainMenu.class);

    private Set<String> userRoles;
    private String coopUserId;
    private String userType;

    private void loadUserDetails(RenderRequest portletRequest) throws PortalException, SystemException {
        PortletSession portletSession = portletRequest.getPortletSession();

        this.userType = (String) portletSession.getAttribute("kite-user-type", PortletSession.APPLICATION_SCOPE);
        this.userRoles = (Set<String>) portletSession.getAttribute("kite-user-roles", PortletSession.APPLICATION_SCOPE);
        this.coopUserId = (String) portletSession.getAttribute("coop-user-id", PortletSession.APPLICATION_SCOPE);
        logger.info("Received coopUserId {}", this.coopUserId);
        logger.info("Received user roles {}", this.userRoles);
        logger.info("Received user type {}", this.userType);
    }

    private void reloadMenu(Window window) {
        window.removeAllComponents();
        VerticalLayout layout = new VerticalLayout();
        window.addComponent(layout);
        MenuAccordion menuAccordion = new MenuAccordion(this);
        Panel accordionPanel = new Panel("Menu");
        accordionPanel.setWidth("225px");
        accordionPanel.addStyleName("accordion-menu-panel");
        accordionPanel.addComponent(menuAccordion);
        layout.addComponent(accordionPanel);
        window.addComponent(layout);
        VerticalLayout informationLayout = new VerticalLayout();
        informationLayout.setWidth("225px");
        informationLayout.setHeight("300px");

        window.addComponent(informationLayout);
    }

    @Override
    protected String getApplicationName() {
        return "menu";
    }

    @Override
    public void setLocale(Locale locale) {
        super.setLocale(locale);
    }

    @Override
    public void init() {
        Window window = new Window("Main Menu");
        setMainWindow(window);
        setResourceBundle("US");
        PortletApplicationContext2 ctx = (PortletApplicationContext2) getContext();
        ctx.addPortletListener(this, this);
    }

    public void handleRenderRequest(RenderRequest request, RenderResponse response, Window window) {
        try {
            if (userType == null) {
                loadUserDetails(request);
                reloadMenu(window);
            }
        } catch (Exception e) {
            logger.error("Unexpected error occurred while reloading", e);
        }
    }

    public void handleEventRequest(EventRequest request, EventResponse response, Window window) {
        super.handleEventRequest(request, response, window);
        String eventName = request.getEvent().getName();
        if (refreshMenuEvent.equals(eventName)) {
            reloadMenu(window);
        }
    }

    public Set<String> getUserRoles() {
        return userRoles;
    }

    public String getCoopUserId() {
        return coopUserId;
    }

    public String getUserType() {
        return userType;
    }
}
