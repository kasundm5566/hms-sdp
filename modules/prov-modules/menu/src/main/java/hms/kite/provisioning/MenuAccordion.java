/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning;

import com.vaadin.ui.Accordion;
import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;
import hms.kite.provisioning.commons.ui.event.PortletEventSender;
import hms.kite.util.SdpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.util.KiteErrorBox.spNotAvailableErrorCode;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class MenuAccordion extends Accordion {

    private static final Logger logger = LoggerFactory.getLogger(MenuAccordion.class);

    private static final Map<String, String> BUTTON_EVENT_MAP = new HashMap<String, String>();

    private MainMenu mainMenu;
    private String userType;
    private Button lastSelectedMenuButton;
    private Set<String> userRoles;

    static {
        BUTTON_EVENT_MAP.put("provisioning.menu.sp.create.app", "sp-create-app");
        BUTTON_EVENT_MAP.put("provisioning.menu.sp.view.app", "sp-view-app");

        BUTTON_EVENT_MAP.put("provisioning.menu.sp.create.sp", "sp-create-sp");
        BUTTON_EVENT_MAP.put("provisioning.menu.sp.view.sp", "sp-view-sp");
        BUTTON_EVENT_MAP.put("provisioning.menu.sp.edit.sp", "sp-edit-sp");
        BUTTON_EVENT_MAP.put("provisioning.menu.sp.cancel.sp", "sp-cancel-sp");

        BUTTON_EVENT_MAP.put("provisioning.menu.admin.app.new.requests", "admin-new-app-requests");
        BUTTON_EVENT_MAP.put("provisioning.menu.admin.app.change.requests", "admin-change-app-requests");
        BUTTON_EVENT_MAP.put("provisioning.menu.admin.manage.app", "admin-manage-apps");
        BUTTON_EVENT_MAP.put("provisioning.menu.admin.manage.downloadable.app", "admin-view-build-file");

        BUTTON_EVENT_MAP.put("provisioning.menu.admin.sp.new.requests", "admin-new-sp-requests");
        BUTTON_EVENT_MAP.put("provisioning.menu.admin.sp.change.requests", "admin-change-sp-requests");
        BUTTON_EVENT_MAP.put("provisioning.menu.admin.manage.sp", "admin-manage-sps");

        BUTTON_EVENT_MAP.put("provisioning.menu.sp.create.cr", "sp-create-cr");
        BUTTON_EVENT_MAP.put("provisioning.menu.sp.upload.build.file.view", "sp-upload-build-files");
    }

    private void loadComponents() {
        if (userType != null) {
            if (userType.equals(adminUserTypeK)) {
                createSideMenuForAdmin();
            } else if (userType.equals(spUserTypeK)) {
                try {
                    Map<String, Object> sp = spRepositoryService().findSpByCoopUserId(mainMenu.getCoopUserId());
                    createSideMenuForSp(sp);
                } catch (SdpException e) {
                    if (spNotAvailableErrorCode.equals(e.getErrorCode())) {
                        createSideMenuForSp(null);
                    } else {
                        logger.error("Unexpected Error occurred, couldn't create side menu", e);
                    }
                }
            } else {
                logger.error("Unknown user type [{}]", userType);
            }
        } else {
            logger.error("User Type is null, couldn't create side menu");
        }
    }

    private void createSideMenuForAdmin() {
        addTab(createAppMenuForAdmin(), mainMenu.getMsg("provisioning.menu.admin.app"), null);
        addTab(createSpMenuForAdmin(), mainMenu.getMsg("provisioning.menu.admin.sp"), null);
    }

    private void createSideMenuForSp(Map<String, Object> sp) {
        if (sp == null || initialK.equals(sp.get(statusK))) {
            addTab(createSpMenuForSp(), mainMenu.getMsg("provisioning.menu.sp.create.sp"), null);
            return;
        }

        String spStatus = (String) sp.get(statusK);

        if (rejectK.equals(spStatus) || suspendedK.equals(spStatus) || pendingApproveK.equals(spStatus)) {
            addTab(createSpMenuForSp(spStatus), mainMenu.getMsg("provisioning.menu.sp.sp"), null);
        } else if (approvedK.equals(spStatus)) {
            addTab(createAppMenuForSp(sp), mainMenu.getMsg("provisioning.menu.sp.app"), null);
            addTab(createSpMenuForSp(spStatus), mainMenu.getMsg("provisioning.menu.sp.sp"), null);
        } else {
            logger.error("Unknown status in sp [{}] couldn't create menu items", spStatus);
        }
    }

    private void addActionToMenu(VerticalLayout layout, String actionRole, Button actionButton) {
        if (actionRole == null) {
            layout.addComponent(actionButton);
        } else if (userRoles.contains(actionRole)) {
            layout.addComponent(actionButton);
        }
    }

    private VerticalLayout createSpMenuForSp(String status) {
        VerticalLayout layout = new VerticalLayout();
        addActionToMenu(layout, null, createButton("provisioning.menu.sp.view.sp"));
        if (pendingApproveK.equals(status)) {
            addActionToMenu(layout, "ROLE_PROV_SIDE_MENU_CANCEL_SP", createButton("provisioning.menu.sp.cancel.sp"));
        }
        if (rejectK.equals(status)) {
            addActionToMenu(layout, "ROLE_PROV_SIDE_MENU_EDIT_SP", createButton("provisioning.menu.sp.edit.sp"));
        }
        return layout;
    }

    private VerticalLayout createSpMenuForAdmin() {
        VerticalLayout layout = new VerticalLayout();
        addActionToMenu(layout, "ROLE_PROV_SIDE_MENU_NEW_SP_REQUESTS", createButton("provisioning.menu.admin.sp.new.requests"));
        addActionToMenu(layout, "ROLE_PROV_SIDE_MENU_MANAGE_SP", createButton("provisioning.menu.admin.manage.sp"));
        //layout.addComponent(createButton("provisioning.menu.admin.sp.change.requests"));
        return layout;
    }

    private VerticalLayout createAppMenuForAdmin() {
        VerticalLayout layout = new VerticalLayout();
        addActionToMenu(layout, "ROLE_PROV_SIDE_MENU_NEW_APP_REQUESTS", createButton("provisioning.menu.admin.app.new.requests"));
        addActionToMenu(layout, "ROLE_PROV_SIDE_MENU_MANAGE_APP", createButton("provisioning.menu.admin.manage.app"));
        addActionToMenu(layout, "ROLE_PROV_SIDE_MENU_MANAGE_BUILD_FILES",
                createButton("provisioning.menu.admin.manage.downloadable.app"));
        //layout.addComponent(createButton("provisioning.menu.admin.app.change.requests"));
        return layout;
    }

    private VerticalLayout createSpMenuForSp() {
        VerticalLayout layout = new VerticalLayout();
        addActionToMenu(layout, "ROLE_PROV_SIDE_MENU_REGISTER_SP", createButton("provisioning.menu.sp.create.sp"));
        return layout;
    }

    private VerticalLayout createAppMenuForSp(Map<String, Object> sp) {
        VerticalLayout layout = new VerticalLayout();
        addActionToMenu(layout, "ROLE_PROV_SIDE_MENU_CREATE_APP", createButton("provisioning.menu.sp.create.app"));
        addActionToMenu(layout, "ROLE_PROV_SIDE_MENU_VIEW_APP", highLightButton(createButton("provisioning.menu.sp.view.app")));
        if (isServiceAvailable(sp, downloadableK)) {
            addActionToMenu(layout, "ROLE_PROV_SIDE_MENU_UPLOAD_BUILD_FILES",
                    createButton("provisioning.menu.sp.upload.build.file.view"));
        }
        //layout.addComponent(createButton("provisioning.menu.sp.create.cr"));
        return layout;
    }

    private boolean isServiceAvailable(Map<String, Object> sp, String service) {
        return ((List<String>) sp.get(spSelectedServicesK)).contains(service);
    }

    private Button createButton(final String key) {
        final Button button = new Button(mainMenu.getMsg(key));
        button.setImmediate(true);
        button.addStyleName("menu_link");

        button.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                String eventName = BUTTON_EVENT_MAP.get(key);
                if (eventName != null) {
                    PortletEventSender.send(null, eventName, mainMenu);
                    highLightSelectedButton(button);
                }
            }
        });
        return button;
    }

    private void highLightSelectedButton(Button button) {
        if (lastSelectedMenuButton != null) {
            lastSelectedMenuButton.removeStyleName("selected-menu-button");
        }
        button.addStyleName("selected-menu-button");
        lastSelectedMenuButton = button;
    }

    private Button highLightButton(Button button) {
        button.addStyleName("selected-menu-button");
        lastSelectedMenuButton = button;
        return lastSelectedMenuButton;
    }

    public MenuAccordion(MainMenu mainMenu) {
        this.mainMenu = mainMenu;
        this.userType = mainMenu.getUserType();
        this.userRoles = mainMenu.getUserRoles();
        loadComponents();
    }
}
