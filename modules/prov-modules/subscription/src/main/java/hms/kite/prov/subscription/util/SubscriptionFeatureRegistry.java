/*
 *   (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.subscription.util;


import hms.kite.util.NullObject;

import java.util.Properties;

public class SubscriptionFeatureRegistry {

    private static Properties featureProperties;

    static {
        setFeatureProperties(new NullObject<Properties>().get());
    }

    private static void setFeatureProperties(Properties properties) {
        SubscriptionFeatureRegistry.featureProperties = properties;
    }

    public static boolean isNotificationEnabled() {
        return Boolean.parseBoolean(featureProperties.getProperty("subscription.notification.enabled"));
    }

    public static boolean isHelpButtonLinkEnable() {
        return Boolean.parseBoolean(featureProperties.getProperty("subscription.help.link.enabled"));
    }

    public static boolean isTrialPeriodEnabled() {
        return Boolean.parseBoolean(featureProperties.getProperty("subscription.trial.period.enabled"));
    }
}
