/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.prov.subscription;

import hms.kite.provisioning.commons.ValidationRegistry;
import hms.kite.util.NullObject;

import java.util.Properties;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SubscriptionServiceRegistry {

    private static ValidationRegistry validationRegistry;
    private static Properties defaultValuesProperties;

    static {
        setValidationRegistry(new NullObject<ValidationRegistry>().get());
        setDefaultValuesProperties(new NullObject<Properties>().get());
    }

    private static void setValidationRegistry(ValidationRegistry validationRegistry) {
        SubscriptionServiceRegistry.validationRegistry = validationRegistry;
    }

    public static ValidationRegistry validationRegistry() {
        return validationRegistry;
    }

    private static void setDefaultValuesProperties(Properties defaultValuesProperties) {
        SubscriptionServiceRegistry.defaultValuesProperties = defaultValuesProperties;
    }

    public static boolean isDefaultMaxMsgPerDayAllow() {
        return Boolean.valueOf(defaultValuesProperties.getProperty("subscription.default.max.msg.per.day.allow"));
    }

    public static String getDefaultMaxMsgPerDay() {
        return defaultValuesProperties.getProperty("subscription.default.max.msg.per.day");
    }

    public static boolean isDefaultRegResponseAllow() {
        return Boolean.valueOf(defaultValuesProperties.getProperty("subscription.default.reg.response.allow"));
    }

    public static String getDefaultRegResponse() {
        return defaultValuesProperties.getProperty("subscription.default.reg.response");
    }

    public static boolean isDefaultUnRegResponseAllow() {
        return Boolean.valueOf(defaultValuesProperties.getProperty("subscription.default.un.reg.response.allow"));
    }

    public static String getDefaultUnRegResponse() {
        return defaultValuesProperties.getProperty("subscription.default.un.reg.response");
    }

    public static boolean getDefaultAllowHttp() {
        return Boolean.valueOf(defaultValuesProperties.getProperty("subscription.default.allow.http"));
    }

    public static boolean getDefaultAllowSendSubNotification() {
        return Boolean.valueOf(defaultValuesProperties.getProperty("subscription.default.allow.notification"));
    }
}
