/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.subscription;

import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.terminal.gwt.server.PortletApplicationContext2;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import hms.kite.datarepo.audit.AuditTrail;
import hms.kite.prov.subscription.ui.SubscriptionSlaBasicLayout;
import hms.kite.prov.subscription.ui.SubscriptionSlaMainLayout;
import hms.kite.prov.subscription.ui.common.ViewMode;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.LiferayUtil;
import hms.kite.provisioning.commons.ui.UiManagerImpl;
import hms.kite.provisioning.commons.util.ProvKeyBox;
import hms.kite.util.KiteKeyBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.ncsRepositoryService;
import static hms.kite.provisioning.commons.util.ProvCommonUtil.getNcsDataFromSessionByAppId;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class SubscriptionPortlet extends BaseApplication {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionPortlet.class);

    private static final String PORTLET_USER_ID = "portlet_user_id";

    private SubscriptionSlaMainLayout subscriptionSlaMainLayout;
    private ThemeDisplay themeDisplay;
    private Panel mainPanel;
    private Panel innerPanel;

    private void setPortletUserId(Long userId) {
        if (getPortletUserId() == null) {
            addToSession(PORTLET_USER_ID, userId);
        }
    }

    private Long getPortletUserId() {
        return (Long) getFromSession(PORTLET_USER_ID);
    }

    private void handleCreateNcsRequest(Map<String, Object> eventData) {
        logger.debug("Subscription NCS SLA Create request received");

        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        innerPanel.removeAllComponents();
        subscriptionSlaMainLayout = new SubscriptionSlaMainLayout(this, (String) eventData.get(appIdK), (String) eventData.get(coopUserIdK));
        innerPanel.addComponent(subscriptionSlaMainLayout);
        subscriptionSlaMainLayout.reloadComponents(ViewMode.EDIT);
    }

    private void handleReconfigureNcsRequest(Map<String, Object> eventData) {
        logger.debug("Subscription NCS SLA Re-Configure request received");

        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        innerPanel.removeAllComponents();
        String appId = (String) eventData.get(appIdK);
        Map<String, Object> sessionSubscriptionData = getSubscriptionData(appId);
        subscriptionSlaMainLayout = new SubscriptionSlaMainLayout(this, appId, (String) eventData.get(coopUserIdK));
        innerPanel.addComponent(subscriptionSlaMainLayout);

        if (sessionSubscriptionData != null) {
            subscriptionSlaMainLayout.reloadData(sessionSubscriptionData);
        } else {
            reloadDataFromRepository(eventData);
        }
        subscriptionSlaMainLayout.reloadComponents(ViewMode.EDIT);
    }

    private void handleViewNcsRequest(Map<String, Object> eventData) {
        logger.debug("Subscription NCS SLA View request received");

        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        reloadNcsData(ViewMode.VIEW, eventData);
    }

    private void handleSaveNcsRequest(Map<String, Object> eventData) {
        logger.debug("Subscription NCS SLA Save request received");

        String operator = "";
        String ncsType = subscriptionK;
        String appId = (String) eventData.get(appIdK);
        String status = (String) eventData.get(statusK);

        Map<String, Object> sessionSubscriptionData = getSubscriptionData(appId);
        logger.debug("Session Data [{}]", sessionSubscriptionData);
        if (deleteK.equals(status)) {
            ncsRepositoryService().update(appId, ncsType, operator, status);
        }
        if (sessionSubscriptionData != null) {
            sessionSubscriptionData.put(statusK, eventData.get(statusK));
            ncsRepositoryService().createNcs(sessionSubscriptionData, (String) eventData.get(spIdK));
        } else {
            ncsRepositoryService().update(appId, ncsType, operator, status);
        }

        removeFromSession(subscriptionDataK);
        closePortlet();

        logger.debug("Subscription SLA Saving request processed successfully");
    }

    private void reloadNcsData(ViewMode viewMode, Map<String, Object> eventData) {
        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        innerPanel.removeAllComponents();
        subscriptionSlaMainLayout = new SubscriptionSlaMainLayout(this, (String) eventData.get(appIdK), (String) eventData.get(coopUserIdK));
        innerPanel.addComponent(subscriptionSlaMainLayout);
        reloadDataFromRepository(eventData);
        subscriptionSlaMainLayout.reloadComponents(viewMode);
    }

    private void reloadDataFromRepository(Map<String, Object> eventData) {
        String appId = (String) eventData.get(KiteKeyBox.appIdK);
        List<Map<String, Object>> ncsList = ncsRepositoryService().findByAppIdNcsType(appId, subscriptionK);
        if (ncsList != null && ncsList.size() > 0) {
            Map<String, Object> data = ncsList.get(0);

            subscriptionSlaMainLayout.reloadData(data);
            setSubscriptionData(data);
        }
    }

    @Override
    protected String getApplicationName() {
        return "subscription";
    }

    @Override
    public void init() {
        setResourceBundle("US");
        Window main = new Window(getMsg("subscription.title"));
        setMainWindow(main);
        setUiManager(new UiManagerImpl(this));
        String caption = this.getMsg("subscription.title");
        VerticalLayout mainLayout = new VerticalLayout();
        mainPanel = new Panel("");
        mainLayout.addComponent(mainPanel);
        mainLayout.addStyleName("container-panel");
        mainLayout.setWidth("762px");
        mainLayout.setMargin(true);
        innerPanel = new Panel(caption);
        mainPanel.addComponent(innerPanel);
        innerPanel.addStyleName("child-panel");
        setPortletContext((PortletApplicationContext2) getContext());
        getPortletContext().addPortletListener(this, this);

        getUiManager().clearAll();
        getUiManager().pushScreen(SubscriptionSlaBasicLayout.class.getName(), mainLayout);
    }

    public void handleEventRequest(EventRequest request, EventResponse response, Window window) {
        try {
            super.handleEventRequest(request, response, window);
            String eventName = request.getEvent().getName();
            themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
            Long userId = themeDisplay.getUserId();
            setPortletUserId(userId);

            Map<String, Object> eventData = (Map<String, Object>) request.getEvent().getValue();
            logger.debug("Received Event Data is [{}]", eventData);
            AuditTrail.setCurrentUser((String) eventData.get(currentUsernameK), (String) eventData.get(currentUserTypeK));

            String appId = (String) eventData.get(ProvKeyBox.appIdK);
            String ncsType = (String) eventData.get(ProvKeyBox.ncsTypeK);

            if (appId == null || ncsType == null || !ncsType.equals(KiteKeyBox.subscriptionK)) {
                logger.debug("Event Request ignored, since it is not belong to me");
                return;
            }

            if (ProvKeyBox.createNcsEvent.equals(eventName)) {
                handleCreateNcsRequest(eventData);
            } else if (ProvKeyBox.reConfigureNcsEvent.equals(eventName)) {
                handleReconfigureNcsRequest(eventData);
            } else if (saveNcsEvent.equals(eventName)) {
                handleSaveNcsRequest(eventData);
            } else if (viewNcsK.equals(eventName)) {
                handleViewNcsRequest(eventData);
            } else {
                logger.error("Unexpected event [{}], Can't handle", eventName);
            }
        } catch (Exception e) {
            logger.error("Unexpected error occurred while handling event ", e);
        }
    }

    public void closePortlet() {
        if (subscriptionSlaMainLayout != null && subscriptionSlaMainLayout.getLayout() != null) {
            subscriptionSlaMainLayout.getLayout().removeAllComponents();
        }
        themeDisplay.getLayoutTypePortlet().removePortletId(getPortletUserId(), LiferayUtil.createPortletId(subscriptionK));
    }

    public Map<String, Object> getSubscriptionData(String appId) {
        return getNcsDataFromSessionByAppId(appId, subscriptionDataK, this);
    }

    public void setSubscriptionData(Map<String, Object> data) {
        addToSession(subscriptionDataK, data);
    }
}
