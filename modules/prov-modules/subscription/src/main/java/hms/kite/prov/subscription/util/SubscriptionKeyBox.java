package hms.kite.prov.subscription.util;

/**
 * <p>
 *     Subscription KeyBox.
 * </p>
 *
 * @author Manuja
 * @version  $Id$
 */
public class SubscriptionKeyBox {
    public static final String subscriptionTrialPeriodUnitMapK = "subscription-ncs-trial-period-unit-map";
    public static final String subscriptionDefaultTrialPeriodMapK = "subscription-ncs-default-trial-period-map";
}
