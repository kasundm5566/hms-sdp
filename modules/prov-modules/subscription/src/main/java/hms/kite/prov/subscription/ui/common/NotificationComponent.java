/*
 *   (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.prov.subscription.ui.common;

import com.vaadin.data.Property;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.addon.customfield.CustomField;


public class NotificationComponent extends CustomField {

    private static final Logger logger = LoggerFactory.getLogger(NotificationComponent.class);

    private HorizontalLayout horizontalLayout;
    private CheckBox notificationChkBox;
    //private DateField dateField;
    private TextField textField;
    private Boolean checkboxSelected;

    private void init() {
        notificationChkBox = new CheckBox();
        notificationChkBox.setImmediate(true);
        notificationChkBox.addListener(new ValueChangeListener() {

            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                validate();
                Boolean isExpirable = (Boolean) event.getProperty().getValue();
                logger.debug("value changed in NotificationComponent - notificationChkBox {}" , isExpirable);
                setCheckboxSelected(isExpirable);
                if (textField != null) {
                    textField.setVisible(isExpirable);
                    textField.setRequired(isExpirable);
                }
            }
        });

        horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(notificationChkBox);
        horizontalLayout.setImmediate(true);
        setCompositionRoot(horizontalLayout);
    }

    public NotificationComponent() {
        init();
    }

    public TextField getTextField() {
        return textField;
    }

    public void setTextField(TextField textField) {
        this.textField = textField;
    }

    @Override
    public Class<?> getType() {
        return HorizontalLayout.class;
    }

    @Override
    public void setValue(Object value) {
        notificationChkBox.setValue(value);
    }

    @Override
    public Object getValue() {
        return notificationChkBox.getValue();
    }

    public void setCheckboxSelected(Boolean checkboxSelected) {
        this.checkboxSelected = checkboxSelected;
    }

    public Boolean getCheckboxSelected() {
        return checkboxSelected;
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        horizontalLayout.setReadOnly(readOnly);
        notificationChkBox.setReadOnly(readOnly);
        if (textField != null) {
            textField.setReadOnly(readOnly);
        }
    }
}