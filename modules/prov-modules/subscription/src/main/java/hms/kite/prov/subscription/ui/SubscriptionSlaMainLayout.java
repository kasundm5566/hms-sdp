/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.subscription.ui;

import com.vaadin.data.Validator;
import com.vaadin.ui.*;
import hms.kite.prov.subscription.SubscriptionPortlet;
import hms.kite.prov.subscription.ui.common.ViewMode;
import hms.kite.provisioning.commons.ui.VaadinUtil;
import hms.kite.provisioning.commons.ui.event.PortletEventSender;
import hms.kite.provisioning.commons.util.ProvKeyBox;
import hms.kite.util.KiteKeyBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.prov.subscription.util.SubscriptionFeatureRegistry.isNotificationEnabled;
import static hms.kite.provisioning.commons.util.ProvKeyBox.ncsDataK;
import static hms.kite.util.KiteKeyBox.chargingK;
import static hms.kite.util.KiteKeyBox.sendSubNotificationK;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class SubscriptionSlaMainLayout extends Form {

    private static Logger logger = LoggerFactory.getLogger(SubscriptionSlaMainLayout.class);

    private SubscriptionPortlet application;
    private SubscriptionSlaBasicLayout subscriptionSlaBasicLayout;
    private ViewMode viewMode = ViewMode.EDIT;
    private String appId;

    public SubscriptionSlaMainLayout(SubscriptionPortlet application, String appId, String corpUserId) {
        this.application = application;
        this.appId = appId;
        subscriptionSlaBasicLayout = new SubscriptionSlaBasicLayout(application, corpUserId);
        subscriptionSlaBasicLayout.setSizeFull();
    }

    private void loadComponents() {
        VerticalLayout formLayout = new VerticalLayout();
        setLayout(formLayout);
        subscriptionSlaBasicLayout.setWidth("80%");
        getLayout().addComponent(subscriptionSlaBasicLayout);
        formLayout.setComponentAlignment(subscriptionSlaBasicLayout, Alignment.MIDDLE_CENTER);
        Component buttonBar = createButtonBar();
        getFooter().addComponent(buttonBar);
        getFooter().setWidth("100%");
        setSizeFull();
    }

    private Component createButtonBar() {
        VerticalLayout buttonBarContainer = new VerticalLayout();
        HorizontalLayout buttonBar = new HorizontalLayout();
        buttonBarContainer.addComponent(buttonBar);
        buttonBarContainer.setComponentAlignment(buttonBar, Alignment.MIDDLE_CENTER);
        buttonBar.setSpacing(true);
        buttonBar.addComponent(createBackButton());

        if (viewMode.equals(ViewMode.EDIT)) {
            buttonBar.addComponent(createSaveButton());
            buttonBar.addComponent(createResetButton());
        } else if (viewMode.equals(ViewMode.CONFIRM)) {
            buttonBar.addComponent(createConfirmButton());
        }

        return buttonBarContainer;
    }

    private Button createResetButton() {
        Button button = new Button(application.getMsg("subscriptionLayout.reset"));
        button.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    reset();
                } catch (Exception e) {
                    logger.error("Unexpected Error occurred while resetting the subscription sla", e);
                }
            }
        });
        return button;
    }

    private Button createSaveButton() {
        Button button = new Button(application.getMsg("subscriptionLayout.save"));
        button.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    save();
                } catch (Exception e) {
                    logger.error("Unexpected Error occurred while configuring subscription sla", e);
                }
            }
        });
        return button;
    }

    private Component createConfirmButton() {
        Button button = new Button(application.getMsg("subscriptionLayout.confirm"));
        button.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    confirmed();
                } catch (Exception e) {
                    logger.error("Unexpected Error occurred while configuring subscription sla", e);
                    application.closePortlet();
                }
            }
        });
        return button;
    }

    private Button createBackButton() {
        Button button = new Button(application.getMsg("subscriptionLayout.back"));
        button.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    goBack();
                } catch (Exception e) {
                    logger.error("Unexpected Error occurred while going back", e);
                }
            }
        });
        return button;
    }

    private void goBack() {
        if (viewMode == ViewMode.CONFIRM) {
            reloadComponents(ViewMode.EDIT);
        } else if (viewMode == ViewMode.EDIT || viewMode == ViewMode.VIEW) {
            logger.debug("Subscription Portlet being closed");
            PortletEventSender.send(new HashMap(), ProvKeyBox.provRefreshEvent, application);
            application.closePortlet();
        }
    }

    private void save() {
        try {
            subscriptionSlaBasicLayout.validate();
            setComponentError(null);
            reloadComponents(ViewMode.CONFIRM);
        } catch (Validator.InvalidValueException e) {
            setComponentError(e);
            logger.debug("Validation Failed ", e.getMessage());
        }
    }

    private void confirmed() {
        Map<String, Object> subscriptionData = application.getSubscriptionData(appId);

        if (subscriptionData == null) {
            subscriptionData = new HashMap<String, Object>();
        }

        subscriptionSlaBasicLayout.getData(subscriptionData);

        logger.debug("Configured Subscription SLA [{}] ", subscriptionData);

        subscriptionData.put(KiteKeyBox.appIdK, appId);
        subscriptionData.put(KiteKeyBox.ncsTypeK, KiteKeyBox.subscriptionK);

        if(isNotificationEnabled()){
            logger.debug("Subscription Notification Enabled [{}] ", subscriptionSlaBasicLayout.isSubscriptionNotificationEnabled());
            if(!subscriptionSlaBasicLayout.isSubscriptionNotificationEnabled()) {
                subscriptionData.remove(sendSubNotificationK);
            }
            logger.debug("Subscription Data ", subscriptionData);
        }

        application.setSubscriptionData(subscriptionData);

        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put(KiteKeyBox.ncsTypeK, KiteKeyBox.subscriptionK);
        dataMap.put(KiteKeyBox.statusK, KiteKeyBox.successK);
        dataMap.put(ncsDataK, subscriptionData.get(chargingK));

        PortletEventSender.send(dataMap, ProvKeyBox.ncsConfiguredK, application);
        application.closePortlet();
    }

    private void reset() {
        subscriptionSlaBasicLayout.reset();
    }

    public void reloadComponents(ViewMode viewMode) {
        this.viewMode = viewMode;
        getLayout().removeAllComponents();
        getFooter().removeAllComponents();
        loadComponents();
        if (viewMode.equals(ViewMode.VIEW)) {
            VaadinUtil.setAllReadonly(true, subscriptionSlaBasicLayout);
        } else if (viewMode.equals(ViewMode.EDIT)) {
            VaadinUtil.setAllReadonly(false, subscriptionSlaBasicLayout);
            subscriptionSlaBasicLayout.setPermissions();
        } else if (viewMode.equals(ViewMode.CONFIRM)) {
            VaadinUtil.setAllReadonly(true, subscriptionSlaBasicLayout);
        }
    }

    public void reloadData(Map<String, Object> subscriptionData) {
        subscriptionSlaBasicLayout.setData(subscriptionData);
    }
}
