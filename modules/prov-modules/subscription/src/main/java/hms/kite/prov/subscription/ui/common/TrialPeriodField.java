package hms.kite.prov.subscription.ui.common;

import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Select;
import com.vaadin.ui.VerticalLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.addon.customfield.CustomField;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * Custom field implementation for trial period.
 * </p>
 *
 * @author Manuja
 * @version $Id$
 */
public class TrialPeriodField extends CustomField {
    private CheckBox checkBox;
    private final Select trialPeriodUnitSelectField;
    private final Select trialPeriodValueSelectField;
    private final int trailPeriodMaxCountDefaultValue;

    /**
     * <p>
     * Default constructor.
     * </p>
     */
    public TrialPeriodField(final int trailPeriodMaxCountDefaultValue) {
        this.trailPeriodMaxCountDefaultValue = trailPeriodMaxCountDefaultValue;
        trialPeriodUnitSelectField = new Select();
        trialPeriodValueSelectField = new Select();
        initUI();
    }

    private void initUI() {
        VerticalLayout verticalLayout = new VerticalLayout();
        checkBox = new CheckBox();
        checkBox.setValue(false);
        checkBox.setImmediate(true);
        verticalLayout.addComponent(checkBox);
        setCompositionRoot(verticalLayout);
    }

    @Override
    public Class<?> getType() {
        return VerticalLayout.class;
    }

    @Override
    public void addListener(ValueChangeListener listener) {
        checkBox.addListener(listener);
    }

    @Override
    public Object getValue() {
        Boolean trialAllowed = (Boolean) checkBox.getValue();
        Map<String, Object> trials = new HashMap();
        if (trialAllowed) {
            String selectedUnit = (String) trialPeriodUnitSelectField.getValue();
            Integer selectedValue = (Integer) trialPeriodValueSelectField.getValue();
            Map<String, Object> trialPeriodData = new HashMap();
            trialPeriodData.put(trialPeriodUnitK, selectedUnit);
            trialPeriodData.put(trialPeriodValueK, selectedValue);
            trials.put(trialPeriodK, trialPeriodData);
            trials.put(maxTrialCountK, trailPeriodMaxCountDefaultValue);
        } else {
            if (trials.containsKey(trialPeriodK)) {
                trials.remove(trialPeriodK);
            }
            if (trials.containsKey(maxTrialCountK)) {
                trials.remove(maxTrialCountK);
            }
        }
        trials.put(trialAllowedK, trialAllowed);
        return trials;
    }

    @Override
    public void setValue(Object newValue) throws ReadOnlyException, ConversionException {
        Map<String, Object> charging = (Map) newValue;
        if (charging.containsKey(trialK)) {
            Map<String, Object> trials = (Map) charging.get(trialK);
            Boolean trialAllowed= (Boolean) trials.get(trialAllowedK);
            checkBox.setValue(null == trialAllowed ? false : trialAllowed);
            if (trials.containsKey(trialPeriodK)) {
                Map<String, Object> trialPeriodData = (Map) trials.get(trialPeriodK);
                checkBox.setValue(true);
                if (trialPeriodData.containsKey(trialPeriodUnitK)) {
                    trialPeriodUnitSelectField.setValue(trialPeriodData.get(trialPeriodUnitK));
                }
                if (trialPeriodData.containsKey(trialPeriodValueK)) {
                    trialPeriodValueSelectField.setValue((Integer) trialPeriodData.get(trialPeriodValueK));
                }
            }
        }
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        checkBox.setReadOnly(readOnly);
    }

    /**
     * <p>
     * Getter for trialPeriodUnitSelectField.
     * </p>
     *
     * @return trialPeriodUnitSelectField instance
     */
    public Select getTrialPeriodUnitSelectField() {
        return trialPeriodUnitSelectField;
    }

    /**
     * <p>
     * Getter for trialPeriodValueSelectField.
     * </p>
     *
     * @return trialPeriodValueSelectField instance
     */
    public Select getTrialPeriodValueSelectField() {
        return trialPeriodValueSelectField;
    }
}
