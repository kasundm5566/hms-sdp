/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.subscription.ui;

import com.vaadin.data.Property;
import com.vaadin.data.validator.NullValidator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.ui.*;
import hms.kite.prov.subscription.ui.common.NotificationComponent;
import hms.kite.prov.subscription.ui.common.TrialPeriodField;
import hms.kite.provisioning.commons.ui.AbstractBasicDetailsLayout;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.component.charging.ChargingLayout;
import hms.kite.provisioning.commons.ui.component.popup.HelpDetails;
import hms.kite.provisioning.commons.ui.validator.SpLimitValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.datarepo.RepositoryServiceRegistry.systemConfiguration;
import static hms.kite.prov.subscription.SubscriptionServiceRegistry.*;
import static hms.kite.prov.subscription.util.SubscriptionFeatureRegistry.isHelpButtonLinkEnable;
import static hms.kite.prov.subscription.util.SubscriptionSlaRoles.*;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.prov.subscription.util.SubscriptionFeatureRegistry.*;
import static hms.kite.prov.subscription.util.SubscriptionKeyBox.*;


public class SubscriptionSlaBasicLayout extends AbstractBasicDetailsLayout {

    private static Logger logger = LoggerFactory.getLogger(SubscriptionSlaBasicLayout.class);
    private static final String HELP_IMAGE = "help.jpg";

    private static final String WIDTH = "280px";
    private static final String HEIGHT = "50px";
    public static final String HELP_IMG_WIDTH = "900px";
    public static final String HELP_IMG_HEIGHT = "340px";
    public static final String HELP_IMG_STYLE_NAME = "link";
    public static final String HELP_BUTTON_NAME = "Help";

    private String corporateUserId;
    private int spMaxAllowMsgPerDay;

    private Form form;

    private TextField maxMsgPerDayTxtFld;
    private TextField regResponseTxtFld;
    private TextField unRegResponseTxtFld;
    private TextField subNotificationUrlField;

    private CheckBox allowHttpRequestChkBox;
    private NotificationComponent notificationComponent;
    private ChargingLayout chargingLayout;

    // Trial period
    private TrialPeriodField allowTrialPeriodChkBox;
    private Select trialPeriodUnitSelectField;
    private Select trialPeriodValueSelectField;
    Map<String, Integer[]> trialPeriodFieldValues = new HashMap();
    Map<String, Object> trialPeriodDefaultValues = new HashMap();
    private boolean trialPeriodAllowed = false;
    private int trailPeriodMaxCountDefaultValue = 1;

    private void init() {
        logger.debug("Initializing the Subscription SLA Basic Information form");
        try {
            Map<String, Object> sp = spRepositoryService().findSpByCoopUserId(corporateUserId);
            if (sp != null) {
                logger.debug("Returned SP Details [{}]", sp.toString());

                spMaxAllowMsgPerDay = Integer.parseInt((String) sp.get(maxNoOfBcMsgsPerDayK));

                createMainLayout();
            } else {
                logger.error("No Sp Details found with the given corporate user id [{}]", corporateUserId);
            }
        } catch (Exception e) {
            logger.error("Error occurred while loading the subscription sla for given sp \n", e);
        }
    }

    private void createMainLayout() {
        form = new Form();
        form.setStyleName("subs-ncs-form");

        if (isHelpButtonLinkEnable()) {
            createHelperLink();
        }
        createMaxMsgPerDayTxtFld();
        createRegResponseTxtFld();
        createUnRegResponseTxtFld();
        createAllowHttpRequestChkBox();
        createChargingLayout();
        if (isNotificationEnabled())
            createSubscriptionNotificationView();
        if (isTrialPeriodEnabled())
            createTrialPeriodLayout();
        addComponent(form);
    }

    private void createMaxMsgPerDayTxtFld() {
        maxMsgPerDayTxtFld = getFieldWithPermission(MAX_MSG_PER_DAY, new TextField());
        if (isFieldNotNull(maxMsgPerDayTxtFld)) {
            maxMsgPerDayTxtFld.setCaption(application.getMsg("applicationSla.maxMsgPerDay.caption"));
            maxMsgPerDayTxtFld.setWidth(WIDTH);
            maxMsgPerDayTxtFld.setRequired(true);
            maxMsgPerDayTxtFld.setRequiredError(application.getMsg("applicationSla.maxMsgPerDay.mandatory.validation"));
            maxMsgPerDayTxtFld.addValidator(new SpLimitValidator(spMaxAllowMsgPerDay,
                    application.getMsg("applicationSla.maxMsgPerDay.number.validation", String.valueOf(spMaxAllowMsgPerDay))));
            maxMsgPerDayTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("mxMessagePerDayValidation"),
                    application.getMsg("applicationSla.maxMsgPerDay.negativeNumber.validation")));
            maxMsgPerDayTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("sp.subscription.zero.validation"),
                    application.getMsg("subscription.sla.zeroValue.validation.error.message")));
            if (isDefaultMaxMsgPerDayAllow()) {
                maxMsgPerDayTxtFld.setValue(getDefaultMaxMsgPerDay());
            }
            form.addField("maxMessagesPerDay", maxMsgPerDayTxtFld);
        }
    }

    private void createRegResponseTxtFld() {
        regResponseTxtFld = getFieldWithPermission(REG_RESPONSE, new TextField());
        if (isFieldNotNull(regResponseTxtFld)) {
            regResponseTxtFld.setWidth(WIDTH);
            regResponseTxtFld.setHeight(HEIGHT);
            regResponseTxtFld.setCaption(application.getMsg("applicationSla.regResponse.caption"));
            regResponseTxtFld.setRequired(true);
            regResponseTxtFld.setRequiredError(application.getMsg("applicationSla.regResponse.mandatory.validation"));
            regResponseTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("maxCharactorsForMessage"),
                    application.getMsg("applicationSla.regResponse.validation")));
            if (isDefaultRegResponseAllow()) {
                regResponseTxtFld.setValue(getDefaultRegResponse());
            }
            form.addField("regResponse", regResponseTxtFld);
        }
    }

    private void createUnRegResponseTxtFld() {
        unRegResponseTxtFld = getFieldWithPermission(UN_REG_RESPONSE, new TextField());
        if (isFieldNotNull(unRegResponseTxtFld)) {
            unRegResponseTxtFld.setWidth(WIDTH);
            unRegResponseTxtFld.setHeight(HEIGHT);
            unRegResponseTxtFld.setCaption(application.getMsg("applicationSla.unRegResponse.caption"));
            unRegResponseTxtFld.setRequired(true);
            unRegResponseTxtFld.setRequiredError(application.getMsg("applicationSla.unRegResponse.mandatory.validation"));
            unRegResponseTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("maxCharactorsForMessage"),
                    application.getMsg("applicationSla.unRegResponse.validation")));
            if (isDefaultUnRegResponseAllow()) {
                regResponseTxtFld.setValue(getDefaultUnRegResponse());
            }
            form.addField("unRegResponse", unRegResponseTxtFld);
        }
    }

    private void createAllowHttpRequestChkBox() {
        allowHttpRequestChkBox = getFieldWithPermission(ALLOW_HTTP_REQUEST, new CheckBox());
        if (isFieldNotNull(allowHttpRequestChkBox)) {
            allowHttpRequestChkBox.setCaption(application.getMsg("applicationSla.httpRequestAllowed.caption"));
            allowHttpRequestChkBox.setValue(getDefaultAllowHttp());
            form.addField("httpAllowed", allowHttpRequestChkBox);
        }
    }

    private void createHelperLink() {
        Button helpButtonLink = createHelpButtonLink();
        addComponent(helpButtonLink);
        setComponentAlignment(helpButtonLink, Alignment.TOP_RIGHT);
    }

    private Button createHelpButtonLink() {
        final HelpDetails helpDetails = new HelpDetails(application, HELP_IMAGE);
        final Embedded image = helpDetails.getHelpImage();
        Button helpButtonLink = new Button(HELP_BUTTON_NAME);
        helpButtonLink.setStyleName(HELP_IMG_STYLE_NAME);
        helpButtonLink.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent clickEvent) {
                getWindow().addWindow(helpDetails.generateHelpImageSubWindow(image, HELP_IMG_WIDTH, HELP_IMG_HEIGHT));
            }
        });
        return helpButtonLink;
    }


    private void createSubscriptionNotificationView() {

        notificationComponent = getFieldWithPermission(ALLOW_SEND_NOTIFICATION, new NotificationComponent());
        if (isFieldNotNull(notificationComponent)) {
            notificationComponent.setCaption(application.getMsg("sub.send.notification"));
            notificationComponent.setValue(true);
            form.addField("expirable", notificationComponent);
        }

        subNotificationUrlField = getFieldWithPermission(ALLOW_SEND_NOTIFICATION, new TextField());
        if (isFieldNotNull(notificationComponent) && isFieldNotNull(subNotificationUrlField)) {
            subNotificationUrlField.setCaption(application.getMsg("sub.send.notification.caption"));
            subNotificationUrlField.setRequired(true);
            subNotificationUrlField.setRequiredError(application.getMsg("sub.send.notification.url.validation"));
            subNotificationUrlField.addValidator(new NullValidator(application.getMsg("sub.notification.null.error"), false));
            subNotificationUrlField.addValidator(new RegexpValidator(validationRegistry().regex("urlValidation")
                    , application.getMsg("sub.notification.url.malformed.error")));
            form.addField("sendNotificationAllowed", subNotificationUrlField);
            notificationComponent.setTextField(subNotificationUrlField);
        }
    }

    private void createChargingLayout() {
        chargingLayout = ChargingLayout.subscriptionChargingLayout(form.getLayout(), application, corporateUserId, SUBSCRIPTION_PERMISSION_ROLE_PREFIX, WIDTH, 3, 21);
    }

    private void createTrialPeriodLayout() {
        createTrialPeriodConfigurationsAndDefaultValues();
        allowTrialPeriodChkBox = getFieldWithPermission(ALLOW_TRIAL_PERIOD, new TrialPeriodField(trailPeriodMaxCountDefaultValue));
        if (isFieldNotNull(allowTrialPeriodChkBox)) {
            createAllowTrialPeriodCheckBox();

            trialPeriodUnitSelectField = getFieldWithPermission(ALLOW_TRIAL_PERIOD,
                    allowTrialPeriodChkBox.getTrialPeriodUnitSelectField());
            if (isFieldNotNull(trialPeriodUnitSelectField)) {
                createTrialPeriodUnitSelectField();

                trialPeriodValueSelectField = getFieldWithPermission(ALLOW_TRIAL_PERIOD,
                        allowTrialPeriodChkBox.getTrialPeriodValueSelectField());
                if (isFieldNotNull(allowTrialPeriodChkBox) &&
                        isFieldNotNull(trialPeriodUnitSelectField) && isFieldNotNull(trialPeriodValueSelectField)) {
                    createTrialPeriodValueSelectField();
                }
            }

        }
        if (trialPeriodAllowed && isFieldNotNull(allowTrialPeriodChkBox)) {
            Map<String, Object> charging = new HashMap();
            Map<String, Object> trials = new HashMap();
            Map<String, Object> trialPeriodData = new HashMap();
            trialPeriodData.put(trialPeriodUnitK, trialPeriodDefaultValues.get(trialPeriodUnitK));
            trialPeriodData.put(trialPeriodValueK, trialPeriodDefaultValues.get(trialPeriodValueK));
            trials.put(trialPeriodK, trialPeriodData);
            charging.put(trialK, trials);
            allowTrialPeriodChkBox.setValue(charging);
        }
    }

    private void createAllowTrialPeriodCheckBox() {
        allowTrialPeriodChkBox.setCaption(application.getMsg("applicationSla.trialPeriodAllowed.caption"));
        allowTrialPeriodChkBox.addListener(
                new Property.ValueChangeListener() {
                    @Override
                    public void valueChange(Property.ValueChangeEvent event) {
                        Boolean isTrialPeriod = (Boolean) event.getProperty().getValue();
                        logger.debug("value changed in allowTrialPeriodChkBox {}", isTrialPeriod);
                        updateSelectField(trialPeriodUnitSelectField, isTrialPeriod, isTrialPeriod);
                        updateSelectField(trialPeriodValueSelectField, isTrialPeriod, isTrialPeriod);
                        if (!isTrialPeriod) {
                            trialPeriodUnitSelectField.setValue(null);
                            trialPeriodValueSelectField.setValue(null);
                            trialPeriodValueSelectField.removeAllItems();
                        }

                    }
                }
        );
        form.addField("trialPeriodAllowed", allowTrialPeriodChkBox);
    }

    private void createTrialPeriodUnitSelectField() {
        updateSelectField(trialPeriodUnitSelectField, application.getMsg("applicationSla.trialPeriodUnit.caption"),
                true, trialPeriodAllowed, trialPeriodAllowed,
                application.getMsg("applicationSla.trialPeriodUnit.mandatory.validation"));
        for (String trialPeriodUnit : getTrialPeriodUnits()) {
            trialPeriodUnitSelectField.addItem(trialPeriodUnit);
        }
        trialPeriodUnitSelectField.addListener(
                new Property.ValueChangeListener() {
                    @Override
                    public void valueChange(Property.ValueChangeEvent event) {
                        String selectedUnit = (String) event.getProperty().getValue();
                        if (selectedUnit != null) {
                            logger.debug("value changed in allowTrialPeriodChkBox {}", selectedUnit);
                            trialPeriodValueSelectField.removeAllItems();
                            for (Integer trialPeriodValue : getTrialPeriodValues(selectedUnit)) {
                                trialPeriodValueSelectField.addItem(trialPeriodValue);
                            }
                        }
                    }
                }
        );
        form.addField("trialPeriodUnitSelectField", trialPeriodUnitSelectField);
    }

    private void createTrialPeriodValueSelectField() {
        updateSelectField(trialPeriodValueSelectField, application.getMsg("applicationSla.trialPeriodValue.caption"),
                true, trialPeriodAllowed, trialPeriodAllowed,
                application.getMsg("applicationSla.trialPeriodValue.mandatory.validation"));
        form.addField("trialPeriodValueSelectField", trialPeriodValueSelectField);
    }

    private void createTrialPeriodConfigurationsAndDefaultValues() {
        logger.debug("Initializing trial period unit field and value field");
        try {
            Map<String, Object> subscriptionTrialPeriodDetailsMap = (Map) systemConfiguration().find(subscriptionTrialPeriodUnitMapK);
            if (subscriptionTrialPeriodDetailsMap.containsKey(trialPeriodK)) {
                Map<String, String> subscriptionTrialPeriodUnitMap =
                        (Map<String, String>) subscriptionTrialPeriodDetailsMap.get(trialPeriodK);
                for (Map.Entry<String, String> entry : subscriptionTrialPeriodUnitMap.entrySet()) {
                    String valueRangeString = entry.getValue();
                    String[] valueRange = valueRangeString.split("\\-");
                    Integer[] actualValueRange = new Integer[0];
                    if (valueRange.length == 1) {
                        actualValueRange = new Integer[1];
                        actualValueRange[0] = Integer.parseInt(valueRange[0]);
                    } else if (valueRange.length == 2) {
                        int lowerBound = Integer.parseInt(valueRange[0]);
                        int upperBound = Integer.parseInt(valueRange[1]);
                        if (lowerBound < upperBound) {
                            actualValueRange = new Integer[(upperBound - lowerBound + 1)];
                            int index = 0;
                            for (int c = lowerBound; c <= upperBound; c++) {
                                actualValueRange[index] = c;
                                index++;
                            }
                        }
                    }
                    trialPeriodFieldValues.put(entry.getKey(), actualValueRange);
                }
            }
            if (subscriptionTrialPeriodDetailsMap.containsKey(maxTrialCountK)) {
                trailPeriodMaxCountDefaultValue = ((Integer) subscriptionTrialPeriodDetailsMap.get(maxTrialCountK));
            }

            Object trailPeriodDefaultValuesObject = systemConfiguration().find(subscriptionDefaultTrialPeriodMapK);
            if (trailPeriodDefaultValuesObject instanceof Map) {
                trialPeriodDefaultValues = (Map) trailPeriodDefaultValuesObject;
                trialPeriodAllowed =
                        trialPeriodDefaultValues.get(trialPeriodUnitK) != null &&
                        trialPeriodDefaultValues.get(trialPeriodValueK) != null;
            }
        } catch (Exception e) {
            logger.error("Error occurred while loading the subscription sla trial period units and values and default values \n", e);
        }
    }

    public SubscriptionSlaBasicLayout(BaseApplication application, String corporateUserId) {
        super(application, SUBSCRIPTION_PERMISSION_ROLE_PREFIX);

        this.corporateUserId = corporateUserId;

        init();
    }

    @Override
    public void setData(Map<String, Object> data) {
        setValueToField(maxMsgPerDayTxtFld, data.get(maxNoOfBcMsgsPerDayK));
        setValueToField(regResponseTxtFld, data.get(subscriptionRespMessageK));
        setValueToField(unRegResponseTxtFld, data.get(unsubscriptionRespMessageK));
        setValueToField(allowHttpRequestChkBox, data.get(allowHttpSubRequestsK));
        setValueToField(chargingLayout, data.get(chargingK));
        setValueToField(notificationComponent, data.get(expireK));
        setValueToField(subNotificationUrlField, data.get(sendSubNotificationK));
        setValueToField(allowTrialPeriodChkBox, data.get(chargingK));
    }

    @Override
    public void getData(Map<String, Object> data) {
        getValueFromField(maxMsgPerDayTxtFld, data, maxNoOfBcMsgsPerDayK);
        getValueFromField(regResponseTxtFld, data, subscriptionRespMessageK);
        getValueFromField(unRegResponseTxtFld, data, unsubscriptionRespMessageK);
        getValueFromField(allowHttpRequestChkBox, data, allowHttpSubRequestsK);
        getValueFromField(chargingLayout, data, chargingK);
        getValueFromField(notificationComponent, data, expireK);
        getValueFromField(subNotificationUrlField, data, sendSubNotificationK);
        getValueFromField(allowTrialPeriodChkBox, (Map<String, Object>) data.get(chargingK), trialK);
    }

    public boolean isSubscriptionNotificationEnabled() {
        return notificationComponent.getCheckboxSelected();
    }

    @Override
    public void validate() {
        form.validate();
        chargingLayout.validate();
    }

    @Override
    public void reset() {
        resetFields("", maxMsgPerDayTxtFld, regResponseTxtFld, unRegResponseTxtFld);
        resetFields(false, allowHttpRequestChkBox, allowTrialPeriodChkBox);
    }

    @Override
    public void setPermissions() {
        setReadWritePermissionToComponent(maxMsgPerDayTxtFld, regResponseTxtFld, unRegResponseTxtFld,
                allowHttpRequestChkBox, allowTrialPeriodChkBox);
        chargingLayout.setPermissions();
    }

    private void updateSelectField(final Select selectField, final boolean visible,
                                   final boolean required) {
        selectField.setVisible(visible);
        selectField.setRequired(required);
    }

    private void updateSelectField(final Select selectField, final String caption,
                                   final boolean immediate, final boolean visible,
                                   final boolean required, final String requiredError) {
        selectField.setCaption(caption);
        selectField.setImmediate(immediate);
        selectField.setVisible(visible);
        selectField.setRequired(required);
        selectField.setRequiredError(requiredError);
    }

    private Set<String> getTrialPeriodUnits() {
        return trialPeriodFieldValues.keySet();
    }

    private Integer[] getTrialPeriodValues(final String selectedUnit) {
        return trialPeriodFieldValues.get(selectedUnit);
    }
}
