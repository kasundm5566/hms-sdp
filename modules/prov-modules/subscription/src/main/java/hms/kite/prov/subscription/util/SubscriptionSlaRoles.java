/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.subscription.util;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SubscriptionSlaRoles {

    public static final String SUBSCRIPTION_PERMISSION_ROLE_PREFIX = "ROLE_PROV_SUBSCRIPTION";

    public static final String MAX_MSG_PER_DAY = "MAX_MSG_PER_DAY";
    public static final String REG_RESPONSE = "REG_RESPONSE";
    public static final String UN_REG_RESPONSE = "UN_REG_RESPONSE";
    public static final String ALLOW_HTTP_REQUEST = "ALLOW_HTTP_REQUEST";
    public static final String ALLOW_SEND_NOTIFICATION = "ALLOW_SEND_NOTIFICATION";
    public static final String ALLOW_TRIAL_PERIOD= "ALLOW_TRIAL_PERIOD";
}
