#!/bin/bash

#-->Destination dir should be to the webapp folder which deployed the modules
DES_DIR="/home/vcity/md5sum"

#-->Path to the directory that creating the backup of the module
BACKUP_DIR_PATH="/hms/backups/prov-modules/prov"
BACKUP_DIR=$(date +"%d-%m-%Y_%H-%M-%S")
SOURCE_DIR="$DES_DIR/patch"

mkdir $BACKUP_DIR_PATH/$BACKUP_DIR

#-->Remove the unnecessary file from the module
remove() {
while read line
do
path=${line#*desDir\/}
echo "Removing... $DES_DIR/$path"
rm -f $DES_DIR/$path
done < "$SOURCE_DIR/remove-path.txt"
#rm -rf $SOURCE_DIR/prov
}

#-->Apply the patches to the module
patch() {
while read line
do
path=${line#*target\/}
echo "Moving.... $DES_DIR/$path"
cp $SOURCE_DIR/$path $DES_DIR/$path
done < "$SOURCE_DIR/patch-path.txt"
#rm -f $SOURCE_DIR/patch-path.txt
echo [INFO] Deployment done.
}

configure() {
echo [INFO] Starting to configure...
while read line
do

countProv=$(echo $line |grep -o "/"|wc -l);
count1=`expr $countProv + 1`
file=$(echo $line |cut -d\/ -f$count1);
path=${line%$file*}

cp $SOURCE_DIR/conf/$file $path

done < "$SOURCE_DIR/configurations.txt"
echo [INFO] Configuration done
}

errorMessage="[ERROR] Error occurred while creating backup and deploying patch"
cp -r $DES_DIR/prov $BACKUP_DIR_PATH/$BACKUP_DIR  && remove && patch || echo $errorMessage

#-->Applying the configuration files
configure