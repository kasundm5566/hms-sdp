##################################################################
################## PATCH DEPLOYMENT AUTOMATION ###################
##################################################################

FOLDERS AND SCRIPTS
======================
prov--
     |-- patch
     |-- ReadMe.txt
     |-- build.xml
     |-- create-patch.sh
     |-- deploy
     |-- deploy-patch.sh
     |-- desDir
     `-- remove.sh


CONFIGURATIONS
====================
01. Configure the following configurations in each scripts at the local machine

 a. build.xml
        <property name="ssh.server.hostname" value="192.168.0.125"/>
        <property name="ssh.server.username" value="vcity"/>
        <property name="ssh.server.password" value="hms12!@"/>
 b. create-patch.sh
        remoteDir="{Full path to the module folder}"
        e.g. {WEB_HOME}/webapp/prov
 c. deploy-patch.sh
        REMOTE_DIR="{path to the contains the patch folder in remote machine}"
        e.g. {WEB_HOME}/webapp/prov/patch

02. Followings should be required at the remote machine which modules deployed.

 a.The folder call "patch" must be created inside the module folder
    e.g. {WEB_HOME/wepapp/prov/patch}
 b.The script call remove.sh should be placed inside above patch folder

03. If there any configuration files to be replaced after patch deployment please follow the following steps

 a. create text file call configurations.txt inside the patch folder
 b. place the full paths with file name line by line for the above text file
    e.g {WEB_HOME}/wepapp/prov/WEB-INF/properties.conf
 c. place the configuration files inside the folder call conf inside the patch folder
    e.g. place all the configuration files in to {WEB_HOME}/webapp/prov/patch/conf to be replaced,

04. To run the deployment automation run ant script at the local machine
 e.g. ant patch

 NOTE: The backup will create for the module in remote machine in following path,
 /hms/backups/prov-modules/prov/
 if it's needed to be change, please change the following path in remove.sh in remote machine
 BACKUP_DIR_PATH="/hms/backups/prov-modules/prov"