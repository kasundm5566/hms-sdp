#!/bin/bash

# This script use to execute the scripts call remove.sh in remote machine
# it's remove the unneccesary files from module of the remote machine and apply the patch

#REMOTE_MACHINE="vcity@192.168.0.125"
REMOTE_DIR="/home/vcity/md5sum/patch"

echo [INFO] Password needed to apply patch on remote computer...
ssh $1 sh $REMOTE_DIR/remove.sh
