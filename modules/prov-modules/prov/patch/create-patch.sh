#!/bin/bash

##################################################################
################## PATCH DEPLOYMENT AUTOMATION ###################
##################################################################

# PATH CONFIGURATIONS
sourceDirectory="../target/prov"
desDirectory="./desDir"
patchDir="./deploy"

#-->Deployed remote location of the module
remoteDir="/home/vcity/installs/liferay-portal-6.0.6/tomcat-6.0.29/webapps/prov"

#-->Remote machine for SSH login
#remoteMachine="vcity@192.168.125"

cp -r $sourceDirectory ./
rm -rf deploy
mkdir deploy

echo "[INFO] PATCH-DEPLOYMENT START..."

#-->Coping the remote module from the remote machine to local folder call desDir
echo "[INFO] Copying remote folder...";

copyDir() {
#for i in $(diff /tmp/provMy.md5 /tmp/prov.md5 | grep '>'| cut -d' ' -f4); do
#rm -f $i;
#echo "Removing... $i"
#done;

echo "[INFO] filtering patch into path $patchDir"

#-->Create patch details inside the deploy folder by taking the differences of the 0
for j in $(diff <(md5deep -r $sourceDirectory) <(md5deep -r $desDirectory) | grep '<'| cut -d' ' -f4); do

    m=$(echo $j | tr \/ ?)

    #path=$(echo $m|sed s/$(echo $pathFrom)/$(echo "prov")/g | tr ? \/)
    path=${j#*target\/}

    #echo $path

    cp --parent $path $patchDir;
done;
echo "[INFO] patch created in path $patchDir"
#-->Finished the creating of patch
}

rsync -avz -e ssh $1:$remoteDir $desDirectory && copyDir || echo [ERROR] Cannot access to remote machine

#md5deep -r $sourceDirectory > /tmp/provMy.md5
#md5deep -r $desDirectory > /tmp/prov.md5

#pathFrom=$(echo $sourceDirectory | tr \/ ?)
#pathTo=$(echo $desDirectory | tr \/ ?)
#pathTo=$(echo $patchDir | tr \/ ?)

#-->Create remove-path.txt which contains the removing files from module
diff <(md5deep -r $sourceDirectory) <(md5deep -r $desDirectory) | grep '>'| cut -d' ' -f4 >./deploy/remove-path.txt
#-->Create patch-path.txt which contains the patch files paths to be deployed in module
diff <(md5deep -r $sourceDirectory) <(md5deep -r $desDirectory) | grep '<'| cut -d' ' -f4 >./deploy/patch-path.txt


rm -rf prov
echo "[INFO] FINISHED..."

