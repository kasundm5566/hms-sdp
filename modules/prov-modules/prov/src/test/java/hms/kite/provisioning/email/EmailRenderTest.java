/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.email;

//import org.mvel2.templates.TemplateRuntime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.antlr.stringtemplate.StringTemplate;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * $LastChangedDate $
 * $LastChangedBy $
 * $LastChangedRevision $
 */
@ContextConfiguration(locations = {"classpath:test-config.xml"})
public class EmailRenderTest extends AbstractTestNGSpringContextTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailRenderTest.class);

    private Map<String, Object> email, data;
    private final String template =
            "Hi $sp.firstname$ $sp.lastname$ \n\n" +
                    "Your application cannot be approved because of $sp.reason$.\n" +
                    "Please register again.\n\n" +
                    "Thank You";

    private final String expentedRenderedTemplate =
            "Hi hSenid Mobile \n\n" +
                    "Your application cannot be approved because of FOR FUN.\n" +
                    "Please register again.\n\n" +
                    "Thank You";

    private final String templateMvel =
            "Hi @{sp.firstname} @{sp.lastname} \n\n" +
                    "Your application cannot be approved because of @{sp.reason}.\n" +
                    "Please register again.\n\n" +
                    "Thank You [@{sp.personal.hometown}]";

    @Test
    public void fun() {

        StringTemplate a = new StringTemplate("$user.name$, $user.phone$");
        HashMap user = new HashMap();
        user.put("name", "Terence");
        user.put("phone", "none-of-your-business");
        a.setAttribute("user", user);
        String results = a.toString();
        LOGGER.info("Rendered Template: {}", results);
    }


    /**
     * The following test case works for the following circumstances
     * 1) Even if a value for an attribute is not found in the map this passes
     */
    @Test(enabled = true)
    public void testSampleRender() {

        String template =
                "Hi $sp.(\"first-name\")$ $sp.lastname$ \n\n" +
                        "Your application cannot be approved because of $app.reason3$.\n" +
                        "Please register again.\n\n" +
                        "Thank You [age = $sp.personal.age$]";

        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("first-name", "hSenid");
        data.put("lastname", "Mobile");
        data.put("reason", "FOR FUN");
        data.put("reason3", "FOR NOT FUN");
        data.put("personal", new HashMap<String, Object>(){{
            put("age", 12);
            put("sex", "female");
        }});
        StringTemplate st = new StringTemplate(template);

        LOGGER.info("StringTemplete: {}", template);
        st.setAttribute("sp", data);
        st.setAttribute("app", data);
        String message = st.toString();

        LOGGER.info("StringTemplete - Rendered Email: {}", message);
    }

    /**
     * The following exception is expected because one of the property
     * sp.lastname is not found
     */
    @Test(enabled = false)//todo ignore till we find a solution
    public void testWhenPropertyHasNoValue() {
        String templateMvel =
                "Hi @{sp.firstname} @{sp.lastname}\n\n" +
                        "Your application cannot be approved because of @{sp.reason}.\n" +
                        "Please register again.\n\n" +
                        "Thank You ";

        LOGGER.info("MVEL Template [Lastname is not there]: {}", templateMvel);
        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("firstname", "hSenid");
        data.put("lastname1", "Mobile");
        data.put("reason", "FOR FUN");
        this.data.put("sp", data);
//        String output = (String) TemplateRuntime.eval(templateMvel, this.data);
//        LOGGER.info("MVEL Template [Lastname is not there]: {}", output);
    }

    @Test(enabled = false)
    public void testWhenPropertyHasUnderscopeCharacter() {
        String templateMvel =
                "Hi @{sp.first_name} \n\n" +
                        "Your application cannot be approved because of @{sp.reason}.\n" +
                        "Please register again.\n\n" +
                        "Thank You ";

        LOGGER.info("MVEL Template [Underscope]: {}", templateMvel);
        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("first_name", "hSenid");
        data.put("lastname", "Mobile");
        data.put("reason", "FOR FUN");
        this.data.put("sp", data);
//        String output = (String) TemplateRuntime.eval(templateMvel, this.data);
//        LOGGER.info("MVEL Template OUTPUT [Underscope]: {}", output);
    }

    @Test(enabled = false)
    public void testWhenPropertyWithDashCharacter() {
        String templateMvel =
                "Hi @{sp.first-\"name} \n\n" +
                        "Your application cannot be approved because of @{sp.reason}.\n" +
                        "Please register again.\n\n" +
                        "Thank You ";

        LOGGER.info("MVEL Template [DASH]: {}", templateMvel);
        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("first-name", "hSenid");
        data.put("lastname", "Mobile");
        data.put("reason", "FOR FUN");
        this.data.put("sp", data);
//        String output = (String) TemplateRuntime.eval(templateMvel, this.data);
//        LOGGER.info("MVEL Template OUTPUT [DASH]: {}", output);
    }



}
