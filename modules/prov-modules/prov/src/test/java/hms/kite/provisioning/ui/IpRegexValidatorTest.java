/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.provisioning.ui;

import com.vaadin.data.Validator;
import com.vaadin.data.validator.RegexpValidator;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class IpRegexValidatorTest {

    private static String ipRegex = "^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";

    @Test
    public void testIpRegex() {
        RegexpValidator regexpValidator = new RegexpValidator(ipRegex, "invalid");
        regexpValidator.validate("1.68.0.251");
        regexpValidator.validate("192.168.0.125");
        regexpValidator.validate("10.12.0.47");
        regexpValidator.validate("172.12.0.251");
    }

    @Test(expected = Validator.InvalidValueException.class)
    public void testInvalidIpRegex() {
        RegexpValidator regexpValidator = new RegexpValidator(ipRegex, "invalid");
        regexpValidator.validate("1.618.0.251");
    }

    @Test
    public void testInvalidStringIpRegex() {
        assertTrue("192.168.0.162".matches(ipRegex));
        assertFalse("192.168.0.262".matches(ipRegex));
    }
}
