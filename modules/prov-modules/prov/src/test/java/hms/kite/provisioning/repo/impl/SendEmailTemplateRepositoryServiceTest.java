/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.repo.impl;

import hms.kite.provisioning.repo.EmailTemplateRepositoryService;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate $
 * $LastChangedBy $
 * $LastChangedRevision $
 */
@ContextConfiguration(locations = {"classpath:test-config.xml"})
public class SendEmailTemplateRepositoryServiceTest extends AbstractTestNGSpringContextTests {

    private Mockery mockery;
    private EmailTemplateRepositoryService repositoryService;

    @BeforeClass
    public void setup () {
        mockery = new Mockery(){{}};
        repositoryService = mockery.mock(EmailTemplateRepositoryService.class);
    }

    private Map<String, Object> getMap (String eventName) {

        HashMap<String, Object> emailSendingEvent = new HashMap<String, Object>();
        List<String> list = new ArrayList<String>();

        list.add("test@testng.com");
        list.add("testing@testng.com");

        emailSendingEvent.put("event-name", eventName);
        emailSendingEvent.put("message-template", "Hi this a empty msg");
        emailSendingEvent.put("to-address-list", list);
        emailSendingEvent.put("cc-address-list", list);
        emailSendingEvent.put("subject", "Test Email");

        return emailSendingEvent;
    }

    @Test
    public void testFindEventByName () {
        final Map<String, Object> event = getMap("ADMIN_REJECT");
        mockery.checking(new Expectations(){{
            allowing(repositoryService).create(event);
            will(returnValue(repositoryService));
            allowing(repositoryService).findEventByName("ADMIN_REJECT");
            will(returnValue(repositoryService));
        }});
        mockery.assertIsSatisfied();
    }

    @Test
    public void testIsEventNameExist () {
        mockery.checking(new Expectations(){{
            allowing(repositoryService).isEventNameExist("ADMIN_REJECT");
            will(returnValue(repositoryService));
        }});
        mockery.assertIsSatisfied();
    }

    @Test
    public void testCreate () {
        final Map<String, Object> event = getMap("ADMIN_ACCEPTS");
        mockery.checking(new Expectations(){{
            allowing(repositoryService).create(event);
            will(returnValue(repositoryService));
        }});
        mockery.assertIsSatisfied();
    }

    @Test
    public void testDeleteEvent () {
        mockery.checking(new Expectations(){{
            allowing(repositoryService).deleteEvent("ADMIN_ACCEPTS");
            will(returnValue(repositoryService));
        }});
        mockery.assertIsSatisfied();
    }

    @Test
    public void updateEvent () {
        final Map<String, Object> event = getMap("ADMIN_ACCEPTS");
        mockery.checking(new Expectations(){{
            allowing(repositoryService).updateEvent(event);
            will(returnValue(repositoryService));
        }});
        mockery.assertIsSatisfied();
    }

}
