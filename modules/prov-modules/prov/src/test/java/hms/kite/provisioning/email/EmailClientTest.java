/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.email;

import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate $
 * $LastChangedBy $
 * $LastChangedRevision $
 */
@ContextConfiguration(locations = {"classpath:test-config.xml"})
public class EmailClientTest extends AbstractTestNGSpringContextTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailClientTest.class);

    private Map<String, Object> emailMassage;

    @Resource
    private EmailClient emailClient;


    @AfterTest
    public void tearDown() {
        //
    }

    @BeforeTest
    public void init () {

        List<String> ccAddressList = new ArrayList<String>();
        List<String> toAddressList = new ArrayList<String>();

        ccAddressList.add("azeemigi@gmail.com");

        emailMassage = new HashMap<String, Object>();
        emailMassage.put(eventNameK, "event-name");
        emailMassage.put(fromAddressK, "appzone.hsenid@gmail.com");
        emailMassage.put(toAddressListK, toAddressList);
        emailMassage.put(ccAddressListK, ccAddressList);
        emailMassage.put(emailSubjectK, "THis is the subject");
        emailMassage.put(emailBodyK, "Hi THis is the email body");

    }

    @Test(enabled = false)
    //TODO: This has a reported bug which is being fixed in 3.1 RC1
    public void testSendEmail () {
        LOGGER.info("Email is being sent: {}", emailMassage);
        try {
            emailClient.sendEmail(emailMassage);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

}
