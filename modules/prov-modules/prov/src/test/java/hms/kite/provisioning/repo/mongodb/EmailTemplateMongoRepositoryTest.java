package hms.kite.provisioning.repo.mongodb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import java.util.Map;

@Test
@ContextConfiguration(locations = "classpath*:test-config.xml")
public class EmailTemplateMongoRepositoryTest extends AbstractTestNGSpringContextTests{

    @Autowired
    EmailTemplateMongoRepository emailTemplateMongoRepository;

    @Test
    public void testFindEventByName() throws Exception {
        Map<String,Object> emailTemplate =
                emailTemplateMongoRepository.findEventByName("app-registration-duplicate-mpesa-paybill-no-event");
        System.out.println(emailTemplate);
    }
}