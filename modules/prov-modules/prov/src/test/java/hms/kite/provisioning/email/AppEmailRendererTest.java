package hms.kite.provisioning.email;

import org.antlr.stringtemplate.StringTemplate;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class AppEmailRendererTest {
    @Test
    public void testRender() throws Exception {
        String emailBody =
                "Dear Admin, \n\n$sp.(\"first-name\")$ has used Paybill number $ncs.(\"mpesa-paybill-number\")$ when provisioning the following \napplication. \nApplication Id: $app.(\"app-id\")$ \nApplication name: $app.name$ \n\nThis Paybill number is already used by $ncs.(\"mpesa-paybill-duplicate-sps\")$. \nPlease resolve this conflict prior to approving $sp.(\"first-name\")$’s application. \n\nPlease login at $system.(\"provisioning-url\")$ \n\nThank you.";


        Map<String, Object> system = new HashMap<String, Object>();
        system.put("provisioning-url", "http://www.google.com");

        Map<String, Object> sp = new HashMap<String, Object>();
        sp.put("first-name", "Isuru");


        Map<String, Object> ncs = new HashMap<String, Object>();
        ncs.put("mpesa-paybill-number", "123456");
        ncs.put("mpesa-paybill-duplicate-sps", "SP_0909, SP_0909");

        Map<String, Object> app = new HashMap<String, Object>();
        app.put("app-id", "APP_009887");
        app.put("name", "Yalu");

        StringTemplate bodyTemplate = new StringTemplate(emailBody);

        bodyTemplate.setAttribute("sp", sp);
        bodyTemplate.setAttribute("ncs", ncs);
        bodyTemplate.setAttribute("system", system);
        bodyTemplate.setAttribute("app", app);

        System.out.println(bodyTemplate.toString());

    }
}
