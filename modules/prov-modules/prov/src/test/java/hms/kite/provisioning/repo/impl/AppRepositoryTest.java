/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.repo.impl;

import static hms.kite.datarepo.RepositoryServiceRegistry.appRepositoryService;
import static hms.kite.provisioning.commons.util.ProvKeyBox.smsConfiguredK;
import static hms.kite.provisioning.commons.util.ProvKeyBox.smsSelectedK;
import static hms.kite.provisioning.commons.util.ProvKeyBox.smsStatusK;
import static hms.kite.provisioning.commons.util.ProvKeyBox.ussdConfiguredK;
import static hms.kite.provisioning.commons.util.ProvKeyBox.ussdSelectedK;
import static hms.kite.provisioning.commons.util.ProvKeyBox.ussdStatusK;
import static hms.kite.util.KiteKeyBox.allowedHostsK;
import static hms.kite.util.KiteKeyBox.appIdK;
import static hms.kite.util.KiteKeyBox.draftK;
import static hms.kite.util.KiteKeyBox.idK;
import static hms.kite.util.KiteKeyBox.nameK;
import static hms.kite.util.KiteKeyBox.pendingApproveK;
import static hms.kite.util.KiteKeyBox.spIdK;
import static hms.kite.util.KiteKeyBox.statusK;
import hms.commons.IdGenerator;
import hms.kite.provisioning.commons.util.ProvIdGenerator;
import hms.kite.provisioning.repo.DbException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;


/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@ContextConfiguration(locations = {"classpath:test-config.xml"})
public class AppRepositoryTest extends AbstractTestNGSpringContextTests {


    @Resource
    private MongoTemplate template;

    private Map<String, Object> app1;

    private Map<String, Object> app2;

    private Map<String, Object> app3;

    private static final Logger logger = LoggerFactory.getLogger(AppRepositoryTest.class);

    @Ignore
//    @BeforeMethod(alwaysRun = true)
    public void setup() {

        app1 = new HashMap<String, Object>();
        app1.put(idK, IdGenerator.generateId());
        app1.put(appIdK, ProvIdGenerator.generateAppId());
        app1.put(spIdK, "sp001");
        app1.put(nameK, "app001_name");

        app1.put(statusK, draftK);
        app1.put(allowedHostsK, "localhost");
        app1.put(smsSelectedK, true);
        app1.put("safaricom-" + smsSelectedK, true);
        app1.put("safaricom-" + smsConfiguredK, true);
        app1.put("safaricom-" + smsStatusK, draftK);
        app1.put(ussdSelectedK, true);
        app1.put("safaricom-" + ussdSelectedK, true);
        app1.put("safaricom-" + ussdConfiguredK, true);
        app1.put("safaricom-" + ussdStatusK, draftK);

        app2 = new HashMap<String, Object>();
        app2.put(idK, IdGenerator.generateId());
        app2.put(appIdK, ProvIdGenerator.generateAppId());
        app2.put(spIdK, "sp001");
        app2.put(nameK, "app002_name");

        app2.put(statusK, draftK);
        app2.put(smsSelectedK, true);
        app2.put(allowedHostsK, "localhost");
        app2.put("safaricom-" + smsSelectedK, true);
        app2.put("safaricom-" + smsConfiguredK, true);
        app2.put("safaricom-" + smsStatusK, draftK);
        app2.put(ussdSelectedK, true);
        app2.put("safaricom-" + ussdSelectedK, true);
        app2.put("safaricom-" + ussdConfiguredK, true);
        app2.put("safaricom-" + ussdStatusK, draftK);

        app3 = new HashMap<String, Object>();
        app3.put(idK, IdGenerator.generateId());
        app3.put(appIdK, ProvIdGenerator.generateAppId());
        app3.put(spIdK, "sp002");
        app3.put(nameK, "app003_name");
        app3.put(allowedHostsK, "localhost");
        app3.put(statusK, draftK);
        app3.put(smsSelectedK, true);
        app3.put("safaricom-" + smsSelectedK, true);
        app3.put("safaricom-" + smsConfiguredK, true);
        app3.put("safaricom-" + smsStatusK, draftK);
        app3.put(ussdSelectedK, true);
        app3.put("safaricom-" + ussdSelectedK, true);
        app3.put("safaricom-" + ussdConfiguredK, true);
        app3.put("safaricom-" + ussdStatusK, draftK);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown() {
        template.dropCollection("app");
    }

    @Test(enabled = false)
    public void testWorkflowCreate() {
        HashMap<String, Object> app = new HashMap<String, Object>();
        app.put(spIdK, "jake");
        app.put(appIdK, "jakesully");
        app.put(nameK, "jakesullyname");
    }

    @Test(enabled = false)
    public void testCreate() throws DbException {
        appRepositoryService().create(app1);
        Map<String, Object> app = appRepositoryService().findById((String) app1.get(idK));
        Assert.assertEquals(app1.get(appIdK), app.get(appIdK));
        Assert.assertEquals(app1.get(statusK), app.get(statusK));
    }

    @Test(enabled = false)
    public void testFindById() throws DbException {
        appRepositoryService() .create(app1);
        Map<String, Object> app = appRepositoryService() .findById((String) app1.get(idK));
        Assert.assertNotNull(app);
        //todo remove lasy loading of app
    }

    @Test(enabled = false)
    public void testFindByAppId() throws DbException {
        appRepositoryService().create(app1);
        Map<String, Object> app = appRepositoryService().findById((String) app1.get(appIdK) );
        Assert.assertNotNull(app);
        Assert.assertEquals(app1.get(nameK), app.get(nameK));
    }

    @Test(enabled = false)
    public void testUpdate() throws DbException {
        appRepositoryService().create(app1);
        Map<String, Object> app = appRepositoryService().findById((String) app1.get(appIdK));
        app.put(statusK, pendingApproveK);
        appRepositoryService().update(app);

        Map<String, Object> updatedApp = appRepositoryService().findById((String) app1.get(appIdK));
        Assert.assertEquals(pendingApproveK, updatedApp.get(statusK));
    }

    @Test(enabled = false)
    public void testFindBySpId() throws DbException {
        appRepositoryService().create(app1);
        //todo add type
        List<Map<String, Object>> appList = appRepositoryService().findBySpId("sp001", "");
        Assert.assertEquals(1, appList.size());

        appRepositoryService().create(app2);
        //todo add type
        appList = appRepositoryService().findBySpId("sp001", "");
        Assert.assertEquals(2, appList.size());

        appRepositoryService().create(app3);
        //todo add type
        appList = appRepositoryService().findBySpId("sp001", "");
        Assert.assertEquals(2, appList.size());
//        todo add type
        appList = appRepositoryService().findBySpId("sp002", "");
        Assert.assertEquals(1, appList.size());
    }

    @Test
    public void testFindByAppName() {
        //todo
    }

    @Ignore
//    @Test
    public void testSearchByAppName() throws DbException {
        List<Map<String, Object>> appList = appRepositoryService().search("app");
        Assert.assertEquals(0, appList.size());

        //todo test for empty results, not matching
        appRepositoryService().create(app1);
        appRepositoryService().create(app2);
        appRepositoryService().create(app3);

        appList = appRepositoryService().search("app");
        Assert.assertEquals(3, appList.size());

        appList = appRepositoryService().search("app00");
        Assert.assertEquals(3, appList.size());

        appList = appRepositoryService().search("p00");
        Assert.assertEquals(3, appList.size());

        appList = appRepositoryService().search("bpp");
        Assert.assertEquals(0, appList.size());

        appList = appRepositoryService().search("app001");
        Assert.assertEquals(1, appList.size());

        appList = appRepositoryService().search("001");
        Assert.assertEquals(1, appList.size());
    }
}
