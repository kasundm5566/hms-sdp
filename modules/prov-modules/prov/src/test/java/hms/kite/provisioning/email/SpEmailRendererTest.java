/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.email;

import hms.kite.util.SdpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.provisioning.ProvisioningServiceRegistry.emailRender;
/**
 * $LastChangedDate $
 * $LastChangedBy $
 * $LastChangedRevision $
 */
@ContextConfiguration(locations = {"classpath:test-config.xml"})
public class SpEmailRendererTest extends AbstractTestNGSpringContextTests {

    private static final Logger logger = LoggerFactory.getLogger(SpEmailRendererTest.class);

    private HashMap<String, Object> sp = new HashMap<String, Object>();
    private Map<String, Object> email = new HashMap<String, Object>();

    private void initializeEmail(String spName) {

        String template =
                "Hi $sp.(\"first-name\")$ $sp.lastname$ " +
                        "Your application cannot be approved because of $sp.reason3$. " +
                        "Please register again. " +
                        "Thank You [age = $sp.personal.age$]\n$admin.footer$";

        logger.info("Template: {}", template);

        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("first-name", "hSenid");
        data.put("lastname", "Mobile");
        data.put(createdByK, spName);
        data.put("reason", "FOR FUN");
        data.put("reason3", "FOR NOT FUN");
        data.put("personal", new HashMap<String, Object>(){{
            put("age", 12);
            put("sex", "female");
        }});

        sp = new HashMap<String, Object>();
        sp.put(spK, data);
        sp.put(adminK, new HashMap());
        sp.put(eventK, new HashMap());

        email.put(emailBodyK, template);
        email.put(toAddressListK, new ArrayList<String>(){{
            add("azeemigi@gmail.com");
        }});
        email.put(ccAddressListK, new ArrayList<String>(){{
            add("azeem@hsenidmobile.com");
        }});
        email.put(emailSubjectK, "Test");
    }

    @Test(enabled = true, expectedExceptions = {SdpException.class})
    public void testRenderWhenSpIsNotInDb(){
        initializeEmail("sp");
        emailRender(spK).render(email, sp);
//        logger.info("Rendered Template: {}", email.get(emailBodyK));
    }

}
