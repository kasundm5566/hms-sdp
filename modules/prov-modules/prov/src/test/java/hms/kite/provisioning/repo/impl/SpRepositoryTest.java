/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.repo.impl;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
//@Ignore
@ContextConfiguration(locations = {"classpath:test-config.xml"})
public class SpRepositoryTest extends AbstractTestNGSpringContextTests {

//    @Autowired
//    private SpRepository repository;

//    @Autowired
//    private MongoTemplate mongoTemplate;
//
//    private Map<String, Object> sp1;
//
//    @BeforeMethod
//    public void setup() {
//        sp1 = new HashMap<String, Object>();
//        sp1.put(KiteKeyBox.coopUserIdK, "coopuser01");
//        sp1.put(spIdK, "sp001");
//        sp1.put(nameK, "sp1");
//        sp1.put(smsSelectedK, true);
//        sp1.put(smsStatusK, draftK);
//        sp1.put(ussdSelectedK, true);
//        sp1.put(ussdStatusK, draftK);
//
//    }
//
//    @AfterMethod
//    public void tearDown() {
//        mongoTemplate.dropCollection("sp");
//    }
//
//    @Test
//    public void testCreate() throws DbException {
//        repository.create(sp1);
//        Map<String, Object> sp = repository.findSpBySpId("sp001");
//
//        Assert.assertEquals(sp1.get(nameK), sp.get(nameK));
//        Assert.assertEquals(sp1.get(statusK), sp.get(statusK));
//        Assert.assertEquals(sp1.get(smsSelectedK), sp.get(smsSelectedK));
//        //todo create with same id, spid, name
//    }
//
//    @Test
//    public void testFindBySpId() {
//        //todo success, id not available,
//    }
//
//    @Test
//    public void testUpdate() throws DbException {
//        repository.create(sp1);
//        Map<String, Object> sp = repository.findSpBySpId("sp001");
//
//        sp.put(statusK, pendingApproveK);
//        repository.update(sp);
//
//        Map<String, Object> updateSp = repository.findSpBySpId("sp001");
//        Assert.assertEquals(pendingApproveK, updateSp.get(statusK));
//
//    }
}
