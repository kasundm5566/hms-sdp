/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.repo.mongodb;

import hms.kite.util.SdpException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.provisioning.ProvisioningServiceRegistry.sendEmailEventRepositoryService;
import static hms.kite.util.KiteKeyBox.*;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertNotSame;

/**
 * $LastChangedDate $
 * $LastChangedBy $
 * $LastChangedRevision $
 */
@ContextConfiguration(locations = {"classpath:test-config.xml"})
public class SendEmailTemplateRepositoryTest extends AbstractTestNGSpringContextTests {

    @BeforeClass
    public void cleanCollection () {
        sendEmailEventRepositoryService().deleteAll();
    }

    @AfterClass
    public void cleanCollectionAfterTest() {
        sendEmailEventRepositoryService().deleteAll();
    }

    private Map<String, Object> getMap (String eventName) {

        HashMap<String, Object> email = new HashMap<String, Object>();
        List<String> list = new ArrayList<String>();

        list.add("test@testng.com");
        list.add("testing@testng.com");

        email.put(eventNameK, eventName);
        email.put(toAddressListK, list);
        email.put(ccAddressListK, list);
        email.put(emailSubjectK, "THis is the subject");
        email.put(emailBodyK, "Hi THis is the email body");

        return email;
    }

    @Test
    public void testIsNameExist () {

        boolean isExist = false;

        isExist = sendEmailEventRepositoryService().isEventNameExist("SP_REGISTRATION_REQ");
        assertEquals(isExist, false);

        Map<String, Object> event = getMap("SP_REGISTRATION_REQ");
        sendEmailEventRepositoryService().create(event);

        isExist = sendEmailEventRepositoryService().isEventNameExist("SP_REGISTRATION_REQ");
        assertEquals(isExist, true);

    }

    @Test
    public void testFindEventByName () {

        Map<String, Object> event = getMap("SP_REGISTRATION_APP");
        sendEmailEventRepositoryService().create(event);
        Map<String, Object> getEvent = sendEmailEventRepositoryService().findEventByName("SP_REGISTRATION_APP");
        assertEquals(getEvent, event);
    }

    @Test(expectedExceptions = {SdpException.class})
    public void testDeleteEvent () {

        Map<String, Object> event = getMap("SP_REGISTRATION_REJ");
        sendEmailEventRepositoryService().create(event);
        Map<String, Object> getEvent = sendEmailEventRepositoryService().findEventByName("SP_REGISTRATION_REJ");
        assertEquals(getEvent, event);
        sendEmailEventRepositoryService().deleteEvent("SP_REGISTRATION_REJ");
        sendEmailEventRepositoryService().findEventByName("SP_REGISTRATION_REJ");

    }

    @Test(expectedExceptions = {IllegalStateException.class})
    public void testCreateEventWithoutMap () {
        sendEmailEventRepositoryService().create(new HashMap<String, Object>());
    }

    @Test
    public void testUpdateEvent () {

        Map<String, Object> event = getMap("SP_REGISTRATION_SUS");
        sendEmailEventRepositoryService().create(event);
        event.put("subject", "subject is changed now");
        sendEmailEventRepositoryService().updateEvent(event);
        Map<String, Object> getEvent = sendEmailEventRepositoryService().findEventByName("SP_REGISTRATION_SUS");
        assertNotSame(getEvent, event);

    }

}
