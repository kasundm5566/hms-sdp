/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.provisioning.ui.common;

import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.terminal.UserError;
import com.vaadin.ui.*;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.util.KiteKeyBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ActiveProdFromLimitedConfirmationDialog {

    private static final Logger logger = LoggerFactory.getLogger(ActiveProdFromLimitedConfirmationDialog.class);

    private BaseApplication application;
    private String header;
    private String message;
    private ActiveProductionConfirmationListener listener;
    private DateField date = new DateField();
    private VerticalLayout confirmationLayout;


    public ActiveProdFromLimitedConfirmationDialog(BaseApplication application, String header, String message, ActiveProductionConfirmationListener listener) {
        this.application = application;
        this.header = header;
        this.message = message;
        this.listener = listener;
    }

    public void showDialog(final Map<String, Object> app) {

        final Window subWindow = new Window();
        subWindow.setCaption(header);
        subWindow.setModal(true);
        subWindow.setWidth("325px");

        confirmationLayout = (VerticalLayout) subWindow.getContent();
        confirmationLayout.setSpacing(true);
        confirmationLayout.setMargin(true);

        date.setDateFormat("yyyy-MM-dd hh:mm a");
        date.setValue(null);
        date.setImmediate(true);
        date.setRequired(true);
        date.setRequiredError(application.getMsg("provisioning.confirm.dialog.Date.required"));
        addDateValidator();

        final Label messageLbl = new Label(message);

        final TextField textArea = new TextField();

        Label noteLbl = new Label(application.getMsg("provisioning.confirm.dialog.note"));

        confirmationLayout.addComponent(messageLbl);
        confirmationLayout.addComponent(date);
        confirmationLayout.addComponent(noteLbl);
        textArea.setWidth("280px");
        textArea.setHeight("100px");
        textArea.setRequired(true);
        textArea.setRequiredError(application.getMsg("provisioning.confirm.dialog.note.required"));
        confirmationLayout.addComponent(textArea);

        HorizontalLayout buttonBar = new HorizontalLayout();
        buttonBar.setSpacing(true);
        confirmationLayout.addComponent(buttonBar);
        confirmationLayout.setComponentAlignment(buttonBar, Alignment.MIDDLE_CENTER);

        Button yesButton = new Button(application.getMsg("provisioning.confirm.dialog.yes"));
        buttonBar.addComponent(yesButton);
        yesButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    textArea.validate();
                    validateDate();
                    HashMap<String, Object> dataMap = new HashMap();
                    dataMap.put(KiteKeyBox.noteK, textArea.getValue());
                    dataMap.put(KiteKeyBox.activeProductionStartDateK, date.getValue());
                    subWindow.getParent().removeWindow(subWindow);
                    listener.onYesConfirmation(dataMap);
                } catch (Validator.InvalidValueException e) {
                    //Nothing to do
                } catch (Exception e) {
                    logger.error("Unexpected error occurred ", e);
                }
            }
        });

        Button noButton = new Button(application.getMsg("provisioning.confirm.dialog.no"));
        buttonBar.addComponent(noButton);
        noButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                subWindow.getParent().removeWindow(subWindow);
            }
        });

        application.getMainWindow().addWindow(subWindow);
    }

    private void addDateValidator() {
        date.addListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                try {
                    validateDate();
                } catch (Validator.InvalidValueException e) {

                }catch (Exception e) {
                    logger.error("Unexpected error occurred ", e);
                }
            }
        });
    }

    private void validateDate() {
        if (((Date) date.getValue()).getTime() >= (new Date().getTime())) {
            logger.debug("Correct date selected.....");
            date.setComponentError(null);
        } else {
            date.setComponentError(new UserError(application.getMsg("provisioning.confirm.dialog.date.error.msg")));
            throw new Validator.InvalidValueException(application.getMsg("provisioning.confirm.dialog.date.error.msg"));
        }
    }
}
