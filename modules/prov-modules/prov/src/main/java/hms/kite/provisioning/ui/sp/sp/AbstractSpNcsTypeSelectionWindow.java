/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/

package hms.kite.provisioning.ui.sp.sp;

import com.vaadin.data.Property;
import com.vaadin.ui.*;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import hms.kite.provisioning.Provisioning;
import hms.kite.util.KiteKeyBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static hms.kite.datarepo.RepositoryServiceRegistry.systemConfiguration;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/*
* $LastChangedDate: 2011-07-13 17:25:30 +0530 (Wed, 13 Jul 2011) $
* $LastChangedBy: chandanaw $
* $LastChangedRevision: 74949 $
* */

public abstract class AbstractSpNcsTypeSelectionWindow {

    private static final Logger logger = LoggerFactory.getLogger(AbstractSpNcsTypeSelectionWindow.class);

    protected Provisioning provisioning;
    protected List spSelectedServices;
    private CheckBox aasChkBox;
    private CheckBox smsChkBox;
    private CheckBox ussdChkBox;
    private CheckBox wapPushChkBox;
    private CheckBox downloadChkBox;
    private CheckBox subscriptionChkBox;
    private CheckBox casChkBox;
    private CheckBox lbsChkBox;
    private CheckBox vdfApiChkBox;

    private CheckBox solturaUserChkBox;
    private CheckBox sdpUserChkBox;

    private Label userTypeLbl;//Create label for user types
    private Label resourceTypeLbl;//Create Label for required resource types

    public AbstractSpNcsTypeSelectionWindow(Provisioning provisioning) {
        this.provisioning = provisioning;
        spSelectedServices = new ArrayList();

        aasChkBox = new CheckBox(provisioning.getMsg("sp.register.sp.aas"));
        smsChkBox = new CheckBox(provisioning.getMsg("sp.register.sp.sms"));
        ussdChkBox = new CheckBox(provisioning.getMsg("sp.register.sp.ussd"));
        wapPushChkBox = new CheckBox(provisioning.getMsg("sp.register.sp.wp"));
        subscriptionChkBox = new CheckBox(provisioning.getMsg("sp.register.sp.subscription"));
        casChkBox = new CheckBox(provisioning.getMsg("sp.register.sp.cas"));
        downloadChkBox = new CheckBox(provisioning.getMsg("sp.register.sp.dlApp"));
        lbsChkBox = new CheckBox(provisioning.getMsg("sp.register.sp.lbs"));
        vdfApiChkBox = new CheckBox(provisioning.getMsg("sp.register.sp.vdfApis"));

        solturaUserChkBox = new CheckBox(provisioning.getMsg("sp.register.sp.solturaUser"));
        solturaUserChkBox.setImmediate(true);
        sdpUserChkBox = new CheckBox(provisioning.getMsg("sp.register.sp.sdpUser"));
        sdpUserChkBox.setImmediate(true);

        userTypeLbl = new Label(provisioning.getMsg("sp.register.sp.usertype"));//Initialize with the value
        userTypeLbl.setHeight("25px");
        resourceTypeLbl = new Label(provisioning.getMsg("sp.register.sp.resourcetype"));//Initialize with the value
        resourceTypeLbl.setHeight("25px");
    }

    public void loadComponents(final AbstractComponentContainer container) {
        container.removeAllComponents();
        Panel panel = new Panel(provisioning.getMsg("sp.register.sp.title"));
        container.addComponent(panel);

        VerticalLayout layout = new VerticalLayout();
        layout.setSpacing(true);

        GridLayout firstCheckBoxGrid = createFirstCheckBoxGrid();
        layout.addComponent(firstCheckBoxGrid);
        layout.setComponentAlignment(firstCheckBoxGrid, Alignment.MIDDLE_CENTER);

        Label layoutSeparator = new Label();
        layout.setWidth("100%");
        layoutSeparator.setStyleName("header-with-border");
        layout.addComponent(layoutSeparator);

        GridLayout secondCheckBoxGrid = createSecondCheckBoxGrid();
        layout.addComponent(secondCheckBoxGrid);
        Component component = createButtonBar(container);
        layout.addComponent(component);
        layout.setComponentAlignment(component, Alignment.MIDDLE_CENTER);

        refreshSolturaRelatedCheckBoxes();
        refreshSdpRelatedCheckBoxes();

        loadData();

        panel.addComponent(layout);
    }

    protected abstract Component createButtonBar(AbstractComponentContainer container);

    private GridLayout createFirstCheckBoxGrid() {
        GridLayout layout = new GridLayout(4, 3);
        layout.setMargin(true, false, true, false);
        layout.setSpacing(true);

        layout.addComponent(sdpUserChkBox, 0, 1);
        layout.addComponent(solturaUserChkBox, 1, 1);
        layout.addComponent(userTypeLbl, 0, 0);

        solturaUserChkBox.addListener(new CheckBox.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                refreshSolturaRelatedCheckBoxes();
            }
        });

        sdpUserChkBox.addListener(new CheckBox.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                refreshSdpRelatedCheckBoxes();
            }
        });

        layout.setWidth("100%");
        return layout;
    }

    private Boolean isSolturaUser() {
        String solturaUser = solturaUserChkBox.getValue().toString();
        if (solturaUser != null && solturaUser.equals(KiteKeyBox.trueK)) {
            return true;
        }
        return false;
    }

    private void refreshSdpRelatedCheckBoxes() {
        if (sdpUserChkBox.getValue().equals(true)) {
            aasChkBox.setEnabled(true);
            smsChkBox.setEnabled(true);
            ussdChkBox.setEnabled(true);
            wapPushChkBox.setEnabled(true);
            downloadChkBox.setEnabled(true);
            subscriptionChkBox.setEnabled(true);
            casChkBox.setEnabled(true);
            lbsChkBox.setEnabled(true);
            vdfApiChkBox.setEnabled(true);
        } else {
            setValueAndEnabled(aasChkBox, false, false);
            setValueAndEnabled(smsChkBox, false, false);
            setValueAndEnabled(ussdChkBox, false, false);
            setValueAndEnabled(wapPushChkBox, false, false);
            setValueAndEnabled(downloadChkBox, false, false);
            setValueAndEnabled(subscriptionChkBox, false, false);
            setValueAndEnabled(casChkBox, false, false);
            setValueAndEnabled(lbsChkBox, false, false);
            setValueAndEnabled(vdfApiChkBox, false, false);
        }
    }

    private void refreshSolturaRelatedCheckBoxes() {
        if (isSolturaUser()) {
            smsChkBox.setReadOnly(false);
            smsChkBox.setValue(true);
            smsChkBox.setReadOnly(true);

            subscriptionChkBox.setReadOnly(false);
            subscriptionChkBox.setValue(true);
            subscriptionChkBox.setReadOnly(true);
        } else {
            smsChkBox.setReadOnly(false);
            smsChkBox.setValue(false);

            subscriptionChkBox.setReadOnly(false);
            subscriptionChkBox.setValue(false);
        }
    }

    void setValueAndEnabled(CheckBox checkBox, Boolean value, Boolean enabled) {
        checkBox.setEnabled(enabled);
        if (!checkBox.isReadOnly()) {
            checkBox.setValue(value);
        }
    }

    private GridLayout createSecondCheckBoxGrid() {
        GridLayout layout = new GridLayout(5, 4);
        layout.addComponent(resourceTypeLbl, 0, 0);
        layout.setMargin(true, false, true, false);
        layout.setSpacing(true);

        Map<String, Object> supportedNcsMap = (Map<String, Object>) systemConfiguration().find(supportNcsAndOperatorsK);
        Set<String> supportedNcses = supportedNcsMap.keySet();

        Point currentPosition = new Point(-1, 1);
        if (supportedNcses.contains(aasK)) {
            currentPosition = addNcsCheckBox(layout, currentPosition, aasChkBox);
        }
        if (supportedNcses.contains(smsK)) {
            currentPosition = addNcsCheckBox(layout, currentPosition, smsChkBox);
        }
        if (supportedNcses.contains(ussdK)) {
            currentPosition = addNcsCheckBox(layout, currentPosition, ussdChkBox);
        }

        if (supportedNcses.contains(wapPushK)) {
            currentPosition = addNcsCheckBox(layout, currentPosition, wapPushChkBox);
        }

        if (supportedNcses.contains(casK)) {
            currentPosition = addNcsCheckBox(layout, currentPosition, casChkBox);
        }

        if (supportedNcses.contains(downloadableK)) {
            currentPosition = addNcsCheckBox(layout, currentPosition, downloadChkBox);
        }

        if (supportedNcses.contains(subscriptionK)) {
            currentPosition = addNcsCheckBox(layout, currentPosition, subscriptionChkBox);
        }
        if(supportedNcses.contains(lbsK)) {
            currentPosition = addNcsCheckBox(layout, currentPosition, lbsChkBox);
        }
        if(supportedNcses.contains(vdfapiK)){
            currentPosition = addNcsCheckBox(layout, currentPosition, vdfApiChkBox);
        }

        layout.setWidth("100%");
        return layout;
    }

    private Point addNcsCheckBox(GridLayout layout, Point currentPosition, CheckBox checkBox) {
        Point nextPosition = getNextPosition(currentPosition);
        layout.addComponent(checkBox, nextPosition.x, nextPosition.y);
        return nextPosition;
    }

    Point getNextPosition(Point point) {
        int col = point.x;
        int row = point.y;
        if (col >= 2) {
            col = 0;
            row++;
        } else {
            col++;
        }
        return new Point(col, row);
    }

    protected boolean validate() {
        if (!anyOneSelect(aasChkBox, smsChkBox, ussdChkBox, wapPushChkBox, casChkBox, downloadChkBox, subscriptionChkBox, lbsChkBox, vdfApiChkBox)) {
            return false;
        }
        return true;
    }

    private boolean anyOneSelect(CheckBox... checkBoxes) {
        for (CheckBox checkBox : checkBoxes) {
            if (checkBox.getValue().equals(true)) {
                return true;
            }
        }
        return false;
    }

    private void loadData() {
        solturaUserChkBox.setValue(getBooleanValue(provisioning.getSp().get(solturaUserK)));
        sdpUserChkBox.setValue(getBooleanValue(provisioning.getSp().get(sdpUserK)));
        aasChkBox.setValue(getBooleanValue(provisioning.getSp().get(aasSelectedK)));
        smsChkBox.setValue(getBooleanValue(provisioning.getSp().get(smsSelectedK)));
        ussdChkBox.setValue(getBooleanValue(provisioning.getSp().get(ussdSelectedK)));
        wapPushChkBox.setValue(getBooleanValue(provisioning.getSp().get(wapPushSelectedK)));
        casChkBox.setValue(getBooleanValue(provisioning.getSp().get(casSelectedK)));
        downloadChkBox.setValue(getBooleanValue(provisioning.getSp().get(downloadableSelectedK)));
        subscriptionChkBox.setValue(getBooleanValue(provisioning.getSp().get(subscriptionSelectedK)));
        lbsChkBox.setValue(getBooleanValue(provisioning.getSp().get(lbsSelectedK)));
        vdfApiChkBox.setValue(getBooleanValue(provisioning.getSp().get(vdfApisSelected)));
    }

    private Boolean getBooleanValue(Object object) {
        if (object != null) {
            return Boolean.parseBoolean(object.toString());
        }
        return false;
    }

    protected void setSessionValues() {
        provisioning.getSp().put(solturaUserK, solturaUserChkBox.getValue().toString());
        provisioning.getSp().put(sdpUserK, sdpUserChkBox.getValue().toString());
        provisioning.getSp().put(aasSelectedK, aasChkBox.getValue().toString());
        provisioning.getSp().put(smsSelectedK, smsChkBox.getValue().toString());
        provisioning.getSp().put(ussdSelectedK, ussdChkBox.getValue().toString());
        provisioning.getSp().put(wapPushSelectedK, wapPushChkBox.getValue().toString());
        provisioning.getSp().put(casSelectedK, casChkBox.getValue().toString());
        provisioning.getSp().put(downloadableSelectedK, downloadChkBox.getValue().toString());
        provisioning.getSp().put(subscriptionSelectedK, subscriptionChkBox.getValue().toString());
        provisioning.getSp().put(lbsSelectedK, lbsChkBox.getValue().toString());
        provisioning.getSp().put(vdfApisSelected, vdfApiChkBox.getValue().toString());

        spSelectedServices.clear();

        checkAndAddSelectedService(aasK, aasChkBox);
        checkAndAddSelectedService(smsK, smsChkBox);
        checkAndAddSelectedService(ussdK, ussdChkBox);
        checkAndAddSelectedService(wapPushK, wapPushChkBox);
        checkAndAddSelectedService(casK, casChkBox);
        checkAndAddSelectedService(downloadableK, downloadChkBox);
        checkAndAddSelectedService(subscriptionK, subscriptionChkBox);
        checkAndAddSelectedService(lbsK, lbsChkBox);
        checkAndAddSelectedService(vdfapiK, vdfApiChkBox);

    }

    protected void checkAndAddSelectedService(String ncsType, CheckBox checkBox) {
        if ((Boolean) checkBox.getValue()) {
            spSelectedServices.add(ncsType);
        }
    }

}
