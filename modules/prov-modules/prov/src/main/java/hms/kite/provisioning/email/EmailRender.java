/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.email;

import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public interface EmailRender {

    /**
     * This abstract method can be implemented that with any logic that renders
     * the email message body
     * @param email
     * @param data
     */
    void render(Map<String, Object> email, Map<String, Object> data);

}
