package hms.kite.provisioning.ui.component;

/**
 * User: azeem
 * Date: 4/18/12
 * Time: 5:18 PM
 */
public interface ValidateComponent {

    void validate();

}
