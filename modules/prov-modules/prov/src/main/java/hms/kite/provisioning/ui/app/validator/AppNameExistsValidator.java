/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.app.validator;

import com.google.common.base.Optional;
import com.vaadin.data.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static hms.kite.datarepo.RepositoryServiceRegistry.appRepositoryService;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class AppNameExistsValidator implements Validator {

    private static final Logger logger = LoggerFactory.getLogger(AppNameExistsValidator.class);

    private String errorMessage;
    private Optional<String> appCurrentName;

    public AppNameExistsValidator(String errorMessage, Optional appCurrentName) {
        this.errorMessage = errorMessage;
        this.appCurrentName = appCurrentName;
    }

    @Override
    public void validate(Object value) throws InvalidValueException {
        if (!isValid(value)) {
            throw new InvalidValueException(errorMessage);
        }
    }

    @Override
    public boolean isValid(Object value) {
        try {
            if(appCurrentName.isPresent() && appCurrentName.get().equals(value)) {
                return true;
            }

            if (!appRepositoryService().isAppNameExists((String) value)) {
                return true;
            }
        } catch (Exception e) {
            logger.error("Unexpected error occurred while validating", e);
        }

        return false;
    }
}
