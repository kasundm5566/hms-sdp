/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/

package hms.kite.provisioning.ui.common;

/*
* $LastChangedDate: 2011-04-28 15:42:12 +0530 (Thu, 28 Apr 2011) $
* $LastChangedBy: nilushikas $
* $LastChangedRevision: 72623 $
*/

public class AppException extends Exception {

    public AppException() {
        super();
    }

    public AppException(String e) {
        super(e);
    }
}