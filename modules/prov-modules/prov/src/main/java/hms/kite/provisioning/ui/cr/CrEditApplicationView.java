/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.provisioning.ui.cr;

import com.vaadin.ui.*;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.ui.app.AppBasicDetailsLayout;
import hms.kite.provisioning.ui.app.AppFeatureLayout;

import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CrEditApplicationView {

    private BaseApplication application;
    private Panel mainPanel;
    private Panel childPanel;
    private Form form;

    private Map<String, Object> crContext;

    public CrEditApplicationView(BaseApplication application, Panel mainPanel, Map<String, Object> cr) {
        this.application = application;
        this.mainPanel = mainPanel;
        this.crContext = cr;
        init();
    }

    private void init() {
        childPanel = new Panel("XXX");
        childPanel.addStyleName("child-panel");
        form = new Form();
        form.setLayout(new VerticalLayout());
        childPanel.addComponent(form);

        AppBasicDetailsLayout appBasicDetailsLayout =AppBasicDetailsLayout.createCrBasicAppDetailsLayout(application, crContext, true);
        form.getLayout().addComponent(appBasicDetailsLayout);
        AppFeatureLayout appFeatureLayout = AppFeatureLayout.createCrAppFeatureLayout(application, crContext);
        form.getLayout().addComponent(appFeatureLayout);

        mainPanel.addComponent(childPanel);
        form.getFooter().addComponent(createButtonBar());
    }

    private Component createButtonBar() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setWidth("100%");
        Button backButton = new Button(application.getMsg("cr.create.cr.back"));
        backButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                mainPanel.removeAllComponents();
                new InitialCrSelectionWindow(application, mainPanel, crContext);

            }
        });
        horizontalLayout.addComponent(backButton);
        return horizontalLayout;
    }

}
