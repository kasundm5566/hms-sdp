/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.services.httpclient;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import hms.common.registration.api.common.RequestType;
import hms.common.registration.api.request.UserDetailByUsernameRequestMessage;
import hms.common.registration.api.response.BasicUserResponseMessage;

import hms.common.rest.util.JsonBodyProvider;
import hms.kite.util.GsonUtil;
import hms.kite.util.SdpException;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.jaxrs.provider.json.JSONProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static hms.kite.util.KiteErrorBox.systemErrorCode;

/**
 * $LastChangedDate $
 * $LastChangedBy $
 * $LastChangedRevision $
 */
public class CurHttpClient {

    private WebClient webClient;
    private String url;

    protected final Logger logger = LoggerFactory.getLogger(CurHttpClient.class);


    public CurHttpClient() {

    }

    public void init() {
        List<Object> providers = new ArrayList<Object>();
        providers.add(new JsonBodyProvider());
        webClient = WebClient.create(url + "/user/detail/username", providers);
        webClient.header("Content-Type", "application/json");
        webClient.accept("application/json");
    }

    public BasicUserResponseMessage get(String userName) {
        try {
            logger.info("WebClient it being initialized with uri [{}] for username [{}]", url, userName);

            UserDetailByUsernameRequestMessage requestMessage = new UserDetailByUsernameRequestMessage();
            requestMessage.setRequestType(RequestType.USER_ADDITIONAL_DETAILS);
            requestMessage.setRequestedTimeStamp(new Date());
            requestMessage.setUserName(userName);
            requestMessage.setModuleName("provisioning");

            logger.info("HTTP POST request via WebClient; uri [{}]", url);
            Response response = webClient.post(requestMessage.convertToMap());

            return readJsonResponse(readJsonResponse(response));
        } catch (Exception e) {
            logger.error("Error in reading user details", e);
            return null;
        }
    }

    public String toJson(Object value) {
        return GsonUtil.toJson(value);
    }

    public Map<String, Object> fromJson(String jsonString) {
        return GsonUtil.fromJson(jsonString, Map.class);
    }

    private Map<String, Object> readJsonResponse(Response response) {
        InputStream is = (InputStream) response.getEntity();
        Class aClass = Map.class;
        JsonBodyProvider jsonProvider = new JsonBodyProvider();
        try {
            return (Map<String, Object>) jsonProvider.readFrom(aClass, null, null, MediaType.APPLICATION_JSON_TYPE, null, is);
        } catch (IOException e) {
            logger.error("can't create the json object", e);
            return null;
        }
    }

    private static BasicUserResponseMessage readJsonResponse(Map<String, Object> m) throws ParseException {
        return BasicUserResponseMessage.convertFromMap(m);
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
