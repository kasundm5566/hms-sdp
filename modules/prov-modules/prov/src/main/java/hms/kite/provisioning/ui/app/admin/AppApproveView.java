/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/

package hms.kite.provisioning.ui.app.admin;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import hms.kite.provisioning.commons.ui.BaseApplication;


/*
* $LastChangedDate$
* $LastChangedBy$
* $LastChangedRevision$
*/

public class AppApproveView extends VerticalLayout {


    public AppApproveView(BaseApplication application) {
        Panel panel = new Panel(application.getMsg("admin.manageApp.ApproveAppRequest"));
        addComponent(panel);
        setComponentAlignment(panel, Alignment.MIDDLE_CENTER);
        final AppApproveEditor editor = new AppApproveEditor(application);
        panel.addComponent(editor.loadComponents());


    }


}
