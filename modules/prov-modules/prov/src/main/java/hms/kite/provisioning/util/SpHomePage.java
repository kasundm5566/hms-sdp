/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.util;

import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import hms.kite.provisioning.Provisioning;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class SpHomePage extends VerticalLayout {

    private Provisioning provisioning;

    private void applyStyles(VerticalLayout layout) {
        layout.addStyleName("container-panel");
        layout.setWidth("760px");
        layout.setMargin(true);
    }

    public SpHomePage(Provisioning provisioning) {
        this.provisioning = provisioning;
    }

    public void showSpInitialHomeScreen() {
        Panel mainPanel = new Panel(provisioning.getMsg("provisioning.welcome.title"));
        VerticalLayout spInitialHomePageLayout = new VerticalLayout();
        Embedded homeImage = new Embedded(null, new ThemeResource("../provisioning/img/home.jpg"));
        spInitialHomePageLayout.addComponent(homeImage);
        applyStyles(this);
        mainPanel.addComponent(spInitialHomePageLayout);
        addComponent(mainPanel);
    }
}