/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/

package hms.kite.provisioning.ui.app.admin;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.BaseTheme;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.ui.app.AppPanel;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.appRepositoryService;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.provisioning.commons.util.ProvKeyBox.coopUserIdK;


/*
* $LastChangedDate$
* $LastChangedBy$
* $LastChangedRevision$
*/

public class AppApproveEditor {

    private BaseApplication application;
    private Table requestTable;


    public AppApproveEditor(BaseApplication application) {
        this.application = application;
        requestTable = new Table();
    }

    public VerticalLayout loadComponents() {
        VerticalLayout layout = new VerticalLayout();
        layout.addComponent(createTable());

        return layout;
    }

    private Table createTable() {
        requestTable.addContainerProperty(application.getMsg("admin.manageApp.Table.appName"), String.class, null);
        requestTable.addContainerProperty(application.getMsg("admin.manageApp.Table.requestedDate"), String.class, null);
        requestTable.addContainerProperty(application.getMsg("admin.manageApp.Table.serviceProvider"), Button.class, null);
        requestTable.addContainerProperty(application.getMsg("admin.manageApp.Table.status"), String.class, null);
        requestTable.addContainerProperty(application.getMsg("admin.manageApp.Table.requestedAttempt"), String.class, null);
        requestTable.addContainerProperty(application.getMsg("admin.manageApp.Table.action"), HorizontalLayout.class, null);

        displayRecord();
        requestTable.setSelectable(true);
        requestTable.requestRepaint();
        requestTable.setSizeFull();
        return requestTable;

    }

    public void displayRecord() {

        int i = 0;
        List<Map<String, Object>> appList = appRepositoryService().findAppsRange(new HashMap(), 0, 100);


        for (Map<String, Object> app : appList) {
            requestTable.addItem(new Object[]{
                    app.get(nameK), app.get(appRequestDateK), createLink((String)app.get(spIdK)), app.get(statusK), "1",
                    createButtonLayout(app)}, i);
            i++;
        }
    }

    private Button createLink(String name) {
        final Button button = new Button(name);
        button.setStyleName(BaseTheme.BUTTON_LINK);
        button.setImmediate(true);
        button.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
//                if ("home".equals(button.getCaption())) {
//                    application.getUiManager().pushScreen(ProvisioningScreen.class.getName(),
//                            new ProvisioningScreen(application));
//                }
            }
        }
        );
        return button;
    }




   private HorizontalLayout createButtonLayout(Map<String, Object> app) {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
            horizontalLayout.removeAllComponents();
            horizontalLayout.addComponent(createLink(application.getMsg("admin.manageApp.approveAppRequestTable.viewButton"), app));

            if (app.get(statusK).equals(draftK)) {
                horizontalLayout.addComponent(createLink(application.getMsg("admin.manageApp.approveAppRequestTable.deleteButton"), app));
            }
            else if (app.get(statusK).equals(limitedProductionK)) {
                horizontalLayout.addComponent(createLink(application.getMsg("admin.manageApp.approveAppRequestTable.productionButton"), app));
                horizontalLayout.addComponent(createLink(application.getMsg("admin.manageApp.approveAppRequestTable.suspendButton"), app));
            }else if (app.get(statusK).equals(activeProductionK)){
                horizontalLayout.addComponent(createLink(application.getMsg("admin.manageApp.approveAppRequestTable.suspendButton"), app));
            } else if(app.get(statusK).equals(rejectK)){
                horizontalLayout.addComponent(createLink(application.getMsg("admin.manageApp.approveAppRequestTable.deleteButton"), app));
            }else if (app.get(statusK).equals(pendingApproveK)){
                horizontalLayout.addComponent(createLink(application.getMsg("admin.manageApp.approveAppRequestTable.productionButton"), app));
                horizontalLayout.addComponent(createLink(application.getMsg("admin.manageApp.approveAppRequestTable.limitedProductionButton"), app));
                horizontalLayout.addComponent(createLink(application.getMsg("admin.manageApp.approveAppRequestTable.rejectButton"), app));
            } else if (app.get(statusK).equals(suspendK)){
                horizontalLayout.addComponent(createLink(application.getMsg("admin.manageApp.approveAppRequestTable.restoreButton"), app));
            }
//

        return horizontalLayout;
    }

     private Button createLink(String name, final Map<String, Object> app) {
        final Button button = new Button(name);
        button.setStyleName(BaseTheme.BUTTON_LINK);
        button.setImmediate(true);
        button.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                if (isDelete(button)) {
                    appRepositoryService().delete((app.get(appIdK)).toString());
                } else if (isView(button)) {
                    HashSet<String> userRoles = (HashSet)application.getFromSession(kiteUserRolesK);
                    String coopUserId = (String)application.getFromSession(coopUserIdK);
                    AppPanel appPanel = AppPanel.newViewAppPanel(app, application );
                    application.getUiManager().pushScreen(AppPanel.class.getName(), appPanel);
                } else if (isEdit(button)) {

                } else if (isApplyForApproval(button)) {
                    app.put(statusK, pendingApproveK);
                    appRepositoryService().update(app);
                }
                else if (isSuspend(button)){
                    app.put(previousStateK, app.get(statusK));
                    app.put(statusK,suspendK);
                    appRepositoryService().update(app);
                } else if (isSendToActiveProduction(button)){
                    app.put(statusK,activeProductionK);
                    appRepositoryService().update(app);
                } else if (isSendToLimitedProduction(button)){
                    app.put(statusK,limitedProductionK);
                    appRepositoryService().update(app);
                } else if(isReject(button)){
                    app.put(statusK,rejectK);
                    appRepositoryService().update(app);
                } else if (isRestore(button)){
                    app.put (statusK, app.get(previousStateK));
                    app.put(previousStateK,"");
                    appRepositoryService().update(app);


                }

            }
        }
        );
        return button;
    }



    private boolean isDelete(Button button) {
        return button.getCaption().equals(application.getMsg("admin.manageApp.approveAppRequestTable.deleteButton"));
    }

    private boolean isView(Button button) {
        return button.getCaption().equals(application.getMsg("admin.manageApp.approveAppRequestTable.viewButton"));
    }

    private boolean isEdit(Button button) {
        return button.getCaption().equals(application.getMsg("sp.viewApp.viewTable.editLinkButton"));
    }

    private boolean isApplyForApproval(Button button) {
        return button.getCaption().equals(application.getMsg("sp.viewApp.viewTable.applyForApprovalLinkButton"));
    }

    private  boolean  isSuspend(Button button){
        return button.getCaption().equals(application.getMsg("admin.manageApp.approveAppRequestTable.suspendButton"));
    }

    private boolean isSendToActiveProduction (Button button){
        return button.getCaption().equals(application.getMsg("admin.manageApp.approveAppRequestTable.productionButton"));
    }

    private boolean isSendToLimitedProduction (Button button){
        return button.getCaption().equals(application.getMsg("admin.manageApp.approveAppRequestTable.limitedProductionButton"));
    }

    private boolean isReject (Button button){
        return button.getCaption().equals(application.getMsg("admin.manageApp.approveAppRequestTable.rejectButton"));
    }

    private boolean isRestore (Button button){
        return button.getCaption().equals(application.getMsg("admin.manageApp.approveAppRequestTable.restoreButton"));
    }




}
