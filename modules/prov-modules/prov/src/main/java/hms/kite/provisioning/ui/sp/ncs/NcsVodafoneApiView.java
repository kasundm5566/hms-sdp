package hms.kite.provisioning.ui.sp.ncs;

import com.vaadin.data.Validator;
import com.vaadin.ui.*;
import hms.kite.provisioning.Provisioning;
import hms.kite.util.KeyNameSpaceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

public class NcsVodafoneApiView extends NcsConfigurationView {
    private static final Logger logger = LoggerFactory.getLogger(NcsVodafoneApiView.class);

    private TextField msisdnTpsTxtFld;
    private TextField msisdnTpdTxtFld;

    private TextField chargingTpsTxtFld;
    private TextField chargingTpdTxtFld;
    private TextField chargingMinAmountTxtFld;
    private TextField chargingMaxAmountTxtFld;

    private TextField subscriptionTpsTxtFld;
    private TextField subscriptionTpdTxtFld;

    public NcsVodafoneApiView(Provisioning provisioning) {
        this.provisioning = provisioning;
        init(provisioning);
    }

    private void init(Provisioning provisioning) {
        createMsisdnTxtFlds();
        createChargingTxtFlds();
        createSubscriptionTxtFlds();
    }

    private void createMsisdnTxtFlds(){
        msisdnTpsTxtFld = new TextField(provisioning.getMsg("sp.ncs.vdfapis.checkmsisdn.tps.caption"));
        msisdnTpsTxtFld.setImmediate(true);
        msisdnTpsTxtFld.setRequired(true);
        msisdnTpsTxtFld.setRequiredError(provisioning.getMsg("sp.ncs.vdfapis.checkmsisdn.tps.required.error"));
        msisdnTpsTxtFld.addValidator(createValidator("ncsVdfApiCheckMsisdnTpsRegex", "sp.ncs.vdfapis.checkmsisdn.tps.validation"));
        msisdnTpsTxtFld.addValidator(createValidator("sp.createApp.zero.validation", "sp.ncs.vdfapis.zero.validation.error.message"));

        msisdnTpdTxtFld = new TextField(provisioning.getMsg("sp.ncs.vdfapis.checkmsisdn.tpd.caption"));
        msisdnTpdTxtFld.setImmediate(true);
        msisdnTpdTxtFld.setRequired(true);
        msisdnTpdTxtFld.setRequiredError(provisioning.getMsg("sp.ncs.vdfapis.checkmsisdn.tpd.required.error"));
        msisdnTpdTxtFld.addValidator(createValidator("ncsVdfApiCheckMsisdnTpdRegex", "sp.ncs.vdfapis.checkmsisdn.tpd.validation"));
        msisdnTpdTxtFld.addValidator(createValidator("sp.createApp.zero.validation", "sp.ncs.vdfapis.zero.validation.error.message"));
    }

    private void createChargingTxtFlds(){
        chargingTpsTxtFld = new TextField(provisioning.getMsg("sp.ncs.vdfapis.sendcharge.tps.caption"));
        chargingTpsTxtFld.setImmediate(true);
        chargingTpsTxtFld.setRequired(true);
        chargingTpsTxtFld.setRequiredError(provisioning.getMsg("sp.ncs.vdfapis.sendcharging.tps.required.error"));
        chargingTpsTxtFld.addValidator(createValidator("ncsVdfApiChargingTpsRegex", "sp.ncs.vdfapis.sendcharging.tps.validation"));
        chargingTpsTxtFld.addValidator(createValidator("sp.createApp.zero.validation", "sp.ncs.vdfapis.zero.validation.error.message"));

        chargingTpdTxtFld = new TextField(provisioning.getMsg("sp.ncs.vdfapis.sendcharge.tpd.caption"));
        chargingTpdTxtFld.setImmediate(true);
        chargingTpdTxtFld.setRequired(true);
        chargingTpdTxtFld.setRequiredError(provisioning.getMsg("sp.ncs.vdfapis.sendcharging.tpd.required.error"));
        chargingTpdTxtFld.addValidator(createValidator("ncsVdfApiChargingTpdRegex", "sp.ncs.vdfapis.sendcharging.tpd.validation"));
        chargingTpdTxtFld.addValidator(createValidator("sp.createApp.zero.validation", "sp.ncs.vdfapis.zero.validation.error.message"));

        chargingMinAmountTxtFld = new TextField(provisioning.getMsg("sp.ncs.vdfapis.sendcharge.minAmount.caption"));
        chargingMinAmountTxtFld.setImmediate(true);
        chargingMinAmountTxtFld.setRequired(true);
        chargingMinAmountTxtFld.setRequiredError(provisioning.getMsg("sp.ncs.vdfapis.sendcharging.minamt.required"));
        chargingMinAmountTxtFld.addValidator(createValidator("ncsVdfApiChargingMinVal", "sp.ncs.vdfapis.sendcharging.minamt.validation"));

        chargingMaxAmountTxtFld = new TextField(provisioning.getMsg("sp.ncs.vdfapis.sendcharge.maxAmount.caption"));
        chargingMaxAmountTxtFld.setImmediate(true);
        chargingMaxAmountTxtFld.setRequired(true);
        chargingMaxAmountTxtFld.setRequiredError(provisioning.getMsg("sp.ncs.vdfapis.sendcharging.maxamt.required"));
        chargingMaxAmountTxtFld.addValidator(createValidator("ncsVdfApiChargingMaxVal", "sp.ncs.vdfapis.sendcharging.maxamt.validation"));
    }

    private void createSubscriptionTxtFlds(){
        subscriptionTpsTxtFld = new TextField(provisioning.getMsg("sp.ncs.vdfapis.sendsubscription.tps.caption"));
        subscriptionTpsTxtFld.setImmediate(true);
        subscriptionTpsTxtFld.setRequired(true);
        subscriptionTpsTxtFld.setRequiredError(provisioning.getMsg("sp.ncs.vdfapis.sendsubscription.tps.required.error"));
        subscriptionTpsTxtFld.addValidator(createValidator("ncsVdfApiSubscriptionTpsRegex", "sp.ncs.vdfapis.sendsubscription.tps.validation"));
        subscriptionTpsTxtFld.addValidator(createValidator("sp.createApp.zero.validation", "sp.ncs.vdfapis.zero.validation.error.message"));

        subscriptionTpdTxtFld = new TextField(provisioning.getMsg("sp.ncs.vdfapis.sendsubscription.tpd.caption"));
        subscriptionTpdTxtFld.setImmediate(true);
        subscriptionTpdTxtFld.setRequired(true);
        subscriptionTpdTxtFld.setRequiredError(provisioning.getMsg("sp.ncs.vdfapis.sendsubscription.tpd.required.error"));
        subscriptionTpdTxtFld.addValidator(createValidator("ncsVdfApiSubscriptionTpdRegex", "sp.ncs.vdfapis.sendsubscription.tpd.validation"));
        subscriptionTpdTxtFld.addValidator(createValidator("sp.createApp.zero.validation", "sp.ncs.vdfapis.zero.validation.error.message"));
    }

    @Override
    protected void setSpSessionValues() {
        Map<String, Object> sp = provisioning.getSp();
        sp.put(KeyNameSpaceResolver.key(checkMsisdnk, vdfApiTpsk), msisdnTpsTxtFld.getValue());
        sp.put(KeyNameSpaceResolver.key(checkMsisdnk, vdfApiTpdk), msisdnTpdTxtFld.getValue());

        sp.put(KeyNameSpaceResolver.key(sendChargek, vdfApiTpsk), chargingTpsTxtFld.getValue());
        sp.put(KeyNameSpaceResolver.key(sendChargek, vdfApiTpdk), chargingTpdTxtFld.getValue());
        sp.put(KeyNameSpaceResolver.key(sendChargek, minAmountK), Double.valueOf((String) chargingMinAmountTxtFld.getValue()));
        sp.put(KeyNameSpaceResolver.key(sendChargek, maxAmountK), Double.valueOf((String) chargingMaxAmountTxtFld.getValue()));

        sp.put(KeyNameSpaceResolver.key(sendSubsk, vdfApiTpsk), subscriptionTpsTxtFld.getValue());
        sp.put(KeyNameSpaceResolver.key(sendSubsk, vdfApiTpdk), subscriptionTpdTxtFld.getValue());
    }

    @Override
    protected void changeToEditMode() {
        msisdnTpsTxtFld.setReadOnly(false);
        msisdnTpdTxtFld.setReadOnly(false);

        chargingTpsTxtFld.setReadOnly(false);
        chargingTpdTxtFld.setReadOnly(false);
        chargingMinAmountTxtFld.setReadOnly(false);
        chargingMaxAmountTxtFld.setReadOnly(false);

        subscriptionTpsTxtFld.setReadOnly(false);
        subscriptionTpdTxtFld.setReadOnly(false);
    }

    @Override
    protected void changeToConfirmMode() {
        msisdnTpsTxtFld.setReadOnly(true);
        msisdnTpdTxtFld.setReadOnly(true);

        chargingTpdTxtFld.setReadOnly(true);
        chargingTpsTxtFld.setReadOnly(true);
        chargingMinAmountTxtFld.setReadOnly(true);
        chargingMaxAmountTxtFld.setReadOnly(true);

        subscriptionTpsTxtFld.setReadOnly(true);
        subscriptionTpdTxtFld.setReadOnly(true);
    }

    @Override
    protected void changeToViewMode() {
        changeToConfirmMode();
    }

    @Override
    protected void changeToCancelView() {
        changeToViewMode();
    }

    @Override
    public Component loadComponents(Panel panel) {
        VerticalLayout layout = new VerticalLayout();

        panel.removeAllComponents();
        panel.setCaption(provisioning.getMsg("sp.ncs.vdfapis.title"));
        panel.addComponent(layout);

        addToForm();
        reloadData();
        refreshInChangedMode();

        layout.addComponent(form);
        layout.setComponentAlignment(form, Alignment.MIDDLE_CENTER);
        layout.setSpacing(true);

        return layout;
    }

    private void reloadData() {
        Map<String, Object> sp = provisioning.getSp();

        setValueToField(msisdnTpsTxtFld, sp.get(KeyNameSpaceResolver.key(checkMsisdnk, vdfApiTpsk)));
        setValueToField(msisdnTpdTxtFld, sp.get(KeyNameSpaceResolver.key(checkMsisdnk, vdfApiTpdk)));

        setValueToField(chargingTpsTxtFld, sp.get(KeyNameSpaceResolver.key(sendChargek, vdfApiTpsk)));
        setValueToField(chargingTpdTxtFld, sp.get(KeyNameSpaceResolver.key(sendChargek, vdfApiTpdk)));
        chargingMinAmountTxtFld.setValue(((Double) sp.get(KeyNameSpaceResolver.key(sendChargek, chargeMink))).toString());
        chargingMaxAmountTxtFld.setValue(((Double) sp.get(KeyNameSpaceResolver.key(sendChargek, chargeMaxk))).toString());

        setValueToField(subscriptionTpsTxtFld, sp.get(KeyNameSpaceResolver.key(sendSubsk, vdfApiTpsk)));
        setValueToField(subscriptionTpdTxtFld, sp.get(KeyNameSpaceResolver.key(sendSubsk, vdfApiTpdk)));
    }

    @Override
    public void saveData() {

    }

    @Override
    public boolean validateData() {
        try {
            setSpSessionValues();

            msisdnTpsTxtFld.validate();
            validateField(msisdnTpsTxtFld);
            validateField(msisdnTpdTxtFld);

            validateField(chargingTpsTxtFld);
            validateField(chargingTpdTxtFld);
            validateField(chargingMinAmountTxtFld);
            validateField(chargingMaxAmountTxtFld);

            validateField(subscriptionTpsTxtFld);
            validateField(subscriptionTpsTxtFld);

            msisdnMaxMinValidation();
            chargingMaxMinValidation();
            subscriptionMaxMinValidation();

            form.setComponentError(null);
        } catch (Validator.InvalidValueException e) {
            form.setComponentError(e);
            return false;
        } catch (Exception e) {
            logger.error("Unexpected error occurred while validating vodafone-api form", e);
            return false;
        }
        return true;
    }

    private void validateField(Field field) {
        if (isFieldNotNull(field)) {
            field.validate();
        }
    }

    private boolean isFieldNotNull(Field field) {
        return field != null;
    }

    private void msisdnMaxMinValidation(){
        integerMaxMinValidation(msisdnTpdTxtFld, msisdnTpsTxtFld, provisioning.getMsg("sp.ncs.vdfapis.checkmsisdn.transactions.minmax.validation"));
    }

    private void chargingMaxMinValidation(){
        integerMaxMinValidation(chargingTpdTxtFld, chargingTpsTxtFld, provisioning.getMsg("sp.ncs.vdfapis.sendcharge.transactions.minmax.validation"));
        doubleMaxMinValidation(chargingMaxAmountTxtFld, chargingMinAmountTxtFld, provisioning.getMsg("sp.ncs.vdfapis.sendcharge.amount.minmax.validation"));
    }

    private void subscriptionMaxMinValidation(){
        integerMaxMinValidation(subscriptionTpdTxtFld, subscriptionTpsTxtFld, provisioning.getMsg("sp.ncs.vdfapis.sendsubs.transactions.minmax.validation"));
    }

    private void integerMaxMinValidation(Field maxfield, Field minfield, String errorMessage){
        if(Integer.parseInt(minfield.getValue().toString()) >= Integer.parseInt(maxfield.getValue().toString())){
            throw new Validator.InvalidValueException(errorMessage);
        }
    }

    private void doubleMaxMinValidation(Field maxfield, Field minfield, String errorMessage){
        if(Double.parseDouble(minfield.getValue().toString()) >= Double.parseDouble(maxfield.getValue().toString())){
            throw new Validator.InvalidValueException(errorMessage);
        }
    }

    private void addToForm() {
        form.addField("cm-tps", msisdnTpsTxtFld);
        form.addField("cm-tpd", msisdnTpdTxtFld);

        form.addField("sc-tps", chargingTpsTxtFld);
        form.addField("sc-tpd", chargingTpdTxtFld);
        form.addField("sc-min-amt", chargingMinAmountTxtFld);
        form.addField("sc-max-amt", chargingMaxAmountTxtFld);

        form.addField("ss-tps", subscriptionTpsTxtFld);
        form.addField("ss-tpd", subscriptionTpdTxtFld);
    }

}
