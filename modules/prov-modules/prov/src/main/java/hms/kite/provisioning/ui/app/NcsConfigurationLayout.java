/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.app;

import com.vaadin.data.Validator;
import com.vaadin.ui.*;
import hms.kite.provisioning.PortletEventHandler;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.LiferayUtil;
import hms.kite.provisioning.commons.ui.component.popup.HelpDetails;
import hms.kite.provisioning.commons.ui.event.PortletEventSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import java.util.*;

import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.provisioning.ProvisioningServiceRegistry.isHelpButtonLinkEnable;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.util.KiteKeyBox.appIdK;
import static hms.kite.util.KiteKeyBox.operatorK;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class NcsConfigurationLayout extends VerticalLayout implements PortletEventHandler {

    private static Logger logger = LoggerFactory.getLogger(NcsConfigurationLayout.class);

    private final Map<String, Object> ncsLoggingData = new HashMap<String, Object>();
    private final Map<String, Object> ncsConfiguredAdditionalData = new HashMap<String, Object>();

    private static final String HELP_IMAGE = "help-resource-config.jpg";
    public static final String HELP_IMG_WIDTH = "720px";
    public static final String HELP_IMG_HEIGHT = "820px";
    public static final String HELP_IMG_STYLE_NAME = "link";
    public static final String HELP_BUTTON_NAME = "Help";
    private String mpesaPayBillNo;

    private BaseApplication application;
    private Map<String, Object> app;
    private PopupView popupView;

    public NcsConfigurationLayout(BaseApplication application, Map<String, Object> app) {
        this.application = application;
        this.app = app;
        if(isHelpButtonLinkEnable()) {
            createHelperLink();
        }
        addComponent(loadNcsConfigurationTable());
        addStyleName("app-feature-layout");
        setWidth("350px");
    }


    private void createHelperLink() {
        Button helpButtonLink = createHelpButtonLink();
        addComponent(helpButtonLink);
        setComponentAlignment(helpButtonLink, Alignment.TOP_RIGHT);
    }

    private Button createHelpButtonLink() {
        final HelpDetails helpDetails = new HelpDetails(application, HELP_IMAGE);
        final Embedded image = helpDetails.getHelpImage();
        Button helpButtonLink = new Button(HELP_BUTTON_NAME);
        helpButtonLink.setStyleName(HELP_IMG_STYLE_NAME);
        helpButtonLink.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent clickEvent) {
                getWindow().addWindow(helpDetails.generateHelpImageSubWindow(image, HELP_IMG_WIDTH, HELP_IMG_HEIGHT));
            }
        });
        return helpButtonLink;
    }

    private VerticalLayout loadNcsConfigurationTable() {
        VerticalLayout tableContainer = new VerticalLayout();
        Table ncsConfigurationTable = new Table();
        ncsConfigurationTable.addStyleName("striped");
        ncsConfigurationTable.setImmediate(true);
        ncsConfigurationTable.setSelectable(true);
        ncsConfigurationTable.setWidth("100%");
        ncsConfigurationTable.setHeight("100px");
        ncsConfigurationTable.setImmediate(true);
        ncsConfigurationTable.addContainerProperty(application.getMsg("sp.createApp.NcsConfigureTable.ncs"), String.class, null);
        ncsConfigurationTable.setColumnWidth(application.getMsg("sp.createApp.NcsConfigureTable.ncs"), 100);
        ncsConfigurationTable.addContainerProperty(application.getMsg("sp.createApp.NcsConfigureTable.operator"), String.class, null);
        ncsConfigurationTable.setColumnWidth(application.getMsg("sp.createApp.NcsConfigureTable.operator"), 110);
        ncsConfigurationTable.addContainerProperty(application.getMsg("sp.createApp.NcsConfigureTable.configure"), Button.class, null);

        List<Map<String, Object>> selectedServices = (List) app.get(ncsesK);

        int rowNum = 0;

        logger.debug("Services : [{}]", selectedServices);
        for (Map<String, Object> selectedServiceMap : selectedServices) {
            String ncsType = (String) selectedServiceMap.get(ncsTypeK);
            String operator = (String) selectedServiceMap.get(operatorK);
            String status = (String) selectedServiceMap.get(statusK);
            logger.debug("NCS [{}] Operator [{}]", ncsType, operator);
            logger.debug("Status [{}]", status);
            ncsConfigurationTable.addItem(new Object[]{ncsType, operator, createConfigurationButton(ncsType, operator, status)}, rowNum);
            rowNum++;
        }
        tableContainer.addComponent(ncsConfigurationTable);
        return tableContainer;
    }

    private Button createConfigurationButton(final String ncsType, final String operator, final String status) {
        final Button configureButton = new Button();
        if (status.equals(selectedK)) {
            configureButton.setCaption(application.getMsg("sp.createApp.ncsConfiguration.configureButton"));

        } else if (status.equals(ncsConfiguredK)) {
            configureButton.setCaption(application.getMsg("sp.createApp.ncsConfiguration.reConfigureButton"));
        }
        configureButton.setImmediate(true);
        configureButton.addStyleName("link");
        final String portletName = LiferayUtil.createPortletId(ncsType, operator);
        configureButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    Map<String, Object> eventDataMap = new HashMap<String, Object>();
                    eventDataMap.put(ncsTypeK, ncsType);
                    eventDataMap.put(operatorK, operator);
                    eventDataMap.put(appIdK, app.get(appIdK));
                    eventDataMap.put(appStatusK, (app.get(statusK) == null) ? initialK : app.get(statusK));
                    eventDataMap.put(spIdK, app.get(spIdK));
                    eventDataMap.put(coopUserIdK, spRepositoryService().findSpBySpId((String) app.get(spIdK)).get(coopUserIdK));
                    eventDataMap.put(portletIdK, portletName);
                    eventDataMap.put(ncsPortletTitle, application.getMsg("sp.createApp.createApplicationTitle"));
                    eventDataMap.put(ncsesK, app.get(ncsesK));
                    application.addToSession(ncsConfiguredEventHandler, NcsConfigurationLayout.this);
                    if (status.equals(selectedK)) {
                        PortletEventSender.send(eventDataMap, provCreateNcsEvent, application);
                    } else if (status.equals(ncsConfiguredK)) {
                        PortletEventSender.send(eventDataMap, provReConfigureNcsEvent, application);
                    }
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while trying to configure a ncs", e);
                }
            }
        });

        return configureButton;
    }

    @Override
    public void handleEventRequest(EventRequest eventRequest, EventResponse eventResponse, Window window) {
        application.removeFromSession(ncsConfiguredEventHandler);
        Map<String, Object> eventData = (Map) eventRequest.getEvent().getValue();
        String ncsType = (String) eventData.get(ncsTypeK);
        String operator = (String) eventData.get(operatorK);

        ncsLoggingData.put(ncsType, eventData.get(ncsDataK));
        ncsConfiguredAdditionalData.put(ncsType, eventData.get(provEventAdditionalDataK));

        if (eventData.get(statusK).equals(successK)) {
            setStatusInNcs(ncsType, operator, ncsConfiguredK);
            reloadConfigurationTable();
        }
    }

    public Map<String, Object> getNcsLoggingData() {
        return ncsLoggingData;
    }

    public Map<String, Object> getNcsConfiguredAdditionalData() {
        return ncsConfiguredAdditionalData;
    }

    private void reloadConfigurationTable() {
        removeAllComponents();
        if(isHelpButtonLinkEnable()) {
            createHelperLink();
        }
        addComponent(loadNcsConfigurationTable());
    }


    private void setStatusInNcs(String ncsType, String operator, String status) {
        List<Map<String, Object>> selectedList = (List) app.get(ncsesK);
        for (Map<String, Object> selectedMap : selectedList) {
            if (selectedMap.get(ncsTypeK).equals(ncsType) &&
                    (selectedMap.get(operatorK) == null || selectedMap.get(operatorK).equals(operator))) {
                selectedMap.put(statusK, status);
                return;
            }
        }
    }

    public void validate() {
        checkDuplicatePayBillNo(this.getNcsLoggingData());
    }

    private void checkDuplicatePayBillNo(Map<String, Object> loggingData) {
        Set<String> ncsPayBillNos = new HashSet<String>();

        for (Object o : loggingData.values()) {
            try {
                Map<String, Object> chargingDetailsMap = (Map<String, Object>) o;
                Object metaData = chargingDetailsMap.get(metaDataK);
                Map<String, Object> metaDataMap = (Map<String, Object>) metaData;
                String mpesaPayBillNo = (String) metaDataMap.get(mpesaPayBillNoK);

                if(mpesaPayBillNo != null && !mpesaPayBillNo.trim().equals("")) {
                    ncsPayBillNos.add(mpesaPayBillNo);
                }
            } catch (Exception e) {
                logger.debug("Mpesa paybill number, not found in the ncs.");
            }
        }

        Boolean isPayBillNoDuplicate = ncsPayBillNos.size() > 1;
        if(isPayBillNoDuplicate) {
            throw new Validator.InvalidValueException(application.getMsg("charging.mpesa.paybill.no.not.unique.error"));
        }
        Boolean uniquePayBillNo = ncsPayBillNos.size() == 1;
        if(uniquePayBillNo) {
            mpesaPayBillNo = ncsPayBillNos.iterator().next();
        }
    }

    public String getMpesaPayBillNo() {
        return mpesaPayBillNo;
    }

    public void setApp(Map<String, Object> app) {
        this.app = app;
    }
}
