/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.daemon;

import hms.kite.provisioning.commons.event.DefaultEventRouter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static hms.kite.provisioning.ProvisioningServiceRegistry.appRepositoryService;
import static hms.kite.provisioning.ProvisioningServiceRegistry.spRepositoryService;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class EmailSummaryDaemon {

    private static Logger logger = LoggerFactory.getLogger(EmailSummaryDaemon.class);
    private DefaultEventRouter eventRouter;
    private String emailFooter;

    public void execute() {
        logger.debug("Executing Email summery daemon");
        spRequestPendingSummary();
        appRequestPendingSummary();
    }

    private void spRequestPendingSummary() {

        Map<String, Object> data = new HashMap<String, Object>();
        final int numberOfPendingSps = spRepositoryService().findSpsByStatus(pendingApproveK).size();
        if(numberOfPendingSps == 0) {
            logger.debug("Sp summery email sending is not required since no updates");
            return;//No meaning of firing email
        }
        data.put(eventNameK, adminSpRequestsSummaryEvent);
        data.putAll(new HashMap<String, Object>(){{
            put("no_of_pending_approval_sp_requests", numberOfPendingSps);
            put("cr-pending-request", 0);
        }});

        data.put(adminK, new HashMap<String, Object>(){{
            put(emailFooterK, emailFooter);
        }});


        eventRouter.fire(data);
    }

    private void appRequestPendingSummary() {

        Map<String, Object> data = new HashMap<String, Object>();
        final int numberOfPendingApps = appRepositoryService().findAppsByStatus(pendingApproveK).size();
        final int numberOfPendingActiveApps = appRepositoryService().findAppsByStatus(pendingApproveForActiveProductionK).size();
        if (numberOfPendingApps == 0) {
            logger.debug("App summery email sending is not required since no updates");
            return;//No meaning of sending mails, since no any pending requests.
        }
        data.put(eventNameK, appAdminRequestsSummaryEvent);
        data.putAll(new HashMap<String, Object>(){{
//            put("app-pending-request", numberOfPendingApps);
            put("no_of_pending_approvals_to_active_production", numberOfPendingActiveApps + numberOfPendingApps);
            put("cr-pending-request", 0);
        }});

        data.put(adminK, new HashMap<String, Object>(){{
            put(emailFooterK, emailFooter);
        }});

        eventRouter.fire(data);
    }

    public void setEventRouter(DefaultEventRouter eventRouter) {
        this.eventRouter = eventRouter;
    }

    public void setEmailFooter(String emailFooter) {
        this.emailFooter = emailFooter;
    }
}
