/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.sp.ncs;

import com.vaadin.data.Validator;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import hms.kite.provisioning.Provisioning;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.provisioning.commons.util.ProvKeyBox.wapPushMtTpdK;
import static hms.kite.provisioning.commons.util.ProvKeyBox.wapPushMtTpsK;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class NcsWapPushView extends NcsConfigurationView {

    private static final Logger logger = LoggerFactory.getLogger(NcsWapPushView.class);

    private TextField mtTpsTxtFld;
    private TextField mtTpdTxtFld;

    private void init() {
        mtTpsTxtFld = new TextField(provisioning.getMsg("sp.ncs.wp.tps"));
        mtTpsTxtFld.setImmediate(true);
        mtTpsTxtFld.setRequired(true);
        mtTpsTxtFld.setRequiredError(provisioning.getMsg("sp.ncs.wp.tps.required.error"));
        mtTpsTxtFld.addValidator(createValidator("ncsWpTpsRegex", "sp.ncs.wp.tps.validation"));
        mtTpsTxtFld.addValidator(createValidator("sp.createApp.zero.validation", "sp.ncs.wp.zero.validation.error.message"));

        mtTpdTxtFld = new TextField(provisioning.getMsg("sp.ncs.wp.tpd"));
        mtTpdTxtFld.setImmediate(true);
        mtTpdTxtFld.setRequired(true);
        mtTpdTxtFld.setRequiredError(provisioning.getMsg("sp.ncs.wp.tpd.required.error"));
        mtTpdTxtFld.addValidator(createValidator("ncsWpTpdRegex", "sp.ncs.wp.tpd.validation"));
        mtTpdTxtFld.addValidator(createValidator("sp.createApp.zero.validation", "sp.ncs.wp.zero.validation.error.message"));
    }

    private void checkTpsTpdValidation() {
        int moTpsValue = Integer.parseInt(mtTpsTxtFld.getValue().toString());
        int moTpdValue = Integer.parseInt(mtTpdTxtFld.getValue().toString());
        if (moTpsValue >= moTpdValue) {
            throw new Validator.InvalidValueException(provisioning.getMsg("sp.ncs.wp.mt.tps.tpd.validation.message"));
        }
    }

    private void reloadData() {
        Map<String, Object> sp = provisioning.getSp();

        setValueToField(mtTpsTxtFld, sp.get(wapPushMtTpsK));
        setValueToField(mtTpdTxtFld, sp.get(wapPushMtTpdK));
    }

    private void addToForm() {
        form.addField("moTps", mtTpsTxtFld);
        form.addField("moTpd", mtTpdTxtFld);
    }

    @Override
    protected void changeToViewMode() {
        changeToConfirmMode();
    }

    @Override
    protected void changeToConfirmMode() {
        mtTpsTxtFld.setReadOnly(true);
        mtTpdTxtFld.setReadOnly(true);
    }

    @Override
    protected void changeToEditMode() {
        mtTpsTxtFld.setReadOnly(false);
        mtTpdTxtFld.setReadOnly(false);
    }

    @Override
    protected void changeToCancelView() {
        changeToViewMode();
    }

    @Override
    protected void setSpSessionValues() {
        Map<String, Object> sp = provisioning.getSp();

        sp.put(wapPushMtTpsK, mtTpsTxtFld.getValue());
        sp.put(wapPushMtTpdK, mtTpdTxtFld.getValue());
    }

    public NcsWapPushView(Provisioning provisioning) {
        this.provisioning = provisioning;

        init();
    }

    @Override
    public void saveData() {
    }

    @Override
    public boolean validateData() {
        try {
            setSpSessionValues();
            mtTpsTxtFld.validate();
            mtTpdTxtFld.validate();
            checkTpsTpdValidation();
            form.setComponentError(null);
        } catch (Validator.InvalidValueException e) {
            form.setComponentError(e);
            return false;
        } catch (Exception e) {
            logger.error("Unexpected error occurred while validating wap-push form", e);
            return false;
        }
        return true;
    }

    @Override
    public VerticalLayout loadComponents(final Panel panel) {
        VerticalLayout layout = new VerticalLayout();

        panel.removeAllComponents();
        panel.setCaption(provisioning.getMsg("sp.ncs.wp.title"));
        panel.addComponent(layout);

        addToForm();
        reloadData();
        refreshInChangedMode();

        layout.addComponent(form);
        layout.setComponentAlignment(form, Alignment.MIDDLE_CENTER);

        return layout;
    }
}
