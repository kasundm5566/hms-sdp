/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.sp.ncs;

import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.ui.*;
import hms.kite.provisioning.Provisioning;
import hms.kite.provisioning.commons.ui.VaadinUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class NcsSmsView extends NcsConfigurationView {

    private static Logger logger = LoggerFactory.getLogger(NcsSmsView.class);

    private CheckBox smsMtChkBox;
    private TextField mtTpsTxtFld;
    private TextField mtTpdTxtFld;

    private CheckBox smsMoChkBox;
    private TextField moTpsTxtFld;
    private TextField moTpdTxtFld;

    VerticalLayout mainLayout;

    private void setSolturaMode() {
        String isSolturaUser = (String) provisioning.getSp().get(solturaUserK);
        if (trueK.equals(isSolturaUser)) {
            smsMoChkBox.setValue(true);
            smsMtChkBox.setValue(true);
            smsMoChkBox.setReadOnly(true);
            smsMtChkBox.setReadOnly(true);
        }
    }

    private void reloadData() {
        Map<String, Object> sp = provisioning.getSp();

        setValueToField(smsMoChkBox, sp.get(smsMoK));
        setValueToField(moTpsTxtFld, sp.get(smsMoTpsK));
        setValueToField(moTpdTxtFld, sp.get(smsMoTpdK));

        setValueToField(smsMtChkBox, sp.get(smsMtK));
        setValueToField(mtTpsTxtFld, sp.get(smsMtTpsK));
        setValueToField(mtTpdTxtFld, sp.get(smsMtTpdK));
    }

    protected void setSpSessionValues() {
        Map<String, Object> sp = provisioning.getSp();

        sp.put(smsMoK, smsMoChkBox.getValue());
        sp.put(smsMoTpsK, moTpsTxtFld.getValue());
        sp.put(smsMoTpdK, moTpdTxtFld.getValue());

        sp.put(smsMtK, smsMtChkBox.getValue());
        sp.put(smsMtTpsK, mtTpsTxtFld.getValue());
        sp.put(smsMtTpdK, mtTpdTxtFld.getValue());
    }

    private void setTpsTpdValues(boolean value, TextField tpsField, TextField tpdField) {
        tpsField.setEnabled(value);
        tpdField.setEnabled(value);

        if (!value) {
            tpsField.setValue("");
            tpdField.setValue("");
        }
    }

    private Component createMtLayout() {
        FormLayout mtButtonLayout = new FormLayout();
        mtButtonLayout.addStyleName("sp-ncs-form");

        smsMtChkBox = new CheckBox(provisioning.getMsg("sp.ncs.sms.mt"));
        smsMtChkBox.setImmediate(true);
        smsMtChkBox.addListener(new Property.ValueChangeListener() {

            @Override
            public void valueChange(final Property.ValueChangeEvent event) {
                onMtCheckBoxChanged(Boolean.parseBoolean(event.getProperty().getValue().toString()));
            }
        });

        mtTpsTxtFld = new TextField(provisioning.getMsg("sp.ncs.sms.mt.tps"));
        mtTpsTxtFld.setImmediate(true);
        mtTpsTxtFld.setRequired(true);
        mtTpsTxtFld.setRequiredError(provisioning.getMsg("sp.ncs.sms.mt.tps.required.error"));
        mtTpsTxtFld.addValidator(createValidator("ncsSmsMtTpsRegex", "sp.ncs.sms.mt.tps.validation"));
        mtTpsTxtFld.addValidator(createValidator("sp.createApp.zero.validation", "sp.ncs.sms.zero.validation.error.message"));

        mtTpdTxtFld = new TextField(provisioning.getMsg("sp.ncs.sms.mt.tpd"));
        mtTpdTxtFld.setImmediate(true);
        mtTpdTxtFld.setRequired(true);
        mtTpdTxtFld.setRequiredError(provisioning.getMsg("sp.ncs.sms.mt.tpd.required.error"));
        mtTpdTxtFld.addValidator(createValidator("ncsSmsMtTpdRegex", "sp.ncs.sms.mt.tpd.validation"));
        mtTpdTxtFld.addValidator(createValidator("sp.createApp.zero.validation", "sp.ncs.sms.zero.validation.error.message"));

        mtButtonLayout.addComponent(smsMtChkBox);
        mtButtonLayout.addComponent(mtTpsTxtFld);
        mtButtonLayout.addComponent(mtTpdTxtFld);

        return mtButtonLayout;
    }

    private void onMtCheckBoxChanged(boolean value) {
        setTpsTpdValues(value, mtTpsTxtFld, mtTpdTxtFld);
    }

    private Component createMoLayout() {
        FormLayout moButtonLayout = new FormLayout();
        moButtonLayout.addStyleName("sp-ncs-form");

        smsMoChkBox = new CheckBox(provisioning.getMsg("sp.ncs.sms.mo"));
        smsMoChkBox.setImmediate(true);
        smsMoChkBox.addListener(new Property.ValueChangeListener() {

            @Override
            public void valueChange(final Property.ValueChangeEvent event) {
                onMoCheckBoxChanged(Boolean.parseBoolean(event.getProperty().getValue().toString()));
            }
        });

        moTpsTxtFld = new TextField(provisioning.getMsg("sp.ncs.sms.mo.tps"));
        moTpsTxtFld.setImmediate(true);
        moTpsTxtFld.setRequired(true);
        moTpsTxtFld.setRequiredError(provisioning.getMsg("sp.ncs.sms.mo.tps.required.error"));
        moTpsTxtFld.addValidator(createValidator("ncsSmsMoTpsRegex", "sp.ncs.sms.mt.tps.validation"));
        moTpsTxtFld.addValidator(createValidator("sp.createApp.zero.validation", "sp.ncs.sms.zero.validation.error.message"));

        moTpdTxtFld = new TextField(provisioning.getMsg("sp.ncs.sms.mo.tpd"));
        moTpdTxtFld.setImmediate(true);
        moTpdTxtFld.setRequired(true);
        moTpdTxtFld.setRequiredError(provisioning.getMsg("sp.ncs.sms.mo.tpd.required.error"));
        moTpdTxtFld.addValidator(createValidator("ncsSmsMoTpdRegex", "sp.ncs.sms.mt.tpd.validation"));
        moTpdTxtFld.addValidator(createValidator("sp.createApp.zero.validation", "sp.ncs.sms.zero.validation.error.message"));

        moButtonLayout.addComponent(smsMoChkBox);
        moButtonLayout.addComponent(moTpsTxtFld);
        moButtonLayout.addComponent(moTpdTxtFld);

        return moButtonLayout;
    }

    private void onMoCheckBoxChanged(boolean value) {
        setTpsTpdValues(value, moTpsTxtFld, moTpdTxtFld);
    }

    private void checkTpsTpdValidation(TextField tpsField, TextField tpdField) {
        int moTpsValue = Integer.parseInt(tpsField.getValue().toString());
        int moTpdValue = Integer.parseInt(tpdField.getValue().toString());
        if (moTpsValue >= moTpdValue) {
            throw new Validator.InvalidValueException(provisioning.getMsg("sp.ncs.sms.mt.tps.tpd.validation.message"));
        }
    }

    @Override
    protected void changeToEditMode() {
        smsMtChkBox.setReadOnly(false);
        smsMoChkBox.setReadOnly(false);

        onMoCheckBoxChanged(smsMoChkBox.booleanValue());
        onMtCheckBoxChanged(smsMtChkBox.booleanValue());
    }

    @Override
    protected void changeToConfirmMode() {
        VaadinUtil.setAllReadonly(true, form.getLayout());
    }

    @Override
    protected void changeToCancelView() {
        changeToViewMode();
    }

    protected void changeToViewMode() {
        changeToConfirmMode();
    }

    @Override
    public void saveData() {
    }

    public NcsSmsView(Provisioning provisioning) {
        this.provisioning = provisioning;

        form.setLayout(new VerticalLayout());
    }

    @Override
    public Component loadComponents(Panel panel) {
        panel.removeAllComponents();
        panel.setCaption(provisioning.getMsg("sp.ncs.sms.title"));

        form.getLayout().removeAllComponents();
        Layout formLayout = form.getLayout();

        Component moLayout = createMoLayout();
        formLayout.addComponent(moLayout);

        Component mtLayout = createMtLayout();
        formLayout.addComponent(mtLayout);

        reloadData();
        refreshInChangedMode();
        setSolturaMode();

        mainLayout = new VerticalLayout();
        mainLayout.addComponent(form);
        mainLayout.setComponentAlignment(form, Alignment.MIDDLE_CENTER);
        panel.addComponent(mainLayout);

        return mainLayout;
    }

    @Override
    public boolean validateData() {
        try {
            setSpSessionValues();

            boolean mtEnabled = smsMtChkBox.booleanValue();
            boolean moEnabled = smsMoChkBox.booleanValue();

            if (!mtEnabled && !moEnabled) {
                throw new Validator.InvalidValueException(provisioning.getMsg("sp.ncs.sms.selectMtOrMoValidation"));
            }
            if (mtEnabled) {
                mtTpsTxtFld.validate();
                mtTpdTxtFld.validate();
                checkTpsTpdValidation(mtTpsTxtFld, mtTpdTxtFld);
            }
            if (moEnabled) {
                moTpsTxtFld.validate();
                moTpdTxtFld.validate();
                checkTpsTpdValidation(moTpsTxtFld, moTpdTxtFld);
            }
            form.setComponentError(null);
        } catch (Validator.InvalidValueException e) {
            form.setComponentError(e);
            logger.debug("Validation failed [{}]", e.getMessage());
            return false;
        } catch (Exception e) {
            logger.error("Error occurred while validating sms ncs ", e);
            return false;
        }
        return true;
    }
}
