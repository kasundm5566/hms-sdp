/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.util;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class AppSlaRoles {

    public static final String APP_PERMISSION_ROLE_PREFIX = "ROLE_PROV_APP";

    public static final String APP_ID = "APP_ID";
    public static final String APP_NAME = "APP_NAME";
    public static final String DESCRIPTION = "DESCRIPTION";
    public static final String ALLOW_HOST_ADDRESS = "ALLOW_HOST_ADDRESS";
    public static final String WHITE_LIST = "WHITE_LIST";
    public static final String BLACK_LIST = "BLACK_LIST";
    public static final String REVENUE_SHARE = "REVENUE_SHARE";
    public static final String REMARKS = "REMARKS";
    public static final String PRODUCTION_REQUEST_DATE = "PRODUCTION_REQUEST_DATE";
    public static final String PRODUCTION_START_DATE = "PRODUCTION_START_DATE";
    public static final String ALLOW_EXPIRATION = "ALLOW_EXPIRATION";
    public static final String PRODUCTION_END_DATE = "PRODUCTION_END_DATE";
    public static final String HOSTING_SPACE_REQUIRED = "HOSTING_SPACE_REQUIRED";
    public static final String ACTIVE_START_DATE = "ACTIVE_START_DATE";

    public static final String ADVERTISING = "ADVERTISING";
    public static final String GOVERN = "GOVERN";
    public static final String MASKING = "MASKING";
    public static final String APPLY_TAX = "APPLY_TAX";
}
