package hms.kite.provisioning.daemon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.*;
import static hms.kite.provisioning.ProvisioningServiceRegistry.eventRouter;
import static hms.kite.provisioning.commons.util.ProvKeyBox.ncsesK;
import static hms.kite.util.KiteKeyBox.*;

/**
 * Created by IntelliJ IDEA.
 * User: chandana
 * Date: 2/1/12
 * Time: 10:26 AM
 * To change this template use File | Settings | File Templates.
 */
public class AppExpirationDaemon {

    private static Logger logger = LoggerFactory.getLogger(AppExpirationDaemon.class);

    public void execute() {
        try {
            Map<String, Object> query = new HashMap();
            query.put(endDateK, new HashMap<String, Object>() {{
                put("$lt", new Date());
            }});
            query.put(statusK, new HashMap<String, Object>() {{
                put("$ne", terminateK);
            }});
            query.put(expireK, true);
            List<Map<String, Object>> toBeExpiredApplication = appRepositoryService().findAppsRange(query, 0, 100);
            logger.debug("Found {} application to expire", toBeExpiredApplication.size());
            for (Map<String, Object> app : toBeExpiredApplication) {
                terminate(app);
            }

        } catch (Exception e) {
            logger.error("Unexpected error occurred in Application Expiration scheduler", e);
        }
    }

    private void terminate(Map<String, Object> app) {
        logger.debug("Terminating application [{}] ", app);
        app.put(statusK, terminateK);
        appRepositoryService().update(app);
        terminateNcses(app);
        fireEventRouter(app);
    }

    private void fireEventRouter(Map<String, Object> app) {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put(eventNameK, appTerminateAutomaticEvent);
        data.put(appK, new HashMap<String, Object>(app));
        data.put(spK, spRepositoryService().findSpBySpId((String) app.get(spIdK)));
        eventRouter().fire(data);
    }

    private void terminateNcses(Map<String, Object> app) {
        List<Map<String, Object>> ncses = (List<Map<String, Object>>) app.get(ncsesK);

        if (ncses == null) return;

        for (Map<String, Object> ncs : ncses) {
            String appId = (String) app.get(appIdK);
            String ncsType = (String) ncs.get(ncsTypeK);
            String operator = (String) ncs.get(operatorK);
            operator = operator == null ? "" : operator;
            ncsRepositoryService().update(appId, ncsType, operator, terminateK);
        }
    }
}
