/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.provisioning.email;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.provisioning.commons.util.ProvKeyBox.currentDateAndTimeK;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public abstract class AbstractEmailRenderer implements EmailRender {

    private Map<String, String> systemDataMap;

    public Map<String, String> getANewSystemDataMap() {
        Map<String, String> newSystemDataMap = new HashMap<String, String>(systemDataMap);
        newSystemDataMap.put(currentDateAndTimeK, EmailUtil.getCurrentDateTime());
        return newSystemDataMap;
    }

    public void setSystemDataMap(Map<String, String> systemDataMap) {
        this.systemDataMap = systemDataMap;
    }
}
