/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.provisioning.ui.common;

import com.vaadin.data.Validator;
import com.vaadin.ui.*;
import hms.kite.provisioning.commons.ui.BaseApplication;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ConfirmMessage  {

    private BaseApplication application;
    private String title;
    private String message;
    private ConfirmationListener listener;
    private boolean noteRequired = true;

    public ConfirmMessage(BaseApplication application, String title, String message, ConfirmationListener listener) {
        this.application = application;
        this.title = title;
        this.message = message;
        this.listener = listener;
    }

     public void showMessageWindow() {

        final Window subWindow = new Window();
        subWindow.setCaption(title);
        subWindow.setModal(true);
        subWindow.setWidth("325px");

        VerticalLayout layout = (VerticalLayout) subWindow.getContent();
        layout.setSpacing(true);
        layout.setMargin(true);

        Label messageLbl = new Label(message);
        layout.addComponent(messageLbl);

        Label noteLabel = new Label(application.getMsg("provisioning.confirm.dialog.note"));
        layout.addComponent(noteLabel);

        final TextArea textArea = new TextArea();

        if (noteRequired) {
            textArea.setRequired(true);
            textArea.setRequiredError(application.getMsg("provisioning.confirm.dialog.note.required"));
            layout.addComponent(textArea);
        }

        HorizontalLayout buttonBar = new HorizontalLayout();
        buttonBar.setSpacing(true);
        layout.addComponent(buttonBar);
        layout.setComponentAlignment(buttonBar, Alignment.MIDDLE_CENTER);

        Button yesButton = new Button(application.getMsg("provisioning.confirm.dialog.yes"));
        buttonBar.addComponent(yesButton);
        yesButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    textArea.validate();
                    String note = textArea.getValue().toString();
                    subWindow.getParent().removeWindow(subWindow);
                    listener.onYes(note);
                } catch (Validator.InvalidValueException e) {
                    //Nothing to do
                }
            }
        });

        Button noButton = new Button(application.getMsg("provisioning.confirm.dialog.no"));
        buttonBar.addComponent(noButton);
        noButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                subWindow.getParent().removeWindow(subWindow);
            }
        });

        application.getMainWindow().addWindow(subWindow);
    }

    public void setNoteRequired(boolean noteRequired) {
        this.noteRequired = noteRequired;
    }





}
