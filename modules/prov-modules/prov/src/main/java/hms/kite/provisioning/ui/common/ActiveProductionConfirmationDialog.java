/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.provisioning.ui.common;

import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.terminal.UserError;
import com.vaadin.ui.*;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.util.KiteKeyBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ActiveProductionConfirmationDialog {

    private static final Logger logger = LoggerFactory.getLogger(ActiveProductionConfirmationDialog.class);

    private BaseApplication application;
    private String header;
    private String message;
    private ActiveProductionConfirmationListener listener;
    private DateField date = new DateField();
    private VerticalLayout confirmationLayout;
    private Property.ValueChangeListener dateValidationListener;

    public ActiveProductionConfirmationDialog(BaseApplication application, String header, String message, ActiveProductionConfirmationListener listener) {
        this.application = application;
        this.header = header;
        this.message = message;
        this.listener = listener;
        dateValidationListener = new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                try {
                    validateDate();
                } catch (Validator.InvalidValueException e) {
                    //
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while validating the date", e);
                }
            }
        };
    }

    public void showDialog(final Map<String, Object> app) {

        final Window subWindow = new Window();
        subWindow.setCaption(header);
        subWindow.setModal(true);
        subWindow.setWidth("325px");

        confirmationLayout = (VerticalLayout) subWindow.getContent();
        confirmationLayout.setSpacing(true);
        confirmationLayout.setMargin(true);

        final VerticalLayout layout = new VerticalLayout();

        final OptionGroup activeProductionConfirmationOpt = new OptionGroup();
        activeProductionConfirmationOpt.setImmediate(true);

        activeProductionConfirmationOpt.addItem(sendActiveProductionImmediatelyK);
        activeProductionConfirmationOpt.setItemCaption(sendActiveProductionImmediatelyK, application.getMsg("provisioning.confirm.dialog.send.active.immediately"));

        activeProductionConfirmationOpt.addItem(sendActiveProductionAtRequestedTimeK);
        activeProductionConfirmationOpt.setItemCaption(sendActiveProductionAtRequestedTimeK, application.getMsg("provisioning.confirm.dialog.send.active.at.requested.time"));

        activeProductionConfirmationOpt.addItem(sendActiveProductionAtScheduledTimeK);
        activeProductionConfirmationOpt.setItemCaption(sendActiveProductionAtScheduledTimeK, application.getMsg("provisioning.confirm.dialog.send.active.at.scheduled.time"));

        activeProductionConfirmationOpt.select(sendActiveProductionImmediatelyK);


        date.setDateFormat("yyyy-MM-dd hh:mm a");
        date.setImmediate(true);

        final Label messageLbl = new Label(message);

        activeProductionConfirmationOpt.addListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                try {
                    changeView(activeProductionConfirmationOpt, messageLbl, app);
                } catch (Exception e) {
                    logger.error("Unexpected error occurred ", e);
                }
            }
        });


        final TextField textArea = new TextField();

        Label noteLbl = new Label(application.getMsg("provisioning.confirm.dialog.note"));

        confirmationLayout.addComponent(activeProductionConfirmationOpt);

        layout.addComponent(messageLbl);
        layout.addComponent(date);
        messageLbl.setVisible(false);
        date.setVisible(false);
        layout.addComponent(noteLbl);
        textArea.setWidth("284px");
        textArea.setHeight("100px");
        textArea.setRequired(true);
        textArea.setRequiredError(application.getMsg("provisioning.confirm.dialog.note.required"));
        layout.addComponent(textArea);

        HorizontalLayout buttonBar = new HorizontalLayout();
        buttonBar.setSpacing(true);
        layout.addComponent(buttonBar);
        layout.setComponentAlignment(buttonBar, Alignment.MIDDLE_CENTER);
        confirmationLayout.addComponent(layout);


        Button yesButton = new Button(application.getMsg("provisioning.confirm.dialog.yes"));
        buttonBar.addComponent(yesButton);
        yesButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    textArea.validate();
                    if ((date.getValue() != null) &&
                            sendActiveProductionAtScheduledTimeK.equals(activeProductionConfirmationOpt.getValue())) {
                        validateDate();
                    }
                    HashMap<String, Object> dataMap = new HashMap();
                    dataMap.put(KiteKeyBox.noteK, textArea.getValue());
                    dataMap.put(KiteKeyBox.activeProductionStartDateK, date.getValue());
                    dataMap.put("active-production-date", activeProductionConfirmationOpt.getValue());
                    subWindow.getParent().removeWindow(subWindow);
                    listener.onYesConfirmation(dataMap);

                } catch (Validator.InvalidValueException e) {
                    //Nothing to do
                } catch (Exception e) {
                    logger.error("Unexpected error occurred ", e);
                }
            }
        });

        Button noButton = new Button(application.getMsg("provisioning.confirm.dialog.no"));
        buttonBar.addComponent(noButton);
        noButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                subWindow.getParent().removeWindow(subWindow);
            }
        });


        application.getMainWindow().addWindow(subWindow);
    }

    private void changeView(OptionGroup activeProductionConfirmationOpt, Label messageLbl, Map<String, Object> app) {
        if (sendActiveProductionImmediatelyK.equals(activeProductionConfirmationOpt.getValue())) {
            reinitializeDate(messageLbl);
        } else if (sendActiveProductionAtScheduledTimeK.equals(activeProductionConfirmationOpt.getValue())) {
            date.setReadOnly(false);
            if (app.containsKey(activeProductionStartDateK)) {
                date.setValue((Date) app.get(activeProductionStartDateK));
            } else {
                date.setValue(null);
            }
            date.setRequired(true);
            date.setRequiredError(application.getMsg("provisioning.confirm.dialog.Date.required"));
            date.addListener(dateValidationListener);
            date.setVisible(true);
            messageLbl.setValue(application.getMsg("provisioning.confirm.dialog.send.active.at.scheduled.time.caption"));
            messageLbl.setVisible(true);
        } else if (sendActiveProductionAtRequestedTimeK.equals(activeProductionConfirmationOpt.getValue())) {
            reinitializeDate(messageLbl);
            date.setValue(app.get(appRequestDateK));
            date.setRequired(false);
            date.setReadOnly(true);
            date.setVisible(true);
            messageLbl.setValue(application.getMsg("provisioning.confirm.dialog.send.active.at.requested.time.caption"));
            messageLbl.setVisible(true);
        }
    }

    private void reinitializeDate(Label messageLbl) {
        date.setValue(null);
        date.setRequired(false);
        date.setRequiredError(null);
        date.setVisible(false);
        messageLbl.setVisible(false);
        date.setReadOnly(false);
        date.removeListener(dateValidationListener);
    }

    private void validateDate() {
        if (((Date) date.getValue()).getTime() >= (new Date().getTime())) {
            logger.debug("Correct date selected.....");
            date.setComponentError(null);
        } else {
            date.setComponentError(new UserError(application.getMsg("provisioning.confirm.dialog.date.error.msg")));
            throw new Validator.InvalidValueException(application.getMsg("provisioning.confirm.dialog.date.error.msg"));
        }
    }


}