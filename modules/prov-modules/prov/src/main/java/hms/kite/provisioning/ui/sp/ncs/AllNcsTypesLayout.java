/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.sp.ncs;

import com.vaadin.ui.*;
import hms.kite.provisioning.Provisioning;
import hms.kite.provisioning.ui.common.ViewMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.provisioning.ui.common.ViewMode.EDIT;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public abstract class AllNcsTypesLayout extends VerticalLayout {

    private static final Logger logger = LoggerFactory.getLogger(AllNcsTypesLayout.class);

    private Panel panel;
    private VerticalLayout mainLayout;

    private void init() {
        this.panel = new Panel();
        this.mainLayout = new VerticalLayout();

        panel.addComponent(mainLayout);
        addComponent(panel);
        setSpacing(true);
    }

    private void addNcsConfigurationView(NcsConfigurationView ncsConfigurationView, Object selectedStatus) {
        if (trueK.equals(selectedStatus)) {
            ncsConfigurationViewList.add(ncsConfigurationView);
        }
    }

    protected Provisioning provisioning;
    protected List<NcsConfigurationView> ncsConfigurationViewList = new ArrayList<NcsConfigurationView>();
    protected ViewMode viewMode = EDIT;

    protected void reloadComponents() {
        mainLayout.removeAllComponents();
        loadComponents();
    }

    protected void showErrorNotification(String message) {
        provisioning.getMainWindow().showNotification(new Window.Notification(message));
    }

    protected abstract Component createHeader();

    protected abstract Component createFooter();

    public AllNcsTypesLayout(Provisioning provisioning, ViewMode viewMode) {
        this.provisioning = provisioning;
        this.viewMode = viewMode;

        init();
    }

    public void setTitle(String title) {
        panel.setCaption(title);
    }

    public void loadComponents() {
        logger.debug("Loading Ncs Window with [{}] mode", viewMode);
        Component header = createHeader();
        mainLayout.addComponent(header);

        for (NcsConfigurationView ncsConfigurationView : ncsConfigurationViewList) {
            Panel childPanel = new Panel();
            childPanel.setStyleName("child-panel");
            mainLayout.addComponent(childPanel);
            mainLayout.setComponentAlignment(childPanel, Alignment.MIDDLE_CENTER);
            ncsConfigurationView.changeMode(viewMode);
            ncsConfigurationView.loadComponents(childPanel);
        }

        Component footer = createFooter();
        mainLayout.addComponent(footer);
        mainLayout.setSpacing(true);
    }

    public void reloadSpNcs(Map<String, Object> sp) {
        ncsConfigurationViewList.clear();
        addNcsConfigurationView(new NcsAasView(provisioning), sp.get(aasSelectedK));
        addNcsConfigurationView(new NcsCasView(provisioning), sp.get(casSelectedK));
        addNcsConfigurationView(new NcsDownloadableView(provisioning), sp.get(downloadableSelectedK));
        addNcsConfigurationView(new NcsLbsView(provisioning), sp.get(lbsSelectedK));
        addNcsConfigurationView(new NcsSmsView(provisioning), sp.get(smsSelectedK));
        addNcsConfigurationView(new NcsSubscriptionView(provisioning), sp.get(subscriptionSelectedK));
        addNcsConfigurationView(new NcsUssdView(provisioning), sp.get(ussdSelectedK));
        addNcsConfigurationView(new NcsWapPushView(provisioning), sp.get(wapPushSelectedK));
        addNcsConfigurationView(new NcsVodafoneApiView(provisioning), sp.get(vdfApisSelected));
    }
}
