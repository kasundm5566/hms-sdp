/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.email;

import hms.common.registration.api.response.BasicUserResponseMessage;
import hms.common.registration.api.util.RestApiKeys;
import hms.kite.provisioning.services.httpclient.CurHttpClient;
import org.antlr.stringtemplate.StringTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SpEmailRenderer extends AbstractEmailRenderer {

    @Resource
    private CurHttpClient curHttpClient;

    private static final Logger logger = LoggerFactory.getLogger(SpEmailRenderer.class);

    @Override
    public void render(Map<String, Object> email, Map<String, Object> data) {

        String emailBodyTemplate = (String) email.get(emailBodyK);
        String subjectTemplate = (String) email.get(emailSubjectK);
        String eventName = (String) data.get(eventNameK);

        HashMap<String, Object> sp = (HashMap<String, Object>) data.get(spK);

        if (sp == null) {
            throw new IllegalStateException("Can not find sp details to notify event "
                    + eventName + ", for Data " + data);
        }

        logger.debug("Email {} is being rendered..", email);

        StringTemplate emailBodyStringTemplate = new StringTemplate(emailBodyTemplate);
        StringTemplate subjectStringTemplate = new StringTemplate(subjectTemplate);

        BasicUserResponseMessage responseMessage = curHttpClient.get((String) sp.get(createdByK));

        List<String> toAddressList = (ArrayList<String>) email.get(toAddressListK);
        String creatorEmailAddress = responseMessage.getAdditionalData(RestApiKeys.EMAIL);
        String corpEmailAddress = responseMessage.getAdditionalData(RestApiKeys.CORPORATE_USER_EMAIL);

        if (creatorEmailAddress == null) {
            logger.error("Cannot find email address for creator [{}]", sp.get(createdByK));
        } else if (creatorEmailAddress.equals(corpEmailAddress)) {
            toAddressList.add(creatorEmailAddress);
        } else if (corpEmailAddress == null) {
            logger.error("Cannot find corporate email address for creator [{}]", sp.get(createdByK));
        } else {
            toAddressList.add(creatorEmailAddress);
            ((List<String>) email.get(ccAddressListK)).add(corpEmailAddress);
        }

        sp.put(firstNameK, sp.get(coopUserNameK));

        Map<String, String> newSystemDataMap = getANewSystemDataMap();

        emailBodyStringTemplate.setAttribute(spK, sp);
        emailBodyStringTemplate.setAttribute(systemK, newSystemDataMap);
        email.put(emailBodyK, emailBodyStringTemplate.toString());

        subjectStringTemplate.setAttribute(spK, sp);
        subjectStringTemplate.setAttribute(systemK, newSystemDataMap);

        email.put(emailSubjectK, subjectStringTemplate.toString());

        logger.debug("Rendered Email: {}", email);
    }

}
