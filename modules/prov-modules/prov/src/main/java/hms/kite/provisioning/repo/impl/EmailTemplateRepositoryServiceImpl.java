/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.repo.impl;

import hms.kite.provisioning.repo.EmailTemplateRepository;
import hms.kite.provisioning.repo.EmailTemplateRepositoryService;

import java.util.Map;

/**
 * $LastChangedDate $
 * $LastChangedBy $
 * $LastChangedRevision $
 */
public class EmailTemplateRepositoryServiceImpl implements EmailTemplateRepositoryService {

    private EmailTemplateRepository emailTemplateRepository;

    @Override
    public boolean isEventNameExist(String eventName) {
        return emailTemplateRepository.isEventNameExist(eventName);
    }

    @Override
    public Map<String, Object> findEventByName(String eventName) {
        return emailTemplateRepository.findEventByName(eventName);
    }

    @Override
    public void create(Map<String, Object> event) {
        emailTemplateRepository.create(event);
    }

    @Override
    public void deleteEvent(String eventName) {
        emailTemplateRepository.deleteEvent(eventName);
    }

    @Override
    public void deleteAll() {
        emailTemplateRepository.deleteAll();
    }

    @Override
    public void updateEvent(Map<String, Object> newEvent) {
        emailTemplateRepository.updateEvent(newEvent);
    }

    public void setEmailTemplateRepository(EmailTemplateRepository emailTemplateRepository) {
        this.emailTemplateRepository = emailTemplateRepository;
    }

}
