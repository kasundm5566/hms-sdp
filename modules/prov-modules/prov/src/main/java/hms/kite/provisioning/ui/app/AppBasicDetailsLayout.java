/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.app;

import com.google.common.base.Optional;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.terminal.*;
import com.vaadin.ui.*;
import com.vaadin.ui.Button;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import hms.kite.provisioning.commons.ui.AbstractBasicDetailsLayout;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.component.popup.HelpDetails;
import hms.kite.provisioning.commons.util.ProvIdGenerator;
import hms.kite.provisioning.ui.app.validator.AppNameExistsValidator;
import hms.kite.provisioning.ui.app.validator.ProvAddressValidator;
import hms.kite.provisioning.ui.app.validator.ProvBooleanExpressionValidator;
import hms.kite.provisioning.ui.component.ExpirableComponent;
import hms.kite.provisioning.ui.component.HostingSpaceComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.ncsRepositoryService;
import static hms.kite.provisioning.ProvisioningServiceRegistry.*;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.provisioning.commons.util.StringConverter.*;
import static hms.kite.provisioning.util.AppSlaRoles.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */

public class AppBasicDetailsLayout extends AbstractBasicDetailsLayout {

    private static final Logger logger = LoggerFactory.getLogger(AppBasicDetailsLayout.class);

    private static final String DATE_FORMAT_STRING = "dd/MM/yyyy h:mm a";
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(DATE_FORMAT_STRING);
    private static final String HELP_IMAGE = "help.jpg";
    public static final String HELP_IMG_WIDTH = "720px";
    public static final String HELP_IMG_HEIGHT = "820px";
    public static final String HELP_IMG_STYLE_NAME = "link";
    public static final String HELP_BUTTON_NAME = "Help";

    private final String width = "350px";
    private final String height = "50px";

    private BaseApplication application;

    private Map<String, Object> app;

    private Form form;

    private TextField appIdTxtFld;
    private TextField appNameTxtFld;
    private TextField descriptionTxtFld;
    private TextField allowHostAddressTxtFld;
    private TextField whiteListTxtFld;
    private TextField blackListTxtFld;
    private TextField revenueShareTxtFld;
    private TextField remarksTxtFld;
    private TextField acStartDateFld;

    private DateField productionRequestedDateFld;
    private DateField productionStartDateFld;
    private ExpirableComponent expirableComponent;
    private DateField productionEndDateFld;
    private HostingSpaceComponent hostingSpaceComponent;

    private String appStatus;
    private PopupView popupView;

    private void init() {
        setWidth("100%");

        form = new Form();
        form.setImmediate(true);

        createAppIdFld();
        createAppNameFld();
        createDescriptionFld();
        createHostAddressFld();
        createWhiteListFld();
        createBlackListFld();
        createRevenueShareFld();
        createProductionRequestDateFld();
        createProductionStartDateFld();
        createProductionEndDateView();
        if(isAppHostingSpaceCheckBoxEnable())
        {
            createHostingSpaceRequiredFld();
        }
        createApprovedProductionView();
        createRemarksFld();
        createAppPublishLink();
        if(isHelpButtonLinkEnable()) {
            createHelperLink();
        }
        form.setWidth("516px");
        addComponent(form);
        setComponentAlignment(form, Alignment.MIDDLE_CENTER);
    }

    private void createAppIdFld() {
        appIdTxtFld = getFieldWithPermission(APP_ID, new TextField());
        if (isFieldNotNull(appIdTxtFld)) {
            appIdTxtFld.setCaption(application.getMsg("sp.createApp.appIdLabel"));
            appIdTxtFld.setWidth(width);
            if (app.get(appIdK) == null && initialK.equals(appStatus)) {
                String appId = ProvIdGenerator.generateAppId();
                appIdTxtFld.setValue(appId);
            }
            form.addField("appId", appIdTxtFld);
        }
    }

    private void createHelperLink() {
        Button helpButtonLink = createHelpButtonLink();
        addComponent(helpButtonLink);
        setComponentAlignment(helpButtonLink, Alignment.TOP_RIGHT);
    }

    private Button createHelpButtonLink() {
        final HelpDetails helpDetails = new HelpDetails(application, HELP_IMAGE);
        final Embedded image = helpDetails.getHelpImage();
        Button helpButtonLink = new Button(HELP_BUTTON_NAME);
        helpButtonLink.setStyleName(HELP_IMG_STYLE_NAME);
        helpButtonLink.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent clickEvent) {
                getWindow().addWindow(helpDetails.generateHelpImageSubWindow(image, HELP_IMG_WIDTH, HELP_IMG_HEIGHT));
            }
        });
        return helpButtonLink;
    }

    private void createAppNameFld() {
        appNameTxtFld = getFieldWithPermission(APP_NAME, new TextField());
        if (isFieldNotNull(appNameTxtFld)) {
            appNameTxtFld.setCaption(application.getMsg("sp.createApp.appNameLabel"));
            appNameTxtFld.setWidth(width);
            appNameTxtFld.setRequired(true);
            appNameTxtFld.setRequiredError(application.getMsg("sp.createApp.appNameIsEmptyValidation"));
            appNameTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("sp.createApp.appname.validator"),
                    application.getMsg("sp.createApp.appNameEntriesValidation")));

            if (initialK.equals(appStatus)) {
                appNameTxtFld.addValidator(new AppNameExistsValidator(application.getMsg("sp.app.create.form.app.name.already.exists.message"), Optional.absent()));
            } else if(draftK.equals(appStatus)) {
                appNameTxtFld.addValidator(new AppNameExistsValidator(application.getMsg("sp.app.create.form.app.name.already.exists.message"), Optional.fromNullable(app.get(nameK))));
            }

            form.addField("appName", appNameTxtFld);
        }
    }

    private void createDescriptionFld() {
        descriptionTxtFld = getFieldWithPermission(DESCRIPTION, new TextField());
        if (isFieldNotNull(descriptionTxtFld)) {
            descriptionTxtFld.setCaption(application.getMsg("sp.createApp.appDescriptionLabel"));
            descriptionTxtFld.setHeight(height);
            descriptionTxtFld.setWidth(width);
            descriptionTxtFld.setRequired(true);
            descriptionTxtFld.setRequiredError(application.getMsg("sp.createApp.appDescriptionEmptyValidation"));
            descriptionTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("sp.createApp.appDescription.validator"),
                    application.getMsg("sp.createApp.appDescriptionValidation")));
            form.addField("description", descriptionTxtFld);
        }
    }

    private void createHostAddressFld() {
        allowHostAddressTxtFld = getFieldWithPermission(ALLOW_HOST_ADDRESS, new TextField());
        if (isFieldNotNull(allowHostAddressTxtFld)) {
            allowHostAddressTxtFld.setCaption(application.getMsg("sp.createApp.appHostAddressLabel"));
            allowHostAddressTxtFld.setHeight(height);
            allowHostAddressTxtFld.setWidth(width);
            allowHostAddressTxtFld.setRequired(true);
            allowHostAddressTxtFld.setRequiredError(application.getMsg("sp.createApp.appHostAddressIsEmptyValidation"));
            allowHostAddressTxtFld.addValidator(new ProvAddressValidator(application,
                    "sp.createApp.ipAddressRegularExpression", "sp.createApp.appHostAddressValidation"));
            if (isDefaultAllowedHostAddressesAllow()) {
                allowHostAddressTxtFld.setValue(getDefaultAllowedHostAddresses());
            }
            form.addField("allowHostAddresses", allowHostAddressTxtFld);
        }
    }

    private void createWhiteListFld() {
        whiteListTxtFld = getFieldWithPermission(WHITE_LIST, new TextField());
        if (isFieldNotNull(whiteListTxtFld)) {
            whiteListTxtFld.setCaption(application.getMsg("sp.createApp.appWhiteListLabel"));
            whiteListTxtFld.setHeight(height);
            whiteListTxtFld.setWidth(width);
            whiteListTxtFld.setRequired(getWhiteListMandatory());
            whiteListTxtFld.setRequiredError(application.getMsg("sp.createApp.appWhiteListIsEmptyValidation"));
            whiteListTxtFld.addValidator(new ProvAddressValidator(application,
                    "sp.createApp.whiteListRegularExpression", "sp.createApp.app.white.list.validation"));
            form.addField("whiteListUsers", whiteListTxtFld);
        }
    }

    private void createBlackListFld() {
        blackListTxtFld = getFieldWithPermission(BLACK_LIST, new TextField());
        if (isFieldNotNull(blackListTxtFld)) {
            blackListTxtFld.setCaption(application.getMsg("sp.createApp.appBlackListLabel"));
            blackListTxtFld.setHeight(height);
            blackListTxtFld.setWidth(width);
            blackListTxtFld.addValidator(new ProvAddressValidator(application,
                    "sp.createApp.blackListRegularExpression", "sp.createApp.app.black.list.validation"));
            form.addField("blackListUsers", blackListTxtFld);
        }
    }

    private void createRevenueShareFld() {
        revenueShareTxtFld = getFieldWithPermission(REVENUE_SHARE, new TextField());
        if (isFieldNotNull(revenueShareTxtFld)) {
            revenueShareTxtFld.setCaption(application.getMsg("sp.createApp.appRevenue"));
            revenueShareTxtFld.setWidth(width);
            if (isDefaultRevenueShareAllow()) {
                revenueShareTxtFld.setValue(getDefaultRevenueShare());
            }
            revenueShareTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("sp.createApp.revenue.validation"),
                    application.getMsg("sp.createApp.appRevenueShareNumericValueValidation")));
            form.addField("revenueShare", revenueShareTxtFld);
        }
    }

    private void createProductionRequestDateFld() {
        productionRequestedDateFld = getFieldWithPermission(PRODUCTION_REQUEST_DATE, new DateField());
        if (!initialK.equals(appStatus) && isFieldNotNull(productionRequestedDateFld)) {
            productionRequestedDateFld.setCaption(application.getMsg("sp.createApp.production.requestedDate"));
            productionRequestedDateFld.setDateFormat(DATE_FORMAT_STRING);
            if (isAppRequestedDateColorAllow()) {
                productionRequestedDateFld.setStyleName("app-requested-date");
            }
            form.addField("productionRequestedDate", productionRequestedDateFld);
        }
    }

    private void createProductionStartDateFld() {
        productionStartDateFld = getFieldWithPermission(PRODUCTION_START_DATE, new DateField());
        if (isFieldNotNull(productionStartDateFld)) {
            productionStartDateFld.setCaption(application.getMsg("sp.createApp.production.startDate"));
            productionStartDateFld.setDateFormat(DATE_FORMAT_STRING);
            form.addField("productionStartDate", productionStartDateFld);
        }
    }

    private void createProductionEndDateView() {
        boolean defaultExpire = getDefaultExpire();
        expirableComponent = getFieldWithPermission(ALLOW_EXPIRATION, new ExpirableComponent());
        if (isFieldNotNull(expirableComponent)) {
            expirableComponent.setCaption(application.getMsg("sp.createApp.is.expirable"));
            expirableComponent.setValue(defaultExpire);
            form.addField("expirable", expirableComponent);
        }

        productionEndDateFld = getFieldWithPermission(PRODUCTION_END_DATE, new DateField());
        if (isFieldNotNull(expirableComponent) && isFieldNotNull(productionEndDateFld)) {
            productionEndDateFld.setCaption(application.getMsg("sp.createApp.production.endDate"));
            productionEndDateFld.setDateFormat(DATE_FORMAT_STRING);
            productionEndDateFld.setRequired(defaultExpire);
            productionEndDateFld.setVisible(defaultExpire);
            productionEndDateFld.setRequiredError(application.getMsg("sp.createApp.end.date.null.validation"));
            productionEndDateFld.setInvalidAllowed(true);
            productionEndDateFld.addValidator(new ProvBooleanExpressionValidator(application.getMsg("sp.createApp.end.date.null.validation"),
                    ProvBooleanExpressionValidator.endDateEmptyValidationExpression(expirableComponent, productionEndDateFld)));
            productionEndDateFld.addValidator(new ProvBooleanExpressionValidator(application.getMsg("sp.createApp.start.greater.than.now.date.validation"),
                    ProvBooleanExpressionValidator.beforeEndDateValidationExpression(expirableComponent, productionEndDateFld)));
            productionEndDateFld.addValidator(new ProvBooleanExpressionValidator(application.getMsg("sp.createApp.start.greater.than.end.date.validation"),
                    ProvBooleanExpressionValidator.startAndEndDateValidationExpression(expirableComponent, productionStartDateFld, productionEndDateFld)));
            productionEndDateFld.addValidator(endTimeWithSubscriptionValidator());
            form.addField("productionEndDate", productionEndDateFld);
            expirableComponent.setDateField(productionEndDateFld);
        }
    }

    private void createHostingSpaceRequiredFld() {
        hostingSpaceComponent = getFieldWithPermission(HOSTING_SPACE_REQUIRED, new HostingSpaceComponent());
        if (isFieldNotNull(hostingSpaceComponent)) {
            hostingSpaceComponent.setCaption(application.getMsg("sp.createApp.hosting.space.required"));
            form.addField("hostingSpaceComponent", hostingSpaceComponent);
        }
    }

    private Validator endTimeWithSubscriptionValidator() {
        return new Validator() {

            private String errorMessage;

            @Override
            public void validate(Object value) throws InvalidValueException {
                if (!isValid(value)) {
                    throw new InvalidValueException(errorMessage);
                }
            }

            @Override
            public boolean isValid(Object value) {
                if (expirableComponent.getValue().equals(false)) {
                    return true;
                }

                Date endDate = (Date) productionEndDateFld.getValue();

                List<Map<String, Object>> ncses = (List<Map<String, Object>>) app.get(ncsesK);

                if (ncses == null) {
                    return true;
                }

                Map<String, Object> subscriptionNcs = null;

                for (Map<String, Object> ncs : ncses) {
                    if (subscriptionK.equals(ncs.get(ncsTypeK))) {
                        subscriptionNcs = ncs;
                    }
                }

                if (subscriptionNcs != null) {
                    Map<String, Object> subscription = ncsRepositoryService().findNcs((String) app.get(appIdK), subscriptionK, "");
                    if (subscription != null) {
                        String frequency = (String) subscription.get(frequencyK);
                        if ("monthly".equals(frequency)) {
                            Calendar nextMonthFirstDate = Calendar.getInstance();
                            nextMonthFirstDate.add(Calendar.MONTH, 1);
                            nextMonthFirstDate.set(Calendar.DAY_OF_MONTH, 1);
                            nextMonthFirstDate.set(Calendar.HOUR, 0);
                            nextMonthFirstDate.set(Calendar.MINUTE, 0);
                            nextMonthFirstDate.set(Calendar.SECOND, 0);
                            if (endDate.before(nextMonthFirstDate.getTime())) {
                                errorMessage = application.getMsg("sp.createApp.appEndDateValidation",
                                        DATE_FORMAT.format(nextMonthFirstDate.getTime()));
                                return false;
                            }
                        } else if ("weekly".equals(frequency)) {
                            Calendar nextWeekFirstDate = Calendar.getInstance();
                            nextWeekFirstDate.add(Calendar.WEEK_OF_YEAR, 1);
                            nextWeekFirstDate.set(Calendar.DAY_OF_WEEK, 1);
                            nextWeekFirstDate.set(Calendar.HOUR, 0);
                            nextWeekFirstDate.set(Calendar.MINUTE, 0);
                            nextWeekFirstDate.set(Calendar.SECOND, 0);
                            if (endDate.before(nextWeekFirstDate.getTime())) {
                                errorMessage = application.getMsg("sp.createApp.appEndDateValidation",
                                        DATE_FORMAT.format(nextWeekFirstDate.getTime()));
                                return false;
                            }
                        } else {
                            logger.error("Unknown frequency [{}] found. validation will not done for the end date");
                        }
                    } else {
                        logger.debug("No Validation is done. since no subscription ncs found");
                    }
                } else {
                    logger.debug("No Validation is done. since no subscription ncs defined for app");
                }

                return true;
            }
        };
    }

    private void createApprovedProductionView() {
        acStartDateFld = getFieldWithPermission(ACTIVE_START_DATE, new TextField());
        if (approvedActiveProductionK.equals(appStatus) && isFieldNotNull(acStartDateFld)) {
            acStartDateFld.setCaption(application.getMsg("sp.createApp.active.production.start.date"));
            acStartDateFld.setWidth(width);
            form.addField("appStartDate", acStartDateFld);
        }
    }

    private void createRemarksFld() {
        remarksTxtFld = getFieldWithPermission(REMARKS, new TextField());
        if (!initialK.equals(appStatus) && isFieldNotNull(remarksTxtFld)) {
            remarksTxtFld.setCaption(application.getMsg("sp.createApp.remarks"));
            remarksTxtFld.setWidth(width);
            remarksTxtFld.setHeight(height);
            form.addField("remarks", remarksTxtFld);
        }
    }

    private void createAppPublishLink() {
        if (application.hasAnyRole("ROLE_PROV_PUBLISH_APP") && activeProductionK.equals(appStatus)) {
            form.getLayout().addComponent(new Link(application.getMsg("sp.createApp.app.publish"),
                    new ExternalResource(appPublishUrl() + app.get(appIdK)), "_blank", -1, -1, Window.BORDER_DEFAULT));
        }
    }

    public static AppBasicDetailsLayout createCrBasicAppDetailsLayout(BaseApplication application,
                                                                      Map<String, Object> crContext,
                                                                      boolean validationRequired) {
        AppBasicDetailsLayout appBasicDetailsLayout = new AppBasicDetailsLayout(application,
                (Map<String, Object>) crContext.get(appK), (String) crContext.get(statusK));
        //TODO do required cr decoration
        return appBasicDetailsLayout;
    }

    public AppBasicDetailsLayout(BaseApplication application, Map<String, Object> app, String appStatus) {
        super(application, APP_PERMISSION_ROLE_PREFIX);

        this.application = application;
        this.app = app;
        this.appStatus = appStatus;

        init();
    }

    public void changeToViewMode() {
        form.setReadOnly(true);
    }

    @Override
    public void reset() {
        resetFields("", appNameTxtFld, descriptionTxtFld, allowHostAddressTxtFld, whiteListTxtFld, blackListTxtFld, hostingSpaceComponent);
    }

    @Override
    public void setPermissions() {
        setReadWritePermissionToComponent(appIdTxtFld, appNameTxtFld, descriptionTxtFld,
                allowHostAddressTxtFld, whiteListTxtFld, blackListTxtFld,
                revenueShareTxtFld, remarksTxtFld, productionRequestedDateFld,
                productionStartDateFld, expirableComponent, productionEndDateFld,
                acStartDateFld, hostingSpaceComponent);
    }

    @Override
    public void setData(Map<String, Object> data) {
        setValueToField(appIdTxtFld, data.get(appIdK));
        setValueToField(appNameTxtFld, data.get(nameK));
        setValueToField(descriptionTxtFld, data.get(descriptionK));
        setValueToField(allowHostAddressTxtFld, convertToString((List<String>) data.get(allowedHostsK), JOIN_SEPARATOR));
        setValueToField(whiteListTxtFld, convertToString((List<String>) data.get(whiteListK), JOIN_SEPARATOR));
        setValueToField(blackListTxtFld, convertToString((List<String>) data.get(blackListK), JOIN_SEPARATOR));
        setValueToField(revenueShareTxtFld, data.get(revenueShareK));
        setValueToField(remarksTxtFld, data.get(remarksK));
        setValueToField(productionRequestedDateFld, data.get(appRequestDateK));
        setValueToField(productionStartDateFld, data.get(activeProductionStartDateK));
        setValueToField(expirableComponent, data.get(expireK));
        setValueToField(productionEndDateFld, data.get(endDateK));
        setValueToField(acStartDateFld, data.get(activeProductionStartDateK));
        setValueToField(hostingSpaceComponent, data.get(hostingReqK));
    }

    @Override
    public void getData(Map<String, Object> data) {
        getValueFromField(appIdTxtFld, data, appIdK);
        getValueFromField(appNameTxtFld, data, nameK);
        getValueFromField(descriptionTxtFld, data, descriptionK);
        getValueFromField(allowHostAddressTxtFld, data, allowedHostsK,
                convertToList(String.valueOf(allowHostAddressTxtFld.getValue()), SEPARATOR));
        getValueFromField(whiteListTxtFld, data, whiteListK,
                convertToList(String.valueOf(whiteListTxtFld.getValue()), SEPARATOR));
        getValueFromField(blackListTxtFld, data, blackListK,
                convertToList(String.valueOf(blackListTxtFld.getValue()), SEPARATOR));
        getValueFromField(revenueShareTxtFld, data, revenueShareK);
        getValueFromField(remarksTxtFld, data, remarksK);
        getValueFromField(productionStartDateFld, data, activeProductionStartDateK);
        getValueFromField(expirableComponent, data, expireK);
        getValueFromField(productionEndDateFld, data, endDateK);
        getValueFromField(hostingSpaceComponent, data, hostingReqK);
    }

    @Override
    public void validate() {
        form.validate();
    }
}