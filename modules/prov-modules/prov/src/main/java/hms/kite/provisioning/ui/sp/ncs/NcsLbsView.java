package hms.kite.provisioning.ui.sp.ncs;

import com.vaadin.data.Validator;
import com.vaadin.ui.*;
import hms.kite.provisioning.Provisioning;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

public class NcsLbsView extends NcsConfigurationView {

    private static final Logger logger = LoggerFactory.getLogger(NcsLbsView.class);

    private TextField tpsTextField;
    private TextField tpdTextField;

    public NcsLbsView(Provisioning provisioning) {
        this.provisioning = provisioning;
        tpsTextField = new TextField(provisioning.getMsg("sp.ncs.lbs.location.requests.per.second"));
        tpsTextField.setRequired(true);
        tpsTextField.setImmediate(true);
        tpsTextField.setRequiredError(provisioning.getMsg("sp.ncs.lbs.location.requests.per.second.required.error.message"));
        tpsTextField.addValidator(createValidator("sp.createApp.zero.validation", "sp.ncs.lbs.zero.validation.error.message"));
        tpsTextField.addValidator(createValidator("ncsLbsTpsRegex", "sp.ncs.lbs.location.requests.per.second.limit.validation"));

        tpdTextField = new TextField(provisioning.getMsg("sp.ncs.lbs.location.requests.per.day"));
        tpdTextField.setRequired(true);
        tpdTextField.setImmediate(true);
        tpdTextField.setRequiredError(provisioning.getMsg("sp.ncs.lbs.location.requests.per.day.required.error.message"));
        tpdTextField.addValidator(createValidator("sp.createApp.zero.validation", "sp.ncs.lbs.zero.validation.error.message"));
        tpdTextField.addValidator(createValidator("ncsLbsTpdRegex", "sp.ncs.lbs.location.requests.per.day.limit.validation"));
    }

    private void checkTpsTpdValidation() {
        int tps = Integer.parseInt(tpsTextField.getValue().toString());
        int tpd = Integer.parseInt(tpdTextField.getValue().toString());
        if (tps >= tpd) {
            throw new Validator.InvalidValueException(provisioning.getMsg("sp.ncs.lbs.location.requests.per.day.mismatch.validation"));
        }
    }

    @Override
    public Component loadComponents(Panel panel) {
        VerticalLayout layout = new VerticalLayout();
        panel.removeAllComponents();
        panel.setCaption(provisioning.getMsg("sp.ncs.lbs.title"));

        panel.addComponent(layout);
        loadLbsComponent();
        reloadData();
        refreshInChangedMode();
        layout.addComponent(form);
        layout.setComponentAlignment(form, Alignment.MIDDLE_CENTER);
        layout.setSpacing(true);

        return layout;
    }

    private void reloadData() {
        tpdTextField.setValue(getSpStringValue(lbsTpdK));
        tpsTextField.setValue(getSpStringValue(lbsTpsK));
    }

    @Override
    protected void setSpSessionValues() {
        provisioning.getSp().put(lbsTpsK, tpsTextField.getValue());
        provisioning.getSp().put(lbsTpdK, tpdTextField.getValue());
    }

    @Override
    public void saveData() {
    }

    @Override
    protected void changeToEditMode() {
        tpsTextField.setReadOnly(false);
        tpdTextField.setReadOnly(false);
    }

    @Override
    protected void changeToConfirmMode() {
        tpsTextField.setReadOnly(true);
        tpdTextField.setReadOnly(true);
    }

    @Override
    protected void changeToViewMode() {
        changeToConfirmMode();
    }

    @Override
    protected void changeToCancelView() {
        changeToViewMode();
    }

    @Override
    public boolean validateData() {
        try {
            setSpSessionValues();
            tpsTextField.validate();
            tpdTextField.validate();
            checkTpsTpdValidation();
            form.setComponentError(null);
        } catch (Validator.InvalidValueException e) {
            form.setComponentError(e);
            return false;
        } catch (Exception e) {
            logger.error("Unexpected error occured while validating the lbs form", e);
            return false;
        }
        return true;
    }

    public void loadLbsComponent() {
        form.addField("lbsTpsTxtFld", tpsTextField);
        form.addField("lbsTpdTxtFld", tpdTextField);
    }
}