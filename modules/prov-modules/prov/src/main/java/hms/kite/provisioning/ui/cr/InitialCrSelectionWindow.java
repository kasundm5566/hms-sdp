/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.provisioning.ui.cr;

import com.vaadin.data.Validator;
import com.vaadin.ui.*;
import hms.kite.datarepo.RepositoryServiceRegistry;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.util.ProvKeyBox;
import hms.kite.util.KiteKeyBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.provisioning.ProvisioningServiceRegistry.appRepositoryService;
import static hms.kite.provisioning.ProvisioningServiceRegistry.spRepositoryService;
import static hms.kite.util.KiteKeyBox.appK;
import static hms.kite.util.KiteKeyBox.spIdK;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class InitialCrSelectionWindow extends VerticalLayout {

    private static Logger logger = LoggerFactory.getLogger(InitialCrSelectionWindow.class);

    private BaseApplication application;
    private Panel mainPanel;
    private Panel childPanel;
    private Form form;
    private Select applicationSelect;
    private Select crTypeSelect;
    private DateField dateField;
    private Map<String, Object> crContext;

    public InitialCrSelectionWindow(BaseApplication application, String spId) {
        this.application = application;
        this.mainPanel = new Panel(application.getMsg("cr.create.cr.main.panel.caption"));
        crContext = new HashMap<String, Object>();
        crContext.put(spIdK, spId);
        addComponent(mainPanel);
        init();
        Map<String, Object> sp = spRepositoryService().findSpBySpId(spId);
        crContext.put(ProvKeyBox.spK, sp);
    }

    public InitialCrSelectionWindow(BaseApplication application, Panel mainPanel, Map<String, Object> cr) {
        this.application = application;
        this.mainPanel = mainPanel;
        this.crContext = cr;
        init();
        updateFormFromCr();
    }

    private void init() {
        childPanel = new Panel(application.getMsg("cr.create.cr.type.select.child.panel.caption"));
        VerticalLayout layout = new VerticalLayout();
        mainPanel.addComponent(childPanel);
        childPanel.addComponent(layout);
        childPanel.addStyleName("child-panel");
        form = new Form();
        form.setWidth("516px");
        form.addStyleName("common-form");
        layout.addComponent(form);
        layout.setComponentAlignment(form, Alignment.MIDDLE_CENTER);
        applicationSelect = new Select(application.getMsg("cr.create.cr.application"));
        populateAplicationSelect();
        crTypeSelect = new Select(application.getMsg("cr.create.cr.cr.type"));
        populateCrTypes();
        dateField = new DateField(application.getMsg("cr.create.cr.cr.apply.time"));
        form.addField("application", applicationSelect);
        form.addField("crType", crTypeSelect);
        form.addField("applyTime", dateField);
        form.getFooter().addComponent(createButtonBar());
    }

    private void updateFormFromCr() {
        applicationSelect.setValue(crContext.get(KiteKeyBox.appIdK));
        crTypeSelect.setValue(crContext.get(KiteKeyBox.crTypeK));
        dateField.setValue(crContext.get(KiteKeyBox.startDateK));
    }

    private void populateCrTypes() {
        crTypeSelect.addItem(CrType.APPLICATION);
        crTypeSelect.addItem(CrType.ADD_REMOVE_NCS);
        crTypeSelect.addItem(CrType.EDIT_EXISTING_NCS);
        crTypeSelect.setItemCaption(CrType.APPLICATION, application.getMsg("cr.create.cr.cr.type.application"));
        crTypeSelect.setItemCaption(CrType.ADD_REMOVE_NCS, application.getMsg("cr.create.cr.cr.type.add.remove.ncs"));
        crTypeSelect.setItemCaption(CrType.EDIT_EXISTING_NCS, application.getMsg("cr.create.cr.cr.type.edit.existing.ncs"));
    }

    private void populateAplicationSelect() {
        Map<String, Object> queryCriteria = new HashMap<String, Object>();
        queryCriteria.put(KiteKeyBox.spIdK, crContext.get(spIdK));
        List<Map<String, Object>> appList = RepositoryServiceRegistry.appRepositoryService().findAppsRange(queryCriteria, 0, 100);
        for (Map<String, Object> application : appList) {
            applicationSelect.addItem(application.get(KiteKeyBox.idK));
            applicationSelect.setItemCaption(application.get(KiteKeyBox.idK), (String) application.get(KiteKeyBox.nameK));
        }
    }

    private Component createButtonBar() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setWidth("516px");
        Button nextButton = new Button(application.getMsg("cr.create.cr.next"));
        nextButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    goNext();
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while trying to goto next window");
                }
            }
        });

        horizontalLayout.addComponent(nextButton);
        horizontalLayout.setComponentAlignment(nextButton, Alignment.MIDDLE_CENTER);
        return horizontalLayout;
    }

    private void goNext() {
        try {
            form.validate();
            openNextCrWindow((String) applicationSelect.getValue(), (CrType) crTypeSelect.getValue(), (Date) dateField.getValue());
        } catch (Validator.InvalidValueException e) {
            form.setComponentError(e);
        }
    }

    private void openNextCrWindow(String appId, CrType crType, Date date) {
        logger.debug("User selected {} CR for application {} to be applied on {}", new Object[]{crType, appId, date});
        crContext.put(KiteKeyBox.appIdK, appId);
        crContext.put(KiteKeyBox.crTypeK, crType);
        crContext.put(KiteKeyBox.startDateK, date);

        Map<String, Object> originalApp = appRepositoryService().findByAppId(appId);
        crContext.put(appK, originalApp);
        switch (crType) {
            case APPLICATION:
                gotoApplicationView();
                break;
            case ADD_REMOVE_NCS:
                gotoAddRemoveNcsView();
                break;
            case EDIT_EXISTING_NCS:
                gotoCrEditNcsView();
                break;
            default:
                logger.error("Unknown CR Type {} can't handle", crType);
        }
    }

    private void gotoApplicationView() {
        mainPanel.removeAllComponents();
        CrEditApplicationView crEditApplicationView = new CrEditApplicationView(application, mainPanel, crContext);
    }

    private void gotoAddRemoveNcsView() {
        mainPanel.removeAllComponents();
        CrAddNewNcsView crEditNcsView = new CrAddNewNcsView(application, mainPanel, crContext);
    }


    private void gotoCrEditNcsView() {
        mainPanel.removeAllComponents();
        CrEditNcsView crEditNcsView = new CrEditNcsView(application, mainPanel, crContext);
    }

}
