/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.email;

import hms.kite.provisioning.ProvisioningServiceRegistry;
import hms.kite.provisioning.commons.event.EventListener;
import hms.kite.util.SdpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.provisioning.ProvisioningServiceRegistry.sendEmailEventRepositoryService;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class EmailSendService implements EventListener {

    @Resource
    private EmailClient emailClient;

    private List<String> supportedEventList;

    private static final Logger logger = LoggerFactory.getLogger(EmailSendService.class);

    public EmailSendService() {
    }

    @Override
    public void handle(Map<String, Object> data) {
        String eventName = (String) data.get(eventNameK);
        try {
            if(sendEmailEventRepositoryService().isEventNameExist(eventName)) {
                Map<String, Object> email = sendEmailEventRepositoryService().findEventByName(eventName);
                logger.info("EventName [{}] is found...", eventName);
                ProvisioningServiceRegistry.emailRender(eventName).render(email, data);
                emailClient.sendEmail(email);
            } else {
                logger.warn("EventName [{}] is not found in the SP Repository...", eventName);
            }
        } catch (Exception ex) {
            logger.error("An Exception is occurred while sending the email. Data ." + data , ex);
        }
    }

    @Override
    public List<String> supportedEvents() {
        return supportedEventList;
    }

    public void setEmailClient(EmailClient emailClient) {
        this.emailClient = emailClient;
    }

    public void setSupportedEventList(List<String> supportedEventList) {
        this.supportedEventList = supportedEventList;
    }

}