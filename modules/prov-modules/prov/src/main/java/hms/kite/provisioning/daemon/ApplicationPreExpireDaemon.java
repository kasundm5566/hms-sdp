/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.provisioning.daemon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.appRepositoryService;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ApplicationPreExpireDaemon {
    
    private static Logger logger = LoggerFactory.getLogger(ApplicationPreExpireDaemon.class);

    public void init(){

    }

    public void execute(){
        try {
            Map<String, Object> query = createPreExpireApplicationQuery();
            List<Map<String, Object>> applications = appRepositoryService().findAppsRange(query, 0, 100);
            for (Map<String, Object> application : applications) {
                proccessPreExpireApplication(application);
            }
        } catch (Exception e) {
            logger.error("Unexpected error occurred in applicationPreExpireDaemon executing", e);
        }
    }

    private void proccessPreExpireApplication(Map<String, Object> application) {
        //To change body of created methods use File | Settings | File Templates.
    }

    private Map<String, Object> createPreExpireApplicationQuery() {
        return null;  //To change body of created methods use File | Settings | File Templates.
    }


}
