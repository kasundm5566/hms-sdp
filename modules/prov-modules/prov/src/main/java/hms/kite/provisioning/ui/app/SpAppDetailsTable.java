/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.app;

import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.component.table.DataManagementPanel;
import hms.kite.provisioning.ui.common.ConfirmationDialog;
import hms.kite.provisioning.ui.common.ConfirmationListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.appRepositoryService;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.provisioning.ui.app.AppDetailsTable.ApplicationAction.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class SpAppDetailsTable extends AppDetailsTable {

    private static final Logger logger = LoggerFactory.getLogger(SpAppDetailsTable.class);

    private HorizontalLayout createActionButtons(Map<String, Object> app) {
        HorizontalLayout buttonLayout = new HorizontalLayout();

        buttonLayout.addComponent(createLink(VIEW, application.getMsg("sp.viewApp.viewTable.viewLinkButton"), app));

        if (pendingApproveK.equals(app.get(statusK))) {
            addActionButton(MOVE_TO_DRAFT, application.getMsg("sp.viewApp.viewTable.moveToDraftLinkButton"), app, buttonLayout);
        } else if (draftK.equals(app.get(statusK))) {
            addActionButton(EDIT, application.getMsg("sp.viewApp.viewTable.editLinkButton"), app, buttonLayout);
            addActionButton(DELETE, application.getMsg("sp.viewApp.viewTable.deleteLinkButton"), app, buttonLayout);
        } else if (limitedProductionK.equals(app.get(statusK))) {
            addActionButton(REQUEST_ACTIVE_PRODUCTION, application.getMsg("sp.viewApp.viewNcsTable.requestForActiveProductionButton"), app, buttonLayout);
        } else if (rejectK.equals(app.get(statusK))) {
            addActionButton(MOVE_TO_DRAFT, application.getMsg("sp.viewApp.viewTable.moveToDraftLinkButton"), app, buttonLayout);
        }

        return buttonLayout;
    }

    private void requestActiveProduction(Map<String, Object> app) {
        viewApp(app);
    }

    private void moveToDraft(final Map<String, Object> app) {
        ConfirmationDialog confirmationDialog = new ConfirmationDialog(application,
                application.getMsg("admin.manageApp.moveToDraftConfirmation.title"),
                application.getMsg("admin.manageApp.moveToDraftConfirmation.message", (String) app.get(nameK)),
                new ConfirmationListener() {

                    @Override
                    public void onYes(String note) {
                        try {
                            app.put(statusK, draftK);
                            appRepositoryService().update(app);
                            reloadAppList();
                            logger.info("Application [{}] moved to draft", app.get(appIdK));
                        } catch (Exception e) {
                            logger.error("Unexpected error occurred while moving to draft an application ", e);
                        }
                    }
                });
        confirmationDialog.setNoteRequired(false);
        confirmationDialog.showDialog();
    }

    private void deleteApp(final Map<String, Object> app) {
        ConfirmationDialog confirmationDialog = new ConfirmationDialog(application,
                application.getMsg("sp.deleteConfirmation.title"),
                application.getMsg("sp.deleteConfirmation.message", (String) app.get(nameK)),
                new ConfirmationListener() {

                    @Override
                    public void onYes(String note) {
                        try {
                            sendEventToNcsPortlet(app, provSaveNcsEvent, deleteK);
                            appRepositoryService().delete((String) app.get(appIdK));
                            reloadAppList();
                            logger.info("Application [{}] deleted", app.get(appIdK));
                        } catch (Exception e) {
                            logger.error("Unexpected error occurred while deleting an application ", e);
                        }
                    }
                });
        confirmationDialog.setNoteRequired(false);
        confirmationDialog.showDialog();
    }

    private void editApp(Map<String, Object> app) {
        AppPanel appPanel = AppPanel.newEditAppPanel(app, application);
        application.getUiManager().pushScreen(AppPanel.class.getName(), appPanel);
    }

    private void viewApp(Map<String, Object> app) {
        AppPanel appPanel = AppPanel.newViewAppPanel(app, application);
        application.getUiManager().pushScreen(AppPanel.class.getName(), appPanel);
    }

    @Override
    protected void initialize() {
        addContainerProperty(application.getMsg("sp.viewApp.viewTableAppId"), Label.class, null);
        setColumnWidth(application.getMsg("sp.viewApp.viewTableAppId"), 120);
        addContainerProperty(application.getMsg("sp.viewApp.viewTableAppName"), Label.class, null);
        setColumnWidth(application.getMsg("sp.viewApp.viewTableAppName"), 130);
        addContainerProperty(application.getMsg("sp.viewApp.viewTableAppType"), Label.class, null);
        setColumnWidth(application.getMsg("sp.viewApp.viewTableAppType"), 70);
        addContainerProperty(application.getMsg("sp.viewApp.viewTableAppStatus"), Label.class, null);
        setColumnWidth(application.getMsg("sp.viewApp.viewTableAppStatus"), 100);
        addContainerProperty(application.getMsg("sp.viewApp.viewTableLastStatusChangedDate"), Label.class, null);
        setColumnWidth(application.getMsg("sp.viewApp.viewTableLastStatusChangedDate"), 140);
        addContainerProperty(application.getMsg("sp.viewApp.viewTableAction"), HorizontalLayout.class, null);
    }

    @Override
    protected void processAction(Map<String, Object> actionData) {
        ApplicationAction action = (ApplicationAction) actionData.get(actionK);
        Map<String, Object> app = (Map<String, Object>) actionData.get(dataK);

        switch (action) {
            case VIEW:
                viewApp(app);
                break;
            case EDIT:
                editApp(app);
                break;
            case DELETE:
                deleteApp(app);
                break;
            case MOVE_TO_DRAFT:
                moveToDraft(app);
                break;
            case REQUEST_ACTIVE_PRODUCTION:
                requestActiveProduction(app);
                break;
            default:
                logger.error("Unknown ApplicationAction {}. Can't handle", action);
        }
    }

    public SpAppDetailsTable(BaseApplication application, DataManagementPanel dataManagementPanel) {
        super(application, dataManagementPanel);
    }

    public void reloadTable(List<Map<String, Object>> records) {
        super.reloadTable(records);

        SimpleDateFormat sdf = new SimpleDateFormat("yy/MM/dd - HH:mm", application.getLocale());

        int rowNumber = 1;

        for (Map<String, Object> record : records) {
            List rowData = new ArrayList();
            rowData.add(new Label((String) record.get(appIdK)));
            rowData.add(new Label((String) record.get(nameK)));
            rowData.add(new Label(getApplicationType(record)));
            rowData.add(new Label((String) record.get(statusK)));
            rowData.add(new Label(sdf.format(record.get(updatedDateK))));
            rowData.add(createActionButtons(record));
            addItem(rowData.toArray(), rowNumber++);
        }
    }
}
