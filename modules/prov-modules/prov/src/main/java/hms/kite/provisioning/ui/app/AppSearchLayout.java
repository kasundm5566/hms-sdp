/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.app;

import com.vaadin.terminal.UserError;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.component.table.AbstractStandardSearchLayout;
import hms.kite.provisioning.commons.ui.component.table.DataManagementPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.datarepo.mongodb.MongoUtil.createLikeQuery;
import static hms.kite.provisioning.ProvisioningServiceRegistry.validationRegistry;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class AppSearchLayout extends AbstractStandardSearchLayout {

    private static final Logger logger = LoggerFactory.getLogger(AppSearchLayout.class);

    private TextField appNameTxtFld;
    private TextField appIdTxtFld;
    private ComboBox typeCmbBox;
    private ComboBox statusCmbBox;

    private boolean searchByStatus = true;
    private boolean searchByType = true;

    private void addAppIdTxtFld(HorizontalLayout horizontalLayout) {
        appIdTxtFld = new TextField(application.getMsg("manage.app.search.app.id"));
        appIdTxtFld.setWidth("90px");
        addToHorizontalLayout(true, horizontalLayout, appIdTxtFld);
    }

    private void addAppNameTxtFld(HorizontalLayout horizontalLayout) {
        appNameTxtFld = new TextField(application.getMsg("manage.app.search.app.name"));
        appNameTxtFld.setWidth("90px");
        addToHorizontalLayout(true, horizontalLayout, appNameTxtFld);
    }

    private void addTypeCmbBox(HorizontalLayout horizontalLayout) {
        typeCmbBox = new ComboBox("Type");
        addType("sdp");
        addType("soltura");
        typeCmbBox.setWidth("90px");
        addToHorizontalLayout(searchByType, horizontalLayout, typeCmbBox);
    }

    private void addType(String itemId) {
        typeCmbBox.addItem(itemId);
        typeCmbBox.setItemCaption(itemId, application.getMsg("prov.manage.app.table.app.category." + itemId));
    }

    private void addStatusCmbBox(HorizontalLayout horizontalLayout) {
        statusCmbBox = new ComboBox(application.getMsg("manage.app.search.app.status"));
        statusCmbBox.addItem(scheduledActiveProductionK);
        statusCmbBox.addItem(limitedProductionK);
        statusCmbBox.addItem(pendingApproveForActiveProductionK);
        statusCmbBox.addItem(activeProductionK);
        statusCmbBox.addItem(pendingApproveK);
        statusCmbBox.addItem(suspendK);
        statusCmbBox.addItem(rejectK);
        statusCmbBox.addItem(draftK);
        statusCmbBox.setWidth("135px");
        addToHorizontalLayout(searchByStatus, horizontalLayout, statusCmbBox);
    }

    @Override
    protected void addSearchFields(HorizontalLayout horizontalLayout) {
        addAppIdTxtFld(horizontalLayout);
        addAppNameTxtFld(horizontalLayout);
        addTypeCmbBox(horizontalLayout);
        addStatusCmbBox(horizontalLayout);
    }

    @Override
    public Map<String, Object> getQueryCriteria() {
        Map<String, Object> queryCriteria = new HashMap<String, Object>();

        String appId = (String) appIdTxtFld.getValue();
        String appName = (String) appNameTxtFld.getValue();
        String appType = (String) typeCmbBox.getValue();
        String status = (String) statusCmbBox.getValue();

        appNameTxtFld.setComponentError(null);
        if (!(appNameTxtFld.getValue().toString().matches(validationRegistry().regex("app.search.name.validation")))) {
            String errorMessage = application.getMsg("app.search.name.validation.errorMessage");
            appNameTxtFld.setComponentError(new UserError(errorMessage));
            queryCriteria.put(nameK, createLikeQuery(appName));
            setStatusMessage(application.getMsg("app.search.name.validation.errorMessage"), "sp-request-fail-notification");
        } else {
            appNameTxtFld.setComponentError(null);
            setStatusMessage(application.getMsg("manage.app.search.no.search.result"), "sp-request-fail-notification");
            queryCriteria.put(nameK, createLikeQuery(appName));
        }

        if (appId != null && !appId.isEmpty()) {
            queryCriteria.put(appIdK, createLikeQuery(appId));
        }
        if (searchByStatus && status != null && !status.isEmpty()) {
            queryCriteria.put(statusK, status);
        }
        if (searchByType && appType != null && !appType.isEmpty()) {
            queryCriteria.put(categoryK, appType);
        }

        return queryCriteria;
    }

    public AppSearchLayout(BaseApplication application, DataManagementPanel dataManagementPanel) {
        super(application, dataManagementPanel);
    }

    public void setSearchByStatus(boolean searchByStatus) {
        this.searchByStatus = searchByStatus;
    }

    public void setSearchByType(boolean searchByType) {
        this.searchByType = searchByType;
    }
}
