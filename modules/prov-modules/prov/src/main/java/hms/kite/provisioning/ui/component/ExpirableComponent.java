package hms.kite.provisioning.ui.component;

import com.vaadin.data.Property;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.addon.customfield.CustomField;

/**
 * User: azeem
 * Date: 4/18/12
 * Time: 12:02 PM
 */
public class ExpirableComponent extends CustomField {

    private static final Logger logger = LoggerFactory.getLogger(ExpirableComponent.class);

    private HorizontalLayout horizontalLayout;
    private CheckBox expirableChkBox;
    private DateField dateField;

    private void init() {
        expirableChkBox = new CheckBox();
        expirableChkBox.setImmediate(true);
        expirableChkBox.addListener(new Property.ValueChangeListener() {

            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                validate();
                if (dateField != null) {
                    Boolean isExpirable = (Boolean) event.getProperty().getValue();
                    dateField.setVisible(isExpirable);
                    dateField.setRequired(isExpirable);

                    // Clear values in DateField when ExpirableChkBox not is selected.
                    if(!isExpirable) {
                       dateField.setValue(null);
                    }
                }
            }
        });

        horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(expirableChkBox);
        horizontalLayout.setImmediate(true);
        setCompositionRoot(horizontalLayout);
    }

    public ExpirableComponent() {
        init();
    }

    public void setDateField(DateField dateField) {
        this.dateField = dateField;
    }

    @Override
    public Class<?> getType() {
        return HorizontalLayout.class;
    }

    @Override
    public void setValue(Object value) {
        expirableChkBox.setValue(value);
    }

    @Override
    public Object getValue() {
        return expirableChkBox.getValue();
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        horizontalLayout.setReadOnly(readOnly);
        expirableChkBox.setReadOnly(readOnly);
        if (dateField != null) {
            dateField.setReadOnly(readOnly);
        }
    }
}