/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.sp.sp;

import com.vaadin.ui.*;
import hms.kite.provisioning.Provisioning;
import hms.kite.provisioning.util.SpHomePage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.provisioning.ui.common.ViewMode.EDIT;
import static hms.kite.util.KiteKeyBox.spSelectedServicesK;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class SpNcsTypeSelectionWindow extends AbstractSpNcsTypeSelectionWindow {

    private static final Logger logger = LoggerFactory.getLogger(SpNcsTypeSelectionWindow.class);

    private void goBack() {
        logger.debug("Going back from sp editing page ");

        SpHomePage spHomePage = new SpHomePage(provisioning);
        spHomePage.showSpInitialHomeScreen();

        provisioning.getUiManager().clearAll();
        provisioning.getUiManager().pushScreen(SpHomePage.class.getName(), spHomePage);
    }

    private void goNext(AbstractComponentContainer container) {
        setSessionValues();

        Map<String, Object> sp = provisioning.getSp();

        NcsTypeConfigurationLayout ncsConfigurationWindow = new NcsTypeConfigurationLayout(provisioning, EDIT,
                provisioning.getMsg("sp.register.sp.window.title"));
        ncsConfigurationWindow.reloadSpNcs(sp);
        ncsConfigurationWindow.loadComponents();

        container.removeAllComponents();
        container.addComponent(ncsConfigurationWindow);

        sp.put(spSelectedServicesK, spSelectedServices);
    }

    private Button createBackButton() {
        Button backButton = new Button(provisioning.getMsg("sp.register.sp.back"));
        backButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    goBack();
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while going back from sp registering page", e);
                }
            }
        });
        return backButton;
    }

    private Button createNextButton(final AbstractComponentContainer container, final Label errorLbl) {
        Button nextButton = new Button(provisioning.getMsg("sp.register.sp.next"));
        nextButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    boolean isValid = validate();
                    if (!isValid) {
                        return;
                    }
                    errorLbl.setVisible(!isValid);
                    goNext(container);
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while going into next phase of sp register", e);
                }
            }
        });
        return nextButton;
    }

    @Override
    protected Component createButtonBar(final AbstractComponentContainer container) {
        final VerticalLayout verticalLayout = new VerticalLayout();

        final Label errorLbl = new Label(provisioning.getMsg("sp.register.sp.NcsSelectionError"));
        errorLbl.setStyleName("sp-request-fail-notification");
        errorLbl.setVisible(false);
        verticalLayout.addComponent(errorLbl);

        HorizontalLayout buttonBar = new HorizontalLayout();
        buttonBar.setSpacing(true);
        verticalLayout.addComponent(buttonBar);

        buttonBar.addComponent(createBackButton());

        buttonBar.addComponent(createNextButton(container, errorLbl));

        verticalLayout.setSpacing(true);
        verticalLayout.setComponentAlignment(buttonBar, Alignment.MIDDLE_CENTER);

        return verticalLayout;
    }

    public SpNcsTypeSelectionWindow(Provisioning provisioning) {
        super(provisioning);
    }
}
