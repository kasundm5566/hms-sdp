/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.common;

import com.vaadin.ui.Button;
import com.vaadin.ui.themes.BaseTheme;
import hms.kite.provisioning.commons.ui.BaseApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ComponentFactory {

    private static final Logger logger = LoggerFactory.getLogger(ComponentFactory.class);

    public static Button createSpDetailsLink(final BaseApplication application, String spName, final String corporateUserId) {
        Button button = new Button(spName);
        button.setStyleName(BaseTheme.BUTTON_LINK);
        button.setImmediate(true);
        button.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    new SpInformationView(application, corporateUserId);
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while trying to show sp information", e);
                }
            }
        });

        return button;
    }
}
