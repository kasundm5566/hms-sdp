/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.repo.mongodb;

import static hms.kite.util.KiteErrorBox.emailTemplateNotFoundErrorCode;
import static hms.kite.util.KiteKeyBox.eventNameK;
import static hms.kite.util.KiteKeyBox.idK;
import hms.kite.provisioning.repo.EmailTemplateRepository;
import hms.kite.util.SdpException;

import java.util.HashMap;
import java.util.Map;

import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.QueryBuilder;

/**
 * $LastChangedDate $
 * $LastChangedBy $
 * $LastChangedRevision $
 */
public class EmailTemplateMongoRepository implements EmailTemplateRepository {

    public static final String EMAIL_TEMPLATE_COLLECTION_NAME = "email_templates";

    private MongoTemplate mongoTemplate;

    private Map<String, Object> findOneEvent (Map<String, String> queryCriteria) {
        BasicDBObject query = new BasicDBObject();
        query.putAll(queryCriteria);
        DBObject result =  mongoTemplate.getCollection(EMAIL_TEMPLATE_COLLECTION_NAME).findOne(query);
        if (result == null) {
            throw new SdpException(emailTemplateNotFoundErrorCode, "Send email event not found");
        }
        return new HashMap<String, Object>(result.toMap());
    }

    @Override
    public boolean isEventNameExist(String eventName) {
        DBObject query = new QueryBuilder().put(eventNameK).is(eventName).get();
        DBObject result = mongoTemplate.getCollection(EMAIL_TEMPLATE_COLLECTION_NAME).findOne(query);
        return result != null;
    }

    @Override
    public Map<String, Object> findEventByName(String eventName) {
        HashMap<String, String> queryCriteria = new HashMap<String, String>();
        queryCriteria.put(eventNameK, eventName);
        return findOneEvent(queryCriteria);
    }

    @Override
    public void create(Map<String, Object> event) {

        if (event.get(eventNameK) == null) {
            throw new IllegalStateException("Send email event name is required");
        }
        if (isEventNameExist((String) event.get(eventNameK))) {
            throw new SdpException(EMAIL_TEMPLATE_COLLECTION_NAME, "Send email event already exist");
        } else {
            event.put(idK, event.get(eventNameK));
            mongoTemplate.getCollection(EMAIL_TEMPLATE_COLLECTION_NAME).save(new BasicDBObject(event));
        }
    }

    @Override
    public void deleteEvent(String eventName) {
        mongoTemplate.getCollection(EMAIL_TEMPLATE_COLLECTION_NAME).remove(new QueryBuilder().put("event-name").is(eventName).get());
    }

    @Override
    public void deleteAll() {
        mongoTemplate.getCollection(EMAIL_TEMPLATE_COLLECTION_NAME).drop();
    }

    @Override
    public void updateEvent(Map<String, Object> newEvent) {
        mongoTemplate.getCollection(EMAIL_TEMPLATE_COLLECTION_NAME).save(new BasicDBObject(newEvent));
    }

    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

}
