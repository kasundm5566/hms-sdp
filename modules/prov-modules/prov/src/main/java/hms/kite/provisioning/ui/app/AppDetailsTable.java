/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.app;

import hms.kite.provisioning.commons.ui.Action;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.component.table.DataManagementPanel;
import hms.kite.provisioning.commons.ui.component.table.DataTable;
import hms.kite.provisioning.commons.ui.event.PortletEventSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.provisioning.commons.util.ProvKeyBox.coopUserIdK;
import static hms.kite.provisioning.commons.util.ProvKeyBox.ncsesK;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public abstract class AppDetailsTable extends DataTable {

    private static final Logger logger = LoggerFactory.getLogger(AppDetailsTable.class);

    private static final String ROLE_PROV_APP_ACTION = "ROLE_PROV_APP_ACTION_";

    protected static enum ApplicationAction implements Action {
        VIEW, EDIT, DELETE, MOVE_TO_DRAFT, SUSPEND, APPROVE, REJECT, RESTORE, TERMINATE,
        REQUEST_ACTIVE_PRODUCTION, REQUEST_LIMITED_PRODUCTION
    }

    protected String getApplicationType(Map<String, Object> app) {
        String appCategory = (String) app.get(categoryK);
        if (appCategory == null) {
            appCategory = "sdp";
        }
        return application.getMsg("prov.manage.app.table.app.category." + appCategory);
    }

    protected void sendEventToNcsPortlet(Map<String, Object> app, String eventName, String status) {
        Map<String, Object> eventData = new HashMap<String, Object>();
        eventData.put(appIdK, app.get(appIdK));
        eventData.put(statusK, status);
        eventData.put(spIdK, app.get(spIdK));
        eventData.put(coopUserIdK, spRepositoryService().findSpBySpId((String) app.get(spIdK)).get(coopUserIdK));
        eventData.put(ncsesK, app.get(ncsesK));

        logger.debug("Sending [{}] with status [{}] to [{}] ", new Object[]{eventName, status, app.get(ncsesK)});

        PortletEventSender.send(eventData, eventName, application);
    }

    protected void reloadAppList() {
        dataManagementPanel.search();
    }

    public AppDetailsTable(BaseApplication application, DataManagementPanel dataManagementPanel) {
        super(application, dataManagementPanel, ROLE_PROV_APP_ACTION);
    }
}
