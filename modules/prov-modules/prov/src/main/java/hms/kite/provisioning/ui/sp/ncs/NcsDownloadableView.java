/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.sp.ncs;

import com.vaadin.data.Validator;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import hms.kite.provisioning.Provisioning;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.util.KiteKeyBox.dlAppMcdK;
import static hms.kite.util.KiteKeyBox.dlAppMdpdK;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class NcsDownloadableView extends NcsConfigurationView {

    private static final Logger logger = LoggerFactory.getLogger(NcsDownloadableView.class);

    private TextField mctTxtFld;
    private TextField tpdTxtFld;

    private void init(Provisioning provisioning) {
        mctTxtFld = new TextField(provisioning.getMsg("sp.ncs.dlApp.maxcondl"));
        mctTxtFld.setImmediate(true);
        mctTxtFld.setRequired(true);
        mctTxtFld.setRequiredError(provisioning.getMsg("sp.ncs.dlApp.maxcondl.required.error"));
        mctTxtFld.addValidator(createValidator("ncsDlAppMaxConDlRegex", "sp.ncs.dlApp.maxcondl.validation"));
        mctTxtFld.addValidator(createValidator("sp.createApp.zero.validation", "sp.ncs.dlApp.zero.validation.error.message"));


        tpdTxtFld = new TextField(provisioning.getMsg("sp.ncs.dlApp.maxdlperday"));
        tpdTxtFld.setImmediate(true);
        tpdTxtFld.setRequired(true);
        tpdTxtFld.setRequiredError(provisioning.getMsg("sp.ncs.dlApp.maxdlperday.required.error"));
        tpdTxtFld.addValidator(createValidator("ncsDlAppMaxDlPerDayRegex", "sp.ncs.dlApp.maxdlperday.validation"));
        tpdTxtFld.addValidator(createValidator("sp.createApp.zero.validation", "sp.ncs.dlApp.zero.validation.error.message"));
    }

    private void checkMaxConDlAndMaxDlPerDayValidation() {
        int mctValue = Integer.parseInt(mctTxtFld.getValue().toString());
        int tpdValue = Integer.parseInt(tpdTxtFld.getValue().toString());
        if (mctValue >= tpdValue) {
            throw new Validator.InvalidValueException(provisioning.getMsg("sp.ncs.dlApp.maxcondl.maxdlperday.validation.message"));
        }
    }

    private void reloadData() {
        Map<String, Object> sp = provisioning.getSp();

        setValueToField(mctTxtFld, sp.get(dlAppMcdK));
        setValueToField(tpdTxtFld, sp.get(dlAppMdpdK));
    }

    private void addToForm() {
        form.addField("mct", mctTxtFld);
        form.addField("tpd", tpdTxtFld);
    }

    @Override
    protected void changeToViewMode() {
        changeToConfirmMode();
    }

    @Override
    protected void changeToConfirmMode() {
        mctTxtFld.setReadOnly(true);
        tpdTxtFld.setReadOnly(true);
    }

    @Override
    protected void changeToEditMode() {
        mctTxtFld.setReadOnly(false);
        tpdTxtFld.setReadOnly(false);
    }

    @Override
    protected void changeToCancelView() {
        changeToViewMode();
    }

    @Override
    protected void setSpSessionValues() {
        Map<String, Object> sp = provisioning.getSp();

        sp.put(dlAppMcdK, mctTxtFld.getValue());
        sp.put(dlAppMdpdK, tpdTxtFld.getValue());
    }

    public VerticalLayout loadComponents(final Panel panel) {
        VerticalLayout layout = new VerticalLayout();

        panel.removeAllComponents();
        panel.setCaption(provisioning.getMsg("sp.ncs.dlApp.title"));
        panel.addComponent(layout);

        addToForm();
        reloadData();
        refreshInChangedMode();

        layout.addComponent(form);
        layout.setComponentAlignment(form, Alignment.MIDDLE_CENTER);
        layout.setSpacing(true);

        return layout;
    }

    @Override
    public void saveData() {
    }

    @Override
    public boolean validateData() {
        try {
            setSpSessionValues();
            mctTxtFld.validate();
            tpdTxtFld.validate();
            checkMaxConDlAndMaxDlPerDayValidation();
            form.setComponentError(null);
        } catch (Validator.InvalidValueException e) {
            form.setComponentError(e);
            return false;
        } catch (Exception e) {
            logger.error("Unexpected error occurred while validating downloadable form", e);
            return false;
        }
        return true;
    }

    public NcsDownloadableView(Provisioning provisioning) {
        this.provisioning = provisioning;

        init(provisioning);
    }
}