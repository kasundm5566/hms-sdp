/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.provisioning.ui.common;

import com.vaadin.terminal.ExternalResource;
import com.vaadin.ui.*;
import hms.kite.provisioning.commons.ui.BaseApplication;

import static hms.kite.provisioning.ProvisioningServiceRegistry.spInformationExternalUrl;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class SpInformationView{

    private BaseApplication application;
    private String corporateUserId;

    public SpInformationView(BaseApplication application, String corporateUserId) {
        this.application = application;
        this.corporateUserId = corporateUserId;
        loadComponents();
    }

    public void loadComponents() {
        final Window window = new Window();
        window.setModal(true);
        window.setWidth("1034px");
        window.setHeight("640px");
        VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();
        //application.getMsg("admin.spSearchAndView.spInformationView.title")
        Embedded embedded = new Embedded(
                "", new ExternalResource(spInformationExternalUrl() + corporateUserId));
        embedded.setType(Embedded.TYPE_BROWSER);
        embedded.setWidth("100%");
        embedded.setHeight("510px");
        layout.addComponent(embedded);

        HorizontalLayout buttonBar = new HorizontalLayout();
        buttonBar.setSizeFull();
        buttonBar.setSpacing(true);
        buttonBar.setHeight("40px");
        layout.addComponent(buttonBar);

        Button okButton = new Button(application.getMsg("admin.spSearchAndView.spInformationView.ok"));
        buttonBar.addComponent(okButton);
        buttonBar.setComponentAlignment(okButton, Alignment.MIDDLE_CENTER);
        okButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                application.getMainWindow().removeWindow(window);
            }
        });

        window.addComponent(layout);
        application.getMainWindow().addWindow(window);

    }


}
