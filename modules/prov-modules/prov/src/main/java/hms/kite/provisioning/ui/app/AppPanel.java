/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/

package hms.kite.provisioning.ui.app;

import com.vaadin.ui.*;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.I18nProvider;
import hms.kite.util.KiteKeyBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/*
* $LastChangedDate: 2011-04-22 12:01:20 +0530 (Fri, 22 Apr 2011) $
* $LastChangedBy: nilushikas $
* $LastChangedRevision: 72369 $
*/

public class AppPanel extends VerticalLayout {

    private Panel panel;

    private static final Logger logger = LoggerFactory.getLogger(AppPanel.class);

    public static AppPanel newViewAppPanel(Map<String, Object> app, BaseApplication application) {
        AppPanel appPanel = new AppPanel();
        appPanel.setCaption(application.getMsg("sp.viewApp.viewApplicationTitle"));
        AppForm appForm = AppForm.newViewAppForm(app, application);
        appPanel.setAppForm(appForm);
        applyStyles(appPanel);
        return appPanel;
    }

    public static AppPanel newEditAppPanel(Map<String, Object> app, BaseApplication application) {
        AppPanel appPanel = new AppPanel();
        appPanel.setCaption(application.getMsg("sp.createApp.createApplicationTitle"));
        AppForm appForm = AppForm.newEditAppForm(app, application, application);
        appPanel.setAppForm(appForm);
        applyStyles(appPanel);
        return appPanel;
    }

    public static AppPanel newCreateAppPanel(I18nProvider i18n, Set<String> userRoles, String spId, BaseApplication application) {
        Map<String, Object> app = new HashMap<String, Object>();
        app.put(KiteKeyBox.spIdK, spId);
        AppPanel appPanel = new AppPanel();
        appPanel.setCaption(i18n.getMsg("sp.createApp.createApplicationTitle"));
        appPanel.setAppForm(AppForm.newCreateAppForm(app, i18n, application));
        applyStyles(appPanel);
        return appPanel;
    }

    private AppPanel() {
        panel = new Panel();
        addComponent(panel);
    }

//    public void setCaption(String caption) {
//        panel.setCaption(caption);
//    }

    private void setAppForm(Component component) {
        VerticalLayout mainLayout = new VerticalLayout();
        mainLayout.addComponent(component);
        mainLayout.setComponentAlignment(component, Alignment.MIDDLE_CENTER);
        panel.addComponent(mainLayout);
    }

    private static void applyStyles(Layout content) {
        content.addStyleName("container-panel");
        content.setWidth("762px");
        content.setMargin(true);
    }

}