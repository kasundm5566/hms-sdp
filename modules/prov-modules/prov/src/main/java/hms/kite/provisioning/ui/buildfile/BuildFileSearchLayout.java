/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.buildfile;

import com.vaadin.ui.*;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.component.table.AbstractStandardSearchLayout;
import hms.kite.provisioning.commons.ui.component.table.DataManagementPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.deviceAndPlatformRepositoryService;
import static hms.kite.datarepo.mongodb.MongoUtil.createLikeQuery;
import static hms.kite.provisioning.commons.util.ProvKeyBox.rejectK;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class BuildFileSearchLayout extends AbstractStandardSearchLayout {

    private static final Logger logger = LoggerFactory.getLogger(BuildFileSearchLayout.class);

    private static final String NULL_ITEM_ID = "Please Select";

    private TextField buildNameTxtFld;
    private TextField appIdTxtFld;
    private Select statusSelect;
    private Select platformSelect;

    private void addBuildNameTxtFld(FormLayout formLayout) {
        buildNameTxtFld = new TextField(application.getMsg("downloadable.buildfile.list.table.buildname.caption"));
        buildNameTxtFld.setWidth("120px");
        formLayout.addComponent(buildNameTxtFld);
    }

    private void addAppIdTxtFld(FormLayout formLayout) {
        appIdTxtFld = new TextField("Application ID");
        appIdTxtFld.setWidth("120px");
        formLayout.addComponent(appIdTxtFld);
    }

    private void addStatusSelect(FormLayout formLayout) {
        statusSelect = new Select(application.getMsg("downloadable.buildfile.list.table.status.caption"));
        statusSelect.setWidth("120px");
        statusSelect.addItem(NULL_ITEM_ID);
        statusSelect.addItem(initialK);
        statusSelect.addItem(pendingApproveK);
        statusSelect.addItem(approvedK);
        statusSelect.addItem(suspendedK);
        statusSelect.addItem(rejectK);
        statusSelect.setNullSelectionItemId(NULL_ITEM_ID);
        formLayout.addComponent(statusSelect);
    }

    private void addPlatformSelect(FormLayout formLayout) {
        platformSelect = new Select("Platform");
        platformSelect.setWidth("120px");
        platformSelect.addItem(NULL_ITEM_ID);
        List<Map<String, Object>> allPlatforms = deviceAndPlatformRepositoryService().findAllPlatforms();
        for (Map<String, Object> platform : allPlatforms) {
            Object itemId = platform.get(platformIdK);
            String itemCaption = platform.get(platformNameK).toString();
            platformSelect.addItem(itemId);
            platformSelect.setItemCaption(itemId, itemCaption);
        }
        platformSelect.setNullSelectionItemId(NULL_ITEM_ID);
        formLayout.addComponent(platformSelect);
    }

    private void addToQueryCriteria(Map<String, Object> queryCriteria, Field field, String parameter) {
        Object value = field.getValue();
        if (value != null) {
            queryCriteria.put(parameter, value);
        }
    }

    private void addToQueryCriteria(Map<String, Object> queryCriteria, Object value, String parameter) {
        if (value != null) {
            queryCriteria.put(parameter, value);
        }
    }

    @Override
    protected void addSearchFields(HorizontalLayout horizontalLayout) {
        FormLayout searchFieldsCol1 = new FormLayout();
        searchFieldsCol1.setSpacing(true);
        FormLayout searchFieldsCol2 = new FormLayout();
        searchFieldsCol2.setSpacing(true);

        addBuildNameTxtFld(searchFieldsCol1);
        addAppIdTxtFld(searchFieldsCol1);
        addStatusSelect(searchFieldsCol2);
        addPlatformSelect(searchFieldsCol2);

        horizontalLayout.addComponent(searchFieldsCol1);
        horizontalLayout.addComponent(searchFieldsCol2);
    }

    @Override
    public Map<String, Object> getQueryCriteria() {
        Map<String, Object> queryCriteria = new HashMap<String, Object>();

        addToQueryCriteria(queryCriteria, createLikeQuery(buildNameTxtFld.getValue().toString()), buildNameK);
        addToQueryCriteria(queryCriteria, createLikeQuery(appIdTxtFld.getValue().toString()), appIdK);

        addToQueryCriteria(queryCriteria, statusSelect, statusK);
        addToQueryCriteria(queryCriteria, platformSelect, platformIdK);

        return queryCriteria;
    }

    public BuildFileSearchLayout(BaseApplication application, DataManagementPanel dataManagementPanel) {
        super(application, dataManagementPanel);
    }
}
