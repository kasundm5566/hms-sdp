package hms.kite.provisioning.ui.component;

import com.vaadin.data.Property;
import com.vaadin.ui.CheckBox;
import org.vaadin.addon.customfield.CustomField;

/**
 * <p>
 * Custom field for Hosting Space Required.
 * </p>
 *
 * @author Manuja
 * @version $Id$
 */
public class HostingSpaceComponent extends CustomField {

    private final CheckBox checkBox;

    /**
     * <p>
     *     Default constructor.
     * </p>
     */
    public HostingSpaceComponent() {
        checkBox = new CheckBox();
        setCompositionRoot(checkBox);
    }

    @Override
    public void setValue(Object newValue) throws ReadOnlyException, ConversionException {
        checkBox.setValue(newValue);
    }

    @Override
    public Object getValue() {
        return checkBox.getValue();
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        checkBox.setReadOnly(readOnly);
        super.setReadOnly(readOnly);
    }

    @Override
    public void setEnabled(boolean enabled) {
        checkBox.setEnabled(enabled);
        super.setEnabled(enabled);
    }

    @Override
    public Class<?> getType() {
        return CheckBox.class;
    }
}
