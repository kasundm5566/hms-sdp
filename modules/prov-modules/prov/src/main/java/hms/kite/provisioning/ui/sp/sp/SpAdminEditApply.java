/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.sp.sp;

import com.vaadin.ui.VerticalLayout;
import hms.kite.provisioning.Provisioning;
import hms.kite.provisioning.ui.sp.admin.AdminActionHandler;

import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class SpAdminEditApply extends VerticalLayout {

    public SpAdminEditApply(Provisioning provisioning, Map<String, Object> sp, AdminActionHandler adminActionHandler) {
        AdminNcsEditLayout editor = new AdminNcsEditLayout(provisioning, sp, adminActionHandler);

        editor.loadComponents(this);
    }
}
