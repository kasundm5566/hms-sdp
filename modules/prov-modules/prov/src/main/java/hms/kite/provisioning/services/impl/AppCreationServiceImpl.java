/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/

package hms.kite.provisioning.services.impl;

import hms.commons.IdGenerator;
import hms.kite.provisioning.commons.util.ProvIdGenerator;
import hms.kite.provisioning.repo.DbException;
import hms.kite.provisioning.services.AppCreationService;
import hms.kite.provisioning.ui.common.AppException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.appRepositoryService;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;


/*
* $LastChangedDate: 2011-04-28 15:42:12 +0530 (Thu, 28 Apr 2011) $
* $LastChangedBy: nilushikas $
* $LastChangedRevision: 72623 $
*/

public class AppCreationServiceImpl implements AppCreationService {

    private static final Logger logger = LoggerFactory.getLogger(AppCreationServiceImpl.class);

    public Map<String, Object> createApp(String spid) {
        Map<String, Object> app = new HashMap<String, Object>();
        app.put(idK, IdGenerator.generateId());
        app.put(appIdK, ProvIdGenerator.generateAppId());
        app.put(spIdK, spid);
        return app;
    }

//    @Override
//    public Boolean isAppNameExisting(String appName) {
//
//        return appRepositoryService().isAppNameExists(appName, (String) app.get(idK));
//
//    }

    public void submit(Map<String, Object> app) throws AppException {
        try {
            appRepositoryService().create(app);
        } catch (DbException e) {
            logger.error("DbException", e);
        }
    }

    public void update(Map<String, Object> app) {
        try {
            appRepositoryService().update(app);
        } catch (DbException e) {
            logger.error("DbException", e);
        }
    }


}
