/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.sp.ncs;

import com.vaadin.data.Validator;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import hms.kite.provisioning.Provisioning;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class NcsUssdView extends NcsConfigurationView {

    private static final Logger logger = LoggerFactory.getLogger(NcsUssdView.class);

    private TextField tpsTxtFld;
    private TextField tpdTxtFld;

    private void init(Provisioning provisioning) {
        tpsTxtFld = new TextField(provisioning.getMsg("sp.ncs.ussd.mo.tps"));
        tpsTxtFld.setImmediate(true);
        tpsTxtFld.setRequired(true);
        tpsTxtFld.setRequiredError(provisioning.getMsg("sp.ncs.ussd.mo.tps.required.error"));
        tpsTxtFld.addValidator(createValidator("ncsUssdMoTpsRegex", "sp.ncs.ussd.mo.tps.validation"));
        tpsTxtFld.addValidator(createValidator("sp.createApp.zero.validation", "sp.ncs.ussd.zero.validation.error.message"));

        tpdTxtFld = new TextField(provisioning.getMsg("sp.ncs.ussd.mo.tpd"));
        tpdTxtFld.setImmediate(true);
        tpdTxtFld.setRequired(true);
        tpdTxtFld.setRequiredError(provisioning.getMsg("sp.ncs.ussd.mo.tpd.required.error"));
        tpdTxtFld.addValidator(createValidator("ncsUssdMoTpdRegex", "sp.ncs.ussd.mo.tpd.validation"));
        tpdTxtFld.addValidator(createValidator("sp.createApp.zero.validation", "sp.ncs.ussd.zero.validation.error.message"));
    }

    private void checkTpsTpdValidation() {
        int moTpsValue = Integer.parseInt(tpsTxtFld.getValue().toString());
        int moTpdValue = Integer.parseInt(tpdTxtFld.getValue().toString());
        if (moTpsValue >= moTpdValue) {
            throw new Validator.InvalidValueException(provisioning.getMsg("sp.ncs.ussd.mt.tps.tpd.validation.message"));
        }
    }

    private void reloadData() {
        Map<String, Object> sp = provisioning.getSp();

        setValueToField(tpsTxtFld, sp.get(ussdMoTpsK));
        setValueToField(tpdTxtFld, sp.get(ussdMoTpdK));
    }

    private void addToForm() {
        form.addField("tps", tpsTxtFld);
        form.addField("tpd", tpdTxtFld);
    }

    @Override
    protected void changeToViewMode() {
        changeToConfirmMode();
    }

    @Override
    protected void changeToConfirmMode() {
        tpsTxtFld.setReadOnly(true);
        tpdTxtFld.setReadOnly(true);
    }

    @Override
    protected void changeToEditMode() {
        tpsTxtFld.setReadOnly(false);
        tpdTxtFld.setReadOnly(false);
    }

    @Override
    protected void changeToCancelView() {
        changeToViewMode();
    }

    @Override
    protected void setSpSessionValues() {
        Map<String, Object> sp = provisioning.getSp();

        sp.put(ussdMoTpdK, tpdTxtFld.getValue());
        sp.put(ussdMoTpsK, tpsTxtFld.getValue());
        sp.put(ussdMtTpdK, tpdTxtFld.getValue());
        sp.put(ussdMtTpsK, tpsTxtFld.getValue());
    }

    public NcsUssdView(Provisioning provisioning) {
        this.provisioning = provisioning;

        init(provisioning);
    }

    @Override
    public void saveData() {
    }

    @Override
    public boolean validateData() {
        try {
            setSpSessionValues();
            tpsTxtFld.validate();
            tpdTxtFld.validate();
            checkTpsTpdValidation();
            form.setComponentError(null);
        } catch (Validator.InvalidValueException e) {
            form.setComponentError(e);
            return false;
        } catch (Exception e) {
            logger.error("Unexpected error occurred while validating ussd form", e);
            return false;
        }
        return true;
    }

    @Override
    public VerticalLayout loadComponents(final Panel panel) {
        VerticalLayout mainLayout = new VerticalLayout();

        panel.removeAllComponents();
        panel.setCaption(provisioning.getMsg("sp.ncs.ussd.title"));
        panel.addComponent(mainLayout);

        addToForm();
        reloadData();
        refreshInChangedMode();

        mainLayout.addComponent(form);
        mainLayout.setComponentAlignment(form, Alignment.MIDDLE_CENTER);

        return mainLayout;
    }
}
