/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Window;
import hms.commons.IdGenerator;
import hms.kite.provisioning.commons.ui.event.PortletEventSender;
import hms.kite.provisioning.commons.util.ProvIdGenerator;
import hms.kite.provisioning.commons.util.ProvKeyBox;
import hms.kite.provisioning.ui.app.AppManagementPanel;
import hms.kite.provisioning.ui.app.AppPanel;
import hms.kite.provisioning.ui.buildfile.BuildFileManagementPanel;
import hms.kite.provisioning.ui.common.ViewMode;
import hms.kite.provisioning.ui.cr.InitialCrSelectionWindow;
import hms.kite.provisioning.ui.sp.SPApplication;
import hms.kite.provisioning.ui.sp.SpManagementPanel;
import hms.kite.provisioning.ui.sp.admin.SPApproveApplication;
import hms.kite.provisioning.ui.sp.admin.SpSearchViewMode;
import hms.kite.provisioning.ui.sp.ncs.AllNcsTypesLayout;
import hms.kite.provisioning.ui.sp.sp.NcsTypeConfigurationLayout;
import hms.kite.util.KiteKeyBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import java.util.*;

import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.provisioning.ProvisioningServiceRegistry.portletId;
import static hms.kite.provisioning.commons.ui.LiferayUtil.createPortletId;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.util.KiteKeyBox.draftK;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ProvisioningEventHandler implements PortletEventHandler {

    private static Logger logger = LoggerFactory.getLogger(ProvisioningEventHandler.class);

    private Provisioning provisioning;

    public ProvisioningEventHandler(Provisioning provisioning) {
        this.provisioning = provisioning;
    }

    private Map<String, Object> getSp() {
        return provisioning.getSp();
    }

    private String getMsg(String key, String... args) {
        return provisioning.getMsg(key, args);
    }

    private void handleReceivedEvent(EventRequest eventRequest, EventResponse eventResponse, Window window) throws SystemException, PortalException {
        String eventName = eventRequest.getEvent().getName();
        Map<String, Object> eventData = (Map<String, Object>) eventRequest.getEvent().getValue();
        ThemeDisplay themeDisplay = (ThemeDisplay) eventRequest.getAttribute(WebKeys.THEME_DISPLAY);
        Long userId = themeDisplay.getUserId();

        logger.debug("Provisioning portlet received event [{}] with event data [{}]", eventName, eventData);

        if (provViewNcsEvent.equals(eventName)) {
            handleViewNcsEvent(eventName, eventData, userId, themeDisplay);
        } else if (provCreateNcsEvent.equals(eventName)) {
            handleCreateNcsEvent(eventName, eventData, userId, themeDisplay);
        } else if (provReConfigureNcsEvent.equals(eventName)) {
            handleReconfigureNcsEvent(eventName, eventData, userId, themeDisplay);
        } else if (eventName.equals(provSaveNcsEvent)) {
            handleSaveNcsEvent(eventName, eventData, userId, themeDisplay);
        } else if (provViewBuildFileEventK.equals(eventName)) {
            handleViewBuildFileEvent(eventName, eventData, userId, themeDisplay);
        } else if (provCreateBuildFileEventK.equals(eventName)) {
            handleCreateBuildFileEvent(eventName, eventData, userId, themeDisplay);
        } else if (provReConfigureBuildFileEventK.equals(eventName)) {
            handleReconfigureBuildFileEvent(eventName, eventData, userId, themeDisplay);
        } else if (eventName.equals(provSaveBuildFileEventK)) {
            handleSaveBuildFileEvent(eventName, eventData, userId, themeDisplay);
        } else if (eventName.equals(ncsConfiguredK)) {
            handleNcsConfiguredEvent(eventRequest, eventResponse, window);
        } else if (eventName.equals(spCreateSpEvent)) {
            handleSpCreateSpEvent(eventRequest, eventResponse, window);
        } else if (eventName.equals(spViewSpEvent)) {
            handleSpViewSpEvent(eventRequest, eventResponse, window);
        } else if (eventName.equals(spCancelSpEvent)) {
            handleSpCancelSpEvent(eventRequest, eventResponse, window);
        } else if (spCreateAppEvent.equals(eventName)) {
            handleSpCreateAppEvent(window);
        } else if (spEditSpEvent.equals(eventName)) {
            handleSpEditSpEvent(eventRequest, eventResponse, window);
        } else if (eventName.equals(spViewAppEvent)) {
            handleSpViewAppEvent(window);
        } else if (eventName.equals(spViewUploadBuildFilesK)) {
            handleSpViewDownloadableAppEvent(window);
        } else if (eventName.equals(spCreateCrEvent)) {
            handleSpCreateCrEvent(window);
        } else if (eventName.equals(adminNewSpRequestsEvent)) {
            handleAdminNewSpRequests(window);
        } else if (eventName.equals(adminChangeSpRequestsEvent)) {
            handleAdminChangeSpRequest(window);
        } else if (eventName.equals(adminManageSpRequestsEvent)) {
            handleAdminManageSpRequests(window);
        } else if (eventName.equals(adminNewAppRequestsEvent)) {
            handleAdminNewAppRequests(window);
        } else if (eventName.equals(adminManageAppsEvent)) {
            handleAdminManageAppRequests(window);
        } else if (adminManageBuildFilesEventK.equals(eventName)) {
            handleAdminManageBuildFileRequests();
        } else if (eventName.equals("draft-app")) {
            HashSet<String> portletIdSet = (HashSet) eventData.get(portletIdSetK);
            for (String portletId : portletIdSet) {
                themeDisplay.getLayoutTypePortlet().addPortletId(userId, portletId);
                PortletEventSender.send(eventData, draftK, provisioning);
            }
        } else if (eventName.equals(provRefreshEvent)) {
            handleProvRefreshRequest(eventData, window);
        } else if (eventName.equals(provCrEditNcsEvent)) {
            handleProvCrEditNcsEvent(eventData, userId, themeDisplay, window);
        }
    }

    private void handleProvCrEditNcsEvent(Map<String, Object> eventData, Long userId, ThemeDisplay themeDisplay, Window window) throws SystemException, PortalException {
        String senderPortletId = (String) eventData.get(portletIdK);
        themeDisplay.getLayoutTypePortlet().addPortletId(userId, senderPortletId, "column-2", 1);
        PortletEventSender.send(eventData, crEditNcsEvent, provisioning);
        themeDisplay.getLayoutTypePortlet().removePortletId(userId, portletId());
    }

    private void handleSpCreateCrEvent(Window window) {
        InitialCrSelectionWindow initialCrSelectionWindow = new InitialCrSelectionWindow(provisioning, (String) provisioning.getFromSession(spIdK));
        applyStyles(initialCrSelectionWindow);
        provisioning.getUiManager().clearAll();
        provisioning.getUiManager().pushScreen(AppManagementPanel.class.getName(), initialCrSelectionWindow);
    }

    private void handleProvRefreshRequest(Map<String, Object> eventData, Window window) {
        if (eventData == null || eventData.get(provViewK) == null) return;

        String view = (String) eventData.get(provViewK);
        if (view.equals(spHomeK)) {
            provisioning.showSpHomeScreen();
        }
    }

    private void handleViewNcsEvent(String eventName, Map<String, Object> eventData, Long userId, ThemeDisplay themeDisplay) throws SystemException, PortalException {
        String senderPortletId = (String) eventData.get(portletIdK);
        themeDisplay.getLayoutTypePortlet().addPortletId(userId, senderPortletId, "column-2", 1);
        PortletEventSender.send(eventData, viewNcsEvent, provisioning);
        themeDisplay.getLayoutTypePortlet().removePortletId(userId, portletId());
    }

    private void handleAdminManageAppRequests(Window window) {
        AppManagementPanel appManagementPanel = new AppManagementPanel(provisioning, adminUserTypeK);
        appManagementPanel.setCaption(getMsg("sp.viewApp.appManagementTitle"));
        appManagementPanel.loadComponents();
        applyStyles(appManagementPanel);
        provisioning.getUiManager().clearAll();
        provisioning.getUiManager().pushScreen(AppManagementPanel.class.getName(), appManagementPanel);
    }

    private void handleAdminManageBuildFileRequests() throws SystemException, PortalException {
        BuildFileManagementPanel buildFileManagementPanel = new BuildFileManagementPanel(provisioning);
        buildFileManagementPanel.setCaption(getMsg("sp.downloadableApp.build.file.managementTitle"));
        buildFileManagementPanel.loadComponents();
        applyStyles(buildFileManagementPanel);
        provisioning.getUiManager().clearAll();
        provisioning.getUiManager().pushScreen(BuildFileManagementPanel.class.getName(), buildFileManagementPanel);
    }

    private void handleAdminNewAppRequests(Window window) {
        AppManagementPanel appManagementPanel = new AppManagementPanel(provisioning, adminUserTypeK);
        appManagementPanel.setCaption(getMsg("sp.viewApp.appManagementTitle"));
        appManagementPanel.setPendingAppOnly(true);
        appManagementPanel.loadComponents();
        applyStyles(appManagementPanel);
        provisioning.getUiManager().clearAll();
        provisioning.getUiManager().pushScreen(AppManagementPanel.class.getName(), appManagementPanel);
    }

    private void handleAdminManageSpRequests(Window window) {
        SpManagementPanel spManagementPanel = new SpManagementPanel(provisioning);
        spManagementPanel.setCaption(getMsg("admin.sp.mange.sp.title"));
        spManagementPanel.loadComponents();
        applyStyles(spManagementPanel);
        provisioning.getUiManager().clearAll();
        provisioning.getUiManager().pushScreen(SpManagementPanel.class.getName(), spManagementPanel);
    }

    private void handleAdminChangeSpRequest(Window window) {
        SPApproveApplication spApproveApplication = new SPApproveApplication(provisioning, SpSearchViewMode.CR, getMsg("admin.sp.change.requests.title"));
        applyStyles(spApproveApplication);
        provisioning.getUiManager().clearAll();
        provisioning.getUiManager().pushScreen(AppManagementPanel.class.getName(), spApproveApplication);
    }

    private void handleAdminNewSpRequests(Window window) {
        SpManagementPanel spManagementPanel = new SpManagementPanel(provisioning);
        spManagementPanel.setCaption(getMsg("admin.sp.new.requests.title"));
        spManagementPanel.setPendingSpOnly(true);
        spManagementPanel.loadComponents();
        applyStyles(spManagementPanel);
        provisioning.getUiManager().clearAll();
        provisioning.getUiManager().pushScreen(SpManagementPanel.class.getName(), spManagementPanel);
    }

    private void handleSpViewAppEvent(Window window) {
        loadSp(window);
        AppManagementPanel appManagementPanel = new AppManagementPanel(provisioning, spUserTypeK);
        appManagementPanel.setCaption(getMsg("sp.viewApp.appManagementTitle"));
        appManagementPanel.setSpId((String) provisioning.getFromSession(spIdK));
        appManagementPanel.loadComponents();
        applyStyles(appManagementPanel);
        provisioning.getUiManager().clearAll();
        provisioning.getUiManager().pushScreen(AppManagementPanel.class.getName(), appManagementPanel);
    }

    private void handleSpCreateAppEvent(Window window) {
        loadSp(window);
        AppPanel appPanel = AppPanel.newCreateAppPanel(provisioning, provisioning.getUserRoles(), (String) provisioning.getFromSession(spIdK), provisioning);
        provisioning.getUiManager().pushScreen(AppPanel.class.getName(), appPanel);
    }

    private void handleSpCancelSpEvent(EventRequest eventRequest, EventResponse eventResponse, Window window) {
        reloadSpData();
        viewSp(window, ViewMode.CANCEL);
    }

    private void handleSpViewSpEvent(EventRequest eventRequest, EventResponse eventResponse, Window window) {
        reloadSpData();
        viewSp(window, ViewMode.VIEW);
    }

    private void handleSpEditSpEvent(EventRequest eventRequest, EventResponse eventResponse, Window window) {
        reloadSpData();
        provisioning.getSp().put(KiteKeyBox.coopUserIdK, provisioning.getCoopUserId());
        getSp().put(statusK, draftK);
        SPApplication spApplication = new SPApplication(provisioning);
        applyStyles(spApplication);
        provisioning.getUiManager().clearAll();
        provisioning.getUiManager().pushScreen(SPApplication.class.getName(), spApplication);
    }

    private void handleSpCreateSpEvent(EventRequest eventRequest, EventResponse eventResponse, Window window) {
        getSp().put(idK, IdGenerator.generateId());
        getSp().put(spIdK, ProvIdGenerator.generateSpid());
        getSp().put(KiteKeyBox.coopUserIdK, provisioning.getCoopUserId());
        getSp().put(statusK, draftK);
        SPApplication spApplication = new SPApplication(provisioning);
        applyStyles(spApplication);
        provisioning.getUiManager().clearAll();
        provisioning.getUiManager().pushScreen(SPApplication.class.getName(), spApplication);
    }

    private void handleSaveNcsEvent(String eventName, Map<String, Object> eventData, Long userId, ThemeDisplay themeDisplay) throws SystemException, PortalException {
        List<Map<String, Object>> selectedList = (List) eventData.get(ncsesK);
        for (Map<String, Object> selectedServiceMap : selectedList) {
            if (selectedK.equals(selectedServiceMap.get(statusK))) {
                continue;//because it is not configured yet and not available in db
            }
            Map<String, Object> newEventData = new HashMap<String, Object>();
            String ncsType = (String) selectedServiceMap.get(ncsTypeK);
            String operator = (String) selectedServiceMap.get(operatorK);
            String portletId = createPortletId(ncsType, operator);
            themeDisplay.getLayoutTypePortlet().addPortletId(userId, portletId);
            newEventData.put(ncsTypeK, ncsType);
            newEventData.put(appIdK, eventData.get(appIdK));
            newEventData.put(operatorK, operator);
            newEventData.put(spIdK, eventData.get(spIdK));
            newEventData.put(statusK, eventData.get(statusK));
            newEventData.put(provEventAppNameK, eventData.get(provEventAppNameK));
            PortletEventSender.send(newEventData, ProvKeyBox.saveNcsEvent, provisioning);
        }
    }

    private void handleCreateNcsEvent(String eventName, Map<String, Object> eventData, Long userId, ThemeDisplay themeDisplay) throws SystemException, PortalException {
        String senderPortletId = (String) eventData.get(portletIdK);
        themeDisplay.getLayoutTypePortlet().addPortletId(userId, senderPortletId, "column-2", 1);
        PortletEventSender.send(eventData, createNcsEvent, provisioning);
        themeDisplay.getLayoutTypePortlet().removePortletId(userId, portletId());
    }

    private void handleReconfigureNcsEvent(String eventName, Map<String, Object> eventData, Long userId, ThemeDisplay themeDisplay) throws SystemException, PortalException {
        String senderPortletId = (String) eventData.get(portletIdK);
        themeDisplay.getLayoutTypePortlet().addPortletId(userId, senderPortletId, "column-2", 1);
        PortletEventSender.send(eventData, reConfigureNcsEvent, provisioning);
        themeDisplay.getLayoutTypePortlet().removePortletId(userId, portletId());
    }

    private void handleSpViewDownloadableAppEvent(Window window) {
        loadSp(window);
        AppManagementPanel appManagementPanel = new AppManagementPanel(provisioning, spUserTypeK);
        appManagementPanel.setCaption(getMsg("sp.downloadableApp.build.file.managementTitle"));
        appManagementPanel.setDownloadableAppOnly(true);
        appManagementPanel.setSpId((String) provisioning.getFromSession(spIdK));
        appManagementPanel.loadComponents();
        applyStyles(appManagementPanel);
        provisioning.getUiManager().clearAll();
        provisioning.getUiManager().pushScreen(AppManagementPanel.class.getName(), appManagementPanel);
    }

    /* v Downloadable portlet only v */

    private void handleViewBuildFileEvent(String eventName, Map<String, Object> eventData, Long userId, ThemeDisplay themeDisplay) throws SystemException, PortalException {
        logger.debug("Provisioning Downloadable portlet received event [{}] with event data [{}]", eventName, eventData);

        String senderPortletId = (String) eventData.get(portletIdK);
        themeDisplay.getLayoutTypePortlet().addPortletId(userId, senderPortletId, "column-2", 1);
        PortletEventSender.send(eventData, viewBuildFileEventK, provisioning);
        themeDisplay.getLayoutTypePortlet().removePortletId(userId, portletId());
    }

    private void handleCreateBuildFileEvent(String eventName, Map<String, Object> eventData, Long userId, ThemeDisplay themeDisplay) throws SystemException, PortalException {
        logger.debug("Provisioning Downloadable portlet received event [{}] with event data [{}]", eventName, eventData);

        String senderPortletId = (String) eventData.get(portletIdK);
        themeDisplay.getLayoutTypePortlet().addPortletId(userId, senderPortletId, "column-2", 1);
        PortletEventSender.send(eventData, createBuildFileEventK, provisioning);
        themeDisplay.getLayoutTypePortlet().removePortletId(userId, portletId());
    }

    private void handleReconfigureBuildFileEvent(String eventName, Map<String, Object> eventData, Long userId, ThemeDisplay themeDisplay) throws SystemException, PortalException {
        logger.debug("Provisioning Downloadable portlet received event [{}] with event data [{}]", eventName, eventData);

        String senderPortletId = (String) eventData.get(portletIdK);
        themeDisplay.getLayoutTypePortlet().addPortletId(userId, senderPortletId, "column-2", 1);
        PortletEventSender.send(eventData, reConfigureBuildFileEventK, provisioning);
        themeDisplay.getLayoutTypePortlet().removePortletId(userId, portletId());
    }

    private void handleSaveBuildFileEvent(String eventName, Map<String, Object> eventData, Long userId, ThemeDisplay themeDisplay) throws SystemException, PortalException {
        logger.debug("Provisioning Downloadable portlet received event [{}] with event data [{}]", eventName, eventData);

        List<Map<String, Object>> selectedList = (List) eventData.get(ncsesK);
        for (Map<String, Object> selectedServiceMap : selectedList) {
            if (selectedK.equals(selectedServiceMap.get(statusK))) {
                continue;//because it is not configured yet and not available in db
            }
            Map<String, Object> newEventData = new HashMap<String, Object>();
            String ncsType = (String) selectedServiceMap.get(ncsTypeK);
            String operator = (String) selectedServiceMap.get(operatorK);
            String portletId = createPortletId(ncsType, operator);
            themeDisplay.getLayoutTypePortlet().addPortletId(userId, portletId);
            newEventData.put(ncsTypeK, ncsType);
            newEventData.put(appIdK, eventData.get(appIdK));
            newEventData.put(operatorK, operator);
            newEventData.put(spIdK, eventData.get(spIdK));
            newEventData.put(statusK, eventData.get(statusK));
            PortletEventSender.send(newEventData, ProvKeyBox.saveBuildFileEventK, provisioning);
        }
    }

    /* ^ Downloadable portlet only  ^ */

    private void handleNcsConfiguredEvent(EventRequest eventRequest, EventResponse eventResponse, Window window) {
        PortletEventHandler eventHandler = (PortletEventHandler) provisioning.getFromSession(ProvKeyBox.ncsConfiguredEventHandler);
        if (eventHandler != null) {
            eventHandler.handleEventRequest(eventRequest, eventResponse, window);
        } else {
            logger.error("Event handler not found to handle {}", ncsConfiguredK);
        }
    }

    private void reloadSpData() {
        if (getSp() == null || getSp().size() == 0) {
            provisioning.addToSession(currentSpDataK, spRepositoryService().findSpByCoopUserId(provisioning.getCoopUserId()));
        }
        if (getSp() == null) {
            provisioning.addToSession(currentSpDataK, new HashMap<String, Object>());
        }
    }

    private void loadSp(Window window) {
        if (provisioning.getCoopUserId() != null) {
            Map<String, Object> serviceProvider = spRepositoryService().findSpByCoopUserId(provisioning.getCoopUserId());

            if (serviceProvider == null) {
                throw new RuntimeException("Logged in user is not a registered service provider");
            }
        }
    }

    private void viewSp(Window window, ViewMode viewMode) {
        AllNcsTypesLayout ncsConfigurationWindow = new NcsTypeConfigurationLayout(provisioning, viewMode, provisioning.getMsg("sp.ncs.configuration.title"));

        ncsConfigurationWindow.reloadSpNcs(getSp());
        ncsConfigurationWindow.loadComponents();
        applyStyles(ncsConfigurationWindow);
        provisioning.getUiManager().clearAll();
        provisioning.getUiManager().pushScreen(AllNcsTypesLayout.class.getName(), ncsConfigurationWindow);
    }

    private Set<String> createAndAddToAllPortletNames(List<Map<String, Object>> selectedServices) {
        HashSet<String> portletNameSet = new HashSet<String>();
        for (Map<String, Object> selectedServiceMap : selectedServices) {
            String ncsType = (String) selectedServiceMap.get(ncsTypeK);
            String operator = (String) selectedServiceMap.get(operatorK);
            String portletName = createPortletId(ncsType, operator);
            portletNameSet.add(portletName);
        }
        return portletNameSet;
    }

    private void applyStyles(Layout content) {
        content.addStyleName("container-panel");
        content.setWidth("760px");
        content.setMargin(true);
    }

    public void handleEventRequest(EventRequest eventRequest, EventResponse eventResponse, Window window) {
        try {
            handleReceivedEvent(eventRequest, eventResponse, window);
        } catch (Exception e) {
            logger.error("Unexpected error occurred while handling event ", e);
        }
    }
}