package hms.kite.provisioning.daemon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.provisioning.ProvisioningServiceRegistry.buildFileRepositoryService;
import static hms.kite.util.KiteKeyBox.*;

/**
 * Created by IntelliJ IDEA.
 * User: isuruanu
 * Date: 6/7/12
 * Time: 5:39 PM
 * <p/>
 * This will delete all the temporary build files
 */
public class BuildFileExpirationDaemon {

    private static Logger logger = LoggerFactory.getLogger(BuildFileExpirationDaemon.class);
    private static long timeOut;

    public void execute() {
        logger.debug("Start searching for unused build files.");
        Map<String, Object> query = new HashMap<String, Object>();
        query.put(buildNameK, "");
        query.put(statusK, initialK);
        List<Map<String, Object>> buildFileList = buildFileRepositoryService().search(query);
        if (buildFileList != null) {
            for (Map<String, Object> buildFile : buildFileList) {
                Date lastUpdateDate = (Date) buildFile.get(updatedDateK);
                if ((new Date()).getTime() - lastUpdateDate.getTime() > timeOut) {
                    logger.debug("Deleting build file [id = {}] uploaded  [{}]", buildFile.get(idK), buildFile.get(updatedDateK));
                    buildFileRepositoryService().deleteBuildFile(buildFile);
                }
            }
        } else {
            logger.debug("build file list is empty");
        }
    }

    public void setTimeOut(long timeOut) {
        BuildFileExpirationDaemon.timeOut = timeOut;
    }
}
