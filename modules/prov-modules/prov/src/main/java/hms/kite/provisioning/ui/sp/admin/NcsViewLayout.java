/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.sp.admin;

import com.vaadin.ui.*;
import hms.kite.provisioning.Provisioning;
import hms.kite.provisioning.ui.common.ViewMode;
import hms.kite.provisioning.ui.sp.ncs.AllNcsTypesLayout;
import hms.kite.provisioning.ui.sp.ncs.NcsConfigurationView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.provisioning.commons.util.ProvKeyBox.spActionStatusK;
import static hms.kite.provisioning.ui.common.ViewMode.VIEW;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class NcsViewLayout extends AllNcsTypesLayout implements AdminActionPerformedListener {

    private static final Logger logger = LoggerFactory.getLogger(NcsViewLayout.class);

    private Map<String, Object> sp;
    private AdminActionHandler adminActionHandler;

    private Label statusLbl = new Label();

    private void setStatusMessage(String status) {
        String spId = (String) provisioning.getSp().get(coopUserNameK);

        statusLbl.setVisible(false);

        if (suspendedK.equals(status)) {
            statusLbl.setValue(provisioning.getMsg("manage.sp.suspend.sp.success.message", spId));
            statusLbl.setVisible(true);
            statusLbl.setStyleName("sp-request-success-notification");
        } else if (approvedK.equals(status)) {
            statusLbl.setValue(provisioning.getMsg("manage.sp.approve.sp.success.message", spId));
            statusLbl.setVisible(true);
            statusLbl.setStyleName("sp-request-success-notification");
        } else if (rejectK.equals(status)) {
            statusLbl.setValue(provisioning.getMsg("manage.sp.reject.sp.success.message", spId));
            statusLbl.setVisible(true);
            statusLbl.setStyleName("sp-request-success-notification");
        } else if (("failNotification").equals(status)) {
            statusLbl.setValue(provisioning.getMsg("manage.sp.fail.to.manage.message", spId));
            statusLbl.setVisible(true);
            statusLbl.setStyleName("sp-request-fail-notification");
        }
    }

    private void setActionStatus(String status) {
        provisioning.addToSession(spActionStatusK, status);
    }

    private String getActionStatus() {
        return provisioning.getFromSession(spActionStatusK).toString();
    }

    private boolean hasPermission(String role) {
        return provisioning.hasAnyRole(role);
    }

    private void createSuspendButton(HorizontalLayout buttonBarLayout) {
        if (hasPermission("ROLE_PROV_SP_ACTION_SUSPEND")) {
            Button suspendButton = new Button(provisioning.getMsg("admin.sp.approve.suspend"));
            suspendButton.addListener(new Button.ClickListener() {

                @Override
                public void buttonClick(Button.ClickEvent event) {
                    try {
                        adminActionHandler.showSuspendConfirmation(NcsViewLayout.this);
                    } catch (Exception e) {
                        logger.error("Unexpected error occurred while suspending the SP", e);
                    }
                }
            });
            buttonBarLayout.addComponent(suspendButton);
        }
    }

    private void createRejectButton(HorizontalLayout buttonBarLayout) {
        if (hasPermission("ROLE_PROV_SP_ACTION_REJECT")) {
            Button rejectButton = new Button(provisioning.getMsg("admin.sp.approve.reject"));
            rejectButton.addListener(new Button.ClickListener() {

                @Override
                public void buttonClick(Button.ClickEvent event) {
                    try {
                        adminActionHandler.showRejectConfirm(NcsViewLayout.this);
                    } catch (Exception e) {
                        logger.error("Unexpected error occurred while rejecting the SP", e);
                    }
                }
            });
            buttonBarLayout.addComponent(rejectButton);
        }
    }

    private void createApproveButton(HorizontalLayout buttonBarLayout) {
        if (hasPermission("ROLE_PROV_SP_ACTION_APPROVE")) {
            Button approveButton = new Button(provisioning.getMsg("admin.sp.approve.approve"));
            approveButton.addListener(new Button.ClickListener() {

                @Override
                public void buttonClick(Button.ClickEvent event) {
                    try {
                        if (validate()) {
                            Map<String, Object> sp = provisioning.getSp();
                            spRepositoryService().update(sp);
                            adminActionHandler.showApproveConfirm(NcsViewLayout.this);
                            viewMode = VIEW;
                        } else {
                            logger.error("Due to validation failure, can't approve SP.");
                        }
                    } catch (Exception e) {
                        logger.error("Unexpected error occurred while approving the SP", e);
                    }
                }
            });
            buttonBarLayout.addComponent(approveButton);
        }
    }

    private void createEnableButton(HorizontalLayout buttonBarLayout) {
        if (hasPermission("ROLE_PROV_SP_ACTION_ENABLE")) {
            Button enableButton = new Button(provisioning.getMsg("admin.sp.approve.enable"));
            enableButton.addListener(new Button.ClickListener() {

                @Override
                public void buttonClick(Button.ClickEvent event) {
                    try {
                        adminActionHandler.showEnableConfirm(NcsViewLayout.this);
                    } catch (Exception e) {
                        logger.error("Unexpected error occurred while enabling sp", e);
                    }
                }
            });
            buttonBarLayout.addComponent(enableButton);
        }
    }

    private void createBackButton(HorizontalLayout buttonBarLayout) {
        Button backButton = new Button(provisioning.getMsg("admin.sp.approve.back"));
        backButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    goBack();
                    setActionStatus("");
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while going back from sp approval page", e);
                }
            }
        });
        buttonBarLayout.addComponent(backButton);
    }

    public boolean validate() {
        for (NcsConfigurationView ncsConfigurationView : ncsConfigurationViewList) {
            if (!ncsConfigurationView.validateData()) {
                return false;
            }
        }
        return true;
    }

    private void goBack() {
        logger.debug("Going back from sp approval page ");

        provisioning.getUiManager().popScreen();
    }

    @Override
    protected Component createHeader() {
        return new VerticalLayout();
    }

    @Override
    protected Component createFooter() {
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setWidth("100%");
        HorizontalLayout buttonBarLayout = new HorizontalLayout();
        buttonBarLayout.setSpacing(true);

        String status = (String) sp.get(statusK);

        createBackButton(buttonBarLayout);

        if (pendingApproveK.equals(status)) {
            createApproveButton(buttonBarLayout);
            createRejectButton(buttonBarLayout);
        } else if (approvedK.equals(status)) {
            createSuspendButton(buttonBarLayout);
        } else if (suspendedK.equals(status)) {
            createEnableButton(buttonBarLayout);
        }

        setStatusMessage(getActionStatus());
        setActionStatus("");

        verticalLayout.addComponent(statusLbl);
        verticalLayout.addComponent(buttonBarLayout);
        verticalLayout.setComponentAlignment(buttonBarLayout, Alignment.MIDDLE_CENTER);
        return verticalLayout;
    }

    @Override
    protected void reloadComponents() {
        sp = spRepositoryService().findSpById((String) sp.get(spIdK));

        super.reloadComponents();
    }

    public NcsViewLayout(Provisioning provisioning, Map<String, Object> sp, AdminActionHandler adminActionHandler,
                         ViewMode viewMode) {
        super(provisioning, viewMode);

        this.sp = sp;
        this.adminActionHandler = adminActionHandler;
    }

    public NcsViewLayout(Provisioning provisioning, Map<String, Object> sp, AdminActionHandler adminActionHandler) {
        this(provisioning, sp, adminActionHandler, VIEW);
    }

    @Override
    public void onActionPerformed() {
        reloadComponents();
    }
}
