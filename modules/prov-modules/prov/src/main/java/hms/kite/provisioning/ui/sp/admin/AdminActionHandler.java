/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.sp.admin;

import hms.kite.provisioning.Provisioning;
import hms.kite.provisioning.ui.common.ConfirmationDialog;
import hms.kite.provisioning.ui.common.ConfirmationListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.provisioning.ProvisioningServiceRegistry.eventRouter;
import static hms.kite.provisioning.commons.util.ProvKeyBox.originalSpK;
import static hms.kite.provisioning.commons.util.ProvKeyBox.spActionStatusK;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class AdminActionHandler {

    private static final Logger logger = LoggerFactory.getLogger(AdminActionHandler.class);

    private Provisioning provisioning;

    private void updateSp(Map<String, Object> originalSp) {
        spRepositoryService().update(provisioning.getSp());
        fireSpEditByAdminEvent(originalSp);
    }

    private void fireSpEditByAdminEvent(Map<String, Object> originalSp) {
        Map<String, Object> spCreatedEvent = createSpEditByAdminEvent(originalSp);
        logger.debug("Firing SP editing event {} ", spCreatedEvent);
        eventRouter().fire(spCreatedEvent);
    }

    private Map<String, Object> createSpEditByAdminEvent(Map<String, Object> originalSp) {
        Map<String, Object> spEditByAdminEventData = new HashMap<String, Object>();
        spEditByAdminEventData.put(eventNameK, spEditByAdminEvent);
        spEditByAdminEventData.put(spK, provisioning.getSp());
        spEditByAdminEventData.put(originalSpK, originalSp);

        return spEditByAdminEventData;
    }

    private void approveSp(Map<String, Object> sp, String note) {
        try {
            changeSpStatus(sp, approvedK, note);
            Map<String, Object> approveEvent = createSpApprovedEvent(sp, note);
            eventRouter().fire(approveEvent);
            logger.info("SP with Id [{}] approved with note [{}]", sp.get(spIdK), note);
        } catch (Exception e) {
            logger.error("Unexpected Error occurred while approving sp", e);

            setActionStatus("failNotification");
        }
    }

    private Map<String, Object> createSpApprovedEvent(Map<String, Object> sp, String note) {
        Map<String, Object> approveEvent = new HashMap<String, Object>();
        approveEvent.put(eventNameK, spRegistrationApprovedEvent);
        approveEvent.put(spK, sp);
        approveEvent.put(remarksK, note);
        return approveEvent;
    }

    private void rejectSp(Map<String, Object> sp, String note) {
        try {
            changeSpStatus(sp, rejectK, note);
            Map<String, Object> rejectEvent = createRejectEvent(sp, note);
            eventRouter().fire(rejectEvent);
            logger.info("SP with Id [{}] rejected with note [{}]", sp.get(spIdK), note);
        } catch (Exception e) {
            logger.error("Unexpected Error occurred while rejecting sp", e);

            setActionStatus("failNotification");
        }
    }

    private Map<String, Object> createRejectEvent(Map<String, Object> sp, String note) {
        Map<String, Object> rejectEvent = new HashMap<String, Object>();
        rejectEvent.put(eventNameK, spRegistrationRejectedEvent);
        rejectEvent.put(spK, sp);
        rejectEvent.put(remarksK, note);
        return rejectEvent;
    }

    private void enableSp(Map<String, Object> sp, String note) {
        try {
            changeSpStatus(sp, (String) sp.get(previousStateK), note);
            Map<String, Object> enableEvent = createRestoreEvent(sp, note);
            eventRouter().fire(enableEvent);
            logger.info("SP with Id [{}] enabled with note [{}]", sp.get(spIdK), note);
        } catch (Exception e) {
            logger.error("Unexpected Error occurred while enabling sp", e);

            setActionStatus("failNotification");
        }
    }

    private Map<String, Object> createRestoreEvent(Map<String, Object> sp, String note) {
        Map<String, Object> suspendEvent = new HashMap<String, Object>();
        suspendEvent.put(eventNameK, spRestoreEvent);
        suspendEvent.put(spK, sp);
        suspendEvent.put(remarksK, note);
        return suspendEvent;
    }

    private void suspendSp(Map<String, Object> sp, String note) {
        try {
            sp.put(previousStateK, sp.get(statusK));
            changeSpStatus(sp, suspendedK, note);
            Map<String, Object> suspendEvent = createSuspendEvent(sp, note);
            eventRouter().fire(suspendEvent);
            logger.info("SP with Id [{}] suspended with note [{}]", sp.get(spIdK), note);
        } catch (Exception e) {
            logger.error("Unexpected Error occurred while suspending sp", e);

            setActionStatus("failNotification");
        }
    }

    private Map<String, Object> createSuspendEvent(Map<String, Object> sp, String note) {
        Map<String, Object> suspendEvent = new HashMap<String, Object>();
        suspendEvent.put(eventNameK, spSuspendEvent);
        suspendEvent.put(spK, sp);
        suspendEvent.put(remarksK, note);
        return suspendEvent;
    }

    private void changeSpStatus(Map<String, Object> sp, String newStatus, String note) {
        sp.put(statusK, newStatus);
        sp.put(remarksK, note);
        spRepositoryService().update(sp);
    }

    private void fireActionPerformed(AdminActionPerformedListener actionPerformedListener) {
        if (actionPerformedListener != null) {
            actionPerformedListener.onActionPerformed();
        }
    }

    private void setActionStatus(String status) {
        provisioning.addToSession(spActionStatusK, status);
    }

    public AdminActionHandler(Provisioning provisioning) {
        this.provisioning = provisioning;
    }

    public void showRejectConfirm(final AdminActionPerformedListener actionPerformedListener) {
        final String spId = (String) provisioning.getSp().get(coopUserNameK);
        logger.debug("Rejecting SP with spId = [{}]", spId);
        ConfirmationDialog confirmationDialog = new ConfirmationDialog(
                provisioning,
                provisioning.getMsg("admin.spSearchAndView.rejectConfirmation.title"),
                provisioning.getMsg("admin.spSearchAndView.rejectConfirmation.message", spId),
                new ConfirmationListener() {

                    @Override
                    public void onYes(String note) {
                        rejectSp(provisioning.getSp(), note);
                        setActionStatus(rejectK);
                        fireActionPerformed(actionPerformedListener);
                    }
                });
        confirmationDialog.showDialog();
    }


    public void showEnableConfirm(final AdminActionPerformedListener actionPerformedListener) {
        final String spId = (String) provisioning.getSp().get(coopUserNameK);
        logger.debug("Enabling SP with spId = [{}]", spId);
        ConfirmationDialog confirmationDialog = new ConfirmationDialog(
                provisioning,
                provisioning.getMsg("admin.spSearchAndView.enableConfirmation.title"),
                provisioning.getMsg("admin.spSearchAndView.enableConfirmation.message", spId),
                new ConfirmationListener() {

                    @Override
                    public void onYes(String note) {
                        enableSp(provisioning.getSp(), note);
                        setActionStatus(approvedK);
                        fireActionPerformed(actionPerformedListener);
                    }
                });
        confirmationDialog.showDialog();
    }

    public void showSuspendConfirmation(final AdminActionPerformedListener actionPerformedListener) {
        final String spId = (String) provisioning.getSp().get(coopUserNameK);
        logger.info("Suspending SP with spId = [{}]", spId);
        ConfirmationDialog confirmationDialog = new ConfirmationDialog(
                provisioning,
                provisioning.getMsg("admin.spSearchAndView.suspendConfirmation.title"),
                provisioning.getMsg("admin.spSearchAndView.suspendConfirmation.message", spId),
                new ConfirmationListener() {

                    @Override
                    public void onYes(String note) {
                        suspendSp(provisioning.getSp(), note);
                        setActionStatus(suspendedK);
                        fireActionPerformed(actionPerformedListener);
                    }
                });
        confirmationDialog.showDialog();
    }

    public void showApproveConfirm(final AdminActionPerformedListener actionPerformedListener) {
        final String spId = (String) provisioning.getSp().get(coopUserNameK);
        logger.info("Approving SP with spId = [{}]", spId);
        ConfirmationDialog confirmationDialog = new ConfirmationDialog(
                provisioning,
                provisioning.getMsg("admin.spSearchAndView.approveConfirmation.title"),
                provisioning.getMsg("admin.spSearchAndView.approveConfirmation.message", spId),
                new ConfirmationListener() {

                    @Override
                    public void onYes(String note) {
                        approveSp(provisioning.getSp(), note);
                        setActionStatus(approvedK);
                        fireActionPerformed(actionPerformedListener);
                    }
                });
        confirmationDialog.showDialog();
    }

    public void showSaveEditConfirm(final Map<String, Object> originalSp, final AdminActionPerformedListener actionPerformedListener) {
        final String spId = (String) provisioning.getSp().get(coopUserNameK);
        logger.info("Approving SP with spId = [{}]", spId);
        ConfirmationDialog confirmationDialog = new ConfirmationDialog(
                provisioning,
                provisioning.getMsg("admin.spSearchAndView.saveEditConfirmation.title"),
                provisioning.getMsg("admin.spSearchAndView.saveEditCnfirmation.message", spId),
                new ConfirmationListener() {

                    @Override
                    public void onYes(String note) {
                        updateSp(originalSp);
                        fireActionPerformed(actionPerformedListener);
                    }
                });
        confirmationDialog.showDialog();
    }
}
