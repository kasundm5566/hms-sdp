/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.sp;

import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.component.table.AbstractDataManagementPanel;
import hms.kite.provisioning.commons.ui.component.table.DataTable;
import hms.kite.provisioning.commons.ui.component.table.SearchLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class SpManagementPanel extends AbstractDataManagementPanel {

    private static final Logger logger = LoggerFactory.getLogger(SpManagementPanel.class);

    private boolean pendingSpOnly = false;

    @Override
    protected DataTable getDataTable() {
        logger.debug("-----------------------------------Getting Data");
        SpDetailsTable spDetailsTable = new SpDetailsTable(application, this);
        spDetailsTable.setStatusEnabled(!pendingSpOnly);
        return spDetailsTable;
    }

    @Override
    protected SearchLayout getSearchLayout() {
        logger.debug("-----------------------------------Getting Search Layout");
        SpSearchLayout spSearchLayout = new SpSearchLayout(application, this);
        spSearchLayout.setSearchByStatus(!pendingSpOnly);
        spSearchLayout.loadComponents();

        return spSearchLayout;
    }

    @Override
    protected void finalizeQueryCriteria(Map<String, Object> queryCriteria) {
        logger.debug("-----------------------------------Finalizing Query Criteria");
        if (pendingSpOnly) {
            queryCriteria.put(statusK, pendingApproveK);
        }
    }

    @Override
    protected int getTotalRecords(Map<String, Object> queryCriteria) {
        logger.debug("-----------------------------------Getting Total Records");
        return spRepositoryService().countSpBySpQueryCriteria(queryCriteria);
    }

    @Override
    protected List<Map<String, Object>> getRecords(int start, int batchSize, Map<String, Object> queryCriteria) {
        logger.debug("-----------------------------------Getting Records");
        Map<String, Object> orderBy = new HashMap<String, Object>();
        orderBy.put(spRequestDateK, -1);
        queryCriteria.put(orderByK, orderBy);
        return spRepositoryService().findAllSpBySpQueryCriteria(queryCriteria, start, batchSize);
    }

    public SpManagementPanel(BaseApplication application) {
        super(application);

        logger.debug("-----------------------------------Getting SpManagement Panel");
        setNoRecordsFoundError(application.getMsg("manage.sp.search.no.search.result"));
    }

    public void setPendingSpOnly(Boolean pendingSpOnly) {
        logger.debug("-----------------------------------Setting Pending SP-ONLY");
        this.pendingSpOnly = pendingSpOnly;
    }
}