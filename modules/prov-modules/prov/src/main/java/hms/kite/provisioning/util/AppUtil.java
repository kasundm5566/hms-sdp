/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.util;


import hms.kite.util.NullObject;

import java.util.*;

import static hms.kite.provisioning.commons.util.ProvKeyBox.ncsConfigurationK;
import static hms.kite.provisioning.commons.util.ProvKeyBox.ncsTypeK;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class AppUtil {

    private static Map<String, String> ncsTypeDisplayNames;

    static {
        setNcsTypeDisplayNames(new NullObject<Map<String, String>>().get());
    }

    private static void setNcsTypeDisplayNames(Map<String, String> ncsTypeDisplayNames) {
        AppUtil.ncsTypeDisplayNames = ncsTypeDisplayNames;
    }

    public static String getNcsDisplayName(String ncsType){
        return AppUtil.ncsTypeDisplayNames.get(ncsType);
    }

    public static Set<String> findNcsTypes(Map<String, Object> app) {
        Set<String> result = new HashSet<String>();

        List<Map<String, Object>> ncsConfigurations = (List<Map<String, Object>>) app.get(ncsConfigurationK);

        if (ncsConfigurations != null) {
            for (Map<String, Object> ncsConfiguration : ncsConfigurations) {
                result.add((String) ncsConfiguration.get(ncsTypeK));
            }
        }

        return result;
    }

    public static List<Map<String, Object>> findNcsesForType(String ncsType, Map<String, Object> app) {

        List<Map<String, Object>> result = new LinkedList<Map<String, Object>>();

        List<Map<String, Object>> ncsConfigurations = (List<Map<String, Object>>) app.get(ncsConfigurationK);

        if (ncsConfigurations != null) {
            for (Map<String, Object> ncsConfiguration : ncsConfigurations) {
                if (ncsConfiguration.get(ncsTypeK).equals(ncsType)) {
                    result.add(ncsConfiguration);
                }
            }
        }

        return result;
    }
}
