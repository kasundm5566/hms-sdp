/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.sp.ncs;

import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.ui.*;
import hms.kite.provisioning.Provisioning;
import hms.kite.provisioning.commons.ui.VaadinUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class NcsAasView extends NcsConfigurationView {

    private static final Logger logger = LoggerFactory.getLogger(NcsAasView.class);

    private Label allowedApiCalls;
    private CheckBox attachDetachChkBox;
    private CheckBox authenticateChkBox;

    private void init(Provisioning provisioning) {
        allowedApiCalls = new Label(provisioning.getMsg("sp.ncs.aas.allowed.api.calls"));
        allowedApiCalls.setImmediate(true);

        attachDetachChkBox =new CheckBox(provisioning.getMsg("sp.ncs.aas.allowed.api.calls.attach.detach"));
        attachDetachChkBox.addListener(new Property.ValueChangeListener() {

            @Override
            public void valueChange(final Property.ValueChangeEvent event) {
                onAttachDetachCheckBoxChanged(Boolean.parseBoolean(event.getProperty().getValue().toString()));
            }
        });

        authenticateChkBox = new CheckBox(provisioning.getMsg("sp.ncs.aas.allowed.api.calls.authenticate"));

        authenticateChkBox.addListener(new Property.ValueChangeListener() {

            @Override
            public void valueChange(final Property.ValueChangeEvent event) {
                onAuthenticateCheckBoxChanged(Boolean.parseBoolean(event.getProperty().getValue().toString()));
            }
        });


       /* attach_detach.setImmediate(true);
        tpdTxtFld.setRequired(true);
        tpdTxtFld.setRequiredError(provisioning.getMsg("sp.ncs.cas.tpd.required.error"));
        tpdTxtFld.addValidator(createValidator("ncsCasTpdRegex", "sp.ncs.cas.tpd.validation"));
        tpdTxtFld.addValidator(createValidator("sp.createApp.zero.validation", "sp.ncs.cas.zero.validation.error.message"));
    */
    }

    private void onAttachDetachCheckBoxChanged(boolean value) {
        //setTpsTpdValues(value, mtTpsTxtFld, mtTpdTxtFld);
    }

    private void onAuthenticateCheckBoxChanged(boolean value) {
        //setTpsTpdValues(value, mtTpsTxtFld, mtTpdTxtFld);
    }

    private void reloadData() {
        Map<String, Object> sp = provisioning.getSp();
        setValueToField(attachDetachChkBox, sp.get(aasAttachDetach));
        setValueToField(authenticateChkBox, sp.get(aasAuthenticate));
    }

 /*   private void checkTpsTpdValidation() {
       int moTpsValue = Integer.parseInt(tpsTxtFld.getValue().toString());
        int moTpdValue = Integer.parseInt(tpdTxtFld.getValue().toString());
        if (moTpsValue >= moTpdValue) {
            throw new Validator.InvalidValueException(provisioning.getMsg("sp.ncs.cas.mt.tps.tpd.validation.message"));
        }
    }*/

    @Override
    protected void changeToViewMode() {
        changeToConfirmMode();
    }

    @Override
    protected void changeToConfirmMode() {
        VaadinUtil.setAllReadonly(true, form.getLayout());
    }

    @Override
    protected void changeToEditMode() {
        attachDetachChkBox.setReadOnly(false);
        authenticateChkBox.setReadOnly(false);

        onAttachDetachCheckBoxChanged(attachDetachChkBox.booleanValue());
        onAuthenticateCheckBoxChanged(authenticateChkBox.booleanValue());
    }

    @Override
    protected void changeToCancelView() {
        changeToViewMode();
    }

    @Override
    protected void setSpSessionValues() {
        Map<String, Object> sp = provisioning.getSp();
    }

    public NcsAasView(Provisioning provisioning) {
        this.provisioning = provisioning;

        init(provisioning);
    }

    @Override
    public void saveData() {
    }

    @Override
    public VerticalLayout loadComponents(final Panel panel) {
        VerticalLayout layout = new VerticalLayout();

        panel.removeAllComponents();
        panel.setCaption(provisioning.getMsg("sp.ncs.aas.title"));
        panel.addComponent(layout);

        loadAasComponents();
        reloadData();
        refreshInChangedMode();

        layout.addComponent(allowedApiCalls);
        layout.setComponentAlignment(allowedApiCalls, Alignment.MIDDLE_CENTER);

        layout.addComponent(form);
        layout.setComponentAlignment(form, Alignment.MIDDLE_CENTER);
        layout.setSpacing(true);

        return layout;
    }

    @Override
    public boolean validateData() {
        try {
            setSpSessionValues();

            boolean attachDetachEnabled = attachDetachChkBox.booleanValue();
            boolean authenticateEnabled = authenticateChkBox.booleanValue();

            if (!attachDetachEnabled && !authenticateEnabled) {
                throw new Validator.InvalidValueException(provisioning.getMsg("sp.ncs.aas.selectAttachOrAuthenticateValidation"));
            }

            form.setComponentError(null);
        } catch (Validator.InvalidValueException e) {
            form.setComponentError(e);
            return false;
        } catch (Exception e) {
            logger.error("Unexpected error occurred while validating cas form", e);
            return false;
        }
        return true;
    }

    public void loadAasComponents() {

        form.addField("aas_attach_detach",attachDetachChkBox);
        form.addField("aas_authenticate",authenticateChkBox);

    }
}
