/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/

package hms.kite.provisioning.ui.sp.ncs;

import com.vaadin.data.Validator;
import com.vaadin.ui.*;
import hms.kite.provisioning.Provisioning;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static hms.kite.provisioning.commons.util.ProvKeyBox.wapPushMtTpdK;
import static hms.kite.provisioning.commons.util.ProvKeyBox.wapPushMtTpsK;

/*
* $LastChangedDate: 2011-07-15 19:33:48 +0530 (Fri, 15 Jul 2011) $
* $LastChangedBy: chandanaw $
* $LastChangedRevision: 75008 $
*/

public class NcsWpView extends NcsConfigurationView {

    private static Logger logger = LoggerFactory.getLogger(NcsWpView.class);

    private TextField moTpsTxtField;
    private TextField moTpdTxtField;

    public NcsWpView(Provisioning provisioning) {
        this.provisioning = provisioning;
        moTpsTxtField = new TextField(provisioning.getMsg("sp.ncs.wp.tps"));
        moTpsTxtField.setImmediate(true);
        moTpsTxtField.setRequired(true);
        moTpsTxtField.setRequiredError(provisioning.getMsg("sp.ncs.wp.tps.required.error"));
        moTpsTxtField.addValidator(createValidator("ncsWpTpsRegex", "sp.ncs.wp.tps.validation"));
        moTpsTxtField.addValidator(createValidator("sp.createApp.zero.validation","sp.ncs.wp.zero.validation.error.message"));

        moTpdTxtField = new TextField(provisioning.getMsg("sp.ncs.wp.tpd"));
        moTpdTxtField.setImmediate(true);
        moTpdTxtField.setRequired(true);
        moTpdTxtField.setRequiredError(provisioning.getMsg("sp.ncs.wp.tpd.required.error"));
        moTpdTxtField.addValidator(createValidator("ncsWpTpdRegex", "sp.ncs.wp.tpd.validation"));
        moTpdTxtField.addValidator(createValidator("sp.createApp.zero.validation","sp.ncs.wp.zero.validation.error.message"));
    }

    private void checkTpsTpdValidation() {
        int moTpsValue = Integer.parseInt(moTpsTxtField.getValue().toString());
        int moTpdValue = Integer.parseInt(moTpdTxtField.getValue().toString());
        if (moTpsValue >= moTpdValue) {
            throw new Validator.InvalidValueException(provisioning.getMsg("sp.ncs.wp.mt.tps.tpd.validation.message"));
        }
    }

        @Override
    public boolean validateData() {
        try {
            setSpSessionValues();
            moTpsTxtField.validate();
            moTpdTxtField.validate();
            checkTpsTpdValidation();
            form.setComponentError(null);
        } catch (Validator.InvalidValueException e) {
            form.setComponentError(e);
            return false;
        } catch (Exception e) {
            logger.error("Unexpected error occurred while validating wap-push form", e);
            return false;
        }
        return true;
    }

    public VerticalLayout loadComponents(final Panel panel) {
        VerticalLayout layout = new VerticalLayout();

        panel.removeAllComponents();
        panel.setCaption(provisioning.getMsg("sp.ncs.wp.title"));
        panel.addComponent(layout);

        loadWPComponents();
        reloadData();
        refreshInChangedMode();
        layout.addComponent(form);
        layout.setComponentAlignment(form, Alignment.MIDDLE_CENTER);
        return layout;
    }

    private void reloadData() {
        moTpsTxtField.setValue(getSpStringValue(wapPushMtTpsK));
        moTpdTxtField.setValue(getSpStringValue(wapPushMtTpdK));
    }

    protected void changeToViewMode() {
        changeToConfirmMode();
    }

    protected void changeToConfirmMode() {
        moTpsTxtField.setReadOnly(true);
        moTpdTxtField.setReadOnly(true);
    }

    protected void changeToEditMode() {
        moTpsTxtField.setReadOnly(false);
        moTpdTxtField.setReadOnly(false);
    }

    @Override
    protected void changeToCancelView() {
        changeToViewMode();
    }

    protected void setSpSessionValues() {
        provisioning.getSp().put(wapPushMtTpsK, moTpsTxtField.getValue());
        provisioning.getSp().put(wapPushMtTpdK, moTpdTxtField.getValue());
    }

    @Override
    public void saveData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void loadWPComponents() {
        form.addField("moTps", moTpsTxtField);
        form.addField("moTpd", moTpdTxtField);
    }
}
