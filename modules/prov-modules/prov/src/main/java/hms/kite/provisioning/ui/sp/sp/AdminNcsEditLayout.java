/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.sp.sp;

import com.vaadin.ui.*;
import hms.kite.provisioning.Provisioning;
import hms.kite.provisioning.ui.common.ViewMode;
import hms.kite.provisioning.ui.sp.admin.AdminActionHandler;
import hms.kite.provisioning.ui.sp.admin.NcsEditLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.provisioning.ui.common.ViewMode.EDIT;
import static hms.kite.util.KiteKeyBox.spSelectedServicesK;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class AdminNcsEditLayout extends AbstractSpNcsTypeSelectionWindow {

    private static Logger logger = LoggerFactory.getLogger(AdminNcsEditLayout.class);

    private Map<String, Object> originalSp;
    private Map<String, Object> sp;
    private AdminActionHandler adminActionHandler;

    private Button createNextButton(final Label errorLbl) {
        Button nextButton = new Button(provisioning.getMsg("sp.register.sp.next"));
        nextButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    boolean isValid = validate();
                    if (!isValid) {
                        return;
                    }
                    errorLbl.setVisible(!isValid);
                    goNext();
                } catch (Exception e) {
                    logger.error("Error occurred while do the next", e);

                    provisioning.getMainWindow().showNotification(e.getMessage(), Window.Notification.TYPE_ERROR_MESSAGE);
                }
            }
        });
        return nextButton;
    }

    private void goNext() {
        setSessionValues();

        NcsEditLayout ncsEditLayout = new NcsEditLayout(provisioning, sp, adminActionHandler, EDIT);
        ncsEditLayout.setOriginalSp(originalSp);
        ncsEditLayout.setTitle(provisioning.getMsg("sp.ncs.configuration.title"));
        ncsEditLayout.addStyleName("container-panel");
        ncsEditLayout.setWidth("760px");
        ncsEditLayout.setMargin(true);
        ncsEditLayout.reloadSpNcs(sp);
        ncsEditLayout.loadComponents();

        provisioning.getSp().put(spSelectedServicesK, spSelectedServices);

        provisioning.getUiManager().pushScreen(NcsEditLayout.class.getName(), ncsEditLayout);
    }

    private Button createBackButton() {
        Button backButton = new Button(provisioning.getMsg("admin.sp.approve.back"));
        backButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    goBack();
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while going back from sp approval page", e);
                }
            }
        });
        return backButton;
    }

    private void goBack() {
        logger.debug("Going back from sp editing page ");

        provisioning.getUiManager().popScreen();
    }

    @Override
    protected Component createButtonBar(final AbstractComponentContainer container) {
        VerticalLayout verticalLayout = new VerticalLayout();

        Label errorLbl = new Label(provisioning.getMsg("sp.register.sp.NcsSelectionError"));
        errorLbl.setStyleName("sp-request-fail-notification");
        errorLbl.setVisible(false);
        verticalLayout.addComponent(errorLbl);

        HorizontalLayout buttonBar = new HorizontalLayout();
        buttonBar.setSpacing(true);

        buttonBar.addComponent(createBackButton());

        buttonBar.addComponent(createNextButton(errorLbl));

        verticalLayout.addComponent(buttonBar);
        verticalLayout.setComponentAlignment(buttonBar, Alignment.MIDDLE_CENTER);

        return verticalLayout;
    }

    public AdminNcsEditLayout(Provisioning provisioning, Map<String, Object> sp, AdminActionHandler adminActionHandler) {
        super(provisioning);

        this.sp = sp;
        this.originalSp = new HashMap<String, Object>(sp);
        this.adminActionHandler = adminActionHandler;
    }
}
