/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.sp;

import com.vaadin.ui.VerticalLayout;
import hms.kite.provisioning.Provisioning;
import hms.kite.provisioning.ui.sp.sp.SpNcsTypeSelectionWindow;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class SPApplication extends VerticalLayout {

    public SPApplication(Provisioning provisioning) {
        SpNcsTypeSelectionWindow editor = new SpNcsTypeSelectionWindow(provisioning);

        editor.loadComponents(this);
    }
}
