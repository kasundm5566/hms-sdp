/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.provisioning.ui.cr;

import com.vaadin.ui.*;
import hms.kite.provisioning.commons.ui.BaseApplication;

import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CrAddNewNcsView {

    private BaseApplication application;
    private Panel mainPanel;
    private Panel childPanel;
    private Form form;
    private Map<String, Object> cr;
    private Map<String, Object> originalApp;

    public CrAddNewNcsView(BaseApplication baseApplication, Panel mainPanel, Map<String, Object> crContext) {
        this.application = baseApplication;
        this.mainPanel = mainPanel;
        this.cr = crContext;
        this.originalApp = originalApp;
        init();
    }

    private void init() {
        childPanel = new Panel("YYY");
        childPanel.addStyleName("child-panel");
        form = new Form();
        VerticalLayout layout = new VerticalLayout();
        childPanel.addComponent(layout);
        layout.addComponent(form);
        layout.setComponentAlignment(form, Alignment.MIDDLE_CENTER);
        mainPanel.addComponent(childPanel);

        form.getFooter().addComponent(createButtonBar());
    }

    private Component createButtonBar() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        Button backButton = new Button(application.getMsg("cr.create.cr.back"));
        backButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                mainPanel.removeAllComponents();
                new InitialCrSelectionWindow(application, mainPanel, cr);

            }
        });
        horizontalLayout.addComponent(backButton);
        return horizontalLayout;
    }

}
