/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.app.validator;

import com.vaadin.data.Validator;
import com.vaadin.ui.DateField;
import hms.kite.provisioning.ui.component.ExpirableComponent;
import hms.kite.provisioning.util.BooleanExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ProvBooleanExpressionValidator implements Validator {

    private static final Logger logger = LoggerFactory.getLogger(ProvBooleanExpressionValidator.class);

    private String errorMessage;
    private BooleanExpression expression;

    private static boolean isExpirable(ExpirableComponent expirableComponent) {
        return expirableComponent.getValue().equals(true);
    }

    public ProvBooleanExpressionValidator(String errorMessage, BooleanExpression expression) {
        this.errorMessage = errorMessage;
        this.expression = expression;
    }

    @Override
    public void validate(Object value) throws InvalidValueException {
        if (!isValid(value)) {
            throw new InvalidValueException(errorMessage);
        }
    }

    @Override
    public boolean isValid(Object value) {
        try {
            if (expression.exp()) {
                return false;
            }
        } catch (Exception e) {
            logger.error("Unexpected error occurred while validating", e);
        }

        return true;
    }

    public static BooleanExpression endDateEmptyValidationExpression(final ExpirableComponent expirableComponent,
                                                                     final DateField productionEndDateFld) {
        return new BooleanExpression() {

            @Override
            public boolean exp() {
                Object endDate = productionEndDateFld.getValue();
                return isExpirable(expirableComponent) && (endDate == null || "".equals(endDate));
            }
        };
    }

    public static BooleanExpression beforeEndDateValidationExpression(final ExpirableComponent expirableComponent,
                                                                      final DateField productionEndDateFld) {
        return new BooleanExpression() {

            @Override
            public boolean exp() {
                Date endDate = (Date) productionEndDateFld.getValue();

                return isExpirable(expirableComponent) && endDate != null && endDate.before(new Date());
            }
        };
    }

    public static BooleanExpression startAndEndDateValidationExpression(final ExpirableComponent expirableComponent,
                                                                        final DateField productionStartDateFld,
                                                                        final DateField productionEndDateFld) {
        return new BooleanExpression() {

            @Override
            public boolean exp() {
                Date startDate = (Date) productionStartDateFld.getValue();
                Date endDate = (Date) productionEndDateFld.getValue();

                return isExpirable(expirableComponent) && startDate != null && endDate != null && endDate.before(startDate);
            }
        };
    }
}
