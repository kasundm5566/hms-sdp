/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.sp.admin;

import com.vaadin.terminal.UserError;
import com.vaadin.ui.*;
import hms.kite.provisioning.Provisioning;
import hms.kite.provisioning.ProvisioningServiceRegistry;
import hms.kite.provisioning.commons.ui.component.table.Paging;
import hms.kite.provisioning.commons.ui.component.table.PagingListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.datarepo.mongodb.MongoUtil.createLikeQuery;
import static hms.kite.provisioning.ProvisioningServiceRegistry.validationRegistry;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
@Deprecated
public class SpSearchAndViewView extends VerticalLayout implements AdminActionPerformedListener {

    private static final Logger logger = LoggerFactory.getLogger(SpSearchAndViewView.class);

    private Provisioning provisioning;
    private Label spNameLbl;
    private Label spStatusLbl;
    private TextField spNameTxtField;
    private Select spStatusSelect;
    private SpSearchResultTable spSearchTable;

    private Paging pagingComponent;
    private SpSearchViewMode viewMode;
    private int totalNumberOfRecords;
    private Label searchResultLbl;
    private String isStatusChange = "";

    private void populateSpStatusSelect() {
        //TODO add as a feature
//        spStatusSelect.addItem(pendingApproveK);   Unnecessary for dialog
        spStatusSelect.addItem(approvedK);
        spStatusSelect.addItem(terminateK);
        spStatusSelect.addItem(suspendedK);

        //TODO add as a feature
//        spStatusSelect.addItem(rejectK);   Unnecessary for dialog
//        spStatusSelect.addItem(draftK);    Unnecessary for dialog

        //TODO add Item caption if required
    }

    private void setLabel(String status) {
        String spId = (String) provisioning.getSp().get(coopUserNameK);
        if (status.equals(approvedK)) {
            setSearchResultLbl(provisioning.getMsg("manage.sp.approve.sp.success.message", spId), "sp-request-success-notification");
        } else if (status.equals(suspendedK)) {
            setSearchResultLbl(provisioning.getMsg("manage.sp.suspend.sp.success.message", spId), "sp-request-success-notification");
        } else if (status.equals(rejectK)) {
            setSearchResultLbl(provisioning.getMsg("manage.sp.reject.sp.success.message", spId), "sp-request-success-notification");
        } else if (status.equals("searchNotFound")) {
            setSearchResultLbl(provisioning.getMsg("manage.sp.search.no.search.result", spId), "sp-request-fail-notification");
        } else if (status.equals("spNameError")) {
            setSearchResultLbl(provisioning.getMsg("sp.search.name.validation.errorMessage", spId), "sp-request-fail-notification");
        } else if (status.equals("failNotification")) {
            setSearchResultLbl(provisioning.getMsg("manage.sp.fail.to.manage.message", spId), "sp-request-fail-notification");
        }
    }

    private void setSearchResultLbl(String message, String style) {
        searchResultLbl.setValue(message);
        searchResultLbl.setVisible(true);
        searchResultLbl.setStyleName(style);
    }

    private Paging createPagingComponent() {
        findTotalNofSp();
        return new Paging(new PagingListener() {

            @Override
            public void onReload(int start, int batchSize) {
                try {
                    List<Map<String, Object>> spList = findSpList(start, batchSize);
                    if (spList.isEmpty()) {
                        if (!isStatusChange.equals("spNameError")) {
                            isStatusChange = "searchNotFound";
                        }
                    }
                    spSearchTable.reloadTable(spList);
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while reloading the table", e);
                }
            }
        }, totalNumberOfRecords, ProvisioningServiceRegistry.nofRecordsForSpResultTable());
    }

    private void findTotalNofSp() {
        if (viewMode == SpSearchViewMode.NEW) {
            totalNumberOfRecords = spRepositoryService().countSpsByStatus(pendingApproveK);
        } else if (viewMode == SpSearchViewMode.CR) {
            logger.error("FIND BY CR Request is not implemented");
            totalNumberOfRecords = 0;
        } else if (viewMode == SpSearchViewMode.ALL) {
            Map<String, Object> queryCriteria = createQueryCriteria();
            totalNumberOfRecords = spRepositoryService().countSpBySpQueryCriteria(queryCriteria);
        } else {
            totalNumberOfRecords = 0;
        }
    }

    private List<Map<String, Object>> findSpList(int start, int batchSize) {
        ArrayList<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();

        Map<String, Object> queryCriteria = createQueryCriteria();
        Map<String, Object> orderBy = new HashMap<String, Object>();
        orderBy.put(spRequestDateK, -1);
        queryCriteria.put(orderByK, orderBy);
        if (viewMode == SpSearchViewMode.NEW) {
            queryCriteria.put(statusK, pendingApproveK);
            return spRepositoryService().findAllSpBySpQueryCriteria(queryCriteria, start, batchSize);
        } else if (viewMode == SpSearchViewMode.CR) {
            logger.error("FIND BY CR Request is not implemented");
        } else if (viewMode == SpSearchViewMode.ALL) {
            return spRepositoryService().findAllSpBySpQueryCriteria(queryCriteria, start, batchSize);
        }

        return resultList;
    }

    private Map<String, Object> createQueryCriteria() {
        Map<String, Object> queryCriteria = new HashMap<String, Object>();
        String spName = (String) spNameTxtField.getValue();
        String spStatus = (String) spStatusSelect.getValue();
        spNameTxtField.setComponentError(null);
        if (spName != null && !spName.isEmpty()) {
            if (!(spNameTxtField.getValue().toString().matches(validationRegistry().regex("sp.search.name.validation")))) {
                String errorMessage = provisioning.getMsg("sp.search.name.validation.errorMessage");
                spNameTxtField.setComponentError(new UserError(errorMessage));
                queryCriteria.put(coopUserNameK, createLikeQuery(spName));
                isStatusChange = "spNameError";
            } else {
                spNameTxtField.setComponentError(null);
                queryCriteria.put(coopUserNameK, createLikeQuery(spName));
            }
        }

        if (spStatus != null && !spStatus.isEmpty()) {
            queryCriteria.put(statusK, spStatus);
        }

        return queryCriteria;
    }

    private Component createSearchLayout() {
        HorizontalLayout searchLayout = new HorizontalLayout();
        searchLayout.setHeight("70px");
        searchLayout.addStyleName("sp-search-layout");
        searchLayout.setSpacing(false);

        searchLayout.addComponent(spNameLbl);
        searchLayout.addComponent(spNameTxtField);
        if (viewMode == SpSearchViewMode.ALL) {
            searchLayout.addComponent(spStatusLbl);
            searchLayout.addComponent(spStatusSelect);
        }
        searchLayout.setMargin(true, false, true, false);
        searchLayout.setSpacing(true);

        Button searchBtn = new Button(provisioning.getMsg("admin.spSearchAndView.search"));
        searchBtn.setImmediate(true);
        searchBtn.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    isStatusChange = "";
                    loadComponents();
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while searching SP ", e);
                }
            }
        });

        searchLayout.addComponent(searchBtn);
        return searchLayout;
    }

    public SpSearchAndViewView(Provisioning provisioning, SpSearchViewMode viewMode) {
        this.provisioning = provisioning;
        this.viewMode = viewMode;

        spNameLbl = new Label(provisioning.getMsg("admin.spSearchAndView.sp.name"));
        spStatusLbl = new Label(provisioning.getMsg("admin.spSearchAndView.sp.status"));
        spNameTxtField = new TextField();
        spNameTxtField.setWidth("200px");
        spStatusSelect = new Select();
        spStatusSelect.setWidth("200px");
        populateSpStatusSelect();

        searchResultLbl = new Label();
        searchResultLbl.setVisible(false);

        spSearchTable = new SpSearchResultTable(provisioning, this, viewMode);

        setSpacing(false);
        loadComponents();
    }

    public VerticalLayout loadComponents() {
        logger.debug("Loading Sp Search and view from View mode [{}]", viewMode);

        removeAllComponents();
        pagingComponent = createPagingComponent();
        addComponent(createSearchLayout());

        pagingComponent.init();
        searchResultLbl.setVisible(false);
        setLabel(isStatusChange);
        addComponent(searchResultLbl);
        isStatusChange = "";
        addComponent(spSearchTable);
        setComponentAlignment(spSearchTable, Alignment.MIDDLE_CENTER);
        addComponent(pagingComponent);
        setComponentAlignment(pagingComponent, Alignment.MIDDLE_CENTER);

        return this;
    }

    @Override
    public void onActionPerformed() {
        loadComponents();
    }
}
