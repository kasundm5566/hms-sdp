/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/

package hms.kite.provisioning.ui.sp.ncs;

import com.vaadin.data.Validator;
import com.vaadin.ui.*;
import hms.kite.provisioning.Provisioning;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static hms.kite.provisioning.commons.util.ProvKeyBox.casTpdK;
import static hms.kite.provisioning.commons.util.ProvKeyBox.casTpsK;

/*
* $LastChangedDate: 2011-07-15 19:33:48 +0530 (Fri, 15 Jul 2011) $
* $LastChangedBy: chandanaw $
* $LastChangedRevision: 75008 $
*/

public class CasNcsView extends NcsConfigurationView {

    private static Logger logger = LoggerFactory.getLogger(CasNcsView.class);

    private TextField moTpsTxtField;
    private TextField moTpdTxtField;

    public CasNcsView(Provisioning provisioning) {
        this.provisioning = provisioning;
        moTpsTxtField = new TextField(provisioning.getMsg("sp.ncs.cas.tps"));
        moTpsTxtField.setImmediate(true);
        moTpsTxtField.setRequired(true);
        moTpsTxtField.setRequiredError(provisioning.getMsg("sp.ncs.cas.tps.required.error"));
        moTpsTxtField.addValidator(createValidator("ncsCasTpsRegex", "sp.ncs.cas.tps.validation"));
        moTpsTxtField.addValidator(createValidator("sp.createApp.zero.validation","sp.ncs.cas.zero.validation.error.message"));

        moTpdTxtField = new TextField(provisioning.getMsg("sp.ncs.cas.tpd"));
        moTpdTxtField.setImmediate(true);
        moTpdTxtField.setRequired(true);
        moTpdTxtField.setRequiredError(provisioning.getMsg("sp.ncs.cas.tpd.required.error"));
        moTpdTxtField.addValidator(createValidator("ncsCasTpdRegex", "sp.ncs.cas.tpd.validation"));
        moTpdTxtField.addValidator(createValidator("sp.createApp.zero.validation","sp.ncs.cas.zero.validation.error.message"));
    }

    public VerticalLayout loadComponents(final Panel panel) {
        VerticalLayout layout = new VerticalLayout();
        panel.removeAllComponents();
        panel.setCaption(provisioning.getMsg("sp.ncs.cas.title"));

        panel.addComponent(layout);

        loadCasComponents();
        reloadData();
        refreshInChangedMode();
        layout.addComponent(form);
        layout.setComponentAlignment(form, Alignment.MIDDLE_CENTER);
        layout.setSpacing(true);
        return layout;
    }

    private void reloadData() {
        moTpsTxtField.setValue(getSpStringValue(casTpsK));
        moTpdTxtField.setValue(getSpStringValue(casTpdK));
    }

    @Override
    protected void changeToViewMode() {
        changeToConfirmMode();
    }

    @Override
    protected void changeToConfirmMode() {
        moTpsTxtField.setReadOnly(true);
        moTpdTxtField.setReadOnly(true);
    }

    @Override
    protected void changeToEditMode() {
        moTpsTxtField.setReadOnly(false);
        moTpdTxtField.setReadOnly(false);
    }

    @Override
    protected void changeToCancelView() {
        changeToViewMode();
    }

    private void checkTpsTpdValidation() {
        int moTpsValue = Integer.parseInt(moTpsTxtField.getValue().toString());
        int moTpdValue = Integer.parseInt(moTpdTxtField.getValue().toString());
        if (moTpsValue >= moTpdValue) {
            throw new Validator.InvalidValueException(provisioning.getMsg("sp.ncs.cas.mt.tps.tpd.validation.message"));
        }
    }

    @Override
    public boolean validateData() {
        try {
            setSpSessionValues();
            moTpsTxtField.validate();
            moTpdTxtField.validate();
            checkTpsTpdValidation();
            form.setComponentError(null);
        } catch (Validator.InvalidValueException e) {
            form.setComponentError(e);
            return false;
        } catch (Exception e) {
            logger.error("Unexpected error occurred while validating cas form", e);
            return false;
        }
        return true;
    }

    protected void setSpSessionValues() {
        provisioning.getSp().put(casTpsK, moTpsTxtField.getValue());
        provisioning.getSp().put(casTpdK, moTpdTxtField.getValue());
    }

    @Override
    public void saveData() {

    }

    public void loadCasComponents() {
        form.addField("moTps", moTpsTxtField);
        form.addField("moTpd", moTpdTxtField);
    }
}
