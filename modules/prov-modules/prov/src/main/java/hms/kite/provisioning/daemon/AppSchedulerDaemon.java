/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.provisioning.daemon;

import hms.kite.provisioning.commons.event.DefaultEventRouter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static hms.kite.datarepo.RepositoryServiceRegistry.appRepositoryService;
import static hms.kite.datarepo.RepositoryServiceRegistry.ncsRepositoryService;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class AppSchedulerDaemon {

    private DefaultEventRouter eventRouter;
    private static final Logger logger = LoggerFactory.getLogger(AppSchedulerDaemon.class);
    private static int counter = 0;

    public void setEventRouter(DefaultEventRouter eventRouter) {
        this.eventRouter = eventRouter;
    }

    public DefaultEventRouter getEventRouter() {
        return eventRouter;
    }

    public void execute() {

        try {
            logger.info("====================================================");
            logger.info("Starting App Scheduler at {} for {}'th time.", new Date(), ++counter);
            logger.info("====================================================");

            doExecute();

        } finally {
            logger.info("====================================================");
            logger.info("Finished App Scheduler at {} for {}'th times.", new Date(), counter);
            logger.info("====================================================");
        }

    }

    private void doExecute() {

        Date date = new GregorianCalendar(TimeZone.getDefault()).getTime();

        List<Map<String, Object>> apps = findEligibleApps(date);

        logger.info("Found {} apps with status {}. Starting moving them to {},  {}",
                new Object[]{apps.size(), approvedActiveProductionK, activeProductionK, date});

        for (Map<String, Object> app : apps) {

            String appId = (String) app.get(appIdK);

            logger.debug("Moving app {} to {}.", app, activeProductionK);

            app.put(statusK, activeProductionK);

            doExecuteForNcses(app);

            appRepositoryService().update(app);

            logger.info("Updated the status of app {} to {}", appId, activeProductionK);
        }
    }

    private List<Map<String, Object>> findEligibleApps(Date date) {
        return appRepositoryService()
                .findAppsByStatusAndActiveProductionTime(approvedActiveProductionK, date);
    }

    private String doExecuteForNcses(Map<String, Object> app) {

        String appId = (String) app.get(appIdK);

        Collection<Map<String,Object>> ncses = ncsRepositoryService().findByAppId(appId).values();

        logger.info("Found {} ncses for app {}", ncses.size(), appId);

        for (Map<String, Object> ncs : ncses) {

            String ncsType = (String) ncs.get(ncsTypeK);
            String operator = (String) ncs.get(operatorK);
            String currentStatus = (String) ncs.get(statusK);

            logger.debug("Changing the status of ncs {}/{}/{} from {} to {}",
                    new Object[]{appId, ncsType, operator, currentStatus, activeProductionK});

            ncsRepositoryService().update(appId, ncsType, operator, activeProductionK);
        }

        return appId;
    }
}
