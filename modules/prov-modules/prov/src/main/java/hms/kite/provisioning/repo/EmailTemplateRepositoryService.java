/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.repo;

import java.util.Map;

/**
 * $LastChangedDate $
 * $LastChangedBy $
 * $LastChangedRevision $
 */
public interface EmailTemplateRepositoryService {

    boolean isEventNameExist (String eventName);

    Map<String, Object> findEventByName (String eventName);

    void create (Map<String, Object> event);

    void deleteEvent (String eventName);

    void deleteAll ();

    void updateEvent (Map<String, Object> newEvent);

}
