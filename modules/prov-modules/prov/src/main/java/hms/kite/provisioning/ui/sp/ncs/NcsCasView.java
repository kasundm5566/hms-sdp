/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.sp.ncs;

import com.vaadin.data.Validator;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import hms.kite.provisioning.Provisioning;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.provisioning.commons.util.ProvKeyBox.casTpdK;
import static hms.kite.provisioning.commons.util.ProvKeyBox.casTpsK;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class NcsCasView extends NcsConfigurationView {

    private static final Logger logger = LoggerFactory.getLogger(NcsCasView.class);

    private TextField tpsTxtFld;
    private TextField tpdTxtFld;

    private void init(Provisioning provisioning) {
        tpsTxtFld = new TextField(provisioning.getMsg("sp.ncs.cas.tps"));
        tpsTxtFld.setImmediate(true);
        tpsTxtFld.setRequired(true);
        tpsTxtFld.setRequiredError(provisioning.getMsg("sp.ncs.cas.tps.required.error"));
        tpsTxtFld.addValidator(createValidator("ncsCasTpsRegex", "sp.ncs.cas.tps.validation"));
        tpsTxtFld.addValidator(createValidator("sp.createApp.zero.validation", "sp.ncs.cas.zero.validation.error.message"));
        tpdTxtFld = new TextField(provisioning.getMsg("sp.ncs.cas.tpd"));

        tpdTxtFld.setImmediate(true);
        tpdTxtFld.setRequired(true);
        tpdTxtFld.setRequiredError(provisioning.getMsg("sp.ncs.cas.tpd.required.error"));
        tpdTxtFld.addValidator(createValidator("ncsCasTpdRegex", "sp.ncs.cas.tpd.validation"));
        tpdTxtFld.addValidator(createValidator("sp.createApp.zero.validation", "sp.ncs.cas.zero.validation.error.message"));
    }

    private void reloadData() {
        Map<String, Object> sp = provisioning.getSp();

        setValueToField(tpsTxtFld, sp.get(casTpsK));
        setValueToField(tpdTxtFld, sp.get(casTpdK));
    }

    private void checkTpsTpdValidation() {
        int moTpsValue = Integer.parseInt(tpsTxtFld.getValue().toString());
        int moTpdValue = Integer.parseInt(tpdTxtFld.getValue().toString());
        if (moTpsValue >= moTpdValue) {
            throw new Validator.InvalidValueException(provisioning.getMsg("sp.ncs.cas.mt.tps.tpd.validation.message"));
        }
    }

    @Override
    protected void changeToViewMode() {
        changeToConfirmMode();
    }

    @Override
    protected void changeToConfirmMode() {
        tpsTxtFld.setReadOnly(true);
        tpdTxtFld.setReadOnly(true);
    }

    @Override
    protected void changeToEditMode() {
        tpsTxtFld.setReadOnly(false);
        tpdTxtFld.setReadOnly(false);
    }

    @Override
    protected void changeToCancelView() {
        changeToViewMode();
    }

    @Override
    protected void setSpSessionValues() {
        Map<String, Object> sp = provisioning.getSp();

        sp.put(casTpsK, tpsTxtFld.getValue());
        sp.put(casTpdK, tpdTxtFld.getValue());
    }

    public NcsCasView(Provisioning provisioning) {
        this.provisioning = provisioning;

        init(provisioning);
    }

    @Override
    public void saveData() {
    }

    @Override
    public VerticalLayout loadComponents(final Panel panel) {
        VerticalLayout layout = new VerticalLayout();

        panel.removeAllComponents();
        panel.setCaption(provisioning.getMsg("sp.ncs.cas.title"));
        panel.addComponent(layout);

        loadCasComponents();
        reloadData();
        refreshInChangedMode();

        layout.addComponent(form);
        layout.setComponentAlignment(form, Alignment.MIDDLE_CENTER);
        layout.setSpacing(true);

        return layout;
    }

    @Override
    public boolean validateData() {
        try {
            setSpSessionValues();
            tpsTxtFld.validate();
            tpdTxtFld.validate();
            checkTpsTpdValidation();
            form.setComponentError(null);
        } catch (Validator.InvalidValueException e) {
            form.setComponentError(e);
            return false;
        } catch (Exception e) {
            logger.error("Unexpected error occurred while validating cas form", e);
            return false;
        }
        return true;
    }

    public void loadCasComponents() {
        form.addField("tps", tpsTxtFld);
        form.addField("tpd", tpdTxtFld);
    }
}
