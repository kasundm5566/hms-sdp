/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.sp.admin;

import com.vaadin.ui.*;
import com.vaadin.ui.themes.BaseTheme;
import hms.kite.provisioning.Provisioning;
import hms.kite.provisioning.commons.util.ProvKeyBox;
import hms.kite.provisioning.ui.common.SpInformationView;
import hms.kite.provisioning.ui.common.ViewMode;
import hms.kite.provisioning.ui.sp.sp.AdminNcsEditLayout;
import hms.kite.provisioning.ui.sp.sp.SpAdminEditApply;
import hms.kite.util.KiteKeyBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
@Deprecated
public class SpSearchResultTable extends Table implements AdminActionPerformedListener {

    private static final Logger logger = LoggerFactory.getLogger(SpSearchResultTable.class);

    private Provisioning provisioning;
    private AdminActionHandler adminActionHandler;
    private AdminActionPerformedListener actionPerformedListener;
    private SpSearchViewMode viewMode;

    public SpSearchResultTable(Provisioning provisioning, AdminActionPerformedListener actionPerformedListener, SpSearchViewMode viewMode) {
        this.provisioning = provisioning;
        this.viewMode = viewMode;
        adminActionHandler = new AdminActionHandler(provisioning);
        this.actionPerformedListener = actionPerformedListener;
        initializeTable();
    }

    private void initializeTable() {
        addContainerProperty(getMessage("admin.spSearchAndView.table.sp.name"), Button.class, null);
        addContainerProperty(getMessage("admin.spSearchAndView.table.sp.requestedDate"), Label.class, null);
        if (viewMode == SpSearchViewMode.ALL) {
            addContainerProperty(getMessage("admin.spSearchAndView.table.sp.status"), Label.class, null);
        }
        addContainerProperty(getMessage("admin.spSearchAndView.table.sp.ncsList"), Label.class, null);
        addContainerProperty(getMessage("admin.spSearchAndView.table.sp.actions"), HorizontalLayout.class, null);

        setStyleName("striped");
        setEditable(false);
        setWidth("100%");
        setSizeFull();
    }

    private String getMessage(String key) {
        return provisioning.getMsg(key);
    }

    private Button createSpLink(final Map<String, Object> sp) {
        Button button = new Button((String) sp.get(ProvKeyBox.coopUserNameK));
        button.setStyleName(BaseTheme.BUTTON_LINK);
        button.setImmediate(true);
        button.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                {
                    try {
                        showSpDetails(sp);
                    } catch (Exception e) {
                        logger.error("Unexpected error occurred while trying to show sp details", e);
                        //TODO show notification
                    }
                }
            }
        });
        return button;
    }

    private HorizontalLayout createActions(HashMap<String, Object> sp) {
        String spId = (String) sp.get(spIdK);
        String status = (String) sp.get(statusK);

        HorizontalLayout actionButtonLayout = new HorizontalLayout();

        createViewButton(actionButtonLayout, spId);
        createEditButton(actionButtonLayout, spId);

        if (pendingApproveK.equals(status)) {
            createRejectButton(actionButtonLayout, spId);
        } else if (approvedK.equals(status)) {
            createSuspendButton(actionButtonLayout, spId);
        } else if (suspendedK.equals(status)) {
            createEnableButton(actionButtonLayout, spId);
        }

        return actionButtonLayout;
    }

    private void createEditButton(HorizontalLayout actionButtonLayout, String spId) {
        final Button editButton = new Button(provisioning.getMsg("admin.spSearchAndView.table.action.edit"));
        actionButtonLayout.addComponent(editButton);
        editButton.setData(spId);
        editButton.setStyleName(BaseTheme.BUTTON_LINK);
        editButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    editSp(editButton.getData().toString());
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while trying to edit SP", e);
                    //TODO show error notification
                }
            }
        });
    }


    private void createEnableButton(HorizontalLayout actionButtonLayout, String spId) {
        final Button enableButton = new Button(provisioning.getMsg("admin.spSearchAndView.table.action.enable"));
        actionButtonLayout.addComponent(enableButton);
        enableButton.setData(spId);
        enableButton.setStyleName(BaseTheme.BUTTON_LINK);
        enableButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    Map<String, Object> sp = spRepositoryService().findSpById((String) enableButton.getData());
                    provisioning.addToSession(currentSpDataK, sp);
                    adminActionHandler.showEnableConfirm(SpSearchResultTable.this);
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while enabling SP", e);
                    //TODO show error notification
                }
            }
        });
    }


    private void createRejectButton(HorizontalLayout actionButtonLayout, String spId) {
        final Button rejectButton = new Button(provisioning.getMsg("admin.spSearchAndView.table.action.reject"));
        actionButtonLayout.addComponent(rejectButton);
        rejectButton.setData(spId);
        rejectButton.setStyleName(BaseTheme.BUTTON_LINK);
        rejectButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    Map<String, Object> sp = spRepositoryService().findSpById((String) rejectButton.getData());
                    provisioning.addToSession(currentSpDataK, sp);
                    adminActionHandler.showRejectConfirm(SpSearchResultTable.this);
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while rejecting SP", e);
                    //TODO show error notification
                }
            }
        });
    }

    private void createSuspendButton(HorizontalLayout actionButtonLayout, String spId) {
        final Button suspendButton = new Button(provisioning.getMsg("admin.spSearchAndView.table.action.suspend"));
        actionButtonLayout.addComponent(suspendButton);
        suspendButton.setData(spId);
        suspendButton.setStyleName(BaseTheme.BUTTON_LINK);
        suspendButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    Map<String, Object> sp = spRepositoryService().findSpById((String) suspendButton.getData());
                    provisioning.addToSession(currentSpDataK, sp);
                    adminActionHandler.showSuspendConfirmation(SpSearchResultTable.this);
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while suspending SP", e);
                    //TODO show error notification
                }
            }
        });
    }

    private void createViewButton(HorizontalLayout actionButtonLayout, String spId) {
        final Button viewButton = new Button(provisioning.getMsg("admin.spSearchAndView.table.action.view"));
        actionButtonLayout.addComponent(viewButton);
        viewButton.setData(spId);
        viewButton.setStyleName(BaseTheme.BUTTON_LINK);
        viewButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    viewSp(viewButton.getData().toString());
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while trying to view SP", e);
                    //TODO show error notification
                }
            }
        });
    }


    private void showSpDetails(Map<String, Object> sp) {
        logger.debug("Showing SP with spId = [{}]", sp.get(spIdK));
        new SpInformationView(provisioning, (String) sp.get(coopUserIdK));
    }

    private String getSelectedSpList(HashMap<String, Object> sp) {
        List selectedServicesList = (List) sp.get(KiteKeyBox.spSelectedServicesK);
        StringBuilder stringBuilder = new StringBuilder();
        for (Object obj : selectedServicesList) {
            stringBuilder.append(obj);
            stringBuilder.append(" ");
        }
        return stringBuilder.toString();
    }

    private void viewSp(String spId) {
        logger.info("Viewing SP with spId = [{}]", spId);
        Map<String, Object> sp = spRepositoryService().findSpById(spId);
        provisioning.addToSession(ProvKeyBox.currentSpDataK, sp);
        showNcsView(sp);
    }

    private void editSp(String spId) {
        logger.info("Viewing SP with spId = [{}]", spId);
        Map<String, Object> sp = spRepositoryService().findSpById(spId);
        provisioning.addToSession(ProvKeyBox.currentSpDataK, sp);
        spNcsSaveEditWindow(sp);
    }

    private void spNcsSaveEditWindow(Map<String, Object> sp) {
        SpAdminEditApply spAdminEditApply;
        spAdminEditApply = new SpAdminEditApply(provisioning, sp, adminActionHandler);
        applyStyles(spAdminEditApply);
        provisioning.getUiManager().pushScreen(AdminNcsEditLayout.class.getName(), spAdminEditApply);
    }

    private void showNcsView(Map<String, Object> sp) {
        NcsViewLayout ncsApprovalLayout;

        if (pendingApproveK.equals(sp.get(statusK))) {
            ncsApprovalLayout = new NcsViewLayout(provisioning, sp, adminActionHandler, ViewMode.EDIT);
        } else {
            ncsApprovalLayout = new NcsViewLayout(provisioning, sp, adminActionHandler);
        }

        ncsApprovalLayout.setTitle(provisioning.getMsg("sp.ncs.configuration.title"));
        ncsApprovalLayout.reloadSpNcs(sp);
        applyStyles(ncsApprovalLayout);
        provisioning.getUiManager().pushScreen(NcsViewLayout.class.getName(), ncsApprovalLayout);
        ncsApprovalLayout.loadComponents();
    }

    private void applyStyles(Layout content) {
        content.addStyleName("container-panel");
        content.setWidth("760px");
        content.setMargin(true);
    }

    @Override
    public void onActionPerformed() {
        actionPerformedListener.onActionPerformed();
    }

    public void reloadTable(List<Map<String, Object>> spList) {
        logger.debug("Reloading table ...");
        removeAllItems();
        int i = 1;
        for (Object spObject : spList) {
            HashMap<String, Object> sp = (HashMap<String, Object>) spObject;
            logger.info("received sp " + sp);
            List items = new ArrayList();
            items.add(createSpLink(sp));
            items.add(new Label((String) sp.get(spRequestDateK)));
            if (viewMode == SpSearchViewMode.ALL) {
                items.add(new Label((String) sp.get(statusK)));
            }
            items.add(new Label(getSelectedSpList(sp)));
            items.add(createActions(sp));


            addItem(items.toArray(), i++);
        }
    }
}
