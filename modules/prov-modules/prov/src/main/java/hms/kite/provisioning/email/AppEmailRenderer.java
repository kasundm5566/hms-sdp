/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.email;

import com.google.common.base.Optional;
import hms.common.registration.api.common.StatusCodes;
import hms.common.registration.api.response.BasicUserResponseMessage;
import hms.common.registration.api.util.RestApiKeys;
import hms.kite.provisioning.commons.util.ProvKeyBox;
import hms.kite.provisioning.services.httpclient.CurHttpClient;
import hms.kite.util.KiteErrorBox;
import hms.kite.util.SdpException;
import org.antlr.stringtemplate.StringTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class AppEmailRenderer extends AbstractEmailRenderer {

    @Resource
    private CurHttpClient curHttpClient;

    private static final Logger logger = LoggerFactory.getLogger(AppEmailRenderer.class);

    @Override
    public void render(Map<String, Object> email, Map<String, Object> data) {

        StringTemplate emailBodyTemplate;
        StringTemplate emailSubjectTemplate;

        String template = (String) email.get(emailBodyK);
        String subjectTemplate = (String) email.get(emailSubjectK);

        HashMap<String, Object> app = (HashMap<String, Object>) data.get(appK);
        HashMap<String, Object> sp = (HashMap<String, Object>) data.get(spK);

        if (sp == null) sp = new HashMap<String, Object>();

        emailBodyTemplate = new StringTemplate(template);
        emailSubjectTemplate = new StringTemplate(subjectTemplate);


        final Optional provisioningAdditionalData = Optional.fromNullable(data.get(provEventAdditionalDataK));
        final Optional additionalContent = Optional.fromNullable(email.get(emailAdditionalContentK));
        
        if(provisioningAdditionalData.isPresent() && additionalContent.isPresent())  {            
            logger.debug("Provisioning additional data received = [{}].", provisioningAdditionalData.get());
            logger.debug("Email additional content available = [{}].", additionalContent.get());

            try {
                emailBodyTemplate.setAttribute(emailAdditionalDataK, setAdditionalData((Map<String, Object>) provisioningAdditionalData.get(), (Map<String, Object>) additionalContent.get()));
            } catch (Exception e) {
                logger.error("Error occurred while constructing the additional content to the email [{}].", e);
                // continue
            }
        }

        extractAndFillDataFormCur(email, app, sp);

        emailBodyTemplate.setAttribute(spK, sp);
//        emailBodyTemplate.setAttribute(extraK, extra);
        emailBodyTemplate.setAttribute(systemK, getANewSystemDataMap());
        emailBodyTemplate.setAttribute(appK, app);

        emailBodyTemplate.setAttribute(ProvKeyBox.dataK, data);

        emailSubjectTemplate.setAttribute(appK, app);
        emailSubjectTemplate.setAttribute(systemK, getANewSystemDataMap());
        emailSubjectTemplate.setAttribute(spK, sp);

        addIfNotNull(emailBodyTemplate, data, "");
        email.put(emailBodyK, emailBodyTemplate.toString());
        email.put(emailSubjectK, emailSubjectTemplate.toString());

        logger.info("Rendered Email, Subject = [{}], Email Body: = [{}]", emailSubjectTemplate, emailBodyTemplate);
    }

    private String setAdditionalData(Map<String, Object> provEventAdditionalData, Map<String, Object> additionalContent) {

        StringBuilder stringBuilder = new StringBuilder();

        logger.debug("Additional data place eligible for email content = [{}], and available from provisioning event = [{}].");

        final Optional inAppApiKey = Optional.fromNullable(provEventAdditionalData.get(inAppApiKeyK)) ;
        final Optional inAppApiContent = Optional.fromNullable(additionalContent.get(emailAttributeInAppApiKeyK));
        if(inAppApiKey.isPresent() && inAppApiContent.isPresent()) {

            stringBuilder.append(MessageFormat.format((String)inAppApiContent.get(), inAppApiKey.get()));
        }

        return stringBuilder.toString();
    }

    private void addIfNotNull(StringTemplate emailBodyTemplate, Map<String, Object> data, String key) {
        if (data.get(key) != null) {
            emailBodyTemplate.setAttribute(key, data.get(key));
        }
    }

    private void extractAndFillDataFormCur(Map<String, Object> email, HashMap<String, Object> app,
                                           HashMap<String, Object> sp) {

        String createdUserName = (String) app.get(createdByK);

        BasicUserResponseMessage responseMessage = curHttpClient.get(createdUserName);

        logger.debug("Received basic user response [{}]", responseMessage.getAdditionalDataMap());

        if (responseMessage.getStatusCode() != StatusCodes.SUCCESS) {
            throw new SdpException(KiteErrorBox.emailNotificationSendErrorCode,
                    "Failure response received from cur for user [" + app.get(createdByK)
                            + "]. Status Code from CUR is [" + responseMessage.getStatusCode() + " ].");
        }

        List<String> toAddressList = (ArrayList<String>) email.get(toAddressListK);

        String creatorEmailAddress = responseMessage.getAdditionalData(RestApiKeys.EMAIL);
        String corpEmailAddress = responseMessage.getAdditionalData(RestApiKeys.CORPORATE_USER_EMAIL);

        if (creatorEmailAddress == null) {
            logger.error("Cannot find email address for creator [{}]", app.get(createdByK));
        } else if (creatorEmailAddress.equals(corpEmailAddress)) {
            toAddressList.add(creatorEmailAddress);
        } else if (corpEmailAddress == null) {
            logger.error("Cannot find corporate email address for creator [{}]", app.get(createdByK));
        } else {
            toAddressList.add(creatorEmailAddress);
            ((List<String>) email.get(ccAddressListK)).add(corpEmailAddress);
        }

//        sp.put(firstNameK, responseMessage.getAdditionalData(RestApiKeys.FIRST_NAME));
        sp.put(firstNameK, sp.get(coopUserNameK));
    }

}
