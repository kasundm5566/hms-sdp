/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.sp;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import hms.kite.provisioning.Provisioning;
import hms.kite.provisioning.commons.ui.Action;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.component.table.DataManagementPanel;
import hms.kite.provisioning.commons.ui.component.table.DataTable;
import hms.kite.provisioning.ui.common.ViewMode;
import hms.kite.provisioning.ui.sp.admin.AdminActionHandler;
import hms.kite.provisioning.ui.sp.admin.AdminActionPerformedListener;
import hms.kite.provisioning.ui.sp.admin.NcsViewLayout;
import hms.kite.provisioning.ui.sp.sp.SpAdminEditApply;
import hms.kite.util.KiteKeyBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.provisioning.ui.common.ComponentFactory.createSpDetailsLink;
import static hms.kite.provisioning.ui.sp.SpDetailsTable.SpAction.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class SpDetailsTable extends DataTable implements AdminActionPerformedListener {

    private static final Logger logger = LoggerFactory.getLogger(SpDetailsTable.class);

    private static final String ROLE_PROV_SP_ACTION = "ROLE_PROV_SP_ACTION_";

    private final AdminActionHandler adminActionHandler;

    private boolean statusEnabled = true;

    private Object addSpLink(final Map<String, Object> sp) {
        return createSpDetailsLink(application, sp.get(createdByK).toString(), sp.get(coopUserIdK).toString());
    }

    private String getSelectedNcsList(Map<String, Object> sp) {
        List selectedNcsList = (List) sp.get(KiteKeyBox.spSelectedServicesK);
        StringBuilder stringBuilder = new StringBuilder();
        for (Object ncs : selectedNcsList) {
            stringBuilder.append(ncs);
            stringBuilder.append(" ");
        }
        return stringBuilder.toString();
    }

    private void applyStyles(Layout content) {
        content.addStyleName("container-panel");
        content.setWidth("760px");
        content.setMargin(true);
    }

    private void setActionStatus(String status, String spName) {
        if (approvedK.equals(status)) {
            setStatusMessage(application.getMsg("manage.sp.approve.sp.success.message", spName),
                    "sp-request-success-notification");
        } else if (suspendedK.equals(status)) {
            setStatusMessage(application.getMsg("manage.sp.suspend.sp.success.message", spName),
                    "sp-request-success-notification");
        } else if (rejectK.equals(status)) {
            setStatusMessage(application.getMsg("manage.sp.reject.sp.success.message", spName),
                    "sp-request-success-notification");
        } else if (("failNotification").equals(status)) {
            setStatusMessage(application.getMsg("manage.sp.fail.to.manage.message", spName),
                    "sp-request-fail-notification");
        }
    }

    private void setSessionDetails(Map<String, Object> sp) {
        application.addToSession(currentSpDataK, sp);
        application.addToSession(spActionStatusK, "");
    }

    private HorizontalLayout createActionButtons(Map<String, Object> sp) {
        HorizontalLayout buttonLayout = new HorizontalLayout();

        buttonLayout.addComponent(createLink(VIEW, application.getMsg("admin.spSearchAndView.table.action.view"), sp));

        addActionButton(EDIT, application.getMsg("admin.spSearchAndView.table.action.edit"), sp, buttonLayout);

        if (approvedK.equals(sp.get(statusK))) {
            addActionButton(SUSPEND, application.getMsg("admin.spSearchAndView.table.action.suspend"), sp, buttonLayout);
        } else if (pendingApproveK.equals(sp.get(statusK))) {
            addActionButton(APPROVE, application.getMsg("admin.spSearchAndView.table.action.approve"), sp, buttonLayout);
            addActionButton(REJECT, application.getMsg("admin.spSearchAndView.table.action.reject"), sp, buttonLayout);
        } else if (suspendedK.equals(sp.get(statusK))) {
            addActionButton(ENABLE, application.getMsg("admin.spSearchAndView.table.action.enable"), sp, buttonLayout);
        }

        return buttonLayout;
    }

    private void editSp(Map<String, Object> sp) {
        setSessionDetails(sp);

        SpAdminEditApply spAdminEditApply = new SpAdminEditApply((Provisioning) application, sp, adminActionHandler);
        applyStyles(spAdminEditApply);

        application.getUiManager().pushScreen(SpAdminEditApply.class.getName(), spAdminEditApply);
    }

    private void enableSp(final Map<String, Object> sp) {
        setSessionDetails(sp);

        adminActionHandler.showEnableConfirm(this);
    }

    private void rejectSp(final Map<String, Object> sp) {
        setSessionDetails(sp);

        adminActionHandler.showRejectConfirm(this);
    }

    private void approveSp(final Map<String, Object> sp) {
        setSessionDetails(sp);

        adminActionHandler.showApproveConfirm(this);
    }

    private void suspendSp(final Map<String, Object> sp) {
        setSessionDetails(sp);

        adminActionHandler.showSuspendConfirmation(this);
    }

    private void viewSp(Map<String, Object> sp) {
        setSessionDetails(sp);

        NcsViewLayout ncsApprovalLayout;

        if (pendingApproveK.equals(sp.get(statusK))) {
            ncsApprovalLayout = new NcsViewLayout((Provisioning) application, sp, adminActionHandler, ViewMode.EDIT);
        } else {
            ncsApprovalLayout = new NcsViewLayout((Provisioning) application, sp, adminActionHandler);
        }

        ncsApprovalLayout.setTitle(application.getMsg("sp.ncs.configuration.title"));
        ncsApprovalLayout.reloadSpNcs(sp);
        applyStyles(ncsApprovalLayout);
        ncsApprovalLayout.loadComponents();
        application.getUiManager().pushScreen(NcsViewLayout.class.getName(), ncsApprovalLayout);
    }

    protected static enum SpAction implements Action {
        VIEW, EDIT, SUSPEND, APPROVE, REJECT, ENABLE
    }

    @Override
    protected void initialize() {
        addContainerProperty(application.getMsg("admin.spSearchAndView.table.sp.name"), Button.class, null);
        setColumnWidth(application.getMsg("admin.spSearchAndView.table.sp.name"), 140);
        addContainerProperty(application.getMsg("admin.spSearchAndView.table.sp.requestedDate"), String.class, null);
        setColumnWidth(application.getMsg("admin.spSearchAndView.table.sp.requestedDate"), 100);
        if (statusEnabled) {
            addContainerProperty(application.getMsg("admin.spSearchAndView.table.sp.status"), String.class, null);
            setColumnWidth(application.getMsg("admin.spSearchAndView.table.sp.status"), 100);
        }
        addContainerProperty(application.getMsg("admin.spSearchAndView.table.sp.ncsList"), String.class, null);
        setColumnWidth(application.getMsg("admin.spSearchAndView.table.sp.ncsList"), 180);
        addContainerProperty(application.getMsg("admin.spSearchAndView.table.sp.actions"), HorizontalLayout.class, null);
    }

    @Override
    protected void processAction(Map<String, Object> actionData) {
        SpAction action = (SpAction) actionData.get(actionK);
        Map<String, Object> sp = (Map<String, Object>) actionData.get(dataK);

        switch (action) {
            case VIEW:
                viewSp(sp);
                break;
            case SUSPEND:
                suspendSp(sp);
                break;
            case APPROVE:
                approveSp(sp);
                break;
            case REJECT:
                rejectSp(sp);
                break;
            case ENABLE:
                enableSp(sp);
                break;
            case EDIT:
                editSp(sp);
                break;
            default:
                logger.error("Unknown SpAction [{}], Can't handle ", action);
        }
    }

    public SpDetailsTable(BaseApplication application, DataManagementPanel dataManagementPanel) {
        super(application, dataManagementPanel, ROLE_PROV_SP_ACTION);

        this.adminActionHandler = new AdminActionHandler((Provisioning) application);
    }

    @Override
    public void onActionPerformed() {
        dataManagementPanel.search();

        Map sp = (Map) application.getFromSession(currentSpDataK);
        setActionStatus(application.getFromSession(spActionStatusK).toString(), sp.get(coopUserNameK).toString());
    }

    public void reloadTable(List<Map<String, Object>> records) {
        super.reloadTable(records);

        int rowNumber = 1;

        for (Map<String, Object> record : records) {
            List rowData = new ArrayList();
            rowData.add(addSpLink(record));
            rowData.add(record.get(spRequestDateK));
            if (statusEnabled) {
                rowData.add(record.get(statusK));
            }
            rowData.add(getSelectedNcsList(record));
            rowData.add(createActionButtons(record));
            addItem(rowData.toArray(), rowNumber++);
        }
    }

    public void setStatusEnabled(boolean statusEnabled) {
        this.statusEnabled = statusEnabled;
    }
}
