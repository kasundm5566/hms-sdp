/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.email;

import hms.kite.util.SdpException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import sun.misc.BASE64Encoder;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteErrorBox.emailNotificationSendErrorCode;
import static hms.kite.util.KiteErrorBox.successCode;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SendEmailClient implements EmailClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(SendEmailClient.class);
    private static final Logger SNMPLOGGER = LoggerFactory.getLogger("snmp_logger");
    private static final Logger EMAILLOGGER = LoggerFactory.getLogger("email_logger");

    @Resource
    private MailSender mailSender;
    private String emailSendFail;
    private String emailSendSuccess;
    private boolean trapSend = false;

    @Override
    public void sendEmail(Map<String, Object> email) {

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        List<String> ccAddressList = (List<String>) email.get(ccAddressListK);
        List<String> toAddressList = (List<String>) email.get(toAddressListK);
        List<String> bccAddressList = (List<String>) email.get(bccAddressListK);

        try {
            mailMessage.setFrom((String) email.get(fromAddressK));
            mailMessage.setReplyTo((String) email.get(fromAddressK));
            if (email.get(ccAddressListK) != null && ccAddressList.size() != 0) {
                mailMessage.setCc(ccAddressList.toArray(new String[ccAddressList.size()]));
            }
            if (email.get(bccAddressListK) != null && bccAddressList.size() != 0) {
                mailMessage.setBcc(bccAddressList.toArray(new String[bccAddressList.size()]));
            }
            mailMessage.setTo(toAddressList.toArray(new String[toAddressList.size()]));
            mailMessage.setSentDate(new Date(System.currentTimeMillis()));
            mailMessage.setSubject((String) email.get(emailSubjectK));
            mailMessage.setText((String) email.get(emailBodyK));
            LOGGER.info("Email is preparing to be sent: [ {} ]", mailMessage.toString());

            sendMail(mailMessage);

            LOGGER.info("Email is successfully sent: {}", successCode);

        } catch (Throwable ex) {
            LOGGER.error("Email sending is failed: {}", ex);
            throw new SdpException(emailNotificationSendErrorCode, "Email cannot be sent", ex);
        }

    }

    private void sendMail(SimpleMailMessage mailMessage) throws MailException {
        try {
            mailSender.send(mailMessage);
            if (trapSend) {
                SNMPLOGGER.info(emailSendSuccess);
                trapSend = false;
            }
        } catch (MailException e) {
            EMAILLOGGER.error("from:" + mailMessage.getFrom()
                    + "\nto:" + StringUtils.join(mailMessage.getTo(), ",")
                    + "\ncc:" + StringUtils.join(mailMessage.getCc(), ",")
                    + "\nbcc:" + StringUtils.join(mailMessage.getBcc(), ",")
                    + "\nsubject:" + mailMessage.getSubject()
                    + "\ncontent:" + new BASE64Encoder().encode(mailMessage.getText().getBytes())
                    + "\n");

            if (!trapSend) {
                SNMPLOGGER.info(emailSendFail);
                trapSend = true;
            }
            throw e;
        }
    }

    public void setMailSender(MailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void setEmailSendFail(String emailSendFail) {
        this.emailSendFail = emailSendFail;
    }

    public void setEmailSendSuccess(String emailSendSuccess) {
        this.emailSendSuccess = emailSendSuccess;
    }
}