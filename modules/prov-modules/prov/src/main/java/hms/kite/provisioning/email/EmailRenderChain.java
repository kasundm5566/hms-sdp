/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.email;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class EmailRenderChain implements EmailRender {

    private List<EmailRender> renders;

    private static final Logger logger = LoggerFactory.getLogger(EmailRenderChain.class);

    @Override
    public void render(Map<String, Object> email, Map<String, Object> data) {
        logger.info("Email Render Chain is being started for - Email: {} Data: {}", email, data);
        for (EmailRender render : renders) {
            render.render(email, data);
        }
    }

    public void setRenders(List<EmailRender> renders) {
        this.renders = renders;
    }

}
