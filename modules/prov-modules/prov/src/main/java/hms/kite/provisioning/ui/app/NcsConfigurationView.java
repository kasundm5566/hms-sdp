/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.app;

import com.vaadin.ui.Button;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.BaseTheme;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.LiferayUtil;
import hms.kite.provisioning.commons.ui.event.PortletEventSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class NcsConfigurationView extends VerticalLayout {

    private static final Logger logger = LoggerFactory.getLogger(NcsConfigurationView.class);

    private BaseApplication application;
    private Table requestTable;
    private Map<String, Object> app;

    public NcsConfigurationView(BaseApplication application, Map<String, Object> app) {
        this.application = application;
        this.app = app;
        loadComponents();
        addStyleName("app-ncs-resoures-view-layout");
        setWidth("512px");
    }

    private void loadComponents() {
        requestTable = new Table();
        requestTable.addStyleName("striped");
        addComponent(createTable());
    }

    private Table createTable() {
        requestTable.setImmediate(true);
        requestTable.addContainerProperty(application.getMsg("sp.viewApp.viewNcsTableNcsType"), String.class, null);
        requestTable.addContainerProperty(application.getMsg("sp.viewApp.viewNcsTableOperator"), String.class, null);
        requestTable.addContainerProperty(application.getMsg("sp.viewApp.viewNcsTableStatus"), String.class, null);
        requestTable.addContainerProperty(application.getMsg("sp.viewApp.viewNcsTableAction"), Button.class, null);
        requestTable.setColumnWidth(application.getMsg("sp.viewApp.viewNcsTableNcsType"), 100);
        requestTable.setColumnWidth(application.getMsg("sp.viewApp.viewNcsTableOperator"), 110);
        requestTable.setColumnWidth(application.getMsg("sp.viewApp.viewNcsTableStatus"), 100);
        displayRecord();
        requestTable.setWidth("100%");
        requestTable.setHeight("200px");
        requestTable.setSelectable(true);
        return requestTable;
    }

    public void displayRecord() {
        List<Map<String, Object>> selectedNcsesList = (List) app.get(ncsesK);

        int rowNumber = 1;

        for (Map<String, Object> selectedNcsMap : selectedNcsesList) {
            String ncsType = (String) selectedNcsMap.get(ncsTypeK);
            String operator = (String) selectedNcsMap.get(operatorK);
            String status = (String) selectedNcsMap.get(statusK);

            if (ncsConfiguredK.equals(status)) {
                requestTable.addItem(new Object[]{ncsType, operator, status, createViewLink(ncsType, operator)}, rowNumber);
            } else {
                requestTable.addItem(new Object[]{ncsType, operator, status, null}, rowNumber);
            }

            rowNumber++;
        }
    }

    private Button createViewLink(final String ncsType, final String operator) {
        final Button button = new Button(application.getMsg("sp.viewApp.viewNcsTable.viewLinkButton"));
        button.setStyleName(BaseTheme.BUTTON_LINK);
        button.setImmediate(true);
        button.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                try {
                    Map<String, Object> valueMap = new HashMap<String, Object>();
                    valueMap.put(ncsTypeK, ncsType);
                    valueMap.put(operatorK, operator);
                    valueMap.put(appIdK, app.get(appIdK));
                    valueMap.put(appStatusK, (app.get(statusK) == null) ? initialK : app.get(statusK));
                    valueMap.put(spIdK, app.get(spIdK));
                    valueMap.put(coopUserIdK, spRepositoryService().findSpBySpId((String) app.get(spIdK)).get(coopUserIdK));
                    valueMap.put(portletIdK, LiferayUtil.createPortletId(ncsType, operator));
                    valueMap.put(ncsPortletTitle, application.getMsg("sp.createApp.createApplicationTitle"));
                    valueMap.put(ncsesK, app.get(ncsesK));
                    PortletEventSender.send(valueMap, provViewNcsEvent, application);
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while trying to view a ncs", e);
                }
            }
        }
        );
        return button;
    }
}