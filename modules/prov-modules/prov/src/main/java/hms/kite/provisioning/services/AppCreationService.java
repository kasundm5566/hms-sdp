/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.services;

import hms.kite.provisioning.ui.common.AppException;

import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public interface AppCreationService {

    Map<String, Object> createApp(String spid);

//    Boolean isAppNameExisting (String appName);

    void submit(Map<String, Object> app) throws AppException;

    void update(Map<String, Object> app) throws AppException;



}
