/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.app;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.component.table.DataManagementPanel;
import hms.kite.provisioning.ui.common.ConfirmationDialog;
import hms.kite.provisioning.ui.common.ConfirmationListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.appRepositoryService;
import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.provisioning.ui.app.AppDetailsTable.ApplicationAction.*;
import static hms.kite.provisioning.ui.common.ComponentFactory.createSpDetailsLink;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class AdminAppDetailsTable extends AppDetailsTable {

    private static final Logger logger = LoggerFactory.getLogger(AdminAppDetailsTable.class);

    private Object addSpLink(final Map<String, Object> app) {
        String corporateUserId = (String) spRepositoryService().findSpBySpId((String) app.get(spIdK)).get(coopUserIdK);
        return createSpDetailsLink(application, (String) app.get(createdByK), corporateUserId);
    }

    private HorizontalLayout createActionButtons(Map<String, Object> app) {
        HorizontalLayout buttonLayout = new HorizontalLayout();

        buttonLayout.addComponent(createLink(VIEW, application.getMsg("admin.manageApp.approveAppRequestTable.viewButton"), app));

        if (draftK.equals(app.get(statusK))) {
            addActionButton(EDIT, application.getMsg("admin.manageApp.approveAppRequestTable.editButton"), app, buttonLayout);
            addActionButton(DELETE, application.getMsg("admin.manageApp.approveAppRequestTable.deleteButton"), app, buttonLayout);
        } else if (limitedProductionK.equals(app.get(statusK))) {
            addActionButton(EDIT, application.getMsg("admin.manageApp.approveAppRequestTable.editButton"), app, buttonLayout);
            addActionButton(APPROVE, application.getMsg("admin.manageApp.approveAppRequestTable.approveButton"), app, buttonLayout);
            addActionButton(SUSPEND, application.getMsg("admin.manageApp.approveAppRequestTable.suspendButton"), app, buttonLayout);
        } else if (pendingApproveForActiveProductionK.equals(app.get(statusK))) {
            addActionButton(EDIT, application.getMsg("admin.manageApp.approveAppRequestTable.editButton"), app, buttonLayout);
            addActionButton(APPROVE, application.getMsg("admin.manageApp.approveAppRequestTable.approveButton"), app, buttonLayout);
            addActionButton(SUSPEND, application.getMsg("admin.manageApp.approveAppRequestTable.suspendButton"), app, buttonLayout);
        } else if (activeProductionK.equals(app.get(statusK))) {
            addActionButton(EDIT, application.getMsg("admin.manageApp.approveAppRequestTable.editButton"), app, buttonLayout);
            addActionButton(SUSPEND, application.getMsg("admin.manageApp.approveAppRequestTable.suspendButton"), app, buttonLayout);
        } else if (rejectK.equals(app.get(statusK))) {
            addActionButton(EDIT, application.getMsg("admin.manageApp.approveAppRequestTable.editButton"), app, buttonLayout);
            addActionButton(DELETE, application.getMsg("admin.manageApp.approveAppRequestTable.deleteButton"), app, buttonLayout);
        } else if (pendingApproveK.equals(app.get(statusK))) {
            addActionButton(EDIT, application.getMsg("admin.manageApp.approveAppRequestTable.editButton"), app, buttonLayout);
            addActionButton(APPROVE, application.getMsg("admin.manageApp.approveAppRequestTable.approveButton"), app, buttonLayout);
            addActionButton(REJECT, application.getMsg("admin.manageApp.approveAppRequestTable.rejectButton"), app, buttonLayout);
        } else if (suspendK.equals(app.get(statusK))) {
            addActionButton(EDIT, application.getMsg("admin.manageApp.approveAppRequestTable.editButton"), app, buttonLayout);
            addActionButton(RESTORE, application.getMsg("admin.manageApp.approveAppRequestTable.restoreButton"), app, buttonLayout);
            addActionButton(TERMINATE, application.getMsg("admin.manageApp.approveAppRequestTable.terminateButton"), app, buttonLayout);
        }

        return buttonLayout;
    }

    private void editApp(Map<String, Object> app) {
        AppPanel appPanel = AppPanel.newEditAppPanel(app, application);
        application.getUiManager().pushScreen(AppPanel.class.getName(), appPanel);
    }

    private void terminateApp(final Map<String, Object> app) {
        AppPanel appPanel = AppPanel.newViewAppPanel(app, application);
        application.getUiManager().pushScreen(AppPanel.class.getName(), appPanel);
    }

    private void restoreApp(final Map<String, Object> app) {
        AppPanel appPanel = AppPanel.newViewAppPanel(app, application);
        application.getUiManager().pushScreen(AppPanel.class.getName(), appPanel);
    }

    private void rejectApp(final Map<String, Object> app) {
        AppPanel appPanel = AppPanel.newViewAppPanel(app, application);
        application.getUiManager().pushScreen(AppPanel.class.getName(), appPanel);
    }

    private void approve(Map<String, Object> app) {
        AppPanel appPanel = AppPanel.newViewAppPanel(app, application);
        application.getUiManager().pushScreen(AppPanel.class.getName(), appPanel);
    }

    private void suspendApp(final Map<String, Object> app) {
        AppPanel appPanel = AppPanel.newViewAppPanel(app, application);
        application.getUiManager().pushScreen(AppPanel.class.getName(), appPanel);
    }

    private void viewApp(Map<String, Object> app) {
        AppPanel appPanel = AppPanel.newViewAppPanel(app, application);
        application.getUiManager().pushScreen(AppPanel.class.getName(), appPanel);
    }

    private void deleteApp(final Map<String, Object> app) {
        ConfirmationDialog confirmationDialog = new ConfirmationDialog(application,
                application.getMsg("admin.manageApp.deleteConfirmation.title"),
                application.getMsg("admin.manageApp.deleteConfirmation.message", (String) app.get(nameK)),
                new ConfirmationListener() {

                    @Override
                    public void onYes(String note) {
                        try {
                            sendEventToNcsPortlet(app, provSaveNcsEvent, deleteK);
                            appRepositoryService().delete((app.get(appIdK)).toString());
                            reloadAppList();
                            logger.info("Application [{}] deleted", app.get(appIdK));
                        } catch (Exception e) {
                            logger.error("Unexpected error occurred while deleting an application ", e);
                        }
                    }
                });
        confirmationDialog.setNoteRequired(false);
        confirmationDialog.showDialog();
    }

    @Override
    protected void initialize() {
        addContainerProperty(application.getMsg("admin.manageApp.Table.appName"), String.class, null);
        setColumnWidth(application.getMsg("admin.manageApp.Table.appName"), 120);
        addContainerProperty(application.getMsg("admin.manageApp.Table.appType"), String.class, null);
        setColumnWidth(application.getMsg("admin.manageApp.Table.appType"), 70);
        addContainerProperty(application.getMsg("admin.manageApp.Table.requestedDate"), String.class, null);
        setColumnWidth(application.getMsg("admin.manageApp.Table.requestedDate"), 110);
        addContainerProperty(application.getMsg("admin.manageApp.Table.serviceProvider"), Button.class, null);
        setColumnWidth(application.getMsg("admin.manageApp.Table.serviceProvider"), 120);
        addContainerProperty(application.getMsg("admin.manageApp.Table.status"), String.class, null);
        setColumnWidth(application.getMsg("admin.manageApp.Table.status"), 100);
        addContainerProperty(application.getMsg("admin.manageApp.Table.action"), HorizontalLayout.class, null);
    }

    @Override
    protected void processAction(Map<String, Object> actionData) {
        ApplicationAction action = (ApplicationAction) actionData.get(actionK);
        Map<String, Object> app = (Map<String, Object>) actionData.get(dataK);

        switch (action) {
            case DELETE:
                deleteApp(app);
                break;
            case VIEW:
                viewApp(app);
                break;
            case SUSPEND:
                suspendApp(app);
                break;
            case APPROVE:
                approve(app);
                break;
            case REJECT:
                rejectApp(app);
                break;
            case RESTORE:
                restoreApp(app);
                break;
            case TERMINATE:
                terminateApp(app);
                break;
            case EDIT:
                editApp(app);
                break;
            default:
                logger.error("Unknown ApplicationAction {}, Can't handle ", action);
        }
    }

    public AdminAppDetailsTable(BaseApplication application, DataManagementPanel dataManagementPanel) {
        super(application, dataManagementPanel);
    }

    public void reloadTable(List<Map<String, Object>> records) {
        super.reloadTable(records);

        SimpleDateFormat sdf = new SimpleDateFormat("yy/MM/dd - HH:mm", application.getLocale());

        int rowNumber = 1;

        for (Map<String, Object> record : records) {
            List rowData = new ArrayList();
            rowData.add(record.get(nameK));
            rowData.add(new Label(getApplicationType(record)));
            rowData.add(sdf.format(record.get(appRequestDateK)));
            rowData.add(addSpLink(record));
            rowData.add(record.get(statusK));
            rowData.add(createActionButtons(record));
            addItem(rowData.toArray(), rowNumber++);
        }
    }
}
