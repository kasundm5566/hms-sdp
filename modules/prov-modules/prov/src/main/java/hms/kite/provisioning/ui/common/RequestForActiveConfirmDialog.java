package hms.kite.provisioning.ui.common;
/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

import com.vaadin.data.Validator;
import com.vaadin.ui.*;
import hms.kite.provisioning.commons.ui.BaseApplication;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class RequestForActiveConfirmDialog {

   private BaseApplication application;
    private String header;
    private String message;
    private ConfirmationListener listener;
    private boolean dateRequired = true;

    public RequestForActiveConfirmDialog(BaseApplication application, String header, String message, ConfirmationListener listener) {
        this.application = application;
        this.header = header;
        this.message = message;
        this.listener = listener;
    }

     public void showDialog() {

        final Window subWindow = new Window();
        subWindow.setCaption(header);
        subWindow.setModal(true);
        subWindow.setWidth("325px");

        VerticalLayout layout = (VerticalLayout) subWindow.getContent();
        layout.setSpacing(true);
        layout.setMargin(true);

        Label messageLbl = new Label(message);
        layout.addComponent(messageLbl);

        Label startDateLbl = new Label(application.getMsg("provisioning.confirm.dialog.startDate"));
        layout.addComponent(startDateLbl);

        final DateField startDate = new DateField();
        startDate.setDateFormat("yyyy-MM-dd hh:mm a");

        if (dateRequired) {

            startDate.setRequired(true);
            startDate.setRequiredError(application.getMsg("provisioning.confirm.dialog.startDate.required"));
            layout.addComponent(startDate);
        }

        HorizontalLayout buttonBar = new HorizontalLayout();
        buttonBar.setSpacing(true);
        layout.addComponent(buttonBar);
        layout.setComponentAlignment(buttonBar, Alignment.MIDDLE_CENTER);

        Button yesButton = new Button(application.getMsg("provisioning.confirm.dialog.yes"));
        buttonBar.addComponent(yesButton);
        yesButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    startDate.validate();
                    String note = startDate.getValue().toString();
                    subWindow.getParent().removeWindow(subWindow);
                    listener.onYes(note);
                } catch (Validator.InvalidValueException e) {
                    //Nothing to do
                }
            }
        });

        Button noButton = new Button(application.getMsg("provisioning.confirm.dialog.no"));
        buttonBar.addComponent(noButton);
        noButton.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                subWindow.getParent().removeWindow(subWindow);
            }
        });

        application.getMainWindow().addWindow(subWindow);
    }

    public void setDateRequired(boolean dateRequired) {
        this.dateRequired = dateRequired;
    }








}
