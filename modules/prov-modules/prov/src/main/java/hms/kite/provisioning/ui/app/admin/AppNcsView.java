/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/

package hms.kite.provisioning.ui.app.admin;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import hms.kite.provisioning.Provisioning;

import java.util.Map;

/*
* $LastChangedDate$
* $LastChangedBy$
* $LastChangedRevision$
*/

public class AppNcsView extends VerticalLayout {


    public AppNcsView(Provisioning provisioning, Map<String, Object> app) {
        Panel panel = new Panel(provisioning.getMsg("admin.manageApp.view.appNcsDetails"));
        addComponent(panel);
        setComponentAlignment(panel, Alignment.MIDDLE_CENTER);
        final AppNcsEditor editor = new AppNcsEditor(provisioning, app);
        panel.addComponent(editor.loadComponents());

    }


}
