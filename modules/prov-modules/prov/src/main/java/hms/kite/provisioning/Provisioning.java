/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning;


import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.vaadin.terminal.gwt.server.PortletApplicationContext2;
import com.vaadin.ui.Window;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.UiManagerImpl;
import hms.kite.provisioning.ui.app.AppManagementPanel;
import hms.kite.provisioning.util.SpHomePage;
import hms.kite.util.SdpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.portlet.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.util.KiteErrorBox.spNotAvailableErrorCode;
import static javax.portlet.PortletSession.APPLICATION_SCOPE;

/**
 * Main Application class for sp and admin.
 * <p/>
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class Provisioning extends BaseApplication {

    private static final Logger logger = LoggerFactory.getLogger(Provisioning.class);

    private ProvisioningEventHandler provisioningEventHandler;

    private Set<String> userRoles;
    private String coopUserId;
    private String coopUserName;
    private String kiteUserName;
    private String userType;

    private void showHomeScreen(AppManagementPanel appManagementPanel) {
        appManagementPanel.setCaption(getMsg("sp.viewApp.appManagementTitle"));
        appManagementPanel.setPendingAppOnly(false);
        appManagementPanel.loadComponents();
        appManagementPanel.addStyleName("container-panel");
        appManagementPanel.setWidth("760px");
        appManagementPanel.setMargin(true);

        this.getUiManager().clearAll();
        this.getUiManager().pushScreen(AppManagementPanel.class.getName(), appManagementPanel);
    }

    void showSpHomeScreen() {
        AppManagementPanel appManagementPanel = new AppManagementPanel(this, spUserTypeK);
        appManagementPanel.setSpId((String) this.getFromSession(spIdK));
        showHomeScreen(appManagementPanel);
    }

    private void showAdminHomeScreen() {
        AppManagementPanel appManagementPanel = new AppManagementPanel(this, adminUserTypeK);
        showHomeScreen(appManagementPanel);
    }

    private void loadUserDetails(RenderRequest portletRequest) throws PortalException, SystemException {
        PortletSession portletSession = portletRequest.getPortletSession();
        userType = (String) portletSession.getAttribute("kite-user-type", APPLICATION_SCOPE);
        userRoles = (Set<String>) portletSession.getAttribute("kite-user-roles", APPLICATION_SCOPE);
        coopUserId = (String) portletSession.getAttribute("coop-user-id", APPLICATION_SCOPE);
        coopUserName = (String) portletSession.getAttribute("coop-user-name", APPLICATION_SCOPE);
        kiteUserName = (String) portletSession.getAttribute("kite-user-name", APPLICATION_SCOPE);

        addToSession(coopUserIdK, portletSession.getAttribute("coop-user-id", APPLICATION_SCOPE));
        addToSession(kiteUserRolesK, portletSession.getAttribute("kite-user-roles", APPLICATION_SCOPE));
        addToSession(userTypeK, userType);

        if (spUserTypeK.equals(userType)) {
            try {
                Map<String, Object> sp = spRepositoryService().findSpByCoopUserId(coopUserId);
                addToSession(spIdK, sp.get(idK));
            } catch (SdpException e) {
                addToSession(spIdK, "");
            }
        }
    }

    public Provisioning() {
        provisioningEventHandler = new ProvisioningEventHandler(this);
    }

    @Override
    public void init() {
        setPortletContext((PortletApplicationContext2) getContext());
        getPortletContext().addPortletListener(this, this);
        setResourceBundle("US");
        setMainWindow(new Window(getMsg("provisioning.app.title")));
        setUiManager(new UiManagerImpl(this));
        addToSession(currentSpDataK, new HashMap<String, Object>());
    }

    public void addHomeView() {
        if (userType == null) {
            return;
        }

        if (userType.equals(spUserTypeK)) {
            try {
                Map<String, Object> sp = spRepositoryService().findSpByCoopUserId(getCoopUserId());
                String status = (String) sp.get(statusK);
                if (!rejectK.equals(status) && !pendingApproveK.equals(status) && !suspendedK.equals(status)) {
                    showSpHomeScreen();
                } else {
                    showSpInitialScreen();
                }
            } catch (SdpException e) {
                if (spNotAvailableErrorCode.equals(e.getErrorCode())) {
                    logger.debug("No Service Provider found for corporate user id [{}]", getCoopUserId());

                    showSpInitialScreen();
                } else {
                    logger.error("Unknown error occurred while generating home page", e);
                }
            } catch (Exception e) {
                logger.error("Unknown error occurred while generating home page", e);
            }
        } else if (userType.equals(adminUserTypeK)) {
            showAdminHomeScreen();
        }
    }

    private void showSpInitialScreen() {
        SpHomePage spHomePage = new SpHomePage(this);
        spHomePage.showSpInitialHomeScreen();
        this.getUiManager().clearAll();
        this.getUiManager().pushScreen(SpHomePage.class.getName(), spHomePage);
    }

    @Override
    protected String getApplicationName() {
        return "provisioning";
    }

    public void handleEventRequest(EventRequest eventRequest, EventResponse eventResponse, Window window) {
        provisioningEventHandler.handleEventRequest(eventRequest, eventResponse, window);
    }

    @Override
    public void handleRenderRequest(RenderRequest request, RenderResponse response, Window window) {
        logger.debug("Provisioning portlet received render request");
        try {
            if (userType == null) {
                loadUserDetails(request);
                addHomeView();
            }
        } catch (Exception e) {
            logger.error("Unexpected error occurred while loading user details", e);
        }
    }

    public Set<String> getUserRoles() {
        return userRoles;
    }

    public String getCoopUserId() {
        return coopUserId;
    }

    public Map<String, Object> getSp() {
        return (Map<String, Object>) getFromSession(currentSpDataK);
    }

    public String getUserType() {
        return userType;
    }

    public String getCoopUserName() {
        return coopUserName;
    }

    public String getKiteUserName() {
        return kiteUserName;
    }
}