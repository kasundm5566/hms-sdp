/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.buildfile;

import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.component.table.AbstractDataManagementPanel;
import hms.kite.provisioning.commons.ui.component.table.DataTable;
import hms.kite.provisioning.commons.ui.component.table.SearchLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.provisioning.ProvisioningServiceRegistry.buildFileRepositoryService;
import static hms.kite.provisioning.commons.util.ProvKeyBox.orderByK;
import static hms.kite.util.KiteKeyBox.createdDateK;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class BuildFileManagementPanel extends AbstractDataManagementPanel {

    private static final Logger logger = LoggerFactory.getLogger(BuildFileManagementPanel.class);

    @Override
    protected DataTable getDataTable() {
        return new BuildFileDetailsTable(application, this);
    }

    @Override
    protected SearchLayout getSearchLayout() {
        BuildFileSearchLayout buildFileSearchLayout = new BuildFileSearchLayout(application, this);
        buildFileSearchLayout.setSearchBarHeight("90px");
        buildFileSearchLayout.loadComponents();

        return buildFileSearchLayout;
    }

    @Override
    protected void finalizeQueryCriteria(Map<String, Object> queryCriteria) {
    }

    @Override
    protected int getTotalRecords(Map<String, Object> queryCriteria) {
        return buildFileRepositoryService().getBuildFilesCount(queryCriteria);
    }

    @Override
    protected List<Map<String, Object>> getRecords(int start, int batchSize, Map<String, Object> queryCriteria) {
        Map<String, Object> orderBy = new HashMap<String, Object>();
        orderBy.put(createdDateK, -1);
        queryCriteria.put(orderByK, orderBy);
        return buildFileRepositoryService().findBuildFilesRange(queryCriteria, start, batchSize);
    }

    public BuildFileManagementPanel(BaseApplication application) {
        super(application);
    }
}