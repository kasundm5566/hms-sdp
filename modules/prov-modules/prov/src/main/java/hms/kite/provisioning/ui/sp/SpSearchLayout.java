/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.sp;

import com.vaadin.terminal.UserError;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.component.table.AbstractStandardSearchLayout;
import hms.kite.provisioning.commons.ui.component.table.DataManagementPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.datarepo.mongodb.MongoUtil.createLikeQuery;
import static hms.kite.provisioning.ProvisioningServiceRegistry.validationRegistry;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class SpSearchLayout extends AbstractStandardSearchLayout {

    private static final Logger logger = LoggerFactory.getLogger(SpSearchLayout.class);

    private TextField spNameTxtFld;
    private ComboBox statusCmbBox;

    private boolean searchByStatus = true;

    private void addSpNameTxtFld(HorizontalLayout horizontalLayout) {
        spNameTxtFld = new TextField(application.getMsg("admin.spSearchAndView.sp.name"));
        spNameTxtFld.setWidth("120px");
        addToHorizontalLayout(true, horizontalLayout, spNameTxtFld);
    }

    private void addStatusCmbBox(HorizontalLayout horizontalLayout) {
        statusCmbBox = new ComboBox(application.getMsg("admin.spSearchAndView.sp.status"));
        statusCmbBox.addItem(pendingApproveK);
        statusCmbBox.addItem(approvedK);
        statusCmbBox.addItem(suspendedK);
        statusCmbBox.addItem(rejectK);
        statusCmbBox.setWidth("135px");
        addToHorizontalLayout(searchByStatus, horizontalLayout, statusCmbBox);
    }

    @Override
    protected void addSearchFields(HorizontalLayout horizontalLayout) {
        addSpNameTxtFld(horizontalLayout);
        addStatusCmbBox(horizontalLayout);
    }

    @Override
    public Map<String, Object> getQueryCriteria() {
        Map<String, Object> queryCriteria = new HashMap<String, Object>();

        String spName = (String) spNameTxtFld.getValue();
        String status = (String) statusCmbBox.getValue();

        spNameTxtFld.setComponentError(null);

        if (spName != null && !spName.isEmpty()) {
            if (!(spName.matches(validationRegistry().regex("sp.search.name.validation")))) {
                String errorMessage = application.getMsg("sp.search.name.validation.errorMessage");
                spNameTxtFld.setComponentError(new UserError(errorMessage));
                queryCriteria.put(coopUserNameK, createLikeQuery(spName));
            } else {
                spNameTxtFld.setComponentError(null);
                queryCriteria.put(coopUserNameK, createLikeQuery(spName));
            }
        }

        if (searchByStatus && status != null && !status.isEmpty()) {
            queryCriteria.put(statusK, status);
        }

        return queryCriteria;
    }

    public SpSearchLayout(BaseApplication application, DataManagementPanel dataManagementPanel) {
        super(application, dataManagementPanel);
    }

    public void setSearchByStatus(boolean searchByStatus) {
        this.searchByStatus = searchByStatus;
    }
}
