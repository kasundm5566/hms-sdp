/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/

package hms.kite.provisioning.ui.app.admin;

import com.vaadin.ui.*;
import com.vaadin.ui.themes.BaseTheme;
import hms.kite.provisioning.Provisioning;
import hms.kite.provisioning.commons.ui.event.PortletEventSender;
import hms.kite.provisioning.ui.common.ConfirmationDialog;
import hms.kite.provisioning.ui.common.ConfirmationListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.appRepositoryService;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;


/*
* $LastChangedDate$
* $LastChangedBy$
* $LastChangedRevision$
*/

public class AppNcsEditor {

    private static final Logger logger = LoggerFactory.getLogger(AppNcsEditor.class);

    private Provisioning provisioning;
    private Map<String, Object> app;
    private Table requestTable;
    private Map<String, Object> treeItemMap;
    private Map<String, Button> buttonMap;
    private Map<String, String> operatorStatusMap;
    private Button productionButton;
    private Button limitedProductionButton;

    public AppNcsEditor(Provisioning provisioning, Map<String, Object> app) {
        this.provisioning = provisioning;
        this.app = app;
        requestTable = new Table();

        treeItemMap = new HashMap<String, Object>();
        operatorStatusMap = new HashMap<String, String>();
        productionButton = new Button(provisioning.getMsg("admin.manageApp.view.productionButton"));
        limitedProductionButton = new Button(provisioning.getMsg("admin.manageApp.view.limitedProductionButton"));
        buttonMap = new HashMap<String, Button>();

        //todo: send save ncs event on approval
    }

    public VerticalLayout loadComponents() {
        VerticalLayout layout = new VerticalLayout();
        layout.addComponent(createTable());
        layout.addComponent(createFooter());

        return layout;
    }

    private HorizontalLayout createFooter() {
        HorizontalLayout hLayout = new HorizontalLayout();
        hLayout.setSpacing(true);
        hLayout.setMargin(true, true, true, true);

        Button backButton = new Button(provisioning.getMsg("admin.manageApp.view.backButton"));
        backButton.setImmediate(true);

        backButton.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                provisioning.getUiManager().popScreen();
            }
        });
        hLayout.addComponent(backButton);

        limitedProductionButton.setImmediate(true);
        limitedProductionButton.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {

                showEnableConfirm(provisioning,provisioning.getMsg("admin.newAppRequest.Table.view.ncs.approveToLimitedProductionTitle"),
                        provisioning.getMsg("admin.newAppRequest.Table.view.ncs.message"));
//                app.put(statusK, limitedProductionK);
//                appRepositoryService().update(app);
//                Window.Notification saveMessage = new Window.Notification(
//                        provisioning.getMsg("admin.newAppRequest.view.limitedProductionButton.limitedProductionApprovedNotification"),
//                        Window.Notification.TYPE_HUMANIZED_MESSAGE);
//                saveMessage.setDelayMsec(-1);
//                provisioning.getMainWindow().showNotification(saveMessage);
//                provisioning.getMainWindow().removeAllComponents();
//                provisioning.getMainWindow().addComponent(new AppApproveView(provisioning));
            }
        });
        hLayout.addComponent(limitedProductionButton);

        productionButton.setImmediate(true);
        productionButton.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {

                showEnableConfirm(provisioning,provisioning.getMsg("admin.newAppRequest.Table.view.ncs.approveProductionTitle"),
                        provisioning.getMsg("admin.newAppRequest.Table.view.ncs.message"));

//                app.put(statusK, productionK);
//                appRepositoryService().update(app);
//                Window.Notification saveMessage = new Window.Notification(
//                        provisioning.getMsg("admin.newAppRequest.view.productionButton.productionApprovedNotification"),
//                        Window.Notification.TYPE_HUMANIZED_MESSAGE);
//                saveMessage.setDelayMsec(-1);
//                provisioning.getMainWindow().showNotification(saveMessage);
//                provisioning.getMainWindow().removeAllComponents();
//                provisioning.getMainWindow().addComponent(new AppApproveView(provisioning));
            }
        });
        hLayout.addComponent(productionButton);

        Button rejectButton = new Button(provisioning.getMsg("admin.manageApp.view.rejectButton"));
        rejectButton.setImmediate(true);
        rejectButton.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {

                showEnableConfirm(provisioning,provisioning.getMsg("admin.newAppRequest.Table.view.ncs.rejectTitle"),
                        provisioning.getMsg("admin.newAppRequest.Table.view.ncs.message"));
// app.put(statusK, rejectK);
//                appRepositoryService().update(app);
            }
        });
        hLayout.addComponent(rejectButton);

        if (provisioning.getUserRoles().contains("ROLE_PROV_APPROVE_FOR_LIMITED_PROD")){
                 limitedProductionButton.setEnabled(true);
        } else {
              limitedProductionButton.setEnabled(false);
        }

        if (provisioning.getUserRoles().contains("ROLE_PROV_APPROVE_FOR_ACTIVE_PROD")){
                  productionButton.setEnabled(true);
        } else {
               productionButton.setEnabled(false);
        }

        return hLayout;
    }



    private Table createTable() {
        requestTable.addContainerProperty(provisioning.getMsg("admin.manageApp.Table.view.ncsTable.ncs"), String.class, null);
        requestTable.addContainerProperty(provisioning.getMsg("admin.manageApp.Table.view.ncsTable.operator"), String.class, null);
        requestTable.addContainerProperty(provisioning.getMsg("admin.manageApp.Table.view.ncsTable.status"), String.class, null);
        requestTable.addContainerProperty(provisioning.getMsg("admin.manageApp.Table.view.ncsTable.action"), Button.class, null);
        displayRecord();
        requestTable.setSelectable(true);
        requestTable.setSizeFull();
        requestTable.requestRepaint();
        return requestTable;
    }


    public void displayRecord() {

        Map<String, Map<String, String>> ncsConfiguration =
                (Map<String, Map<String, String>>) app.get(ncsConfigurationK);
        int i = 0;

        for (Map.Entry<String, Map<String, String>> e : ncsConfiguration.entrySet()) {
            String ncsTYpe = e.getKey();
            Map<String, String> operatorStatus = e.getValue();
            int subRowNumber = 1;
            for (Map.Entry<String, String> entry : operatorStatus.entrySet()) {
                String operator = entry.getKey();
                String status = entry.getValue();
                StringBuilder key = new StringBuilder();
                key.append(ncsTYpe).append("_").append(operator);
                Object id = null;
                if (subRowNumber == 1) {
                    id = requestTable.addItem(new Object[]{ncsTYpe, operator, status,
                            createViewLink(ncsTYpe, operator, key.toString())}, i);
                } else {
                    id = requestTable.addItem(new Object[]{"", operator, status,
                            createViewLink(ncsTYpe, operator, key.toString())}, i);
                }
                treeItemMap.put(key.toString(), id);
                operatorStatusMap.put(key.toString(), status);
                i++;
                subRowNumber++;
            }
        }
    }


    private Button createViewLink(final String ncsType, final String operator, String key) {

        final Button button = new Button(provisioning.getMsg("admin.manageApp.Table.view.ncsTable.approveButton"));
        button.setStyleName(BaseTheme.BUTTON_LINK);
        button.setImmediate(true);
        buttonMap.put(key, button);
        button.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                String eventName = provisionNcsK;
                Map<String, Object> valueMap = new HashMap<String, Object>();
                valueMap.put(ncsTypeK, ncsType);
                valueMap.put(operatorK, operator);
                final StringBuilder portletName = new StringBuilder();
                portletName.append(operator).append("provisioning_WAR_").append(operator).append(ncsType);
                valueMap.put(portletIdK, portletName.toString());
                valueMap.put(appIdK, (String) app.get(appIdK));
                PortletEventSender.send(valueMap, eventName, provisioning);
            }
        });
        return button;
    }



    private void showEnableConfirm(final Provisioning provisioning, final String title, String message) {
        final ConfirmationDialog confirmationDialog = new ConfirmationDialog(
                provisioning,
                title,
                message,
                new ConfirmationListener() {

                    @Override
                    public void onYes(String note) {

                        if (title.equals(provisioning.getMsg("admin.newAppRequest.Table.view.ncs.approveToLimitedProductionTitle"))){

                            app.put(statusK, limitedProductionK);
                            appRepositoryService().update(app);

                            provisioning.getMainWindow().removeAllComponents();
                            provisioning.getMainWindow().addComponent(new AppApproveView(provisioning));


                        } else if (title.equals(provisioning.getMsg("admin.newAppRequest.Table.view.ncs.approveProductionTitle"))){

                            app.put(statusK, activeProductionK);
                            appRepositoryService().update(app);
                            provisioning.getMainWindow().removeAllComponents();
                            provisioning.getMainWindow().addComponent(new AppApproveView(provisioning));


                        } else if (title.equals(provisioning.getMsg("admin.newAppRequest.Table.view.ncs.rejectTitle"))){

                             app.put(statusK, rejectK);
                             appRepositoryService().update(app);
                        }
                    }


                });
        confirmationDialog.showDialog();
    }


}
