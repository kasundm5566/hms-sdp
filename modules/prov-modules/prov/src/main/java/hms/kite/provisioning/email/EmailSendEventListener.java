/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.email;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static hms.kite.provisioning.ProvisioningServiceRegistry.sendEmailEventRepositoryService;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.KiteKeyBox.emailSubjectK;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

public class EmailSendEventListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailSendEventListener.class);

    private static EmailClient emailClient;

    private EmailSendEventListener() {}

    @Deprecated
    private static void createDummyData () {
        HashMap<String, Object> email = new HashMap<String, Object>();
        List<String> list = new ArrayList<String>();

        list.add("test@testng.com");
        list.add("testing@testng.com");

        String[] name = new String[] {"SP_REQUEST_SUMMARY_EVENT",
                "SP_REGISTRATION_REQUEST_EVENT",
                "SP_REGISTRATION_APPROVE_EVENT",
                "SP_REGISTRATION_REJECT_EVENT",
                "SP_REGISTRATION_SUSPEND_EVENT"};

        sendEmailEventRepositoryService().deleteAll();

        for (String aName : name) {
            email.put(eventNameK, aName);
            email.put(emailBodyK, "Hi this a empty msg");
            email.put(toAddressListK, list);
            email.put(ccAddressListK, list);
            email.put(emailSubjectK, "Test Email");
            sendEmailEventRepositoryService().create(email);
        }
    }

    public static boolean handle (String eventName) {

        HashMap<String, Object> email;
        LOGGER.info("Handling sending email by event name: {}", eventName);

        if (sendEmailEventRepositoryService().isEventNameExist(eventName)) {
            email = (HashMap) sendEmailEventRepositoryService().findEventByName(eventName);
            emailClient.sendEmail(email);
        } else {
            LOGGER.info("Event [{}] doesn't exist", eventName);
            return false;
        }

        return true;
    }

    public static void setEmailClient(EmailClient emailClient) {
        EmailSendEventListener.emailClient = emailClient;
    }

}
