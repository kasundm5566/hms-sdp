/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.provisioning.ui.cr;

import com.vaadin.ui.*;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.LiferayUtil;
import hms.kite.provisioning.commons.ui.event.PortletEventSender;
import hms.kite.provisioning.commons.util.ProvKeyBox;
import hms.kite.util.KiteKeyBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CrEditNcsView {

    private static Logger logger = LoggerFactory.getLogger(CrEditNcsView.class);

    private BaseApplication application;
    private Panel mainPanel;
    private Panel childPanel;
    private Form form;
    private Map<String, Object> crContext;

    public CrEditNcsView(BaseApplication baseApplication, Panel mainPanel, Map<String, Object> crContext) {
        this.application = baseApplication;
        this.mainPanel = mainPanel;
        this.crContext = crContext;
        init();
    }

    private void init() {
        childPanel = new Panel("XXX");
        childPanel.addStyleName("child-panel");
        form = new Form();
        VerticalLayout layout = new VerticalLayout();
        childPanel.addComponent(layout);
        layout.addComponent(form);
        layout.setComponentAlignment(form, Alignment.MIDDLE_CENTER);
        mainPanel.addComponent(childPanel);
        form.getFooter().addComponent(createButtonBar());
        form.setLayout(new VerticalLayout());
        initializeComponents(form.getLayout());
    }

    private void initializeComponents(Layout layout) {
        Table table = new Table();
        table.addContainerProperty("ncs", String.class, "");
        table.addContainerProperty("operator", String.class, "");
        table.addContainerProperty("action", Layout.class, "");

        Map<String, Object> originalApp = (Map<String, Object>) crContext.get(KiteKeyBox.appK);
        List<Map<String, Object>> ncses = (List<Map<String, Object>>) originalApp.get(ProvKeyBox.ncsesK);

        int i = 0;
        for (Map<String, Object> ncs : ncses) {
            String ncsType = (String) ncs.get(KiteKeyBox.ncsTypeK);
            String operator = (String) ncs.get(KiteKeyBox.operatorK);
            table.addItem(new Object[]{ncsType, operator, createActionButtons(ncsType, operator)}, i++);
        }


        layout.addComponent(table);

    }

    private Object createActionButtons(String ncsType, String operator) {
        HorizontalLayout buttonBar = new HorizontalLayout();
        Button editButton = new Button(application.getMsg("cr.create.cr.edit.ncs.action.edit"));
        Map<String, String> data = new HashMap<String, String>();
        data.put(KiteKeyBox.ncsTypeK, ncsType);
        data.put(KiteKeyBox.operatorK, operator);
        editButton.setData(data);
        editButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    Map<String, String> buttonData = (Map<String, String>) event.getButton().getData();
                    goToEditNcs(buttonData.get(KiteKeyBox.ncsTypeK), buttonData.get(KiteKeyBox.operatorK));
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while handling edit action");
                }
            }
        });
        buttonBar.addComponent(editButton);
        return buttonBar;
    }

    private void goToEditNcs(String ncsType, String operator) {
        logger.debug("Going to edit ncsType {} and operator {}", ncsType, operator);
        Map<String, Object> eventData = new HashMap<String, Object>();
        eventData.put(KiteKeyBox.coopUserIdK, crContext.get(KiteKeyBox.coopUserIdK));
        eventData.put(ProvKeyBox.portletIdK, LiferayUtil.createPortletId(ncsType, operator));
        eventData.put(KiteKeyBox.ncsTypeK, ncsType);
        eventData.put(KiteKeyBox.operatorK, operator);
        eventData.put(KiteKeyBox.crK, crContext);
        PortletEventSender.send(eventData, KiteKeyBox.provCrEditNcsEvent, application);
    }

    private Component createButtonBar() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        Button backButton = new Button(application.getMsg("cr.create.cr.back"));
        backButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                mainPanel.removeAllComponents();
                new InitialCrSelectionWindow(application, mainPanel, crContext);

            }
        });
        horizontalLayout.addComponent(backButton);
        return horizontalLayout;
    }


}
