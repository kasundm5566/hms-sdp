/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.sp.sp;

import com.vaadin.ui.*;
import hms.kite.provisioning.Provisioning;
import hms.kite.provisioning.commons.ui.event.PortletEventSender;
import hms.kite.provisioning.ui.common.ConfirmationDialog;
import hms.kite.provisioning.ui.common.ConfirmationListener;
import hms.kite.provisioning.ui.common.ViewMode;
import hms.kite.provisioning.ui.sp.ncs.AllNcsTypesLayout;
import hms.kite.provisioning.ui.sp.ncs.NcsConfigurationView;
import hms.kite.provisioning.util.SpHomePage;
import hms.kite.util.SdpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.vaadin.ui.Alignment.MIDDLE_CENTER;
import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.provisioning.ProvisioningServiceRegistry.eventRouter;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.provisioning.ui.common.ViewMode.*;
import static hms.kite.util.KiteErrorBox.spNotAvailableErrorCode;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class NcsTypeConfigurationLayout extends AllNcsTypesLayout {

    private static final Logger logger = LoggerFactory.getLogger(NcsTypeConfigurationLayout.class);

    private Label statusLbl;

    private void init(String title) {
        setTitle(title);

        statusLbl = new Label();
        statusLbl.setVisible(false);
    }

    private void setStatusMessage(String message, boolean success) {
        statusLbl = new Label();

        if (success) {
            statusLbl.addStyleName("sp-request-success-notification");
        } else {
            statusLbl.addStyleName("sp-request-fail-notification");
        }

        statusLbl.setValue(message);
    }

    private void removeSp() {
        ConfirmationDialog confirmationDialog = new ConfirmationDialog(provisioning,
                provisioning.getMsg("sp.cancelConfirmation.title"),
                provisioning.getMsg("sp.cancelConfirmation.message"),
                new ConfirmationListener() {

                    @Override
                    public void onYes(String note) {
                        try {
                            logger.info("Canceling SP create request");

                            String id = (String) provisioning.getSp().get(spIdK);
                            if (id != null) {
                                logger.debug("Deleting SP from repo with id [{}] ", id);

                                spRepositoryService().deleteSpById(id);
                                provisioning.getSp().clear();
                                PortletEventSender.send(null, refreshMenuEvent, provisioning);

                                goToSpHomePage();
                                reloadComponents();
                            }
                        } catch (Exception e) {
                            setStatusMessage(provisioning.getMsg("sp.request.cancel.failure.message"), false);
                        }
                    }
                });
        confirmationDialog.setNoteRequired(false);
        confirmationDialog.showDialog();
    }

    private void createCancelSpButton(HorizontalLayout buttonBarLayout) {
        Button cancelCpButton = new Button(provisioning.getMsg("sp.ncs.configuration.cancelSpRequest"));
        buttonBarLayout.addComponent(cancelCpButton);
        cancelCpButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                try {
                    removeSp();
                } catch (Exception e) {
                    logger.error("Error occurred while canceling the SP request", e);
                }
            }
        });
    }

    private void goToSpHomePage() {
        logger.debug("Going back to the Sp initial page ");

        SpHomePage spHomePage = new SpHomePage(provisioning);
        spHomePage.showSpInitialHomeScreen();

        provisioning.getUiManager().clearAll();
        provisioning.getUiManager().pushScreen(SpHomePage.class.getName(), spHomePage);
    }

    private void createCancelButton(HorizontalLayout buttonBarLayout) {
        Button cancelButton = new Button(provisioning.getMsg("sp.ncs.configuration.cancel"));
        buttonBarLayout.addComponent(cancelButton);
        cancelButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                try {
                    onCancelButtonClicked();
                } catch (Exception e) {
                    logger.error("Error occurred while canceling ", e);
                    showErrorNotification("Internal Error occurred");
                }
            }
        });
    }

    private void createConfirmButton(HorizontalLayout buttonBarLayout) {
        Button confirmButton = new Button(provisioning.getMsg("sp.ncs.configuration.confirm"));
        buttonBarLayout.addComponent(confirmButton);
        confirmButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                try {
                    onConfirmButtonClicked();
                } catch (Exception e) {
                    logger.error("Error occurred while confirming ", e);
                    showErrorNotification("Internal Error occurred");
                }
            }
        });
        buttonBarLayout.addComponent(confirmButton);
    }

    private void createNextButton(HorizontalLayout buttonBarLayout) {
        Button nextButton = new Button(provisioning.getMsg("sp.ncs.configuration.next"));
        buttonBarLayout.addComponent(nextButton);
        nextButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                try {
                    onNextButtonClicked();
                } catch (Exception e) {
                    logger.error("Error occurred while going to next page ", e);
                    showErrorNotification("Internal Error occurred");
                }
            }
        });
    }

    private void createBackButton(HorizontalLayout buttonBarLayout) {
        Button backButton = new Button(provisioning.getMsg("sp.ncs.configuration.back"));
        buttonBarLayout.addComponent(backButton);
        backButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                try {
                    onBackButtonClicked();
                } catch (Exception e) {
                    logger.error("Error occurred while going to back page ", e);
                    showErrorNotification("Internal Error occurred");
                }
            }
        });
    }

    private void onBackButtonClicked() {
        if (viewMode == CONFIRM) {
            viewMode = EDIT;
            for (NcsConfigurationView ncsConfigurationView : ncsConfigurationViewList) {
                ncsConfigurationView.changeMode(EDIT);
            }
            reloadComponents();
        } else if (viewMode == EDIT) {
            PortletEventSender.send(null, "sp-create-sp", provisioning);
        }
    }

    private void onNextButtonClicked() {
        boolean validated = true;

        for (NcsConfigurationView ncsConfigurationView : ncsConfigurationViewList) {
            if (!ncsConfigurationView.validateData()) {
                validated = false;
            }
        }

        if (validated) {
            for (NcsConfigurationView ncsConfigurationView : ncsConfigurationViewList) {
                ncsConfigurationView.changeMode(CONFIRM);
            }
            viewMode = CONFIRM;
        }

        reloadComponents();
    }

    private void onConfirmButtonClicked() {
        try {
            try {
                Map<String, Object> sp = spRepositoryService().findSpByCoopUserId(provisioning.getCoopUserId());
                if (sp != null && rejectK.equals(sp.get(statusK))) {
                    provisioning.getSp().put(statusK, pendingApproveK);
                    spRepositoryService().update(provisioning.getSp());
                    Map<String, Object> spCreatedEvent = createSpCreatedEvent();
                    logger.debug("Firing SP created event {} ", spCreatedEvent);
                    eventRouter().fire(spCreatedEvent);
                    viewMode = VIEW;
                    setStatusMessage(provisioning.getMsg("sp.request.success.notification.message"), true);
                    reloadComponents();
                    PortletEventSender.send(null, refreshMenuEvent, provisioning);
                    return;
                }
            } catch (SdpException e) {
                if (spNotAvailableErrorCode.equals(e.getErrorCode())) {
                    logger.debug("There is no SP registered in this coop user ID, new SP created");
                } else {
                    logger.error("Unknown error occurred while creating new SP", e);
                    return;
                }
            }

            for (NcsConfigurationView ncsConfigurationView : ncsConfigurationViewList) {
                ncsConfigurationView.saveData();
            }
            provisioning.getSp().put(statusK, pendingApproveK);
            provisioning.getSp().put(coopUserNameK, provisioning.getCoopUserName());
            provisioning.getSp().put(createdByK, provisioning.getKiteUserName());
            provisioning.getSp().put(spRequestDateK, DateFormat.getDateInstance(DateFormat.SHORT).format(new Date()));
            spRepositoryService().create(provisioning.getSp());

            Map<String, Object> spCreatedEvent = createSpCreatedEvent();
            logger.debug("Firing SP created event {} ", spCreatedEvent);
            eventRouter().fire(spCreatedEvent);

            setStatusMessage(provisioning.getMsg("sp.request.success.notification.message"), true);

            viewMode = VIEW;
            reloadComponents();

            PortletEventSender.send(null, refreshMenuEvent, provisioning);
        } catch (Exception e) {
            logger.error("Error occurred while saving the SP ", e);

            setStatusMessage(provisioning.getMsg("sp.request.failure.notification.message"), false);

            reloadComponents();
        }
    }

    private Map<String, Object> createSpCreatedEvent() {
        Map<String, Object> spCreatedMap = new HashMap<String, Object>();
        spCreatedMap.put(eventNameK, spRegistrationRequestEvent);
        spCreatedMap.put(spK, provisioning.getSp());
        return spCreatedMap;
    }

    private void onCancelButtonClicked() {
        provisioning.addHomeView();
    }

    @Override
    protected Component createHeader() {
        return new VerticalLayout();
    }

    @Override
    protected Component createFooter() {
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setWidth("100%");

        HorizontalLayout buttonBarLayout = new HorizontalLayout();
        buttonBarLayout.setHeight("40px");
        buttonBarLayout.setSpacing(true);

        switch (viewMode) {
            case VIEW:
                break;
            case EDIT:
                createBackButton(buttonBarLayout);
                createNextButton(buttonBarLayout);
                createCancelButton(buttonBarLayout);
                break;
            case CONFIRM:
                createBackButton(buttonBarLayout);
                createConfirmButton(buttonBarLayout);
                createCancelButton(buttonBarLayout);
                break;
            case CANCEL:
                createCancelSpButton(buttonBarLayout);
        }

        verticalLayout.addComponent(statusLbl);
        verticalLayout.addComponent(buttonBarLayout);
        verticalLayout.setComponentAlignment(buttonBarLayout, MIDDLE_CENTER);

        return verticalLayout;
    }

    public NcsTypeConfigurationLayout(Provisioning provisioning, ViewMode viewMode, String title) {
        super(provisioning, viewMode);

        init(title);
    }
}
