/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.sp.admin;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import hms.kite.provisioning.Provisioning;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class SPApproveApplication extends VerticalLayout {

    private Panel panel;

    public SPApproveApplication(Provisioning provisioning, SpSearchViewMode viewMode, String title) {
        panel = new Panel(title);
        panel.setSizeFull();

        addComponent(panel);
        setComponentAlignment(panel, Alignment.MIDDLE_CENTER);
        SpSearchAndViewView editor = new SpSearchAndViewView(provisioning, viewMode);
        panel.addComponent(editor.loadComponents());
    }
}
