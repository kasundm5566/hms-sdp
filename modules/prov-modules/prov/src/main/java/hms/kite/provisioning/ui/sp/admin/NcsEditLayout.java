/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.sp.admin;

import com.vaadin.ui.*;
import hms.kite.provisioning.Provisioning;
import hms.kite.provisioning.ui.common.ViewMode;
import hms.kite.provisioning.ui.sp.ncs.AllNcsTypesLayout;
import hms.kite.provisioning.ui.sp.ncs.NcsConfigurationView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.provisioning.ui.common.ViewMode.EDIT;
import static hms.kite.provisioning.ui.common.ViewMode.VIEW;
import static hms.kite.util.KiteKeyBox.coopUserNameK;
import static hms.kite.util.KiteKeyBox.spIdK;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class NcsEditLayout extends AllNcsTypesLayout implements AdminActionPerformedListener {

    private static final Logger logger = LoggerFactory.getLogger(NcsEditLayout.class);

    private Map<String, Object> sp;
    private Map<String, Object> originalSp;
    private AdminActionHandler adminActionHandler;

    private Label statusLbl;

    private void init() {
        statusLbl = new Label();
        statusLbl.setVisible(false);
    }

    private void setStatusMessage(String message, boolean success) {
        statusLbl = new Label();

        if (success) {
            statusLbl.addStyleName("sp-request-success-notification");
        } else {
            statusLbl.addStyleName("sp-request-fail-notification");
        }

        statusLbl.setValue(message);
    }

    private void createSaveButton(HorizontalLayout buttonBarLayout) {
        Button saveButton = new Button(provisioning.getMsg("admin.sp.approve.save"));
        saveButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                String spName = (String) provisioning.getSp().get(coopUserNameK);
                try {
                    if (validate()) {
                        adminActionHandler.showSaveEditConfirm(originalSp, NcsEditLayout.this);
                        viewMode = VIEW;
                        setStatusMessage(provisioning.getMsg("admin.spEdit.success.notification.message", spName), true);
                    } else {
                        logger.error("Due to validation failure, can't edit SP.");

                        setStatusMessage(provisioning.getMsg("admin.spEdit.validationFailure.notification.message", spName),
                                false);
                    }
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while going back from editing page", e);

                    setStatusMessage(provisioning.getMsg("admin.spEdit.failure.notification.message", spName),
                            false);
                }
            }
        });
        buttonBarLayout.addComponent(saveButton);

    }

    private void createBackButton(HorizontalLayout buttonBarLayout) {
        Button backButton = new Button(provisioning.getMsg("admin.sp.approve.back"));
        backButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    goBack();
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while going back from sp approval page", e);
                }
            }
        });
        buttonBarLayout.addComponent(backButton);
    }

    private void goBack() {
        logger.debug("Going back from sp editing page ");

        provisioning.getUiManager().popScreen();
    }

    @Override
    protected Component createHeader() {
        return new VerticalLayout();
    }

    @Override
    protected Component createFooter() {
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setWidth("100%");
        HorizontalLayout buttonBarLayout = new HorizontalLayout();
        buttonBarLayout.setSpacing(true);

        createBackButton(buttonBarLayout);

        if (viewMode.equals(EDIT)) {
            createSaveButton(buttonBarLayout);
        }

        verticalLayout.addComponent(statusLbl);
        verticalLayout.addComponent(buttonBarLayout);
        verticalLayout.setComponentAlignment(buttonBarLayout, Alignment.MIDDLE_CENTER);

        return verticalLayout;
    }

    @Override
    protected void reloadComponents() {
        sp = spRepositoryService().findSpById((String) sp.get(spIdK));

        super.reloadComponents();
    }

    public NcsEditLayout(Provisioning provisioning, Map<String, Object> sp, AdminActionHandler adminActionHandler,
                         ViewMode viewMode) {
        super(provisioning, viewMode);

        this.sp = sp;
        this.adminActionHandler = adminActionHandler;

        init();
    }

    @Override
    public void onActionPerformed() {
        reloadComponents();
    }

    public boolean validate() {
        for (NcsConfigurationView ncsConfigurationView : ncsConfigurationViewList) {
            logger.debug("Validating [{}] Validated [{}]", ncsConfigurationView.getClass().getName(), ncsConfigurationView.validateData());
            if (!ncsConfigurationView.validateData()) {
                return false;
            }
        }
        return true;
    }

    public void setOriginalSp(Map<String, Object> originalSp) {
        this.originalSp = originalSp;
    }
}
