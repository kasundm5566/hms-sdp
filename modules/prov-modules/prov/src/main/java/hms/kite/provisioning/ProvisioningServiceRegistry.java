/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning;

import hms.kite.datarepo.AppRepositoryService;
import hms.kite.datarepo.BuildFileRepositoryService;
import hms.kite.datarepo.CrRepositoryService;
import hms.kite.datarepo.SpRepositoryService;
import hms.kite.provisioning.commons.ValidationRegistry;
import hms.kite.provisioning.commons.event.EventRouter;
import hms.kite.provisioning.commons.ui.ErrorCodeResolver;
import hms.kite.provisioning.email.EmailRender;
import hms.kite.provisioning.repo.EmailTemplateRepositoryService;
import hms.kite.util.NullObject;

import java.util.Map;
import java.util.Properties;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ProvisioningServiceRegistry {

    private static ValidationRegistry validationRegistry;
    private static EmailTemplateRepositoryService emailTemplateRepositoryService;
    private static EventRouter eventRouter;
    private static ErrorCodeResolver errorCodeResolver;
    private static SpRepositoryService spRepositoryService;
    private static AppRepositoryService appRepositoryService;
    private static CrRepositoryService crRepositoryService;
    private static BuildFileRepositoryService buildFileRepositoryService;

    private static Map<String, EmailRender> renders;

    private static String portletId;
    private static String spInformationExternalUrl;
    private static String appPublishUrl;
    private static Integer nofRecordsForAppResultTable;
    private static Integer nofRecordsForSpResultTable;

    private static Properties defaultValuesProperties;

    static {
        setEmailTemplateRepositoryService(new NullObject<EmailTemplateRepositoryService>().get());
        setEventRouter(new NullObject<EventRouter>().get());
        setValidationRegistry(new NullObject<ValidationRegistry>().get());
        setRenders(new NullObject<Map<String, EmailRender>>().get());
        setPortletId(new NullObject<String>().get());
        setSpInformationExternalUrl(new NullObject<String>().get());
        setErrorCodeResolver(new NullObject<ErrorCodeResolver>().get());
        setAppPublishUrl(new NullObject<String>().get());
        setNofRecordsForAppResultTable("0");
        setNofRecordsForSpResultTable("0");
        setAppRepositoryService(new NullObject<AppRepositoryService>().get());
        setSpRepositoryService(new NullObject<SpRepositoryService>().get());
        setCrRepositoryService(new NullObject<CrRepositoryService>().get());
        setBuildFileRepositoryService(new NullObject<BuildFileRepositoryService>().get());
        setDefaultValuesProperties(new NullObject<Properties>().get());
    }

    private static void setEmailTemplateRepositoryService(EmailTemplateRepositoryService repo) {
        emailTemplateRepositoryService = repo;
    }

    public static EmailTemplateRepositoryService sendEmailEventRepositoryService() {
        return emailTemplateRepositoryService;
    }

    public static ValidationRegistry validationRegistry() {
        return validationRegistry;
    }

    private static void setValidationRegistry(ValidationRegistry validationRegistry) {
        ProvisioningServiceRegistry.validationRegistry = validationRegistry;
    }

    public static EventRouter eventRouter() {
        return eventRouter;
    }

    private static void setEventRouter(EventRouter eventRouter) {
        ProvisioningServiceRegistry.eventRouter = eventRouter;
    }

    public static ErrorCodeResolver errorCodeResolver() {
        return errorCodeResolver;
    }

    public static EmailRender emailRender(String eventName) {
        return renders.get(eventName);
    }

    private static void setRenders(Map<String, EmailRender> renders) {
        ProvisioningServiceRegistry.renders = renders;
    }

    public static String portletId() {
        return ProvisioningServiceRegistry.portletId;
    }

    private static void setPortletId(String portletId) {
        ProvisioningServiceRegistry.portletId = portletId;
    }

    public static String spInformationExternalUrl() {
        return spInformationExternalUrl;
    }

    private static void setSpInformationExternalUrl(String spInformationExternalUrl) {
        ProvisioningServiceRegistry.spInformationExternalUrl = spInformationExternalUrl;
    }

    private static void setNofRecordsForAppResultTable(String nofRecordsForAppResultTable) {
        ProvisioningServiceRegistry.nofRecordsForAppResultTable = Integer.valueOf(nofRecordsForAppResultTable);
    }

    public static Integer nofRecordsForAppResultTable() {
        return nofRecordsForAppResultTable;
    }

    private static void setErrorCodeResolver(ErrorCodeResolver errorCodeResolver) {
        ProvisioningServiceRegistry.errorCodeResolver = errorCodeResolver;
    }

    public static AppRepositoryService appRepositoryService() {
        return appRepositoryService;
    }

    private static void setAppRepositoryService(AppRepositoryService appRepositoryService) {
        ProvisioningServiceRegistry.appRepositoryService = appRepositoryService;
    }

    public static SpRepositoryService spRepositoryService() {
        return spRepositoryService;
    }

    private static void setSpRepositoryService(SpRepositoryService spRepositoryService) {
        ProvisioningServiceRegistry.spRepositoryService = spRepositoryService;
    }

    public static CrRepositoryService crRepositoryService() {
        return crRepositoryService;
    }

    private static void setCrRepositoryService(CrRepositoryService crRepositoryService) {
        ProvisioningServiceRegistry.crRepositoryService = crRepositoryService;
    }

    public static String appPublishUrl() {
        return appPublishUrl;
    }

    private static void setAppPublishUrl(String appPublishUrl) {
        ProvisioningServiceRegistry.appPublishUrl = appPublishUrl;
    }

    public static int nofRecordsForSpResultTable() {
        return nofRecordsForSpResultTable;
    }

    private static void setNofRecordsForSpResultTable(String nofRecordsForSpResultTable) {
        ProvisioningServiceRegistry.nofRecordsForSpResultTable = Integer.valueOf(nofRecordsForSpResultTable);
    }

    public static BuildFileRepositoryService buildFileRepositoryService() {
        return buildFileRepositoryService;
    }

    private static void setBuildFileRepositoryService(BuildFileRepositoryService buildFileRepositoryService) {
        ProvisioningServiceRegistry.buildFileRepositoryService = buildFileRepositoryService;
    }

    private static void setDefaultValuesProperties(Properties defaultValuesProperties) {
        ProvisioningServiceRegistry.defaultValuesProperties = defaultValuesProperties;
    }

    public static boolean isDefaultAllowedHostAddressesAllow() {
        return Boolean.valueOf(defaultValuesProperties.getProperty("prov.app.default.allowed.host.addresses.allow"));
    }

    public static String getDefaultAllowedHostAddresses() {
        return defaultValuesProperties.getProperty("prov.app.default.allowed.host.addresses");
    }

    public static boolean isDefaultRevenueShareAllow() {
        return Boolean.valueOf(defaultValuesProperties.getProperty("prov.app.default.revenue.share.allow"));
    }

    public static int getDefaultRevenueShare() {
        return Integer.valueOf(defaultValuesProperties.getProperty("prov.app.default.revenue.share"));
    }

    public static boolean isAppRequestedDateColorAllow() {
        return Boolean.valueOf(defaultValuesProperties.getProperty("prov.app.requested.date.color.allow"));
    }

    public static boolean getDefaultExpire() {
        return Boolean.parseBoolean(defaultValuesProperties.getProperty("prov.app.default.expire"));
    }

    public static boolean getDefaultAdvertising() {
        return Boolean.valueOf(defaultValuesProperties.getProperty("prov.app.default.advertising"));
    }

    public static boolean getDefaultGovern() {
        return Boolean.valueOf(defaultValuesProperties.getProperty("prov.app.default.govern"));
    }

    public static boolean getDefaultMasking() {
        return Boolean.valueOf(defaultValuesProperties.getProperty("prov.app.default.masking"));
    }

    public static boolean getDefaultTax() {
        return Boolean.valueOf(defaultValuesProperties.getProperty("prov.app.default.tax"));
    }

    public static boolean getLogChargingDetails() {
        return Boolean.valueOf(defaultValuesProperties.getProperty("prov.logging.app.charging.details"));
    }

    public static boolean getWhiteListMandatory() {
        return Boolean.valueOf(defaultValuesProperties.getProperty("prov.app.WhiteList.mandatory"));
    }

    public static boolean isHelpButtonLinkEnable() {
        return Boolean.parseBoolean(defaultValuesProperties.getProperty("prov.app.help.link.enable"));
    }

    public static boolean isAppHostingSpaceCheckBoxEnable() {
        return Boolean.parseBoolean(defaultValuesProperties.getProperty("prov.app.hosting.space.checkbox.enable"));
    }

    public static boolean isInAppApiAllowed() {
        return Boolean.parseBoolean(defaultValuesProperties.getProperty("prov.app.in.app.api.allowed"));
    }
}
