/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.sp.ncs;

import com.vaadin.data.Validator;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import hms.kite.provisioning.Provisioning;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.provisioning.commons.util.ProvKeyBox.maxNoOfBcMsgsPerDayK;
import static hms.kite.provisioning.commons.util.ProvKeyBox.maxNoOfSubscribersK;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class NcsSubscriptionView extends NcsConfigurationView {

    private static final Logger logger = LoggerFactory.getLogger(NcsSubscriptionView.class);

    private TextField maxNoSubscribersTxtFld;
    private TextField maxNoTpdTxtFld;

    private void init(Provisioning provisioning) {
        maxNoSubscribersTxtFld = new TextField(provisioning.getMsg("sp.ncs.subscription.maxNumOfSubscribers"));
        maxNoSubscribersTxtFld.setImmediate(true);
        maxNoSubscribersTxtFld.addValidator(createValidator("ncsMaxNoOfSubscribersRegex", "sp.ncs.subscription.MaxNumOfSubscribers.validation"));
        maxNoSubscribersTxtFld.addValidator(createValidator("sp.createApp.zero.validation", "sp.ncs.subscription.zero.validation.error.message"));

        maxNoTpdTxtFld = new TextField(provisioning.getMsg("sp.ncs.subscription.maxNumOfMsgsPerDay"));
        maxNoTpdTxtFld.setImmediate(true);
        maxNoTpdTxtFld.setRequired(true);
        maxNoTpdTxtFld.setRequiredError(provisioning.getMsg("sp.ncs.subscription.maxNumOfMsgsPerDay.required.error"));
        maxNoTpdTxtFld.addValidator(createValidator("ncsSubscriptionTpdRegex", "sp.ncs.subscription.maxNumOfMsgsPerDay.validation"));
        maxNoTpdTxtFld.addValidator(createValidator("sp.createApp.zero.validation", "sp.ncs.subscription.zero.validation.error.message"));
    }

    private void reloadData() {
        Map<String, Object> sp = provisioning.getSp();
        Object value = sp.get(maxNoOfSubscribersK);
        if (value == null) {
            setValueToField(maxNoSubscribersTxtFld, provisioning.getMsg("sp.ncs.subscription.default.value"));
        } else {
            setValueToField(maxNoSubscribersTxtFld, sp.get(maxNoOfSubscribersK));
            setValueToField(maxNoTpdTxtFld, sp.get(maxNoOfBcMsgsPerDayK));
        }
        setSpSessionValues();
    }

    private void setDefaultNumberOfSubscribers() {
        if (maxNoSubscribersTxtFld.getValue() == null || (maxNoSubscribersTxtFld.getValue()).equals("")) {
            maxNoSubscribersTxtFld.setValue(provisioning.getMsg("sp.ncs.subscription.default.value"));
        }
        setSpSessionValues();
    }

    @Override
    protected void changeToViewMode() {
        changeToConfirmMode();
    }

    @Override
    protected void changeToConfirmMode() {
        setDefaultNumberOfSubscribers();
        maxNoSubscribersTxtFld.setReadOnly(true);
        maxNoTpdTxtFld.setReadOnly(true);
    }

    private void addToForm() {
        form.addField("maxNoSubValue", maxNoSubscribersTxtFld);
        form.addField("maxNoTpd", maxNoTpdTxtFld);
    }

    @Override
    protected void changeToEditMode() {
        maxNoSubscribersTxtFld.setReadOnly(false);
        maxNoTpdTxtFld.setReadOnly(false);
    }

    @Override
    protected void changeToCancelView() {
        changeToViewMode();
    }

    @Override
    protected void setSpSessionValues() {
        provisioning.getSp().put(maxNoOfSubscribersK, maxNoSubscribersTxtFld.getValue());
        provisioning.getSp().put(maxNoOfBcMsgsPerDayK, maxNoTpdTxtFld.getValue());
    }

    public NcsSubscriptionView(Provisioning provisioning) {
        this.provisioning = provisioning;

        init(provisioning);
    }

    @Override
    public void saveData() {
    }

    @Override
    public VerticalLayout loadComponents(final Panel panel) {
        VerticalLayout layout = new VerticalLayout();

        panel.removeAllComponents();
        panel.setCaption(provisioning.getMsg("sp.ncs.subscription.title"));
        panel.addComponent(layout);

        addToForm();
        reloadData();
        refreshInChangedMode();

        layout.addComponent(form);
        layout.setComponentAlignment(form, Alignment.MIDDLE_CENTER);
        layout.setSpacing(true);

        return layout;
    }

    @Override
    public boolean validateData() {
        logger.debug("Validating Subscription NCS configuration");
        setSpSessionValues();
        try {
            maxNoSubscribersTxtFld.validate();
            maxNoTpdTxtFld.validate();
            form.setComponentError(null);
        } catch (Validator.InvalidValueException e) {
            form.setComponentError(e);
            return false;
        } catch (Exception e) {
            logger.error("Unexpected error occurred while validating", e);
            return false;
        }
        return true;
    }
}
