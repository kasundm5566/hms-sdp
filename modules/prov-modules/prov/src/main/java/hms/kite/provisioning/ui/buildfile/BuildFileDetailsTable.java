/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.buildfile;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Window;
import hms.kite.provisioning.commons.ui.Action;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.component.popup.ConfirmationBox;
import hms.kite.provisioning.commons.ui.component.table.DataManagementPanel;
import hms.kite.provisioning.commons.ui.component.table.DataTable;
import hms.kite.provisioning.commons.ui.event.PortletEventSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.provisioning.ProvisioningServiceRegistry.buildFileRepositoryService;
import static hms.kite.provisioning.commons.ui.LiferayUtil.createPortletId;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.provisioning.ui.buildfile.BuildFileDetailsTable.BuildFileAction.*;
import static hms.kite.provisioning.ui.common.ComponentFactory.createSpDetailsLink;
import static java.text.MessageFormat.format;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class BuildFileDetailsTable extends DataTable {

    private static final Logger logger = LoggerFactory.getLogger(BuildFileDetailsTable.class);

    private static final String ROLE_PROV_DOWNLOADABLE = "ROLE_PROV_DOWNLOADABLE_";

    private Object addSpLink(final Map<String, Object> buildFile) {
        String spName = buildFile.get(createdByK).toString();
        String corporateUserId = (String) spRepositoryService()
                .findSpBySpId(buildFile.get(spIdK).toString()).get(coopUserIdK);
        return createSpDetailsLink(application, spName, corporateUserId);
    }

    private HorizontalLayout createActionButtons(Map<String, Object> buildFile) {
        HorizontalLayout buttonLayout = new HorizontalLayout();
        buttonLayout.removeAllComponents();

        Object buildFileStatus = buildFile.get(statusK);
        logger.trace("Current Build File status [{}]", buildFileStatus);

        buttonLayout.addComponent(createLink(VIEW_BUILD,
                application.getMsg("downloadable.buildfile.list.table.action.view.button"), buildFile));

        if (pendingApproveK.equals(buildFileStatus)) {
            addActionButton(APPROVE_BUILD, application.getMsg("downloadable.buildfile.list.table.action.approve.button"),
                    buildFile, buttonLayout);
            addActionButton(REJECT_BUILD, application.getMsg("downloadable.buildfile.list.table.action.reject.button"),
                    buildFile, buttonLayout);
            addActionButton(DELETE_BUILD, application.getMsg("downloadable.buildfile.list.table.action.delete.button"),
                    buildFile, buttonLayout);
        } else if (approvedK.equals(buildFileStatus)) {
            addActionButton(EDIT_BUILD, application.getMsg("downloadable.buildfile.list.table.action.edit.button"),
                    buildFile, buttonLayout);
            addActionButton(SUSPEND_BUILD, application.getMsg("downloadable.buildfile.list.table.action.suspend.button"),
                    buildFile, buttonLayout);
            addActionButton(DELETE_BUILD, application.getMsg("downloadable.buildfile.list.table.action.delete.button"),
                    buildFile, buttonLayout);
        } else if (suspendedK.equals(buildFileStatus)) {
            addActionButton(RESTORE_BUILD, application.getMsg("downloadable.buildfile.list.table.action.restore.button"),
                    buildFile, buttonLayout);
            addActionButton(DELETE_BUILD, application.getMsg("downloadable.buildfile.list.table.action.delete.button"),
                    buildFile, buttonLayout);
        } else if (rejectK.equals(buildFileStatus)) {
            addActionButton(EDIT_BUILD, application.getMsg("downloadable.buildfile.list.table.action.edit.button"),
                    buildFile, buttonLayout);
            addActionButton(DELETE_BUILD, application.getMsg("downloadable.buildfile.list.table.action.delete.button"),
                    buildFile, buttonLayout);
        } else if (initialK.equals(buildFileStatus) || draftK.equals(buildFileStatus)) {
            addActionButton(DELETE_BUILD, application.getMsg("downloadable.buildfile.list.table.action.delete.button"),
                    buildFile, buttonLayout);
        } else {
            logger.error("Unexpected status [{}]", buildFileStatus);
        }
        return buttonLayout;
    }

    private void checkConfirmation(final BuildFileAction action, final Map<String, Object> buildFile) {
        Object appId = buildFile.get(appIdK);

        if (DELETE_BUILD.equals(action) && isLastBuildFile(appId)) {
            showNotification(format(application.getMsg("downloadable.buildfile.list.delete.error"), appId), 5000);
            return;
        }

        ConfirmationBox confirmationBox = new ConfirmationBox(
                application.getMsg("downloadable.buildfile.confirmationbox.title"),
                application.getMsg("downloadable.buildfile.confirmationbox.yes.caption"),
                application.getMsg("downloadable.buildfile.confirmationbox.no.caption"),
                format(application.getMsg("downloadable.buildfile.confirmationbox.action.message"), action.getName()),
                new ConfirmationBox.ConfirmationListener() {

                    @Override
                    public void onYes() {
                        switch (action) {
                            case APPROVE_BUILD:
                                approveBuildFile(buildFile);
                                break;
                            case SUSPEND_BUILD:
                                suspendBuildFile(buildFile);
                                break;
                            case RESTORE_BUILD:
                                restoreBuildFile(buildFile);
                                break;
                            case REJECT_BUILD:
                                rejectBuildFile(buildFile);
                                break;
                            case DELETE_BUILD:
                                deleteBuildFile(buildFile);
                                break;
                            default:
                                logger.error("Unknown Action [{}], Can't handle ", action);
                        }
                    }

                    @Override
                    public void onNo() {
                    }
                },
                application.getMainWindow());

        confirmationBox.viewConfirmation();
    }

    private boolean isLastBuildFile(Object appId) {
        Map<String, Object> queryCriteria = new HashMap<String, Object>();
        queryCriteria.put(appIdK, appId);
        return buildFileRepositoryService().getBuildFilesCount(queryCriteria) == 1;
    }

    private void showNotification(String message, int delay) {
        Window.Notification notification = new Window.Notification(message, Window.Notification.TYPE_HUMANIZED_MESSAGE);
        notification.setDelayMsec(delay);
        application.getMainWindow().showNotification(notification);
    }

    private void viewBuildFile(Map<String, Object> buildFile) {
        fireEvent(provViewBuildFileEventK, buildFile);
    }

    private void editBuildFile(Map<String, Object> buildFile) {
        fireEvent(provReConfigureBuildFileEventK, buildFile);
    }

    private void updateBuildFileStatus(Map<String, Object> buildFile, String status) {
        buildFile.put(statusK, status);
        buildFileRepositoryService().updateBuildFile(buildFile);
        dataManagementPanel.search();
    }

    private void approveBuildFile(Map<String, Object> buildFile) {
        updateBuildFileStatus(buildFile, approvedK);
    }

    private void suspendBuildFile(Map<String, Object> buildFile) {
        updateBuildFileStatus(buildFile, suspendedK);
    }

    private void restoreBuildFile(Map<String, Object> buildFile) {
        updateBuildFileStatus(buildFile, approvedK);
    }

    private void rejectBuildFile(Map<String, Object> buildFile) {
        updateBuildFileStatus(buildFile, rejectK);
    }

    private void deleteBuildFile(Map<String, Object> buildFile) {
        buildFileRepositoryService().deleteBuildFile(buildFile);
        dataManagementPanel.search();
    }

    private void fireEvent(String eventType, Map<String, Object> buildFile) {
        Map<String, Object> eventData = new HashMap<String, Object>();
        eventData.put(buildAppFileIdK, buildFile.get(buildAppFileIdK));
        eventData.put(appIdK, buildFile.get(appIdK));
        eventData.put(ncsTypeK, downloadableK);
        eventData.put(operatorK, "");
        eventData.put(spIdK, buildFile.get(spIdK));
        eventData.put(coopUserIdK, spRepositoryService().findSpBySpId((String) buildFile.get(spIdK)).get(coopUserIdK));
        eventData.put(portletIdK, createPortletId(downloadableK));
        eventData.put(ncsPortletTitle, application.getMsg("sp.downloadableApp.build.file.managementTitle"));
        eventData.put(eventOriginK, adminManageBuildFilesEventK);
        PortletEventSender.send(eventData, eventType, application);
    }

    protected static enum BuildFileAction implements Action {
        VIEW_BUILD("view"), EDIT_BUILD("edit"), APPROVE_BUILD("approve"), SUSPEND_BUILD("suspend"),
        RESTORE_BUILD("restore"), REJECT_BUILD("reject"), DELETE_BUILD("delete");

        private final String name;

        BuildFileAction(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    @Override
    protected void initialize() {
        addContainerProperty(application.getMsg("downloadable.buildfile.list.table.buildname.caption"), Label.class, null);
        setColumnWidth(application.getMsg("downloadable.buildfile.list.table.buildname.caption"), 100);
        addContainerProperty(application.getMsg("downloadable.buildfile.list.table.app.id.caption"), Label.class, null);
        setColumnWidth(application.getMsg("downloadable.buildfile.list.table.app.id.caption"), 90);
        addContainerProperty(application.getMsg("downloadable.buildfile.list.table.sp.name.caption"), Button.class, null);
        setColumnWidth(application.getMsg("downloadable.buildfile.list.table.sp.name.caption"), 110);
        addContainerProperty(application.getMsg("downloadable.buildfile.list.table.platform.caption"), Label.class, null);
        setColumnWidth(application.getMsg("downloadable.buildfile.list.table.platform.caption"), 100);
        addContainerProperty(application.getMsg("downloadable.buildfile.list.table.platformversions.caption"), Label.class, null);
        setColumnWidth(application.getMsg("downloadable.buildfile.list.table.platformversions.caption"), 130);
        addContainerProperty(application.getMsg("downloadable.buildfile.list.table.uploadeddate.caption"), Label.class, null);
        setColumnWidth(application.getMsg("downloadable.buildfile.list.table.uploadeddate.caption"), 100);
        addContainerProperty(application.getMsg("downloadable.buildfile.list.table.status.caption"), Label.class, null);
        setColumnWidth(application.getMsg("downloadable.buildfile.list.table.status.caption"), 100);
        addContainerProperty(application.getMsg("downloadable.buildfile.list.table.action.caption"), HorizontalLayout.class, null);
    }

    @Override
    protected void processAction(Map<String, Object> actionData) {
        BuildFileAction action = (BuildFileAction) actionData.get(actionK);
        Map<String, Object> buildFile = (Map<String, Object>) actionData.get(dataK);

        logger.debug("Current action [{}]", action);

        switch (action) {
            case VIEW_BUILD:
                viewBuildFile(buildFile);
                break;
            case EDIT_BUILD:
                editBuildFile(buildFile);
                break;
            case APPROVE_BUILD:
                checkConfirmation(action, buildFile);
                break;
            case SUSPEND_BUILD:
                checkConfirmation(action, buildFile);
                break;
            case RESTORE_BUILD:
                checkConfirmation(action, buildFile);
                break;
            case REJECT_BUILD:
                checkConfirmation(action, buildFile);
                break;
            case DELETE_BUILD:
                checkConfirmation(action, buildFile);
                break;
            default:
                logger.error("Unknown Action [{}], Can't handle ", action);
        }
    }

    public BuildFileDetailsTable(BaseApplication application, DataManagementPanel dataManagementPanel) {
        super(application, dataManagementPanel, ROLE_PROV_DOWNLOADABLE);
    }

    public void reloadTable(List<Map<String, Object>> records) {
        super.reloadTable(records);

        SimpleDateFormat sdf = new SimpleDateFormat("yy/MM/dd - HH:mm", application.getLocale());

        logger.trace("Loading [{}] build files to the table", records.size());

        int rowNumber = 1;

        for (Map<String, Object> record : records) {
            logger.trace("Current build file information [{}]", record);

            List rowData = new ArrayList();
            rowData.add(record.get(buildNameK));
            rowData.add(record.get(appIdK));
            rowData.add(addSpLink(record));
            rowData.add(record.get(platformNameK));
            rowData.add(record.get(platformVersionsK));
            rowData.add(sdf.format(record.get(createdDateK)));
            rowData.add(record.get(statusK));
            rowData.add(createActionButtons(record));
            addItem(rowData.toArray(), rowNumber++);
        }
    }
}
