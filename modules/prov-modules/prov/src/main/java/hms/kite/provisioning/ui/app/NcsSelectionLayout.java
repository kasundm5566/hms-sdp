/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.app;

import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.VerticalLayout;
import hms.kite.provisioning.commons.ui.AbstractBasicDetailsLayout;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.util.AppUtil;
import hms.kite.util.KiteKeyBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.datarepo.RepositoryServiceRegistry.systemConfiguration;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.provisioning.util.AppSlaRoles.APP_PERMISSION_ROLE_PREFIX;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class NcsSelectionLayout extends AbstractBasicDetailsLayout {

    private static final Logger logger = LoggerFactory.getLogger(NcsSelectionLayout.class);

    private Map<String, CheckBox> ncsCheckBoxMap;
    private Map<String, List<CheckBox>> operatorCheckBoxMap;
    private Map<String, Object> app;

    private void init() {
        addStyleName("app-feature-layout");
        setWidth("350px");

        ncsCheckBoxMap = new HashMap<String, CheckBox>();
        operatorCheckBoxMap = new HashMap<String, List<CheckBox>>();

        Map<String, Object> sp = spRepositoryService().findSpById((String) app.get(spIdK));
        List<String> spSelectedNcsTypes = (List<String>) sp.get(KiteKeyBox.spSelectedServicesK);
        Map<String, List<String>> operatorsForNcses = (Map) systemConfiguration().find(supportNcsAndOperatorsK);
        Map<String, List<String>> finalSpSelectedNcsTypesOperatorsMap = new HashMap<String, List<String>>();

        for (String selectedNcsTypeBySp : spSelectedNcsTypes) {
            if (operatorsForNcses.keySet().contains(selectedNcsTypeBySp)) {
                finalSpSelectedNcsTypesOperatorsMap.put(selectedNcsTypeBySp, operatorsForNcses.get(selectedNcsTypeBySp));
            }
        }

        for (Map.Entry<String, List<String>> entry : finalSpSelectedNcsTypesOperatorsMap.entrySet()) {
            String ncsType = entry.getKey();
            CheckBox ncsTypeCheckBox = new CheckBox();

            if(AppUtil.getNcsDisplayName(ncsType) != null && !AppUtil.getNcsDisplayName(ncsType).equals("")){
                ncsTypeCheckBox.setCaption(AppUtil.getNcsDisplayName(ncsType));
            } else {
                ncsTypeCheckBox.setCaption(ncsType);
            }


            ncsCheckBoxMap.put(ncsType, ncsTypeCheckBox);

            List<String> operators = entry.getValue();
            List<CheckBox> operatorCheckBoxList = new ArrayList<CheckBox>();
            for (String operatorName : operators) {
                CheckBox operatorCheckBox = new CheckBox(operatorName);
                operatorCheckBox.setData(operatorName);
                operatorCheckBoxList.add(operatorCheckBox);
            }
            operatorCheckBoxMap.put(ncsType, operatorCheckBoxList);
        }

        loadNcsComponents();
    }

    private void loadNcsComponents() {
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setSpacing(true);

        GridLayout checkBoxLayout = new GridLayout(2, 2 * ncsCheckBoxMap.size());
        checkBoxLayout.setSpacing(true);

        int rowNumber = 0;

        for (final String ncsType : ncsCheckBoxMap.keySet()) {
            final CheckBox ncsTypeChkBox = ncsCheckBoxMap.get(ncsType);
            final List<CheckBox> operatorCheckBoxList = operatorCheckBoxMap.get(ncsType);
            final VerticalLayout operatorCheckBoxLayout = new VerticalLayout();

            for (CheckBox operatorCheckBox : operatorCheckBoxList) {
                operatorCheckBoxLayout.addComponent(operatorCheckBox);
            }
            operatorCheckBoxLayout.setSpacing(true);
            operatorCheckBoxLayout.setVisible(false);

            ncsTypeChkBox.setImmediate(true);
            ncsTypeChkBox.addListener(new Property.ValueChangeListener() {

                @Override
                public void valueChange(final Property.ValueChangeEvent event) {
                    if (isSelected(event, ncsType)) {
                        operatorCheckBoxLayout.setVisible(true);
                    } else {
                        operatorCheckBoxLayout.setVisible(false);
                    }
                }
            });

            checkBoxLayout.addComponent(ncsTypeChkBox, 0, rowNumber);
            checkBoxLayout.addComponent(operatorCheckBoxLayout, 1, rowNumber++);
        }

        verticalLayout.addComponent(checkBoxLayout);

        addComponent(verticalLayout);
    }

    private boolean isSelected(Property.ValueChangeEvent event, String ncsType) {
        return (Boolean) event.getProperty().getValue() && ((List) operatorCheckBoxMap.get(ncsType)).size() > 1;
    }

    private List<Map<String, Object>> getFinalNcsOperatorsSelectedList() {
        List<Map<String, Object>> finalNcsOperatorsSelectedList = new ArrayList<Map<String, Object>>();

        for (Map.Entry<String, CheckBox> ncsCheckBoxEntry : ncsCheckBoxMap.entrySet()) {

            if ((Boolean) ncsCheckBoxEntry.getValue().getValue()) {
                String ncsType = ncsCheckBoxEntry.getKey();

                List<CheckBox> operatorCheckBoxes = operatorCheckBoxMap.get(ncsType);
                if (!operatorCheckBoxes.isEmpty()) {
                    for (CheckBox operatorCheckBox : operatorCheckBoxes) {
                        if ((((Boolean) operatorCheckBox.getValue()) && operatorCheckBoxes.size() > 1)
                                || operatorCheckBoxes.size() == 1) {
                            finalNcsOperatorsSelectedList.add(createSelectedNcs(ncsType, (String) operatorCheckBox.getData()));
                        }
                    }
                } else {
                    finalNcsOperatorsSelectedList.add(createSelectedNcs(ncsType, null));
                }
            }
        }

        return finalNcsOperatorsSelectedList;
    }

    private Map<String, Object> createSelectedNcs(String ncsType, String operator) {
        HashMap<String, Object> data = new HashMap<String, Object>();

        data.put(ncsTypeK, ncsType);
        if (operator != null) {
            data.put(operatorK, operator);
        }
        data.put(statusK, selectedK);

        Map<String, Object> persistedNcs = findPersistedNcs(app, ncsType, operator);

        if (persistedNcs != null) {
            data.put(statusK, persistedNcs.get(statusK));
        }

        return data;
    }

    private Map<String, Object> findPersistedNcs(Map<String, Object> data, String ncsType, String operator) {
        List<Map<String, Object>> ncsList = (List<Map<String, Object>>) data.get(ncsesK);

        if (ncsList != null) {
            for (Map<String, Object> selectedNcs : ncsList) {
                String foundNcsType = (String) selectedNcs.get(ncsTypeK);
                String foundOperator = (String) selectedNcs.get(operatorK);
                if (ncsType.equals(foundNcsType) && (operator == null || operator.equals(foundOperator))) {
                    return selectedNcs;
                }
            }
        }

        return null;
    }

    private CheckBox findCheckBox(String ncsType, String operator) {
        List<CheckBox> operatorCheckBoxes = operatorCheckBoxMap.get(ncsType);
        for (CheckBox operatorCheckBox : operatorCheckBoxes) {
            String op = (String) operatorCheckBox.getData();
            if (operator.equals(op)) {
                return operatorCheckBox;
            }
        }
        return null;
    }

    public NcsSelectionLayout(BaseApplication application, Map<String, Object> app) {
        super(application, APP_PERMISSION_ROLE_PREFIX);

        this.app = app;

        init();
    }

    @Override
    public void reset() {
        throw new UnsupportedOperationException("This function is not implemented");
    }

    @Override
    public void setPermissions() {
        throw new UnsupportedOperationException("This function is not implemented");
    }

    @Override
    public void setData(Map<String, Object> data) {
        List<Map<String, Object>> ncses = (List<Map<String, Object>>) data.get(ncsesK);
        for (Map<String, Object> ncs : ncses) {
            String ncsType = (String) ncs.get(ncsTypeK);
            String operator = (String) ncs.get(operatorK);

            if(!ncsCheckBoxMap.containsKey(ncs.get(ncsTypeK))) {
                continue;
            }

            CheckBox ncsTypeCheckBox = ncsCheckBoxMap.get(ncs.get(ncsTypeK));
            ncsTypeCheckBox.setValue(true);

            if (operator == null) {
                continue;
            }

            CheckBox operatorCheckBox = findCheckBox(ncsType, operator);
            if (operatorCheckBox != null) {
                operatorCheckBox.setValue(true);
            }
        }
    }

    @Override
    public void getData(Map<String, Object> data) {
        data.put(ncsesK, getFinalNcsOperatorsSelectedList());
    }

    @Override
    public void validate() {
        if (getFinalNcsOperatorsSelectedList().isEmpty()) {
            throw new Validator.InvalidValueException(application.getMsg("sp.createApp.appNcsAndOperatorSelectionValidation"));
        }
    }
}
