/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.common;

import com.vaadin.data.Validator;
import com.vaadin.ui.*;
import hms.kite.provisioning.commons.ui.BaseApplication;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ConfirmationDialog {

    private BaseApplication application;
    private String header;
    private String message;
    private ConfirmationListener listener;
    private boolean noteRequired = true;

    public ConfirmationDialog(BaseApplication application, String header, String message, ConfirmationListener listener) {
        this.application = application;
        this.header = header;
        this.message = message;
        this.listener = listener;
    }

    public void showDialog() {
        final Window subWindow = new Window();
        subWindow.setCaption(header);
        subWindow.setModal(true);
        subWindow.setWidth("330px");

        final VerticalLayout layout = (VerticalLayout) subWindow.getContent();
        layout.setSpacing(true);
        layout.setMargin(true);

        Label messageLbl = new Label(message);
        layout.addComponent(messageLbl);


        final TextField textArea = new TextField();

        if (noteRequired) {
            Label noteLbl = new Label(application.getMsg("provisioning.confirm.dialog.note"));

            layout.addComponent(noteLbl);
            textArea.setWidth("280px");
            textArea.setHeight("100px");
            textArea.setRequired(true);
            textArea.setRequiredError(application.getMsg("provisioning.confirm.dialog.note.required"));
            layout.addComponent(textArea);
            layout.setComponentAlignment(textArea, Alignment.MIDDLE_CENTER);
        }

        final Label requiredError = new Label(application.getMsg("provisioning.confirm.required.error.message"));
        requiredError.setVisible(false);
        layout.addComponent(requiredError);
        requiredError.addStyleName("sp-request-fail-notification");
        HorizontalLayout buttonBar = new HorizontalLayout();
        buttonBar.setSpacing(true);
        layout.addComponent(buttonBar);
        layout.setComponentAlignment(buttonBar, Alignment.MIDDLE_CENTER);

        Button yesButton = new Button(application.getMsg("provisioning.confirm.dialog.yes"));
        buttonBar.addComponent(yesButton);
        yesButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    String note = (String) textArea.getValue();
                    if (note == null || note.isEmpty()) {
                        requiredError.setVisible(true);
                        requiredError.setWidth("280px");
                    }
                    textArea.validate();
                    subWindow.getParent().removeWindow(subWindow);
                    listener.onYes(note);
                } catch (Validator.InvalidValueException e) {
                    //Nothing to do
                }

            }
        });

        Button noButton = new Button(application.getMsg("provisioning.confirm.dialog.no"));
        buttonBar.addComponent(noButton);
        noButton.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                subWindow.getParent().removeWindow(subWindow);
            }
        });

        application.getMainWindow().addWindow(subWindow);
    }

    public void setNoteRequired(boolean noteRequired) {
        this.noteRequired = noteRequired;
    }
}
