/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.app;

import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.component.table.AbstractDataManagementPanel;
import hms.kite.provisioning.commons.ui.component.table.DataTable;
import hms.kite.provisioning.commons.ui.component.table.SearchLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.appRepositoryService;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.util.KiteKeyBox.ncsTypeK;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class AppManagementPanel extends AbstractDataManagementPanel {

    private static final Logger logger = LoggerFactory.getLogger(AppManagementPanel.class);

    private String userType;
    private String spId;

    private boolean pendingAppOnly = false;
    private boolean downloadableAppOnly = false;

    @Override
    protected DataTable getDataTable() {
        if (adminUserTypeK.equals(userType)) {
            return new AdminAppDetailsTable(application, this);
        } else {
            if (downloadableAppOnly) {
                return new SpDownloadableAppDetailsTable(application, this);
            } else {
                return new SpAppDetailsTable(application, this);
            }
        }
    }

    @Override
    protected SearchLayout getSearchLayout() {
        AppSearchLayout appSearchLayout = new AppSearchLayout(application, this);
        if (pendingAppOnly) {
            appSearchLayout.setSearchByStatus(false);
        }
        if (downloadableAppOnly) {
            appSearchLayout.setSearchByType(false);
        }
        appSearchLayout.loadComponents();

        return appSearchLayout;
    }

    @Override
    protected void finalizeQueryCriteria(Map<String, Object> queryCriteria) {
        if (pendingAppOnly) {
            queryCriteria.put(statusK, pendingApproveK);
        }
        if (spId != null) {
            queryCriteria.put(spIdK, spId);
        }
        if (downloadableAppOnly) {
            StringBuilder sb = new StringBuilder();
            sb.append(ncsesK).append(".").append(ncsTypeK);
            queryCriteria.put(sb.toString(), downloadableK);
        }
    }

    @Override
    protected int getTotalRecords(Map<String, Object> queryCriteria) {
        return appRepositoryService().countApps(queryCriteria);
    }

    @Override
    protected List<Map<String, Object>> getRecords(int start, int batchSize, Map<String, Object> queryCriteria) {
        Map<String, Object> orderBy = new HashMap<String, Object>();
        orderBy.put(appRequestDateK, -1);
        queryCriteria.put(orderByK, orderBy);
        return appRepositoryService().findAppsRange(queryCriteria, start, batchSize);
    }

    public AppManagementPanel(BaseApplication application, String userType) {
        super(application);

        setNoRecordsFoundError(application.getMsg("manage.app.search.no.search.result"));

        this.userType = userType;
    }

    public void setPendingAppOnly(Boolean pendingAppOnly) {
        this.pendingAppOnly = pendingAppOnly;
    }

    public void setDownloadableAppOnly(boolean downloadableAppOnly) {
        this.downloadableAppOnly = downloadableAppOnly;
    }

    public void setSpId(String spId) {
        this.spId = spId;
    }
}