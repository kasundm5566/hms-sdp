/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.app;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.LiferayUtil;
import hms.kite.provisioning.commons.ui.component.table.DataManagementPanel;
import hms.kite.provisioning.commons.ui.event.PortletEventSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class SpDownloadableAppDetailsTable extends AppDetailsTable {

    private static final Logger logger = LoggerFactory.getLogger(SpDownloadableAppDetailsTable.class);

    private HorizontalLayout createButton(final String message, final String key, final Map<String, Object> app) {
        HorizontalLayout buttonLayout = new HorizontalLayout();

        Button button = new Button(application.getMsg(message));
        button.setImmediate(true);
        button.addStyleName("menu_link");
        button.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                Map<String, Object> eventDataMap = new HashMap<String, Object>();
                eventDataMap.put(ncsTypeK, downloadableK);
                eventDataMap.put(appIdK, app.get(appIdK));
                eventDataMap.put(appStatusK, (app.get(statusK) == null) ? initialK : app.get(statusK));
                eventDataMap.put(spIdK, app.get(spIdK));
                eventDataMap.put(coopUserIdK, spRepositoryService().findSpBySpId((String) app.get(spIdK)).get(coopUserIdK));
                eventDataMap.put(portletIdK, LiferayUtil.createPortletId(downloadableK));
                eventDataMap.put(ncsPortletTitle, application.getMsg("sp.downloadable.applications"));

                if (key != null) {
                    PortletEventSender.send(eventDataMap, key, application);
                }
            }
        });

        buttonLayout.addComponent(button);

        return buttonLayout;
    }

    @Override
    protected void initialize() {
        addContainerProperty(application.getMsg("sp.viewApp.viewTableAppId"), Label.class, null);
        setColumnWidth(application.getMsg("sp.viewApp.viewTableAppId"), 120);
        addContainerProperty(application.getMsg("sp.viewApp.viewTableAppName"), Label.class, null);
        setColumnWidth(application.getMsg("sp.viewApp.viewTableAppName"), 130);
        addContainerProperty(application.getMsg("sp.viewApp.viewTableLastStatusChangedDate"), Label.class, null);
        setColumnWidth(application.getMsg("sp.viewApp.viewTableLastStatusChangedDate"), 140);
        addContainerProperty(application.getMsg("sp.viewApp.viewTableAppStatus"), Label.class, null);
        setColumnWidth(application.getMsg("sp.viewApp.viewTableAppStatus"), 100);
        addContainerProperty(application.getMsg("sp.viewApp.viewTableAction"), HorizontalLayout.class, null);
    }

    @Override
    protected void processAction(Map<String, Object> actionData) {
        throw new UnsupportedOperationException("This function is not supported for " + getClass());
    }

    public SpDownloadableAppDetailsTable(BaseApplication application, DataManagementPanel dataManagementPanel) {
        super(application, dataManagementPanel);
    }

    public void reloadTable(List<Map<String, Object>> records) {
        super.reloadTable(records);

        SimpleDateFormat sdf = new SimpleDateFormat("yy/MM/dd - HH:mm", application.getLocale());

        int rowNumber = 1;

        for (Map<String, Object> record : records) {
            List rowData = new ArrayList();
            rowData.add(new Label((String) record.get(appIdK)));
            rowData.add(new Label((String) record.get(nameK)));
            rowData.add(new Label(sdf.format(record.get(updatedDateK))));
            rowData.add(new Label((String) record.get(statusK)));
            rowData.add(createButton("sp.upload.build.files", provViewNcsEvent, record));
            addItem(rowData.toArray(), rowNumber++);
        }
    }
}
