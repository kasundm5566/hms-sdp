/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.email;

import hms.kite.provisioning.services.httpclient.CurHttpClient;
import org.antlr.stringtemplate.StringTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.provisioning.commons.util.ProvKeyBox.*;


public class AdminEmailRenderer extends AbstractEmailRenderer {

    @Resource
    private CurHttpClient curHttpClient;

    private static final Logger logger = LoggerFactory.getLogger(AdminEmailRenderer.class);

    @Override
    public void render(Map<String, Object> email, Map<String, Object> data) {

        String emailBody = (String) email.get(emailBodyK);
        String emailSubject = (String) email.get(emailSubjectK);

        HashMap<String, Object> event = (HashMap<String, Object>) data.get(eventK);
        logger.info("Email {} is being rendered..", email);

        StringTemplate emailBodyTemplate = new StringTemplate(emailBody);
        emailBodyTemplate.setAttribute(eventK, event);
        emailBodyTemplate.setAttribute(systemK, getANewSystemDataMap());
        addAllKeyValuesToTemplate(emailBodyTemplate, data);

        StringTemplate emailSubjectTemplate = new StringTemplate(emailSubject);
        emailSubjectTemplate.setAttribute(systemK, getANewSystemDataMap());
        addAllKeyValuesToTemplate(emailSubjectTemplate, data);

        email.put(emailBodyK, emailBodyTemplate.toString());
        email.put(emailSubjectK, emailSubjectTemplate.toString());
        List<String> toList = (List<String>) email.get(toAddressListK);
        if (toList == null) {
            toList = new ArrayList<String>();
            email.put(toAddressListK, toList);
        }
        toList.add(getANewSystemDataMap().get(sdpAdminEmailAddressK));

        logger.info("Rendered Email: {}", email);
    }

    private void addAllKeyValuesToTemplate(StringTemplate emailBodyTemplate, Map<String, Object> data) {
        for (String key : data.keySet()) {
            emailBodyTemplate.setAttribute(key, data.get(key));
        }
    }
}
