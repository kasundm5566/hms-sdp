/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.provisioning.ui.sp.ncs;

import com.vaadin.data.Validator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.ui.Component;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.Panel;
import hms.kite.provisioning.Provisioning;
import hms.kite.provisioning.ui.common.ViewMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static hms.kite.provisioning.ProvisioningServiceRegistry.validationRegistry;
import static hms.kite.provisioning.ui.common.ViewMode.EDIT;

/**
 * $LastChangedDate: 2011-07-15 19:12:07 +0530 (Fri, 15 Jul 2011) $
 * $LastChangedBy: chandanaw $
 * $LastChangedRevision: 75004 $
 */
public abstract class NcsConfigurationView {

    private static final Logger logger = LoggerFactory.getLogger(NcsConfigurationView.class);

    protected Provisioning provisioning;

    private ViewMode viewMode = EDIT;

    protected Form form;

    protected void setValueToField(Field field, Object value) {
        if (value != null) {
            field.setValue(value);
        }
    }

    protected NcsConfigurationView() {
        form = new Form();
        form.setWidth("90%");
        form.addStyleName("sp-ncs-form");
    }

    protected Validator createValidator(String regexKey, String errorMsgKey) {
        logger.trace("Creating a regex validator with regex [{}] and  error message [{}]", regexKey, errorMsgKey);

        return new RegexpValidator(validationRegistry().regex(regexKey), provisioning.getMsg(errorMsgKey));
    }

    protected void refreshInChangedMode() {
        switch (viewMode) {
            case EDIT:
                changeToEditMode();
                break;
            case CONFIRM:
                changeToConfirmMode();
                break;
            case VIEW:
                changeToViewMode();
                break;
            case CANCEL:
                changeToCancelView();
                break;
        }
    }

    protected String getSpStringValue(String key) {
        String value = (String) provisioning.getSp().get(key);
        if (value == null) {
            value = "";
        }
        return value;
    }

    protected abstract void setSpSessionValues();

    protected abstract void changeToEditMode();

    protected abstract void changeToConfirmMode();

    protected abstract void changeToViewMode();

    protected abstract void changeToCancelView();

    public abstract Component loadComponents(Panel panel);

    public abstract void saveData();

    public boolean validateData() {
        setSpSessionValues();

        try {
            form.validate();
            form.setComponentError(null);
        } catch (Validator.InvalidValueException e) {
            form.setComponentError(e);
            return false;
        } catch (Exception e) {
            logger.error("Unexpected error occurred while validating");
            return false;
        }

        return true;
    }

    public void changeMode(ViewMode viewMode) {
        this.viewMode = viewMode;
    }
}
