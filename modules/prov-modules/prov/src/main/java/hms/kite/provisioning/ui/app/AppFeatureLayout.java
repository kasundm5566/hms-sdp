/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/

package hms.kite.provisioning.ui.app;

import com.vaadin.ui.CheckBox;
import com.vaadin.ui.GridLayout;
import hms.kite.provisioning.commons.ui.AbstractBasicDetailsLayout;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.I18nProvider;
import hms.kite.provisioning.util.AppSlaRoles;

import java.util.Map;

import static hms.kite.provisioning.ProvisioningServiceRegistry.*;
import static hms.kite.provisioning.util.AppSlaRoles.*;
import static hms.kite.util.KiteKeyBox.*;


/*
* $LastChangedDate: 2011-04-29 14:31:00 +0530 (Fri, 29 Apr 2011) $
* $LastChangedBy: nilushikas $
* $LastChangedRevision: 72646 $
*/

public class AppFeatureLayout extends AbstractBasicDetailsLayout {

    private CheckBox advertisingChkBox;
    private CheckBox governanceChkBox;
    private CheckBox taxChkBox;
    private CheckBox maskingChkBox;
    private GridLayout gridLayout;

    private void setFieldReadOnly(CheckBox field) {
        if (isFieldNotNull(field)) {
            field.setReadOnly(true);
        }
    }

    private void init() {
        addStyleName("app-feature-layout");
        setWidth("350px");

        gridLayout = new GridLayout();
        gridLayout.setColumns(2);
        gridLayout.setRows(5);
        gridLayout.setSpacing(true);
        addComponent(gridLayout);

        createAdvertisingChkBox();
        createGovernanceChkBox();
        createMaskingChkBox();
        createTaxChkBox();
    }

    private void createAdvertisingChkBox() {
        advertisingChkBox = getFieldWithPermission(ADVERTISING, new CheckBox());
        if (isFieldNotNull(advertisingChkBox)) {
            advertisingChkBox.setCaption(application.getMsg("sp.createApp.appAdvertising"));
            advertisingChkBox.setValue(getDefaultAdvertising());
            gridLayout.addComponent(advertisingChkBox, 0, 0);
        } else {
            advertisingChkBox = new CheckBox();
            advertisingChkBox.setValue(getDefaultAdvertising());
        }
    }

    private void createGovernanceChkBox() {
        governanceChkBox = getFieldWithPermission(GOVERN, new CheckBox());
        if (isFieldNotNull(governanceChkBox)) {
            governanceChkBox.setCaption(application.getMsg("sp.createApp.appGovernance"));
            governanceChkBox.setValue(!getDefaultGovern());
            gridLayout.addComponent(governanceChkBox, 1, 0);
        } else {
            governanceChkBox = new CheckBox();
            governanceChkBox.setValue(!getDefaultGovern());
        }
    }

    private void createMaskingChkBox() {
        maskingChkBox = getFieldWithPermission(MASKING, new CheckBox());
        if (isFieldNotNull(maskingChkBox)) {
            maskingChkBox.setCaption(application.getMsg("sp.createApp.appMasking"));
            maskingChkBox.setValue(getDefaultMasking());
            gridLayout.addComponent(maskingChkBox, 0, 1);
        } else {
            maskingChkBox = new CheckBox();
            maskingChkBox.setValue(getDefaultMasking());
        }
    }

    private void createTaxChkBox() {
        taxChkBox = getFieldWithPermission(APPLY_TAX, new CheckBox()); // Reported as a bug this field not in SRS Bug # 17906
        if (isFieldNotNull(taxChkBox)) {
            taxChkBox.setCaption(application.getMsg("sp.createApp.appTax"));
            taxChkBox.setValue(getDefaultTax());
            gridLayout.addComponent(taxChkBox, 1, 1);
        }
    }

    public static AppFeatureLayout createCrAppFeatureLayout(I18nProvider i18n, Map<String, Object> crContext) {
        AppFeatureLayout appFeatureLayout = new AppFeatureLayout((BaseApplication) i18n);
        //TODO decorate as required in CR
        return appFeatureLayout;
    }

    public AppFeatureLayout(BaseApplication application) {
        super(application, AppSlaRoles.APP_PERMISSION_ROLE_PREFIX);

        init();
    }

    public void changeToReadOnlyMode() {
        setFieldReadOnly(advertisingChkBox);
        setFieldReadOnly(governanceChkBox);
        setFieldReadOnly(maskingChkBox);
        setFieldReadOnly(taxChkBox);
    }

    @Override
    public void reset() {
        throw new UnsupportedOperationException("This function is not implemented");
    }

    @Override
    public void setPermissions() {
        setReadWritePermissionToComponent(advertisingChkBox, governanceChkBox, maskingChkBox, taxChkBox);
    }

    @Override
    public void setData(Map<String, Object> data) {
        setValueToField(advertisingChkBox, data.get(advertiseK));
        if(governanceChkBox != null & data.get(governK) != null) {
           setValueToField(governanceChkBox, (!(Boolean) data.get(governK)));
        }
        setValueToField(maskingChkBox, data.get(maskNumberK));
        setValueToField(taxChkBox, data.get(applyTaxK));
    }

    @Override
    public void getData(Map<String, Object> data) {
        getValueFromField(advertisingChkBox, data, advertiseK);
        if(governanceChkBox != null) {
            getValueFromField(governanceChkBox, data, governK, !governanceChkBox.booleanValue());
        }
        getValueFromField(maskingChkBox, data, maskNumberK);
        getValueFromField(taxChkBox, data, applyTaxK);
    }

    public void validate() {
        throw new UnsupportedOperationException("This function is not implemented");
    }
}
