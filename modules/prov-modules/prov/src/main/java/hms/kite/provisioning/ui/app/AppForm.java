/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.app;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import com.vaadin.data.Validator;
import com.vaadin.terminal.UserError;
import com.vaadin.ui.*;
import hms.kite.provisioning.ProvisioningServiceRegistry;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.I18nProvider;
import hms.kite.provisioning.commons.ui.event.PortletEventSender;
import hms.kite.provisioning.commons.util.InAppServiceAdapter;
import hms.kite.provisioning.ui.common.*;
import hms.kite.util.SdpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static hms.kite.datarepo.RepositoryServiceRegistry.*;
import static hms.kite.provisioning.ProvisioningServiceRegistry.appRepositoryService;
import static hms.kite.provisioning.ProvisioningServiceRegistry.buildFileRepositoryService;
import static hms.kite.provisioning.ProvisioningServiceRegistry.eventRouter;
import static hms.kite.provisioning.ProvisioningServiceRegistry.getLogChargingDetails;
import static hms.kite.provisioning.ProvisioningServiceRegistry.isInAppApiAllowed;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.provisioning.ui.common.ViewMode.EDIT;
import static hms.kite.provisioning.ui.common.ViewMode.VIEW;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class AppForm extends Form {

    private static final Logger logger = LoggerFactory.getLogger(AppForm.class);

    private static final ExecutorService caasFailedEmailSendExecutor = Executors.newSingleThreadExecutor();

    private static enum Page {
        PAGE1, PAGE2
    }

    private static enum Action {
        SAVE_AS_DRAFT, SAVE_BY_ADMIN, APPLY_FOR_APPROVAL, APPLY_FOR_AP_APPROVAL, MOVE_TO_LIMITED_PRODUCTION, MOVE_TO_PRODUCTION,
        REJECT, DELETE, SUSPEND, TERMINATE, RESTORE, BACK, NEXT
    }

    private String appStatus;

    private ViewMode viewMode = EDIT;
    private boolean appSaved = false;
    private Page page = Page.PAGE1;
    private Map<String, Object> app;
    private Map<String, Object> unEditedApp;

    private AppBasicDetailsLayout appBasicDetailsLayout;
    private AppFeatureLayout appFeatureLayout;
    private NcsSelectionLayout ncsSelectionLayout;
    private NcsConfigurationLayout ncsConfigurationLayout;
    private Label createStatus = new Label();

    private BaseApplication application;

    public static AppForm newCreateAppForm(Map<String, Object> app, I18nProvider i18n, BaseApplication application) {
        AppForm appForm = new AppForm(app, application);
        appForm.viewMode = EDIT;
        appForm.appSaved = false;
        appForm.setAppStatus(initialK);
        appForm.loadComponents();
        return appForm;
    }

    public static AppForm newViewAppForm(Map<String, Object> app, BaseApplication application) {
        AppForm appForm = new AppForm(app, application);
        appForm.viewMode = VIEW;
        appForm.appSaved = true;
        appForm.setAppStatus((String) app.get(statusK));
        appForm.loadComponents();
        appForm.updateFormDataFromApp(app);
        appForm.changeToReadOnlyMode();
        return appForm;
    }

    public static AppForm newEditAppForm(Map<String, Object> app, I18nProvider i18n, BaseApplication application) {
        AppForm appForm = new AppForm(app, application);
        appForm.viewMode = EDIT;
        appForm.appSaved = true;
        appForm.setAppStatus((String) app.get(statusK));
        appForm.loadComponents();
        appForm.updateFormDataFromApp(app);
        return appForm;
    }

    public AppForm(Map<String, Object> app, BaseApplication application) {
        this.app = app;
        this.unEditedApp = new HashMap<String, Object>(app);
        this.application = application;
        VerticalLayout layout = new VerticalLayout();
        layout.setSpacing(true);
        setLayout(layout);
    }

    private void loadComponents() {
        getLayout().removeAllComponents();
        getFooter().removeAllComponents();

        Panel basicConfigurationsPanel = new Panel(application.getMsg("sp.createApp.basic.configurations"));
        basicConfigurationsPanel.addStyleName("child-panel");

        appBasicDetailsLayout = new AppBasicDetailsLayout(application, app, appStatus);
        if (EDIT.equals(viewMode)) {
            appBasicDetailsLayout.setPermissions();
        }

        getLayout().addComponent(createStatus);
        basicConfigurationsPanel.addComponent(appBasicDetailsLayout);
        getLayout().addComponent(basicConfigurationsPanel);

        appFeatureLayout = new AppFeatureLayout(application);
        if (EDIT.equals(viewMode)) {
            appFeatureLayout.setPermissions();
        }
        basicConfigurationsPanel.addComponent(appFeatureLayout);

        Panel resourcesConfigurationPanel = new Panel(application.getMsg("sp.createApp.ncs.configuration"));
        resourcesConfigurationPanel.addStyleName("child-panel");
        getLayout().addComponent(resourcesConfigurationPanel);
        resourcesConfigurationPanel.setComponentError(null);

        if (viewMode == EDIT) {
            if (page == Page.PAGE1) {
                ncsSelectionLayout = new NcsSelectionLayout(application, app);
                resourcesConfigurationPanel.addComponent(ncsSelectionLayout);
            } else {

                resourcesConfigurationPanel.addStyleName("child-panel");
                ncsConfigurationLayout = new NcsConfigurationLayout(application, app);
                resourcesConfigurationPanel.addComponent(ncsConfigurationLayout);
                getLayout().addComponent(resourcesConfigurationPanel);
            }
        } else if (viewMode == VIEW) {
            NcsConfigurationView ncsConfigurationView = new NcsConfigurationView(application, app);
            resourcesConfigurationPanel.addComponent(ncsConfigurationView);
        }

        VerticalLayout buttonBarContainer = new VerticalLayout();
        buttonBarContainer.setWidth("100%");
        HorizontalLayout buttonBar = createActionButtonLayout();
        buttonBar.addStyleName("button-bar");
        buttonBar.setHeight("50px");
        buttonBarContainer.addComponent(buttonBar);
        buttonBarContainer.setComponentAlignment(buttonBar, Alignment.MIDDLE_CENTER);

        buttonBar.setSpacing(true);
        getFooter().addComponent(buttonBarContainer);
        getFooter().setWidth("100%");
        getFooter().setHeight("40px");
        setSizeFull();
        setWidth("100%");
    }

    private HorizontalLayout createActionButtonLayout() {
        HorizontalLayout buttonBar = new HorizontalLayout();
        buttonBar.addComponent(createActionButton(Action.BACK, this.application.getMsg("sp.createApp.backButton")));
        if (viewMode == VIEW) {
            if (isAppInState(pendingApproveK)) {
                if (application.hasAnyRole("ROLE_PROV_APPROVE_APP")) {
                    buttonBar.addComponent(createActionButton(Action.MOVE_TO_PRODUCTION, this.application.getMsg("sp.createApp.moveToActiveProduction")));
                }
                if (application.hasAnyRole("ROLE_PROV_APPROVE_FOR_LIMITED_PROD")) {
                    buttonBar.addComponent(createActionButton(Action.MOVE_TO_LIMITED_PRODUCTION, this.application.getMsg("sp.createApp.moveToLimitedProduction")));
                    buttonBar.addComponent(createActionButton(Action.REJECT, this.application.getMsg("sp.createApp.reject")));
                }
            } else if (isAppInState(pendingApproveForActiveProductionK)) {
                if (application.hasAnyRole("ROLE_PROV_APPROVE_APP")) {
                    buttonBar.addComponent(createActionButton(Action.MOVE_TO_PRODUCTION, this.application.getMsg("sp.createApp.moveToActiveProduction")));
                }
                if (application.hasAnyRole("ROLE_PROV_SUSPEND_APP")) {
                    buttonBar.addComponent(createActionButton(Action.SUSPEND, this.application.getMsg("sp.createApp.suspend")));
                }
            } else if (isAppInState(limitedProductionK)) {
                if (application.hasAnyRole("ROLE_PROV_APPROVE_APP")) {
                    buttonBar.addComponent(createActionButton(Action.MOVE_TO_PRODUCTION, this.application.getMsg("sp.createApp.moveToActiveProduction")));
                }
                if (application.hasAnyRole("ROLE_PROV_SUSPEND_APP")) {
                    buttonBar.addComponent(createActionButton(Action.SUSPEND, this.application.getMsg("sp.createApp.suspend")));
                }
            } else if (isAppInState(activeProductionK)) {
                if (application.hasAnyRole("ROLE_PROV_SUSPEND_APP")) {
                    buttonBar.addComponent(createActionButton(Action.SUSPEND, this.application.getMsg("sp.createApp.suspend")));
                }
            } else if (isAppInState(suspendK)) {
                if (application.hasAnyRole("ROLE_PROV_TERMINATE_APP")) {
                    buttonBar.addComponent(createActionButton(Action.TERMINATE, this.application.getMsg("sp.createApp.terminate")));
                    buttonBar.addComponent(createActionButton(Action.RESTORE, this.application.getMsg("sp.createApp.restore")));
                }
            } else if (isAppInState(rejectK)) {
                if (application.hasAnyRole("ROLE_PROV_TERMINATE_APP")) {
                    buttonBar.addComponent(createActionButton(Action.DELETE, this.application.getMsg("sp.createApp.delete")));
                }
            }
            if (spUserTypeK.equals(getUserType())) {
                if (isAppInState(limitedProductionK)) {
                    buttonBar.addComponent(createActionButton(Action.APPLY_FOR_AP_APPROVAL, this.application.getMsg("sp.createApp.apply.for.active.production")));
                }
            }
        } else if (viewMode == EDIT) {
            if (spUserTypeK.equals(getUserType())) {
                buttonBar.addComponent(createActionButton(Action.SAVE_AS_DRAFT, this.application.getMsg("sp.createApp.saveButton")));
                if (page == AppForm.Page.PAGE1) {
                    buttonBar.addComponent(createActionButton(Action.NEXT, this.application.getMsg("sp.createApp.nextButton")));
                } else if (page == AppForm.Page.PAGE2) {
                    buttonBar.addComponent(createActionButton(Action.APPLY_FOR_APPROVAL, this.application.getMsg("sp.createApp.applyButton")));
                }
            } else if (adminUserTypeK.equals(getUserType())) {
                buttonBar.addComponent(createActionButton(AppForm.Action.SAVE_BY_ADMIN, this.application.getMsg("sp.createApp.admin.save")));
                if (page == AppForm.Page.PAGE1) {
                    buttonBar.addComponent(createActionButton(Action.NEXT, this.application.getMsg("sp.createApp.nextButton")));
                }
            }
        }

        return buttonBar;
    }

    private String getUserType() {
        return (String) application.getFromSession(userTypeK);
    }

    private boolean isAppInState(String state) {
        String status = (String) app.get(statusK);
        return status != null && status.equals(state);
    }

    private void reloadComponents() {
        loadComponents();
        updateFormDataFromApp(app);
        setComponentError(null);
    }

    private void persistApp(Map<String, Object> app) {
        // for logging purposes only will be removed when app is persisted
        if(getLogChargingDetails()) {
            if(viewMode == ViewMode.EDIT ) {
                if(page == Page.PAGE1) {
                    Map<String, Map<String, Object>> ncsDetails = ncsRepositoryService().findByAppId((String) app.get(appIdK));
                    HashMap<String, Object> ncsLoginDetails = new HashMap<String, Object>();

                    for (String ncs : ncsDetails.keySet()) {
                        if(ncsDetails.containsKey(ncs) && ncsDetails.get(ncs).containsKey(chargingK)) {
                            ncsLoginDetails.put(ncs, ncsDetails.get(ncs).get(chargingK));
                        }
                    }
                    app.put(loggingDataK, ncsLoginDetails);
                } else {
                    app.put(loggingDataK, ncsConfigurationLayout.getNcsLoggingData());
                    checkDuplicateMpesaPayBillNoSps();
                }

            }
        }

        if (appSaved) {
            appRepositoryService().update(app);
        } else {
            appRepositoryService().create(app);
        }
        appSaved = true;
    }

    private void checkDuplicateMpesaPayBillNoSps() {

        Set<String> payBillNoDuplicateSps = new HashSet<String>();

        String mpesaPayBillNo = ncsConfigurationLayout.getMpesaPayBillNo();
        boolean payBillNoNotExist = mpesaPayBillNo == null || mpesaPayBillNo.trim().equals("");
        if(payBillNoNotExist){
            return;
        }

        Map<String, Object> filterMap = new HashMap<String, Object>();
        filterMap.put(chargingK + "." + metaDataK + "." + mpesaPayBillNoK, mpesaPayBillNo);
        List<Map<String, Object>> ncsWithPayBillNo = ncsRepositoryService().filter(filterMap);

        for (Map<String, Object> ncs : ncsWithPayBillNo) {
            try {
                Map<String, Object> app = appRepositoryService().findByAppId((String) ncs.get(appIdK));
                payBillNoDuplicateSps.add((String) app.get(spIdK));
            } catch (SdpException e) {
                logger.error("App [{}] no found in the repository.", ncs.get(appIdK));
                continue;
            }
        }


        // remove current sp's sp-id from duplicate list
        payBillNoDuplicateSps.remove(app.get(spIdK));

        boolean payBillNoDuplicated = payBillNoDuplicateSps.size() > 0;

        if(payBillNoDuplicated) {
            firePayBillNoDuplicatedEvent(payBillNoDuplicateSps, mpesaPayBillNo);
        }
    }

    private void firePayBillNoDuplicatedEvent(Set<String> payBillNoDuplicateSps, String mpesaPayBillNo) {

        Map<String, Object> data = new HashMap<String, Object>();
        data.put(eventNameK, duplicateMpesaPayBillNoEvent);
        data.put(appK, app);
        data.put(spK, spRepositoryService().findSpBySpId((String) app.get(spIdK)));
        data.put(mpesaPayBillNoK, mpesaPayBillNo);

        List<Map<String, Object>>  duplicateSpList = new ArrayList<Map<String, Object>>();
        for (String duplicateSp : payBillNoDuplicateSps) {
            duplicateSpList.add(spRepositoryService().findSpBySpId(duplicateSp));
        }


        data.put(mpesaPayBillDuplicatedSpsK, duplicateSpList);
        eventRouter().fire(data);
    }

    private void showNotification(String notificationMessage) {
        Window.Notification saveMessage = new Window.Notification(notificationMessage, Window.Notification.TYPE_HUMANIZED_MESSAGE);
        application.getMainWindow().showNotification(saveMessage);
    }

    private void showErrorNotification(Validator.InvalidValueException e) {
        setComponentError(e);
    }

    private void showErrorNotification(String errorMessage) {
        setComponentError(new UserError(errorMessage));
    }

    private void sendEventToNcsPortlets(String eventName, String status) {
        Map<String, Object> eventData = new HashMap<String, Object>();
        eventData.put(appIdK, app.get(appIdK));
        eventData.put(statusK, status);
        eventData.put(spIdK, app.get(spIdK));
        eventData.put(coopUserIdK, spRepositoryService().findSpBySpId((String) app.get(spIdK)).get(coopUserIdK));
        eventData.put(ncsesK, app.get(ncsesK));
        eventData.put(provEventAppNameK, app.get(nameK));
        logger.debug("Sending [{}] with status [{}] to [{}] ", new Object[]{eventName, status, app.get(ncsesK)});
        PortletEventSender.send(eventData, eventName, application);
        List<Map<String, Object>> toBeDeletedNcs = findNcsToBeDeleted();
        if (!toBeDeletedNcs.isEmpty()) {
            logger.debug("Deleted ncs found for the {}, sending event to delete ncs", toBeDeletedNcs);
            Map<String, Object> deleteEventData = new HashMap<String, Object>();
            deleteEventData.put(appIdK, app.get(appIdK));
            deleteEventData.put(statusK, deleteK);
            deleteEventData.put(spIdK, app.get(spIdK));
            deleteEventData.put(coopUserIdK, spRepositoryService().findSpBySpId((String) app.get(spIdK)).get(coopUserIdK));
            deleteEventData.put(ncsesK, toBeDeletedNcs);
            deleteEventData.put(provEventAppNameK, app.get(nameK));
            PortletEventSender.send(deleteEventData, provSaveNcsEvent, application);
        }
    }

    private List<Map<String, Object>> findNcsToBeDeleted() {
        if (unEditedApp.get(ncsesK) == null) {
            return Collections.emptyList();
        }

        List<Map<String, Object>> deletedNcsList = new ArrayList<Map<String, Object>>();

        Set<String> oldSet = findNcsSet(unEditedApp);
        Set<String> newSet = findNcsSet(app);

        oldSet.removeAll(newSet);

        if (oldSet.isEmpty()) {
            return Collections.emptyList();
        }

        for (Map<String, Object> oldNcs : ((List<Map<String, Object>>) unEditedApp.get(ncsesK))) {
            String ncsType = (String) oldNcs.get(ncsTypeK);
            String operator = (String) oldNcs.get(operatorK);

            if (oldSet.contains((ncsType + operator))) {
                deletedNcsList.add(oldNcs);
            }
        }

        return deletedNcsList;
    }

    private Set<String> findNcsSet(Map<String, Object> app) {
        Set<String> oldSet = new HashSet<String>();
        List<Map<String, Object>> oldNcsList = (List<Map<String, Object>>) app.get(ncsesK);
        for (Map<String, Object> ncsSummary : oldNcsList) {

            String ncsType = (String) ncsSummary.get(ncsTypeK);
            String operator = (String) ncsSummary.get(operatorK);
            oldSet.add((ncsType + operator));
        }
        return oldSet;
    }

    private void validateInternally() {
        appBasicDetailsLayout.validate();

        if (page == AppForm.Page.PAGE1) {
            ncsSelectionLayout.validate();
        } else if (page == AppForm.Page.PAGE2) {
            // TODO added as a vcity feature
            ncsConfigurationLayout.validate();
        }
    }

    private void validateAndUpdateApp(Map<String, Object> data) {
        validateInternally();
        updateAppFromFormData(data);
    }

    private void goNextLayout() {
        try {
            if (page == AppForm.Page.PAGE1) {
                validateAndUpdateApp(app);
                page = AppForm.Page.PAGE2;
            }
            reloadComponents();
        } catch (Validator.InvalidValueException e) {
            showErrorNotification(e);
        }
    }

    private void updateAppFromFormData(Map<String, Object> data) {
        appBasicDetailsLayout.getData(data);
        appFeatureLayout.getData(data);
        ncsSelectionLayout.getData(data);
    }

    private void changeToReadOnlyMode() {
        appFeatureLayout.changeToReadOnlyMode();
        appBasicDetailsLayout.changeToViewMode();
    }

    public void updateFormDataFromApp(Map<String, Object> data) {
        appBasicDetailsLayout.setData(data);
        appFeatureLayout.setData(data);
        if (ncsSelectionLayout != null) {
            ncsSelectionLayout.setData(data);
        }
    }

    private Boolean isAllNcsesConfigured() {
        List<Map<String, Object>> selectedServices = (List) app.get(ncsesK);
        if (selectedServices == null) {
            return false;
        }

        for (Map<String, Object> selectedService : selectedServices) {
            if (!ncsConfiguredK.equals(selectedService.get(statusK))) {
                return false;
            }
        }
        return true;
    }

    private Button createActionButton(Action action, String caption) {
        Button button = new Button(caption);
        button.setData(action);
        button.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    handleAction((Action) event.getButton().getData());
                } catch (Exception e) {
                    showErrorNotification(application.getMsg("provisioning.system.error"));
                    goBack();
                    logger.error("Unexpected error occurred while handling the action", e);
                }
            }
        });
        return button;
    }

    private void handleAction(Action action) {
        switch (action) {
            case BACK:
                goBack();
                break;
            case NEXT:
                goNext();
                break;
            case SAVE_AS_DRAFT:
                saveAsDraft();
                break;
            case APPLY_FOR_APPROVAL:
                applyForApproval();
                break;
            case APPLY_FOR_AP_APPROVAL:
                applyForActiveProductionFromLimitedProduction();
                break;
            case MOVE_TO_LIMITED_PRODUCTION:
                moveToLimitedProduction();
                break;
            case MOVE_TO_PRODUCTION:
                moveToProduction();
                break;
            case REJECT:
                reject();
                break;
            case SUSPEND:
                suspend();
                break;
            case TERMINATE:
                terminate();
                break;
            case RESTORE:
                restore();
                break;
            case DELETE:
                delete();
                break;
            case SAVE_BY_ADMIN:
                saveByAdmin();
                break;
            default:
                logger.error("Unknown ApplicationAction {}, Can't handle", action);
        }
    }

    private void saveByAdmin() {
        String spName = (String) app.get(createdByK);
        String appName = (String) app.get(nameK);
        try {
            validateAndUpdateApp(app);
            ConfirmationDialog confirmationDialog = new ConfirmationDialog(application,
                    application.getMsg("admin.manageApps.save.confirmation.title"),
                    application.getMsg("admin.manageApps.save.confirmation.message", appName, spName), new ConfirmationListener() {
                @Override
                public void onYes(String note) {
                    try {
                        app.put(remarksK, note);
                        Map<String, Object> newApp = new HashMap<String, Object>(AppForm.this.app);
                        sendEventToNcsPortlets(provSaveNcsEvent, (String) newApp.get(statusK));
                        persistApp(newApp);
                        updateAppInAllLayouts(newApp);
                        fireAppEditByAdminEvent();
                        showNotification(application.getMsg("admin.manageApps.save.success.notification"));
                        viewMode = VIEW;
                        page = Page.PAGE1;
                        reloadComponents();
                        changeToReadOnlyMode();
                    } catch (SdpException e) {
                        logger.error("Unexpected Error occurred.", e);
                        String key = ProvisioningServiceRegistry.errorCodeResolver().findMessageKey(e);
                        showErrorNotification(key);
                    } catch (Exception e) {
                        logger.error("Unexpected Error occurred.", e);
                        showErrorNotification(AppForm.this.application.getMsg("sp.createApp.unknown.errorCode.Description"));
                    }
                }
            });
            confirmationDialog.showDialog();
        } catch (Validator.InvalidValueException e) {
            showErrorNotification(e.getMessage());
        }
    }

    private void applyForActiveProductionFromLimitedProduction() {
        ActiveProdFromLimitedConfirmationDialog activeProductionConfirmationDialog = new ActiveProdFromLimitedConfirmationDialog(application,
                this.application.getMsg("provisioning.active.production.confirm.dialog"),
                this.application.getMsg("provisioning.confirm.dialog.active.production.start.Date.required"),
                new ActiveProductionConfirmationListener() {

                    @Override
                    public void onYesConfirmation(Map<String, Object> map) {
                        try {
                            Map<String, Object> newApp = new HashMap<String, Object>(AppForm.this.app);
                            newApp.put(previousStateK, newApp.get(statusK));
                            newApp.put(activeProductionStartDateK, map.get(activeProductionStartDateK));
                            newApp.put(remarksK, map.get(noteK));
                            appStatus = pendingApproveForActiveProductionK;
                            newApp.put(statusK, appStatus);
                            appRepositoryService().update(newApp);
                            updateAppInAllLayouts(newApp);
                            fireEventRouter();
                            sendEventToNcsPortlets(provSaveNcsEvent, pendingApproveForActiveProductionK);
                            reloadComponents();
                            changeToReadOnlyMode();
                            showNotification(application.getMsg("admin.manageApps.approve.active.production.notification"));
                        } catch (Exception e) {
                            logger.error("Unexpected error occurred while deleting the app", e);
                        }
                    }
                });
        activeProductionConfirmationDialog.showDialog(app);
    }

    private void updateAppInAllLayouts(Map<String, Object> app) {
        this.app = app;
        appBasicDetailsLayout.setData(app);
        appFeatureLayout.setData(app);
        if (ncsSelectionLayout != null) {
            ncsSelectionLayout.setData(app);
        }
        if (ncsConfigurationLayout != null) {
            ncsConfigurationLayout.setApp(app);
        }
    }

    private void saveAsDraft() {
        try {
            validateAndUpdateApp(app);
            Map<String, Object> newApp = new HashMap<String, Object>(this.app);
            newApp.put(appRequestDateK, new Date());
            newApp.put(categoryK, "sdp");
            appStatus = draftK;
            newApp.put(statusK, appStatus);
            sendEventToNcsPortlets(provSaveNcsEvent, draftK);
            persistApp(newApp);
            updateAppInAllLayouts(newApp);
            showNotification(this.application.getMsg("sp.createApp.appSaveAsDraftNotification"));
            viewMode = VIEW;
            page = Page.PAGE1;
            reloadComponents();
            changeToReadOnlyMode();
        } catch (Validator.InvalidValueException e) {
            showErrorNotification(e.getMessage());
        } catch (SdpException e) {
            logger.error("Unexpected Error occurred while trying to Save app ", e);
            String key = ProvisioningServiceRegistry.errorCodeResolver().findMessageKey(e);
            showErrorNotification(key);
        } catch (Exception e) {
            logger.error("Unexpected Error occurred while trying to Save app ", e);
            showErrorNotification(this.application.getMsg("sp.createApp.unknown.errorCode.Description"));
        }
    }

    private void applyForApproval() {
        try {
            Boolean allConfigured = isAllNcsesConfigured();
            if (allConfigured) {
                validateAndUpdateApp(app);
                Map<String, Object> newApp = new HashMap<String, Object>(this.app);
                newApp.put(appRequestDateK, new Date());
                newApp.put(categoryK, "sdp");
                appStatus = pendingApproveK;
                newApp.put(statusK, appStatus);
                sendEventToNcsPortlets(provSaveNcsEvent, pendingApproveK);
                persistApp(newApp);
                updateAppInAllLayouts(newApp);
                createStatus.setValue(this.application.getMsg("sp.createApp.appSaveNotification"));
                createStatus.setStyleName("sp-request-success-notification");
//                showNotification(this.application.getMsg("sp.createApp.appSaveNotification"));
                viewMode = VIEW;
                fireEventRouter();
                fireApplyForApprovalEvent(appSpSendPasswordEvent);

                //TODO add as a feature
                if (isCaasApplication()) {
                    sendCaasApplicationDetailsToAdmin();
                }
                reloadComponents();
                changeToReadOnlyMode();
            } else {
                showErrorNotification(this.application.getMsg("sp.create.app.is.all.ncses.configured.message"));
            }
        } catch (Validator.InvalidValueException e) {
            showErrorNotification(e.getMessage());
        } catch (SdpException e) {
            logger.error("Unexpected Error occurred ", e);
            String key = ProvisioningServiceRegistry.errorCodeResolver().findMessageKey(e);
            application.getMainWindow().showNotification(key);

        } catch (Exception e) {
            logger.error("Unexpected Error occurred ", e);
            application.getMainWindow().showNotification(this.application.getMsg("sp.createApp.unknown.errorCode.Description"));
        }
    }

    private void goNext() {
        try {
            goNextLayout();
        } catch (Exception e) {
            logger.error("UnExpected error occurred while going next window", e);
        }
    }

    private void goBack() {
        if (viewMode == EDIT && page == AppForm.Page.PAGE2) {
            page = AppForm.Page.PAGE1;
            reloadComponents();
        } else {
            application.getUiManager().popScreen();
        }
    }

    private void moveToProduction() {
        Boolean allConfigured = isAllNcsesConfigured();
        if (allConfigured) {
            ActiveProductionConfirmationDialog activeProductionConfirmationDialog = new ActiveProductionConfirmationDialog(application, "Active Confirmation Dialog", "Enter your date..",
                    new ActiveProductionConfirmationListener() {
                        @Override
                        public void onYesConfirmation(Map<String, Object> eventData) {
                            try {
                                Map<String, Object> app = new HashMap<String, Object>(AppForm.this.app);
                                if (sendActiveProductionAtRequestedTimeK.equals(eventData.get("active-production-date"))) {
                                    logger.debug("Application active production date is  at requested date {}", eventData.get(appRequestDateK));
                                    app.put(activeProductionStartDateK, app.get(appRequestDateK));
                                    app.put(statusK, approvedActiveProductionK);
                                } else if (sendActiveProductionAtScheduledTimeK.equals(eventData.get("active-production-date"))) {
                                    logger.debug("Application active production date is  at scheduled date {}", eventData.get(appRequestDateK));
                                    app.put(activeProductionStartDateK, eventData.get(activeProductionStartDateK));
                                    app.put(statusK, approvedActiveProductionK);
                                } else {
                                    logger.debug("send immediately active production");
                                    app.put(activeProductionStartDateK, new Date());
                                    app.put(statusK, activeProductionK);
                                }
                                app.put(remarksK, eventData.get(noteK));
                                sendEventToNcsPortlets(provSaveNcsEvent, activeProductionK);
                                appRepositoryService().update(app);
                                updateAppInAllLayouts(app);
                                fireEventRouter();
                                viewMode = VIEW;
                                reloadComponents();
                                changeToReadOnlyMode();

                                StringBuilder sb = new StringBuilder(application.getMsg("admin.manageApps.approve.active.production.notification"));
                                // Check for existing build files with pending approval.
                                List<Map<String, Object>> buildFileList = buildFileRepositoryService().findByAppId((String) app.get(appIdK));
                                if (buildFileList != null) {
                                    for (Map<String, Object> buildFileMap : buildFileList) {
                                        if (pendingApproveK.equals(buildFileMap.get(statusK))) {
                                            sb.append(" ").append(application.getMsg("admin.manageApps.approve.build.file.exist.with.pending.approve"));
                                            break;
                                        }
                                    }
                                }
                                showNotification(sb.toString());
                            } catch (Validator.InvalidValueException e) {
                                showErrorNotification(e.getMessage());
                            } catch (SdpException e) {
                                logger.error("Unexpected Error occurred.", e);
                                String key = ProvisioningServiceRegistry.errorCodeResolver().findMessageKey(e);
                                showErrorNotification(key);
                            } catch (Exception e) {
                                logger.error("Unexpected Error occurred.", e);
                                showErrorNotification(AppForm.this.application.getMsg("sp.createApp.unknown.errorCode.Description"));
                            }
                        }
                    });
            activeProductionConfirmationDialog.showDialog(app);
        } else {
            showErrorNotification(this.application.getMsg("admin.create.app.is.all.ncses.configured.message"));
        }

    }

    private void moveToLimitedProduction() {
        Boolean allConfigured = isAllNcsesConfigured();
        if (allConfigured) {
            final Map<String, Object> app = new HashMap<String, Object>(this.app);
            appStatus = limitedProductionK;
            app.put(statusK, appStatus);
            appRepositoryService().update(app);
            updateAppInAllLayouts(app);
            sendEventToNcsPortlets(provSaveNcsEvent, limitedProductionK);
            fireEventRouter();
            viewMode = VIEW;
            reloadComponents();
            changeToReadOnlyMode();
            StringBuilder sb = new StringBuilder(application.getMsg("admin.manageApps.approve.limited.production.notification"));
            // Check for existing build files with pending approval.
            List<Map<String, Object>> buildFileList = buildFileRepositoryService().findByAppId((String) app.get(appIdK));
            if (buildFileList != null) {
                for (Map<String, Object> buildFileMap : buildFileList) {
                    if (pendingApproveK.equals(buildFileMap.get(statusK))) {
                        sb.append(" ").append(application.getMsg("admin.manageApps.approve.build.file.exist.with.pending.approve"));
                        break;
                    }
                }
            }
            showNotification(sb.toString());
        } else {
            showErrorNotification(this.application.getMsg("admin.create.app.is.all.ncses.configured.message"));
        }
    }

    private void reject() {
        String spName = (String) app.get(createdByK);
        String appName = (String) app.get(nameK);

        ConfirmationDialog confirmationDialog = new ConfirmationDialog(application,
                application.getMsg("admin.manageApps.reject.confirmation.title"),
                application.getMsg("admin.manageApps.reject.confirmation.message", appName, spName), new ConfirmationListener() {
            @Override
            public void onYes(String note) {
                try {
                    Map<String, Object> app = new HashMap<String, Object>(AppForm.this.app);
                    app.put(previousStateK, app.get(statusK));
                    appStatus = rejectK;
                    app.put(statusK, appStatus);
                    app.put(remarksK, note);
                    appRepositoryService().update(app);
                    updateAppInAllLayouts(app);
                    sendEventToNcsPortlets(provSaveNcsEvent, rejectK);
                    reloadComponents();
                    fireEventRouter();
                    viewMode = VIEW;
                    reloadComponents();
                    changeToReadOnlyMode();
                    showNotification(application.getMsg("admin.manageApps.reject.success.notification"));
                } catch (SdpException e) {
                    logger.error("Unexpected Error occurred.", e);
                    String key = ProvisioningServiceRegistry.errorCodeResolver().findMessageKey(e);
                    showErrorNotification(key);
                } catch (Exception e) {
                    logger.error("Unexpected Error occurred.", e);
                    showErrorNotification(AppForm.this.application.getMsg("sp.createApp.unknown.errorCode.Description"));
                }
            }
        });
        confirmationDialog.showDialog();
    }

    private void delete() {
        String spName = (String) app.get(createdByK);
        String appName = (String) app.get(nameK);

        ConfirmationDialog confirmationDialog = new ConfirmationDialog(application,
                application.getMsg("admin.manageApps.delete.confirmation.title"),
                application.getMsg("admin.manageApps.delete.confirmation.message", appName, spName),
                new ConfirmationListener() {
                    @Override
                    public void onYes(String note) {
                        try {
                            app.put(remarksK, note);
                            app.put(statusK, deleteK);
                            sendEventToNcsPortlets(provSaveNcsEvent, deleteK);
                            appRepositoryService().delete((app.get(appIdK)).toString());
                            showNotification(application.getMsg("admin.manageApps.delete.success.notification"));

                            Map<String, Object> eventData = new HashMap<String, Object>();
                            if (adminUserTypeK.equals(getUserType())) {
                                PortletEventSender.send(eventData, adminManageAppsEvent, application);
                            } else {
                                PortletEventSender.send(eventData, spViewAppEvent, application);
                            }
                            /*viewMode = VIEW;
                            reloadComponents();
                            changeToReadOnlyMode();*/
//                            fireEventRouter();
                        } catch (SdpException e) {
                            logger.error("Unexpected Error occurred.", e);
                            String key = ProvisioningServiceRegistry.errorCodeResolver().findMessageKey(e);
                            showErrorNotification(key);
                        } catch (Exception e) {
                            logger.error("Unexpected Error occurred.", e);
                            showErrorNotification(AppForm.this.application.getMsg("sp.createApp.unknown.errorCode.Description"));
                        }
                    }
                });
        confirmationDialog.showDialog();
    }

    private void suspend() {
        String spName = (String) app.get(createdByK);
        String appName = (String) app.get(nameK);

        ConfirmationDialog confirmationDialog = new ConfirmationDialog(application,
                application.getMsg("admin.manageApps.suspend.confirmation.title"),
                application.getMsg("admin.manageApps.suspend.confirmation.message", appName, spName), new ConfirmationListener() {
            @Override
            public void onYes(String note) {
                try {
                    Map<String, Object> app = new HashMap<String, Object>(AppForm.this.app);
                    app.put(previousStateK, app.get(statusK));
                    appStatus = suspendK;
                    app.put(statusK, appStatus);
                    app.put(remarksK, note);
                    appRepositoryService().update(app);
                    updateAppInAllLayouts(app);
                    sendEventToNcsPortlets(provSaveNcsEvent, suspendK);
                    fireEventRouter();
                    viewMode = VIEW;
                    reloadComponents();
                    changeToReadOnlyMode();
                    showNotification(application.getMsg("admin.manageApps.suspend.success.notification"));
                } catch (SdpException e) {
                    logger.error("Unexpected Error occurred.", e);
                    String key = ProvisioningServiceRegistry.errorCodeResolver().findMessageKey(e);
                    showErrorNotification(key);
                } catch (Exception e) {
                    logger.error("Unexpected Error occurred.", e);
                    showErrorNotification(AppForm.this.application.getMsg("sp.createApp.unknown.errorCode.Description"));
                }
            }
        });
        confirmationDialog.showDialog();
    }

    private void terminate() {
        String spName = (String) app.get(createdByK);
        String appName = (String) app.get(nameK);

        ConfirmationDialog confirmationDialog = new ConfirmationDialog(application,
                application.getMsg("admin.manageApps.terminate.confirmation.title"),
                application.getMsg("admin.manageApps.terminate.confirmation.message", appName, spName), new ConfirmationListener() {
            @Override
            public void onYes(String note) {
                try {
                    Map<String, Object> app = new HashMap<String, Object>(AppForm.this.app);
                    app.put(previousStateK, app.get(statusK));
                    appStatus = terminateK;
                    app.put(statusK, appStatus);
                    app.put(remarksK, note);
                    appRepositoryService().update(app);
                    updateAppInAllLayouts(app);
                    sendEventToNcsPortlets(provSaveNcsEvent, terminateK);
                    fireEventRouter();
                    viewMode = VIEW;
                    reloadComponents();
                    changeToReadOnlyMode();
                    showNotification(application.getMsg("admin.manageApps.terminate.success.notification"));
                } catch (SdpException e) {
                    logger.error("Unexpected Error occurred.", e);
                    String key = ProvisioningServiceRegistry.errorCodeResolver().findMessageKey(e);
                    showErrorNotification(key);
                } catch (Exception e) {
                    logger.error("Unexpected Error occurred.", e);
                    showErrorNotification(AppForm.this.application.getMsg("sp.createApp.unknown.errorCode.Description"));
                }
            }
        });
        confirmationDialog.showDialog();
    }

    private void restore() {
        String spName = (String) app.get(createdByK);
        String appName = (String) app.get(nameK);

        ConfirmationDialog confirmationDialog = new ConfirmationDialog(application,
                application.getMsg("admin.manageApps.restore.confirmation.title"),
                application.getMsg("admin.manageApps.restore.confirmation.message", appName, spName), new ConfirmationListener() {
            @Override
            public void onYes(String note) {
                try {
                    Map<String, Object> app = new HashMap<String, Object>(AppForm.this.app);
                    app.put(statusK, app.get(previousStateK));
                    appStatus = suspendK;
                    app.put(previousStateK, appStatus);
                    app.put(remarksK, note);
                    appRepositoryService().update(app);
                    updateAppInAllLayouts(app);
                    fireEventRouter();
                    sendEventToNcsPortlets(provSaveNcsEvent, restoreK);
                    viewMode = VIEW;
                    reloadComponents();
                    changeToReadOnlyMode();
                    showNotification(application.getMsg("admin.manageApps.restore.success.notification"));
                } catch (SdpException e) {
                    logger.error("Unexpected Error occurred.", e);
                    String key = ProvisioningServiceRegistry.errorCodeResolver().findMessageKey(e);
                    showErrorNotification(key);
                } catch (Exception e) {
                    logger.error("Unexpected Error occurred.", e);
                    showErrorNotification(AppForm.this.application.getMsg("sp.createApp.unknown.errorCode.Description"));
                }
            }
        });
        confirmationDialog.showDialog();
    }

    private void fireEventRouter() {
        if (spUserTypeK.equals(getUserType())) {
            logger.debug("ApplicationAction is not done by the admin, emails will not be fired");
            return;
        }

        String status = (String) app.get(statusK);
        String eventName = "";
        Map<String, Object> data = new HashMap<String, Object>();

        if (suspendK.equals(app.get(previousStateK)) && terminateK.equals(app.get(statusK))) {
            eventName = appTerminateByAdminEvent;
        } else if (suspendK.equals(app.get(previousStateK))) {
            eventName = appRestoreFromSuspendEvent;
        } else if (pendingApproveK.equals(status)) {
            eventName = appRegistrationRequestEvent;
        } else if (limitedProductionK.equals(status)) {
            eventName = appRegistrationApprovedEvent;
        } else if (approvedActiveProductionK.equals(status)) {
            data.put("approvedActiveProduction", true);
            data.put("activeProductionStartDate", app.get(activeProductionStartDateK));
            eventName = appRegistrationApprovedEvent;
        } else if (activeProductionK.equals(status)) {
            eventName = appRegistrationApprovedEvent;
        } else if (suspendK.equals(status)) {
            eventName = appSuspendByAdminEvent;
        } else if (rejectK.equals(status)) {
            eventName = appRegistrationRejectedEvent;
        } else if (deleteK.equals(status)) {
            eventName = appDeleteByAdminEvent;
        }

        data.put(eventNameK, eventName);

        Map<String, Object> removedNcs = new HashMap<String, Object>();
        data.put(removedNcsesK, removedNcs);
        data.put(appK, new HashMap<String, Object>(app));
        data.put(spK, spRepositoryService().findSpBySpId((String) app.get(spIdK)));

        eventRouter().fire(data);
    }

    private void fireApplyForApprovalEvent(String eventName) {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put(eventNameK, eventName);
        data.put(appK, new HashMap<String, Object>(app));
        data.put(spK, spRepositoryService().findSpBySpId((String) app.get(spIdK)));

        processCasSlaAdditionalData(data);

        eventRouter().fire(data);
    }

    private void processCasSlaAdditionalData(Map<String, Object> data) {
        final Optional<Object> casNcsAdditionalData = Optional.fromNullable(ncsConfigurationLayout.getNcsConfiguredAdditionalData().get(casK));
        logger.debug("Cas sla additional data = [{}].", casNcsAdditionalData.or("N/A"));
        if(isInAppApiAllowed() && isCaasApplication() && casNcsAdditionalData.isPresent()) {

            final Map casAdditionalDataMap = (Map) casNcsAdditionalData.get();
            final Optional<Object> inAppAllowedForAppOpt = Optional.fromNullable(casAdditionalDataMap.get(inAppPurchasingAllowedK));

            if(inAppAllowedForAppOpt.isPresent() && (Boolean) inAppAllowedForAppOpt.get() ) {
                final String appId = (String)app.get(appIdK);
                logger.debug("Application [{}] has cas ncs sla, selecting in-app api. creating in-app api for in-app api key.", appId);
                final Optional<String> inAppApiKeyOpt = InAppServiceAdapter.createKey(appId, (String)app.get(nameK), (String) app.get(spIdK));
                logger.debug("Result for in-app api key creation request = [{}].", inAppApiKeyOpt.or("N/A"), appId);
                if(inAppApiKeyOpt.isPresent()) {
                    data.put(provEventAdditionalDataK, ImmutableMap.<String, Object>builder().put(inAppApiKeyK, inAppApiKeyOpt.get()).build());
                }
            }
        }
    }

    private void sendCaasApplicationDetailsToAdmin() {
        caasFailedEmailSendExecutor.submit(new Runnable() {
            @Override
            public void run() {
                Map<String, Object> ncs = null;
                try {
                    Thread.sleep(10000);

                    String appId = (String) app.get(appIdK);
                    ncs = ncsRepositoryService().findByAppIdOperatorNcsType(appId, "", casK);

                    if (ncs != null) {
                        logger.debug("Sending admin email for caas application [{}]", ncs);

                        Map<String, Object> data = new HashMap<String, Object>();
                        data.put(eventNameK, appRegistrationWithCaasRequestEvent);
                        data.put(ncsK, ncs);
                        data.put(appK, new HashMap<String, Object>(app));
                        data.put(spK, spRepositoryService().findSpBySpId((String) app.get(spIdK)));

                        eventRouter().fire(data);
                    } else {
                        logger.error("Sending admin email for caas application [{}] failed permanently", appId);
                    }
                } catch (InterruptedException e) {
                    logger.error("Error occurred while sending admin email for caas application [{}]", ncs);
                }
            }
        });
    }

    private boolean isCaasApplication() {
        for (Map<String, String> currentNcs : (List<Map<String, String>>) app.get(ncsesK)) {
            if (casK.equals(currentNcs.get(ncsTypeK))) {
                return true;
            }
        }

        return false;
    }

    private void fireAppEditByAdminEvent() {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put(eventNameK, appEditByAdminEvent);
        data.put(appK, new HashMap<String, Object>(app));
        data.put(spK, spRepositoryService().findSpBySpId((String) app.get(spIdK)));
        eventRouter().fire(data);
    }

    public void setAppStatus(String appStatus) {
        this.appStatus = appStatus;
    }
}