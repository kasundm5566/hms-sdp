/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.provisioning.repo;

import hms.kite.util.NullObject;

/**
 * $LastChangedDate $
 * $LastChangedBy $
 * $LastChangedRevision $
 */
@Deprecated
public class RepositoryServiceRegistry {

    private static EmailTemplateRepositoryService emailTemplateRepositoryService;

    private RepositoryServiceRegistry () { }

    static {
        setEmailTemplateRepositoryService(new NullObject<EmailTemplateRepositoryService>().get());
    }

    private static void setEmailTemplateRepositoryService(EmailTemplateRepositoryService repo) {
        RepositoryServiceRegistry.emailTemplateRepositoryService = repo;
    }

    public static EmailTemplateRepositoryService sendEmailEventRepositoryService() {
        return emailTemplateRepositoryService;
    }

}
