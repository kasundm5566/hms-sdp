package hms.kite.provisioning.email;

import hms.kite.provisioning.services.httpclient.CurHttpClient;
import org.antlr.stringtemplate.StringTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.util.*;

import static hms.kite.provisioning.commons.util.ProvKeyBox.mpesaPayBillNoK;
import static hms.kite.provisioning.commons.util.ProvKeyBox.sdpAdminEmailAddressK;
import static hms.kite.provisioning.commons.util.ProvKeyBox.systemK;
import static hms.kite.util.KiteKeyBox.*;

public class MpesaPaybillDuplicateEmailRenderer extends AbstractEmailRenderer {

    private static final Logger logger = LoggerFactory.getLogger(MpesaPaybillDuplicateEmailRenderer.class);

    @Override
    public void render(Map<String, Object> email, Map<String, Object> data) {

        String emailBody = (String) email.get(emailBodyK);
        String emailSubject = (String) email.get(emailSubjectK);

        logger.info("Email {} is being rendered..", email);

        StringTemplate emailBodyTemplate = new StringTemplate(emailBody);
        emailBodyTemplate.setAttribute(systemK, getANewSystemDataMap());
        emailBodyTemplate.setAttribute(appK, data.get(appK));
        emailBodyTemplate.setAttribute(spK, data.get(spK));
        emailBodyTemplate.setAttribute(ncsK, createNcsMap(data));

        StringTemplate emailSubjectTemplate = new StringTemplate(emailSubject);

        // email body and subject
        email.put(emailBodyK, emailBodyTemplate.toString());
        email.put(emailSubjectK, emailSubjectTemplate.toString());

        // to address list
        List<String> toList = (List<String>) email.get(toAddressListK);
        if (toList == null) {
            toList = new ArrayList<String>();
            email.put(toAddressListK, toList);
        }
        toList.add(getANewSystemDataMap().get(sdpAdminEmailAddressK));

        logger.info("Rendered Email: {}", email);
    }

    private Map<String, Object> createNcsMap(Map<String, Object> data) {
        HashMap<String, Object> ncsMap = new HashMap<String, Object>();
        ncsMap.put(mpesaPayBillNoK, data.get(mpesaPayBillNoK));


        List<Map<String, Object>> payBillNoDuplicateSps = (List<Map<String, Object>>) data.get(mpesaPayBillDuplicatedSpsK);
        StringBuffer buffer = new StringBuffer();
        final String spNameSeparator = ", ";
        for (Map<String, Object> duplicateSp : payBillNoDuplicateSps) {
            buffer.append(duplicateSp.get(spIdK) + "/" + duplicateSp.get(coopUserNameK)).append(spNameSeparator);
        }

        buffer.replace(buffer.length() - spNameSeparator.length(), buffer.length(), "");

        ncsMap.put(mpesaPayBillDuplicatedSpsK, buffer.toString());

        return ncsMap;
    }
}

