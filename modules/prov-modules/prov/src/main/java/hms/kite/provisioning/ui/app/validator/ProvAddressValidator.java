/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.provisioning.ui.app.validator;

import com.vaadin.data.Validator;
import hms.kite.provisioning.commons.ui.BaseApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static hms.kite.provisioning.ProvisioningServiceRegistry.validationRegistry;
import static hms.kite.provisioning.commons.util.StringConverter.SEPARATOR;
import static hms.kite.provisioning.commons.util.StringConverter.convertToList;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ProvAddressValidator implements Validator {

    private static final Logger logger = LoggerFactory.getLogger(ProvAddressValidator.class);

    private BaseApplication application;
    private String regExKey;
    private String errorMessageKey;
    private String invalidAddress;

    public ProvAddressValidator(BaseApplication application, String regExKey, String errorMessageKey) {
        this.application = application;
        this.regExKey = regExKey;
        this.errorMessageKey = errorMessageKey;
    }

    @Override
    public void validate(Object value) throws InvalidValueException {
        if (!isValid(value)) {
            throw new InvalidValueException(application.getMsg(errorMessageKey, invalidAddress));
        }
    }

    @Override
    public boolean isValid(Object value) {

        try {
            String trimmedValue = value.toString().trim();

            if (trimmedValue.equals("")) {
                return true;
            }

            for (String ipAddress : convertToList(trimmedValue, SEPARATOR)) {
                if (!ipAddress.trim().matches(validationRegistry().regex(regExKey))) {
                    invalidAddress = ipAddress;
                    return false;
                }
            }
        } catch (Exception e) {
            logger.error("Unexpected error occurred while validating", e);
        }
        return true;
    }
}
