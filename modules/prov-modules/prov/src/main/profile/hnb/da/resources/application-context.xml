<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:util="http://www.springframework.org/schema/util"
       xmlns:p="http://www.springframework.org/schema/p"
       xmlns:mongo="http://www.springframework.org/schema/data/mongo"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
           http://www.springframework.org/schema/beans/spring-beans-3.0.xsd
           http://www.springframework.org/schema/context
           http://www.springframework.org/schema/context/spring-context-3.0.xsd
           http://www.springframework.org/schema/util
           http://www.springframework.org/schema/util/spring-util-2.0.xsd
           http://www.springframework.org/schema/data/mongo
           http://www.springframework.org/schema/data/mongo/spring-mongo.xsd">

    <context:annotation-config/>
    <context:component-scan base-package="hms.kite"/>
    <import resource="classpath:sdp-repo-spring.xml"/>

    <util:properties id="defaultValuesProperties" location="classpath:app.properties"/>

    <!--Configuration of Build Files-->

    <mongo:mongo id="cms.mongo" host="${cms.db.host}" port="${cms.db.port}">
        <mongo:options connections-per-host="10"
                       threads-allowed-to-block-for-connection-multiplier="5"
                       connect-timeout="1000"
                       max-wait-time="1500"
                       auto-connect-retry="true"
                       socket-keep-alive="true"
                       socket-timeout="1500"
                       slave-ok="false"
                       write-number="0"
                       write-timeout="0"
                       write-fsync="false"/>
    </mongo:mongo>

    <bean id="cms.mongo.template" class="org.springframework.data.mongodb.core.MongoTemplate">
        <constructor-arg ref="cms.mongo"/>
        <constructor-arg value="${cms.db.name}"/>
    </bean>

    <bean id="repo.mongo.buildFileMongoRepository" class="hms.kite.datarepo.mongodb.BuildFileMongoRepository"
          p:mongoTemplate-ref="cms.mongo.template"/>

    <bean id="repo.service.buildFileRepositoryService" class="hms.kite.datarepo.impl.BuildFileRepositoryServiceImpl"
          p:buildFileMongoRepository-ref="repo.mongo.buildFileMongoRepository"/>

    <!--Configuration of Email Client-->

    <bean id="mailSender" class="org.springframework.mail.javamail.JavaMailSenderImpl"
          p:host="${mail.sender.host}"
          p:port="${mail.sender.port}"
          p:username="${mail.sender.username}"
          p:password="${mail.sender.password}"
          p:protocol="${mail.sender.protocol}"
          p:javaMailProperties-ref="mailProperties"/>

    <bean id="emailClient" class="hms.kite.provisioning.email.SendEmailClient"
          p:emailSendFail="${email.send.fail}"
          p:emailSendSuccess="${email.send.success}"
          p:mailSender-ref="mailSender"/>

    <bean id="emailTemplateRepository" class="hms.kite.provisioning.repo.mongodb.EmailTemplateMongoRepository"
          p:mongoTemplate-ref="repo.mongo.template"/>

    <bean id="emailTemplateRepositoryService" class="hms.kite.provisioning.repo.impl.EmailTemplateRepositoryServiceImpl"
          p:emailTemplateRepository-ref="emailTemplateRepository"/>

    <bean id="curHttpClient" class="hms.kite.provisioning.services.httpclient.CurHttpClient"
          p:url="${common.registration.api.url}" init-method="init"/>

    <!--Application Scheduler Daemon-->

    <bean id="appSchedulerDaemon" class="hms.kite.provisioning.daemon.AppSchedulerDaemon"
          p:eventRouter-ref="eventRouter"/>

    <bean id="appScheduler" class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject" ref="appSchedulerDaemon"/>
        <property name="targetMethod" value="execute"/>
    </bean>

    <bean id="appSchedulerCronTrigger" class="org.springframework.scheduling.quartz.CronTriggerBean">
        <property name="jobDetail" ref="appScheduler"/>
        <property name="startDelay" value="${app.scheduler.daemon.cron.trigger.bean.start.delay.time}"/>
        <property name="cronExpression" value="${app.scheduler.daemon.cron.expression}"/>
    </bean>

    <!--Email Daemon-->

    <bean id="emailSummaryDaemon" class="hms.kite.provisioning.daemon.EmailSummaryDaemon"
          p:eventRouter-ref="eventRouter"
          p:emailFooter="${admin.email.spEmailFooter}"/>

    <bean id="emailScheduler" class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject" ref="emailSummaryDaemon"/>
        <property name="targetMethod" value="execute"/>
    </bean>

    <bean id="emailSchedulerCronTrigger" class="org.springframework.scheduling.quartz.CronTriggerBean">
        <property name="jobDetail" ref="emailScheduler"/>
        <property name="startDelay" value="${admin.email.daemon.cron.trigger.bean.start.delay.time}"/>
        <property name="cronExpression" value="${admin.email.daemon.cron.expression}"/>
    </bean>

    <!-- Application Expritation Scheduler -->

    <bean id="appExpirationDaemon" class="hms.kite.provisioning.daemon.AppExpirationDaemon"/>

    <bean id="appExpirationScheduler" class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject" ref="appExpirationDaemon"/>
        <property name="targetMethod" value="execute"/>
    </bean>

    <bean id="appExpirationTrigger" class="org.springframework.scheduling.quartz.SimpleTriggerBean">
        <property name="jobDetail" ref="appExpirationScheduler"/>
        <property name="startDelay" value="10000"/>
        <property name="repeatInterval" value="60000"/>
    </bean>

    <!-- Build file expiration Scheduler -->

    <bean id="buildFileExpirationDaemon" class="hms.kite.provisioning.daemon.BuildFileExpirationDaemon">
        <property name="timeOut" value="86400000"/>
    </bean>

    <bean id="buildFileExpirationScheduler"
          class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject" ref="buildFileExpirationDaemon"/>
        <property name="targetMethod" value="execute"/>
    </bean>

    <bean id="buildFileExpirationTrigger" class="org.springframework.scheduling.quartz.SimpleTriggerBean">
        <property name="jobDetail" ref="buildFileExpirationScheduler"/>
        <property name="startDelay" value="10000"/>
        <property name="repeatInterval" value="60000"/>
    </bean>

    <bean id="schedulerFactoryBean" class="org.springframework.scheduling.quartz.SchedulerFactoryBean">
        <property name="triggers">
            <util:list>
                <ref bean="emailSchedulerCronTrigger"/>
                <ref bean="appSchedulerCronTrigger"/>
                <ref bean="appExpirationTrigger"/>
                <ref bean="buildFileExpirationTrigger"/>
            </util:list>
        </property>
    </bean>

    <!--Configuration of Place Holder-->
    <bean id="placeholderConfig" class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
        <property name="locations">
            <list>
                <value>classpath:provisioning.properties</value>
            </list>
        </property>
        <property name="ignoreUnresolvablePlaceholders" value="false"/>
    </bean>

    <bean id="validationRegistry" class="hms.kite.provisioning.commons.impl.ValidationRegistryImpl">
        <property name="validators">
            <map>
                <entry key="sp.create.app.description.validator" value="[1-9]"/>
                <entry key="ncsSmsMoTpsRegex" value="[0-9]{1,2}"/>
                <entry key="ncsSmsMoTpdRegex" value="([0-9]{1,6})"/>
                <entry key="ncsSmsMtTpsRegex" value="[0-9]{1,2}"/>
                <entry key="ncsSmsMtTpdRegex" value="([0-9]{1,6})"/>
                <entry key="ncsUssdMoTpsRegex" value="[0-9]{1,2}"/>
                <entry key="ncsUssdMoTpdRegex" value="([0-9]{1,6})"/>
                <entry key="ncsWpTpsRegex" value="[0-9]{1,2}"/>
                <entry key="ncsWpTpdRegex" value="([0-9]{1,6})"/>
                <entry key="ncsCasTpsRegex" value="[0-9]{1,2}"/>
                <entry key="ncsCasTpdRegex" value="([0-9]{1,6})"/>

                <entry key="ncsVdfApiCheckMsisdnTpsRegex" value="[0-9]{1,2}"/>
                <entry key="ncsVdfApiCheckMsisdnTpdRegex" value="([0-9]{1,6})"/>
                <entry key="ncsVdfApiChargingTpsRegex" value="[0-9]{1,2}"/>
                <entry key="ncsVdfApiChargingTpdRegex" value="([0-9]{1,6})"/>
                <entry key="ncsVdfApiChargingMinVal" value="^([0-9][.]{0,2})$|^([0-9])?[.][0-9]*$"/>
                <entry key="ncsVdfApiChargingMaxVal" value="^([0-9]{0,2}[.]{0,3})$|^([0-9]{0,2})?[.][0-9]*$"/>
                <entry key="ncsVdfApiSubscriptionTpsRegex" value="[0-9]{1,2}"/>
                <entry key="ncsVdfApiSubscriptionTpdRegex" value="([0-9]{1,6})"/>

                <entry key="ncsDlAppMaxConDlRegex" value="\b([0-9]|1[0-9]|2[0-5])\b"/>
                <entry key="ncsDlAppMaxDlPerDayRegex" value="\b([0-9]{1,5}|100000)\b"/>
                <entry key="ncsMaxNoOfSubscribersRegex" value="([0-9]{0,5})"/>
                <entry key="ncsSubscriptionTpdRegex" value="([0-9]{1,6})"/>
                <entry key="ncsLbsTpsRegex" value="[0-9]{1,2}"/>
                <entry key="ncsLbsTpdRegex" value="[0-9]{1,6}"/>
                <entry key="sp.createApp.ipAddressRegularExpression"
                       value="^([01]?\d\d?|2[0-4]\d|25[0-5])\.([01]?\d\d?|2[0-4]\d|25[0-5])\.([01]?\d\d?|2[0-4]\d|25[0-5])\.([01]?\d\d?|2[0-4]\d|25[0-5])$"/>
                <entry key="sp.createApp.ipHostAddressFieldRegex" value=".{0,1000}"/>
                <entry key="sp.createApp.blackListRegularExpression" value="^[0-9]{11}"/>
                <entry key="sp.createApp.whiteListRegularExpression" value="^[0-9]{11}"/> <!--Change this according country code, if the country code has two digits length will be 11 then the regex will be ^[0-9]{11}-->
                <entry key="sp.createApp.appname.validator" value="^[A-Za-z0-9_-]{4,32}$"/>
                <entry key="sp.createApp.appDescription.validator" value="^[\s\S]{4,1000}$"/>
                <entry key="app.search.name.validation" value=".{0,32}"/>
                <entry key="sp.search.name.validation" value=".{0,32}"/>
                <entry key="sp.createApp.zero.validation" value="[1-9]([0-9]{0,10})?"/>
                <entry key="sp.createApp.revenue.validation" value="[0-9]?[0-9]?(100)?"/>
            </map>
        </property>
    </bean>

    <bean id="staticDependencyInjector" class="hms.commons.StaticDependencyInjector" init-method="doInitialize">
        <property name="dependencySet">
            <util:map>
                <entry key="portletId" value="${portlet.id}"/>
                <entry key="spInformationExternalUrl" value="${spInformationExternalUrl}"/>
                <entry key="validationRegistry" value-ref="validationRegistry"/>
                <entry key="emailTemplateRepositoryService" value-ref="emailTemplateRepositoryService"/>
                <entry key="eventRouter" value-ref="eventRouter"/>
                <entry key="renders" value-ref="renders"/>
                <entry key="nofRecordsForAppResultTable" value="15"/>
                <entry key="nofRecordsForSpResultTable" value="15"/>
                <entry key="errorCodeResolver" value-ref="defaultErrorCodeResolver"/>
                <entry key="spRepositoryService" value-ref="repo.service.spRepositoryService"/>
                <entry key="appRepositoryService" value-ref="repo.service.appRepositoryService"/>
                <entry key="crRepositoryService" value-ref="repo.service.crRepositoryService"/>
                <entry key="appPublishUrl" value="${application.publish.url}"/>
                <entry key="buildFileRepositoryService" value-ref="repo.service.buildFileRepositoryService"/>
                <entry key="defaultValuesProperties" value-ref="defaultValuesProperties"/>
                <entry key="ncsTypeDisplayNames" value-ref="ncsTypeDisplayNames"/>
            </util:map>
        </property>
        <property name="staticClassSet">
            <util:set>
                <value>hms.kite.provisioning.ProvisioningServiceRegistry</value>
                <value>hms.kite.provisioning.util.AppUtil</value>
            </util:set>
        </property>
    </bean>

    <util:map id="ncsTypeDisplayNames" key-type="java.lang.String" value-type="java.lang.String">
        <entry key="cas" value="${prov.ncs.sla.caas}"/>
        <entry key="wap-push" value="${prov.ncs.sla.wappush}"/>
        <entry key="subscription" value="${prov.ncs.sla.subscription}"/>
        <entry key="sms" value="${prov.ncs.sla.sms}"/>
        <entry key="vdf-apis" value="${prov.ncs.sla.vdfApis}"/>
        <entry key="ussd" value="${prov.ncs.sla.ussd}"/>
        <entry key="downloadable" value="${prov.ncs.sla.downloadable}"/>
        <entry key="lbs" value="${prov.ncs.sla.lbs}"/>
    </util:map>

    <bean id="appCreationService" class="hms.kite.provisioning.services.impl.AppCreationServiceImpl"/>

    <!--Event Publisher Configurations-->
    <bean id="eventRouter" class="hms.kite.provisioning.commons.event.DefaultEventRouter"
          p:eventListeners-ref="eventListeners"
          p:numberOfThreads="${number.of.event.router.threads}"
          p:queueWarnLimit="${blocking.queue.warn.limit}"
          init-method="start"/>

    <bean id="emailSendService" class="hms.kite.provisioning.email.EmailSendService"
          p:supportedEventList-ref="supportedEvents"
          p:emailClient-ref="emailClient"/>

    <bean id="passwordSendService" class="hms.kite.provisioning.email.PasswordSendService"
          p:supportedEventList-ref="supportedEventsForPassword"
          p:emailClient-ref="emailClient"/>

    <bean id="abstractEmailRenderer" abstract="true" class="hms.kite.provisioning.email.AbstractEmailRenderer">
        <property name="systemDataMap" ref="emailSystemDatatMap"/>
    </bean>
    <bean id="spEmailRenderer" class="hms.kite.provisioning.email.SpEmailRenderer"
          parent="abstractEmailRenderer"/>

    <bean id="appEmailRenderer" class="hms.kite.provisioning.email.AppEmailRenderer"
          parent="abstractEmailRenderer"/>

    <bean id="adminEmailRenderer" class="hms.kite.provisioning.email.AdminEmailRenderer"
          parent="abstractEmailRenderer"/>

    <bean id="passwordEmailRenderer" class="hms.kite.provisioning.email.AppEmailRenderer"
          parent="abstractEmailRenderer"/>

    <util:map id="emailSystemDatatMap">
        <entry key="sdp-product-name" value="${email.sdp.product.name}"/>
        <entry key="sdp-admin-email-address" value="${email.sdp.admin.email.address}"/>
        <entry key="provisioning-url" value="${email.provisioning.url}"/>
        <entry key="sp-email-footer" value="${email.sp.email.footer}"/>
    </util:map>

    <bean id="emailRenderChain" class="hms.kite.provisioning.email.EmailRenderChain"
          p:renders-ref="rendersList"/>

    <util:list id="eventListeners" list-class="java.util.ArrayList">
        <ref bean="emailSendService"/>
        <ref bean="passwordSendService"/>
    </util:list>

    <util:list id="supportedEventsForPassword" list-class="java.util.ArrayList">
        <value>app-cr-reset-password</value>
        <value>app-sp-send-password-event</value>
    </util:list>

    <util:list id="supportedEvents" list-class="java.util.ArrayList">
        <value>sp-registration-request-event</value>
        <value>sp-registration-approved-event</value>
        <value>sp-registration-rejected-event</value>
        <value>sp-registration-approved-with-changes-event</value>
        <value>sp-suspend-event</value>
        <value>sp-restore-event</value>
        <value>sp-cr-request-event</value>
        <value>sp-cr-approved-event</value>
        <value>sp-cr-approved-with-changes-event</value>
        <value>sp-cr-rejected-event</value>
        <value>admin-sp-requests-summary-event</value>
        <value>sp-edit-by-admin-event</value>
        <value>app-registration-request-event</value>
        <value>app-registration-with-caas-request-event</value>
        <value>app-registration-approved-event</value>
        <value>app-registration-approved-to-active-event</value>
        <value>app-registration-approved-with-changes-event</value>
        <value>app-registration-approved-with-changes-to-active-event</value>
        <value>app-registration-rejected-event</value>
        <value>app-cr-request-event</value>
        <value>app-cr-approved-event</value>
        <value>app-cr-approved-with-changes-event</value>
        <value>app-cr-rejected-event</value>
        <value>app-cr-applied-to-app-event</value>
        <value>app-admin-requests-summary-event</value>
        <value>app-suspend-by-admin-event</value>
        <value>app-terminate-by-admin-event</value>
        <value>app-terminate-automatic-event</value>
        <value>app-delete-by-admin-event</value>
        <value>app-edit-by-admin-event</value>
        <value>app-restore-from-suspend-event</value>
    </util:list>

    <util:map id="renders" map-class="java.util.HashMap">
        <entry key="sp" value-ref="spEmailRenderer"/>
        <entry key="sp-registration-request-event" value-ref="spEmailRenderer"/>
        <entry key="sp-registration-approved-event" value-ref="spEmailRenderer"/>
        <entry key="sp-registration-rejected-event" value-ref="spEmailRenderer"/>
        <entry key="sp-registration-approved-with-changes-event" value-ref="spEmailRenderer"/>
        <entry key="sp-suspend-event" value-ref="spEmailRenderer"/>
        <entry key="sp-restore-event" value-ref="spEmailRenderer"/>
        <entry key="sp-cr-request-event" value-ref="spEmailRenderer"/>
        <entry key="sp-cr-approved-event" value-ref="spEmailRenderer"/>
        <entry key="sp-cr-approved-with-changes-event" value-ref="spEmailRenderer"/>
        <entry key="sp-cr-rejected-event" value-ref="spEmailRenderer"/>
        <entry key="admin-sp-requests-summary-event" value-ref="adminEmailRenderer"/>
        <entry key="sp-edit-by-admin-event" value-ref="spEmailRenderer"/>
        <entry key="app" value-ref="appEmailRenderer"/>
        <entry key="app-registration-request-event" value-ref="appEmailRenderer"/>
        <entry key="app-registration-with-caas-request-event" value-ref="adminEmailRenderer"/>
        <entry key="app-registration-approved-event" value-ref="appEmailRenderer"/>
        <entry key="app-registration-approved-to-active-event" value-ref="appEmailRenderer"/>
        <entry key="app-registration-approved-with-changes-event" value-ref="appEmailRenderer"/>
        <entry key="app-registration-approved-with-changes-to-active-event" value-ref="appEmailRenderer"/>
        <entry key="app-registration-rejected-event" value-ref="appEmailRenderer"/>
        <entry key="app-cr-request-event" value-ref="appEmailRenderer"/>
        <entry key="app-cr-approved-event" value-ref="appEmailRenderer"/>
        <entry key="app-cr-approved-with-changes-event" value-ref="appEmailRenderer"/>
        <entry key="app-cr-rejected-event" value-ref="appEmailRenderer"/>
        <entry key="app-cr-applied-to-app-event" value-ref="appEmailRenderer"/>
        <entry key="app-cr-reset-password-event" value-ref="passwordEmailRenderer"/>
        <entry key="app-sp-send-password-event" value-ref="passwordEmailRenderer"/>
        <entry key="app-admin-requests-summary-event" value-ref="adminEmailRenderer"/>
        <entry key="app-suspend-by-admin-event" value-ref="appEmailRenderer"/>
        <entry key="app-terminate-by-admin-event" value-ref="appEmailRenderer"/>
        <entry key="app-terminate-automatic-event" value-ref="appEmailRenderer"/>
        <entry key="app-delete-by-admin-event" value-ref="appEmailRenderer"/>
        <entry key="app-edit-by-admin-event" value-ref="appEmailRenderer"/>
        <entry key="app-restore-from-suspend-event" value-ref="appEmailRenderer"/>
    </util:map>

    <util:list id="rendersList" list-class="java.util.ArrayList">
        <ref bean="spEmailRenderer"/>
        <ref bean="appEmailRenderer"/>
        <ref bean="adminEmailRenderer"/>
        <ref bean="passwordEmailRenderer"/>
    </util:list>

    <util:properties id="mailProperties" location="classpath:provisioning.properties"/>

    <bean id="defaultErrorCodeResolver" class="hms.kite.provisioning.commons.ui.DefaultErrorCodeResolver"
          p:defaultKey="System error occurred"
          p:errorCodeKeyMapping-ref="errorCodeKeyMapping"/>
    <!--todo fix this by chandana-->
    <util:map id="errorCodeKeyMapping">
        <entry key="1" value="Internal Error"/>
    </util:map>
</beans>
