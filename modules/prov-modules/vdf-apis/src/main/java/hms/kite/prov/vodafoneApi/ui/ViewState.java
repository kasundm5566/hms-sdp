package hms.kite.prov.vodafoneApi.ui;

/**
 * Created by ayesh on 5/4/17.
 */
public enum ViewState {
    INITIAL_STATE, VIEW_STATE, SAVE_STATE, RECONFIGURE_STATE
}
