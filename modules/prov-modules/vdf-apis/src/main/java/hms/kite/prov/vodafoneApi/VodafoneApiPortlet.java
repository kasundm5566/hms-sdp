package hms.kite.prov.vodafoneApi;

import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.terminal.gwt.server.PortletApplicationContext2;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import hms.kite.prov.vodafoneApi.ui.ViewState;
import hms.kite.prov.vodafoneApi.ui.VodafoneApiSlaForm;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.UiManagerImpl;
import hms.kite.provisioning.commons.util.ProvKeyBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.ncsRepositoryService;
import static hms.kite.provisioning.commons.ui.LiferayUtil.createPortletId;
import static hms.kite.provisioning.commons.util.ProvCommonUtil.getNcsDataFromSessionByAppId;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.util.KiteKeyBox.ncsTypeK;

public class VodafoneApiPortlet extends BaseApplication {
    private static final Logger logger = LoggerFactory.getLogger(VodafoneApiPortlet.class);
    private static final String PORTLET_USER_ID = "portlet_user_id";

    private ThemeDisplay themeDisplay;

    private Panel mainPanel;
    private Panel innerPanel;

    @Override
    protected String getApplicationName() {
        return "vdf-apis";
    }

    @Override
    public void init() {
        logger.debug("This is Vodafone-API portlet  [{}] UserId [{}]", getApplicationName(), getPortletUserId());
        setResourceBundle("US");
        Window main = new Window(this.getMsg("vdfApis.sla.window.title"));
        setMainWindow(main);
        setUiManager(new UiManagerImpl(this));

        VerticalLayout mainLayout = new VerticalLayout();

        mainPanel = new Panel();
        mainLayout.addComponent(mainPanel);

        mainLayout.addStyleName("container-panel");
        mainLayout.setWidth("762px");
        mainLayout.setMargin(true);

        innerPanel = new Panel();
        mainPanel.addComponent(innerPanel);
        innerPanel.addStyleName("child-panel");

        setPortletContext((PortletApplicationContext2) getContext());
        getPortletContext().addPortletListener(this, this);

        getUiManager().clearAll();
        getUiManager().pushScreen(VodafoneApiPortlet.class.getName(), mainLayout);
    }

    @Override
    public void handleEventRequest(EventRequest request, EventResponse response, Window window) {
        super.handleEventRequest(request, response, window);
        Map<String, Object> eventData = (Map<String, Object>) request.getEvent().getValue();

        themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
        Long userId = themeDisplay.getUserId();
        setPortletUserId(userId);

        if(ProvKeyBox.createNcsEvent.equals(request.getEvent().getName())){
            handleCreateNcsRequest(eventData);
        }else if(ProvKeyBox.reConfigureNcsEvent.equals(request.getEvent().getName())){
            handleReconfigureNcsRequest(eventData);
        }else if (saveNcsEvent.equals(request.getEvent().getName())) {
            handleSaveNcsRequest(eventData);
        } else if (viewNcsK.equals(request.getEvent().getName())) {
            handleViewNcsRequest(eventData);
        } else {
            logger.error("Unexpected event [{}], Can't handle", request.getEvent().getName());
        }
    }

    private void handleCreateNcsRequest(Map<String, Object> eventData){
        logger.debug("Vodafone-API NCS SLA Create request received");
        eventData.put(currentEventOriginK, createNcsK);
        createLayout(eventData, ViewState.INITIAL_STATE);
    }

    private void handleReconfigureNcsRequest(Map<String, Object> eventData){
        logger.debug("Vodafone-API NCS SLA Reconfigure request received");

        String ncsType = vdfapiK;
        String appId = (String) eventData.get(appIdK);
        Map<String, Object> sessionData = getNcsDataFromSessionByAppId((String) eventData.get(appIdK), vdfApiSlaDataK, this);

        if (sessionData != null){
            eventData.put(vdfApiSlaDataK, sessionData);
        }else {
            eventData.put(vdfApiSlaDataK, ncsRepositoryService().findByAppIdNcsType(appId, ncsType).get(0));
            addToSession(vdfApiSlaDataK, ncsRepositoryService().findByAppIdNcsType(appId, ncsType).get(0));
        }

        eventData.put(currentEventOriginK, reconfigureNcsK);
        createLayout(eventData, ViewState.RECONFIGURE_STATE);
    }

    private void handleViewNcsRequest(Map<String, Object> eventData){
        logger.debug("Vodafone-API NCS SLA View request received");
        logger.debug("Data [{}]", ncsRepositoryService().findByAppIdNcsType((String) eventData.get(appIdK), vdfapiK));

        String ncsType = vdfapiK;
        String appId = (String) eventData.get(appIdK);
        Map<String, Object> sessionData = getNcsDataFromSessionByAppId((String) eventData.get(appIdK), vdfApiSlaDataK, this);

        if (sessionData != null){
            eventData.put(vdfApiSlaDataK, sessionData);
        }else {
            eventData.put(vdfApiSlaDataK, ncsRepositoryService().findByAppIdNcsType(appId, ncsType).get(0));
        }

        eventData.put(currentEventOriginK, viewNcsK);
        createLayout(eventData, ViewState.VIEW_STATE);
    }

    private void handleSaveNcsRequest(Map<String, Object> eventData){
        logger.debug("Vodafone-API NCS SLA Save request received");
        String operator = "";
        String ncsType = vdfapiK;
        String appId = (String) eventData.get(appIdK);
        String status = (String) eventData.get(statusK);
        Map<String, Object> sessionData = getNcsDataFromSessionByAppId((String) eventData.get(appIdK), vdfApiSlaDataK, this);
        logger.debug("Session Data [{}]", sessionData);

        if (deleteK.equals(status)) {
            ncsRepositoryService().update(appId, ncsType, operator, status);
        }
        if (sessionData != null) {
            sessionData.put(ncsTypeK, ncsType);
            sessionData.put(statusK, eventData.get(statusK));
            ncsRepositoryService().createNcs(sessionData, (String) eventData.get(spIdK));
        } else {
            ncsRepositoryService().update(appId, ncsType, operator, status);
        }

        removeFromSession(vdfApiSlaDataK);
        closePortlet();

        logger.debug("Vodafone-API SLA Saving request processed successfully");
    }

    private void createLayout(Map<String, Object> eventData, ViewState viewState){
        logger.debug("Creating Layout");
        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        innerPanel.setCaption(this.getMsg("vdfApis.sla.window.title"));
        innerPanel.removeAllComponents();
        VodafoneApiSlaForm form = new VodafoneApiSlaForm((String) eventData.get("app-id"), eventData, this, viewState);
        innerPanel.addComponent(form);
    }

    private void setPortletUserId(Long userId) {
        if (getPortletUserId() == null) {
            addToSession(PORTLET_USER_ID, userId);
        }
    }

    private Long getPortletUserId() {
        return (Long) getFromSession(PORTLET_USER_ID);
    }

    public void closePortlet() {
        logger.debug("UserId : [{}] PortletId [{}]", getPortletUserId(), createPortletId(vdfapiK, null));
        themeDisplay.getLayoutTypePortlet().removePortletId(getPortletUserId(), createPortletId(vdfapiK, null));
    }
}
