package hms.kite.prov.vodafoneApi.utils;

/**
 * Created by ayesh on 5/4/17.
 */
public class VodafoneApiSlaRoles {
    public static final String VDFAPIS_PERMISSION_ROLE_PREFIX = "ROLE_PROV_VDFAPIS";
    public static final String MSISDN_CHECK_ALLOWED = "MSISDN_CHECK_ALLOWED";
    public static final String MSISDN_CHECK_TPS = "MSISDN_CHECK_TPS";
    public static final String MSISDN_CHECK_TPD = "MSISDN_CHECK_TPD";
}
