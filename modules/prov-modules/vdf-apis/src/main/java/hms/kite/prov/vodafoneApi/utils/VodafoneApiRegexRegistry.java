package hms.kite.prov.vodafoneApi.utils;

import hms.kite.provisioning.commons.ValidationRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class VodafoneApiRegexRegistry {
    private static ValidationRegistry validationRegistry;

    @Autowired
    public void setValidationRegistry(ValidationRegistry validationRegistry){
        VodafoneApiRegexRegistry.validationRegistry = validationRegistry;
    }

    public static String getRegexPerKey(String key){
        return VodafoneApiRegexRegistry.validationRegistry.regex(key);
    }
}
