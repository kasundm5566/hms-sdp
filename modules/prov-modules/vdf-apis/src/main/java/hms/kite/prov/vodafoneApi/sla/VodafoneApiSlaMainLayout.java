package hms.kite.prov.vodafoneApi.sla;

import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import hms.kite.datarepo.RepositoryServiceRegistry;
import hms.kite.prov.vodafoneApi.utils.VodafoneApiFloatValidator;
import hms.kite.prov.vodafoneApi.utils.VodafoneApiIntegerValidator;
import hms.kite.prov.vodafoneApi.utils.VodafoneApiRegexRegistry;
import hms.kite.prov.vodafoneApi.utils.VodafoneApiSlaRoles;
import hms.kite.provisioning.commons.ui.AbstractBasicDetailsLayout;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.util.KeyNameSpaceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.provisioning.commons.ui.VaadinUtil.setAllReadonly;
import static hms.kite.provisioning.commons.util.ProvKeyBox.vdfApiSlaDataK;
import static hms.kite.util.KiteKeyBox.*;

public class VodafoneApiSlaMainLayout extends AbstractBasicDetailsLayout {
    private static final Logger logger = LoggerFactory.getLogger(VodafoneApiSlaMainLayout.class);

    private static final String HELP_IMAGE = "help.jpg";
    private static final String WIDTH = "260px";
    public static final String HELP_IMG_WIDTH = "910px";
    public static final String HELP_IMG_HEIGHT = "500px";
    public static final String HELP_IMG_STYLE_NAME = "link";
    public static final String HELP_BUTTON_NAME = "Help";
    private static final String CSS_FORMAT = "vdf-api-ncs-form";

    private String coopUserId;
    private String appId;
    private String spId;
    private List<Map> selectedNcses;

    private CheckBox checkMsisdnAllowed;
    private CheckBox sendChargingAllowed;
    private CheckBox sendSubscriptionAllowed;

    private FormLayout msisdnForm;
    private FormLayout chargingForm;
    private FormLayout subscriptionForm;

    private TextField msisdnTpsTxtFld;
    private TextField msisdnTpdTxtFld;

    private TextField chargingTpsTxtFld;
    private TextField chargingTpdTxtFld;
    private TextField chargingMinAmountTxtFld;
    private TextField chargingMaxAmountTxtFld;

    private TextField subscriptionTpsTxtFld;
    private TextField subscriptionTpdTxtFld;

    public VodafoneApiSlaMainLayout(String coopUserId, String spId, String appId, BaseApplication application,
                                    List<Map> selectedNcses) {
        super(application, VodafoneApiSlaRoles.VDFAPIS_PERMISSION_ROLE_PREFIX);

        this.coopUserId = coopUserId;
        this.appId = appId;
        this.spId = spId;
        this.selectedNcses = selectedNcses;
        init();
    }

    public void init(){
        logger.debug("Initiating Vodafone API NCS SLA Main Layout.");
        createMsisdnCheckLayout();
        createChargingLayout();
        createSubscriptionLayout();
        enableLayouts(false, false, false);
        populateCheckBoxesWithListeners();
    }

    public void createMsisdnCheckLayout(){
        checkMsisdnAllowed = new CheckBox();
        checkMsisdnAllowed.setCaption(application.getMsg("vdfApis.sla.checkMsisdn.checkbox.caption"));
        checkMsisdnAllowed.setDescription(application.getMsg("vdfApis.sla.checkMsisdn.checkbox.tooltip"));
        addComponent(checkMsisdnAllowed);

        msisdnForm = new FormLayout();
        msisdnForm.addStyleName(CSS_FORMAT);

        msisdnTpsTxtFld = new TextField();
        msisdnTpsTxtFld.setImmediate(true);
        msisdnTpsTxtFld.setWidth(WIDTH);
        msisdnTpsTxtFld.setCaption(application.getMsg("vdfApis.sla.checkMsisdn.tps.caption"));
        msisdnTpsTxtFld.setDescription(application.getMsg("vdfApis.sla.checkMsisdn.tps.tooltip"));
        msisdnTpsTxtFld.setRequiredError(application.getMsg("vdfApis.sla.checkMsisdn.tps.required.error"));
        msisdnTpsTxtFld.addValidator(new VodafoneApiIntegerValidator(
                application.getMsg("vdfApis.sla.checkMsisdn.tps.error"),
                spId,
                VodafoneApiRegexRegistry.getRegexPerKey("ncsVdfApiCheckMsisdnTpsRegex"),
                checkMsisdnk, vdfApiTpsk));
        msisdnTpsTxtFld.setValidationVisible(true);
        msisdnTpsTxtFld.setRequired(true);

        msisdnTpdTxtFld = new TextField();
        msisdnTpdTxtFld.setImmediate(true);
        msisdnTpdTxtFld.setWidth(WIDTH);
        msisdnTpdTxtFld.setCaption(application.getMsg("vdfApis.sla.checkMsisdn.tpd.caption"));
        msisdnTpdTxtFld.setDescription(application.getMsg("vdfApis.sla.checkMsisdn.tpd.tooltip"));
        msisdnTpdTxtFld.setRequiredError(application.getMsg("vdfApis.sla.checkMsisdn.tpd.required.error"));
        msisdnTpdTxtFld.addValidator(new VodafoneApiIntegerValidator(
                application.getMsg("vdfApis.sla.checkMsisdn.tpd.error"),
                spId,
                VodafoneApiRegexRegistry.getRegexPerKey("ncsVdfApiCheckMsisdnTpdRegex"),
                checkMsisdnk, vdfApiTpdk));
        msisdnTpdTxtFld.setValidationVisible(true);
        msisdnTpdTxtFld.setRequired(true);

        msisdnForm.addComponent(msisdnTpsTxtFld);
        msisdnForm.addComponent(msisdnTpdTxtFld);
        addComponent(msisdnForm);
    }

    public void createChargingLayout(){
        sendChargingAllowed = new CheckBox();
        sendChargingAllowed.setCaption(application.getMsg("vdfAPis.sla.sendCharge.checkbox.caption"));
        sendChargingAllowed.setDescription(application.getMsg("vdfAPis.sla.sendCharge.checkbox.tooltip"));
        addComponent(sendChargingAllowed);

        chargingForm = new FormLayout();
        chargingForm.addStyleName(CSS_FORMAT);

        chargingTpsTxtFld = new TextField();
        chargingTpsTxtFld.setImmediate(true);
        chargingTpsTxtFld.setWidth(WIDTH);
        chargingTpsTxtFld.setCaption(application.getMsg("vdfAPis.sla.sendCharge.tps.caption"));
        chargingTpsTxtFld.setDescription(application.getMsg("vdfAPis.sla.sendCharge.tps.tooltip"));
        chargingTpsTxtFld.setRequiredError(application.getMsg("vdfApis.sla.setCharging.tps.required.error"));
        chargingTpsTxtFld.addValidator(new VodafoneApiIntegerValidator(
                application.getMsg("vdfApis.sla.sendCharging.tps.error"),
                spId,
                VodafoneApiRegexRegistry.getRegexPerKey("ncsVdfApiChargingTpsRegex"),
                sendChargek, vdfApiTpsk));
        chargingTpsTxtFld.setValidationVisible(true);
        chargingTpsTxtFld.setRequired(true);

        chargingTpdTxtFld = new TextField();
        chargingTpdTxtFld.setImmediate(true);
        chargingTpdTxtFld.setWidth(WIDTH);
        chargingTpdTxtFld.setCaption(application.getMsg("vdfApis.sla.sendCharge.tpd.caption"));
        chargingTpdTxtFld.setDescription(application.getMsg("vdfApis.sla.sendCharge.tpd.tooltip"));
        chargingTpdTxtFld.setRequiredError(application.getMsg("vdfApis.sla.setCharging.tpd.required.error"));
        chargingTpdTxtFld.addValidator(new VodafoneApiIntegerValidator(
                application.getMsg("vdfApis.sla.sendCharging.tpd.error"),
                spId,
                VodafoneApiRegexRegistry.getRegexPerKey("ncsVdfApiChargingTpdRegex"),
                sendChargek, vdfApiTpdk));
        chargingTpdTxtFld.setValidationVisible(true);
        chargingTpdTxtFld.setRequired(true);

        chargingMinAmountTxtFld = new TextField();
        chargingMinAmountTxtFld.setImmediate(true);
        chargingMinAmountTxtFld.setWidth(WIDTH);
        chargingMinAmountTxtFld.setCaption(application.getMsg("vdfApis.sla.sendCharge.minAmt.caption"));
        chargingMinAmountTxtFld.setDescription(application.getMsg("vdfApis.sla.sendCharge.minAmt.tooltip"));
        chargingMinAmountTxtFld.setRequiredError(application.getMsg("vdfApis.sla.setCharging.minAmount.required.error"));
        chargingMinAmountTxtFld.addValidator(new RegexpValidator( VodafoneApiRegexRegistry.getRegexPerKey("ncsVdfApiChargingMinVal"), application.getMsg("vdfApis.sla.sendCharging.regex.error")));
        chargingMinAmountTxtFld.addValidator(new VodafoneApiFloatValidator(
                application.getMsg("vdfApis.sla.sendCharging.minAmount.error"),
                spId,
                VodafoneApiRegexRegistry.getRegexPerKey("ncsVdfApiChargingMinVal"),
                sendChargek, chargeMink));
        chargingMinAmountTxtFld.setValidationVisible(true);
        chargingMinAmountTxtFld.setRequired(true);

        chargingMaxAmountTxtFld = new TextField();
        chargingMaxAmountTxtFld.setImmediate(true);
        chargingMaxAmountTxtFld.setWidth(WIDTH);
        chargingMaxAmountTxtFld.setCaption(application.getMsg("vdfApis.sla.sendCharge.maxAmt.caption"));
        chargingMaxAmountTxtFld.setDescription(application.getMsg("vdfApis.sla.sendCharge.maxAmt.tooltip"));
        chargingMaxAmountTxtFld.setRequiredError(application.getMsg("vdfApis.sla.setCharging.maxAmount.required.error"));
        chargingMaxAmountTxtFld.addValidator(new RegexpValidator( VodafoneApiRegexRegistry.getRegexPerKey("ncsVdfApiChargingMaxVal"), application.getMsg("vdfApis.sla.sendCharging.regex.error")));
        chargingMaxAmountTxtFld.addValidator(new VodafoneApiFloatValidator(
                application.getMsg("vdfApis.sla.sendCharging.maxAmount.error"),
                spId,
                VodafoneApiRegexRegistry.getRegexPerKey("ncsVdfApiChargingMaxVal"),
                sendChargek, chargeMaxk));
        chargingMaxAmountTxtFld.setValidationVisible(true);
        chargingMaxAmountTxtFld.setRequired(true);

        chargingForm.addComponent(chargingTpsTxtFld);
        chargingForm.addComponent(chargingTpdTxtFld);
        chargingForm.addComponent(chargingMinAmountTxtFld);
        chargingForm.addComponent(chargingMaxAmountTxtFld);

        addComponent(chargingForm);
    }

    public void createSubscriptionLayout(){
        sendSubscriptionAllowed = new CheckBox();
        sendSubscriptionAllowed.setCaption(application.getMsg("vdfAPis.sla.sendSubs.checkbox.caption"));
        sendSubscriptionAllowed.setDescription(application.getMsg("vdfAPis.sla.sendSubs.checkbox.tooltip"));
        addComponent(sendSubscriptionAllowed);

        subscriptionForm = new FormLayout();
        subscriptionForm.addStyleName(CSS_FORMAT);

        subscriptionTpsTxtFld = new TextField();
        subscriptionTpsTxtFld.setImmediate(true);
        subscriptionTpsTxtFld.setWidth(WIDTH);
        subscriptionTpsTxtFld.setCaption(application.getMsg("vdfAPis.sla.sendSubs.tps.caption"));
        subscriptionTpsTxtFld.setDescription(application.getMsg("vdfAPis.sla.sendSubs.tps.tooltip"));
        subscriptionTpsTxtFld.setRequiredError(application.getMsg("vdfApis.sla.setSubscription.tps.required.error"));
        subscriptionTpsTxtFld.addValidator(new VodafoneApiIntegerValidator(
                application.getMsg("vdfApis.sla.sendSubscription.tps.error"),
                spId,
                VodafoneApiRegexRegistry.getRegexPerKey("ncsVdfApiSubscriptionTpsRegex"),
                sendSubsk, vdfApiTpsk));
        subscriptionTpsTxtFld.setValidationVisible(true);
        subscriptionTpsTxtFld.setRequired(true);

        subscriptionTpdTxtFld = new TextField();
        subscriptionTpdTxtFld.setImmediate(true);
        subscriptionTpdTxtFld.setWidth(WIDTH);
        subscriptionTpdTxtFld.setCaption(application.getMsg("vdfAPis.sla.sendSubs.tpd.caption"));
        subscriptionTpdTxtFld.setDescription(application.getMsg("vdfAPis.sla.sendSubs.tpd.tooltip"));
        subscriptionTpdTxtFld.setRequiredError(application.getMsg("vdfApis.sla.setSubscription.tpd.required.error"));
        subscriptionTpdTxtFld.addValidator(new VodafoneApiIntegerValidator(
                application.getMsg("vdfApis.sla.sendSubscription.tpd.error"),
                spId,
                VodafoneApiRegexRegistry.getRegexPerKey("ncsVdfApiSubscriptionTpdRegex"),
                sendSubsk, vdfApiTpdk));
        subscriptionTpdTxtFld.setValidationVisible(true);
        subscriptionTpdTxtFld.setRequired(true);

        subscriptionForm.addComponent(subscriptionTpsTxtFld);
        subscriptionForm.addComponent(subscriptionTpdTxtFld);

        addComponent(subscriptionForm);
    }

    private void enableLayouts(boolean msisdn, boolean charging, boolean subscription){
        msisdnForm.setEnabled(msisdn);
        chargingForm.setEnabled(charging);
        subscriptionForm.setEnabled(subscription);
    }

    private void populateCheckBoxesWithListeners(){
        addListenerToCheckBox(checkMsisdnAllowed, msisdnForm);
        addListenerToCheckBox(sendChargingAllowed, chargingForm);
        addListenerToCheckBox(sendSubscriptionAllowed, subscriptionForm);
    }

    private void addListenerToCheckBox(CheckBox checkBox, final FormLayout formLayout){
        checkBox.setImmediate(true);
        checkBox.addListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                Boolean enabled = (Boolean) valueChangeEvent.getProperty().getValue();
                if (enabled){
                    formLayout.setEnabled(true);
                }else {
                    formLayout.setEnabled(false);
                }
            }
        });
    }

    @Override
    public void setData(Map<String, Object> data) {
        Map<String, Object> sp = RepositoryServiceRegistry.spRepositoryService().findSpById(spId);

        if(data.get(vdfApiSlaDataK) != null){
            Map<String, Object> vdfAPiSlaData = (Map<String, Object>) data.get(vdfApiSlaDataK);
            logger.debug("Vodafone API SLA Data [{}]", vdfAPiSlaData);

            if((Boolean) vdfAPiSlaData.get(KeyNameSpaceResolver.key(checkMsisdnk, vdfApiAllowedk))){
                Map<String, Object> msisdnData = (Map<String, Object>) vdfAPiSlaData.get(checkMsisdnk);

                checkMsisdnAllowed.setValue(true);
                msisdnTpsTxtFld.setValue((String) msisdnData.get(vdfApiTpsk));
                msisdnTpdTxtFld.setValue((String) msisdnData.get(vdfApiTpdk));
            }else {
                msisdnTpsTxtFld.setValue((String) sp.get(KeyNameSpaceResolver.key(checkMsisdnk, vdfApiTpsk)));
                msisdnTpdTxtFld.setValue((String) sp.get(KeyNameSpaceResolver.key(checkMsisdnk, vdfApiTpdk)));
            }

            if((Boolean) vdfAPiSlaData.get(KeyNameSpaceResolver.key(sendChargek, vdfApiAllowedk))){
                Map<String, Object> chargingData = (Map<String, Object>) vdfAPiSlaData.get(sendChargek);

                sendChargingAllowed.setValue(true);
                chargingTpsTxtFld.setValue((String) chargingData.get(vdfApiTpsk));
                chargingTpdTxtFld.setValue((String) chargingData.get(vdfApiTpdk));
                chargingMinAmountTxtFld.setValue(((Double) chargingData.get(chargeMink)).toString());
                chargingMaxAmountTxtFld.setValue(((Double) chargingData.get(chargeMaxk)).toString());
            }else {
                chargingTpsTxtFld.setValue((String) sp.get(KeyNameSpaceResolver.key(sendChargek, vdfApiTpsk)));
                chargingTpdTxtFld.setValue((String) sp.get(KeyNameSpaceResolver.key(sendChargek, vdfApiTpdk)));
                chargingMinAmountTxtFld.setValue(((Double)sp.get(KeyNameSpaceResolver.key(sendChargek, chargeMink))).toString());
                chargingMaxAmountTxtFld.setValue(((Double) sp.get(KeyNameSpaceResolver.key(sendChargek, chargeMaxk))).toString());
            }

            if ((Boolean) vdfAPiSlaData.get(KeyNameSpaceResolver.key(sendSubsk, vdfApiAllowedk))){
                Map<String, Object> subscriptionData = (Map<String, Object>) vdfAPiSlaData.get(sendSubsk);

                sendSubscriptionAllowed.setValue(true);
                subscriptionTpsTxtFld.setValue((String) subscriptionData.get(vdfApiTpsk));
                subscriptionTpdTxtFld.setValue((String) subscriptionData.get(vdfApiTpdk));
            }else {
                subscriptionTpsTxtFld.setValue((String) sp.get(KeyNameSpaceResolver.key(sendSubsk, vdfApiTpsk)));
                subscriptionTpdTxtFld.setValue((String) sp.get(KeyNameSpaceResolver.key(sendSubsk, vdfApiTpdk)));
            }

        }else {
            if(sp.get(KeyNameSpaceResolver.key(checkMsisdnk, vdfApiTpsk)) != null){
                msisdnTpsTxtFld.setValue((String) sp.get(KeyNameSpaceResolver.key(checkMsisdnk, vdfApiTpsk)));
            }

            if(sp.get(KeyNameSpaceResolver.key(checkMsisdnk, vdfApiTpdk)) != null){
                msisdnTpdTxtFld.setValue((String) sp.get(KeyNameSpaceResolver.key(checkMsisdnk, vdfApiTpdk)));
            }

            if(sp.get(KeyNameSpaceResolver.key(sendChargek, vdfApiTpsk)) != null){
                chargingTpsTxtFld.setValue((String) sp.get(KeyNameSpaceResolver.key(sendChargek, vdfApiTpsk)));
            }

            if(sp.get(KeyNameSpaceResolver.key(sendChargek, vdfApiTpdk)) != null){
                chargingTpdTxtFld.setValue((String) sp.get(KeyNameSpaceResolver.key(sendChargek, vdfApiTpdk)));
            }

            if(sp.get(KeyNameSpaceResolver.key(sendChargek, chargeMink)) != null){
                chargingMinAmountTxtFld.setValue(((Double)sp.get(KeyNameSpaceResolver.key(sendChargek, chargeMink))).toString());
            }

            if(sp.get(KeyNameSpaceResolver.key(sendChargek, chargeMaxk)) != null){
                chargingMaxAmountTxtFld.setValue(((Double) sp.get(KeyNameSpaceResolver.key(sendChargek, chargeMaxk))).toString());
            }

            if(sp.get(KeyNameSpaceResolver.key(sendSubsk, vdfApiTpsk)) != null){
                subscriptionTpsTxtFld.setValue((String) sp.get(KeyNameSpaceResolver.key(sendSubsk, vdfApiTpsk)));
            }

            if(sp.get(KeyNameSpaceResolver.key(sendSubsk, vdfApiTpdk)) != null){
                subscriptionTpdTxtFld.setValue((String) sp.get(KeyNameSpaceResolver.key(sendSubsk, vdfApiTpdk)));
            }
        }
    }

    @Override
    public void getData(Map<String, Object> data) {
        logger.debug("Data Comes Here..... [{}]", data);

        data.put(KeyNameSpaceResolver.key(checkMsisdnk, vdfApiAllowedk), checkMsisdnAllowed.booleanValue());
        if(checkMsisdnAllowed.booleanValue()){
            Map<String, Object> checkMsisdn = new HashMap<String, Object>();
            getValueFromField(msisdnTpsTxtFld, checkMsisdn, vdfApiTpsk);
            getValueFromField(msisdnTpdTxtFld, checkMsisdn, vdfApiTpdk);
            data.put(checkMsisdnk, checkMsisdn);
        }

        data.put(KeyNameSpaceResolver.key(sendChargek, vdfApiAllowedk), sendChargingAllowed.booleanValue());
        if(sendChargingAllowed.booleanValue()){
            Map<String, Object> sendCharging = new HashMap<String, Object>();
            getValueFromField(chargingTpsTxtFld, sendCharging, vdfApiTpsk);
            getValueFromField(chargingTpdTxtFld, sendCharging, vdfApiTpdk);
            sendCharging.put(minAmountK, Double.valueOf((String) chargingMinAmountTxtFld.getValue()));
            sendCharging.put(maxAmountK, Double.valueOf((String) chargingMaxAmountTxtFld.getValue()));
            data.put(sendChargek, sendCharging);
        }

        data.put(KeyNameSpaceResolver.key(sendSubsk, vdfApiAllowedk), sendSubscriptionAllowed.booleanValue());
        if(sendSubscriptionAllowed.booleanValue()){
            Map<String, Object> sendSubscription = new HashMap<String, Object>();
            getValueFromField(subscriptionTpsTxtFld, sendSubscription, vdfApiTpsk);
            getValueFromField(subscriptionTpdTxtFld, sendSubscription, vdfApiTpdk);
            data.put(sendSubsk, sendSubscription);
        }
    }

    @Override
    public void validate() {
        if(checkMsisdnAllowed.booleanValue()){
            validateField(msisdnTpsTxtFld);
            validateField(msisdnTpdTxtFld);
            integerMaxMinValidation(msisdnTpdTxtFld, msisdnTpsTxtFld, application.getMsg("vdfAPis.sla.checkMsisdn.transactions.minmax.error"));
        }

        if(sendChargingAllowed.booleanValue()){
            validateField(chargingTpsTxtFld);
            validateField(chargingTpdTxtFld);
            integerMaxMinValidation(chargingTpdTxtFld, chargingTpsTxtFld, application.getMsg("vdfAPis.sla.sendCharge.transactions.minmax.error"));
            validateField(chargingMinAmountTxtFld);
            validateField(chargingMaxAmountTxtFld);
            doubleMaxMinValidation(chargingMaxAmountTxtFld, chargingMinAmountTxtFld, application.getMsg("vdfAPis.sla.sendCharge.amount.minmax.error"));
        }

        if(sendSubscriptionAllowed.booleanValue()){
            validateField(subscriptionTpsTxtFld);
            validateField(subscriptionTpdTxtFld);
            integerMaxMinValidation(subscriptionTpdTxtFld, subscriptionTpsTxtFld, application.getMsg("vdfAPis.sla.sendSubs.transactions.minmax.error"));
        }
    }

    private void integerMaxMinValidation(Field maxfield, Field minfield, String errorMessage){
        if(Integer.parseInt(minfield.getValue().toString()) >= Integer.parseInt(maxfield.getValue().toString())){
            throw new Validator.InvalidValueException(errorMessage);
        }
    }

    private void doubleMaxMinValidation(Field maxfield, Field minfield, String errorMessage){
        if(Double.parseDouble(minfield.getValue().toString()) >= Double.parseDouble(maxfield.getValue().toString())){
            throw new Validator.InvalidValueException(errorMessage);
        }
    }

    private void validateCheckMsisdnLayout() throws Validator.InvalidValueException {
        if(checkMsisdnAllowed.booleanValue()){
            validateField(msisdnTpsTxtFld);
            validateField(msisdnTpdTxtFld);
        }
    }

    private void validateSendChargingLayout() throws Validator.InvalidValueException{
        if(checkMsisdnAllowed.booleanValue()){
            validateField(chargingTpsTxtFld);
            validateField(chargingTpdTxtFld);
            validateField(chargingMinAmountTxtFld);
            validateField(chargingMaxAmountTxtFld);
        }
    }

    private void validateSendSubscriptionLayout() throws Validator.InvalidValueException{
        if(sendSubscriptionAllowed.booleanValue()){
            validateField(subscriptionTpsTxtFld);
            validateField(subscriptionTpdTxtFld);
        }
    }

    @Override
    public void reset() {
        resetFields("", msisdnTpsTxtFld, msisdnTpdTxtFld, chargingTpsTxtFld, chargingTpdTxtFld, chargingMinAmountTxtFld, chargingMaxAmountTxtFld, subscriptionTpsTxtFld, subscriptionTpdTxtFld);
    }

    @Override
    public void setPermissions() {

    }

    public void setReadonly(boolean isReadOnly) {
        setAllReadonly(isReadOnly, this);
        if (!isReadOnly) {
            setPermissions();
        }
    }
}
