package hms.kite.prov.vodafoneApi.ui;

import com.vaadin.data.Validator;
import com.vaadin.terminal.UserError;
import com.vaadin.ui.*;
import hms.kite.prov.vodafoneApi.VodafoneApiPortlet;
import hms.kite.prov.vodafoneApi.sla.VodafoneApiSlaMainLayout;
import hms.kite.provisioning.commons.ui.VaadinUtil;
import hms.kite.provisioning.commons.ui.event.PortletEventSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.ncsRepositoryService;
import static hms.kite.provisioning.commons.util.ProvCommonUtil.getNcsDataFromSessionByAppId;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

public class VodafoneApiSlaForm extends Form {
    private final Logger logger = LoggerFactory.getLogger(VodafoneApiSlaForm.class);

    private String appId;
    private Map<String, Object> vdfApiNcsSla;
    private VodafoneApiPortlet application;

    private VerticalLayout vdfApiSlaBaseLayout = new VerticalLayout();
    private VodafoneApiSlaMainLayout vodafoneApiSlaMainLayout;

    private Button reset;
    private Button configure;
    private Button back;
    private Button confirm;

    private int currentState = INITIAL_STATE;
    private static final int INITIAL_STATE = 0;
    private static final int RE_CONFIGURE_STATE = 1;
    private static final int VIEW_STATE = 2;

    public VodafoneApiSlaForm(String appId, Map<String, Object> vdfApiNcsSla, VodafoneApiPortlet application, ViewState viewState) {
        this.appId = appId;
        this.vdfApiNcsSla = vdfApiNcsSla;
        this.application = application;
        init(viewState, (String)vdfApiNcsSla.get("coop-user-id"), (String)vdfApiNcsSla.get("sp-id"), (List<Map>) vdfApiNcsSla.get("ncses"));
    }

    public void init(ViewState eventState, String coopUserId, String spId, List<Map> selectedNcses){
        logger.debug("Initialising Vodafone-API-SLA-Form Data [{}]", ncsRepositoryService().findByAppIdOperatorNcsType(appId, null, vdfapiK));
        loadComponents(eventState, coopUserId, spId, selectedNcses);
        addStyleName("vdf-api-ncs-form");
        populateButtons();
    }

    public void loadComponents(ViewState eventState, String coopUserId, String spId, List<Map> selectedNcses){
        logger.debug("Loading Components to the Vodafone-API-SLA-Form.");

        vodafoneApiSlaMainLayout = new VodafoneApiSlaMainLayout(coopUserId, spId, appId, application, selectedNcses);
        vodafoneApiSlaMainLayout.setWidth("80%");
        vodafoneApiSlaMainLayout.setData(vdfApiNcsSla);

        reset = new Button(application.getMsg("vdfApis.ncs.create.sla.form.reset"));
        configure = new Button(application.getMsg("vdfApis.ncs.create.sla.form.configure"));
        back = new Button(application.getMsg("vdfApis.ncs.create.sla.form.back"));
        confirm = new Button(application.getMsg("vdfApis.ncs.create.sla.form.confirm"));

        setLayout(vdfApiSlaBaseLayout);

        if(eventState.equals(ViewState.INITIAL_STATE)){
            vdfApiNcsSla = new HashMap<String, Object>();
            setButtonVisibility(true, true, true, false);
            setReadOnly(false);
        }else if(eventState.equals(ViewState.RECONFIGURE_STATE)){
            setButtonVisibility(true, false, false, true);
            setReadOnly(false);
        }else if (eventState.equals(ViewState.VIEW_STATE)){
            setButtonVisibility(true, false, false, false);
            vodafoneApiSlaMainLayout.setReadonly(true);
            setReadOnly(true);
        }

        vdfApiSlaBaseLayout.setVisible(true);

        vdfApiSlaBaseLayout.addComponent(vodafoneApiSlaMainLayout);
        vdfApiSlaBaseLayout.setComponentAlignment(vodafoneApiSlaMainLayout, Alignment.MIDDLE_CENTER);
    }

    private void populateButtons(){
        reset.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    logger.debug("Vodafone Sla reset form is executed");
                    vodafoneApiSlaMainLayout.reset();
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while resetting", e);
                }
            }
        });

        back.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    logger.debug("Vodafone-API SLA form is being re-edited before confirming it");
                    if (currentState == INITIAL_STATE) {
                        PortletEventSender.send(new HashMap(), provRefreshEvent, application);
                        application.closePortlet();
                    } else if (currentState == VIEW_STATE) {
                        PortletEventSender.send(new HashMap(), provRefreshEvent, application);
                        application.closePortlet();
                    } else {
                        intialConfState();
                    }
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while going back", e);
                }
            }
        });

        configure.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                logger.debug("Vodafone-API SLA form is being configured");
                try {
                    VaadinUtil.cleanErrorComponents(VodafoneApiSlaForm.this);
                    vodafoneApiSlaMainLayout.validate();
                    reConfigurationState();
                    logger.debug("Configured Data Map: {}", vdfApiNcsSla);
                } catch (Validator.InvalidValueException ex) {
                    logger.debug("Validation failed  {}", ex.getMessage());
                    setComponentError(ex);
                } catch (Exception ex) {
                    logger.error("Unknown Exception Occurred: {}", ex);
                    setComponentError(new UserError(application.getMsg("vdfApi.ncs.create.sla.form.unknown.error")));
                }
            }
        });

        confirm.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                try {
                    logger.debug("Vodafone-API Sla form is being confirmed by the user");
                    vodafoneApiSlaMainLayout.validate();
                    confirmed();
                } catch (Validator.InvalidValueException ex) {
                    logger.debug("Validation failed  {}", ex.getMessage());
                    setComponentError(ex);
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while saving Vodafone-API sla", e);
                    setComponentError(new UserError(application.getMsg("vdfApi.ncs.create.sla.form.unknown.error")));
                }
            }
        });

        HorizontalLayout buttonLayout = new HorizontalLayout();

        for (Button button : new Button[]{back, reset, configure, confirm}) {
            button.setImmediate(true);
            buttonLayout.addComponent(button);
        }
        buttonLayout.setSpacing(true);
        this.getFooter().setWidth("100%");
        this.getFooter().addComponent(buttonLayout);
        ((HorizontalLayout) this.getFooter()).setComponentAlignment(buttonLayout, Alignment.MIDDLE_CENTER);
    }

    private void confirmed(){
        Map<String, Object> sessionData = getNcsDataFromSessionByAppId(appId, vdfApiSlaDataK, application);
        if(sessionData == null){
            sessionData = new HashMap<String, Object>();
        }
        vodafoneApiSlaMainLayout.getData(sessionData);
        sessionData.put(appIdK, appId);
        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put(ncsTypeK, vdfapiK);
        dataMap.put(statusK, successK);
        PortletEventSender.send(dataMap, ncsConfiguredK, application);
        logger.debug("NCS-SLA Data : [{}]", sessionData);
        application.addToSession(vdfApiSlaDataK, sessionData);
        application.closePortlet();

    }

    private void reConfigurationState() {
        logger.info("Re Configuration State");
        currentState = RE_CONFIGURE_STATE;
        vodafoneApiSlaMainLayout.setReadonly(true);
        setButtonVisibility(true, false, false, true);
    }

    private void intialConfState(){
        logger.info("Initial Configuration State");
        currentState = INITIAL_STATE;
        vodafoneApiSlaMainLayout.setReadonly(false);
        setButtonVisibility(true, true, true, false);
    }

    private void setButtonVisibility(boolean backVisible, boolean resetVisible, boolean configureVisible, boolean confirmVisible){
        back.setVisible(backVisible);
        reset.setVisible(resetVisible);
        configure.setVisible(configureVisible);
        confirm.setVisible(confirmVisible);
    }
}
