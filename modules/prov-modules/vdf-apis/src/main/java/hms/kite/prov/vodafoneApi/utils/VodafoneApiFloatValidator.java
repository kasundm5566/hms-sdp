package hms.kite.prov.vodafoneApi.utils;

import com.vaadin.data.Validator;
import hms.kite.datarepo.RepositoryServiceRegistry;
import hms.kite.util.KeyNameSpaceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.util.KiteKeyBox.chargeMaxk;
import static hms.kite.util.KiteKeyBox.chargeMink;

/**
 * Created by ayesh on 5/8/17.
 */
public class VodafoneApiFloatValidator implements Validator {
    private static final Logger logger = LoggerFactory.getLogger(VodafoneApiFloatValidator.class);

    private String errorMessage;
    private String spId;
    private String regEx;
    private String ncsType;
    private String limitType;
    private float limit;

    public VodafoneApiFloatValidator(String errorMessage, String spId, String regEx, String ncsType, String limitType) {
        this.errorMessage = errorMessage;
        this.spId = spId;
        this.regEx = regEx;
        this.ncsType = ncsType;
        this.limitType = limitType;
    }

    @Override
    public void validate(Object value) throws InvalidValueException {
        if (!isValid(value)) {
            throw new InvalidValueException(errorMessage + " " + limit);
        }
    }

    @Override
    public boolean isValid(Object value) {
        System.out.println("Inside the Validator ");
        try {
            Map<String, Object> sp = RepositoryServiceRegistry.spRepositoryService().findSpById(spId);
            limit = ((Double)sp.get(KeyNameSpaceResolver.key(ncsType, limitType))).floatValue();
            if(limitType.equals(chargeMaxk)){
                return Double.valueOf((String) value).floatValue() <= limit;
            }else if (limitType.equals(chargeMink)){
                return limit <= Double.valueOf((String) value).floatValue();
            }else {
                return false;
            }
        }catch (Exception e){
            logger.debug("Unexpected error occurred in validating the field.");
            return false;
        }
    }
}
