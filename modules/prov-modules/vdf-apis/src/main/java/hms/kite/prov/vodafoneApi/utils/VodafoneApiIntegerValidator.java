package hms.kite.prov.vodafoneApi.utils;

import com.vaadin.data.Validator;
import hms.kite.datarepo.RepositoryServiceRegistry;
import hms.kite.util.KeyNameSpaceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.regex.Pattern;

public class VodafoneApiIntegerValidator implements Validator{
    private static final Logger logger = LoggerFactory.getLogger(VodafoneApiIntegerValidator.class);

    private String errorMessage;
    private String spId;
    private String regEx;
    private String ncsType;
    private String limitType;
    private int limit;

    public VodafoneApiIntegerValidator(String errorMessage, String spId, String regEx, String ncsType, String limitType) {
        this.errorMessage = errorMessage;
        this.spId = spId;
        this.regEx = regEx;
        this.ncsType = ncsType;
        this.limitType = limitType;
    }

    @Override
    public void validate(Object value) throws InvalidValueException {
        if (!isValid(value)) {
            throw new InvalidValueException(errorMessage + " " + limit);
        }
    }

    @Override
    public boolean isValid(Object value) {
        try {
            Map<String, Object> sp = RepositoryServiceRegistry.spRepositoryService().findSpById(spId);
            limit = Integer.parseInt((String) sp.get(KeyNameSpaceResolver.key(ncsType, limitType)));
            if(!Pattern.matches(regEx, (String) value)){
                return false;
            }else {
                return Integer.parseInt((String) value) <= limit;
            }
        }catch (Exception e){
            logger.debug("Unexpected error occurred in validating the field.");
            return false;
        }
    }
}
