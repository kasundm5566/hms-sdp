/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.liferay;

import hms.common.registration.api.request.UserDetailByUsernameRequestMessage;
import hms.common.registration.api.response.BasicUserResponseMessage;
import hms.common.rest.util.JsonBodyProvider;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static hms.common.registration.api.common.RequestType.USER_BASIC_DETAILS;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CurClient {

    private static final Logger logger = LoggerFactory.getLogger(CurClient.class);

    private static final String APPLICATION_CONTENT_TYPE = "application/json";

    private static BasicUserResponseMessage readJsonResponse(Map<String, Object> m) throws ParseException {
        return BasicUserResponseMessage.convertFromMap(m);
    }

    private static Map<String, Object> readJsonResponse(Response response) {
        InputStream is = (InputStream) response.getEntity();
        Class aClass = Map.class;
        JsonBodyProvider jsonProvider = new JsonBodyProvider();
        try {
            return (Map<String, Object>) jsonProvider.readFrom(aClass, null, null, MediaType.APPLICATION_JSON_TYPE, null, is);
        } catch (IOException e) {
            logger.error("can't create the json object", e);
            return null;
        }
    }

    /**
     * Sample client implementation for get user basic details request
     */
    public static BasicUserResponseMessage getBasicUserDetailRequest(String curUrl, String username) {
        try {
            List<Object> providers = new ArrayList<Object>();
            providers.add(new JsonBodyProvider());
            WebClient webClient = WebClient
                    .create(curUrl + "/user/detail/username", providers);

            webClient.header("Content-Type", APPLICATION_CONTENT_TYPE);
            webClient.accept(APPLICATION_CONTENT_TYPE);
            UserDetailByUsernameRequestMessage reqMessage = new UserDetailByUsernameRequestMessage();

            reqMessage.setRequestType(USER_BASIC_DETAILS);
            reqMessage.setRequestedTimeStamp(new Date());
            reqMessage.setUserName(username);
            reqMessage.setModuleName("provisioning");

            logger.debug("Sending Get User Basic Details Request " + reqMessage.toString());

            javax.ws.rs.core.Response response = webClient.post(reqMessage.convertToMap());

            return readJsonResponse(readJsonResponse(response));
        } catch (Exception e) {
            logger.error("Error in reading user details", e);
            return null;
        }
    }
}
