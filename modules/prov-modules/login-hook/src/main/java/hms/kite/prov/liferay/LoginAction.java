/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.liferay;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.NoSuchUserException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Contact;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.LayoutSet;
import com.liferay.portal.model.User;
import com.liferay.portal.security.auth.AutoLogin;
import com.liferay.portal.security.auth.AutoLoginException;
import com.liferay.portal.service.*;
import com.liferay.portal.util.PortalUtil;
import hms.common.registration.api.common.UserType;
import hms.common.registration.api.response.BasicUserResponseMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

import static com.liferay.portal.service.UserLocalServiceUtil.getUserByScreenName;
import static hms.common.registration.api.util.RestApiKeys.CORPORATE_USER_NAME;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class LoginAction implements AutoLogin {

    private static final Logger logger = LoggerFactory.getLogger(LoginAction.class);

    private static String curUrl;
    private static String noCredentialUrl;

    static {
        InputStream resourceAsStream = null;
        try {
            Properties properties = new Properties();
            resourceAsStream = LoginAction.class.getClassLoader().getResourceAsStream("portal.properties");
            properties.load(resourceAsStream);
            curUrl = properties.getProperty("cur.basic.detail.request.url");
            noCredentialUrl = properties.getProperty("login.hook.no.credential.url");
        } catch (IOException e) {
            logger.error("Error reading file portal.properties from class path", e);
        } finally {
            if (resourceAsStream != null) {
                try {
                    resourceAsStream.close();
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
    }

    private String[] registerAndGetCredentials(HttpServletRequest req, String username, long companyId) throws PortalException, SystemException {
        try {
            logger.debug("Getting credentials from Existing User [{}]", username);
            return getCredentialsFromExistingUser(req, getUserByScreenName(companyId, username));
        } catch (NoSuchUserException e) {
            logger.error("User [{}] not found in the system. Exception [{}]", username, e);
            return registerNonExistingUserAndGetCredentials(req, username, companyId);
        }
    }

    private String[] getCredentialsFromExistingUser(HttpServletRequest req, User existingUser) {
        logger.info("Getting credentials for user [{}]", existingUser.getScreenName());

        String userName = existingUser.getScreenName();
        long userId = existingUser.getUserId();
        String password = existingUser.getPassword();

        setSessionAttributeFromCUR(req, userName);

        return new String[]{Long.toString(userId), password, Boolean.TRUE.toString()};
    }

    private String[] registerNonExistingUserAndGetCredentials(HttpServletRequest req, String username, long companyId) throws SystemException, PortalException {
        logger.info("Register new user with username [{}]", username);

        String coopUserId = setSessionAttributeFromCUR(req, username);

        HashSet sessionRoles = (HashSet) req.getSession().getAttribute("kite-user-roles");

        if (sessionRoles != null && !sessionRoles.isEmpty()) {
            logger.info("Create user with username {} company-id {}", username, companyId);
            long userId = createProvUser(username, companyId, coopUserId);
            return new String[]{String.valueOf(userId), username, Boolean.TRUE.toString()};
        } else {
            logger.info("User will not create, since no user roles available.");
            req.setAttribute(AUTO_LOGIN_REDIRECT, noCredentialUrl);
            return null;
        }
    }

    private String setSessionAttributeFromCUR(HttpServletRequest req, String username) {
        BasicUserResponseMessage basicUserDetailRequest = CurClient.getBasicUserDetailRequest(curUrl, username);
        List<String> roles = basicUserDetailRequest.getRoles();

        logger.trace("============ Received user details ============");
        logger.trace("kite-user-name [{}]", username);
        logger.trace("kite-user-roles [{}]", roles);
        logger.trace("additional-data [{}]", basicUserDetailRequest.getAdditionalDataMap());
        logger.trace("===============================================");
        String coopUserId = basicUserDetailRequest.getCorporateUserId();
        HashSet<String> rolesSet = new HashSet<String>(roles);
        String userType = getUserType(basicUserDetailRequest);
        String corporateUserName = basicUserDetailRequest.getAdditionalData(CORPORATE_USER_NAME);

        if ("SP-USER-TYPE".equals(userType)) {
            req.getSession().setAttribute("coop-user-id", coopUserId);
            req.getSession().setAttribute("coop-user-name", corporateUserName);
        }
        req.getSession().setAttribute("kite-user-name", username);
        req.getSession().setAttribute("kite-user-roles", rolesSet);
        req.getSession().setAttribute("kite-user-type", userType);

        logger.debug("coop-user-id [{}]", coopUserId);
        logger.debug("coop-user-name [{}]", corporateUserName);
        logger.debug("kite-user-type [{}]", userType);

        return coopUserId;
    }

    private long createProvUser(String username, long companyId, String coopUserId) throws SystemException, PortalException {

        //todo handle transaction

        long userId = CounterLocalServiceUtil.increment();
        long contactId = CounterLocalServiceUtil.increment();
        long groupId = CounterLocalServiceUtil.increment();

        User user1 = UserLocalServiceUtil.createUser(userId);
        user1.setCreateDate(new Date());
        user1.setModifiedDate(new Date());
        user1.setCompanyId(companyId);
        user1.setContactId(contactId);
        user1.setFirstName(username);
        user1.setLastName(coopUserId);
        user1.setPassword(username + "I don't know");
        user1.setPasswordEncrypted(false);
        user1.setEmailAddress(username + "@email.com");

        user1.setScreenName(username);
        user1.setActive(true);
        user1.setAgreedToTermsOfUse(true);
        user1.setReminderQueryQuestion("This what u need to ask ....");
        user1.setReminderQueryAnswer("This is what u need to remember.....");

        UserLocalServiceUtil.addUser(user1);

        Contact contact1 = ContactLocalServiceUtil.createContact(contactId);
        contact1.setCompanyId(companyId);
        contact1.setUserId(userId);
        contact1.setUserName(username);
        contact1.setCreateDate(new Date());
        contact1.setModifiedDate(new Date());

        contact1.setAccountId(AccountLocalServiceUtil.getAccounts(0, 1).get(0).getAccountId());
        contact1.setFirstName(username);
        contact1.setLastName(coopUserId);
        ContactLocalServiceUtil.addContact(contact1);

        UserLocalServiceUtil.addGroupUsers(GroupLocalServiceUtil.getGroup(companyId, "Guest").getGroupId(), new long[]{userId});

        Group group = GroupLocalServiceUtil.createGroup(groupId);

        group.setCreatorUserId(userId);

        long classNameId = ClassNameLocalServiceUtil.getClassNameId("com.liferay.portal.model.User");
        group.setClassNameId(classNameId);
        group.setClassPK(userId);//; use to get the private page count.
        group.setActive(true);
        group.setCompanyId(companyId);
        group.setName("" + username);
        group.setType(1);
        group.setFriendlyURL("/" + username);
        GroupLocalServiceUtil.addGroup(group);

        LayoutSet publicLayoutSet = LayoutSetLocalServiceUtil.createLayoutSet(CounterLocalServiceUtil.increment());
        publicLayoutSet.setCompanyId(companyId);
        publicLayoutSet.setGroupId(groupId);
        publicLayoutSet.setThemeId("classic");
        publicLayoutSet.setColorSchemeId("01");
        publicLayoutSet.setPrivateLayout(false);
        publicLayoutSet.setPageCount(0);
        LayoutSetLocalServiceUtil.updateLayoutSet(publicLayoutSet);

        LayoutSet privateLayoutSet = LayoutSetLocalServiceUtil.createLayoutSet(CounterLocalServiceUtil.increment());
        privateLayoutSet.setCompanyId(companyId);
        privateLayoutSet.setGroupId(groupId);
        privateLayoutSet.setThemeId("classic");
        privateLayoutSet.setColorSchemeId("01");
        privateLayoutSet.setPrivateLayout(true);
        privateLayoutSet.setPageCount(0);
        LayoutSetLocalServiceUtil.updateLayoutSet(privateLayoutSet);

        return userId;
    }

    private String getUserType(BasicUserResponseMessage basicUserResponseMessage) {
        String userType;

        UserType curUserType = basicUserResponseMessage.getType();
        logger.debug("User type given by CUR is {}", curUserType);
        switch (curUserType) {
            case ADMIN_USER:
                userType = "ADMIN-USER-TYPE";
                break;
            case CORPORATE:
            case SUB_CORPORATE:
                userType = "SP-USER-TYPE";
                break;
            default:
                userType = "UNKNOWN";
        }
        return userType;
    }

    public String[] login(HttpServletRequest req, HttpServletResponse resp) throws AutoLoginException {
        long companyId = PortalUtil.getCompanyId(req);

        try {
            HttpSession session = req.getSession();
            String username = (String) session.getAttribute("com.liferay.portal.servlet.filters.sso.cas.CASFilterLOGIN");

            logger.info("Validating session for user {}", username);

            User user = PortalUtil.getUser(req);

            if (user != null) {
                logger.info("User is Existing. No validation required.");
                return new String[]{Long.toString(user.getUserId()), user.getPassword(), Boolean.TRUE.toString()};
            } else if (username != null) {
                if (req.getSession().getAttribute("cur.credentials.for.user") != null) {
                    logger.debug("It seems I have authenticated this guy.");
                    return (String[]) req.getSession().getAttribute("cur.credentials.for.user");
                }
                String[] credentials = registerAndGetCredentials(req, username, companyId);
                req.getSession().setAttribute("cur.credentials.for.user", credentials);
                return credentials;
            } else {
                logger.debug("No CAS details found. Required common authentication first.");
                req.setAttribute(AUTO_LOGIN_REDIRECT, noCredentialUrl);
                return null;
            }
        } catch (AutoLoginException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Exception occurred while validating user login", e);
            throw new AutoLoginException(e.getMessage(), e);
        }
    }
}
