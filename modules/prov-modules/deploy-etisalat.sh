#!/bin/sh

LIFERAY_HOME=/hms/installs/liferay-portal-6.0.6/
export LIFERAY_DEPLOY_DIR=$LIFERAY_HOME/deploy
cp login-hook/target/login-hook.war $LIFERAY_DEPLOY_DIR
cp theme/target/prov-theme.war $LIFERAY_DEPLOY_DIR
cp prov/target/prov.war $LIFERAY_DEPLOY_DIR
cp menu/target/prov-menu.war $LIFERAY_DEPLOY_DIR
cp sms/operator-sms/target/etisalat-sms.war $LIFERAY_DEPLOY_DIR
cp ussd/operator-ussd/target/etisalat-ussd.war $LIFERAY_DEPLOY_DIR
cp subscription/target/subscription.war $LIFERAY_DEPLOY_DIR
cp caas/target/cas.war $LIFERAY_DEPLOY_DIR
cp downloadable/target/downloadable.war $LIFERAY_DEPLOY_DIR
#cp lbs-ncs/target/etisalat-lbs.war $LIFERAY_DEPLOY_DIR
cp rest/target/prov-api.war $LIFERAY_HOME/tomcat-6.0.29/webapps
