package hms.kite.prov.lbs.ui.util;

import java.util.Map;

public class CollectionUtil {

    /**
     * Iterate through the values of the map and return the first key that match the provided value
     * @param map
     * @return
     */
    public static <T, U>  T getKeyByValue(Map<T, U> map, U value) {
        for (Map.Entry<T, U> u : map.entrySet()) {
            if(u != null && u.getValue()!= null && u.getValue().equals(value)) {
                return u.getKey();
            }
        }
        return null;
    }
}
