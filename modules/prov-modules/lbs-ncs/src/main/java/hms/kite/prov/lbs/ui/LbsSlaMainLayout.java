/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.lbs.ui;

import com.vaadin.data.Validator;
import com.vaadin.ui.*;
import hms.kite.prov.lbs.LbsPortlet;
import hms.kite.prov.lbs.ui.common.ViewMode;
import hms.kite.provisioning.commons.ui.VaadinUtil;
import hms.kite.provisioning.commons.ui.event.PortletEventSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.prov.lbs.LbsServiceRegistry.operatorId;
import static hms.kite.prov.lbs.ui.common.ViewMode.*;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class LbsSlaMainLayout extends VerticalLayout {

    private static Logger logger = LoggerFactory.getLogger(LbsSlaMainLayout.class);

    private final LbsPortlet application;
    private final String appId;
    private final String corporateUserId;
    private final List<Map> ncses;

    private LbsBasicDetailsLayout basicDetailsLayout;
    private ViewMode viewMode = EDIT;

    private void init() {
        basicDetailsLayout = new LbsBasicDetailsLayout(application, corporateUserId, ncses);
        basicDetailsLayout.setWidth("80%");
    }

    private void loadComponents() {
        addComponent(basicDetailsLayout);
        setComponentAlignment(basicDetailsLayout, Alignment.MIDDLE_CENTER);

        Component buttonBar = createButtonBar();
        addComponent(buttonBar);
        setComponentAlignment(buttonBar, Alignment.MIDDLE_CENTER);
    }

    private Component createButtonBar() {
        HorizontalLayout buttonBar = new HorizontalLayout();
        buttonBar.setSpacing(true);

        buttonBar.addComponent(createBackButton());

        if (viewMode.equals(EDIT)) {
            buttonBar.addComponent(createSaveButton());
            buttonBar.addComponent(createResetButton());
        } else if (viewMode.equals(CONFIRM)) {
            buttonBar.addComponent(createConfirmButton());
        }

        return buttonBar;
    }

    private Button createResetButton() {
        Button button = new Button(application.getMsg("lbs.sla.main.layout.reset"));
        button.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    logger.debug("Resetting form values");
                    reset();
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while resetting the lbs sla", e);
                }
            }
        });
        return button;
    }


    private Button createSaveButton() {
        Button button = new Button(application.getMsg("lbs.sla.main.layout.save"));
        button.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    logger.debug("Validating LBS SLA parameters");
                    save();
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while configuring lbs sla", e);
                }
            }
        });
        return button;
    }

    private Component createConfirmButton() {
        Button button = new Button(application.getMsg("lbs.sla.main.layout.confirm"));
        button.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    logger.debug("Saving LBS SLA Parameter values in to the session");
                    confirmed();
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while configuring lbs sla", e);
                }
            }
        });
        return button;
    }


    private Button createBackButton() {
        Button button = new Button(application.getMsg("lbs.sla.main.layout.back"));
        button.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    logger.debug("Redirecting to the previous UI");
                    goBack();
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while going back", e);
                }
            }
        });
        return button;
    }

    private void goBack() {
        if (viewMode == CONFIRM) {
            reloadComponents(EDIT);
        } else if (viewMode == EDIT || viewMode == VIEW) {
            PortletEventSender.send(new HashMap<String, Object>(), provRefreshEvent, application);
            application.closePortlet();
        }
    }

    private void save() {
        try {
            basicDetailsLayout.validate();
            reloadComponents(CONFIRM);
        } catch (Validator.InvalidValueException e) {
            logger.error("Validation Failed for App ID [{}] - Cause [{}]  ", appId, e.getMessage());
        }
    }

    private void confirmed() {
        Map<String, Object> lbsSlaData = application.getLbsSlaData(appId);

        if (lbsSlaData == null) {
            lbsSlaData = new HashMap<String, Object>();
        }

        basicDetailsLayout.getData(lbsSlaData);

        logger.debug("Configured LBS SLA [{}] ", lbsSlaData);

        lbsSlaData.put(appIdK, appId);
        lbsSlaData.put(ncsTypeK, lbsK);
        lbsSlaData.put(operatorK, operatorId());
        application.setLbsSlaData(lbsSlaData);

        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put(ncsTypeK, lbsK);
        dataMap.put(statusK, successK);
        dataMap.put(operatorK, operatorId());
        dataMap.put(ncsDataK, lbsSlaData.get(chargingK));

        PortletEventSender.send(dataMap, ncsConfiguredK, application);
        application.closePortlet();
    }

    private void reset() {
        basicDetailsLayout.reset();
    }

    public LbsSlaMainLayout(LbsPortlet application, String appId, String corporateUserId, List<Map> ncses) {
        this.application = application;
        this.appId = appId;
        this.corporateUserId = corporateUserId;
        this.ncses = ncses;

        init();
    }

    public void reloadComponents(ViewMode viewMode) {
        this.viewMode = viewMode;

        removeAllComponents();
        loadComponents();

        if (viewMode.equals(VIEW)) {
            VaadinUtil.setAllReadonly(true, basicDetailsLayout);
        } else if (viewMode.equals(EDIT)) {
            VaadinUtil.setAllReadonly(false, basicDetailsLayout);
            basicDetailsLayout.setPermissions();
        } else if (viewMode.equals(CONFIRM)) {
            VaadinUtil.setAllReadonly(true, basicDetailsLayout);
        }
    }

    public void reloadData(Map<String, Object> data) {
        basicDetailsLayout.setData(data);
    }
}
