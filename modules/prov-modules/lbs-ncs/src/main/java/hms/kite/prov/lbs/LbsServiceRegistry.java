/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.lbs;

import hms.kite.provisioning.commons.ValidationRegistry;
import hms.kite.provisioning.commons.impl.ValidationRegistryImpl;
import hms.kite.util.NullObject;

import java.util.Properties;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class LbsServiceRegistry {

    private static String operatorId;
    private static ValidationRegistry validationRegistry;
    private static Properties lbsDefaultProperties;

    static {
        setOperatorId("");
        setValidationRegistry(new NullObject<ValidationRegistryImpl>().get());
        setLbsDefaultProperties(new NullObject<Properties>().get());
    }

    public static String operatorId() {
        return operatorId;
    }

    private static void setOperatorId(String operatorId) {
        LbsServiceRegistry.operatorId = operatorId;
    }

    public static ValidationRegistry validationRegistry() {
        return validationRegistry;
    }

    private static void setValidationRegistry(ValidationRegistry validationRegistry) {
        LbsServiceRegistry.validationRegistry = validationRegistry;
    }

    private static void setLbsDefaultProperties(Properties lbsDefaultProperties) {
        LbsServiceRegistry.lbsDefaultProperties = lbsDefaultProperties;
    }

    public static boolean isDefaultTpsAllow() {
        return Boolean.parseBoolean(lbsDefaultProperties.getProperty("lbs.default.tps.allow"));
    }

    public static String getDefaultTps() {
        return lbsDefaultProperties.getProperty("lbs.default.tps");
    }

    public static boolean isDefaultTpdAllow() {
        return Boolean.parseBoolean(lbsDefaultProperties.getProperty("lbs.default.tpd.allow"));
    }

    public static String getDefaultTpd() {
        return lbsDefaultProperties.getProperty("lbs.default.tpd");
    }

    public static Boolean isSubscriptionReqDefault() {
        return Boolean.parseBoolean(lbsDefaultProperties.getProperty("lbs.sub.required"));
    }

    public static boolean isHelpButtonLinkEnable() {
        return Boolean.parseBoolean(lbsDefaultProperties.getProperty("lbs.help.link.enable"));
    }
}
