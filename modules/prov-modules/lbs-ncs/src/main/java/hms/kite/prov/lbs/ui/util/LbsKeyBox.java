package hms.kite.prov.lbs.ui.util;

public class LbsKeyBox {

    public static final String lbsHorizontalAccuracyMapK = "lbs-horizontal-accuracy-map";
    public static final String lbsFreshnessMapK = "lbs-freshness-of-location-map";
    public static final String lbsNcsDefaultsK = "lbs-ncs-defaults-map";
    public static final String lbsDisplayDefaultsK = "lbs-ncs-display-defaults";
    public static final String lbsResponseTimeListK = "lbs-response-time-list";
}
