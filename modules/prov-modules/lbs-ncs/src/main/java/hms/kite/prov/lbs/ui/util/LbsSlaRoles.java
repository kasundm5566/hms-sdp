/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.lbs.ui.util;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class LbsSlaRoles {

    public static final String LBS_PERMISSION_ROLE_PREFIX = "ROLE_PROV_LBS";

    public static final String TPS = "TPS";
    public static final String TPD = "TPD";
    public static final String RESPONSE_TIME = "RESPONSE_TIME";
    public static final String HORIZONTAL_ACCURACY = "HORIZONTAL_ACCURACY";
    public static final String FRESHNESS_OF_LOCATION = "FRESHNESS_OF_LOCATION";
    public static final String SUBSCRIPTION_REQUIRED = "SUBSCRIPTION_REQUIRED";
}
