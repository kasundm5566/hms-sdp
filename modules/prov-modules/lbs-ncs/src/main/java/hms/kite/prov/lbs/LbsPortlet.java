/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.lbs;

import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.terminal.gwt.server.PortletApplicationContext2;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import hms.kite.datarepo.audit.AuditTrail;
import hms.kite.prov.lbs.ui.LbsBasicDetailsLayout;
import hms.kite.prov.lbs.ui.LbsSlaMainLayout;
import hms.kite.prov.lbs.ui.common.ViewMode;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.LiferayUtil;
import hms.kite.provisioning.commons.ui.UiManagerImpl;
import org.apache.commons.lang.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.ncsRepositoryService;
import static hms.kite.prov.lbs.LbsServiceRegistry.operatorId;
import static hms.kite.prov.lbs.ui.common.ViewMode.EDIT;
import static hms.kite.prov.lbs.ui.common.ViewMode.VIEW;
import static hms.kite.provisioning.commons.util.ProvCommonUtil.getNcsDataFromSessionByAppId;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class LbsPortlet extends BaseApplication {

    private static final Logger logger = LoggerFactory.getLogger(LbsPortlet.class);

    private static final String PORTLET_USER_ID = "portlet_user_id";

    private LbsSlaMainLayout mainLayout;
    private ThemeDisplay themeDisplay;
    private Panel mainPanel;
    private Panel innerPanel;

    private void setPortletUserId(Long userId) {
        addToSession(PORTLET_USER_ID, userId);
    }

    private Long getPortletUserId() {
        return (Long) getFromSession(PORTLET_USER_ID);
    }

    private void handleCreateNcsRequest(Map<String, Object> eventData) {
        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        innerPanel.removeAllComponents();
        mainLayout = new LbsSlaMainLayout(this, (String) eventData.get(appIdK), (String) eventData.get(coopUserIdK),
                (List<Map>) eventData.get(ncsesK));
        mainLayout.reloadComponents(EDIT);
        innerPanel.addComponent(mainLayout);
    }

    private void handleReconfigureNcsRequest(Map<String, Object> eventData) {
        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        innerPanel.removeAllComponents();
        String appId = (String) eventData.get(appIdK);
        String corporateUserId = (String) eventData.get(coopUserIdK);
        Map<String, Object> sessionLbsData = getLbsSlaData(appId);
        mainLayout = new LbsSlaMainLayout(this, appId, corporateUserId, (List<Map>) eventData.get(ncsesK));
        innerPanel.addComponent(mainLayout);
        if (sessionLbsData != null) {
            mainLayout.reloadData(sessionLbsData);
        } else {
            reloadDataFromRepository(eventData);
        }
        mainLayout.reloadComponents(EDIT);
    }

    private void handleViewNcsRequest(Map<String, Object> eventData) {
        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        reloadNcsData(VIEW, eventData);
    }

    private void handleSaveNcsRequest(Map<String, Object> eventData) {
        String operator = operatorId();
        String appId = (String) eventData.get(appIdK);
        String status = (String) eventData.get(statusK);

        Map<String, Object> sessionLbsData = getLbsSlaData(appId);

        if (deleteK.equals(status)) {
            ncsRepositoryService().update(appId, lbsK, operator, status);
        } else if (sessionLbsData != null) {
            sessionLbsData.put(statusK, eventData.get(statusK));
            ncsRepositoryService().createNcs(sessionLbsData, (String) eventData.get(spIdK));
        } else {
            ncsRepositoryService().update(appId, lbsK, operator, status);
        }
        removeFromSession(lbsSlaDataK);
        closePortlet();
    }

    private void reloadNcsData(ViewMode viewMode, Map<String, Object> eventData) {
        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        innerPanel.removeAllComponents();
        mainLayout = new LbsSlaMainLayout(this, (String) eventData.get(appIdK), (String) eventData.get(coopUserIdK),
                (List<Map>) eventData.get(ncsesK));
        innerPanel.addComponent(mainLayout);
        reloadDataFromRepository(eventData);
        mainLayout.reloadComponents(viewMode);
    }

    private void reloadDataFromRepository(Map<String, Object> data) {
        String appId = (String) data.get(appIdK);
        Map<String, Object> lbsSla = ncsRepositoryService().findNcs(appId, lbsK, operatorId());
        if (lbsSla != null) {
            mainLayout.reloadData(lbsSla);
            setLbsSlaData(lbsSla);
        }
    }

    @Override
    protected String getApplicationName() {
        return "lbs";
    }

    @Override
    public void init() {
        setResourceBundle("US");
        String title = getMsg("lbs.sla.main.title", WordUtils.capitalize(operatorId()));
        Window main = new Window(title);
        setMainWindow(main);
        setUiManager(new UiManagerImpl(this));
        VerticalLayout mainLayout = new VerticalLayout();
        mainPanel = new Panel("");
        mainLayout.addComponent(mainPanel);
        mainLayout.addStyleName("container-panel");
        mainLayout.setWidth("762px");
        mainLayout.setMargin(true);
        innerPanel = new Panel(title);
        mainPanel.addComponent(innerPanel);
        innerPanel.addStyleName("child-panel");
        setPortletContext((PortletApplicationContext2) getContext());
        getPortletContext().addPortletListener(this, this);

        getUiManager().clearAll();
        getUiManager().pushScreen(LbsBasicDetailsLayout.class.getName(), mainLayout);
    }

    public void handleEventRequest(EventRequest request, EventResponse response, Window window) {
        super.handleEventRequest(request, response, window);

        try {
            String eventName = request.getEvent().getName();
            themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
            Long userId = themeDisplay.getUserId();
            setPortletUserId(userId);

            Map<String, Object> eventData = (Map<String, Object>) request.getEvent().getValue();
            logger.debug("LBS [{}] portlet Received Event Data is [{}]", operatorId(), eventData);
            AuditTrail.setCurrentUser((String) eventData.get(currentUsernameK), (String) eventData.get(currentUserTypeK));

            String appId = (String) eventData.get(appIdK);
            String ncsType = (String) eventData.get(ncsTypeK);
            String operator = (String) eventData.get(operatorK);

            if (!lbsK.equals(ncsType) && !operatorId().equals(operator)) {
                logger.debug("Event is not for me [{}/{}]", lbsK, operatorId());
                return;
            }

            if (appId == null || ncsType == null || !ncsType.equals(lbsK)
                    || operator == null || !operator.equals(operatorId())) {
                logger.debug("Event Request ignored, since it is not belong to me");
                return;
            }

            if (createNcsEvent.equals(eventName)) {
                handleCreateNcsRequest(eventData);
            } else if (reConfigureNcsEvent.equals(eventName)) {
                handleReconfigureNcsRequest(eventData);
            } else if (saveNcsEvent.equals(eventName)) {
                handleSaveNcsRequest(eventData);
            } else if (viewNcsK.equals(eventName)) {
                handleViewNcsRequest(eventData);
            } else {
                logger.error("Unexpected event [{}], Can't handle", eventName);
            }
        } catch (Exception e) {
            logger.error("Unexpected error occurred while handling event ", e);
        }
    }

    public void closePortlet() {
        innerPanel.removeAllComponents();
        String portletId = LiferayUtil.createPortletId(lbsK, operatorId());
        logger.debug("closing portlet [{}] by user [{}]", portletId, getPortletUserId());
        themeDisplay.getLayoutTypePortlet().removePortletId(getPortletUserId(), portletId);
    }

    public Map<String, Object> getLbsSlaData(String appId) {
        return getNcsDataFromSessionByAppId(appId, lbsSlaDataK, this);
    }

    public void setLbsSlaData(Map<String, Object> casSlaData) {
        addToSession(lbsSlaDataK, casSlaData);
    }
}
