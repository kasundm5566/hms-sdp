/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.lbs.ui;

import com.vaadin.data.Validator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.ui.*;
import hms.kite.prov.lbs.ui.util.CollectionUtil;
import hms.kite.provisioning.commons.ui.AbstractBasicDetailsLayout;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.component.popup.HelpDetails;
import hms.kite.provisioning.commons.ui.validator.SpLimitValidator;
import hms.kite.provisioning.commons.ui.validator.SubscriptionRequiredValidator;
import hms.kite.provisioning.commons.ui.validator.TpsTpdValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.datarepo.RepositoryServiceRegistry.systemConfiguration;
import static hms.kite.prov.lbs.LbsServiceRegistry.*;
import static hms.kite.prov.lbs.LbsServiceRegistry.isSubscriptionReqDefault;
import static hms.kite.prov.lbs.ui.util.LbsSlaRoles.*;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.util.KeyNameSpaceResolver.key;
import static hms.kite.prov.lbs.ui.util.LbsKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class LbsBasicDetailsLayout extends AbstractBasicDetailsLayout {

    private static final Logger logger = LoggerFactory.getLogger(LbsBasicDetailsLayout.class);

    private static final String WIDTH = "250px";
    private static final String HELP_IMAGE = "help.jpg";
    public static final String HELP_IMG_WIDTH = "850px";
    public static final String HELP_IMG_HEIGHT = "460px";
    public static final String HELP_IMG_STYLE_NAME = "link";
    public static final String HELP_BUTTON_NAME = "Help";

    private String corporateUserId;
    private final List<Map> ncses;

    private Form form;

    private TextField tpsTxtFld;
    private TextField tpdTxtFld;

    private Select responseTimeSelect;
    private Select horizontalAccuracySelect;
    private Select freshnessOfLocationSelect;

    private CheckBox subscriptionRequiredChkBox;
    private PopupView popupView;
    /*private ChargingLayout chargingLayout;*/
    private Map<String, String> fieldDefaults = new HashMap<String, String>();

    private List getItemList(String configurationId) {
        String key = key(operatorId(), configurationId);
        return (List) systemConfiguration().find(key);
    }

    private Map<String, Object> getSystemConfigurationsMapById(String configurationId) {
        String key = key(operatorId(), configurationId);
        return (Map<String, Object>) systemConfiguration().find(key);
    }

    private void initAndLoadValuesToSelect(Select container, Collection<?> items) {
        if (items == null) {
            return;
        }

        for (Object item : items) {
            container.addItem(item);
        }
    }

    private void init() {
        form = new Form();
        form.addStyleName("sms-ncs-form");

        if(isHelpButtonLinkEnable()) {
            createHelperLink();
        }

        addComponent(form);

        int spTpd;
        int spTps;

        fieldDefaults = (Map<String, String>) systemConfiguration().find(key(operatorId(), lbsDisplayDefaultsK));

        try {
            Map<String, Object> sp = spRepositoryService().findSpByCoopUserId(corporateUserId);
            if (sp != null) {
                logger.debug("Returned SP Details [{}]", sp.toString());

                spTpd = Integer.parseInt(sp.get(lbsTpdK).toString());
                spTps = Integer.parseInt(sp.get(lbsTpsK).toString());

                createMainLayout(spTpd, spTps);
            } else {
                logger.error("No Sp Details found with the given corporate user id [{}]", corporateUserId);
            }
        } catch (Exception e) {
            logger.error("Error occurred while searching SP details for corporate user id [{}] ", corporateUserId, e);
        }
    }

    private void createMainLayout(int spTpd, int spTps) {
        createTpsFld(spTps);
        createTpdFld(spTpd);
        createResponseTimeFld();
        createHorizontalAccuracyFld();
        createFreshnessOfLocationFld();
        createSubscriptionRequiredFld();

        setDefaultValues();
    }

    private void createHelperLink() {
        Button helpButtonLink = createHelpButtonLink();
        addComponent(helpButtonLink);
        setComponentAlignment(helpButtonLink, Alignment.TOP_RIGHT);
    }

    private Button createHelpButtonLink() {
        final HelpDetails helpDetails = new HelpDetails(application, HELP_IMAGE);
        final Embedded image = helpDetails.getHelpImage();
        Button helpButtonLink = new Button(HELP_BUTTON_NAME);
        helpButtonLink.setStyleName(HELP_IMG_STYLE_NAME);
        helpButtonLink.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent clickEvent) {
                getWindow().addWindow(helpDetails.generateHelpImageSubWindow(image, HELP_IMG_WIDTH, HELP_IMG_HEIGHT));
            }
        });
        return helpButtonLink;
    }

    private void createTpsFld(int spTps) {
        tpsTxtFld = getFieldWithPermission(TPS, new TextField());
        if (isFieldNotNull(tpsTxtFld)) {
            tpsTxtFld.setCaption(application.getMsg("lbs.sla.tps.caption"));
            tpsTxtFld.setWidth(WIDTH);
            tpsTxtFld.setRequired(true);
            tpsTxtFld.setRequiredError(application.getMsg("lbs.sla.tps.required.error"));
            tpsTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("tps.regex"),
                    application.getMsg("lbs.sla.tps.format.invalid.error")));
            tpsTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("zero.validation.regex"),
                    application.getMsg("lbs.sla.zero.validation.regex.error")));
            tpsTxtFld.addValidator(new SpLimitValidator(spTps, application.getMsg("lbs.sla.tps.sp.limit.error", Integer.toString(spTps))));
            if (isDefaultTpsAllow()) {
                tpsTxtFld.setValue(getDefaultTps());
            }
            form.addField("tps", tpsTxtFld);
        }
    }

    private void createTpdFld(int spTpd) {
        tpdTxtFld = getFieldWithPermission(TPD, new TextField());
        if (isFieldNotNull(tpdTxtFld)) {
            tpdTxtFld.setCaption(application.getMsg("lbs.sla.tpd.caption"));
            tpdTxtFld.setWidth(WIDTH);
            tpdTxtFld.setRequired(true);
            tpdTxtFld.setRequiredError(application.getMsg("lbs.sla.tpd.required.error"));
            tpdTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("tpd.regex"),
                    application.getMsg("lbs.sla.tpd.format.invalid.error")));
            tpdTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("zero.validation.regex"),
                    application.getMsg("lbs.sla.zero.validation.regex.error")));
            tpdTxtFld.addValidator(new SpLimitValidator(spTpd, application.getMsg("lbs.sla.tpd.sp.limit.error", Integer.toString(spTpd))));
            tpdTxtFld.addValidator(new TpsTpdValidator(tpsTxtFld, tpdTxtFld, application.getMsg("lbs.sla.tps.tpd.validation.error")));
            if (isDefaultTpdAllow()) {
                tpdTxtFld.setValue(getDefaultTpd());
            }
            form.addField("tpd", tpdTxtFld);
        }
    }

    private void createResponseTimeFld() {
        responseTimeSelect = getFieldWithPermission(RESPONSE_TIME, new Select());
        if (isFieldNotNull(responseTimeSelect)) {
            responseTimeSelect.setCaption(application.getMsg("lbs.sla.response.time.caption"));
            responseTimeSelect.setWidth(WIDTH);
            responseTimeSelect.setRequired(true);
            responseTimeSelect.setRequiredError(application.getMsg("lbs.sla.response.time.required.error"));
            initAndLoadValuesToSelect(responseTimeSelect, getItemList(lbsResponseTimeListK));
            responseTimeSelect.setNullSelectionAllowed(false);

            form.addField("responseTime", responseTimeSelect);
        }
    }

    private void createHorizontalAccuracyFld() {
        horizontalAccuracySelect = getFieldWithPermission(HORIZONTAL_ACCURACY, new Select());
        if (isFieldNotNull(horizontalAccuracySelect)) {
            horizontalAccuracySelect.setCaption(application.getMsg("lbs.sla.horizontal.accuracy.caption"));
            horizontalAccuracySelect.setWidth(WIDTH);
            horizontalAccuracySelect.setRequired(true);
            horizontalAccuracySelect.setRequiredError(application.getMsg("lbs.sla.horizontal.accuracy.required.error"));
            initAndLoadValuesToSelect(horizontalAccuracySelect, getSystemConfigurationsMapById(lbsHorizontalAccuracyMapK).keySet());
            horizontalAccuracySelect.setNullSelectionAllowed(false);

            form.addField("horizontalAccuracy", horizontalAccuracySelect);
        }
    }

    private void createFreshnessOfLocationFld() {
        freshnessOfLocationSelect = getFieldWithPermission(FRESHNESS_OF_LOCATION, new Select());
        if (isFieldNotNull(freshnessOfLocationSelect)) {
            freshnessOfLocationSelect.setCaption(application.getMsg("lbs.sla.freshness.of.location.caption"));
            freshnessOfLocationSelect.setWidth(WIDTH);
            freshnessOfLocationSelect.setRequired(true);
            freshnessOfLocationSelect.setRequiredError(application.getMsg("lbs.sla.freshness.of.location.required.error"));
            initAndLoadValuesToSelect(freshnessOfLocationSelect, getSystemConfigurationsMapById(lbsFreshnessMapK).keySet());
            freshnessOfLocationSelect.setNullSelectionAllowed(false);

            form.addField("freshnessOfLocation", freshnessOfLocationSelect);
        }
    }

    private void createSubscriptionRequiredFld() {
        subscriptionRequiredChkBox = getFieldWithPermission(SUBSCRIPTION_REQUIRED, new CheckBox());
        if (isFieldNotNull(subscriptionRequiredChkBox)) {
            subscriptionRequiredChkBox.setCaption(application.getMsg("lbs.sla.subscription.required.caption"));
            subscriptionRequiredChkBox.setValue(isSubscriptionReqDefault());
            subscriptionRequiredChkBox.addValidator(new SubscriptionRequiredValidator(ncses,
                    application.getMsg("lbs.sla.subscription.ncs.not.selected.error")));
            form.addField("subscriptionRequired", subscriptionRequiredChkBox);
        }
    }

    public LbsBasicDetailsLayout(BaseApplication application, String corporateUserId, List<Map> ncses) {
        super(application, LBS_PERMISSION_ROLE_PREFIX);

        this.corporateUserId = corporateUserId;
        this.ncses = ncses;

        init();
    }

    @Override
    public void setData(Map<String, Object> data) {
        setValueToField(tpsTxtFld, data.get(tpsK));
        setValueToField(tpdTxtFld, data.get(tpdK));

        Map qos = (Map) data.get(qosK);
        setValueToField(responseTimeSelect, qos.get(responseTimeK));
        setValueToField(horizontalAccuracySelect,
                CollectionUtil.getKeyByValue(getSystemConfigurationsMapById(lbsHorizontalAccuracyMapK), qos.get(horizontalAccuracyK)));
        setValueToField(freshnessOfLocationSelect,
                CollectionUtil.getKeyByValue(getSystemConfigurationsMapById(lbsFreshnessMapK), qos.get(freshnessOfLocationK)));

        setValueToField(subscriptionRequiredChkBox, data.get(subscriptionRequiredK));
    }

    @Override
    public void getData(Map<String, Object> data) {
        getValueFromField(tpsTxtFld, data, tpsK);
        getValueFromField(tpdTxtFld, data, tpdK);

        Map<String, Object> qos = new HashMap<String, Object>();
        getValueFromField(responseTimeSelect, qos, responseTimeK);

        qos.put(horizontalAccuracyK, getSystemConfigurationsMapById(lbsHorizontalAccuracyMapK).get(horizontalAccuracySelect.getValue()));
        qos.put(freshnessOfLocationK, getSystemConfigurationsMapById(lbsFreshnessMapK).get(freshnessOfLocationSelect.getValue()));

        data.put(qosK, qos);

        /**
         * set default configurations
         */
        Map<String, Object> lbsNcsDefaults = getSystemConfigurationsMapById(lbsNcsDefaultsK);
        for (String lbsNcsDefault : lbsNcsDefaults.keySet()) {
            data.put(lbsNcsDefault, lbsNcsDefaults.get(lbsNcsDefault));
        }

        getValueFromField(subscriptionRequiredChkBox, data, subscriptionRequiredK);
    }

    @Override
    public void validate() {
        try {
            validateField(tpsTxtFld);
            validateField(tpdTxtFld);
            validateField(responseTimeSelect);
            validateField(horizontalAccuracySelect);
            validateField(freshnessOfLocationSelect);
            validateField(subscriptionRequiredChkBox);
            form.setComponentError(null);
        } catch (Validator.InvalidValueException e) {
            form.setComponentError(e);
            throw e;
        }
    }

    @Override
    public void reset() {
        setDefaultValues();
    }

    private void setDefaultValues() {
        //resetFields("", tpsTxtFld, tpdTxtFld);
        responseTimeSelect.setValue(fieldDefaults.get(lbsResponseTimeListK));
        horizontalAccuracySelect.setValue(fieldDefaults.get(lbsHorizontalAccuracyMapK));
        freshnessOfLocationSelect.setValue(fieldDefaults.get(lbsFreshnessMapK));

        //subscriptionRequiredChkBox.setValue(Boolean.FALSE);
    }

    @Override
    public void setPermissions() {
        setReadWritePermissionToComponent(tpsTxtFld, tpdTxtFld, responseTimeSelect, horizontalAccuracySelect,
                freshnessOfLocationSelect, subscriptionRequiredChkBox);
    }
}
