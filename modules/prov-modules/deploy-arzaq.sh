#!/bin/sh

LIFERAY_HOME=/home/mayooran/installs/life-ray/
export LIFERAY_DEPLOY_DIR=$LIFERAY_HOME/deploy

cp login-hook/target/login-hook.war $LIFERAY_DEPLOY_DIR
cp theme/target/prov-theme.war $LIFERAY_DEPLOY_DIR
cp prov/target/prov.war $LIFERAY_DEPLOY_DIR
cp menu/target/prov-menu.war $LIFERAY_DEPLOY_DIR

cp caas/target/cas.war $LIFERAY_DEPLOY_DIR
cp sms/operator-sms/target/telkomsel-sms.war $LIFERAY_DEPLOY_DIR
cp ussd/operator-ussd/target/telkomsel-ussd.war $LIFERAY_DEPLOY_DIR

cp rest/target/prov-api.war $LIFERAY_HOME/tomcat-6.0.29/webapps
