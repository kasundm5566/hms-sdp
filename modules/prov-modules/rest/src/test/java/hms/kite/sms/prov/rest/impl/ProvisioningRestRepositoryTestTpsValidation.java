/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.sms.prov.rest.impl;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static hms.kite.provisioning.commons.util.ProvKeyBox.descriptionK;
import static hms.kite.provisioning.commons.util.ProvKeyBox.ncsTypeK;
import static hms.kite.provisioning.commons.util.ProvKeyBox.routingKeysK;
import static hms.kite.util.KiteKeyBox.chargedPartyK;
import static hms.kite.util.KiteKeyBox.chargingAmountK;
import static hms.kite.util.KiteKeyBox.chargingTypeK;
import static hms.kite.util.KiteKeyBox.connectionUrlK;
import static hms.kite.util.KiteKeyBox.enableK;
import static hms.kite.util.KiteKeyBox.endDateK;
import static hms.kite.util.KiteKeyBox.freeK;
import static hms.kite.util.KiteKeyBox.maxNoOfBcMsgsPerDayK;
import static hms.kite.util.KiteKeyBox.maxNoOfSubscribersK;
import static hms.kite.util.KiteKeyBox.nameK;
import static hms.kite.util.KiteKeyBox.operatorK;
import static hms.kite.util.KiteKeyBox.smsK;
import static hms.kite.util.KiteKeyBox.spIdK;
import static hms.kite.util.KiteKeyBox.startDateK;
import static hms.kite.util.KiteKeyBox.subscriberK;
import static hms.kite.util.KiteKeyBox.subscriptionK;
import static hms.kite.util.KiteKeyBox.subscriptionRespMessageK;
import static hms.kite.util.KiteKeyBox.tpdK;
import static hms.kite.util.KiteKeyBox.tpsK;
import static hms.kite.util.KiteKeyBox.unsubscriptionRespMessageK;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ProvisioningRestRepositoryTestTpsValidation {

    @Test
    public void testAppMoThrottling() {
        Map<String, Object> app = createApp();
        HashMap<String, Object> safaricomSmsSla = createSafaricomSmsSla();
        int appTps = Integer.parseInt(String.valueOf(app.get(tpsK)));
        Assert.assertEquals(appTps, 10);

    }

    private Map<String, Object> createApp() {
        Map<String, Object> app = new HashMap<String, Object>();

        app.put(descriptionK, "sample description");
        app.put(spIdK, "sample-sp-id");
        app.put(nameK, "sample app name");
        app.put(subscriptionK, enableK);
        app.put(maxNoOfSubscribersK, "20");
        app.put(maxNoOfBcMsgsPerDayK, "20");
        app.put(subscriptionRespMessageK, "subscription success message");
        app.put(unsubscriptionRespMessageK, "unsubscription success message");

        app.put(startDateK, DateFormat.getDateInstance(DateFormat.SHORT).format(new Date()));
        app.put(endDateK, DateFormat.getDateInstance(DateFormat.SHORT).format(new Date()));

        HashMap<String, Map<String, Object>> ncsSlas = new HashMap<String, Map<String, Object>>();
        ncsSlas.put("safaricom-sms", createSafaricomSmsSla());

        app.put("ncs-slas", ncsSlas);

        app.put(tpsK, "10");

        return app;
    }

    private HashMap<String, Object> createSafaricomSmsSla() {
        HashMap<String, Object> safaricomSmsSla = new HashMap<String, Object>();

        safaricomSmsSla.put("mo-" + tpsK, "10");
        safaricomSmsSla.put("mo-" + chargingTypeK, freeK);
        safaricomSmsSla.put("mt-" + chargingTypeK, freeK);
        safaricomSmsSla.put("mo-" + chargedPartyK, subscriberK);
        safaricomSmsSla.put("mt-" + chargedPartyK, subscriberK);
        safaricomSmsSla.put(ncsTypeK, smsK);
        safaricomSmsSla.put(operatorK, "safaricom");
        safaricomSmsSla.put("mt-" + tpdK, "10");
        safaricomSmsSla.put("mt-" + tpsK, "10");
        safaricomSmsSla.put("mo-" + connectionUrlK, "http://soltura.sdp.dialog:6000");
        safaricomSmsSla.put("mt-" + chargingAmountK, "0.00");
        safaricomSmsSla.put("mo-" + chargingAmountK, "0.00");
        safaricomSmsSla.put("mo-" + tpdK, "10");

        ArrayList<Map<String, String>> routingKeys = new ArrayList<Map<String, String>>();
        HashMap<String, String> rk1 = new HashMap<String, String>();
        rk1.put("shortcode", "9696");
        rk1.put("keyword", "test");
        routingKeys.add(rk1);

        safaricomSmsSla.put(routingKeysK, routingKeys);
        return safaricomSmsSla;
    }
}
