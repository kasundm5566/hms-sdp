/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.sms.prov.rest.impl;

import com.mongodb.QueryBuilder;
import hms.kite.util.KiteErrorBox;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.appRepositoryService;
import static hms.kite.datarepo.RepositoryServiceRegistry.ncsRepositoryService;
import static hms.kite.provisioning.commons.util.ProvKeyBox.descriptionK;
import static hms.kite.provisioning.commons.util.ProvKeyBox.ncsTypeK;
import static hms.kite.provisioning.commons.util.ProvKeyBox.routingKeysK;
import static hms.kite.util.KiteKeyBox.appIdK;
import static hms.kite.util.KiteKeyBox.chargedPartyK;
import static hms.kite.util.KiteKeyBox.chargingAmountK;
import static hms.kite.util.KiteKeyBox.chargingTypeK;
import static hms.kite.util.KiteKeyBox.connectionUrlK;
import static hms.kite.util.KiteKeyBox.enableK;
import static hms.kite.util.KiteKeyBox.endDateK;
import static hms.kite.util.KiteKeyBox.freeK;
import static hms.kite.util.KiteKeyBox.idK;
import static hms.kite.util.KiteKeyBox.maxNoOfBcMsgsPerDayK;
import static hms.kite.util.KiteKeyBox.maxNoOfSubscribersK;
import static hms.kite.util.KiteKeyBox.nameK;
import static hms.kite.util.KiteKeyBox.ncsSlasK;
import static hms.kite.util.KiteKeyBox.operatorK;
import static hms.kite.util.KiteKeyBox.pendingApproveK;
import static hms.kite.util.KiteKeyBox.smsK;
import static hms.kite.util.KiteKeyBox.spIdK;
import static hms.kite.util.KiteKeyBox.startDateK;
import static hms.kite.util.KiteKeyBox.statusCodeK;
import static hms.kite.util.KiteKeyBox.statusK;
import static hms.kite.util.KiteKeyBox.subscriberK;
import static hms.kite.util.KiteKeyBox.subscriptionK;
import static hms.kite.util.KiteKeyBox.subscriptionRespMessageK;
import static hms.kite.util.KiteKeyBox.tpdK;
import static hms.kite.util.KiteKeyBox.tpsK;
import static hms.kite.util.KiteKeyBox.unsubscriptionRespMessageK;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
@ContextConfiguration(locations = {"classpath:rest-test-config.xml"})
public class ProvisioningRestRepositoryTest extends AbstractTestNGSpringContextTests {

    @Resource
    MongoTemplate mongoTemplate;

    @Resource
    ProvisioningRestRepository repository;

    @BeforeTest
    public void setUp() {
        // Add your code here
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown() {
        mongoTemplate.getCollection("app").drop();
        mongoTemplate.getCollection("ncs_sla").drop();
        mongoTemplate.getCollection("routing_keys").remove(new QueryBuilder().put("sp-id").notEquals("").get());
    }

    @Test
    public void testCreateSmsApp() {

        Map<String, Object> app = createApp();

        Map<String, Object> response = repository.createOrUpdateApp(new HashMap<String, Object>(app));

        String appId = (String) response.get(appIdK);
        assertNotNull(appId);
        assertEquals(response.get(statusCodeK), KiteErrorBox.successCode);


        Map<String, Object> readApp = appRepositoryService().findByAppId(appId);
        Map<String, Map<String, Object>> slas = ncsRepositoryService().findByAppId(appId);
        readApp.put(ncsSlasK, slas);

        Object id = readApp.remove(idK);//just to match with orginal request

        Map<String, Object> expectedApp = new HashMap<String, Object>(readApp);
        expectedApp.put(appIdK, appId);
        expectedApp.put(statusK, pendingApproveK);

        assertEquals(readApp, expectedApp);
    }

    private Map<String, Object> createApp() {
        Map<String, Object> app = new HashMap<String, Object>();

        app.put(descriptionK, "sample description");
        app.put(spIdK, "sample-sp-id");
        app.put(nameK, "sample app name");
        app.put(subscriptionK, enableK);
        app.put(maxNoOfSubscribersK, "20");
        app.put(maxNoOfBcMsgsPerDayK, "20");
        app.put(subscriptionRespMessageK, "subscription success message");
        app.put(unsubscriptionRespMessageK, "unsubscription success message");

        app.put(startDateK, DateFormat.getDateInstance(DateFormat.SHORT).format(new Date()));
        app.put(endDateK, DateFormat.getDateInstance(DateFormat.SHORT).format(new Date()));

        HashMap<String, Map<String, Object>> ncsSlas = new HashMap<String, Map<String, Object>>();
        ncsSlas.put("safaricom-sms", createSafaricomSmsSla());

        app.put("ncs-slas", ncsSlas);
        return app;
    }

    private HashMap<String, Object> createSafaricomSmsSla() {
        HashMap<String, Object> safaricomSmsSla = new HashMap<String, Object>();

        safaricomSmsSla.put("mo-" + tpsK, "10");
        safaricomSmsSla.put("mo-" + chargingTypeK, freeK);
        safaricomSmsSla.put("mt-" + chargingTypeK, freeK);
        safaricomSmsSla.put("mo-" + chargedPartyK, subscriberK);
        safaricomSmsSla.put("mt-" + chargedPartyK, subscriberK);
        safaricomSmsSla.put(ncsTypeK, smsK);
        safaricomSmsSla.put(operatorK, "safaricom");
        safaricomSmsSla.put("mt-" + tpdK, "10");
        safaricomSmsSla.put("mt-" + tpsK, "10");
        safaricomSmsSla.put("mo-" + connectionUrlK, "http://prov.sdp:6000");
        safaricomSmsSla.put("mt-" + chargingAmountK, "0.00");
        safaricomSmsSla.put("mo-" + chargingAmountK, "0.00");
        safaricomSmsSla.put("mo-" + tpdK, "10");

        ArrayList<Map<String, String>> routingKeys = new ArrayList<Map<String, String>>();
        HashMap<String, String> rk1 = new HashMap<String, String>();
        rk1.put("shortcode", "9696");
        rk1.put("keyword", "test");
        routingKeys.add(rk1);

        safaricomSmsSla.put(routingKeysK, routingKeys);
        return safaricomSmsSla;
    }
}
