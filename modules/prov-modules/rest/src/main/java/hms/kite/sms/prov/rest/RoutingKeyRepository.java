/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.sms.prov.rest;

import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public interface RoutingKeyRepository {

    /**
     * Create the routing-key in system and send the status reponse.
     *
     * @param request {sp-id:somespid, shortcode:9696, operator:telkomsel, keyword:test}
     * @return {status:success|failed, description:some-reason-to-fail}
     */
    Map<String, Object> registerRoutingKey(Map<String, Object> request);

    /**
     * Check the availability of routing keys.
     *
     * @param request - {shortcode:9696, operator:telkomsel, keyword:test}
     * @return {status:available/not-available, error-code:duplicate-key|reserved-key}
     */
    Map<String, Object> isRoutingKeyAvailable(Map<String, Object> request);

    /**
     * Return all the routing keys assigned for sp.
     *
     * @param request {sp-id:someid}
     * @return {operator1:{9696:[key1, key2],1234:[key2],3456:[yek3]}, operator2:{123:[sdd],1234:[ert],345:[sdfsd]}}
     */
    Map<String, Object> retrieveRoutingKeys(Map<String, Object> request);

    /**
     * Return all the available operators and shortcodes for a particular sp.
     *
     * @param message {sp-id=someid}
     * @return {operator1:(9696,1234,3456), operator2:(123,1234,345)}
     */
    Map<String, Object> retrieveSharedShortcodes(Map<String, Object> message);

    /**
     * Find all routing keys assigned to the sp.
     *
     * @param request {sp-id : some-sp-id}
     * @return (
     *         {operator : safaricom, shortcode : 9696, keyword : test, sp-id=some-sp-id, app-id=some-app-id}
     *         {operator : safaricom, shortcode : 9696, keyword : test2, sp-id=some-sp-id, app-id=} app id empty since this sp haven't assigned it to an app.
     *         {operator : airtel, shortcode : 7777, keyword : good, sp-id=some-sp-id, app-id=someother-app-id}
     */
    List<Map<String, Object>> findAll(Map<String, Object> request);

}
