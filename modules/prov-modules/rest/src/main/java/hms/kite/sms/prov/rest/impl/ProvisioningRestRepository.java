/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.sms.prov.rest.impl;

import hms.kite.datarepo.audit.AuditTrail;
import hms.kite.provisioning.commons.util.ProvIdGenerator;
import hms.kite.sms.prov.rest.ProvisioningRepository;
import hms.kite.util.SdpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static hms.kite.datarepo.RepositoryServiceRegistry.*;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.allowedPaymentInstrumentsK;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
@Produces({"application/json"})
@Consumes({"application/json"})
@Path("/app/")
public class ProvisioningRestRepository implements ProvisioningRepository {

    private static final Logger logger = LoggerFactory.getLogger(ProvisioningRestRepository.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    private void validateAppThrottling(Map<String, Object> sla, String spId, String direction) {
        String directionName = direction.toUpperCase();

        Map<String, Object> directionSla = (Map<String, Object>) sla.get(direction);
        if (directionSla != null) {
            int appTps = Integer.parseInt(String.valueOf(directionSla.get(tpsK)));
            int appTpd = Integer.parseInt(String.valueOf(directionSla.get(tpdK)));

            Map<String, Object> sp = spRepositoryService().findSpBySpId(spId);

            Object spSmsTps;
            Object spSmsTpd;

            if (moK.equals(direction)) {
                spSmsTps = sp.get(smsMoTpsK);
                spSmsTpd = sp.get(smsMoTpdK);
            } else if (mtK.equals(direction)) {
                spSmsTps = sp.get(smsMtTpsK);
                spSmsTpd = sp.get(smsMtTpdK);
            } else {
                logger.error("Cannot validate {} TPS and {} TPD values for [{}]",
                        new Object[]{directionName, directionName, spId});
                return;
            }

            int spTps = Integer.parseInt(String.valueOf(spSmsTps));
            int spTpd = Integer.parseInt(String.valueOf(spSmsTpd));

            if (appTps >= spTps) {
                logger.debug("APP {} TPS [{}] is greater than SP {} TPS [{}]. Therefore, resetting APP {} TPS to [{}]",
                        new Object[]{directionName, appTps, directionName, spTps, directionName, spTps});
                directionSla.put(tpsK, Integer.toString(spTps - 1));
            } else {
                logger.debug("APP {} TPS [{}] | SP {} TPS [{}]", new Object[]{directionName, appTps, directionName, spTps});
            }

            if (appTpd >= spTpd) {
                logger.debug("APP {} TPD [{}] is greater than SP {} TPD [{}]. Therefore, resetting APP {} TPD to [{}]",
                        new Object[]{directionName, appTpd, directionName, spTpd, directionName, spTpd});
                directionSla.put(tpdK, Integer.toString(spTpd - 1));
            } else {
                logger.debug("APP {} TPD [{}] | SP {} TPD [{}]", new Object[]{directionName, appTpd, directionName, spTpd});
            }
        } else {
            logger.info("SLA is not configured for direction[{}]", direction);
        }
    }

    private void replaceOperatorChargingAmountWithServiceCode(Map<String, Object> smsSla) {
//        Map<String, Object> moChargingDetails = (Map<String, Object>) ((Map<String, Object>) smsSla.get(moK)).get(chargingK);
//        Map<String, Object> mtChargingDetails = (Map<String, Object>) ((Map<String, Object>) smsSla.get(mtK)).get(chargingK);
//
//        String operator = (String) smsSla.get(operatorK);
//
//        doReplaceOperatorChargingAmountWithServiceCode(moChargingDetails, operator);
//        doReplaceOperatorChargingAmountWithServiceCode(mtChargingDetails, operator);
        replaceOperatorChargingAmountWithServiceCode(smsSla, moK);
        replaceOperatorChargingAmountWithServiceCode(smsSla, mtK);
    }

    private void replaceOperatorChargingAmountWithServiceCode(Map<String, Object> smsSla, String direction) {
        Map<String, Object> directionSla = (Map<String, Object>) smsSla.get(direction);
        if(directionSla != null){
            Map<String, Object> chargingDetails = (Map<String, Object>) directionSla.get(chargingK);
            String operator = (String) smsSla.get(operatorK);
            doReplaceOperatorChargingAmountWithServiceCode(chargingDetails, operator);
        } else {
            logger.info("No sms sla available for direction[{}]", direction);
        }
    }

    private void doReplaceOperatorChargingAmountWithServiceCode(Map<String, Object> chargingDetails, String operator) {
        boolean isPaymentInstrumentAvailable = chargingDetails != null && chargingDetails.containsKey(allowedPaymentInstrumentsK);
        if(!isPaymentInstrumentAvailable) {
            return;
        }
        List<String> allowedPaymentInsList = (List<String>) chargingDetails.get(allowedPaymentInstrumentsK);

        if (allowedPaymentInsList != null && allowedPaymentInsList.contains(operatorChargingK)) {
            Map<String, String> serviceCodeValueMap = (Map<String, String>) systemConfiguration().find(operator + "-sms-service-code-charging-amount-mapping");

            for (Map.Entry<String, String> entry : serviceCodeValueMap.entrySet()) {
                if (entry.getValue().equals(chargingDetails.get(amountK))) {
                    chargingDetails.put(serviceCodeK, entry.getKey());
                    return;
                }
            }
        }
    }

    private Map<String, Object> parseChargingTrialPeriod(Map<String, Object> chargingTrialDetails) {
        Map<String, Object> chargingTrialPeriod = (Map<String, Object>) chargingTrialDetails.remove(chargingTrialPeriodK);

        if (chargingTrialPeriod != null) {
            chargingTrialPeriod.put(chargingTrialPeriodValueK,
                    Integer.parseInt((String) chargingTrialPeriod.remove(chargingTrialPeriodValueK)));
            chargingTrialDetails.put(chargingTrialPeriodK, chargingTrialPeriod);
        }
        return chargingTrialDetails;
    }

    private Map<String, Object> parseChargingDetails(Map<String, Object> chargingDetails) {
        if (chargingDetails != null) {
            Map<String, Object> chargingTrialDetails = (Map<String, Object>) chargingDetails.remove(chargingTrialK);

            if (chargingTrialDetails != null) {
                if (Boolean.parseBoolean((String) chargingTrialDetails.remove(chargingTrialAllowedK))) {
                    chargingTrialDetails.put(chargingTrialAllowedK, true);
                    chargingTrialDetails.put(chargingTrialMaxCountK,
                            Integer.parseInt((String) chargingTrialDetails.remove(chargingTrialMaxCountK)));
                } else {
                    chargingTrialDetails.put(chargingTrialAllowedK, false);
                }
                chargingDetails.put(chargingTrialK, parseChargingTrialPeriod(chargingTrialDetails));
            }

            return chargingDetails;
        } else {
            return new HashMap<String, Object>();
        }
    }

    private Date parseAppRequestDate(Object date) {
        Date appRequestDate = new Date();
        if(date != null) {
            try{
                appRequestDate = dateFormat.parse((String) date);
                if(appRequestDate.before(new Date())) appRequestDate = new Date();
            } catch (ParseException pe) {
                logger.debug("Invalid app request date [{}]", date);
                appRequestDate = new Date();
            }
        }
        return appRequestDate;
    }

    @Override
    @POST
    @Path("/create")
    public Map<String, Object> createOrUpdateApp(Map<String, Object> appReq) {
        logger.trace("Create Application Request Received [{}]", appReq);
        try {
            String currentUserName = (String) appReq.remove(createdByK);

            AuditTrail.setCurrentUser(currentUserName == null ? "" : currentUserName, spUserTypeK);

            Map<String, Map<String, Object>> ncsSlas = (Map<String, Map<String, Object>>) appReq.remove(ncsSlasK);

            appReq.put(maskNumberK, Boolean.parseBoolean((String) appReq.get(maskNumberK)));
            appReq.put(advertiseK, Boolean.parseBoolean((String) appReq.get(advertiseK)));
            appReq.put(governK, Boolean.parseBoolean((String) appReq.get(governK)));
            appReq.put(applyTaxK, Boolean.parseBoolean((String) appReq.get(applyTaxK)));
            appReq.put(categoryK, solturaK);
            appReq.put(activeProductionStartDateK, parseAppRequestDate(appReq.get(appRequestDateK)));
            appReq.put(appRequestDateK, new Date());

            boolean isExpirable = !appReq.containsKey(expireK) || Boolean.parseBoolean((String) appReq.get(expireK));
            appReq.put(expireK, isExpirable);

            if (isExpirable) {
                Date endDate = dateFormat.parse((String) appReq.get(endDateK));
                endDate = new Date(endDate.getTime() + (1000 * 60 * 60 * 24 - 1000));
                appReq.put(endDateK, endDate);
            } else {
                appReq.remove(endDateK);
            }

            if (appReq.get(idK) == null) {
                String appId = ProvIdGenerator.generateAppId();
                appReq.put(idK, appId);
                appReq.put(appIdK, appId);
                appReq.put(statusK, pendingApproveK);
            }

            List<Map<String, Object>> ncsesSummary = new ArrayList<Map<String, Object>>();

            for (Map<String, Object> ncsSla : ncsSlas.values()) {
                Map<String, Object> ncsSummary = new HashMap<String, Object>();
                ncsSummary.put(ncsTypeK, ncsSla.get(ncsTypeK));

                Object operator = ncsSla.get(operatorK);
                if (operator != null) {
                    ncsSummary.put(operatorK, operator);
                }
                ncsSummary.put(statusK, ncsConfiguredK);

                ncsesSummary.add(ncsSummary);
            }

            appReq.put(ncsesK, ncsesSummary);

            appRepositoryService().create(appReq);

            logger.info("Saving all {} slas received with app {}", ncsSlas.size(), appReq.get(appIdK));

            for (Map.Entry<String, Map<String, Object>> ncsSlaEntry : ncsSlas.entrySet()) {
                logger.info("creating ncs-sla named [{}]", ncsSlaEntry.getKey());

                Map<String, Object> sla = ncsSlaEntry.getValue();
                sla.put(statusK, pendingApproveK);
                sla.put(appIdK, appReq.get(appIdK));
                Object ncsType = sla.get(ncsTypeK);
                if (smsK.equals(ncsType)) {
                    if(sla.get(moK) == null && sla.get(mtK) == null){
                        throw new RuntimeException("Both MO and MT flows are not configured for SMS");
                    }

                    if(sla.get(moK) != null){
                        sla.put(moAllowedK, true);
                    }
                    if(sla.get(mtK) != null){
                        sla.put(mtAllowedK, true);
                    }

                    sla.put(subscriptionRequiredK, trueK.equals(sla.get(subscriptionRequiredK)));

                    validateAppThrottling(sla, appReq.get(spIdK).toString(), moK);
                    validateAppThrottling(sla, appReq.get(spIdK).toString(), mtK);

                    replaceOperatorChargingAmountWithServiceCode(sla);
                } else if (subscriptionK.equals(ncsType)) {
                    sla.put(allowHttpSubRequestsK, Boolean.parseBoolean((String) sla.remove(allowHttpSubRequestsK)));
                    sla.put(subscriptionConfirmationRequiredK, Boolean.parseBoolean((String) sla.remove(subscriptionConfirmationRequiredK)));
                    sla.put(chargingK, parseChargingDetails((Map<String, Object>) sla.remove(chargingK)));
                }

                ncsRepositoryService().createNcs(sla, (String) appReq.get(spIdK));
            }

            //todo handle transactions with mongo.
            Map<String, Object> response = new HashMap<String, Object>();
            response.put(appIdK, appReq.get(appIdK));
            response.put(statusK, pendingApproveK);
            response.put(statusCodeK, successCode);
            response.put(passwordK, appReq.get(passwordK));
            return response;
        } catch (SdpException e) {
            logger.error("Error while creating app", e);

            Map<String, Object> response = new HashMap<String, Object>();
            response.put(appIdK, appReq.get(appIdK));
            response.put(statusCodeK, e.getErrorCode());
            response.put(statusDescriptionK, e.getMessage());
            return response;
        } catch (Exception e) {
            logger.error("Error while creating app", e);

            Map<String, Object> response = new HashMap<String, Object>();
            response.put(appIdK, appReq.get(appIdK));
            response.put(statusCodeK, unknownErrorCode);
            response.put(statusDescriptionK, e.getMessage());
            return response;
        }
    }

    /**
     * @param detail - {app-id = someid}
     * @return {status = draft | pending | limited-production | production | rejected}
     */
    @Override
    @POST
    @Path("/status")
    public Map<String, Object> getAppStatus(Map<String, Object> detail) {
        logger.trace("App Status Request received", detail);
        logger.info("App status request received for appId [{}]", detail.get(appIdK));
        String status;
        String statusDescription;
        try {
            status = appRepositoryService().findAppStatus((String) detail.get(appIdK));
            statusDescription = "Request successfully processed";
        } catch (SdpException ex) {
            status = ex.getErrorCode();
            statusDescription = ex.getErrorDescription();
        }
        Map<String, Object> response = new HashMap<String, Object>();
        response.put(statusK, status);
        response.put(statusDescriptionK, statusDescription);
        return response;
    }

    /**
     * Return the app details map. Inside this map all the ncs-slas will be added as map.
     * Key of the ncs-slas map will bel 'operator'-'ncs-type'.
     *
     * @param detail {app-id : someid}
     * @return Return the app with ncs-slas as key. {...... app details ......, ncs-slas : {dialog-sms : {}}}}
     */
    @Override
    @POST
    @Path("/find")
    public Map<String, Object> getApp(Map<String, Object> detail) {
        logger.trace("Find App Request received", detail);
        logger.info("Find App Request received for appId [{}]", detail.get(appIdK));
        try {
            Map<String, Object> app = appRepositoryService().findByAppId((String) detail.get(appIdK));

            Map<String, Map<String, Object>> operatorSmsSlas = ncsRepositoryService().findByAppId((String) detail.get(appIdK));

            app.put(ncsSlasK, operatorSmsSlas);

            return app;

        } catch (Exception e) {

            logger.error("Error while finding app with app-id : " + detail.get(appIdK), e);
            javax.ws.rs.core.Response response = javax.ws.rs.core.Response.status(INTERNAL_SERVER_ERROR).build();
            throw new WebApplicationException(response);
        }
    }

    /**
     * Return a list of app details map. Inside each of these app detail map's all the ncs-slas will be added as map.
     * Key of the ncs-slas map will be 'operator'-'ncs-type'.
     *
     * @param appIds {app-ids : ["app-id1", "app-id2"]}
     * @return Return the app list {"apps":[{app_details_map1}, {app_details_map2}]}
     */
    @Override
    @POST
    @Path("/find/list")
    public Map<String, Object> getAppList(Map<String, Object> appIds) {
        logger.trace("Find Apps By App Ids Request received", appIds);
        logger.info("Find Apps By App Ids Request received for appIds [{}]", appIds.get(appIdsK));
        List<String> appIdList = (List<String>) appIds.get(appIdsK);
        try {
            List<Map<String, Object>> appList = new ArrayList<Map<String, Object>>();

            for(String appId: appIdList) {
                logger.info("Find App Details for app-id [{}]", appId);
                try {
                    Map<String, Object> app = appRepositoryService().findByAppId(appId);
                    Map<String, Map<String, Object>> operatorSmsSlas = ncsRepositoryService().findByAppId(appId);
                    app.put(ncsSlasK, operatorSmsSlas);
                    appList.add(app);
                } catch (SdpException e) {
                    logger.debug("Error occurred while processing the request [{}].", e.getErrorCode());
                }
            }
            Map<String, Object> response = new HashMap<String, Object>();
            response.put(appsK, appList);

            return response;
        } catch (Exception e) {
            logger.error("Error while finding apps with app-ids : " + appIds.get(appIdsK), e);
            javax.ws.rs.core.Response response = javax.ws.rs.core.Response.status(INTERNAL_SERVER_ERROR).build();
            throw new WebApplicationException(response);
        }
    }

    @Override
    @POST
    @Path("/is-app-name-available")
    public Map<String, Object> isAppNameAvailable(Map<String, Object> req) {
        logger.trace("Is App Name Available Request received", req);
        logger.info("Is App Name Available Request received for app name  [{}] ", req, req.get(nameK));
        Map<String, Object> response = new HashMap<String, Object>();
        try {
            boolean appNameExists = appRepositoryService().isAppNameExists((String) req.get(nameK));
            response.put(availableK, !appNameExists);
        } catch (Exception e) {
            logger.error("Error while validating app name", e);
            response.put(statusCodeK, systemErrorCode);
            response.put(statusDescriptionK, e.getMessage());
        }
        return response;
    }

    @Override
    @POST
    @Path("/disable")
    public Map<String, Object> disableApp(Map<String, Object> request) {
        logger.debug("Disable application request received {}", request);

        Map<String, Object> response = new HashMap<String, Object>();

        Object appId = request.get(appIdK);

        try {
            String currentUserName = (String) request.get("username");//TODO add a key in kite key box

            AuditTrail.setCurrentUser(currentUserName == null ? "" : currentUserName, adminUserTypeK);

            Map<String, Object> app = appRepositoryService().findById((String) appId);

            Object currentAppStatus = app.get(statusK);

            logger.info("Change the app [{}] status from [{}] to [{}]",
                    new Object[]{appId, currentAppStatus, suspendK});

            app.put(previousStateK, currentAppStatus);
            app.put(statusK, suspendK);
            app.put(reasonK, request.get(reasonK));

            appRepositoryService().update(app);

            response.put(statusCodeK, successCode);
        } catch (SdpException e) {
            logger.error("Error while disabling app [" + appId + "]", e);
            response.put(statusCodeK, e.getErrorCode());
        } catch (Exception e) {
            logger.error("Error while disabling app [" + appId + "]", e);
            response.put(statusCodeK, systemErrorCode);
        }

        return response;
    }
}
