/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.sms.prov.rest.impl;

import hms.kite.datarepo.audit.AuditTrail;
import hms.kite.provisioning.commons.util.ProvIdGenerator;
import hms.kite.sms.prov.rest.SpRestRepository;
import hms.kite.util.SdpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.util.KiteErrorBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
@Produces({"application/json"})
@Consumes({"application/json"})
@Path("/sp/")
public class SpRestRepositoryImpl implements SpRestRepository {

    private static final Logger logger = LoggerFactory.getLogger(SpRestRepositoryImpl.class);

    private List<String> spSlaStringParameterList;
    private List<String> spSlaBooleanParameterList;
    private List<String> spDoubleParameterList;

    private String getCurrentDate(String pattern) {
        return new SimpleDateFormat(pattern).format(new Date());
    }

    private String getDescription(String missingKey) {
        return "A key is not found withing the request [" + missingKey + "]";
    }

    private Map<String, Object> createSpResponse(String statusCode, String statusDescription) {
        Map<String, Object> response = new HashMap<String, Object>();

        response.put(statusCodeK, statusCode);
        response.put(statusDescriptionK, statusDescription);

        logger.debug("Response [{}]", response);

        return response;
    }

    @Override
    @POST
    @Path("/authenticate")
    public Map<String, Object> authenticate(Map<String, Object> request) {

        logger.trace("Sp Authenticate Request Received [{}]", request);
        logger.info("Sp Authenticate Request Received for corp user id [{}]", request.get(coopUserIdK));

        Map<String, Object> spMap;
        Map<String, Object> output = new HashMap<String, Object>();
        String status = blockedK;
        String statusCode;
        String spStatus;
        String spId = "";
        boolean isSolUser = false;

        try {
            spMap = spRepositoryService().findSpByCoopUserId((String) request.get(coopUserIdK));
            spStatus = (String) spMap.get(statusK);
            spId = (String) spMap.get(spIdK);
            logger.debug("SP [{}] is in status [{}]", spId, spStatus);

            if (approvedK.equals(spStatus)) {
                statusCode = successCode;
                status = allowedK;

            } else if (rejectK.equals(spStatus)) {
                statusCode = spIsSuspendedErrorCode;

            } else if (suspendedK.equals(spStatus)) {
                statusCode = spIsSuspendedErrorCode;

            } else if (pendingApproveK.equals(spStatus)) {
                statusCode = spNotApprovedErrorCode;

            } else {
                logger.error("Don't know how to handle the error: SP Status {}", spStatus);
                statusCode = systemErrorCode;
            }

            if (trueK.equals(spMap.get(solturaUserK))) {
                isSolUser = true;
            }

        } catch (SdpException ex) {
            logger.error("SP is not found error: corporate-user-id : {}, error : {}", request.get(coopUserIdK), ex);
            statusCode = ex.getErrorCode();
        } catch (Exception e) {
            logger.error("Don't know how to handle the error: {}", e);
            statusCode = systemErrorCode;
        }

        output.put(statusCodeK, statusCode);
        output.put(statusK, status);
        output.put(spIdK, spId);
        output.put(solturaUserK, isSolUser);

        return output;
    }

    @Override
    @POST
    @Path("/is-soltura-user")
    public Map<String, Object> isEnabledSolturaUser(Map<String, Object> request) {
        Map<String, Object> authResponse = authenticate(request);
        if (successCode.equals(authResponse.get(statusCodeK))) {
            if ((Boolean) authResponse.get(solturaUserK)) {
                authResponse.put(statusK, allowedK);
            } else {
                authResponse.put(statusK, blockedK);
                authResponse.put(statusCodeK, spIsASolturaUserErrorCode);
            }
        }
        return authResponse;
    }

    @Override
    @POST
    @Path("/create-sp")
    public Map<String, Object> createSp(Map<String, Object> request) {
        logger.debug("Create SP request is received: [{}]", request);

        String statusCode = systemErrorCode;
        String statusDescription = "Error occurred while validating the request";
        Map<String, Object> sp = new HashMap<String, Object>();

        try {
            for (String key : spSlaStringParameterList) {
                if (!request.containsKey(key)) {
                    statusDescription = getDescription(key);
                    return createSpResponse(statusCode, statusDescription);
                }

                sp.put(key, request.get(key));
            }

            for (String key : spSlaBooleanParameterList) {
                if (!request.containsKey(key)) {
                    statusDescription = getDescription(key);
                    return createSpResponse(statusCode, statusDescription);
                }

                sp.put(key, Boolean.valueOf(String.valueOf(request.get(key))));
            }

            for (String key: spDoubleParameterList) {
                if (!request.containsKey(key)) {
                    statusDescription = getDescription(key);
                    return createSpResponse(statusCode, statusDescription);
                }

                sp.put(key, Double.valueOf(String.valueOf(request.get(key))));
            }

            sp.put(spRequestDateK, getCurrentDate("MM/dd/yy"));
            sp.put(spIdK, ProvIdGenerator.generateSpid());

            logger.debug("SP is about to be saved [{}]", sp);

            spRepositoryService().create(sp);
            statusCode = successCode;
            statusDescription = "Requested Operation Completed Successfully";
        } catch (IllegalStateException ex) {
            logger.error("SP cannot be saved: errorMessage [{}], exception [{}]", ex.getMessage(), ex);
            statusDescription = "An error occurred while saving the data";
        } catch (Exception ex) {
            logger.error("An unknown error occurred while saving SP [{}]", ex);
            statusDescription = "An unknown error occurred while saving SP";
        }

        return createSpResponse(statusCode, statusDescription);
    }

    @Override
    @POST
    @Path("/get-sp")
    public Map<String, Object> getSp(Map<String, Object> sp) {
        logger.trace("SP availability verification request is received: [{}]", sp);
        String statusCode;
        Map<String, Object> output = new HashMap<String, Object>();

        if (sp.get(spIdK) != null) {
            try {
                output = spRepositoryService().findSpBySpId((String) sp.get(spIdK));
                statusCode = successCode;
            } catch (SdpException ex) {
                logger.error("No SP found for given sp-id [{}]", sp.get(spIdK));
                statusCode = spNotAvailableErrorCode;
            }
        } else {
            logger.error("sp-id is null: [request : {}]", sp);
            statusCode = systemErrorCode;
        }
        output.put(statusCodeK, statusCode);
        return output;
    }

    @Override
    @POST
    @Path("/get-sp-status")
    public Map<String, Object> getSpStatus(Map<String, Object> req) {
        Map<String, Object> sp = getSp(req);
        Map<String, Object> output = new HashMap<String, Object>();
        output.put(statusCodeK, sp.get(statusCodeK));
        output.put(spIdK, sp.get(spIdK));
        output.put(statusK, sp.get(statusK));
        return output;
    }

    @Override
    @POST
    @Path("/disable")
    public Map<String, Object> disableSp(Map<String, Object> req) {
        logger.info("Disable Sp request received {}", req);
        Map<String, Object> response = new HashMap<String, Object>();

        try {
            String currentUserName = (String) req.get("username");

            AuditTrail.setCurrentUser(currentUserName == null ? "" : currentUserName, adminUserTypeK);

            Map<String, Object> sp = spRepositoryService().findSpById((String) req.get(spIdK));

            Object currentSpStatus = sp.get(statusK);

            logger.info("Change the Sp [{}] status from [{}] to [{}]",
                    new Object[]{req.get(spIdK), currentSpStatus, suspendedK});

            sp.put(previousStateK, currentSpStatus);
            sp.put(statusK, suspendedK);
            sp.put(reasonK, req.get(reasonK));

            spRepositoryService().update(sp);

            response.put(statusCodeK, successCode);
        } catch (SdpException e) {
            logger.error("Error while disabling sp [" + req.get(spIdK) + "]", e);
            response.put(statusCodeK, e.getErrorCode());
        } catch (Exception e) {
            logger.error("Error while disabling sp [" + req.get(spIdK) + "]", e);
            response.put(statusCodeK, systemErrorCode);
        }
        return response;
    }

    public void setSpSlaStringParameterList(List<String> spSlaStringParameterList) {
        this.spSlaStringParameterList = spSlaStringParameterList;
    }

    public void setSpSlaBooleanParameterList(List spSlaBooleanParameterList) {
        this.spSlaBooleanParameterList = spSlaBooleanParameterList;
    }

    public void setSpDoubleParameterList(List<String> spDoubleParameterList) {
        this.spDoubleParameterList = spDoubleParameterList;
    }
}
