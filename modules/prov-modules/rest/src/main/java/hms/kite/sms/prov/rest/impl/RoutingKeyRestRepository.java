/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.sms.prov.rest.impl;

import com.mongodb.MongoException;
import hms.kite.util.KiteErrorBox;
import hms.kite.util.SdpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.smsRoutingKeyRepositoryService;
import static hms.kite.datarepo.RepositoryServiceRegistry.systemConfiguration;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.util.KiteKeyBox.spIdK;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
@Produces({"application/json"})
@Consumes({"application/json"})
@Path("/rk/")
public class RoutingKeyRestRepository implements hms.kite.sms.prov.rest.RoutingKeyRepository {

    private static final Logger logger = LoggerFactory.getLogger(RoutingKeyRestRepository.class);

    @POST
    @Path("/register")
    public Map<String, Object> registerRoutingKey(Map<String, Object> request) {
        logger.trace("Register Routing Key Request received [{}]", request);
        String spId = (String) request.get(spIdK);
        String operatorName = (String) request.get(operatorK);
        String shortcode = (String) request.get(shortcodeK);
        String keyword = (String) request.get(keywordK);

        logger.info("Register Routing Key Request received. operator [{}], short code [{}], keyword [{}]", new Object[]{operatorName, shortcode, keyword});

        HashMap<String, Object> response = new HashMap<String, Object>();

        try {
            smsRoutingKeyRepositoryService().registerRoutingKey(spId, operatorName, shortcode, keyword);
            response.put(statusK, successK);
            response.put(statusCodeK, KiteErrorBox.successCode);
            response.put(statusDescriptionK, "Key registered successfully");
        } catch (SdpException e) {
            logger.error("Error in registering the key ", e);
            response.put(statusK, failedK);
            response.put(statusCodeK, e.getErrorCode());
            response.put(statusDescriptionK, e.getMessage());
        } catch (Exception e) {
            logger.error("Error in registering the key ", e);
            response.put(statusK, failedK);
            response.put(statusCodeK, KiteErrorBox.systemErrorCode);
            response.put(statusDescriptionK, e.getMessage());
        }
        return response;
    }

    /**
     * Check the availability of routing keys.
     *
     * @param request - {shortcode:9696, operator:telkomsel, keyword:test}
     * @return {status:available/not-available, error-code:duplicate-key|reserved-key}
     */
    @POST
    @Path("/validate")
    public Map<String, Object> isRoutingKeyAvailable(Map<String, Object> request) {

        logger.trace("Validate Routing key Request Received [{}]", request);

        String operatorName = (String) request.get(operatorK);
        String shortcode = (String) request.get(shortcodeK);
        String keyword = (String) request.get(keywordK);

        logger.info("Validate Routing key Request Received. operator [{}], shortCode [{}], keyword [{}]", new Object[]{operatorName, shortcode, keyword});

        boolean serviceKeyword = systemConfiguration().isServiceKeyword(keyword);
        if (serviceKeyword) {
            Map<String, Object> response = new HashMap<String, Object>();
            response.put(statusK, notAvailableK);
            response.put(statusDescriptionK, KiteErrorBox.keywordIsServiceKeywordErrorCode);
            response.put(statusCodeK, KiteErrorBox.keywordIsServiceKeywordErrorCode);
            return response;
        }
        try {
            boolean routingKeyAvailable = smsRoutingKeyRepositoryService().isRoutingKeyAvailableForSp(operatorName, shortcode, keyword);
            //{status:available/not-available, error-code:duplicate-key|reserved-key}
            Map<String, Object> response = new HashMap<String, Object>();
            response.put(statusK, routingKeyAvailable ? availableK : notAvailableK);
            response.put(descriptionK, routingKeyAvailable ? availableK : duplicateKeywordK);
            return response;
        } catch (MongoException e) {
            logger.error("Error while checking the key ", e);
            javax.ws.rs.core.Response response = javax.ws.rs.core.Response.status(INTERNAL_SERVER_ERROR).build();
            throw new WebApplicationException(response);
        } catch (Exception e) {
            logger.error("Error in registering the key ", e);
            javax.ws.rs.core.Response response = javax.ws.rs.core.Response.status(INTERNAL_SERVER_ERROR).build();
            throw new WebApplicationException(response);
        }
    }

    @POST
    @Path("/find")
    public Map<String, Object> retrieveRoutingKeys(Map<String, Object> request) {
        logger.trace("Find Routing Key Request Received [{}]", request);
        String spId = (String) request.get(spIdK);
        logger.info("Find Routing Key Request Received for spId [{}]", spId);
        try {
            return (Map) smsRoutingKeyRepositoryService().retrieveAvailableRoutingKeys(spId);
        } catch (SdpException e) {
            logger.error("Error in registering the key ", e);
            javax.ws.rs.core.Response response = javax.ws.rs.core.Response.status(INTERNAL_SERVER_ERROR).build();
            throw new WebApplicationException(response);
        } catch (Exception e) {
            logger.error("Error in retrieving the key ", e);
            javax.ws.rs.core.Response response = javax.ws.rs.core.Response.status(INTERNAL_SERVER_ERROR).build();
            throw new WebApplicationException(response);
        }
    }

    @Override
    @POST
    @Path("/find-shared-shortcodes")
    public Map<String, Object> retrieveSharedShortcodes(Map<String, Object> message) {
        logger.trace("Find Shared Short Codes Request received [{}]", message);
        logger.info("Find Shared Short Codes Request received");
        Map<String, Object> reqObj = new HashMap<String, Object>();
        if (message.get(categoryK) == null) {
            throw new SdpException("E1700", "Invalid request, Domain category not found in the request");
        }
        Map<String, Object> reqQuery = new HashMap<String, Object>();
        reqQuery.put("$in", message.get(categoryK));
        reqObj.put(categoryK, reqQuery);

        try {
            Map result = smsRoutingKeyRepositoryService().retrieveDomainSpecificSharedShortCodes(reqObj);
            logger.debug("result size {} with elements {}.", result.size(), result);
            return result;
        } catch (SdpException e) {
            logger.error("Error in finding shared short codes ", e);
            javax.ws.rs.core.Response response = javax.ws.rs.core.Response.status(BAD_REQUEST).build();
            throw new WebApplicationException(response);
        } catch (Exception e) {
            logger.error("Error in finding shared short codes ", e);
            javax.ws.rs.core.Response response = javax.ws.rs.core.Response.status(INTERNAL_SERVER_ERROR).build();
            throw new WebApplicationException(response);
        }
    }

    @Override
    @POST
    @Path("/find-all")
    public List<Map<String, Object>> findAll(Map<String, Object> request) {
        logger.trace("Find All Routing Keys Request received [{}]", request);
        logger.info("Find All Routing Keys Request received for sp [{}].", request.get(spIdK));

        try {

            List result = smsRoutingKeyRepositoryService().findRoutingKeys((String) request.get(spIdK));
            logger.debug("result size {} with elements {}.", result.size(), result);
            return result;
        } catch (Exception e) {
            logger.error("Error in finding all routing keys assigned to sp", e);
            javax.ws.rs.core.Response response = javax.ws.rs.core.Response.status(INTERNAL_SERVER_ERROR).build();
            throw new WebApplicationException(response);
        }
    }
}
