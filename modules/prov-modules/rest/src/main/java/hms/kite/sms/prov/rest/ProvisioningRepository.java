/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.sms.prov.rest;

import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public interface ProvisioningRepository {

    /**
     * Create an app and related work flows and return generated app-id. In error send the error code.
     *
     * @param app {
     *            (read kite sla json format doc)
     *            <p/>
     *            ncs-slas :
     *            {
     *            safaricom-sms :
     *            {
     *            (read kite sla json format doc)
     *            routing-keys:({shortcode:9696, keyword=key1}, {shortcode:9694, keyword=key4})
     *            }
     *            airtel-sms :
     *            {
     *            (read kite sla json format doc)
     *            }
     *            subscription :
     *            {
     *            (read kite sla json format doc)
     *            }
     *            }
     * @return {
     *         app-id : someid, status = draft | pending | limited-production | production | rejected | failed ,
     *         status-code : sdp-errorcode }
     */
    Map<String, Object> createOrUpdateApp(Map<String, Object> app);

    /**
     * @param detail {app-id : someid}
     * @return same as the request send in #createSmsApp
     */
    Map<String, Object> getApp(Map<String, Object> detail);

    /**
     * @param appList {app-ids : [id1, id2]}
     * @return a list of objects same as the request send in #createSmsApp
     *         {"apps":[{app_details_map1}, {app_details_map2}]}
     */
    Map<String, Object> getAppList(Map<String, Object> appList);


    /**
     * @param detail - {app-id = someid}
     * @return {status = draft | pending | limited-production | production | rejected}
     */
    Map<String, Object> getAppStatus(Map<String, Object> detail);

    /**
     * @param req (name : "some-name"}
     * @return success - {available : true},
     *         fail - {status-code : "some -error code ", status-description : {"error description "}}
     */
    public Map<String, Object> isAppNameAvailable(Map<String, Object> req);

    /**
     * @param req - {app-id : "" , username : "", reason : ""}
     *            here the username will be the current logged in user's name
     * @return success response - {status-code : S1000, }
     *         failed resposne - {status-code : E1600 }
     */
    Map<String, Object> disableApp(Map<String, Object> req);
}
