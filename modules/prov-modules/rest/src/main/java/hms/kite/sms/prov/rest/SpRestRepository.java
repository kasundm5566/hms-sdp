/**
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.sms.prov.rest;

import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public interface SpRestRepository {

    /**
     * @param request {corporate-user-id = "some-id"}
     * @return success - {status-code : S1000, status : allowed|blocked, sp-id : "sp-id"}
     */
    public Map<String, Object> authenticate(Map<String, Object> request);

    /**
     * @param request {corporate-user-id = "some-id"}
     * @return success response - {status-code : S1000, status : allowed, sp-id : "sp-id"}
     *         failed resposne - {status-code : E1819 , status : blocked}
     */
    public Map<String, Object> isEnabledSolturaUser(Map<String, Object> request);

    /**
     * TODO
     *
     * @param sp
     * @return
     */
    Map<String, Object> createSp(Map<String, Object> sp);

    /**
     * TODO
     *
     * @param req
     * @return
     */
    Map<String, Object> getSp(Map<String, Object> req);

    /**
     * TODO
     *
     * @param req
     * @return
     */
    Map<String, Object> getSpStatus(Map<String, Object> req);

    /**
     * @param req - {sp-id : "" , username : "", reason : ""}
     *            here the username will be the current logged in user's name
     * @return success response - {status-code : S1000, }
     *         failed resposne - {status-code : E1600 }
     */
    Map<String, Object> disableSp(Map<String, Object> req);
}
