/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.downloadable.ui.buildfile.common.impl;

import com.vaadin.data.Validator;
import com.vaadin.ui.*;
import hms.kite.prov.downloadable.DownloadablePortlet;
import hms.kite.prov.downloadable.ui.buildfile.common.UploadHandler;
import hms.kite.prov.downloadable.ui.buildfile.create.DownloadableBuildFilePlatformLayout;
import hms.kite.provisioning.commons.ui.component.popup.ConfirmationBox;
import hms.kite.util.SystemUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.OutputStream;
import java.net.URL;
import java.util.List;

import static hms.kite.prov.downloadable.DownloadableServiceRegistry.buildFileRepositoryService;
import static hms.kite.prov.downloadable.DownloadableServiceRegistry.getMaxBuildFileSize;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 * <p/>
 * This component will allow user to upload build files
 */
public class DefaultUploadHandler extends CustomComponent
        implements Upload.SucceededListener,
        Upload.FailedListener,
        Upload.ProgressListener,
        Upload.Receiver,
        UploadHandler {

    private static Logger logger = LoggerFactory.getLogger(DefaultUploadHandler.class);

    private DownloadablePortlet application;
    private DownloadableBuildFilePlatformLayout downloadableBuildFilePlatformLayout;
    private VerticalLayout mainVLayout;
    private HorizontalLayout uploadHLayout;
    private HorizontalLayout processingHLayout;

    private long maxSize;
    private boolean cancel;
    private String caption;
    private String description;
    private String filename;
    private Upload upload;
    private Label statusLbl;
    private ProgressIndicator progressIndicator;
    private Button deleteBtn;
    private String removeStyle;
    private String addStyle;
    private String fileId;

    private void init() {
        maxSize = getMaxBuildFileSize() * 1024;
        cancel = true;
        removeStyle = "downloadable-error";
        addStyle = "downloadable-success";
        statusLbl.addStyleName("downloadable-upload-status");

        createMainLayout();
        createProcessingLayout();
        createDeleteBtn();
        reloadUploadComponents();
    }

    private void createMainLayout() {
        mainVLayout.setCaption(caption);
        mainVLayout.addComponent(uploadHLayout);
        mainVLayout.addComponent(processingHLayout);
    }

    private void createProcessingLayout() {
        processingHLayout.setSpacing(true);
        processingHLayout.setVisible(false);

        progressIndicator = new ProgressIndicator();
        progressIndicator.setWidth("100%");
        processingHLayout.addComponent(progressIndicator);

        Button cancelBtn = new Button("Cancel", new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                cancel = true;
                upload.interruptUpload();
            }
        });
        cancelBtn.setStyleName("small");
        processingHLayout.addComponent(cancelBtn);
    }

    private void createDeleteBtn() {
        deleteBtn = new Button(application.getMsg("downloadable.buildfile.upload.file.delete.caption"));
        deleteBtn.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                deleteUpload();
            }
        });
        deleteBtn.setVisible(false);
    }

    private void reloadUploadComponents() {
        createUploadComponent();

        statusLbl.setValue(null);

        uploadHLayout.removeAllComponents();
        uploadHLayout.setDescription(description);
        uploadHLayout.addComponent(upload);
        uploadHLayout.addComponent(deleteBtn);
        uploadHLayout.addComponent(statusLbl);
    }

    private void createUploadComponent() {
        upload = new Upload(null, this);
        upload.setImmediate(true);

        upload.addListener((Upload.SucceededListener) this);
        upload.addListener((Upload.FailedListener) this);
        upload.addListener((Upload.ProgressListener) this);
    }

    private void checkFileTypes(String filename) {
        cancel = true;

        String[] splitFileName = filename.split("\\.(?=[^\\.]+$)");

        if (splitFileName.length > 1) {
            String fileType = splitFileName[1];

            List<String> currentPlatformSupportedFileTypes = downloadableBuildFilePlatformLayout.getCurrentPlatformSupportedFileTypes();

            logger.debug("File extension is [{}] and supported file extension list [{}]", fileType, currentPlatformSupportedFileTypes);

            for (String type : currentPlatformSupportedFileTypes) {
                if (fileType.equals(type)) {
                    cancel = false;
                    break;
                }
            }
        }

        if (cancel) {
            setStatusLbl(addStyle, removeStyle, application.getMsg("downloadable.buildfile.upload.file.file.type.error"));
            upload.interruptUpload();
        }
    }

    private void setStatusLbl(String removeStyle, String addStyle, String message) {
        statusLbl.removeStyleName(removeStyle);
        statusLbl.addStyleName(addStyle);
        statusLbl.setValue(message);
    }

    private void checkConfirmation() {
        ConfirmationBox confirmationBox = new ConfirmationBox(
                application.getMsg("downloadable.buildfile.confirmationbox.title"),
                application.getMsg("downloadable.buildfile.confirmationbox.yes.caption"),
                application.getMsg("downloadable.buildfile.confirmationbox.no.caption"),
                application.getMsg("downloadable.buildfile.confirmationbox.upload.delete.message"),
                new ConfirmationBox.ConfirmationListener() {
                    @Override
                    public void onYes() {
                        reloadUploadComponents();
                        deleteBtn.setVisible(false);
                        cancel = true;
                        filename = "";
                        buildFileRepositoryService().closeGridFsFileOutputStream(fileId);
                    }

                    @Override
                    public void onNo() {
                    }
                },
                application.getMainWindow());
        confirmationBox.viewConfirmation();
    }

    @Override
    public OutputStream receiveUpload(String filename, String MIMEType) {
        logger.debug("File information [{}] [{}]", filename, MIMEType);

        checkFileTypes(filename);
        this.filename = filename;
        fileId = Long.toString(SystemUtil.getCorrelationId());
        return buildFileRepositoryService().getGridFsFileOutputStream(fileId, filename);
    }

    @Override
    public void uploadSucceeded(Upload.SucceededEvent event) {
        if (!cancel) {
            logger.debug("File uploaded successfully");
            setStatusLbl(removeStyle, addStyle, application.getMsg("downloadable.buildfile.upload.file.success.status"));
            cancel = false;
            processingHLayout.setVisible(false);
            uploadHLayout.removeComponent(upload);
            deleteBtn.setVisible(true);
        } else {
            logger.debug("File has uploaded but it is not valid");
        }
    }

    @Override
    public void uploadFailed(Upload.FailedEvent event) {
        logger.debug("File uploading has failed");
        if (!cancel) {
            setStatusLbl(addStyle, removeStyle, application.getMsg("downloadable.buildfile.upload.file.fail.status"));
        }
        processingHLayout.setVisible(false);
    }

    @Override
    public void updateProgress(long readBytes, long contentLength) {
        if (readBytes > maxSize || contentLength > maxSize) {
            setStatusLbl(addStyle, removeStyle, application.getMsg("downloadable.buildfile.upload.file.file.size.error"));
            cancel = true;
            upload.interruptUpload();
            return;
        }
        processingHLayout.setVisible(true);
        progressIndicator.setValue(new Float(readBytes / (float) contentLength));
    }

    @Override
    public void validate() {
        if (cancel) {
            throw new Validator.InvalidValueException(String.format(application.getMsg("downloadable.buildfile.upload.file.file.not.upload.error"), caption));
        }
        if (upload.isUploading()) {
            throw new Validator.InvalidValueException(application.getMsg("downloadable.buildfile.upload.file.file.is.uploading.error"));
        }
    }

    @Override
    public void deleteUpload() {
        checkConfirmation();
    }

    @Override
    public void deleteUploadByForce() {
        buildFileRepositoryService().closeGridFsFileOutputStream(fileId);
    }

    @Override
    public void setUploadFile(URL url, String fileId, String filename) {
        this.fileId = fileId;
        this.filename = filename;

        uploadHLayout.removeComponent(upload);
        cancel = false;
        statusLbl.setValue("<a href='" + url + "' target='_blank' >Download</a>");
        statusLbl.setContentMode(Label.CONTENT_XHTML);
    }

    @Override
    public void setDeleteBtn(boolean check) {
        deleteBtn.setVisible(check);
    }

    @Override
    public boolean isFileUploaded() {
        return !cancel;
    }

    @Override
    public String getFilename() {
        if (filename != null) {
            return filename;
        } else {
            return "";
        }
    }

    @Override
    public VerticalLayout getUploadHLayout() {
        return mainVLayout;
    }

    @Override
    public String getFileId() {
        return fileId;
    }

    public DefaultUploadHandler(DownloadablePortlet application,
                                String caption,
                                String description,
                                DownloadableBuildFilePlatformLayout downloadableBuildFilePlatformLayout) {
        this.application = application;
        this.caption = caption;
        this.description = description;
        this.downloadableBuildFilePlatformLayout = downloadableBuildFilePlatformLayout;

        mainVLayout = new VerticalLayout();
        uploadHLayout = new HorizontalLayout();
        processingHLayout = new HorizontalLayout();
        statusLbl = new Label();

        init();
    }
}