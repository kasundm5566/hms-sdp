/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.downloadable.ui.buildfile.create;

import com.vaadin.data.Validator;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.BaseTheme;
import hms.kite.prov.downloadable.DownloadablePortlet;
import hms.kite.prov.downloadable.ui.buildfile.common.DevicesPlatformVersions;
import hms.kite.prov.downloadable.ui.common.ViewMode;
import hms.kite.provisioning.commons.ui.DataComponent;
import hms.kite.provisioning.commons.ui.VaadinUtil;
import hms.kite.provisioning.commons.ui.component.popup.ConfirmationBox;
import hms.kite.provisioning.commons.ui.event.PortletEventSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.prov.downloadable.DownloadableServiceRegistry.buildFileRepositoryService;
import static hms.kite.prov.downloadable.ui.common.ViewMode.*;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 * <p/>
 */
public class DownloadableBuildFileMainLayout extends VerticalLayout implements DataComponent {

    private static final Logger logger = LoggerFactory.getLogger(DownloadableBuildFileMainLayout.class);

    private DownloadablePortlet application;
    private Map<String, Object> eventData;
    private String corporateUserId;

    private DownloadableBuildFilePlatformLayout buildFilePlatformLayout;
    private DownloadableBuildFileBuildInfoLayout buildFileBuildInfoLayout;
    private DownloadableBuildFileSupportedDevLayout buildFileSupportedDevLayout;

    private Panel platformConfigPanel;
    private Panel buildInfoConfigPanel;
    private Panel supportedDevConfigPanel;
    private Button controlSupportedDeviceBtn;
    private ViewMode viewMode;
    private Label validationErrorLbl;
    private String appId;
    private Object currentEventOrigin;

    private void init() {
        logger.debug("Initializing the Downloadable Application Build File configuration page started");

        try {
            Map<String, Object> sp = spRepositoryService().findSpByCoopUserId(corporateUserId);
            if (sp != null) {
                logger.debug("Returned SP Details [{}]", sp.toString());

                appId = (String) eventData.get(appIdK);
                viewMode = EDIT;
                currentEventOrigin = eventData.get(currentEventOriginK);

                createMainLayout();
            } else {
                logger.error("No Sp Details found with the given corporate user id [{}]", corporateUserId);
            }
        } catch (Exception e) {
            logger.error("No Sp found with the given corporate user id [{}]", corporateUserId);
        }
    }

    private void createMainLayout() {
        Map<String, DevicesPlatformVersions> devicesPlatformVersionsMap = new LinkedHashMap<String, DevicesPlatformVersions>();
        VerticalLayout anyDeviceVLayout = new VerticalLayout();

        createValidationErrorLbl();
        createBuildFilePlatformLayout(devicesPlatformVersionsMap, anyDeviceVLayout);
        createBuildFileBuildInfoLayout();
        createBuildFileSupportedDevLayout(devicesPlatformVersionsMap, anyDeviceVLayout);
        createControlButton();
    }

    private void createValidationErrorLbl() {
        validationErrorLbl = new Label();
        validationErrorLbl.addStyleName("downloadable-error");
        validationErrorLbl.addStyleName("downloadable-validation");
    }

    private void createBuildFilePlatformLayout(Map<String, DevicesPlatformVersions> devicesPlatformVersionsMap, VerticalLayout anyDeviceVLayout) {
        buildFilePlatformLayout = new DownloadableBuildFilePlatformLayout(application, devicesPlatformVersionsMap, anyDeviceVLayout, this, validationErrorLbl);
        platformConfigPanel = new Panel(application.getMsg("downloadable.buildfile.platform.configuration.caption"), buildFilePlatformLayout);
    }

    private void createBuildFileBuildInfoLayout() {
        buildFileBuildInfoLayout = new DownloadableBuildFileBuildInfoLayout(application, buildFilePlatformLayout, validationErrorLbl);
        buildInfoConfigPanel = new Panel(application.getMsg("downloadable.buildfile.build.configuration.caption"), buildFileBuildInfoLayout);
        buildInfoConfigPanel.setVisible(false);
        buildInfoConfigPanel.addStyleName("downloadable-build-info");
    }

    private void createBuildFileSupportedDevLayout(Map<String, DevicesPlatformVersions> devicesPlatformVersionsMap, VerticalLayout anyDeviceVLayout) {
        buildFileSupportedDevLayout = new DownloadableBuildFileSupportedDevLayout(application, devicesPlatformVersionsMap, anyDeviceVLayout, validationErrorLbl);
        supportedDevConfigPanel = new Panel(application.getMsg("downloadable.buildfile.supported.device.configuration.caption"), buildFileSupportedDevLayout);
        supportedDevConfigPanel.setVisible(false);
        supportedDevConfigPanel.addStyleName("downloadable-supported");
    }

    private void createControlButton() {
        final String control = application.getMsg("downloadable.buildfile.supported.device.configuration.caption");

        controlSupportedDeviceBtn = new Button();
        controlSupportedDeviceBtn.setVisible(false);
        controlSupportedDeviceBtn.setCaption("Show " + control);
        controlSupportedDeviceBtn.setStyleName(BaseTheme.BUTTON_LINK);
        controlSupportedDeviceBtn.addStyleName("downloadable-show-hide");
        controlSupportedDeviceBtn.setHeight("30px");
        controlSupportedDeviceBtn.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    if (supportedDevConfigPanel.isVisible()) {
                        logger.debug("Collapsing supported devices");
                        supportedDevConfigPanel.setVisible(false);
                        controlSupportedDeviceBtn.setCaption("Show " + control);
                    } else {
                        logger.debug("Expanding supported devices");
                        supportedDevConfigPanel.setVisible(true);
                        controlSupportedDeviceBtn.setCaption("Hide " + control);
                    }
                } catch (Exception e) {
                    logger.error("Unexpected Error occurred while expanding external repository", e);
                }
            }
        });
    }

    private Component createButtonBar() {
        HorizontalLayout buttonBar = new HorizontalLayout();
        buttonBar.setSpacing(true);

        buttonBar.addComponent(createBackButton());

        if (viewMode.equals(EDIT)) {
            buttonBar.addComponent(createSaveButton());
            buttonBar.addComponent(createCancelButton());
        } else if (viewMode.equals(CONFIRM)) {
            buttonBar.addComponent(createConfirmButton());
        }
        return buttonBar;
    }

    private Button createSaveButton() {
        Button button = new Button(application.getMsg("downloadable.form.save.button"));
        button.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    save();
                } catch (Exception e) {
                    logger.error("Unexpected Error occurred while configuring Downloadable Downloadable Application Build File", e);
                }
            }
        });
        return button;
    }

    private Button createConfirmButton() {
        Button button = new Button(application.getMsg("downloadable.form.confirm.button"));
        button.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    confirmed();
                } catch (Exception e) {
                    logger.error("Unexpected Error occurred while configuring Downloadable Application Build File", e);
                }
            }
        });
        return button;
    }

    private Button createBackButton() {
        Button button = new Button(application.getMsg("downloadable.form.back.button"));
        button.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    if (viewMode.equals(EDIT)) {
                        checkConfirmation("go back");
                    } else {
                        goBack();
                    }
                } catch (Exception e) {
                    logger.error("Unexpected Error occurred while going back", e);
                }
            }
        });
        return button;
    }

    private Button createCancelButton() {
        Button button = new Button(application.getMsg("downloadable.form.cancel.button"));
        button.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    if (viewMode.equals(EDIT)) {
                        checkConfirmation("cancel");
                    } else {
                        cancel();
                    }
                } catch (Exception e) {
                    logger.error("Unexpected Error occurred while configuring going back", e);
                }
            }
        });
        return button;
    }

    private void checkConfirmation(final String action) {
        ConfirmationBox confirmationBox = new ConfirmationBox(
                application.getMsg("downloadable.buildfile.confirmationbox.title"),
                application.getMsg("downloadable.buildfile.confirmationbox.yes.caption"),
                application.getMsg("downloadable.buildfile.confirmationbox.no.caption"),
                String.format(application.getMsg("downloadable.buildfile.confirmationbox.main.message"), action),
                new ConfirmationBox.ConfirmationListener() {

                    @Override
                    public void onYes() {
                        if (createBuildFileEventK.equals(currentEventOrigin)) {
                            buildFileBuildInfoLayout.cancelUploads();
                        }

                        if (action.equals("go back")) {
                            goBack();
                        } else if (action.equals("cancel")) {
                            cancel();
                        }
                    }

                    @Override
                    public void onNo() {
                    }
                },
                application.getMainWindow());
        confirmationBox.viewConfirmation();
    }

    private void goBack() {
        logger.debug("Redirecting to the previous UI");

        if (viewMode.equals(CONFIRM)) {
            viewMode = EDIT;
            reloadComponents();
        } else if (viewMode.equals(EDIT) || viewMode.equals(VIEW)) {
            selectEventAndFire();
        }
    }

    private void cancel() {
        logger.debug("Redirecting to the application creation form");

        PortletEventSender.send(new HashMap(), provRefreshEvent, application);
        application.closePortlet();
    }

    private void save() {
        try {
            validate();
            viewMode = CONFIRM;
            reloadComponents();
        } catch (Validator.InvalidValueException e) {
            logger.warn("Validation Failed for App ID [{}] - Cause [{}]  ", appId, e.getMessage());
        }
    }

    private void confirmed() {
        logger.debug("Saving Downloadable Application Build File....");

        List<Map<String, Object>> buildFilesList = application.getBuildFilesList(appId);
        Map<String, Object> buildFile = application.getBuildFileData(eventData);

        if (buildFile == null) {
            buildFile = new HashMap<String, Object>();
        }
        if (buildFilesList == null) {
            buildFilesList = new LinkedList<Map<String, Object>>();
        }

        getData(buildFile);

        if (createBuildFileEventK.equals(currentEventOrigin)) {
            buildFile.put(appIdK, appId);
            buildFile.put(spIdK, eventData.get(spIdK));

            Object appStatus = eventData.get(appStatusK);

            if (initialK.equals(appStatus)) {
                buildFile.put(statusK, initialK);
            } else if (draftK.equals(appStatus)) {
                buildFile.put(statusK, draftK);
            } else {
                buildFile.put(statusK, pendingApproveK);
            }

            if (initialK.equals(application.getFromSession(downloadableSlaStatusSessionK))) {
                application.addToSession(downloadableSlaStatusSessionK, configuredK);
            }

            Map<String, Object> dbBuildFile = buildFileRepositoryService().createBuildFile(buildFile);
            buildFilesList.add(dbBuildFile);

            logger.debug("Configured Build File Information [{}] ", dbBuildFile);
        } else if (reConfigureBuildFileEventK.equals(currentEventOrigin)) {
            if (buildFile.containsKey(idK)) {
                buildFileRepositoryService().updateBuildFile(buildFile);

                logger.debug("Reconfigured Build File Information [{}] ", buildFile);
            } else {
                throw new IllegalStateException("Unknown error occurred while updating build file");
            }
        }

        application.addToSession(buildFilesDataListSessionK, buildFilesList);

        selectEventAndFire();
    }

    private void selectEventAndFire() {
        Object eventOrigin = eventData.get(eventOriginK);
        eventData.put(eventOriginK, currentEventOrigin);
        eventData.remove(currentEventOriginK);
        if (eventOrigin != null) {
            if (eventOrigin.equals(createNcsK)) {
                PortletEventSender.send(eventData, provCreateNcsEvent, application);
            } else if (eventOrigin.equals(viewNcsK)) {
                PortletEventSender.send(eventData, provViewNcsEvent, application);
            } else if (eventOrigin.equals(reconfigureNcsK)) {
                PortletEventSender.send(eventData, provReConfigureNcsEvent, application);
            } else if (eventOrigin.equals(adminManageBuildFilesEventK)) {
                PortletEventSender.send(null, adminManageBuildFilesEventK, application);
            } else {
                logger.error("Event fired from unknown origin [{}]", eventOrigin);
            }
        } else {
            logger.error("Event doesn't have an origin");
        }
        application.closePortlet();
    }

    public DownloadableBuildFileMainLayout(DownloadablePortlet application, Map<String, Object> eventData,
                                           String corporateUserId) {
        this.application = application;
        this.eventData = eventData;
        this.corporateUserId = corporateUserId;

        init();
    }

    public void reloadComponents() {
        removeAllComponents();

        addComponent(validationErrorLbl);
        setComponentAlignment(validationErrorLbl, Alignment.MIDDLE_CENTER);
        addComponent(platformConfigPanel);
        addComponent(buildInfoConfigPanel);
        addComponent(supportedDevConfigPanel);
        addComponent(controlSupportedDeviceBtn);

        Component buttonBar = createButtonBar();
        buttonBar.addStyleName("downloadable-margin");
        addComponent(buttonBar);
        setComponentAlignment(buttonBar, Alignment.MIDDLE_CENTER);

        if (viewMode.equals(ViewMode.VIEW)) {
            VaadinUtil.setAllReadonly(true, buildFilePlatformLayout);
            buildFileBuildInfoLayout.setUploadDeleteBtn();
            VaadinUtil.setAllReadonly(true, buildFileBuildInfoLayout);
            VaadinUtil.setAllReadonly(true, buildFileSupportedDevLayout);
        } else if (viewMode.equals(ViewMode.EDIT)) {
            VaadinUtil.setAllReadonly(false, buildFilePlatformLayout);
            VaadinUtil.setAllReadonly(false, buildFileBuildInfoLayout);
            VaadinUtil.setAllReadonly(false, buildFileSupportedDevLayout);
        } else if (viewMode.equals(ViewMode.CONFIRM)) {
            VaadinUtil.setAllReadonly(true, buildFilePlatformLayout);
            VaadinUtil.setAllReadonly(true, buildFileBuildInfoLayout);
            VaadinUtil.setAllReadonly(true, buildFileSupportedDevLayout);
        }
    }

    public void setConfigPanelVisible(boolean value) {
        buildInfoConfigPanel.setVisible(value);
        controlSupportedDeviceBtn.setVisible(value);
    }

    public boolean isConfigPanelVisible() {
        return (buildInfoConfigPanel.isVisible() || controlSupportedDeviceBtn.isVisible());
    }

    public void getData(Map<String, Object> buildFile) {
        buildFilePlatformLayout.getData(buildFile);
        buildFileBuildInfoLayout.getData(buildFile);
        buildFileSupportedDevLayout.getData(buildFile);
    }

    public void setData(Map<String, Object> buildFile) {
        buildFilePlatformLayout.setData(buildFile);
        buildFileBuildInfoLayout.setData(buildFile);
        buildFileSupportedDevLayout.setData(buildFile);
    }

    public void validate() {
        logger.debug("Validating Downloadable Application Build File parameters");

        buildFilePlatformLayout.validate();
        buildFileBuildInfoLayout.validate();
        buildFileSupportedDevLayout.validate();
    }

    public void setViewMode(ViewMode viewMode) {
        this.viewMode = viewMode;
    }

    public void setJadFileVisible(List<String> platformSupportedFileTypes) {
        for (String fileType : platformSupportedFileTypes) {
            if ("jad".equals(fileType)) {
                buildFileBuildInfoLayout.setJadFileEnable(true);
                break;
            } else {
                buildFileBuildInfoLayout.setJadFileEnable(false);
            }
        }
    }

    public boolean isFileUploaded() {
        return buildFileBuildInfoLayout.isFilesUploaded();
    }

    public void resetUploads() {
        buildFileBuildInfoLayout.resetUploads();
    }
}