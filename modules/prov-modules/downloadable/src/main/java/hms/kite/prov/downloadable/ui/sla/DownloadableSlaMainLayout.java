/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.downloadable.ui.sla;

import com.vaadin.data.Validator;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.BaseTheme;
import hms.kite.prov.downloadable.DownloadablePortlet;
import hms.kite.prov.downloadable.ui.buildfile.show.DownloadableBuildFileListLayout;
import hms.kite.prov.downloadable.ui.common.ViewMode;
import hms.kite.provisioning.commons.ui.DataComponent;
import hms.kite.provisioning.commons.ui.VaadinUtil;
import hms.kite.provisioning.commons.ui.component.popup.ConfirmationBox;
import hms.kite.provisioning.commons.ui.event.PortletEventSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.prov.downloadable.ui.common.ViewMode.*;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.prov.downloadable.DownloadableServiceRegistry.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 * <p/>
 * Description : This Class contains Downloadable Application SLA Main Layout
 */
public class DownloadableSlaMainLayout extends VerticalLayout implements DataComponent {

    private static final Logger logger = LoggerFactory.getLogger(DownloadableSlaMainLayout.class);

    private DownloadablePortlet application;
    private Map<String, Object> eventData;
    private String corporateUserId;

    private DownloadableSlaBasicLayout basicLayout;
    private DownloadableSlaRepoLayout repoLayout;
    private DownloadableBuildFileListLayout buildFileListLayout;
    private Panel basicConfPanel;
    private Panel repoConfPanel;
    private Panel buildFilesPanel;
    private Button controlRepoBtn;
    private ViewMode viewMode;
    private String appId;
    private Label validationErrorLbl;
    private boolean isStatusInitial;

    private void init() {
        logger.debug("Initializing the Downloadable Application SLA configuration page started");

        try {
            Map<String, Object> sp = spRepositoryService().findSpByCoopUserId(corporateUserId);
            if (sp != null) {
                logger.debug("Returned SP Details [{}]", sp.toString());

                eventData.put(dlAppMcdK, sp.get(dlAppMcdK));
                eventData.put(dlAppMdpdK, sp.get(dlAppMdpdK));
                appId = (String) eventData.get(appIdK);
                viewMode = EDIT;
                isStatusInitial = eventData.get(downloadableSlaStatusSessionK).equals(initialK);

                createMainLayout();
            } else {
                logger.error("No Sp Details found with the given corporate user id [{}]", corporateUserId);
            }
        } catch (Exception e) {
            logger.error("Error occurred while loading the downloadable sla for given sp \n", e);
        }
    }

    private void createMainLayout() {
        createValidationErrorLbl();
        createBasicInfo();

        if (!isBuildFilesDataListNull()) {
            createBuildFilesLayout();
        }
        createRepoLayout();

         if (getDefaultDownlodAllowed()) {
            createControlButton();
        }
    }

    private void createValidationErrorLbl() {
        validationErrorLbl = new Label();
        validationErrorLbl.addStyleName("downloadable-error");
        validationErrorLbl.addStyleName("downloadable-validation");
    }

    private void createBasicInfo() {
        basicLayout = new DownloadableSlaBasicLayout(application, eventData, corporateUserId, validationErrorLbl);
        basicConfPanel = new Panel(application.getMsg("downloadable.sla.basic.configuration.caption"), basicLayout);
    }

    private void createRepoLayout() {
        repoLayout = new DownloadableSlaRepoLayout(application, validationErrorLbl);
        repoConfPanel = new Panel(application.getMsg("downloadable.sla.repository,configuration.caption"), repoLayout);
        repoConfPanel.addStyleName("downloadable-repository");
        repoConfPanel.setVisible(false);
    }

    private void createBuildFilesLayout() {
        buildFileListLayout = new DownloadableBuildFileListLayout(application, eventData, corporateUserId, validationErrorLbl);
        buildFileListLayout.setAppId(appId);
        buildFilesPanel = new Panel(application.getMsg("downloadable.buildfile.list.buildfile.configuration.caption"), buildFileListLayout);
        buildFilesPanel.addStyleName("downloadable-build-files");
        buildFilesPanel.setVisible(false);
    }

    private void createControlButton() {
        final String control = application.getMsg("downloadable.sla.repository,configuration.caption");

        controlRepoBtn = new Button();
        controlRepoBtn.setCaption("Show " + control);
        controlRepoBtn.setStyleName(BaseTheme.BUTTON_LINK);
        controlRepoBtn.addStyleName("downloadable-show-hide");
        controlRepoBtn.setHeight("30px");
        controlRepoBtn.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    if (repoConfPanel.isVisible()) {
                        logger.debug("Collapsing external repository");
                        repoConfPanel.setVisible(false);
                        controlRepoBtn.setCaption("Show " + control);
                    } else {
                        logger.debug("Expanding external repository");
                        repoConfPanel.setVisible(true);
                        controlRepoBtn.setCaption("Hide " + control);
                    }
                } catch (Exception e) {
                    logger.error("Unexpected Error occurred while expanding external repository", e);
                }
            }
        });
    }

    private boolean isBuildFilesDataListNull() {
        return application.getBuildFilesList(appId) == null;
    }

    private Component createButtonBar() {
        HorizontalLayout buttonBar = new HorizontalLayout();
        buttonBar.setSpacing(true);

        buttonBar.addComponent(createBackButton());

        if (viewMode.equals(EDIT)) {
            if (isStatusInitial && isBuildFilesDataListNull()) {
                buttonBar.addComponent(createNextButton());
            } else if (!isBuildFilesDataListNull()) {
                buildFilesPanel.setVisible(true);
                buttonBar.addComponent(createSaveButton());
            } else {
                buttonBar.addComponent(createSaveButton());
            }
        } else if (viewMode.equals(CONFIRM)) {
            buttonBar.addComponent(createConfirmButton());
        } else if (viewMode.equals(VIEW)) {
            if (!isBuildFilesDataListNull()) {
                buildFilesPanel.setVisible(true);
            }
        }

        return buttonBar;
    }

    private Button createSaveButton() {
        Button button = new Button(application.getMsg("downloadable.form.save.button"));
        button.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    logger.debug("Validating Downloadable Application SLA parameters");
                    save();
                } catch (Exception e) {
                    logger.error("Unexpected Error occurred while configuring Downloadable Application SLA", e);
                }
            }
        });
        return button;
    }

    private Button createConfirmButton() {
        Button button = new Button(application.getMsg("downloadable.form.confirm.button"));
        button.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    logger.debug("Saving Downloadable Application SLA Parameter values in to the session");
                    confirmed();
                } catch (Exception e) {
                    logger.error("Unexpected Error occurred while configuring Downloadable Application SLA", e);
                }
            }
        });
        return button;
    }

    private Button createBackButton() {
        Button button = new Button(application.getMsg("downloadable.form.back.button"));
        button.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    logger.debug("Redirecting to the previous UI");
                    if (viewMode.equals(EDIT)
                            && eventData.get(currentEventOriginK).equals(createNcsK)
                            && !isBuildFilesDataListNull()) {
                        checkConfirmation("go back");
                    } else {
                        goBack();
                    }
                } catch (Exception e) {
                    logger.error("Unexpected Error occurred while going back", e);
                }
            }
        });
        return button;
    }

    private Button createNextButton() {
        Button button = new Button(application.getMsg("downloadable.form.next.button"));
        button.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    logger.debug("Redirecting to the next UI");
                    goNext();
                } catch (Exception e) {
                    logger.error("Unexpected Error occurred while going in to next UI", e);
                }
            }
        });
        return button;
    }


    private void checkConfirmation(final String action) {
        ConfirmationBox confirmationBox = new ConfirmationBox(
                application.getMsg("downloadable.buildfile.confirmationbox.title"),
                application.getMsg("downloadable.buildfile.confirmationbox.yes.caption"),
                application.getMsg("downloadable.buildfile.confirmationbox.no.caption"),
                String.format(application.getMsg("downloadable.buildfile.confirmationbox.main.message"), action),
                new ConfirmationBox.ConfirmationListener() {

                    @Override
                    public void onYes() {
                        if (action.equals("go back")) {
                            goBack();
                        }
                    }

                    @Override
                    public void onNo() {
                    }
                },
                application.getMainWindow());
        confirmationBox.viewConfirmation();
    }

    private void goBack() {
        if (viewMode.equals(CONFIRM)) {
            viewMode = EDIT;
            reloadComponents();
        } else if (viewMode.equals(EDIT) || viewMode.equals(VIEW)) {
            PortletEventSender.send(new HashMap(), provRefreshEvent, application);
            application.closePortlet();
        }
    }

    private void goNext() {
        try {
            validate();
            Map<String, Object> downloadableSlaData = new HashMap<String, Object>();

            getData(downloadableSlaData);
            downloadableSlaData.put(appIdK, appId);
            application.addToSession(downloadableSlaDataSessionK, downloadableSlaData);

            if (isStatusInitial) {
                application.addToSession(downloadableSlaStatusSessionK, initialK);
            }

            logger.debug("Saving SLA data in session {}", downloadableSlaData);

            fireEvent(provCreateBuildFileEventK);
        } catch (Validator.InvalidValueException e) {
            logger.warn("Validation Failed for App ID [{}] - Cause [{}]  ", appId, e.getMessage());
        }
    }

    private void save() {
        try {
            validate();
            viewMode = CONFIRM;
            reloadComponents();
        } catch (Validator.InvalidValueException e) {
            logger.warn("Validation Failed for App ID [{}] - Cause [{}]  ", appId, e.getMessage());
        }
    }

    private void confirmed() {
        Map<String, Object> downloadableSlaData = application.getDownloadableSlaData(appId);

        if (downloadableSlaData == null) {
            downloadableSlaData = new HashMap<String, Object>();
        }

        getData(downloadableSlaData);
        downloadableSlaData.put(appIdK, appId);
        downloadableSlaData.put(ncsTypeK, downloadableK);

        if (isStatusInitial) {
            application.addToSession(downloadableSlaStatusSessionK, configuredK);
        }

        application.addToSession(downloadableSlaDataSessionK, downloadableSlaData);

        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put(ncsTypeK, downloadableK);
        dataMap.put(statusK, successK);
        dataMap.put(ncsDataK, downloadableSlaData.get(chargingK));

        logger.debug("Configured Downloadable SLA [{}] ", downloadableSlaData);

        PortletEventSender.send(dataMap, ncsConfiguredK, application);
        application.closePortlet();
    }

    private void fireEvent(String eventType) {
        Object currentEventOrigin = eventData.get(currentEventOriginK);
        eventData.put(eventOriginK, currentEventOrigin);
        eventData.remove(currentEventOriginK);
        PortletEventSender.send(eventData, eventType, application);
        application.closePortlet();
    }

    public DownloadableSlaMainLayout(DownloadablePortlet application,
                                     Map<String, Object> eventData,
                                     String corporateUserId) {
        this.application = application;
        this.eventData = eventData;
        this.corporateUserId = corporateUserId;

        init();
    }

    public void reloadComponents() {
        removeAllComponents();

        addComponent(validationErrorLbl);
        addComponent(basicConfPanel);
        if(repoConfPanel != null) {
            addComponent(repoConfPanel);
            if (getDefaultDownlodAllowed()) {
                addComponent(controlRepoBtn);
            }
        }
        if (buildFilesPanel != null) {
            addComponent(buildFilesPanel);
        }

        Component buttonBar = createButtonBar();
        buttonBar.addStyleName("downloadable-margin");
        addComponent(buttonBar);
        setComponentAlignment(buttonBar, Alignment.MIDDLE_CENTER);

        if (viewMode.equals(ViewMode.VIEW)) {
            VaadinUtil.setAllReadonly(true, basicLayout);
            VaadinUtil.setAllReadonly(true, repoLayout);
        } else if (viewMode.equals(ViewMode.EDIT)) {
            VaadinUtil.setAllReadonly(false, basicLayout);
            if (isStatusInitial) {
                VaadinUtil.setAllReadonly(false, repoLayout);
            } else {
                VaadinUtil.setAllReadonly(true, repoLayout);
            }
            basicLayout.setPermissions();
            repoLayout.setPermissions();
        } else if (viewMode.equals(ViewMode.CONFIRM)) {
            VaadinUtil.setAllReadonly(true, basicLayout);
            VaadinUtil.setAllReadonly(true, repoLayout);
        }
        if (getDefaultDownlodAllowed()) {
            controlRepoBtn.setReadOnly(false);
        }
    }

    public void reloadBuildFiles(List<Map<String, Object>> buildFilesList) {
        if (buildFilesList != null) {
            if (buildFilesPanel == null) {
                createBuildFilesLayout();
                reloadComponents();
            }
            buildFileListLayout.reloadTable(buildFilesList);
        }
    }

    public void setViewMode(ViewMode viewMode) {
        this.viewMode = viewMode;
    }

    public void getData(Map<String, Object> data) {
        basicLayout.getData(data);
        repoLayout.getData(data);
    }

    public void setData(Map<String, Object> data) {
        basicLayout.setData(data);
        repoLayout.setData(data);
    }

    public void validate() {
        basicLayout.validate();
        repoLayout.validate();
    }
}
