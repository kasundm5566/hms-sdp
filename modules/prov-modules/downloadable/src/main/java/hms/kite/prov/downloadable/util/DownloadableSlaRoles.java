/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.downloadable.util;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class DownloadableSlaRoles {

    public static final String DOWNLOADABLE_PERMISSION_ROLE_PREFIX = "ROLE_PROV_DOWNLOADABLE";

    public static final String APPLICATION_ID = "APPLICATION_ID";
    public static final String CHARGING_NOTIFICATION_ENABLE = "CHARGING_NOTIFICATION_ENABLE";
    public static final String DOWNLOAD_ATTEMPT = "DOWNLOAD_ATTEMPT";
    public static final String MAX_CONCURRENT_DOWNLOAD = "MAX_CONCURRENT_DOWNLOAD";
    public static final String MAX_DOWNLOAD_PER_DAY = "MAX_DOWNLOAD_PER_DAY";
    public static final String LINK_EXPIRATION_TIME = "LINK_EXPIRATION_TIME";
    public static final String LINK_EXPIRATION_TIME_TYPE = "LINK_EXPIRATION_TIME_TYPE";
    public static final String SUBSCRIPTION_REQUIRED = "SUBSCRIPTION_REQUIRED";

    public static final String REPOSITORY = "REPOSITORY";

    public static final String UPLOAD_BUILD = DOWNLOADABLE_PERMISSION_ROLE_PREFIX + "_UPLOAD_BUILD";
    public static final String EDIT_BUILD = DOWNLOADABLE_PERMISSION_ROLE_PREFIX + "_EDIT_BUILD";
    public static final String DELETE_BUILD = DOWNLOADABLE_PERMISSION_ROLE_PREFIX + "_DELETE_BUILD";
    public static final String APPROVE_BUILD = DOWNLOADABLE_PERMISSION_ROLE_PREFIX + "_APPROVE_BUILD";
    public static final String REJECT_BUILD = DOWNLOADABLE_PERMISSION_ROLE_PREFIX + "_REJECT_BUILD";
    public static final String SUSPEND_BUILD = DOWNLOADABLE_PERMISSION_ROLE_PREFIX + "_SUSPEND_BUILD";
    public static final String RESTORE_BUILD = DOWNLOADABLE_PERMISSION_ROLE_PREFIX + "_RESTORE_BUILD";
}
