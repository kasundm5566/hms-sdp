/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.downloadable.ui.buildfile.common;

import com.vaadin.ui.CheckBox;

import java.util.LinkedList;
import java.util.List;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 * <p/>
 * This class will keep track of one platform version related checkboxes
 */
public class DevicesPlatformVersions {

    private List<CheckBox> specDeviceList;
    private CheckBox anyDeviceChkBox;
    private CheckBox versionChkBox;

    public DevicesPlatformVersions(List<CheckBox> specDeviceList) {
        this.specDeviceList = specDeviceList;
    }

    public DevicesPlatformVersions(CheckBox versionChkBox, String platformName, String version) {
        this.versionChkBox = versionChkBox;
        this.anyDeviceChkBox = new CheckBox("All " + platformName + " " + version);
        this.specDeviceList = new LinkedList<CheckBox>();
    }

    public List<CheckBox> getSpecDeviceList() {
        return specDeviceList;
    }

    public CheckBox getAnyDeviceChkBox() {
        return anyDeviceChkBox;
    }

    public CheckBox getVersionChkBox() {
        return versionChkBox;
    }

    public void setVersionChkBox(CheckBox versionChkBox) {
        this.versionChkBox = versionChkBox;
    }
}