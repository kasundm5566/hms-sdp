/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.downloadable.ui.sla;

import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Select;
import hms.kite.provisioning.commons.ui.AbstractBasicDetailsLayout;
import hms.kite.provisioning.commons.ui.BaseApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.prov.downloadable.util.DownloadableSlaRoles.DOWNLOADABLE_PERMISSION_ROLE_PREFIX;
import static hms.kite.prov.downloadable.util.DownloadableSlaRoles.REPOSITORY;
import static hms.kite.util.KiteKeyBox.dlRepoK;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class DownloadableSlaRepoLayout extends AbstractBasicDetailsLayout {

    private static final Logger logger = LoggerFactory.getLogger(DownloadableSlaRepoLayout.class);

    private Label validationErrorLbl;

    private FormLayout formLayout;
    private Select repoSelect;

    private void init() {
        logger.debug("Initializing the Downloadable Application SLA Repository Information form");

        formLayout = new FormLayout();
        formLayout.addStyleName("downloadable-ncs-form");
        formLayout.addStyleName("downloadable-padding-repository");

        createDlRepoSelect();

        addComponent(formLayout);
    }

    private void createDlRepoSelect() {
        repoSelect = getFieldWithPermission(REPOSITORY, new Select());
        if (isFieldNotNull(repoSelect)) {
            repoSelect.setCaption(application.getMsg("downloadable.sla.download.repository.caption"));
            repoSelect.setDescription(application.getMsg("downloadable.sla.download.repository.tooltip"));
            repoSelect.setWidth("170px");
            repoSelect.addItem("Internal");
            repoSelect.addItem("External");
            repoSelect.setNullSelectionAllowed(false);
            repoSelect.setValue("Internal");  //TODO remove hardcoded values
            formLayout.addComponent(repoSelect);
        }
    }

    public DownloadableSlaRepoLayout(BaseApplication application, Label validationErrorLbl) {
        super(application, DOWNLOADABLE_PERMISSION_ROLE_PREFIX);

        this.validationErrorLbl = validationErrorLbl;

        init();
    }

    @Override
    public void getData(Map<String, Object> dlAppSla) {
        getValueFromField(repoSelect, dlAppSla, dlRepoK);
    }

    @Override
    public void setData(Map<String, Object> dlAppSla) {
        setValueToField(repoSelect, dlAppSla.get(dlRepoK));
    }

    @Override
    public void validate() {
        if (isFieldNotNull(repoSelect)) {
            repoSelect.validate();
        }
    }

    @Override
    public void reset() {
        throw new UnsupportedOperationException("This function is not implemented");
    }

    @Override
    public void setPermissions() {
        setReadWritePermissionToComponent(repoSelect);
    }
}
