/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.downloadable;

import hms.kite.datarepo.BuildFileRepositoryService;
import hms.kite.provisioning.commons.ValidationRegistry;
import hms.kite.util.NullObject;

import java.util.Properties;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class DownloadableServiceRegistry {

    private static ValidationRegistry validationRegistry;
    private static Properties properties;
    private static Properties defaultValuesProperties;
    private static BuildFileRepositoryService buildFileRepositoryService;

    static {
        setValidationRegistry(new NullObject<ValidationRegistry>().get());
        setProperties(new NullObject<Properties>().get());
        setDefaultValuesProperties(new NullObject<Properties>().get());
        setBuildFileRepositoryService(new NullObject<BuildFileRepositoryService>().get());
    }

    public static ValidationRegistry validationRegistry() {
        return validationRegistry;
    }

    private static void setValidationRegistry(ValidationRegistry validationRegistry) {
        DownloadableServiceRegistry.validationRegistry = validationRegistry;
    }

    private static void setProperties(Properties properties) {
        DownloadableServiceRegistry.properties = properties;
    }

    public static BuildFileRepositoryService buildFileRepositoryService() {
        return buildFileRepositoryService;
    }

    private static void setBuildFileRepositoryService(BuildFileRepositoryService buildFileRepositoryService) {
        DownloadableServiceRegistry.buildFileRepositoryService = buildFileRepositoryService;
    }

    public static int getMaxNoOfDownloadAttempts() {
        return Integer.parseInt(properties.getProperty("downloadable.sla.max.attempts"));
    }

    public static int getMaxBuildFileSize() {
        return Integer.parseInt(properties.getProperty("downloadable.buildfile.max.file.size"));
    }

    public static String getCmsUrl() {
        return properties.getProperty("cms.core.download.url.prefix");
    }

    private static void setDefaultValuesProperties(Properties defaultValuesProperties) {
        DownloadableServiceRegistry.defaultValuesProperties = defaultValuesProperties;
    }

    public static boolean isDefaultDownloadAttemptAllow() {
        return Boolean.valueOf(defaultValuesProperties.getProperty("cms.default.value.download.attempt.allow"));
    }

    public static Integer getDefaultDownloadAttempt() {
        return Integer.valueOf(defaultValuesProperties.getProperty("cms.default.value.download.attempt"));
    }

    public static boolean isDefaultMaxConcurrentDownloadAllow() {
        return Boolean.valueOf(defaultValuesProperties.getProperty("cms.default.value.max.concurrent.download.allow"));
    }

    public static String getDefaultMaxConcurrentDownload() {
        return defaultValuesProperties.getProperty("cms.default.value.max.concurrent.download");
    }

    public static boolean isDefaultMaxDownloadPerDayAllow() {
        return Boolean.valueOf(defaultValuesProperties.getProperty("cms.default.value.max.download.per.day.allow"));
    }

    public static String getDefaultMaxDownloadPerDay() {
        return defaultValuesProperties.getProperty("cms.default.value.max.download.per.day");
    }

    public static boolean isDefaultExpirationTimeAllow() {
        return Boolean.valueOf(defaultValuesProperties.getProperty("cms.default.value.link.expiration.time.allow"));
    }

    public static String getDefaultExpirationTime() {
        return defaultValuesProperties.getProperty("cms.default.value.link.expiration.time");
    }

    public static boolean isDefaultExpirationTimeTypeAllow() {
        return Boolean.valueOf(defaultValuesProperties.getProperty("cms.default.value.link.expiration.time.type.allow"));
    }

    public static String getDefaultExpirationTimeType() {
        return defaultValuesProperties.getProperty("cms.default.value.link.expiration.time.type");
    }

    public static boolean getDefaultDownlodAllowed() {
        return Boolean.valueOf(defaultValuesProperties.getProperty("cms.default.value.download.allow"));
    }

    public static boolean isHelpButtonLinkEnable() {
        return Boolean.parseBoolean(defaultValuesProperties.getProperty("cms.help.link.enable"));
    }
}
