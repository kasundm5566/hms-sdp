/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.downloadable.ui.buildfile.create;

import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Label;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.VerticalLayout;
import hms.kite.prov.downloadable.DownloadablePortlet;
import hms.kite.prov.downloadable.ui.buildfile.common.DevicesPlatformVersions;
import hms.kite.provisioning.commons.ui.DataComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static hms.kite.datarepo.RepositoryServiceRegistry.deviceAndPlatformRepositoryService;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 * <p/>
 */
public class DownloadableBuildFileSupportedDevLayout extends VerticalLayout implements DataComponent {

    private static final Logger logger = LoggerFactory.getLogger(DownloadableBuildFileSupportedDevLayout.class);

    private DownloadablePortlet application;

    private VerticalLayout layout;
    private OptionGroup supportSpecDevOpGrp;
    private OptionGroup supportAnyDevOpGrp;
    private List<Map<String, Object>> allDevicesList;
    private Map<String, Map<String, CheckBox>> allDevicesChkBoxMap;
    private Map<String, CheckBox> allBrandsChkBoxMap;
    private Map<String, DevicesPlatformVersions> devicesPlatformVersionsMap;
    private VerticalLayout anyDeviceVLayout;
    private Label validationErrorLbl;

    private void init() {
        logger.debug("Initializing the Downloadable Build File Supported Devices information form");

        allDevicesList = deviceAndPlatformRepositoryService().findAllDevices();

        createDevicesTree();
    }

    private void createDevicesTree() {
        allDevicesChkBoxMap = new LinkedHashMap<String, Map<String, CheckBox>>();
        allBrandsChkBoxMap = new LinkedHashMap<String, CheckBox>();
        final Map<String, VerticalLayout> brandVerticalMap = new LinkedHashMap<String, VerticalLayout>();

        final VerticalLayout specDevVLayout = new VerticalLayout();
        final VerticalLayout anyDevVLayout = new VerticalLayout();

        supportSpecDevOpGrp = new OptionGroup();
        supportAnyDevOpGrp = new OptionGroup();

        supportSpecDevOpGrp.addItem(supportSpecDevTypeK);
        supportSpecDevOpGrp.setItemCaption(supportSpecDevTypeK,
                application.getMsg("downloadable.buildfile.supported.specific.device.caption"));
        supportSpecDevOpGrp.addStyleName("downloadable-margin");
        supportSpecDevOpGrp.select(supportSpecDevTypeK);
        supportSpecDevOpGrp.setImmediate(true);
        supportSpecDevOpGrp.addListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                logger.trace("Changing View of Supported Devices Type as [{}]", supportSpecDevTypeK);
                if (valueChangeEvent.getProperty().getValue() != null
                        && valueChangeEvent.getProperty().getValue().equals(supportSpecDevTypeK)) {
                    supportAnyDevOpGrp.select(null);
                    specDevVLayout.setVisible(true);
                    anyDevVLayout.setVisible(false);
                }
            }
        });

        supportAnyDevOpGrp.addItem(supportAnyDevTypeK);
        supportAnyDevOpGrp.setItemCaption(supportAnyDevTypeK,
                application.getMsg("downloadable.buildfile.supported.any.device.caption"));
        supportAnyDevOpGrp.addStyleName("downloadable-margin");
        supportAnyDevOpGrp.setImmediate(true);
        supportAnyDevOpGrp.addListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                logger.trace("Changing View of Supported Devices Type as [{}]", supportAnyDevTypeK);
                if (valueChangeEvent.getProperty().getValue() != null
                        && valueChangeEvent.getProperty().getValue().equals(supportAnyDevTypeK)) {
                    supportSpecDevOpGrp.select(null);
                    anyDevVLayout.setVisible(true);
                    specDevVLayout.setVisible(false);
                }
            }
        });

        for (Map<String, Object> device : allDevicesList) {
            final String devBrand = (String) device.get(supportDevBrandK);
            String devModel = (String) device.get(supportDevModelK);
            String devPlatform = (String) device.get(platformIdK);
            List<String> devPlatformVersionList = (List<String>) device.get(platformVersionsK);

            if (!allBrandsChkBoxMap.containsKey(devBrand)) {
                final VerticalLayout tempVLayout = new VerticalLayout();
                tempVLayout.addStyleName("downloadable-padding-child-category");
                tempVLayout.setVisible(false);
                brandVerticalMap.put(devBrand, tempVLayout);

                final CheckBox tempChkBox = new CheckBox("+ " + devBrand);
                tempChkBox.setImmediate(true);
                tempChkBox.addListener(new Property.ValueChangeListener() {
                    @Override
                    public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                        try {
                            if (valueChangeEvent.getProperty().getValue().equals(false)) {
                                logger.debug("Collapsing - " + devBrand);
                                tempVLayout.setVisible(false);
                                tempChkBox.setCaption("+ " + devBrand);
                            } else {
                                logger.debug("Expanding - " + devBrand);
                                tempVLayout.setVisible(true);
                                tempChkBox.setCaption("- " + devBrand);
                            }
                        } catch (Exception e) {
                            logger.error("Unexpected Error occurred while expanding or collapsing the brand", e);
                        }
                    }
                });
                allBrandsChkBoxMap.put(devBrand, tempChkBox);

                VerticalLayout brandVLayout = new VerticalLayout();

                brandVLayout.addComponent(tempChkBox);
                brandVLayout.addComponent(tempVLayout);

                specDevVLayout.addComponent(brandVLayout);
            }

            CheckBox childChkBox = new CheckBox(devModel);
            brandVerticalMap.get(devBrand).addComponent(childChkBox);

            if (allDevicesChkBoxMap.containsKey(devBrand)) {
                allDevicesChkBoxMap.get(devBrand).put(devModel, childChkBox);
            } else {
                Map<String, CheckBox> childMap = new HashMap<String, CheckBox>();
                childMap.put(devModel, childChkBox);
                allDevicesChkBoxMap.put(devBrand, childMap);
            }

            for (String version : devPlatformVersionList) {
                String pvKey = devPlatform + "-" + version;
                if (devicesPlatformVersionsMap.containsKey(pvKey)) {
                    devicesPlatformVersionsMap.get(pvKey).getSpecDeviceList().add(childChkBox);
                } else {
                    List<CheckBox> childChkBoxList = new LinkedList<CheckBox>();
                    childChkBoxList.add(childChkBox);
                    devicesPlatformVersionsMap.put(pvKey, new DevicesPlatformVersions(childChkBoxList));
                }
            }
        }

        layout = new VerticalLayout();
        layout.addComponent(supportSpecDevOpGrp);
        specDevVLayout.addStyleName("downloadable-padding-child-category");
        layout.addComponent(specDevVLayout);

        layout.addComponent(supportAnyDevOpGrp);
        anyDevVLayout.addStyleName("downloadable-padding-child-category");
        anyDevVLayout.setVisible(false);
        anyDevVLayout.addComponent(anyDeviceVLayout);
        layout.addComponent(anyDevVLayout);

        layout.addStyleName("downloadable-padding-devices");
        addComponent(layout);
    }

    private void clearAll() {
        for (Map.Entry brand : allBrandsChkBoxMap.entrySet()) {
            Map<String, CheckBox> modelMap = allDevicesChkBoxMap.get(brand.getKey());
            for (Map.Entry model : modelMap.entrySet()) {
                ((CheckBox) model.getValue()).setValue(false);
            }

            ((CheckBox) brand.getValue()).setValue(false);
        }
    }

    public DownloadableBuildFileSupportedDevLayout(DownloadablePortlet application,
                                                   Map<String, DevicesPlatformVersions> devicesPlatformVersionsMap,
                                                   VerticalLayout anyDeviceVLayout,
                                                   Label validationErrorLbl) {
        this.application = application;
        this.devicesPlatformVersionsMap = devicesPlatformVersionsMap;
        this.anyDeviceVLayout = anyDeviceVLayout;
        this.validationErrorLbl = validationErrorLbl;

        init();
    }

    public void getData(Map<String, Object> buildFile) {
        logger.debug("Get Data from Supported Devices Configuration Form");

        String supportedDevType = "";

        List<Map<String, Object>> deviceList = new LinkedList<Map<String, Object>>();

        boolean isModelAvailable;
        List<String> modelList;

        if (supportSpecDevOpGrp.getValue() != null) {
            supportedDevType = (String) supportSpecDevOpGrp.getValue();

            for (Map.Entry brand : allBrandsChkBoxMap.entrySet()) {
                CheckBox brandChkBox = (CheckBox) brand.getValue();

                if (brandChkBox.getValue().equals(true)) {

                    isModelAvailable = false;
                    modelList = new LinkedList<String>();

                    for (Map.Entry model : allDevicesChkBoxMap.get(brand.getKey()).entrySet()) {
                        if (((CheckBox) model.getValue()).getValue().equals(true)) {
                            modelList.add((String) model.getKey());
                            isModelAvailable = true;
                        }
                    }

                    if (isModelAvailable) {
                        Map<String, Object> deviceGrp = new HashMap<String, Object>();
                        deviceGrp.put(supportDevBrandK, brand.getKey());
                        deviceGrp.put(supportDevModelsK, modelList);
                        deviceList.add(deviceGrp);
                    }
                }
            }
        } else if (supportAnyDevOpGrp.getValue() != null) {
            supportedDevType = (String) supportAnyDevOpGrp.getValue();

            VerticalLayout anyDeviceChildVLayout = (VerticalLayout) anyDeviceVLayout.getComponent(0);

            isModelAvailable = false;
            modelList = new LinkedList<String>();

            for (Iterator i = anyDeviceChildVLayout.getComponentIterator(); i.hasNext(); ) {
                CheckBox model = (CheckBox) i.next();

                if (model.getValue().equals(true)) {
                    modelList.add(model.getCaption());
                    isModelAvailable = true;
                }
            }

            if (isModelAvailable) {
                Map<String, Object> deviceGrp = new HashMap<String, Object>();
                deviceGrp.put(supportDevBrandK, allK);
                deviceGrp.put(supportDevModelsK, modelList);
                deviceList.add(deviceGrp);
            }
        }

        buildFile.put(supportDevK, deviceList);

        logger.debug("Getting Supported Model List - [{}]", deviceList);

        buildFile.put(supportDevTypeK, supportedDevType);

        logger.debug("Getting Supported Device Type - [{}]", supportedDevType);
    }

    public void setData(Map<String, Object> buildFile) {
        logger.debug("Set Data to Supported Devices Configuration Form");

        String supportedDevType = (String) buildFile.get(supportDevTypeK);

        logger.debug("Setting Supported Device Type - [{}]", supportedDevType);

        List<Map<String, Object>> deviceList = (List<Map<String, Object>>) buildFile.get(supportDevK);

        logger.debug("Setting Supported Model List - [{}]", deviceList);

        clearAll();

        if (supportedDevType.equals(supportSpecDevTypeK)) {
            supportSpecDevOpGrp.select(supportedDevType);

            for (Map<String, Object> device : deviceList) {
                String devBrand = (String) device.get(supportDevBrandK);

                allBrandsChkBoxMap.get(devBrand).setValue(true);

                List<String> modelList = (List<String>) device.get(supportDevModelsK);

                for (String model : modelList) {
                    allDevicesChkBoxMap.get(devBrand).get(model).setValue(true);
                }
            }
        } else if (supportedDevType.equals(supportAnyDevTypeK)) {
            supportAnyDevOpGrp.select(supportedDevType);

            VerticalLayout anyDeviceChildVLayout = (VerticalLayout) anyDeviceVLayout.getComponent(0);

            Map<String, Object> deviceGrp = deviceList.get(0);
            List<String> modelList = (List<String>) deviceGrp.get(supportDevModelsK);

            for (Iterator i = anyDeviceChildVLayout.getComponentIterator(); i.hasNext(); ) {
                CheckBox model = (CheckBox) i.next();
                if (modelList.contains(model.getCaption())) {
                    model.setValue(true);
                } else {
                    model.setValue(false);
                }
            }
        }
    }

    public void validate() {
        try {
            validationErrorLbl.setValue(null);
        } catch (Validator.InvalidValueException e) {
            validationErrorLbl.setValue(e.getMessage());
            logger.error("Validation failed [{}]", e.getMessage());
            throw new Validator.InvalidValueException(e.getMessage());
        } catch (Exception e) {
            logger.error("Unknown exception occurred while validating fields", e);
        }
    }
}