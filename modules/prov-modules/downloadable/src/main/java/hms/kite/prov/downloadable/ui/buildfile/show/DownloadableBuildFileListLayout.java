/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.downloadable.ui.buildfile.show;

import com.vaadin.ui.*;
import com.vaadin.ui.themes.BaseTheme;
import hms.kite.prov.downloadable.DownloadablePortlet;
import hms.kite.provisioning.commons.ui.component.popup.ConfirmationBox;
import hms.kite.provisioning.commons.ui.component.popup.HelpDetails;
import hms.kite.provisioning.commons.ui.event.PortletEventSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.*;

import static hms.kite.prov.downloadable.DownloadableServiceRegistry.buildFileRepositoryService;
import static hms.kite.prov.downloadable.DownloadableServiceRegistry.isHelpButtonLinkEnable;
import static hms.kite.prov.downloadable.util.DownloadableSlaRoles.*;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 * <p/>
 * This will display all the build files in a table
 */
public class DownloadableBuildFileListLayout extends VerticalLayout {

    private static final Logger logger = LoggerFactory.getLogger(DownloadableBuildFileListLayout.class);

    private static final String HELP_IMAGE = "help-build-config.jpg";
    public static final String HELP_IMG_WIDTH = "840px";
    public static final String HELP_IMG_HEIGHT = "740px";
    public static final String HELP_IMG_STYLE_NAME = "link";
    public static final String HELP_BUTTON_NAME = "Help";

    private PopupView popupView;

    private static enum Action {
        VIEW("view"), EDIT("edit"), APPROVE("approve"), SUSPEND("suspend"), RESTORE("restore"), REJECT("reject"), DELETE("delete"), DELETE_INITIAL("delete");

        private String name;

        private Action(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    private DownloadablePortlet application;
    private Map<String, Object> eventData;
    private Label validationErrorLbl;
    private String corporateUserId;
    private String appId;

    private Table buildFileTbl;
    private List<Map<String, Object>> buildFilesDataList;
    private TextField buildNameTxtFld;
    private Select statusSelect;

    private void init() {
        logger.debug("Initializing the Downloadable Build Files table");

        appId = "";

        buildFilesDataList = application.getBuildFilesList(appId);
        application.removeFromSession(selectedPlatformVersions);
        if(isHelpButtonLinkEnable()) {
            createHelperLink();
        }
        createBuildFileTable();
    }

    private void createBuildFileTable() {
        HorizontalLayout searchFieldsHLayout = createSearchBar();

        addComponent(searchFieldsHLayout);

        VerticalLayout tableVLayout = createTable();

        addComponent(tableVLayout);

        search();
    }

    private void createHelperLink() {
        Button helpButtonLink = createHelpButtonLink();
        addComponent(helpButtonLink);
        setComponentAlignment(helpButtonLink, Alignment.TOP_RIGHT);
    }

    private Button createHelpButtonLink() {
        final HelpDetails helpDetails = new HelpDetails(application, HELP_IMAGE);
        final Embedded image = helpDetails.getHelpImage();
        Button helpButtonLink = new Button(HELP_BUTTON_NAME);
        helpButtonLink.setStyleName(HELP_IMG_STYLE_NAME);
        helpButtonLink.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent clickEvent) {
                getWindow().addWindow(helpDetails.generateHelpImageSubWindow(image, HELP_IMG_WIDTH, HELP_IMG_HEIGHT));
            }
        });
        return helpButtonLink;
    }

    private HorizontalLayout createSearchBar() {
        HorizontalLayout searchFieldsHLayout = new HorizontalLayout();

        createBuildNameTxtFld();
        createStatusSelect();

        Button searchBtn = createSearchButton();
        Button uploadBtn = createUploadButton();
        Button approveAllBtn = createApproveAllButton();

        Label buildNameLbl = new Label(application.getMsg("downloadable.buildfile.list.table.buildname.caption"));
        searchFieldsHLayout.addComponent(buildNameLbl);
        searchFieldsHLayout.addComponent(buildNameTxtFld);
        Label statusLbl = new Label(application.getMsg("downloadable.buildfile.list.table.status.caption"));
        searchFieldsHLayout.addComponent(statusLbl);
        searchFieldsHLayout.addComponent(statusSelect);
        searchFieldsHLayout.addComponent(searchBtn);

        if (application.hasAnyRole(UPLOAD_BUILD)) {
            searchFieldsHLayout.addComponent(uploadBtn);
            searchFieldsHLayout.setComponentAlignment(uploadBtn, Alignment.MIDDLE_CENTER);
        } else if (application.hasAnyRole(APPROVE_BUILD)) {
            searchFieldsHLayout.addComponent(approveAllBtn);
            searchFieldsHLayout.setComponentAlignment(approveAllBtn, Alignment.MIDDLE_CENTER);
        }

        searchFieldsHLayout.setSpacing(true);
        searchFieldsHLayout.setMargin(true);
        searchFieldsHLayout.setComponentAlignment(buildNameLbl, Alignment.MIDDLE_CENTER);
        searchFieldsHLayout.setComponentAlignment(buildNameTxtFld, Alignment.MIDDLE_CENTER);
        searchFieldsHLayout.setComponentAlignment(statusLbl, Alignment.MIDDLE_CENTER);
        searchFieldsHLayout.setComponentAlignment(statusSelect, Alignment.MIDDLE_CENTER);
        searchFieldsHLayout.setComponentAlignment(searchBtn, Alignment.MIDDLE_CENTER);

        return searchFieldsHLayout;
    }

    private void createBuildNameTxtFld() {
        buildNameTxtFld = new TextField();
        buildNameTxtFld.setWidth("120px");
    }

    private void createStatusSelect() {
        statusSelect = new Select();
        statusSelect.setWidth("120px");
        String itemId = "Please Select";
        statusSelect.addItem(itemId);
        statusSelect.addItem(initialK);
        statusSelect.addItem(pendingApproveK);
        statusSelect.addItem(approvedK);
        statusSelect.addItem(suspendedK);
        statusSelect.addItem(rejectK);
        statusSelect.setNullSelectionItemId(itemId);
    }

    private Button createSearchButton() {
        Button searchBtn = new Button(application.getMsg("downloadable.buildfile.list.search.button.caption"));
        searchBtn.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                search();
            }
        });
        return searchBtn;
    }

    private void search() {
        Map<String, Object> query = new HashMap<String, Object>();
        if (!appId.isEmpty()) {
            query.put(appIdK, appId);
        }
        query.put(buildNameK, buildNameTxtFld.getValue());
        Object status = statusSelect.getValue();
        if (status != null) {
            query.put(statusK, status);
        }
        reloadTable(buildFileRepositoryService().search(query));
    }

    private Button createUploadButton() {
        final Button uploadBtn = new Button(application.getMsg("downloadable.buildfile.list.upload.button.caption"));
        uploadBtn.setStyleName(BaseTheme.BUTTON_LINK);
        uploadBtn.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                if(eventData.get(appStatusK).toString().equals(initialK)) {
                    List<String> selectedVersions = new ArrayList<String>();
                    for (Map<String, Object> buildFile : buildFileRepositoryService().findByAppId(appId)) {
                        logger.trace("Current build file information [{}]", buildFile);

                        for(String version: (List<String>) buildFile.get(platformVersionsK)){
                            selectedVersions.add(version);
                        }
                        application.addToSession(buildFileDataSessionK, buildFile);
                        eventData.put(buildAppFileIdK, buildFile.get(buildAppFileIdK));
                    }
                    application.addToSession(selectedPlatformVersions, selectedVersions);
                }
                fireEvent(provCreateBuildFileEventK);
            }
        });

        return uploadBtn;
    }

    private Button createApproveAllButton() {
        Button approveAllBtn = new Button(application.getMsg("downloadable.buildfile.list.approve.all.button.caption"));
        approveAllBtn.setStyleName(BaseTheme.BUTTON_LINK);
        approveAllBtn.addListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                ConfirmationBox confirmationBox = new ConfirmationBox(
                        application.getMsg("downloadable.buildfile.confirmationbox.title"),
                        application.getMsg("downloadable.buildfile.confirmationbox.yes.caption"),
                        application.getMsg("downloadable.buildfile.confirmationbox.no.caption"),
                        String.format(application.getMsg("downloadable.buildfile.confirmationbox.action.message"), "approve all"),
                        new ConfirmationBox.ConfirmationListener() {

                            @Override
                            public void onYes() {
                                approveAllBuildFiles();
                            }

                            @Override
                            public void onNo() {
                            }
                        },
                        application.getMainWindow());

                confirmationBox.viewConfirmation();
            }
        });

        return approveAllBtn;
    }

    private VerticalLayout createTable() {
        VerticalLayout tableVLayout = new VerticalLayout();

        buildFileTbl = new Table();
        buildFileTbl.setHeight("150px");

        buildFileTbl.addContainerProperty(application.getMsg("downloadable.buildfile.list.table.buildname.caption"), Label.class, null);
        buildFileTbl.setColumnWidth(application.getMsg("downloadable.buildfile.list.table.buildname.caption"), 100);
        buildFileTbl.addContainerProperty(application.getMsg("downloadable.buildfile.list.table.platform.caption"), Label.class, null);
        buildFileTbl.setColumnWidth(application.getMsg("downloadable.buildfile.list.table.platform.caption"), 100);
        buildFileTbl.addContainerProperty(application.getMsg("downloadable.buildfile.list.table.platformversions.caption"), Label.class, null);
        buildFileTbl.setColumnWidth(application.getMsg("downloadable.buildfile.list.table.platformversions.caption"), 130);
        buildFileTbl.addContainerProperty(application.getMsg("downloadable.buildfile.list.table.uploadeddate.caption"), Label.class, null);
        buildFileTbl.setColumnWidth(application.getMsg("downloadable.buildfile.list.table.uploadeddate.caption"), 100);
        buildFileTbl.addContainerProperty(application.getMsg("downloadable.buildfile.list.table.status.caption"), Label.class, null);
        buildFileTbl.setColumnWidth(application.getMsg("downloadable.buildfile.list.table.status.caption"), 100);
        buildFileTbl.addContainerProperty(application.getMsg("downloadable.buildfile.list.table.action.caption"), HorizontalLayout.class, null);
        buildFileTbl.setSizeFull();

        tableVLayout.setHeight("200px");
        tableVLayout.addComponent(buildFileTbl);
        tableVLayout.setMargin(true);

        return tableVLayout;
    }

    private void fireEvent(String eventType) {
        Object currentEventOrigin = eventData.get(currentEventOriginK);
        eventData.put(eventOriginK, currentEventOrigin);
        eventData.remove(currentEventOriginK);
        PortletEventSender.send(eventData, eventType, application);
        application.closePortlet();
    }

    private HorizontalLayout createActionButtons(Map<String, Object> buildFile) {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.removeAllComponents();

        logger.trace("Current User Type [{}]", eventData.get(currentUserTypeK));

        Object buildFileStatus = buildFile.get(statusK);
        logger.trace("Current Build File status [{}]", buildFileStatus);

        horizontalLayout.addComponent(createActionButtonLink(Action.VIEW, application.getMsg("downloadable.buildfile.list.table.action.view.button"), buildFile));

        if (pendingApproveK.equals(buildFileStatus)) {
            addUserAction(APPROVE_BUILD, horizontalLayout, Action.APPROVE, "downloadable.buildfile.list.table.action.approve.button", buildFile);
            addUserAction(REJECT_BUILD, horizontalLayout, Action.REJECT, "downloadable.buildfile.list.table.action.reject.button", buildFile);
            addUserAction(DELETE_BUILD, horizontalLayout, Action.DELETE, "downloadable.buildfile.list.table.action.delete.button", buildFile);
        } else if (approvedK.equals(buildFileStatus)) {
            addUserAction(EDIT_BUILD, horizontalLayout, Action.EDIT, "downloadable.buildfile.list.table.action.edit.button", buildFile);
            addUserAction(SUSPEND_BUILD, horizontalLayout, Action.SUSPEND, "downloadable.buildfile.list.table.action.suspend.button", buildFile);
            addUserAction(DELETE_BUILD, horizontalLayout, Action.DELETE, "downloadable.buildfile.list.table.action.delete.button", buildFile);
        } else if (suspendedK.equals(buildFileStatus)) {
            addUserAction(RESTORE_BUILD, horizontalLayout, Action.RESTORE, "downloadable.buildfile.list.table.action.restore.button", buildFile);
            addUserAction(DELETE_BUILD, horizontalLayout, Action.DELETE, "downloadable.buildfile.list.table.action.delete.button", buildFile);
        } else if (rejectK.equals(buildFileStatus)) {
            addUserAction(EDIT_BUILD, horizontalLayout, Action.EDIT, "downloadable.buildfile.list.table.action.edit.button", buildFile);
            addUserAction(DELETE_BUILD, horizontalLayout, Action.DELETE, "downloadable.buildfile.list.table.action.delete.button", buildFile);
        } else if (initialK.equals(buildFileStatus) || draftK.equals(buildFileStatus)) {
            addUserAction(DELETE_BUILD, horizontalLayout, Action.DELETE, "downloadable.buildfile.list.table.action.delete.button", buildFile);
        } else {
            logger.error("Unexpected status [{}]", buildFileStatus);
        }
        return horizontalLayout;
    }

    private void addUserAction(String role, HorizontalLayout horizontalLayout, Action action, String key, Map<String, Object> buildFile) {
        if (application.hasAnyRole(role)) {
            horizontalLayout.addComponent(createActionButtonLink(action, application.getMsg(key), buildFile));
        }
    }

    private Button createActionButtonLink(Action action, String caption, Map<String, Object> buildFile) {
        final Button button = new Button(caption);
        button.setStyleName(BaseTheme.BUTTON_LINK);
        button.setImmediate(true);
        button.setData(createAction(action, buildFile));
        button.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    Map<String, Object> actionData = (Map<String, Object>) button.getData();
                    processAction(actionData);
                } catch (Exception e) {
                    logger.error("Unexpected error occurred ", e);
                }
            }
        });
        return button;
    }

    private Map<String, Object> createAction(Action action, Map<String, Object> buildFile) {
        Map<String, Object> actionData = new HashMap<String, Object>();
        actionData.put(actionK, action);
        actionData.put(buildFileK, buildFile);
        return actionData;
    }

    private void processAction(Map<String, Object> actionData) {
        Action action = (Action) actionData.get(actionK);
        Map<String, Object> buildFile = (Map<String, Object>) actionData.get(buildFileK);

        logger.debug("Current action [{}]", action);

        switch (action) {
            case VIEW:
                viewBuildFile(buildFile);
                break;
            case EDIT:
                editBuildFile(buildFile);
                break;
            case APPROVE:
                checkConfirmation(action, buildFile);
                break;
            case SUSPEND:
                checkConfirmation(action, buildFile);
                break;
            case RESTORE:
                checkConfirmation(action, buildFile);
                break;
            case REJECT:
                checkConfirmation(action, buildFile);
                break;
            case DELETE:
                if (buildFilesDataList.size() > 1) {
                    checkConfirmation(action, buildFile);
                } else {
                    validationErrorLbl.setValue(application.getMsg("downloadable.buildfile.list.delete.error"));
                }
                break;
            default:
                logger.error("Unknown Action [{}], Can't handle ", action);
        }
    }

    private void checkConfirmation(final Action action, final Map<String, Object> buildFile) {
        ConfirmationBox confirmationBox = new ConfirmationBox(
                application.getMsg("downloadable.buildfile.confirmationbox.title"),
                application.getMsg("downloadable.buildfile.confirmationbox.yes.caption"),
                application.getMsg("downloadable.buildfile.confirmationbox.no.caption"),
                String.format(application.getMsg("downloadable.buildfile.confirmationbox.action.message"), action.getName()),
                new ConfirmationBox.ConfirmationListener() {
                    @Override
                    public void onYes() {
                        switch (action) {
                            case APPROVE:
                                approveBuildFile(buildFile);
                                break;
                            case SUSPEND:
                                suspendBuildFile(buildFile);
                                break;
                            case RESTORE:
                                restoreBuildFile(buildFile);
                                break;
                            case REJECT:
                                rejectBuildFile(buildFile);
                                break;
                            case DELETE:
                                deleteBuildFile(buildFile);
                                break;
                            default:
                                logger.error("Unknown Action [{}], Can't handle ", action);
                        }
                    }

                    @Override
                    public void onNo() {
                    }
                },
                application.getMainWindow());

        confirmationBox.viewConfirmation();
    }

    private void viewBuildFile(Map<String, Object> buildFile) {
        application.addToSession(buildFileDataSessionK, buildFile);
        eventData.put(buildAppFileIdK, buildFile.get(buildAppFileIdK));
        fireEvent(provViewBuildFileEventK);
    }

    private void editBuildFile(Map<String, Object> buildFile) {
        application.addToSession(buildFileDataSessionK, buildFile);
        eventData.put(buildAppFileIdK, buildFile.get(buildAppFileIdK));
        fireEvent(provReConfigureBuildFileEventK);
    }

    private void approveAllBuildFiles() {
        Map<String, Object> query = new HashMap<String, Object>();
        query.put(appIdK, appId);
        query.put(buildNameK, "");
        query.put(statusK, pendingApproveK);
        List<Map<String, Object>> pendingApproveBuildList = buildFileRepositoryService().search(query);

        for (Map<String, Object> buildFile : pendingApproveBuildList) {
            buildFile.put(statusK, approvedK);
            buildFileRepositoryService().updateBuildFile(buildFile);
        }
        reloadTable(buildFileRepositoryService().findByAppId(appId));
    }

    private void updateBuildFileStatus(Map<String, Object> buildFile, String status) {
        buildFile.put(statusK, status);
        buildFileRepositoryService().updateBuildFile(buildFile);
        reloadTable(buildFilesDataList);
    }

    private void approveBuildFile(Map<String, Object> buildFile) {
        updateBuildFileStatus(buildFile, approvedK);
    }

    private void suspendBuildFile(Map<String, Object> buildFile) {
        updateBuildFileStatus(buildFile, suspendedK);
    }

    private void restoreBuildFile(Map<String, Object> buildFile) {
        updateBuildFileStatus(buildFile, approvedK);
    }

    private void rejectBuildFile(Map<String, Object> buildFile) {
        updateBuildFileStatus(buildFile, rejectK);
    }

    private void deleteBuildFile(Map<String, Object> buildFile) {
        buildFileRepositoryService().deleteBuildFile(buildFile);
        if (buildFilesDataList.remove(buildFile)) {
            reloadTable(buildFilesDataList);
        }
    }

    public DownloadableBuildFileListLayout(DownloadablePortlet application,
                                           Map<String, Object> eventData,
                                           String corporateUserId,
                                           Label validationErrorLbl) {
        this.application = application;
        this.corporateUserId = corporateUserId;
        this.eventData = eventData;
        this.validationErrorLbl = validationErrorLbl;

        init();
    }

    public void reloadTable(List<Map<String, Object>> buildFilesDataList) {
        this.buildFilesDataList = buildFilesDataList;

        SimpleDateFormat sdf = new SimpleDateFormat("yy/MM/dd - HH:mm", application.getLocale());

        logger.trace("Loading [{}] build files to the table", buildFilesDataList.size());

        buildFileTbl.removeAllItems();
        int rowNo = 0;
        for (Map<String, Object> buildFile : buildFilesDataList) {
            logger.trace("Current build file information [{}]", buildFile);

            buildFileTbl.addItem(new Object[]{buildFile.get(buildNameK),
                    buildFile.get(platformNameK),
                    buildFile.get(platformVersionsK),
                    sdf.format(buildFile.get(createdDateK)),
                    buildFile.get(statusK),
                    createActionButtons(buildFile)}, rowNo);
            rowNo++;
        }
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }
}
