/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.downloadable.ui.buildfile.create;

import com.vaadin.data.Validator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.ui.*;
import hms.kite.prov.downloadable.DownloadablePortlet;
import hms.kite.prov.downloadable.ui.buildfile.common.UploadHandler;
import hms.kite.prov.downloadable.ui.buildfile.common.impl.DefaultUploadHandler;
import hms.kite.provisioning.commons.ui.DataComponent;
import hms.kite.provisioning.commons.ui.component.popup.HelpDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import static hms.kite.prov.downloadable.DownloadableServiceRegistry.getCmsUrl;
import static hms.kite.prov.downloadable.DownloadableServiceRegistry.isHelpButtonLinkEnable;
import static hms.kite.prov.downloadable.DownloadableServiceRegistry.validationRegistry;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 * <p/>
 * Description : This Class contains Downloadable Application Build file build information
 * <p/>
 */
public class DownloadableBuildFileBuildInfoLayout extends VerticalLayout implements DataComponent {

    private static final Logger logger = LoggerFactory.getLogger(DownloadableBuildFileBuildInfoLayout.class);

    private static final String WIDTH = "170px";
    private static final String HELP_IMAGE = "help-upload-build.jpg";
    public static final String HELP_IMG_WIDTH = "730px";
    public static final String HELP_IMG_HEIGHT = "270px";
    public static final String HELP_IMG_STYLE_NAME = "link";
    public static final String HELP_BUTTON_NAME = "Help";
    private DownloadablePortlet application;

    private DownloadableBuildFilePlatformLayout buildFilePlatformLayout;
    private Label validationErrorLbl;
    private TextField buildNameTxtFld;
    private TextField buildVersionTxtFld;
    private TextArea buildDescriptionTxtFld;
    private UploadHandler appFile;
    private UploadHandler jadFile;
    private Label statusLbl;
    private FormLayout formLayout;

    private PopupView popupView;

    private void init() {
        logger.debug("Initializing the Downloadable Application Build File Build information form");

        formLayout = new FormLayout();
        formLayout.addStyleName("downloadable-ncs-form");
        formLayout.addStyleName("downloadable-padding-build-info");
        if(isHelpButtonLinkEnable()) {
            createHelperLink();
        }
        createBuildNameTxtFld();
        createBuildVersionTxtFld();
        createBuildDescriptionTxtFld();
        createAppFileUploadHandler();
        createJadFileUploadHandler();
        createStatusLbl();

        addComponent(formLayout);
    }

    private void createHelperLink() {
        Button helpButtonLink = createHelpButtonLink();
        addComponent(helpButtonLink);
        setComponentAlignment(helpButtonLink, Alignment.TOP_RIGHT);
    }

    private Button createHelpButtonLink() {
        final HelpDetails helpDetails = new HelpDetails(application, HELP_IMAGE);
        final Embedded image = helpDetails.getHelpImage();
        Button helpButtonLink = new Button(HELP_BUTTON_NAME);
        helpButtonLink.setStyleName(HELP_IMG_STYLE_NAME);
        helpButtonLink.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent clickEvent) {
                getWindow().addWindow(helpDetails.generateHelpImageSubWindow(image, HELP_IMG_WIDTH, HELP_IMG_HEIGHT));
            }
        });
        return helpButtonLink;
    }

    private void createBuildNameTxtFld() {
        buildNameTxtFld = new TextField();
        buildNameTxtFld.setCaption(application.getMsg("downloadable.buildfile.build.name.caption"));
        buildNameTxtFld.setDescription(application.getMsg("downloadable.buildfile.build.name.tooltip"));
        buildNameTxtFld.setRequired(true);
        buildNameTxtFld.setRequiredError(application.getMsg("downloadable.buildfile.build.name.required.error"));
        buildNameTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("dlAppBuildFileNameRegex"),
                application.getMsg("downloadable.buildfile.build.name.length.error")));
        buildNameTxtFld.setWidth(WIDTH);
        formLayout.addComponent(buildNameTxtFld);
    }

    private void createBuildVersionTxtFld() {
        buildVersionTxtFld = new TextField();
        buildVersionTxtFld.setCaption(application.getMsg("downloadable.buildfile.build.version.caption"));
        buildVersionTxtFld.setDescription(application.getMsg("downloadable.buildfile.build.version.tooltip"));
        buildVersionTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("dlAppBuildFileVersionRegex"),
                application.getMsg("downloadable.buildfile.build.version.length.error")));
        buildVersionTxtFld.setWidth(WIDTH);
        formLayout.addComponent(buildVersionTxtFld);
    }

    private void createBuildDescriptionTxtFld() {
        buildDescriptionTxtFld = new TextArea();
        buildDescriptionTxtFld.setCaption(application.getMsg("downloadable.buildfile.build.description.caption"));
        buildDescriptionTxtFld.setDescription(application.getMsg("downloadable.buildfile.build.description.tooltip"));
        buildDescriptionTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("dlAppBuildFileDescRegex"),
                application.getMsg("downloadable.buildfile.build.description.length.error")));
        buildDescriptionTxtFld.setWidth(WIDTH);
        buildDescriptionTxtFld.setRows(6);
        formLayout.addComponent(buildDescriptionTxtFld);
    }

    private DefaultUploadHandler getUploadHandler(String caption, String tooltip) {
        return new DefaultUploadHandler(
                application,
                application.getMsg(caption),
                application.getMsg(tooltip),
                buildFilePlatformLayout
        );
    }

    private void createAppFileUploadHandler() {
        appFile = getUploadHandler("downloadable.buildfile.build.appfile.caption",
                "downloadable.buildfile.build.appfile.tooltip");
        formLayout.addComponent(appFile.getUploadHLayout());
    }

    private void createJadFileUploadHandler() {
        jadFile = getUploadHandler("downloadable.buildfile.build.jadfile.caption",
                "downloadable.buildfile.build.jadfile.tooltip");
        VerticalLayout uploadHLayout = jadFile.getUploadHLayout();
        uploadHLayout.setVisible(false);
        formLayout.addComponent(uploadHLayout);
    }

    private void createStatusLbl() {
        statusLbl = new Label();
        formLayout.addComponent(statusLbl);
    }

    private boolean isJadBuildAvailable() {
        return !jadFile.getFilename().isEmpty();
    }

    private boolean isJadFileUploaded() {
        return jadFile.isFileUploaded();
    }

    private boolean isAppFileUploaded() {
        return appFile.isFileUploaded();
    }

    private String createDownloadUrl(String fileId, String appId) {
        StringBuilder url = new StringBuilder();

        url.append(getCmsUrl())
                .append("?id=")
                .append(fileId)
                .append("&appid=")
                .append(appId);

        return url.toString();
    }

    private void setUploadedFileInfo(String fileIdKey, String fileNameKey, UploadHandler uploadHandler,
                                     Map<String, Object> buildFile) {
        if (buildFile.containsKey(fileIdKey)) {
            String fileId = (String) buildFile.get(fileIdKey);
            String filename = (String) buildFile.get(fileNameKey);

            URL appUrl = null;

            try {
                appUrl = new URL(createDownloadUrl(fileId, (String) buildFile.get(appIdK)));
            } catch (MalformedURLException e) {
                logger.error("Error in Download URL ", e);
            } catch (Exception e) {
                logger.error("Error occurred while setting url ", e);
            }

            uploadHandler.setUploadFile(appUrl, fileId, filename);
        }
    }

    public DownloadableBuildFileBuildInfoLayout(DownloadablePortlet application,
                                                DownloadableBuildFilePlatformLayout buildFilePlatformLayout,
                                                Label validationErrorLbl) {
        this.application = application;
        this.buildFilePlatformLayout = buildFilePlatformLayout;
        this.validationErrorLbl = validationErrorLbl;

        init();
    }

    public void getData(Map<String, Object> buildFile) {
        buildFile.put(buildNameK, buildNameTxtFld.getValue());
        buildFile.put(buildVersionK, buildVersionTxtFld.getValue());
        buildFile.put(buildDescK, buildDescriptionTxtFld.getValue());
        buildFile.put(buildAppFileNameK, appFile.getFilename());
        buildFile.put(buildAppFileIdK, appFile.getFileId());
        if (isJadBuildAvailable()) {
            buildFile.put(buildJadFileNameK, jadFile.getFilename());
            buildFile.put(buildJadFileIdK, jadFile.getFileId());
        }
    }

    public void setData(Map<String, Object> buildFile) {
        buildNameTxtFld.setValue(buildFile.get(buildNameK));
        buildVersionTxtFld.setValue(buildFile.get(buildVersionK));
        buildDescriptionTxtFld.setValue(buildFile.get(buildDescK));
        setUploadedFileInfo(buildAppFileIdK, buildAppFileNameK, appFile, buildFile);
        setUploadedFileInfo(buildJadFileIdK, buildJadFileNameK, jadFile, buildFile);
    }

    public void validate() {
        try {
            buildNameTxtFld.validate();
            buildVersionTxtFld.validate();
            buildDescriptionTxtFld.validate();
            appFile.validate();
            if (isJadBuildAvailable()) {
                jadFile.validate();
            }
            validationErrorLbl.setValue(null);
        } catch (Validator.InvalidValueException e) {
            validationErrorLbl.setValue(e.getMessage());
            logger.warn("Validation failed [{}]", e.getMessage());
            throw new Validator.InvalidValueException(e.getMessage());
        } catch (Exception e) {
            logger.error("Unknown exception occurred while validating fields", e);
        }
    }

    public void setJadFileEnable(boolean value) {
        jadFile.getUploadHLayout().setVisible(value);
    }

    public boolean isFilesUploaded() {
        return isAppFileUploaded() || isJadFileUploaded();
    }

    public void resetUploads() {
        if (isAppFileUploaded()) {
            appFile.deleteUpload();
        }
        if (isJadFileUploaded()) {
            jadFile.deleteUpload();
        }
    }

    public void cancelUploads() {
        if (isAppFileUploaded()) {
            appFile.deleteUploadByForce();
        }
        if (isJadFileUploaded()) {
            jadFile.deleteUploadByForce();
        }
    }

    public void setUploadDeleteBtn() {
        appFile.setDeleteBtn(false);
        jadFile.setDeleteBtn(false);
    }
}
