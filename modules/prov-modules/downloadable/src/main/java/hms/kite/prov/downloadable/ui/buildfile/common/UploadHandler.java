/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.downloadable.ui.buildfile.common;

import com.vaadin.ui.VerticalLayout;

import java.net.URL;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public interface UploadHandler {

    void validate();

    void deleteUpload();

    void deleteUploadByForce();

    void setUploadFile(URL url, String fileId, String filename);

    void setDeleteBtn(boolean check);

    boolean isFileUploaded();

    String getFilename();

    VerticalLayout getUploadHLayout();

    String getFileId();
}