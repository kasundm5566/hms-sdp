/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.downloadable.ui.buildfile.create;

import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.ui.*;
import hms.kite.datarepo.RepositoryServiceRegistry;
import hms.kite.prov.downloadable.DownloadablePortlet;
import hms.kite.prov.downloadable.ui.buildfile.common.DevicesPlatformVersions;
import hms.kite.provisioning.commons.ui.DataComponent;
import hms.kite.provisioning.commons.ui.component.popup.ConfirmationBox;
import hms.kite.provisioning.commons.ui.component.popup.HelpDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static hms.kite.prov.downloadable.DownloadableServiceRegistry.isHelpButtonLinkEnable;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 * <p/>
 */
public class DownloadableBuildFilePlatformLayout extends VerticalLayout implements DataComponent {

    private static final Logger logger = LoggerFactory.getLogger(DownloadableBuildFilePlatformLayout.class);

    private static final int MAX_NO_OF_VERSIONS_PER_ROW = 2;
    private static final String WIDTH = "170px";
    public static final String HELP_IMG_WIDTH = "735px";
    public static final String HELP_IMG_HEIGHT = "275px";
    private static final String HELP_IMAGE = "help-platform.jpg";
    public static final String HELP_IMG_STYLE_NAME = "link";
    public static final String HELP_BUTTON_NAME = "Help";

    private DownloadablePortlet application;

    private FormLayout formLayout;
    private VerticalLayout versionsVLayout;
    private String prevSelectedPlatform;
    private Select platformSelect;
    private Map<String, DevicesPlatformVersions> devicesPlatformVersionsMap;
    private VerticalLayout anyDeviceVLayout;
    private DownloadableBuildFileMainLayout downloadableBuildFileMainLayout;
    private Map<String, VerticalLayout> anyDeviceVLayoutMap;
    private List<Map<String, Object>> basePlatformInfoList;
    private Map<String, Map<String, CheckBox>> platformsMap;
    private Map<String, VerticalLayout> platformVersionsLayoutMap;
    private Map<String, List<String>> platformSupportedFileTypesMap;
    private Label validationErrorLbl;
    private Property.ValueChangeListener platformChangeListener;
    private PopupView popupView;

    private void init() {
        logger.debug("Initializing the Downloadable Application Build File Platform information form");

        basePlatformInfoList = RepositoryServiceRegistry.deviceAndPlatformRepositoryService().findAllPlatforms();

        versionsVLayout = new VerticalLayout();
        versionsVLayout.setCaption(application.getMsg("downloadable.buildfile.platform.version.caption"));
        versionsVLayout.setDescription(application.getMsg("downloadable.buildfile.platform.version.tooltip"));

        createBuildFilePlatformForm();
    }

    private void createBuildFilePlatformForm() {
        logger.debug("Create Platform Configuration Form");

        createPlatformSelect();

        formLayout = new FormLayout();
        formLayout.addComponent(platformSelect);
        formLayout.addStyleName("downloadable-ncs-form");
        formLayout.addStyleName("downloadable-padding-platform");
        if(isHelpButtonLinkEnable()) {
            createHelperLink();
        }
        addComponent(formLayout);
    }

    private void createPlatformSelect() {
        logger.debug("Loading all platform versions");

        platformsMap = new LinkedHashMap<String, Map<String, CheckBox>>();
        anyDeviceVLayoutMap = new LinkedHashMap<String, VerticalLayout>();
        platformVersionsLayoutMap = new LinkedHashMap<String, VerticalLayout>();
        platformSupportedFileTypesMap = new LinkedHashMap<String, List<String>>();

        platformSelect = new Select();
        platformSelect.setCaption(application.getMsg("downloadable.buildfile.platform.caption"));
        platformSelect.setDescription(application.getMsg("downloadable.buildfile.platform.tooltip"));
        platformSelect.setRequired(true);
        platformSelect.setRequiredError(application.getMsg("downloadable.buildfile.platform.required.error"));
        platformSelect.setWidth(WIDTH);
        platformSelect.setNullSelectionAllowed(false);
        platformSelect.addItem("-------Please Select-------");
        platformSelect.setNullSelectionItemId("-------Please Select-------");
        platformSelect.setImmediate(true);

        for (Map<String, Object> platform : basePlatformInfoList) {
            String id = platform.get(platformIdK).toString();
            String name = (String) platform.get(platformNameK);
            List<String> versions = (List<String>) platform.get(platformVersionsK);
            List<String> fileTypes = (List<String>) platform.get(platformSupportedFileTypesK);

            platformSelect.addItem(id);
            platformSelect.setItemCaption(id, name);
            platformSupportedFileTypesMap.put(id, fileTypes);
            loadPlatform(id, name, versions);
        }

        platformSelect.addListener(createPlatformChangeListener());
    }


    private void createHelperLink() {
        Button helpButtonLink = createHelpButtonLink();
        addComponent(helpButtonLink);
        setComponentAlignment(helpButtonLink, Alignment.TOP_RIGHT);
    }

    private Button createHelpButtonLink() {
        final HelpDetails helpDetails = new HelpDetails(application, HELP_IMAGE);
        final Embedded image = helpDetails.getHelpImage();
        Button helpButtonLink = new Button(HELP_BUTTON_NAME);
        helpButtonLink.setStyleName(HELP_IMG_STYLE_NAME);
        helpButtonLink.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent clickEvent) {
                getWindow().addWindow(helpDetails.generateHelpImageSubWindow(image, HELP_IMG_WIDTH, HELP_IMG_HEIGHT));
            }
        });
        return helpButtonLink;
    }

    private Property.ValueChangeListener createPlatformChangeListener() {
        platformChangeListener = new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                if (downloadableBuildFileMainLayout.isFileUploaded()) {
                    checkConfirmation();
                } else {
                    changingPlatform();
                }
            }
        };
        return platformChangeListener;
    }

    private void changingPlatform() {
        String platformSelectValue = platformSelect.getValue().toString();

        logger.debug("Changing Platform Versions to - " + platformSelectValue);

        if (prevSelectedPlatform != null) {
            resetDevices(prevSelectedPlatform);
        }

        if (!downloadableBuildFileMainLayout.isConfigPanelVisible()) {
            downloadableBuildFileMainLayout.setConfigPanelVisible(true);
        }

        downloadableBuildFileMainLayout.setJadFileVisible(platformSupportedFileTypesMap.get(platformSelectValue));

        loadPlatformVersionsLayout(platformSelectValue);
        prevSelectedPlatform = platformSelectValue;

        anyDeviceVLayout.removeAllComponents();
        anyDeviceVLayout.addComponent(anyDeviceVLayoutMap.get(platformSelectValue));
    }

    private void loadPlatform(final String platformId, final String platformName, List<String> platformVersions) {
        logger.debug("Create Platform Versions - " + platformId);

        Map<String, CheckBox> versionsMap = new LinkedHashMap<String, CheckBox>();

        int limitNo = 0;

        VerticalLayout currentPlatformVLayout = new VerticalLayout();
        VerticalLayout anyDevVLayout = new VerticalLayout();

        HorizontalLayout currentVersions = new HorizontalLayout();
        setHorizontalVersionsFormatting(currentVersions);

        for (String version : platformVersions) {
            if (limitNo >= MAX_NO_OF_VERSIONS_PER_ROW) {
                currentPlatformVLayout.addComponent(currentVersions);
                currentVersions = new HorizontalLayout();
                setHorizontalVersionsFormatting(currentVersions);
                limitNo = 0;
            }

            final String pvKey = platformId + "-" + version;

            CheckBox versionChkBox = new CheckBox(version, false);
            versionChkBox.setImmediate(true);
            if(application.getFromSession(selectedPlatformVersions)!=null){
                List<String> selectedVersions = (List<String>) application.getFromSession(selectedPlatformVersions);
                if(selectedVersions.contains(version)){
                    versionChkBox.setEnabled(false);
                }
            }

            if (devicesPlatformVersionsMap.containsKey(pvKey)) {
                devicesPlatformVersionsMap.get(pvKey).setVersionChkBox(versionChkBox);
            } else {
                devicesPlatformVersionsMap.put(pvKey, new DevicesPlatformVersions(versionChkBox, platformName, version));
            }

            anyDevVLayout.addComponent(devicesPlatformVersionsMap.get(pvKey).getAnyDeviceChkBox());

            versionChkBox.addListener(new Property.ValueChangeListener() {
                @Override
                public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                    selectDevices(pvKey, platformId, (Boolean) valueChangeEvent.getProperty().getValue());
                }
            });

            versionsMap.put(version, versionChkBox);
            currentVersions.addComponent(versionChkBox);
            limitNo++;
        }

        currentVersions.setSpacing(true);
        currentPlatformVLayout.addComponent(currentVersions);

        platformsMap.put(platformId, versionsMap);
        anyDeviceVLayoutMap.put(platformId, anyDevVLayout);
        platformVersionsLayoutMap.put(platformId, currentPlatformVLayout);
    }

    private void setHorizontalVersionsFormatting(HorizontalLayout currentVersions) {
        currentVersions.setSpacing(true);
        currentVersions.setWidth(WIDTH);
    }

    private void checkConfirmation() {
        ConfirmationBox confirmationBox = new ConfirmationBox(
                application.getMsg("downloadable.buildfile.confirmationbox.title"),
                application.getMsg("downloadable.buildfile.confirmationbox.yes.caption"),
                application.getMsg("downloadable.buildfile.confirmationbox.no.caption"),
                application.getMsg("downloadable.buildfile.confirmationbox.platform.message"),
                new ConfirmationBox.ConfirmationListener() {
                    @Override
                    public void onYes() {
                        changingPlatform();
                        downloadableBuildFileMainLayout.resetUploads();
                    }

                    @Override
                    public void onNo() {
                        platformSelect.removeListener(platformChangeListener);
                        platformSelect.setValue(prevSelectedPlatform);
                        platformSelect.addListener(platformChangeListener);
                    }
                },
                application.getMainWindow());
        confirmationBox.viewConfirmation();
    }

    private void loadPlatformVersionsLayout(String platformSelectValue) {
        logger.debug("Loading Platform Versions - " + platformSelectValue);

        versionsVLayout.removeAllComponents();
        versionsVLayout.addComponent(platformVersionsLayoutMap.get(platformSelectValue));

        formLayout.addComponent(versionsVLayout);
    }

    private void selectDevices(String pvKey, String platformSelectValue, boolean value) {
        List<String> alreadyCheckedList = new LinkedList<String>();

        Map<String, CheckBox> versionMap = platformsMap.get(platformSelectValue);

        if (!value) {
            for (final Map.Entry version : versionMap.entrySet()) {
                CheckBox currentChkBox = (CheckBox) version.getValue();
                if ((Boolean) currentChkBox.getValue()) {
                    alreadyCheckedList.add(platformSelectValue + "-" + version.getKey());
                    currentChkBox.setValue(false);
                }
            }
        }

        if (devicesPlatformVersionsMap.containsKey(pvKey)) {
            DevicesPlatformVersions devicesPlatformVersions = devicesPlatformVersionsMap.get(pvKey);
            CheckBox anyDevChkBox = devicesPlatformVersions.getAnyDeviceChkBox();
            anyDevChkBox.setValue(value);

            List<CheckBox> deviceList = devicesPlatformVersions.getSpecDeviceList();

            for (CheckBox model : deviceList) {
                logger.trace("Changing - [{}]", model.getCaption());
                setParentChkBox(value, model);
                model.setValue(value);
            }
        }

        for (String version : alreadyCheckedList) {
            devicesPlatformVersionsMap.get(version).getVersionChkBox().setValue(true);
        }
    }

    private void setParentChkBox(boolean value, CheckBox model) {
        VerticalLayout brandVLayout = (VerticalLayout) model.getParent().getParent();

        CheckBox parent = (CheckBox) brandVLayout.getComponent(0);
        parent.setValue(value);
    }

    private void resetDevices(String platformSelectValue) {
        List<String> alreadyCheckedList = new LinkedList<String>();

        Map<String, CheckBox> versionMap = platformsMap.get(platformSelectValue);

        for (final Map.Entry version : versionMap.entrySet()) {
            CheckBox currentChkBox = (CheckBox) version.getValue();
            if ((Boolean) currentChkBox.getValue()) {
                alreadyCheckedList.add(platformSelectValue + "-" + version.getKey());
                currentChkBox.setValue(false);
            }
        }
    }

    private void validateVersions() {
        boolean flag = false;
        for (Map.Entry version : platformsMap.get(platformSelect.getValue()).entrySet()) {
            CheckBox tempChkBox = (CheckBox) version.getValue();
            if (tempChkBox.getValue().equals(true)) {
                flag = true;
            }
        }

        if (!flag) {
            throw new Validator.InvalidValueException(
                    application.getMsg("downloadable.buildfile.platform.version.required.error"));
        }
    }

    public DownloadableBuildFilePlatformLayout(DownloadablePortlet application,
                                               Map<String, DevicesPlatformVersions> devicesPlatformVersionsMap,
                                               VerticalLayout anyDeviceVLayout,
                                               DownloadableBuildFileMainLayout downloadableBuildFileMainLayout,
                                               Label validationErrorLbl) {
        this.application = application;
        this.devicesPlatformVersionsMap = devicesPlatformVersionsMap;
        this.anyDeviceVLayout = anyDeviceVLayout;
        this.downloadableBuildFileMainLayout = downloadableBuildFileMainLayout;
        this.validationErrorLbl = validationErrorLbl;

        init();
    }

    public void getData(Map<String, Object> buildFile) {
        logger.debug("Get Data from Platform Configuration Form");

        Object platformId = platformSelect.getValue();

        buildFile.put(platformIdK, platformId);
        buildFile.put(platformNameK, platformSelect.getItemCaption(platformId));

        List<String> versionList = new LinkedList<String>();

        Map<String, CheckBox> versionMap = platformsMap.get(platformId);

        for (final Map.Entry version : versionMap.entrySet()) {
            CheckBox currentChkBox = (CheckBox) version.getValue();
            if (currentChkBox.getValue().equals(true)) {
                versionList.add((String) version.getKey());
            }
        }

        buildFile.put(platformVersionsK, versionList);
    }

    public void setData(Map<String, Object> buildFile) {
        logger.debug("Set Data to Platform Configuration Form");

        platformSelect.setValue(buildFile.get(platformIdK));

        String platformSelectValue = (String) platformSelect.getValue();

        List<String> versionList = (List<String>) buildFile.get(platformVersionsK);

        Map<String, CheckBox> versionMap = platformsMap.get(platformSelectValue);

        for (final Map.Entry version : versionMap.entrySet()) {
            CheckBox currentChkBox = (CheckBox) version.getValue();
            if (versionList.contains(version.getKey())) {
                currentChkBox.setValue(true);
            }
        }

        loadPlatformVersionsLayout(platformSelectValue);

        /**
         * Don't remove or change following line
         *
         * If its removed or changed, it'll create an issue when
         *
         * 1. Edit buildfile
         * 2. Change platform and click on Yes
         * 3. Delete build file click on Yes
         * 4. Cancel editing the build file
         *
         * */
        platformSelect.setEnabled(false);
    }

    public void validate() {
        try {
            platformSelect.validate();
            validateVersions();
            validationErrorLbl.setValue(null);
        } catch (Validator.InvalidValueException e) {
            validationErrorLbl.setValue(e.getMessage());
            logger.warn("Validation failed [{}]", e.getMessage());
            throw new Validator.InvalidValueException(e.getMessage());
        } catch (Exception e) {
            logger.error("Unknown exception occurred while validating fields", e);
        }
    }

    public List<String> getCurrentPlatformSupportedFileTypes() {
        return platformSupportedFileTypesMap.get(platformSelect.getValue());
    }
}
