/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.downloadable.ui.sla;

import com.vaadin.data.Validator;
import com.vaadin.data.validator.IntegerValidator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.ui.*;
import hms.kite.prov.downloadable.DownloadableServiceRegistry;
import hms.kite.provisioning.commons.ui.AbstractBasicDetailsLayout;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.component.charging.ChargingLayout;
import hms.kite.provisioning.commons.ui.component.popup.HelpDetails;
import hms.kite.provisioning.commons.ui.validator.SpLimitValidator;
import hms.kite.provisioning.commons.ui.validator.SubscriptionRequiredValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import static hms.kite.prov.downloadable.DownloadableServiceRegistry.*;
import static hms.kite.prov.downloadable.util.DownloadableSlaRoles.*;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class DownloadableSlaBasicLayout extends AbstractBasicDetailsLayout {

    private static final Logger logger = LoggerFactory.getLogger(DownloadableSlaBasicLayout.class);

    private static final String WIDTH = "170px";
    private static final String HELP_IMAGE = "help.jpg";
    public static final String HELP_IMG_WIDTH = "850px";
    public static final String HELP_IMG_HEIGHT = "460px";
    public static final String HELP_IMG_STYLE_NAME = "link";
    public static final String HELP_BUTTON_NAME = "Help";

    private Map<String, Object> eventData;
    private String appId;

    private FormLayout formLayout;

    private TextField appIdTxtFld;
    private TextField chargNotificationTxtFld;
    private TextField maxConcurrentDownloadTxtFld;
    private TextField maxDownloadPerDayTxtFld;
    private TextField linkExpTimeTxtFld;

    private Select downloadAttemptSelect;
    private Select linkExpTimeTypeSelect;

    private ChargingLayout chargingLayout;

    private String corporateUserId;
    private Label validationErrorLbl;

    private CheckBox subscriptionRequiredChkBox;

    private List<Map> selectedNcses;
    private PopupView popupView;

    private void init() {
        logger.debug("Initializing the Downloadable Application SLA Basic Information form");

        appId = (String) eventData.get(appIdK);
        selectedNcses = (List<Map>) eventData.get(ncsesK);

        createSlaBasicForm();
    }

    private void createSlaBasicForm() {
        formLayout = new FormLayout();
        formLayout.addStyleName("downloadable-ncs-form");
        formLayout.addStyleName("downloadable-padding-basic");

        createAppIdTxtFld();
        createDownloadAttemptSelect();
        createMaxConcurrentDownloadTxtFld();
        createMaxDownloadPerDayTxtFld();
        createExpTimeHLayout();
        createChargingForm();
        createSubscriptionRequiredChkBox();
        createChargingNotificationUrlView();
        if(isHelpButtonLinkEnable()) {
            createHelperLink();
        }
        addComponent(formLayout);
        addComponent(validationErrorLbl);
    }

    private void createHelperLink() {
        Button helpButtonLink = createHelpButtonLink();
        addComponent(helpButtonLink);
        setComponentAlignment(helpButtonLink, Alignment.TOP_RIGHT);
    }

    private Button createHelpButtonLink() {
        final HelpDetails helpDetails = new HelpDetails(application, HELP_IMAGE);
        final Embedded image = helpDetails.getHelpImage();
        Button helpButtonLink = new Button(HELP_BUTTON_NAME);
        helpButtonLink.setStyleName(HELP_IMG_STYLE_NAME);
        helpButtonLink.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent clickEvent) {
                getWindow().addWindow(helpDetails.generateHelpImageSubWindow(image, HELP_IMG_WIDTH, HELP_IMG_HEIGHT));
            }
        });
        return helpButtonLink;
    }

    private void createAppIdTxtFld() {
        appIdTxtFld = getFieldWithPermission(APPLICATION_ID, new TextField());
        if (isFieldNotNull(appIdTxtFld)) {
            appIdTxtFld.setCaption(application.getMsg("downloadable.sla.appid.caption"));
            appIdTxtFld.setDescription(application.getMsg("downloadable.sla.appid.tooltip"));
            appIdTxtFld.setWidth(WIDTH);
            appIdTxtFld.setValue(appId);
            formLayout.addComponent(appIdTxtFld);
        }
    }

    private void createChargingNotificationUrlView() {
        chargNotificationTxtFld = getFieldWithPermission(CHARGING_NOTIFICATION_ENABLE, new TextField());
        if (isFieldNotNull(chargNotificationTxtFld)) {
            chargNotificationTxtFld.setCaption(application.getMsg("downloadable.sla.charging.url.caption"));
            chargNotificationTxtFld.setDescription(application.getMsg("downloadable.sla.charging.url.tooltip"));
            chargNotificationTxtFld.setWidth(WIDTH);
            chargNotificationTxtFld.setRequired(true);
            chargNotificationTxtFld.setImmediate(true);
            chargNotificationTxtFld.setRequiredError(application.getMsg("downloadable.sla.async.charging.notification.url.required.error"));
            chargNotificationTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("notificationUrlValidation"),
                    application.getMsg("downloadable.sla.async.charging.notification.url.validation.error")));
            formLayout.addComponent(chargNotificationTxtFld);
        }
    }

    private void createDownloadAttemptSelect() {
        downloadAttemptSelect = getFieldWithPermission(DOWNLOAD_ATTEMPT, new Select());
        if (isFieldNotNull(downloadAttemptSelect)) {
            downloadAttemptSelect.setCaption(application.getMsg("downloadable.sla.download.attempt.caption"));
            downloadAttemptSelect.setDescription(application.getMsg("downloadable.sla.download.attempt.tooltip"));
            downloadAttemptSelect.setNullSelectionAllowed(false);
            downloadAttemptSelect.setWidth(WIDTH);

            int maxNoAttempts = DownloadableServiceRegistry.getMaxNoOfDownloadAttempts();

            for (int i = 1; i <= maxNoAttempts; i++) {
                downloadAttemptSelect.addItem(i);
            }
            if (isDefaultDownloadAttemptAllow()) {
                downloadAttemptSelect.setValue(getDefaultDownloadAttempt());
            }
            formLayout.addComponent(downloadAttemptSelect);
        }
    }

    private void createMaxConcurrentDownloadTxtFld() {
        maxConcurrentDownloadTxtFld = getFieldWithPermission(MAX_CONCURRENT_DOWNLOAD, new TextField());
        if (isFieldNotNull(maxConcurrentDownloadTxtFld)) {
            maxConcurrentDownloadTxtFld.setCaption(application.getMsg("downloadable.sla.max.concurrent.download.caption"));
            maxConcurrentDownloadTxtFld.setDescription(application.getMsg("downloadable.sla.max.concurrent.download.tooltip"));
            maxConcurrentDownloadTxtFld.setRequired(true);
            maxConcurrentDownloadTxtFld.setImmediate(true);
            maxConcurrentDownloadTxtFld.setRequiredError(application.getMsg("downloadable.sla.max.concurrent.download.required.error"));
            Object maxConcurrentDownload = eventData.get(dlAppMcdK);
            maxConcurrentDownloadTxtFld.addValidator(new SpLimitValidator(Integer.valueOf(String.valueOf(maxConcurrentDownload)),
                    String.format(application.getMsg("downloadable.sla.max.concurrent.download.sp.limit.error"),
                            maxConcurrentDownload)));
            maxConcurrentDownloadTxtFld.addValidator(new IntegerValidator(application.getMsg("downloadable.sla.max.concurrent.download.format.error")));
            maxConcurrentDownloadTxtFld.setWidth(WIDTH);
            if (isDefaultMaxConcurrentDownloadAllow()) {
                maxConcurrentDownloadTxtFld.setValue(getDefaultMaxConcurrentDownload());
            }
            formLayout.addComponent(maxConcurrentDownloadTxtFld);
        }
    }

    private void createMaxDownloadPerDayTxtFld() {
        maxDownloadPerDayTxtFld = getFieldWithPermission(MAX_DOWNLOAD_PER_DAY, new TextField());
        if (isFieldNotNull(maxDownloadPerDayTxtFld)) {
            maxDownloadPerDayTxtFld.setCaption(application.getMsg("downloadable.sla.max.download.per.day.caption"));
            maxDownloadPerDayTxtFld.setDescription(application.getMsg("downloadable.sla.max.download.per.day.tooltip"));
            maxDownloadPerDayTxtFld.setRequired(true);
            maxDownloadPerDayTxtFld.setRequired(true);
            maxDownloadPerDayTxtFld.setRequiredError(application.getMsg("downloadable.sla.max.download.per.day.required.error"));
            Object maxDownloadPerDay = eventData.get(dlAppMdpdK);
            maxDownloadPerDayTxtFld.addValidator(new SpLimitValidator(Integer.valueOf(String.valueOf(maxDownloadPerDay)),
                    String.format(application.getMsg("downloadable.sla.max.download.per.day.sp.limit.error"), maxDownloadPerDay)));
            maxDownloadPerDayTxtFld.addValidator(new IntegerValidator(application.getMsg("downloadable.sla.max.download.per.day.format.error")));
            maxDownloadPerDayTxtFld.setWidth(WIDTH);
            if (isDefaultMaxDownloadPerDayAllow()) {
                maxDownloadPerDayTxtFld.setValue(getDefaultMaxDownloadPerDay());
            }
            formLayout.addComponent(maxDownloadPerDayTxtFld);
        }
    }

    private void createExpTimeHLayout() {
        linkExpTimeTxtFld = getFieldWithPermission(LINK_EXPIRATION_TIME, new TextField());
        if (isFieldNotNull(linkExpTimeTxtFld)) {
            linkExpTimeTxtFld.setWidth("50px");
            linkExpTimeTxtFld.setRequired(true);
            linkExpTimeTxtFld.setImmediate(true);
            linkExpTimeTxtFld.setRequiredError(application.getMsg("downloadable.sla.download.link.expiration.required.error"));
            if (isDefaultExpirationTimeAllow()) {
                linkExpTimeTxtFld.setValue(getDefaultExpirationTime());
            }
        }

        linkExpTimeTypeSelect = getFieldWithPermission(LINK_EXPIRATION_TIME_TYPE, new Select());
        if (isFieldNotNull(linkExpTimeTypeSelect)) {
            linkExpTimeTypeSelect.setWidth("80px");
            linkExpTimeTypeSelect.addStyleName("downloadable-link-expiration");
            linkExpTimeTypeSelect.setNullSelectionAllowed(false);
            linkExpTimeTypeSelect.addItem(minutesK);
            linkExpTimeTypeSelect.addItem(hoursK);
            linkExpTimeTypeSelect.addItem(daysK);
            if (isDefaultExpirationTimeTypeAllow()) {
                linkExpTimeTypeSelect.setValue(getDefaultExpirationTimeType());
            }
        }

        if (isFieldNotNull(linkExpTimeTxtFld) && isFieldNotNull(linkExpTimeTypeSelect)) {
            HorizontalLayout expTimeHLayout = new HorizontalLayout();
            expTimeHLayout.setCaption(application.getMsg("downloadable.sla.download.link.expiration.caption"));
            expTimeHLayout.setDescription(application.getMsg("downloadable.sla.download.link.expiration.tooltip"));
            expTimeHLayout.addComponent(linkExpTimeTxtFld);
            expTimeHLayout.addComponent(linkExpTimeTypeSelect);
            formLayout.addComponent(expTimeHLayout);
        }
    }

    private void createChargingForm() {
        chargingLayout = ChargingLayout.downloadableChargingLayout(formLayout, application, corporateUserId, DOWNLOADABLE_PERMISSION_ROLE_PREFIX, WIDTH, 3, 13);
    }

    private void createSubscriptionRequiredChkBox() {
        subscriptionRequiredChkBox = getFieldWithPermission(SUBSCRIPTION_REQUIRED, new CheckBox());
        if (isFieldNotNull(subscriptionRequiredChkBox)) {
            subscriptionRequiredChkBox.setEnabled(true);
            subscriptionRequiredChkBox.setCaption(application.getMsg("downloadable.sla.subscription.required.caption"));
            subscriptionRequiredChkBox.setDescription(application.getMsg("downloadable.sla.subscription.required.tooltip"));
            subscriptionRequiredChkBox.setWidth(WIDTH);
            subscriptionRequiredChkBox.addValidator(new SubscriptionRequiredValidator(selectedNcses,
                    application.getMsg("downloadable.sla.subscription.not.selected.error")));
            subscriptionRequiredChkBox.setValue(false);
            formLayout.addComponent(subscriptionRequiredChkBox);
        }
    }

    public DownloadableSlaBasicLayout(BaseApplication application,
                                      Map<String, Object> eventData,
                                      String corporateUserId, Label validationErrorLbl) {
        super(application, DOWNLOADABLE_PERMISSION_ROLE_PREFIX);

        this.eventData = eventData;
        this.corporateUserId = corporateUserId;
        this.validationErrorLbl = validationErrorLbl;

        init();
    }

    @Override
    public void getData(Map<String, Object> data) {
        getValueFromField(appIdTxtFld, data, appIdK);
        getValueFromField(downloadAttemptSelect, data, dlAtmptK);
        getValueFromField(maxConcurrentDownloadTxtFld, data, maxConDlK);
        getValueFromField(maxDownloadPerDayTxtFld, data, maxDlPerDayK);
        getValueFromField(linkExpTimeTxtFld, data, linkExpTimeK);
        getValueFromField(linkExpTimeTypeSelect, data, linkExpTimeTypeK);
        getValueFromField(chargingLayout, data, chargingK);
        getValueFromField(subscriptionRequiredChkBox, data, subscriptionRequiredK);
        getValueFromField(chargNotificationTxtFld, data, asyncChargingNotificationUrl);
    }

    @Override
    public void setData(Map<String, Object> data) {
        setValueToField(appIdTxtFld, data.get(appIdK));
        setValueToField(downloadAttemptSelect, data.get(dlAtmptK));
        setValueToField(maxConcurrentDownloadTxtFld, data.get(maxConDlK));
        setValueToField(maxDownloadPerDayTxtFld, data.get(maxDlPerDayK));
        setValueToField(linkExpTimeTxtFld, data.get(linkExpTimeK));
        setValueToField(linkExpTimeTypeSelect, data.get(linkExpTimeTypeK));
        setValueToField(chargingLayout, data.get(chargingK));
        setValueToField(subscriptionRequiredChkBox, data.get(subscriptionRequiredK));
        setValueToField(chargNotificationTxtFld, data.get(asyncChargingNotificationUrl));
    }

    @Override
    public void validate() {
        try {
            validateField(maxConcurrentDownloadTxtFld);
            validateField(maxDownloadPerDayTxtFld);
            if (isFieldNotNull(maxConcurrentDownloadTxtFld) && isFieldNotNull(maxDownloadPerDayTxtFld) &&
                    Integer.parseInt((String) maxConcurrentDownloadTxtFld.getValue()) > Integer.parseInt((String) maxDownloadPerDayTxtFld.getValue())) {
                throw new Validator.InvalidValueException(application.getMsg("downloadable.sla.max.download.per.day.less.than.max.concurrent.download.error"));
            }
            validateField(linkExpTimeTxtFld);
            validateField(chargingLayout);
            validateField(subscriptionRequiredChkBox);
            validateField(chargNotificationTxtFld);
        } catch (Validator.InvalidValueException e) {
            validationErrorLbl.setValue(e.getMessage());
            logger.warn("Validation failed [{}]", e.getMessage());
            throw new Validator.InvalidValueException(e.getMessage());
        } catch (Exception e) {
            logger.error("Unknown exception occurred while validating fields", e);
        }
    }

    @Override
    public void reset() {
        throw new UnsupportedOperationException("This function is not implemented");
    }

    @Override
    public void setPermissions() {
        setReadWritePermissionToComponent(appIdTxtFld, downloadAttemptSelect, maxConcurrentDownloadTxtFld,
                maxDownloadPerDayTxtFld, linkExpTimeTxtFld, linkExpTimeTypeSelect, subscriptionRequiredChkBox, chargNotificationTxtFld);
        chargingLayout.setPermissions();
    }
}
