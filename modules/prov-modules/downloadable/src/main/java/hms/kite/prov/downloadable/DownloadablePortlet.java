/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.prov.downloadable;

import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.terminal.gwt.server.PortletApplicationContext2;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import hms.kite.datarepo.audit.AuditTrail;
import hms.kite.prov.downloadable.ui.buildfile.create.DownloadableBuildFileMainLayout;
import hms.kite.prov.downloadable.ui.common.ViewMode;
import hms.kite.prov.downloadable.ui.sla.DownloadableSlaMainLayout;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.UiManagerImpl;
import hms.kite.provisioning.commons.ui.event.PortletEventSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.ncsRepositoryService;
import static hms.kite.prov.downloadable.DownloadableServiceRegistry.buildFileRepositoryService;
import static hms.kite.provisioning.commons.ui.LiferayUtil.createPortletId;
import static hms.kite.provisioning.commons.util.ProvCommonUtil.getNcsDataFromSessionByAppId;
import static hms.kite.provisioning.commons.util.ProvCommonUtil.getNcsDataFromSessionByKey;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class DownloadablePortlet extends BaseApplication {

    private static final Logger logger = LoggerFactory.getLogger(DownloadablePortlet.class);

    private static final String PORTLET_USER_ID = "portlet_user_id";

    private DownloadableSlaMainLayout downloadableSlaMainLayout;
    private DownloadableBuildFileMainLayout downloadableBuildFileMainLayout;
    private ThemeDisplay themeDisplay;
    private String corporateUserId;
    private Panel mainPanel;
    private Panel innerPanel;

    private void handleCreateNcsRequest(Map<String, Object> eventData) {
        logger.debug("Downloadable NCS SLA Create request received");

        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        setSlaInnerPanel();
        innerPanel.removeAllComponents();
        eventData.put(currentEventOriginK, createNcsK);
        downloadableSlaMainLayout = new DownloadableSlaMainLayout(this, eventData, corporateUserId);
        Object eventOrigin = eventData.get(eventOriginK);
        if (eventOrigin != null
                && (eventOrigin.equals(createBuildFileEventK)
                || eventOrigin.equals(reConfigureBuildFileEventK)
                || eventOrigin.equals(viewBuildFileEventK))) {
            String appId = (String) eventData.get(appIdK);
            Map<String, Object> downloadableSlaData = getDownloadableSlaData(appId);

            logger.debug("Downloadable application NCS SLA details [{}]", downloadableSlaData);

            downloadableSlaMainLayout.setData(downloadableSlaData);
            downloadableSlaMainLayout.reloadBuildFiles(getBuildFilesList(appId));
        } else {
            removeFromSession(buildFileDataSessionK);
            removeFromSession(buildFilesDataListSessionK);
            removeFromSession(downloadableSlaDataSessionK);
            removeFromSession(downloadableSlaStatusSessionK);
            removeFromSession(selectedPlatformVersions);
        }
        innerPanel.addComponent(downloadableSlaMainLayout);
        downloadableSlaMainLayout.setViewMode(ViewMode.EDIT);
        downloadableSlaMainLayout.reloadComponents();
    }

    private void handleSaveNcsRequest(Map<String, Object> eventData) {
        logger.debug("Downloadable NCS SLA Save request received");

        String operator = "";
        String ncsType = downloadableK;
        String appId = (String) eventData.get(appIdK);
        String status = (String) eventData.get(statusK);
        String spId = (String) eventData.get(spIdK);
        Map<String, Object> downloadableSlaData = getDownloadableSlaData(appId);
        List<Map<String, Object>> buildFilesList = getBuildFilesList(appId);

        if (deleteK.equals(status)) {
            ncsRepositoryService().update(appId, ncsType, operator, status);
            buildFileRepositoryService().deleteBuildFileByAppId(appId);
        } else if (downloadableSlaData != null) {
            downloadableSlaData.put(statusK, status);
            ncsRepositoryService().createNcs(downloadableSlaData, spId);

            if (buildFilesList != null) {
                for (Map<String, Object> buildFile : buildFilesList) {
                    Object buildFileStatus = buildFile.get(statusK);
                    if (initialK.equals(buildFileStatus) || draftK.equals(buildFileStatus)) {
                        buildFile.put(statusK, status);
                        buildFileRepositoryService().updateBuildFile(buildFile);
                    }
                }
            }
        } else {
            ncsRepositoryService().update(appId, ncsType, operator, status);
        }
        removeFromSession(downloadableSlaDataSessionK);
        removeFromSession(buildFileDataSessionK);
        removeFromSession(buildFilesDataListSessionK);
        removeFromSession(downloadableSlaStatusSessionK);
        closePortlet();

        logger.debug("Downloadable NCS SLA Save request processed successfully");
    }

    private void handleViewNcsRequest(Map<String, Object> eventData) {
        logger.debug("Downloadable NCS SLA View request received");

        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        setSlaInnerPanel();
        innerPanel.removeAllComponents();
        eventData.put(currentEventOriginK, viewNcsK);
        downloadableSlaMainLayout = new DownloadableSlaMainLayout(this, eventData, corporateUserId);
        innerPanel.addComponent(downloadableSlaMainLayout);
        reloadSlaDataFromRepository(eventData);
        downloadableSlaMainLayout.setViewMode(ViewMode.VIEW);
        downloadableSlaMainLayout.reloadComponents();
    }

    private void handleReconfigureNcsRequest(Map<String, Object> eventData) {
        logger.debug("Downloadable NCS SLA Re-Configure request received");

        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        setSlaInnerPanel();
        innerPanel.removeAllComponents();
        String appId = (String) eventData.get(appIdK);
        Map<String, Object> downloadableSlaData = getDownloadableSlaData(appId);
        List<Map<String, Object>> buildFilesList = getBuildFilesList(appId);
        eventData.put(currentEventOriginK, reconfigureNcsK);
        downloadableSlaMainLayout = new DownloadableSlaMainLayout(this, eventData, corporateUserId);
        innerPanel.addComponent(downloadableSlaMainLayout);
        if (downloadableSlaData != null) {
            logger.debug("Reloading APP SLA data from session");

            downloadableSlaMainLayout.setData(downloadableSlaData);
            if (buildFilesList != null) {
                downloadableSlaMainLayout.reloadBuildFiles(buildFilesList);
            }
        } else {
            logger.debug("Reloading data from db repository");

            reloadSlaDataFromRepository(eventData);
        }
        downloadableSlaMainLayout.setViewMode(ViewMode.EDIT);
        downloadableSlaMainLayout.reloadComponents();
    }

    private void handleCrEditNcsEvent(Map<String, Object> eventData) {
        logger.debug("Downloadable NCS SLA CR Edit request received");

        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        setSlaInnerPanel();
        innerPanel.removeAllComponents();
        Map<String, Object> downloadableSlaData = getDownloadableSlaData((String) eventData.get(appIdK));
        downloadableSlaMainLayout = new DownloadableSlaMainLayout(this, eventData, corporateUserId);
        innerPanel.addComponent(downloadableSlaMainLayout);
        if (downloadableSlaData != null) {
            logger.debug("Reloading data from session");

            downloadableSlaMainLayout.setData(downloadableSlaData);
        } else {
            logger.debug("Reloading data from db repository");

            reloadSlaDataFromRepository(eventData);
        }
        downloadableSlaMainLayout.setViewMode(ViewMode.EDIT);
        downloadableSlaMainLayout.reloadComponents();
    }

    private void setSlaInnerPanel() {
        innerPanel.setCaption(this.getMsg("downloadable.sla.window.title"));
    }

    private void handleCreateBuildFileRequest(Map<String, Object> eventData) {
        logger.debug("Downloadable Build File Create request received");

        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        setBuildFileInnerPanel();
        innerPanel.removeAllComponents();
        eventData.put(currentEventOriginK, createBuildFileEventK);
        downloadableBuildFileMainLayout = new DownloadableBuildFileMainLayout(this, eventData, corporateUserId);
        innerPanel.addComponent(downloadableBuildFileMainLayout);
        removeFromSession(buildFileDataSessionK);
        downloadableBuildFileMainLayout.setViewMode(ViewMode.EDIT);
        downloadableBuildFileMainLayout.reloadComponents();
    }

    private void handleSaveBuildFileRequest(Map<String, Object> eventData) {
        logger.debug("Downloadable Build File Save request received");

        Map<String, Object> buildFileData = getBuildFileData(eventData);
        String status = (String) eventData.get(statusK);

        if (buildFileData != null) {
            buildFileData.put(statusK, status);
            buildFileRepositoryService().createBuildFile(buildFileData);
        }
        removeFromSession(buildFileDataSessionK);
        PortletEventSender.send(eventData, provViewNcsEvent, this);
        closePortlet();

        logger.debug("Downloadable Build File Save request processed successfully");
    }

    private void handleViewBuildFileRequest(Map<String, Object> eventData) {
        logger.debug("Downloadable Build File View request received");

        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        setBuildFileInnerPanel();
        innerPanel.removeAllComponents();
        eventData.put(currentEventOriginK, viewBuildFileEventK);
        downloadableBuildFileMainLayout = new DownloadableBuildFileMainLayout(this, eventData, corporateUserId);
        innerPanel.addComponent(downloadableBuildFileMainLayout);
        loadBuildFileData(eventData);
        downloadableBuildFileMainLayout.setViewMode(ViewMode.VIEW);
        downloadableBuildFileMainLayout.reloadComponents();
    }

    private void handleReconfigureBuildFileRequest(Map<String, Object> eventData) {
        logger.debug("Downloadable Build File Re-Configure request received");

        mainPanel.setCaption((String) eventData.get(ncsPortletTitle));
        setBuildFileInnerPanel();
        innerPanel.removeAllComponents();
        eventData.put(currentEventOriginK, reConfigureBuildFileEventK);
        downloadableBuildFileMainLayout = new DownloadableBuildFileMainLayout(this, eventData, corporateUserId);
        innerPanel.addComponent(downloadableBuildFileMainLayout);
        loadBuildFileData(eventData);
        downloadableBuildFileMainLayout.setViewMode(ViewMode.EDIT);
        downloadableBuildFileMainLayout.reloadComponents();
    }

    private void setBuildFileInnerPanel() {
        innerPanel.setCaption(this.getMsg("downloadable.buildfile.window.title"));
    }

    private void reloadSlaDataFromRepository(Map<String, Object> eventData) {
        String appId = (String) eventData.get(appIdK);
        List<Map<String, Object>> downloadableSla = ncsRepositoryService().findByAppIdNcsType(appId, downloadableK);
        List<Map<String, Object>> downloadableBuildFilesList = buildFileRepositoryService().findByAppId(appId);

        if (downloadableSla != null && downloadableSla.size() > 0) {
            Map<String, Object> dlSlaData = downloadableSla.get(0);
            addToSession(downloadableSlaDataSessionK, dlSlaData);
            downloadableSlaMainLayout.setData(dlSlaData);

            logger.debug("Downloadable SLA information {}", dlSlaData);
        }
        if (downloadableBuildFilesList != null && downloadableBuildFilesList.size() > 0) {
            addToSession(buildFilesDataListSessionK, downloadableBuildFilesList);
            downloadableSlaMainLayout.reloadBuildFiles(downloadableBuildFilesList);

            logger.debug("Downloadable Build File List information.");
        }
    }

    private void loadBuildFileData(Map<String, Object> eventData) {
        Map<String, Object> buildFileData = getBuildFileData(eventData);
        if (buildFileData != null) {
            logger.debug("Reloading build file data from session");

            downloadableBuildFileMainLayout.setData(buildFileData);
        } else {
            if (adminManageBuildFilesEventK.equals(eventData.get(eventOriginK))) {
                logger.debug("Reloading data from db repository");

                reloadBuildFileDataFromRepository(eventData);
            }
        }
    }

    private void reloadBuildFileDataFromRepository(Map<String, Object> eventData) {
        String fileId = eventData.get(buildAppFileIdK).toString();
        Map<String, Object> buildFile = buildFileRepositoryService().findByBuildFileId(fileId);

        if (buildFile != null) {
            addToSession(buildFileDataSessionK, buildFile);
            downloadableBuildFileMainLayout.setData(buildFile);

            logger.debug("Downloadable Build File information {}", buildFile);
        }
    }

    private void setCorporateUserId(EventRequest request) {
        Map<String, Object> eventData = (HashMap<String, Object>) request.getEvent().getValue();
        this.corporateUserId = (String) eventData.get(coopUserIdK);
    }

    private void setPortletUserId(Long userId) {
        if (getPortletUserId() == null) {
            addToSession(PORTLET_USER_ID, userId);
        }
    }

    private Long getPortletUserId() {
        return (Long) getFromSession(PORTLET_USER_ID);
    }

    @Override
    protected String getApplicationName() {
        return "downloadable";
    }

    @Override
    public void init() {
        setResourceBundle("US");
        Window main = new Window(this.getMsg("downloadable.sla.window.title"));
        setMainWindow(main);
        setUiManager(new UiManagerImpl(this));

        VerticalLayout mainLayout = new VerticalLayout();

        mainPanel = new Panel();
        mainLayout.addComponent(mainPanel);

        mainLayout.addStyleName("container-panel");
        mainLayout.setWidth("762px");
        mainLayout.setMargin(true);

        innerPanel = new Panel();
        mainPanel.addComponent(innerPanel);
        innerPanel.addStyleName("child-panel");

        setPortletContext((PortletApplicationContext2) getContext());
        getPortletContext().addPortletListener(this, this);

        getUiManager().clearAll();
        getUiManager().pushScreen(DownloadablePortlet.class.getName(), mainLayout);
    }

    @Override
    public void handleEventRequest(EventRequest request, EventResponse response, Window window) {
        super.handleEventRequest(request, response, window);

        try {
            setCorporateUserId(request);

            String eventName = request.getEvent().getName();
            themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
            Long userId = themeDisplay.getUserId();
            setPortletUserId(userId);
            Map<String, Object> eventData = (Map<String, Object>) request.getEvent().getValue();
            logger.debug("Received Event Data is [{}]", eventData);
            AuditTrail.setCurrentUser((String) eventData.get(currentUsernameK), (String) eventData.get(currentUserTypeK));
            addToSession(currentUserTypeK, eventData.get(currentUserTypeK));

            Object appStatus = eventData.get(appStatusK);

            if (appStatus != null) {
                if ((initialK.equals(appStatus) || draftK.equals(appStatus))
                        && (getFromSession(downloadableSlaStatusSessionK) == null
                        || initialK.equals(getFromSession(downloadableSlaStatusSessionK)))) {
                    eventData.put(downloadableSlaStatusSessionK, initialK);
                } else {
                    eventData.put(downloadableSlaStatusSessionK, configuredK);
                }
            }

            String ncsType = (String) eventData.get(ncsTypeK);

            if (ncsType == null || !ncsType.equals(downloadableK)) {
                logger.debug("Event Request ignored, since it is not belong to me");
                return;
            }

            if (createNcsK.equals(eventName)) {
                handleCreateNcsRequest(eventData);
            } else if (saveNcsK.equals(eventName)) {
                handleSaveNcsRequest(eventData);
            } else if (viewNcsK.equals(eventName)) {
                handleViewNcsRequest(eventData);
            } else if (reconfigureNcsK.equals(eventName)) {
                handleReconfigureNcsRequest(eventData);
            } else if (crEditNcsEvent.equals(eventName)) {
                handleCrEditNcsEvent(eventData);
            } else if (createBuildFileEventK.equals(eventName)) {
                handleCreateBuildFileRequest(eventData);
            } else if (saveBuildFileEventK.equals(eventName)) {
                handleSaveBuildFileRequest(eventData);
            } else if (viewBuildFileEventK.equals(eventName)) {
                handleViewBuildFileRequest(eventData);
            } else if (reConfigureBuildFileEventK.equals(eventName)) {
                handleReconfigureBuildFileRequest(eventData);
            } else {
                logger.error("Unexpected event [{}], Can't handle", eventName);
            }
        } catch (Exception e) {
            logger.error("Unexpected error occurred while handling event ", e);
        }
    }

    public Map<String, Object> getDownloadableSlaData(String appId) {
        return getNcsDataFromSessionByAppId(appId, downloadableSlaDataSessionK, this);
    }

    public Map<String, Object> getBuildFileData(Map<String, Object> eventData) {
        String fileId = (String) eventData.get(buildAppFileIdK);

        if (fileId != null) {
            return getNcsDataFromSessionByKey(buildAppFileIdK, fileId, buildFileDataSessionK, this);
        } else {
            return getNcsDataFromSessionByKey(appIdK, (String) eventData.get(appIdK), buildFileDataSessionK, this);
        }
    }

    public List<Map<String, Object>> getBuildFilesList(String appId) {
        List<Map<String, Object>> result = (List<Map<String, Object>>) getFromSession(buildFilesDataListSessionK);
        if (appId != null && appId.isEmpty()) {
            return result;
        } else if (result != null && !result.isEmpty() && appId != null && appId.equals(result.get(0).get(appIdK))) {
            return result;
        }
        return null;
    }

    public void closePortlet() {
        if (downloadableSlaMainLayout != null) {
            downloadableSlaMainLayout.removeAllComponents();
        }

        if (downloadableBuildFileMainLayout != null) {
            downloadableBuildFileMainLayout.removeAllComponents();
        }

        themeDisplay.getLayoutTypePortlet().removePortletId(getPortletUserId(), createPortletId(downloadableK));
    }
}
