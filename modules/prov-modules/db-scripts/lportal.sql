-- MySQL dump 10.13  Distrib 5.1.56, for redhat-linux-gnu (i386)
--
-- Host: localhost    Database: lportal
-- ------------------------------------------------------
-- Server version	5.1.39-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Account_`
--

DROP TABLE IF EXISTS `Account_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Account_` (
  `accountId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `parentAccountId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `legalName` varchar(75) DEFAULT NULL,
  `legalId` varchar(75) DEFAULT NULL,
  `legalType` varchar(75) DEFAULT NULL,
  `sicCode` varchar(75) DEFAULT NULL,
  `tickerSymbol` varchar(75) DEFAULT NULL,
  `industry` varchar(75) DEFAULT NULL,
  `type_` varchar(75) DEFAULT NULL,
  `size_` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`accountId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Account_`
--

LOCK TABLES `Account_` WRITE;
/*!40000 ALTER TABLE `Account_` DISABLE KEYS */;
INSERT INTO `Account_` VALUES (10134,10132,0,'','2011-07-01 20:41:15','2011-07-01 20:41:15',0,'liferay.com','','','','','','','','');
/*!40000 ALTER TABLE `Account_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Address`
--

DROP TABLE IF EXISTS `Address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Address` (
  `addressId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `street1` varchar(75) DEFAULT NULL,
  `street2` varchar(75) DEFAULT NULL,
  `street3` varchar(75) DEFAULT NULL,
  `city` varchar(75) DEFAULT NULL,
  `zip` varchar(75) DEFAULT NULL,
  `regionId` bigint(20) DEFAULT NULL,
  `countryId` bigint(20) DEFAULT NULL,
  `typeId` int(11) DEFAULT NULL,
  `mailing` tinyint(4) DEFAULT NULL,
  `primary_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`addressId`),
  KEY `IX_93D5AD4E` (`companyId`),
  KEY `IX_ABD7DAC0` (`companyId`,`classNameId`),
  KEY `IX_71CB1123` (`companyId`,`classNameId`,`classPK`),
  KEY `IX_923BD178` (`companyId`,`classNameId`,`classPK`,`mailing`),
  KEY `IX_9226DBB4` (`companyId`,`classNameId`,`classPK`,`primary_`),
  KEY `IX_5BC8B0D4` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Address`
--

LOCK TABLES `Address` WRITE;
/*!40000 ALTER TABLE `Address` DISABLE KEYS */;
/*!40000 ALTER TABLE `Address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AnnouncementsDelivery`
--

DROP TABLE IF EXISTS `AnnouncementsDelivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AnnouncementsDelivery` (
  `deliveryId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `type_` varchar(75) DEFAULT NULL,
  `email` tinyint(4) DEFAULT NULL,
  `sms` tinyint(4) DEFAULT NULL,
  `website` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`deliveryId`),
  UNIQUE KEY `IX_BA4413D5` (`userId`,`type_`),
  KEY `IX_6EDB9600` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AnnouncementsDelivery`
--

LOCK TABLES `AnnouncementsDelivery` WRITE;
/*!40000 ALTER TABLE `AnnouncementsDelivery` DISABLE KEYS */;
/*!40000 ALTER TABLE `AnnouncementsDelivery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AnnouncementsEntry`
--

DROP TABLE IF EXISTS `AnnouncementsEntry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AnnouncementsEntry` (
  `uuid_` varchar(75) DEFAULT NULL,
  `entryId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `title` varchar(75) DEFAULT NULL,
  `content` longtext,
  `url` longtext,
  `type_` varchar(75) DEFAULT NULL,
  `displayDate` datetime DEFAULT NULL,
  `expirationDate` datetime DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `alert` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`entryId`),
  KEY `IX_A6EF0B81` (`classNameId`,`classPK`),
  KEY `IX_14F06A6B` (`classNameId`,`classPK`,`alert`),
  KEY `IX_D49C2E66` (`userId`),
  KEY `IX_1AFBDE08` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AnnouncementsEntry`
--

LOCK TABLES `AnnouncementsEntry` WRITE;
/*!40000 ALTER TABLE `AnnouncementsEntry` DISABLE KEYS */;
/*!40000 ALTER TABLE `AnnouncementsEntry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AnnouncementsFlag`
--

DROP TABLE IF EXISTS `AnnouncementsFlag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AnnouncementsFlag` (
  `flagId` bigint(20) NOT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `entryId` bigint(20) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  PRIMARY KEY (`flagId`),
  UNIQUE KEY `IX_4539A99C` (`userId`,`entryId`,`value`),
  KEY `IX_9C7EB9F` (`entryId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AnnouncementsFlag`
--

LOCK TABLES `AnnouncementsFlag` WRITE;
/*!40000 ALTER TABLE `AnnouncementsFlag` DISABLE KEYS */;
/*!40000 ALTER TABLE `AnnouncementsFlag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AssetCategory`
--

DROP TABLE IF EXISTS `AssetCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AssetCategory` (
  `uuid_` varchar(75) DEFAULT NULL,
  `categoryId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `parentCategoryId` bigint(20) DEFAULT NULL,
  `leftCategoryId` bigint(20) DEFAULT NULL,
  `rightCategoryId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `title` longtext,
  `vocabularyId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`categoryId`),
  UNIQUE KEY `IX_BE4DF2BF` (`parentCategoryId`,`name`,`vocabularyId`),
  UNIQUE KEY `IX_E8D019AA` (`uuid_`,`groupId`),
  KEY `IX_E639E2F6` (`groupId`),
  KEY `IX_D61ABE08` (`name`,`vocabularyId`),
  KEY `IX_7BB1826B` (`parentCategoryId`),
  KEY `IX_9DDD15EA` (`parentCategoryId`,`name`),
  KEY `IX_B185E980` (`parentCategoryId`,`vocabularyId`),
  KEY `IX_4D37BB00` (`uuid_`),
  KEY `IX_287B1F89` (`vocabularyId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AssetCategory`
--

LOCK TABLES `AssetCategory` WRITE;
/*!40000 ALTER TABLE `AssetCategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `AssetCategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AssetCategoryProperty`
--

DROP TABLE IF EXISTS `AssetCategoryProperty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AssetCategoryProperty` (
  `categoryPropertyId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `categoryId` bigint(20) DEFAULT NULL,
  `key_` varchar(75) DEFAULT NULL,
  `value` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`categoryPropertyId`),
  UNIQUE KEY `IX_DBD111AA` (`categoryId`,`key_`),
  KEY `IX_99DA856` (`categoryId`),
  KEY `IX_8654719F` (`companyId`),
  KEY `IX_52340033` (`companyId`,`key_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AssetCategoryProperty`
--

LOCK TABLES `AssetCategoryProperty` WRITE;
/*!40000 ALTER TABLE `AssetCategoryProperty` DISABLE KEYS */;
/*!40000 ALTER TABLE `AssetCategoryProperty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AssetEntries_AssetCategories`
--

DROP TABLE IF EXISTS `AssetEntries_AssetCategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AssetEntries_AssetCategories` (
  `entryId` bigint(20) NOT NULL,
  `categoryId` bigint(20) NOT NULL,
  PRIMARY KEY (`entryId`,`categoryId`),
  KEY `IX_A188F560` (`categoryId`),
  KEY `IX_E119938A` (`entryId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AssetEntries_AssetCategories`
--

LOCK TABLES `AssetEntries_AssetCategories` WRITE;
/*!40000 ALTER TABLE `AssetEntries_AssetCategories` DISABLE KEYS */;
/*!40000 ALTER TABLE `AssetEntries_AssetCategories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AssetEntries_AssetTags`
--

DROP TABLE IF EXISTS `AssetEntries_AssetTags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AssetEntries_AssetTags` (
  `entryId` bigint(20) NOT NULL,
  `tagId` bigint(20) NOT NULL,
  PRIMARY KEY (`entryId`,`tagId`),
  KEY `IX_2ED82CAD` (`entryId`),
  KEY `IX_B2A61B55` (`tagId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AssetEntries_AssetTags`
--

LOCK TABLES `AssetEntries_AssetTags` WRITE;
/*!40000 ALTER TABLE `AssetEntries_AssetTags` DISABLE KEYS */;
/*!40000 ALTER TABLE `AssetEntries_AssetTags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AssetEntry`
--

DROP TABLE IF EXISTS `AssetEntry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AssetEntry` (
  `entryId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `classUuid` varchar(75) DEFAULT NULL,
  `visible` tinyint(4) DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `publishDate` datetime DEFAULT NULL,
  `expirationDate` datetime DEFAULT NULL,
  `mimeType` varchar(75) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` longtext,
  `summary` longtext,
  `url` longtext,
  `height` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `priority` double DEFAULT NULL,
  `viewCount` int(11) DEFAULT NULL,
  PRIMARY KEY (`entryId`),
  UNIQUE KEY `IX_1E9D371D` (`classNameId`,`classPK`),
  KEY `IX_FC1F9C7B` (`classUuid`),
  KEY `IX_7306C60` (`companyId`),
  KEY `IX_1EBA6821` (`groupId`,`classUuid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AssetEntry`
--

LOCK TABLES `AssetEntry` WRITE;
/*!40000 ALTER TABLE `AssetEntry` DISABLE KEYS */;
INSERT INTO `AssetEntry` VALUES (10155,10149,10132,10135,' ','2011-07-01 20:41:16','2011-07-01 20:41:16',10093,10153,'10a0bd46-ecc7-46df-9374-2b2d6ae33e88',0,NULL,NULL,NULL,NULL,'text/html','10152','','','',0,0,0,0),(10163,10157,10132,10135,' ','2011-07-01 20:41:17','2011-07-01 20:41:17',10093,10161,'d36ae8ff-2904-4f60-92ce-21ffaa967f39',0,NULL,NULL,NULL,NULL,'text/html','10160','','','',0,0,0,0),(10174,10165,10132,10169,'Test Test','2011-07-01 20:41:17','2011-07-01 20:41:17',10046,10169,'37b23206-85e3-4af9-856b-6fbb28af1b2f',0,NULL,NULL,NULL,NULL,'','Test Test','','','',0,0,0,0),(10274,10171,10132,10169,'Test Test','2011-07-01 20:42:55','2011-07-01 20:42:55',10093,10272,'94ccf957-319c-49cb-a1b0-f82108d3ad38',0,NULL,NULL,NULL,NULL,'text/html','10271','','','',0,0,0,0),(10279,10171,10132,10169,'Test Test','2011-07-01 20:42:55','2011-07-01 20:42:55',10093,10277,'46b378fd-e67d-4bec-bf81-b194919fdb08',0,NULL,NULL,NULL,NULL,'text/html','10276','','','',0,0,0,0);
/*!40000 ALTER TABLE `AssetEntry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AssetLink`
--

DROP TABLE IF EXISTS `AssetLink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AssetLink` (
  `linkId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `entryId1` bigint(20) DEFAULT NULL,
  `entryId2` bigint(20) DEFAULT NULL,
  `type_` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  PRIMARY KEY (`linkId`),
  KEY `IX_128516C8` (`entryId1`),
  KEY `IX_56E0AB21` (`entryId1`,`entryId2`),
  KEY `IX_8F542794` (`entryId1`,`entryId2`,`type_`),
  KEY `IX_14D5A20D` (`entryId1`,`type_`),
  KEY `IX_12851A89` (`entryId2`),
  KEY `IX_91F132C` (`entryId2`,`type_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AssetLink`
--

LOCK TABLES `AssetLink` WRITE;
/*!40000 ALTER TABLE `AssetLink` DISABLE KEYS */;
/*!40000 ALTER TABLE `AssetLink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AssetTag`
--

DROP TABLE IF EXISTS `AssetTag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AssetTag` (
  `tagId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `assetCount` int(11) DEFAULT NULL,
  PRIMARY KEY (`tagId`),
  KEY `IX_7C9E46BA` (`groupId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AssetTag`
--

LOCK TABLES `AssetTag` WRITE;
/*!40000 ALTER TABLE `AssetTag` DISABLE KEYS */;
/*!40000 ALTER TABLE `AssetTag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AssetTagProperty`
--

DROP TABLE IF EXISTS `AssetTagProperty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AssetTagProperty` (
  `tagPropertyId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `tagId` bigint(20) DEFAULT NULL,
  `key_` varchar(75) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`tagPropertyId`),
  UNIQUE KEY `IX_2C944354` (`tagId`,`key_`),
  KEY `IX_DFF1F063` (`companyId`),
  KEY `IX_13805BF7` (`companyId`,`key_`),
  KEY `IX_3269E180` (`tagId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AssetTagProperty`
--

LOCK TABLES `AssetTagProperty` WRITE;
/*!40000 ALTER TABLE `AssetTagProperty` DISABLE KEYS */;
/*!40000 ALTER TABLE `AssetTagProperty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AssetTagStats`
--

DROP TABLE IF EXISTS `AssetTagStats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AssetTagStats` (
  `tagStatsId` bigint(20) NOT NULL,
  `tagId` bigint(20) DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `assetCount` int(11) DEFAULT NULL,
  PRIMARY KEY (`tagStatsId`),
  UNIQUE KEY `IX_56682CC4` (`tagId`,`classNameId`),
  KEY `IX_50702693` (`classNameId`),
  KEY `IX_9464CA` (`tagId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AssetTagStats`
--

LOCK TABLES `AssetTagStats` WRITE;
/*!40000 ALTER TABLE `AssetTagStats` DISABLE KEYS */;
/*!40000 ALTER TABLE `AssetTagStats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AssetVocabulary`
--

DROP TABLE IF EXISTS `AssetVocabulary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AssetVocabulary` (
  `uuid_` varchar(75) DEFAULT NULL,
  `vocabularyId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `title` longtext,
  `description` longtext,
  `settings_` longtext,
  PRIMARY KEY (`vocabularyId`),
  UNIQUE KEY `IX_C0AAD74D` (`groupId`,`name`),
  UNIQUE KEY `IX_1B2B8792` (`uuid_`,`groupId`),
  KEY `IX_B22D908C` (`companyId`),
  KEY `IX_B6B8CA0E` (`groupId`),
  KEY `IX_55F58818` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AssetVocabulary`
--

LOCK TABLES `AssetVocabulary` WRITE;
/*!40000 ALTER TABLE `AssetVocabulary` DISABLE KEYS */;
/*!40000 ALTER TABLE `AssetVocabulary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `BlogsEntry`
--

DROP TABLE IF EXISTS `BlogsEntry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BlogsEntry` (
  `uuid_` varchar(75) DEFAULT NULL,
  `entryId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `title` varchar(150) DEFAULT NULL,
  `urlTitle` varchar(150) DEFAULT NULL,
  `content` longtext,
  `displayDate` datetime DEFAULT NULL,
  `allowPingbacks` tinyint(4) DEFAULT NULL,
  `allowTrackbacks` tinyint(4) DEFAULT NULL,
  `trackbacks` longtext,
  `status` int(11) DEFAULT NULL,
  `statusByUserId` bigint(20) DEFAULT NULL,
  `statusByUserName` varchar(75) DEFAULT NULL,
  `statusDate` datetime DEFAULT NULL,
  PRIMARY KEY (`entryId`),
  UNIQUE KEY `IX_DB780A20` (`groupId`,`urlTitle`),
  UNIQUE KEY `IX_1B1040FD` (`uuid_`,`groupId`),
  KEY `IX_72EF6041` (`companyId`),
  KEY `IX_430D791F` (`companyId`,`displayDate`),
  KEY `IX_BB0C2905` (`companyId`,`displayDate`,`status`),
  KEY `IX_EB2DCE27` (`companyId`,`status`),
  KEY `IX_8CACE77B` (`companyId`,`userId`),
  KEY `IX_A5F57B61` (`companyId`,`userId`,`status`),
  KEY `IX_81A50303` (`groupId`),
  KEY `IX_621E19D` (`groupId`,`displayDate`),
  KEY `IX_F0E73383` (`groupId`,`displayDate`,`status`),
  KEY `IX_1EFD8EE9` (`groupId`,`status`),
  KEY `IX_FBDE0AA3` (`groupId`,`userId`,`displayDate`),
  KEY `IX_DA04F689` (`groupId`,`userId`,`displayDate`,`status`),
  KEY `IX_49E15A23` (`groupId`,`userId`,`status`),
  KEY `IX_69157A4D` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BlogsEntry`
--

LOCK TABLES `BlogsEntry` WRITE;
/*!40000 ALTER TABLE `BlogsEntry` DISABLE KEYS */;
/*!40000 ALTER TABLE `BlogsEntry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `BlogsStatsUser`
--

DROP TABLE IF EXISTS `BlogsStatsUser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BlogsStatsUser` (
  `statsUserId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `entryCount` int(11) DEFAULT NULL,
  `lastPostDate` datetime DEFAULT NULL,
  `ratingsTotalEntries` int(11) DEFAULT NULL,
  `ratingsTotalScore` double DEFAULT NULL,
  `ratingsAverageScore` double DEFAULT NULL,
  PRIMARY KEY (`statsUserId`),
  UNIQUE KEY `IX_82254C25` (`groupId`,`userId`),
  KEY `IX_90CDA39A` (`companyId`,`entryCount`),
  KEY `IX_43840EEB` (`groupId`),
  KEY `IX_28C78D5C` (`groupId`,`entryCount`),
  KEY `IX_BB51F1D9` (`userId`),
  KEY `IX_507BA031` (`userId`,`lastPostDate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BlogsStatsUser`
--

LOCK TABLES `BlogsStatsUser` WRITE;
/*!40000 ALTER TABLE `BlogsStatsUser` DISABLE KEYS */;
/*!40000 ALTER TABLE `BlogsStatsUser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `BookmarksEntry`
--

DROP TABLE IF EXISTS `BookmarksEntry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BookmarksEntry` (
  `uuid_` varchar(75) DEFAULT NULL,
  `entryId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `folderId` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `url` longtext,
  `comments` longtext,
  `visits` int(11) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`entryId`),
  UNIQUE KEY `IX_EAA02A91` (`uuid_`,`groupId`),
  KEY `IX_E52FF7EF` (`groupId`),
  KEY `IX_5200100C` (`groupId`,`folderId`),
  KEY `IX_E2E9F129` (`groupId`,`userId`),
  KEY `IX_B670BA39` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BookmarksEntry`
--

LOCK TABLES `BookmarksEntry` WRITE;
/*!40000 ALTER TABLE `BookmarksEntry` DISABLE KEYS */;
/*!40000 ALTER TABLE `BookmarksEntry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `BookmarksFolder`
--

DROP TABLE IF EXISTS `BookmarksFolder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BookmarksFolder` (
  `uuid_` varchar(75) DEFAULT NULL,
  `folderId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `parentFolderId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  PRIMARY KEY (`folderId`),
  UNIQUE KEY `IX_DC2F8927` (`uuid_`,`groupId`),
  KEY `IX_2ABA25D7` (`companyId`),
  KEY `IX_7F703619` (`groupId`),
  KEY `IX_967799C0` (`groupId`,`parentFolderId`),
  KEY `IX_451E7AE3` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BookmarksFolder`
--

LOCK TABLES `BookmarksFolder` WRITE;
/*!40000 ALTER TABLE `BookmarksFolder` DISABLE KEYS */;
/*!40000 ALTER TABLE `BookmarksFolder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `BrowserTracker`
--

DROP TABLE IF EXISTS `BrowserTracker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BrowserTracker` (
  `browserTrackerId` bigint(20) NOT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `browserKey` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`browserTrackerId`),
  UNIQUE KEY `IX_E7B95510` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `BrowserTracker`
--

LOCK TABLES `BrowserTracker` WRITE;
/*!40000 ALTER TABLE `BrowserTracker` DISABLE KEYS */;
/*!40000 ALTER TABLE `BrowserTracker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CalEvent`
--

DROP TABLE IF EXISTS `CalEvent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CalEvent` (
  `uuid_` varchar(75) DEFAULT NULL,
  `eventId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `title` varchar(75) DEFAULT NULL,
  `description` longtext,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `durationHour` int(11) DEFAULT NULL,
  `durationMinute` int(11) DEFAULT NULL,
  `allDay` tinyint(4) DEFAULT NULL,
  `timeZoneSensitive` tinyint(4) DEFAULT NULL,
  `type_` varchar(75) DEFAULT NULL,
  `repeating` tinyint(4) DEFAULT NULL,
  `recurrence` longtext,
  `remindBy` int(11) DEFAULT NULL,
  `firstReminder` int(11) DEFAULT NULL,
  `secondReminder` int(11) DEFAULT NULL,
  PRIMARY KEY (`eventId`),
  UNIQUE KEY `IX_5CCE79C8` (`uuid_`,`groupId`),
  KEY `IX_D6FD9496` (`companyId`),
  KEY `IX_12EE4898` (`groupId`),
  KEY `IX_4FDDD2BF` (`groupId`,`repeating`),
  KEY `IX_FCD7C63D` (`groupId`,`type_`),
  KEY `IX_F6006202` (`remindBy`),
  KEY `IX_C1AD2122` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CalEvent`
--

LOCK TABLES `CalEvent` WRITE;
/*!40000 ALTER TABLE `CalEvent` DISABLE KEYS */;
/*!40000 ALTER TABLE `CalEvent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ClassName_`
--

DROP TABLE IF EXISTS `ClassName_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ClassName_` (
  `classNameId` bigint(20) NOT NULL,
  `value` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`classNameId`),
  UNIQUE KEY `IX_B27A301F` (`value`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ClassName_`
--

LOCK TABLES `ClassName_` WRITE;
/*!40000 ALTER TABLE `ClassName_` DISABLE KEYS */;
INSERT INTO `ClassName_` VALUES (10001,'com.liferay.counter.model.Counter'),(10002,'com.liferay.portal.kernel.workflow.WorkflowTask'),(10003,'com.liferay.portal.model.Account'),(10004,'com.liferay.portal.model.Address'),(10005,'com.liferay.portal.model.BrowserTracker'),(10006,'com.liferay.portal.model.ClassName'),(10007,'com.liferay.portal.model.ClusterGroup'),(10008,'com.liferay.portal.model.Company'),(10009,'com.liferay.portal.model.Contact'),(10010,'com.liferay.portal.model.Country'),(10011,'com.liferay.portal.model.EmailAddress'),(10012,'com.liferay.portal.model.Group'),(10013,'com.liferay.portal.model.Image'),(10014,'com.liferay.portal.model.Layout'),(10015,'com.liferay.portal.model.LayoutPrototype'),(10016,'com.liferay.portal.model.LayoutSet'),(10017,'com.liferay.portal.model.LayoutSetPrototype'),(10018,'com.liferay.portal.model.ListType'),(10019,'com.liferay.portal.model.Lock'),(10020,'com.liferay.portal.model.MembershipRequest'),(10024,'com.liferay.portal.model.Organization'),(10021,'com.liferay.portal.model.OrgGroupPermission'),(10022,'com.liferay.portal.model.OrgGroupRole'),(10023,'com.liferay.portal.model.OrgLabor'),(10025,'com.liferay.portal.model.PasswordPolicy'),(10026,'com.liferay.portal.model.PasswordPolicyRel'),(10027,'com.liferay.portal.model.PasswordTracker'),(10028,'com.liferay.portal.model.Permission'),(10029,'com.liferay.portal.model.Phone'),(10030,'com.liferay.portal.model.PluginSetting'),(10031,'com.liferay.portal.model.Portlet'),(10032,'com.liferay.portal.model.PortletItem'),(10033,'com.liferay.portal.model.PortletPreferences'),(10034,'com.liferay.portal.model.Region'),(10035,'com.liferay.portal.model.Release'),(10036,'com.liferay.portal.model.Resource'),(10037,'com.liferay.portal.model.ResourceAction'),(10038,'com.liferay.portal.model.ResourceCode'),(10039,'com.liferay.portal.model.ResourcePermission'),(10040,'com.liferay.portal.model.Role'),(10041,'com.liferay.portal.model.ServiceComponent'),(10042,'com.liferay.portal.model.Shard'),(10043,'com.liferay.portal.model.Subscription'),(10044,'com.liferay.portal.model.Team'),(10045,'com.liferay.portal.model.Ticket'),(10046,'com.liferay.portal.model.User'),(10047,'com.liferay.portal.model.UserGroup'),(10048,'com.liferay.portal.model.UserGroupGroupRole'),(10049,'com.liferay.portal.model.UserGroupRole'),(10050,'com.liferay.portal.model.UserIdMapper'),(10051,'com.liferay.portal.model.UserTracker'),(10052,'com.liferay.portal.model.UserTrackerPath'),(10053,'com.liferay.portal.model.WebDAVProps'),(10054,'com.liferay.portal.model.Website'),(10055,'com.liferay.portal.model.WorkflowDefinitionLink'),(10056,'com.liferay.portal.model.WorkflowInstanceLink'),(10057,'com.liferay.portlet.announcements.model.AnnouncementsDelivery'),(10058,'com.liferay.portlet.announcements.model.AnnouncementsEntry'),(10059,'com.liferay.portlet.announcements.model.AnnouncementsFlag'),(10060,'com.liferay.portlet.asset.model.AssetCategory'),(10061,'com.liferay.portlet.asset.model.AssetCategoryProperty'),(10062,'com.liferay.portlet.asset.model.AssetEntry'),(10063,'com.liferay.portlet.asset.model.AssetLink'),(10064,'com.liferay.portlet.asset.model.AssetTag'),(10065,'com.liferay.portlet.asset.model.AssetTagProperty'),(10066,'com.liferay.portlet.asset.model.AssetTagStats'),(10067,'com.liferay.portlet.asset.model.AssetVocabulary'),(10068,'com.liferay.portlet.blogs.model.BlogsEntry'),(10069,'com.liferay.portlet.blogs.model.BlogsStatsUser'),(10070,'com.liferay.portlet.bookmarks.model.BookmarksEntry'),(10071,'com.liferay.portlet.bookmarks.model.BookmarksFolder'),(10072,'com.liferay.portlet.calendar.model.CalEvent'),(10073,'com.liferay.portlet.documentlibrary.model.DLFileEntry'),(10074,'com.liferay.portlet.documentlibrary.model.DLFileRank'),(10075,'com.liferay.portlet.documentlibrary.model.DLFileShortcut'),(10076,'com.liferay.portlet.documentlibrary.model.DLFileVersion'),(10077,'com.liferay.portlet.documentlibrary.model.DLFolder'),(10078,'com.liferay.portlet.expando.model.ExpandoColumn'),(10079,'com.liferay.portlet.expando.model.ExpandoRow'),(10080,'com.liferay.portlet.expando.model.ExpandoTable'),(10081,'com.liferay.portlet.expando.model.ExpandoValue'),(10082,'com.liferay.portlet.imagegallery.model.IGFolder'),(10083,'com.liferay.portlet.imagegallery.model.IGImage'),(10084,'com.liferay.portlet.journal.model.JournalArticle'),(10085,'com.liferay.portlet.journal.model.JournalArticleImage'),(10086,'com.liferay.portlet.journal.model.JournalArticleResource'),(10087,'com.liferay.portlet.journal.model.JournalContentSearch'),(10088,'com.liferay.portlet.journal.model.JournalFeed'),(10089,'com.liferay.portlet.journal.model.JournalStructure'),(10090,'com.liferay.portlet.journal.model.JournalTemplate'),(10091,'com.liferay.portlet.messageboards.model.MBBan'),(10092,'com.liferay.portlet.messageboards.model.MBCategory'),(10093,'com.liferay.portlet.messageboards.model.MBDiscussion'),(10094,'com.liferay.portlet.messageboards.model.MBMailingList'),(10095,'com.liferay.portlet.messageboards.model.MBMessage'),(10096,'com.liferay.portlet.messageboards.model.MBMessageFlag'),(10097,'com.liferay.portlet.messageboards.model.MBStatsUser'),(10098,'com.liferay.portlet.messageboards.model.MBThread'),(10099,'com.liferay.portlet.polls.model.PollsChoice'),(10100,'com.liferay.portlet.polls.model.PollsQuestion'),(10101,'com.liferay.portlet.polls.model.PollsVote'),(10102,'com.liferay.portlet.ratings.model.RatingsEntry'),(10103,'com.liferay.portlet.ratings.model.RatingsStats'),(10104,'com.liferay.portlet.shopping.model.ShoppingCart'),(10105,'com.liferay.portlet.shopping.model.ShoppingCategory'),(10106,'com.liferay.portlet.shopping.model.ShoppingCoupon'),(10107,'com.liferay.portlet.shopping.model.ShoppingItem'),(10108,'com.liferay.portlet.shopping.model.ShoppingItemField'),(10109,'com.liferay.portlet.shopping.model.ShoppingItemPrice'),(10110,'com.liferay.portlet.shopping.model.ShoppingOrder'),(10111,'com.liferay.portlet.shopping.model.ShoppingOrderItem'),(10112,'com.liferay.portlet.social.model.SocialActivity'),(10113,'com.liferay.portlet.social.model.SocialEquityAssetEntry'),(10114,'com.liferay.portlet.social.model.SocialEquityGroupSetting'),(10115,'com.liferay.portlet.social.model.SocialEquityHistory'),(10116,'com.liferay.portlet.social.model.SocialEquityLog'),(10117,'com.liferay.portlet.social.model.SocialEquitySetting'),(10118,'com.liferay.portlet.social.model.SocialEquityUser'),(10119,'com.liferay.portlet.social.model.SocialRelation'),(10120,'com.liferay.portlet.social.model.SocialRequest'),(10121,'com.liferay.portlet.softwarecatalog.model.SCFrameworkVersion'),(10122,'com.liferay.portlet.softwarecatalog.model.SCLicense'),(10123,'com.liferay.portlet.softwarecatalog.model.SCProductEntry'),(10124,'com.liferay.portlet.softwarecatalog.model.SCProductScreenshot'),(10125,'com.liferay.portlet.softwarecatalog.model.SCProductVersion'),(10126,'com.liferay.portlet.tasks.model.TasksProposal'),(10127,'com.liferay.portlet.tasks.model.TasksReview'),(10128,'com.liferay.portlet.wiki.model.WikiNode'),(10129,'com.liferay.portlet.wiki.model.WikiPage'),(10130,'com.liferay.portlet.wiki.model.WikiPageResource');
/*!40000 ALTER TABLE `ClassName_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ClusterGroup`
--

DROP TABLE IF EXISTS `ClusterGroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ClusterGroup` (
  `clusterGroupId` bigint(20) NOT NULL,
  `name` varchar(75) DEFAULT NULL,
  `clusterNodeIds` varchar(75) DEFAULT NULL,
  `wholeCluster` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`clusterGroupId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ClusterGroup`
--

LOCK TABLES `ClusterGroup` WRITE;
/*!40000 ALTER TABLE `ClusterGroup` DISABLE KEYS */;
/*!40000 ALTER TABLE `ClusterGroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Company`
--

DROP TABLE IF EXISTS `Company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Company` (
  `companyId` bigint(20) NOT NULL,
  `accountId` bigint(20) DEFAULT NULL,
  `webId` varchar(75) DEFAULT NULL,
  `key_` longtext,
  `virtualHost` varchar(75) DEFAULT NULL,
  `mx` varchar(75) DEFAULT NULL,
  `homeURL` longtext,
  `logoId` bigint(20) DEFAULT NULL,
  `system` tinyint(4) DEFAULT NULL,
  `maxUsers` int(11) DEFAULT NULL,
  PRIMARY KEY (`companyId`),
  UNIQUE KEY `IX_975996C0` (`virtualHost`),
  UNIQUE KEY `IX_EC00543C` (`webId`),
  KEY `IX_38EFE3FD` (`logoId`),
  KEY `IX_12566EC2` (`mx`),
  KEY `IX_35E3E7C6` (`system`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Company`
--

LOCK TABLES `Company` WRITE;
/*!40000 ALTER TABLE `Company` DISABLE KEYS */;
INSERT INTO `Company` VALUES (10132,10134,'liferay.com','rO0ABXNyABRqYXZhLnNlY3VyaXR5LktleVJlcL35T7OImqVDAgAETAAJYWxnb3JpdGhtdAASTGphdmEvbGFuZy9TdHJpbmc7WwAHZW5jb2RlZHQAAltCTAAGZm9ybWF0cQB+AAFMAAR0eXBldAAbTGphdmEvc2VjdXJpdHkvS2V5UmVwJFR5cGU7eHB0AANERVN1cgACW0Ks8xf4BghU4AIAAHhwAAAACPd2/WuFT7l8dAADUkFXfnIAGWphdmEuc2VjdXJpdHkuS2V5UmVwJFR5cGUAAAAAAAAAABIAAHhyAA5qYXZhLmxhbmcuRW51bQAAAAAAAAAAEgAAeHB0AAZTRUNSRVQ=','localhost','liferay.com','',0,0,0);
/*!40000 ALTER TABLE `Company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Contact_`
--

DROP TABLE IF EXISTS `Contact_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Contact_` (
  `contactId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `accountId` bigint(20) DEFAULT NULL,
  `parentContactId` bigint(20) DEFAULT NULL,
  `firstName` varchar(75) DEFAULT NULL,
  `middleName` varchar(75) DEFAULT NULL,
  `lastName` varchar(75) DEFAULT NULL,
  `prefixId` int(11) DEFAULT NULL,
  `suffixId` int(11) DEFAULT NULL,
  `male` tinyint(4) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `smsSn` varchar(75) DEFAULT NULL,
  `aimSn` varchar(75) DEFAULT NULL,
  `facebookSn` varchar(75) DEFAULT NULL,
  `icqSn` varchar(75) DEFAULT NULL,
  `jabberSn` varchar(75) DEFAULT NULL,
  `msnSn` varchar(75) DEFAULT NULL,
  `mySpaceSn` varchar(75) DEFAULT NULL,
  `skypeSn` varchar(75) DEFAULT NULL,
  `twitterSn` varchar(75) DEFAULT NULL,
  `ymSn` varchar(75) DEFAULT NULL,
  `employeeStatusId` varchar(75) DEFAULT NULL,
  `employeeNumber` varchar(75) DEFAULT NULL,
  `jobTitle` varchar(100) DEFAULT NULL,
  `jobClass` varchar(75) DEFAULT NULL,
  `hoursOfOperation` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`contactId`),
  KEY `IX_66D496A3` (`companyId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Contact_`
--

LOCK TABLES `Contact_` WRITE;
/*!40000 ALTER TABLE `Contact_` DISABLE KEYS */;
INSERT INTO `Contact_` VALUES (10136,10132,10135,'','2011-07-01 20:41:14','2011-07-01 20:41:14',10134,0,'','','',0,0,1,'2011-07-01 20:41:14','','','','','','','','','','','','','','',''),(10170,10132,10169,'','2011-07-01 20:41:17','2011-07-01 20:41:17',10134,0,'Test','','Test',0,0,1,'1970-01-01 00:00:00','','','','','','','','','','','','','','','');
/*!40000 ALTER TABLE `Contact_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Counter`
--

DROP TABLE IF EXISTS `Counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Counter` (
  `name` varchar(75) NOT NULL,
  `currentId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Counter`
--

LOCK TABLES `Counter` WRITE;
/*!40000 ALTER TABLE `Counter` DISABLE KEYS */;
INSERT INTO `Counter` VALUES ('com.liferay.counter.model.Counter',10500),('com.liferay.portal.model.Layout#10149#true',1),('com.liferay.portal.model.Layout#10157#false',1),('com.liferay.portal.model.Layout#10171#false',1),('com.liferay.portal.model.Layout#10171#true',1),('com.liferay.portal.model.Permission',100),('com.liferay.portal.model.Resource',100),('com.liferay.portal.model.ResourceAction',800),('com.liferay.portal.model.ResourcePermission',500);
/*!40000 ALTER TABLE `Counter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Country`
--

DROP TABLE IF EXISTS `Country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Country` (
  `countryId` bigint(20) NOT NULL,
  `name` varchar(75) DEFAULT NULL,
  `a2` varchar(75) DEFAULT NULL,
  `a3` varchar(75) DEFAULT NULL,
  `number_` varchar(75) DEFAULT NULL,
  `idd_` varchar(75) DEFAULT NULL,
  `active_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`countryId`),
  UNIQUE KEY `IX_717B97E1` (`a2`),
  UNIQUE KEY `IX_717B9BA2` (`a3`),
  UNIQUE KEY `IX_19DA007B` (`name`),
  KEY `IX_25D734CD` (`active_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Country`
--

LOCK TABLES `Country` WRITE;
/*!40000 ALTER TABLE `Country` DISABLE KEYS */;
INSERT INTO `Country` VALUES (1,'Canada','CA','CAN','124','001',1),(2,'China','CN','CHN','156','086',1),(3,'France','FR','FRA','250','033',1),(4,'Germany','DE','DEU','276','049',1),(5,'Hong Kong','HK','HKG','344','852',1),(6,'Hungary','HU','HUN','348','036',1),(7,'Israel','IL','ISR','376','972',1),(8,'Italy','IT','ITA','380','039',1),(9,'Japan','JP','JPN','392','081',1),(10,'South Korea','KR','KOR','410','082',1),(11,'Netherlands','NL','NLD','528','031',1),(12,'Portugal','PT','PRT','620','351',1),(13,'Russia','RU','RUS','643','007',1),(14,'Singapore','SG','SGP','702','065',1),(15,'Spain','ES','ESP','724','034',1),(16,'Turkey','TR','TUR','792','090',1),(17,'Vietnam','VM','VNM','704','084',1),(18,'United Kingdom','GB','GBR','826','044',1),(19,'United States','US','USA','840','001',1),(20,'Afghanistan','AF','AFG','4','093',1),(21,'Albania','AL','ALB','8','355',1),(22,'Algeria','DZ','DZA','12','213',1),(23,'American Samoa','AS','ASM','16','684',1),(24,'Andorra','AD','AND','20','376',1),(25,'Angola','AO','AGO','24','244',1),(26,'Anguilla','AI','AIA','660','264',1),(27,'Antarctica','AQ','ATA','10','672',1),(28,'Antigua','AG','ATG','28','268',1),(29,'Argentina','AR','ARG','32','054',1),(30,'Armenia','AM','ARM','51','374',1),(31,'Aruba','AW','ABW','533','297',1),(32,'Australia','AU','AUS','36','061',1),(33,'Austria','AT','AUT','40','043',1),(34,'Azerbaijan','AZ','AZE','31','994',1),(35,'Bahamas','BS','BHS','44','242',1),(36,'Bahrain','BH','BHR','48','973',1),(37,'Bangladesh','BD','BGD','50','880',1),(38,'Barbados','BB','BRB','52','246',1),(39,'Belarus','BY','BLR','112','375',1),(40,'Belgium','BE','BEL','56','032',1),(41,'Belize','BZ','BLZ','84','501',1),(42,'Benin','BJ','BEN','204','229',1),(43,'Bermuda','BM','BMU','60','441',1),(44,'Bhutan','BT','BTN','64','975',1),(45,'Bolivia','BO','BOL','68','591',1),(46,'Bosnia-Herzegovina','BA','BIH','70','387',1),(47,'Botswana','BW','BWA','72','267',1),(48,'Brazil','BR','BRA','76','055',1),(49,'British Virgin Islands','VG','VGB','92','284',1),(50,'Brunei','BN','BRN','96','673',1),(51,'Bulgaria','BG','BGR','100','359',1),(52,'Burkina Faso','BF','BFA','854','226',1),(53,'Burma (Myanmar)','MM','MMR','104','095',1),(54,'Burundi','BI','BDI','108','257',1),(55,'Cambodia','KH','KHM','116','855',1),(56,'Cameroon','CM','CMR','120','237',1),(57,'Cape Verde Island','CV','CPV','132','238',1),(58,'Cayman Islands','KY','CYM','136','345',1),(59,'Central African Republic','CF','CAF','140','236',1),(60,'Chad','TD','TCD','148','235',1),(61,'Chile','CL','CHL','152','056',1),(62,'Christmas Island','CX','CXR','162','061',1),(63,'Cocos Islands','CC','CCK','166','061',1),(64,'Colombia','CO','COL','170','057',1),(65,'Comoros','KM','COM','174','269',1),(66,'Republic of Congo','CD','COD','180','242',1),(67,'Democratic Republic of Congo','CG','COG','178','243',1),(68,'Cook Islands','CK','COK','184','682',1),(69,'Costa Rica','CR','CRI','188','506',1),(70,'Croatia','HR','HRV','191','385',1),(71,'Cuba','CU','CUB','192','053',1),(72,'Cyprus','CY','CYP','196','357',1),(73,'Czech Republic','CZ','CZE','203','420',1),(74,'Denmark','DK','DNK','208','045',1),(75,'Djibouti','DJ','DJI','262','253',1),(76,'Dominica','DM','DMA','212','767',1),(77,'Dominican Republic','DO','DOM','214','809',1),(78,'Ecuador','EC','ECU','218','593',1),(79,'Egypt','EG','EGY','818','020',1),(80,'El Salvador','SV','SLV','222','503',1),(81,'Equatorial Guinea','GQ','GNQ','226','240',1),(82,'Eritrea','ER','ERI','232','291',1),(83,'Estonia','EE','EST','233','372',1),(84,'Ethiopia','ET','ETH','231','251',1),(85,'Faeroe Islands','FO','FRO','234','298',1),(86,'Falkland Islands','FK','FLK','238','500',1),(87,'Fiji Islands','FJ','FJI','242','679',1),(88,'Finland','FI','FIN','246','358',1),(89,'French Guiana','GF','GUF','254','594',1),(90,'French Polynesia','PF','PYF','258','689',1),(91,'Gabon','GA','GAB','266','241',1),(92,'Gambia','GM','GMB','270','220',1),(93,'Georgia','GE','GEO','268','995',1),(94,'Ghana','GH','GHA','288','233',1),(95,'Gibraltar','GI','GIB','292','350',1),(96,'Greece','GR','GRC','300','030',1),(97,'Greenland','GL','GRL','304','299',1),(98,'Grenada','GD','GRD','308','473',1),(99,'Guadeloupe','GP','GLP','312','590',1),(100,'Guam','GU','GUM','316','671',1),(101,'Guatemala','GT','GTM','320','502',1),(102,'Guinea','GN','GIN','324','224',1),(103,'Guinea-Bissau','GW','GNB','624','245',1),(104,'Guyana','GY','GUY','328','592',1),(105,'Haiti','HT','HTI','332','509',1),(106,'Honduras','HN','HND','340','504',1),(107,'Iceland','IS','ISL','352','354',1),(108,'India','IN','IND','356','091',1),(109,'Indonesia','ID','IDN','360','062',1),(110,'Iran','IR','IRN','364','098',1),(111,'Iraq','IQ','IRQ','368','964',1),(112,'Ireland','IE','IRL','372','353',1),(113,'Ivory Coast','CI','CIV','384','225',1),(114,'Jamaica','JM','JAM','388','876',1),(115,'Jordan','JO','JOR','400','962',1),(116,'Kazakhstan','KZ','KAZ','398','007',1),(117,'Kenya','KE','KEN','404','254',1),(118,'Kiribati','KI','KIR','408','686',1),(119,'Kuwait','KW','KWT','414','965',1),(120,'North Korea','KP','PRK','408','850',1),(121,'Kyrgyzstan','KG','KGZ','471','996',1),(122,'Laos','LA','LAO','418','856',1),(123,'Latvia','LV','LVA','428','371',1),(124,'Lebanon','LB','LBN','422','961',1),(125,'Lesotho','LS','LSO','426','266',1),(126,'Liberia','LR','LBR','430','231',1),(127,'Libya','LY','LBY','434','218',1),(128,'Liechtenstein','LI','LIE','438','423',1),(129,'Lithuania','LT','LTU','440','370',1),(130,'Luxembourg','LU','LUX','442','352',1),(131,'Macau','MO','MAC','446','853',1),(132,'Macedonia','MK','MKD','807','389',1),(133,'Madagascar','MG','MDG','450','261',1),(134,'Malawi','MW','MWI','454','265',1),(135,'Malaysia','MY','MYS','458','060',1),(136,'Maldives','MV','MDV','462','960',1),(137,'Mali','ML','MLI','466','223',1),(138,'Malta','MT','MLT','470','356',1),(139,'Marshall Islands','MH','MHL','584','692',1),(140,'Martinique','MQ','MTQ','474','596',1),(141,'Mauritania','MR','MRT','478','222',1),(142,'Mauritius','MU','MUS','480','230',1),(143,'Mayotte Island','YT','MYT','175','269',1),(144,'Mexico','MX','MEX','484','052',1),(145,'Micronesia','FM','FSM','583','691',1),(146,'Moldova','MD','MDA','498','373',1),(147,'Monaco','MC','MCO','492','377',1),(148,'Mongolia','MN','MNG','496','976',1),(149,'Montenegro','ME','MNE','499','382',1),(150,'Montserrat','MS','MSR','500','664',1),(151,'Morocco','MA','MAR','504','212',1),(152,'Mozambique','MZ','MOZ','508','258',1),(153,'Namibia','NA','NAM','516','264',1),(154,'Nauru','NR','NRU','520','674',1),(155,'Nepal','NP','NPL','524','977',1),(156,'Netherlands Antilles','AN','ANT','530','599',1),(157,'New Caledonia','NC','NCL','540','687',1),(158,'New Zealand','NZ','NZL','554','064',1),(159,'Nicaragua','NI','NIC','558','505',1),(160,'Niger','NE','NER','562','227',1),(161,'Nigeria','NG','NGA','566','234',1),(162,'Niue','NU','NIU','570','683',1),(163,'Norfolk Island','NF','NFK','574','672',1),(164,'Norway','NO','NOR','578','047',1),(165,'Oman','OM','OMN','512','968',1),(166,'Pakistan','PK','PAK','586','092',1),(167,'Palau','PW','PLW','585','680',1),(168,'Palestine','PS','PSE','275','970',1),(169,'Panama','PA','PAN','591','507',1),(170,'Papua New Guinea','PG','PNG','598','675',1),(171,'Paraguay','PY','PRY','600','595',1),(172,'Peru','PE','PER','604','051',1),(173,'Philippines','PH','PHL','608','063',1),(174,'Poland','PL','POL','616','048',1),(175,'Puerto Rico','PR','PRI','630','787',1),(176,'Qatar','QA','QAT','634','974',1),(177,'Reunion Island','RE','REU','638','262',1),(178,'Romania','RO','ROU','642','040',1),(179,'Rwanda','RW','RWA','646','250',1),(180,'St. Helena','SH','SHN','654','290',1),(181,'St. Kitts','KN','KNA','659','869',1),(182,'St. Lucia','LC','LCA','662','758',1),(183,'St. Pierre & Miquelon','PM','SPM','666','508',1),(184,'St. Vincent','VC','VCT','670','784',1),(185,'San Marino','SM','SMR','674','378',1),(186,'Sao Tome & Principe','ST','STP','678','239',1),(187,'Saudi Arabia','SA','SAU','682','966',1),(188,'Senegal','SN','SEN','686','221',1),(189,'Serbia','RS','SRB','688','381',1),(190,'Seychelles','SC','SYC','690','248',1),(191,'Sierra Leone','SL','SLE','694','249',1),(192,'Slovakia','SK','SVK','703','421',1),(193,'Slovenia','SI','SVN','705','386',1),(194,'Solomon Islands','SB','SLB','90','677',1),(195,'Somalia','SO','SOM','706','252',1),(196,'South Africa','ZA','ZAF','710','027',1),(197,'Sri Lanka','LK','LKA','144','094',1),(198,'Sudan','SD','SDN','736','095',1),(199,'Suriname','SR','SUR','740','597',1),(200,'Swaziland','SZ','SWZ','748','268',1),(201,'Sweden','SE','SWE','752','046',1),(202,'Switzerland','CH','CHE','756','041',1),(203,'Syria','SY','SYR','760','963',1),(204,'Taiwan','TW','TWN','158','886',1),(205,'Tajikistan','TJ','TJK','762','992',1),(206,'Tanzania','TZ','TZA','834','255',1),(207,'Thailand','TH','THA','764','066',1),(208,'Togo','TG','TGO','768','228',1),(209,'Tonga','TO','TON','776','676',1),(210,'Trinidad & Tobago','TT','TTO','780','868',1),(211,'Tunisia','TN','TUN','788','216',1),(212,'Turkmenistan','TM','TKM','795','993',1),(213,'Turks & Caicos','TC','TCA','796','649',1),(214,'Tuvalu','TV','TUV','798','688',1),(215,'Uganda','UG','UGA','800','256',1),(216,'Ukraine','UA','UKR','804','380',1),(217,'United Arab Emirates','AE','ARE','784','971',1),(218,'Uruguay','UY','URY','858','598',1),(219,'Uzbekistan','UZ','UZB','860','998',1),(220,'Vanuatu','VU','VUT','548','678',1),(221,'Vatican City','VA','VAT','336','039',1),(222,'Venezuela','VE','VEN','862','058',1),(223,'Wallis & Futuna','WF','WLF','876','681',1),(224,'Western Samoa','EH','ESH','732','685',1),(225,'Yemen','YE','YEM','887','967',1),(226,'Zambia','ZM','ZMB','894','260',1),(227,'Zimbabwe','ZW','ZWE','716','263',1);
/*!40000 ALTER TABLE `Country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CyrusUser`
--

DROP TABLE IF EXISTS `CyrusUser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CyrusUser` (
  `userId` varchar(75) NOT NULL,
  `password_` varchar(75) NOT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CyrusUser`
--

LOCK TABLES `CyrusUser` WRITE;
/*!40000 ALTER TABLE `CyrusUser` DISABLE KEYS */;
/*!40000 ALTER TABLE `CyrusUser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CyrusVirtual`
--

DROP TABLE IF EXISTS `CyrusVirtual`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CyrusVirtual` (
  `emailAddress` varchar(75) NOT NULL,
  `userId` varchar(75) NOT NULL,
  PRIMARY KEY (`emailAddress`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CyrusVirtual`
--

LOCK TABLES `CyrusVirtual` WRITE;
/*!40000 ALTER TABLE `CyrusVirtual` DISABLE KEYS */;
/*!40000 ALTER TABLE `CyrusVirtual` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DLFileEntry`
--

DROP TABLE IF EXISTS `DLFileEntry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DLFileEntry` (
  `uuid_` varchar(75) DEFAULT NULL,
  `fileEntryId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `versionUserId` bigint(20) DEFAULT NULL,
  `versionUserName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `folderId` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `extension` varchar(75) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` longtext,
  `extraSettings` longtext,
  `version` varchar(75) DEFAULT NULL,
  `size_` bigint(20) DEFAULT NULL,
  `readCount` int(11) DEFAULT NULL,
  PRIMARY KEY (`fileEntryId`),
  UNIQUE KEY `IX_5391712` (`groupId`,`folderId`,`name`),
  UNIQUE KEY `IX_ED5CA615` (`groupId`,`folderId`,`title`),
  UNIQUE KEY `IX_BC2E7E6A` (`uuid_`,`groupId`),
  KEY `IX_4CB1B2B4` (`companyId`),
  KEY `IX_F4AF5636` (`groupId`),
  KEY `IX_93CF8193` (`groupId`,`folderId`),
  KEY `IX_43261870` (`groupId`,`userId`),
  KEY `IX_D20C434D` (`groupId`,`userId`,`folderId`),
  KEY `IX_64F0FE40` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DLFileEntry`
--

LOCK TABLES `DLFileEntry` WRITE;
/*!40000 ALTER TABLE `DLFileEntry` DISABLE KEYS */;
/*!40000 ALTER TABLE `DLFileEntry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DLFileRank`
--

DROP TABLE IF EXISTS `DLFileRank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DLFileRank` (
  `fileRankId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `folderId` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`fileRankId`),
  UNIQUE KEY `IX_CE705D48` (`companyId`,`userId`,`folderId`,`name`),
  KEY `IX_40B56512` (`folderId`,`name`),
  KEY `IX_BAFB116E` (`groupId`,`userId`),
  KEY `IX_EED06670` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DLFileRank`
--

LOCK TABLES `DLFileRank` WRITE;
/*!40000 ALTER TABLE `DLFileRank` DISABLE KEYS */;
/*!40000 ALTER TABLE `DLFileRank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DLFileShortcut`
--

DROP TABLE IF EXISTS `DLFileShortcut`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DLFileShortcut` (
  `uuid_` varchar(75) DEFAULT NULL,
  `fileShortcutId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `folderId` bigint(20) DEFAULT NULL,
  `toFolderId` bigint(20) DEFAULT NULL,
  `toName` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `statusByUserId` bigint(20) DEFAULT NULL,
  `statusByUserName` varchar(75) DEFAULT NULL,
  `statusDate` datetime DEFAULT NULL,
  PRIMARY KEY (`fileShortcutId`),
  UNIQUE KEY `IX_FDB4A946` (`uuid_`,`groupId`),
  KEY `IX_B0051937` (`groupId`,`folderId`),
  KEY `IX_ECCE311D` (`groupId`,`folderId`,`status`),
  KEY `IX_55C736AC` (`groupId`,`toFolderId`,`toName`),
  KEY `IX_346A0992` (`groupId`,`toFolderId`,`toName`,`status`),
  KEY `IX_4831EBE4` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DLFileShortcut`
--

LOCK TABLES `DLFileShortcut` WRITE;
/*!40000 ALTER TABLE `DLFileShortcut` DISABLE KEYS */;
/*!40000 ALTER TABLE `DLFileShortcut` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DLFileVersion`
--

DROP TABLE IF EXISTS `DLFileVersion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DLFileVersion` (
  `fileVersionId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `folderId` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `extension` varchar(75) DEFAULT NULL,
  `title` varchar(75) DEFAULT NULL,
  `description` longtext,
  `changeLog` varchar(75) DEFAULT NULL,
  `extraSettings` varchar(75) DEFAULT NULL,
  `version` varchar(75) DEFAULT NULL,
  `size_` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `statusByUserId` bigint(20) DEFAULT NULL,
  `statusByUserName` varchar(75) DEFAULT NULL,
  `statusDate` datetime DEFAULT NULL,
  PRIMARY KEY (`fileVersionId`),
  UNIQUE KEY `IX_2F8FED9C` (`groupId`,`folderId`,`name`,`version`),
  KEY `IX_B413F1EC` (`groupId`,`folderId`,`name`),
  KEY `IX_94E784D2` (`groupId`,`folderId`,`name`,`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DLFileVersion`
--

LOCK TABLES `DLFileVersion` WRITE;
/*!40000 ALTER TABLE `DLFileVersion` DISABLE KEYS */;
/*!40000 ALTER TABLE `DLFileVersion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DLFolder`
--

DROP TABLE IF EXISTS `DLFolder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DLFolder` (
  `uuid_` varchar(75) DEFAULT NULL,
  `folderId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `parentFolderId` bigint(20) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` longtext,
  `lastPostDate` datetime DEFAULT NULL,
  PRIMARY KEY (`folderId`),
  UNIQUE KEY `IX_902FD874` (`groupId`,`parentFolderId`,`name`),
  UNIQUE KEY `IX_3CC1DED2` (`uuid_`,`groupId`),
  KEY `IX_A74DB14C` (`companyId`),
  KEY `IX_F2EA1ACE` (`groupId`),
  KEY `IX_49C37475` (`groupId`,`parentFolderId`),
  KEY `IX_51556082` (`parentFolderId`,`name`),
  KEY `IX_CBC408D8` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DLFolder`
--

LOCK TABLES `DLFolder` WRITE;
/*!40000 ALTER TABLE `DLFolder` DISABLE KEYS */;
/*!40000 ALTER TABLE `DLFolder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EmailAddress`
--

DROP TABLE IF EXISTS `EmailAddress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EmailAddress` (
  `emailAddressId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `address` varchar(75) DEFAULT NULL,
  `typeId` int(11) DEFAULT NULL,
  `primary_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`emailAddressId`),
  KEY `IX_1BB072CA` (`companyId`),
  KEY `IX_49D2DEC4` (`companyId`,`classNameId`),
  KEY `IX_551A519F` (`companyId`,`classNameId`,`classPK`),
  KEY `IX_2A2CB130` (`companyId`,`classNameId`,`classPK`,`primary_`),
  KEY `IX_7B43CD8` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EmailAddress`
--

LOCK TABLES `EmailAddress` WRITE;
/*!40000 ALTER TABLE `EmailAddress` DISABLE KEYS */;
/*!40000 ALTER TABLE `EmailAddress` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ExpandoColumn`
--

DROP TABLE IF EXISTS `ExpandoColumn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ExpandoColumn` (
  `columnId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `tableId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `type_` int(11) DEFAULT NULL,
  `defaultData` longtext,
  `typeSettings` longtext,
  PRIMARY KEY (`columnId`),
  UNIQUE KEY `IX_FEFC8DA7` (`tableId`,`name`),
  KEY `IX_A8C0CBE8` (`tableId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ExpandoColumn`
--

LOCK TABLES `ExpandoColumn` WRITE;
/*!40000 ALTER TABLE `ExpandoColumn` DISABLE KEYS */;
/*!40000 ALTER TABLE `ExpandoColumn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ExpandoRow`
--

DROP TABLE IF EXISTS `ExpandoRow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ExpandoRow` (
  `rowId_` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `tableId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`rowId_`),
  UNIQUE KEY `IX_81EFBFF5` (`tableId`,`classPK`),
  KEY `IX_D3F5D7AE` (`tableId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ExpandoRow`
--

LOCK TABLES `ExpandoRow` WRITE;
/*!40000 ALTER TABLE `ExpandoRow` DISABLE KEYS */;
/*!40000 ALTER TABLE `ExpandoRow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ExpandoTable`
--

DROP TABLE IF EXISTS `ExpandoTable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ExpandoTable` (
  `tableId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`tableId`),
  UNIQUE KEY `IX_37562284` (`companyId`,`classNameId`,`name`),
  KEY `IX_B5AE8A85` (`companyId`,`classNameId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ExpandoTable`
--

LOCK TABLES `ExpandoTable` WRITE;
/*!40000 ALTER TABLE `ExpandoTable` DISABLE KEYS */;
/*!40000 ALTER TABLE `ExpandoTable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ExpandoValue`
--

DROP TABLE IF EXISTS `ExpandoValue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ExpandoValue` (
  `valueId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `tableId` bigint(20) DEFAULT NULL,
  `columnId` bigint(20) DEFAULT NULL,
  `rowId_` bigint(20) DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `data_` longtext,
  PRIMARY KEY (`valueId`),
  UNIQUE KEY `IX_9DDD21E5` (`columnId`,`rowId_`),
  UNIQUE KEY `IX_D27B03E7` (`tableId`,`columnId`,`classPK`),
  KEY `IX_B29FEF17` (`classNameId`,`classPK`),
  KEY `IX_F7DD0987` (`columnId`),
  KEY `IX_9112A7A0` (`rowId_`),
  KEY `IX_F0566A77` (`tableId`),
  KEY `IX_1BD3F4C` (`tableId`,`classPK`),
  KEY `IX_CA9AFB7C` (`tableId`,`columnId`),
  KEY `IX_B71E92D5` (`tableId`,`rowId_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ExpandoValue`
--

LOCK TABLES `ExpandoValue` WRITE;
/*!40000 ALTER TABLE `ExpandoValue` DISABLE KEYS */;
/*!40000 ALTER TABLE `ExpandoValue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Group_`
--

DROP TABLE IF EXISTS `Group_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Group_` (
  `groupId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `creatorUserId` bigint(20) DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `parentGroupId` bigint(20) DEFAULT NULL,
  `liveGroupId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  `type_` int(11) DEFAULT NULL,
  `typeSettings` longtext,
  `friendlyURL` varchar(100) DEFAULT NULL,
  `active_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`groupId`),
  UNIQUE KEY `IX_D0D5E397` (`companyId`,`classNameId`,`classPK`),
  UNIQUE KEY `IX_5DE0BE11` (`companyId`,`classNameId`,`liveGroupId`,`name`),
  UNIQUE KEY `IX_5BDDB872` (`companyId`,`friendlyURL`),
  UNIQUE KEY `IX_BBCA55B` (`companyId`,`liveGroupId`,`name`),
  UNIQUE KEY `IX_5AA68501` (`companyId`,`name`),
  KEY `IX_ABA5CEC2` (`companyId`),
  KEY `IX_16218A38` (`liveGroupId`),
  KEY `IX_7B590A7A` (`type_`,`active_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Group_`
--

LOCK TABLES `Group_` WRITE;
/*!40000 ALTER TABLE `Group_` DISABLE KEYS */;
INSERT INTO `Group_` VALUES (10149,10132,10135,10012,10149,0,0,'Control Panel','',3,'','/control_panel',1),(10157,10132,10135,10012,10157,0,0,'Guest','',1,'','/guest',1),(10165,10132,10135,10008,10132,0,0,'10132','',0,'','/null',1),(10171,10132,10169,10046,10169,0,0,'10169','',0,'','/test',1);
/*!40000 ALTER TABLE `Group_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Groups_Orgs`
--

DROP TABLE IF EXISTS `Groups_Orgs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Groups_Orgs` (
  `groupId` bigint(20) NOT NULL,
  `organizationId` bigint(20) NOT NULL,
  PRIMARY KEY (`groupId`,`organizationId`),
  KEY `IX_75267DCA` (`groupId`),
  KEY `IX_6BBB7682` (`organizationId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Groups_Orgs`
--

LOCK TABLES `Groups_Orgs` WRITE;
/*!40000 ALTER TABLE `Groups_Orgs` DISABLE KEYS */;
/*!40000 ALTER TABLE `Groups_Orgs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Groups_Permissions`
--

DROP TABLE IF EXISTS `Groups_Permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Groups_Permissions` (
  `groupId` bigint(20) NOT NULL,
  `permissionId` bigint(20) NOT NULL,
  PRIMARY KEY (`groupId`,`permissionId`),
  KEY `IX_C48736B` (`groupId`),
  KEY `IX_EC97689D` (`permissionId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Groups_Permissions`
--

LOCK TABLES `Groups_Permissions` WRITE;
/*!40000 ALTER TABLE `Groups_Permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `Groups_Permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Groups_Roles`
--

DROP TABLE IF EXISTS `Groups_Roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Groups_Roles` (
  `groupId` bigint(20) NOT NULL,
  `roleId` bigint(20) NOT NULL,
  PRIMARY KEY (`groupId`,`roleId`),
  KEY `IX_84471FD2` (`groupId`),
  KEY `IX_3103EF3D` (`roleId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Groups_Roles`
--

LOCK TABLES `Groups_Roles` WRITE;
/*!40000 ALTER TABLE `Groups_Roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `Groups_Roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Groups_UserGroups`
--

DROP TABLE IF EXISTS `Groups_UserGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Groups_UserGroups` (
  `groupId` bigint(20) NOT NULL,
  `userGroupId` bigint(20) NOT NULL,
  PRIMARY KEY (`groupId`,`userGroupId`),
  KEY `IX_31FB749A` (`groupId`),
  KEY `IX_3B69160F` (`userGroupId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Groups_UserGroups`
--

LOCK TABLES `Groups_UserGroups` WRITE;
/*!40000 ALTER TABLE `Groups_UserGroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `Groups_UserGroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `IGFolder`
--

DROP TABLE IF EXISTS `IGFolder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IGFolder` (
  `uuid_` varchar(75) DEFAULT NULL,
  `folderId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `parentFolderId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  PRIMARY KEY (`folderId`),
  UNIQUE KEY `IX_9BBAFB1E` (`groupId`,`parentFolderId`,`name`),
  UNIQUE KEY `IX_B10EFD68` (`uuid_`,`groupId`),
  KEY `IX_60214CF6` (`companyId`),
  KEY `IX_206498F8` (`groupId`),
  KEY `IX_1A605E9F` (`groupId`,`parentFolderId`),
  KEY `IX_F73C0982` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `IGFolder`
--

LOCK TABLES `IGFolder` WRITE;
/*!40000 ALTER TABLE `IGFolder` DISABLE KEYS */;
/*!40000 ALTER TABLE `IGFolder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `IGImage`
--

DROP TABLE IF EXISTS `IGImage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IGImage` (
  `uuid_` varchar(75) DEFAULT NULL,
  `imageId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `folderId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  `smallImageId` bigint(20) DEFAULT NULL,
  `largeImageId` bigint(20) DEFAULT NULL,
  `custom1ImageId` bigint(20) DEFAULT NULL,
  `custom2ImageId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`imageId`),
  UNIQUE KEY `IX_E97342D9` (`uuid_`,`groupId`),
  KEY `IX_E597322D` (`custom1ImageId`),
  KEY `IX_D9E0A34C` (`custom2ImageId`),
  KEY `IX_63820A7` (`groupId`),
  KEY `IX_8956B2C4` (`groupId`,`folderId`),
  KEY `IX_AAE8DF83` (`groupId`,`folderId`,`name`),
  KEY `IX_BE79E1E1` (`groupId`,`userId`),
  KEY `IX_64F0B572` (`largeImageId`),
  KEY `IX_D3D32126` (`smallImageId`),
  KEY `IX_265BB0F1` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `IGImage`
--

LOCK TABLES `IGImage` WRITE;
/*!40000 ALTER TABLE `IGImage` DISABLE KEYS */;
/*!40000 ALTER TABLE `IGImage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Image`
--

DROP TABLE IF EXISTS `Image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Image` (
  `imageId` bigint(20) NOT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `text_` longtext,
  `type_` varchar(75) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `size_` int(11) DEFAULT NULL,
  PRIMARY KEY (`imageId`),
  KEY `IX_6A925A4D` (`size_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Image`
--

LOCK TABLES `Image` WRITE;
/*!40000 ALTER TABLE `Image` DISABLE KEYS */;
/*!40000 ALTER TABLE `Image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `JournalArticle`
--

DROP TABLE IF EXISTS `JournalArticle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `JournalArticle` (
  `uuid_` varchar(75) DEFAULT NULL,
  `id_` bigint(20) NOT NULL,
  `resourcePrimKey` bigint(20) DEFAULT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `articleId` varchar(75) DEFAULT NULL,
  `version` double DEFAULT NULL,
  `title` varchar(300) DEFAULT NULL,
  `urlTitle` varchar(150) DEFAULT NULL,
  `description` longtext,
  `content` longtext,
  `type_` varchar(75) DEFAULT NULL,
  `structureId` varchar(75) DEFAULT NULL,
  `templateId` varchar(75) DEFAULT NULL,
  `displayDate` datetime DEFAULT NULL,
  `expirationDate` datetime DEFAULT NULL,
  `reviewDate` datetime DEFAULT NULL,
  `indexable` tinyint(4) DEFAULT NULL,
  `smallImage` tinyint(4) DEFAULT NULL,
  `smallImageId` bigint(20) DEFAULT NULL,
  `smallImageURL` longtext,
  `status` int(11) DEFAULT NULL,
  `statusByUserId` bigint(20) DEFAULT NULL,
  `statusByUserName` varchar(75) DEFAULT NULL,
  `statusDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id_`),
  UNIQUE KEY `IX_85C52EEC` (`groupId`,`articleId`,`version`),
  UNIQUE KEY `IX_3463D95B` (`uuid_`,`groupId`),
  KEY `IX_DFF98523` (`companyId`),
  KEY `IX_323DF109` (`companyId`,`status`),
  KEY `IX_9356F865` (`groupId`),
  KEY `IX_68C0F69C` (`groupId`,`articleId`),
  KEY `IX_4D5CD982` (`groupId`,`articleId`,`status`),
  KEY `IX_301D024B` (`groupId`,`status`),
  KEY `IX_2E207659` (`groupId`,`structureId`),
  KEY `IX_8DEAE14E` (`groupId`,`templateId`),
  KEY `IX_22882D02` (`groupId`,`urlTitle`),
  KEY `IX_D2D249E8` (`groupId`,`urlTitle`,`status`),
  KEY `IX_33F49D16` (`resourcePrimKey`),
  KEY `IX_3E2765FC` (`resourcePrimKey`,`status`),
  KEY `IX_EF9B7028` (`smallImageId`),
  KEY `IX_F029602F` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `JournalArticle`
--

LOCK TABLES `JournalArticle` WRITE;
/*!40000 ALTER TABLE `JournalArticle` DISABLE KEYS */;
/*!40000 ALTER TABLE `JournalArticle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `JournalArticleImage`
--

DROP TABLE IF EXISTS `JournalArticleImage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `JournalArticleImage` (
  `articleImageId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `articleId` varchar(75) DEFAULT NULL,
  `version` double DEFAULT NULL,
  `elInstanceId` varchar(75) DEFAULT NULL,
  `elName` varchar(75) DEFAULT NULL,
  `languageId` varchar(75) DEFAULT NULL,
  `tempImage` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`articleImageId`),
  UNIQUE KEY `IX_103D6207` (`groupId`,`articleId`,`version`,`elInstanceId`,`elName`,`languageId`),
  KEY `IX_3B51BB68` (`groupId`),
  KEY `IX_158B526F` (`groupId`,`articleId`,`version`),
  KEY `IX_D4121315` (`tempImage`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `JournalArticleImage`
--

LOCK TABLES `JournalArticleImage` WRITE;
/*!40000 ALTER TABLE `JournalArticleImage` DISABLE KEYS */;
/*!40000 ALTER TABLE `JournalArticleImage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `JournalArticleResource`
--

DROP TABLE IF EXISTS `JournalArticleResource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `JournalArticleResource` (
  `uuid_` varchar(75) DEFAULT NULL,
  `resourcePrimKey` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `articleId` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`resourcePrimKey`),
  UNIQUE KEY `IX_88DF994A` (`groupId`,`articleId`),
  UNIQUE KEY `IX_84AB0309` (`uuid_`,`groupId`),
  KEY `IX_F8433677` (`groupId`),
  KEY `IX_DCD1FAC1` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `JournalArticleResource`
--

LOCK TABLES `JournalArticleResource` WRITE;
/*!40000 ALTER TABLE `JournalArticleResource` DISABLE KEYS */;
/*!40000 ALTER TABLE `JournalArticleResource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `JournalContentSearch`
--

DROP TABLE IF EXISTS `JournalContentSearch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `JournalContentSearch` (
  `contentSearchId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `privateLayout` tinyint(4) DEFAULT NULL,
  `layoutId` bigint(20) DEFAULT NULL,
  `portletId` varchar(200) DEFAULT NULL,
  `articleId` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`contentSearchId`),
  UNIQUE KEY `IX_C3AA93B8` (`groupId`,`privateLayout`,`layoutId`,`portletId`,`articleId`),
  KEY `IX_9207CB31` (`articleId`),
  KEY `IX_6838E427` (`groupId`,`articleId`),
  KEY `IX_20962903` (`groupId`,`privateLayout`),
  KEY `IX_7CC7D73E` (`groupId`,`privateLayout`,`articleId`),
  KEY `IX_B3B318DC` (`groupId`,`privateLayout`,`layoutId`),
  KEY `IX_7ACC74C9` (`groupId`,`privateLayout`,`layoutId`,`portletId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `JournalContentSearch`
--

LOCK TABLES `JournalContentSearch` WRITE;
/*!40000 ALTER TABLE `JournalContentSearch` DISABLE KEYS */;
/*!40000 ALTER TABLE `JournalContentSearch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `JournalFeed`
--

DROP TABLE IF EXISTS `JournalFeed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `JournalFeed` (
  `uuid_` varchar(75) DEFAULT NULL,
  `id_` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `feedId` varchar(75) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  `type_` varchar(75) DEFAULT NULL,
  `structureId` varchar(75) DEFAULT NULL,
  `templateId` varchar(75) DEFAULT NULL,
  `rendererTemplateId` varchar(75) DEFAULT NULL,
  `delta` int(11) DEFAULT NULL,
  `orderByCol` varchar(75) DEFAULT NULL,
  `orderByType` varchar(75) DEFAULT NULL,
  `targetLayoutFriendlyUrl` varchar(255) DEFAULT NULL,
  `targetPortletId` varchar(75) DEFAULT NULL,
  `contentField` varchar(75) DEFAULT NULL,
  `feedType` varchar(75) DEFAULT NULL,
  `feedVersion` double DEFAULT NULL,
  PRIMARY KEY (`id_`),
  UNIQUE KEY `IX_65576CBC` (`groupId`,`feedId`),
  UNIQUE KEY `IX_39031F51` (`uuid_`,`groupId`),
  KEY `IX_35A2DB2F` (`groupId`),
  KEY `IX_50C36D79` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `JournalFeed`
--

LOCK TABLES `JournalFeed` WRITE;
/*!40000 ALTER TABLE `JournalFeed` DISABLE KEYS */;
/*!40000 ALTER TABLE `JournalFeed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `JournalStructure`
--

DROP TABLE IF EXISTS `JournalStructure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `JournalStructure` (
  `uuid_` varchar(75) DEFAULT NULL,
  `id_` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `structureId` varchar(75) DEFAULT NULL,
  `parentStructureId` varchar(75) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  `xsd` longtext,
  PRIMARY KEY (`id_`),
  UNIQUE KEY `IX_AB6E9996` (`groupId`,`structureId`),
  UNIQUE KEY `IX_42E86E58` (`uuid_`,`groupId`),
  KEY `IX_B97F5608` (`groupId`),
  KEY `IX_CA0BD48C` (`groupId`,`parentStructureId`),
  KEY `IX_8831E4FC` (`structureId`),
  KEY `IX_6702CA92` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `JournalStructure`
--

LOCK TABLES `JournalStructure` WRITE;
/*!40000 ALTER TABLE `JournalStructure` DISABLE KEYS */;
/*!40000 ALTER TABLE `JournalStructure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `JournalTemplate`
--

DROP TABLE IF EXISTS `JournalTemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `JournalTemplate` (
  `uuid_` varchar(75) DEFAULT NULL,
  `id_` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `templateId` varchar(75) DEFAULT NULL,
  `structureId` varchar(75) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  `xsl` longtext,
  `langType` varchar(75) DEFAULT NULL,
  `cacheable` tinyint(4) DEFAULT NULL,
  `smallImage` tinyint(4) DEFAULT NULL,
  `smallImageId` bigint(20) DEFAULT NULL,
  `smallImageURL` longtext,
  PRIMARY KEY (`id_`),
  UNIQUE KEY `IX_E802AA3C` (`groupId`,`templateId`),
  UNIQUE KEY `IX_62D1B3AD` (`uuid_`,`groupId`),
  KEY `IX_77923653` (`groupId`),
  KEY `IX_1701CB2B` (`groupId`,`structureId`),
  KEY `IX_25FFB6FA` (`smallImageId`),
  KEY `IX_1B12CA20` (`templateId`),
  KEY `IX_2857419D` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `JournalTemplate`
--

LOCK TABLES `JournalTemplate` WRITE;
/*!40000 ALTER TABLE `JournalTemplate` DISABLE KEYS */;
/*!40000 ALTER TABLE `JournalTemplate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Layout`
--

DROP TABLE IF EXISTS `Layout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Layout` (
  `uuid_` varchar(75) DEFAULT NULL,
  `plid` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `privateLayout` tinyint(4) DEFAULT NULL,
  `layoutId` bigint(20) DEFAULT NULL,
  `parentLayoutId` bigint(20) DEFAULT NULL,
  `name` longtext,
  `title` longtext,
  `description` longtext,
  `type_` varchar(75) DEFAULT NULL,
  `typeSettings` longtext,
  `hidden_` tinyint(4) DEFAULT NULL,
  `friendlyURL` varchar(255) DEFAULT NULL,
  `iconImage` tinyint(4) DEFAULT NULL,
  `iconImageId` bigint(20) DEFAULT NULL,
  `themeId` varchar(75) DEFAULT NULL,
  `colorSchemeId` varchar(75) DEFAULT NULL,
  `wapThemeId` varchar(75) DEFAULT NULL,
  `wapColorSchemeId` varchar(75) DEFAULT NULL,
  `css` longtext,
  `priority` int(11) DEFAULT NULL,
  `layoutPrototypeId` bigint(20) DEFAULT NULL,
  `dlFolderId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`plid`),
  UNIQUE KEY `IX_BC2C4231` (`groupId`,`privateLayout`,`friendlyURL`),
  UNIQUE KEY `IX_7162C27C` (`groupId`,`privateLayout`,`layoutId`),
  UNIQUE KEY `IX_CED31606` (`uuid_`,`groupId`),
  KEY `IX_C7FBC998` (`companyId`),
  KEY `IX_FAD05595` (`dlFolderId`),
  KEY `IX_C099D61A` (`groupId`),
  KEY `IX_705F5AA3` (`groupId`,`privateLayout`),
  KEY `IX_6DE88B06` (`groupId`,`privateLayout`,`parentLayoutId`),
  KEY `IX_1A1B61D2` (`groupId`,`privateLayout`,`type_`),
  KEY `IX_23922F7D` (`iconImageId`),
  KEY `IX_D0822724` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Layout`
--

LOCK TABLES `Layout` WRITE;
/*!40000 ALTER TABLE `Layout` DISABLE KEYS */;
INSERT INTO `Layout` VALUES ('8dddfa9f-f3b4-4ae2-ab17-416e2a2fb0aa',10152,10149,10132,1,1,0,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US\" default-locale=\"en_US\"><name language-id=\"en_US\">Control Panel</name></root>','','','control_panel','',0,'/manage',0,0,'','','','','',0,0,0),('da0ababb-caaf-41ad-a327-9f25cc7ce42e',10160,10157,10132,0,1,0,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US\" default-locale=\"en_US\"><name language-id=\"en_US\">Welcome</name></root>','','','portlet','layout-template-id=2_columns_ii\ncolumn-2=Provision_WAR_prov,\ncolumn-1=provmenu_WAR_provmenu_INSTANCE_8Vt1\n',0,'/home',0,0,'provisioningtheme_WAR_provtheme','01','','','',0,0,0),('2ee554f6-6a4c-4c80-9db8-956333716023',10271,10171,10132,1,1,0,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US\" default-locale=\"en_US\"><name language-id=\"en_US\">Welcome</name></root>','','','portlet','layout-template-id=2_columns_ii\ncolumn-2=29,8,\ncolumn-1=82,23,11,\n',0,'/home',0,0,'','','','','',0,0,0),('a6836d02-98a2-4476-9b3b-cc991f646d0b',10276,10171,10132,0,1,0,'<?xml version=\'1.0\' encoding=\'UTF-8\'?><root available-locales=\"en_US\" default-locale=\"en_US\"><name language-id=\"en_US\">Welcome</name></root>','','','portlet','layout-template-id=2_columns_ii\ncolumn-2=33,\ncolumn-1=82,3,\n',0,'/home',0,0,'','','','','',0,0,0);
/*!40000 ALTER TABLE `Layout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LayoutPrototype`
--

DROP TABLE IF EXISTS `LayoutPrototype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LayoutPrototype` (
  `layoutPrototypeId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `name` longtext,
  `description` longtext,
  `settings_` longtext,
  `active_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`layoutPrototypeId`),
  KEY `IX_30616AAA` (`companyId`),
  KEY `IX_557A639F` (`companyId`,`active_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LayoutPrototype`
--

LOCK TABLES `LayoutPrototype` WRITE;
/*!40000 ALTER TABLE `LayoutPrototype` DISABLE KEYS */;
/*!40000 ALTER TABLE `LayoutPrototype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LayoutSet`
--

DROP TABLE IF EXISTS `LayoutSet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LayoutSet` (
  `layoutSetId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `privateLayout` tinyint(4) DEFAULT NULL,
  `logo` tinyint(4) DEFAULT NULL,
  `logoId` bigint(20) DEFAULT NULL,
  `themeId` varchar(75) DEFAULT NULL,
  `colorSchemeId` varchar(75) DEFAULT NULL,
  `wapThemeId` varchar(75) DEFAULT NULL,
  `wapColorSchemeId` varchar(75) DEFAULT NULL,
  `css` longtext,
  `pageCount` int(11) DEFAULT NULL,
  `virtualHost` varchar(75) DEFAULT NULL,
  `settings_` longtext,
  `layoutSetPrototypeId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`layoutSetId`),
  UNIQUE KEY `IX_48550691` (`groupId`,`privateLayout`),
  KEY `IX_A40B8BEC` (`groupId`),
  KEY `IX_5ABC2905` (`virtualHost`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LayoutSet`
--

LOCK TABLES `LayoutSet` WRITE;
/*!40000 ALTER TABLE `LayoutSet` DISABLE KEYS */;
INSERT INTO `LayoutSet` VALUES (10150,10149,10132,1,0,0,'classic','01','mobile','01','',1,'','',0),(10151,10149,10132,0,0,0,'classic','01','mobile','01','',0,'','',0),(10158,10157,10132,1,0,0,'classic','01','mobile','01','',0,'','',0),(10159,10157,10132,0,0,0,'classic','01','mobile','01','',1,'','',0),(10166,10165,10132,1,0,0,'classic','01','mobile','01','',0,'','',0),(10167,10165,10132,0,0,0,'classic','01','mobile','01','',0,'','',0),(10172,10171,10132,1,0,0,'classic','01','mobile','01','',1,'','',0),(10173,10171,10132,0,0,0,'classic','01','mobile','01','',1,'','',0);
/*!40000 ALTER TABLE `LayoutSet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LayoutSetPrototype`
--

DROP TABLE IF EXISTS `LayoutSetPrototype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LayoutSetPrototype` (
  `layoutSetPrototypeId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `name` longtext,
  `description` longtext,
  `settings_` longtext,
  `active_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`layoutSetPrototypeId`),
  KEY `IX_55F63D98` (`companyId`),
  KEY `IX_9178FC71` (`companyId`,`active_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LayoutSetPrototype`
--

LOCK TABLES `LayoutSetPrototype` WRITE;
/*!40000 ALTER TABLE `LayoutSetPrototype` DISABLE KEYS */;
/*!40000 ALTER TABLE `LayoutSetPrototype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ListType`
--

DROP TABLE IF EXISTS `ListType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ListType` (
  `listTypeId` int(11) NOT NULL,
  `name` varchar(75) DEFAULT NULL,
  `type_` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`listTypeId`),
  KEY `IX_2932DD37` (`type_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ListType`
--

LOCK TABLES `ListType` WRITE;
/*!40000 ALTER TABLE `ListType` DISABLE KEYS */;
INSERT INTO `ListType` VALUES (10000,'Billing','com.liferay.portal.model.Account.address'),(10001,'Other','com.liferay.portal.model.Account.address'),(10002,'P.O. Box','com.liferay.portal.model.Account.address'),(10003,'Shipping','com.liferay.portal.model.Account.address'),(10004,'E-mail','com.liferay.portal.model.Account.emailAddress'),(10005,'E-mail 2','com.liferay.portal.model.Account.emailAddress'),(10006,'E-mail 3','com.liferay.portal.model.Account.emailAddress'),(10007,'Fax','com.liferay.portal.model.Account.phone'),(10008,'Local','com.liferay.portal.model.Account.phone'),(10009,'Other','com.liferay.portal.model.Account.phone'),(10010,'Toll-Free','com.liferay.portal.model.Account.phone'),(10011,'TTY','com.liferay.portal.model.Account.phone'),(10012,'Intranet','com.liferay.portal.model.Account.website'),(10013,'Public','com.liferay.portal.model.Account.website'),(11000,'Business','com.liferay.portal.model.Contact.address'),(11001,'Other','com.liferay.portal.model.Contact.address'),(11002,'Personal','com.liferay.portal.model.Contact.address'),(11003,'E-mail','com.liferay.portal.model.Contact.emailAddress'),(11004,'E-mail 2','com.liferay.portal.model.Contact.emailAddress'),(11005,'E-mail 3','com.liferay.portal.model.Contact.emailAddress'),(11006,'Business','com.liferay.portal.model.Contact.phone'),(11007,'Business Fax','com.liferay.portal.model.Contact.phone'),(11008,'Mobile','com.liferay.portal.model.Contact.phone'),(11009,'Other','com.liferay.portal.model.Contact.phone'),(11010,'Pager','com.liferay.portal.model.Contact.phone'),(11011,'Personal','com.liferay.portal.model.Contact.phone'),(11012,'Personal Fax','com.liferay.portal.model.Contact.phone'),(11013,'TTY','com.liferay.portal.model.Contact.phone'),(11014,'Dr.','com.liferay.portal.model.Contact.prefix'),(11015,'Mr.','com.liferay.portal.model.Contact.prefix'),(11016,'Mrs.','com.liferay.portal.model.Contact.prefix'),(11017,'Ms.','com.liferay.portal.model.Contact.prefix'),(11020,'II','com.liferay.portal.model.Contact.suffix'),(11021,'III','com.liferay.portal.model.Contact.suffix'),(11022,'IV','com.liferay.portal.model.Contact.suffix'),(11023,'Jr.','com.liferay.portal.model.Contact.suffix'),(11024,'PhD.','com.liferay.portal.model.Contact.suffix'),(11025,'Sr.','com.liferay.portal.model.Contact.suffix'),(11026,'Blog','com.liferay.portal.model.Contact.website'),(11027,'Business','com.liferay.portal.model.Contact.website'),(11028,'Other','com.liferay.portal.model.Contact.website'),(11029,'Personal','com.liferay.portal.model.Contact.website'),(12000,'Billing','com.liferay.portal.model.Organization.address'),(12001,'Other','com.liferay.portal.model.Organization.address'),(12002,'P.O. Box','com.liferay.portal.model.Organization.address'),(12003,'Shipping','com.liferay.portal.model.Organization.address'),(12004,'E-mail','com.liferay.portal.model.Organization.emailAddress'),(12005,'E-mail 2','com.liferay.portal.model.Organization.emailAddress'),(12006,'E-mail 3','com.liferay.portal.model.Organization.emailAddress'),(12007,'Fax','com.liferay.portal.model.Organization.phone'),(12008,'Local','com.liferay.portal.model.Organization.phone'),(12009,'Other','com.liferay.portal.model.Organization.phone'),(12010,'Toll-Free','com.liferay.portal.model.Organization.phone'),(12011,'TTY','com.liferay.portal.model.Organization.phone'),(12012,'Administrative','com.liferay.portal.model.Organization.service'),(12013,'Contracts','com.liferay.portal.model.Organization.service'),(12014,'Donation','com.liferay.portal.model.Organization.service'),(12015,'Retail','com.liferay.portal.model.Organization.service'),(12016,'Training','com.liferay.portal.model.Organization.service'),(12017,'Full Member','com.liferay.portal.model.Organization.status'),(12018,'Provisional Member','com.liferay.portal.model.Organization.status'),(12019,'Intranet','com.liferay.portal.model.Organization.website'),(12020,'Public','com.liferay.portal.model.Organization.website');
/*!40000 ALTER TABLE `ListType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Lock_`
--

DROP TABLE IF EXISTS `Lock_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Lock_` (
  `uuid_` varchar(75) DEFAULT NULL,
  `lockId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `className` varchar(75) DEFAULT NULL,
  `key_` varchar(200) DEFAULT NULL,
  `owner` varchar(75) DEFAULT NULL,
  `inheritable` tinyint(4) DEFAULT NULL,
  `expirationDate` datetime DEFAULT NULL,
  PRIMARY KEY (`lockId`),
  KEY `IX_228562AD` (`className`,`key_`),
  KEY `IX_E3F1286B` (`expirationDate`),
  KEY `IX_13C5CD3A` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Lock_`
--

LOCK TABLES `Lock_` WRITE;
/*!40000 ALTER TABLE `Lock_` DISABLE KEYS */;
/*!40000 ALTER TABLE `Lock_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MBBan`
--

DROP TABLE IF EXISTS `MBBan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MBBan` (
  `banId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `banUserId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`banId`),
  UNIQUE KEY `IX_8ABC4E3B` (`groupId`,`banUserId`),
  KEY `IX_69951A25` (`banUserId`),
  KEY `IX_5C3FF12A` (`groupId`),
  KEY `IX_48814BBA` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MBBan`
--

LOCK TABLES `MBBan` WRITE;
/*!40000 ALTER TABLE `MBBan` DISABLE KEYS */;
/*!40000 ALTER TABLE `MBBan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MBCategory`
--

DROP TABLE IF EXISTS `MBCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MBCategory` (
  `uuid_` varchar(75) DEFAULT NULL,
  `categoryId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `parentCategoryId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  `threadCount` int(11) DEFAULT NULL,
  `messageCount` int(11) DEFAULT NULL,
  `lastPostDate` datetime DEFAULT NULL,
  PRIMARY KEY (`categoryId`),
  UNIQUE KEY `IX_F7D28C2F` (`uuid_`,`groupId`),
  KEY `IX_BC735DCF` (`companyId`),
  KEY `IX_BB870C11` (`groupId`),
  KEY `IX_ED292508` (`groupId`,`parentCategoryId`),
  KEY `IX_C2626EDB` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MBCategory`
--

LOCK TABLES `MBCategory` WRITE;
/*!40000 ALTER TABLE `MBCategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `MBCategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MBDiscussion`
--

DROP TABLE IF EXISTS `MBDiscussion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MBDiscussion` (
  `discussionId` bigint(20) NOT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `threadId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`discussionId`),
  UNIQUE KEY `IX_33A4DE38` (`classNameId`,`classPK`),
  UNIQUE KEY `IX_B5CA2DC` (`threadId`),
  KEY `IX_79D0120B` (`classNameId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MBDiscussion`
--

LOCK TABLES `MBDiscussion` WRITE;
/*!40000 ALTER TABLE `MBDiscussion` DISABLE KEYS */;
INSERT INTO `MBDiscussion` VALUES (10156,10014,10152,10154),(10164,10014,10160,10162),(10275,10014,10271,10273),(10280,10014,10276,10278);
/*!40000 ALTER TABLE `MBDiscussion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MBMailingList`
--

DROP TABLE IF EXISTS `MBMailingList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MBMailingList` (
  `uuid_` varchar(75) DEFAULT NULL,
  `mailingListId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `categoryId` bigint(20) DEFAULT NULL,
  `emailAddress` varchar(75) DEFAULT NULL,
  `inProtocol` varchar(75) DEFAULT NULL,
  `inServerName` varchar(75) DEFAULT NULL,
  `inServerPort` int(11) DEFAULT NULL,
  `inUseSSL` tinyint(4) DEFAULT NULL,
  `inUserName` varchar(75) DEFAULT NULL,
  `inPassword` varchar(75) DEFAULT NULL,
  `inReadInterval` int(11) DEFAULT NULL,
  `outEmailAddress` varchar(75) DEFAULT NULL,
  `outCustom` tinyint(4) DEFAULT NULL,
  `outServerName` varchar(75) DEFAULT NULL,
  `outServerPort` int(11) DEFAULT NULL,
  `outUseSSL` tinyint(4) DEFAULT NULL,
  `outUserName` varchar(75) DEFAULT NULL,
  `outPassword` varchar(75) DEFAULT NULL,
  `active_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`mailingListId`),
  UNIQUE KEY `IX_76CE9CDD` (`groupId`,`categoryId`),
  UNIQUE KEY `IX_E858F170` (`uuid_`,`groupId`),
  KEY `IX_BFEB984F` (`active_`),
  KEY `IX_4115EC7A` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MBMailingList`
--

LOCK TABLES `MBMailingList` WRITE;
/*!40000 ALTER TABLE `MBMailingList` DISABLE KEYS */;
/*!40000 ALTER TABLE `MBMailingList` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MBMessage`
--

DROP TABLE IF EXISTS `MBMessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MBMessage` (
  `uuid_` varchar(75) DEFAULT NULL,
  `messageId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `categoryId` bigint(20) DEFAULT NULL,
  `threadId` bigint(20) DEFAULT NULL,
  `rootMessageId` bigint(20) DEFAULT NULL,
  `parentMessageId` bigint(20) DEFAULT NULL,
  `subject` varchar(75) DEFAULT NULL,
  `body` longtext,
  `attachments` tinyint(4) DEFAULT NULL,
  `anonymous` tinyint(4) DEFAULT NULL,
  `priority` double DEFAULT NULL,
  `allowPingbacks` tinyint(4) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `statusByUserId` bigint(20) DEFAULT NULL,
  `statusByUserName` varchar(75) DEFAULT NULL,
  `statusDate` datetime DEFAULT NULL,
  PRIMARY KEY (`messageId`),
  UNIQUE KEY `IX_8D12316E` (`uuid_`,`groupId`),
  KEY `IX_51A8D44D` (`classNameId`,`classPK`),
  KEY `IX_F6687633` (`classNameId`,`classPK`,`status`),
  KEY `IX_B1432D30` (`companyId`),
  KEY `IX_1AD93C16` (`companyId`,`status`),
  KEY `IX_5B153FB2` (`groupId`),
  KEY `IX_1073AB9F` (`groupId`,`categoryId`),
  KEY `IX_4257DB85` (`groupId`,`categoryId`,`status`),
  KEY `IX_B674AB58` (`groupId`,`categoryId`,`threadId`),
  KEY `IX_385E123E` (`groupId`,`categoryId`,`threadId`,`status`),
  KEY `IX_ED39AC98` (`groupId`,`status`),
  KEY `IX_8EB8C5EC` (`groupId`,`userId`),
  KEY `IX_377858D2` (`groupId`,`userId`,`status`),
  KEY `IX_75B95071` (`threadId`),
  KEY `IX_A7038CD7` (`threadId`,`parentMessageId`),
  KEY `IX_9DC8E57` (`threadId`,`status`),
  KEY `IX_7A040C32` (`userId`),
  KEY `IX_C57B16BC` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MBMessage`
--

LOCK TABLES `MBMessage` WRITE;
/*!40000 ALTER TABLE `MBMessage` DISABLE KEYS */;
INSERT INTO `MBMessage` VALUES ('10a0bd46-ecc7-46df-9374-2b2d6ae33e88',10153,10149,10132,10135,' ','2011-07-01 20:41:16','2011-07-01 20:41:16',10014,10152,-1,10154,10153,0,'10152','10152',0,1,0,0,0,10135,' ','2011-07-01 20:41:16'),('d36ae8ff-2904-4f60-92ce-21ffaa967f39',10161,10157,10132,10135,' ','2011-07-01 20:41:17','2011-07-01 20:41:17',10014,10160,-1,10162,10161,0,'10160','10160',0,1,0,0,0,10135,' ','2011-07-01 20:41:17'),('94ccf957-319c-49cb-a1b0-f82108d3ad38',10272,10171,10132,10169,'Test Test','2011-07-01 20:42:55','2011-07-01 20:42:55',10014,10271,-1,10273,10272,0,'10271','10271',0,0,0,0,0,10169,'Test Test','2011-07-01 20:42:55'),('46b378fd-e67d-4bec-bf81-b194919fdb08',10277,10171,10132,10169,'Test Test','2011-07-01 20:42:55','2011-07-01 20:42:55',10014,10276,-1,10278,10277,0,'10276','10276',0,0,0,0,0,10169,'Test Test','2011-07-01 20:42:55');
/*!40000 ALTER TABLE `MBMessage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MBMessageFlag`
--

DROP TABLE IF EXISTS `MBMessageFlag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MBMessageFlag` (
  `messageFlagId` bigint(20) NOT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `threadId` bigint(20) DEFAULT NULL,
  `messageId` bigint(20) DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  PRIMARY KEY (`messageFlagId`),
  UNIQUE KEY `IX_E9EB6194` (`userId`,`messageId`,`flag`),
  KEY `IX_D180D4AE` (`messageId`),
  KEY `IX_A6973A8E` (`messageId`,`flag`),
  KEY `IX_C1C9A8FD` (`threadId`),
  KEY `IX_3CFD579D` (`threadId`,`flag`),
  KEY `IX_7B2917BE` (`userId`),
  KEY `IX_2EA537D7` (`userId`,`threadId`,`flag`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MBMessageFlag`
--

LOCK TABLES `MBMessageFlag` WRITE;
/*!40000 ALTER TABLE `MBMessageFlag` DISABLE KEYS */;
/*!40000 ALTER TABLE `MBMessageFlag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MBStatsUser`
--

DROP TABLE IF EXISTS `MBStatsUser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MBStatsUser` (
  `statsUserId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `messageCount` int(11) DEFAULT NULL,
  `lastPostDate` datetime DEFAULT NULL,
  PRIMARY KEY (`statsUserId`),
  UNIQUE KEY `IX_9168E2C9` (`groupId`,`userId`),
  KEY `IX_A00A898F` (`groupId`),
  KEY `IX_FAB5A88B` (`groupId`,`messageCount`),
  KEY `IX_847F92B5` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MBStatsUser`
--

LOCK TABLES `MBStatsUser` WRITE;
/*!40000 ALTER TABLE `MBStatsUser` DISABLE KEYS */;
/*!40000 ALTER TABLE `MBStatsUser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MBThread`
--

DROP TABLE IF EXISTS `MBThread`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MBThread` (
  `threadId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `categoryId` bigint(20) DEFAULT NULL,
  `rootMessageId` bigint(20) DEFAULT NULL,
  `messageCount` int(11) DEFAULT NULL,
  `viewCount` int(11) DEFAULT NULL,
  `lastPostByUserId` bigint(20) DEFAULT NULL,
  `lastPostDate` datetime DEFAULT NULL,
  `priority` double DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `statusByUserId` bigint(20) DEFAULT NULL,
  `statusByUserName` varchar(75) DEFAULT NULL,
  `statusDate` datetime DEFAULT NULL,
  PRIMARY KEY (`threadId`),
  KEY `IX_41F6DC8A` (`categoryId`,`priority`),
  KEY `IX_95C0EA45` (`groupId`),
  KEY `IX_9A2D11B2` (`groupId`,`categoryId`),
  KEY `IX_50F1904A` (`groupId`,`categoryId`,`lastPostDate`),
  KEY `IX_485F7E98` (`groupId`,`categoryId`,`status`),
  KEY `IX_E1E7142B` (`groupId`,`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MBThread`
--

LOCK TABLES `MBThread` WRITE;
/*!40000 ALTER TABLE `MBThread` DISABLE KEYS */;
INSERT INTO `MBThread` VALUES (10154,10149,-1,10153,1,0,0,'2011-07-01 20:41:16',0,0,10135,' ','2011-07-01 20:41:16'),(10162,10157,-1,10161,1,0,0,'2011-07-01 20:41:17',0,0,10135,' ','2011-07-01 20:41:17'),(10273,10171,-1,10272,1,0,10169,'2011-07-01 20:42:55',0,0,10169,'Test Test','2011-07-01 20:42:55'),(10278,10171,-1,10277,1,0,10169,'2011-07-01 20:42:55',0,0,10169,'Test Test','2011-07-01 20:42:55');
/*!40000 ALTER TABLE `MBThread` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MembershipRequest`
--

DROP TABLE IF EXISTS `MembershipRequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MembershipRequest` (
  `membershipRequestId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `comments` longtext,
  `replyComments` longtext,
  `replyDate` datetime DEFAULT NULL,
  `replierUserId` bigint(20) DEFAULT NULL,
  `statusId` int(11) DEFAULT NULL,
  PRIMARY KEY (`membershipRequestId`),
  KEY `IX_8A1CC4B` (`groupId`),
  KEY `IX_C28C72EC` (`groupId`,`statusId`),
  KEY `IX_66D70879` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MembershipRequest`
--

LOCK TABLES `MembershipRequest` WRITE;
/*!40000 ALTER TABLE `MembershipRequest` DISABLE KEYS */;
/*!40000 ALTER TABLE `MembershipRequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OrgGroupPermission`
--

DROP TABLE IF EXISTS `OrgGroupPermission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OrgGroupPermission` (
  `organizationId` bigint(20) NOT NULL,
  `groupId` bigint(20) NOT NULL,
  `permissionId` bigint(20) NOT NULL,
  PRIMARY KEY (`organizationId`,`groupId`,`permissionId`),
  KEY `IX_A425F71A` (`groupId`),
  KEY `IX_6C53DA4E` (`permissionId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OrgGroupPermission`
--

LOCK TABLES `OrgGroupPermission` WRITE;
/*!40000 ALTER TABLE `OrgGroupPermission` DISABLE KEYS */;
/*!40000 ALTER TABLE `OrgGroupPermission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OrgGroupRole`
--

DROP TABLE IF EXISTS `OrgGroupRole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OrgGroupRole` (
  `organizationId` bigint(20) NOT NULL,
  `groupId` bigint(20) NOT NULL,
  `roleId` bigint(20) NOT NULL,
  PRIMARY KEY (`organizationId`,`groupId`,`roleId`),
  KEY `IX_4A527DD3` (`groupId`),
  KEY `IX_AB044D1C` (`roleId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OrgGroupRole`
--

LOCK TABLES `OrgGroupRole` WRITE;
/*!40000 ALTER TABLE `OrgGroupRole` DISABLE KEYS */;
/*!40000 ALTER TABLE `OrgGroupRole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OrgLabor`
--

DROP TABLE IF EXISTS `OrgLabor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OrgLabor` (
  `orgLaborId` bigint(20) NOT NULL,
  `organizationId` bigint(20) DEFAULT NULL,
  `typeId` int(11) DEFAULT NULL,
  `sunOpen` int(11) DEFAULT NULL,
  `sunClose` int(11) DEFAULT NULL,
  `monOpen` int(11) DEFAULT NULL,
  `monClose` int(11) DEFAULT NULL,
  `tueOpen` int(11) DEFAULT NULL,
  `tueClose` int(11) DEFAULT NULL,
  `wedOpen` int(11) DEFAULT NULL,
  `wedClose` int(11) DEFAULT NULL,
  `thuOpen` int(11) DEFAULT NULL,
  `thuClose` int(11) DEFAULT NULL,
  `friOpen` int(11) DEFAULT NULL,
  `friClose` int(11) DEFAULT NULL,
  `satOpen` int(11) DEFAULT NULL,
  `satClose` int(11) DEFAULT NULL,
  PRIMARY KEY (`orgLaborId`),
  KEY `IX_6AF0D434` (`organizationId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OrgLabor`
--

LOCK TABLES `OrgLabor` WRITE;
/*!40000 ALTER TABLE `OrgLabor` DISABLE KEYS */;
/*!40000 ALTER TABLE `OrgLabor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Organization_`
--

DROP TABLE IF EXISTS `Organization_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Organization_` (
  `organizationId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `parentOrganizationId` bigint(20) DEFAULT NULL,
  `leftOrganizationId` bigint(20) DEFAULT NULL,
  `rightOrganizationId` bigint(20) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `type_` varchar(75) DEFAULT NULL,
  `recursable` tinyint(4) DEFAULT NULL,
  `regionId` bigint(20) DEFAULT NULL,
  `countryId` bigint(20) DEFAULT NULL,
  `statusId` int(11) DEFAULT NULL,
  `comments` longtext,
  PRIMARY KEY (`organizationId`),
  UNIQUE KEY `IX_E301BDF5` (`companyId`,`name`),
  KEY `IX_834BCEB6` (`companyId`),
  KEY `IX_418E4522` (`companyId`,`parentOrganizationId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Organization_`
--

LOCK TABLES `Organization_` WRITE;
/*!40000 ALTER TABLE `Organization_` DISABLE KEYS */;
/*!40000 ALTER TABLE `Organization_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PasswordPolicy`
--

DROP TABLE IF EXISTS `PasswordPolicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PasswordPolicy` (
  `passwordPolicyId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `defaultPolicy` tinyint(4) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  `changeable` tinyint(4) DEFAULT NULL,
  `changeRequired` tinyint(4) DEFAULT NULL,
  `minAge` bigint(20) DEFAULT NULL,
  `checkSyntax` tinyint(4) DEFAULT NULL,
  `allowDictionaryWords` tinyint(4) DEFAULT NULL,
  `minAlphanumeric` int(11) DEFAULT NULL,
  `minLength` int(11) DEFAULT NULL,
  `minLowerCase` int(11) DEFAULT NULL,
  `minNumbers` int(11) DEFAULT NULL,
  `minSymbols` int(11) DEFAULT NULL,
  `minUpperCase` int(11) DEFAULT NULL,
  `history` tinyint(4) DEFAULT NULL,
  `historyCount` int(11) DEFAULT NULL,
  `expireable` tinyint(4) DEFAULT NULL,
  `maxAge` bigint(20) DEFAULT NULL,
  `warningTime` bigint(20) DEFAULT NULL,
  `graceLimit` int(11) DEFAULT NULL,
  `lockout` tinyint(4) DEFAULT NULL,
  `maxFailure` int(11) DEFAULT NULL,
  `lockoutDuration` bigint(20) DEFAULT NULL,
  `requireUnlock` tinyint(4) DEFAULT NULL,
  `resetFailureCount` bigint(20) DEFAULT NULL,
  `resetTicketMaxAge` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`passwordPolicyId`),
  UNIQUE KEY `IX_3FBFA9F4` (`companyId`,`name`),
  KEY `IX_2C1142E` (`companyId`,`defaultPolicy`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PasswordPolicy`
--

LOCK TABLES `PasswordPolicy` WRITE;
/*!40000 ALTER TABLE `PasswordPolicy` DISABLE KEYS */;
INSERT INTO `PasswordPolicy` VALUES (10168,10132,10135,' ','2011-07-01 20:41:17','2011-07-01 20:41:17',1,'Default Password Policy','Default Password Policy',1,0,0,0,1,0,6,0,1,0,1,0,6,0,8640000,86400,0,0,3,0,1,600,86400);
/*!40000 ALTER TABLE `PasswordPolicy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PasswordPolicyRel`
--

DROP TABLE IF EXISTS `PasswordPolicyRel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PasswordPolicyRel` (
  `passwordPolicyRelId` bigint(20) NOT NULL,
  `passwordPolicyId` bigint(20) DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`passwordPolicyRelId`),
  KEY `IX_C3A17327` (`classNameId`,`classPK`),
  KEY `IX_CD25266E` (`passwordPolicyId`),
  KEY `IX_ED7CF243` (`passwordPolicyId`,`classNameId`,`classPK`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PasswordPolicyRel`
--

LOCK TABLES `PasswordPolicyRel` WRITE;
/*!40000 ALTER TABLE `PasswordPolicyRel` DISABLE KEYS */;
/*!40000 ALTER TABLE `PasswordPolicyRel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PasswordTracker`
--

DROP TABLE IF EXISTS `PasswordTracker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PasswordTracker` (
  `passwordTrackerId` bigint(20) NOT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `password_` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`passwordTrackerId`),
  KEY `IX_326F75BD` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PasswordTracker`
--

LOCK TABLES `PasswordTracker` WRITE;
/*!40000 ALTER TABLE `PasswordTracker` DISABLE KEYS */;
/*!40000 ALTER TABLE `PasswordTracker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Permission_`
--

DROP TABLE IF EXISTS `Permission_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Permission_` (
  `permissionId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `actionId` varchar(75) DEFAULT NULL,
  `resourceId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`permissionId`),
  UNIQUE KEY `IX_4D19C2B8` (`actionId`,`resourceId`),
  KEY `IX_F090C113` (`resourceId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Permission_`
--

LOCK TABLES `Permission_` WRITE;
/*!40000 ALTER TABLE `Permission_` DISABLE KEYS */;
/*!40000 ALTER TABLE `Permission_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Phone`
--

DROP TABLE IF EXISTS `Phone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Phone` (
  `phoneId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `number_` varchar(75) DEFAULT NULL,
  `extension` varchar(75) DEFAULT NULL,
  `typeId` int(11) DEFAULT NULL,
  `primary_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`phoneId`),
  KEY `IX_9F704A14` (`companyId`),
  KEY `IX_A2E4AFBA` (`companyId`,`classNameId`),
  KEY `IX_9A53569` (`companyId`,`classNameId`,`classPK`),
  KEY `IX_812CE07A` (`companyId`,`classNameId`,`classPK`,`primary_`),
  KEY `IX_F202B9CE` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Phone`
--

LOCK TABLES `Phone` WRITE;
/*!40000 ALTER TABLE `Phone` DISABLE KEYS */;
/*!40000 ALTER TABLE `Phone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PluginSetting`
--

DROP TABLE IF EXISTS `PluginSetting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PluginSetting` (
  `pluginSettingId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `pluginId` varchar(75) DEFAULT NULL,
  `pluginType` varchar(75) DEFAULT NULL,
  `roles` longtext,
  `active_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`pluginSettingId`),
  UNIQUE KEY `IX_7171B2E8` (`companyId`,`pluginId`,`pluginType`),
  KEY `IX_B9746445` (`companyId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PluginSetting`
--

LOCK TABLES `PluginSetting` WRITE;
/*!40000 ALTER TABLE `PluginSetting` DISABLE KEYS */;
/*!40000 ALTER TABLE `PluginSetting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PollsChoice`
--

DROP TABLE IF EXISTS `PollsChoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PollsChoice` (
  `uuid_` varchar(75) DEFAULT NULL,
  `choiceId` bigint(20) NOT NULL,
  `questionId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  PRIMARY KEY (`choiceId`),
  UNIQUE KEY `IX_D76DD2CF` (`questionId`,`name`),
  KEY `IX_EC370F10` (`questionId`),
  KEY `IX_6660B399` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PollsChoice`
--

LOCK TABLES `PollsChoice` WRITE;
/*!40000 ALTER TABLE `PollsChoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `PollsChoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PollsQuestion`
--

DROP TABLE IF EXISTS `PollsQuestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PollsQuestion` (
  `uuid_` varchar(75) DEFAULT NULL,
  `questionId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `title` longtext,
  `description` longtext,
  `expirationDate` datetime DEFAULT NULL,
  `lastVoteDate` datetime DEFAULT NULL,
  PRIMARY KEY (`questionId`),
  UNIQUE KEY `IX_F3C9F36` (`uuid_`,`groupId`),
  KEY `IX_9FF342EA` (`groupId`),
  KEY `IX_51F087F4` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PollsQuestion`
--

LOCK TABLES `PollsQuestion` WRITE;
/*!40000 ALTER TABLE `PollsQuestion` DISABLE KEYS */;
/*!40000 ALTER TABLE `PollsQuestion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PollsVote`
--

DROP TABLE IF EXISTS `PollsVote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PollsVote` (
  `voteId` bigint(20) NOT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `questionId` bigint(20) DEFAULT NULL,
  `choiceId` bigint(20) DEFAULT NULL,
  `voteDate` datetime DEFAULT NULL,
  PRIMARY KEY (`voteId`),
  UNIQUE KEY `IX_1BBFD4D3` (`questionId`,`userId`),
  KEY `IX_D5DF7B54` (`choiceId`),
  KEY `IX_12112599` (`questionId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PollsVote`
--

LOCK TABLES `PollsVote` WRITE;
/*!40000 ALTER TABLE `PollsVote` DISABLE KEYS */;
/*!40000 ALTER TABLE `PollsVote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Portlet`
--

DROP TABLE IF EXISTS `Portlet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Portlet` (
  `id_` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `portletId` varchar(200) DEFAULT NULL,
  `roles` longtext,
  `active_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_`),
  UNIQUE KEY `IX_12B5E51D` (`companyId`,`portletId`),
  KEY `IX_80CC9508` (`companyId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Portlet`
--

LOCK TABLES `Portlet` WRITE;
/*!40000 ALTER TABLE `Portlet` DISABLE KEYS */;
INSERT INTO `Portlet` VALUES (10175,10132,'98','',1),(10176,10132,'66','',1),(10177,10132,'27','',1),(10178,10132,'152','',1),(10179,10132,'134','',1),(10180,10132,'130','',1),(10181,10132,'122','',1),(10182,10132,'36','',1),(10183,10132,'26','',1),(10184,10132,'104','',1),(10185,10132,'153','',1),(10186,10132,'64','',1),(10187,10132,'129','',1),(10188,10132,'100','',1),(10189,10132,'19','',1),(10190,10132,'157','',1),(10191,10132,'128','',1),(10192,10132,'154','',1),(10193,10132,'148','',1),(10194,10132,'11','',1),(10195,10132,'29','',1),(10196,10132,'158','',1),(10197,10132,'8','',1),(10198,10132,'58','',1),(10199,10132,'155','',1),(10200,10132,'71','',1),(10201,10132,'97','',1),(10202,10132,'39','',1),(10203,10132,'85','',1),(10204,10132,'118','',1),(10205,10132,'79','',1),(10206,10132,'107','',1),(10207,10132,'30','',1),(10208,10132,'147','',1),(10209,10132,'48','',1),(10210,10132,'125','',1),(10211,10132,'146','',1),(10212,10132,'62','',1),(10213,10132,'159','',1),(10214,10132,'108','',1),(10215,10132,'84','',1),(10216,10132,'101','',1),(10217,10132,'121','',1),(10218,10132,'37','',1),(10219,10132,'143','',1),(10220,10132,'77','',1),(10221,10132,'115','',1),(10222,10132,'56','',1),(10223,10132,'16','',1),(10224,10132,'111','',1),(10225,10132,'3','',1),(10226,10132,'23','',1),(10227,10132,'20','',1),(10228,10132,'83','',1),(10229,10132,'99','',1),(10230,10132,'70','',1),(10231,10132,'141','',1),(10232,10132,'9','',1),(10233,10132,'28','',1),(10234,10132,'137','',1),(10235,10132,'47','',1),(10236,10132,'15','',1),(10237,10132,'116','',1),(10238,10132,'82','',1),(10239,10132,'151','',1),(10240,10132,'54','',1),(10241,10132,'132','',1),(10242,10132,'34','',1),(10243,10132,'61','',1),(10244,10132,'73','',1),(10245,10132,'31','',1),(10246,10132,'136','',1),(10247,10132,'50','',1),(10248,10132,'127','',1),(10249,10132,'25','',1),(10250,10132,'33','',1),(10251,10132,'150','',1),(10252,10132,'126','',1),(10253,10132,'114','',1),(10254,10132,'149','',1),(10255,10132,'67','',1),(10256,10132,'110','',1),(10257,10132,'59','',1),(10258,10132,'135','',1),(10259,10132,'131','',1),(10260,10132,'102','',1),(10261,10132,'Provision_WAR_prov','',1),(10262,10132,'provmenu_WAR_provmenu','',1),(10263,10132,'safaricomprovisioning_WAR_safaricomsms','',1),(10264,10132,'airtelprovisioning_WAR_airtelsms','',1),(10301,10132,'subscriptionprovisioning_WAR_subscription','',1),(10302,10132,'safaricomsmsprovisioning_WAR_safaricomsms','',1),(10303,10132,'airtelsmsprovisioning_WAR_airtelsms','',1),(10304,10132,'airtelwappushprovisioning_WAR_airtelwappush','',1),(10305,10132,'casprovisioning_WAR_cas','',1),(10306,10132,'safaricomwappushprovisioning_WAR_safaricomwappush','',1);
/*!40000 ALTER TABLE `Portlet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PortletItem`
--

DROP TABLE IF EXISTS `PortletItem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PortletItem` (
  `portletItemId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `portletId` varchar(75) DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`portletItemId`),
  KEY `IX_96BDD537` (`groupId`,`classNameId`),
  KEY `IX_D699243F` (`groupId`,`name`,`portletId`,`classNameId`),
  KEY `IX_2C61314E` (`groupId`,`portletId`),
  KEY `IX_E922D6C0` (`groupId`,`portletId`,`classNameId`),
  KEY `IX_8E71167F` (`groupId`,`portletId`,`classNameId`,`name`),
  KEY `IX_33B8CE8D` (`groupId`,`portletId`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PortletItem`
--

LOCK TABLES `PortletItem` WRITE;
/*!40000 ALTER TABLE `PortletItem` DISABLE KEYS */;
/*!40000 ALTER TABLE `PortletItem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PortletPreferences`
--

DROP TABLE IF EXISTS `PortletPreferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PortletPreferences` (
  `portletPreferencesId` bigint(20) NOT NULL,
  `ownerId` bigint(20) DEFAULT NULL,
  `ownerType` int(11) DEFAULT NULL,
  `plid` bigint(20) DEFAULT NULL,
  `portletId` varchar(200) DEFAULT NULL,
  `preferences` longtext,
  PRIMARY KEY (`portletPreferencesId`),
  UNIQUE KEY `IX_C7057FF7` (`ownerId`,`ownerType`,`plid`,`portletId`),
  KEY `IX_E4F13E6E` (`ownerId`,`ownerType`,`plid`),
  KEY `IX_F15C1C4F` (`plid`),
  KEY `IX_D340DB76` (`plid`,`portletId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PortletPreferences`
--

LOCK TABLES `PortletPreferences` WRITE;
/*!40000 ALTER TABLE `PortletPreferences` DISABLE KEYS */;
INSERT INTO `PortletPreferences` VALUES (10131,0,1,0,'LIFERAY_PORTAL','<portlet-preferences />'),(10137,10132,1,0,'LIFERAY_PORTAL','<portlet-preferences />'),(10265,0,3,10160,'provmenu_WAR_provmenu_INSTANCE_abcd','<portlet-preferences />'),(10266,0,3,10160,'Provision_WAR_prov','<portlet-preferences />'),(10267,0,3,10160,'103','<portlet-preferences />'),(10270,10135,4,0,'LIFERAY_PORTAL','<portlet-preferences />'),(10281,10169,4,0,'LIFERAY_PORTAL','<portlet-preferences />'),(10282,0,3,10160,'145','<portlet-preferences />'),(10283,0,3,10160,'88','<portlet-preferences />'),(10284,0,3,10160,'87','<portlet-preferences />'),(10285,0,3,10160,'58','<portlet-preferences />'),(10401,0,3,10160,'provmenu_WAR_provmenu_INSTANCE_5fpZ','<portlet-preferences />'),(10402,0,3,10160,'provmenu_WAR_provmenu_INSTANCE_8Vt1','<portlet-preferences />');
/*!40000 ALTER TABLE `PortletPreferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QUARTZ_BLOB_TRIGGERS`
--

DROP TABLE IF EXISTS `QUARTZ_BLOB_TRIGGERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `QUARTZ_BLOB_TRIGGERS` (
  `TRIGGER_NAME` varchar(80) NOT NULL,
  `TRIGGER_GROUP` varchar(80) NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY (`TRIGGER_NAME`,`TRIGGER_GROUP`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QUARTZ_BLOB_TRIGGERS`
--

LOCK TABLES `QUARTZ_BLOB_TRIGGERS` WRITE;
/*!40000 ALTER TABLE `QUARTZ_BLOB_TRIGGERS` DISABLE KEYS */;
/*!40000 ALTER TABLE `QUARTZ_BLOB_TRIGGERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QUARTZ_CALENDARS`
--

DROP TABLE IF EXISTS `QUARTZ_CALENDARS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `QUARTZ_CALENDARS` (
  `CALENDAR_NAME` varchar(80) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`CALENDAR_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QUARTZ_CALENDARS`
--

LOCK TABLES `QUARTZ_CALENDARS` WRITE;
/*!40000 ALTER TABLE `QUARTZ_CALENDARS` DISABLE KEYS */;
/*!40000 ALTER TABLE `QUARTZ_CALENDARS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QUARTZ_CRON_TRIGGERS`
--

DROP TABLE IF EXISTS `QUARTZ_CRON_TRIGGERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `QUARTZ_CRON_TRIGGERS` (
  `TRIGGER_NAME` varchar(80) NOT NULL,
  `TRIGGER_GROUP` varchar(80) NOT NULL,
  `CRON_EXPRESSION` varchar(80) NOT NULL,
  `TIME_ZONE_ID` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`TRIGGER_NAME`,`TRIGGER_GROUP`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QUARTZ_CRON_TRIGGERS`
--

LOCK TABLES `QUARTZ_CRON_TRIGGERS` WRITE;
/*!40000 ALTER TABLE `QUARTZ_CRON_TRIGGERS` DISABLE KEYS */;
/*!40000 ALTER TABLE `QUARTZ_CRON_TRIGGERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QUARTZ_FIRED_TRIGGERS`
--

DROP TABLE IF EXISTS `QUARTZ_FIRED_TRIGGERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `QUARTZ_FIRED_TRIGGERS` (
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(80) NOT NULL,
  `TRIGGER_GROUP` varchar(80) NOT NULL,
  `IS_VOLATILE` tinyint(4) NOT NULL,
  `INSTANCE_NAME` varchar(80) NOT NULL,
  `FIRED_TIME` bigint(20) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(80) DEFAULT NULL,
  `JOB_GROUP` varchar(80) DEFAULT NULL,
  `IS_STATEFUL` tinyint(4) DEFAULT NULL,
  `REQUESTS_RECOVERY` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ENTRY_ID`),
  KEY `IX_804154AF` (`INSTANCE_NAME`),
  KEY `IX_BAB9A1F7` (`JOB_GROUP`),
  KEY `IX_ADEE6A17` (`JOB_NAME`),
  KEY `IX_64B194F2` (`TRIGGER_GROUP`),
  KEY `IX_5FEABBC` (`TRIGGER_NAME`),
  KEY `IX_20D8706C` (`TRIGGER_NAME`,`TRIGGER_GROUP`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QUARTZ_FIRED_TRIGGERS`
--

LOCK TABLES `QUARTZ_FIRED_TRIGGERS` WRITE;
/*!40000 ALTER TABLE `QUARTZ_FIRED_TRIGGERS` DISABLE KEYS */;
/*!40000 ALTER TABLE `QUARTZ_FIRED_TRIGGERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QUARTZ_JOB_DETAILS`
--

DROP TABLE IF EXISTS `QUARTZ_JOB_DETAILS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `QUARTZ_JOB_DETAILS` (
  `JOB_NAME` varchar(80) NOT NULL,
  `JOB_GROUP` varchar(80) NOT NULL,
  `DESCRIPTION` varchar(120) DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(128) NOT NULL,
  `IS_DURABLE` tinyint(4) NOT NULL,
  `IS_VOLATILE` tinyint(4) NOT NULL,
  `IS_STATEFUL` tinyint(4) NOT NULL,
  `REQUESTS_RECOVERY` tinyint(4) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`JOB_NAME`,`JOB_GROUP`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QUARTZ_JOB_DETAILS`
--

LOCK TABLES `QUARTZ_JOB_DETAILS` WRITE;
/*!40000 ALTER TABLE `QUARTZ_JOB_DETAILS` DISABLE KEYS */;
INSERT INTO `QUARTZ_JOB_DETAILS` VALUES ('com.liferay.portlet.admin.messaging.LDAPImportMessageListener','com.liferay.portlet.admin.messaging.LDAPImportMessageListener',NULL,'com.liferay.portal.scheduler.job.MessageSenderJob',0,0,0,0,'��\0sr\0org.quartz.JobDataMap���迩��\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�����](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap�.�(v\n�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xpsr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0messagesr\0+com.liferay.portal.kernel.messaging.Message��Aofg�^\0L\0_destinationNamet\0Ljava/lang/String;L\0_payloadt\0Ljava/lang/Object;L\0_responseDestinationNameq\0~\0	L\0_responseIdq\0~\0	L\0_valuesq\0~\0xpppppsq\0~\0?@\0\0\0\0\0w\0\0\0\0\0\0t\0receiver_keyt\0{com.liferay.portlet.admin.messaging.LDAPImportMessageListener:com.liferay.portlet.admin.messaging.LDAPImportMessageListenerxt\0descriptiont\0\0t\0destinationt\0\Zliferay/scheduler_dispatchx\0'),('com.liferay.portlet.journal.messaging.CheckArticleMessageListener','com.liferay.portlet.journal.messaging.CheckArticleMessageListener',NULL,'com.liferay.portal.scheduler.job.MessageSenderJob',0,0,0,0,'��\0sr\0org.quartz.JobDataMap���迩��\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�����](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap�.�(v\n�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xpsr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0messagesr\0+com.liferay.portal.kernel.messaging.Message��Aofg�^\0L\0_destinationNamet\0Ljava/lang/String;L\0_payloadt\0Ljava/lang/Object;L\0_responseDestinationNameq\0~\0	L\0_responseIdq\0~\0	L\0_valuesq\0~\0xpppppsq\0~\0?@\0\0\0\0\0w\0\0\0\0\0\0t\0receiver_keyt\0�com.liferay.portlet.journal.messaging.CheckArticleMessageListener:com.liferay.portlet.journal.messaging.CheckArticleMessageListenerxt\0descriptiont\0\0t\0destinationt\0\Zliferay/scheduler_dispatchx\0'),('com.liferay.portlet.blogs.messaging.LinkbackMessageListener','com.liferay.portlet.blogs.messaging.LinkbackMessageListener',NULL,'com.liferay.portal.scheduler.job.MessageSenderJob',0,0,0,0,'��\0sr\0org.quartz.JobDataMap���迩��\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�����](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap�.�(v\n�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xpsr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0messagesr\0+com.liferay.portal.kernel.messaging.Message��Aofg�^\0L\0_destinationNamet\0Ljava/lang/String;L\0_payloadt\0Ljava/lang/Object;L\0_responseDestinationNameq\0~\0	L\0_responseIdq\0~\0	L\0_valuesq\0~\0xpppppsq\0~\0?@\0\0\0\0\0w\0\0\0\0\0\0t\0receiver_keyt\0wcom.liferay.portlet.blogs.messaging.LinkbackMessageListener:com.liferay.portlet.blogs.messaging.LinkbackMessageListenerxt\0descriptiont\0\0t\0destinationt\0\Zliferay/scheduler_dispatchx\0'),('com.liferay.portlet.messageboards.messaging.ExpireBanMessageListener','com.liferay.portlet.messageboards.messaging.ExpireBanMessageListener',NULL,'com.liferay.portal.scheduler.job.MessageSenderJob',0,0,0,0,'��\0sr\0org.quartz.JobDataMap���迩��\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�����](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap�.�(v\n�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xpsr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0messagesr\0+com.liferay.portal.kernel.messaging.Message��Aofg�^\0L\0_destinationNamet\0Ljava/lang/String;L\0_payloadt\0Ljava/lang/Object;L\0_responseDestinationNameq\0~\0	L\0_responseIdq\0~\0	L\0_valuesq\0~\0xpppppsq\0~\0?@\0\0\0\0\0w\0\0\0\0\0\0t\0receiver_keyt\0�com.liferay.portlet.messageboards.messaging.ExpireBanMessageListener:com.liferay.portlet.messageboards.messaging.ExpireBanMessageListenerxt\0descriptiont\0\0t\0destinationt\0\Zliferay/scheduler_dispatchx\0'),('com.liferay.portlet.calendar.messaging.CheckEventMessageListener','com.liferay.portlet.calendar.messaging.CheckEventMessageListener',NULL,'com.liferay.portal.scheduler.job.MessageSenderJob',0,0,0,0,'��\0sr\0org.quartz.JobDataMap���迩��\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�����](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap�.�(v\n�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xpsr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0messagesr\0+com.liferay.portal.kernel.messaging.Message��Aofg�^\0L\0_destinationNamet\0Ljava/lang/String;L\0_payloadt\0Ljava/lang/Object;L\0_responseDestinationNameq\0~\0	L\0_responseIdq\0~\0	L\0_valuesq\0~\0xpppppsq\0~\0?@\0\0\0\0\0w\0\0\0\0\0\0t\0receiver_keyt\0�com.liferay.portlet.calendar.messaging.CheckEventMessageListener:com.liferay.portlet.calendar.messaging.CheckEventMessageListenerxt\0descriptiont\0\0t\0destinationt\0\Zliferay/scheduler_dispatchx\0'),('com.liferay.portlet.announcements.messaging.CheckEntryMessageListener','com.liferay.portlet.announcements.messaging.CheckEntryMessageListener',NULL,'com.liferay.portal.scheduler.job.MessageSenderJob',0,0,0,0,'��\0sr\0org.quartz.JobDataMap���迩��\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�����](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap�.�(v\n�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xpsr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0messagesr\0+com.liferay.portal.kernel.messaging.Message��Aofg�^\0L\0_destinationNamet\0Ljava/lang/String;L\0_payloadt\0Ljava/lang/Object;L\0_responseDestinationNameq\0~\0	L\0_responseIdq\0~\0	L\0_valuesq\0~\0xpppppsq\0~\0?@\0\0\0\0\0w\0\0\0\0\0\0t\0receiver_keyt\0�com.liferay.portlet.announcements.messaging.CheckEntryMessageListener:com.liferay.portlet.announcements.messaging.CheckEntryMessageListenerxt\0descriptiont\0\0t\0destinationt\0\Zliferay/scheduler_dispatchx\0'),('com.liferay.portlet.admin.messaging.PluginRepositoriesMessageListener','com.liferay.portlet.admin.messaging.PluginRepositoriesMessageListener',NULL,'com.liferay.portal.scheduler.job.MessageSenderJob',0,0,0,0,'��\0sr\0org.quartz.JobDataMap���迩��\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�����](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap�.�(v\n�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xpsr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0messagesr\0+com.liferay.portal.kernel.messaging.Message��Aofg�^\0L\0_destinationNamet\0Ljava/lang/String;L\0_payloadt\0Ljava/lang/Object;L\0_responseDestinationNameq\0~\0	L\0_responseIdq\0~\0	L\0_valuesq\0~\0xpppppsq\0~\0?@\0\0\0\0\0w\0\0\0\0\0\0t\0receiver_keyt\0�com.liferay.portlet.admin.messaging.PluginRepositoriesMessageListener:com.liferay.portlet.admin.messaging.PluginRepositoriesMessageListenerxt\0descriptiont\0\0t\0destinationt\0\Zliferay/scheduler_dispatchx\0'),('com.liferay.portlet.social.messaging.CheckEquityLogMessageListener','com.liferay.portlet.social.messaging.CheckEquityLogMessageListener',NULL,'com.liferay.portal.scheduler.job.MessageSenderJob',0,0,0,0,'��\0sr\0org.quartz.JobDataMap���迩��\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�����](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap�.�(v\n�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xpsr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0messagesr\0+com.liferay.portal.kernel.messaging.Message��Aofg�^\0L\0_destinationNamet\0Ljava/lang/String;L\0_payloadt\0Ljava/lang/Object;L\0_responseDestinationNameq\0~\0	L\0_responseIdq\0~\0	L\0_valuesq\0~\0xpppppsq\0~\0?@\0\0\0\0\0w\0\0\0\0\0\0t\0receiver_keyt\0�com.liferay.portlet.social.messaging.CheckEquityLogMessageListener:com.liferay.portlet.social.messaging.CheckEquityLogMessageListenerxt\0descriptionpt\0destinationt\0\Zliferay/scheduler_dispatchx\0');
/*!40000 ALTER TABLE `QUARTZ_JOB_DETAILS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QUARTZ_JOB_LISTENERS`
--

DROP TABLE IF EXISTS `QUARTZ_JOB_LISTENERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `QUARTZ_JOB_LISTENERS` (
  `JOB_NAME` varchar(80) NOT NULL,
  `JOB_GROUP` varchar(80) NOT NULL,
  `JOB_LISTENER` varchar(80) NOT NULL,
  PRIMARY KEY (`JOB_NAME`,`JOB_GROUP`,`JOB_LISTENER`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QUARTZ_JOB_LISTENERS`
--

LOCK TABLES `QUARTZ_JOB_LISTENERS` WRITE;
/*!40000 ALTER TABLE `QUARTZ_JOB_LISTENERS` DISABLE KEYS */;
/*!40000 ALTER TABLE `QUARTZ_JOB_LISTENERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QUARTZ_LOCKS`
--

DROP TABLE IF EXISTS `QUARTZ_LOCKS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `QUARTZ_LOCKS` (
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`LOCK_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QUARTZ_LOCKS`
--

LOCK TABLES `QUARTZ_LOCKS` WRITE;
/*!40000 ALTER TABLE `QUARTZ_LOCKS` DISABLE KEYS */;
INSERT INTO `QUARTZ_LOCKS` VALUES ('CALENDAR_ACCESS'),('JOB_ACCESS'),('MISFIRE_ACCESS'),('STATE_ACCESS'),('TRIGGER_ACCESS');
/*!40000 ALTER TABLE `QUARTZ_LOCKS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QUARTZ_PAUSED_TRIGGER_GRPS`
--

DROP TABLE IF EXISTS `QUARTZ_PAUSED_TRIGGER_GRPS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `QUARTZ_PAUSED_TRIGGER_GRPS` (
  `TRIGGER_GROUP` varchar(80) NOT NULL,
  PRIMARY KEY (`TRIGGER_GROUP`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QUARTZ_PAUSED_TRIGGER_GRPS`
--

LOCK TABLES `QUARTZ_PAUSED_TRIGGER_GRPS` WRITE;
/*!40000 ALTER TABLE `QUARTZ_PAUSED_TRIGGER_GRPS` DISABLE KEYS */;
/*!40000 ALTER TABLE `QUARTZ_PAUSED_TRIGGER_GRPS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QUARTZ_SCHEDULER_STATE`
--

DROP TABLE IF EXISTS `QUARTZ_SCHEDULER_STATE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `QUARTZ_SCHEDULER_STATE` (
  `INSTANCE_NAME` varchar(80) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(20) NOT NULL,
  `CHECKIN_INTERVAL` bigint(20) NOT NULL,
  PRIMARY KEY (`INSTANCE_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QUARTZ_SCHEDULER_STATE`
--

LOCK TABLES `QUARTZ_SCHEDULER_STATE` WRITE;
/*!40000 ALTER TABLE `QUARTZ_SCHEDULER_STATE` DISABLE KEYS */;
/*!40000 ALTER TABLE `QUARTZ_SCHEDULER_STATE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QUARTZ_SIMPLE_TRIGGERS`
--

DROP TABLE IF EXISTS `QUARTZ_SIMPLE_TRIGGERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `QUARTZ_SIMPLE_TRIGGERS` (
  `TRIGGER_NAME` varchar(80) NOT NULL,
  `TRIGGER_GROUP` varchar(80) NOT NULL,
  `REPEAT_COUNT` bigint(20) NOT NULL,
  `REPEAT_INTERVAL` bigint(20) NOT NULL,
  `TIMES_TRIGGERED` bigint(20) NOT NULL,
  PRIMARY KEY (`TRIGGER_NAME`,`TRIGGER_GROUP`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QUARTZ_SIMPLE_TRIGGERS`
--

LOCK TABLES `QUARTZ_SIMPLE_TRIGGERS` WRITE;
/*!40000 ALTER TABLE `QUARTZ_SIMPLE_TRIGGERS` DISABLE KEYS */;
INSERT INTO `QUARTZ_SIMPLE_TRIGGERS` VALUES ('com.liferay.portlet.admin.messaging.LDAPImportMessageListener','com.liferay.portlet.admin.messaging.LDAPImportMessageListener',-1,600000,1),('com.liferay.portlet.journal.messaging.CheckArticleMessageListener','com.liferay.portlet.journal.messaging.CheckArticleMessageListener',-1,900000,1),('com.liferay.portlet.blogs.messaging.LinkbackMessageListener','com.liferay.portlet.blogs.messaging.LinkbackMessageListener',-1,300000,2),('com.liferay.portlet.messageboards.messaging.ExpireBanMessageListener','com.liferay.portlet.messageboards.messaging.ExpireBanMessageListener',-1,7200000,1),('com.liferay.portlet.calendar.messaging.CheckEventMessageListener','com.liferay.portlet.calendar.messaging.CheckEventMessageListener',-1,900000,1),('com.liferay.portlet.announcements.messaging.CheckEntryMessageListener','com.liferay.portlet.announcements.messaging.CheckEntryMessageListener',-1,900000,1),('com.liferay.portlet.admin.messaging.PluginRepositoriesMessageListener','com.liferay.portlet.admin.messaging.PluginRepositoriesMessageListener',-1,86400000,1),('com.liferay.portlet.social.messaging.CheckEquityLogMessageListener','com.liferay.portlet.social.messaging.CheckEquityLogMessageListener',-1,86400000,1);
/*!40000 ALTER TABLE `QUARTZ_SIMPLE_TRIGGERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QUARTZ_TRIGGERS`
--

DROP TABLE IF EXISTS `QUARTZ_TRIGGERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `QUARTZ_TRIGGERS` (
  `TRIGGER_NAME` varchar(80) NOT NULL,
  `TRIGGER_GROUP` varchar(80) NOT NULL,
  `JOB_NAME` varchar(80) NOT NULL,
  `JOB_GROUP` varchar(80) NOT NULL,
  `IS_VOLATILE` tinyint(4) NOT NULL,
  `DESCRIPTION` varchar(120) DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(20) DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(20) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(20) NOT NULL,
  `END_TIME` bigint(20) DEFAULT NULL,
  `CALENDAR_NAME` varchar(80) DEFAULT NULL,
  `MISFIRE_INSTR` int(11) DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IX_F7655CC3` (`NEXT_FIRE_TIME`),
  KEY `IX_9955EFB5` (`TRIGGER_STATE`),
  KEY `IX_8040C593` (`TRIGGER_STATE`,`NEXT_FIRE_TIME`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QUARTZ_TRIGGERS`
--

LOCK TABLES `QUARTZ_TRIGGERS` WRITE;
/*!40000 ALTER TABLE `QUARTZ_TRIGGERS` DISABLE KEYS */;
INSERT INTO `QUARTZ_TRIGGERS` VALUES ('com.liferay.portlet.admin.messaging.LDAPImportMessageListener','com.liferay.portlet.admin.messaging.LDAPImportMessageListener','com.liferay.portlet.admin.messaging.LDAPImportMessageListener','com.liferay.portlet.admin.messaging.LDAPImportMessageListener',0,NULL,1319695774090,1319695174090,5,'WAITING','SIMPLE',1319695174090,0,NULL,0,''),('com.liferay.portlet.journal.messaging.CheckArticleMessageListener','com.liferay.portlet.journal.messaging.CheckArticleMessageListener','com.liferay.portlet.journal.messaging.CheckArticleMessageListener','com.liferay.portlet.journal.messaging.CheckArticleMessageListener',0,NULL,1319696074338,1319695174338,5,'WAITING','SIMPLE',1319695174338,0,NULL,0,''),('com.liferay.portlet.blogs.messaging.LinkbackMessageListener','com.liferay.portlet.blogs.messaging.LinkbackMessageListener','com.liferay.portlet.blogs.messaging.LinkbackMessageListener','com.liferay.portlet.blogs.messaging.LinkbackMessageListener',0,NULL,1319695774671,1319695474671,5,'WAITING','SIMPLE',1319695174671,0,NULL,0,''),('com.liferay.portlet.messageboards.messaging.ExpireBanMessageListener','com.liferay.portlet.messageboards.messaging.ExpireBanMessageListener','com.liferay.portlet.messageboards.messaging.ExpireBanMessageListener','com.liferay.portlet.messageboards.messaging.ExpireBanMessageListener',0,NULL,1319702372946,1319695172946,5,'WAITING','SIMPLE',1319695172946,0,NULL,0,''),('com.liferay.portlet.calendar.messaging.CheckEventMessageListener','com.liferay.portlet.calendar.messaging.CheckEventMessageListener','com.liferay.portlet.calendar.messaging.CheckEventMessageListener','com.liferay.portlet.calendar.messaging.CheckEventMessageListener',0,NULL,1319696073214,1319695173214,5,'WAITING','SIMPLE',1319695173214,0,NULL,0,''),('com.liferay.portlet.announcements.messaging.CheckEntryMessageListener','com.liferay.portlet.announcements.messaging.CheckEntryMessageListener','com.liferay.portlet.announcements.messaging.CheckEntryMessageListener','com.liferay.portlet.announcements.messaging.CheckEntryMessageListener',0,NULL,1319696073549,1319695173549,5,'WAITING','SIMPLE',1319695173549,0,NULL,0,''),('com.liferay.portlet.admin.messaging.PluginRepositoriesMessageListener','com.liferay.portlet.admin.messaging.PluginRepositoriesMessageListener','com.liferay.portlet.admin.messaging.PluginRepositoriesMessageListener','com.liferay.portlet.admin.messaging.PluginRepositoriesMessageListener',0,NULL,1319781574088,1319695174088,5,'WAITING','SIMPLE',1319695174088,0,NULL,0,''),('com.liferay.portlet.social.messaging.CheckEquityLogMessageListener','com.liferay.portlet.social.messaging.CheckEquityLogMessageListener','com.liferay.portlet.social.messaging.CheckEquityLogMessageListener','com.liferay.portlet.social.messaging.CheckEquityLogMessageListener',0,NULL,1319781575522,1319695175522,5,'WAITING','SIMPLE',1319695175522,0,NULL,0,'');
/*!40000 ALTER TABLE `QUARTZ_TRIGGERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QUARTZ_TRIGGER_LISTENERS`
--

DROP TABLE IF EXISTS `QUARTZ_TRIGGER_LISTENERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `QUARTZ_TRIGGER_LISTENERS` (
  `TRIGGER_NAME` varchar(80) NOT NULL,
  `TRIGGER_GROUP` varchar(80) NOT NULL,
  `TRIGGER_LISTENER` varchar(80) NOT NULL,
  PRIMARY KEY (`TRIGGER_NAME`,`TRIGGER_GROUP`,`TRIGGER_LISTENER`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QUARTZ_TRIGGER_LISTENERS`
--

LOCK TABLES `QUARTZ_TRIGGER_LISTENERS` WRITE;
/*!40000 ALTER TABLE `QUARTZ_TRIGGER_LISTENERS` DISABLE KEYS */;
/*!40000 ALTER TABLE `QUARTZ_TRIGGER_LISTENERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RatingsEntry`
--

DROP TABLE IF EXISTS `RatingsEntry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RatingsEntry` (
  `entryId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `score` double DEFAULT NULL,
  PRIMARY KEY (`entryId`),
  UNIQUE KEY `IX_B47E3C11` (`userId`,`classNameId`,`classPK`),
  KEY `IX_16184D57` (`classNameId`,`classPK`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RatingsEntry`
--

LOCK TABLES `RatingsEntry` WRITE;
/*!40000 ALTER TABLE `RatingsEntry` DISABLE KEYS */;
/*!40000 ALTER TABLE `RatingsEntry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RatingsStats`
--

DROP TABLE IF EXISTS `RatingsStats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RatingsStats` (
  `statsId` bigint(20) NOT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `totalEntries` int(11) DEFAULT NULL,
  `totalScore` double DEFAULT NULL,
  `averageScore` double DEFAULT NULL,
  PRIMARY KEY (`statsId`),
  UNIQUE KEY `IX_A6E99284` (`classNameId`,`classPK`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RatingsStats`
--

LOCK TABLES `RatingsStats` WRITE;
/*!40000 ALTER TABLE `RatingsStats` DISABLE KEYS */;
/*!40000 ALTER TABLE `RatingsStats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Region`
--

DROP TABLE IF EXISTS `Region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Region` (
  `regionId` bigint(20) NOT NULL,
  `countryId` bigint(20) DEFAULT NULL,
  `regionCode` varchar(75) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `active_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`regionId`),
  KEY `IX_2D9A426F` (`active_`),
  KEY `IX_16D87CA7` (`countryId`),
  KEY `IX_11FB3E42` (`countryId`,`active_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Region`
--

LOCK TABLES `Region` WRITE;
/*!40000 ALTER TABLE `Region` DISABLE KEYS */;
INSERT INTO `Region` VALUES (1001,1,'AB','Alberta',1),(1002,1,'BC','British Columbia',1),(1003,1,'MB','Manitoba',1),(1004,1,'NB','New Brunswick',1),(1005,1,'NL','Newfoundland and Labrador',1),(1006,1,'NT','Northwest Territories',1),(1007,1,'NS','Nova Scotia',1),(1008,1,'NU','Nunavut',1),(1009,1,'ON','Ontario',1),(1010,1,'PE','Prince Edward Island',1),(1011,1,'QC','Quebec',1),(1012,1,'SK','Saskatchewan',1),(1013,1,'YT','Yukon',1),(3001,3,'A','Alsace',1),(3002,3,'B','Aquitaine',1),(3003,3,'C','Auvergne',1),(3004,3,'P','Basse-Normandie',1),(3005,3,'D','Bourgogne',1),(3006,3,'E','Bretagne',1),(3007,3,'F','Centre',1),(3008,3,'G','Champagne-Ardenne',1),(3009,3,'H','Corse',1),(3010,3,'GF','Guyane',1),(3011,3,'I','Franche ComtÃ©',1),(3012,3,'GP','Guadeloupe',1),(3013,3,'Q','Haute-Normandie',1),(3014,3,'J','ÃŽle-de-France',1),(3015,3,'K','Languedoc-Roussillon',1),(3016,3,'L','Limousin',1),(3017,3,'M','Lorraine',1),(3018,3,'MQ','Martinique',1),(3019,3,'N','Midi-PyrÃ©nÃ©es',1),(3020,3,'O','Nord Pas de Calais',1),(3021,3,'R','Pays de la Loire',1),(3022,3,'S','Picardie',1),(3023,3,'T','Poitou-Charentes',1),(3024,3,'U','Provence-Alpes-CÃ´te-d\'Azur',1),(3025,3,'RE','RÃ©union',1),(3026,3,'V','RhÃ´ne-Alpes',1),(4001,4,'BW','Baden-WÃ¼rttemberg',1),(4002,4,'BY','Bayern',1),(4003,4,'BE','Berlin',1),(4004,4,'BR','Brandenburg',1),(4005,4,'HB','Bremen',1),(4006,4,'HH','Hamburg',1),(4007,4,'HE','Hessen',1),(4008,4,'MV','Mecklenburg-Vorpommern',1),(4009,4,'NI','Niedersachsen',1),(4010,4,'NW','Nordrhein-Westfalen',1),(4011,4,'RP','Rheinland-Pfalz',1),(4012,4,'SL','Saarland',1),(4013,4,'SN','Sachsen',1),(4014,4,'ST','Sachsen-Anhalt',1),(4015,4,'SH','Schleswig-Holstein',1),(4016,4,'TH','ThÃ¼ringen',1),(8001,8,'AG','Agrigento',1),(8002,8,'AL','Alessandria',1),(8003,8,'AN','Ancona',1),(8004,8,'AO','Aosta',1),(8005,8,'AR','Arezzo',1),(8006,8,'AP','Ascoli Piceno',1),(8007,8,'AT','Asti',1),(8008,8,'AV','Avellino',1),(8009,8,'BA','Bari',1),(8010,8,'BT','Barletta-Andria-Trani',1),(8011,8,'BL','Belluno',1),(8012,8,'BN','Benevento',1),(8013,8,'BG','Bergamo',1),(8014,8,'BI','Biella',1),(8015,8,'BO','Bologna',1),(8016,8,'BZ','Bolzano',1),(8017,8,'BS','Brescia',1),(8018,8,'BR','Brindisi',1),(8019,8,'CA','Cagliari',1),(8020,8,'CL','Caltanissetta',1),(8021,8,'CB','Campobasso',1),(8022,8,'CI','Carbonia-Iglesias',1),(8023,8,'CE','Caserta',1),(8024,8,'CT','Catania',1),(8025,8,'CZ','Catanzaro',1),(8026,8,'CH','Chieti',1),(8027,8,'CO','Como',1),(8028,8,'CS','Cosenza',1),(8029,8,'CR','Cremona',1),(8030,8,'KR','Crotone',1),(8031,8,'CN','Cuneo',1),(8032,8,'EN','Enna',1),(8033,8,'FM','Fermo',1),(8034,8,'FE','Ferrara',1),(8035,8,'FI','Firenze',1),(8036,8,'FG','Foggia',1),(8037,8,'FC','Forli-Cesena',1),(8038,8,'FR','Frosinone',1),(8039,8,'GE','Genova',1),(8040,8,'GO','Gorizia',1),(8041,8,'GR','Grosseto',1),(8042,8,'IM','Imperia',1),(8043,8,'IS','Isernia',1),(8044,8,'AQ','L\'Aquila',1),(8045,8,'SP','La Spezia',1),(8046,8,'LT','Latina',1),(8047,8,'LE','Lecce',1),(8048,8,'LC','Lecco',1),(8049,8,'LI','Livorno',1),(8050,8,'LO','Lodi',1),(8051,8,'LU','Lucca',1),(8052,8,'MC','Macerata',1),(8053,8,'MN','Mantova',1),(8054,8,'MS','Massa-Carrara',1),(8055,8,'MT','Matera',1),(8056,8,'MA','Medio Campidano',1),(8057,8,'ME','Messina',1),(8058,8,'MI','Milano',1),(8059,8,'MO','Modena',1),(8060,8,'MZ','Monza',1),(8061,8,'NA','Napoli',1),(8062,8,'NO','Novara',1),(8063,8,'NU','Nuoro',1),(8064,8,'OG','Ogliastra',1),(8065,8,'OT','Olbia-Tempio',1),(8066,8,'OR','Oristano',1),(8067,8,'PD','Padova',1),(8068,8,'PA','Palermo',1),(8069,8,'PR','Parma',1),(8070,8,'PV','Pavia',1),(8071,8,'PG','Perugia',1),(8072,8,'PU','Pesaro e Urbino',1),(8073,8,'PE','Pescara',1),(8074,8,'PC','Piacenza',1),(8075,8,'PI','Pisa',1),(8076,8,'PT','Pistoia',1),(8077,8,'PN','Pordenone',1),(8078,8,'PZ','Potenza',1),(8079,8,'PO','Prato',1),(8080,8,'RG','Ragusa',1),(8081,8,'RA','Ravenna',1),(8082,8,'RC','Reggio Calabria',1),(8083,8,'RE','Reggio Emilia',1),(8084,8,'RI','Rieti',1),(8085,8,'RN','Rimini',1),(8086,8,'RM','Roma',1),(8087,8,'RO','Rovigo',1),(8088,8,'SA','Salerno',1),(8089,8,'SS','Sassari',1),(8090,8,'SV','Savona',1),(8091,8,'SI','Siena',1),(8092,8,'SR','Siracusa',1),(8093,8,'SO','Sondrio',1),(8094,8,'TA','Taranto',1),(8095,8,'TE','Teramo',1),(8096,8,'TR','Terni',1),(8097,8,'TO','Torino',1),(8098,8,'TP','Trapani',1),(8099,8,'TN','Trento',1),(8100,8,'TV','Treviso',1),(8101,8,'TS','Trieste',1),(8102,8,'UD','Udine',1),(8103,8,'VA','Varese',1),(8104,8,'VE','Venezia',1),(8105,8,'VB','Verbano-Cusio-Ossola',1),(8106,8,'VC','Vercelli',1),(8107,8,'VR','Verona',1),(8108,8,'VV','Vibo Valentia',1),(8109,8,'VI','Vicenza',1),(8110,8,'VT','Viterbo',1),(15001,15,'AN','Andalusia',1),(15002,15,'AR','Aragon',1),(15003,15,'AS','Asturias',1),(15004,15,'IB','Balearic Islands',1),(15005,15,'PV','Basque Country',1),(15006,15,'CN','Canary Islands',1),(15007,15,'CB','Cantabria',1),(15008,15,'CL','Castile and Leon',1),(15009,15,'CM','Castile-La Mancha',1),(15010,15,'CT','Catalonia',1),(15011,15,'CE','Ceuta',1),(15012,15,'EX','Extremadura',1),(15013,15,'GA','Galicia',1),(15014,15,'LO','La Rioja',1),(15015,15,'M','Madrid',1),(15016,15,'ML','Melilla',1),(15017,15,'MU','Murcia',1),(15018,15,'NA','Navarra',1),(15019,15,'VC','Valencia',1),(19001,19,'AL','Alabama',1),(19002,19,'AK','Alaska',1),(19003,19,'AZ','Arizona',1),(19004,19,'AR','Arkansas',1),(19005,19,'CA','California',1),(19006,19,'CO','Colorado',1),(19007,19,'CT','Connecticut',1),(19008,19,'DC','District of Columbia',1),(19009,19,'DE','Delaware',1),(19010,19,'FL','Florida',1),(19011,19,'GA','Georgia',1),(19012,19,'HI','Hawaii',1),(19013,19,'ID','Idaho',1),(19014,19,'IL','Illinois',1),(19015,19,'IN','Indiana',1),(19016,19,'IA','Iowa',1),(19017,19,'KS','Kansas',1),(19018,19,'KY','Kentucky ',1),(19019,19,'LA','Louisiana ',1),(19020,19,'ME','Maine',1),(19021,19,'MD','Maryland',1),(19022,19,'MA','Massachusetts',1),(19023,19,'MI','Michigan',1),(19024,19,'MN','Minnesota',1),(19025,19,'MS','Mississippi',1),(19026,19,'MO','Missouri',1),(19027,19,'MT','Montana',1),(19028,19,'NE','Nebraska',1),(19029,19,'NV','Nevada',1),(19030,19,'NH','New Hampshire',1),(19031,19,'NJ','New Jersey',1),(19032,19,'NM','New Mexico',1),(19033,19,'NY','New York',1),(19034,19,'NC','North Carolina',1),(19035,19,'ND','North Dakota',1),(19036,19,'OH','Ohio',1),(19037,19,'OK','Oklahoma ',1),(19038,19,'OR','Oregon',1),(19039,19,'PA','Pennsylvania',1),(19040,19,'PR','Puerto Rico',1),(19041,19,'RI','Rhode Island',1),(19042,19,'SC','South Carolina',1),(19043,19,'SD','South Dakota',1),(19044,19,'TN','Tennessee',1),(19045,19,'TX','Texas',1),(19046,19,'UT','Utah',1),(19047,19,'VT','Vermont',1),(19048,19,'VA','Virginia',1),(19049,19,'WA','Washington',1),(19050,19,'WV','West Virginia',1),(19051,19,'WI','Wisconsin',1),(19052,19,'WY','Wyoming',1),(32001,32,'ACT','Australian Capital Territory',1),(32002,32,'NSW','New South Wales',1),(32003,32,'NT','Northern Territory',1),(32004,32,'QLD','Queensland',1),(32005,32,'SA','South Australia',1),(32006,32,'TAS','Tasmania',1),(32007,32,'VIC','Victoria',1),(32008,32,'WA','Western Australia',1),(202001,202,'AG','Aargau',1),(202002,202,'AR','Appenzell Ausserrhoden',1),(202003,202,'AI','Appenzell Innerrhoden',1),(202004,202,'BL','Basel-Landschaft',1),(202005,202,'BS','Basel-Stadt',1),(202006,202,'BE','Bern',1),(202007,202,'FR','Fribourg',1),(202008,202,'GE','Geneva',1),(202009,202,'GL','Glarus',1),(202010,202,'GR','GraubÃ¼nden',1),(202011,202,'JU','Jura',1),(202012,202,'LU','Lucerne',1),(202013,202,'NE','NeuchÃ¢tel',1),(202014,202,'NW','Nidwalden',1),(202015,202,'OW','Obwalden',1),(202016,202,'SH','Schaffhausen',1),(202017,202,'SZ','Schwyz',1),(202018,202,'SO','Solothurn',1),(202019,202,'SG','St. Gallen',1),(202020,202,'TG','Thurgau',1),(202021,202,'TI','Ticino',1),(202022,202,'UR','Uri',1),(202023,202,'VS','Valais',1),(202024,202,'VD','Vaud',1),(202025,202,'ZG','Zug',1),(202026,202,'ZH','ZÃ¼rich',1);
/*!40000 ALTER TABLE `Region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Release_`
--

DROP TABLE IF EXISTS `Release_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Release_` (
  `releaseId` bigint(20) NOT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `servletContextName` varchar(75) DEFAULT NULL,
  `buildNumber` int(11) DEFAULT NULL,
  `buildDate` datetime DEFAULT NULL,
  `verified` tinyint(4) DEFAULT NULL,
  `testString` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`releaseId`),
  KEY `IX_8BD6BCA7` (`servletContextName`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Release_`
--

LOCK TABLES `Release_` WRITE;
/*!40000 ALTER TABLE `Release_` DISABLE KEYS */;
INSERT INTO `Release_` VALUES (1,'2011-07-01 16:39:18','2011-10-27 05:59:30','portal',6006,'2011-02-17 00:00:00',1,'You take the blue pill, the story ends, you wake up in your bed and believe whatever you want to believe. You take the red pill, you stay in Wonderland, and I show you how deep the rabbit hole goes.');
/*!40000 ALTER TABLE `Release_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ResourceAction`
--

DROP TABLE IF EXISTS `ResourceAction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ResourceAction` (
  `resourceActionId` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `actionId` varchar(75) DEFAULT NULL,
  `bitwiseValue` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`resourceActionId`),
  UNIQUE KEY `IX_EDB9986E` (`name`,`actionId`),
  KEY `IX_81F2DB09` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ResourceAction`
--

LOCK TABLES `ResourceAction` WRITE;
/*!40000 ALTER TABLE `ResourceAction` DISABLE KEYS */;
INSERT INTO `ResourceAction` VALUES (1,'98','ACCESS_IN_CONTROL_PANEL',2),(2,'98','ADD_TO_PAGE',4),(3,'98','CONFIGURATION',8),(4,'98','VIEW',1),(5,'com.liferay.portlet.softwarecatalog.model.SCFrameworkVersion','DELETE',2),(6,'com.liferay.portlet.softwarecatalog.model.SCFrameworkVersion','PERMISSIONS',4),(7,'com.liferay.portlet.softwarecatalog.model.SCFrameworkVersion','UPDATE',8),(8,'com.liferay.portlet.softwarecatalog','ADD_FRAMEWORK_VERSION',2),(9,'com.liferay.portlet.softwarecatalog','ADD_PRODUCT_ENTRY',4),(10,'com.liferay.portlet.softwarecatalog','PERMISSIONS',8),(11,'com.liferay.portlet.softwarecatalog.model.SCLicense','DELETE',2),(12,'com.liferay.portlet.softwarecatalog.model.SCLicense','PERMISSIONS',4),(13,'com.liferay.portlet.softwarecatalog.model.SCLicense','UPDATE',8),(14,'com.liferay.portlet.softwarecatalog.model.SCLicense','VIEW',1),(15,'com.liferay.portlet.softwarecatalog.model.SCProductEntry','ADD_DISCUSSION',2),(16,'com.liferay.portlet.softwarecatalog.model.SCProductEntry','DELETE',4),(17,'com.liferay.portlet.softwarecatalog.model.SCProductEntry','DELETE_DISCUSSION',8),(18,'com.liferay.portlet.softwarecatalog.model.SCProductEntry','PERMISSIONS',16),(19,'com.liferay.portlet.softwarecatalog.model.SCProductEntry','UPDATE',32),(20,'com.liferay.portlet.softwarecatalog.model.SCProductEntry','UPDATE_DISCUSSION',64),(21,'com.liferay.portlet.softwarecatalog.model.SCProductEntry','VIEW',1),(22,'66','VIEW',1),(23,'66','ADD_TO_PAGE',2),(24,'66','CONFIGURATION',4),(25,'156','VIEW',1),(26,'156','ADD_TO_PAGE',2),(27,'156','CONFIGURATION',4),(28,'152','ACCESS_IN_CONTROL_PANEL',2),(29,'152','CONFIGURATION',4),(30,'152','VIEW',1),(31,'27','VIEW',1),(32,'27','ADD_TO_PAGE',2),(33,'27','CONFIGURATION',4),(34,'88','VIEW',1),(35,'88','ADD_TO_PAGE',2),(36,'88','CONFIGURATION',4),(37,'87','VIEW',1),(38,'87','ADD_TO_PAGE',2),(39,'87','CONFIGURATION',4),(40,'134','ACCESS_IN_CONTROL_PANEL',2),(41,'134','CONFIGURATION',4),(42,'134','VIEW',1),(43,'com.liferay.portal.model.Layout','ADD_DISCUSSION',2),(44,'com.liferay.portal.model.Layout','DELETE',4),(45,'com.liferay.portal.model.Layout','DELETE_DISCUSSION',8),(46,'com.liferay.portal.model.Layout','UPDATE',16),(47,'com.liferay.portal.model.Layout','UPDATE_DISCUSSION',32),(48,'com.liferay.portal.model.Layout','VIEW',1),(49,'com.liferay.portal.model.Layout','PERMISSIONS',64),(50,'com.liferay.portal.model.LayoutSetPrototype','DELETE',2),(51,'com.liferay.portal.model.LayoutSetPrototype','PERMISSIONS',4),(52,'com.liferay.portal.model.LayoutSetPrototype','UPDATE',8),(53,'com.liferay.portal.model.LayoutSetPrototype','VIEW',1),(54,'com.liferay.portal.model.LayoutPrototype','DELETE',2),(55,'com.liferay.portal.model.LayoutPrototype','PERMISSIONS',4),(56,'com.liferay.portal.model.LayoutPrototype','UPDATE',8),(57,'com.liferay.portal.model.LayoutPrototype','VIEW',1),(58,'com.liferay.portal.model.Team','ASSIGN_MEMBERS',2),(59,'com.liferay.portal.model.Team','DELETE',4),(60,'com.liferay.portal.model.Team','PERMISSIONS',8),(61,'com.liferay.portal.model.Team','UPDATE',16),(62,'com.liferay.portal.model.Team','VIEW',1),(63,'com.liferay.portal.model.Group','APPROVE_PROPOSAL',2),(64,'com.liferay.portal.model.Group','ASSIGN_MEMBERS',4),(65,'com.liferay.portal.model.Group','ASSIGN_REVIEWER',8),(66,'com.liferay.portal.model.Group','ASSIGN_USER_ROLES',16),(67,'com.liferay.portal.model.Group','DELETE',32),(68,'com.liferay.portal.model.Group','MANAGE_ANNOUNCEMENTS',64),(69,'com.liferay.portal.model.Group','MANAGE_ARCHIVED_SETUPS',128),(70,'com.liferay.portal.model.Group','MANAGE_LAYOUTS',256),(71,'com.liferay.portal.model.Group','MANAGE_STAGING',512),(72,'com.liferay.portal.model.Group','MANAGE_TEAMS',1024),(73,'com.liferay.portal.model.Group','PERMISSIONS',2048),(74,'com.liferay.portal.model.Group','PUBLISH_STAGING',4096),(75,'com.liferay.portal.model.Group','PUBLISH_TO_REMOTE',8192),(76,'com.liferay.portal.model.Group','UPDATE',16384),(77,'com.liferay.portlet.tasks.model.TasksProposal','ADD_DISCUSSION',2),(78,'com.liferay.portlet.tasks.model.TasksProposal','DELETE',4),(79,'com.liferay.portlet.tasks.model.TasksProposal','DELETE_DISCUSSION',8),(80,'com.liferay.portlet.tasks.model.TasksProposal','UPDATE',16),(81,'com.liferay.portlet.tasks.model.TasksProposal','UPDATE_DISCUSSION',32),(82,'com.liferay.portlet.tasks.model.TasksProposal','VIEW',1),(83,'com.liferay.portlet.tasks.model.TasksProposal','PERMISSIONS',64),(84,'130','ACCESS_IN_CONTROL_PANEL',2),(85,'130','CONFIGURATION',4),(86,'130','VIEW',1),(87,'122','VIEW',1),(88,'122','ADD_TO_PAGE',2),(89,'122','CONFIGURATION',4),(90,'36','ADD_TO_PAGE',2),(91,'36','CONFIGURATION',4),(92,'36','VIEW',1),(93,'com.liferay.portlet.wiki.model.WikiPage','ADD_DISCUSSION',2),(94,'com.liferay.portlet.wiki.model.WikiPage','DELETE',4),(95,'com.liferay.portlet.wiki.model.WikiPage','DELETE_DISCUSSION',8),(96,'com.liferay.portlet.wiki.model.WikiPage','PERMISSIONS',16),(97,'com.liferay.portlet.wiki.model.WikiPage','SUBSCRIBE',32),(98,'com.liferay.portlet.wiki.model.WikiPage','UPDATE',64),(99,'com.liferay.portlet.wiki.model.WikiPage','UPDATE_DISCUSSION',128),(100,'com.liferay.portlet.wiki.model.WikiPage','VIEW',1),(101,'com.liferay.portlet.wiki.model.WikiNode','ADD_ATTACHMENT',2),(102,'com.liferay.portlet.wiki.model.WikiNode','ADD_PAGE',4),(103,'com.liferay.portlet.wiki.model.WikiNode','DELETE',8),(104,'com.liferay.portlet.wiki.model.WikiNode','IMPORT',16),(105,'com.liferay.portlet.wiki.model.WikiNode','PERMISSIONS',32),(106,'com.liferay.portlet.wiki.model.WikiNode','SUBSCRIBE',64),(107,'com.liferay.portlet.wiki.model.WikiNode','UPDATE',128),(108,'com.liferay.portlet.wiki.model.WikiNode','VIEW',1),(109,'com.liferay.portlet.wiki','ADD_NODE',2),(110,'com.liferay.portlet.wiki','PERMISSIONS',4),(111,'26','VIEW',1),(112,'26','ADD_TO_PAGE',2),(113,'26','CONFIGURATION',4),(114,'104','VIEW',1),(115,'104','ADD_TO_PAGE',2),(116,'104','CONFIGURATION',4),(117,'64','VIEW',1),(118,'64','ADD_TO_PAGE',2),(119,'64','CONFIGURATION',4),(120,'153','ACCESS_IN_CONTROL_PANEL',2),(121,'153','CONFIGURATION',4),(122,'153','VIEW',1),(123,'129','ACCESS_IN_CONTROL_PANEL',2),(124,'129','CONFIGURATION',4),(125,'129','VIEW',1),(126,'com.liferay.portal.model.PasswordPolicy','ASSIGN_MEMBERS',2),(127,'com.liferay.portal.model.PasswordPolicy','DELETE',4),(128,'com.liferay.portal.model.PasswordPolicy','PERMISSIONS',8),(129,'com.liferay.portal.model.PasswordPolicy','UPDATE',16),(130,'com.liferay.portal.model.PasswordPolicy','VIEW',1),(131,'100','VIEW',1),(132,'100','ADD_TO_PAGE',2),(133,'100','CONFIGURATION',4),(134,'157','ACCESS_IN_CONTROL_PANEL',2),(135,'157','CONFIGURATION',4),(136,'157','VIEW',1),(137,'19','ACCESS_IN_CONTROL_PANEL',2),(138,'19','ADD_TO_PAGE',4),(139,'19','CONFIGURATION',8),(140,'19','VIEW',1),(141,'com.liferay.portlet.messageboards.model.MBCategory','ADD_FILE',2),(142,'com.liferay.portlet.messageboards.model.MBCategory','ADD_MESSAGE',4),(143,'com.liferay.portlet.messageboards.model.MBCategory','ADD_SUBCATEGORY',8),(144,'com.liferay.portlet.messageboards.model.MBCategory','DELETE',16),(145,'com.liferay.portlet.messageboards.model.MBCategory','LOCK_THREAD',32),(146,'com.liferay.portlet.messageboards.model.MBCategory','MOVE_THREAD',64),(147,'com.liferay.portlet.messageboards.model.MBCategory','PERMISSIONS',128),(148,'com.liferay.portlet.messageboards.model.MBCategory','REPLY_TO_MESSAGE',256),(149,'com.liferay.portlet.messageboards.model.MBCategory','SUBSCRIBE',512),(150,'com.liferay.portlet.messageboards.model.MBCategory','UPDATE',1024),(151,'com.liferay.portlet.messageboards.model.MBCategory','UPDATE_THREAD_PRIORITY',2048),(152,'com.liferay.portlet.messageboards.model.MBCategory','VIEW',1),(153,'com.liferay.portlet.messageboards','ADD_CATEGORY',2),(154,'com.liferay.portlet.messageboards','ADD_FILE',4),(155,'com.liferay.portlet.messageboards','ADD_MESSAGE',8),(156,'com.liferay.portlet.messageboards','BAN_USER',16),(157,'com.liferay.portlet.messageboards','MOVE_THREAD',32),(158,'com.liferay.portlet.messageboards','LOCK_THREAD',64),(159,'com.liferay.portlet.messageboards','PERMISSIONS',128),(160,'com.liferay.portlet.messageboards','REPLY_TO_MESSAGE',256),(161,'com.liferay.portlet.messageboards','SUBSCRIBE',512),(162,'com.liferay.portlet.messageboards','UPDATE_THREAD_PRIORITY',1024),(163,'com.liferay.portlet.messageboards.model.MBMessage','DELETE',2),(164,'com.liferay.portlet.messageboards.model.MBMessage','PERMISSIONS',4),(165,'com.liferay.portlet.messageboards.model.MBMessage','SUBSCRIBE',8),(166,'com.liferay.portlet.messageboards.model.MBMessage','UPDATE',16),(167,'com.liferay.portlet.messageboards.model.MBMessage','VIEW',1),(168,'160','VIEW',1),(169,'160','ADD_TO_PAGE',2),(170,'160','CONFIGURATION',4),(171,'128','ACCESS_IN_CONTROL_PANEL',2),(172,'128','CONFIGURATION',4),(173,'128','VIEW',1),(174,'com.liferay.portal.model.Role','ASSIGN_MEMBERS',2),(175,'com.liferay.portal.model.Role','DEFINE_PERMISSIONS',4),(176,'com.liferay.portal.model.Role','DELETE',8),(177,'com.liferay.portal.model.Role','MANAGE_ANNOUNCEMENTS',16),(178,'com.liferay.portal.model.Role','PERMISSIONS',32),(179,'com.liferay.portal.model.Role','UPDATE',64),(180,'com.liferay.portal.model.Role','VIEW',1),(181,'86','VIEW',1),(182,'86','ADD_TO_PAGE',2),(183,'86','CONFIGURATION',4),(184,'154','ACCESS_IN_CONTROL_PANEL',2),(185,'154','CONFIGURATION',4),(186,'154','VIEW',1),(187,'148','VIEW',1),(188,'148','ADD_TO_PAGE',2),(189,'148','CONFIGURATION',4),(190,'11','ADD_TO_PAGE',2),(191,'11','CONFIGURATION',4),(192,'11','VIEW',1),(193,'120','VIEW',1),(194,'120','ADD_TO_PAGE',2),(195,'120','CONFIGURATION',4),(196,'29','ADD_TO_PAGE',2),(197,'29','CONFIGURATION',4),(198,'29','VIEW',1),(199,'158','ACCESS_IN_CONTROL_PANEL',2),(200,'158','CONFIGURATION',4),(201,'158','VIEW',1),(202,'124','VIEW',1),(203,'124','ADD_TO_PAGE',2),(204,'124','CONFIGURATION',4),(205,'8','ACCESS_IN_CONTROL_PANEL',2),(206,'8','ADD_TO_PAGE',4),(207,'8','CONFIGURATION',8),(208,'8','VIEW',1),(209,'com.liferay.portlet.calendar','ADD_EVENT',2),(210,'com.liferay.portlet.calendar','EXPORT_ALL_EVENTS',4),(211,'com.liferay.portlet.calendar','PERMISSIONS',8),(212,'com.liferay.portlet.calendar.model.CalEvent','ADD_DISCUSSION',2),(213,'com.liferay.portlet.calendar.model.CalEvent','DELETE',4),(214,'com.liferay.portlet.calendar.model.CalEvent','DELETE_DISCUSSION',8),(215,'com.liferay.portlet.calendar.model.CalEvent','PERMISSIONS',16),(216,'com.liferay.portlet.calendar.model.CalEvent','UPDATE',32),(217,'com.liferay.portlet.calendar.model.CalEvent','UPDATE_DISCUSSION',64),(218,'com.liferay.portlet.calendar.model.CalEvent','VIEW',1),(219,'58','ADD_TO_PAGE',2),(220,'58','CONFIGURATION',4),(221,'58','VIEW',1),(222,'155','ACCESS_IN_CONTROL_PANEL',2),(223,'155','VIEW',1),(224,'155','CONFIGURATION',4),(225,'97','VIEW',1),(226,'97','ADD_TO_PAGE',2),(227,'97','CONFIGURATION',4),(228,'71','ADD_TO_PAGE',2),(229,'71','CONFIGURATION',4),(230,'71','VIEW',1),(231,'39','VIEW',1),(232,'39','ADD_TO_PAGE',2),(233,'39','CONFIGURATION',4),(234,'85','ADD_TO_PAGE',2),(235,'85','CONFIGURATION',4),(236,'85','VIEW',1),(237,'118','VIEW',1),(238,'118','ADD_TO_PAGE',2),(239,'118','CONFIGURATION',4),(240,'107','VIEW',1),(241,'107','ADD_TO_PAGE',2),(242,'107','CONFIGURATION',4),(243,'79','CONFIGURATION',2),(244,'79','VIEW',1),(245,'79','ADD_TO_PAGE',4),(246,'30','VIEW',1),(247,'30','ADD_TO_PAGE',2),(248,'30','CONFIGURATION',4),(249,'147','ACCESS_IN_CONTROL_PANEL',2),(250,'147','CONFIGURATION',4),(251,'147','VIEW',1),(252,'48','VIEW',1),(253,'48','ADD_TO_PAGE',2),(254,'48','CONFIGURATION',4),(255,'125','ACCESS_IN_CONTROL_PANEL',2),(256,'125','CONFIGURATION',4),(257,'125','EXPORT_USER',8),(258,'125','VIEW',1),(259,'com.liferay.portal.model.User','DELETE',2),(260,'com.liferay.portal.model.User','IMPERSONATE',4),(261,'com.liferay.portal.model.User','PERMISSIONS',8),(262,'com.liferay.portal.model.User','UPDATE',16),(263,'com.liferay.portal.model.User','VIEW',1),(264,'144','VIEW',1),(265,'144','ADD_TO_PAGE',2),(266,'144','CONFIGURATION',4),(267,'146','ACCESS_IN_CONTROL_PANEL',2),(268,'146','CONFIGURATION',4),(269,'146','VIEW',1),(270,'62','VIEW',1),(271,'62','ADD_TO_PAGE',2),(272,'62','CONFIGURATION',4),(273,'159','VIEW',1),(274,'159','ADD_TO_PAGE',2),(275,'159','CONFIGURATION',4),(276,'108','VIEW',1),(277,'108','ADD_TO_PAGE',2),(278,'108','CONFIGURATION',4),(279,'139','ADD_EXPANDO',2),(280,'139','CONFIGURATION',4),(281,'139','VIEW',1),(282,'139','ADD_TO_PAGE',8),(283,'com.liferay.portlet.expando.model.ExpandoColumn','DELETE',2),(284,'com.liferay.portlet.expando.model.ExpandoColumn','PERMISSIONS',4),(285,'com.liferay.portlet.expando.model.ExpandoColumn','UPDATE',8),(286,'com.liferay.portlet.expando.model.ExpandoColumn','VIEW',1),(287,'84','ADD_ENTRY',2),(288,'84','ADD_TO_PAGE',4),(289,'84','CONFIGURATION',8),(290,'84','VIEW',1),(291,'com.liferay.portlet.announcements.model.AnnouncementsEntry','DELETE',2),(292,'com.liferay.portlet.announcements.model.AnnouncementsEntry','UPDATE',4),(293,'com.liferay.portlet.announcements.model.AnnouncementsEntry','VIEW',1),(294,'com.liferay.portlet.announcements.model.AnnouncementsEntry','PERMISSIONS',8),(295,'101','VIEW',1),(296,'101','ADD_TO_PAGE',2),(297,'101','CONFIGURATION',4),(298,'121','VIEW',1),(299,'121','ADD_TO_PAGE',2),(300,'121','CONFIGURATION',4),(301,'49','VIEW',1),(302,'49','ADD_TO_PAGE',2),(303,'49','CONFIGURATION',4),(304,'143','VIEW',1),(305,'143','ADD_TO_PAGE',2),(306,'143','CONFIGURATION',4),(307,'37','VIEW',1),(308,'37','ADD_TO_PAGE',2),(309,'37','CONFIGURATION',4),(310,'77','VIEW',1),(311,'77','ADD_TO_PAGE',2),(312,'77','CONFIGURATION',4),(313,'115','VIEW',1),(314,'115','ADD_TO_PAGE',2),(315,'115','CONFIGURATION',4),(316,'56','ADD_TO_PAGE',2),(317,'56','CONFIGURATION',4),(318,'56','VIEW',1),(319,'142','VIEW',1),(320,'142','ADD_TO_PAGE',2),(321,'142','CONFIGURATION',4),(322,'111','VIEW',1),(323,'111','ADD_TO_PAGE',2),(324,'111','CONFIGURATION',4),(325,'16','PREFERENCES',2),(326,'16','GUEST_PREFERENCES',4),(327,'16','VIEW',1),(328,'16','ADD_TO_PAGE',8),(329,'16','CONFIGURATION',16),(330,'3','VIEW',1),(331,'3','ADD_TO_PAGE',2),(332,'3','CONFIGURATION',4),(333,'20','ACCESS_IN_CONTROL_PANEL',2),(334,'20','ADD_TO_PAGE',4),(335,'20','CONFIGURATION',8),(336,'20','VIEW',1),(337,'com.liferay.portlet.documentlibrary.model.DLFolder','ACCESS',2),(338,'com.liferay.portlet.documentlibrary.model.DLFolder','ADD_DOCUMENT',4),(339,'com.liferay.portlet.documentlibrary.model.DLFolder','ADD_SHORTCUT',8),(340,'com.liferay.portlet.documentlibrary.model.DLFolder','ADD_SUBFOLDER',16),(341,'com.liferay.portlet.documentlibrary.model.DLFolder','DELETE',32),(342,'com.liferay.portlet.documentlibrary.model.DLFolder','PERMISSIONS',64),(343,'com.liferay.portlet.documentlibrary.model.DLFolder','UPDATE',128),(344,'com.liferay.portlet.documentlibrary.model.DLFolder','VIEW',1),(345,'com.liferay.portlet.documentlibrary','ADD_DOCUMENT',2),(346,'com.liferay.portlet.documentlibrary','ADD_FOLDER',4),(347,'com.liferay.portlet.documentlibrary','ADD_SHORTCUT',8),(348,'com.liferay.portlet.documentlibrary','PERMISSIONS',16),(349,'com.liferay.portlet.documentlibrary','VIEW',1),(350,'com.liferay.portlet.documentlibrary.model.DLFileEntry','ADD_DISCUSSION',2),(351,'com.liferay.portlet.documentlibrary.model.DLFileEntry','DELETE',4),(352,'com.liferay.portlet.documentlibrary.model.DLFileEntry','DELETE_DISCUSSION',8),(353,'com.liferay.portlet.documentlibrary.model.DLFileEntry','PERMISSIONS',16),(354,'com.liferay.portlet.documentlibrary.model.DLFileEntry','UPDATE',32),(355,'com.liferay.portlet.documentlibrary.model.DLFileEntry','UPDATE_DISCUSSION',64),(356,'com.liferay.portlet.documentlibrary.model.DLFileEntry','VIEW',1),(357,'com.liferay.portlet.documentlibrary.model.DLFileShortcut','ADD_DISCUSSION',2),(358,'com.liferay.portlet.documentlibrary.model.DLFileShortcut','DELETE',4),(359,'com.liferay.portlet.documentlibrary.model.DLFileShortcut','DELETE_DISCUSSION',8),(360,'com.liferay.portlet.documentlibrary.model.DLFileShortcut','PERMISSIONS',16),(361,'com.liferay.portlet.documentlibrary.model.DLFileShortcut','UPDATE',32),(362,'com.liferay.portlet.documentlibrary.model.DLFileShortcut','UPDATE_DISCUSSION',64),(363,'com.liferay.portlet.documentlibrary.model.DLFileShortcut','VIEW',1),(364,'23','VIEW',1),(365,'23','ADD_TO_PAGE',2),(366,'23','CONFIGURATION',4),(367,'145','VIEW',1),(368,'145','ADD_TO_PAGE',2),(369,'145','CONFIGURATION',4),(370,'com.liferay.portlet.asset','ADD_CATEGORY',2),(371,'com.liferay.portlet.asset','ADD_VOCABULARY',4),(372,'com.liferay.portlet.asset','ADD_TAG',8),(373,'com.liferay.portlet.asset','PERMISSIONS',16),(374,'com.liferay.portlet.asset.model.AssetCategory','ADD_CATEGORY',2),(375,'com.liferay.portlet.asset.model.AssetCategory','DELETE',4),(376,'com.liferay.portlet.asset.model.AssetCategory','PERMISSIONS',8),(377,'com.liferay.portlet.asset.model.AssetCategory','UPDATE',16),(378,'com.liferay.portlet.asset.model.AssetCategory','VIEW',1),(379,'com.liferay.portlet.asset.model.AssetVocabulary','DELETE',2),(380,'com.liferay.portlet.asset.model.AssetVocabulary','PERMISSIONS',4),(381,'com.liferay.portlet.asset.model.AssetVocabulary','UPDATE',8),(382,'com.liferay.portlet.asset.model.AssetVocabulary','VIEW',1),(383,'83','ADD_ENTRY',2),(384,'83','ADD_TO_PAGE',4),(385,'83','CONFIGURATION',8),(386,'83','VIEW',1),(387,'99','VIEW',1),(388,'99','ADD_TO_PAGE',2),(389,'99','CONFIGURATION',4),(390,'com.liferay.portlet.asset.model.AssetTag','DELETE',2),(391,'com.liferay.portlet.asset.model.AssetTag','PERMISSIONS',4),(392,'com.liferay.portlet.asset.model.AssetTag','UPDATE',8),(393,'com.liferay.portlet.asset.model.AssetTag','VIEW',1),(394,'70','VIEW',1),(395,'70','ADD_TO_PAGE',2),(396,'70','CONFIGURATION',4),(397,'141','VIEW',1),(398,'141','ADD_TO_PAGE',2),(399,'141','CONFIGURATION',4),(400,'9','VIEW',1),(401,'9','ADD_TO_PAGE',2),(402,'9','CONFIGURATION',4),(403,'137','ACCESS_IN_CONTROL_PANEL',2),(404,'137','CONFIGURATION',4),(405,'137','VIEW',1),(406,'28','ACCESS_IN_CONTROL_PANEL',2),(407,'28','ADD_TO_PAGE',4),(408,'28','CONFIGURATION',8),(409,'28','VIEW',1),(410,'com.liferay.portlet.bookmarks.model.BookmarksFolder','ACCESS',2),(411,'com.liferay.portlet.bookmarks.model.BookmarksFolder','ADD_ENTRY',4),(412,'com.liferay.portlet.bookmarks.model.BookmarksFolder','ADD_SUBFOLDER',8),(413,'com.liferay.portlet.bookmarks.model.BookmarksFolder','DELETE',16),(414,'com.liferay.portlet.bookmarks.model.BookmarksFolder','PERMISSIONS',32),(415,'com.liferay.portlet.bookmarks.model.BookmarksFolder','UPDATE',64),(416,'com.liferay.portlet.bookmarks.model.BookmarksFolder','VIEW',1),(417,'com.liferay.portlet.bookmarks','ADD_ENTRY',2),(418,'com.liferay.portlet.bookmarks','ADD_FOLDER',4),(419,'com.liferay.portlet.bookmarks','PERMISSIONS',8),(420,'com.liferay.portlet.bookmarks.model.BookmarksEntry','DELETE',2),(421,'com.liferay.portlet.bookmarks.model.BookmarksEntry','PERMISSIONS',4),(422,'com.liferay.portlet.bookmarks.model.BookmarksEntry','UPDATE',8),(423,'com.liferay.portlet.bookmarks.model.BookmarksEntry','VIEW',1),(424,'133','VIEW',1),(425,'133','ADD_TO_PAGE',2),(426,'133','CONFIGURATION',4),(427,'116','VIEW',1),(428,'116','ADD_TO_PAGE',2),(429,'116','CONFIGURATION',4),(430,'15','ACCESS_IN_CONTROL_PANEL',2),(431,'15','ADD_TO_PAGE',4),(432,'15','CONFIGURATION',8),(433,'15','VIEW',1),(434,'com.liferay.portlet.journal.model.JournalFeed','DELETE',2),(435,'com.liferay.portlet.journal.model.JournalFeed','PERMISSIONS',4),(436,'com.liferay.portlet.journal.model.JournalFeed','UPDATE',8),(437,'com.liferay.portlet.journal.model.JournalFeed','VIEW',1),(438,'com.liferay.portlet.journal.model.JournalArticle','ADD_DISCUSSION',2),(439,'com.liferay.portlet.journal.model.JournalArticle','DELETE',4),(440,'com.liferay.portlet.journal.model.JournalArticle','DELETE_DISCUSSION',8),(441,'com.liferay.portlet.journal.model.JournalArticle','EXPIRE',16),(442,'com.liferay.portlet.journal.model.JournalArticle','PERMISSIONS',32),(443,'com.liferay.portlet.journal.model.JournalArticle','UPDATE',64),(444,'com.liferay.portlet.journal.model.JournalArticle','UPDATE_DISCUSSION',128),(445,'com.liferay.portlet.journal.model.JournalArticle','VIEW',1),(446,'com.liferay.portlet.journal','ADD_ARTICLE',2),(447,'com.liferay.portlet.journal','ADD_FEED',4),(448,'com.liferay.portlet.journal','ADD_STRUCTURE',8),(449,'com.liferay.portlet.journal','ADD_TEMPLATE',16),(450,'com.liferay.portlet.journal','SUBSCRIBE',32),(451,'com.liferay.portlet.journal','PERMISSIONS',64),(452,'com.liferay.portlet.journal.model.JournalStructure','DELETE',2),(453,'com.liferay.portlet.journal.model.JournalStructure','PERMISSIONS',4),(454,'com.liferay.portlet.journal.model.JournalStructure','UPDATE',8),(455,'com.liferay.portlet.journal.model.JournalStructure','VIEW',1),(456,'com.liferay.portlet.journal.model.JournalTemplate','DELETE',2),(457,'com.liferay.portlet.journal.model.JournalTemplate','PERMISSIONS',4),(458,'com.liferay.portlet.journal.model.JournalTemplate','UPDATE',8),(459,'com.liferay.portlet.journal.model.JournalTemplate','VIEW',1),(460,'47','VIEW',1),(461,'47','ADD_TO_PAGE',2),(462,'47','CONFIGURATION',4),(463,'82','VIEW',1),(464,'82','ADD_TO_PAGE',2),(465,'82','CONFIGURATION',4),(466,'103','VIEW',1),(467,'103','ADD_TO_PAGE',2),(468,'103','CONFIGURATION',4),(469,'151','ACCESS_IN_CONTROL_PANEL',2),(470,'151','CONFIGURATION',4),(471,'151','VIEW',1),(472,'140','VIEW',1),(473,'140','ADD_TO_PAGE',2),(474,'140','CONFIGURATION',4),(475,'54','VIEW',1),(476,'54','ADD_TO_PAGE',2),(477,'54','CONFIGURATION',4),(478,'132','ACCESS_IN_CONTROL_PANEL',2),(479,'132','CONFIGURATION',4),(480,'132','VIEW',1),(481,'34','ADD_TO_PAGE',2),(482,'34','CONFIGURATION',4),(483,'34','VIEW',1),(484,'com.liferay.portlet.shopping','ADD_CATEGORY',2),(485,'com.liferay.portlet.shopping','ADD_ITEM',4),(486,'com.liferay.portlet.shopping','MANAGE_COUPONS',8),(487,'com.liferay.portlet.shopping','MANAGE_ORDERS',16),(488,'com.liferay.portlet.shopping','PERMISSIONS',32),(489,'com.liferay.portlet.shopping.model.ShoppingCategory','ADD_ITEM',2),(490,'com.liferay.portlet.shopping.model.ShoppingCategory','ADD_SUBCATEGORY',4),(491,'com.liferay.portlet.shopping.model.ShoppingCategory','DELETE',8),(492,'com.liferay.portlet.shopping.model.ShoppingCategory','PERMISSIONS',16),(493,'com.liferay.portlet.shopping.model.ShoppingCategory','UPDATE',32),(494,'com.liferay.portlet.shopping.model.ShoppingCategory','VIEW',1),(495,'com.liferay.portlet.shopping.model.ShoppingOrder','DELETE',2),(496,'com.liferay.portlet.shopping.model.ShoppingOrder','PERMISSIONS',4),(497,'com.liferay.portlet.shopping.model.ShoppingOrder','UPDATE',8),(498,'com.liferay.portlet.shopping.model.ShoppingOrder','VIEW',1),(499,'com.liferay.portlet.shopping.model.ShoppingItem','DELETE',2),(500,'com.liferay.portlet.shopping.model.ShoppingItem','PERMISSIONS',4),(501,'com.liferay.portlet.shopping.model.ShoppingItem','UPDATE',8),(502,'com.liferay.portlet.shopping.model.ShoppingItem','VIEW',1),(503,'61','VIEW',1),(504,'61','ADD_TO_PAGE',2),(505,'61','CONFIGURATION',4),(506,'73','ADD_TO_PAGE',2),(507,'73','CONFIGURATION',4),(508,'73','VIEW',1),(509,'31','ACCESS_IN_CONTROL_PANEL',2),(510,'31','ADD_TO_PAGE',4),(511,'31','CONFIGURATION',8),(512,'31','VIEW',1),(513,'com.liferay.portlet.imagegallery.model.IGImage','DELETE',2),(514,'com.liferay.portlet.imagegallery.model.IGImage','PERMISSIONS',4),(515,'com.liferay.portlet.imagegallery.model.IGImage','UPDATE',8),(516,'com.liferay.portlet.imagegallery.model.IGImage','VIEW',1),(517,'com.liferay.portlet.imagegallery.model.IGFolder','ACCESS',2),(518,'com.liferay.portlet.imagegallery.model.IGFolder','ADD_IMAGE',4),(519,'com.liferay.portlet.imagegallery.model.IGFolder','ADD_SUBFOLDER',8),(520,'com.liferay.portlet.imagegallery.model.IGFolder','DELETE',16),(521,'com.liferay.portlet.imagegallery.model.IGFolder','PERMISSIONS',32),(522,'com.liferay.portlet.imagegallery.model.IGFolder','UPDATE',64),(523,'com.liferay.portlet.imagegallery.model.IGFolder','VIEW',1),(524,'com.liferay.portlet.imagegallery','ADD_FOLDER',2),(525,'com.liferay.portlet.imagegallery','ADD_IMAGE',4),(526,'com.liferay.portlet.imagegallery','PERMISSIONS',8),(527,'com.liferay.portlet.imagegallery','VIEW',1),(528,'136','ACCESS_IN_CONTROL_PANEL',2),(529,'136','CONFIGURATION',4),(530,'136','VIEW',1),(531,'127','ACCESS_IN_CONTROL_PANEL',2),(532,'127','CONFIGURATION',4),(533,'127','VIEW',1),(534,'com.liferay.portal.model.UserGroup','ASSIGN_MEMBERS',2),(535,'com.liferay.portal.model.UserGroup','DELETE',4),(536,'com.liferay.portal.model.UserGroup','MANAGE_ANNOUNCEMENTS',8),(537,'com.liferay.portal.model.UserGroup','PERMISSIONS',16),(538,'com.liferay.portal.model.UserGroup','MANAGE_LAYOUTS',32),(539,'com.liferay.portal.model.UserGroup','UPDATE',64),(540,'com.liferay.portal.model.UserGroup','VIEW',1),(541,'50','VIEW',1),(542,'50','ADD_TO_PAGE',2),(543,'50','CONFIGURATION',4),(544,'25','ACCESS_IN_CONTROL_PANEL',2),(545,'25','CONFIGURATION',4),(546,'25','VIEW',1),(547,'com.liferay.portlet.polls','ADD_QUESTION',2),(548,'com.liferay.portlet.polls','PERMISSIONS',4),(549,'com.liferay.portlet.polls.model.PollsQuestion','ADD_VOTE',2),(550,'com.liferay.portlet.polls.model.PollsQuestion','DELETE',4),(551,'com.liferay.portlet.polls.model.PollsQuestion','PERMISSIONS',8),(552,'com.liferay.portlet.polls.model.PollsQuestion','UPDATE',16),(553,'com.liferay.portlet.polls.model.PollsQuestion','VIEW',1),(554,'90','ADD_COMMUNITY',2),(555,'90','ADD_LAYOUT_PROTOTYPE',4),(556,'90','ADD_LAYOUT_SET_PROTOTYPE',8),(557,'90','ADD_LICENSE',16),(558,'90','ADD_ORGANIZATION',32),(559,'90','ADD_PASSWORD_POLICY',64),(560,'90','ADD_ROLE',128),(561,'90','ADD_USER',256),(562,'90','ADD_USER_GROUP',512),(563,'90','CONFIGURATION',1024),(564,'90','EXPORT_USER',2048),(565,'150','ACCESS_IN_CONTROL_PANEL',2),(566,'150','CONFIGURATION',4),(567,'150','VIEW',1),(568,'113','VIEW',1),(569,'113','ADD_TO_PAGE',2),(570,'113','CONFIGURATION',4),(571,'33','ACCESS_IN_CONTROL_PANEL',2),(572,'33','ADD_TO_PAGE',4),(573,'33','CONFIGURATION',8),(574,'33','VIEW',1),(575,'com.liferay.portlet.blogs','ADD_ENTRY',2),(576,'com.liferay.portlet.blogs','PERMISSIONS',4),(577,'com.liferay.portlet.blogs','SUBSCRIBE',8),(578,'com.liferay.portlet.blogs.model.BlogsEntry','ADD_DISCUSSION',2),(579,'com.liferay.portlet.blogs.model.BlogsEntry','DELETE',4),(580,'com.liferay.portlet.blogs.model.BlogsEntry','DELETE_DISCUSSION',8),(581,'com.liferay.portlet.blogs.model.BlogsEntry','PERMISSIONS',16),(582,'com.liferay.portlet.blogs.model.BlogsEntry','UPDATE',32),(583,'com.liferay.portlet.blogs.model.BlogsEntry','UPDATE_DISCUSSION',64),(584,'com.liferay.portlet.blogs.model.BlogsEntry','VIEW',1),(585,'2','VIEW',1),(586,'2','ADD_TO_PAGE',2),(587,'2','CONFIGURATION',4),(588,'119','VIEW',1),(589,'119','ADD_TO_PAGE',2),(590,'119','CONFIGURATION',4),(591,'126','ACCESS_IN_CONTROL_PANEL',2),(592,'126','CONFIGURATION',4),(593,'126','VIEW',1),(594,'com.liferay.portal.model.Organization','APPROVE_PROPOSAL',2),(595,'com.liferay.portal.model.Organization','ASSIGN_MEMBERS',4),(596,'com.liferay.portal.model.Organization','ASSIGN_REVIEWER',8),(597,'com.liferay.portal.model.Organization','ASSIGN_USER_ROLES',16),(598,'com.liferay.portal.model.Organization','DELETE',32),(599,'com.liferay.portal.model.Organization','MANAGE_ANNOUNCEMENTS',64),(600,'com.liferay.portal.model.Organization','MANAGE_ARCHIVED_SETUPS',128),(601,'com.liferay.portal.model.Organization','MANAGE_LAYOUTS',256),(602,'com.liferay.portal.model.Organization','MANAGE_STAGING',512),(603,'com.liferay.portal.model.Organization','MANAGE_SUBORGANIZATIONS',1024),(604,'com.liferay.portal.model.Organization','MANAGE_TEAMS',2048),(605,'com.liferay.portal.model.Organization','MANAGE_USERS',4096),(606,'com.liferay.portal.model.Organization','PERMISSIONS',8192),(607,'com.liferay.portal.model.Organization','PUBLISH_STAGING',16384),(608,'com.liferay.portal.model.Organization','UPDATE',32768),(609,'com.liferay.portal.model.Organization','VIEW',1),(610,'114','VIEW',1),(611,'114','ADD_TO_PAGE',2),(612,'114','CONFIGURATION',4),(613,'149','ACCESS_IN_CONTROL_PANEL',2),(614,'149','CONFIGURATION',4),(615,'149','VIEW',1),(616,'67','VIEW',1),(617,'67','ADD_TO_PAGE',2),(618,'67','CONFIGURATION',4),(619,'110','VIEW',1),(620,'110','ADD_TO_PAGE',2),(621,'110','CONFIGURATION',4),(622,'135','ACCESS_IN_CONTROL_PANEL',2),(623,'135','CONFIGURATION',4),(624,'135','VIEW',1),(625,'59','VIEW',1),(626,'59','ADD_TO_PAGE',2),(627,'59','CONFIGURATION',4),(628,'131','ACCESS_IN_CONTROL_PANEL',2),(629,'131','CONFIGURATION',4),(630,'131','VIEW',1),(631,'102','VIEW',1),(632,'102','ADD_TO_PAGE',2),(633,'102','CONFIGURATION',4),(634,'Provision_WAR_prov','VIEW',1),(635,'Provision_WAR_prov','ADD_TO_PAGE',2),(636,'Provision_WAR_prov','CONFIGURATION',4),(637,'provmenu_WAR_provmenu','VIEW',1),(638,'provmenu_WAR_provmenu','ADD_TO_PAGE',2),(639,'provmenu_WAR_provmenu','CONFIGURATION',4),(640,'safaricomprovisioning_WAR_safaricomsms','VIEW',1),(641,'safaricomprovisioning_WAR_safaricomsms','ADD_TO_PAGE',2),(642,'safaricomprovisioning_WAR_safaricomsms','CONFIGURATION',4),(643,'airtelprovisioning_WAR_airtelsms','VIEW',1),(644,'airtelprovisioning_WAR_airtelsms','ADD_TO_PAGE',2),(645,'airtelprovisioning_WAR_airtelsms','CONFIGURATION',4),(701,'subscriptionprovisioning_WAR_subscription','VIEW',1),(702,'subscriptionprovisioning_WAR_subscription','ADD_TO_PAGE',2),(703,'subscriptionprovisioning_WAR_subscription','CONFIGURATION',4),(704,'safaricomsmsprovisioning_WAR_safaricomsms','VIEW',1),(705,'safaricomsmsprovisioning_WAR_safaricomsms','ADD_TO_PAGE',2),(706,'safaricomsmsprovisioning_WAR_safaricomsms','CONFIGURATION',4),(707,'airtelsmsprovisioning_WAR_airtelsms','VIEW',1),(708,'airtelsmsprovisioning_WAR_airtelsms','ADD_TO_PAGE',2),(709,'airtelsmsprovisioning_WAR_airtelsms','CONFIGURATION',4),(710,'airtelwappushprovisioning_WAR_airtelwappush','VIEW',1),(711,'airtelwappushprovisioning_WAR_airtelwappush','ADD_TO_PAGE',2),(712,'airtelwappushprovisioning_WAR_airtelwappush','CONFIGURATION',4),(713,'casprovisioning_WAR_cas','VIEW',1),(714,'casprovisioning_WAR_cas','ADD_TO_PAGE',2),(715,'casprovisioning_WAR_cas','CONFIGURATION',4),(716,'safaricomwappushprovisioning_WAR_safaricomwappush','VIEW',1),(717,'safaricomwappushprovisioning_WAR_safaricomwappush','ADD_TO_PAGE',2),(718,'safaricomwappushprovisioning_WAR_safaricomwappush','CONFIGURATION',4);
/*!40000 ALTER TABLE `ResourceAction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ResourceCode`
--

DROP TABLE IF EXISTS `ResourceCode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ResourceCode` (
  `codeId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `scope` int(11) DEFAULT NULL,
  PRIMARY KEY (`codeId`),
  UNIQUE KEY `IX_A32C097E` (`companyId`,`name`,`scope`),
  KEY `IX_717FDD47` (`companyId`),
  KEY `IX_AACAFF40` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ResourceCode`
--

LOCK TABLES `ResourceCode` WRITE;
/*!40000 ALTER TABLE `ResourceCode` DISABLE KEYS */;
/*!40000 ALTER TABLE `ResourceCode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ResourcePermission`
--

DROP TABLE IF EXISTS `ResourcePermission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ResourcePermission` (
  `resourcePermissionId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `scope` int(11) DEFAULT NULL,
  `primKey` varchar(255) DEFAULT NULL,
  `roleId` bigint(20) DEFAULT NULL,
  `actionIds` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`resourcePermissionId`),
  UNIQUE KEY `IX_8D83D0CE` (`companyId`,`name`,`scope`,`primKey`,`roleId`),
  KEY `IX_60B99860` (`companyId`,`name`,`scope`),
  KEY `IX_2200AA69` (`companyId`,`name`,`scope`,`primKey`),
  KEY `IX_A37A0588` (`roleId`),
  KEY `IX_2F80C17C` (`roleId`,`scope`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ResourcePermission`
--

LOCK TABLES `ResourcePermission` WRITE;
/*!40000 ALTER TABLE `ResourcePermission` DISABLE KEYS */;
INSERT INTO `ResourcePermission` VALUES (1,10132,'com.liferay.portal.model.Layout',4,'10152',10140,127),(2,10132,'com.liferay.portal.model.Layout',4,'10152',10144,3),(3,10132,'com.liferay.portal.model.Layout',4,'10152',10139,1),(4,10132,'com.liferay.portal.model.Layout',4,'10160',10140,127),(5,10132,'com.liferay.portal.model.Layout',4,'10160',10144,3),(6,10132,'com.liferay.portal.model.Layout',4,'10160',10139,1),(7,10132,'com.liferay.portal.model.User',4,'10169',10140,31),(8,10132,'98',1,'10132',10141,4),(9,10132,'98',1,'10132',10142,4),(10,10132,'66',1,'10132',10141,2),(11,10132,'66',1,'10132',10142,2),(12,10132,'27',1,'10132',10141,2),(13,10132,'27',1,'10132',10142,2),(14,10132,'122',1,'10132',10139,2),(15,10132,'122',1,'10132',10141,2),(16,10132,'122',1,'10132',10142,2),(17,10132,'36',1,'10132',10141,2),(18,10132,'36',1,'10132',10142,2),(19,10132,'26',1,'10132',10141,2),(20,10132,'26',1,'10132',10142,2),(21,10132,'104',1,'10132',10138,2),(22,10132,'64',1,'10132',10139,2),(23,10132,'64',1,'10132',10141,2),(24,10132,'64',1,'10132',10142,2),(25,10132,'100',1,'10132',10141,2),(26,10132,'100',1,'10132',10142,2),(27,10132,'19',1,'10132',10141,4),(28,10132,'19',1,'10132',10142,4),(29,10132,'148',1,'10132',10139,2),(30,10132,'148',1,'10132',10141,2),(31,10132,'148',1,'10132',10142,2),(32,10132,'11',1,'10132',10141,2),(33,10132,'11',1,'10132',10142,2),(34,10132,'29',1,'10132',10141,2),(35,10132,'29',1,'10132',10142,2),(36,10132,'8',1,'10132',10141,4),(37,10132,'8',1,'10132',10142,4),(38,10132,'58',1,'10132',10139,2),(39,10132,'58',1,'10132',10141,2),(40,10132,'58',1,'10132',10142,2),(41,10132,'71',1,'10132',10139,2),(42,10132,'71',1,'10132',10141,2),(43,10132,'71',1,'10132',10142,2),(44,10132,'97',1,'10132',10141,2),(45,10132,'97',1,'10132',10142,2),(46,10132,'39',1,'10132',10141,2),(47,10132,'39',1,'10132',10142,2),(48,10132,'85',1,'10132',10139,2),(49,10132,'85',1,'10132',10141,2),(50,10132,'85',1,'10132',10142,2),(51,10132,'118',1,'10132',10139,2),(52,10132,'118',1,'10132',10141,2),(53,10132,'118',1,'10132',10142,2),(54,10132,'79',1,'10132',10138,4),(55,10132,'107',1,'10132',10141,2),(56,10132,'107',1,'10132',10142,2),(57,10132,'30',1,'10132',10141,2),(58,10132,'30',1,'10132',10142,2),(59,10132,'48',1,'10132',10141,2),(60,10132,'48',1,'10132',10142,2),(61,10132,'62',1,'10132',10141,2),(62,10132,'62',1,'10132',10142,2),(63,10132,'159',1,'10132',10139,2),(64,10132,'159',1,'10132',10141,2),(65,10132,'159',1,'10132',10142,2),(66,10132,'108',1,'10132',10141,2),(67,10132,'108',1,'10132',10142,2),(68,10132,'84',1,'10132',10141,4),(69,10132,'84',1,'10132',10142,4),(70,10132,'101',1,'10132',10139,2),(71,10132,'101',1,'10132',10141,2),(72,10132,'101',1,'10132',10142,2),(73,10132,'121',1,'10132',10139,2),(74,10132,'121',1,'10132',10141,2),(75,10132,'121',1,'10132',10142,2),(76,10132,'37',1,'10132',10141,2),(77,10132,'37',1,'10132',10142,2),(78,10132,'143',1,'10132',10139,2),(79,10132,'143',1,'10132',10141,2),(80,10132,'143',1,'10132',10142,2),(81,10132,'77',1,'10132',10139,2),(82,10132,'77',1,'10132',10141,2),(83,10132,'77',1,'10132',10142,2),(84,10132,'115',1,'10132',10139,2),(85,10132,'115',1,'10132',10141,2),(86,10132,'115',1,'10132',10142,2),(87,10132,'56',1,'10132',10139,2),(88,10132,'56',1,'10132',10141,2),(89,10132,'56',1,'10132',10142,2),(90,10132,'16',1,'10132',10141,8),(91,10132,'16',1,'10132',10142,8),(92,10132,'111',1,'10132',10138,2),(93,10132,'3',1,'10132',10139,2),(94,10132,'3',1,'10132',10141,2),(95,10132,'3',1,'10132',10142,2),(96,10132,'23',1,'10132',10141,2),(97,10132,'23',1,'10132',10142,2),(98,10132,'20',1,'10132',10139,4),(99,10132,'20',1,'10132',10141,4),(100,10132,'20',1,'10132',10142,4),(101,10132,'83',1,'10132',10141,4),(102,10132,'83',1,'10132',10142,4),(103,10132,'99',1,'10132',10138,2),(104,10132,'70',1,'10132',10141,2),(105,10132,'70',1,'10132',10142,2),(106,10132,'141',1,'10132',10139,2),(107,10132,'141',1,'10132',10141,2),(108,10132,'141',1,'10132',10142,2),(109,10132,'9',1,'10132',10138,2),(110,10132,'28',1,'10132',10141,4),(111,10132,'28',1,'10132',10142,4),(112,10132,'47',1,'10132',10139,2),(113,10132,'47',1,'10132',10141,2),(114,10132,'47',1,'10132',10142,2),(115,10132,'15',1,'10132',10141,4),(116,10132,'15',1,'10132',10142,4),(117,10132,'116',1,'10132',10139,2),(118,10132,'116',1,'10132',10141,2),(119,10132,'116',1,'10132',10142,2),(120,10132,'82',1,'10132',10139,2),(121,10132,'82',1,'10132',10141,2),(122,10132,'82',1,'10132',10142,2),(123,10132,'54',1,'10132',10141,2),(124,10132,'54',1,'10132',10142,2),(125,10132,'34',1,'10132',10141,2),(126,10132,'34',1,'10132',10142,2),(127,10132,'61',1,'10132',10141,2),(128,10132,'61',1,'10132',10142,2),(129,10132,'73',1,'10132',10139,2),(130,10132,'73',1,'10132',10141,2),(131,10132,'73',1,'10132',10142,2),(132,10132,'31',1,'10132',10139,4),(133,10132,'31',1,'10132',10141,4),(134,10132,'31',1,'10132',10142,4),(135,10132,'50',1,'10132',10139,2),(136,10132,'50',1,'10132',10141,2),(137,10132,'50',1,'10132',10142,2),(138,10132,'33',1,'10132',10139,4),(139,10132,'33',1,'10132',10141,4),(140,10132,'33',1,'10132',10142,4),(141,10132,'114',1,'10132',10139,2),(142,10132,'114',1,'10132',10141,2),(143,10132,'114',1,'10132',10142,2),(144,10132,'67',1,'10132',10141,2),(145,10132,'67',1,'10132',10142,2),(146,10132,'110',1,'10132',10141,2),(147,10132,'110',1,'10132',10142,2),(148,10132,'59',1,'10132',10141,2),(149,10132,'59',1,'10132',10142,2),(150,10132,'102',1,'10132',10139,2),(151,10132,'102',1,'10132',10141,2),(152,10132,'102',1,'10132',10142,2),(153,10132,'Provision_WAR_prov',1,'10132',10138,2),(154,10132,'Provision_WAR_prov',1,'10132',10139,2),(155,10132,'Provision_WAR_prov',1,'10132',10141,2),(156,10132,'Provision_WAR_prov',1,'10132',10142,2),(157,10132,'provmenu_WAR_provmenu',1,'10132',10138,2),(158,10132,'provmenu_WAR_provmenu',1,'10132',10139,2),(159,10132,'provmenu_WAR_provmenu',1,'10132',10141,2),(160,10132,'provmenu_WAR_provmenu',1,'10132',10142,2),(161,10132,'safaricomprovisioning_WAR_safaricomsms',1,'10132',10138,2),(162,10132,'safaricomprovisioning_WAR_safaricomsms',1,'10132',10139,2),(163,10132,'safaricomprovisioning_WAR_safaricomsms',1,'10132',10141,2),(164,10132,'safaricomprovisioning_WAR_safaricomsms',1,'10132',10142,2),(165,10132,'airtelprovisioning_WAR_airtelsms',1,'10132',10138,2),(166,10132,'airtelprovisioning_WAR_airtelsms',1,'10132',10139,2),(167,10132,'airtelprovisioning_WAR_airtelsms',1,'10132',10141,2),(168,10132,'airtelprovisioning_WAR_airtelsms',1,'10132',10142,2),(169,10132,'103',4,'10160_LAYOUT_103',10140,7),(170,10132,'103',4,'10160_LAYOUT_103',10144,1),(171,10132,'103',4,'10160_LAYOUT_103',10139,1),(178,10132,'provmenu_WAR_provmenu',4,'10160_LAYOUT_provmenu_WAR_provmenu_INSTANCE_abcd',10140,7),(179,10132,'provmenu_WAR_provmenu',4,'10160_LAYOUT_provmenu_WAR_provmenu_INSTANCE_abcd',10144,1),(180,10132,'provmenu_WAR_provmenu',4,'10160_LAYOUT_provmenu_WAR_provmenu_INSTANCE_abcd',10139,1),(181,10132,'com.liferay.portal.model.Layout',4,'10271',10140,127),(182,10132,'com.liferay.portal.model.Layout',4,'10271',10141,3),(183,10132,'com.liferay.portal.model.Layout',4,'10271',10139,1),(184,10132,'com.liferay.portal.model.Layout',4,'10276',10140,127),(185,10132,'com.liferay.portal.model.Layout',4,'10276',10141,3),(186,10132,'com.liferay.portal.model.Layout',4,'10276',10139,1),(187,10132,'145',4,'10160_LAYOUT_145',10140,7),(188,10132,'145',4,'10160_LAYOUT_145',10144,1),(189,10132,'145',4,'10160_LAYOUT_145',10139,1),(190,10132,'com.liferay.portlet.asset',4,'10157',10140,30),(191,10132,'88',4,'10160_LAYOUT_88',10140,7),(192,10132,'88',4,'10160_LAYOUT_88',10144,1),(193,10132,'88',4,'10160_LAYOUT_88',10139,1),(194,10132,'87',4,'10160_LAYOUT_87',10140,7),(195,10132,'87',4,'10160_LAYOUT_87',10144,1),(196,10132,'87',4,'10160_LAYOUT_87',10139,1),(197,10132,'Provision_WAR_prov',4,'10160_LAYOUT_Provision_WAR_prov',10140,7),(198,10132,'Provision_WAR_prov',4,'10160_LAYOUT_Provision_WAR_prov',10144,1),(199,10132,'Provision_WAR_prov',4,'10160_LAYOUT_Provision_WAR_prov',10139,1),(200,10132,'58',4,'10160_LAYOUT_58',10140,7),(201,10132,'58',4,'10160_LAYOUT_58',10144,1),(202,10132,'58',4,'10160_LAYOUT_58',10139,1),(301,10132,'subscriptionprovisioning_WAR_subscription',1,'10132',10138,2),(302,10132,'subscriptionprovisioning_WAR_subscription',1,'10132',10139,2),(303,10132,'subscriptionprovisioning_WAR_subscription',1,'10132',10141,2),(304,10132,'subscriptionprovisioning_WAR_subscription',1,'10132',10142,2),(305,10132,'safaricomsmsprovisioning_WAR_safaricomsms',1,'10132',10138,2),(306,10132,'safaricomsmsprovisioning_WAR_safaricomsms',1,'10132',10139,2),(307,10132,'safaricomsmsprovisioning_WAR_safaricomsms',1,'10132',10141,2),(308,10132,'safaricomsmsprovisioning_WAR_safaricomsms',1,'10132',10142,2),(309,10132,'airtelsmsprovisioning_WAR_airtelsms',1,'10132',10138,2),(310,10132,'airtelsmsprovisioning_WAR_airtelsms',1,'10132',10139,2),(311,10132,'airtelsmsprovisioning_WAR_airtelsms',1,'10132',10141,2),(312,10132,'airtelsmsprovisioning_WAR_airtelsms',1,'10132',10142,2),(313,10132,'airtelwappushprovisioning_WAR_airtelwappush',1,'10132',10138,2),(314,10132,'airtelwappushprovisioning_WAR_airtelwappush',1,'10132',10139,2),(315,10132,'airtelwappushprovisioning_WAR_airtelwappush',1,'10132',10141,2),(316,10132,'airtelwappushprovisioning_WAR_airtelwappush',1,'10132',10142,2),(317,10132,'casprovisioning_WAR_cas',1,'10132',10138,2),(318,10132,'casprovisioning_WAR_cas',1,'10132',10139,2),(319,10132,'casprovisioning_WAR_cas',1,'10132',10141,2),(320,10132,'casprovisioning_WAR_cas',1,'10132',10142,2),(321,10132,'safaricomwappushprovisioning_WAR_safaricomwappush',1,'10132',10138,2),(322,10132,'safaricomwappushprovisioning_WAR_safaricomwappush',1,'10132',10139,2),(323,10132,'safaricomwappushprovisioning_WAR_safaricomwappush',1,'10132',10141,2),(324,10132,'safaricomwappushprovisioning_WAR_safaricomwappush',1,'10132',10142,2),(401,10132,'provmenu_WAR_provmenu',4,'10160_LAYOUT_provmenu_WAR_provmenu_INSTANCE_8Vt1',10140,7),(402,10132,'provmenu_WAR_provmenu',4,'10160_LAYOUT_provmenu_WAR_provmenu_INSTANCE_8Vt1',10144,1),(403,10132,'provmenu_WAR_provmenu',4,'10160_LAYOUT_provmenu_WAR_provmenu_INSTANCE_8Vt1',10139,1);
/*!40000 ALTER TABLE `ResourcePermission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Resource_`
--

DROP TABLE IF EXISTS `Resource_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Resource_` (
  `resourceId` bigint(20) NOT NULL,
  `codeId` bigint(20) DEFAULT NULL,
  `primKey` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`resourceId`),
  UNIQUE KEY `IX_67DE7856` (`codeId`,`primKey`),
  KEY `IX_2578FBD3` (`codeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Resource_`
--

LOCK TABLES `Resource_` WRITE;
/*!40000 ALTER TABLE `Resource_` DISABLE KEYS */;
/*!40000 ALTER TABLE `Resource_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Role_`
--

DROP TABLE IF EXISTS `Role_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Role_` (
  `roleId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `title` longtext,
  `description` longtext,
  `type_` int(11) DEFAULT NULL,
  `subtype` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`roleId`),
  UNIQUE KEY `IX_A88E424E` (`companyId`,`classNameId`,`classPK`),
  UNIQUE KEY `IX_EBC931B8` (`companyId`,`name`),
  KEY `IX_449A10B9` (`companyId`),
  KEY `IX_5EB4E2FB` (`subtype`),
  KEY `IX_CBE204` (`type_`,`subtype`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Role_`
--

LOCK TABLES `Role_` WRITE;
/*!40000 ALTER TABLE `Role_` DISABLE KEYS */;
INSERT INTO `Role_` VALUES (10138,10132,10040,10138,'Administrator','','Administrators are super users who can do anything.',1,''),(10139,10132,10040,10139,'Guest','','Unauthenticated users always have this role.',1,''),(10140,10132,10040,10140,'Owner','','This is an implied role with respect to the objects users create.',1,''),(10141,10132,10040,10141,'Power User','','Power Users have their own public and private pages.',1,''),(10142,10132,10040,10142,'User','','Authenticated users should be assigned this role.',1,''),(10143,10132,10040,10143,'Community Administrator','','Community Administrators are super users of their community but cannot make other users into Community Administrators.',2,''),(10144,10132,10040,10144,'Community Member','','All users who belong to a community have this role within that community.',2,''),(10145,10132,10040,10145,'Community Owner','','Community Owners are super users of their community and can assign community roles to users.',2,''),(10146,10132,10040,10146,'Organization Administrator','','Organization Administrators are super users of their organization but cannot make other users into Organization Administrators.',3,''),(10147,10132,10040,10147,'Organization Member','','All users who belong to an organization have this role within that organization.',3,''),(10148,10132,10040,10148,'Organization Owner','','Organization Owners are super users of their organization and can assign organization roles to users.',3,'');
/*!40000 ALTER TABLE `Role_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Roles_Permissions`
--

DROP TABLE IF EXISTS `Roles_Permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Roles_Permissions` (
  `roleId` bigint(20) NOT NULL,
  `permissionId` bigint(20) NOT NULL,
  PRIMARY KEY (`roleId`,`permissionId`),
  KEY `IX_7A3619C6` (`permissionId`),
  KEY `IX_E04E486D` (`roleId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Roles_Permissions`
--

LOCK TABLES `Roles_Permissions` WRITE;
/*!40000 ALTER TABLE `Roles_Permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `Roles_Permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SCFrameworkVersi_SCProductVers`
--

DROP TABLE IF EXISTS `SCFrameworkVersi_SCProductVers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SCFrameworkVersi_SCProductVers` (
  `frameworkVersionId` bigint(20) NOT NULL,
  `productVersionId` bigint(20) NOT NULL,
  PRIMARY KEY (`frameworkVersionId`,`productVersionId`),
  KEY `IX_3BB93ECA` (`frameworkVersionId`),
  KEY `IX_E8D33FF9` (`productVersionId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SCFrameworkVersi_SCProductVers`
--

LOCK TABLES `SCFrameworkVersi_SCProductVers` WRITE;
/*!40000 ALTER TABLE `SCFrameworkVersi_SCProductVers` DISABLE KEYS */;
/*!40000 ALTER TABLE `SCFrameworkVersi_SCProductVers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SCFrameworkVersion`
--

DROP TABLE IF EXISTS `SCFrameworkVersion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SCFrameworkVersion` (
  `frameworkVersionId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `url` longtext,
  `active_` tinyint(4) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`frameworkVersionId`),
  KEY `IX_C98C0D78` (`companyId`),
  KEY `IX_272991FA` (`groupId`),
  KEY `IX_6E1764F` (`groupId`,`active_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SCFrameworkVersion`
--

LOCK TABLES `SCFrameworkVersion` WRITE;
/*!40000 ALTER TABLE `SCFrameworkVersion` DISABLE KEYS */;
/*!40000 ALTER TABLE `SCFrameworkVersion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SCLicense`
--

DROP TABLE IF EXISTS `SCLicense`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SCLicense` (
  `licenseId` bigint(20) NOT NULL,
  `name` varchar(75) DEFAULT NULL,
  `url` longtext,
  `openSource` tinyint(4) DEFAULT NULL,
  `active_` tinyint(4) DEFAULT NULL,
  `recommended` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`licenseId`),
  KEY `IX_1C841592` (`active_`),
  KEY `IX_5327BB79` (`active_`,`recommended`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SCLicense`
--

LOCK TABLES `SCLicense` WRITE;
/*!40000 ALTER TABLE `SCLicense` DISABLE KEYS */;
/*!40000 ALTER TABLE `SCLicense` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SCLicenses_SCProductEntries`
--

DROP TABLE IF EXISTS `SCLicenses_SCProductEntries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SCLicenses_SCProductEntries` (
  `licenseId` bigint(20) NOT NULL,
  `productEntryId` bigint(20) NOT NULL,
  PRIMARY KEY (`licenseId`,`productEntryId`),
  KEY `IX_27006638` (`licenseId`),
  KEY `IX_D7710A66` (`productEntryId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SCLicenses_SCProductEntries`
--

LOCK TABLES `SCLicenses_SCProductEntries` WRITE;
/*!40000 ALTER TABLE `SCLicenses_SCProductEntries` DISABLE KEYS */;
/*!40000 ALTER TABLE `SCLicenses_SCProductEntries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SCProductEntry`
--

DROP TABLE IF EXISTS `SCProductEntry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SCProductEntry` (
  `productEntryId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `type_` varchar(75) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `shortDescription` longtext,
  `longDescription` longtext,
  `pageURL` longtext,
  `author` varchar(75) DEFAULT NULL,
  `repoGroupId` varchar(75) DEFAULT NULL,
  `repoArtifactId` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`productEntryId`),
  KEY `IX_5D25244F` (`companyId`),
  KEY `IX_72F87291` (`groupId`),
  KEY `IX_98E6A9CB` (`groupId`,`userId`),
  KEY `IX_7311E812` (`repoGroupId`,`repoArtifactId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SCProductEntry`
--

LOCK TABLES `SCProductEntry` WRITE;
/*!40000 ALTER TABLE `SCProductEntry` DISABLE KEYS */;
/*!40000 ALTER TABLE `SCProductEntry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SCProductScreenshot`
--

DROP TABLE IF EXISTS `SCProductScreenshot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SCProductScreenshot` (
  `productScreenshotId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `productEntryId` bigint(20) DEFAULT NULL,
  `thumbnailId` bigint(20) DEFAULT NULL,
  `fullImageId` bigint(20) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`productScreenshotId`),
  KEY `IX_AE8224CC` (`fullImageId`),
  KEY `IX_467956FD` (`productEntryId`),
  KEY `IX_DA913A55` (`productEntryId`,`priority`),
  KEY `IX_6C572DAC` (`thumbnailId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SCProductScreenshot`
--

LOCK TABLES `SCProductScreenshot` WRITE;
/*!40000 ALTER TABLE `SCProductScreenshot` DISABLE KEYS */;
/*!40000 ALTER TABLE `SCProductScreenshot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SCProductVersion`
--

DROP TABLE IF EXISTS `SCProductVersion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SCProductVersion` (
  `productVersionId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `productEntryId` bigint(20) DEFAULT NULL,
  `version` varchar(75) DEFAULT NULL,
  `changeLog` longtext,
  `downloadPageURL` longtext,
  `directDownloadURL` varchar(2000) DEFAULT NULL,
  `repoStoreArtifact` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`productVersionId`),
  KEY `IX_7020130F` (`directDownloadURL`(767)),
  KEY `IX_8377A211` (`productEntryId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SCProductVersion`
--

LOCK TABLES `SCProductVersion` WRITE;
/*!40000 ALTER TABLE `SCProductVersion` DISABLE KEYS */;
/*!40000 ALTER TABLE `SCProductVersion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ServiceComponent`
--

DROP TABLE IF EXISTS `ServiceComponent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ServiceComponent` (
  `serviceComponentId` bigint(20) NOT NULL,
  `buildNamespace` varchar(75) DEFAULT NULL,
  `buildNumber` bigint(20) DEFAULT NULL,
  `buildDate` bigint(20) DEFAULT NULL,
  `data_` longtext,
  PRIMARY KEY (`serviceComponentId`),
  UNIQUE KEY `IX_4F0315B8` (`buildNamespace`,`buildNumber`),
  KEY `IX_7338606F` (`buildNamespace`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ServiceComponent`
--

LOCK TABLES `ServiceComponent` WRITE;
/*!40000 ALTER TABLE `ServiceComponent` DISABLE KEYS */;
/*!40000 ALTER TABLE `ServiceComponent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Shard`
--

DROP TABLE IF EXISTS `Shard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Shard` (
  `shardId` bigint(20) NOT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`shardId`),
  KEY `IX_DA5F4359` (`classNameId`,`classPK`),
  KEY `IX_941BA8C3` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Shard`
--

LOCK TABLES `Shard` WRITE;
/*!40000 ALTER TABLE `Shard` DISABLE KEYS */;
INSERT INTO `Shard` VALUES (10133,10008,10132,'default');
/*!40000 ALTER TABLE `Shard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ShoppingCart`
--

DROP TABLE IF EXISTS `ShoppingCart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ShoppingCart` (
  `cartId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `itemIds` longtext,
  `couponCodes` varchar(75) DEFAULT NULL,
  `altShipping` int(11) DEFAULT NULL,
  `insure` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`cartId`),
  UNIQUE KEY `IX_FC46FE16` (`groupId`,`userId`),
  KEY `IX_C28B41DC` (`groupId`),
  KEY `IX_54101CC8` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ShoppingCart`
--

LOCK TABLES `ShoppingCart` WRITE;
/*!40000 ALTER TABLE `ShoppingCart` DISABLE KEYS */;
/*!40000 ALTER TABLE `ShoppingCart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ShoppingCategory`
--

DROP TABLE IF EXISTS `ShoppingCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ShoppingCategory` (
  `categoryId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `parentCategoryId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  PRIMARY KEY (`categoryId`),
  KEY `IX_5F615D3E` (`groupId`),
  KEY `IX_1E6464F5` (`groupId`,`parentCategoryId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ShoppingCategory`
--

LOCK TABLES `ShoppingCategory` WRITE;
/*!40000 ALTER TABLE `ShoppingCategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `ShoppingCategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ShoppingCoupon`
--

DROP TABLE IF EXISTS `ShoppingCoupon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ShoppingCoupon` (
  `couponId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `code_` varchar(75) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `active_` tinyint(4) DEFAULT NULL,
  `limitCategories` longtext,
  `limitSkus` longtext,
  `minOrder` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `discountType` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`couponId`),
  UNIQUE KEY `IX_DC60CFAE` (`code_`),
  KEY `IX_3251AF16` (`groupId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ShoppingCoupon`
--

LOCK TABLES `ShoppingCoupon` WRITE;
/*!40000 ALTER TABLE `ShoppingCoupon` DISABLE KEYS */;
/*!40000 ALTER TABLE `ShoppingCoupon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ShoppingItem`
--

DROP TABLE IF EXISTS `ShoppingItem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ShoppingItem` (
  `itemId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `categoryId` bigint(20) DEFAULT NULL,
  `sku` varchar(75) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `description` longtext,
  `properties` longtext,
  `fields_` tinyint(4) DEFAULT NULL,
  `fieldsQuantities` longtext,
  `minQuantity` int(11) DEFAULT NULL,
  `maxQuantity` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `taxable` tinyint(4) DEFAULT NULL,
  `shipping` double DEFAULT NULL,
  `useShippingFormula` tinyint(4) DEFAULT NULL,
  `requiresShipping` tinyint(4) DEFAULT NULL,
  `stockQuantity` int(11) DEFAULT NULL,
  `featured_` tinyint(4) DEFAULT NULL,
  `sale_` tinyint(4) DEFAULT NULL,
  `smallImage` tinyint(4) DEFAULT NULL,
  `smallImageId` bigint(20) DEFAULT NULL,
  `smallImageURL` longtext,
  `mediumImage` tinyint(4) DEFAULT NULL,
  `mediumImageId` bigint(20) DEFAULT NULL,
  `mediumImageURL` longtext,
  `largeImage` tinyint(4) DEFAULT NULL,
  `largeImageId` bigint(20) DEFAULT NULL,
  `largeImageURL` longtext,
  PRIMARY KEY (`itemId`),
  UNIQUE KEY `IX_1C717CA6` (`companyId`,`sku`),
  KEY `IX_FEFE7D76` (`groupId`,`categoryId`),
  KEY `IX_903DC750` (`largeImageId`),
  KEY `IX_D217AB30` (`mediumImageId`),
  KEY `IX_FF203304` (`smallImageId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ShoppingItem`
--

LOCK TABLES `ShoppingItem` WRITE;
/*!40000 ALTER TABLE `ShoppingItem` DISABLE KEYS */;
/*!40000 ALTER TABLE `ShoppingItem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ShoppingItemField`
--

DROP TABLE IF EXISTS `ShoppingItemField`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ShoppingItemField` (
  `itemFieldId` bigint(20) NOT NULL,
  `itemId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `values_` longtext,
  `description` longtext,
  PRIMARY KEY (`itemFieldId`),
  KEY `IX_6D5F9B87` (`itemId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ShoppingItemField`
--

LOCK TABLES `ShoppingItemField` WRITE;
/*!40000 ALTER TABLE `ShoppingItemField` DISABLE KEYS */;
/*!40000 ALTER TABLE `ShoppingItemField` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ShoppingItemPrice`
--

DROP TABLE IF EXISTS `ShoppingItemPrice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ShoppingItemPrice` (
  `itemPriceId` bigint(20) NOT NULL,
  `itemId` bigint(20) DEFAULT NULL,
  `minQuantity` int(11) DEFAULT NULL,
  `maxQuantity` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `taxable` tinyint(4) DEFAULT NULL,
  `shipping` double DEFAULT NULL,
  `useShippingFormula` tinyint(4) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`itemPriceId`),
  KEY `IX_EA6FD516` (`itemId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ShoppingItemPrice`
--

LOCK TABLES `ShoppingItemPrice` WRITE;
/*!40000 ALTER TABLE `ShoppingItemPrice` DISABLE KEYS */;
/*!40000 ALTER TABLE `ShoppingItemPrice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ShoppingOrder`
--

DROP TABLE IF EXISTS `ShoppingOrder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ShoppingOrder` (
  `orderId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `number_` varchar(75) DEFAULT NULL,
  `tax` double DEFAULT NULL,
  `shipping` double DEFAULT NULL,
  `altShipping` varchar(75) DEFAULT NULL,
  `requiresShipping` tinyint(4) DEFAULT NULL,
  `insure` tinyint(4) DEFAULT NULL,
  `insurance` double DEFAULT NULL,
  `couponCodes` varchar(75) DEFAULT NULL,
  `couponDiscount` double DEFAULT NULL,
  `billingFirstName` varchar(75) DEFAULT NULL,
  `billingLastName` varchar(75) DEFAULT NULL,
  `billingEmailAddress` varchar(75) DEFAULT NULL,
  `billingCompany` varchar(75) DEFAULT NULL,
  `billingStreet` varchar(75) DEFAULT NULL,
  `billingCity` varchar(75) DEFAULT NULL,
  `billingState` varchar(75) DEFAULT NULL,
  `billingZip` varchar(75) DEFAULT NULL,
  `billingCountry` varchar(75) DEFAULT NULL,
  `billingPhone` varchar(75) DEFAULT NULL,
  `shipToBilling` tinyint(4) DEFAULT NULL,
  `shippingFirstName` varchar(75) DEFAULT NULL,
  `shippingLastName` varchar(75) DEFAULT NULL,
  `shippingEmailAddress` varchar(75) DEFAULT NULL,
  `shippingCompany` varchar(75) DEFAULT NULL,
  `shippingStreet` varchar(75) DEFAULT NULL,
  `shippingCity` varchar(75) DEFAULT NULL,
  `shippingState` varchar(75) DEFAULT NULL,
  `shippingZip` varchar(75) DEFAULT NULL,
  `shippingCountry` varchar(75) DEFAULT NULL,
  `shippingPhone` varchar(75) DEFAULT NULL,
  `ccName` varchar(75) DEFAULT NULL,
  `ccType` varchar(75) DEFAULT NULL,
  `ccNumber` varchar(75) DEFAULT NULL,
  `ccExpMonth` int(11) DEFAULT NULL,
  `ccExpYear` int(11) DEFAULT NULL,
  `ccVerNumber` varchar(75) DEFAULT NULL,
  `comments` longtext,
  `ppTxnId` varchar(75) DEFAULT NULL,
  `ppPaymentStatus` varchar(75) DEFAULT NULL,
  `ppPaymentGross` double DEFAULT NULL,
  `ppReceiverEmail` varchar(75) DEFAULT NULL,
  `ppPayerEmail` varchar(75) DEFAULT NULL,
  `sendOrderEmail` tinyint(4) DEFAULT NULL,
  `sendShippingEmail` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`orderId`),
  UNIQUE KEY `IX_D7D6E87A` (`number_`),
  KEY `IX_1D15553E` (`groupId`),
  KEY `IX_119B5630` (`groupId`,`userId`,`ppPaymentStatus`),
  KEY `IX_F474FD89` (`ppTxnId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ShoppingOrder`
--

LOCK TABLES `ShoppingOrder` WRITE;
/*!40000 ALTER TABLE `ShoppingOrder` DISABLE KEYS */;
/*!40000 ALTER TABLE `ShoppingOrder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ShoppingOrderItem`
--

DROP TABLE IF EXISTS `ShoppingOrderItem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ShoppingOrderItem` (
  `orderItemId` bigint(20) NOT NULL,
  `orderId` bigint(20) DEFAULT NULL,
  `itemId` varchar(75) DEFAULT NULL,
  `sku` varchar(75) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `description` longtext,
  `properties` longtext,
  `price` double DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `shippedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`orderItemId`),
  KEY `IX_B5F82C7A` (`orderId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ShoppingOrderItem`
--

LOCK TABLES `ShoppingOrderItem` WRITE;
/*!40000 ALTER TABLE `ShoppingOrderItem` DISABLE KEYS */;
/*!40000 ALTER TABLE `ShoppingOrderItem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SocialActivity`
--

DROP TABLE IF EXISTS `SocialActivity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SocialActivity` (
  `activityId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `createDate` bigint(20) DEFAULT NULL,
  `mirrorActivityId` bigint(20) DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `type_` int(11) DEFAULT NULL,
  `extraData` longtext,
  `receiverUserId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`activityId`),
  UNIQUE KEY `IX_8F32DEC9` (`groupId`,`userId`,`createDate`,`classNameId`,`classPK`,`type_`,`receiverUserId`),
  KEY `IX_82E39A0C` (`classNameId`),
  KEY `IX_A853C757` (`classNameId`,`classPK`),
  KEY `IX_64B1BC66` (`companyId`),
  KEY `IX_2A2468` (`groupId`),
  KEY `IX_1271F25F` (`mirrorActivityId`),
  KEY `IX_1F00C374` (`mirrorActivityId`,`classNameId`,`classPK`),
  KEY `IX_121CA3CB` (`receiverUserId`),
  KEY `IX_3504B8BC` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SocialActivity`
--

LOCK TABLES `SocialActivity` WRITE;
/*!40000 ALTER TABLE `SocialActivity` DISABLE KEYS */;
/*!40000 ALTER TABLE `SocialActivity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SocialEquityAssetEntry`
--

DROP TABLE IF EXISTS `SocialEquityAssetEntry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SocialEquityAssetEntry` (
  `equityAssetEntryId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `assetEntryId` bigint(20) DEFAULT NULL,
  `informationK` double DEFAULT NULL,
  `informationB` double DEFAULT NULL,
  PRIMARY KEY (`equityAssetEntryId`),
  UNIQUE KEY `IX_22F6B5CB` (`assetEntryId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SocialEquityAssetEntry`
--

LOCK TABLES `SocialEquityAssetEntry` WRITE;
/*!40000 ALTER TABLE `SocialEquityAssetEntry` DISABLE KEYS */;
/*!40000 ALTER TABLE `SocialEquityAssetEntry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SocialEquityGroupSetting`
--

DROP TABLE IF EXISTS `SocialEquityGroupSetting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SocialEquityGroupSetting` (
  `equityGroupSettingId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `type_` int(11) DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`equityGroupSettingId`),
  UNIQUE KEY `IX_E4F84168` (`groupId`,`classNameId`,`type_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SocialEquityGroupSetting`
--

LOCK TABLES `SocialEquityGroupSetting` WRITE;
/*!40000 ALTER TABLE `SocialEquityGroupSetting` DISABLE KEYS */;
/*!40000 ALTER TABLE `SocialEquityGroupSetting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SocialEquityHistory`
--

DROP TABLE IF EXISTS `SocialEquityHistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SocialEquityHistory` (
  `equityHistoryId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `personalEquity` int(11) DEFAULT NULL,
  PRIMARY KEY (`equityHistoryId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SocialEquityHistory`
--

LOCK TABLES `SocialEquityHistory` WRITE;
/*!40000 ALTER TABLE `SocialEquityHistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `SocialEquityHistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SocialEquityLog`
--

DROP TABLE IF EXISTS `SocialEquityLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SocialEquityLog` (
  `equityLogId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `assetEntryId` bigint(20) DEFAULT NULL,
  `actionId` varchar(75) DEFAULT NULL,
  `actionDate` int(11) DEFAULT NULL,
  `active_` tinyint(4) DEFAULT NULL,
  `expiration` int(11) DEFAULT NULL,
  `type_` int(11) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  PRIMARY KEY (`equityLogId`),
  UNIQUE KEY `IX_55B2F00C` (`userId`,`assetEntryId`,`actionId`,`actionDate`,`active_`,`type_`),
  KEY `IX_DB6958D2` (`assetEntryId`,`actionId`,`actionDate`,`active_`,`type_`),
  KEY `IX_FEB4055A` (`assetEntryId`,`actionId`,`active_`,`type_`),
  KEY `IX_E8DA181D` (`assetEntryId`,`type_`,`active_`),
  KEY `IX_15A017B` (`userId`,`actionId`,`actionDate`,`active_`,`type_`),
  KEY `IX_3525A383` (`userId`,`actionId`,`active_`,`type_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SocialEquityLog`
--

LOCK TABLES `SocialEquityLog` WRITE;
/*!40000 ALTER TABLE `SocialEquityLog` DISABLE KEYS */;
/*!40000 ALTER TABLE `SocialEquityLog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SocialEquitySetting`
--

DROP TABLE IF EXISTS `SocialEquitySetting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SocialEquitySetting` (
  `equitySettingId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `actionId` varchar(75) DEFAULT NULL,
  `dailyLimit` int(11) DEFAULT NULL,
  `lifespan` int(11) DEFAULT NULL,
  `type_` int(11) DEFAULT NULL,
  `uniqueEntry` tinyint(4) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  PRIMARY KEY (`equitySettingId`),
  UNIQUE KEY `IX_903C1B28` (`groupId`,`classNameId`,`actionId`,`type_`),
  KEY `IX_F3AAD60D` (`groupId`,`classNameId`,`actionId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SocialEquitySetting`
--

LOCK TABLES `SocialEquitySetting` WRITE;
/*!40000 ALTER TABLE `SocialEquitySetting` DISABLE KEYS */;
/*!40000 ALTER TABLE `SocialEquitySetting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SocialEquityUser`
--

DROP TABLE IF EXISTS `SocialEquityUser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SocialEquityUser` (
  `equityUserId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `contributionK` double DEFAULT NULL,
  `contributionB` double DEFAULT NULL,
  `participationK` double DEFAULT NULL,
  `participationB` double DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  PRIMARY KEY (`equityUserId`),
  UNIQUE KEY `IX_D65D3521` (`groupId`,`userId`),
  KEY `IX_6B42B3E7` (`groupId`),
  KEY `IX_945E27C7` (`groupId`,`rank`),
  KEY `IX_166A8F03` (`rank`),
  KEY `IX_6ECBD5D` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SocialEquityUser`
--

LOCK TABLES `SocialEquityUser` WRITE;
/*!40000 ALTER TABLE `SocialEquityUser` DISABLE KEYS */;
/*!40000 ALTER TABLE `SocialEquityUser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SocialRelation`
--

DROP TABLE IF EXISTS `SocialRelation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SocialRelation` (
  `uuid_` varchar(75) DEFAULT NULL,
  `relationId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `createDate` bigint(20) DEFAULT NULL,
  `userId1` bigint(20) DEFAULT NULL,
  `userId2` bigint(20) DEFAULT NULL,
  `type_` int(11) DEFAULT NULL,
  PRIMARY KEY (`relationId`),
  UNIQUE KEY `IX_12A92145` (`userId1`,`userId2`,`type_`),
  KEY `IX_61171E99` (`companyId`),
  KEY `IX_95135D1C` (`companyId`,`type_`),
  KEY `IX_C31A64C6` (`type_`),
  KEY `IX_5A40CDCC` (`userId1`),
  KEY `IX_4B52BE89` (`userId1`,`type_`),
  KEY `IX_5A40D18D` (`userId2`),
  KEY `IX_3F9C2FA8` (`userId2`,`type_`),
  KEY `IX_F0CA24A5` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SocialRelation`
--

LOCK TABLES `SocialRelation` WRITE;
/*!40000 ALTER TABLE `SocialRelation` DISABLE KEYS */;
/*!40000 ALTER TABLE `SocialRelation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SocialRequest`
--

DROP TABLE IF EXISTS `SocialRequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SocialRequest` (
  `uuid_` varchar(75) DEFAULT NULL,
  `requestId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `createDate` bigint(20) DEFAULT NULL,
  `modifiedDate` bigint(20) DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `type_` int(11) DEFAULT NULL,
  `extraData` longtext,
  `receiverUserId` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`requestId`),
  UNIQUE KEY `IX_36A90CA7` (`userId`,`classNameId`,`classPK`,`type_`,`receiverUserId`),
  UNIQUE KEY `IX_4F973EFE` (`uuid_`,`groupId`),
  KEY `IX_D3425487` (`classNameId`,`classPK`,`type_`,`receiverUserId`,`status`),
  KEY `IX_A90FE5A0` (`companyId`),
  KEY `IX_32292ED1` (`receiverUserId`),
  KEY `IX_D9380CB7` (`receiverUserId`,`status`),
  KEY `IX_80F7A9C2` (`userId`),
  KEY `IX_CC86A444` (`userId`,`classNameId`,`classPK`,`type_`,`status`),
  KEY `IX_AB5906A8` (`userId`,`status`),
  KEY `IX_49D5872C` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SocialRequest`
--

LOCK TABLES `SocialRequest` WRITE;
/*!40000 ALTER TABLE `SocialRequest` DISABLE KEYS */;
/*!40000 ALTER TABLE `SocialRequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Subscription`
--

DROP TABLE IF EXISTS `Subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Subscription` (
  `subscriptionId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `frequency` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`subscriptionId`),
  UNIQUE KEY `IX_2E1A92D4` (`companyId`,`userId`,`classNameId`,`classPK`),
  KEY `IX_786D171A` (`companyId`,`classNameId`,`classPK`),
  KEY `IX_54243AFD` (`userId`),
  KEY `IX_E8F34171` (`userId`,`classNameId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Subscription`
--

LOCK TABLES `Subscription` WRITE;
/*!40000 ALTER TABLE `Subscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `Subscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TasksProposal`
--

DROP TABLE IF EXISTS `TasksProposal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TasksProposal` (
  `proposalId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` varchar(75) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  `publishDate` datetime DEFAULT NULL,
  `dueDate` datetime DEFAULT NULL,
  PRIMARY KEY (`proposalId`),
  UNIQUE KEY `IX_181A4A1B` (`classNameId`,`classPK`),
  KEY `IX_7FB27324` (`groupId`),
  KEY `IX_6EEC675E` (`groupId`,`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TasksProposal`
--

LOCK TABLES `TasksProposal` WRITE;
/*!40000 ALTER TABLE `TasksProposal` DISABLE KEYS */;
/*!40000 ALTER TABLE `TasksProposal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TasksReview`
--

DROP TABLE IF EXISTS `TasksReview`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TasksReview` (
  `reviewId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `proposalId` bigint(20) DEFAULT NULL,
  `assignedByUserId` bigint(20) DEFAULT NULL,
  `assignedByUserName` varchar(75) DEFAULT NULL,
  `stage` int(11) DEFAULT NULL,
  `completed` tinyint(4) DEFAULT NULL,
  `rejected` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`reviewId`),
  UNIQUE KEY `IX_5C6BE4C7` (`userId`,`proposalId`),
  KEY `IX_4D0C7F8D` (`proposalId`),
  KEY `IX_70AFEA01` (`proposalId`,`stage`),
  KEY `IX_1894B29A` (`proposalId`,`stage`,`completed`),
  KEY `IX_41AFC20C` (`proposalId`,`stage`,`completed`,`rejected`),
  KEY `IX_36F512E6` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TasksReview`
--

LOCK TABLES `TasksReview` WRITE;
/*!40000 ALTER TABLE `TasksReview` DISABLE KEYS */;
/*!40000 ALTER TABLE `TasksReview` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Team`
--

DROP TABLE IF EXISTS `Team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Team` (
  `teamId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  PRIMARY KEY (`teamId`),
  UNIQUE KEY `IX_143DC786` (`groupId`,`name`),
  KEY `IX_AE6E9907` (`groupId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Team`
--

LOCK TABLES `Team` WRITE;
/*!40000 ALTER TABLE `Team` DISABLE KEYS */;
/*!40000 ALTER TABLE `Team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Ticket`
--

DROP TABLE IF EXISTS `Ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Ticket` (
  `ticketId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `key_` varchar(75) DEFAULT NULL,
  `expirationDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ticketId`),
  KEY `IX_B2468446` (`key_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Ticket`
--

LOCK TABLES `Ticket` WRITE;
/*!40000 ALTER TABLE `Ticket` DISABLE KEYS */;
/*!40000 ALTER TABLE `Ticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserGroup`
--

DROP TABLE IF EXISTS `UserGroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserGroup` (
  `userGroupId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `parentUserGroupId` bigint(20) DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  PRIMARY KEY (`userGroupId`),
  UNIQUE KEY `IX_23EAD0D` (`companyId`,`name`),
  KEY `IX_524FEFCE` (`companyId`),
  KEY `IX_69771487` (`companyId`,`parentUserGroupId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserGroup`
--

LOCK TABLES `UserGroup` WRITE;
/*!40000 ALTER TABLE `UserGroup` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserGroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserGroupGroupRole`
--

DROP TABLE IF EXISTS `UserGroupGroupRole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserGroupGroupRole` (
  `userGroupId` bigint(20) NOT NULL,
  `groupId` bigint(20) NOT NULL,
  `roleId` bigint(20) NOT NULL,
  PRIMARY KEY (`userGroupId`,`groupId`,`roleId`),
  KEY `IX_CCBE4063` (`groupId`),
  KEY `IX_CAB0CCC8` (`groupId`,`roleId`),
  KEY `IX_1CDF88C` (`roleId`),
  KEY `IX_DCDED558` (`userGroupId`),
  KEY `IX_73C52252` (`userGroupId`,`groupId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserGroupGroupRole`
--

LOCK TABLES `UserGroupGroupRole` WRITE;
/*!40000 ALTER TABLE `UserGroupGroupRole` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserGroupGroupRole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserGroupRole`
--

DROP TABLE IF EXISTS `UserGroupRole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserGroupRole` (
  `userId` bigint(20) NOT NULL,
  `groupId` bigint(20) NOT NULL,
  `roleId` bigint(20) NOT NULL,
  PRIMARY KEY (`userId`,`groupId`,`roleId`),
  KEY `IX_1B988D7A` (`groupId`),
  KEY `IX_871412DF` (`groupId`,`roleId`),
  KEY `IX_887A2C95` (`roleId`),
  KEY `IX_887BE56A` (`userId`),
  KEY `IX_4D040680` (`userId`,`groupId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserGroupRole`
--

LOCK TABLES `UserGroupRole` WRITE;
/*!40000 ALTER TABLE `UserGroupRole` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserGroupRole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserIdMapper`
--

DROP TABLE IF EXISTS `UserIdMapper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserIdMapper` (
  `userIdMapperId` bigint(20) NOT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `type_` varchar(75) DEFAULT NULL,
  `description` varchar(75) DEFAULT NULL,
  `externalUserId` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`userIdMapperId`),
  UNIQUE KEY `IX_41A32E0D` (`type_`,`externalUserId`),
  UNIQUE KEY `IX_D1C44A6E` (`userId`,`type_`),
  KEY `IX_E60EA987` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserIdMapper`
--

LOCK TABLES `UserIdMapper` WRITE;
/*!40000 ALTER TABLE `UserIdMapper` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserIdMapper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserTracker`
--

DROP TABLE IF EXISTS `UserTracker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserTracker` (
  `userTrackerId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `sessionId` varchar(200) DEFAULT NULL,
  `remoteAddr` varchar(75) DEFAULT NULL,
  `remoteHost` varchar(75) DEFAULT NULL,
  `userAgent` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`userTrackerId`),
  KEY `IX_29BA1CF5` (`companyId`),
  KEY `IX_46B0AE8E` (`sessionId`),
  KEY `IX_E4EFBA8D` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserTracker`
--

LOCK TABLES `UserTracker` WRITE;
/*!40000 ALTER TABLE `UserTracker` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserTracker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserTrackerPath`
--

DROP TABLE IF EXISTS `UserTrackerPath`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserTrackerPath` (
  `userTrackerPathId` bigint(20) NOT NULL,
  `userTrackerId` bigint(20) DEFAULT NULL,
  `path_` longtext,
  `pathDate` datetime DEFAULT NULL,
  PRIMARY KEY (`userTrackerPathId`),
  KEY `IX_14D8BCC0` (`userTrackerId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserTrackerPath`
--

LOCK TABLES `UserTrackerPath` WRITE;
/*!40000 ALTER TABLE `UserTrackerPath` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserTrackerPath` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User_`
--

DROP TABLE IF EXISTS `User_`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User_` (
  `uuid_` varchar(75) DEFAULT NULL,
  `userId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `defaultUser` tinyint(4) DEFAULT NULL,
  `contactId` bigint(20) DEFAULT NULL,
  `password_` varchar(75) DEFAULT NULL,
  `passwordEncrypted` tinyint(4) DEFAULT NULL,
  `passwordReset` tinyint(4) DEFAULT NULL,
  `passwordModifiedDate` datetime DEFAULT NULL,
  `digest` varchar(255) DEFAULT NULL,
  `reminderQueryQuestion` varchar(75) DEFAULT NULL,
  `reminderQueryAnswer` varchar(75) DEFAULT NULL,
  `graceLoginCount` int(11) DEFAULT NULL,
  `screenName` varchar(75) DEFAULT NULL,
  `emailAddress` varchar(75) DEFAULT NULL,
  `facebookId` bigint(20) DEFAULT NULL,
  `openId` varchar(1024) DEFAULT NULL,
  `portraitId` bigint(20) DEFAULT NULL,
  `languageId` varchar(75) DEFAULT NULL,
  `timeZoneId` varchar(75) DEFAULT NULL,
  `greeting` varchar(255) DEFAULT NULL,
  `comments` longtext,
  `firstName` varchar(75) DEFAULT NULL,
  `middleName` varchar(75) DEFAULT NULL,
  `lastName` varchar(75) DEFAULT NULL,
  `jobTitle` varchar(100) DEFAULT NULL,
  `loginDate` datetime DEFAULT NULL,
  `loginIP` varchar(75) DEFAULT NULL,
  `lastLoginDate` datetime DEFAULT NULL,
  `lastLoginIP` varchar(75) DEFAULT NULL,
  `lastFailedLoginDate` datetime DEFAULT NULL,
  `failedLoginAttempts` int(11) DEFAULT NULL,
  `lockout` tinyint(4) DEFAULT NULL,
  `lockoutDate` datetime DEFAULT NULL,
  `agreedToTermsOfUse` tinyint(4) DEFAULT NULL,
  `active_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `IX_615E9F7A` (`companyId`,`emailAddress`),
  UNIQUE KEY `IX_C5806019` (`companyId`,`screenName`),
  UNIQUE KEY `IX_9782AD88` (`companyId`,`userId`),
  UNIQUE KEY `IX_5ADBE171` (`contactId`),
  KEY `IX_3A1E834E` (`companyId`),
  KEY `IX_5204C37B` (`companyId`,`active_`),
  KEY `IX_6EF03E4E` (`companyId`,`defaultUser`),
  KEY `IX_1D731F03` (`companyId`,`facebookId`),
  KEY `IX_89509087` (`companyId`,`openId`(767)),
  KEY `IX_762F63C6` (`emailAddress`),
  KEY `IX_A18034A4` (`portraitId`),
  KEY `IX_E0422BDA` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User_`
--

LOCK TABLES `User_` WRITE;
/*!40000 ALTER TABLE `User_` DISABLE KEYS */;
INSERT INTO `User_` VALUES ('78310748-07e4-4d88-922e-4d322c73a17e',10135,10132,'2011-07-01 20:41:14','2011-07-01 20:41:14',1,10136,'password',0,0,NULL,'5533ed38b5e33c076a804bb4bca644f9,4b2c83b9c14991181b3134f9975c6229,4b2c83b9c14991181b3134f9975c6229','','',0,'10135','default@liferay.com',0,'',0,'en_US','UTC','Welcome!','','','','','','2011-07-01 20:41:14','',NULL,'',NULL,0,0,NULL,1,1),('37b23206-85e3-4af9-856b-6fbb28af1b2f',10169,10132,'2011-07-01 20:41:17','2011-07-01 20:41:17',0,10170,'qUqP5cyxm6YcTAhz05Hph5gvu9M=',1,0,NULL,'e5d86c6f3672e52795891c3597f20de0,751da756639bc033b572ba2e7849b589,96dc35369e5f37bdf80cb3bda0e8fb3b','what-is-your-father\'s-middle-name','test',0,'test','test@liferay.com',0,'',0,'en_US','UTC','Welcome Test Test!','','Test','','Test','','2011-10-27 06:00:23','127.0.0.1','2011-07-01 20:45:02','127.0.0.1',NULL,0,0,NULL,1,1);
/*!40000 ALTER TABLE `User_` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users_Groups`
--

DROP TABLE IF EXISTS `Users_Groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users_Groups` (
  `userId` bigint(20) NOT NULL,
  `groupId` bigint(20) NOT NULL,
  PRIMARY KEY (`userId`,`groupId`),
  KEY `IX_C4F9E699` (`groupId`),
  KEY `IX_F10B6C6B` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users_Groups`
--

LOCK TABLES `Users_Groups` WRITE;
/*!40000 ALTER TABLE `Users_Groups` DISABLE KEYS */;
INSERT INTO `Users_Groups` VALUES (10169,10157);
/*!40000 ALTER TABLE `Users_Groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users_Orgs`
--

DROP TABLE IF EXISTS `Users_Orgs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users_Orgs` (
  `userId` bigint(20) NOT NULL,
  `organizationId` bigint(20) NOT NULL,
  PRIMARY KEY (`userId`,`organizationId`),
  KEY `IX_7EF4EC0E` (`organizationId`),
  KEY `IX_FB646CA6` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users_Orgs`
--

LOCK TABLES `Users_Orgs` WRITE;
/*!40000 ALTER TABLE `Users_Orgs` DISABLE KEYS */;
/*!40000 ALTER TABLE `Users_Orgs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users_Permissions`
--

DROP TABLE IF EXISTS `Users_Permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users_Permissions` (
  `userId` bigint(20) NOT NULL,
  `permissionId` bigint(20) NOT NULL,
  PRIMARY KEY (`userId`,`permissionId`),
  KEY `IX_8AE58A91` (`permissionId`),
  KEY `IX_C26AA64D` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users_Permissions`
--

LOCK TABLES `Users_Permissions` WRITE;
/*!40000 ALTER TABLE `Users_Permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `Users_Permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users_Roles`
--

DROP TABLE IF EXISTS `Users_Roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users_Roles` (
  `userId` bigint(20) NOT NULL,
  `roleId` bigint(20) NOT NULL,
  PRIMARY KEY (`userId`,`roleId`),
  KEY `IX_C19E5F31` (`roleId`),
  KEY `IX_C1A01806` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users_Roles`
--

LOCK TABLES `Users_Roles` WRITE;
/*!40000 ALTER TABLE `Users_Roles` DISABLE KEYS */;
INSERT INTO `Users_Roles` VALUES (10135,10139),(10169,10138),(10169,10141),(10169,10142);
/*!40000 ALTER TABLE `Users_Roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users_Teams`
--

DROP TABLE IF EXISTS `Users_Teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users_Teams` (
  `userId` bigint(20) NOT NULL,
  `teamId` bigint(20) NOT NULL,
  PRIMARY KEY (`userId`,`teamId`),
  KEY `IX_4D06AD51` (`teamId`),
  KEY `IX_A098EFBF` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users_Teams`
--

LOCK TABLES `Users_Teams` WRITE;
/*!40000 ALTER TABLE `Users_Teams` DISABLE KEYS */;
/*!40000 ALTER TABLE `Users_Teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users_UserGroups`
--

DROP TABLE IF EXISTS `Users_UserGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users_UserGroups` (
  `userGroupId` bigint(20) NOT NULL,
  `userId` bigint(20) NOT NULL,
  PRIMARY KEY (`userGroupId`,`userId`),
  KEY `IX_66FF2503` (`userGroupId`),
  KEY `IX_BE8102D6` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users_UserGroups`
--

LOCK TABLES `Users_UserGroups` WRITE;
/*!40000 ALTER TABLE `Users_UserGroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `Users_UserGroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Vocabulary`
--

DROP TABLE IF EXISTS `Vocabulary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Vocabulary` (
  `vocabularyId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` varchar(75) DEFAULT NULL,
  `folksonomy` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`vocabularyId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Vocabulary`
--

LOCK TABLES `Vocabulary` WRITE;
/*!40000 ALTER TABLE `Vocabulary` DISABLE KEYS */;
/*!40000 ALTER TABLE `Vocabulary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WebDAVProps`
--

DROP TABLE IF EXISTS `WebDAVProps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WebDAVProps` (
  `webDavPropsId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `props` longtext,
  PRIMARY KEY (`webDavPropsId`),
  UNIQUE KEY `IX_97DFA146` (`classNameId`,`classPK`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WebDAVProps`
--

LOCK TABLES `WebDAVProps` WRITE;
/*!40000 ALTER TABLE `WebDAVProps` DISABLE KEYS */;
/*!40000 ALTER TABLE `WebDAVProps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Website`
--

DROP TABLE IF EXISTS `Website`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Website` (
  `websiteId` bigint(20) NOT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `url` longtext,
  `typeId` int(11) DEFAULT NULL,
  `primary_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`websiteId`),
  KEY `IX_96F07007` (`companyId`),
  KEY `IX_4F0F0CA7` (`companyId`,`classNameId`),
  KEY `IX_F960131C` (`companyId`,`classNameId`,`classPK`),
  KEY `IX_1AA07A6D` (`companyId`,`classNameId`,`classPK`,`primary_`),
  KEY `IX_F75690BB` (`userId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Website`
--

LOCK TABLES `Website` WRITE;
/*!40000 ALTER TABLE `Website` DISABLE KEYS */;
/*!40000 ALTER TABLE `Website` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WikiNode`
--

DROP TABLE IF EXISTS `WikiNode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WikiNode` (
  `uuid_` varchar(75) DEFAULT NULL,
  `nodeId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `name` varchar(75) DEFAULT NULL,
  `description` longtext,
  `lastPostDate` datetime DEFAULT NULL,
  PRIMARY KEY (`nodeId`),
  UNIQUE KEY `IX_920CD8B1` (`groupId`,`name`),
  UNIQUE KEY `IX_7609B2AE` (`uuid_`,`groupId`),
  KEY `IX_5D6FE3F0` (`companyId`),
  KEY `IX_B480A672` (`groupId`),
  KEY `IX_6C112D7C` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WikiNode`
--

LOCK TABLES `WikiNode` WRITE;
/*!40000 ALTER TABLE `WikiNode` DISABLE KEYS */;
/*!40000 ALTER TABLE `WikiNode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WikiPage`
--

DROP TABLE IF EXISTS `WikiPage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WikiPage` (
  `uuid_` varchar(75) DEFAULT NULL,
  `pageId` bigint(20) NOT NULL,
  `resourcePrimKey` bigint(20) DEFAULT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `nodeId` bigint(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `version` double DEFAULT NULL,
  `minorEdit` tinyint(4) DEFAULT NULL,
  `content` longtext,
  `summary` longtext,
  `format` varchar(75) DEFAULT NULL,
  `head` tinyint(4) DEFAULT NULL,
  `parentTitle` varchar(255) DEFAULT NULL,
  `redirectTitle` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `statusByUserId` bigint(20) DEFAULT NULL,
  `statusByUserName` varchar(75) DEFAULT NULL,
  `statusDate` datetime DEFAULT NULL,
  PRIMARY KEY (`pageId`),
  UNIQUE KEY `IX_3D4AF476` (`nodeId`,`title`,`version`),
  UNIQUE KEY `IX_2CD67C81` (`resourcePrimKey`,`nodeId`,`version`),
  UNIQUE KEY `IX_899D3DFB` (`uuid_`,`groupId`),
  KEY `IX_A2001730` (`format`),
  KEY `IX_C8A9C476` (`nodeId`),
  KEY `IX_E7F635CA` (`nodeId`,`head`),
  KEY `IX_65E84AF4` (`nodeId`,`head`,`parentTitle`),
  KEY `IX_9F7655DA` (`nodeId`,`head`,`parentTitle`,`status`),
  KEY `IX_432F0AB0` (`nodeId`,`head`,`status`),
  KEY `IX_46EEF3C8` (`nodeId`,`parentTitle`),
  KEY `IX_1ECC7656` (`nodeId`,`redirectTitle`),
  KEY `IX_546F2D5C` (`nodeId`,`status`),
  KEY `IX_997EEDD2` (`nodeId`,`title`),
  KEY `IX_E745EA26` (`nodeId`,`title`,`head`),
  KEY `IX_BEA33AB8` (`nodeId`,`title`,`status`),
  KEY `IX_B771D67` (`resourcePrimKey`,`nodeId`),
  KEY `IX_94D1054D` (`resourcePrimKey`,`nodeId`,`status`),
  KEY `IX_FBBE7C96` (`userId`,`nodeId`,`status`),
  KEY `IX_9C0E478F` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WikiPage`
--

LOCK TABLES `WikiPage` WRITE;
/*!40000 ALTER TABLE `WikiPage` DISABLE KEYS */;
/*!40000 ALTER TABLE `WikiPage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WikiPageResource`
--

DROP TABLE IF EXISTS `WikiPageResource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WikiPageResource` (
  `uuid_` varchar(75) DEFAULT NULL,
  `resourcePrimKey` bigint(20) NOT NULL,
  `nodeId` bigint(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`resourcePrimKey`),
  UNIQUE KEY `IX_21277664` (`nodeId`,`title`),
  KEY `IX_BE898221` (`uuid_`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WikiPageResource`
--

LOCK TABLES `WikiPageResource` WRITE;
/*!40000 ALTER TABLE `WikiPageResource` DISABLE KEYS */;
/*!40000 ALTER TABLE `WikiPageResource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WorkflowDefinitionLink`
--

DROP TABLE IF EXISTS `WorkflowDefinitionLink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WorkflowDefinitionLink` (
  `workflowDefinitionLinkId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `workflowDefinitionName` varchar(75) DEFAULT NULL,
  `workflowDefinitionVersion` int(11) DEFAULT NULL,
  PRIMARY KEY (`workflowDefinitionLinkId`),
  KEY `IX_A8B0D276` (`companyId`),
  KEY `IX_A4DB1F0F` (`companyId`,`workflowDefinitionName`,`workflowDefinitionVersion`),
  KEY `IX_B6EE8C9E` (`groupId`,`companyId`,`classNameId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WorkflowDefinitionLink`
--

LOCK TABLES `WorkflowDefinitionLink` WRITE;
/*!40000 ALTER TABLE `WorkflowDefinitionLink` DISABLE KEYS */;
/*!40000 ALTER TABLE `WorkflowDefinitionLink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WorkflowInstanceLink`
--

DROP TABLE IF EXISTS `WorkflowInstanceLink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WorkflowInstanceLink` (
  `workflowInstanceLinkId` bigint(20) NOT NULL,
  `groupId` bigint(20) DEFAULT NULL,
  `companyId` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `userName` varchar(75) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `modifiedDate` datetime DEFAULT NULL,
  `classNameId` bigint(20) DEFAULT NULL,
  `classPK` bigint(20) DEFAULT NULL,
  `workflowInstanceId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`workflowInstanceLinkId`),
  KEY `IX_415A7007` (`groupId`,`companyId`,`classNameId`,`classPK`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WorkflowInstanceLink`
--

LOCK TABLES `WorkflowInstanceLink` WRITE;
/*!40000 ALTER TABLE `WorkflowInstanceLink` DISABLE KEYS */;
/*!40000 ALTER TABLE `WorkflowInstanceLink` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-10-27 11:35:13
