/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.ussd.ope.provisioning.util;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class UssdSlaRoles {

    public static final String USSD_PERMISSION_ROLE_PREFIX = "ROLE_PROV_USSD";

    public static final String SHORT_CODE_SELECT = "SHORT_CODE_SELECT";
    public static final String KEYWORD = "KEYWORD";
    public static final String CONNECTION_URL = "CONNECTION_URL";
    public static final String MO_TPS = "MO_TPS";
    public static final String MO_TPD = "MO_TPD";

    public static final String MO_CHARGING_ALLOWED = "MO_CHARGING_ALLOWED";
    public static final String MO_CHARGING = USSD_PERMISSION_ROLE_PREFIX + "_MO_CHARGING";
    public static final String MT_CHARGING_ALLOWED = "MT_CHARGING_ALLOWED";
    public static final String MT_CHARGING = USSD_PERMISSION_ROLE_PREFIX + "_MT_CHARGING";
    public static final String SESSION_CHARGING_ALLOWED = "SESSION_CHARGING_ALLOWED";
    public static final String SESSION_CHARGING = USSD_PERMISSION_ROLE_PREFIX + "_SESSION_CHARGING";

    public static final String SUBSCRIPTION_REQUIRED = "SUBSCRIPTION_REQUIRED";
}
