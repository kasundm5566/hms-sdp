/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.ussd.ope.provisioning;

import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.vaadin.terminal.gwt.server.PortletApplicationContext2;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import hms.kite.datarepo.audit.AuditTrail;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.UiManagerImpl;
import hms.kite.ussd.ope.provisioning.ui.UssdSlaForm;
import hms.kite.ussd.ope.provisioning.ui.UssdSlaMainLayout;
import hms.kite.ussd.ope.provisioning.ui.commons.ViewState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.ncsRepositoryService;
import static hms.kite.provisioning.commons.ui.LiferayUtil.createPortletId;
import static hms.kite.provisioning.commons.util.ProvCommonUtil.getNcsDataFromSessionByAppId;
import static hms.kite.provisioning.commons.util.ProvKeyBox.*;
import static hms.kite.ussd.ope.provisioning.OperatorUssdServiceRegistry.locale;
import static hms.kite.ussd.ope.provisioning.OperatorUssdServiceRegistry.operatorId;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class OperatorUssdPortlet extends BaseApplication {

    private static final Logger logger = LoggerFactory.getLogger(OperatorUssdPortlet.class);

    private static final String PORTLET_USER_ID = "portlet_user_id";

    private ThemeDisplay themeDisplay;
    private Panel mainPanel;
    private Panel innerPanel;

    private void handleSaveNcsEvent(Map<String, Object> requestInfo) {
        String operator = operatorId();
        String ncsType = ussdK;
        String appId = (String) requestInfo.get(appIdK);
        String status = (String) requestInfo.get(statusK);
        Map<String, Object> ussdSlaFromSession = getUssdSlaData(appId);

        if (deleteK.equals(status)) {
            ncsRepositoryService().update(appId, ncsType, operator, status);
        } else if (ussdSlaFromSession != null) {
            ussdSlaFromSession.put(operatorK, operator);
            ussdSlaFromSession.put(ncsTypeK, ncsType);
            ussdSlaFromSession.put(appIdK, appId);
            ussdSlaFromSession.put(statusK, status);
            ncsRepositoryService().createNcs(ussdSlaFromSession, (String) requestInfo.get(spIdK));
        } else {
            ncsRepositoryService().update(appId, ncsType, operator, status);
        }
        removeFromSession(ussdSlaDataK);
        closePortlet();
    }

    private void handleCreateNcsEvent(Map<String, Object> requestInfo) {
        Map<String, Object> ussdSla = new HashMap<String, Object>();
        String corpUserId = (String) requestInfo.get(coopUserIdK);
        String appId = (String) requestInfo.get(appIdK);
        String spId = (String) requestInfo.get(spIdK);
        List<Map> selectedNcses = (List<Map>) requestInfo.get(ncsesK);
        mainPanel.setCaption((String) requestInfo.get(ncsPortletTitle));
        innerPanel.removeAllComponents();
        UssdSlaForm form = new UssdSlaForm(ViewState.INITIAL_STATE, this, ussdSla, corpUserId, spId, appId, selectedNcses);
        innerPanel.addComponent(form);
    }

    private void handleReconfigureNcsEvent(Map<String, Object> requestInfo) {
        String corpUserId = (String) requestInfo.get(coopUserIdK);
        String appId = (String) requestInfo.get(appIdK);
        String spId = (String) requestInfo.get(spIdK);
        List<Map> selectedNcses = (List<Map>) requestInfo.get(ncsesK);

        Map<String, Object> ussdSla = getUssdSlaData(appId);

        if (ussdSla == null) {
            ussdSla = findNcsFromRepository(requestInfo);
        }

        UssdSlaForm form = new UssdSlaForm(ViewState.RECONFIGURE_STATE, this, ussdSla, corpUserId, spId, appId, selectedNcses);
        mainPanel.setCaption((String) requestInfo.get(ncsPortletTitle));
        innerPanel.removeAllComponents();
        innerPanel.addComponent(form);
    }

    private void handleViewNcsEvent(Map<String, Object> requestInfo) {
        logger.debug("View NCS event is being triggered for: {}", requestInfo);
        Map<String, Object> ussdSla = findNcsFromRepository(requestInfo);
        String corpUserId = (String) requestInfo.get(coopUserIdK);
        String appId = (String) requestInfo.get(appIdK);
        String spId = (String) requestInfo.get(spIdK);
        List<Map> selectedNcses = (List<Map>) requestInfo.get(ncsesK);
        UssdSlaForm form = new UssdSlaForm(ViewState.VIEW_STATE, this, ussdSla, corpUserId, spId, appId, selectedNcses);
        mainPanel.setCaption((String) requestInfo.get(ncsPortletTitle));
        innerPanel.removeAllComponents();
        innerPanel.addComponent(form);
    }

    private Map<String, Object> findNcsFromRepository(Map<String, Object> requestInfo) {
        Map<String, Object> ncsQuery = new HashMap<String, Object>();
        ncsQuery.put(operatorK, requestInfo.get(operatorK));
        ncsQuery.put(ncsTypeK, requestInfo.get(ncsTypeK));
        ncsQuery.put(appIdK, requestInfo.get(appIdK));

        return ncsRepositoryService().findOperatorSla(ncsQuery);
    }

    private void setPortletUserId(Long userId) {
        if (getPortletUserId() == null) {
            addToSession(PORTLET_USER_ID, userId);
        }
    }

    private Long getPortletUserId() {
        return (Long) getFromSession(PORTLET_USER_ID);
    }

    private Map<String, Object> getUssdSlaData(String appId) {
        return getNcsDataFromSessionByAppId(appId, ussdSlaDataK, this);
    }

    private String operatorPresentationName() {
        return operatorId().substring(0, 1).toUpperCase() + operatorId().substring(1);
    }


    @Override
    protected String getApplicationName() {
        return "ussd-prov";
    }

    @Override
    public void init() {
        setResourceBundle(locale());
        Window main = new Window(getMsg("ussd.ncs.create.window.title"));
        setMainWindow(main);
        setUiManager(new UiManagerImpl(this));
        VerticalLayout mainLayout = new VerticalLayout();
        String caption = this.getMsg("ussd.ncs.create.panel.header", operatorPresentationName());
        mainPanel = new Panel("");
        mainLayout.addComponent(mainPanel);
        mainLayout.addStyleName("container-panel");
        mainLayout.setWidth("762px");
        mainLayout.setMargin(true);
        innerPanel = new Panel(caption);
        mainPanel.addComponent(innerPanel);
        innerPanel.addStyleName("child-panel");

        setPortletContext((PortletApplicationContext2) getContext());
        getPortletContext().addPortletListener(this, this);

        getUiManager().clearAll();
        getUiManager().pushScreen(UssdSlaMainLayout.class.getName(), mainLayout);
    }

    @Override
    public void handleEventRequest(EventRequest request, EventResponse response, Window window) {
        super.handleEventRequest(request, response, window);

        try {
            String eventName = request.getEvent().getName();
            logger.debug("USSD Provisioning portlet has received a request - EventName : [{}]", eventName);

            Map<String, Object> requestInfo = (Map<String, Object>) request.getEvent().getValue();
            AuditTrail.setCurrentUser((String) requestInfo.get(currentUsernameK), (String) requestInfo.get(currentUserTypeK));

            themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
            Long userId = themeDisplay.getUserId();
            setPortletUserId(userId);

            if (!ussdK.equals(requestInfo.get(ncsTypeK))) {
                logger.debug("Received [{}] event is not a [{}] NCS type", eventName, ussdK);
                return;
            }
            if (!operatorId().equals(requestInfo.get(operatorK))) {
                logger.debug("Received [{}] event is not for this event handler: [{}-ussd]. But it is for [{}-ussd]",
                        new Object[]{eventName, operatorId(), requestInfo.get(operatorK)});
                return;
            }
            if (createNcsK.equals(eventName)) {
                handleCreateNcsEvent(requestInfo);
            } else if (saveNcsK.equals(eventName)) {
                handleSaveNcsEvent(requestInfo);
            } else if (reconfigureNcsK.equals(eventName)) {
                handleReconfigureNcsEvent(requestInfo);
            } else if (viewNcsK.equals(eventName)) {
                handleViewNcsEvent(requestInfo);
            } else {
                logger.error("USSD Provisioning portlet don't know how to process the event - EventName : [{}]", eventName);
            }
        } catch (Exception e) {
            logger.error("Unexpected error occurred while handling event ", e);
        }
    }

    public void closePortlet() {
        themeDisplay.getLayoutTypePortlet().removePortletId(getPortletUserId(), createPortletId(ussdK, operatorId()));
    }
}
