/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.ussd.ope.provisioning.ui;

import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.ui.*;
import hms.kite.provisioning.commons.ui.AbstractBasicDetailsLayout;
import hms.kite.provisioning.commons.ui.BaseApplication;
import hms.kite.provisioning.commons.ui.component.charging.ChargingLayout;
import hms.kite.provisioning.commons.ui.component.popup.HelpDetails;
import hms.kite.provisioning.commons.ui.validator.MessageLimitValidator;
import hms.kite.provisioning.commons.ui.validator.SubscriptionRequiredValidator;
import hms.kite.provisioning.commons.ui.validator.TpsTpdValidator;
import hms.kite.ussd.ope.provisioning.ui.commons.KeywordValidator;
import hms.kite.ussd.ope.provisioning.util.UssdSlaRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.vaadin.data.Validator.InvalidValueException;
import static hms.kite.datarepo.RepositoryServiceRegistry.ussdRoutingKeyRepositoryService;
import static hms.kite.provisioning.commons.ui.VaadinUtil.setAllReadonly;
import static hms.kite.ussd.ope.provisioning.OperatorUssdServiceRegistry.*;
import static hms.kite.ussd.ope.provisioning.util.UssdSlaRoles.*;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.ussd.ope.provisioning.util.UssdFeatureRegistry.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class UssdSlaMainLayout extends AbstractBasicDetailsLayout {

    private static final Logger logger = LoggerFactory.getLogger(UssdSlaMainLayout.class);
    private static final String HELP_IMAGE = "help.jpg";

    private static final String WIDTH = "280px";
    public static final String HELP_IMG_WIDTH = "910px";
    public static final String HELP_IMG_HEIGHT = "500px";
    public static final String HELP_IMG_STYLE_NAME = "link";
    public static final String HELP_BUTTON_NAME = "Help";
    private static final String CSS_FORMAT = "ussd-ncs-form";

    private String corporateUserId;
    private String appId;
    private String spId;
    private List<Map> selectedNcses;

    private FormLayout formLayout;
    private FormLayout moFormLayout;
    private FormLayout mtFormLayout;
    private FormLayout sessionFormLayout;

    private Select shortCodeSelect;

    private TextField keywordTxtFld;
    private TextField maxTpsTxtFld;
    private TextField maxTpdTxtFld;
    private TextField connectionUrlTxtFld;

    private ChargingLayout moChargingLayout;
    private ChargingLayout mtChargingLayout;
    private ChargingLayout sessionChargingLayout;

    private CheckBox moChargingAllowedChkBox;
    private CheckBox mtChargingAllowedChkBox;
    private CheckBox sessionChargingAllowedChkBox;
    private CheckBox subscriptionReqChkBox;

    private PopupView popupView;

    private Validator createRegexValidator(String regexKey, String errorMsgKey) {
        return new RegexpValidator(validationRegistry().regex(regexKey), errorMsgKey);
    }

    private Map<String, Object> findRoutingKey(Map<String, Object> smsSla) {
        List<Map<String, Object>> routingKeys = (List<Map<String, Object>>) smsSla.get(routingKeysK);
        if (routingKeys == null || routingKeys.size() != 1)
            throw new IllegalStateException("Found more than one  or no rouging key. Currently Only one routing key will be supported. Routing Keys = " + routingKeys);

        return routingKeys.get(0);
    }

    private List<Map<String, Object>> createRoutingKeys(String shortcode, String keyword, boolean exclusiveShortcode) {
        List<Map<String, Object>> routingKeys = new ArrayList<Map<String, Object>>();
        HashMap<String, Object> routingKey = new HashMap<String, Object>();
        routingKey.put(shortcodeK, shortcode);
        routingKey.put(keywordK, keyword);
        routingKey.put(exclusiveK, exclusiveShortcode);
        routingKeys.add(routingKey);
        return routingKeys;
    }

    private void initChargingAllowedChkBox(CheckBox chargingAllowedChkBox, String caption) {
        chargingAllowedChkBox.setCaption(caption);
        chargingAllowedChkBox.setImmediate(true);
        addComponent(chargingAllowedChkBox);
    }

    private void initChargingFormLayout(FormLayout chargingLayout) {
        chargingLayout.addStyleName(CSS_FORMAT);
        chargingLayout.setVisible(false);
        addComponent(chargingLayout);
    }

    private void addValueChangeListenerToChargingAllowedChkBox(CheckBox chargingAllowedChkBox, final FormLayout chargingFormLayout) {
        chargingAllowedChkBox.addListener(new Property.ValueChangeListener() {

            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                if (event.getProperty().getValue().equals(true)) {
                    chargingFormLayout.setVisible(true);
                } else {
                    chargingFormLayout.setVisible(false);
                }
            }
        });
    }

    private boolean isChargingAllowed(CheckBox chargingAllowedChkBox) {
        if (isFieldNotNull(chargingAllowedChkBox)) {
            return (Boolean) chargingAllowedChkBox.getValue();
        }
        return false;
    }

    private void addFreeCharging(Map<String, Object> data) {
        data.put(chargingK, new HashMap<String, Object>() {{
            put(typeK, freeK);
        }});
    }

    private void getChargingDetails(Map<String, Object> data, Map<String, Object> chargingSla, CheckBox chargingAllowedChkBox,
                                    String chargingAllowedKey, ChargingLayout chargingLayout) {
        boolean chargingAllowed = isChargingAllowed(chargingAllowedChkBox);
        if (chargingAllowed) {
            getValueFromField(chargingLayout, chargingSla, chargingK);
        } else {
            addFreeCharging(chargingSla);
        }
        data.put(chargingAllowedKey, Boolean.toString(chargingAllowed));
    }

    private void setChargingDetails(Map<String, Object> data, Map chargingSla, CheckBox chargingAllowedChkBox,
                                    ChargingLayout chargingLayout, String chargingAllowedKey) {
        if (trueK.equals(data.get(chargingAllowedKey)) && chargingSla != null) {
            setValueToField(chargingAllowedChkBox, true);
            setValueToField(chargingLayout, chargingSla.get(chargingK));
        } else {
            setValueToField(chargingAllowedChkBox, false);
        }
    }

    private void setChargingLayoutPermissions(ChargingLayout chargingLayout) {
        if (isFieldNotNull(chargingLayout)) {
            chargingLayout.setPermissions();
        }
    }

    private void validateChargingLayout(CheckBox chargingAllowedChkBox, ChargingLayout chargingLayout, String messagePrefix) {
        try {
            if (isChargingAllowed(chargingAllowedChkBox)) {
                validateField(chargingLayout);
            }
        } catch (InvalidValueException e) {
            throw new InvalidValueException(MessageFormat.format(messagePrefix, e.getMessage()));
        }
    }

    private void init() {
        formLayout = new FormLayout();
        formLayout.addStyleName(CSS_FORMAT);

        if (isHelpButtonLinkEnable()) {
            createHelperLink();
        }

        addComponent(formLayout);

        createShortCodeSelect();
        createKeywordFld();
        createTpsFld();
        createTpdFld();
        createConnectionUrl();

        if(isDefaultUssdChargingLayout()) {
            createStandardMoChargingView();
        } else {
            createMoChargingView();
            createMtChargingView();
            createSessionChargingView();
        }

        createSubscriptionReqChkBox();
    }



    private void createHelperLink() {
        Button helpButtonLink = createHelpButtonLink();
        addComponent(helpButtonLink);
        setComponentAlignment(helpButtonLink, Alignment.TOP_RIGHT);
    }

    private Button createHelpButtonLink() {
        final HelpDetails helpDetails = new HelpDetails(application, HELP_IMAGE);
        final Embedded image = helpDetails.getHelpImage();
        Button helpButtonLink = new Button(HELP_BUTTON_NAME);
        helpButtonLink.setStyleName(HELP_IMG_STYLE_NAME);
        helpButtonLink.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent clickEvent) {
                getWindow().addWindow(helpDetails.generateHelpImageSubWindow(image, HELP_IMG_WIDTH, HELP_IMG_HEIGHT));
            }
        });
        return helpButtonLink;
    }


    private void createShortCodeSelect() {
        shortCodeSelect = getFieldWithPermission(SHORT_CODE_SELECT, new Select());
        if (isFieldNotNull(shortCodeSelect)) {
            String caption = application.getMsg("ussd.ncs.create.application.sla.service.code");
            shortCodeSelect.setCaption(caption);
            shortCodeSelect.setWidth(WIDTH);
            shortCodeSelect.setRequired(true);
            shortCodeSelect.setRequiredError(application.getMsg("ussd.ncs.create.charging.sla.null.error", caption));
            shortCodeSelect.setNullSelectionAllowed(false);

            List<Map<String, Object>> routingKeys = ussdRoutingKeyRepositoryService().findAllForOperator(operatorId());
            for (Map<String, Object> routingKey : routingKeys) {
                shortCodeSelect.addItem(routingKey.get(shortcodeK));
                shortCodeSelect.setItemCaption(routingKey.get(shortcodeK), (String) routingKey.get(shortcodeK));
            }
            shortCodeSelect.addListener(new Property.ValueChangeListener() {
                @Override
                public void valueChange(Property.ValueChangeEvent event) {
                    try {
                        String code = (String) shortCodeSelect.getValue();
                        if (isFieldNotNull(keywordTxtFld)) {
                            if (ussdRoutingKeyRepositoryService().isExclusive(ussdK, operatorId(), code)) {
                                keywordTxtFld.setVisible(false);
                                keywordTxtFld.setRequired(false);
                                keywordTxtFld.setValue("");
                            } else {
                                keywordTxtFld.setVisible(true);
                                keywordTxtFld.setRequired(true);
                            }
                        }
                    } catch (Exception e) {
                        logger.error("Unexpected error occurred ", e);
                    }
                }
            });

            formLayout.addComponent(shortCodeSelect);
        }
    }

    private void createKeywordFld() {
        keywordTxtFld = getFieldWithPermission(KEYWORD, new TextField());
        if (isFieldNotNull(keywordTxtFld)) {
            String caption = application.getMsg("ussd.ncs.create.application.sla.keyword");
            keywordTxtFld.setCaption(caption);
            keywordTxtFld.setWidth(WIDTH);
            keywordTxtFld.setRequired(true);
            keywordTxtFld.setRequiredError(application.getMsg("ussd.ncs.create.charging.sla.null.error", caption));
            keywordTxtFld.addValidator(new StringLengthValidator(
                    application.getMsg("ussd.ncs.create.application.sla.keyword.length.error"), 0, 10, false));
            keywordTxtFld.addValidator(createRegexValidator("ussd.keyword.regex",
                    application.getMsg("ussd.ncs.create.application.sla.keyword.numeric.error")));
            formLayout.addComponent(keywordTxtFld);
        }
    }

    private void createTpsFld() {
        maxTpsTxtFld = getFieldWithPermission(MO_TPS, new TextField());
        if (isFieldNotNull(maxTpsTxtFld)) {
            String caption = application.getMsg("ussd.ncs.create.application.sla.maximum.messages.per.second");
            maxTpsTxtFld.setCaption(caption);
            maxTpsTxtFld.setWidth(WIDTH);
            maxTpsTxtFld.setRequired(true);
            maxTpsTxtFld.setRequiredError(application.getMsg("ussd.ncs.create.charging.sla.null.error", caption));
            maxTpsTxtFld.addValidator(createRegexValidator("ussd.msg.per.sec.regex",
                    application.getMsg("ussd.ncs.create.application.sla.maximum.messages.per.second.numeric.error")));
            maxTpsTxtFld.addValidator(new MessageLimitValidator(
                    application.getMsg("ussd.ncs.create.application.sla.maximum.messages.per.second.limit.error"),
                    corporateUserId, ussdK, moK, tpsK));
            maxTpsTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("sp.ussd.zero.validation")
                    , application.getMsg("ussd.sla.zeroValue.validation.error.message")));
            if (isDefaultMoTpsAllow()) {
                maxTpsTxtFld.setValue(getDefaultMoTps());
            }
            formLayout.addComponent(maxTpsTxtFld);
        }
    }

    private void createTpdFld() {
        maxTpdTxtFld = getFieldWithPermission(MO_TPD, new TextField());
        if (isFieldNotNull(maxTpdTxtFld)) {
            String caption = application.getMsg("ussd.ncs.create.application.sla.maximum.messages.per.day");
            maxTpdTxtFld.setCaption(caption);
            maxTpdTxtFld.setWidth(WIDTH);
            maxTpdTxtFld.setRequired(true);
            maxTpdTxtFld.setRequiredError(application.getMsg("ussd.ncs.create.charging.sla.null.error", caption));
            maxTpdTxtFld.addValidator(createRegexValidator("ussd.msg.per.day.regex",
                    application.getMsg("ussd.ncs.create.application.sla.maximum.messages.per.day.numeric.error")));
            maxTpdTxtFld.addValidator(new MessageLimitValidator(
                    application.getMsg("ussd.ncs.create.application.sla.maximum.messages.per.day.limit.error"),
                    corporateUserId, ussdK, moK, tpdK));
            maxTpdTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("sp.ussd.zero.validation"),
                    application.getMsg("ussd.sla.zeroValue.validation.error.message")));
            maxTpdTxtFld.addValidator(new TpsTpdValidator(maxTpsTxtFld, maxTpdTxtFld,
                    application.getMsg("ussd.tps.tpd.validation")));
            if (isDefaultMoTpdAllow()) {
                maxTpdTxtFld.setValue(getDefaultMoTpd());
            }
            formLayout.addComponent(maxTpdTxtFld);
        }
    }

    private void createConnectionUrl() {
        connectionUrlTxtFld = getFieldWithPermission(CONNECTION_URL, new TextField());
        if (isFieldNotNull(connectionUrlTxtFld)) {
            String caption = application.getMsg("ussd.ncs.create.application.sla.connection.url");
            connectionUrlTxtFld.setCaption(caption);
            connectionUrlTxtFld.setWidth(WIDTH);
            connectionUrlTxtFld.setRequired(true);
            connectionUrlTxtFld.setRequiredError(application.getMsg("ussd.ncs.create.charging.sla.null.error", caption));
            connectionUrlTxtFld.addValidator(new RegexpValidator(validationRegistry().regex("url.validation.regex")
                    , application.getMsg("ussd.ncs.create.application.sla.connection.url.error")));
            formLayout.addComponent(connectionUrlTxtFld);
        }
    }

    private void createMoChargingView() {
        moChargingAllowedChkBox = getFieldWithPermission(MO_CHARGING_ALLOWED, new CheckBox());
        if (isFieldNotNull(moChargingAllowedChkBox)) {
            initChargingAllowedChkBox(moChargingAllowedChkBox,
                    application.getMsg("ussd.ncs.create.application.sla.mo.charging.allowed"));

            moFormLayout = new FormLayout();
            initChargingFormLayout(moFormLayout);

            moChargingLayout = ChargingLayout.ussdMoChargingLayout(moFormLayout, operatorId(), application,
                    corporateUserId, MO_CHARGING, WIDTH, 3, 21);
            if (isDefaultMoChargingAllow()) {
                setValueToField(moChargingLayout, getDefaultMoCharging());
            }

            addValueChangeListenerToChargingAllowedChkBox(moChargingAllowedChkBox, moFormLayout);
        }
    }

    private void createStandardMoChargingView() {
        moChargingAllowedChkBox = getFieldWithPermission(MO_CHARGING_ALLOWED, new CheckBox());
        if (isFieldNotNull(moChargingAllowedChkBox)) {
            moChargingAllowedChkBox.setVisible(false);
            initChargingAllowedChkBox(moChargingAllowedChkBox,
                    application.getMsg("ussd.ncs.create.application.sla.mo.charging.allowed"));

            moFormLayout = new FormLayout();
            initChargingFormLayout(moFormLayout);

            moChargingLayout = ChargingLayout.ussdStandardMoChargingLayout(moFormLayout, operatorId(), application,
                    corporateUserId, MO_CHARGING, WIDTH, 3, 21);
            if (isDefaultMoChargingAllow()) {
                setValueToField(moChargingLayout, getDefaultMoCharging());
            }

            addValueChangeListenerToChargingAllowedChkBox(moChargingAllowedChkBox, moFormLayout);
            moChargingAllowedChkBox.setValue(true);
        }
    }


    private void createMtChargingView() {
        mtChargingAllowedChkBox = getFieldWithPermission(MT_CHARGING_ALLOWED, new CheckBox());
        if (isFieldNotNull(mtChargingAllowedChkBox)) {
            initChargingAllowedChkBox(mtChargingAllowedChkBox,
                    application.getMsg("ussd.ncs.create.application.sla.mt.charging.allowed"));

            mtFormLayout = new FormLayout();
            initChargingFormLayout(mtFormLayout);

            mtChargingLayout = ChargingLayout.ussdMtChargingLayout(mtFormLayout, operatorId(), application,
                    corporateUserId, MT_CHARGING, WIDTH, 3, 21);
            if (isDefaultMtChargingAllow()) {
                setValueToField(mtChargingLayout, getDefaultMtCharging());
            }

            addValueChangeListenerToChargingAllowedChkBox(mtChargingAllowedChkBox, mtFormLayout);
        }
    }

    private void createSessionChargingView() {
        sessionChargingAllowedChkBox = getFieldWithPermission(SESSION_CHARGING_ALLOWED, new CheckBox());
        if (isFieldNotNull(sessionChargingAllowedChkBox)) {
            initChargingAllowedChkBox(sessionChargingAllowedChkBox,
                    application.getMsg("ussd.ncs.create.application.sla.session.charging.allowed"));

            sessionFormLayout = new FormLayout();
            initChargingFormLayout(sessionFormLayout);

            sessionChargingLayout = ChargingLayout.ussdSessionChargingLayout(sessionFormLayout, operatorId(), application,
                    corporateUserId, SESSION_CHARGING, WIDTH, 3, 21);
            if (isDefaultSessionChargingAllow()) {
                setValueToField(sessionChargingLayout, getDefaultSessionCharging());
            }

            addValueChangeListenerToChargingAllowedChkBox(sessionChargingAllowedChkBox, sessionFormLayout);
        }
    }

    private void createSubscriptionReqChkBox() {
        subscriptionReqChkBox = getFieldWithPermission(SUBSCRIPTION_REQUIRED, new CheckBox());
        if (isFieldNotNull(subscriptionReqChkBox)) {
            subscriptionReqChkBox.setCaption(application.getMsg("ussd.ncs.create.application.sla.subscription.required"));
            subscriptionReqChkBox.addValidator(new SubscriptionRequiredValidator(selectedNcses,
                    application.getMsg("ussd.ncs.create.application.sla.subscription.required.error")));
            formLayout.addComponent(subscriptionReqChkBox);
        }
    }

    public UssdSlaMainLayout(String corporateUserId, String spId, String appId, BaseApplication application,
                             List<Map> selectedNcses) {
        super(application, UssdSlaRoles.USSD_PERMISSION_ROLE_PREFIX);

        this.corporateUserId = corporateUserId;
        this.appId = appId;
        this.spId = spId;
        this.selectedNcses = selectedNcses;

        init();
    }

    public void checkForKeywordAvailable() {
        if (isFieldNotNull(keywordTxtFld)) {
            keywordTxtFld.addValidator(new KeywordValidator(spId, appId, operatorId(), shortCodeSelect,
                    application.getMsg("ussd.ncs.create.application.sla.keyword.already.exist.error")));
        }
    }

    @Override
    public void setData(Map<String, Object> data) {
        Map<String, Object> routingKey = findRoutingKey(data);

        if (isFieldNotNull(shortCodeSelect)) {
            String shortcode = (String) routingKey.get(shortcodeK);
            if (!shortCodeSelect.getItemIds().contains(shortcode)) {
                shortCodeSelect.addItem(shortcode);
            }
            setValueToField(shortCodeSelect, shortcode);
        }

        if (isFieldNotNull(keywordTxtFld)) {
            Boolean exclusive = (Boolean) routingKey.get(exclusiveK);
            if (!exclusive) {
                setValueToField(keywordTxtFld, routingKey.get(keywordK));
                keywordTxtFld.setVisible(true);
            } else {
                keywordTxtFld.setVisible(false);
            }
        }

        Map mo = (Map) data.get(moK);
        if (mo != null) {
            setValueToField(maxTpsTxtFld, mo.get(tpsK));
            setValueToField(maxTpdTxtFld, mo.get(tpdK));
            setValueToField(connectionUrlTxtFld, mo.get(connectionUrlK));
            setChargingDetails(data, mo, moChargingAllowedChkBox, moChargingLayout, moChargingAllowedK);
        }

        setChargingDetails(data, (Map) data.get(mtK), mtChargingAllowedChkBox, mtChargingLayout, mtChargingAllowedK);

        setChargingDetails(data, (Map) data.get(sessionK), sessionChargingAllowedChkBox, sessionChargingLayout, sessionChargingAllowedK);

        setValueToField(subscriptionReqChkBox, data.get(subscriptionRequiredK));
    }

    @Override
    public void getData(Map<String, Object> data) {
        String shortcode = shortCodeSelect.getValue().toString();
        String keyword = keywordTxtFld.getValue().toString();

        Map<String, Object> mo = new HashMap<String, Object>();
        Map<String, Object> mt = new HashMap<String, Object>();
        Map<String, Object> session = new HashMap<String, Object>();

        getValueFromField(maxTpsTxtFld, mo, tpsK);
        getValueFromField(maxTpdTxtFld, mo, tpdK);
        getValueFromField(connectionUrlTxtFld, mo, connectionUrlK);
        getChargingDetails(data, mo, moChargingAllowedChkBox, moChargingAllowedK, moChargingLayout);
        data.put(moAllowedK, true);
        data.put(moK, mo);

        getValueFromField(maxTpsTxtFld, mt, tpsK);
        getValueFromField(maxTpdTxtFld, mt, tpdK);
        getChargingDetails(data, mt, mtChargingAllowedChkBox, mtChargingAllowedK, mtChargingLayout);
        data.put(mtAllowedK, true);
        data.put(mtK, mt);

        getChargingDetails(data, session, sessionChargingAllowedChkBox, sessionChargingAllowedK, sessionChargingLayout);
        data.put(sessionAllowedK, true);
        data.put(sessionK, session);

        boolean exclusiveShortcode = !keywordTxtFld.isVisible();
        List<Map<String, Object>> routingKeys = createRoutingKeys(shortcode, keyword, exclusiveShortcode);
        data.put(routingKeysK, routingKeys);

        getValueFromField(subscriptionReqChkBox, data, subscriptionRequiredK);
    }

    @Override
    public void validate() {
        validateField(shortCodeSelect);
        validateField(keywordTxtFld);
        validateField(maxTpsTxtFld);
        validateField(maxTpdTxtFld);
        validateField(connectionUrlTxtFld);
        validateChargingLayout(moChargingAllowedChkBox, moChargingLayout,
                application.getMsg("ussd.ncs.create.application.sla.mo.charging.error.message.pattern"));
        validateChargingLayout(mtChargingAllowedChkBox, mtChargingLayout,
                application.getMsg("ussd.ncs.create.application.sla.mt.charging.error.message.pattern"));
        validateChargingLayout(sessionChargingAllowedChkBox, sessionChargingLayout,
                application.getMsg("ussd.ncs.create.application.sla.session.charging.error.message.pattern"));
        validateField(subscriptionReqChkBox);
    }

    @Override
    public void reset() {
        resetFields("", keywordTxtFld, maxTpdTxtFld, maxTpsTxtFld, connectionUrlTxtFld);
    }

    @Override
    public void setPermissions() {
        setReadWritePermissionToComponent(shortCodeSelect, keywordTxtFld, maxTpsTxtFld, maxTpdTxtFld,
                connectionUrlTxtFld, subscriptionReqChkBox);
        setChargingLayoutPermissions(moChargingLayout);
        setChargingLayoutPermissions(mtChargingLayout);
        setChargingLayoutPermissions(sessionChargingLayout);
    }

    public void setReadonly(boolean isReadOnly) {
        setAllReadonly(isReadOnly, this);
        if (!isReadOnly) {
            setPermissions();
        }
    }
}
