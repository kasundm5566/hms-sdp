/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.ussd.ope.provisioning.ui;

import com.vaadin.data.Validator;
import com.vaadin.terminal.UserError;
import com.vaadin.ui.*;
import hms.kite.provisioning.commons.ui.VaadinUtil;
import hms.kite.provisioning.commons.ui.event.PortletEventSender;
import hms.kite.ussd.ope.provisioning.OperatorUssdPortlet;
import hms.kite.ussd.ope.provisioning.OperatorUssdServiceRegistry;
import hms.kite.ussd.ope.provisioning.ui.commons.ViewState;
import hms.kite.util.KiteKeyBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.provisioning.commons.util.ProvKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class UssdSlaForm extends Form {

    private static final Logger logger = LoggerFactory.getLogger(UssdSlaForm.class);

    private UssdSlaMainLayout ussdSlaMainLayout;

    private OperatorUssdPortlet application;
    private String appId;
    private Map<String, Object> ussdSla;

    private VerticalLayout ussdVerticalLayout = new VerticalLayout();

    private Button reset;
    private Button configure;
    private Button back;
    private Button confirm;

    private int currentState = INITIAL_STATE;
    private static final int INITIAL_STATE = 0;
    private static final int RE_CONFIGURE_STATE = 1;
    private static final int VIEW_STATE = 2;

    private void init(ViewState eventState, String corpUserId, String spId, List<Map> selectedNcs) {
        loadComponent(eventState, corpUserId, spId, selectedNcs);
        populateButtons();
        addStyleName("ussd-ncs-form");
    }

    private void loadComponent(ViewState eventState, String corpUserId, String spId, List<Map> selectedNcses) {
        logger.debug("USSD Sla Form is being initialized");

        ussdSlaMainLayout = new UssdSlaMainLayout(corpUserId, spId, appId, application, selectedNcses);
        ussdSlaMainLayout.setWidth("80%");

        reset = new Button(application.getMsg("ussd.ncs.create.sla.form.reset"));
        configure = new Button(application.getMsg("ussd.ncs.create.sla.form.configure"));
        back = new Button(application.getMsg("ussd.ncs.create.sla.form.back"));
        confirm = new Button(application.getMsg("ussd.ncs.create.sla.form.confirm"));

        setLayout(ussdVerticalLayout);

        try {
            if (ViewState.RECONFIGURE_STATE.equals(eventState)) {
                updateUssdSlaInLayouts(ussdSla);
                initialConfigurationState();
                ussdSlaMainLayout.checkForKeywordAvailable();
            } else if (ViewState.VIEW_STATE.equals(eventState)) {
                updateUssdSlaInLayouts(ussdSla);
                viewState();
            } else {
                initialConfigurationState();
                ussdSlaMainLayout.checkForKeywordAvailable();
                ussdSla = new HashMap<String, Object>();
                setReadOnly(true);
            }
        } catch (Exception e) {
            logger.error("Error occurred while loading data into ussd sla form", e);
            throw new IllegalStateException(e);
        }
        ussdVerticalLayout.addComponent(ussdSlaMainLayout);
        ussdVerticalLayout.setComponentAlignment(ussdSlaMainLayout, Alignment.MIDDLE_CENTER);
    }

    private void updateUssdSlaInLayouts(Map<String, Object> ussdSla) {
        ussdSlaMainLayout.setData(ussdSla);
    }

    private void populateButtons() {
        reset.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    logger.debug("USSD Sla reset form is executed");
                    ussdSlaMainLayout.reset();
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while resetting", e);
                }
            }
        });

        configure.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                logger.debug("USSD Sla form is being configured");
                try {
                    VaadinUtil.cleanErrorComponents(UssdSlaForm.this);
                    ussdSlaMainLayout.validate();
                    reConfigurationState();
                    logger.debug("Configured Data Map: {}", ussdSla);
                } catch (Validator.InvalidValueException ex) {
                    logger.debug("Validation failed  {}", ex.getMessage());
                    setComponentError(ex);
                } catch (Exception ex) {
                    logger.error("Unknown Exception Occurred: {}", ex);
                    showErrorNotification(application.getMsg("ussd.ncs.create.sla.form.unknown.error"));
                }
            }
        });

        back.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    logger.debug("USSD Sla form is being re-edited before confirming it");
                    if (currentState == INITIAL_STATE) {
                        PortletEventSender.send(new HashMap(), provRefreshEvent, application);
                        application.closePortlet();
                    } else if (currentState == VIEW_STATE) {
                        PortletEventSender.send(new HashMap(), provRefreshEvent, application);
                        application.closePortlet();
                    } else {
                        initialConfigurationState();
                    }
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while going back", e);
                }
            }
        });

        confirm.addListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    logger.debug("USSD Sla form is being confirmed by the user");
                    ussdSlaMainLayout.getData(ussdSla);
                    ussdSla.put(appIdK, appId);
                    Map<String, Object> dataMap = new HashMap<String, Object>();
                    dataMap.put(ncsTypeK, ussdK);
                    dataMap.put(statusK, successK);
                    String operator = OperatorUssdServiceRegistry.operatorId();
                    dataMap.put(KiteKeyBox.operatorK, operator);
                    PortletEventSender.send(dataMap, ncsConfiguredK, application);
                    application.addToSession(ussdSlaDataK, ussdSla);
                    application.closePortlet();
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while saving USSD sla", e);
                }
            }
        });

        HorizontalLayout buttonLayout = new HorizontalLayout();

        for (Button button : new Button[]{back, reset, configure, confirm}) {
            button.setImmediate(true);
            buttonLayout.addComponent(button);
        }
        buttonLayout.setSpacing(true);
        this.getFooter().setWidth("100%");
        this.getFooter().addComponent(buttonLayout);
        ((HorizontalLayout) this.getFooter()).setComponentAlignment(buttonLayout, Alignment.MIDDLE_CENTER);
    }

    private void initialConfigurationState() {
        logger.info("Initial Configuration State");
        currentState = INITIAL_STATE;
        ussdSlaMainLayout.setReadonly(false);
        setButtonVisibility(true, true, true, false);
    }

    private void reConfigurationState() {
        logger.info("Re Configuration State");
        currentState = RE_CONFIGURE_STATE;
        ussdSlaMainLayout.setReadonly(true);
        setButtonVisibility(false, false, true, true);
    }

    private void viewState() {
        logger.info("View State");
        currentState = VIEW_STATE;
        ussdSlaMainLayout.setReadonly(true);
        setButtonVisibility(false, false, true, false);
    }

    private void setButtonVisibility(boolean resetVisibility, boolean configureVisibility, boolean backVisibility,
                                     boolean confirmVisibility) {
        reset.setVisible(resetVisibility);
        configure.setVisible(configureVisibility);
        back.setVisible(backVisibility);
        confirm.setVisible(confirmVisibility);
    }

    private void showErrorNotification(String message) {
        setComponentError(new UserError(message));
    }

    public UssdSlaForm(ViewState eventState, OperatorUssdPortlet application, Map<String, Object> ussdSla,
                       String corpUserId, String spId, String appId, List<Map> selectedNcs) {
        this.ussdSla = ussdSla;
        this.application = application;
        this.appId = appId;
        logger.debug("USSD SLA [{}]", this.ussdSla);
        init(eventState, corpUserId, spId, selectedNcs);
    }
}
