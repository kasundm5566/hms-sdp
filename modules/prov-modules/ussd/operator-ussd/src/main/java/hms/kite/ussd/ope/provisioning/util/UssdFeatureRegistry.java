package hms.kite.ussd.ope.provisioning.util;

import hms.kite.util.NullObject;

import java.util.Properties;

public class UssdFeatureRegistry {

    private static Properties featureProperties;

    static {
        setFeatureProperties(new NullObject<Properties>().get());
    }

    private static void setFeatureProperties(Properties properties) {
        UssdFeatureRegistry.featureProperties = properties;
    }

    public static boolean isDefaultUssdChargingLayout() {
        return Boolean.parseBoolean(featureProperties.getProperty("ussd.charging.default.layout"));
    }

    public static boolean isHelpButtonLinkEnable() {
        return Boolean.parseBoolean(featureProperties.getProperty("ussd.help.link.enable"));
    }
}
