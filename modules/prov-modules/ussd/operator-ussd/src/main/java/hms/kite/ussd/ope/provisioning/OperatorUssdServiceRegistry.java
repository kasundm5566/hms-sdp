/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.ussd.ope.provisioning;

import hms.kite.datarepo.SpRepositoryService;
import hms.kite.provisioning.commons.ValidationRegistry;
import hms.kite.provisioning.commons.service.PaymentGatewayAdaptor;
import hms.kite.util.NullObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class OperatorUssdServiceRegistry {

    private static PaymentGatewayAdaptor paymentGatewayAdaptor;
    private static SpRepositoryService spRepositoryService;
    private static ValidationRegistry validationRegistry;
    private static Properties ussdDefaultProperties;

    private static String operatorId;
    private static String locale;

    private static Map<String, Object> defaultMoCharging;
    private static Map<String, Object> defaultMtCharging;
    private static Map<String, Object> defaultSessionCharging;

    static {
        setSpRepositoryService(new NullObject<SpRepositoryService>().get());
        setValidationRegistry(new NullObject<ValidationRegistry>().get());
        setUssdDefaultProperties(new NullObject<Properties>().get());
        setOperatorId("");
        setLocale("");
        setDefaultMoCharging(new HashMap<String, Object>());
        setDefaultMtCharging(new HashMap<String, Object>());
        setDefaultSessionCharging(new HashMap<String, Object>());
    }

    public static String operatorId() {
        return operatorId;
    }

    private static void setOperatorId(String operatorId) {
        OperatorUssdServiceRegistry.operatorId = operatorId;
    }

    public static SpRepositoryService getSpRepositoryService() {
        return spRepositoryService;
    }

    private static void setSpRepositoryService(SpRepositoryService spRepositoryService) {
        OperatorUssdServiceRegistry.spRepositoryService = spRepositoryService;
    }

    public static ValidationRegistry validationRegistry() {
        return validationRegistry;
    }

    private static void setValidationRegistry(ValidationRegistry validationRegistry) {
        OperatorUssdServiceRegistry.validationRegistry = validationRegistry;
    }

    public static String locale() {
        return locale;
    }

    private static void setLocale(String locale) {
        OperatorUssdServiceRegistry.locale = locale;
    }

    private static void setUssdDefaultProperties(Properties ussdDefaultProperties) {
        OperatorUssdServiceRegistry.ussdDefaultProperties = ussdDefaultProperties;
    }

    public static boolean isDefaultMoTpsAllow() {
        return Boolean.parseBoolean(ussdDefaultProperties.getProperty("ussd.default.mo.tps.allow"));
    }

    public static String getDefaultMoTps() {
        return ussdDefaultProperties.getProperty("ussd.default.mo.tps");
    }

    public static boolean isDefaultMoTpdAllow() {
        return Boolean.parseBoolean(ussdDefaultProperties.getProperty("ussd.default.mo.tpd.allow"));
    }

    public static String getDefaultMoTpd() {
        return ussdDefaultProperties.getProperty("ussd.default.mo.tpd");
    }

    public static boolean isDefaultMoChargingAllow() {
        return Boolean.parseBoolean(ussdDefaultProperties.getProperty("ussd.default.mo.charging.allow"));
    }

    private static void setDefaultMoCharging(Map<String, Object> defaultMoCharging) {
        OperatorUssdServiceRegistry.defaultMoCharging = defaultMoCharging;
    }

    public static Map<String, Object> getDefaultMoCharging() {
        return defaultMoCharging;
    }

    public static boolean isDefaultMtChargingAllow() {
        return Boolean.parseBoolean(ussdDefaultProperties.getProperty("ussd.default.mt.charging.allow"));
    }

    private static void setDefaultMtCharging(Map<String, Object> defaultMtCharging) {
        OperatorUssdServiceRegistry.defaultMtCharging = defaultMtCharging;
    }

    public static Map<String, Object> getDefaultMtCharging() {
        return defaultMtCharging;
    }

    public static boolean isDefaultSessionChargingAllow() {
        return Boolean.parseBoolean(ussdDefaultProperties.getProperty("ussd.default.session.charging.allow"));
    }

    private static void setDefaultSessionCharging(Map<String, Object> defaultSessionCharging) {
        OperatorUssdServiceRegistry.defaultSessionCharging = defaultSessionCharging;
    }

    public static Map<String, Object> getDefaultSessionCharging() {
        return defaultSessionCharging;
    }
}
