package hms.kite.wfengine.util;

import hms.common.rest.util.MapAdapter;
import hms.kite.wfengine.impl.ServiceImpl;

/**
 * Useful implementation for serialising Map<String,String> types.
 *
 * @version $Id$
 */
public final class MapStringServiceImplAdapter extends MapAdapter<MapStringServiceImplElement, String, ServiceImpl> {

    @Override
    public MapStringServiceImplElement newMapElement(final String key, final ServiceImpl value) {
        final MapStringServiceImplElement result = new MapStringServiceImplElement();
        result.setKey(key);
        result.setValue(value);
        return result;
    }

    @Override
    public MapStringServiceImplElement[] newMapElement(int size) {
        return new MapStringServiceImplElement[size];
    }

}