/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.wfengine;

import hms.kite.util.SdpException;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteErrorBox.successCode;
import static hms.kite.util.KiteKeyBox.requestK;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 */
public class WorkflowUtility {

	private static final Logger logger = LoggerFactory.getLogger(WorkflowUtility.class);

    public static void forwardToChannelAndUpdateRequestStatus(Channel channel,
                                                              Map<String, Map<String, Object>> requestContext) throws SdpException {

        Map<String, Object> res = doForwardToChannel(channel, requestContext);

        for (Map.Entry<String, Object> responseEntry : res.entrySet()) {
            // todo check res.put(responseEntry.getKey(),
            // responseEntry.getValue());
            requestContext.get(requestK).put(responseEntry.getKey(), responseEntry.getValue());
        }
    }

	public static void forwardToErrorRoutingChannel(Channel channel, Map<String, Map<String, Object>> requestContext)
			throws SdpException {

		Map<String, Object> res = doForwardToChannel(channel, requestContext);
	}

    private static Map<String, Object> doForwardToChannel(Channel channel,
                                                          Map<String, Map<String, Object>> requestContext) {
        logger.debug("About to execute the channel [{}]: ", channel);

        Map<String, Object> res = null;

        res = channel.execute(requestContext);

        if (res == null) {
            throw new SdpException(unknownErrorCode, String.format("No response received from the channel [%s]",
                    channel));
        }

        if (isStatusCodeRelatedToPartialPayments((String) res.get(statusCodeK)))  {
            return res;
        } else if (!isStatusCodeRelatedToSuccess((String) res.get(statusCodeK))) {
			logger.warn("Fail response received from channel[{}] error code[{}]", channel, res.get(statusCodeK));
			throw new SdpException((String) res.get(statusCodeK), (String) res.get(statusDescriptionK));
		}
		return res;
    }

    private static boolean isStatusCodeRelatedToPartialPayments(String statusCode) {
        if(statusCode != null)  {
            return statusCode.startsWith("P");
        }
        return false;
    }

    private static boolean isStatusCodeRelatedToSuccess(String statusCode) {
        if (statusCode != null) {
            return statusCode.startsWith("S");
        }
        return false;
    }
}
