/**
 *   (C) Copyright 2011-2012 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.wfengine.control;

import java.util.Arrays;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ContainsExpression extends LogicalExpression {

	private String contextKey;
	private String[] parameterKeys;

	public ContainsExpression(String contextKey, String... parameterKeys) {
		this.contextKey = contextKey;
		this.parameterKeys = parameterKeys;
	}

	@Override
	public boolean execute(Map<String, Map<String, Object>> requestContext) {
		if (null == parameterKeys) {
			return false;
		}

		if (!requestContext.containsKey(contextKey)) {
			return false;
		}

		Object context = requestContext.get(contextKey);

		for (String key : parameterKeys) {
			if (((Map<String, Object>) context).containsKey(key) && null != ((Map<String, Object>) context).get(key)) {
				context = ((Map<String, Object>) context).get(key);
			} else {
				return false;
			}
		}

		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ContainsExpression [contextKey=");
		builder.append(contextKey);
		builder.append(", parameterKeys=");
		builder.append(Arrays.toString(parameterKeys));
		builder.append("]");
		return builder.toString();
	}

}
