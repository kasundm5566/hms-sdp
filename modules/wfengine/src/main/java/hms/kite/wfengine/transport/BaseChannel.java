/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.wfengine.transport;

import org.springframework.beans.factory.BeanNameAware;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public abstract class BaseChannel implements Channel, BeanNameAware {
    
    String beanName;

    @Override
    public Map<String, Object> send(Map<String, Object> request) {

        Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String, Object>>();
        requestContext.put(requestK, request);

        return execute(requestContext);
    }

    @Override
    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    @Override
    public String toString() {
        return beanName;
    }
}
