/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.wfengine.control;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import hms.common.rest.util.MapStringStringAdapter;
import org.apache.commons.lang.ArrayUtils;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static hms.kite.wfengine.control.LogicalOperand.OPERAND.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class LogicalOperand {
    enum OPERAND {
        AND, OR, NOT, IN, NOTIN, LESSTHAN, GREATERTHAN, REGEX
    }

    @Expose
    @SerializedName("field")
    private String field;

    @Expose
    @SerializedName("AND")
    private LogicalOperand[] andConditions;

    @Expose
    @SerializedName("OR")
    private LogicalOperand[] orConditions;

    @Expose
    @SerializedName("NOT")
    private LogicalOperand notCondition;

    @Expose
    @SerializedName("IN")
    private String[] inConditions;

    @Expose
    @SerializedName("REGEX")
    private String regExCondtion;

    @Expose
    @SerializedName("NOIIN")
    private String[] notinConditions;

    @Expose
    @SerializedName("LESSTHAN")
    private double lessThanCondition;

    @Expose
    @SerializedName("GREATERTHAN")
    private double greaterThanCondition;

    @Expose
    @SerializedName("result")
    @XmlElement(name = "result")
    @XmlJavaTypeAdapter(MapStringStringAdapter.class)
    protected Map<String, String> result = new HashMap<String, String>();

    private OPERAND operand;

    public static LogicalOperand buildREGEXLogic(String field, String regex) {
        LogicalOperand operand = new LogicalOperand();
        if (regex == null) {
            throw new NullPointerException("regex can not be null");
        }
        if (field == null) {
            throw new NullPointerException("field can not be null");

        }
        operand.setField(field);
        operand.setREGEXCondition(regex);
        return operand;
    }

    public static LogicalOperand buildINLogic(String field, String... conditions) {
        if (conditions == null) {
            throw new NullPointerException("regex can not be null");
        }
        if (field == null) {
            throw new NullPointerException("field can not be null");

        }
        LogicalOperand operand = new LogicalOperand();
        operand.setField(field);
        operand.setINConditions(conditions);
        return operand;
    }

    public static LogicalOperand buildNOTINLogic(String field, String... conditions) {
        if (conditions == null) {
            throw new NullPointerException("regex can not be null");
        }
        if (field == null) {
            throw new NullPointerException("field can not be null");

        }
        LogicalOperand operand = new LogicalOperand();
        operand.setField(field);
        operand.setNOTINConditions(conditions);
        return operand;
    }

    public static LogicalOperand buildANDLogic(LogicalOperand... operands) {
        if (operands.length < 2) {
            throw new IllegalStateException("And logic should have 2 or more operands");
        }
        LogicalOperand operand = new LogicalOperand();
        operand.setANDConditions(operands);
        return operand;
    }


    public static LogicalOperand buildORLogic(LogicalOperand... operands) {
        if (operands.length < 2) {
            throw new IllegalStateException("Or logic should have 2 or more operands");
        }
        LogicalOperand operand = new LogicalOperand();
        operand.setORConditions(operands);
        return operand;
    }

    public static LogicalOperand buildNOTLogic(LogicalOperand operand) {
        if (operand == null) {
            throw new NullPointerException("operand can not be null");
        }
        LogicalOperand notOperand = new LogicalOperand();
        notOperand.setNOTCondition(operand);
        return operand;
    }

    public static LogicalOperand buildLESSTHANLogic(String field, double lessThan) {
        LogicalOperand operand = new LogicalOperand();
        operand.setLESSTHANCondition(lessThan);
        operand.setField(field);
        return operand;
    }

    public static LogicalOperand buildGREATERTHANLogic(String field, double greaterThan) {
        LogicalOperand operand = new LogicalOperand();
        operand.setGREATERTHANCondition(greaterThan);
        operand.setField(field);
        return operand;
    }


    protected LogicalOperand() {
        initOperand();
    }

    public boolean matches(Map<String, String> matchers) {
        initOperand();
        if (null == operand) {
            return true;
        }
        final boolean match = matchOperands(matchers);
        if (match && null != result && !result.isEmpty()) {
            matchers.putAll(result);
        }
        return match;
    }

    private boolean matchOperands(Map<String, String> matchers) {
        switch (operand) {
            case IN: {
                String value = matchers.get(field);
                return !(null == value || value.isEmpty()) && ArrayUtils.contains(inConditions, value);
            }
            case NOTIN: {
                String value = matchers.get(field);
                return null == value || value.isEmpty() || !ArrayUtils.contains(notinConditions, value);
            }

            case REGEX: {
                String value = matchers.get(field);
                return !(null == value || value.isEmpty()) && value.matches(regExCondtion);
            }
            case AND: {
                for (LogicalOperand andCondition : andConditions) {
                    if (!andCondition.matches(matchers)) {
                        return false;
                    }
                }
                return true;
            }
            case OR: {
                for (LogicalOperand orCondition : orConditions) {
                    if (orCondition.matches(matchers)) {
                        return true;
                    }
                }
                return false;
            }
            case NOT: {
                return !notCondition.matches(matchers);
            }
            case LESSTHAN: {
                String value = matchers.get(field);
                try {
                    double numValue = Double.parseDouble(value);
                    return numValue <= lessThanCondition;
                } catch (Throwable e) {
                    return false;
                }
            }
            case GREATERTHAN: {
                String value = matchers.get(field);
                try {
                    double numValue = Double.parseDouble(value);
                    return numValue >= greaterThanCondition;
                } catch (Throwable e) {
                    return false;
                }
            }
            default:
                return true; // no conditions enforced
        }
    }

    private void initOperand() {
        if (null != operand) {
            return;
        }
        if (null != inConditions) {
            operand = IN;
        } else if (null != notinConditions) {
            operand = NOTIN;
        } else if (null != andConditions) {
            operand = AND;
        } else if (null != orConditions) {
            operand = OR;
        } else if (null != notCondition) {
            operand = NOT;
        } else if (0 < lessThanCondition) {
            operand = LESSTHAN;
        } else if (0 < lessThanCondition) {
            operand = GREATERTHAN;
        } else if (null != regExCondtion && !regExCondtion.isEmpty()) {
            operand = REGEX;
        }
    }


    private void setANDConditions(LogicalOperand... andConditions) {
        operand = AND;
        this.andConditions = andConditions;
    }

    private void setINConditions(String... inConditions) {
        operand = IN;
        this.inConditions = inConditions;
    }

    private void setREGEXCondition(String regex) {
        operand = REGEX;
        this.regExCondtion = regex;
    }

    private void setORConditions(LogicalOperand... orConditions) {
        operand = OR;
        this.orConditions = orConditions;
    }

    private void setNOTCondition(LogicalOperand notCondition) {
        operand = NOT;
        this.notCondition = notCondition;
    }

    private void setLESSTHANCondition(double lessThanCondition) {
        operand = LESSTHAN;
        this.lessThanCondition = lessThanCondition;
    }

    private void setGREATERTHANCondition(double greaterThanCondition) {
        operand = GREATERTHAN;
        this.greaterThanCondition = greaterThanCondition;
    }

    private void setNOTINConditions(String[] notinConditions) {
        operand = NOTIN;
        this.notinConditions = notinConditions;
    }

    public LogicalOperand[] getANDConditions() {
        return andConditions;
    }

    public String[] getINConditions() {
        return inConditions;
    }

    public String getREGEXConditions() {
        return regExCondtion;
    }

    public String[] getNOTINConditions() {
        return notinConditions;
    }

    public String getField() {
        return field;
    }

    private void setField(String field) {
        this.field = field;
    }

    public LogicalOperand[] getOrConditions() {
        return orConditions;
    }

    public void setResult(Map<String, String> result) {
        this.result = result;
    }

    public Map<String, String> getResult() {
        return result;
    }

    public void addResult(String key, String value) {
        this.result.put(key, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        if (null == operand) {
            sb.append(" NO OPERAND (true)");
        }
        switch (operand) {
            case IN: {
                sb.append("Field [").append(field).append("] IN ").append(Arrays.toString(inConditions));
                break;
            }

            case NOTIN: {
                sb.append("Field [").append(field).append("] NOT IN ").append(Arrays.toString(notinConditions));
                break;
            }

            case REGEX: {
                sb.append("Field [").append(field).append("] match [").append(regExCondtion).append("]");
                break;
            }
            case AND: {
                if (andConditions != null) {
                    for (LogicalOperand andCondition : andConditions) {
                        sb.append(andCondition).append(" AND ");
                    }
                    sb.delete(sb.length() - 5, sb.length()); //to clean up last appended and
                } else {
                    sb.append("NULL_AND_NULL");
                }
                break;
            }
            case OR: {

                if (orConditions != null) {
                    for (LogicalOperand orCondition : orConditions) {
                        sb.append(orCondition).append(" OR ");
                    }
                    sb.delete(sb.length() - 4, sb.length()); // to clean up last appended or
                } else {
                    sb.append("NULL_OR_NULL");
                }
                break;
            }
            case NOT: {
                sb.append("NOT ").append(notCondition);
                break;
            }
            case LESSTHAN: {
                sb.append("Field [").append(field).append("] <= [").append(lessThanCondition).append("]");
                break;
            }
            case GREATERTHAN: {
                sb.append("Field [").append(field).append("] >= [").append(greaterThanCondition).append("]");
                break;
            }
            default:
                sb.append(" DEFAULT OPERAND(true)");
                break;
        }
        sb.append("}");
        return sb.toString();
    }
}

