/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.wfengine.impl;

import static hms.kite.util.KiteErrorBox.unknownErrorCode;
import static hms.kite.wfengine.WorkflowUtility.forwardToErrorRoutingChannel;
import hms.kite.util.SdpException;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 *
 */
public class ErrorResponseRoutingService extends ServiceImpl {
	private static final Logger logger = LoggerFactory.getLogger(ErrorResponseRoutingService.class);

	public ErrorResponseRoutingService(String serviceName) {
		super(serviceName);
	}

	public ErrorResponseRoutingService(String serviceName, ServiceImpl onSuccess) {
        this(serviceName);
        this.onSuccess = onSuccess;
    }


	@Override
	public void execute(Map<String, Map<String, Object>> requestContext) {
		logger.debug("Execute the service named [{}]", serviceName);
        try {

            if(channel != null) {
                createServiceRequest(requestContext);
                forwardToErrorRoutingChannel(channel, requestContext);
            }
            if (noConditionsToApply()) {
                success();
                addResultsToRequest(requestContext);
            } else {
                applyConditions(requestContext);
            }
        } catch (SdpException e) {
            //This is the place where the standard gateway errors handled.
            logger.warn("Service execution failed [" + serviceName + "] error code[" + e.getErrorCode() + "] description[" +
                    e.getErrorDescription() + "] message [" + e.getMessage() + "]");
            fail(e, requestContext);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            //This is the place where the standard gateway errors handled.
            fail(unknownErrorCode, null, requestContext);
        } finally {
            removeServiceParameters(requestContext);
        }
	}

}
