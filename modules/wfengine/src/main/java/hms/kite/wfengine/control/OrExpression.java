/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.wfengine.control;

import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class OrExpression extends LogicalExpression {

    private LogicalExpression[] expressions;

    public OrExpression(LogicalExpression... expressions) {
        this.expressions = expressions;
    }

    @Override
    public boolean execute(Map<String, Map<String, Object>> requestContext) {

        if (null == expressions) {
            return true;
        }

        for(LogicalExpression expression : expressions) {
            if(expression.execute(requestContext)) {
                return true;
            }
        }

        return false;
    }

    public void setExpressions(LogicalExpression[] expressions) {
        this.expressions = expressions;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        if (expressions != null) {
            for (LogicalExpression expression : expressions) {
                sb.append(expression).append(" OR ");
            }
            sb.delete(sb.length() - 5, sb.length()); //to clean up last appended and
        } else {
            sb.append("NULL_AND_NULL");
        }
        sb.append("}");
        return sb.toString();
    }
}
