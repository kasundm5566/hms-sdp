/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.wfengine.control;

import hms.kite.util.KeyNameSpaceResolver;
import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class InExpression extends LogicalExpression {

    private Object[] inConditions;
    private String requestKey;
    private String requestParamKey;
    private String contextKey;
    private String contextParamKey;
    private String spaceSeparatedRequestKey;
    private String spaceSeparatedContextKey;


    /**
     * Check inConditions contain requestParamKey
     * If either one is null return false
     * @param requestKey
     * @param requestParamKey
     * @param inConditions
     */
    public InExpression(String requestKey, String requestParamKey, Object[] inConditions) {
        this.requestKey = requestKey;
        this.requestParamKey = requestParamKey;
        this.inConditions = inConditions;
    }

    /**
     * contextParam is a comma separated string and check if requestParam is in contextParam
     * @param requestKey
     * @param requestParamKey
     * @param contextKey
     * @param contextParamKey
     */
    public InExpression(String requestKey, String requestParamKey, String contextKey, String contextParamKey) {
        this.requestKey = requestKey;
        this.requestParamKey = requestParamKey;
        this.contextKey = contextKey;
        this.contextParamKey = contextParamKey;
    }

    public InExpression(String spaceSeparatedRequestKey, String spaceSeparatedContextKey) {
        this.spaceSeparatedRequestKey = spaceSeparatedRequestKey;
        this.spaceSeparatedContextKey = spaceSeparatedContextKey;
    }


    @Override
    public boolean execute(Map<String, Map<String, Object>> requestContext) {
        if (null != inConditions) {
            return matchInConditions(requestContext);
        } else if (null != contextKey) {
            return matchContextParam(requestContext);
        } else {
            return matchSpaceSeparatedKeys(requestContext);
        }
    }

    private boolean matchContextParam(Map<String, Map<String, Object>> requestContext) {
        String requestValue = (String) requestContext.get(requestKey).get(requestParamKey);

        if (isString(requestContext)) {
            String contextValue = (String) requestContext.get(contextKey).get(contextParamKey);
            if (null == requestValue || null == contextValue) {
                return false;
            }

            if (contextValue.contains(requestValue)) {
                return true;
            }
        } else if (isCollection(requestContext)) {
            return ArrayUtils.contains(((Collection) requestContext.get(contextKey).get(contextParamKey)).toArray(),
                    requestValue);
        }
        return false;
    }

    private boolean isCollection(Map<String, Map<String, Object>> requestContext) {
        return requestContext.get(contextKey).get(contextParamKey) instanceof Collection;
    }

    private boolean isString(Map<String, Map<String, Object>> requestContext) {
        return requestContext.get(contextKey).get(contextParamKey) instanceof String;
    }


    private boolean matchInConditions(Map<String, Map<String, Object>> requestContext) {
        Object requestValue = requestContext.get(requestKey).get(requestParamKey);

        if (null == requestValue) {
            return false;
        }

        if (requestValue instanceof Collection) {
            Collection requestCol = (Collection) requestValue;
            for (Object o : requestCol) {
                if(ArrayUtils.contains(inConditions, o)) {
                    return true;
                }
            }
            return false;
        } else {

            return ArrayUtils.contains(inConditions, requestValue);
        }
    }

    private boolean matchSpaceSeparatedKeys(Map<String, Map<String, Object>> requestContext) {
        Object request = KeyNameSpaceResolver.data((Map) requestContext, spaceSeparatedRequestKey);
        Object context = KeyNameSpaceResolver.data((Map) requestContext, spaceSeparatedContextKey);

        if (null == request || null == context) {
            return false;
        }

        if (context instanceof String) {
            return ((String) context).contains((String) request);
        } else if (context instanceof Collection) {
            return ArrayUtils.contains(((Collection) context).toArray(), request);
        }

        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        if(null != inConditions) {
            sb.append("Context[").append(requestKey).append("] Field [").append(requestParamKey).append("] IN ").append(Arrays.toString(inConditions));
        } else if (null != contextKey) {
            sb.append("Context[").append(requestKey).append("] Field [").append(requestParamKey).append("] IN ")
                    .append("Context[").append(contextKey).append("] Field [").append(contextParamKey);
        } else {
            sb.append("Context[").append(spaceSeparatedContextKey).append("] IN Context[").append(spaceSeparatedRequestKey).append("]");
        }
        sb.append("}");
        return sb.toString();
    }
}
