/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hms.kite.wfengine.impl;

import static hms.kite.util.KiteErrorBox.unknownErrorCode;
import static hms.kite.util.KiteKeyBox.requestK;
import hms.kite.util.SdpException;
import hms.kite.wfengine.Service;
import hms.kite.wfengine.control.Condition;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public class BranchingService extends ServiceImpl {
	private static final Logger logger = LoggerFactory.getLogger(BranchingService.class);

	public BranchingService(String serviceName, ServiceImpl onSuccess, ServiceImpl onFailure, Condition... conditions) {
		super(serviceName, onSuccess, onFailure, conditions);
	}

	@Override
	public void execute(Map<String, Map<String, Object>> requestContext) {
		logger.debug("Execute the service named [{}]", serviceName);
		try {
			executeConditions(requestContext);
		} catch (SdpException e) {
			// This is the place where the standard gateway errors handled.
			logger.warn("Service execution failed [" + serviceName + "] error code[" + e.getErrorCode()
					+ "] description[" + e.getErrorDescription() + "] message [" + e.getMessage() + "]");
			fail(e, requestContext);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			// This is the place where the standard gateway errors handled.
			fail(new SdpException(unknownErrorCode), requestContext);
		}
	}

	private void executeConditions(Map<String, Map<String, Object>> requestContext) {

		for (Condition condition : conditions) {

			if (condition.execute(requestContext)) {
				success();
			} else {
				logger.debug("Condition [{}] failed for request [{}]", condition, requestContext.get(requestK));
				String errorCode = getErrorCode(condition);
				if (onErrors == null) {
					next = null;
					return;
				}
				next = onErrors.get(errorCode);
				if (null == next) {
					next = onErrors.get(unknownErrorCode);
				}
				return;
			}
		}
	}

	private String getErrorCode(Condition condition) {
		String errorCode = condition.getErrorCode();

		if (errorCode == null) {
			final Service service = onErrors.get(unknownErrorCode);

			if (null != service && service instanceof ErrorService) {
				errorCode = ((ErrorService) service).getErrorCode();
			} else {
				errorCode = unknownErrorCode;
			}
		}
		return errorCode;
	}

}
