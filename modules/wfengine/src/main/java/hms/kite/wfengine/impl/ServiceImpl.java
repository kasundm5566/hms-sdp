/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.wfengine.impl;

import hms.kite.util.SdpException;
import hms.kite.wfengine.Service;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static hms.kite.util.KiteErrorBox.unknownErrorCode;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.wfengine.WorkflowUtility.forwardToChannelAndUpdateRequestStatus;
import static hms.kite.wfengine.impl.ErrorService.DEFAULT_ERROR_SERVICE;
import static hms.kite.wfengine.impl.ErrorService.createErrorService;


/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ServiceImpl implements Service {

    private static final Logger logger = LoggerFactory.getLogger(ServiceImpl.class);

    protected Map<String, Service> onErrors = new HashMap<String, Service>();
    protected Service onSuccess;

    protected String serviceName;

    @Deprecated
    protected String channelName;
    protected Channel channel;
    protected Condition[] conditions;
    protected Service next;
    private String[] serviceParameterKeys;
    private Map<String, Object> serviceParameters = new HashMap<String, Object>();

    /**
     * Default constructor to support serialisation
     */
    public ServiceImpl() {
    }

    public ServiceImpl(String serviceName) {
        this.serviceName = serviceName;
    }

    public ServiceImpl(String serviceName, Service onSuccess, final Service onError) {
        this(serviceName, onSuccess, new HashMap<String, Service>() {
            {
                put(unknownErrorCode, onError);
            }
        });
    }

    public ServiceImpl(String serviceName, ServiceImpl onSuccess) {
        this(serviceName);
        this.onSuccess = onSuccess;
    }

    public ServiceImpl(String call, ServiceImpl onSuccess, String errorCode) {
        this(call, onSuccess, createErrorService(errorCode));
    }

    public ServiceImpl(String call, String errorCode) {
        this(call, null, createErrorService(errorCode));
    }

    public ServiceImpl(String call, ServiceImpl onSuccess, ServiceImpl onError, Condition... conditions) {
        this(call, onSuccess, onError);
        this.conditions = conditions;
    }

    public ServiceImpl(Condition... conditions) {
        this.conditions = conditions;
    }

    public ServiceImpl(String call, Service onSuccess, Map<String, Service> onErrors) {
        if (null == onErrors) {
            onErrors = new HashMap<String, Service>();
        }
        if (!onErrors.containsKey(unknownErrorCode)) {
            logger.warn(unknownErrorCode + " is not in the errors mapping, it will be impossible to handle not defined errors");
            onErrors.put(unknownErrorCode, DEFAULT_ERROR_SERVICE);
        }
        this.serviceName = call;
        this.onSuccess = onSuccess;
        this.onErrors = onErrors;
    }

    public ServiceImpl(String serviceName, Condition... conditions) {
        this(conditions);
        this.serviceName = serviceName;
    }

    public void execute(Map<String, Map<String, Object>> requestContext) {
        logger.debug("Execute the service named [{}] with channel named [{}]", serviceName, channel);
        try {
            if(channel != null) {
                createServiceRequest(requestContext);
                forwardToChannelAndUpdateRequestStatus(channel, requestContext);
            }
            if (noConditionsToApply()) {
                success();
                addResultsToRequest(requestContext);
            } else {
                applyConditions(requestContext);
            }
        } catch (SdpException e) {
            //This is the place where the standard gateway errors handled.
            logger.warn("Service execution failed [" + serviceName + "] error code[" + e.getErrorCode() + "] description[" +
                    e.getErrorDescription() + "] message [" + e.getMessage() + "]");
            fail(e, requestContext);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            //This is the place where the standard gateway errors handled.
            fail(unknownErrorCode, null, requestContext);
        } finally {
            removeServiceParameters(requestContext);
        }
    }

    protected void createServiceRequest(Map<String, Map<String, Object>> requestContext) {
        if (null != serviceParameterKeys) {
            requestContext.get(requestK).put(serviceParameterKeysK, serviceParameterKeys);
        }

        if (null != serviceParameters) {
            requestContext.get(requestK).putAll(serviceParameters);
        }
    }

    protected void removeServiceParameters(Map<String, Map<String, Object>> requestContext) {
        if (requestContext.get(requestK).containsKey(serviceParameterKeysK)) {
            requestContext.get(requestK).remove(serviceParameterKeysK);
        }

        if (null != serviceParameters) {
            for (Map.Entry entry : serviceParameters.entrySet()) {
                requestContext.get(requestK).remove(entry.getKey());
            }
        }
    }

    protected void applyConditions(Map<String, Map<String, Object>> requestContext) {

        for (Condition condition : conditions) {

            if (condition.execute(requestContext)) {
                success();
            } else {
                logger.debug("Condition [{}] failed.", condition);

                logger.trace("Request for which the condition failed was {}", requestContext.get(requestK));

                String errorCode = condition.getErrorCode();

                if (errorCode == null) {
                    final Service service = onErrors.get(unknownErrorCode);

                    if (null != service && service instanceof ErrorService) {
                        errorCode = ((ErrorService) service).getErrorCode();
                    } else {
                        errorCode = unknownErrorCode;
                    }
                }
                fail(errorCode, null, requestContext);
                return;
            }
        }
        addResultsToRequest(requestContext);
    }

    protected void addResultsToRequest(Map<String, Map<String, Object>> requestContext) {
        //todo check if needed
        Map<String, Object> request = requestContext.get(requestK);

        for (Map.Entry<String, Object> resultEntry : request.entrySet()) {

            Object requestValue =  request.get(resultEntry.getKey());

            if (null == requestValue) {
                request.put(resultEntry.getKey(), resultEntry.getValue());
            }
        }
    }

    protected boolean noConditionsToApply() {
        return null == conditions || 0 == conditions.length;
    }

    public boolean hasNext() {
        return null != next;
    }

    public Service next() {
        if (next != null) {
            return next;
        } else {
            throw new IllegalStateException("No more services available");
        }
    }

    public String getName() {
        return serviceName;
    }

    public void success() {
        next = onSuccess;
    }

    public void fail(SdpException exception, Map<String, Map<String, Object>> request) {
        fail(exception.getErrorCode(), exception.getErrorDescription(), request);
    }

    protected void fail(String errorCode, String description, Map<String, Map<String, Object>> requestContext) {
        requestContext.get(requestK).put(statusCodeK, errorCode);
        if (null == description) {
            description = ResponseBuilder.getErrorDescription(errorCode, requestContext);
        }
        requestContext.get(requestK).put(statusDescriptionK, description);
        requestContext.get(requestK).put(statusK, Boolean.toString(false));

        if (onErrors == null) {
            next = null;
            return;
        }
        next = onErrors.get(errorCode);

        if (null == next) {
            next = onErrors.get(unknownErrorCode);
        }

    }

    public ServiceImpl setOnSuccess(ServiceImpl onSuccess) {
        this.onSuccess = onSuccess;
        return onSuccess;
    }

    public void setOnErrors(Map<String, Service> onErrors) {
        this.onErrors = onErrors;
    }

    @Deprecated
    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public void addOnError(String errorCode, Service errorService) {
        if (null == onErrors) {
            onErrors = new HashMap<String, Service>();
        }
        onErrors.put(errorCode, errorService);
    }

    public void onDefaultError(Service errorService) {
        if (null == onErrors) {
            onErrors = new HashMap<String, Service>();
        }
        onErrors.put(unknownErrorCode, errorService);
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public void setConditions(Condition... conditions) {
        this.conditions = conditions;
    }

    public void addCondition(Condition condition) {
        if (null == conditions) {
            conditions = new Condition[]{condition};
            return;
        }
        List<Condition> conditionList = new ArrayList<Condition>();
        conditionList.addAll(Arrays.asList(conditions));
        conditionList.add(condition);
        conditions = conditionList.toArray(new Condition[conditionList.size()]);
    }

    @Override
    public void remove() {
        this.next = null;
    }

    public ServiceImpl setServiceParameterKeys(String... serviceParameterKeys) {
        this.serviceParameterKeys = serviceParameterKeys;
        return this;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("ServiceImpl{")
                .append("serviceName='").append(serviceName).append('\'')
                .append(", onErrors=").append(onErrors)
                .append(", conditions=").append(conditions == null ? null : conditions)
                .append(", onSuccess=").append(onSuccess)
                .append(", next=").append(next)
                .append('}').toString();
    }

    /**
     * Creates a service to which will be called after this service while execution
     */
    public ServiceImpl chain(Condition... conditions) {
        ServiceImpl result = new ServiceImpl(conditions);
        this.setOnSuccess(result);
        return result;
    }

    /**
     * Creates a service to which will be called after this service while execution
     */
    public ServiceImpl chain(String serviceName, Condition... conditions) {
        ServiceImpl result = new ServiceImpl(serviceName, conditions);
        this.setOnSuccess(result);
        return result;
    }

    /**
     *  Create next service to be executed with a branch
     *
     *  This Service
     *        +
     *        |
     *        v
     *    Condition
     *        +
     *        |
     *        +-----Yes----> Branching Service
     *        |                          +
     *        NO                         |
     *        |                          |
     *        v                          |
     *    Common Service <---------------+
     *        +
     *        |
     *        |
     *        v
     *   Next Service
     *
     * @param condition - Condition to be evaluated
     * @param branchingService - Service to be executed if condition evaluation returns True.
     * @param commonService  - Common Service to be executed to converge the flow
     * @return - commonService
     */
    public ServiceImpl chainBranch(Condition condition, ServiceImpl branchingService, ServiceImpl commonService){

        branchingService.setOnSuccess(commonService);
        ServiceImpl validationService = new ServiceImpl("validation-service", condition);
        validationService.setOnSuccess(branchingService);
        validationService.onDefaultError(commonService);

        setOnSuccess(validationService);

        return commonService;

    }

    public ServiceImpl addServiceParameter(String key, Object value) {
        this.serviceParameters.put(key, value);
        return this;
    }

//    /**
//     * Attach tha channel name to this service and return the same (this) service
//     * @param channelName
//     * @return
//     */
//    public ServiceImpl attachChannel(String channelName) {
//        this.setChannelName(channelName);
//        return this;
//    }

    public ServiceImpl attachChannel(Channel channel) {
        this.channel = channel;
        return this;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }
}
