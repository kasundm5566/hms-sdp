/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.wfengine.control;

import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class Condition {

    private LogicalExpression expression;

    private String errorCode;


    public Condition() {
    }

    public Condition(LogicalExpression expression) {
        this(expression, null);
    }

    public Condition(LogicalExpression expression, String errorCode) {
        this.expression = expression;
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public boolean execute(Map<String, Map<String, Object>> requestContext) {
        return null == expression || expression.execute(requestContext);
    }


    @Override
    public String toString() {
        return new StringBuilder().append("Condition{")
                .append("errorCode='").append(errorCode).append('\'')
                .append(", logicalExpression=").append(expression)
                .append('}').toString();
    }
}
