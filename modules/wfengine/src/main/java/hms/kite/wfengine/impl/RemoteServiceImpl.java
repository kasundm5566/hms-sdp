/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.wfengine.impl;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 * <p/>
 */
@XmlRootElement(name = "remote-service")
@XmlAccessorType(XmlAccessType.NONE)
public class RemoteServiceImpl extends ServiceImpl {
    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceImpl.class);

    @Expose
    @SerializedName("service")
    @XmlElement(name = "service")
    private WorkflowImpl remoteWorkflow;

    public RemoteServiceImpl(String serviceName, WorkflowImpl remoteWorkflow, ServiceImpl success, ErrorService error) {
        super(serviceName, success, error);
        this.remoteWorkflow = remoteWorkflow;
    }

    public RemoteServiceImpl(String serviceName, WorkflowImpl remoteWorkflow, ErrorService error) {
        this(serviceName, remoteWorkflow, null, error);
    }

    public RemoteServiceImpl(String serviceName, WorkflowImpl remoteWorkflow, String errorCode, String errorDesc) {
        super(serviceName, null, errorCode);
        this.remoteWorkflow = remoteWorkflow;
    }

    @Override
    public void execute(Map<String, Map<String, Object>> requestContext) {
        super.execute(requestContext);
    }
}
