/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.wfengine.impl;

import hms.kite.util.SdpException;
import hms.kite.util.StatusDescriptionBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static hms.kite.util.KiteErrorBox.unknownErrorCode;
import static hms.kite.util.KiteErrorBox.successCode;
import static hms.kite.util.KiteKeyBox.*;
import static java.lang.Boolean.parseBoolean;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ResponseBuilder {
    private static final Logger logger = LoggerFactory.getLogger(ResponseBuilder.class);

    private static StatusDescriptionBuilder statusDescriptionBuilder;

    static {
        loadProperties();
    }

    public static Map<String, Object> generateResponse(Map<String, Map<String, Object>> requestContext) {
        return doGenerate(requestContext);
    }

    public static Map<String, Object> generate(Map<String, Object> response) {
        String code = (String) response.get(statusCodeK);
        String description = (String)response.get(statusDescriptionK);
        boolean status = false;
        if (null != code && successCode.equals(code)) {
            status = true;
        }
        return generate(code, description, status);
    }

    public static Map<String, Object> generate(Map<String,Map<String,Object>> requestContext, Exception e) {
        SdpException wfe;
        if (e instanceof SdpException) {
            wfe = (SdpException) e;
        } else {
            wfe = new SdpException(unknownErrorCode, e);
        }
        requestContext.get(requestK).put(statusK, Boolean.toString(false));
        requestContext.get(requestK).put(statusCodeK, wfe.getErrorCode());
        String errorDescription = wfe.getErrorDescription();
        if (null == errorDescription) {
            errorDescription = getErrorDescription(wfe.getErrorCode(), requestContext);
        }
        requestContext.get(requestK).put(statusDescriptionK, errorDescription);
        return generateResponse(requestContext);
    }

    private static Map<String, Object> doGenerate(Map<String, Map<String, Object>> requestContext) {
        if (requestContext.get(requestK).get(statusCodeK) == null) {
            return generateSuccess();
        } else {
            String code = (String)requestContext.get(requestK).get(statusCodeK);
            String description = (String)requestContext.get(requestK).get(statusDescriptionK);
            if (null == description) {
                description = getErrorDescription(code, requestContext);
                requestContext.get(requestK).put(statusDescriptionK, description);
            }
            return generate(code, description, parseBoolean((String) requestContext.get(requestK).get(statusK)));
        }
    }

    public static Map<String, Object> generate(String code, String description, Boolean status) {
        Map<String, Object> resp = new HashMap<String, Object>();
        resp.put(statusK, status.toString());
        resp.put(statusCodeK, code);
        resp.put(statusDescriptionK, description);
        return resp;

    }

    public static Map<String, Object> generate(String code, Boolean status,
                                               Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> resp = new HashMap<String, Object>();
        resp.put(appK, requestContext.get(appK));
        resp.put(statusK, status.toString());
        resp.put(statusCodeK, code);
        resp.put(statusDescriptionK, getErrorDescription(code, requestContext));
        return resp;
    }

    public static Map<String, Object> generateSuccess() {
        Map<String, Object> resp = new HashMap<String, Object>();
        resp.put(statusK, Boolean.toString(true));
        resp.put(statusCodeK, successCode);
        resp.put(statusDescriptionK, "Success");
        return resp;
    }

    public static Map<String, Object> generateSuccess(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> resp = new HashMap<String, Object>();
        resp.put(statusK, Boolean.toString(true));
        resp.put(statusCodeK, successCode);
        resp.put(statusDescriptionK, getErrorDescription(successCode, requestContext));
        return resp;
    }

    private Map<String, Object> convert2NblResponse(Map<String, Map<String, Object>> requestContext) {
    	return convert2NblErrorResponse(requestContext.get(requestK));
    }

    public static Map<String, Object> convert2NblErrorResponse(Map<String, Object> response) {
    	Map<String, Object> nblResp = new HashMap<String, Object>();

        if (response.containsKey(versionK) && null != response.get(versionK)) {
            nblResp.put(versionK, response.get(versionK));
        } else {
            nblResp.put(versionK, defaultVersionK);
        }

        nblResp.put(statusCodeNblK, response.get(statusCodeK));
        nblResp.put(statusDescriptionNblK, response.get(statusDescriptionK));
        return nblResp;
    }

    public static String getErrorDescription(String errorCode, Map<String, Map<String, Object>> requestContext) {
        return statusDescriptionBuilder.createStatusDescription(errorCode, requestContext);
    }

    private static void loadProperties() {
        statusDescriptionBuilder = new StatusDescriptionBuilder("status-message");
        statusDescriptionBuilder.init();
    }
}
