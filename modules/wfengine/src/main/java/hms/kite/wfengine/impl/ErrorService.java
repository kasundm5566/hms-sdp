/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.wfengine.impl;

import java.util.Map;

import static hms.kite.util.KiteErrorBox.unknownErrorCode;
import static hms.kite.util.KiteKeyBox.*;


/**
 * This class just set the status instead of calling any gateways
 * <p/>
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ErrorService extends ServiceImpl {

    private String errorCode;

    public static final ErrorService DEFAULT_ERROR_SERVICE = new ErrorService(unknownErrorCode);


    public ErrorService() {
    }

    private ErrorService(String errorCode) {
        this();
        this.errorCode = errorCode;
    }

    public static ErrorService createErrorService(String errorCode) {
        return new ErrorService(errorCode);
    }

    @Override
    public void execute(Map<String, Map<String, Object>> requestContext) {
        requestContext.get(requestK).put(statusCodeK, errorCode);
        requestContext.get(requestK).put(statusK, Boolean.toString(false));
    }

    public String getErrorCode() {
        return errorCode;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("ErrorService{")
                .append("errorCode=").append(errorCode)
                .append('\'').append('}').toString();
    }
}
