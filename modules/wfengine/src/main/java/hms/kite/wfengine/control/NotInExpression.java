/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.wfengine.control;

import java.util.Collection;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class NotInExpression extends LogicalExpression {
    private String inConditionKey;
    private String inConditionParamKey;
    private String requestKey;
    private String requestParamKey;

    public NotInExpression(String requestKey, String requestParamKey, String inConditionsKey, String inConditionParamKey) {
        this.inConditionKey = inConditionsKey;
        this.inConditionParamKey = inConditionParamKey;
        this.requestKey = requestKey;
        this.requestParamKey = requestParamKey;
    }

    @Override
    public boolean execute(Map<String, Map<String, Object>> requestContext) {
        Object requestValue = requestContext.get(requestKey).get(requestParamKey);
        Object contextValue = requestContext.get(inConditionKey).get(inConditionParamKey);

        if (null == contextValue || null == requestValue) {
            return false;
        } else if (contextValue instanceof Collection) {
            Collection c = (Collection) contextValue;
            return !((Collection) contextValue).contains(requestValue);
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("Context[").append(inConditionKey).append("] Field [").append(inConditionParamKey).append("] NOT IN ")
                .append("Context[").append(requestKey).append("] Field [").append(requestParamKey);
        sb.append("}");
        return sb.toString();
    }

}
