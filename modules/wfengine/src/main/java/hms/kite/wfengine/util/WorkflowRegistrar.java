package hms.kite.wfengine.util;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.transport.Channel;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.Map;


/**
 * Spring helper to register channels to Workflow at the Spring context.xml of the respective module
 * $LastChangedDate 7/13/11 10:45 AM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
public class WorkflowRegistrar {

    private final Map registry;

    private String key;
    private Workflow value;

    /**
     * Constructor for Dependency injection
     * @param registry
     */
    @Autowired
    public WorkflowRegistrar(Map registry) {
      this.registry = registry;
    }

    public void setKey(String key) {
      this.key = key;
    }

    public void setValue(Workflow value) {
      this.value = value;
    }

    @PostConstruct
    public void registerMapping() {
      registry.put(key, value);
    }

}
