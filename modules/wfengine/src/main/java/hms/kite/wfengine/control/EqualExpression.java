/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.wfengine.control;

import hms.kite.util.KeyNameSpaceResolver;

import java.util.Map;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 */
public class EqualExpression extends LogicalExpression {

	private String contextKey;
	private String contextParamKey;
	private String requestKey;
	private String requestParamKey;
	private Object condition;
	private String spaceSeparatedRequestKey;
	private String spaceSeparatedContextKey;
	private String[] individualKeys;

	public EqualExpression(String spaceSeparatedRequestKey, String spaceSeparatedContextKey) {
		this.spaceSeparatedRequestKey = spaceSeparatedRequestKey;
		this.spaceSeparatedContextKey = spaceSeparatedContextKey;
	}

	public EqualExpression(String requestKey, String requestParamKey, String contextKey, String contextParamKey) {
		this.contextKey = contextKey;
		this.contextParamKey = contextParamKey;
		this.requestKey = requestKey;
		this.requestParamKey = requestParamKey;
	}

	public EqualExpression(String requestKey, String requestParamKey, Object condition) {
		this.requestKey = requestKey;
		this.requestParamKey = requestParamKey;
		this.condition = condition;
	}

	/**
	 * Check whether given condition matches with the given namespace keys
	 *
	 * @param condition
	 *            - condition value to match
	 * @param individualKeys
	 *            - Namespace key to get the value from request context.
	 */
	public EqualExpression(Object condition, String... individualKeys) {
		this.individualKeys = individualKeys;
		this.condition = condition;
	}

	@Override
	public boolean execute(Map<String, Map<String, Object>> requestContext) {
		if (individualKeys != null) {
			Object requestValue = KeyNameSpaceResolver.data((Map) requestContext, individualKeys);
			if (null == requestValue) {
				return false;
			}
			if (condition != null) {
				return condition.equals(requestValue);
			} else {
				return false;
			}
		} else if (null != contextKey) {
			Object contextValue = requestContext.get(contextKey).get(contextParamKey);
			Object requestValue = requestContext.get(requestKey).get(requestParamKey);
			if (null == contextValue || null == requestValue) {
				return false;
			}
			return contextValue.equals(requestValue);

		} else if (null != condition) {
            Map<String, Object> value = requestContext.get(requestKey);
            if (value == null) {
                return false;
            }
            Object requestValue = value.get(requestParamKey);
			if (null == requestValue) {
				return false;
			}
			return condition.equals(requestValue);
		} else {
			Object request = KeyNameSpaceResolver.data((Map) requestContext, spaceSeparatedRequestKey);
			Object context = KeyNameSpaceResolver.data((Map) requestContext, spaceSeparatedContextKey);
			if (null != request && null != context && ((String) request).equals((String) context)) {
				return true;
			}
			return false;
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		if (null == condition) {
			sb.append("Context[").append(contextKey).append("] Field [").append(contextParamKey).append("] EQUALS ")
					.append("Context[").append(requestKey).append("] Field [").append(requestParamKey);
		} else {
			sb.append("Condition[").append(condition).append("] EQUALS ").append("Context[").append(requestKey)
					.append("] Field [").append(requestParamKey);
		}
		sb.append("}");
		return sb.toString();
	}
}
