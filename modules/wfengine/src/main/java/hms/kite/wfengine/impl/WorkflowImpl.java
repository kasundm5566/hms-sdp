/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.wfengine.impl;

import hms.kite.wfengine.Service;
import hms.kite.wfengine.Workflow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class WorkflowImpl implements Workflow {

    private static final Logger logger = LoggerFactory.getLogger(WorkflowImpl.class);

    private Service service;

    public WorkflowImpl() {
        this(new ServiceImpl());
    }

    public WorkflowImpl(Service reqService) {
        this.service = reqService;
    }

    public Service getService() {
        return service;
    }

    public void executeWorkflow(Map<String, Map<String, Object>> requestContext) {

        logger.debug("Start executing the workflow ...");

        Service currentService = service;
        currentService.execute(requestContext);

        while (currentService.hasNext()) {

            currentService = currentService.next();
            currentService.execute(requestContext);
        }
    }

    public String getName() {
        return service.getName();
    }
}
