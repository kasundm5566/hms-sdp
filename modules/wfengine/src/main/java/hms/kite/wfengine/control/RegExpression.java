/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.wfengine.control;

import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class RegExpression extends LogicalExpression {
    private String contextKey;
    private String contextParamKey;
    private String requestKey;
    private String requestParamKey;

    public RegExpression(String requestKey, String requestParamKey, String contextKey, String contextParamKey) {
        this.contextKey = contextKey;
        this.contextParamKey = contextParamKey;
        this.requestKey = requestKey;
        this.requestParamKey = requestParamKey;
    }

    @Override
    public boolean execute(Map<String, Map<String, Object>> requestContext) {

        String contextValue = (String) requestContext.get(contextKey).get(contextParamKey);
        String requestValue = (String) requestContext.get(requestKey).get(requestParamKey);

        if (null == contextValue || null == requestValue) {
            return false;
        }

        return  requestValue.matches(contextValue);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("Context[").append(contextKey).append("] Field [").append(contextParamKey).append("] MATCHES ")
                .append("Context[").append(requestKey).append("] Field [").append(requestParamKey);
        sb.append("}");
        return sb.toString();
    }
}
