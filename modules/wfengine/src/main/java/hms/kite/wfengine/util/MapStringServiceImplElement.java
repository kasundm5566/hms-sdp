package hms.kite.wfengine.util;

import hms.common.rest.util.MapElement;
import hms.kite.wfengine.impl.ServiceImpl;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Useful implementation for serialising Map<String,String> types.
 *
 * @version $Id$
 */
public final class MapStringServiceImplElement extends MapElement<String, ServiceImpl> {

    @XmlAttribute
    @Override
    public String getKey() {
        return key;
    }

    @XmlElement
    @Override
    public ServiceImpl getValue() {
        return value;
    }

    @Override
    public void setKey(final String key) {
        this.key = key;
    }

    @Override
    public void setValue(final ServiceImpl value) {
        this.value = value;
    }

}