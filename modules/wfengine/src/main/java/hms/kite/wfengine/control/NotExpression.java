/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.wfengine.control;

import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class NotExpression extends LogicalExpression {

    private LogicalExpression expression;

    public NotExpression(LogicalExpression expression) {
        this.expression = expression;
    }

    @Override
    public boolean execute(Map<String, Map<String, Object>> requestContext) {
        if (null == expression) {
            return true;
        }

        return !expression.execute(requestContext);
    }

    public void setExpression(LogicalExpression expression) {
        this.expression = expression;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("NOT ").append(expression);
        sb.append("}");
        return sb.toString();
    }
}
