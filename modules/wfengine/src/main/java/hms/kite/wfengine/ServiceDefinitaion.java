/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.wfengine;

import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ServiceDefinitaion {

    private Map serviceDefinitionMap;

    public <T> T getProperty(String key, Class<T> t) {
        return (T) serviceDefinitionMap.get(key);
    }

    public Map getServiceDefinitionMap() {
        return serviceDefinitionMap;
    }

    public void setServiceDefinitionMap(Map serviceDefinitionMap) {
        this.serviceDefinitionMap = serviceDefinitionMap;
    }
}
