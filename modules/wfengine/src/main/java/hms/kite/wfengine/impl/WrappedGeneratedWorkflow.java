package hms.kite.wfengine.impl;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Generic abstract class to generateResponse another wrapped workflow
 *
 * $LastChangedDate 7/13/11 5:55 PM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 * TODO: It would speedup things if the generated workflow is cached. Perhaps we could use Spring DI for caching
 */
public abstract class WrappedGeneratedWorkflow implements Workflow {

    private static final Logger LOGGER = LoggerFactory.getLogger(WrappedGeneratedWorkflow.class);

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void executeWorkflow(Map<String, Map<String, Object>> requestContext) {
        LOGGER.trace("executing workflow");
        generateWorkflow().executeWorkflow(requestContext);
    }


    //TODO remove this
    public String getName(Channel channel) {
        if(channel != null) {
            return channel.toString();
        } else {
            return "null";
        }
    }

    /**
     * Generate a workflow to be executed by (this) wrapped workflow
     *
     * @return
     */
    protected abstract Workflow generateWorkflow();
}
