/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.wfengine;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import static org.testng.AssertJUnit.assertEquals;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */

@ContextConfiguration(locations = {"classpath:wfengine-beans-test.xml"})
public class ServiceDefinitionTest {

    ServiceDefinitaion serviceDefinition;

    @BeforeTest
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext(new String[]{"wfengine-beans-test.xml"});
        serviceDefinition = (ServiceDefinitaion) ctx.getBean("serviceDefinition");
    }

    @Test
    public void testGetProperty() {
        String value = serviceDefinition.getProperty("testKey", String.class);
        assertEquals("TestValue", value);
    }
}
