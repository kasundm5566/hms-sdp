/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.wfengine.control;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static hms.kite.util.KiteErrorBox.unknownErrorCode;
import static hms.kite.util.KiteKeyBox.*;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ConditionTest {
    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();

    @BeforeTest
    public void setUp() {
        // Add your code here
    }

    @AfterTest
    public void tearDown() {
        // Add your code here
    }

    @Test
    public void testInCondition() {
        InExpression expression = new InExpression(requestK, "subscriberType", new Object[]{"PREPAID"});

        Condition condition = new Condition(expression, null);

        Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String, Object>>();
        requestContext.put(requestK, Collections.<String, Object>singletonMap("subscriberType", "null"));
        assertFalse(condition.execute(requestContext), "Checking Null");

        requestContext.put(requestK, Collections.<String, Object>singletonMap("subscriberType", ""));
        assertFalse(condition.execute(requestContext), "Checking Empty");

        requestContext.put(requestK, Collections.<String, Object>singletonMap("subscriberType", "POSTPAID"));
        assertFalse(condition.execute(requestContext), "Checking Different Value");

        requestContext.put(requestK, Collections.<String, Object>singletonMap("operatorType", "PREPAID"));
        assertFalse(condition.execute(requestContext), "Checking Different Value");

        requestContext.put(requestK, Collections.<String, Object>singletonMap("subscriberType", "PREPAID"));
        assertTrue(condition.execute(requestContext), "Checking Success Condition");

        requestContext.put(requestK, Collections.<String, Object>singletonMap(spSelectedServicesK, Collections.singletonList(smsK)));
        expression = new InExpression(requestK, spSelectedServicesK, new Object[]{smsK});
        condition = new Condition(expression, null);
        assertTrue(condition.execute(requestContext), "Checking Success Condition");

        expression = new InExpression(requestK, spSelectedServicesK, new Object[]{"mms"});
        condition = new Condition(expression, null);
        assertFalse(condition.execute(requestContext), "Checking Success Condition");

        requestContext.put(requestK, Collections.<String, Object>singletonMap(spSelectedServicesK, new ArrayList()));
        expression = new InExpression(requestK, spSelectedServicesK, new Object[]{"mms"});
        condition = new Condition(expression, null);
        assertFalse(condition.execute(requestContext), "Checking Success Condition");

        requestContext.put(requestK, Collections.<String, Object>singletonMap(spSelectedServicesK, null));
        expression = new InExpression(requestK, spSelectedServicesK, new Object[]{"mms"});
        condition = new Condition(expression, null);
        assertFalse(condition.execute(requestContext), "Checking Success Condition");
    }

    @Test
    public void testAndCondition() {
        InExpression A = new InExpression(requestK, "subscriberType", new Object[]{"PREPAID", "POSTPAID"});

        InExpression B = new InExpression(requestK, "operatorType", new Object[]{"M1", "Starhub", "Singtel"});

        AndExpression andOperand = new AndExpression(A, B);

        Condition condition = new Condition(andOperand, null);

        Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String, Object>>();

        requestContext.put(requestK, Collections.<String, Object>singletonMap("subscriberType", "null"));
        assertFalse(condition.execute(requestContext), "Checking Null");

        requestContext.put(requestK, Collections.<String, Object>singletonMap("subscriberType", ""));
        assertFalse(condition.execute(requestContext), "Checking Empty");

        requestContext.put(requestK, Collections.<String, Object>singletonMap("subscriberType", "POSTPAID"));
        assertFalse(condition.execute(requestContext), "Checking Different Value");

        requestContext.put(requestK, Collections.<String, Object>singletonMap("operatorType", "PREPAID"));
        assertFalse(condition.execute(requestContext), "Checking Different Value");

        requestContext.put(requestK, Collections.<String, Object>singletonMap("operatorType", "M1"));
        assertFalse(condition.execute(requestContext), "Checking Different Value");

        requestContext.put(requestK, new HashMap<String, Object>() {
            {
                put("operatorType", "Dialog");
                put("subscriberType", "PREPAID");
            }
        });
        assertFalse(condition.execute(requestContext), "Checking invalid values");

        requestContext.put(requestK, new HashMap<String, Object>() {
            {
                put("operatorType", "M1");
                put("subscriberType", "MVNO");
            }
        });
        assertFalse(condition.execute(requestContext), "Checking invalid values");

        requestContext.put(requestK, new HashMap<String, Object>() {
            {
                put("operatorType", "Dialog");
                put("subscriberType", "MVNO");
            }
        });
        assertFalse(condition.execute(requestContext), "Checking invalid values");

        requestContext.put(requestK, new HashMap<String, Object>() {
            {
                put("operatdsdsorType", "M1");
                put("subscriberType", "PREPAID");
            }
        });
        assertFalse(condition.execute(requestContext), "Checking with invalid field");

        requestContext.put(requestK, new HashMap<String, Object>() {
            {
                put("operatorType", "M1");
                put("subscriberType", "PREPAID");
            }
        });
        assertTrue(condition.execute(requestContext), "Checking Success Condition");
    }

    @Test
    public void testOrCondition() {
        InExpression A = new InExpression(requestK, "subscriberType", new Object[]{"PREPAID", "POSTPAID"});

        InExpression B = new InExpression(requestK, "operatorType", new Object[]{"M1", "Starhub", "Singtel"});

        OrExpression orExpression = new OrExpression(A, B);

        Condition condition = new Condition(orExpression, null);

        Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String, Object>>();

        requestContext.put(requestK, Collections.<String, Object>singletonMap("subscriberType", "null"));
        assertFalse(condition.execute(requestContext), "Checking Null");

        requestContext.put(requestK, Collections.<String, Object>singletonMap("subscriberType", ""));
        assertFalse(condition.execute(requestContext), "Checking Empty");

        requestContext.put(requestK, new HashMap<String, Object>() {
            {
                put("operatorType", "Dialog");
                put("subscriberType", "MVNO");
            }
        });
        assertFalse(condition.execute(requestContext), "Checking invalid values");

        requestContext.put(requestK, new HashMap<String, Object>() {
            {
                put("operatdsdsorType", "M1");
                put("sudssdbscriberType", "PREPAID");
            }
        });
        assertFalse(condition.execute(requestContext), "Checking with correct field");

        requestContext.put(requestK, Collections.<String, Object>singletonMap("operatorType", "PREPAID"));
        assertFalse(condition.execute(requestContext), "Checking Correct Value");

        requestContext.put(requestK, Collections.<String, Object>singletonMap("subscriberType", "POSTPAID"));
        assertTrue(condition.execute(requestContext), "Checking Correct Value");

        requestContext.put(requestK, Collections.<String, Object>singletonMap("operatorType", "M1"));
        assertTrue(condition.execute(requestContext), "Checking Correct Value");

        requestContext.put(requestK, new HashMap<String, Object>() {
            {
                put("operatorType", "Dialog");
                put("subscriberType", "PREPAID");
            }
        });
        assertTrue(condition.execute(requestContext), "Checking Correct Values");

        requestContext.put(requestK, new HashMap<String, Object>() {
            {
                put("operatorType", "M1");
                put("subscriberType", "MVNO");
            }
        });
        assertTrue(condition.execute(requestContext), "Checking Correct Values");

        requestContext.put(requestK, new HashMap<String, Object>() {
            {
                put("operatdsdsorType", "M1");
                put("subscriberType", "PREPAID");
            }
        });
        assertTrue(condition.execute(requestContext), "Checking with correct field");

        requestContext.put(requestK, new HashMap<String, Object>() {
            {
                put("operatorType", "M1");
                put("subscriberType", "PREPAID");
            }
        });
        assertTrue(condition.execute(requestContext), "Checking Success Condition");
    }

    @Test
    public void testEqualCondition() {
        EqualExpression maskingApplied = new EqualExpression(appK, maskNumberK, true);
        Condition condition = new Condition(maskingApplied, unknownErrorCode);

        Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String, Object>>();
        requestContext.put(appK, Collections.<String, Object>singletonMap(maskNumberK, true));
        assertTrue(condition.execute(requestContext));

        requestContext.put(appK, Collections.<String, Object>singletonMap(maskNumberK, false));
        assertFalse(condition.execute(requestContext));

        requestContext.put(appK, Collections.<String, Object>singletonMap("something-else", true));
        assertFalse(condition.execute(requestContext));

        maskingApplied = new EqualExpression(requestK, maskNumberK, appK, maskNumberK);
        condition = new Condition(maskingApplied, unknownErrorCode);

        requestContext = new HashMap<String, Map<String, Object>>();
        requestContext.put(appK, Collections.<String, Object>singletonMap(maskNumberK, true));
        requestContext.put(requestK, Collections.<String, Object>singletonMap(maskNumberK, true));
        assertTrue(condition.execute(requestContext));

        requestContext.put(requestK, Collections.<String, Object>singletonMap(maskNumberK, false));
        assertFalse(condition.execute(requestContext));
    }

    @Test
    public void testContainsCondition() {
        Map<String, Object> ncsSla = new HashMap<String, Object>();
        Map<String, Object> moSla = new HashMap<String, Object>();
        Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String, Object>>();

        Condition condition = new Condition(new ContainsExpression("ncs", null));
        assertFalse(condition.execute(requestContext));

        requestContext.put("ncs", ncsSla);
        condition = new Condition(new ContainsExpression("ncs", "ncs"));
        assertFalse(condition.execute(requestContext));

        ncsSla.put("mo", moSla);
        condition = new Condition(new ContainsExpression("ncs", "mo"));
        assertTrue(condition.execute(requestContext));
    }
}
