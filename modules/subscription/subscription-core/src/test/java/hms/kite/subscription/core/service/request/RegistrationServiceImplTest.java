package hms.kite.subscription.core.service.request;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import hms.kite.subscription.core.domain.SubscriptionResponse;
import hms.kite.subscription.core.domain.SubscriptionStatus;
import hms.kite.subscription.core.mandate.client.MandateClient;
import hms.kite.subscription.core.mandate.client.api.CancelMandateRequest;
import hms.kite.subscription.core.mandate.client.api.CancelMandateResponse;
import hms.kite.subscription.core.mandate.client.api.CreateMandateRequest;
import hms.kite.subscription.core.mandate.client.api.CreateMandateResponse;
import hms.kite.subscription.core.service.repo.Subscription;
import hms.kite.subscription.core.service.repo.SubscriptionRepoService;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.springframework.data.mongodb.core.query.Query;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

import static hms.kite.subscription.core.service.request.RequestContextParser.*;
import static org.testng.Assert.*;

public class RegistrationServiceImplTest {

    RegistrationServiceImpl registrationService = new RegistrationServiceImpl();

    @Test
    public void testRegister1() throws Exception {

        final Mockery context = new Mockery();
        final SubscriptionRepoService subscriptionRepoService =
                context.mock(SubscriptionRepoService.class, "subscription-repo");
        final MandateClient mandateClient = context.mock(MandateClient.class, "mandate-client");

        registrationService.setMandateClient(mandateClient);
        registrationService.setSubscriptionRepoService(subscriptionRepoService);

        context.checking(
                new Expectations() {
                    {
                        oneOf(mandateClient).createMandate(with(any(CreateMandateRequest.class)));
                        will(returnValue(Optional.of(new CreateMandateResponse("909898774847", "S1000", "Success"))));
                        oneOf(subscriptionRepoService).query(with(any(Query.class)));
                        will(returnValue(Collections.emptyList()));
                        oneOf(subscriptionRepoService).create(with(any(Subscription.class)));
                        oneOf(subscriptionRepoService).upsert(with(any(Subscription.class)));
                    }
                }
        );

        final Map requestContext =
                (Map)ImmutableMap.of("ncs", ImmutableMap.of("charging", ImmutableMap.of("type", "flat")),
                                     "request", ImmutableMap.of("sender-address", "94775038419"));

        final SubscriptionResponse response1 = registrationService.register(requestContext);
        assertEquals(response1, SubscriptionResponse.CHARGING_PENDING);
        context.assertIsSatisfied();
    }

    @Test
    public void testRegister2() throws InvalidRequestContextException {

        final Mockery context = new Mockery();
        final SubscriptionRepoService subscriptionRepoService =
                context.mock(SubscriptionRepoService.class, "subscription-repo");
        final MandateClient mandateClient = context.mock(MandateClient.class, "mandate-client");

        registrationService.setMandateClient(mandateClient);
        registrationService.setSubscriptionRepoService(subscriptionRepoService);

        context.checking(
                new Expectations() {
                    {
                        final Subscription subscription = new Subscription();
                        subscription.setCurrentStatus(SubscriptionStatus.UNREGISTERED.name());

                        oneOf(mandateClient).createMandate(with(any(CreateMandateRequest.class)));
                        will(returnValue(Optional.of(new CreateMandateResponse("909898774847", "S1000", "Success"))));
                        oneOf(subscriptionRepoService).query(with(any(Query.class)));
                        will(returnValue(Arrays.asList(subscription)));
                        exactly(2).of(subscriptionRepoService).upsert(with(any(Subscription.class)));
                    }
                }
        );

        final Map requestContext =
                (Map)ImmutableMap.of("ncs", ImmutableMap.of("charging", ImmutableMap.of("type", "flat")),
                                     "request", ImmutableMap.of("sender-address", "94775038419"));

        final SubscriptionResponse response2 = registrationService.register(requestContext);
        assertEquals(response2, SubscriptionResponse.CHARGING_PENDING);
        context.assertIsSatisfied();
    }

    @Test
    public void testUnRegister1() throws Exception {
        final Mockery context = new Mockery();
        final SubscriptionRepoService subscriptionRepoService =
                context.mock(SubscriptionRepoService.class, "subscription-repo");
        final MandateClient mandateClient = context.mock(MandateClient.class, "mandate-client");

        registrationService.setMandateClient(mandateClient);
        registrationService.setSubscriptionRepoService(subscriptionRepoService);

        context.checking(
                new Expectations() {
                    {
                        final Subscription subscription = new Subscription();
                        subscription.setCurrentStatus(SubscriptionStatus.REGISTERED.name());
                        subscription.setMandateId("9098373737");

                        oneOf(mandateClient).cancelMandate(with(any(CancelMandateRequest.class)));
                        will(returnValue(Optional.of(new CancelMandateResponse("S1000", "Success"))));
                        oneOf(subscriptionRepoService).query(with(any(Query.class)));
                        will(returnValue(Arrays.asList(subscription)));
                        oneOf(subscriptionRepoService).upsert(with(any(Subscription.class)));
                    }
                }
        );


        final Map requestContext =
                (Map)ImmutableMap.of("ncs", ImmutableMap.of("charging", ImmutableMap.of("type", "flat")),
                                    "request", ImmutableMap.of("sender-address", "94775038419"));

        final SubscriptionResponse response = registrationService.unRegister(requestContext);
        context.assertIsSatisfied();
    }

    @Test
    public void testSubscriptionStatus1() throws Exception {
        final Mockery context = new Mockery();
        final SubscriptionRepoService subscriptionRepoService =
                context.mock(SubscriptionRepoService.class, "subscription-repo");

        registrationService.setSubscriptionRepoService(subscriptionRepoService);

        context.checking(
                new Expectations() {
                    {
                        oneOf(subscriptionRepoService).findOne(with(any(Query.class)));
                        will(returnValue(Optional.absent()));
                    }
                }
        );

        final SubscriptionStatus status = registrationService.subscriptionStatus("94775038419", "APP_0911");

        context.assertIsSatisfied();
        assertEquals(status, SubscriptionStatus.UNREGISTERED);
    }

    @Test
    public void testCreateMandate() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        final Mockery context = new Mockery();
        final MandateClient mandateClient = context.mock(MandateClient.class, "mandate-client");
        final SubscriptionRepoService subscriptionRepoService = context.mock(SubscriptionRepoService.class, "subscription-repo");

        registrationService.setMandateClient(mandateClient);
        registrationService.setSubscriptionRepoService(subscriptionRepoService);
        registrationService.setTrialsAllowed(true);

        final Method createMandateMethod = registrationService.getClass().getDeclaredMethod("createMandate", Subscription.class, Optional.class);
        createMandateMethod.setAccessible(true);


        context.checking(new Expectations(){
            {
                oneOf(mandateClient).createMandate(with(any(CreateMandateRequest.class)));
                will(returnValue(Optional.of(new CreateMandateResponse("8988388383", "S1000", "success"))));
                oneOf(subscriptionRepoService).upsert(with(any(Subscription.class)));
            }
        });

        final Subscription build = new Subscription.SubscriptionBuilder().
                                                    withMsisdn("94775038419").
                                                    withAppId("APP_0911").
                                                    withConsumedTrials(1).
                                                    build();

        final SubscriptionResponse response = (SubscriptionResponse)createMandateMethod.invoke(registrationService, build, Optional.of(new Trial(TrialsTimeUnit.DAY, 9, 2)));

        context.assertIsSatisfied();
        assertEquals(response, SubscriptionResponse.SUCCESS);
    }
}