package hms.kite.subscription.core.mandate.notification;

import org.apache.cxf.jaxrs.client.WebClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import javax.ws.rs.core.MediaType;

import static hms.kite.util.GsonUtil.getGson;
import static org.testng.Assert.*;

@ContextConfiguration(locations = "classpath:bean-test.xml")
public class MandateNotificationEndPointTest extends AbstractTestNGSpringContextTests{

    final ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("bean-test.xml");
    private @Value("${hschedule.interface.address}") String hscheduleInterfaceAddress;


    @BeforeTest
    public void setUp() {
        context.start();
    }

    @Test
    public void testMandateExecutionNotification() throws Exception {
        final WebClient webClient = WebClient.create(hscheduleInterfaceAddress).
                path("/mandate-notify").
                accept(MediaType.APPLICATION_JSON_TYPE).
                type(MediaType.APPLICATION_JSON_TYPE);

        final String resp = webClient.post(getGson().toJson(
                new NotifyRequest("success", "S1000", "9098098098", 2, 1, "27.00", "94778909897", "SUCCESS", "recurring")), String.class);

        assertEquals(resp.isEmpty(), true);
    }

    @AfterTest
    public void tearDown() {
        context.stop();
    }
}