package hms.kite.subscription.core.service.request;

import hms.kite.subscription.core.service.repo.SubscriptionRepoService;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.springframework.data.mongodb.core.query.Query;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static hms.kite.subscription.core.service.repo.Subscription.*;
import static org.testng.Assert.*;

public class StatServiceImplTest {

    StatServiceImpl statService = new StatServiceImpl();

    @Test
    public void testGetSubscriberBaseSize() throws Exception {

        final Mockery context = new Mockery();
        final SubscriptionRepoService subscriptionRepo =
                context.mock(SubscriptionRepoService.class, "subscription-mongo-repo");
        statService.setSubscriptionRepoService(subscriptionRepo);

        context.checking(new Expectations() {
            {
                oneOf(subscriptionRepo).query(with(any(Query.class)));
                will(returnValue(Arrays.asList(new SubscriptionBuilder().build(),
                        new SubscriptionBuilder().build())));
            }
        });

        final long count = statService.getSubscriberBaseSize("APP_00011");
        context.assertIsSatisfied();
        assertEquals(count, 2);
    }

    @Test
    public void testGetSubscribedAppsByMsisdn() throws Exception {
        final Mockery context = new Mockery();
        final SubscriptionRepoService subscriptionRepo =
                context.mock(SubscriptionRepoService.class, "subscription-mongo-repo");
        statService.setSubscriptionRepoService(subscriptionRepo);

        context.checking(new Expectations() {
            {
                oneOf(subscriptionRepo).query(with(any(Query.class)));
                will(returnValue(Arrays.asList(new SubscriptionBuilder().withAppId("APP_0019").build(),
                        new SubscriptionBuilder().withAppId("APP_0089").build())));
            }
        });

        final List<String> apps = statService.getSubscribedAppsByMsisdn("94775038419");
        context.assertIsSatisfied();
        assertEquals(apps.get(0), "APP_0019");
        assertEquals(apps.size(), 2);
    }

    @Test
    public void testGetSubscribers() throws Exception {

    }
}
