package hms.kite.subscription.core.service.request;


import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import hms.kite.subscription.core.service.repo.Subscription;
import org.testng.annotations.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import static hms.kite.subscription.core.service.request.RequestContextParser.*;
import static org.testng.Assert.*;

public class RequestContextParserTest {

    @Test
    public void testGetFieldIfPresent() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        final Method method = RequestContextParser.class.getDeclaredMethod("getFieldIfPresent", String.class, Map.class);
        method.setAccessible(true);
        final Object value =
                method.invoke(null, "lvl1.lvl2.lvl3", ImmutableMap.of("lvl1", ImmutableMap.of("lvl2", ImmutableMap.of("lvl3", "hello"))));

        assertEquals(value,  Optional.of("hello"));
    }

    @Test
    public void testSubscription() throws InvalidRequestContextException {
        final Object mapOfMap =
                ImmutableMap.of("ncs", ImmutableMap.of("charging", ImmutableMap.of("amount", "30")),
                                "request", ImmutableMap.of("sender-address", "94775038419"));

        final Subscription subscription =  subscription((Map<String, Map<String, Object>>) mapOfMap);
        assertEquals(subscription.getChargingAmount(), Double.valueOf("30"));
    }

    @Test
    public void testCalculateTrailDuration() {
        final int dayInMillis = 1 * 24 * 60 * 60 * 1000;
        final int hourInMillis = 1 * 60 * 60 * 1000;

        final long duration1 = Trial.calculateTrailDuration(new Trial(TrialsTimeUnit.DAY, 1, 2));
        final long duration2 = Trial.calculateTrailDuration(new Trial(TrialsTimeUnit.HOUR, 1, 2));

        assertEquals(duration1, dayInMillis);
        assertEquals(duration2, hourInMillis);
    }


    @Test
    public void testParseMsisdnThroughHttpSubscription() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        final Method parseMsisdnThroughHttpSubscription =
                RequestContextParser.class.getDeclaredMethod("parseMsisdnThroughHttpSubscription", String.class);
        parseMsisdnThroughHttpSubscription.setAccessible(true);
        final Optional<String> msisdn = (Optional<String>) parseMsisdnThroughHttpSubscription.invoke(null, "tel:94775038419");
        assertTrue(msisdn.isPresent());
        assertEquals(msisdn.get(), "94775038419");

        final Optional<String> invalidSubscriberId = (Optional<String>) parseMsisdnThroughHttpSubscription.invoke(null, "tel:   94775038419");
        assertFalse(invalidSubscriberId.isPresent());
    }

    @Test
    public void testExtractMsisdn() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        final Method extractMsisdn =
                RequestContextParser.class.getDeclaredMethod("extractMsisdn", Map.class);

        extractMsisdn.setAccessible(true);

        String msisdn = (String) extractMsisdn.invoke(null, ImmutableMap.of("request", ImmutableMap.of("sender-address", "94775038419")));
        assertEquals(msisdn, "94775038419");
    }
}
