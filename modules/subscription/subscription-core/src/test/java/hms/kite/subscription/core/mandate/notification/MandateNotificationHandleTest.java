package hms.kite.subscription.core.mandate.notification;


import com.google.common.base.Optional;
import hms.kite.subscription.core.NotificationHandler;
import hms.kite.subscription.core.SubscriptionService;
import hms.kite.subscription.core.SubscriptionServiceImpl;
import hms.kite.subscription.core.domain.SubscriptionStatus;
import hms.kite.subscription.core.service.repo.Subscription;
import hms.kite.subscription.core.service.repo.SubscriptionRepoService;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.springframework.data.mongodb.core.query.Query;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Map;

import static hms.kite.subscription.core.service.repo.Subscription.*;
import static hms.kite.util.GsonUtil.getGson;

public class MandateNotificationHandleTest {


    private MandateNotificationEndPoint mandateNotificationEndPoint;

    @BeforeClass
    public void setUp() throws Exception {
        mandateNotificationEndPoint = new MandateNotificationEndPoint();
    }

    @Test
    public void testMandateExecutionNotification() {
        final Mockery context = new Mockery();

        final SubscriptionRepoService subscriptionRepoService = context.mock(SubscriptionRepoService.class, "subscription-repo-service");
        final NotificationHandler notificationHandler = context.mock(NotificationHandler.class, "notification-handler");

        final SubscriptionService subscriptionService = new SubscriptionServiceImpl();
        subscriptionService.setNotificationHandler(notificationHandler);

        mandateNotificationEndPoint.setSubscriptionRepoService(subscriptionRepoService);
        mandateNotificationEndPoint.setSubscriptionService(subscriptionService);
        mandateNotificationEndPoint.setCurrencyCode("LKR");

        final String resp = getGson().toJson(new NotifyRequest("success", "S1000", "9098098098", 2, 1, "27.00", "94778909897", "SUCCESS", "recurring"));

        context.checking(new Expectations(){
            {
                oneOf(subscriptionRepoService).findOne(with(any(Query.class)));

                will(returnValue(Optional.of(new SubscriptionBuilder().
                                                    withCurrentStatus(SubscriptionStatus.REG_PENDING.name()).
                                                    withOperator("dialog").
                                                    withCorrelationId("09098809809").
                                                    withAppId("APP_89090").
                                                    withSpId("SP_098909").
                                                    withShortcode("77000").
                                                    withChargingAmount(Double.valueOf("30")).
                                                    withKeyword("learntamil").
                                                    withMsisdn("94770909898").
                                                    withStatus("S1000").
                                                    build())));

                oneOf(subscriptionRepoService).upsert(with(any(Subscription.class)));
                oneOf(notificationHandler).sendNotification(with(any(Map.class)));
            }
        });

        mandateNotificationEndPoint.mandateExecutionNotification(resp);

        context.assertIsSatisfied();

    }
}