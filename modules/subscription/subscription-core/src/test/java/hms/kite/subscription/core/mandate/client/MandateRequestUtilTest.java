package hms.kite.subscription.core.mandate.client;

import hms.kite.subscription.core.mandate.client.api.CreateMandateRequest;
import hms.kite.util.GsonUtil;
import org.joda.time.DateTime;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.testng.Assert.*;
import static hms.kite.subscription.core.mandate.client.MandateRequestUtil.*;

import java.lang.reflect.InvocationTargetException;

public class MandateRequestUtilTest {

    @BeforeClass
    public void setUp() {
    }

    @Test
    public void testTraverseKeyStack() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        final CreateMandateRequest createMandateRequest = new CreateMandateRequestBuilder().build();
        assertEquals(createMandateRequest.getStartCriteria(), StartCriteria.NOW.getId());
    }

    @Test
    public void testConvertMandateRequestToJson() {
        final CreateMandateRequest createMandateRequest =
                new CreateMandateRequestBuilder().withStartDate(new DateTime()).withAmount(Double.valueOf("30")).build();
        GsonUtil.getGson().toJson(createMandateRequest);
    }
}
