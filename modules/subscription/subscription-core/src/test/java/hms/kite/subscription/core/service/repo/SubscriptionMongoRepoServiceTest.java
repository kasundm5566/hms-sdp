package hms.kite.subscription.core.service.repo;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.mongodb.DBObject;
import hms.kite.subscription.core.domain.SubscriptionStatus;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.*;

import javax.annotation.Resource;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;

import static hms.kite.subscription.core.domain.SubscriptionStatus.*;
import static hms.kite.subscription.core.service.repo.Subscription.*;
import static hms.kite.util.KiteKeyBox.*;
import static org.springframework.data.mongodb.core.query.Criteria.*;

import static org.springframework.data.mongodb.core.query.Query.query;
import static org.testng.Assert.*;

@ContextConfiguration(locations = {"classpath:bean-test.xml"})
public class SubscriptionMongoRepoServiceTest extends AbstractTestNGSpringContextTests{

    @Autowired
    SubscriptionRepoService subscriptionRepoService;

    @Resource
    @Qualifier("subscription.mongo.template")
    private MongoTemplate subscriptionRepoTemplate;

    @BeforeClass
    public void setUp() {
    }

    @BeforeClass
    public void testCreate() throws Exception {
        subscriptionRepoTemplate.dropCollection(Subscription.class);

        final List<Subscription> subscriptions = Arrays.asList(
            new SubscriptionBuilder().withMsisdn("94775038419").withAppId("APP_09111").withCurrentStatus(REGISTERED.name()).build(),
            new SubscriptionBuilder().withMsisdn("94775038419").withAppId("APP_09190").withCurrentStatus(REGISTERED.name()).build(),
            new SubscriptionBuilder().withMsisdn("94775038419").withAppId("APP_08700").withCurrentStatus(TRIAL.name()).build(),
            new SubscriptionBuilder().withMsisdn("94775038419").withAppId("APP_08800").withCurrentStatus(UNREGISTERED.name()).build(),
            new SubscriptionBuilder().withMsisdn("94775038009").withAppId("APP_09111").withCurrentStatus(REGISTERED.name()).build(),
            new SubscriptionBuilder().withMsisdn("94775038232").withAppId("APP_09111").withCurrentStatus(TRIAL.name()).build(),
            new SubscriptionBuilder().withMsisdn("94775038999").withAppId("APP_09111").withCurrentStatus(UNREGISTERED.name()).build(),
            new SubscriptionBuilder().withMsisdn("94775038420").withAppId("APP_09112").withCurrentStatus(UNREGISTERED.name()).build(),
            new SubscriptionBuilder().withMsisdn("94775038421").withAppId("APP_09112").withCurrentStatus(TRIAL.name()).build(),
            new SubscriptionBuilder().withMsisdn("94775038424").withAppId("APP_09112").withCurrentStatus(INITIAL.name()).build(),
            new SubscriptionBuilder().withMsisdn("94775038422").withAppId("APP_09112").withCurrentStatus(REGISTERED.name()).build(),
            new SubscriptionBuilder().withMsisdn("94775038423").withAppId("APP_09115").withCurrentStatus(REGISTERED.name()).build(),
            new SubscriptionBuilder().withMsisdn("94775038423").withAppId("APP_09116").withCurrentStatus(REGISTERED.name()).build(),
            new SubscriptionBuilder().withMsisdn("94775038423").withAppId("APP_09116").withCurrentStatus(UNREGISTERED.name()).build(),
            new SubscriptionBuilder().withMsisdn("94775038423").withAppId("APP_09117").withCurrentStatus(UNREGISTERED.name()).build()
        );

        for (Subscription subscription : subscriptions) {
            subscriptionRepoService.create(subscription);
        }
    }

    @Test
    public void testFindById() {
        final List<Subscription> subscriptions = subscriptionRepoService.query(new Query(where("app-id").is("APP_09112")));
        assertEquals(subscriptions.size(), 4);
        final ObjectId id = subscriptions.get(0).getId();
        final Subscription subscription = subscriptionRepoService.findById(id);
        assertEquals(subscription.getAppId(), "APP_09112");

    }

    @Test
    public void testQuery1(){
        final List<Subscription> subscriptions = subscriptionRepoService.query(new Query(where("app-id").is("APP_09111")));
        assertEquals(subscriptions.size(), 4);
        assertEquals(subscriptions.get(0).getAppId(), "APP_09111");
    }

    @Test
    public void testQuery3() {
        final Criteria criteria = where(currentStatusK).in(Arrays.asList(REG_PENDING.name(), REGISTERED.name(), TEMPORARY_BLOCKED.name(), TRIAL.name()));
        Query query = query(where(appIdK).is("APP_09112")).addCriteria(criteria);
        final List<Subscription> subscriptions = subscriptionRepoService.query(query);
        assertEquals(subscriptions.size(), 2);
    }

    @Test
    public void testQuery4() {
        Criteria registeredSubscriptionFilter = where(currentStatusK).in(Arrays.asList(REG_PENDING.name(), REGISTERED.name(), TEMPORARY_BLOCKED.name(), TRIAL.name()));
        final Query query = query(where(msisdnK).is("94775038423")).addCriteria(registeredSubscriptionFilter);

        final List<Subscription> subscriptions = subscriptionRepoService.query(query);
        assertEquals(subscriptions.size(), 2);
    }

    @Test
    public void testFindOne(){
        final Optional<Subscription> subscriptions = subscriptionRepoService.findOne(new Query(where(msisdnK).is("94775038419")));
        assertEquals(subscriptions.isPresent(), true);
        assertEquals(subscriptions.get().getAppId(), "APP_09111");
    }

    @Test
    public void testCount() throws Exception {
        Criteria registeredSubscriptionFilter = where(currentStatusK).in(Arrays.asList(REG_PENDING.name(), REGISTERED.name(), TEMPORARY_BLOCKED.name(), TRIAL.name()));
        final Query query = query(where(msisdnK).is("94775038423")).addCriteria(registeredSubscriptionFilter);
        final long count = subscriptionRepoService.count(query);
        assertEquals(count, 2);
    }

    @Test
    public void testQueryDbObject() throws Exception {
        Criteria registeredSubscriptionFilter = where(currentStatusK).in(Arrays.asList(REG_PENDING.name(), REGISTERED.name(), TEMPORARY_BLOCKED.name(), TRIAL.name()));
        final Query query = query(where(msisdnK).is("94775038423")).addCriteria(registeredSubscriptionFilter);

        final List<DBObject> subscriptions = subscriptionRepoService.queryDbObjects(query);
        assertEquals(subscriptions.size(), 2);
    }

    @Test
    public void testQueryDbObject2() throws Exception {
        final Query nextSubscribersBatchQuery = query(where(appIdK).
                is("APP_09111")).
                addCriteria(where(currentStatusK).in(Arrays.asList(SubscriptionStatus.REGISTERED.name(), SubscriptionStatus.TRIAL.name()))).
                skip(0).
                limit(4);

        final List<DBObject> subscriptions = subscriptionRepoService.queryDbObjects(nextSubscribersBatchQuery);

        final List<String> msisdnList = Lists.transform(subscriptions, new Function<DBObject, String>() {
            @Override
            public String apply(DBObject input) {
                return (String) input.get(msisdnK);
            }
        });

        assertEquals(msisdnList, Arrays.asList("94775038419", "94775038009", "94775038232"));
    }

    @Test
    public void testQueryDbObject3() throws Exception {
        List<DBObject> subscriptions = subscriptionRepoService.queryDbObjects(query(where(msisdnK).is("94775038419")).
                addCriteria(where(currentStatusK).in(Arrays.asList(SubscriptionStatus.REGISTERED.name(), SubscriptionStatus.TRIAL.name()))));

        final List<String> appList = Lists.transform(subscriptions, new Function<DBObject, String>() {
            @Override
            public String apply(DBObject input) {
                return (String)input.get(appIdK);
            }
        });
        assertEquals(appList, Arrays.asList("APP_09111", "APP_09190", "APP_08700"));
    }

    @Test
    public void testFindOne2() throws Exception {
        final Query query = new Query().addCriteria(where(appIdK).is("APP_09111")).addCriteria(where(msisdnK).is("94775038419"));
        final Optional<Subscription> subscriptionOptional = subscriptionRepoService.findOne(query);
        assertTrue(subscriptionOptional.isPresent());
        assertEquals(subscriptionOptional.get().getAppId(), "APP_09111");

        final List<Subscription> subscriptionList = subscriptionRepoService.query(query);
        assertEquals(subscriptionList.size(), 1);
    }

    @AfterClass
    public void tearDown() throws UnknownHostException {
        subscriptionRepoTemplate.dropCollection(Subscription.class);
    }
}