package hms.kite.subscription.core.mandate.client;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.google.common.base.Optional;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import hms.kite.subscription.core.mandate.client.api.CancelMandateRequest;
import hms.kite.subscription.core.mandate.client.api.CancelMandateResponse;
import hms.kite.subscription.core.mandate.client.api.CreateMandateRequest;
import hms.kite.subscription.core.mandate.client.api.CreateMandateResponse;

import hms.kite.subscription.core.mandate.notification.MandateEventListener;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static hms.kite.subscription.core.mandate.client.MandateRequestUtil.*;
import static hms.kite.subscription.core.mandate.notification.MandateEventListener.MandateEvent.*;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.testng.Assert.assertEquals;

@ContextConfiguration(locations={"classpath:bean-test.xml"})
public class MandateClientTest extends AbstractTestNGSpringContextTests{

    @Autowired
    MandateClientImpl mandateClient;

    final WireMockServer wireMockServer = new WireMockServer(18000);

    @BeforeClass
    public void setUp() {
        WireMock.configureFor("127.0.0.1", 18000);
        wireMockServer.start();
    }

    @Test
    public void testCreateMandate() throws Exception {

        final Mockery context = new Mockery();
        final MandateEventListener mandateEventListener = context.mock(MandateEventListener.class, "mandate-event-listener");
        mandateClient.setMandateEventListener(mandateEventListener);

        context.checking(new Expectations(){
            {
                oneOf(mandateEventListener).handle(CreateMandateRespReceived);
            }
        });

        WireMock.stubFor(post(urlEqualTo("/create-mandate"))
                .withHeader("Content-Type", WireMock.equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"mandate-id\":\"101311111603030003\",\"status-code\":\"S1000\",\"status-description\":\"request processed successfully\"}")));

        final Optional<CreateMandateResponse> response = mandateClient.createMandate(new CreateMandateRequestBuilder().build());

        assertEquals(response.isPresent(), true);
        assertEquals(response.get().getStatusCode(), "S1000");
    }

    @Test
    public void testCancelMandate() throws Exception {

        final Mockery context = new Mockery();
        final MandateEventListener mandateEventListener = context.mock(MandateEventListener.class, "mandate-event-listener");
        mandateClient.setMandateEventListener(mandateEventListener);

        context.checking(new Expectations(){
            {
                oneOf(mandateEventListener).handle(CancelMandateRespReceived);
            }
        });

        WireMock.stubFor(post(urlEqualTo("/cancle-mandate"))
                .withHeader("Content-Type", WireMock.equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"status-code\":\"S1000\",\"status-description\":\"request processed successfully\"}")));

        final Optional<CancelMandateResponse> response = mandateClient.cancelMandate(new CancelMandateRequest("101311111603030003"));

        assertEquals(response.isPresent(), true);
        assertEquals(response.get().getStatusCode(), "S1000");
    }

    @Test
    public void testCreateMandateFailureTrap() {
        final Mockery context = new Mockery();
        final MandateEventListener mandateEventListener = context.mock(MandateEventListener.class, "mandate-event-listener");
        mandateClient.setMandateEventListener(mandateEventListener);

        WireMock.stubFor(post(urlEqualTo("/create-mandate")).
                        withHeader("Content-Type", WireMock.equalTo("application/json")).
                        willReturn(aResponse().withFixedDelay(500000).
                                                withStatus(200).
                                                withHeader("Content-Type", "application/json").
                                                withBody("{\"mandate-id\":\"101311111603030003\",\"status-code\":\"S1000\",\"status-description\":\"request processed successfully\"}")));

        context.checking(new Expectations(){
            {
                oneOf(mandateEventListener).handle(CreateMandateRespFailed);
            }
        });

        final Optional<CreateMandateResponse> createMandate = mandateClient.createMandate(new CreateMandateRequestBuilder().build());

        assertEquals(createMandate.isPresent(), false);

        context.assertIsSatisfied();
    }

    @Test(dependsOnMethods = {"testCreateMandateFailureTrap"})
    public void testCreateMandateSuccessTrap() {
        final Mockery context = new Mockery();
        final MandateEventListener mandateEventListener = context.mock(MandateEventListener.class, "mandate-event-listener");
        mandateClient.setMandateEventListener(mandateEventListener);

        WireMock.stubFor(post(urlEqualTo("/create-mandate")).
                withHeader("Content-Type", WireMock.equalTo("application/json")).
                willReturn(aResponse().
                        withStatus(200).
                        withHeader("Content-Type", "application/json").
                        withBody("{\"mandate-id\":\"101311111603030003\",\"status-code\":\"S1000\",\"status-description\":\"request processed successfully\"}")));

        context.checking(new Expectations(){
            {
                oneOf(mandateEventListener).handle(CreateMandateRespReceived);
            }
        });

        final Optional<CreateMandateResponse> createMandate = mandateClient.createMandate(new CreateMandateRequestBuilder().build());

        assertEquals(createMandate.isPresent(), true);

        context.assertIsSatisfied();

    }

    @Test(dependsOnMethods = {"testCreateMandateSuccessTrap"})
    public void testCancelMandateFailureTrap() {
        final Mockery context = new Mockery();
        final MandateEventListener mandateEventListener = context.mock(MandateEventListener.class, "mandate-event-listener");
        mandateClient.setMandateEventListener(mandateEventListener);


        WireMock.stubFor(post(urlEqualTo("/cancle-mandate"))
                .withHeader("Content-Type", WireMock.equalTo("application/json"))
                .willReturn(aResponse()
                        .withFixedDelay(500000)
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"status-code\":\"S1000\",\"status-description\":\"request processed successfully\"}")));

        context.checking(new Expectations(){
            {
                oneOf(mandateEventListener).handle(CancelMandateRespFailed);
            }
        });

        final Optional<CancelMandateResponse> cancelMandate = mandateClient.cancelMandate(new CancelMandateRequest("101311111603030003"));

        assertEquals(cancelMandate.isPresent(), false);

        context.assertIsSatisfied();
    }

    @Test(dependsOnMethods = {"testCancelMandateFailureTrap"})
    public void testCancelMandateSuccessTrap() {
        final Mockery context = new Mockery();
        final MandateEventListener mandateEventListener = context.mock(MandateEventListener.class, "mandate-event-listener");
        mandateClient.setMandateEventListener(mandateEventListener);

        WireMock.stubFor(post(urlEqualTo("/cancle-mandate"))
                .withHeader("Content-Type", WireMock.equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"status-code\":\"S1000\",\"status-description\":\"request processed successfully\"}")));

        context.checking(new Expectations(){
            {
                oneOf(mandateEventListener).handle(CancelMandateRespReceived);
            }
        });

        final Optional<CancelMandateResponse> cancelMandate = mandateClient.cancelMandate(new CancelMandateRequest("101311111603030003"));

        assertEquals(cancelMandate.isPresent(), true);

        context.assertIsSatisfied();
    }

    @Test
    public void gsonMarshallingTest() {
        final Gson gson = new GsonBuilder().serializeNulls().create();
        final CreateMandateRequest createMandateRequest = new CreateMandateRequestBuilder().build();
        assertEquals(Optional.fromNullable(createMandateRequest.getEndDate()).isPresent(), false);
    }

    @AfterMethod
    public void cleanUp() {
        WireMock.reset();
    }

    @AfterClass
    public void tearDown() {
        wireMockServer.stop();
    }
}
