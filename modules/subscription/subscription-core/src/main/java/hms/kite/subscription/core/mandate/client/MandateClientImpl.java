package hms.kite.subscription.core.mandate.client;

import com.google.common.base.Optional;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import hms.common.rest.util.client.AbstractWebClient;
import hms.kite.subscription.core.mandate.client.api.CancelMandateRequest;
import hms.kite.subscription.core.mandate.client.api.CancelMandateResponse;
import hms.kite.subscription.core.mandate.client.api.CreateMandateRequest;
import hms.kite.subscription.core.mandate.client.api.CreateMandateResponse;
import static hms.kite.util.GsonUtil.*;

import hms.kite.subscription.core.mandate.notification.MandateEventListener;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;

public class MandateClientImpl extends AbstractWebClient implements MandateClient {

    private static final Logger logger = LoggerFactory.getLogger(MandateClientImpl.class);
    private static final Gson gson;

    private MandateEventListener mandateEventListener;

    static {
        gson = new GsonBuilder().serializeNulls().create();
    }

    private final String mandateUrl;
    private final WebClient createMandateWebClient;
    private final WebClient cancelMandateWebClient;

    public MandateClientImpl(String mandateUrl, HTTPClientPolicy httpClientPolicy) {
        this.mandateUrl = mandateUrl;


        createMandateWebClient = WebClient.create(this.mandateUrl).
                                            path("/create-mandate").
                                            accept(MediaType.APPLICATION_JSON_TYPE).
                                            type(MediaType.APPLICATION_JSON_TYPE);


        cancelMandateWebClient = WebClient.create(this.mandateUrl).
                                            path("/cancel-mandate").
                                            accept(MediaType.APPLICATION_JSON_TYPE).
                                            type(MediaType.APPLICATION_JSON_TYPE);

        WebClient.getConfig(createMandateWebClient).getHttpConduit().setClient(httpClientPolicy);
        WebClient.getConfig(cancelMandateWebClient).getHttpConduit().setClient(httpClientPolicy);

    }

    @Override
    public Optional<CreateMandateResponse> createMandate(CreateMandateRequest request) {
        try {
            final String createMandateReqBody = gson.toJson(request, CreateMandateRequest.class);
            final String response = createMandateWebClient.post(createMandateReqBody, String.class);
            mandateEventListener.handle(MandateEventListener.MandateEvent.CreateMandateRespReceived);
            return  Optional.of(getGson().fromJson(response, CreateMandateResponse.class));
        } catch (ClientWebApplicationException e) {
            mandateEventListener.handle(MandateEventListener.MandateEvent.CreateMandateRespFailed);
            return Optional.absent();
        }
    }

    @Override
    public Optional<CancelMandateResponse> cancelMandate(CancelMandateRequest request) {
        try {
            final String cancelMandateReq = gson.toJson(request, CancelMandateRequest.class);
            final String response = cancelMandateWebClient.post(cancelMandateReq, String.class);
            mandateEventListener.handle(MandateEventListener.MandateEvent.CancelMandateRespReceived);
            return  Optional.of(getGson().fromJson(response, CancelMandateResponse.class));
        } catch (ClientWebApplicationException e) {
            mandateEventListener.handle(MandateEventListener.MandateEvent.CancelMandateRespFailed);
            return Optional.absent();
        }
    }

    public void setMandateEventListener(MandateEventListener mandateEventListener) {
        this.mandateEventListener = mandateEventListener;
    }
}