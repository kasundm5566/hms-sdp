package hms.kite.subscription.core.service.request;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.mongodb.DBObject;
import hms.kite.subscription.core.domain.SubscriptionStatus;
import hms.kite.subscription.core.service.repo.SubscriptionRepoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

import static hms.kite.util.KiteKeyBox.*;
import static org.springframework.data.mongodb.core.query.Criteria.*;
import static org.springframework.data.mongodb.core.query.Query.*;

public class StatServiceImpl implements StatService {

    private static final Logger logger = LoggerFactory.getLogger(StatServiceImpl.class);
    private static final Criteria registeredSubscriptionFilter;
    private static final List<SubscriptionStatus> activeSubscriberStatuses;

    static {

        /* Only registered and trail status are consider as active subscriber state. */
        activeSubscriberStatuses =  ImmutableList.<SubscriptionStatus>builder().
                add(SubscriptionStatus.REGISTERED).
                add(SubscriptionStatus.TRIAL).
                build();

        registeredSubscriptionFilter =
                where(currentStatusK).in(Lists.transform(activeSubscriberStatuses, new Function<SubscriptionStatus, String>() {
                    @Override
                    public String apply(SubscriptionStatus input) {
                        return input.name();
                    }
                }));
    }

    @Autowired
    private SubscriptionRepoService subscriptionRepoService;

    @Override
    public long getSubscriberBaseSize(String appId) {
        final long baseSize = subscriptionRepoService.count(query(where(appIdK).is(appId)).addCriteria(registeredSubscriptionFilter));

        logger.info("Found [{}] subscribers for application [{}].", baseSize, appId);
        return baseSize;
    }

    @Override
    public List<String> getSubscribedAppsByMsisdn(String msisdn) {

        List<DBObject> subscriptions = subscriptionRepoService.queryDbObjects(query(where(msisdnK).is(msisdn)).addCriteria(registeredSubscriptionFilter));

        final List<String> appList = Lists.transform(subscriptions, new Function<DBObject, String>() {
            @Override
            public String apply(DBObject input) {
                return (String)input.get(appIdK);
            }
        });
        logger.info("Msisdn [{}], subscribed app list = [{}].", msisdn, appList);
        return appList;
    }

    public void setSubscriptionRepoService(SubscriptionRepoService subscriptionRepoService) {
        this.subscriptionRepoService = subscriptionRepoService;
    }
}