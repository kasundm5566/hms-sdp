package hms.kite.subscription.core.service.request;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import hms.kite.subscription.core.domain.SubscriptionResponse;
import hms.kite.subscription.core.domain.SubscriptionStatus;
import hms.kite.subscription.core.mandate.client.MandateClient;
import hms.kite.subscription.core.mandate.client.api.CancelMandateRequest;
import hms.kite.subscription.core.mandate.client.api.CancelMandateResponse;
import hms.kite.subscription.core.mandate.client.api.CreateMandateRequest;
import hms.kite.subscription.core.mandate.client.api.CreateMandateResponse;
import hms.kite.subscription.core.service.repo.Subscription;
import hms.kite.subscription.core.service.repo.SubscriptionRepoService;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.query.Query;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.google.common.collect.ImmutableMap.*;
import static hms.kite.subscription.core.mandate.client.MandateRequestUtil.*;
import static hms.kite.subscription.core.service.request.RegistrationServiceImpl.MandateCreationStrategy.*;
import static hms.kite.subscription.core.service.request.RegistrationServiceImpl.RegistrationStateMachine.*;
import static hms.kite.subscription.core.service.request.RequestContextParser.*;
import static hms.kite.subscription.core.service.request.RequestContextParser.Trial.calculateTrailDuration;
import static hms.kite.util.KiteKeyBox.*;
import static org.springframework.data.mongodb.core.query.Criteria.where;

public class RegistrationServiceImpl implements RegistrationService {

    private static final Logger logger = LoggerFactory.getLogger(RegistrationServiceImpl.class);

    private SubscriptionRepoService subscriptionRepoService;

    private MandateClient mandateClient;

    private boolean trialsAllowed;

    private static final List<SubscriptionStatus> alreadyRegisteredStatusList;


    static {
           alreadyRegisteredStatusList = ImmutableList.<SubscriptionStatus>builder().
                                                add(SubscriptionStatus.TEMPORARY_BLOCKED).
                                                add(SubscriptionStatus.REG_PENDING).
                                                add(SubscriptionStatus.REGISTERED).
                                                add(SubscriptionStatus.TRIAL).
                                                build();
    }

    @Override
    public SubscriptionResponse register(Map<String, Map<String, Object>> requestContext) throws InvalidRequestContextException {

        Subscription newSubscription = subscription(requestContext);
        logger.debug("Subscription = [{}], retrieved from the request context = [{}].", newSubscription, requestContext);

        final boolean isFreeApp = isFreeApp(requestContext);
        final Optional<Trial> trialPeriod = getTrialPeriod(requestContext);

        logger.debug("Application is free = [{}], Application trial period = [{}].", isFreeApp, trialPeriod.isPresent() ? trialPeriod.get() : "N/A");

        List<Subscription> subscriptions = getSubscriptions(newSubscription);

        if(subscriptions.size() > 1) {
            logger.error("More than one record ([{}]), retrieved for particular app/msisdn combination.", subscriptions.size());
            return SubscriptionResponse.INTERNAL_ERROR;

        } else if(subscriptions.size() == 1){
            final Subscription subscription = subscriptions.get(0);
            logger.debug("Existing subscription record = [{}] found for particular app/msisdn combination.", subscription);
            SubscriptionStatus curState = SubscriptionStatus.valueOf(subscription.getCurrentStatus());
            if(alreadyRegisteredStatusList.contains(curState)) {
                return SubscriptionResponse.ALREADY_REGISTERED;
            }
            return updateExistingSubscription(subscription, newSubscription, isFreeApp, trialPeriod);
        } else {
            return createNewSubscription(newSubscription, isFreeApp, trialPeriod);
        }
    }

    @Override
    public SubscriptionResponse unRegister(Map<String, Map<String, Object>> requestContext) throws InvalidRequestContextException {
        final Subscription cancelSubscription = subscription(requestContext);

        List<Subscription> subscriptions = getSubscriptions(cancelSubscription);

        if(subscriptions.size() > 1) {
            return SubscriptionResponse.INTERNAL_ERROR;
        } else if(subscriptions.size() == 1){
            return unRegisterSubscription(subscriptions.get(0));
        } else {
            return SubscriptionResponse.USER_NOT_REGISTERED;
        }
    }

    public Optional<Subscription> getSubscriber(String msisdn, String appId) {
        logger.debug("Finding the subscriber by the appId [{}]/msisdn [{}] combination.", appId, msisdn);
        Optional<Subscription> subscriptions = subscriptionRepoService.findOne(createQuery(msisdn, appId));
        return subscriptions;
    }

    private SubscriptionResponse unRegisterSubscription(Subscription existingSubscription) {
        SubscriptionStatus currentStatus = SubscriptionStatus.valueOf(existingSubscription.getCurrentStatus());
        SubscriptionStatus nextStatus = SubscriptionStatus.UNREGISTERED;

        if(possibleTransition(currentStatus, nextStatus)) {

            final boolean noMandateExist = existingSubscription.getMandateId().endsWith(Subscription.DUMMY_MANDATE_ID);

            if(noMandateExist) {
                existingSubscription.setCurrentStatus(SubscriptionStatus.UNREGISTERED.name());
                subscriptionRepoService.upsert(existingSubscription);
                return SubscriptionResponse.SUCCESS;
            } else {
                return cancelMandate(existingSubscription);
            }

        } else {
            return errorTransition(currentStatus, nextStatus);
        }
    }

    private List<Subscription> getSubscriptions(Subscription newSubscription) {
        return subscriptionRepoService.query(createQuery(newSubscription.getMsisdn(), newSubscription.getAppId()));
    }

    @Override
    public SubscriptionStatus subscriptionStatus(String msisdn, String appId) {

        logger.debug("Finding the subscription status by the appId [{}]/msisdn [{}] combination.", appId, msisdn);
        Optional<Subscription> subscriptions = subscriptionRepoService.findOne(createQuery(msisdn, appId));

        return subscriptions.isPresent() ?
                SubscriptionStatus.valueOf(subscriptions.get().getCurrentStatus()) :
                SubscriptionStatus.UNREGISTERED;
    }

    private Query createQuery(String msisdn, String appId) {
        return new Query().
                addCriteria(where(appIdK).is(appId)).
                addCriteria(where(msisdnK).is(msisdn));
    }

    private SubscriptionResponse updateExistingSubscription(Subscription existingSubscription, Subscription newSubscription, boolean freeApp, Optional<Trial> trialPeriod) {

        SubscriptionStatus nextState = freeApp ? SubscriptionStatus.REGISTERED : SubscriptionStatus.INITIAL;
        SubscriptionStatus curState = SubscriptionStatus.valueOf(existingSubscription.getCurrentStatus());

        logger.debug("Trying to transit subscription state from [{}], to [{}].", curState, nextState);

        if(possibleTransition(curState, nextState)) {
            mergeSubscriptionRecord(existingSubscription, newSubscription, nextState);
            subscriptionRepoService.upsert(newSubscription);
            logger.debug("Transition possible from [{}], to [{}]. Updating the subscription [{}].", new Object[]{curState, nextState, newSubscription});

            if(!freeApp) {
                return createMandate(newSubscription, trialPeriod);
            } else {
                logger.debug("Immediately register to the free subscription based application [{}].", newSubscription.getAppId());
                return SubscriptionResponse.SUCCESS_FREE;
            }

        } else {
            return errorTransition(curState, nextState);
        }
    }

    private void mergeSubscriptionRecord(Subscription existingSubscription, Subscription newSubscription, SubscriptionStatus nextState) {
        newSubscription.setId(existingSubscription.getId());
        newSubscription.setCurrentStatus(nextState.name());
        newSubscription.setConsumedTrials(existingSubscription.getConsumedTrials());
    }

    private SubscriptionResponse createNewSubscription(Subscription newSubscription, boolean freeApp, Optional<Trial> trialPeriod) {
        final SubscriptionStatus nextStatus = freeApp ? SubscriptionStatus.REGISTERED : SubscriptionStatus.INITIAL;
        newSubscription.setCurrentStatus(nextStatus.name());
        subscriptionRepoService.create(newSubscription);

        if(!freeApp) {
            return createMandate(newSubscription, trialPeriod);
        }

        return SubscriptionResponse.SUCCESS_FREE;
    }

    private SubscriptionResponse createMandate(Subscription newSubscription, Optional<Trial> trialPeriod) {

        final Optional<DateTime> endDate = Optional.fromNullable(newSubscription.getEndDate()).isPresent() ?
                Optional.of(new DateTime(newSubscription.getEndDate())) : Optional.<DateTime>absent();

        final CreateMandateRequestBuilder createMandateRequestBuilder = new CreateMandateRequestBuilder().
                withAmount(newSubscription.getChargingAmount()).
                withCorrelationId(newSubscription.getCorrelationId()).
                withFrequency(newSubscription.getFrequency()).
                withFromAccountId(newSubscription.getMsisdn()).
                withMsisdn(newSubscription.getMsisdn()).
                withMerchantId(newSubscription.getAppId()).
                withMerchantName(newSubscription.getAppName()).
                withAppId(newSubscription.getAppId()).
                withSpId(newSubscription.getSpId());

        if(endDate.isPresent()) {
            createMandateRequestBuilder.withEndDate(endDate.get());
        }

        // trial period calculation
        final boolean trialPeriodAllowed = trialsAllowed && trialPeriod.isPresent();
        final boolean isTrailEligible = trialPeriodAllowed ? (newSubscription.getConsumedTrials() < trialPeriod.get().getMaxCount()) : false;

        logger.trace("System level trail allowed = [{}], Application level trail allowed = [{}], subscriber eligible for trail = [{}]", new Object[]{trialsAllowed, trialPeriodAllowed, isTrailEligible});

        if(isTrailEligible) {
            logger.debug("Msisdn [{}] is eligible for using application [{}] trial period, total consumed trail count(s) = [{}]/[{}].",
                    new Object[]{newSubscription.getMsisdn(), newSubscription.getAppId(), newSubscription.getConsumedTrials(), trialPeriod.get().getMaxCount()});

            final Trial trial = trialPeriod.get();

            final long trailDuration = calculateTrailDuration(trial);

            createMandateRequestBuilder.withStartDate(new DateTime().plus(trailDuration)).
                                        withStartCriteria(GIVEN_DATE.getId());
        } else {
            createMandateRequestBuilder.withStartDate(new DateTime()).
                    withStartCriteria(NOW.getId());
        }

        CreateMandateRequest createMandateRequest = createMandateRequestBuilder.build();

        Optional<CreateMandateResponse> createMandateResponseOpt = mandateClient.createMandate(createMandateRequest);

        if(!createMandateResponseOpt.isPresent()) {
            return SubscriptionResponse.INTERNAL_ERROR;
        }

        logger.debug("Received response to create mandate request = [{}], response = [{}].", createMandateRequest, createMandateResponseOpt);

        final CreateMandateResponse createMandateResp = createMandateResponseOpt.get();
        if(createMandateResp.getStatusCode().equals("S1000")) {

            if(isTrailEligible) {
                newSubscription.setCurrentStatus(SubscriptionStatus.TRIAL.name());
                newSubscription.setConsumedTrials(newSubscription.getConsumedTrials() + 1);
            } else {
                newSubscription.setCurrentStatus(SubscriptionStatus.REG_PENDING.name());
            }
            newSubscription.setMandateId(createMandateResp.getMandateId());

            subscriptionRepoService.upsert(newSubscription);

            final SubscriptionResponse subscriptionResponse =
                    isTrailEligible ? SubscriptionResponse.SUCCESS : SubscriptionResponse.CHARGING_PENDING;

            return subscriptionResponse;
        } else {
            return SubscriptionResponse.INTERNAL_ERROR;
        }
    }

    private SubscriptionResponse cancelMandate(Subscription existingSubscription) {
        final Optional<CancelMandateResponse> response = mandateClient.cancelMandate(new CancelMandateRequest(existingSubscription.getMandateId()));
        if(!response.isPresent()) {
            return SubscriptionResponse.INTERNAL_ERROR;
        }


        if(response.get().getStatusCode().equals("S1000")) {
            existingSubscription.setCurrentStatus(SubscriptionStatus.UNREGISTERED.name());
            subscriptionRepoService.upsert(existingSubscription);

            return SubscriptionResponse.SUCCESS;
        } else {
            return SubscriptionResponse.INTERNAL_ERROR;
        }
    }

    public void setSubscriptionRepoService(SubscriptionRepoService subscriptionRepoService) {
        this.subscriptionRepoService = subscriptionRepoService;
    }

    public void setMandateClient(MandateClient mandateClient) {
        this.mandateClient = mandateClient;
    }

    public void setTrialsAllowed(boolean trialsAllowed) {
        this.trialsAllowed = trialsAllowed;
    }

    public static class RegistrationStateMachine {

        private static final ImmutableMap<SubscriptionStatus, List<SubscriptionStatus>> stateTransitionMap;
        private static final ImmutableMap<SubscriptionStatus, Map<SubscriptionStatus, SubscriptionResponse>> errorTransitionMap;

        static {
            stateTransitionMap = ImmutableMap.<SubscriptionStatus, List<SubscriptionStatus>>builder().
                    put(SubscriptionStatus.INITIAL, Arrays.asList(SubscriptionStatus.INITIAL,
                                                                    SubscriptionStatus.REG_PENDING,
                                                                    SubscriptionStatus.TRIAL,
                                                                    SubscriptionStatus.REGISTERED)).

                    put(SubscriptionStatus.REG_PENDING, Arrays.asList(SubscriptionStatus.REGISTERED,
                                                                        SubscriptionStatus.UNREGISTERED,
                                                                        SubscriptionStatus.REG_PENDING)).

                    put(SubscriptionStatus.REGISTERED, Arrays.asList(SubscriptionStatus.TEMPORARY_BLOCKED,
                                                                        SubscriptionStatus.UNREGISTERED,
                                                                        SubscriptionStatus.REGISTERED)).

                    put(SubscriptionStatus.TRIAL, Arrays.asList(SubscriptionStatus.REGISTERED,
                                                                SubscriptionStatus.TEMPORARY_BLOCKED,
                                                                SubscriptionStatus.UNREGISTERED)).

                    put(SubscriptionStatus.TEMPORARY_BLOCKED, Arrays.asList(SubscriptionStatus.UNREGISTERED,
                                                                            SubscriptionStatus.REGISTERED,
                                                                            SubscriptionStatus.TEMPORARY_BLOCKED)).

                    put(SubscriptionStatus.UNREGISTERED, Arrays.asList(SubscriptionStatus.INITIAL,
                                                                        SubscriptionStatus.REGISTERED,
                                                                        SubscriptionStatus.TRIAL)).
                    build();


            errorTransitionMap = ImmutableMap.<SubscriptionStatus, Map<SubscriptionStatus, SubscriptionResponse>>builder().
                                        put(SubscriptionStatus.INITIAL,
                                            ImmutableMap.of(SubscriptionStatus.TEMPORARY_BLOCKED, SubscriptionResponse.USER_NOT_REGISTERED,
                                                            SubscriptionStatus.UNREGISTERED, SubscriptionResponse.USER_NOT_REGISTERED)).

                                        put(SubscriptionStatus.REG_PENDING,
                                                ImmutableMap.of(SubscriptionStatus.TEMPORARY_BLOCKED, SubscriptionResponse.CHARGING_PENDING,
                                                        SubscriptionStatus.INITIAL, SubscriptionResponse.CHARGING_PENDING)).

                                        put(SubscriptionStatus.REGISTERED,
                                                ImmutableMap.of(SubscriptionStatus.INITIAL, SubscriptionResponse.ALREADY_REGISTERED)).

                                        put(SubscriptionStatus.TRIAL,
                                                ImmutableMap.of(SubscriptionStatus.INITIAL, SubscriptionResponse.ALREADY_REGISTERED)).

                                        put(SubscriptionStatus.TEMPORARY_BLOCKED,
                                                ImmutableMap.of(SubscriptionStatus.INITIAL, SubscriptionResponse.USER_BLOCKED,
                                                        SubscriptionStatus.TEMPORARY_BLOCKED, SubscriptionResponse.ALREADY_BLOCKED)).

                                        put(SubscriptionStatus.UNREGISTERED,
                                                ImmutableMap.of(SubscriptionStatus.TEMPORARY_BLOCKED, SubscriptionResponse.USER_NOT_REGISTERED,
                                                                SubscriptionStatus.UNREGISTERED, SubscriptionResponse.USER_NOT_REGISTERED)).
                                        build();
        }

        public static boolean possibleTransition(SubscriptionStatus current, SubscriptionStatus next) {
            return stateTransitionMap.containsKey(current) && stateTransitionMap.get(current).contains(next);
        }

        public static SubscriptionResponse errorTransition(SubscriptionStatus current, SubscriptionStatus next) {
            if(errorTransitionMap.containsKey(current) && errorTransitionMap.get(current).containsKey(next)) {
                return errorTransitionMap.get(current).get(next);
            } else {
                return SubscriptionResponse.INTERNAL_ERROR;
            }
        }
    }

    public static enum MandateCreationStrategy {
        NOW("now"),
        GIVEN_DATE("given-date");

        private String id;

        MandateCreationStrategy(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }
    }
}
