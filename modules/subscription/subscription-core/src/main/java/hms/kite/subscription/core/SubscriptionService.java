package hms.kite.subscription.core;

import com.google.common.base.Optional;
import hms.kite.subscription.core.domain.SubscriptionResponse;
import hms.kite.subscription.core.domain.SubscriptionStatus;
import hms.kite.subscription.core.service.repo.Subscription;

import java.util.List;
import java.util.Map;

public interface SubscriptionService {

    public SubscriptionResponse register(Map<String, Map<String, Object>> requestContext);

    public SubscriptionResponse unRegister(Map<String, Map<String, Object>> requestContext);

    public long baseSize(String appId);

    public List<String> subscribedApps(String msisdn);

    public SubscriptionStatus subscriptionStatus(String msisdn, String appId);

    Optional<Subscription> getSubscriber(String msisdn, String appId);

    public void setNotificationHandler(NotificationHandler notificationHandler);

    public NotificationHandler getNotificationHandler();
}
