package hms.kite.subscription.core.mandate.client.api;

import com.google.gson.annotations.SerializedName;

public class CancelMandateRequest {
    @SerializedName("mandate-id") private String mandateId;

    public CancelMandateRequest(String mandateId) {
        this.mandateId = mandateId;
    }

    public String getMandateId() {
        return mandateId;
    }

    public void setMandateId(String mandateId) {
        this.mandateId = mandateId;
    }

    @Override
    public String toString() {
        return "CancelMandateRequest{" +
                "mandateId='" + mandateId + '\'' +
                '}';
    }
}
