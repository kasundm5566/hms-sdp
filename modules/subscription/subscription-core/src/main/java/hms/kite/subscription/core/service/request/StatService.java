package hms.kite.subscription.core.service.request;

import java.util.List;
import java.util.Map;

public interface StatService {

    public long getSubscriberBaseSize(String appId);

    public List<String> getSubscribedAppsByMsisdn(String msisdn);

}
