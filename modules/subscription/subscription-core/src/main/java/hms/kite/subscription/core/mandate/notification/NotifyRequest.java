package hms.kite.subscription.core.mandate.notification;


/*{
        "status-description":"success-code",
        "status-code":"S1000",
        "mandate-id":"101305162002155968",
        "execution-count":2,
        "retry-attempt":0,
        "charge-amount":"30.00",
        "msisdn":"94777310254",
        "pgw-status-code":"SUCCESS",
        "charging-type":"prorate"
}*/

import com.google.gson.annotations.SerializedName;

public class NotifyRequest {
    private @SerializedName("status-description") String statusDescription;
    private @SerializedName("status-code") String statusCode;
    private @SerializedName("mandate-id") String mandateId;
    private @SerializedName("executionCount") int executionCount;
    private @SerializedName("retry-attempt") int retryAttempt;
    private @SerializedName("charge-amount") String chargedAmount;
    private @SerializedName("msisdn") String msisdn;
    private @SerializedName("pgw-status-code") String pgwStatusCode;
    private @SerializedName("charging-type") String chargingType;

    public NotifyRequest(String statusDescription,
                         String statusCode,
                         String mandateId,
                         int executionCount,
                         int retryAttempt,
                         String chargedAmount,
                         String msisdn,
                         String pgwStatusCode,
                         String chargingType) {
        this.statusDescription = statusDescription;
        this.statusCode = statusCode;
        this.mandateId = mandateId;
        this.executionCount = executionCount;
        this.retryAttempt = retryAttempt;
        this.chargedAmount = chargedAmount;
        this.msisdn = msisdn;
        this.pgwStatusCode = pgwStatusCode;
        this.chargingType = chargingType;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public void setMandateId(String mandateId) {
        this.mandateId = mandateId;
    }

    public void setExecutionCount(int executionCount) {
        this.executionCount = executionCount;
    }

    public void setRetryAttempt(int retryAttempt) {
        this.retryAttempt = retryAttempt;
    }

    public void setChargedAmount(String chargedAmount) {
        this.chargedAmount = chargedAmount;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public void setPgwStatusCode(String pgwStatusCode) {
        this.pgwStatusCode = pgwStatusCode;
    }

    public void setChargingType(String chargingType) {
        this.chargingType = chargingType;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public String getMandateId() {
        return mandateId;
    }

    public int getExecutionCount() {
        return executionCount;
    }

    public int getRetryAttempt() {
        return retryAttempt;
    }

    public String getChargedAmount() {
        return chargedAmount;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public String getPgwStatusCode() {
        return pgwStatusCode;
    }

    public String getChargingType() {
        return chargingType;
    }

    @Override
    public String toString() {
        return "NotifyRequest{" +
                "statusDescription='" + statusDescription + '\'' +
                ", statusCode='" + statusCode + '\'' +
                ", mandateId='" + mandateId + '\'' +
                ", executionCount=" + executionCount +
                ", retryAttempt=" + retryAttempt +
                ", chargedAmount='" + chargedAmount + '\'' +
                ", msisdn='" + msisdn + '\'' +
                ", pgwStatusCode='" + pgwStatusCode + '\'' +
                ", chargingType='" + chargingType + '\'' +
                '}';
    }
}
