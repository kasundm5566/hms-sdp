package hms.kite.subscription.core;


import com.google.common.base.Optional;
import hms.kite.subscription.core.domain.SubscriptionResponse;
import hms.kite.subscription.core.domain.SubscriptionStatus;
import hms.kite.subscription.core.service.repo.Subscription;
import hms.kite.subscription.core.service.request.RegistrationService;
import hms.kite.subscription.core.service.request.RequestContextParser;
import hms.kite.subscription.core.service.request.StatService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

public class SubscriptionServiceImpl implements SubscriptionService{

    private NotificationHandler notificationHandler;

    @Autowired
    private StatService statService;

    @Autowired
    private RegistrationService registrationService;

    @Override
    public SubscriptionResponse register(Map<String, Map<String, Object>> requestContext) {
        try {
            return registrationService.register(requestContext);
        } catch (RequestContextParser.InvalidRequestContextException e) {
            return SubscriptionResponse.INTERNAL_ERROR;
        }
    }

    @Override
    public SubscriptionResponse unRegister(Map<String, Map<String, Object>> requestContext) {
        try {
            return registrationService.unRegister(requestContext);
        } catch (RequestContextParser.InvalidRequestContextException e) {
            return SubscriptionResponse.INTERNAL_ERROR;
        }
    }

    @Override
    public long baseSize(String appId) {
        return statService.getSubscriberBaseSize(appId);
    }

    @Override
    public List<String> subscribedApps(String msisdn) {
        return statService.getSubscribedAppsByMsisdn(msisdn);
    }

    public SubscriptionStatus subscriptionStatus(String msisdn, String appId) {
        return registrationService.subscriptionStatus(msisdn, appId);
    }

    public Optional<Subscription> getSubscriber(String msisdn, String appId) {
        return registrationService.getSubscriber(msisdn, appId);
    }

    public void setNotificationHandler(NotificationHandler notificationHandler) {
        this.notificationHandler = notificationHandler;
    }

    public NotificationHandler getNotificationHandler() {
        return notificationHandler;
    }

}
