package hms.kite.subscription.core.mandate.client;

import com.google.common.base.Optional;
import hms.kite.subscription.core.mandate.client.api.CancelMandateRequest;
import hms.kite.subscription.core.mandate.client.api.CancelMandateResponse;
import hms.kite.subscription.core.mandate.client.api.CreateMandateRequest;
import hms.kite.subscription.core.mandate.client.api.CreateMandateResponse;

public interface MandateClient {

    public Optional<CreateMandateResponse> createMandate(CreateMandateRequest request);

    public Optional<CancelMandateResponse> cancelMandate(CancelMandateRequest request);

}
