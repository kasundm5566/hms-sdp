/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.subscription.core.domain;

public enum SubscriptionResponse {
    INITIAL,
    CHARGING_PENDING,
    SUCCESS,
    SUCCESS_COMPLETELY_FREE,
    SUCCESS_ONLY_PER_MSG_MT_CHARGING,
    SUCCESS_PER_MESSAGE_SUBSCRIPTION,
    SUCCESS_UNREGISTERED_DUE_TO_CHARGING_ERROR,
    SUCCESS_UNREGISTERED_FROM_REG_PENDING_DUE_TO_CHARGING_ERROR,
    SUCCESS_FREE,
    ALREADY_REGISTERED,
    ALREADY_BLOCKED,
    USER_BLOCKED,
    USER_NOT_REGISTERED,
    SLA_LIMITATION_ERROR,
    SP_SLA_LIMITATION_ERROR,
    PGW_CONNECTOR_ERROR,
    INVALID_USER_ACCOUNT,
    INTERNAL_ERROR,
    EXTERNAL_ERROR,
    INSUFFICIENT_BALANCE,
    TEMP_SYSTEM_ERROR,
    RECURSIVE_SUCCESS;

}
