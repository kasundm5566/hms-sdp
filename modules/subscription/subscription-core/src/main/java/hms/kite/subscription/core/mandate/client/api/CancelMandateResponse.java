package hms.kite.subscription.core.mandate.client.api;

import com.google.gson.annotations.SerializedName;

public class CancelMandateResponse {

    @SerializedName("status-code") private String statusCode;
    @SerializedName("status-description") private String statusDescription;

    public CancelMandateResponse(String statusCode, String statusDescription) {
        this.statusCode = statusCode;
        this.statusDescription = statusDescription;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    @Override
    public String toString() {
        return "CancelMandateResponse{" +
                "statusCode='" + statusCode + '\'' +
                ", statusDescription='" + statusDescription + '\'' +
                '}';
    }
}
