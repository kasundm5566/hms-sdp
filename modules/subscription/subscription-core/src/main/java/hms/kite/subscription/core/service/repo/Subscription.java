package hms.kite.subscription.core.service.repo;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "subscription")
@CompoundIndexes({
        @CompoundIndex(name = "app_msisdn", def = "{'app-id': 1, 'msisdn': 1}")
})
public class Subscription {

    public static final String DUMMY_MANDATE_ID = "-1";

    private @Id ObjectId id;
    private String status;
    private String lastChargedAmount;
    private String frequency;
    private String correlationId;
    private String currentStatus;
    private String appId;
    private String spId;
    private Date endDate;
    private String appName;
    private Double chargingAmount;
    private boolean expirableApp;
    private String msisdn;
    private String mandateId;
    private String keyword;
    private String shortcode;
    private Date lastModifiedDate;
    private String appStatus;
    private Date lastChargedTime;
    private String coopUserName;
    private Date createdDate;
    private String operator;
    private long version;
    private int consumedTrials;
    private boolean useLegacyMask = false;


    public Subscription() {
    }

    public static class SubscriptionBuilder{

        private Subscription subscription = new Subscription();

        public SubscriptionBuilder withId(ObjectId id) {
            subscription.setId(id);
            return this;
        }

        public SubscriptionBuilder withStatus(String status) {
            subscription.setStatus(status);
            return this;
        }

        public SubscriptionBuilder withLastChargedAmount(String lastChargedAmount) {
            subscription.setLastChargedAmount(lastChargedAmount);
            return this;
        }

        public SubscriptionBuilder withFrequency(String frequency) {
            subscription.setFrequency(frequency);
            return this;
        }

        public SubscriptionBuilder withCorrelationId(String correlationId) {
            subscription.setCorrelationId(correlationId);
            return this;
        }

        public SubscriptionBuilder withCurrentStatus(String currentStatus) {
            subscription.setCurrentStatus(currentStatus);
            return this;
        }

        public SubscriptionBuilder withAppId(String appId) {
            subscription.setAppId(appId);
            return this;
        }

        public SubscriptionBuilder withSpId(String spId) {
            subscription.setSpId(spId);
            return this;
        }

        public SubscriptionBuilder withEndDate(Date endDate) {
            subscription.setEndDate(endDate);
            return this;
        }

        public SubscriptionBuilder withAppName(String appName) {
            subscription.setAppName(appName);
            return this;
        }

        public SubscriptionBuilder withChargingAmount(Double chargingAmount) {
            subscription.setChargingAmount(chargingAmount);
            return this;
        }

        public SubscriptionBuilder withExpirableApp(boolean expirableApp) {
            subscription.setExpirableApp(expirableApp);
            return this;
        }

        public SubscriptionBuilder withMsisdn(String msisdn) {
            subscription.setMsisdn(msisdn);
            return this;
        }

        public SubscriptionBuilder withMandateId(String mandateId) {
            subscription.setMandateId(mandateId);
            return this;
        }


        public SubscriptionBuilder withKeyword(String keyword) {
            subscription.setKeyword(keyword);
            return this;
        }

        public SubscriptionBuilder withShortcode(String shortcode) {
            subscription.setShortcode(shortcode);
            return this;
        }

        public SubscriptionBuilder withLastModifiedDate(Date lastModifiedDate) {
            subscription.setLastModifiedDate(lastModifiedDate);
            return this;
        }

        public SubscriptionBuilder withAppStatus(String appStatus) {
            subscription.setAppStatus(appStatus);
            return this;
        }

        public SubscriptionBuilder withLastChargedTime(Date lastChargedTime) {
            subscription.setLastChargedTime(lastChargedTime);
            return this;
        }


        public SubscriptionBuilder withCoopUserName(String coopUserName) {
            subscription.setCoopUserName(coopUserName);
            return this;
        }


        public SubscriptionBuilder withCreatedDate(Date createdDate) {
            subscription.setCreatedDate(createdDate);
            return this;
        }


        public SubscriptionBuilder withOperator(String operator) {
            subscription.setOperator(operator);
            return this;
        }

        public SubscriptionBuilder withConsumedTrials(int consumedTrials) {
            subscription.setConsumedTrials(consumedTrials);
            return this;
        }

        public SubscriptionBuilder withVersion(long version) {
            subscription.setVersion(version);
            return this;
        }

        public SubscriptionBuilder withUseLegacyMask(boolean use){
            subscription.setUseLegacyMask(use);
            return  this;
        }

        public Subscription build() {
            return subscription;
        }

    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLastChargedAmount() {
        return lastChargedAmount;
    }

    public void setLastChargedAmount(String lastChargedAmount) {
        this.lastChargedAmount = lastChargedAmount;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getSpId() {
        return spId;
    }

    public void setSpId(String spId) {
        this.spId = spId;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public Double getChargingAmount() {
        return chargingAmount;
    }

    public void setChargingAmount(Double chargingAmount) {
        this.chargingAmount = chargingAmount;
    }

    public boolean isExpirableApp() {
        return expirableApp;
    }

    public void setExpirableApp(boolean expirableApp) {
        this.expirableApp = expirableApp;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getMandateId() {
        return mandateId;
    }

    public void setMandateId(String mandateId) {
        this.mandateId = mandateId;
    }

    public int getConsumedTrials() {
        return consumedTrials;
    }

    public void setConsumedTrials(int consumedTrials) {
        this.consumedTrials = consumedTrials;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getShortcode() {
        return shortcode;
    }

    public void setShortcode(String shortcode) {
        this.shortcode = shortcode;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getAppStatus() {
        return appStatus;
    }

    public void setAppStatus(String appStatus) {
        this.appStatus = appStatus;
    }

    public Date getLastChargedTime() {
        return lastChargedTime;
    }

    public void setLastChargedTime(Date lastChargedTime) {
        this.lastChargedTime = lastChargedTime;
    }

    public String getCoopUserName() {
        return coopUserName;
    }

    public void setCoopUserName(String coopUserName) {
        this.coopUserName = coopUserName;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public boolean isUseLegacyMask() {
        return useLegacyMask;
    }

    public void setUseLegacyMask(boolean useLegacyMask) {
        this.useLegacyMask = useLegacyMask;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Subscription{");
        sb.append("id=").append(id);
        sb.append(", status='").append(status).append('\'');
        sb.append(", lastChargedAmount='").append(lastChargedAmount).append('\'');
        sb.append(", frequency='").append(frequency).append('\'');
        sb.append(", correlationId='").append(correlationId).append('\'');
        sb.append(", currentStatus='").append(currentStatus).append('\'');
        sb.append(", appId='").append(appId).append('\'');
        sb.append(", spId='").append(spId).append('\'');
        sb.append(", endDate=").append(endDate);
        sb.append(", appName='").append(appName).append('\'');
        sb.append(", chargingAmount=").append(chargingAmount);
        sb.append(", expirableApp=").append(expirableApp);
        sb.append(", msisdn='").append(msisdn).append('\'');
        sb.append(", mandateId='").append(mandateId).append('\'');
        sb.append(", keyword='").append(keyword).append('\'');
        sb.append(", shortcode='").append(shortcode).append('\'');
        sb.append(", lastModifiedDate=").append(lastModifiedDate);
        sb.append(", appStatus='").append(appStatus).append('\'');
        sb.append(", lastChargedTime=").append(lastChargedTime);
        sb.append(", coopUserName='").append(coopUserName).append('\'');
        sb.append(", createdDate=").append(createdDate);
        sb.append(", operator='").append(operator).append('\'');
        sb.append(", version=").append(version);
        sb.append(", consumedTrials=").append(consumedTrials);
        sb.append(", useLegacyMask=").append(useLegacyMask);
        sb.append('}');
        return sb.toString();
    }
}
