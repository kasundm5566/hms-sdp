package hms.kite.subscription.core.service.repo;

import com.google.common.base.Optional;
import com.mongodb.DBObject;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

public interface SubscriptionRepoService {

    public void create (Subscription subscription);

    public void upsert (Subscription subscription);

    public Subscription findById(ObjectId id);

    public List<Subscription> query(Query query);

    public List<DBObject> queryDbObjects(Query query);

    public long count(Query query);

    public Optional<Subscription> findOne(Query query);
}
