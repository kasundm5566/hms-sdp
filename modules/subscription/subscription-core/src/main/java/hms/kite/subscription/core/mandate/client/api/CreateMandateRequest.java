package hms.kite.subscription.core.mandate.client.api;

import com.google.gson.annotations.SerializedName;

public class CreateMandateRequest{

    @SerializedName("headers") private Header header;
    @SerializedName("description") private String description;
    @SerializedName("end-date") private String endDate;
    @SerializedName("start-date") private String startDate;
    @SerializedName("start-criteria") private String startCriteria;
    @SerializedName("frequency") private String frequency;
    @SerializedName("charging") private ChargingInfo chargingInfo;
    @SerializedName("control-data") private ControlData controlData;
    @SerializedName("meta-data") private MetaData metaData;

    public CreateMandateRequest() {
    }

    public CreateMandateRequest(Header header,
                                String description,
                                String endDate,
                                String startDate,
                                String startCriteria,
                                String frequency,
                                ChargingInfo chargingInfo,
                                ControlData controlData,
                                MetaData metaData) {
        this.header = header;
        this.description = description;
        this.endDate = endDate;
        this.startDate = startDate;
        this.startCriteria = startCriteria;
        this.frequency = frequency;
        this.chargingInfo = chargingInfo;
        this.controlData = controlData;
        this.metaData = metaData;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartCriteria() {
        return startCriteria;
    }

    public void setStartCriteria(String startCriteria) {
        this.startCriteria = startCriteria;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public ChargingInfo getChargingInfo() {
        return chargingInfo;
    }

    public void setChargingInfo(ChargingInfo chargingInfo) {
        this.chargingInfo = chargingInfo;
    }

    public ControlData getControlData() {
        return controlData;
    }

    public void setControlData(ControlData controlData) {
        this.controlData = controlData;
    }

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }

    public static class MetaData {
    }

    public static class Header {

        @SerializedName("system-id") private String systemId;
        @SerializedName("correlation-id") private String correlationId;

        public Header(String systemId, String correlationId) {
            this.systemId = systemId;
            this.correlationId = correlationId;
        }

        public String getSystemId() {
            return systemId;
        }

        public void setSystemId(String systemId) {
            this.systemId = systemId;
        }

        public String getCorrelationId() {
            return correlationId;
        }

        public void setCorrelationId(String correlationId) {
            this.correlationId = correlationId;
        }

        @Override
        public String toString() {
            return "Header{" +
                    "systemId='" + systemId + '\'' +
                    ", correlationId='" + correlationId + '\'' +
                    '}';
        }
    }

    public static class ChargingInfo {
        @SerializedName("model") private String model = "prorate";
        @SerializedName("type") private String type = "subscription";
        @SerializedName("from-account-id") private String fromAccountId;
        @SerializedName("msisdn") private String msisdn;
        @SerializedName("merchant-id") private String merchantId;
        @SerializedName("merchant-name") private String merchantName;
        @SerializedName("category") private String category = "subscription";
        @SerializedName("from-payment-instrument") private String fromPaymentInstrument;
        @SerializedName("amount") private Amount amount;

        public ChargingInfo(String model,
                            String type,
                            String fromAccountId,
                            String msisdn,
                            String merchantId,
                            String merchantName,
                            String category,
                            String fromPaymentInstrument,
                            Amount amount) {
            this.model = model;
            this.type = type;
            this.fromAccountId = fromAccountId;
            this.msisdn = msisdn;
            this.merchantId = merchantId;
            this.merchantName = merchantName;
            this.category = category;
            this.fromPaymentInstrument = fromPaymentInstrument;
            this.amount = amount;
        }

        public static class Amount {
            @SerializedName("value") private String value;
            @SerializedName("currency-code") private String currencyCode;

            public Amount(String value, String currencyCode) {
                this.value = value;
                this.currencyCode = currencyCode;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getCurrencyCode() {
                return currencyCode;
            }

            public void setCurrencyCode(String currencyCode) {
                this.currencyCode = currencyCode;
            }

            @Override
            public String toString() {
                return "Amount{" +
                        "value=" + value +
                        ", currencyCode='" + currencyCode + '\'' +
                        '}';
            }
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getFromAccountId() {
            return fromAccountId;
        }

        public void setFromAccountId(String fromAccountId) {
            this.fromAccountId = fromAccountId;
        }

        public String getMsisdn() {
            return msisdn;
        }

        public void setMsisdn(String msisdn) {
            this.msisdn = msisdn;
        }

        public String getMerchantId() {
            return merchantId;
        }

        public void setMerchantId(String merchantId) {
            this.merchantId = merchantId;
        }

        public String getMerchantName() {
            return merchantName;
        }

        public void setMerchantName(String merchantName) {
            this.merchantName = merchantName;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getFromPaymentInstrument() {
            return fromPaymentInstrument;
        }

        public void setFromPaymentInstrument(String fromPaymentInstrument) {
            this.fromPaymentInstrument = fromPaymentInstrument;
        }

        public Amount getAmount() {
            return amount;
        }

        public void setAmount(Amount amount) {
            this.amount = amount;
        }

        @Override
        public String toString() {
            return "ChargingInfo{" +
                    "model='" + model + '\'' +
                    ", type='" + type + '\'' +
                    ", fromAccountId='" + fromAccountId + '\'' +
                    ", msisdn='" + msisdn + '\'' +
                    ", merchantId='" + merchantId + '\'' +
                    ", merchantName='" + merchantName + '\'' +
                    ", category='" + category + '\'' +
                    ", fromPaymentInstrument='" + fromPaymentInstrument + '\'' +
                    ", amount=" + amount +
                    '}';
        }
    }

    public static class ControlData {

        @SerializedName("app") private App app;
        @SerializedName("sp") private Sp sp;

        public ControlData(App app, Sp sp) {
            this.app = app;
            this.sp = sp;
        }

        public App getApp() {
            return app;
        }

        public void setApp(App app) {
            this.app = app;
        }

        public Sp getSp() {
            return sp;
        }

        public void setSp(Sp sp) {
            this.sp = sp;
        }

        public static class App {
            @SerializedName("id") private String id;
            @SerializedName("state") private String state = "enable";

            public App(String id, String state) {
                this.id = id;
                this.state = state;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            @Override
            public String toString() {
                return "App{" +
                        "id='" + id + '\'' +
                        ", state='" + state + '\'' +
                        '}';
            }
        }

        public static class Sp {
            @SerializedName("id") private String id;
            @SerializedName("state") private String state = "enable";

            public Sp(String id, String state) {
                this.id = id;
                this.state = state;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            @Override
            public String toString() {
                return "Sp{" +
                        "id='" + id + '\'' +
                        ", state='" + state + '\'' +
                        '}';
            }
        }

        @Override
        public String toString() {
            return "ControlData{" +
                    "app=" + app +
                    ", sp=" + sp +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "CreateMandateRequest{" +
                "header=" + header +
                ", description='" + description + '\'' +
                ", endDate='" + endDate + '\'' +
                ", startDate='" + startDate + '\'' +
                ", startCriteria='" + startCriteria + '\'' +
                ", frequency='" + frequency + '\'' +
                ", chargingInfo=" + chargingInfo +
                ", controlData=" + controlData +
                ", metaData=" + metaData +
                '}';
    }
}