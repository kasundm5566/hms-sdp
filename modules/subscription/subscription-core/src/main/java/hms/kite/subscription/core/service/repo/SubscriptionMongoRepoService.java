package hms.kite.subscription.core.service.repo;

import com.google.common.base.Optional;
import com.mongodb.DBObject;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

public class SubscriptionMongoRepoService implements SubscriptionRepoService {

    private MongoTemplate subscriptionRepoTemplate;

    @Override
    public void create(Subscription subscription) {
        subscriptionRepoTemplate.insert(subscription);
    }

    @Override
    public void upsert(Subscription subscription) {
        subscriptionRepoTemplate.save(subscription);
    }

    @Override
    public Subscription findById(ObjectId id) {
        final Subscription subscription = subscriptionRepoTemplate.findById(id, Subscription.class);
        return subscription;
    }

    @Override
    public List<Subscription> query(Query query) {
        final List<Subscription> subscriptions = subscriptionRepoTemplate.find(query, Subscription.class);
        return subscriptions;
    }

    @Override
    public List<DBObject> queryDbObjects(Query query) {
        final List<DBObject> subscriptions = subscriptionRepoTemplate.find(query, DBObject.class, "subscription");
        return subscriptions;
    }

    @Override
    public long count(Query query) {
        return subscriptionRepoTemplate.count(query, Subscription.class);
    }

    @Override
    public Optional<Subscription> findOne(Query query) {
        final Subscription subscriptions = subscriptionRepoTemplate.findOne(query, Subscription.class);
        return Optional.fromNullable(subscriptions);
    }


    public void setSubscriptionRepoTemplate(MongoTemplate subscriptionRepoTemplate) {
        this.subscriptionRepoTemplate = subscriptionRepoTemplate;
    }
}
