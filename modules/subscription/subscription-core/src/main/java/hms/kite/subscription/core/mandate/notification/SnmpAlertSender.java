package hms.kite.subscription.core.mandate.notification;

import com.google.common.collect.ImmutableMap;
import hms.commons.SnmpLogUtil;
import org.springframework.beans.factory.annotation.Value;

import static hms.kite.subscription.core.mandate.notification.MandateEventListener.MandateEvent.*;
import static hms.kite.subscription.core.mandate.notification.SnmpAlertSender.SnmpAlert.*;

public class SnmpAlertSender implements MandateEventListener {


    private @Value("${sdp.to.mandate.connection.success.snmp.message}") String connectionToMandateSuccessSnmpMsg;
    private @Value("${sdp.to.mandate.connection.failure.snmp.message}") String connectionToMandateFailedSnmpMsg;
    private @Value("${sdp.to.mandate.connection.trap.delay.in.seconds}") int snmpTimeTrapDelay;

    private static final ImmutableMap<MandateEvent, SnmpAlert> mandateEventSnmpAlertMapping;

    static {
        mandateEventSnmpAlertMapping = ImmutableMap.of(CancelMandateRespFailed, ConnectionToMandateFail,
                                                        CreateMandateRespFailed, ConnectionToMandateFail,
                                                        CancelMandateRespReceived, ConnectionToMandateSuccess,
                                                        CreateMandateRespReceived, ConnectionToMandateSuccess);
    }


    public static enum SnmpAlert {
        ConnectionToMandateSuccess,
        ConnectionToMandateFail;
    }

    @Override
    public void handle(MandateEvent mandateEvent) {
        if(ConnectionToMandateFail.equals(mandateEventSnmpAlertMapping.get(mandateEvent))) {
            SnmpLogUtil.timeTrap("sdpToMandateConnection", snmpTimeTrapDelay, connectionToMandateFailedSnmpMsg);
        } else if(ConnectionToMandateSuccess.equals(mandateEventSnmpAlertMapping.get(mandateEvent))) {
            SnmpLogUtil.clearTrap("sdpToMandateConnection", connectionToMandateSuccessSnmpMsg);
        } else {
            // do nothing
        }
    }




}
