package hms.kite.subscription.core.mandate.client.api;

import com.google.gson.annotations.SerializedName;

public class CreateMandateResponse {

    @SerializedName("mandate-id") private String mandateId;
    @SerializedName("status-code") private String statusCode;
    @SerializedName("status-description") private String statusDescription;

    public CreateMandateResponse(String mandateId, String statusCode, String statusDescription) {
        this.mandateId = mandateId;
        this.statusCode = statusCode;
        this.statusDescription = statusDescription;
    }

    public String getMandateId() {
        return mandateId;
    }

    public void setMandateId(String mandateId) {
        this.mandateId = mandateId;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    @Override
    public String toString() {
        return "CreateMandateResponse{" +
                "mandateId='" + mandateId + '\'' +
                ", statusCode='" + statusCode + '\'' +
                ", statusDescription='" + statusDescription + '\'' +
                '}';
    }
}
