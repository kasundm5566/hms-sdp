package hms.kite.subscription.core.mandate.notification;

public interface MandateEventListener {

    public static enum MandateEvent {
        CreateMandateRespReceived,
        CreateMandateRespFailed,
        CancelMandateRespReceived,
        CancelMandateRespFailed,
    }

    public void handle(MandateEvent mandateEvent);

}
