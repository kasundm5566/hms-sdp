package hms.kite.subscription.core.mandate.client;

import com.google.common.base.Optional;
import hms.kite.subscription.core.mandate.client.api.CreateMandateRequest;
import hms.kite.util.GsonUtil;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import static hms.kite.subscription.core.mandate.client.api.CreateMandateRequest.*;

public class MandateRequestUtil {

    private static String createMandateRequestTemplate;

    private static final DateTimeFormatter mandateDateFormat = DateTimeFormat.forPattern("YYYY-MM-dd'T'HH:mm:ss.SSSZZ");

    static {
        try {
            final BufferedReader bufferedReader = new BufferedReader(
                                                    new InputStreamReader(
                                                        MandateRequestUtil.class.getClassLoader().getResourceAsStream("mandate_request.json"), StandardCharsets.UTF_8));

            String nextLine = "";
            StringBuffer stringBuffer = new StringBuffer(nextLine);
            while(Optional.fromNullable((nextLine = bufferedReader.readLine())).isPresent()) {
                stringBuffer.append(nextLine);
            }

            createMandateRequestTemplate = stringBuffer.toString();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static class CreateMandateRequestBuilder {

        final CreateMandateRequest createMandateRequest;

        public CreateMandateRequestBuilder() {
            createMandateRequest = GsonUtil.getGson().fromJson(createMandateRequestTemplate, CreateMandateRequest.class);
        }

        public CreateMandateRequestBuilder withAmount(Double amount) {
            createMandateRequest.getChargingInfo().getAmount().setValue(String.valueOf(amount));
            return this;
        }

        public CreateMandateRequestBuilder withCorrelationId(String correlationId) {
            createMandateRequest.getHeader().setCorrelationId(correlationId);
            return this;
        }

        public CreateMandateRequestBuilder withStartDate(DateTime startDate) {
            createMandateRequest.setStartDate(startDate.toString(mandateDateFormat));
            return this;
        }

        public CreateMandateRequestBuilder withEndDate(DateTime endDate) {
            final Optional<String> endDateAsStr = Optional.fromNullable(endDate).isPresent() ? Optional.of(endDate.toString(mandateDateFormat)) : Optional.<String>absent();
            if(endDateAsStr.isPresent()) {
                createMandateRequest.setEndDate(endDateAsStr.get());
            }
            return this;
        }

        public CreateMandateRequestBuilder withStartCriteria(String startCriteria) {
            createMandateRequest.setStartCriteria(startCriteria);
            return this;
        }

        public CreateMandateRequestBuilder withFrequency(String frequency) {
            createMandateRequest.setFrequency(frequency);
            return this;
        }

        public CreateMandateRequestBuilder withFromAccountId(String accountId) {
            createMandateRequest.getChargingInfo().setFromAccountId(accountId);
            return this;
        }

        public CreateMandateRequestBuilder withMsisdn(String msisdn) {
            createMandateRequest.getChargingInfo().setMsisdn(msisdn);
            return this;
        }

        public CreateMandateRequestBuilder withMerchantId(String merchantId) {
            createMandateRequest.getChargingInfo().setMerchantId(merchantId);
            return this;
        }

        public CreateMandateRequestBuilder withMerchantName(String merchantName) {
            createMandateRequest.getChargingInfo().setMerchantName(merchantName);
            return this;
        }

        public CreateMandateRequestBuilder withAppId(String appId) {
            createMandateRequest.getControlData().getApp().setId(appId);
            return this;
        }

        public CreateMandateRequestBuilder withSpId(String spId) {
            createMandateRequest.getControlData().getSp().setId(spId);
            return this;
        }

        public CreateMandateRequest build() {
            // hack for time being
            createMandateRequest.setMetaData(new MetaData());
            return createMandateRequest;
        }
    }

    public static enum StartCriteria {
        NOW("now");

        private final String id;

        StartCriteria(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }
    }

    public static enum ChargingFrequency {
        DAILY("daily"),
        MONTHLY("monthly");

        private String id;

        ChargingFrequency(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }
    }

    public static class InvalidMandateRequestCreationException extends Exception {

        public InvalidMandateRequestCreationException(String key) {
            super(String.format("Invalid mandate request creation attempt, invalid key found %s", key));
        }
    }

}