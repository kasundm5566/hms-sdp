package hms.kite.subscription.core.mandate.notification;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import hms.kite.subscription.core.SubscriptionService;
import hms.kite.subscription.core.domain.SubscriptionResponse;
import hms.kite.subscription.core.domain.SubscriptionStatus;
import hms.kite.subscription.core.service.repo.Subscription;
import hms.kite.subscription.core.service.repo.SubscriptionRepoService;
import hms.kite.subscription.core.mandate.notification.NotifyResponse.ScheduleNotificationResp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.collect.ImmutableMap.Builder;
import static hms.kite.subscription.core.service.request.RegistrationServiceImpl.RegistrationStateMachine.possibleTransition;
import static hms.kite.util.GsonUtil.*;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.subscription.core.mandate.notification.NotifyResponse.*;
import static org.springframework.data.mongodb.core.query.Criteria.*;
import static org.springframework.data.mongodb.core.query.Query.*;

@Produces({"application/json"})
@Consumes({"application/json"})
@Path("/")
public class MandateNotificationEndPoint {


    private static final Logger logger = LoggerFactory.getLogger(MandateNotificationEndPoint.class);

    private static final ImmutableMap<SubscriptionStatus, ImmutableMap<SubscriptionStatus, SubscriptionResponse>> mandateSuccessNotificationTransitions;
    private static final ImmutableMap<String, SubscriptionResponse> mandateFailureNotificationMapping;
    private static final List<SubscriptionResponse> successfulSubscriptionResponses =
             Lists.newArrayList(SubscriptionResponse.SUCCESS, SubscriptionResponse.RECURSIVE_SUCCESS);

    static {
        mandateSuccessNotificationTransitions =
                ImmutableMap.of(SubscriptionStatus.REG_PENDING, ImmutableMap.of(SubscriptionStatus.REGISTERED, SubscriptionResponse.SUCCESS),
                                SubscriptionStatus.TEMPORARY_BLOCKED, ImmutableMap.of(SubscriptionStatus.REGISTERED, SubscriptionResponse.RECURSIVE_SUCCESS),
                                SubscriptionStatus.REGISTERED, ImmutableMap.of(SubscriptionStatus.REGISTERED, SubscriptionResponse.RECURSIVE_SUCCESS),
                                SubscriptionStatus.TRIAL, ImmutableMap.of(SubscriptionStatus.REGISTERED, SubscriptionResponse.RECURSIVE_SUCCESS));

        mandateFailureNotificationMapping = ImmutableMap.<String, SubscriptionResponse>builder().
                                                                        put("P9000", SubscriptionResponse.INTERNAL_ERROR).
                                                                        put("E9000", SubscriptionResponse.INTERNAL_ERROR).
                                                                        put("E9001", SubscriptionResponse.INSUFFICIENT_BALANCE).
                                                                        put("E9002", SubscriptionResponse.PGW_CONNECTOR_ERROR).
                                                                        put("E9003", SubscriptionResponse.INTERNAL_ERROR).
                                                                        put("E9004", SubscriptionResponse.INTERNAL_ERROR).
                                                                        put("E9101", SubscriptionResponse.INTERNAL_ERROR).
                                                                        put("E9102", SubscriptionResponse.INTERNAL_ERROR).
                                                                        put("E9103", SubscriptionResponse.INTERNAL_ERROR).
                                                                        put("E9104", SubscriptionResponse.INTERNAL_ERROR).
                                                                        put("E9105", SubscriptionResponse.INTERNAL_ERROR).
                                                                        build();
    }



    @Autowired
    private SubscriptionRepoService subscriptionRepoService;

    @Autowired
    private SubscriptionService subscriptionService;

    @Value("${subscription.currency.code}")
    private String currencyCode;

    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    @Path("/mandate-notify")
    public String mandateExecutionNotification(String str) {
        final NotifyRequest notification = getGson().fromJson(str, NotifyRequest.class);

        logger.debug("Mandate notification request received [{}].", notification);

        final Optional<Subscription> subscriptionOptional = subscriptionRepoService.findOne(query(where("mandate-id").is(notification.getMandateId())));
        ScheduleNotificationResp response = getErrorResponse();
        if(!subscriptionOptional.isPresent()) {
            // TODO : Check this
        } else {
            final Subscription subscription = subscriptionOptional.get();

            final SubscriptionResponse subscriptionResponse = updateSubscription(notification, subscription);

            final HashMap notificationContext = createNotificationContext(notification, subscription, subscriptionResponse);

            if(successfulSubscriptionResponses.contains(subscriptionResponse)) {
                subscriptionService.getNotificationHandler().sendNotification(notificationContext);
            }
            response = getSuccessResponse();
        }

        return getGson().toJson(response);
    }

    private SubscriptionResponse updateSubscription(NotifyRequest notification, Subscription subscription) {

        final SubscriptionStatus curStatus = SubscriptionStatus.valueOf(subscription.getCurrentStatus());
        final Date currentDate = new Date();
        final String mandateStatusCode = notification.getStatusCode();

        if(mandateStatusCode.equals("S1000")) {
            final SubscriptionStatus nextStatus = SubscriptionStatus.REGISTERED;

            if(possibleTransition(curStatus, nextStatus)) {
                logger.info("Transitioning the msisdn [{}]'s registration-status from [{}] to [{}] for application = [{}].",
                        new Object[]{subscription.getMsisdn(), curStatus, nextStatus, subscription.getAppId()});

                subscription.setCurrentStatus(nextStatus.name());
                subscription.setLastModifiedDate(currentDate);
                subscription.setLastChargedAmount(notification.getChargedAmount());
                subscription.setLastChargedTime(currentDate);

                subscriptionRepoService.upsert(subscription);

                return subscriptionResponse(curStatus, nextStatus);
            }

            return SubscriptionResponse.INTERNAL_ERROR;

        } else {

            SubscriptionStatus nextStatus = SubscriptionStatus.TEMPORARY_BLOCKED;
            if (curStatus.equals(SubscriptionStatus.REG_PENDING)) {
                nextStatus = SubscriptionStatus.REG_PENDING;
            }

            if (possibleTransition(curStatus, nextStatus)) {
                logger.info("Transitioning the msisdn [{}]'s registration-status from [{}] to [{}] for application = [{}].",
                        new Object[]{subscription.getMsisdn(), curStatus, nextStatus, subscription.getAppId()});

                subscription.setCurrentStatus(nextStatus.name());
                subscription.setLastModifiedDate(currentDate);

                subscriptionRepoService.upsert(subscription);

                if(mandateFailureNotificationMapping.containsKey(mandateStatusCode)) {
                    return mandateFailureNotificationMapping.get(mandateStatusCode);
                }
            }
            return SubscriptionResponse.INTERNAL_ERROR;
        }
    }

    public static SubscriptionResponse subscriptionResponse(SubscriptionStatus curStatus, SubscriptionStatus nextStatus) {
        if(mandateSuccessNotificationTransitions.containsKey(curStatus) && mandateSuccessNotificationTransitions.get(curStatus).containsKey(nextStatus)) {
            return mandateSuccessNotificationTransitions.get(curStatus).get(nextStatus);
        } else {
            return SubscriptionResponse.INTERNAL_ERROR;
        }
    }


    private HashMap createNotificationContext(NotifyRequest notification, Subscription subscription, SubscriptionResponse subscriptionResponse) {

        Map<String, String> recipientsMap = new HashMap();
        recipientsMap.put(recipientAddressStatusK, subscription.getStatus());
        recipientsMap.put(recipientAddressK, subscription.getMsisdn());

        return Maps.newHashMap(new Builder().
                put(operatorK, subscription.getOperator()).
                put(recipientsK, Lists.newArrayList(recipientsMap)).
                put(correlationIdK, subscription.getCorrelationId()).
                put(appIdK, subscription.getAppId()).
                put(spIdK, subscription.getSpId()).
                put(directionK, aoK).
                put(subscriptionRespK, subscriptionResponse).
                put(ncsTypeK, subscriptionK).
                put(subscriptionRequestTypeK, subscriptionRecursiveChargingK).
                put(senderAddressK, subscription.getShortcode()).
                put(chargedAmountK, notification.getChargedAmount()).
                put(currencyCodeK, currencyCode).
                put(notificationStatusCodeK, notification.getStatusCode()).
                put(pgwStatusCodeK, notification.getPgwStatusCode()).
                put(chargingTypeK, notification.getChargingType()).
                put(keywordK, subscription.getKeyword()).
                put(shortcodeK, subscription.getShortcode()).
                put(msisdnK, subscription.getMsisdn()).
                put(executionCountK, notification.getExecutionCount()).
                build());
    }

    public void setSubscriptionService(SubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    public void setSubscriptionRepoService(SubscriptionRepoService subscriptionRepoService) {
        this.subscriptionRepoService = subscriptionRepoService;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
}
