package hms.kite.subscription.core.domain;

import com.google.common.collect.ImmutableMap;

public enum SubscriptionNblResponse {
    REGISTERED      ("REGISTERED"),
    UNREGISTERED    ("UNREGISTERED"),
    PENDING_CHARGE  ("PENDING CHARGE");

    private String nblResponseValue;
    private static final ImmutableMap<SubscriptionStatus, SubscriptionNblResponse> subscriptionStatusNblResponseMap;
    private static final ImmutableMap<String, SubscriptionNblResponse> subscriptionNblResponseValueReverseMap;

    static {
        subscriptionStatusNblResponseMap = ImmutableMap.<SubscriptionStatus, SubscriptionNblResponse>builder().
                put(SubscriptionStatus.INITIAL,          UNREGISTERED).
                put(SubscriptionStatus.REG_PENDING,      PENDING_CHARGE).
                put(SubscriptionStatus.REGISTERED,       REGISTERED).
                put(SubscriptionStatus.TRIAL,            REGISTERED).
                put(SubscriptionStatus.UNREGISTERED,     UNREGISTERED).
                put(SubscriptionStatus.TEMPORARY_BLOCKED,PENDING_CHARGE).
                put(SubscriptionStatus.BLOCKED,          UNREGISTERED).
                build();

        subscriptionNblResponseValueReverseMap = ImmutableMap.<String, SubscriptionNblResponse>builder().
                put("REGISTERED", REGISTERED).
                put("UNREGISTERED", UNREGISTERED).
                put("PENDING CHARGE", PENDING_CHARGE).
                build();
    }

    SubscriptionNblResponse(String nblResponseValue) {
        this.nblResponseValue = nblResponseValue;
    }

    public String getNblResponseValue() {
        return nblResponseValue;
    }

    public static SubscriptionNblResponse mapToSubscriptionStatusNblResponse(SubscriptionStatus status) {
        return subscriptionStatusNblResponseMap.get(status);
    }

    public static SubscriptionNblResponse subscriptionStatusValueToSubscriptionNblResponse(String status) {
        return subscriptionNblResponseValueReverseMap.get(status);
    }
}