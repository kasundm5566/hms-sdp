package hms.kite.subscription.core.service.request;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import hms.kite.subscription.core.domain.SubscriptionStatus;
import hms.kite.subscription.core.service.repo.Subscription;
import hms.kite.util.KiteKeyBox;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static hms.kite.subscription.core.service.repo.Subscription.*;
import static hms.kite.util.KiteKeyBox.*;

public class RequestContextParser {

    private static final Map<String, TrialsTimeUnit> timeUnitIdMap;
    private static final Pattern pattern;

    static {
        String subscriberIdRegEx = "^" + telK + ":(?<msisdn>[\\d]+)" + "$";
        pattern = Pattern.compile(subscriberIdRegEx);
    }


    static {
        timeUnitIdMap = ImmutableMap.of("days", TrialsTimeUnit.DAY,
                                        "hours", TrialsTimeUnit.HOUR);
    }

    public static Subscription subscription(Map<String, Map<String, Object>> context) throws InvalidRequestContextException {
        final Date now = new Date();
        final SubscriptionBuilder builder = new SubscriptionBuilder();

        builder.withCurrentStatus(SubscriptionStatus.INITIAL.name());

        final Optional<Object> status = getFieldIfPresent("request.status-code", context);
        if (status.isPresent()) builder.withStatus((String) status.get());

        final Optional<Object> correlationId = getFieldIfPresent("request.correlation-id", context);
        if (correlationId.isPresent()) builder.withCorrelationId((String) correlationId.get());

        final Optional<Object> appId = getFieldIfPresent("app.app-id", context);
        if (appId.isPresent()) builder.withAppId((String) appId.get());

        final Optional<Object> spId = getFieldIfPresent("sp.sp-id", context);
        if (spId.isPresent()) builder.withSpId((String) spId.get());

        final Optional<Object> endDate = getFieldIfPresent("app.end-date", context);
        if (endDate.isPresent()) builder.withEndDate((Date) endDate.get());

        final Optional<Object> appName = getFieldIfPresent("app.name", context);
        if (appName.isPresent()) builder.withAppName((String) appName.get());

        final Optional<Object> chargingAmount = getFieldIfPresent("ncs.charging.amount", context);
        if (chargingAmount.isPresent()) builder.withChargingAmount(Double.valueOf((String)chargingAmount.get()));

        final Optional<Object> expirableApp = getFieldIfPresent("app.expire", context);
        if (expirableApp.isPresent()) builder.withExpirableApp((Boolean) expirableApp.get());

        // add Subscriber-id/Msisdn
        final String msisdn = extractMsisdn(context);
        builder.withMsisdn(msisdn);

        /*final Optional<Object> mandateId = getFieldIfPresent("", context);*/

        final Optional<Object> keyword = getFieldIfPresent("request.keyword", context);
        if (keyword.isPresent()) builder.withKeyword((String) keyword.get());

        final Optional<Object> shortCode = getFieldIfPresent("request.shortcode", context);
        if (shortCode.isPresent()) builder.withShortcode((String) shortCode.get());

        /*final Optional<Object> lastModifiedDate = getFieldIfPresent("", context);*/

        final Optional<Object> appStatus = getFieldIfPresent("app.status", context);
        if (appStatus.isPresent()) builder.withAppStatus((String) appStatus.get());

        /*final Optional<Object> lastChargedTime = getFieldIfPresent("", context);*/

        final Optional<Object> coopUserName = getFieldIfPresent("sp.coop-user-name", context);
        if (coopUserName.isPresent()) builder.withCoopUserName((String) coopUserName.get());

        builder.withCreatedDate(now);

        final Optional<Object> operator = getFieldIfPresent("request.operator", context);
        if (operator.isPresent()) builder.withOperator((String) operator.get());

        final Optional<Object> frequency = getFieldIfPresent("ncs.charging.frequency", context);
        if(frequency.isPresent()) builder.withFrequency((String) frequency.get());

        // Initially with dummy mandate id
        builder.withMandateId(Subscription.DUMMY_MANDATE_ID);

        builder.withConsumedTrials(0);

        builder.withLastModifiedDate(new Date());

        return builder.build();
    }

    private static String extractMsisdn(Map<String, Map<String, Object>> context) throws InvalidRequestContextException {
        Optional<Object> msisdn = getFieldIfPresent("request.sender-address", context);
        //Optional<Object> msisdnThroughHttp = getFieldIfPresent("request.subscriberID", context);
        String recipientAddress = getRecipientAddress(context);
        if(msisdn.isPresent()) {
            return (String) msisdn.get();
        } else if (recipientAddress != null) {
             return recipientAddress;
        } else {
            throw new InvalidRequestContextException("Subscriber-Id/Msisdn not found in the request context.");
        }
         /*else if(msisdnThroughHttp.isPresent()) {
            Optional<String> msisdnOpt = parseMsisdnThroughHttpSubscription((String) msisdnThroughHttp.get());
            if(!msisdnOpt.isPresent()) {
                throw new InvalidRequestContextException("Subscriber-Id/Msisdn not found in the request context.");
            } else {
                return msisdnOpt.get();
            }
        }*/
    }

    public static String getRecipientAddress(Map<String, Map<String, Object>> context) {
        String recipientAddress = null;
        List<Map<String, String>> recipients = (List<Map<String, String>>) context.get(requestK).get(recipientsK);
        if (recipients != null) {
            Map<String, String> recipient = recipients.get(0);
            if (recipient != null) {
                recipientAddress = recipient.get(recipientAddressK);
            }
        }
        return recipientAddress;
    }

    public static Optional<String> parseMsisdnThroughHttpSubscription(String msisdnThroughHttp) {
            final Matcher matcher = pattern.matcher(msisdnThroughHttp);
            if(!matcher.matches()) {
                return Optional.absent();
            } else {
                try {
                    String msisdn = matcher.group("msisdn");
                    return Optional.of(msisdn);
                } catch (Exception e) {
                    return Optional.absent();
                }
            }
    }


    public static boolean isFreeApp(Map<String, Map<String, Object>> context) throws InvalidRequestContextException {
        final Optional<Object> chargingType = getFieldIfPresent("ncs.charging.type", context);
        if(!chargingType.isPresent()) {
            throw new InvalidRequestContextException(
                    String.format("Charging type for subscription ncs is a mandatory field. app-id %s", getFieldIfPresent("app.app-id", context).or("_")));
        }
        return (chargingType.get()).equals("free");
    }

    public static Optional<Trial> getTrialPeriod(Map<String, Map<String, Object>> context) {
        final Optional<Object> unit = getFieldIfPresent("ncs.charging.trials.period.unit", context);
        final Optional<Object> value = getFieldIfPresent("ncs.charging.trials.period.value", context);
        final Optional<Object> maxCount = getFieldIfPresent("ncs.charging.trials.max-count", context);

        if(unit.isPresent() && value.isPresent() && maxCount.isPresent()) {
            return generateTrail((String) unit.get(), (Integer) value.get(), (Integer) maxCount.get() );
        }

        return Optional.absent();
    }

    private static Optional<Object> getFieldIfPresent(String fieldKey, Map map) {
        final String[] keys = fieldKey.split("\\.");
        final Stack<String> keyStack = new Stack<String>();
        keyStack.addAll(Lists.reverse(Arrays.asList(keys)));
        return traverse(keyStack, map);
    }

    private static Optional<Object> traverse(Stack<String> keyStack, Map map) {

        if (keyStack.isEmpty()) {
            return Optional.absent();
        }

        final Object value = map.get(keyStack.pop());

        if (keyStack.empty()) {
            return Optional.fromNullable(value);
        } else if (value instanceof Map) {
            return traverse(keyStack, (Map) value);
        } else return Optional.absent();
    }

    public static Optional<Trial> generateTrail(String unit, int value, int maxCount) {
        final Optional<Trial> trial =
                timeUnitIdMap.containsKey(unit) ? Optional.of(new Trial(timeUnitIdMap.get(unit), value, maxCount)) : Optional.<Trial>absent();
        return trial;
    }

    public static class InvalidRequestContextException extends Exception {
        public InvalidRequestContextException(String message) {
            super(message);
        }
    }

    public static enum TrialsTimeUnit {
        DAY(TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS)),
        HOUR(TimeUnit.MILLISECONDS.convert(1, TimeUnit.HOURS));

        private final long duration;

        private TrialsTimeUnit(long duration) {
            this.duration = duration;
        }

        public long getDuration() {
            return duration;
        }
    }

    public static class Trial {

        private final TrialsTimeUnit trialTimeUnit;
        private final int value;
        private final int maxCount;

        public Trial(TrialsTimeUnit timeUnit, int value, int maxCount) {
            trialTimeUnit = timeUnit;
            this.value = value;
            this.maxCount = maxCount;
        }

        public TrialsTimeUnit getTimeUnit() {
            return trialTimeUnit;
        }

        public int getValue() {
            return value;
        }

        public int getMaxCount() {
            return maxCount;
        }

        public static long calculateTrailDuration(Trial trial) {
            return trial.getValue() * trial.getTimeUnit().getDuration();
        }

        @Override
        public String toString() {
            return "Trial{" +
                    "timeUnit=" + trialTimeUnit +
                    ", value=" + value +
                    ", maxCount=" + maxCount +
                    '}';
        }
    }

}

/*
Through SMPP

App
{updated-date=Wed May 22 09:21:00 IST 2013, govern=true, active-production-start-date=Wed May 22 09:21:00 IST 2013, advertise=false, user-type=CORPORATE, remarks=Application approved. , sp-id=SPP_000016, type=flat, password=e8a2b1aef246087f9930dcbf931d0d29, _id=APP_002693, description=Teaching tamil words and meanings via SMS. - Alert, name=Vanakkam, ncses=[ { "status" : "ncs-configured" , "ncs-type" : "subscription"} , { "status" : "ncs-configured" , "ncs-type" : "sms" , "operator" : "dialog"}], app-id=APP_002693, black-list=[ ], allowed-hosts=[ "127.0.0.1" , "192.168.30.10" , "192.168.30.12" , "10.62.230.231" , "10.62.230.232" , "10.62.230.233" , "10.62.230.234" , "192.168.30.1" , "192.168.30.2" , "192.168.30.3" , "192.168.30.4"], expire=false, mask-number=false, apply-tax=false, created-date=Tue May 21 17:14:05 IST 2013, status=active-production, created-by=evision, sms={_id=519b5e060cf2fafadf5c994b, app-id=APP_002693, created-by=evision, created-date=Tue May 21 17:14:06 IST 2013, mo={ "tpd" : "5000" , "charging" : { "type" : "free"} , "connection-url" : "http://core.sol:65382/smsmo" , "tps" : "5"}, mo-allowed=true, mt={ "aliasing" : [ "Vanakkam"] , "charging" : { "type" : "free"} , "default-sender-address" : "*Vanakkam*" , "tpd" : "5000" , "tps" : "5"}, mt-allowed=true, ncs-type=sms, operator=dialog, status=active-production, subscription-required=false, updated-by=groupbusiness, updated-date=Wed May 22 09:21:01 IST 2013, routing-keys=[{_id=519b5d540cf2fafadf5c9947, sp-id=SPP_000016, operator=dialog, shortcode=77000, keyword=learntamil, app-id=APP_002693, exclusive=false, ncs-type=sms, last-updated-time=Tue May 21 17:14:06 IST 2013}]}, app-request-date=Tue May 21 17:14:05 IST 2013, category=soltura, white-list=[ ], revenue-share=70, updated-by=groupbusiness}

Ncs
{_id=519b5e050cf2fafadf5c9949, allow-http-subscription-requests=true, app-id=APP_002693, charging={ "allowed-payment-instruments" : [ "Mobile Account"] , "amount" : "30" , "frequency" : "monthly" , "party" : "subscriber" , "type" : "flat"}, created-by=evision, created-date=Tue May 21 17:14:05 IST 2013, max-no-of-bc-msgs-per-day=100, ncs-type=subscription, operator=, status=active-production, subscription-response-message=Await updates, unsubscription-response-message=You will not receive updates, updated-by=groupbusiness, updated-date=Wed May 22 09:21:01 IST 2013}

Sp
{ussd-mt-tps=10, sms-mt=true, cas-mo-tps=10, remarks=Automatic Configuration, sp-id=SPP_000016, sp-request-date=03/08/12, coop-user-id=20120308144946717, ussd-mt-tpd=10000, sms-mo=true, soltura-user=true, max-no-of-subscribers=100000, ussd-selected=true, _id=SPP_000016, ussd-mo-tpd=10000, lbs-tpd=100, sms-mt-tps=30, sms-mo-tps=10, sdp-user=true, coop-user-name=evision, ussd-mo-tps=10, status=approved, cas-selected=true, sms-mo-tpd=30000, sms-mt-tpd=30000, created-by=evision, sp-selected-services=[ "sms" , "ussd" , "cas" , "subscription" , "downloadable" , "lbs"], lbs-tps=10, downloadable-mdpd=100, subscription-selected=true, lbs-selected=true, downloadable-selected=true, downloadable-mcd=10, sms-selected=true, max-no-of-bc-msgs-per-day=9999, cas-mo-tpd=5000}

Request
{sender-address-optype=dialog, sender-address-subtype=any, service-keyword=reg, operator-id=dialog, status=true, recipients=[{recipient-address=77000, recipient-address-optype=unknown, charging-trx-id=1384430228540, recipient-address-status=E1325, recipient-address-subtype=any}], direction=mo, keyword=learntamil, ncs-type=subscription, charging-details={subscription-full=30, subscription-frequency=monthly}, operator=dialog, message=reg learntamil, status-code=S1000, status-description=Success, sender-address-status=S1000, charging-trx-id=101311141727080001, action=1, encoding=0, receiveTime=1384430228538, shortcode=77000, app-id=APP_002693, correlation-id=071311141727080003, sender-address=94775089345}
 */

/*
Through HTTP

{app={updated-date=Wed May 22 09:21:00 IST 2013, govern=true, active-production-start-date=Wed May 22 09:21:00 IST 2013, advertise=false, user-type=CORPORATE, remarks=Application approved. , sp-id=SPP_000016, type=flat, password=e8a2b1aef246087f9930dcbf931d0d29, _id=APP_002693, description=Teaching tamil words and meanings via SMS. - Alert, name=Vanakkam, ncses=[ { "status" : "ncs-configured" , "ncs-type" : "subscription"} , { "status" : "ncs-configured" , "ncs-type" : "sms" , "operator" : "dialog"}], app-id=APP_002693, black-list=[ ], allowed-hosts=[ "127.0.0.1" , "192.168.30.10" , "192.168.30.12" , "10.62.230.231" , "10.62.230.232" , "10.62.230.233" , "10.62.230.234" , "192.168.30.1" , "192.168.30.2" , "192.168.30.3" , "192.168.30.4"], expire=false, mask-number=false, apply-tax=false, created-date=Tue May 21 17:14:05 IST 2013, status=active-production, created-by=evision, app-request-date=Tue May 21 17:14:05 IST 2013, category=soltura, white-list=[ ], revenue-share=70, updated-by=groupbusiness},
ncs={_id=519b5e050cf2fafadf5c9949, allow-http-subscription-requests=true, app-id=APP_002693, charging={ "allowed-payment-instruments" : [ "Mobile Account"] , "amount" : "30" , "frequency" : "monthly" , "party" : "subscriber" , "trials" : { "allowed" : true , "max-count" : 1 , "period" : { "unit" : "day" , "value" : 9}} , "type" : "flat"}, created-by=evision, created-date=Tue May 21 17:14:05 IST 2013, max-no-of-bc-msgs-per-day=100, ncs-type=subscription, operator=, status=active-production, subscription-response-message=Await updates, unsubscription-response-message=You will not receive updates, updated-by=groupbusiness, updated-date=Wed May 22 09:21:01 IST 2013},
sp={ussd-mt-tps=10, sms-mt=true, cas-mo-tps=10, remarks=Automatic Configuration, sp-id=SPP_000016, sp-request-date=03/08/12, coop-user-id=20120308144946717, ussd-mt-tpd=10000, sms-mo=true, soltura-user=true, max-no-of-subscribers=100000, ussd-selected=true, _id=SPP_000016, ussd-mo-tpd=10000, lbs-tpd=100, sms-mt-tps=30, sms-mo-tps=10, sdp-user=true, coop-user-name=evision, ussd-mo-tps=10, status=approved, cas-selected=true, sms-mo-tpd=30000, sms-mt-tpd=30000, created-by=evision, sp-selected-services=[ "sms" , "ussd" , "cas" , "subscription" , "downloadable" , "lbs"], lbs-tps=10, downloadable-mdpd=100, subscription-selected=true, lbs-selected=true, downloadable-selected=true, downloadable-mcd=10, sms-selected=true, max-no-of-bc-msgs-per-day=9999, cas-mo-tpd=5000},
request={sender-address-optype=unknown, sender-address-subtype=any, status=true, recipients=[{recipient-address=94776177781, recipient-address-optype=dialog, charging-trx-id=101311221853260002, recipient-address-status=S1000, recipient-address-subtype=any}], direction=ao, keyword=, applicationID=APP_002693, ncs-type=subscription, password=e8a2b1aef246087f9930dcbf931d0d29, charging-details={subscription-full=30, subscription-frequency=monthly}, operator=dialog, remote-host=127.0.0.1, status-code=S1000, status-description=Success, sender-address-status=E1325, action=1, receiveTime=1385126606472, shortcode=, subscriberID=tel:94776177781, app-id=APP_002693, correlation-id=101311221853260001, internal-host=true, subscription-request-type=reg}}
*/
