package hms.kite.subscription.core.mandate.notification;

/**
 * <p>
 *     Response message for acknowledge mandate.
 * </p>
 *
 * @author Manuja
 * @version $Id$
 */
public class NotifyResponse {

    private static ScheduleNotificationResp successResponse;
    private static ScheduleNotificationResp errorResponse;

    private NotifyResponse() {
    }

    public static ScheduleNotificationResp getSuccessResponse() {
        if (successResponse == null) {
            successResponse = new ScheduleNotificationResp("200", "SUCCESS");
        }

        return successResponse;
    }

    public static ScheduleNotificationResp getErrorResponse() {
        if (errorResponse == null) {
            errorResponse = new ScheduleNotificationResp("500", "ERROR");
        }

        return errorResponse;
    }

    /**
     * <p>
     *     Schedule notification response.
     * </p>
     *
     * @author  Manuja
     * @version  $Id$
     */
    public static class ScheduleNotificationResp {

        private String statusCode;
        private String statusDescription;

        public ScheduleNotificationResp(String statusCode, String statusDescription) {
            this.statusCode = statusCode;
            this.statusDescription = statusDescription;
        }

        public String getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(String statusCode) {
            this.statusCode = statusCode;
        }

        public String getStatusDescription() {
            return statusDescription;
        }

        public void setStatusDescription(String statusDescription) {
            this.statusDescription = statusDescription;
        }
    }
}
