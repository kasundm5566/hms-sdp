package hms.kite.subscription.core.service.repo;

import com.google.common.base.Optional;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.bson.types.ObjectId;
import org.springframework.core.convert.converter.Converter;

import java.util.Date;

import static hms.kite.subscription.core.service.repo.Subscription.SubscriptionBuilder;

public class SubscriptionConverter {

    public static class ReadConverter implements Converter<DBObject, Subscription> {

        @Override
        public Subscription convert(DBObject dbObject) {
            return new SubscriptionBuilder().
                    withStatus((String) dbObject.get("status")).
                    withId((ObjectId) dbObject.get("_id")).
                    withLastChargedAmount((String) dbObject.get("last-charged-amount")).
                    withFrequency((String) dbObject.get("frequency")).
                    withCorrelationId((String) dbObject.get("correlation-id")).
                    withCurrentStatus((String) dbObject.get("current-status")).
                    withAppId((String) dbObject.get("app-id")).
                    withSpId((String) dbObject.get("sp-id")).
                    withEndDate((Date) dbObject.get("end-date")).
                    withAppName((String) dbObject.get("app-name")).
                    withChargingAmount((Double) dbObject.get("charging-amount")).
                    withExpirableApp((Boolean) dbObject.get("expirable-app")).
                    withMsisdn((String) dbObject.get("msisdn")).
                    withMandateId((String) dbObject.get("mandate-id")).
                    withKeyword((String) dbObject.get("keyword")).
                    withShortcode((String) dbObject.get("shortcode")).
                    withLastModifiedDate((Date) dbObject.get("last-modified-date")).
                    withAppStatus((String) dbObject.get("app-status")).
                    withLastChargedTime((Date) dbObject.get("last-charged-time")).
                    withCoopUserName((String) dbObject.get("coop-user-name")).
                    withCreatedDate((Date) dbObject.get("created-date")).
                    withOperator((String) dbObject.get("operator")).
                    withConsumedTrials(Optional.fromNullable(dbObject.get("consumed-trials-count")).isPresent() ? (Integer) dbObject.get("consumed-trials-count") : 0).
                    withUseLegacyMask(Optional.fromNullable(dbObject.get("use-legacy-mask")).isPresent() ? (Boolean) dbObject.get("use-legacy-mask") : false).
                    build();
        }
    }

    public static class WriteConverter implements Converter<Subscription, DBObject> {

        @Override
        public DBObject convert(Subscription subscription) {
            DBObject dbObject = new BasicDBObject();
            dbObject.put("_id", subscription.getId());
            dbObject.put("status", subscription.getStatus());
            dbObject.put("last-charged-amount", subscription.getLastChargedAmount());
            dbObject.put("frequency", subscription.getFrequency());
            dbObject.put("correlation-id", subscription.getCorrelationId());
            dbObject.put("current-status", subscription.getCurrentStatus());
            dbObject.put("app-id", subscription.getAppId());
            dbObject.put("sp-id", subscription.getSpId());
            dbObject.put("end-date", subscription.getEndDate());
            dbObject.put("app-name", subscription.getAppName());
            dbObject.put("charging-amount", subscription.getChargingAmount());
            dbObject.put("expirable-app", subscription.isExpirableApp());
            dbObject.put("msisdn", subscription.getMsisdn());
            dbObject.put("mandate-id", subscription.getMandateId());
            dbObject.put("keyword", subscription.getKeyword());
            dbObject.put("shortcode", subscription.getShortcode());
            dbObject.put("last-modified-date", subscription.getLastModifiedDate());
            dbObject.put("app-status", subscription.getAppStatus());
            dbObject.put("last-charged-time", subscription.getLastChargedTime());
            dbObject.put("coop-user-name", subscription.getCoopUserName());
            dbObject.put("created-date", subscription.getCreatedDate());
            dbObject.put("operator", subscription.getOperator());
            dbObject.put("consumed-trials-count", subscription.getConsumedTrials());
            dbObject.put("use-legacy-mask", subscription.isUseLegacyMask());

            return dbObject;
        }
    }


}
