package hms.kite.subscription.core.service.request;


import com.google.common.base.Optional;
import hms.kite.subscription.core.domain.SubscriptionResponse;
import hms.kite.subscription.core.domain.SubscriptionStatus;
import hms.kite.subscription.core.service.repo.Subscription;

import java.util.Map;

public interface RegistrationService {

    public SubscriptionResponse register(Map<String, Map<String, Object>> requestContext) throws RequestContextParser.InvalidRequestContextException;

    public SubscriptionResponse unRegister(Map<String, Map<String, Object>> requestContext) throws RequestContextParser.InvalidRequestContextException;

    public SubscriptionStatus subscriptionStatus(String msisdn, String appId);

    public Optional<Subscription> getSubscriber(String msisdn, String appId);

}
