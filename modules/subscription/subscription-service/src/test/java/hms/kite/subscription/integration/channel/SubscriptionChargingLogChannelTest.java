package hms.kite.subscription.integration.channel;

import com.google.gson.JsonParser;
import hms.kite.util.GsonUtil;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import static org.testng.Assert.assertEquals;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SubscriptionChargingLogChannelTest {

    private SubscriptionChargingLogChannel channel;

    private String messageContextJson = "{\"app\":{\"updated-date\":\"Mar 14, 2012 7:07:12 PM\",\"govern\":true,\"active-production-start-date\":\"Mar 14, 2" +
            "012 7:02:27 PM\",\"advertise\":false,\"user-type\":\"CORPORATE\",\"remarks\":\"Changed shortcode\",\"sp-id\":\"SPP_000026\",\"type\":\"flat\",\"password\":\"9bd9ced93bcdc5c64a5a569ff80b86e8\",\"_id\":\"APP_000129\",\"description\":\"Need news and info abo" +
            "ut Romith? - Alert\",\"name\":\"RomiAlert\",\"ncses\":[{\"status\":\"ncs-configured\",\"ncs-type\":\"subscription\"},{\"status\":\"ncs-configured\",\"ncs-type\":\"sms\",\"operator\":\"dialog\"}],\"black-list\":[],\"app-id\":\"APP_000129\",\"allowed-hosts\":[\"1" +
            "27.0.0.1\",\"192.168.0.38\",\"192.168.0.125\",\"192.168.0.76\",\"192.168.0.12\",\"192.168.0.244\"],\"expire\":true,\"mask-number\":false,\"end-date\":\"Apr 13, 2012 12:00:00 AM\",\"apply-tax\":false,\"status\":\"active-production\",\"created-date\":\"Ma" +
            "r 13, 2012 11:11:37 AM\",\"created-by\":\"romith\",\"app-request-date\":\"Mar 13, 2012 12:00:00 AM\",\"category\":\"soltura\",\"white-list\":[],\"revenue-share\":\"70\",\"updated-by\":\"sdpadmin\"},\"ncs\":{\"_id\":{\"_time\":1331617297,\"_machine\":179035" +
            "7756,\"_inc\":150882324,\"_new\":false},\"updated-date\":\"Mar 14, 2012 7:07:12 PM\",\"subscription-response-message\":\"Await updates\",\"status\":\"active-production\",\"created-date\":\"Mar 13, 2012 11:11:37 AM\",\"charging-type\":\"flat\",\"creat" +
            "ed-by\":\"romith\",\"ncs-type\":\"subscription\",\"frequency\":\"monthly\",\"unsubscription-response-message\":\"You will not receive updates\",\"operator\":\"\",\"amount\":\"30\",\"allow-http-subscription-requests\":true,\"app-id\":\"APP_000129\",\"max-n" +
            "o-of-bc-msgs-per-day\":\"100\",\"updated-by\":\"sdpadmin\"},\"sp\":{\"sdp-user\":\"true\",\"sms-mt\":true,\"coop-user-name\":\"romith\",\"status\":\"approved\",\"sms-mo-tpd\":\"10000\",\"created-by\":\"romith\",\"remarks\":\"Automatic Configuration\",\"sms-mt-t" +
            "pd\":\"10000\",\"sp-selected-services\":[\"sms\",\"subscription\"],\"sp-id\":\"SPP_000026\",\"sp-request-date\":\"03/05/12\",\"coop-user-id\":\"20120305090045978\",\"subscription-selected\":\"true\",\"sms-mo\":true,\"soltura-user\":\"true\",\"max-no-of-subs" +
            "cribers\":\"100000\",\"_id\":\"SPP_000026\",\"sms-selected\":\"true\",\"sms-mt-tps\":\"10\",\"max-no-of-bc-msgs-per-day\":\"9999\",\"sms-mo-tps\":\"10\"},\"request\":{\"charging-amount\":\"15.49\",\"status\":\"true\",\"recipients\":[{\"recipient-address-status\"" +
            ":\"S1000\",\"recipient-address\":\"94770102555\"}],\"msisdn\":\"94770102555\",\"keyword\":\"roma\",\"currency-code\":\"LKR\",\"sp-id\":\"SPP_000026\",\"ncs-type\":\"subscription\",\"operator\":\"dialog\",\"status-code\":\"S1000\",\"status-description\":\"Success" +
            "\",\"subscription-response\":\"SUCCESS\",\"shortcode\":\"8938\",\"app-id\":\"APP_000129\",\"correlation-id\":\"112033001190003\",\"sender-address\":\"8938\",\"notification-status-code\":\"SUCCESS\"}}" +
            "";

    private Map<String, Map<String, Object>> messageContext;

    @BeforeMethod
    public void test() {
        messageContext  = (Map<String, Map<String, Object>>) GsonUtil.parseElement(
                new JsonParser().parse(messageContextJson));
    }

    @Test
    public void testCreateTransLog() throws ParseException {

        channel = new SubscriptionChargingLogChannel();
        StringBuilder logBuilder = channel.doGenerateLog(messageContext, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").parse("2012-03-29 18:10:19 792"));
       /* assertEquals("112033001190003|2012-03-29 18:10:19 792|SPP_000026|romith|APP_000129|RomiAlert|live|||||94770102555||sms|smpp||subscription|unknown|subscriber|15.49|LKR|[{\"currencyCode\":\"LKR\", \"buyingRate\": 1, \"sellingRate\": 1}]|subscription|94770102555|0|registrationRecursiveCharging|S1000|Success|success|roma|8938|dialog||percentageFromMonthlyRevenue|70||||S1000|Success||pro-rate|0||||||",
                logBuilder.toString());*/
    }
}
