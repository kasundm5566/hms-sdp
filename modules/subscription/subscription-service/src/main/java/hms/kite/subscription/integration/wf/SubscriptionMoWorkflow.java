/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.subscription.integration.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.control.InExpression;
import hms.kite.wfengine.impl.ErrorResponseRoutingService;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;


public class SubscriptionMoWorkflow extends WrappedGeneratedWorkflow implements Workflow {

    @Autowired private Channel smsMoReplySenderChannel;
    @Autowired private Channel smsMoReplyMapperChannel;
    @Autowired private Channel subscriptionServiceWrapper;
    @Autowired private Channel whitelistService;
    @Autowired private Channel blacklistingService;
    @Autowired private Channel mnpService;
    @Autowired private Channel provNcsSlaChannel;
    @Autowired private Channel provSpSlaChannel;
    @Autowired private Channel provDetailedAppSlaChannel;
    @Autowired private Channel smsMoRoutingKeyChannel;
    @Autowired private Channel allNcsFetcherChannel;
    @Autowired private Channel subscriptionAtChannel;
    @Autowired private Channel numberMaskingChannel;

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionMoWorkflow.class);

    @Override
    protected Workflow generateWorkflow() {
        logger.debug("Generating work flow");
    	ErrorResponseRoutingService replyChannel = new ErrorResponseRoutingService("sms-mo-reply-channel");
        replyChannel.setChannel(smsMoReplySenderChannel);

        ErrorResponseRoutingService replyMapperChannel = new ErrorResponseRoutingService("sms-mo-reply-mapper-channel", replyChannel);
        replyMapperChannel.setChannel(smsMoReplyMapperChannel);

        //TODO: Move separate sub workflow
        ServiceImpl subscriptionAtService = new ServiceImpl("subscription-at-channel", replyMapperChannel);
        subscriptionAtService.setChannel(subscriptionAtChannel);
        subscriptionAtService.onDefaultError(replyMapperChannel);

        ServiceImpl numberMaskingChannelService = new ServiceImpl("number-masking-channel", subscriptionAtService);
        numberMaskingChannelService.setChannel(numberMaskingChannel);
        numberMaskingChannelService.addServiceParameter(maskK, true);

        EqualExpression maskingApplied = new EqualExpression(appK, maskNumberK, true);
        Condition maskingCondition = new Condition(maskingApplied, unknownErrorCode);

        ServiceImpl numberMaskingApplied = new ServiceImpl("masking.eligibility.validation", maskingCondition);
        numberMaskingApplied.setOnSuccess(numberMaskingChannelService);
        numberMaskingApplied.onDefaultError(subscriptionAtService);
         //---------------------------------

        ServiceImpl subscriptionChannel = new ServiceImpl("subscription-channel", numberMaskingApplied);
        subscriptionChannel.setChannel(subscriptionServiceWrapper);
        subscriptionChannel.onDefaultError(replyMapperChannel);

        ServiceImpl allNcsFetcher = new ServiceImpl("allNcsFetcherChannel", subscriptionChannel);
        allNcsFetcher.setChannel(allNcsFetcherChannel);

        ServiceImpl whitelist = new ServiceImpl("white-list");
        whitelist.setChannel(whitelistService);
        whitelist.setOnSuccess(allNcsFetcher);
        whitelist.onDefaultError(replyMapperChannel);

        EqualExpression appStatus = new EqualExpression(appK, statusK, limitedProductionK);
        Condition condition = new Condition(appStatus, unknownErrorCode);

        ServiceImpl whitelistApplied = new ServiceImpl("whitelist.eligibility.service", condition);
        whitelistApplied.setOnSuccess(whitelist);
        whitelistApplied.onDefaultError(allNcsFetcher);

        ServiceImpl blacklist = new ServiceImpl("black-list");
        blacklist.setChannel(blacklistingService);
        blacklist.setOnSuccess(whitelistApplied);
        blacklist.onDefaultError(replyMapperChannel);

        ServiceImpl numberChecking = new ServiceImpl("number-checking");
        numberChecking.setChannel(mnpService);
        numberChecking.setOnSuccess(blacklist);
        numberChecking.onDefaultError(replyMapperChannel);

        ServiceImpl ncsSlaChannel = new ServiceImpl("prov.ncs.sla.channel");
        ncsSlaChannel.setChannel(provNcsSlaChannel);
        ncsSlaChannel.setOnSuccess(numberChecking);
        ncsSlaChannel.onDefaultError(replyMapperChannel);

        ServiceImpl appStateValidationService = createAppStateValidationService();
        appStateValidationService.setOnSuccess(ncsSlaChannel);
        appStateValidationService.onDefaultError(replyMapperChannel);

        ServiceImpl spSlaValidation = createSpStateValidation();
        spSlaValidation.setOnSuccess(appStateValidationService);
        spSlaValidation.onDefaultError(replyMapperChannel);

        ServiceImpl spSlaChannel = new ServiceImpl("prov.sp.sla.channel", spSlaValidation);
        spSlaChannel.setChannel(provSpSlaChannel);
        spSlaChannel.onDefaultError(replyMapperChannel);

        ServiceImpl appSlaChannel = new ServiceImpl("prov.detailed.app.sla.channel", spSlaChannel);
        appSlaChannel.setChannel(provDetailedAppSlaChannel);
        appSlaChannel.onDefaultError(replyMapperChannel);

        ServiceImpl routingKeyChannel = new ServiceImpl("sms-mo-routing-key-channel", appSlaChannel);
        routingKeyChannel.setChannel(smsMoRoutingKeyChannel);
        routingKeyChannel.onDefaultError(replyMapperChannel);

        return new WorkflowImpl(routingKeyChannel);
    }

    private static ServiceImpl createAppStateValidationService() {
        InExpression statusExpression = new InExpression(appK, statusK, new Object[]{limitedProductionK,
                activeProductionK});
        Condition statusCondition = new Condition(statusExpression, authenticationFailedErrorCode);

        return new ServiceImpl("app.state.validation.service", statusCondition);
    }

    private static ServiceImpl createSpStateValidation() {
        EqualExpression statusExpression = new EqualExpression(spK, statusK, approvedK);
        Condition statusCondition = new Condition(statusExpression, authenticationFailedErrorCode);

        InExpression ncsAllowedExpression = new InExpression(spK, spSelectedServicesK, new Object[]{smsK});
        Condition ncsAllowedCondition = new Condition(ncsAllowedExpression, ncsNotAllowedErrorCode);

        return new ServiceImpl("sp.state.validation.service", statusCondition, ncsAllowedCondition);
    }

    @Override
    public void executeWorkflow(Map<String, Map<String, Object>> requestContext) {
        super.executeWorkflow(requestContext);
    }
}
