/**
 *   (C) Copyright 2011-2012 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.subscription.integration.channel;

import hms.kite.subscription.core.domain.SubscriptionResponse;
import hms.kite.subscription.util.ReplyBuilder;
import hms.kite.util.IdGenerator;
import hms.kite.util.SystemUtil;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;
import hms.kite.wfengine.transport.Channel;
import org.apache.log4j.NDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteErrorBox.invalidRequestErrorCode;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SubscriptionNblWfChannel extends BaseChannel {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionNblWfChannel.class);

    private Workflow subscriptionNblWf;

    private Channel subscriptionNotificationWfChannel;
    private ReplyBuilder replyBuilder;

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        try {
            if(!validate(requestContext)) {
                logger.error("Validation failed for the request [{}]", requestContext.get(requestK));
                return ResponseBuilder.convert2NblErrorResponse(ResponseBuilder.generate(invalidRequestErrorCode,
                        false, requestContext));
            }
            addNblParameters(requestContext);
            NDC.push((String) requestContext.get(requestK).get(correlationIdK));
            if (requestContext.get(requestK).get(subscriptionRequestTypeK).equals(subscriptionNotificationK)) {
                return executeSubscriptionNotificationFlow(requestContext);
            }
            return executeSubscriptionFlow(requestContext);
        } catch (Exception e) {
            logger.error("Exception occurred while executing flow", e);
            return ResponseBuilder.convert2NblErrorResponse(ResponseBuilder.generate(requestContext, e));
        } finally {
            NDC.pop();
        }
    }

    private Map<String, Object> executeSubscriptionFlow(Map<String, Map<String, Object>> requestContext) {
        try {
            subscriptionNblWf.executeWorkflow(requestContext);
            return generateNblResponse(ResponseBuilder.generateResponse(requestContext), requestContext);
        } catch (Exception e) {
            logger.error("Exception occurred while executing flow", e);
            return ResponseBuilder.generate(requestContext, e);
        }
    }

    private Map<String, Object> executeSubscriptionNotificationFlow(Map<String, Map<String, Object>> requestContext) {
        try {
            Map<String, Object> request = requestContext.get(requestK);
            SubscriptionResponse mandateResponse = (SubscriptionResponse) request.get("mandate-response");
            String message = replyBuilder.createGenericResponse(requestContext, mandateResponse);
            request.put(messageK, message);
            subscriptionNotificationWfChannel.execute(requestContext);
            return generateNblResponse(ResponseBuilder.generateResponse(requestContext), requestContext);
        } catch (Exception e) {
            logger.error("Exception occurred while executing flow", e);
            return ResponseBuilder.generate(requestContext, e);
        }
    }

    private void addNblParameters(Map<String, Map<String, Object>> requestContext) {
        requestContext.get(requestK).put(correlationIdK, IdGenerator.generate());
        requestContext.get(requestK).put(receiveTimeK, Long.toString(System.currentTimeMillis()));
        requestContext.get(requestK).put(ncsTypeK, subscriptionK);
        requestContext.get(requestK).put(appIdK, requestContext.get(requestK).get(applicationIdK));
        requestContext.get(requestK).put(directionK, aoK);
        if (requestContext.get(requestK).containsKey(senderAddressNblK)) {
            requestContext.get(requestK).put(senderAddressK, requestContext.get(requestK).get(senderAddressNblK));
        }

        if (requestContext.get(requestK).containsKey(subscriberIdNblK)) {
            List<Map<String, String>> recipients = new ArrayList<Map<String, String>>();
            recipients.add(SystemUtil.convert2NblAddresses((String) requestContext.get(requestK).get(subscriberIdNblK)));
            requestContext.get(requestK).put(recipientsK, recipients);
        }

        addMissingRequiredParameters(requestContext);
    }

    private void addMissingRequiredParameters(Map<String, Map<String, Object>> requestContext) {
        if(!requestContext.get(requestK).containsKey(operatorK)){
            requestContext.get(requestK).put(operatorK, "");
        }

        if(!requestContext.get(requestK).containsKey(keywordK)){
            requestContext.get(requestK).put(keywordK, "");
        }

        if(!requestContext.get(requestK).containsKey(shortcodeK)){
            requestContext.get(requestK).put(shortcodeK, "");
        }
    }

    private Map<String, Object> generateNblResponse(Map<String, Object> resp,
                                                    Map<String, Map<String, Object>> requestContext) {
        Object status = resp.remove(statusK);
        resp = ResponseBuilder.convert2NblErrorResponse(resp);
        addResultOfRequest(resp, requestContext.get(requestK), status);
        if (requestContext.get(requestK).containsKey(subscriptionStatusK)) {
            resp.put(subscriptionStatusK, requestContext.get(requestK).get(subscriptionStatusK).toString());
        }
        return resp;
    }

    private void addResultOfRequest(Map<String, Object> resp, Map<String, Object> request ,Object status) {
        if(status != null && Boolean.parseBoolean(status.toString())) {
            String subscriptionType = (String) request.get(subscriptionRequestTypeK);
            if(subscriptionType.equals(baseSizeK)) {
                resp.put(baseSizeK, request.get(baseSizeK));
            } else if(subscriptionType.equals(subscribedAppsK)) {
                resp.put(subscribedAppsK, request.get(subscribedAppsK));
            } else if(subscriptionType.equals(subscriptionStatusK)) {
                resp.put(subscriptionStatusK, request.get(subscriptionStatusK));
            }
        }
    }

    private boolean validate(Map<String, Map<String, Object>> requestContext) {
        logger.debug("Validation subscription request for type - {}",
                requestContext.get(requestK).get(subscriptionRequestTypeK));
        if(regOrUnreg(requestContext)) {
            return validateRegOrUnregRequest(requestContext);
        } else if(requestContext.get(requestK).get(subscriptionRequestTypeK).equals(baseSizeK)) {
            return validateBaseSizeRequest(requestContext);
        } else if(requestContext.get(requestK).get(subscriptionRequestTypeK).equals(subscriptionStatusK)) {
            return validateStatusRequest(requestContext);
        } else if(requestContext.get(requestK).get(subscriptionRequestTypeK).equals(subscribedAppsK)) {
            return validateAppListRequest(requestContext);
        } else if(requestContext.get(requestK).get(subscriptionRequestTypeK).equals(subscriptionNotificationK)) {
            return validateSubscriptionNotificationRequest(requestContext);
        }
        return false;
    }

    //TODO add subscription status description to the request map
    private boolean validateRegOrUnregRequest(Map<String, Map<String, Object>> requestContext) {
        boolean isValid = true;
        if(!validAppParameter(requestContext)) {
            isValid = false;
        } else if(!requestContext.get(requestK).containsKey(subscriberIdNblK)
                || requestContext.get(requestK).get(subscriberIdNblK) == null
                || requestContext.get(requestK).get(subscriberIdNblK).toString().isEmpty()) {
            isValid = false;
            logger.warn("Invalid subscriber id [{}]",
                    requestContext.get(requestK).get(subscriberIdNblK));
        } else if(!requestContext.get(requestK).containsKey(actionK)
                || !(optInK.equals(requestContext.get(requestK).get(actionK))
                || optOutK.equals(requestContext.get(requestK).get(actionK)))) {
            isValid = false;
            logger.warn("Invalid action to reg/unreg [{}]",
                    requestContext.get(requestK).get(actionK));
        }
        return isValid;
    }

    private boolean validateBaseSizeRequest(Map<String, Map<String, Object>> requestContext) {
        return validAppParameter(requestContext);
    }

    private boolean validateStatusRequest(Map<String, Map<String, Object>> requestContext) {
        boolean isValid = false;
        if(validAppParameter(requestContext)) {
            if(requestContext.get(requestK).containsKey(subscriberIdNblK)
                    && requestContext.get(requestK).get(subscriberIdNblK) != null
                    && !requestContext.get(requestK).get(subscriberIdNblK).toString().isEmpty()) {
                isValid = true;
            } else {
                logger.warn("Invalid sender address [{}]",
                        requestContext.get(requestK).get(senderAddressNblK));
            }
        }
        return isValid;
    }

    private boolean validateAppListRequest(Map<String, Map<String, Object>> requestContext) {
        if(requestContext.get(requestK).containsKey(senderAddressNblK)
                && requestContext.get(requestK).get(senderAddressNblK) != null
                && !requestContext.get(requestK).get(senderAddressNblK).toString().isEmpty()) {
            return true;
        } else {
            logger.warn("Invalid sender address [{}]",
                    requestContext.get(requestK).get(senderAddressNblK));
        }
        return false;
    }

    private boolean validateSubscriptionNotificationRequest(Map<String, Map<String, Object>> requestContext) {
        return true;
    }

    private boolean validAppParameter(Map<String, Map<String, Object>> requestContext) {
        boolean isValid = false;
        if(requestContext.get(requestK).containsKey(applicationIdK)
                && requestContext.get(requestK).get(applicationIdK) != null
                && !requestContext.get(requestK).get(applicationIdK).toString().isEmpty()) {
            isValid = true;
        } else {
            logger.warn("Invalid app parameter [{}]",
                    requestContext.get(requestK).containsKey(applicationIdK));
        }
        return isValid;
    }

    private boolean regOrUnreg(Map<String, Map<String, Object>> requestContext) {
        return ((requestContext.get(requestK).get(subscriptionRequestTypeK) != null)
                && (requestContext.get(requestK).get(subscriptionRequestTypeK).equals(regK)
                || requestContext.get(requestK).get(subscriptionRequestTypeK).equals(unregK)));
    }

    public void setSubscriptionNblWf(Workflow subscriptionNblWf) {
        this.subscriptionNblWf = subscriptionNblWf;
    }

    public void setSubscriptionNotificationWfChannel(Channel subscriptionNotificationWfChannel) {
        this.subscriptionNotificationWfChannel = subscriptionNotificationWfChannel;
    }

    public void setReplyBuilder(ReplyBuilder replyBuilder) {
        this.replyBuilder = replyBuilder;
    }
}
