/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.subscription;

import com.google.common.base.Optional;
import hms.kite.subscription.core.domain.SubscriptionNblResponse;
import hms.kite.subscription.core.domain.SubscriptionResponse;
import hms.kite.subscription.mail.BaseSizeExceedNotifyEmailSender;
import hms.kite.subscription.util.ReplyBuilder;
import hms.kite.util.KeyNameSpaceResolver;
import hms.kite.util.SdpException;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import static hms.kite.subscription.core.service.request.RequestContextParser.getRecipientAddress;
import static hms.kite.subscription.core.service.request.RequestContextParser.parseMsisdnThroughHttpSubscription;
import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 */
public class SubscriptionServiceWrapper implements Channel {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionServiceWrapper.class);
    private SubscriptionAdapter subscriptionAdapter;
    private ReplyBuilder replyBuilder;
    private BaseSizeExceedNotifyEmailSender baseSizeExceedNotifyEmailSender;

    private boolean successResponse;
    private boolean alreadyRegisteredResponse;
    private boolean chargingErrorResponse;
    private boolean slaLimitationErrorResponse;
    private boolean spSlaLimitationErrorResponse;
    private boolean userBlockedResponse;
    private boolean initialResponse;
    private boolean sendChargingPendingResponse;
    private boolean userNotRegisteredResponse;
    private boolean successUnregisteredDueToChargingErrorResponse;


    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        try {
            String subType = (String) requestContext.get(requestK).get(subscriptionRequestTypeK);
            String keyword = (String) requestContext.get(requestK).get(serviceKeywordK);
            if (logger.isDebugEnabled()) {
                logger.debug("request context received: [{}]", requestContext);
                logger.debug("subscription-type [{}], service-keyword [{}]", subType, keyword);
            }
            if (regK.equals(subType) || isActionSet(requestContext.get(requestK), keyword)) {
                return handleRegistrations(requestContext);
            } else if (unregK.equals(subType) || unregK.equals(keyword)) {
                return processUnreg(requestContext);
            } else if (baseSizeK.equals(subType)) {
                return processBaseSize(requestContext);
            } else if (subscribedAppsK.equals(subType)) {
                return processSubscribedApps(requestContext);
            } else if (subscriptionStatusK.equals(subType)) {
                return processSusbscriptionStatus(requestContext);
            }
        } catch (SdpException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error when executing channel", e);
            return ResponseBuilder.generate(requestContext, e);
        }
        logger.warn("unknown request is received");
        return ResponseBuilder.generate(systemErrorCode, false, requestContext);
    }

    private boolean isActionSet(Map<String, Object> request, String keyword) {
        boolean actionSet = false;
        if (request.get(actionK) == null && keyword != null) {
            if (regK.equals(keyword)) {
                request.put(actionK, optInK);
                actionSet = true;
            } else if (unregK.equals(keyword)) {
                request.put(actionK, optOutK);
                actionSet = true;
            }
        }
        return actionSet;
    }

    private Map<String, Object> handleRegistrations(Map<String, Map<String, Object>> requestContext) {
        Object action = requestContext.get(requestK).get(actionK);
        logger.debug("registration request with action [{}]", action);
        if (action != null) { // TODO: opt-in opt-out values are to be changed, so the implementaiton has to change too.
            int actionValue = Integer.parseInt(action.toString());
            if (actionValue == 0) {
                return processUnreg(requestContext);
            } else if (actionValue == 1) {
                return processReg(requestContext);
            }
        }
        return ResponseBuilder.generate(requestContext, new SdpException(systemErrorCode));
    }

    /*private SubscriptionStatus determineSubscriptionStatus(SubscriptionResponse resp, boolean isReg) {
        if (isReg) {
            if ((resp == SubscriptionResponse.SUCCESS) || (resp == SubscriptionResponse.SUCCESS_FREE)) {
                return SubscriptionStatus.REGISTERED;
            }
        }
    }*/

    private void getSubscriptionStatus(Map<String, Map<String, Object>> requestContext) {
        // (if (= "mo" direction) sender-address (-> recipients first :recipient-address))
        String msisdn = getSubscriberMsisdn(requestContext);
        String appId = (String)requestContext.get(appK).get(appIdK);
        logger.debug("Finding subscription status for msisdn[{}], appId[{}]", msisdn, appId);
        SubscriptionNblResponse status = subscriptionAdapter.findStatus(msisdn, appId);
        requestContext.get(requestK).put(subscriptionStatusK, status.getNblResponseValue());
    }

    private String getSubscriberMsisdn(Map<String, Map<String, Object>> requestContext) {
        if(moK.equals(KeyNameSpaceResolver.data(requestContext, requestK, directionK))) {
            return (String) KeyNameSpaceResolver.data(requestContext, requestK, senderAddressK);
        } else {
            return ((Map) (((List) requestContext.get(requestK).get(recipientsK)).get(0))).get(recipientAddressK).toString();
        }
    }

    private void getOperatorForSubscriber(Map<String, Map<String, Object>> requestContext) {
        validateRecipientAddressStatus(requestContext);
        String operator = ((Map) (((List) requestContext.get(requestK).get(recipientsK)).get(0))).get(recipientAddressOptypeK).toString();
        requestContext.get(requestK).put(operatorK, operator);
    }

    private void validateRecipientAddressStatus(Map<String, Map<String, Object>> requestContext) {
        String recipientAddressStatus = ((List<Map<String, String>>) requestContext.get(requestK).get(recipientsK))
                .get(0).get(recipientAddressStatusK);
        if (!successCode.equals(recipientAddressStatus)) {
            logger.warn("Recipient address status is invalid - ignoring request");
            throw new SdpException(recipientAddressStatus);
        }
    }

    private Map<String, Object> processReg(Map<String, Map<String, Object>> requestContext) {
        logger.debug("Reg message received");
        if (aoK.equals(requestContext.get(requestK).get(directionK))) {
            getOperatorForSubscriber(requestContext);
        }
        SubscriptionResponse resp = subscriptionAdapter.register(requestContext);
        getSubscriptionStatus(requestContext);
        logger.debug("Registration response returned [{}]", resp);
        return returnResponse(requestContext, resp, true);
    }

    private Map<String, Object> processUnreg(Map<String, Map<String, Object>> requestContext) {
        logger.debug("Unreg message received");
        SubscriptionResponse resp = subscriptionAdapter.unRegister(requestContext);
        getSubscriptionStatus(requestContext);
        if (aoK.equals(requestContext.get(requestK).get(directionK))) {
            getOperatorForSubscriber(requestContext);
        }
        logger.debug("Unregistration response returned [{}]", resp);
        logger.debug("Context after processing[{}]", requestContext);
        return returnResponse(requestContext, resp, false);
    }

    private Map<String, Object> processBaseSize(Map<String, Map<String, Object>> requestContext) {
        try {
            logger.info("Finding subscription base size for app[{}]", requestContext.get(appK).get(appIdK));
            Long baseSize = subscriptionAdapter.baseSize((String) requestContext.get(appK).get(appIdK));
            logger.info("Base size for app[{}] is [{}]", requestContext.get(appK).get(appIdK), baseSize);
            requestContext.get(requestK).put(baseSizeK, baseSize.toString());
            return ResponseBuilder.generateSuccess(requestContext);
        } catch (Exception e) {
            logger.error("Error occured while getting base size", e);
            return ResponseBuilder.generate(unknownErrorCode, false, requestContext);
        }
    }

    private Map<String, Object> processSubscribedApps(Map<String, Map<String, Object>> requestContext) {
        logger.info("app-list request received");
        List<String> apps = subscriptionAdapter.findAppList(getSenderAddress(requestContext));
        if (logger.isDebugEnabled()) {
            for (String str : apps) {
                logger.debug("app [{}]", str);
            }
        }
        requestContext.get(requestK).put(subscribedAppsK, apps);
        return ResponseBuilder.generateSuccess(requestContext);
    }

    private void addSubscriptionStatus(Map<String, Map<String, Object>> requestContext) {
        validateRecipientAddressStatus(requestContext);
        final Optional<String> subscriberIdOpt = Optional.fromNullable((String)requestContext.get(requestK).get(subscriberIdNblK));

        final String senderAddressStatus = (String) requestContext.get(requestK).get(senderAddressStatusK);
        String recipientAddress = getRecipientAddress(requestContext);
        logger.info("Received recipientAddress [{}]", recipientAddress);

        if (recipientAddress != null) {
            SubscriptionNblResponse status = subscriptionAdapter.findStatus(recipientAddress,
                    requestContext.get(appK).get(appIdK).toString());
            requestContext.get(requestK).put(subscriptionStatusK, status.getNblResponseValue());
        } else {
            throw new SdpException(senderAddressStatus);
        }

        /*if(subscriberIdOpt.isPresent()) {
            final Optional<String> msisdnOpt = parseMsisdnThroughHttpSubscription(subscriberIdOpt.get());

            if(msisdnOpt.isPresent()) {
                SubscriptionNblResponse status = subscriptionAdapter.findStatus(msisdnOpt.get(),
                        requestContext.get(appK).get(appIdK).toString());
                requestContext.get(requestK).put(subscriptionStatusK, status.getNblResponseValue());
            }
            else {
                throw new SdpException(senderAddressStatus);
            }
        } else {
            throw new SdpException(senderAddressStatus);
        }*/

    }

    private Map<String, Object> processSusbscriptionStatus(Map<String, Map<String, Object>> requestContext) {
        logger.debug("subscription status request received");
        addSubscriptionStatus(requestContext);
        return ResponseBuilder.generateSuccess(requestContext);
    }

    private String getSenderAddress(Map<String, Map<String, Object>> requestContext) {
        String senderAddressStatus = (String) requestContext.get(requestK).get(senderAddressStatusK);
        if (!successCode.equals(senderAddressStatus)) {
            logger.warn("Sender address status is invalid - ignoring request");
            throw new SdpException(senderAddressStatus);
        }
        return requestContext.get(requestK).get(senderAddressK).toString();
    }

    private Map<String, Object> returnResponse(Map<String, Map<String, Object>> requestContext,
                                               SubscriptionResponse resp, boolean reg) { // TODO: add descriptions
        // to properties
        requestContext.get(requestK).put(subscriptionRespK,resp.toString());
        if (reg) {
            if (resp.equals(SubscriptionResponse.SUCCESS) || resp.equals(SubscriptionResponse.SUCCESS_FREE)) {
                requestContext.get(requestK).put(replyK,
                        replyBuilder.createGenericResponse(requestContext, resp));
                return ResponseBuilder.generateSuccess(requestContext);

            } else if (resp.equals(SubscriptionResponse.ALREADY_REGISTERED)) {
                requestContext.get(requestK).put(replyK,
                        replyBuilder.createAlreadyResigeredResponse(requestContext, resp));
                return ResponseBuilder.generate(subscriptionRegAlreadyRegErrorCode, "user already registered", false);
            } else if (resp.equals(SubscriptionResponse.TEMP_SYSTEM_ERROR)) {
                requestContext.get(requestK).put(replyK,
                        replyBuilder.createRegistrationFailChargingResponse(requestContext, resp));
                return ResponseBuilder.generate(subscriptionRegChargingErrorCode, "Temporary system error", false);
            } else if (resp.equals(SubscriptionResponse.SLA_LIMITATION_ERROR)) {
                requestContext.get(requestK).put(replyK,
                        replyBuilder.createRegistrationFailSlaResponse(requestContext, resp));
                baseSizeExceedNotifyEmailSender.sendMail(requestContext, resp);
                return ResponseBuilder.generate(subscriptionRegSlaErrorCode, "sla error", false);
            } else if (resp.equals(SubscriptionResponse.SP_SLA_LIMITATION_ERROR)) {
                requestContext.get(requestK).put(replyK,
                        replyBuilder.createRegistrationFailSpSlaResponse(requestContext, resp));
                baseSizeExceedNotifyEmailSender.sendMail(requestContext, resp);
                return ResponseBuilder.generate(subscriptionRegSlaErrorCode, "sp sla error", false);
            } else if (resp.equals(SubscriptionResponse.USER_BLOCKED)) {
                requestContext.get(requestK).put(replyK,
                        replyBuilder.createRegistrationFailBlockedResponse(requestContext, resp));
                return ResponseBuilder.generate(subscriptionRegBlockedErrorCode, "user blocked", false);
            } else if (resp.equals(SubscriptionResponse.INITIAL)) {
                requestContext.get(requestK).put(replyK,
                        replyBuilder.createRegistrationPendingChargingResponse(requestContext, resp));
                return ResponseBuilder.generateResponse(requestContext);
            } else if (resp.equals(SubscriptionResponse.CHARGING_PENDING)) {
                if (sendChargingPendingResponse) {
                    requestContext.get(requestK).put(replyK,
                            replyBuilder.createRegistrationPendingChargingResponse(requestContext, resp));
                }
                return ResponseBuilder.generateSuccess(requestContext);
            } else if (resp.equals(SubscriptionResponse.SUCCESS_UNREGISTERED_FROM_REG_PENDING_DUE_TO_CHARGING_ERROR)) {
                requestContext.get(requestK)
                        .put(replyK, replyBuilder.createUnRegChargingFailureResponse(requestContext, resp));
                return ResponseBuilder.generate(subscriptionRegChargingErrorCode, "Subscription failed from reg pending state due to charging error. no more retry", false);
            } else if (resp.equals(SubscriptionResponse.PGW_CONNECTOR_ERROR)) {
                requestContext.get(requestK)
                        .put(replyK, replyBuilder.createUnRegChargingFailureResponse(requestContext, resp));
                return ResponseBuilder.generate(subscriptionRegChargingErrorCode, "Subscription failed due to pgw connection error. no more retry", false);
            } else if (resp.equals(SubscriptionResponse.INTERNAL_ERROR)) {
                requestContext.get(requestK)
                        .put(replyK, replyBuilder.createGenericResponse(requestContext, resp));
                return ResponseBuilder.generate(systemErrorCode, "Subscription failed due to Internal error.", false);
            } else if (resp.equals(SubscriptionResponse.EXTERNAL_ERROR)) {
                requestContext.get(requestK)
                        .put(replyK, replyBuilder.createGenericResponse(requestContext, resp));
                return ResponseBuilder.generate(externalErrorCode, "Subscription failed due to pgw external error", false);
            } else if (resp.equals(SubscriptionResponse.INVALID_USER_ACCOUNT)) {
                requestContext.get(requestK)
                        .put(replyK, replyBuilder.createGenericResponse(requestContext, resp));
                return ResponseBuilder.generate(invalidUserAccountErrorCode, "Subscription failed due to invalid user account", false);
            }
        } else {
            if (resp.equals(SubscriptionResponse.SUCCESS)) {
                requestContext.get(requestK).put(replyK,
                        replyBuilder.createUnregistrationSuccessResponse(requestContext, resp));
                return ResponseBuilder.generateSuccess(requestContext);
            } else if (resp.equals(SubscriptionResponse.USER_BLOCKED)) {
                requestContext.get(requestK).put(replyK,
                        replyBuilder.createUnregistrationFailBlokedResponse(requestContext, resp));
                return ResponseBuilder.generate(subscriptionUnregBlockedErrorCode, "user blocked", false);
            } else if (resp.equals(SubscriptionResponse.USER_NOT_REGISTERED)) {
                requestContext.get(requestK)
                        .put(replyK, replyBuilder.createNotRegisteredResponse(requestContext, resp));
                return ResponseBuilder.generate(subscriptionUnregNotRegErrorCode, "not registered", false);
            } else if (resp.equals(SubscriptionResponse.SUCCESS_UNREGISTERED_DUE_TO_CHARGING_ERROR)) {
                requestContext.get(requestK)
                        .put(replyK, replyBuilder.createUnRegChargingFailureResponse(requestContext, resp));
                return ResponseBuilder.generate(subscriptionRegChargingErrorCode, "un-registered due to charging error. no more retry", false);
            } else if (resp.equals(SubscriptionResponse.SUCCESS_UNREGISTERED_FROM_REG_PENDING_DUE_TO_CHARGING_ERROR)) {
                requestContext.get(requestK)
                        .put(replyK, replyBuilder.createUnRegChargingFailureResponse(requestContext, resp));
                return ResponseBuilder.generate(subscriptionRegChargingErrorCode, "un-registered from reg pending state due to charging error. no more retry", false);
            } else if (resp.equals(SubscriptionResponse.PGW_CONNECTOR_ERROR)) {
                requestContext.get(requestK)
                        .put(replyK, replyBuilder.createUnRegChargingFailureResponse(requestContext, resp));
                return ResponseBuilder.generate(subscriptionRegChargingErrorCode, "un-registered due to pgw connection error. no more retry", false);
            } else if (resp.equals(SubscriptionResponse.INTERNAL_ERROR)) {
                requestContext.get(requestK)
                        .put(replyK, replyBuilder.createGenericResponse(requestContext, resp));
                return ResponseBuilder.generate(systemErrorCode, "un-registered due to pgw internal error. no more retry", false);
            } else if (resp.equals(SubscriptionResponse.EXTERNAL_ERROR)) {
                requestContext.get(requestK)
                        .put(replyK, replyBuilder.createGenericResponse(requestContext, resp));
                return ResponseBuilder.generate(externalErrorCode, "un-registered due to pgw external error", false);
            } else if (resp.equals(SubscriptionResponse.INVALID_USER_ACCOUNT)) {
                requestContext.get(requestK)
                        .put(replyK, replyBuilder.createGenericResponse(requestContext, resp));
                return ResponseBuilder.generate(invalidUserAccountErrorCode, "un-registered due to invalid user account", false);
            }
        }
        logger.warn("unmapped response [{}] reg[{}]", resp.name(), reg);
        return null;
    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        // no impl needed
        return null;
    }

    public void setSubscriptionAdapter(SubscriptionAdapter subscriptionAdapter) {
        this.subscriptionAdapter = subscriptionAdapter;
    }

    public void setReplyBuilder(ReplyBuilder replyBuilder) {
        this.replyBuilder = replyBuilder;
    }

    public void setBaseSizeExceedNotifyEmailSender(BaseSizeExceedNotifyEmailSender baseSizeExceedNotifyEmailSender) {
        this.baseSizeExceedNotifyEmailSender = baseSizeExceedNotifyEmailSender;
    }

    public void setSuccessResponse(boolean successResponse) {
        this.successResponse = successResponse;
    }

    public void setAlreadyRegisteredResponse(boolean alreadyRegisteredResponse) {
        this.alreadyRegisteredResponse = alreadyRegisteredResponse;
    }

    public void setChargingErrorResponse(boolean chargingErrorResponse) {
        this.chargingErrorResponse = chargingErrorResponse;
    }

    public void setSlaLimitationErrorResponse(boolean slaLimitationErrorResponse) {
        this.slaLimitationErrorResponse = slaLimitationErrorResponse;
    }

    public void setSpSlaLimitationErrorResponse(boolean spSlaLimitationErrorResponse) {
        this.spSlaLimitationErrorResponse = spSlaLimitationErrorResponse;
    }

    public void setUserBlockedResponse(boolean userBlockedResponse) {
        this.userBlockedResponse = userBlockedResponse;
    }

    public void setInitialResponse(boolean initialResponse) {
        this.initialResponse = initialResponse;
    }

    public void setSendChargingPendingResponse(boolean sendChargingPendingResponse) {
        this.sendChargingPendingResponse = sendChargingPendingResponse;
    }

    public void setUserNotRegisteredResponse(boolean userNotRegisteredResponse) {
        this.userNotRegisteredResponse = userNotRegisteredResponse;
    }

    public void setSuccessUnregisteredDueToChargingErrorResponse(boolean successUnregisteredDueToChargingErrorResponse) {
        this.successUnregisteredDueToChargingErrorResponse = successUnregisteredDueToChargingErrorResponse;
    }
}
