/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.subscription.integration.channel;

import hms.kite.subscription.core.domain.SubscriptionResponse;
import hms.kite.util.GsonUtil;
import hms.kite.util.KiteErrorBox;
import hms.kite.util.KiteKeyBox;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import static hms.kite.util.KeyNameSpaceResolver.data;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 * todo: Reuse existing translog intercepter #TransLogLoggingInterceptor
 */
public class SubscriptionChargingLogChannel extends BaseChannel {

    private static ThreadLocal<DateFormat> dateTimeFormat = new ThreadLocal<DateFormat>();
    public static final String APP_STATE_LIMITED_LIVE = "limitedLive";
    public static final String APP_STATE_LIVE = "live";
    public static final String APP_STATE_INTERMEDIATE = "intermediate";
    private static final String PERCENTAGE_FROM_MONTHLY_REVENUE = "percentageFromMonthlyRevenue";

    private static final Logger reportLogger = LoggerFactory.getLogger("SDP.TransLog");
    private static final Logger logger = LoggerFactory.getLogger(SubscriptionChargingLogChannel.class);
    private static String delemeter = "|";
    private static String datePattern = "yyyy-MM-dd HH:mm:ss SSS";
    private String defaultRateCard = "['{'\"currencyCode\":\"{0}\", \"buyingRate\": 1, \"sellingRate\": 1'}']";

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        logger.debug("Going to log charging information from request context {}", GsonUtil.toJson(requestContext));
        try {
            StringBuilder logMessage = doGenerateLog(requestContext, new Date());
            reportLogger.info(logMessage.toString());
        } catch (Exception e) {
            logger.error("Unexpected error occurred while trying to log charging information", e);
            ResponseBuilder.generate(requestContext, e);
        }
        return ResponseBuilder.generateSuccess();
    }

    StringBuilder doGenerateLog(Map<String, Map<String, Object>> requestContext, Date date) {
        if (dateTimeFormat.get() == null) {
            dateTimeFormat.set(new SimpleDateFormat(datePattern));
        }
        Map<String, Object> request = requestContext.get("request");

        StringBuilder logMessage = new StringBuilder();
        addRecode(logMessage, request.get(correlationIdK));             // (1) Correlation Id
        addRecode(logMessage, dateTimeFormat.get().format(date));       // (2) Time Stamp
        addRecode(logMessage, request.get(spIdK));                      // (3) sp id
        addRecode(logMessage, findSpName(requestContext));              // (4) Sp Name
        addRecode(logMessage, request.get(appIdK));                     // (5) App Id
        addRecode(logMessage, findAppName(requestContext));             // (6) App Name
        addRecode(logMessage, translateAppState(data((Map) requestContext, appK, statusK)));
        // (7) App Status
        addRecode(logMessage, "");                                      // (8) source_address
        addRecode(logMessage, "");                                      // (9) masked_source_address
        addRecode(logMessage, "");                                      // (10) source_channel_type
        addRecode(logMessage, "");                                      // (11) source_channel_type
        addRecode(logMessage, request.get(msisdnK));                     // (12) destination_address
        addRecode(logMessage, "");                                      // (13) masked_destination_address
        addRecode(logMessage, "sms");                                   // (14) destination_channel_type
        addRecode(logMessage, "smpp");                                  // (15) destination_channel_protocol
        addRecode(logMessage, "");                                      // (16) direction
        addRecode(logMessage, "subscription");                          // (17) ncs
        addRecode(logMessage, "unknown");                               // (18) billing_type
        addRecode(logMessage, "subscriber");                            // (19) charge_party_type
        addRecode(logMessage, request.get(chargingAmountK));            // (20) charge_amount
        addRecode(logMessage, request.get(currencyCodeK));              // (21) charge_amount_currency;
        addRecode(logMessage, MessageFormat.format(defaultRateCard, request.get(currencyCodeK)));//todo get the actual rate card
        // (22) rate_card
        addRecode(logMessage, "subscription");                          // (23) charged_category
        addRecode(logMessage, request.get(msisdnK));                    // (24)charge_address
        addRecode(logMessage, "0");                                     // (25) masked_charge_address
        addRecode(logMessage, getEventType(request));                   // (26) event_type
        addRecode(logMessage, findStatusCode(request));                 // (27) response_code
        addRecode(logMessage, request.get(statusDescriptionK));         // (28) response_description
        addRecode(logMessage, findTransactionStatus(request));          // (29)transaction status
        addRecode(logMessage, request.get(keywordK));                   // (30)Key word
        addRecode(logMessage, request.get(shortcodeK));                 // (31)short_code
        addRecode(logMessage, request.get(operatorK));                  // (32)operator
        addRecode(logMessage, "");                                      // (33)operator_cost
        addRecode(logMessage, PERCENTAGE_FROM_MONTHLY_REVENUE);         // (34)revenue_share_type
        addRecode(logMessage, data((Map)requestContext, appK, revenueShareK)); // (35)revenue_share_percentage
        addRecode(logMessage, "");                                      // (36)advertisement_name
        addRecode(logMessage, "");                                      // (37)advertisement_content
        addRecode(logMessage, "");                                      // (38)is_multiple_recipients
        addRecode(logMessage, findStatusCode(request));                 // (39)overall_response_code
        addRecode(logMessage, request.get(statusDescriptionK));         // (40)overall_response_description
        addRecode(logMessage, "");                                      // (41)pgw_transaction_id
        addRecode(logMessage, "pro-rate");                              // (42)charging_type
        addRecode(logMessage, "0");                                     // (43)is_number_masked
        addRecode(logMessage, "");                                      // (44)order_no
        addRecode(logMessage, "");                                      // (45)invoice_no
        addRecode(logMessage, "");                                      // (46)external_trx_id
        addRecode(logMessage, "");                                      // (47)session_id
        addRecode(logMessage, "");                                      // (48)ussd_operation
        addRecode(logMessage, "");                                      // (49)balance_due
        addRecode(logMessage, "");                                      // (50)total_amount
        addRecode(logMessage, "");                                      // (51)account_type
        addRecode(logMessage, "");                                      // (52)account_statue
        addRecode(logMessage, "");                                      // (53)chargeable_balance
        addRecode(logMessage, "");                                      // (54)service_type
        addRecode(logMessage, "");                                      // (55)response_time
        addRecode(logMessage, "");                                      // (56)freshness
        addLastRecode(logMessage, "");                                  // (57)horizontal_accuracy
        return logMessage;
    }

    private String getEventType(Map<String, Object> request) {
        SubscriptionResponse subscriptionResponse = SubscriptionResponse.valueOf((String)request.get(subscriptionRespK));
        if(SubscriptionResponse.RECURSIVE_SUCCESS == subscriptionResponse){
           return "registrationRecursiveCharging";
        } else   if(SubscriptionResponse.SUCCESS == subscriptionResponse){
            return "registrationCharging";
        }  else {
            return "registrationRecursiveCharging";
        }
    }

    private String findStatusCode(Map<String, Object> request) {
        String status = (String) request.get("notification-status-code");
        if (status == null) return KiteErrorBox.unknownErrorCode;
        else if (status.equalsIgnoreCase("SUCCESS")) return KiteErrorBox.successCode;
        else return KiteErrorBox.unknownErrorCode;
        //todo do proper error code mapping after mandate provide proper error mapping
    }

    private Object findSpName(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> sp = requestContext.get(spK);
        if(sp == null) {
            return "";
        }
        return sp.get(coopUserNameK);
    }

    private Object findTransactionStatus(Map<String, Object> request) {
        String notificationStatus = (String) request.get("notification-status-code");
        if ("SUCCESS".equals(notificationStatus)) {
            return "success";
        } else {
            return "failure";
        }
    }

    public static String translateAppState(Object appState) {
        if (appState == null)
            return APP_STATE_INTERMEDIATE;
        if (KiteKeyBox.activeProductionK.equals(appState)) {
            return APP_STATE_LIVE;
        }
        if (KiteKeyBox.limitedProductionK.equals(appState)) {
            return APP_STATE_LIMITED_LIVE;
        }
        return APP_STATE_INTERMEDIATE;
    }

    private Object findAppName(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> app = requestContext.get(appK);
        if (app != null) {
            return app.get(nameK);
        } else {
            return "";
        }
    }

    private void addRecode(StringBuilder logMessage, Object message) {
        if (message == null) {
            message = "";
        }
        logMessage.append(message.toString().replace(delemeter, "_"));
        logMessage.append(delemeter);
    }

    private void addLastRecode(StringBuilder logMessage, Object message) {
        if (message == null) {
            message = "";
        }
        logMessage.append(message.toString().replace(delemeter, "_"));
    }

    public void setDateTimeFormat(String dateTimeFormat) {
        SubscriptionChargingLogChannel.datePattern = dateTimeFormat;
    }
}
