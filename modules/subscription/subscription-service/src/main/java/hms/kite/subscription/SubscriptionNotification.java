/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.subscription;

import hms.kite.subscription.core.NotificationHandler;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.requestK;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SubscriptionNotification implements NotificationHandler {


    @Override
    public Map<String, Object> sendNotification(Map<String, Object> request) {
        Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String, Object>>();
        requestContext.put(requestK, request);

        return null;
    }
}
