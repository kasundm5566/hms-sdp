package hms.kite.subscription.integration.channel;

import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Blank work-flow sink channel. This is use as end point of a workflow which ends with a
 * conditional execution of a real channel. Since if the condition fails, real channel will not be
 * executed and the final status of the work-flow will be in error status. But if we want to change it to
 * success error code can use this channel to be executed once the condition fails.
 *
 */
public class WorkflowSinkChannel implements Channel {
    private static final Logger LOGGER = LoggerFactory.getLogger(WorkflowSinkChannel.class);

    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        LOGGER.debug("Executing blank WorkflowSinkChannel");
        return ResponseBuilder.generateSuccess();
    }

    public Map<String, Object> send(Map<String, Object> request) {
        return null;
    }
}
