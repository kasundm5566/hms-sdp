package hms.kite.subscription.integration.channel;

import com.google.common.base.Optional;
import hms.kite.subscription.SubscriptionAdapter;

import static hms.kite.util.KiteErrorBox.subscriberNotRegisteredCode;
import static hms.kite.util.KiteKeyBox.*;

import hms.kite.subscription.core.domain.SubscriptionNblResponse;
import hms.kite.subscription.core.domain.SubscriptionStatus;
import hms.kite.subscription.core.service.repo.Subscription;
import hms.kite.util.KeyNameSpaceResolver;
import hms.kite.util.SdpException;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.xml.transform.OutputKeys;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteErrorBox.systemErrorCode;
import static hms.kite.wfengine.impl.ResponseBuilder.generate;
import static hms.kite.wfengine.impl.ResponseBuilder.generateSuccess;

/**
 * (C) Copyright 2011-2012 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 * <p/>
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 * <p/>
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
public class SubscriptionCheckChannel implements Channel {

    @Autowired
    SubscriptionAdapter subscriptionAdapter;

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionCheckChannel.class);

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        try {
            return checkForSubscription(requestContext);
        } catch (SdpException e) {
            logger.warn("Unexpected error occurred", e);
            return generate(requestContext, e);
        } catch (Exception e) {
            logger.error("Unexpected error occurred while checking subscription", e);
            return generate(systemErrorCode, false, requestContext);
        }
    }

    private Map<String, Object> checkForSubscription(Map<String, Map<String, Object>> requestContext) {
        logger.debug("Processing received request {}", requestContext);
        String appId = (String) requestContext.get(appK).get(appIdK);
        List<String> msisdnList = getMsisdn(requestContext);
        logger.debug("Found msisdn list for subscription validation[{}]", msisdnList);
        for (String msisdn : msisdnList) {
            msisdn = decodeMsisdn(msisdn);
            Optional<Subscription> subscriber = subscriptionAdapter.getSubscriber(msisdn, appId);
            logger.debug("Found subscriber for appId[{}], msisdn[{}], subscriber [{}]", new Object[]{appId, msisdn, subscriber});
            if (subscriber.isPresent()) {
                Subscription subs = subscriber.get();
                /* This is used in MO flow the indicate which masking algo to be used. Since MO flow only has one msisdn
                   setting the key this way is OK.
                */
                putLegacyMaskingUsedKey2Request(requestContext, subs);
                if (SubscriptionNblResponse.REGISTERED != getSubscriptionStatus(subs)) {
                    return generate(subscriberNotRegisteredCode, false, requestContext);
                }
            } else {
                return generate(subscriberNotRegisteredCode, false, requestContext);
            }


        }
        return generateSuccess(requestContext);
    }

    private void putLegacyMaskingUsedKey2Request(Map<String, Map<String, Object>> requestContext, Subscription subs) {
        Map<String, Object> request = requestContext.get(requestK);
        if(request != null){
            request.put(legacyNumberMaskingUsedK, subs.isUseLegacyMask());
        }
    }

    private SubscriptionNblResponse getSubscriptionStatus(Subscription subs) {
        return SubscriptionNblResponse.mapToSubscriptionStatusNblResponse(SubscriptionStatus.valueOf(subs.getCurrentStatus()));
    }

    private String decodeMsisdn(String msisdn) {
        if (msisdn != null && msisdn.startsWith(telK + ":")) {
            return msisdn.replace(telK + ":", "");
        }
        return msisdn;
    }

    private List<String> getMsisdn(Map<String, Map<String, Object>> requestContext) {
        String direction = (String) requestContext.get(requestK).get(directionK);
        String ncsType = findNcsType(requestContext);

        ArrayList<String> msisdns = new ArrayList<String>();

        if (ncsType.equals(smsK)) {
            if (moK.equals(direction)) {
                msisdns.add((String) requestContext.get(requestK).get(senderAddressK));
                return msisdns;
            } else if (mtK.equals(direction)) {
                List<Map<String, String>> recipients = (List<Map<String, String>>) requestContext.get(requestK).get(recipientsK);
                if (recipients != null) {
                    for (Map<String, String> recipient : recipients) {
                        msisdns.add(recipient.get(recipientAddressK));
                    }
                }
                return msisdns;
            }
        } else if (ncsType.equals(casK)) {
            List<Map<String, String>> recipients = (List<Map<String, String>>) requestContext.get(requestK).get(recipientsK);
            if (recipients != null) {
                for (Map<String, String> recipient : recipients) {
                    msisdns.add(recipient.get(recipientAddressK));
                }
            }
            return msisdns;
        } else if (ncsType.equals(ussdK)) {
            if (moK.equals(direction)) {
                msisdns.add((String) requestContext.get(requestK).get(senderAddressK));
                return msisdns;
            } else if (mtK.equals(direction)) {
                List<Map<String, String>> recipients = (List<Map<String, String>>) requestContext.get(requestK).get(recipientsK);
                if (recipients != null) {
                    for (Map<String, String> recipient : recipients) {
                        msisdns.add(recipient.get(recipientAddressK));
                    }
                }
                return msisdns;
            }
        } else if (ncsType.equals(lbsK)) {
            List<Map<String, String>> recipients = (List<Map<String, String>>) requestContext.get(requestK).get(recipientsK);
            if (recipients != null) {
                for (Map<String, String> recipient : recipients) {
                    msisdns.add(recipient.get(recipientAddressK));
                }
            }
            return msisdns;
        } else {
            logger.warn("MSISDN extraction method not configured for[{}]", ncsType);
        }
        return msisdns;
    }

    private String findNcsType(Map<String, Map<String, Object>> requestContext) {
        String ncsType = (String) requestContext.get(ncsK).get(ncsTypeK);
        if (ncsType == null) {
            ncsType = (String) requestContext.get(requestK).get(ncsTypeK);
        }
        return ncsType;
    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

}
