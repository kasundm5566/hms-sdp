package hms.kite.subscription;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.transport.BaseChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SubscriptionNotificationWfChannel extends BaseChannel {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionNotificationWfChannel.class);

    private Workflow subscriptionNotificationWorkflow;

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        subscriptionNotificationWorkflow.executeWorkflow(requestContext);
        return new HashMap<String, Object>();
    }

    public void setSubscriptionNotificationWorkflow(Workflow subscriptionNotificationWorkflow) {
        this.subscriptionNotificationWorkflow = subscriptionNotificationWorkflow;
    }
}
