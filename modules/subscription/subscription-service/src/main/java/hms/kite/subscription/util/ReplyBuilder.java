/*
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.subscription.util;

import hms.kite.subscription.core.domain.SubscriptionResponse;
import hms.kite.subscription.integration.channel.AllNcsFetcherChannel;
import hms.kite.util.KeyNameSpaceResolver;
import hms.kite.util.SdpException;
import hms.kite.util.StatusDescriptionBuilder;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.wfengine.impl.ResponseBuilder.generateSuccess;

/**
 * Generate reply sms messages to be sent for subscription related actions.
 */
public class ReplyBuilder implements Channel {

    public static final String MSG_COMPONENT_SEPARATOR = ", ";
    public static final String SPACE = " ";
    private Map<SubscriptionResponse, String> unregReplyKeys;
    private Map<SubscriptionResponse, String> regReplyKeys;
    private int operatorSpecificResponseMaxSize;
    private StatusDescriptionBuilder statusDescriptionBuilder;
    private boolean useDetailedChargingResp = false;
    private boolean sendRecursiveChargingNotify = true;

    private static final Logger logger = LoggerFactory.getLogger(ReplyBuilder.class);
    private String systemSenderAddress;

    private SubscriptionResponse mapSubscriptionResponse(Map<String, Map<String, Object>> requestContext, SubscriptionResponse resp) {
        SubscriptionResponse response;
        try {
            response = determineSuccessResponse(retrieveSmsNcsSla(requestContext), resp);
            logger.debug("Subscription Response is mapped from [{}] to [{}]", resp, response);
        } catch (SdpException e) {
            logger.error(e.getErrorDescription());
            response = resp;
        }
        return response;
    }

    private Map<String, Object> retrieveSmsNcsSla(Map<String, Map<String, Object>> requestContext) {
        String operator = (String) requestContext.get(requestK).get(operatorK);
        if (requestContext.get(appK).containsKey(smsK)) {
            return (Map<String, Object>) requestContext.get(appK).get(smsK);
        } else {
            return Collections.emptyMap();
        }
    }

    private SubscriptionResponse determineSuccessResponse(Map<String, Object> smsNcsSla,
                                                          SubscriptionResponse response) {
        if (response == SubscriptionResponse.SUCCESS || response == SubscriptionResponse.SUCCESS_FREE) {
            boolean isSubscriptionFree = response == SubscriptionResponse.SUCCESS_FREE;
            if (isSmsMtAllowed(smsNcsSla)) {
                String mtChargingType = (String) ((Map) ((Map) smsNcsSla.get(mtK)).get(chargingK)).get(typeK);
                if (freeK.equals(mtChargingType) && isSubscriptionFree) {
                    return SubscriptionResponse.SUCCESS_COMPLETELY_FREE;
                } else if (freeK.equals(mtChargingType) && !isSubscriptionFree) {
                    return SubscriptionResponse.SUCCESS;
                } else if (!freeK.equals(mtChargingType) && isSubscriptionFree) {
                    return SubscriptionResponse.SUCCESS_ONLY_PER_MSG_MT_CHARGING;
                } else { //(!freeK.equals(mtChargingType) && !isSubscriptionFree)
                    return SubscriptionResponse.SUCCESS_PER_MESSAGE_SUBSCRIPTION;
                }
            }
        }
        return response;

    }

    private boolean isSmsMtAllowed(Map<String, Object> smsNcsSla) {
        Boolean isMtAllowed = (Boolean) smsNcsSla.get(mtAllowedK);
        return null != isMtAllowed && isMtAllowed;
    }


    public String createGenericResponse(Map<String, Map<String, Object>> requestContext,
                                        SubscriptionResponse resp) {

        List<SubscriptionResponse> successResps = Arrays.asList(new SubscriptionResponse[]{SubscriptionResponse.SUCCESS,
                SubscriptionResponse.SUCCESS_FREE});
        String message = "";
        if (useDetailedChargingResp && successResps.contains(resp)) {
            message = generateDetailedNotification(requestContext);
        } else {
            if (SubscriptionResponse.RECURSIVE_SUCCESS == resp) {
                message = statusDescriptionBuilder.createStatusDescription(regReplyKeys.get(resp), requestContext);
            } else {
                String respMsgKey = regReplyKeys.get(mapSubscriptionResponse(requestContext, resp));
                if(respMsgKey != null){
                    message = statusDescriptionBuilder.createStatusDescription(respMsgKey, requestContext);
                }  else {
                   logger.warn("No reply key found for SubscriptionResponse[{}]", resp);
                }

            }
        }

        logger.debug("Generated charging notification[{}]", message);

        if (resp == SubscriptionResponse.SUCCESS || resp == SubscriptionResponse.SUCCESS_FREE) {
            //First time
            return appendOperatorSpecificMessage(message, requestContext.get(ncsK).get(subscriptionRespMessageK).toString());
        } else {
            return message;
        }
    }

    private String generateDetailedNotification(Map<String, Map<String, Object>> requestContext) {
        boolean isAppCompletelyFree = true;
        if (KeyNameSpaceResolver.contains(requestContext, requestK, AllNcsFetcherChannel.CHARGING_DETAILS, AllNcsFetcherChannel.SUBSCRIPTION_FULL)) {
            String subscriptionMsg = statusDescriptionBuilder.createStatusDescription("subs-detail-success-subscription-charge", requestContext);
            logger.debug("Created Msg component[{}]", subscriptionMsg);
            isAppCompletelyFree = false;
            KeyNameSpaceResolver.putData(requestContext, subscriptionMsg, requestK, AllNcsFetcherChannel.CHARGING_DETAILS, "subscription-charges-msg");
        } else {
            String subscriptionMsg = statusDescriptionBuilder.createStatusDescription("subs-detail-success-subscription-free", requestContext);
            logger.debug("Created Msg component[{}]", subscriptionMsg);
            KeyNameSpaceResolver.putData(requestContext, subscriptionMsg, requestK, AllNcsFetcherChannel.CHARGING_DETAILS, "subscription-charges-msg");
        }

        StringBuilder sb = new StringBuilder(500);

        if (KeyNameSpaceResolver.contains(requestContext, requestK, AllNcsFetcherChannel.CHARGING_DETAILS, AllNcsFetcherChannel.SMS_MO)) {
            String smsMoMsg = statusDescriptionBuilder.createStatusDescription("subs-detail-success-sms-mo-charge", requestContext);
            sb.append(smsMoMsg).append(MSG_COMPONENT_SEPARATOR);
            logger.debug("Created Msg component[{}]", smsMoMsg);
            isAppCompletelyFree = false;
        }

        if (KeyNameSpaceResolver.contains(requestContext, requestK, AllNcsFetcherChannel.CHARGING_DETAILS, AllNcsFetcherChannel.SMS_MT)) {
            String smsMtMsg = statusDescriptionBuilder.createStatusDescription("subs-detail-success-sms-mt-charge", requestContext);
            sb.append(smsMtMsg).append(MSG_COMPONENT_SEPARATOR);
            logger.debug("Created Msg component[{}]", smsMtMsg);
            isAppCompletelyFree = false;
        }

        if (KeyNameSpaceResolver.contains(requestContext, requestK, AllNcsFetcherChannel.CHARGING_DETAILS, AllNcsFetcherChannel.USSD_MO)) {
            String ussdMsg = statusDescriptionBuilder.createStatusDescription("subs-detail-success-ussd-mo-charge", requestContext);
            sb.append(ussdMsg).append(MSG_COMPONENT_SEPARATOR);
            logger.debug("Created Msg component[{}]", ussdMsg);
            isAppCompletelyFree = false;
        }

        if (KeyNameSpaceResolver.contains(requestContext, requestK, AllNcsFetcherChannel.CHARGING_DETAILS, casK)) {
            String casMsg = statusDescriptionBuilder.createStatusDescription("subs-detail-success-caas-charge", requestContext);
            sb.append(casMsg).append(MSG_COMPONENT_SEPARATOR);
            logger.debug("Created Msg component[{}]", casMsg);
            isAppCompletelyFree = false;
        }

        if (isAppCompletelyFree) {
            return statusDescriptionBuilder.createStatusDescription("completely-free-subs-reg-success", requestContext);
        } else {

            String ncsMsg = formatNcsSpecificMsg(sb);

            if (ncsMsg.trim().isEmpty()) {
                KeyNameSpaceResolver.putData(requestContext, "", requestK, AllNcsFetcherChannel.CHARGING_DETAILS,
                        "ncs-charges-msg");
            } else {
                String additionalMsg = statusDescriptionBuilder
                        .createStatusDescription("subs-detail-success-subscription-additional", requestContext) + SPACE;
                KeyNameSpaceResolver.putData(requestContext, additionalMsg + ncsMsg, requestK,
                        AllNcsFetcherChannel.CHARGING_DETAILS, "ncs-charges-msg");
            }

           return statusDescriptionBuilder.createStatusDescription("subs-detail-success-msg", requestContext);
        }
    }

    private String formatNcsSpecificMsg(StringBuilder sb) {
        String ncsMsg = sb.toString();
        if (ncsMsg.endsWith(MSG_COMPONENT_SEPARATOR)) {
            ncsMsg = ncsMsg.substring(0, ncsMsg.length() - MSG_COMPONENT_SEPARATOR.length());
        }
        return ncsMsg;
    }

    public String createRegistrationFailSlaResponse(Map<String, Map<String, Object>> requestContext,
                                                    SubscriptionResponse resp) {
        return statusDescriptionBuilder.createStatusDescription(regReplyKeys.get(resp), requestContext);
    }

    public String createRegistrationFailChargingResponse(Map<String, Map<String, Object>> requestContext,
                                                         SubscriptionResponse resp) {
        return statusDescriptionBuilder.createStatusDescription(regReplyKeys.get(resp), requestContext);
    }

    public String createRegistrationPendingChargingResponse(Map<String, Map<String, Object>> requestContext,
                                                            SubscriptionResponse resp) {
        return statusDescriptionBuilder.createStatusDescription(regReplyKeys.get(resp), requestContext);
    }

    public String createAlreadyResigeredResponse(Map<String, Map<String, Object>> requestContext,
                                                 SubscriptionResponse resp) {
        return statusDescriptionBuilder.createStatusDescription(regReplyKeys.get(resp), requestContext);
    }

    public Object createRegistrationFailSpSlaResponse(Map<String, Map<String, Object>> requestContext,
                                                      SubscriptionResponse resp) {
        return statusDescriptionBuilder.createStatusDescription(regReplyKeys.get(resp), requestContext);
    }

    public String createRegistrationFailBlockedResponse(Map<String, Map<String, Object>> requestContext,
                                                        SubscriptionResponse resp) {
        return statusDescriptionBuilder.createStatusDescription(unregReplyKeys.get(resp), requestContext);
    }

    public String createUnregistrationSuccessResponse(Map<String, Map<String, Object>> requestContext,
                                                      SubscriptionResponse resp) {
        return appendOperatorSpecificMessage(
                statusDescriptionBuilder.createStatusDescription(unregReplyKeys.get(resp), requestContext),
                requestContext.get(ncsK).get(unsubscriptionRespMessageK).toString());
    }

    public String createUnregistrationFailBlokedResponse(Map<String, Map<String, Object>> requestContext,
                                                         SubscriptionResponse resp) {
        return statusDescriptionBuilder.createStatusDescription(unregReplyKeys.get(resp), requestContext);
    }

    public String createNotRegisteredResponse(Map<String, Map<String, Object>> requestContext, SubscriptionResponse resp) {
        return statusDescriptionBuilder.createStatusDescription(unregReplyKeys.get(resp), requestContext);
    }

    public String createUnRegChargingFailureResponse(Map<String, Map<String, Object>> requestContext, SubscriptionResponse resp) {
        return statusDescriptionBuilder.createStatusDescription(unregReplyKeys.get(resp), requestContext);
    }

    private String appendOperatorSpecificMessage(String commonMsg, String operatorMsg) {
        if (operatorMsg != null && operatorMsg.length() <= operatorSpecificResponseMaxSize && !operatorMsg.isEmpty()) {
            commonMsg = commonMsg + SPACE + operatorMsg;
        }
        return commonMsg;
    }

    public void setOperatorSpecificResponseMaxSize(int operatorSpecificResponseMaxSize) {
        this.operatorSpecificResponseMaxSize = operatorSpecificResponseMaxSize;
    }

    public void setUnregReplyKeys(Map<SubscriptionResponse, String> unregReplyKeys) {
        this.unregReplyKeys = unregReplyKeys;
    }

    public void setRegReplyKeys(Map<SubscriptionResponse, String> regReplyKeys) {
        this.regReplyKeys = regReplyKeys;
    }

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        logger.debug("Executing Reply Builder channel for request context: [{}]", requestContext);
        SubscriptionResponse subscriptionResponse = (SubscriptionResponse) requestContext.get(requestK).get(subscriptionRespK);
        logger.debug("Subscription Response Received: [{}]", subscriptionResponse);

        if (canSendNotification(subscriptionResponse)) {
            String message = createGenericResponse(requestContext, subscriptionResponse);
            requestContext.get(requestK).put(sendSubsNotifyK, trueK);
            requestContext.get(requestK).put(messageK, message);
            requestContext.get(requestK).put(senderAddressK, systemSenderAddress);
        } else {
            logger.debug("Not creating notification message for status[{}]", subscriptionResponse);
        }
        return generateSuccess(requestContext);
    }

    private boolean canSendNotification(SubscriptionResponse subsResp) {
        if (subsResp == SubscriptionResponse.RECURSIVE_SUCCESS) {
            return sendRecursiveChargingNotify;
        } else {
            return true;
        }
    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        return null;
    }

    public void setStatusDescriptionBuilder(StatusDescriptionBuilder statusDescriptionBuilder) {
        this.statusDescriptionBuilder = statusDescriptionBuilder;
    }

    public void setSystemSenderAddress(String systemSenderAddress) {
        this.systemSenderAddress = systemSenderAddress;
    }

    public void setUseDetailedChargingResp(boolean useDetailedChargingResp) {
        this.useDetailedChargingResp = useDetailedChargingResp;
    }

    public void setSendRecursiveChargingNotify(boolean sendRecursiveChargingNotify) {
        this.sendRecursiveChargingNotify = sendRecursiveChargingNotify;
    }
}