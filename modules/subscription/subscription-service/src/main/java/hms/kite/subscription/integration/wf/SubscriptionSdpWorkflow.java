/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.subscription.integration.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.ContainsExpression;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.control.InExpression;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.springframework.beans.factory.annotation.Autowired;

import static hms.kite.util.KiteErrorBox.unknownErrorCode;
import static hms.kite.util.KiteErrorBox.subscriptionViaHttpNotAllowedErrorCode;
import static hms.kite.util.KiteKeyBox.allowHttpSubRequestsK;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SubscriptionSdpWorkflow extends WrappedGeneratedWorkflow implements Workflow {

    @Autowired private Channel subscriptionServiceWrapper;
    @Autowired private Channel mnpService;
    @Autowired private Channel whitelistService;
    @Autowired private Channel blacklistingService;
    @Autowired private Channel provNcsSlaChannel;
    @Autowired private Channel provSpSlaChannel;
    @Autowired private Channel provDetailedAppSlaChannel;
    @Autowired private Channel allNcsFetcherChannel;
    @Autowired private Channel subscriptionAtChannel;
    @Autowired private Channel numberMaskingChannel;

    @Override
    protected Workflow generateWorkflow() {

        ServiceImpl subscriptionAtService = new ServiceImpl("subscription-at-channel");
        subscriptionAtService.setChannel(subscriptionAtChannel);

        ServiceImpl numberMaskingChannelService = new ServiceImpl("number-masking-channel", subscriptionAtService);
        numberMaskingChannelService.setChannel(numberMaskingChannel);
        numberMaskingChannelService.addServiceParameter(maskK, true);

        EqualExpression maskingApplied = new EqualExpression(appK, maskNumberK, true);
        Condition maskingCondition = new Condition(maskingApplied, unknownErrorCode);

        ServiceImpl numberMaskingApplied = new ServiceImpl("masking.eligibility.validation", maskingCondition);
        numberMaskingApplied.setOnSuccess(numberMaskingChannelService);
        numberMaskingApplied.onDefaultError(subscriptionAtService);

        ServiceImpl subscriptionChannel = new ServiceImpl("subscription-channel", numberMaskingApplied);
        subscriptionChannel.setChannel(subscriptionServiceWrapper);

        ServiceImpl allNcsFetcher = new ServiceImpl("allNcsFetcherChannel", subscriptionChannel);
        allNcsFetcher.setChannel(allNcsFetcherChannel);

        ServiceImpl numberChecking = new ServiceImpl("number-checking");
        numberChecking.setChannel(mnpService);
        numberChecking.setOnSuccess(allNcsFetcher);

        ServiceImpl whitelist = new ServiceImpl("white-list");
        whitelist.setChannel(whitelistService);
        whitelist.setOnSuccess(numberChecking);

        EqualExpression appStatus = new EqualExpression(appK, statusK, limitedProductionK);
        Condition condition = new Condition(appStatus, unknownErrorCode);

        ServiceImpl whitelistApplied = new ServiceImpl("whitelist.eligibility.service", condition);
        whitelistApplied.setOnSuccess(whitelist);
        whitelistApplied.onDefaultError(numberChecking);

        ServiceImpl blacklist = new ServiceImpl("black-list");
        blacklist.setChannel(blacklistingService);
        blacklist.setOnSuccess(whitelistApplied);

        /*ServiceImpl subscriptionSlaValidation = generateSubscriptionSlaValidation();
        subscriptionSlaValidation.setOnSuccess(blacklist);

        EqualExpression internalHost = new EqualExpression(requestK, internalHostK, true);
        Condition internalHostCondition = new Condition(internalHost, unknownErrorCode);

        ServiceImpl subscriptionValidationEligibility = new ServiceImpl("subscription.validation.eligibility.service",
                internalHostCondition);
        subscriptionValidationEligibility.setOnSuccess(blacklist); */

        ServiceImpl numberUnmaskingService = new ServiceImpl("number-masking-channel", blacklist);
        numberUnmaskingService.setChannel(numberMaskingChannel);
        numberUnmaskingService.addServiceParameter(unmaskK, true);

        EqualExpression maskingAppliedForUnmask = new EqualExpression(appK, maskNumberK, true);
        Condition maskingConditionForUnmask = new Condition(maskingAppliedForUnmask, unknownErrorCode);

        ServiceImpl numberMaskingAppliedForUnmask = new ServiceImpl("masking.eligibility.validation", maskingConditionForUnmask);
        numberMaskingAppliedForUnmask.setOnSuccess(numberUnmaskingService);
        numberMaskingAppliedForUnmask.onDefaultError(blacklist);

        ServiceImpl subscriptionSlaValidation = generateSubscriptionSlaValidation();
        subscriptionSlaValidation.setOnSuccess(numberMaskingAppliedForUnmask);

        EqualExpression internalHost = new EqualExpression(requestK, internalHostK, true);
        Condition internalHostCondition = new Condition(internalHost, unknownErrorCode);

        ServiceImpl subscriptionValidationEligibility = new ServiceImpl("subscription.validation.eligibility.service",
                internalHostCondition);
        subscriptionValidationEligibility.setOnSuccess(blacklist);

        subscriptionValidationEligibility.onDefaultError(subscriptionSlaValidation);

        ServiceImpl ncsSlaChannel = new ServiceImpl("prov.ncs.sla.channel");
        ncsSlaChannel.setChannel(provNcsSlaChannel);
        ncsSlaChannel.setOnSuccess(subscriptionValidationEligibility);

        ServiceImpl spSlaChannel = new ServiceImpl("prov.sp.sla.channel", ncsSlaChannel);
        spSlaChannel.setChannel(provSpSlaChannel);

        ServiceImpl appSlaChannel = new ServiceImpl("prov.detailed.app.sla.channel", spSlaChannel);
        appSlaChannel.setChannel(provDetailedAppSlaChannel);

        ServiceImpl appIdAvailable = new ServiceImpl(new Condition(new ContainsExpression(requestK, appIdK), unknownErrorCode));
        appIdAvailable.setOnSuccess(appSlaChannel);
        appIdAvailable.onDefaultError(numberChecking);

        return new WorkflowImpl(appIdAvailable);
    }

    private ServiceImpl generateSubscriptionSlaValidation() {
        InExpression expression = new InExpression(ncsK, allowHttpSubRequestsK, new Object[]{true});
        Condition condition = new Condition(expression, subscriptionViaHttpNotAllowedErrorCode);

        return new ServiceImpl("subscription.sla.validation", condition);
    }

}
