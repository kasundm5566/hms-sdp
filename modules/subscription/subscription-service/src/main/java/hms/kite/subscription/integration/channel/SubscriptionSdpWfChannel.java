/*
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.subscription.integration.channel;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SubscriptionSdpWfChannel extends BaseChannel {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionSdpWfChannel.class);

    private Workflow subscriptionWf;
    
    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        return executeSubscriptionFlow(requestContext);
    }

    private Map<String, Object> executeSubscriptionFlow(Map<String, Map<String, Object>> requestContext) {

        try {
        	subscriptionWf.executeWorkflow(requestContext);
            return ResponseBuilder.generateResponse(requestContext);
        } catch (Exception e) {
            logger.error("Exception occurred while executing flow", e);
            return ResponseBuilder.generate(requestContext, e);
        }
    }

    public void setSubscriptionWf(Workflow subscriptionWf) {
        this.subscriptionWf = subscriptionWf;
    }
    
}
