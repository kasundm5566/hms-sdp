package hms.kite.subscription.integration.channel;

import hms.common.rest.util.client.AbstractWebClient;
import hms.kite.subscription.SubscriptionAdapter;
import hms.kite.subscription.core.domain.SubscriptionResponse;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.log4j.NDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static hms.kite.util.KeyNameSpaceResolver.data;
import static hms.kite.util.KiteKeyBox.*;

public class SubscriptionAtChannel extends AbstractWebClient implements Channel {
    private static final Logger LOGGER = LoggerFactory.getLogger(SubscriptionAtChannel.class);
    private SubscriptionAdapter subscriptionAdapter;
    private Executor notifiSender = Executors.newFixedThreadPool(2);
    private static final String DATE_TIME_FORMAT = "yyyyMMddhhmmss";

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        LOGGER.debug("Received context[{}]", requestContext);
        String subsResp = getSubscriptionResponseStr(requestContext);
        if (subsResp != null) {

            SubscriptionResponse subscriptionResponse = getSubscriptionResponse(subsResp);

            if (SubscriptionResponse.SUCCESS == subscriptionResponse
                    || SubscriptionResponse.SUCCESS_FREE == subscriptionResponse
                    || subscriptionResponse.SUCCESS_UNREGISTERED_DUE_TO_CHARGING_ERROR == subscriptionResponse) {
                String notifyUrl = getNotificationUrl(requestContext);
                if (null == notifyUrl) {
                    LOGGER.info("Subscription Notification sending URL is not configured.");
                } else {
                    sendNotification(requestContext, notifyUrl);
                }
            } else {
                LOGGER.info("Not sending subscription notification for status[{}]", subsResp);
            }
        }

        return ResponseBuilder.generateSuccess();
    }

    private void sendNotification(Map<String, Map<String, Object>> requestContext, String notifyUrl) {
        final WebClient webClient = createWebClient(notifyUrl);
        final Map<String, Object> atMessage = createMessage(requestContext);
        final String correlationId = (String) requestContext.get(requestK).get(correlationIdK);
        LOGGER.info("Created subscription notification message [{}] to url[{}]", atMessage, webClient.getBaseURI());
        notifiSender.execute(new Runnable() {
            public void run() {
                try {
                    NDC.push(correlationId);
                    LOGGER.info("Sending subscription notification message [{}] to url[{}]", atMessage, webClient.getBaseURI());
                    Response response = webClient.post(atMessage);
                    if (isSuccessHttpResponse(response)) {
                        LOGGER.info("Subscription notification sent successfully. response [{}]", response.getEntity());
                        truncateResponse(response);
                    } else {
                        LOGGER.error("Subscription notification sending failed http status is [{}]", response.getStatus());
                    }

                } catch (Throwable th) {
                    LOGGER.error("Exception occurred while sending subscription notification[" + atMessage + "] to App", th);
                } finally {
                    NDC.pop();
                }
            }
        });
    }

    private SubscriptionResponse getSubscriptionResponse(String subsRespStr) {
        try {
            return SubscriptionResponse.valueOf(subsRespStr);
        } catch (Exception ex) {
            return SubscriptionResponse.INTERNAL_ERROR;
        }
    }

    private String getSubscriptionResponseStr(Map<String, Map<String, Object>> requestContext) {
        Object subsResp = data((Map) requestContext, requestK, subscriptionRespK);
        return subsResp != null ? subsResp.toString() : null;
    }

    private Map<String, Object> createMessage(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> params = new HashMap<String, Object>();
        LOGGER.info("Request on Create Message {}", requestContext.toString());
        String timestamp = new SimpleDateFormat(DATE_TIME_FORMAT).format(Calendar.getInstance().getTime());

        params.put(versionK, "1.0");
        String appId = (String) requestContext.get(appK).get(appIdK);
        params.put(applicationSubIdK, appId);
        LOGGER.info("getChargingType {}", getChargingType(requestContext));

        if (chargingTypeFlatK.equals(getChargingType(requestContext))) {
            params.put(frequencyIdK, getFrequency(requestContext));
        }
        LOGGER.info("Frequency {}", getFrequency(requestContext));

        params.put(timeStampK, timestamp);

        Object direction = data(requestContext, requestK, directionK);
        if (subscriptionRecursiveChargingNotifyK.equals(direction) || aoK.equals(direction)) {
            List<Map<String, String>> recipients = (List<Map<String, String>>) requestContext.get(requestK)
                    .get(recipientsK);
            if(recipients.size() > 0){
                Map<String, String> recipient = recipients.get(0);
                if(recipient.containsKey(recipientAddressMaskedK)){
                    params.put(subscriberSubIdNblK, recipient.get(recipientAddressMaskedK));
                } else {
                    params.put(subscriberSubIdNblK, recipient.get(recipientAddressK));
                }
                params.put(statusK, subscriptionAdapter.findStatus(recipient.get(recipientAddressK), appId));
            }
        } else {

            if (requestContext.get(requestK).containsKey(senderAddressMaskedK)) {
                params.put(subscriberSubIdNblK, (String) requestContext.get(requestK)
                        .get(senderAddressMaskedK));
            } else {
                params.put(subscriberSubIdNblK, (String) requestContext.get(requestK)
                        .get(senderAddressK));
            }
            params.put(statusK, requestContext.get(requestK).get(subscriptionStatusK));
        }

        return params;
    }

    private void truncateResponse(Response response) {
        try {
            readJsonResponse(response);
        } catch (Exception e) {

        }
    }

    private String getNotificationUrl(Map<String, Map<String, Object>> requestContext) {
        return (String) data((Map) requestContext, ncsK, notificationUrlK);
    }

    private String getFrequency(Map<String, Map<String, Object>> requestContext) {
        return (String) data((Map) requestContext, ncsK, chargingK, frequencyIdK);
    }

    private String getChargingType(Map<String, Map<String, Object>> requestContext) {
        return (String) data((Map) requestContext, ncsK, chargingK, typeK);
    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        return null;
    }

    public void setSubscriptionAdapter(SubscriptionAdapter subscriptionAdapter) {
        this.subscriptionAdapter = subscriptionAdapter;
    }
}
