/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.subscription.integration;

import hms.kite.util.NblKeyMapper;
import hms.kite.util.SdpException;
import hms.kite.util.SystemUtil;
import hms.kite.wfengine.ServiceEndpoint;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import java.util.HashMap;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@Produces({"application/json", "application/xml"})
@Consumes({"application/json", "application/xml"})
@Path("/subscription/")
public class SubscriptionServiceEndpoint extends ServiceEndpoint {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionServiceEndpoint.class);

    private Channel nblChannel;
    private Channel sdpChannel;
    private Map<String, String> newNblSubscriptionKeyMapper;
    private Map<String, String> newNblSubscriptionStatusKeyMapper;
    private Map<String, String> newNblSubscriptionBaseSizeKeyMapper;

    @Context
    private org.apache.cxf.jaxrs.ext.MessageContext mc;
    @Context
    private ServletContext sc;

    @Produces({"application/json", "application/xml"})
    @Consumes({"application/json", "application/xml"})
    @POST
    @Path("/reg")
    public Map<String, Object> reg(Map<String, Object> msg, @Context javax.servlet.http.HttpServletRequest request) {
        NblKeyMapper nblKeyMapper = new NblKeyMapper();
        nblKeyMapper.setNewNblApiKeyMapper(newNblSubscriptionKeyMapper);
        Map<String, Object> respNbl;
        try {
            logger.info("Received Request [{}]", msg);
            Map<String, Object> newMsg = nblKeyMapper.convert(msg, true);
            logger.info("Received Converted Request [{}]", newMsg);
            newMsg.put(subscriptionRequestTypeK, regK);
            addHostDetails(newMsg, request);
            logger.debug("Received request from host " + newMsg.get(remotehostK) + " port : " + newMsg.get(portK));
            Map<String, Object> resp = nblChannel.send(newMsg);
            respNbl = nblKeyMapper.convert(resp, false);
            logger.info("Sending response [{}]", resp);

        } catch (SdpException ex) {
            logger.error("An exception occurred: [{}]", ex);
            respNbl = new HashMap<String, Object>();
            respNbl.put(NblKeyMapper.statusCode, ex.getErrorCode());
            respNbl.put(NblKeyMapper.statusDetail, ex.getErrorDescription());
        }
        logger.info("Sending Converted response [{}]", respNbl);
        return respNbl;
    }

    @Produces({"application/json", "application/xml"})
    @Consumes({"application/json", "application/xml"})
    @POST
    @Path("/unreg")
    public Map<String, Object> unreg(Map<String, Object> msg, @Context javax.servlet.http.HttpServletRequest request) {
        NblKeyMapper nblKeyMapper = new NblKeyMapper();
        nblKeyMapper.setNewNblApiKeyMapper(newNblSubscriptionKeyMapper);
        Map<String, Object> respNbl;
        try {
            logger.info("Received Request [{}]", msg);
            Map<String, Object> newMsg = nblKeyMapper.convert(msg, true);
            logger.debug("Received Converted Request [{}]", newMsg);
            newMsg.put(subscriptionRequestTypeK, unregK);
            addHostDetails(newMsg, request);
            logger.debug("Received request from host " + newMsg.get(remotehostK) + " port : " + newMsg.get(portK));
            Map<String, Object> resp = nblChannel.send(newMsg);
            logger.debug("Sending response [{}]", resp);
            respNbl = nblKeyMapper.convert(resp, false);
            logger.debug("Sending response [{}]", resp);

        } catch (SdpException ex) {
            logger.error("An exception occurred: [{}]", ex);
            respNbl = new HashMap<String, Object>();
            respNbl.put(NblKeyMapper.statusCode, ex.getErrorCode());
            respNbl.put(NblKeyMapper.statusDetail, ex.getErrorDescription());
        }
        logger.info("Sending Converted response [{}]", respNbl);
        return respNbl;
    }

    @Produces({"application/json", "application/xml"})
    @Consumes({"application/json", "application/xml"})
    @POST
    @Path("/basesize")
    public Map<String, Object> query(Map<String, Object> msg,
                                     @Context javax.servlet.http.HttpServletRequest request) {
        NblKeyMapper nblKeyMapper = new NblKeyMapper();
        nblKeyMapper.setNewNblApiKeyMapper(newNblSubscriptionBaseSizeKeyMapper);
        Map<String, Object> respNbl;
        try {
            logger.info("Received Request [{}]", msg);
            Map<String, Object> newMsg = nblKeyMapper.convert(msg, true);
            logger.info("Received Converted Request [{}]", newMsg);
            newMsg.put(subscriptionRequestTypeK, baseSizeK);
            addHostDetails(newMsg, request);
            logger.debug("Received request from host " + newMsg.get(remotehostK) + " port : " + newMsg.get(portK));
            Map<String, Object> resp = nblChannel.send(newMsg);
            respNbl = nblKeyMapper.convert(resp, false);
            logger.info("Sending response [{}]", resp);

        } catch (SdpException ex) {
            logger.error("An exception occurred: [{}]", ex);
            respNbl = new HashMap<String, Object>();
            respNbl.put(NblKeyMapper.statusCode, ex.getErrorCode());
            respNbl.put(NblKeyMapper.statusDetail, ex.getErrorDescription());
        }
        logger.info("Sending Converted response [{}]", respNbl);
        return respNbl;
    }

    @Produces({"application/json", "application/xml"})
    @Consumes({"application/json", "application/xml"})
    @POST
    @Path("/applist")
    public Map<String, Object> appList(Map<String, Object> msg,
                                       @Context javax.servlet.http.HttpServletRequest request) {
        logger.info("Received Request [{}]", msg);
        msg.put(subscriptionRequestTypeK, subscribedAppsK);
        addHostDetails(msg, request);
        logger.debug("Received request from host " + msg.get(remotehostK) + " port : " + msg.get(portK));
        Map<String, Object> resp = nblChannel.send(msg);
        logger.info("Sending response [{}]", resp);
        return resp;
    }

    @Produces({"application/json", "application/xml"})
    @Consumes({"application/json", "application/xml"})
    @POST
    @Path("/subscriptionstatus")
    public Map<String, Object> subscriptionStatusList(Map<String, Object> msg,
                                                      @Context javax.servlet.http.HttpServletRequest request) {
        NblKeyMapper nblKeyMapper = new NblKeyMapper();
        nblKeyMapper.setNewNblApiKeyMapper(newNblSubscriptionStatusKeyMapper);
        Map<String, Object> respNbl;
        try {
            logger.info("Received Request [{}]", msg);
            Map<String, Object> newMsg = nblKeyMapper.convert(msg, true);
            logger.info("Received Converted Request [{}]", newMsg);
            newMsg.put(subscriptionRequestTypeK, subscriptionStatusK);
            addHostDetails(newMsg, request);
            logger.debug("Received request from host " + newMsg.get(remotehostK) + " port : " + newMsg.get(portK));
            Map<String, Object> resp = nblChannel.send(newMsg);
            respNbl = nblKeyMapper.convert(resp, false);
            logger.info("Sending response [{}]", resp);

        } catch (SdpException ex) {
            logger.error("An exception occurred: [{}]", ex);
            respNbl = new HashMap<String, Object>();
            respNbl.put(NblKeyMapper.statusCode, ex.getErrorCode());
            respNbl.put(NblKeyMapper.statusDetail, ex.getErrorDescription());
        }
        logger.info("Sending Converted response [{}]", respNbl);
        return respNbl;
    }

    @Produces({"application/json", "application/xml"})
    @Consumes({"application/json", "application/xml"})
    @POST
    @Path("/notification")
    public Map<String, Object> subscriptionNotificationHandler(Map<String, Object> msg,
                                                               @Context javax.servlet.http.HttpServletRequest request) {
        logger.info("Received Request [{}]", msg);
        msg.put(subscriptionRequestTypeK, subscriptionNotificationK);
        addHostDetails(msg, request);
        logger.debug("Received request from host " + msg.get(remotehostK) + " port : " + msg.get(portK));
        Map<String, Object> resp = nblChannel.send(msg);
        logger.info("Sending response [{}]", resp);
        return resp;
    }

    private void addHostDetails(Map<String, Object> msg, HttpServletRequest request) {
        String host = SystemUtil.findHostIp(request);
        if (null != host) {
            msg.put(remotehostK, host);
        }
    }

    public void setNblChannel(Channel nblChannel) {
        this.nblChannel = nblChannel;
    }

    public void setNewNblSubscriptionKeyMapper(Map<String, String> newNblSubscriptionKeyMapper) {
        this.newNblSubscriptionKeyMapper = newNblSubscriptionKeyMapper;
    }

    public void setNewNblSubscriptionStatusKeyMapper(Map<String, String> newNblSubscriptionStatusKeyMapper) {
        this.newNblSubscriptionStatusKeyMapper = newNblSubscriptionStatusKeyMapper;
    }

    public void setNewNblSubscriptionBaseSizeKeyMapper(Map<String, String> newNblSubscriptionBaseSizeKeyMapper) {
        this.newNblSubscriptionBaseSizeKeyMapper = newNblSubscriptionBaseSizeKeyMapper;
    }

    public void setSdpChannel(Channel sdpChannel) {
        this.sdpChannel = sdpChannel;
    }
}
