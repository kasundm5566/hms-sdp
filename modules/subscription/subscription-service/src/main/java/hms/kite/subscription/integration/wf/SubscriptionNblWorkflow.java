/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.subscription.integration.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.ContainsExpression;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.control.InExpression;
import hms.kite.wfengine.impl.ErrorResponseRoutingService;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;


public class SubscriptionNblWorkflow extends WrappedGeneratedWorkflow implements Workflow {

    @Autowired private Channel internalHostResolverChannel;
    @Autowired private Channel subscriptionSdpWfChannel;
    @Autowired private Channel provSpSlaChannel;
    @Autowired private Channel provDetailedAppSlaChannel;

    @Autowired private Channel smsMoReplySenderChannel;
    @Autowired private Channel smsMoReplyMapperChannel;

    @Override
    protected Workflow generateWorkflow() {
        ServiceImpl internalHostResolver = new ServiceImpl("internal-host-resolver-channel");
        internalHostResolver.setChannel(internalHostResolverChannel);
        internalHostResolver.setOnSuccess(generateInternalWf());
        internalHostResolver.onDefaultError(generateExternalWf());

        return new WorkflowImpl(internalHostResolver);
    }

    @Override
    public void executeWorkflow(Map<String, Map<String, Object>> requestContext) {
        super.executeWorkflow(requestContext);
    }

    private ServiceImpl generateInternalWf() {

        ErrorResponseRoutingService replyChannel = new ErrorResponseRoutingService("sms-mo-reply-channel");
        replyChannel.setChannel(smsMoReplySenderChannel);

        ErrorResponseRoutingService replyMapperChannel = new ErrorResponseRoutingService("sms-mo-reply-mapper-channel", replyChannel);
        replyMapperChannel.setChannel(smsMoReplyMapperChannel);

        ServiceImpl subscriptionChannel = new ServiceImpl("subscription-sdp-channel");
        subscriptionChannel.setChannel(subscriptionSdpWfChannel);
        subscriptionChannel.setOnSuccess(replyMapperChannel);

        ServiceImpl appStateValidationService = createAppStateValidationService();
        appStateValidationService.setOnSuccess(subscriptionChannel);

        ServiceImpl spSlaValidation = createSpStateValidation();
        spSlaValidation.setOnSuccess(appStateValidationService);

        ServiceImpl spSlaChannel = new ServiceImpl("prov.sp.sla.channel", spSlaValidation);
        spSlaChannel.setChannel(provSpSlaChannel);

        ServiceImpl appSlaChannel = new ServiceImpl("prov.detailed.app.sla.channel", spSlaChannel);
        appSlaChannel.setChannel(provDetailedAppSlaChannel);

        ServiceImpl appIdAvailable = new ServiceImpl(new Condition(new ContainsExpression(requestK, appIdK), unknownErrorCode));
        appIdAvailable.setOnSuccess(appSlaChannel);
        appIdAvailable.onDefaultError(subscriptionChannel);

        return appIdAvailable;
    }

    private ServiceImpl generateExternalWf() {
        ErrorResponseRoutingService replyChannel = new ErrorResponseRoutingService("sms-mo-reply-channel");
        replyChannel.setChannel(smsMoReplySenderChannel);

        ErrorResponseRoutingService replyMapperChannel = new ErrorResponseRoutingService("sms-mo-reply-mapper-channel", replyChannel);
        replyMapperChannel.setChannel(smsMoReplyMapperChannel);

        ServiceImpl subscriptionChannel = new ServiceImpl("subscription-sdp-channel");
        subscriptionChannel.setChannel(subscriptionSdpWfChannel);
        subscriptionChannel.setOnSuccess(replyMapperChannel);

        ServiceImpl appStateValidationService = createAppStateValidationService();
        appStateValidationService.setOnSuccess(subscriptionChannel);

        ServiceImpl spSlaValidation = createSpStateValidation();
        spSlaValidation.setOnSuccess(appStateValidationService);

        ServiceImpl authenticationService = createAuthenticationService();
        authenticationService.setOnSuccess(spSlaValidation);

        ServiceImpl hostValidation = createHostValidationService();
        hostValidation.setOnSuccess(authenticationService);

        ServiceImpl spSlaChannel = new ServiceImpl("prov.sp.sla.channel", hostValidation);
        spSlaChannel.setChannel(provSpSlaChannel);

        ServiceImpl appSlaChannel = new ServiceImpl("prov.detailed.app.sla.channel", spSlaChannel);
        appSlaChannel.setChannel(provDetailedAppSlaChannel);

        return appSlaChannel;
    }

    private static ServiceImpl createAppStateValidationService() {
        InExpression statusExpression = new InExpression(appK, statusK, new Object[]{limitedProductionK,
                activeProductionK});
        Condition statusCondition = new Condition(statusExpression, authenticationFailedErrorCode);

        return new ServiceImpl("app.state.validation.service", statusCondition);
    }

    private static ServiceImpl createHostValidationService() {
        InExpression hostExpression = new InExpression(requestK, remotehostK, appK, allowedHostsK);
        Condition condition = new Condition(hostExpression, invalidHostIpErrorCode);

        return new ServiceImpl("host.validation.service", condition);
    }

    private static ServiceImpl createAuthenticationService() {
        EqualExpression authenticationExpression = new EqualExpression(requestK, passwordK, appK, passwordK);
        Condition condition = new Condition(authenticationExpression, authenticationFailedErrorCode);

        return new ServiceImpl("authentication.service", condition);
    }

    private static ServiceImpl createSpStateValidation() {
        EqualExpression statusExpression = new EqualExpression(spK, statusK, approvedK);
        Condition statusCondition = new Condition(statusExpression, authenticationFailedErrorCode);

        InExpression ncsAllowedExpression = new InExpression(spK, spSelectedServicesK, new Object[]{smsK});
        Condition ncsAllowedCondition = new Condition(ncsAllowedExpression, ncsNotAllowedErrorCode);

        return new ServiceImpl("sp.state.validation.service", statusCondition, ncsAllowedCondition);
    }
}
