/**
 *   (C) Copyright 2010-2012 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.subscription.integration.channel;

import com.mongodb.BasicDBList;
import hms.kite.util.SdpException;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.ncsRepositoryService;
import static hms.kite.util.KiteErrorBox.systemErrorCode;
import static hms.kite.util.KiteKeyBox.*;


public class AllNcsFetcherChannel implements Channel {
    private static final Logger logger = LoggerFactory.getLogger(AllNcsFetcherChannel.class);
    public static final String USSD_MO = "ussd-mo";
    public static final String USSD_MT = "ussd-mt";
    public static final String USSD_SESSION = "ussd-session";
    public static final String SMS_MO = "sms-mo";
    public static final String SMS_MT = "sms-mt";
    public static final String SUBSCRIPTION_FULL = "subscription-full";
    public static final String SUBSCRIPTION_FREQUENCY = "subscription-frequency";
    public static final String CHARGING_DETAILS = "charging-details";

    private boolean useDetailedChargingResp = false;

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        if (useDetailedChargingResp && isNcsChargingDetailsNeeded(requestContext)) {
            return fetchNcsChargingDetails(requestContext);
        } else {
            logger.debug("Not fetching other NCS information since detailed charging notification is disabled.");
            return ResponseBuilder.generateSuccess();
        }

    }

    private boolean isNcsChargingDetailsNeeded(Map<String, Map<String, Object>> requestContext) {
        return requestContext.containsKey(appK);
    }

    private Map<String, Object> fetchNcsChargingDetails(Map<String, Map<String, Object>> requestContext) {
        try {
            String appId = (String) requestContext.get(requestK).get(appIdK);

            BasicDBList appNcses = (BasicDBList) requestContext.get(appK).get(ncsesK);
            logger.info("Found available NCS list for appId[{}], [{}]", appId, appNcses);
            Map<String, Map<String, Object>> fullNcsList = new HashMap<String, Map<String, Object>>();

            for (Object ncsObj : appNcses) {
                Map<String, String> ncs = (Map<String, String>) ncsObj;
                String ncsOperator = (String) ncs.get(operatorK);
                String ncsType = (String) ncs.get(ncsTypeK);
                Map<String, Object> ncsDetail = findNcsDetails(appId, ncsType, ncsOperator);
                fullNcsList.put(ncsType, ncsDetail);
                logger.trace("Loaded Ncs details appId[{}], ncs[{}], details[{}]", new Object[]{appId, ncs, ncsDetail});
            }

            logger.info("Loaded full list of supported NcsSlas[{}]", fullNcsList);
            requestContext.get(requestK).put(CHARGING_DETAILS, extractNcsChargingDetails(fullNcsList));
            return ResponseBuilder.generateSuccess();

        } catch (SdpException e) {
            logger.warn("Sdp exception occurred", e);
            return ResponseBuilder.generate(requestContext, e);
        } catch (Exception e) {
            logger.error("Unexpected error occurred", e);
            return ResponseBuilder.generate(systemErrorCode, false, requestContext);
        } finally {
            logger.debug("Request Context after executing AllNcsFetcherChannel[{}]", requestContext);
        }
    }

    private Map<String, String> extractNcsChargingDetails(Map<String, Map<String, Object>> fullNcsList) {
        if (fullNcsList != null) {
            Map<String, String> chargingInfo = new HashMap<String, String>();
            for (Map.Entry<String, Map<String, Object>> ncsSla : fullNcsList.entrySet()) {
                if (smsK.equals(ncsSla.getKey())) {
                    Map<String, String> sms = getSmsChargingDetails(ncsSla.getValue());
                    chargingInfo.putAll(sms);
                } else if (ussdK.equals(ncsSla.getKey())) {
                    Map<String, String> ussd = getUssdChargingDetails(ncsSla.getValue());
                    chargingInfo.putAll(ussd);
                } else if (subscriptionK.equals(ncsSla.getKey())) {
                    Map<String, String> subscription = getSubscriptionChargingDetails(ncsSla.getValue());
                    chargingInfo.putAll(subscription);
                } else if (casK.equals(ncsSla.getKey())) {
                    chargingInfo.put(casK, "available");
                }
            }
            logger.info("Extracted charging details[{}]", chargingInfo);

            return chargingInfo;

        } else {
            return Collections.emptyMap();
        }

    }

    private Map<String, String> getSubscriptionChargingDetails(Map<String, Object> ncs) {
        Map<String, String> chargingInfo = new HashMap<String, String>();

        if (!freeK.equals(getData(ncs, chargingK, typeK))) {
            putToMap(chargingInfo, SUBSCRIPTION_FULL, (String) getData(ncs, chargingK, amountK));
            putToMap(chargingInfo, SUBSCRIPTION_FREQUENCY, (String) getData(ncs, chargingK, frequencyK));
        }
        //TODO: Get actual charged amount from mandate notify

        logger.debug("Extracted Subscription charging details[{}]", chargingInfo);
        return chargingInfo;
    }

    private Map<String, String> getUssdChargingDetails(Map<String, Object> ussdNcs) {
        String moChargeType = (String) getData(ussdNcs, moK, chargingK, typeK);
        String mtChargeType = (String) getData(ussdNcs, mtK, chargingK, typeK);
        String sessionChargeType = (String) getData(ussdNcs, sessionK, chargingK, typeK);

        Map<String, String> chargingInfo = new HashMap<String, String>();

        if (!freeK.equals(moChargeType)) {
            putToMap(chargingInfo, USSD_MO, (String) getData(ussdNcs, moK, chargingK, amountK));
        }

        if (!freeK.equals(mtChargeType)) {
            putToMap(chargingInfo, USSD_MT, (String) getData(ussdNcs, mtK, chargingK, amountK));
        }

        if (!freeK.equals(sessionChargeType)) {
            putToMap(chargingInfo, USSD_SESSION, (String) getData(ussdNcs, sessionK, chargingK, amountK));
        }
        logger.debug("Extracted USSD charging details[{}]", chargingInfo);
        return chargingInfo;
    }

    private Map<String, String> getSmsChargingDetails(Map<String, Object> smsNcs) {
        String moChargeType = (String) getData(smsNcs, moK, chargingK, typeK);
        String mtChargeType = (String) getData(smsNcs, mtK, chargingK, typeK);

        Map<String, String> chargingInfo = new HashMap<String, String>();

        if (!freeK.equals(moChargeType)) {
            putToMap(chargingInfo, SMS_MO, (String) getData(smsNcs, moK, chargingK, amountK));
        }

        if (!freeK.equals(mtChargeType)) {
            putToMap(chargingInfo, SMS_MT, (String) getData(smsNcs, mtK, chargingK, amountK));
        }
        logger.debug("Extracted SMS charging details[{}]", chargingInfo);
        return chargingInfo;
    }

    private void putToMap(Map<String, String> map, String key, String value) {
        if (map != null && value != null) {
            map.put(key, value);
        }
    }


    private Object getData(Map data, String... keys) {
        Object innerData = data;
        for (String key : keys) {
            if (innerData != null) {
                innerData = ((Map<String, Object>) innerData).get(key);
            } else {
                return null;
            }

        }
        return innerData;
    }

    private Map<String, Object> findNcsDetails(String appId, String ncs, String ncsOperator) {
        if (ncsOperator == null || ncsOperator.isEmpty()) {
            return findNcs(appId, ncs);
        } else {
            return findNcs(appId, ncs, ncsOperator);
        }
    }


    private Map<String, Object> findNcs(String appId, String ncsType, String recipientOperator) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put(appIdK, appId);
        map.put(ncsTypeK, ncsType);
        map.put(operatorK, recipientOperator);

        return ncsRepositoryService().findOperatorSla(map);
    }

    private Map<String, Object> findNcs(String appId, String ncsType) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put(appIdK, appId);
        map.put(ncsTypeK, ncsType);

        return ncsRepositoryService().findOperatorSla(map);
    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        //no impl needed
        return null;
    }

    public void setUseDetailedChargingResp(boolean useDetailedChargingResp) {
        this.useDetailedChargingResp = useDetailedChargingResp;
    }
}
