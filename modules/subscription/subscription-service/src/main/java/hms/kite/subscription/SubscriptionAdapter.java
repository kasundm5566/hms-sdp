/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.subscription;

import com.google.common.base.Optional;
import hms.kite.subscription.core.NotificationHandler;
import hms.kite.subscription.core.SubscriptionService;
import hms.kite.subscription.core.domain.SubscriptionNblResponse;
import hms.kite.subscription.core.domain.SubscriptionResponse;
import hms.kite.subscription.core.domain.SubscriptionStatus;
import hms.kite.subscription.core.service.repo.Subscription;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.subscription.core.domain.SubscriptionNblResponse.mapToSubscriptionStatusNblResponse;
import static hms.kite.util.KiteKeyBox.*;

public class SubscriptionAdapter implements NotificationHandler {

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionAdapter.class);

    private SubscriptionService subscriptionService;
    private Channel subscriptionNotificationWfChannel;

    public SubscriptionResponse register(Map<String, Map<String, Object>> requestContext) {
        logger.debug("Register mobile-no request received = [{}]", requestContext);
        return subscriptionService.register(requestContext);
    }

    public SubscriptionResponse unRegister(Map<String, Map<String, Object>> requestContext) {
        logger.debug("Un-register mobile-no request received = [{}]", requestContext);
        return subscriptionService.unRegister(requestContext);
    }

    public long baseSize(String appId) {
        logger.debug("Find subscriber base size request received for application = [{}]", appId);
        return subscriptionService.baseSize(appId);
    }

    public List<String> findAppList(String msisdn) {
        logger.debug("Find app list request received for msisdn = [{}]", msisdn);
        return subscriptionService.subscribedApps(msisdn);
    }

    public SubscriptionNblResponse findStatus(String msisdn, String appId) {
        logger.debug("Find subscriber registration status request received for msisdn = [{}], app-id [{}]", msisdn, appId);
        final SubscriptionStatus status = subscriptionService.subscriptionStatus(msisdn, appId);
        return mapToSubscriptionStatusNblResponse(status);
    }

    public Optional<Subscription> getSubscriber(String msisdn, String appId) {
        return subscriptionService.getSubscriber(msisdn, appId);
    }

    public void setSubscriptionService(SubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    public void setSubscriptionNotificationWfChannel(Channel subscriptionNotificationWfChannel) {
        this.subscriptionNotificationWfChannel = subscriptionNotificationWfChannel;
    }

    public void init() {
        this.subscriptionService.setNotificationHandler(this);
    }

    @Override
    public Map<String, Object> sendNotification(Map<String, Object> request) {
        logger.debug("Sending Subscription notification to : [{}]", request);

        request.put(directionK, subscriptionRecursiveChargingNotifyK);
        Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String, Object>>();
        requestContext.put(requestK, request);
        return subscriptionNotificationWfChannel.execute(requestContext);
    }
}