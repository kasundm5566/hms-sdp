package hms.kite.subscription;

import hms.kite.subscription.integration.channel.WorkflowSinkChannel;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.impl.ErrorResponseRoutingService;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static hms.kite.util.KiteErrorBox.unknownErrorCode;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SubscriptionNotificationWf  extends WrappedGeneratedWorkflow implements Workflow  {

    @Autowired private Channel subscriptionReplyBuilder;
    @Autowired private Channel provNcsSlaChannel;
    @Autowired private Channel provSpSlaChannel;
    @Autowired private Channel provDetailedAppSlaChannel;
    @Autowired private Channel allNcsFetcherChannel;
    @Autowired private Channel subsChargingNotifyChannel;
    @Autowired private Channel subscriptionAtChannel;
    @Autowired private Channel numberMaskingChannel;
    private Channel workflowSinkChannel = new WorkflowSinkChannel();

    private static final Logger logger = LoggerFactory.getLogger(SubscriptionNotificationWf.class);

    @Override
    protected Workflow generateWorkflow() {
        ServiceImpl workflowSinkService =    new ServiceImpl("subs-wf-sink");
        workflowSinkService.setChannel(workflowSinkChannel);

        ServiceImpl subsChargingNotifyService = new ServiceImpl("subs-charging-notify-channel");
        subsChargingNotifyService.setChannel(subsChargingNotifyChannel);

        EqualExpression notificationSendingValidator = new EqualExpression(requestK, sendSubsNotifyK, trueK);
        Condition condition = new Condition(notificationSendingValidator, unknownErrorCode);

        ServiceImpl notificationSender = new ServiceImpl("subscription-notify-send-validator", condition);
        notificationSender.setOnSuccess(subsChargingNotifyService);
        notificationSender.onDefaultError(workflowSinkService);

        ServiceImpl subscriptionAtService = new ServiceImpl("subscription-at-channel", notificationSender);
        subscriptionAtService.setChannel(subscriptionAtChannel);

        ServiceImpl numberMaskingChannelService = new ServiceImpl("number-masking-channel", subscriptionAtService);
        numberMaskingChannelService.setChannel(numberMaskingChannel);
        numberMaskingChannelService.addServiceParameter(maskK, true);

        EqualExpression maskingApplied = new EqualExpression(appK, maskNumberK, true);
        Condition maskingCondition = new Condition(maskingApplied, unknownErrorCode);

        ServiceImpl numberMaskingApplied = new ServiceImpl("masking.eligibility.validation", maskingCondition);
        numberMaskingApplied.setOnSuccess(numberMaskingChannelService);
        numberMaskingApplied.onDefaultError(subscriptionAtService);


        ServiceImpl replyBuilder = new ServiceImpl("subscription-reply-builder", numberMaskingApplied);
        replyBuilder.setChannel(subscriptionReplyBuilder);

        ServiceImpl allNcsFetcher = new ServiceImpl("allNcsFetcherChannel", replyBuilder);
        allNcsFetcher.setChannel(allNcsFetcherChannel);

        ServiceImpl ncsSlaChannel = new ServiceImpl("prov.ncs.sla.channel", allNcsFetcher);
        ncsSlaChannel.setChannel(provNcsSlaChannel);

        ServiceImpl spSlaChannel = new ServiceImpl("prov.sp.sla.channel", ncsSlaChannel);
        spSlaChannel.setChannel(provSpSlaChannel);

        ServiceImpl appSlaChannel = new ServiceImpl("prov.detailed.app.sla.channel", spSlaChannel);
        appSlaChannel.setChannel(provDetailedAppSlaChannel);

        /*ServiceImpl spSlaChannel = new ServiceImpl("prov.sp.sla.channel", appSlaChannel);
        spSlaChannel.setChannelName("prov.sp.sla.channel");*/

        return new WorkflowImpl(appSlaChannel);
    }

    @Override
    public void executeWorkflow(Map<String, Map<String, Object>> requestContext) {
        super.executeWorkflow(requestContext);
    }

}
