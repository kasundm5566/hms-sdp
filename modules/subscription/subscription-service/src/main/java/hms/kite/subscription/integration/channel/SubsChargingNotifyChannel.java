/**
 *   (C) Copyright 2010-2012 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.subscription.integration.channel;

import hms.kite.util.KeyNameSpaceResolver;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

public class SubsChargingNotifyChannel implements Channel {
    private static final Logger LOGGER = LoggerFactory.getLogger(SubsChargingNotifyChannel.class);
    private Channel smsMtChannel;

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        Object sendNotify = KeyNameSpaceResolver.data(requestContext, requestK, sendSubsNotifyK);
        if (sendNotify != null && trueK.equals(sendNotify)) {
            LOGGER.info("Send charging notification");

            //notification-status-code
            Map<String, Object> smsMtResp = smsMtChannel.execute(requestContext);
            Map<String, Object> flowResp = new HashMap<String, Object>();
            flowResp.put(statusCodeK, KeyNameSpaceResolver.data(requestContext, requestK, "notification-status-code"));
            flowResp.put(statusDescriptionK, String.valueOf(KeyNameSpaceResolver.data(requestContext, requestK, "subscription-response")));
            LOGGER.info("Response of charging notification SmsMt sending[{}], returning subscription response as final status[{}]", smsMtResp, flowResp);
            return ResponseBuilder.generate(flowResp);

        } else {
            LOGGER.info("Not sending charging notification.");
            return ResponseBuilder.generateSuccess();
        }
    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        //no impl needed
        return null;
    }

    public void setSmsMtChannel(Channel smsMtChannel) {
        this.smsMtChannel = smsMtChannel;
    }
}
