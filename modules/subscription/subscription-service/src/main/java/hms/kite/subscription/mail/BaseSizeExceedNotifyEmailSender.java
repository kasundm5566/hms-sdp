/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.subscription.mail;

import static hms.kite.subscription.util.MessageFormatter.formatMessage;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.KiteKeyBox.spK;
import static hms.kite.util.KiteKeyBox.spNameK;
import static hms.common.registration.api.util.RestApiKeys.EMAIL;
import hms.common.registration.api.response.BasicUserResponseMessage;
import hms.kite.subscription.core.domain.SubscriptionResponse;
import hms.kite.util.EmailClient;
import hms.kite.util.RegistrationClient;

import java.util.HashMap;
import java.util.Map;

import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.velocity.VelocityEngineUtils;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */
public class BaseSizeExceedNotifyEmailSender {

	private EmailClient emailClient;
	private String fromAddress;
	private String subject;
	private VelocityEngine velocityEngine;
	private String templateLocation;
	private RegistrationClient registrationClient;
	
	private static final String SP_NAME_KEY = "sp_name";
	private static final String SLA_TYPE_KEY = "app_or_sp";
	private static final Logger LOGGER = LoggerFactory.getLogger(BaseSizeExceedNotifyEmailSender.class);
	
	public void sendMail(Map<String, Map<String, Object>> requestContext, SubscriptionResponse resp) { // TODO: testing with actual
		String slaType = getAppOrSp(resp);
		Map<String, String> replacements = new HashMap<String, String>();
		replacements.put(SP_NAME_KEY, requestContext.get(spK).get(spNameK).toString());
		replacements.put(SLA_TYPE_KEY, slaType);
		BasicUserResponseMessage user = registrationClient.getUserInfoByUserId(requestContext.get(spK).get(coopUserIdK).toString());
		emailClient.send((String) requestContext.get(spK).get(user.getAdditionalData(EMAIL)), 
				fromAddress, null, createSubject(slaType), createText(replacements));
	}
	
	private String createText(Map<String, String> replacements) {
		LOGGER.debug("creating text with [{}]", replacements);
		return VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, templateLocation, replacements);
	}

	private String createSubject(String slaType) {
		return formatMessage(subject, new Object[]{slaType});
	}
	
	private String getAppOrSp(SubscriptionResponse resp) {
		if(resp.equals(SubscriptionResponse.SLA_LIMITATION_ERROR)) {
			return appK.toUpperCase();
		} else if(resp.equals(SubscriptionResponse.SP_SLA_LIMITATION_ERROR)) {
			return spK.toUpperCase();
		}
		LOGGER.warn("Unexpected response [{}]", resp.name());
		return null;
	}
	
	public void setEmailClient(EmailClient emailClient) {
		this.emailClient = emailClient;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setVelocityEngine(VelocityEngine velocityEngine) {
		this.velocityEngine = velocityEngine;
	}

	public void setTemplateLocation(String templateLocation) {
		this.templateLocation = templateLocation;
	}

	public void setRegistrationClient(RegistrationClient registrationClient) {
		this.registrationClient = registrationClient;
	}
	
}
