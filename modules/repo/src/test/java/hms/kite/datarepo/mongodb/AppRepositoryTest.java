/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/
package hms.kite.datarepo.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoException;
import hms.kite.datarepo.RepositoryServiceRegistry;
import hms.kite.datarepo.impl.AppRepositoryServiceImpl;
import hms.kite.datarepo.impl.SpRepositoryServiceImpl;
import hms.kite.util.SdpException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.appRepositoryService;
import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.util.KiteKeyBox.*;
import static org.testng.AssertJUnit.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@ContextConfiguration(locations = {"classpath:beans-test.xml"})
public class AppRepositoryTest extends AbstractTestNGSpringContextTests {

    @Resource
    @Qualifier("repo.mongo.template")
    private MongoTemplate mongoTemplate;

    @BeforeClass
    public void cleanCollection() {
        ((AppRepositoryServiceImpl) appRepositoryService()).deleteAll();
        ((SpRepositoryServiceImpl) spRepositoryService()).deleteAll();
        HashMap<String, Object> sp = new HashMap<String, Object>();
        sp.put(spIdK, "SP_101");
        sp.put(nameK, "SPP_123_12345");
        sp.put(coopUserIdK,"SP_101");
        spRepositoryService().create(sp);

        HashMap<String, Object> sp1 = new HashMap<String, Object>();
        sp1.put(spIdK, "NEW_SP_ID");
        sp1.put(nameK, "NEW_SP_ID");
        sp1.put(coopUserIdK,"NEW_SP_ID");
        spRepositoryService().create(sp1);

    }

    @AfterClass
    public void tearDown() {
        ((AppRepositoryServiceImpl) appRepositoryService()).deleteAll();
        ((SpRepositoryServiceImpl) spRepositoryService()).deleteAll();
    }

    @BeforeMethod
    public void cleanCollectionCountAppsInaRange(){
        try {
            ((AppRepositoryServiceImpl) appRepositoryService()).deleteAll();
        } catch (Exception e) {
            logger.warn("Exception in before test", e);
        }
    }

    @Test
    public void findStatusForAvailableApp() {
        HashMap<String, Object> app = new HashMap<String, Object>();
        app.put(statusK, draftK);
        app.put(appIdK, "APP_1234");
        app.put(nameK, "KITE_1");
        app.put(spIdK, "SP_101");
        app.put(revenueShareK,10);
        appRepositoryService().create(app);
        String appStatus = appRepositoryService().findAppStatus("APP_1234");

        assertEquals(draftK, appStatus);
    }

    @Test
    public void findAppById() {
        HashMap<String, Object> app = new HashMap<String, Object>();
        app.put(statusK, draftK);
        app.put(appIdK, "APP_123");
        app.put(nameK, "KITE_2");
        app.put(spIdK, "SP_101");
        app.put(revenueShareK,10);
        appRepositoryService().create(app);
        Map<String, Object> resultMap = appRepositoryService().findById("APP_123");

        assertEquals(app, resultMap);
    }

    @Test
    public void findAppByName() {
        HashMap<String, Object> app = new HashMap<String, Object>();
        app.put(statusK, draftK);
        app.put(appIdK, "APP_12309");
        app.put(nameK, "KITE_KITE");
        app.put(spIdK, "SP_101");
        app.put(revenueShareK,10);
        appRepositoryService().create(app);
        Map<String, Object> resultMap = appRepositoryService().findByAppName("KITE_KITE");

        assertEquals(app, resultMap);
    }

    @Test
    public void findAppBySpId() {
        HashMap<String, Object> app = new HashMap<String, Object>();
        app.put(statusK, draftK);
        app.put(appIdK, "APP_321");
        app.put(spIdK, "SP_101");
        app.put(appTypeK, "MCHOICE_KITE");
        app.put(nameK, "KITE_3");
        app.put(revenueShareK,10);
        appRepositoryService().create(app);
        List<Map<String, Object>> resultList = appRepositoryService().findBySpId("SP_101", "MCHOICE_KITE");

        assertEquals(resultList.contains(app), true);
    }

    @Test
    public void findAPpByAppId() {
        HashMap<String, Object> app = new HashMap<String, Object>();
        app.put(statusK, draftK);
        app.put(appIdK, "APP_124");
        app.put(spIdK, "SP_101");
        app.put(appTypeK, "MCHOICE_KITE");
        app.put(nameK, "KITE_4");
        app.put(revenueShareK,10);
        appRepositoryService().create(app);
        Map<String, Object> resultMap = appRepositoryService().findByAppId("APP_124");

        assertEquals(app, resultMap);
    }

    @Test
    public void findAppByCoopId() {
        HashMap<String, Object> app = new HashMap<String, Object>();
        app.put(statusK, draftK);
        app.put(appIdK, "APP_432");
        app.put(coopUserIdK, "COOP_1231");
        app.put(appTypeK, "MCHOICE_KITE");
        app.put(spIdK, "SP_101");
        app.put(nameK, "KITE_5");
        app.put(revenueShareK,10);
        appRepositoryService().create(app);
        List<Map<String, Object>> resultList = appRepositoryService().findByCoopUserId("COOP_1231", "MCHOICE_KITE");

        assertEquals(resultList.contains(app), true);
    }

    @Test
    public void findAppStatusById() {
        HashMap<String, Object> app = new HashMap<String, Object>();
        app.put(statusK, draftK);
        app.put(appIdK, "APP_14");
        app.put(appTypeK, "MCHOICE_KITE");
        app.put(nameK, "KITE_6");
        app.put(spIdK, "SP_101");
        app.put(revenueShareK,10);
        appRepositoryService().create(app);
        List<Map<String, Object>> resultList = appRepositoryService().findAppsByStatus(draftK);

        assertEquals(resultList.contains(app), true);
    }

    @Test
    public void findAppNullStatusById() {
        HashMap<String, Object> app = new HashMap<String, Object>();
        app.put(appIdK, "APP_14_654");
        app.put(appTypeK, "MCHOICE_KITE");
        app.put(nameK, "KITE_6_9845");
        app.put(spIdK, "SP_101");
        app.put(revenueShareK,10);
        appRepositoryService().create(app);
        List<Map<String, Object>> resultList = appRepositoryService().findAppsByStatus("NULL_IF");

        assertEquals(0, resultList.size());
    }

    @Test
    public void updateAppStatusById() {
        HashMap<String, Object> app = new HashMap<String, Object>();
        app.put(appIdK, "APP_456");
        app.put(statusK, draftK);
        app.put(nameK, "KITE_7");
        app.put(spIdK, "SP_101");
        app.put(revenueShareK,10);
        appRepositoryService().create(app);
        Map<String, Object> app1 = appRepositoryService().findByAppId("APP_456");
        app1.put(statusK, "NEW_STATUS");
        appRepositoryService().update(app1);
        Map<String, Object> app2 = appRepositoryService().findByAppId("APP_456");

        assertEquals(app2.containsValue("NEW_STATUS"), true);
    }

    @Test
    public void updateAppCoopId() {
        HashMap<String, Object> app = new HashMap<String, Object>();
        app.put(appIdK, "APP_4561");
        app.put(statusK, draftK);
        app.put(coopUserIdK, "COOP_1231");
        app.put(nameK, "KITE_8");
        app.put(spIdK, "SP_101");
        app.put(revenueShareK,10);
        appRepositoryService().create(app);
        Map<String, Object> app1 = appRepositoryService().findByAppId("APP_4561");
        app1.put(coopUserIdK, "COOP_NEW_ID");
        appRepositoryService().update(app1);
        Map<String, Object> app2 = appRepositoryService().findByAppId("APP_4561");

        assertEquals(app2.containsValue("COOP_1231"), false);
    }

    @Test
    public void updateAppSpId() {
        HashMap<String, Object> app = new HashMap<String, Object>();
        app.put(appIdK, "APP_4562");
        app.put(statusK, draftK);
        app.put(coopUserIdK, "COOP_1231");
        app.put(spIdK, "SP_101");
        app.put(nameK, "KITE_9");
        app.put(revenueShareK,10);
        appRepositoryService().create(app);
        Map<String, Object> app1 = appRepositoryService().findByAppId("APP_4562");
        app1.put(spIdK, "NEW_SP_ID");
        appRepositoryService().update(app1);
        Map<String, Object> app2 = appRepositoryService().findByAppId("APP_4562");

        assertEquals(app2.containsValue("SP_101"), false);
    }

    @Test(expectedExceptions = {SdpException.class})
    public void deleteAppById() {
        HashMap<String, Object> app = new HashMap<String, Object>();
        app.put(appIdK, "APP_456223");
        app.put(statusK, draftK);
        app.put(coopUserIdK, "COOP_1231");
        app.put(spIdK, "SP_101");
        app.put(nameK, "KITE_10");
        app.put(revenueShareK,10);
        appRepositoryService().create(app);
        Map<String, Object> app1 = appRepositoryService().findByAppId("APP_456223");

        assertEquals(app1, app);

        appRepositoryService().delete("APP_456223");
        appRepositoryService().findByAppId("APP_456223");
    }

    @Test
    public void countApps() {
        int count = 5;

        for (int i = 0; i < count; i++) {
            HashMap<String, Object> app = new HashMap<String, Object>();
            app.put(appIdK, "APPP_" + i);
            app.put(statusK, draftK);
            app.put(coopUserIdK, "COOP_1231");
            app.put(spIdK, "SP_101");
            app.put(appTypeK, "KITE");
            app.put(nameK, "KITE_11" + i);
            app.put(revenueShareK,10);
            appRepositoryService().create(app);
        }

        List<Map<String, Object>> listOfApps = appRepositoryService().findBySpId("SP_101", "KITE");

        assertEquals(listOfApps.size(), count);
    }


    @Test
    public void testNullAppExistence() {
        assertFalse(appRepositoryService().isAppNameExists("AppName"));

        BasicDBObject app = new BasicDBObject();
        app.put(statusK, draftK);
        app.put(idK, "APP_12345");
        app.put(appIdK, "APP_12345");
        app.put(nameK, "AppName");
        app.put(spIdK, "SP_101");
        mongoTemplate.getCollection("app").save(app);

        assertTrue(appRepositoryService().isAppNameExists("appname"));
        assertTrue(appRepositoryService().isAppNameExists("AppName"));

        mongoTemplate.getCollection("app").remove(app);

        app.put(idK, "APP_123456");
        app.put(appIdK, "APP_123456");
        app.put(nameK, "1AppName1");

        mongoTemplate.getCollection("app").save(app);

        assertFalse(appRepositoryService().isAppNameExists("appname"));
        assertFalse(appRepositoryService().isAppNameExists("AppName"));
    }


    @Test
    public void countAppsInaRange() {
        int count = 10, skip = 3, limit = 5;

        for (int i = 0; i < count; i++) {
            HashMap<String, Object> app = new HashMap<String, Object>();
            app.put(appIdK, "APPlica_" + i);
            app.put(statusK, draftK);
            app.put(coopUserIdK, "COOP_1231");
            app.put(spIdK, "SP_101");
            app.put(appTypeK, "KITE");
            app.put(nameK, "KITE_111" + i);
            app.put(revenueShareK, 10);
            appRepositoryService().create(app);
        }

        HashMap<String, Object> query = new HashMap<String, Object>();
        query.put(spIdK, "SP_101");
        query.put(appTypeK, "KITE");
        List<Map<String, Object>> listOfApps = appRepositoryService().findAppsRange(query, skip, limit);
        assertEquals(listOfApps.get(0).containsValue("APPlica_3"), true);
        assertEquals(listOfApps.size(), limit);
    }

    @Test(expectedExceptions = {MongoException.class})
    public void countAppsInaInvalidRange() {
        HashMap<String, Object> query = new HashMap<String, Object>();
        query.put(spIdK, "SP_101");
        query.put(appTypeK, "KITE");
        appRepositoryService().findAppsRange(query, -1, 5);
    }

    @Test
    public void countAppsInaInvalidRange2() {
        int limit = -3;
        HashMap<String, Object> query = new HashMap<String, Object>();
        query.put(spIdK, "SP_101");
        query.put(appTypeK, "KITE");
        appRepositoryService().findAppsRange(query, 1, limit);
    }

    @Test
    public void searchApp() {
        HashMap<String, Object> app = new HashMap<String, Object>();
        app.put(statusK, draftK);
        app.put(appIdK, "APP_1245309");
        app.put(nameK, "KITE_KITE_KITE");
        app.put(spIdK, "SP_101");
        app.put(revenueShareK,10);
        appRepositoryService().create(app);
        List<Map<String, Object>> resultMap = appRepositoryService().search("KITE_KITE_KITE");

        assertEquals(1, resultMap.size());
    }

    @Test(expectedExceptions = {IllegalStateException.class})
    public void testIfAppNameIsNull() {
        HashMap<String, Object> app = new HashMap<String, Object>();
        app.put(statusK, draftK);
        app.put(appIdK, "APP_1245309");
        appRepositoryService().create(app);
    }

    @Test(expectedExceptions = {IllegalStateException.class})
    public void testIfAppIdIsNull() {
        HashMap<String, Object> app = new HashMap<String, Object>();
        appRepositoryService().create(app);
    }

    @Test(expectedExceptions = {SdpException.class})
    public void duplicateAppName() {
        for (int i = 0; i < 2; i++) {
            HashMap<String, Object> app = new HashMap<String, Object>();
            app.put(appIdK, "APPPP_" + i);
            app.put(nameK, "KITE_111");
            app.put(revenueShareK,10);
            app.put(spIdK,"SPP_000100");
            appRepositoryService().create(app);
        }
    }

    @Test(expectedExceptions = {SdpException.class})
    public void duplicateAppId() {
        for (int i = 0; i < 2; i++) {
            HashMap<String, Object> app = new HashMap<String, Object>();
            app.put(appIdK, "APPPP_0");
            app.put(nameK, "KITE_12324" + i);
            app.put(revenueShareK,10);
            app.put(spIdK,"SPP_00011");
            appRepositoryService().create(app);
        }
    }

    @Test(expectedExceptions = {SdpException.class})
    public void findStatusForNotAvailableApp() {
        appRepositoryService().findAppStatus("this app is not there");
    }

    @Test(expectedExceptions = {SdpException.class})
    public void findNonExistingAppId() {
        appRepositoryService().findByAppId("is this an app id");
    }

    @Test(expectedExceptions = {SdpException.class})
    public void findNonExistingAppById() {
        appRepositoryService().findById("is this an app id");
    }

    @Test(expectedExceptions = {SdpException.class})
    public void findNonExistingAppByStatus() {
        appRepositoryService().findAppStatus("whatever app status");
    }
}
