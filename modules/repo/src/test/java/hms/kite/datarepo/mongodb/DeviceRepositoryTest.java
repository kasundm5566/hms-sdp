/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/
package hms.kite.datarepo.mongodb;

import com.mongodb.BasicDBObject;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.annotation.Resource;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.deviceAndPlatformRepositoryService;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@ContextConfiguration(locations = {"classpath:beans-test.xml"})
public class DeviceRepositoryTest extends AbstractTestNGSpringContextTests {

    @Resource(name = "repo.mongo.template")
    MongoTemplate mongoTemplate;

    private Map<String, Object> deviceInfoMap;

    @BeforeMethod
    public void beforeTest() {
        mongoTemplate.getCollection("device").drop();
        deviceInfoMap = new LinkedHashMap<String, Object>();
        List<String> platformVersions = new LinkedList<String>();
        platformVersions.add("S60");
        platformVersions.add("S40");
        platformVersions.add("S80");

        deviceInfoMap.put("brand", "Nokia");
        deviceInfoMap.put("model", "Nokia 5800");
        deviceInfoMap.put("platform", "symbian");
        deviceInfoMap.put("platform-versions", platformVersions);
    }

    @Test
    public void testFindAll() {
        mongoTemplate.getCollection("device").save(new BasicDBObject(deviceInfoMap));

        List<Map<String, Object>> allDevicesList = deviceAndPlatformRepositoryService().findAllDevices();

        assertTrue(allDevicesList.size() == 1);

        Map<String, Object> currentDevInfoMap = allDevicesList.get(0);
        currentDevInfoMap.remove("_id");

        assertEquals(deviceInfoMap, currentDevInfoMap);
    }

    @AfterTest
    public void tearDown() {
        mongoTemplate.getCollection("device").drop();
    }
}
