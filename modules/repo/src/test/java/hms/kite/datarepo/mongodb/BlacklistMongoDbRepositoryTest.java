/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo.mongodb;

import javax.annotation.Resource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@ContextConfiguration(locations = {"classpath:beans-test.xml"})
public class BlacklistMongoDbRepositoryTest {

    @Resource
    private BlacklistMongoDbRepository blacklistMongoDbRepository;

    @BeforeClass
    public void setup() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext(new String[]{"beans-test.xml"});
        blacklistMongoDbRepository = (BlacklistMongoDbRepository) ctx.getBean("repo.mongo.blacklistMongoDbRepository");
    }


    @BeforeTest
    public void setUp() {
        // Add your code here
    }

    @AfterTest
    public void tearDown() {
        System.out.println("printiing blacklist");
        blacklistMongoDbRepository.removeAllBlacklists();
        System.out.println("After executing tear down");


    }

    @Test
    public void testIsExist() {
        String bl1 = "(11\\d\\d\\d\\d\\d\\d\\d\\d\\d\\d)|" + "(131\\d\\d\\d\\d\\d\\d\\d\\d\\d)|" + "(132\\d\\d\\d\\d\\d\\d\\d\\d\\d)|";
        String bl2 = "(133\\d\\d\\d\\d\\d\\d\\d\\d\\d)|" + "(134\\d\\d\\d\\d\\d\\d\\d\\d\\d)|" + "(1351\\d\\d\\d\\d\\d\\d\\d\\d)|" + "(1351\\d\\d\\d\\d\\d\\d\\d\\d\\d\\d)|";
        blacklistMongoDbRepository.createBlacklist("bl1", bl1);
        blacklistMongoDbRepository.createBlacklist("bl2", bl2);
        String[] randamNonBlackListedNos = {"710000000001", "831000000001", "932000000001", "140000000000", "149999999999", "231000000000", "231999999999", "1872000000000", "1329999999999", "122210000000009", "139091999999990"};

        for (String randamNonBlackListedNo : randamNonBlackListedNos) {
            Assert.assertFalse(blacklistMongoDbRepository.isExist("bl1", randamNonBlackListedNo));
            Assert.assertFalse(blacklistMongoDbRepository.isExist("bl2", randamNonBlackListedNo));
        }

        String[] randamBlackListedNosForBl1 = {"110000000000", "119999999999", "131000000000", "131999999999", "132000000000", "132999999999", "110000000009",
                "131999999990", "132000000600"};
        String[] randamBlackListedNosForBl2 = {"133000000000", "133999999999", "134000000000", "134999999999", "135100000000", "135199999999", "135100000000",
                "135199999999", "133997699999"};

        for (String randamBlackListedNo : randamBlackListedNosForBl1) {
            Assert.assertTrue(blacklistMongoDbRepository.isExist("bl1", randamBlackListedNo));
            Assert.assertFalse(blacklistMongoDbRepository.isExist("bl2", randamBlackListedNo));
        }

        for (String randamBlackListedNo : randamBlackListedNosForBl2) {
            Assert.assertFalse(blacklistMongoDbRepository.isExist("bl1", randamBlackListedNo));
            Assert.assertTrue(blacklistMongoDbRepository.isExist("bl2", randamBlackListedNo));
        }
    }
}
