/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo.mongodb;

import static hms.kite.datarepo.mongodb.RoutingKeyMongoDBRepository.RK_COLLECTION;
import static hms.kite.datarepo.mongodb.RoutingKeyMongoDBRepository.TERMINATED_RK_COLLECTION;
import static hms.kite.util.KiteKeyBox.exclusiveK;
import static hms.kite.util.KiteKeyBox.keywordK;
import static hms.kite.util.KiteKeyBox.ncsTypeK;
import static hms.kite.util.KiteKeyBox.operatorK;
import static hms.kite.util.KiteKeyBox.shortcodeK;
import static hms.kite.util.KiteKeyBox.smsK;
import static hms.kite.util.KiteKeyBox.spIdK;
import static hms.kite.util.KiteKeyBox.ussdK;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;
import hms.kite.util.SdpException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.junit.Ignore;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.mongodb.BasicDBObject;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@ContextConfiguration(locations = {"classpath:beans-test.xml"})
public class RoutingkeyMongoDBRepositoryTest extends AbstractTestNGSpringContextTests {

    @Resource(name = "repo.mongo.smsRoutingKeyRepository")
    private RoutingKeyMongoDBRepository smsRoutingkeyMongoDBRepository;

    @Resource(name = "repo.mongo.ussdRoutingKeyRepository")
    private RoutingKeyMongoDBRepository ussdRoutingkeyMongoDBRepository;

    @Resource(name = "repo.mongo.template")
    private MongoTemplate template;

    @AfterMethod(alwaysRun = true)
    public void tearDown() {
        template.dropCollection(RK_COLLECTION);
    }

    @Ignore
//    @Test
    public void testRegisterRoutingKey() {
        smsRoutingkeyMongoDBRepository.registerRoutingKey("JK", "SAFARI-CON", "5555", "IDOL");
        smsRoutingkeyMongoDBRepository.registerRoutingKey("JK", "SAFARI-CON", "5555", "KDOL");
        smsRoutingkeyMongoDBRepository.registerRoutingKey("JK", "SAFARI-CON", "5545", "ODOL");
        smsRoutingkeyMongoDBRepository.registerRoutingKey("JK", "SAFARI-CON", "5553", "IDOL");
        smsRoutingkeyMongoDBRepository.registerRoutingKey("JK", "AIRTEL", "5553", "IDOL");
        smsRoutingkeyMongoDBRepository.registerRoutingKey("KK", "AIRTEL", "5453", "IDOL");

        Map<String, Map<String, List<String>>> rks = smsRoutingkeyMongoDBRepository.retrieveAvailableRoutingKeys("JK");
        Map<String, List<String>> safaricomRkMap = rks.get("SAFARI-CON");
        assertNotNull(safaricomRkMap);
        assertEquals(safaricomRkMap.isEmpty(), false);

        List<String> keywordsFor5555 = new LinkedList<String>();
        keywordsFor5555.add("IDOL");
        keywordsFor5555.add("KDOL");

        assertEquals(keywordsFor5555, safaricomRkMap.get("5555"));

        List<String> keywordsFor5553 = new LinkedList<String>();
        keywordsFor5553.add("IDOL");

        assertEquals(keywordsFor5553, safaricomRkMap.get("5553"));

        List<String> keywordsFor5545 = new LinkedList<String>();
        keywordsFor5545.add("ODOL");

        assertEquals(keywordsFor5545, safaricomRkMap.get("5545"));

        Map<String, List<String>> airtelRkMap = rks.get("AIRTEL");

        List<String> airtelKeywordListFor5553 = new LinkedList<String>();
        airtelKeywordListFor5553.add("IDOL");

        assertEquals(airtelKeywordListFor5553, airtelRkMap.get("5553"));

        try {
            smsRoutingkeyMongoDBRepository.registerRoutingKey("JK", "SAFARI-CON", "5555", "IDOL");
            fail("Exception expected");
        } catch (SdpException expected) {
            assertNotNull(expected);
        }
    }

    @Ignore
//    @Test
    public void testIsRoutingKeyAvailable() {
        smsRoutingkeyMongoDBRepository.registerRoutingKey("JK", "SAFARI-CON", "5555", "IDOL");
        assertFalse(smsRoutingkeyMongoDBRepository.isRoutingKeyAvailableForSp("SAFARI-CON", "5555", "IDOL"));
        assertTrue(smsRoutingkeyMongoDBRepository.isRoutingKeyAvailableForSp("SAFARI-CONN", "5555", "IDOL"));
        assertTrue(smsRoutingkeyMongoDBRepository.isRoutingKeyAvailableForSp("SAFARI-CON", "4555", "IDOL"));
        assertTrue(smsRoutingkeyMongoDBRepository.isRoutingKeyAvailableForSp("SAFARI-CON", "5555", "IDOLL"));
    }


    @Test
    public void createExclusiveShortcode() {
        smsRoutingkeyMongoDBRepository.createExclusiveShortcode("airtel", "9696");
        smsRoutingkeyMongoDBRepository.createExclusiveShortcode("airtel", "1234");
        smsRoutingkeyMongoDBRepository.createExclusiveShortcode("safaricom", "1234");
        smsRoutingkeyMongoDBRepository.createExclusiveShortcode("safaricom", "9696");
        smsRoutingkeyMongoDBRepository.createExclusiveShortcode("safaricom", "3455");

        smsRoutingkeyMongoDBRepository.createNonExclusiveShortcode("airtel", "0988");
        smsRoutingkeyMongoDBRepository.createNonExclusiveShortcode("airtel", "0456");
        smsRoutingkeyMongoDBRepository.createNonExclusiveShortcode("safaricom", "0988");
        smsRoutingkeyMongoDBRepository.createNonExclusiveShortcode("safaricom", "0456");

        Set<String> exclusiveShortcodesForAirtel = smsRoutingkeyMongoDBRepository.findExclusiveShortcodes("airtel");

        assertNotNull(exclusiveShortcodesForAirtel);
        assertEquals(exclusiveShortcodesForAirtel.size(), 2);
        assertTrue(exclusiveShortcodesForAirtel.contains("9696"));
        assertTrue(exclusiveShortcodesForAirtel.contains("1234"));

        Set<String> nonExclusiveShortcodesForAirtel = smsRoutingkeyMongoDBRepository.findNonExclusiveShortcodes("airtel");
        assertNotNull(nonExclusiveShortcodesForAirtel);
        assertEquals(nonExclusiveShortcodesForAirtel.size(), 2);
        assertTrue(nonExclusiveShortcodesForAirtel.contains("0988"));
        assertTrue(nonExclusiveShortcodesForAirtel.contains("0456"));

    }

    @Test
    public void testRetrieveSharedShortcodes() {

        Map<String, Set<String>> emptyResult = smsRoutingkeyMongoDBRepository.retrieveSharedShortcodes();
        assertTrue(emptyResult.isEmpty());

        smsRoutingkeyMongoDBRepository.createNonExclusiveShortcode("airtel", "9696");
        smsRoutingkeyMongoDBRepository.createNonExclusiveShortcode("airtel", "1234");
        smsRoutingkeyMongoDBRepository.createNonExclusiveShortcode("safaricom", "1234");
        smsRoutingkeyMongoDBRepository.createNonExclusiveShortcode("safaricom", "3455");
        smsRoutingkeyMongoDBRepository.createNonExclusiveShortcode("safaricom", "12346");

        Map<String, Set<String>> actualSet = smsRoutingkeyMongoDBRepository.retrieveSharedShortcodes();


        HashMap<String, Set<String>> expectedSet = new HashMap<String, Set<String>>();
        HashSet<String> airtelSet = new HashSet<String>();
        airtelSet.add("9696");
        airtelSet.add("1234");
        expectedSet.put("airtel", airtelSet);

        HashSet<String> safaricomSet = new HashSet<String>();
        safaricomSet.add("1234");
        safaricomSet.add("3455");
        safaricomSet.add("12346");
        expectedSet.put("safaricom", safaricomSet);

        assertNotNull(actualSet);
        assertEquals(actualSet.size(), expectedSet.size());
        assertEquals(actualSet, expectedSet);

    }

    @Ignore
//    @Test
    public void testFindRoutingKey() {
        smsRoutingkeyMongoDBRepository.registerRoutingKey("SP01", "SAFARICOM", "5555", "IDOL");
        smsRoutingkeyMongoDBRepository.registerRoutingKey("SP01", "SAFARICOM", "5555", "IDOL2");
        smsRoutingkeyMongoDBRepository.registerRoutingKey("SP01", "SAFARICOM", "5555", "");

        Map<String, Object> rk = smsRoutingkeyMongoDBRepository.findSharedShortCodeRoutingKey("SAFARICOM", "5555", "IDOL");
        assertNotNull(rk);
        assertEquals(rk.get(operatorK), "SAFARICOM");
        assertEquals(rk.get(spIdK), "SP01");
        assertEquals(rk.get(shortcodeK), "5555");
        assertEquals(rk.get(keywordK), "IDOL");

        rk = smsRoutingkeyMongoDBRepository.findSharedShortCodeRoutingKey("SAFARICOM", "5555", "I");
        assertNull(rk);

        rk = smsRoutingkeyMongoDBRepository.findSharedShortCodeRoutingKey("SAFARICOM", "5555", "");
        assertNotNull(rk);
        assertEquals(rk.size(), 1);

        rk = smsRoutingkeyMongoDBRepository.findSharedShortCodeRoutingKey("SAFARICOM", "5555", "IDOL2");
        assertNotNull(rk);
    }

    @Test
    public void testRetireRoutingKey() {
        template.getCollection(RK_COLLECTION).drop();
        template.getCollection(TERMINATED_RK_COLLECTION).drop();
        template.createCollection(RK_COLLECTION);
        template.createCollection(TERMINATED_RK_COLLECTION);

        List<Map> rks = new ArrayList<Map>();

        //add initial data
        //{ "ncs-type" : "sms", "operator" : "airtel", "shortcode" : "9696", "keyword" : "", "exclusive" : true, "sp-id" : "" }
        rks.add(initRk(smsK, "airtel", "9696", "", true));
        //{ "ncs-type" : "sms", "operator" : "airtel", "shortcode" : "1234", "keyword" : "", "exclusive" : false, "sp-id" : "" }
        rks.add(initRk(smsK, "airtel", "1234", "", false));
        //{ "ncs-type" : "sms", "operator" : "safaricom", "shortcode" : "3455", "keyword" : "", "exclusive" : true , "sp-id" : "" }
        rks.add(initRk(smsK, "safaricom", "3455", "", true));
        //{ "ncs-type" : "sms", "operator" : "safaricom", "shortcode" : "4584", "keyword" : "", "exclusive" : false, "sp-id" : "" }
        rks.add(initRk(smsK, "safaricom", "4584", "", false));
        //{ "ncs-type" : "ussd", "operator" : "airtel", "shortcode" : "7490", "keyword" : "", "exclusive" : false, "sp-id" : "" }
        rks.add(initRk(smsK, "airtel", "7490", "", false));


        //{ "ncs-type" : "ussd", "operator" : "airtel", "shortcode" : "7484", "keyword" : "", "exclusive" : true, "sp-id" : "" }
        rks.add(initRk(ussdK, "airtel", "7484", "", true));
        //{ "ncs-type" : "sms", "operator" : "airtel", "shortcode" : "1234", "keyword" : "", "exclusive" : false, "sp-id" : "" }
        rks.add(initRk(ussdK, "airtel", "1234", "", false));
        //{ "ncs-type" : "ussd", "operator" : "safaricom", "shortcode" : "8546", "keyword" : "", "exclusive" : false , "sp-id" : "" }
        rks.add(initRk(ussdK, "safaricom", "8546", "", false));
        //{ "ncs-type" : "ussd", "operator" : "safaricom", "shortcode" : "9612", "keyword" : "", "exclusive" : false, "sp-id" : "" }
        rks.add(initRk(ussdK, "safaricom", "9612", "", false));
        //{ "ncs-type" : "ussd", "operator" : "safaricom", "shortcode" : "1232", "keyword" : "test", "exclusive" : true, "sp-id" : "" }
        rks.add(initRk(ussdK, "safaricom", "1232", "", true));

        for (Map rk : rks) {
            template.getCollection(RK_COLLECTION).save(new BasicDBObject(rk));
        }

        smsRoutingkeyMongoDBRepository.registerRoutingKey("sample-sp", "sample-app", "airtel", "9696", "");
        smsRoutingkeyMongoDBRepository.registerRoutingKey("sample-sp", "sample-app", "airtel", "1234", "test1");
        smsRoutingkeyMongoDBRepository.registerRoutingKey("sample-sp", "sample-app", "safaricom", "3455", "");
        smsRoutingkeyMongoDBRepository.registerRoutingKey("sample-sp", "sample-app", "safaricom", "4584", "test");
        smsRoutingkeyMongoDBRepository.registerRoutingKey("sample-sp", "sample-app2", "safaricom", "4584", "not-test");
        smsRoutingkeyMongoDBRepository.registerRoutingKey("sample-sp", "sample-app2", "airtel", "1234", "not-test");

        ussdRoutingkeyMongoDBRepository.registerRoutingKey("sample-sp", "sample-app", "airtel", "7484", "");
        ussdRoutingkeyMongoDBRepository.registerRoutingKey("sample-sp", "sample-app", "airtel", "1234", "test1");
        ussdRoutingkeyMongoDBRepository.registerRoutingKey("sample-sp", "sample-app", "safaricom", "1232", "");
        ussdRoutingkeyMongoDBRepository.registerRoutingKey("sample-sp", "sample-app", "safaricom", "9612", "test");

        smsRoutingkeyMongoDBRepository.terminate("sample-app", "safaricom");

        assertEquals(smsRoutingkeyMongoDBRepository.findRoutingKeys("sample-app", "safaricom").size(), 0);
        assertEquals(smsRoutingkeyMongoDBRepository.findRoutingKeys("sample-app", "airtel").size(), 2);

        assertEquals(smsRoutingkeyMongoDBRepository.findRoutingKeys("sample-app2", "safaricom").size(), 1);
        assertEquals(smsRoutingkeyMongoDBRepository.findRoutingKeys("sample-app2", "airtel").size(), 1);

        assertEquals(ussdRoutingkeyMongoDBRepository.findRoutingKeys("sample-app", "airtel").size(), 2);
        assertEquals(ussdRoutingkeyMongoDBRepository.findRoutingKeys("sample-app", "safaricom").size(), 2);

        assertEquals(smsRoutingkeyMongoDBRepository.findTerminatedRoutingKeys("sample-app", "airtel").size(), 0);
        assertEquals(smsRoutingkeyMongoDBRepository.findTerminatedRoutingKeys("sample-app", "safaricom").size(), 2);

    }

    private HashMap initRk(String ncsType, String operator, String shortcode, String keyword, boolean exclusive) {
        HashMap rk1 = new HashMap();
        rk1.put(ncsTypeK, ncsType);
        rk1.put(operatorK, operator);
        rk1.put(shortcodeK, shortcode);
        rk1.put(keywordK, keyword);
        rk1.put(exclusiveK, exclusive);
        rk1.put(spIdK, "");
        return rk1;
    }
}

