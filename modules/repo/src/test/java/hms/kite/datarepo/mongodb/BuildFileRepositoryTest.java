/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/
package hms.kite.datarepo.mongodb;

import com.mongodb.BasicDBObject;
import hms.kite.datarepo.BuildFileRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.mongodb.MongoUtil.createLikeQuery;
import static hms.kite.util.KiteKeyBox.*;
import static org.testng.AssertJUnit.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@ContextConfiguration(locations = {"classpath:beans-test.xml"})
public class BuildFileRepositoryTest extends AbstractTestNGSpringContextTests {

    @Autowired
    @Qualifier("cms.mongo.template")
    private MongoTemplate template;
    @Autowired
    private BuildFileRepositoryService buildFileRepositoryService;

    private final String build_file = "build_file";

    private Map<String, Object> makeContent(String contentId, String version) {
        BasicDBObject content = new BasicDBObject();
        content.put(contentIdK, contentId);
        content.put(versionK, version);

        return content;
    }

    @BeforeClass
    public void setUp() {
        template.getCollection(build_file).drop();
        template.getCollection(build_file + ".files").drop();
        template.getCollection(build_file + ".chunks").drop();
    }

    @AfterMethod
    public void tearDown() {
        template.getCollection(build_file).drop();
        template.getCollection(build_file + ".files").drop();
        template.getCollection(build_file + ".chunks").drop();
    }

    @Test
    public void testGetDevicesAndPlatformsByAppId() {
        Map<String, Object> dev1 = new HashMap<String, Object>();
        dev1.put(supportDevBrandK, "Samsung");
        dev1.put(supportDevModelsK, Arrays.asList("Galaxy Y", "Galaxy S I"));

        Map<String, Object> dev2 = new HashMap<String, Object>();
        dev2.put(supportDevBrandK, "HTC");
        dev2.put(supportDevModelsK, Arrays.asList("My Touch 3G"));

        Map<String, Object> buildFile1 = new HashMap<String, Object>();
        buildFile1.put(supportDevK, Arrays.asList(dev1, dev2));
        buildFile1.put(buildNameK, "Build1");
        buildFile1.put(buildDescK, "Description");
        buildFile1.put(buildVersionK, "1.0");
        buildFile1.put(buildAppFileIdK, "1312050916150002");
        buildFile1.put(platformNameK, "Android");
        buildFile1.put(platformVersionsK, Arrays.asList("2.2", "3.0", "3.1"));
        buildFile1.put(appIdK, "APP_000001");
        buildFile1.put(statusK, approvedK);

        Map<String, Object> dev3 = new HashMap<String, Object>();
        dev3.put(supportDevBrandK, "Nokia");
        dev3.put(supportDevModelsK, Arrays.asList("6120"));

        Map<String, Object> buildFile2 = new HashMap<String, Object>();
        buildFile2.put(supportDevK, Arrays.asList(dev3));
        buildFile2.put(buildNameK, "Build2");
        buildFile2.put(buildDescK, "Description");
        buildFile2.put(buildVersionK, "1.0");
        buildFile2.put(buildAppFileIdK, "1312050916150003");
        buildFile2.put(platformNameK, "Symbian");
        buildFile2.put(platformVersionsK, Arrays.asList("S60"));
        buildFile2.put(appIdK, "APP_000001");
        buildFile2.put(statusK, approvedK);

        Map<String, Object> buildFile3 = new HashMap<String, Object>();
        buildFile3.put(supportDevK, Arrays.asList(dev2));
        buildFile3.put(buildNameK, "Build3");
        buildFile3.put(buildDescK, "Description");
        buildFile3.put(buildVersionK, "1.0");
        buildFile3.put(buildAppFileIdK, "1312050916150004");
        buildFile3.put(platformNameK, "Android");
        buildFile3.put(platformVersionsK, Arrays.asList("2.2"));
        buildFile3.put(appIdK, "APP_000001");
        buildFile3.put(statusK, approvedK);

        Map<String, Object> dev4 = new HashMap<String, Object>();
        dev4.put(supportDevBrandK, "Samsung");
        dev4.put(supportDevModelsK, Arrays.asList("Galaxy S I", "Galaxy Nexus"));

        Map<String, Object> buildFile4 = new HashMap<String, Object>();
        buildFile4.put(supportDevK, Arrays.asList(dev4));
        buildFile4.put(buildNameK, "Build3");
        buildFile4.put(buildDescK, "Description");
        buildFile4.put(buildVersionK, "1.0");
        buildFile4.put(buildAppFileIdK, "1312050916150005");
        buildFile4.put(platformNameK, "Android");
        buildFile4.put(platformVersionsK, Arrays.asList("3.0", "4.0"));
        buildFile4.put(appIdK, "APP_000001");
        buildFile4.put(statusK, approvedK);


        Map<String, Object> devices = new HashMap<String, Object>();
        BasicDBObject models1 = new BasicDBObject();
        models1.put("Galaxy Nexus", Arrays.asList(makeContent("1312050916150005", "1.0")));
        models1.put("Galaxy S I", Arrays.asList(makeContent("1312050916150005", "1.0")));
        models1.put("Galaxy Y", Arrays.asList(makeContent("1312050916150002", "1.0")));
        devices.put("Samsung", models1);

        BasicDBObject models2 = new BasicDBObject();
        models2.put("My Touch 3G", Arrays.asList(makeContent("1312050916150004", "1.0")));
        devices.put("HTC", models2);

        BasicDBObject models3 = new BasicDBObject();
        models3.put("6120", Arrays.asList(makeContent("1312050916150003", "1.0")));
        devices.put("Nokia", models3);

        Map<String, Object> platforms = new HashMap<String, Object>();
        BasicDBObject versions1 = new BasicDBObject();
        versions1.put("2.2", Arrays.asList(makeContent("1312050916150004", "1.0")));
        versions1.put("3.0", Arrays.asList(makeContent("1312050916150005", "1.0")));
        versions1.put("3.1", Arrays.asList(makeContent("1312050916150002", "1.0")));
        versions1.put("4.0", Arrays.asList(makeContent("1312050916150005", "1.0")));
        platforms.put("Android", versions1);

        BasicDBObject versions2 = new BasicDBObject();
        versions2.put("S60", Arrays.asList(makeContent("1312050916150003", "1.0")));
        platforms.put("Symbian", versions2);

        final Map<String, Object> appDevicesAndPlatforms = new HashMap<String, Object>();
        appDevicesAndPlatforms.put(cmsDevicesK, devices);
        appDevicesAndPlatforms.put(cmsPlatformsK, platforms);

        buildFileRepositoryService.createBuildFile(buildFile1);
        buildFileRepositoryService.createBuildFile(buildFile2);
        buildFileRepositoryService.createBuildFile(buildFile3);
        buildFileRepositoryService.createBuildFile(buildFile4);

        List<Map<String, Object>> buildFileList = buildFileRepositoryService.findByAppId("APP_000001");

        assertTrue(buildFileList.size() == 4);

        Map<String, Object> contentMap = buildFileRepositoryService.getDevicesAndPlatformsByAppId("APP_000001");

        assertNotNull(appDevicesAndPlatforms);
        assertEquals(appDevicesAndPlatforms, contentMap);
    }


    @Test
    public void testCreateBuildFile() {
        Map<String, Object> buildFile1 = createTestInfo("1312050916150002");

        buildFileRepositoryService.createBuildFile(buildFile1);

        List<Map<String, Object>> buildFileList = buildFileRepositoryService.findByAppId("APP_000001");

        assertTrue(buildFileList.size() == 1);

        Map<String, Object> actualRes = buildFileList.get(0);

        assertEquals(buildFile1, actualRes);
    }

    @Test
    public void testFindByAppId() {
        Map<String, Object> buildFile1 = createTestInfo("1312050916150002");

        Map<String, Object> buildFile2 = createTestInfo("1312050916150004");
        buildFile2.put(statusK, draftK);
        buildFile2.put(appIdK, "APP_000002");

        buildFileRepositoryService.createBuildFile(buildFile1);
        buildFileRepositoryService.createBuildFile(buildFile2);

        List<Map<String, Object>> buildFileList = buildFileRepositoryService.findByAppId("APP_000002");

        assertTrue(buildFileList.size() == 1);

        Map<String, Object> actualBuildFile = buildFileList.get(0);

        assertEquals(buildFile2, actualBuildFile);
    }

    @Test
    public void testSearch() {
        Map<String, Object> buildFile1 = createTestInfo("1312050916150002");
        buildFile1.put(buildNameK, "Build1");

        Map<String, Object> buildFile2 = createTestInfo("1312050916150002");
        buildFile2.put(statusK, pendingApproveK);
        buildFile2.put(appIdK, "APP_000002");
        buildFile2.put(buildNameK, "Test file");
        buildFile2.put(buildAppFileIdK, "1312050916150005");

        buildFileRepositoryService.createBuildFile(buildFile1);
        buildFileRepositoryService.createBuildFile(buildFile2);

        Map<String, Object> query = new HashMap<String, Object>();
        query.put(buildNameK, "te");
        query.put(statusK, draftK);

        List<Map<String, Object>> buildFileList = buildFileRepositoryService.search(query);

        assertTrue(buildFileList.size() == 0);

        query.put(buildNameK, "te");
        query.put(statusK, pendingApproveK);

        buildFileList = buildFileRepositoryService.search(query);

        assertTrue(buildFileList.size() == 1);

        Map<String, Object> actualBuildFile = buildFileList.get(0);

        assertEquals(buildFile2, actualBuildFile);
    }

    @Test
    public void testGetBuildFilesCount() {
        Map<String, Object> buildFile1 = createTestInfo("1312050916150002");
        buildFile1.put(buildNameK, "Build1");

        Map<String, Object> buildFile2 = createTestInfo("1312050916150005");
        buildFile2.put(statusK, pendingApproveK);
        buildFile2.put(appIdK, "APP_000002");
        buildFile2.put(buildNameK, "Test file");

        Map<String, Object> buildFile3 = createTestInfo("1312050916150006");
        buildFile3.put(statusK, pendingApproveK);
        buildFile3.put(appIdK, "APP_000002");
        buildFile3.put(buildNameK, "Test file 1");

        buildFileRepositoryService.createBuildFile(buildFile1);
        buildFileRepositoryService.createBuildFile(buildFile2);
        buildFileRepositoryService.createBuildFile(buildFile3);

        Map<String, Object> query = new HashMap<String, Object>();
        query.put(buildNameK, createLikeQuery("te"));
        query.put(statusK, draftK);

        int buildFilesCount = buildFileRepositoryService.getBuildFilesCount(query);

        assertTrue(buildFilesCount == 0);

        query.put(buildNameK, createLikeQuery("te"));
        query.put(statusK, pendingApproveK);

        buildFilesCount = buildFileRepositoryService.getBuildFilesCount(query);

        assertTrue(buildFilesCount == 2);
    }

    @Test
    public void testFindBuildFilesRange() {
        Map<String, Object> buildFile1 = createTestInfo("1312050916150002");
        buildFile1.put(buildNameK, "Build1");

        Map<String, Object> buildFile2 = createTestInfo("1312050916150005");
        buildFile2.put(statusK, pendingApproveK);
        buildFile2.put(appIdK, "APP_000002");
        buildFile2.put(buildNameK, "Test file");

        Map<String, Object> buildFile3 = createTestInfo("1312050916150006");
        buildFile3.put(statusK, pendingApproveK);
        buildFile3.put(appIdK, "APP_000002");
        buildFile3.put(buildNameK, "Test file 1");

        buildFileRepositoryService.createBuildFile(buildFile1);
        buildFileRepositoryService.createBuildFile(buildFile2);
        buildFileRepositoryService.createBuildFile(buildFile3);

        Map<String, Object> query = new HashMap<String, Object>();
        query.put(buildNameK, createLikeQuery("te"));

        List<Map<String, Object>> buildFiles = buildFileRepositoryService.findBuildFilesRange(query, 1, 2);

        assertTrue(buildFiles.size() == 1);

        Map<String, Object> orderBy = new HashMap<String, Object>();
        orderBy.put(createdDateK, -1);
        query.put(orderByK, orderBy);
        query.put(statusK, pendingApproveK);

        buildFiles = buildFileRepositoryService.findBuildFilesRange(query, 0, 2);

        assertTrue(buildFiles.size() == 2);

        assertEquals(buildFile3, buildFiles.get(0));
    }

    /*@Test
    public void testGetBuildFile() {
        HashMap<String, Object> buildFile1 = new HashMap<String, Object>();
        buildFile1.put(statusK, draftK);
        buildFile1.put(appIdK, "APP_000001");
        buildFile1.put(buildAppFileNameK, "test-app.apk");

        InputStream in = null;
        try {
            in = buildFileRepositoryService.getBuildFile("test-app.apk");
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertTrue(in != null);
    }*/

    @Test
    public void testUpdateBuildFile() {
        Map<String, Object> buildFile1 = createTestInfo("1312050916150002");

        buildFileRepositoryService.createBuildFile(buildFile1);

        List<Map<String, Object>> buildFileList = buildFileRepositoryService.findByAppId("APP_000001");

        assertTrue(buildFileList.size() == 1);

        Map<String, Object> tempRes = buildFileList.get(0);

        Map<String, Object> actualRes = new HashMap<String, Object>();

        actualRes.putAll(tempRes);

        assertEquals(buildFile1, actualRes);

        tempRes.put(statusK, approvedK);

        buildFileRepositoryService.updateBuildFile(tempRes);

        buildFileList = buildFileRepositoryService.findByAppId("APP_000001");

        actualRes = buildFileList.get(0);

        assertEquals(tempRes, actualRes);
    }

    @Test
    public void testDeleteBuildFile() {
        Map<String, Object> buildFile1 = createTestInfo("1312050916150002");

        buildFileRepositoryService.createBuildFile(buildFile1);

        List<Map<String, Object>> buildFileList;

        buildFileList = buildFileRepositoryService.findByAppId("APP_000001");

        assertTrue(buildFileList.size() == 1);

        Map<String, Object> tempRes = buildFileList.get(0);

        Map<String, Object> actualRes = new HashMap<String, Object>();

        actualRes.putAll(tempRes);

        assertEquals(buildFile1, actualRes);

        buildFileRepositoryService.deleteBuildFile(tempRes);

        buildFileList = buildFileRepositoryService.findByAppId("APP_000001");

        assertTrue(buildFileList.size() == 0);
    }

    @Test
    public void testDeleteBuildFileByAppId() {
        Map<String, Object> buildFile1 = createTestInfo("1312050916150002");
        Map<String, Object> buildFile2 = createTestInfo("1312050916150003");

        buildFileRepositoryService.createBuildFile(buildFile1);
        buildFileRepositoryService.createBuildFile(buildFile2);

        List<Map<String, Object>> buildFileList;

        buildFileList = buildFileRepositoryService.findByAppId("APP_000001");

        assertTrue(buildFileList.size() == 2);

        buildFileRepositoryService.deleteBuildFileByAppId("APP_000001");

        buildFileList = buildFileRepositoryService.findByAppId("APP_000001");

        assertTrue(buildFileList.size() == 0);
    }

    private Map<String, Object> createTestInfo(String id) {
        Map<String, Object> buildFile = new HashMap<String, Object>();
        buildFile.put(statusK, draftK);
        buildFile.put(appIdK, "APP_000001");
        buildFile.put(buildAppFileIdK, id);

        return buildFile;
    }
}
