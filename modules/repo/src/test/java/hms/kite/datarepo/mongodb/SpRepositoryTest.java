package hms.kite.datarepo.mongodb;

import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.util.KiteKeyBox.appIdK;
import static hms.kite.util.KiteKeyBox.coopUserIdK;
import static hms.kite.util.KiteKeyBox.coopUserNameK;
import static hms.kite.util.KiteKeyBox.draftK;
import static hms.kite.util.KiteKeyBox.nameK;
import static hms.kite.util.KiteKeyBox.spIdK;
import static hms.kite.util.KiteKeyBox.statusK;
import static org.testng.AssertJUnit.assertEquals;
import hms.kite.datarepo.impl.SpRepositoryServiceImpl;
import hms.kite.util.SdpException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.mongodb.MongoException;

/**
 * Created by IntelliJ IDEA.
 * User: mazeem
 * Date: 7/11/11
 * Time: 3:15 PM
 * To change this template use File | Settings | File Templates.
 */

@ContextConfiguration(locations = {"classpath:beans-test.xml"})
public class SpRepositoryTest extends AbstractTestNGSpringContextTests {

    @BeforeClass
    public void clear() {
        ((SpRepositoryServiceImpl) spRepositoryService()).deleteAll();
    }

    @Test
    public void findSpById () {

        HashMap<String, Object> sp = new HashMap<String, Object>();
        sp.put(spIdK, "SP_12345");
        sp.put(appIdK, "APP_123");
        spRepositoryService().create(sp);
        Map<String, Object> addedSp = spRepositoryService().findSpById("SP_12345");
        assertEquals(sp, addedSp);

    }

    @Test(expectedExceptions = {SdpException.class})
    public void searchSpWithInvalidId() {
        spRepositoryService().findSpById("SP_NULL");
    }

    @Test(expectedExceptions = {IllegalStateException.class})
    public void createNullSp() {
        HashMap<String, Object> sp = new HashMap<String, Object>();
        spRepositoryService().create(sp);
    }

    @Test
    public void findSpByName() {
        HashMap<String, Object> sp = new HashMap<String, Object>();
        sp.put(spIdK, "SP_12345_12345");
        sp.put(nameK, "APP_123_12345");
        spRepositoryService().create(sp);
        Map<String, Object> addedSp = spRepositoryService().findSpByName("APP_123_12345");
        assertEquals(sp, addedSp);
    }

    @Test
    public void findSpByCoopUserId () {

        HashMap<String, Object> sp = new HashMap<String, Object>();
        sp.put(spIdK, "SP_12");
        sp.put(coopUserIdK, "COOP_131");
        spRepositoryService().create(sp);
        Map<String, Object> addedSp = spRepositoryService().findSpByCoopUserId("COOP_131");
        assertEquals(sp, addedSp);

    }

    @Test(expectedExceptions = {SdpException.class})
    public void findSpByCoopUserIdIfNotAvailable () {
        spRepositoryService().findSpByCoopUserId("COOP_131_MOCJ");
    }

    @Test
    public void findSpByCoopUserName () {

        HashMap<String, Object> sp = new HashMap<String, Object>();
        sp.put(spIdK, "SP_21");
        sp.put(coopUserIdK, "COOP_111");
        sp.put(coopUserNameK, "KITE_USER");
        spRepositoryService().create(sp);
        Map<String, Object> addedSp = spRepositoryService().findSpByCoopUserName("KITE_USER");
        assertEquals(sp, addedSp);

    }

    @Test
    public void findSpBySpId () {

        HashMap<String, Object> sp = new HashMap<String, Object>();
        sp.put(spIdK, "SP_1234");
        sp.put(appIdK, "APP_123");
        sp.put(coopUserIdK, "COOP_121");
        spRepositoryService().create(sp);
        Map<String, Object> addedSp = spRepositoryService().findSpBySpId("SP_1234");
        assertEquals(sp, addedSp);

    }

    @Test
    public void findSpsByStatus () {

        HashMap<String, Object> sp = new HashMap<String, Object>();
        sp.put(spIdK, "SP_1234");
        sp.put(appIdK, "APP_123");
        sp.put(statusK, draftK);
        spRepositoryService().create(sp);
        List<Map<String, Object>> listOfSP = spRepositoryService().findSpsByStatus(draftK);
        assertEquals(listOfSP.contains(sp), true);

    }

    @Test
    public void findSpsByStatusRange() {
        int limit = 5, skip = 2;
        for (int i = 0; i < 10; i ++) {
            HashMap<String, Object> sp = new HashMap<String, Object>();
            sp.put(spIdK, "SP_1_234" + i);
            sp.put(appIdK, "APP_1_123" + i);
            sp.put(statusK, "MOCK_DRAFT");
            spRepositoryService().create(sp);
        }
        List<Map<String, Object>> listOfSP = spRepositoryService().findSpsByStatus("MOCK_DRAFT", skip, limit);
        assertEquals(listOfSP.size(), limit);
    }

    @Test(expectedExceptions = {MongoException.class})
    public void findSpsByStatusRangeInvalidLimit() {
        int limit = 5, skip = -2;
        for (int i = 0; i < 10; i ++) {
            HashMap<String, Object> sp = new HashMap<String, Object>();
            sp.put(spIdK, "SP_1_234" + i);
            sp.put(appIdK, "APP_1_123" + i);
            sp.put(statusK, "MOCK_DRAFT");
            spRepositoryService().create(sp);
        }
        spRepositoryService().findSpsByStatus("MOCK_DRAFT", skip, limit);
    }

    @Test
    public void testSpCount(){
        int count = 5;

        for (int i = 0; i < count; i ++) {
            HashMap<String, Object> sp = new HashMap<String, Object>();
            sp.put(spIdK, "SP_1_234_SP" + i);
            sp.put(appIdK, "APP_1_123_APP" + i);
            sp.put(statusK, "SP_MOCK_DRAFT_ACTIVE");
            spRepositoryService().create(sp);
        }

        assertEquals(count, spRepositoryService().countSpsByStatus("SP_MOCK_DRAFT_ACTIVE"));
    }
    @Test
    public void updateSpStatus () {

        HashMap<String, Object> sp = new HashMap<String, Object>();
        sp.put(spIdK, "SP_456");
        sp.put(statusK, draftK);
        spRepositoryService().create(sp);
        Map<String, Object> addedSp = spRepositoryService().findSpBySpId("SP_456");
        addedSp.put(statusK, "NEW_STATUS");
        spRepositoryService().update(addedSp);
        Map<String, Object> addedSp1 = spRepositoryService().findSpBySpId("SP_456");
        assertEquals(addedSp.containsValue("NEW_STATUS"), true);

    }

    @Test
    public void updateSpCoopId () {

        HashMap<String, Object> sp = new HashMap<String, Object>();
        sp.put(spIdK, "SP_120");
        sp.put(coopUserIdK, "COOP_987");
        spRepositoryService().create(sp);
        Map<String, Object> addedSp = spRepositoryService().findSpBySpId("SP_120");
        addedSp.put(coopUserIdK, "COOP_NEW_ID");
        spRepositoryService().update(addedSp);
        Map<String, Object> addedSp1 = spRepositoryService().findSpBySpId("SP_120");
        assertEquals(addedSp1.containsValue("COOP_987"), false);

    }

    @Test(expectedExceptions = {SdpException.class})
    public void updateSpId () {

        HashMap<String, Object> sp = new HashMap<String, Object>();
        sp.put(spIdK, "SP_122");
        sp.put(coopUserIdK, "COOP_987");
        spRepositoryService().create(sp);
        Map<String, Object> addedSp = spRepositoryService().findSpBySpId("SP_122");
        addedSp.put(spIdK, "SP_NEW_ID");
        spRepositoryService().update(addedSp);
        spRepositoryService().findSpBySpId("SP_122");

    }

    @Test(expectedExceptions = {SdpException.class})
    public void deleteSpById () {

        HashMap<String, Object> sp = new HashMap<String, Object>();
        sp.put(spIdK, "SP_1212");
        spRepositoryService().create(sp);
        Map<String, Object> addedSp = spRepositoryService().findSpBySpId("SP_1212");
        assertEquals(addedSp, sp);
        ((SpRepositoryServiceImpl) spRepositoryService()).deleteSpById("SP_1212");
        spRepositoryService().findSpBySpId("SP_1212");
    }

    @Test
    public void findAllApps() {
        ((SpRepositoryServiceImpl) spRepositoryService()).deleteAll();
        int count = 10, limit = count, skip = 0;
        for (int i = 0; i < count; i ++) {
            HashMap<String, Object> sp = new HashMap<String, Object>();
            sp.put(spIdK, "SP_MOCK_123" + i);
            sp.put(appIdK, "APP_MOCK_123" + i);
            sp.put(statusK, "MOCK_DRAFT");
            spRepositoryService().create(sp);
        }
        List<Map<String, Object>> listOfSP = spRepositoryService().findAllSp(skip, limit);
        assertEquals(listOfSP.size(), limit);
    }

    @Test
    public void countAllApps() {
        ((SpRepositoryServiceImpl) spRepositoryService()).deleteAll();
        int count = 10;
        for (int i = 0; i < count; i ++) {
            HashMap<String, Object> sp = new HashMap<String, Object>();
            sp.put(spIdK, "SP_MOCK_123" + i);
            sp.put(appIdK, "APP_MOCK_123" + i);
            sp.put(statusK, "MOCK_DRAFT");
            spRepositoryService().create(sp);
        }
        assertEquals(count, spRepositoryService().countAllSps());
    }

}
