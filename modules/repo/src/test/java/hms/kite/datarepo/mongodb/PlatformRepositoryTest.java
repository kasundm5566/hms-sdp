/*
*   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/
package hms.kite.datarepo.mongodb;

import com.mongodb.BasicDBObject;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.annotation.Resource;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.deviceAndPlatformRepositoryService;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@ContextConfiguration(locations = {"classpath:beans-test.xml"})
public class PlatformRepositoryTest extends AbstractTestNGSpringContextTests {

    @Resource(name = "repo.mongo.template")
    MongoTemplate mongoTemplate;

    private Map<String, Object> platformInfoMap;

    @BeforeMethod
    public void beforeTest() {
        mongoTemplate.getCollection("platform").drop();
        platformInfoMap = new LinkedHashMap<String, Object>();
        List<String> supportedFileTypes = new LinkedList<String>();
        supportedFileTypes.add("apk");
        List<String> platformVersions = new LinkedList<String>();
        platformVersions.add("2.1");
        platformVersions.add("2.2");
        platformVersions.add("3.0");

        platformInfoMap.put("platform-id", "android");
        platformInfoMap.put("platform-name", "Android");
        platformInfoMap.put("suppported-file-types", supportedFileTypes);
        platformInfoMap.put("platform-versions", platformVersions);
    }

    @Test
    public void testFindAll() {
        mongoTemplate.getCollection("platform").save(new BasicDBObject(platformInfoMap));

        List<Map<String, Object>> allPlatformList = deviceAndPlatformRepositoryService().findAllPlatforms();

        assertTrue(allPlatformList.size() == 1);

        Map<String, Object> currentPlatformInfoMap = allPlatformList.get(0);
        currentPlatformInfoMap.remove("_id");

        assertEquals(platformInfoMap, currentPlatformInfoMap);
    }

    @AfterTest
    public void tearDown() {
        mongoTemplate.getCollection("platform").drop();
    }
}
