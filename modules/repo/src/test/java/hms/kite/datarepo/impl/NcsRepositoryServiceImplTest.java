package hms.kite.datarepo.impl;


import hms.kite.datarepo.NcsRepositoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.appIdK;
import static hms.kite.util.KiteKeyBox.chargingK;
import static hms.kite.util.KiteKeyBox.metaDataK;

@Test
@ContextConfiguration(locations = {"classpath:sdp-repo-spring-test.xml"})
public class NcsRepositoryServiceImplTest extends AbstractTestNGSpringContextTests {

    private static final Logger logger = LoggerFactory.getLogger(NcsRepositoryServiceImplTest.class);

    @Autowired
    NcsRepositoryService ncsRepositoryService;

    @Test
    public void testFindByAppId() throws Exception {
        Map<String,Map<String,Object>> ncsMap = ncsRepositoryService.findByAppId("APP_001240");
    }

    @Test
    public void testFilter() throws Exception {
        Map<String, Object> filterMap = new HashMap<String, Object>();
        filterMap.put(chargingK + "." + metaDataK + "." + "mpesa-paybill-number", "123456");
        List<Map<String,Object>> filter = ncsRepositoryService.filter(filterMap);
        for (Map<String, Object> ncs : filter) {
            logger.debug("NCS found with m-pesa paybill no ", ncs.get(appIdK));
        }

    }
}
