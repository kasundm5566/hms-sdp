/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo.mongodb;

import static hms.kite.datarepo.RepositoryServiceRegistry.appRepositoryService;
import static hms.kite.datarepo.RepositoryServiceRegistry.ncsRepositoryService;
import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.util.KiteKeyBox.*;
import static org.testng.AssertJUnit.assertEquals;
import hms.kite.datarepo.CrBuilder;
import hms.kite.datarepo.impl.AppRepositoryServiceImpl;
import hms.kite.datarepo.impl.SpRepositoryServiceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Unit test for the CrBuilder
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
@ContextConfiguration(locations = {"classpath:beans-test.xml"})
public class CrBuilderTest extends AbstractTestNGSpringContextTests {

    public static final String SP_ID = "SP_CR_12345";
    public static final String APP_ID = "CR_APP_1234";
    public static final String NCS_SMS = "sms";
    public static final String SAFARICOM_OPERATOR = "safaricom";
    @Resource(name = "repo.service.crBuilder")
    private CrBuilder crBuilder;

    @BeforeClass
    public void loadInitialData () {
        ((SpRepositoryServiceImpl) spRepositoryService()).deleteAll();
        ((AppRepositoryServiceImpl) appRepositoryService()).deleteAll();
        ncsRepositoryService().deleteAll();
        HashMap<String, Object> sp = new HashMap<String, Object>();
        sp.put(spIdK, SP_ID);
        sp.put(spNameK, "sp1");
        sp.put(coopUserIdK,SP_ID);
        spRepositoryService().create(sp);
        HashMap<String, Object> app = new HashMap<String, Object>();
        app.put(statusK, productionK);
        app.put(appIdK, APP_ID);
        app.put(nameK, "CR_KITE_1");
        app.put(spIdK, SP_ID);
        app.put(revenueShareK,10);
        Map<String, Object> smsSla = new HashMap<String, Object>();
        appRepositoryService().create(app);
        Map<String, Object> ncsSlas = new HashMap<String, Object>();
        generateAppNcsSlaParams("safaricom-1234-key1", ncsSlas);
        for (Map.Entry<String, Object> stringObjectEntry : ncsSlas.entrySet()) {
            smsSla = (Map<String, Object>) stringObjectEntry.getValue();
        }
        smsSla.put(statusK, pendingApproveK);
        smsSla.put(appIdK, APP_ID);
        smsSla.put(moAllowedK, true);
        smsSla.put(mtAllowedK, true);
        ncsRepositoryService().createNcs(smsSla, SP_ID);
    }

    @AfterTest
    public void tearDown() {

    }

    @Test
    public void createSpCr(){
        Map<String, Object> sp = spRepositoryService().findSpBySpId(SP_ID);
        Map<String, Object> spCr = crBuilder.createSpCr((String) sp.get(spIdK));
        assertEquals(crSpTypeK, spCr.get(crTypeK));
        assertEquals(initialK, spCr.get(statusK));
    }

    @Test
    public void createAppCr() {
        Map<String, Object> sp = spRepositoryService().findSpBySpId(SP_ID);
        Map<String, Object> app = appRepositoryService().findByAppId(APP_ID);
        Map<String, Object> appCr = crBuilder.createAppCr((String) sp.get(spIdK),
                (String) app.get(appIdK));
        assertEquals(crAppTypeK, appCr.get(crTypeK));
        assertEquals(initialK, appCr.get(statusK));
    }

    @Test
    public void createNcsCr() {
        Map<String, Object> sp = spRepositoryService().findSpBySpId(SP_ID);
        Map<String, Object> app = appRepositoryService().findByAppId(APP_ID);
        Map<String, Object> ncs = ncsRepositoryService().findNcs(APP_ID, NCS_SMS, SAFARICOM_OPERATOR);
        Map<String, Object> ncsCr = crBuilder.createNcsCr(NCS_SMS, SAFARICOM_OPERATOR, (String) sp.get(spIdK),
                (String) app.get(appIdK));
        assertEquals(crNcsTypeK, ncsCr.get(crTypeK));
        assertEquals(initialK, ncsCr.get(statusK));
    }

    /**
     * Creates NCS SLA request parameters
     * @param rkEntry
     */
    private void generateAppNcsSlaParams(String rkEntry, Map<String, Object> ncsSlas) {
        Map<String, Object> ncsSlaMap = new HashMap<String, Object>();
        Map<String, String> rkMap = new HashMap<String, String>();
        List<Map<String, String>> rkList = new ArrayList<Map<String, String>>();
        String[] keywordData = rkEntry.split("-");
        String operator = keywordData[0].trim();
        String ncsType = NCS_SMS;
        rkMap.put(shortcodeK, keywordData[1].trim());
        rkMap.put(keywordK, keywordData[2].trim());
        rkList.add(rkMap);
        ncsSlaMap.put(messageReceivingUrlK, "http://127.0.0.1");
        ncsSlaMap.put(operatorK, SAFARICOM_OPERATOR);
        ncsSlaMap.put(ncsTypeK, NCS_SMS);
        ncsSlaMap.put(appIdK, APP_ID);
//        ncsSlaMap.put(MO_CHARGING_AMOUNT, moChargingAmount);
//        ncsSlaMap.put(MO_CHARGING_PARTY, moChargeParty);
//        ncsSlaMap.put(MT_CHARGING_TYPE, mtChargingType);
//        ncsSlaMap.put(MT_CHARGING_AMOUNT, mtChargingAmount);
//        ncsSlaMap.put(MT_CHARGING_PARTY, mtChargeParty);
//        ncsSlaMap.put(MO_TPS, moTps);
//        ncsSlaMap.put(MO_TPD, moTpd);
//        ncsSlaMap.put(MT_TPS, mtTps);
//        ncsSlaMap.put(MT_TPD, mtTpd);
//        ncsSlaMap.put(ROUTING_KEYS, rkList);
        String key = operator + "_" + ncsType;
        ncsSlas.put(key, ncsSlaMap);
    }

}
