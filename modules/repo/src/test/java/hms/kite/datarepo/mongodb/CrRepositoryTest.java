/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo.mongodb;

import static hms.kite.datarepo.RepositoryServiceRegistry.appRepositoryService;
import static hms.kite.datarepo.RepositoryServiceRegistry.crRepositoryService;
import static hms.kite.datarepo.RepositoryServiceRegistry.ncsRepositoryService;
import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.util.KiteKeyBox.*;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;
import hms.kite.datarepo.CrBuilder;
import hms.kite.datarepo.impl.AppRepositoryServiceImpl;
import hms.kite.datarepo.impl.SpRepositoryServiceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Unit test for the CR repository
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
@ContextConfiguration(locations = {"classpath:beans-test.xml"})
public class CrRepositoryTest extends AbstractTestNGSpringContextTests {

    public static final String SP_ID = "SP_CR_12345";
    public static final String APP_ID = "CR_APP_1234";
    public static final String NCS_SMS = "sms";
    public static final String SAFARICOM_OPERATOR = "safaricom";

    @Resource(name = "repo.service.crBuilder")
    private CrBuilder crBuilder;


    @BeforeClass
    public void loadInitialData () {
        ((SpRepositoryServiceImpl) spRepositoryService()).deleteAll();
        ((AppRepositoryServiceImpl) appRepositoryService()).deleteAll();
        ncsRepositoryService().deleteAll();
        crRepositoryService().deleteAll();
        HashMap<String, Object> sp = new HashMap<String, Object>();
        sp.put(spIdK, SP_ID);
        sp.put(spNameK, "sp1");
        sp.put(coopUserIdK,"SP1");
        spRepositoryService().create(sp);
        HashMap<String, Object> app = new HashMap<String, Object>();
        app.put(statusK, productionK);
        app.put(appIdK, APP_ID);
        app.put(nameK, "CR_KITE_1");
        app.put(spIdK, SP_ID);
        app.put(descriptionK, "sample application");
        app.put(revenueShareK,10);
        Map<String, Object> smsSla = new HashMap<String, Object>();
        appRepositoryService().create(app);
        Map<String, Object> ncsSlas = new HashMap<String, Object>();
        generateAppNcsSlaParams("safaricom-1234-key1", ncsSlas);
        for (Map.Entry<String, Object> stringObjectEntry : ncsSlas.entrySet()) {
            smsSla = (Map<String, Object>) stringObjectEntry.getValue();
        }
        smsSla.put(statusK, pendingApproveK);
        smsSla.put(appIdK, APP_ID);
        smsSla.put(moAllowedK, true);
        smsSla.put(mtAllowedK, true);
        ncsRepositoryService().createNcs(smsSla, SP_ID);
    }


    @Test
    public void createSpCr(){
        Map<String, Object> spCr = crBuilder.createSpCr(SP_ID);
        assertEquals(crSpTypeK, spCr.get(crTypeK));
        assertEquals(initialK, spCr.get(statusK));
        crRepositoryService().create(spCr);
        List<Map<String, Object>> savedCrs = crRepositoryService().findCrBySpId(SP_ID);
        Map<String, Object> persistedCr = savedCrs.get(0);
        String name = "Sp_changed";
        Map<String, Object> updatedData = (Map<String, Object>) persistedCr.get(updatedDataK);
        updatedData.put(spNameK, name);
        crRepositoryService().update(persistedCr);
        Map<String, Object> updatedCr = crRepositoryService().findCrBySpId(SP_ID).get(0);
        Map<String, Object>finalCr = (Map<String, Object>) updatedCr.get(updatedDataK);
        assertEquals("Sp_changed", finalCr.get(spNameK));

    }

//    @Test
    public void createAppCr(){
        Map<String, Object> appCr = crBuilder.createAppCr(SP_ID, APP_ID);
        assertEquals(crAppTypeK, appCr.get(crTypeK));
        assertEquals(initialK, appCr.get(statusK));
        String description = "Description updated";
        Map<String, Object> updatedData = (Map<String, Object>) appCr.get(updatedDataK);
        updatedData.put(descriptionK, description);
        System.out.println("==========" + appCr.get(spIdK));
        crRepositoryService().create(appCr);
    }

    @Test
    public void createNcsSlaCr(){
        Map<String, Object> ncsSla = crBuilder.createNcsCr(NCS_SMS ,SAFARICOM_OPERATOR, SP_ID, APP_ID);
        assertEquals(crNcsTypeK, ncsSla.get(crTypeK));
        assertEquals(initialK, ncsSla.get(statusK));
        int newTps = 10;
        Map<String, Object> updatedData = (Map<String, Object>) ncsSla.get(updatedDataK);
        updatedData.put(tpsK, newTps);
        crRepositoryService().update(ncsSla);
    }


    @Test
    public void checkDuplications() {
//        Map<String, Object> appCr = crBuilder.createAppCr(SP_ID, APP_ID);
//        assertEquals(crAppTypeK, appCr.get(crTypeK));
//        assertEquals(initialK, appCr.get(statusK));
//        String description = "App changed when there is an ongoing CR";
//        Map<String, Object> updatedData = (Map<String, Object>) appCr.get(updatedDataK);
//        updatedData.put(descriptionK, description);
//        crRepositoryService().create(appCr);
    }

    @Test(dependsOnMethods = { "createSpCr" })
    public void checkCrEntryAlreadyExist() {
        Map<String, Object> criteria = new HashMap<String, Object>() {{
            put(spIdK, SP_ID);
            put(crTypeK, crSpTypeK);
        }};
        boolean isExists = crRepositoryService().isCrEntryExists(criteria);
        assertTrue(crRepositoryService().isCrEntryExists(criteria));
    }

    @Test(dependsOnMethods = { "createSpCr" })
    public void findBySpId() {
        List<Map<String, Object>> crs = crRepositoryService().findCrBySpId(SP_ID);
        assertEquals(1, crs.size());
    }

    @Test
    public void testUpdate() {
        List<Map<String, Object>> crs = crRepositoryService().findCrBySpId(SP_ID);
        Map<String, Object> cr = crs.get(0);
        cr.put(statusK, approvedK);
        crRepositoryService().update(cr);
        List<Map<String, Object>> persistedCrs = crRepositoryService().findCrBySpId(SP_ID);
        Map<String, Object> persistedCr = persistedCrs.get(0);
        assertEquals(approvedK, persistedCr.get(statusK));
    }
    /**
     * Creates NCS SLA request parameters
     * @param rkEntry
     */
    private void generateAppNcsSlaParams(String rkEntry, Map<String, Object> ncsSlas) {
        Map<String, Object> ncsSlaMap = new HashMap<String, Object>();
        Map<String, String> rkMap = new HashMap<String, String>();
        List<Map<String, String>> rkList = new ArrayList<Map<String, String>>();
        String[] keywordData = rkEntry.split("-");
        String operator = keywordData[0].trim();
        String ncsType = NCS_SMS;
        rkMap.put(shortcodeK, keywordData[1].trim());
        rkMap.put(keywordK, keywordData[2].trim());
        rkList.add(rkMap);
        ncsSlaMap.put(messageReceivingUrlK, "http://127.0.0.1");
        ncsSlaMap.put(operatorK, SAFARICOM_OPERATOR);
        ncsSlaMap.put(ncsTypeK, NCS_SMS);
        ncsSlaMap.put(appIdK,APP_ID);
//        ncsSlaMap.put(MO_CHARGING_TYPE, moChargingType);
//        ncsSlaMap.put(MO_CHARGING_AMOUNT, moChargingAmount);
//        ncsSlaMap.put(MO_CHARGING_PARTY, moChargeParty);
//        ncsSlaMap.put(MT_CHARGING_TYPE, mtChargingType);
//        ncsSlaMap.put(MT_CHARGING_AMOUNT, mtChargingAmount);
//        ncsSlaMap.put(MT_CHARGING_PARTY, mtChargeParty);
        ncsSlaMap.put(tpsK, 5);
        ncsSlaMap.put(tpdK, 50);
//        ncsSlaMap.put(MT_TPS, mtTps);
//        ncsSlaMap.put(MT_TPD, mtTpd);
//        ncsSlaMap.put(ROUTING_KEYS, rkList);
        String key = operator + "_" + ncsType;
        ncsSlas.put(key, ncsSlaMap);
    }
}
