package hms.kite.datarepo.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import hms.kite.datarepo.ThrottlingRepositoryService;
import hms.kite.util.ThrottlingType;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

import static hms.kite.datarepo.mongodb.ThrottlingMongoDbRepository.throttlingCollectionName;
import static hms.kite.util.KiteKeyBox.tpsK;

/**
 * User: azeem
 * Date: 3/13/12
 * Time: 12:43 PM
 */
@ContextConfiguration(locations = {"classpath:beans-test.xml"})
public class ThrottlingRepositoryServiceTest extends AbstractTestNGSpringContextTests {

    private static final String systemRepositoryKey = "system.tps";

    @Resource(name = "repo.mongo.template")
    private MongoTemplate mongoTemplate;

    @Resource
    private ThrottlingRepositoryService throttlingRepositoryService;

    @BeforeMethod(alwaysRun = true)
    public void before() {
        mongoTemplate.dropCollection(throttlingCollectionName);
        long initialCount = mongoTemplate.getCollection(throttlingCollectionName).getCount();
        Assert.assertEquals(initialCount, 0, "Initial Count should be zero");
    }

    @Test
    public void testIncreaseThrottlingWhenThereAreNoDocuments() {

        int delta = 5;
        ThrottlingType type = ThrottlingType.TPS;
        int currentValue = throttlingRepositoryService.increaseThrottling(systemRepositoryKey, delta, type);
        Assert.assertEquals(currentValue, delta,
                "Throttling value must = to current value, because of upsert");
    }

    @Test
    public void testIncreaseThrottlingWithExistingDocuments() {

        int delta = 2;
        ThrottlingType type = ThrottlingType.TPS;

        int numberOfRequests = 7;
        int currentValue = 0;

        for (int i = 0; i < numberOfRequests; i++) {
            currentValue = throttlingRepositoryService.increaseThrottling(systemRepositoryKey, delta, type);
        }

        Assert.assertEquals(currentValue, numberOfRequests * delta);
    }

    @Test
    public void testIncreaseThrottlingBeyondThreshold() {

        int delta = 1;
        int threshold = 50;
        int currentTps = 0;
        ThrottlingType type = ThrottlingType.TPS;

        int numberOfRequests = threshold + 5;

        for (int i = 0; i < numberOfRequests; i++) {
            currentTps = throttlingRepositoryService.increaseThrottling(systemRepositoryKey, delta, type);
        }
        Assert.assertEquals(numberOfRequests, currentTps);

    }

    @Test
    public void testResetThrottling() {

        int numberOfRequests = 7;
        List<ThrottlingType> throttlingTypes = Arrays.asList(ThrottlingType.TPD, ThrottlingType.TPS);
        for (ThrottlingType type : throttlingTypes) {
            int throttlingValue = 1;
            for (int i = 0; i < numberOfRequests; i++) {
                throttlingRepositoryService.increaseThrottling(String.valueOf(System.currentTimeMillis() * (i + 1)),
                        throttlingValue, type);
            }
        }

        long initialCount = mongoTemplate.getCollection(throttlingCollectionName).getCount();
        Assert.assertEquals(initialCount, throttlingTypes.size() * numberOfRequests,
                "Initial Count should be equals to total number of documents");

        throttlingRepositoryService.resetThrottling(ThrottlingType.TPS);

        BasicDBObject query = new BasicDBObject();
        query.put(tpsK, 0);
        DBCursor dbObjects = mongoTemplate.getCollection(throttlingCollectionName).find(query);

        Assert.assertEquals(dbObjects.count(), throttlingTypes.size() * numberOfRequests);
    }

}
