/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.datarepo.mongodb;

import static hms.kite.util.KiteKeyBox.appIdK;
import static hms.kite.util.KiteKeyBox.appTypeK;
import static hms.kite.util.KiteKeyBox.coopUserIdK;
import static hms.kite.util.KiteKeyBox.draftK;
import static hms.kite.util.KiteKeyBox.nameK;
import static hms.kite.util.KiteKeyBox.spIdK;
import static hms.kite.util.KiteKeyBox.statusK;
import hms.kite.datarepo.audit.AuditTrail;

import java.util.HashMap;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@ContextConfiguration(locations = {"classpath:beans-test.xml"})
public class AuditRepositoryTest extends AbstractTestNGSpringContextTests {

    private static final Logger logger = LoggerFactory.getLogger(AuditRepositoryTest.class);

    @Resource(name = "repo.mongo.template")
    private MongoTemplate mongoTemplate;

    @BeforeClass
    public void cleanCollection () {
        clean();
    }

    @AfterTest
    public void tearDown() {
        //clean();
    }

    private void clean() {
        mongoTemplate.getCollection("audit").drop();
    }

    @Test
    public void auditApp() {
        logger.debug("Audit APP Test begins");
        AuditTrail logTrail = AuditTrail.getInstance();
        logTrail.auditApp(new HashMap<String, Object>(){{
            put(statusK, draftK);
            put(appIdK, "APP_321");
            put(spIdK, "SP_1");
            put(appTypeK, "MCHOICE_KITE");
            put(nameK, "KITE_3");
        }}, "create");
    }

    @Test
    public void auditSp() {
        logger.debug("Audit SP Test begins");
        AuditTrail logTrail = AuditTrail.getInstance();
        logTrail.auditSp(new HashMap<String, Object>(){{
            put(spIdK, "SP_1234");
            put(appIdK, "APP_123");
            put(coopUserIdK, "COOP_121");
        }}, "update");
    }

}
