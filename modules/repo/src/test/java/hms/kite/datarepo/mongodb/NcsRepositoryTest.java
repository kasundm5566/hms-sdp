package hms.kite.datarepo.mongodb;


import com.mongodb.BasicDBObject;
import hms.kite.util.KiteKeyBox;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.appRepositoryService;
import static hms.kite.util.KiteKeyBox.activeK;
import static hms.kite.util.KiteKeyBox.statusK;
import static hms.kite.util.KiteKeyBox.terminateK;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertNotNull;

/**
 * Created by IntelliJ IDEA.
 * User: chandana
 * Date: 2/1/12
 * Time: 12:15 PM
 * To change this template use File | Settings | File Templates.
 */
@ContextConfiguration(locations = {"classpath:beans-test.xml"})
public class NcsRepositoryTest extends AbstractTestNGSpringContextTests {

    @Resource(name = "repo.mongo.template")
    private MongoTemplate mongoTemplate;

    Map<String, Object> app = new HashMap();

    @BeforeMethod
    public void setUp() {
        mongoTemplate.getCollection("app").drop();
        app.put(KiteKeyBox.appIdK, "testAppid");
        app.put(KiteKeyBox.nameK, "123456");
        app.put(KiteKeyBox.revenueShareK, false);
        app.put("end-date", new Date(2011, 1, 15));
        app.put(statusK, activeK);
        mongoTemplate.getCollection("app").save(new BasicDBObject(app));
    }

    @AfterMethod
    public void tearDown() {
        mongoTemplate.getCollection("app").remove(new BasicDBObject(app));
    }

    @Test
    public void testUpdateTerminateQuery() {
        Map<String, Object> query = new HashMap();
        query.put("end-date", new HashMap<String, Object>() {{
            put("$lt", new Date(2011, 1, 16));
        }});

        query.put(statusK, new HashMap<String, Object>(){{
            put("$ne", terminateK);
        }});

        List<Map<String, Object>> apps = appRepositoryService().findAppsRange(query, 0, 100);
        assertNotNull(apps);
        assertEquals(1, apps.size());

        query.put("end-date", new HashMap<String, Object>() {{
            put("$lt", new Date(2011, 1, 13));
        }});

        apps = appRepositoryService().findAppsRange(query, 0, 100);
        assertNotNull(apps);
        assertEquals(0, apps.size());
    }
}
