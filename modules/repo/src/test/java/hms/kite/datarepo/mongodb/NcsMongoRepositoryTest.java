package hms.kite.datarepo.mongodb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static hms.kite.util.KiteKeyBox.*;

@Test
@ContextConfiguration(locations = {"classpath:sdp-repo-spring-test.xml"})
public class NcsMongoRepositoryTest extends AbstractTestNGSpringContextTests {

    private static final Logger logger = LoggerFactory.getLogger(NcsRepositoryTest.class);

    @Autowired
    NcsMongoRepository ncsMongoRepository;

    @Test
    public void testFilter() throws Exception {
        Map<String, Object> filterMap = new HashMap<String, Object>();
        filterMap.put(chargingK + "." + metaDataK + "." + "mpesa-paybill-number", "123456");

        List<Map<String,Object>> filter = ncsMongoRepository.filter(filterMap);
        logger.debug("Filtering NCS returned [{}] result/s.", filter.size());
    }
}
