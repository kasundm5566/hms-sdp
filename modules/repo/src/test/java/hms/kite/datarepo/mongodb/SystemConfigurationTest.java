/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo.mongodb;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.Ignore;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;


/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@ContextConfiguration(locations = {"classpath:beans-test.xml"})
public class SystemConfigurationTest extends AbstractTestNGSpringContextTests {

    @Resource(name = "repo.mongo.systemConfiguration")
    SystemConfigurationMongoImpl systemConfigurationMongo;

    @Resource(name = "repo.mongo.template")
    MongoTemplate template;

    @BeforeMethod(alwaysRun = true)
    public void beforeTest() {
        template.getCollection("service_keywords").drop();
        template.getCollection("unique_id_collection").drop();
        template.getCollection("sdp_utils").drop();
    }

    @Test
    public void insertOne() {

        DBCollection serviceKeywords = template.getCollection("service_keywords");
        BasicDBObject reg = new BasicDBObject();
        reg.put("_id", "REG");
        BasicDBObject unreg = new BasicDBObject();
        unreg.put("_id", "UNREG");
        BasicDBObject abuse = new BasicDBObject();
        abuse.put("_id", "ABUSE");

        serviceKeywords.insert(reg);
        serviceKeywords.insert(unreg);
        serviceKeywords.insert(abuse);

        assertEquals(serviceKeywords.count(), 3);

        assertEquals(systemConfigurationMongo.isServiceKeyword("REG"), true);
        assertEquals(systemConfigurationMongo.isServiceKeyword("reg"), true);
        assertEquals(systemConfigurationMongo.isServiceKeyword("unreg"), true);
        assertEquals(systemConfigurationMongo.isServiceKeyword("unReg"), true);
        assertEquals(systemConfigurationMongo.isServiceKeyword("aBUSE"), true);
        assertEquals(systemConfigurationMongo.isServiceKeyword("aBUS"), false);
        assertEquals(systemConfigurationMongo.isServiceKeyword("BUS"), false);
        assertEquals(systemConfigurationMongo.isServiceKeyword("reger"), false);
        assertEquals(systemConfigurationMongo.isServiceKeyword("unr"), false);
    }

    @Test
    public void testGenerateId() {
        assertEquals(systemConfigurationMongo.incrementAndGetNextId("app"), 1);
        assertEquals(systemConfigurationMongo.incrementAndGetNextId("app"), 2);
        assertEquals(systemConfigurationMongo.incrementAndGetNextId("app"), 3);
        assertEquals(systemConfigurationMongo.incrementAndGetNextId("app"), 4);
    }

    @Ignore
    public void testFind() {
    	List<String> internalHosts = new ArrayList<String>();
    	internalHosts.add("207.154.10.23");
    	internalHosts.add("207.158.10.27");
    	Map<String, Object> miscellanious = new HashMap<String, Object>();
    	miscellanious.put("date", new Date());
    	miscellanious.put("string", "xxx");
    	BasicDBObject ob1 = new BasicDBObject();
    	ob1.put("_id", "internal-hosts");
    	ob1.put("value", internalHosts);
    	BasicDBObject ob2 = new BasicDBObject();
    	ob2.put("_id", "miscellanious");
    	ob2.put("value", miscellanious);
    	BasicDBObject ob3 = new BasicDBObject();
    	ob3.put("_id", "some_id");
    	ob3.put("value", "xxx");
    	template.getCollection("sdp_utils").save(ob1);
    	template.getCollection("sdp_utils").save(ob2);
    	template.getCollection("sdp_utils").save(ob3);
    	Object result1 = systemConfigurationMongo.find("internal-hosts");
    	assertNotNull(result1);
    	assertTrue(result1 instanceof List);
    	Object result2 = systemConfigurationMongo.find("miscellanious");
    	assertNotNull(result2);
    	assertTrue(result2 instanceof Map);
    	Object result3 = systemConfigurationMongo.find("some_id");
    	assertNotNull(result3);
    	assertTrue(result3 instanceof String);
    }
}
