/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.datarepo.audit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.auditRepositoryService;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class AuditTrail {

    private static AuditTrail uniqueInstance;
    private static final Logger logger = LoggerFactory.getLogger(AuditTrail.class);

    private static ThreadLocal<Map<String, Object>> currentUserData = new ThreadLocal<Map<String, Object>>() {
        @Override
        protected Map<String, Object> initialValue() {
            return new HashMap<String, Object>();
        }
    };

    private AuditTrail() {

    }

    public static AuditTrail getInstance() {
        synchronized (AuditTrail.class) {
            if (uniqueInstance == null) {
                uniqueInstance = new AuditTrail();
            }
            return uniqueInstance;
        }
    }


    public void auditSp(Map<String, Object> sp, String operation) {
        Map<String, Object> log = new HashMap<String, Object>();
        log.put(currentDataK, sp);
        log.put(typeK, spK);
        log.put(operationK, operation);
        audit(log);
    }

    public void auditApp(Map<String, Object> app, String operation) {
        Map<String, Object> log = new HashMap<String, Object>();
        log.put(currentDataK, app);
        log.put(typeK, appK);
        log.put(operationK, operation);
        audit(log);
    }

    public void auditNcs(Map<String, Object> ncs, String operation) {
        Map<String, Object> log = new HashMap<String, Object>();
        log.put(currentDataK, ncs);
        log.put(typeK, ncsK);
        log.put(operationK, operation);
        audit(log);
    }

    public void auditBuildFile(Map<String, Object> buildFileInfo, String operation) {
        Map<String, Object> log = new HashMap<String, Object>();
        log.put(currentDataK, buildFileInfo);
        log.put(typeK, buildFileK);
        log.put(operationK, operation);
        audit(log);
    }

    public void auditCr(String createdBy, String spId, String crType, String auditOperation, Map<String, Object> previousState) {
        logger.info("CR is Being Audited: [created by: {}], [sp id: {}], [CR Type: {}], [previousState: {}]", new Object[]{createdBy, spId, crType, previousState});
        Map<String, Object> log = new HashMap<String, Object>();
        log.put(createdByK, AuditTrail.getCurrentUsername());
        log.put(createdUserTypeK, AuditTrail.getCurrentUserType());
        log.put(typeK, "cr");
        log.put(crTypeK, crType);
        log.put(timestampK, System.currentTimeMillis());
        log.put(spIdK, spId);
        log.put(operationK, auditOperation);
        log.put(previousStateK, previousState);
        currentUserData.set(log);
        audit(log);
    }

    private void audit(Map data) {
        data.put("username", AuditTrail.getCurrentUsername());
        data.put("user-type", AuditTrail.getCurrentUserType());
        data.put(timestampK, System.currentTimeMillis());
        auditRepositoryService().audit(data);
    }

    public static void setCurrentUser(String username, String userType) {
        AuditTrail.currentUserData.get().put("current-username", username);
        AuditTrail.currentUserData.get().put("current-user-type", userType);
    }

    public static String getCurrentUsername() {
        return (String) AuditTrail.currentUserData.get().get("current-username");
    }

    public static String getCurrentUserType() {
        return (String) AuditTrail.currentUserData.get().get("current-user-type");
    }
}
