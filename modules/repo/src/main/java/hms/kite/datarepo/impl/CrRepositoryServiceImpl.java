/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo.impl;

import hms.kite.datarepo.CrRepository;
import hms.kite.datarepo.CrRepositoryService;
import hms.kite.datarepo.mongodb.CrMongoRepository;

import java.util.List;
import java.util.Map;

/**
 * Service implementation for handling provisioning CR Requests
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CrRepositoryServiceImpl implements CrRepositoryService {

    private CrRepository crRepository;

    @Override
    public void create(Map<String, Object> crReq) {
        crRepository.create(crReq);
    }

    @Override
    public boolean isCrEntryExists(Map<String, Object> queryCriteria) {
        return crRepository.isCrEntryExists(queryCriteria);
    }

    @Override
    public void delete(String crId) {
        crRepository.delete(crId);
    }

    @Override
    public void deleteAll() {
        crRepository.deleteAll();
    }

    @Override
    public void update(Map<String, Object> crReq) {
        crRepository.update(crReq);
    }

    @Override
    public Map<String, Object> findCrByCrId(String crId) {
        return crRepository.findCrByCrId(crId);
    }

    @Override
    public List<Map<String, Object>> findCrBySpId(String spId) {
        return crRepository.findCrBySpId(spId);
    }

    public void setCrRepository(CrRepository crRepository) {
        this.crRepository = crRepository;
    }
}
