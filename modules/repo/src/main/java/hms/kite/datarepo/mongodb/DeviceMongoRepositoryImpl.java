/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import hms.kite.datarepo.DeviceRepository;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class DeviceMongoRepositoryImpl implements DeviceRepository {

    private MongoTemplate mongoTemplate;
    private static final String COLLECTION_NAME = "device";

    @Override
    public List<Map<String, Object>> findAll() {

        DBCursor dbCursor = mongoTemplate.getCollection(COLLECTION_NAME).find(new BasicDBObject());

        List<Map<String, Object>> phoneList = getPhoneList(dbCursor);

        return phoneList;
    }

    private List<Map<String, Object>> getPhoneList(DBCursor dbCursor) {
        List<Map<String, Object>> phoneList = new ArrayList<Map<String, Object>>();

        while (dbCursor.hasNext()) {
            phoneList.add(dbCursor.next().toMap());
        }

        return phoneList;
    }

    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
}
