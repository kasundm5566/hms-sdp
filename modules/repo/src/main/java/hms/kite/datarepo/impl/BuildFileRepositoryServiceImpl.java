/*
 * (C) Copyright 1996-2012 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.datarepo.impl;

import com.mongodb.gridfs.GridFSDBFile;
import hms.commons.StaticDependencyInjector;
import hms.kite.datarepo.AppRepository;
import hms.kite.datarepo.AppRepositoryService;
import hms.kite.datarepo.BuildFileRepositoryService;
import hms.kite.datarepo.audit.AuditTrail;
import hms.kite.datarepo.discovery.connector.AppUpdateNotificationRequest;
import hms.kite.datarepo.discovery.connector.AppUpdateNotificationResponse;
import hms.kite.datarepo.discovery.connector.DiscoveryServiceConnector;
import hms.kite.datarepo.discovery.connector.DiscoveryServiceConnectorImpl;
import hms.kite.datarepo.mongodb.AppMongoRepository;
import hms.kite.datarepo.mongodb.BuildFileMongoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

public class BuildFileRepositoryServiceImpl implements BuildFileRepositoryService {

    private BuildFileMongoRepository buildFileMongoRepository;
    private AppRepository appRepository = new AppMongoRepository();
    private DiscoveryServiceConnector discoveryServiceConnector = new DiscoveryServiceConnectorImpl();
    private static final Logger logger = LoggerFactory.getLogger(BuildFileRepositoryServiceImpl.class);

    @Override
    public Map<String, Object> getDevicesAndPlatformsByAppId(String appId) {
        return buildFileMongoRepository.getDevicesAndPlatformsByAppId(appId);
    }

    @Override
    public Map<String, Object> getDevicesAndPlatformsByAppIdList(List<String> appIdList, String platform, String version) {
        return buildFileMongoRepository.getDevicesAndPlatformsByAppIdList(appIdList, platform, version);
    }

    @Override
    public List<Map<String, Object>> findByAppId(String appId) {
        return buildFileMongoRepository.findByAppId(appId);
    }

    @Override
    public Map<String, Object> findByBuildFileId(String fileId) {
        return buildFileMongoRepository.findByBuildFileId(fileId);
    }

    @Override
    public List<Map<String, Object>> search(Map<String, Object> query) {
        return buildFileMongoRepository.search(query);
    }

    @Override
    public int getBuildFilesCount(Map<String, Object> queryCriteria) {
        return buildFileMongoRepository.getBuildFilesCount(queryCriteria);
    }

    @Override
    public List<Map<String, Object>> findBuildFilesRange(Map<String, Object> queryCriteria, int skip, int limit) {
        return buildFileMongoRepository.findBuildFilesRange(queryCriteria, skip, limit);
    }

    @Override
    public GridFSDBFile getBuildFile(String fileId) {
        return buildFileMongoRepository.getBuildFile(fileId);
    }

    @Override
    public OutputStream getGridFsFileOutputStream(String fileId, String filename) {
        return buildFileMongoRepository.createGridFsFileOutputStream(fileId, filename);
    }

    @Override
    public void closeGridFsFileOutputStream(String fileId) {
        buildFileMongoRepository.closeGridFsFileOutputStream(fileId);
    }

    @Override
    public Map<String, Object> createBuildFile(Map<String, Object> buildFileInfo) {
        Date createdDate = new Date();

        buildFileInfo.put(createdDateK, createdDate);
        buildFileInfo.put(updatedDateK, createdDate);
        buildFileInfo.put(createdByK, AuditTrail.getCurrentUsername());
        buildFileInfo.put(updatedByK, AuditTrail.getCurrentUsername());

        buildFileInfo.put(idK, buildFileInfo.get(buildAppFileIdK));

        Map<String, Object> dbBuildFile = buildFileMongoRepository.create(buildFileInfo);

        AuditTrail.getInstance().auditBuildFile(buildFileInfo, createK);

        return dbBuildFile;
    }

    @Override
    public void updateBuildFile(Map<String, Object> buildFileInfo) {
        Date updatedDate = new Date();

        buildFileInfo.put(updatedDateK, updatedDate);
        buildFileInfo.put(updatedByK, AuditTrail.getCurrentUsername());

        buildFileMongoRepository.update(buildFileInfo);

        if (buildFileInfo.get(statusK).toString().equals(approvedK)) {
            AppUpdateNotificationRequest req = new AppUpdateNotificationRequest();
            req.setRequestId(String.valueOf(new Timestamp(System.currentTimeMillis()).getTime()));
            req.setAppId(buildFileInfo.get(appIdK).toString());
            String appName = appRepository.findByAppId(buildFileInfo.get(appIdK).toString()).get("name").toString();
            req.setAppName(appName);
            req.setBuildVersion(buildFileInfo.get(versionK).toString());
            req.setStatus(buildFileInfo.get(statusK).toString());
            req.setDispatchType("new");
            notifyAppUpdate(req);
        }
        AuditTrail.getInstance().auditBuildFile(buildFileInfo, updateK);
    }

    private AppUpdateNotificationResponse notifyAppUpdate(AppUpdateNotificationRequest notificationRequest){
        return discoveryServiceConnector.sendAppUpdateNotification(notificationRequest);
    }

    @Override
    public void deleteBuildFile(Map<String, Object> buildFileInfo) {
        Date updatedDate = new Date();

        buildFileInfo.put(updatedDateK, updatedDate);
        buildFileInfo.put(updatedByK, AuditTrail.getCurrentUsername());

        buildFileMongoRepository.delete(buildFileInfo.get(idK));

        AuditTrail.getInstance().auditBuildFile(buildFileInfo, deleteK);
    }

    @Override
    public void deleteBuildFileByAppId(String appId) {
        Date updatedDate = new Date();

        Map<String, Object> appInfo = new HashMap<String, Object>();
        appInfo.put(appIdK, appId);
        appInfo.put(updatedDateK, updatedDate);
        appInfo.put(updatedByK, AuditTrail.getCurrentUsername());

        buildFileMongoRepository.deleteByAppId(appId);

        AuditTrail.getInstance().auditBuildFile(appInfo, deleteK);
    }

    public void setBuildFileMongoRepository(BuildFileMongoRepository buildFileMongoRepository) {
        this.buildFileMongoRepository = buildFileMongoRepository;
    }
}
