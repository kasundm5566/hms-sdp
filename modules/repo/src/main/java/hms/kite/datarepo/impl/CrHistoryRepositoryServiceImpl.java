/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo.impl;

import hms.kite.datarepo.CrHistoryRepository;
import hms.kite.datarepo.CrHistoryRepositoryService;

import java.util.Map;

/**
 * Service implementation for handling provisioning CR history requests
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CrHistoryRepositoryServiceImpl implements CrHistoryRepositoryService {

    private CrHistoryRepository crHistoryRepository;

    @Override
    public void create(Map<String, Object> crReq) {
       crHistoryRepository.create(crReq);
    }

    public void setCrHistoryRepository(CrHistoryRepository crHistoryRepository) {
        this.crHistoryRepository = crHistoryRepository;
    }
}
