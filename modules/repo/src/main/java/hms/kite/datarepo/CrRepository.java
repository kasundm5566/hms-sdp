/**
 * (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 * All Rights Reserved.
 * <p/>
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 * International (pvt) Limited.
 * <p/>
 * hSenid International (pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.kite.datarepo;

import java.util.List;
import java.util.Map;

/**
 * Repository interface for the Provisioning CR Flow
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public interface CrRepository {

    void create(Map<String, Object> crReq);

    boolean isCrEntryExists(Map<String, Object> queryCriteria);

    void delete(String crId);

    void deleteAll ();

    void update(Map<String, Object> crReq);

    Map<String, Object> findCrByCrId(String crId);

    List<Map<String, Object>> findCrBySpId(String spId);

}
