/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo.mongodb;

import com.mongodb.*;
import hms.kite.datarepo.RoutingKeyRepository;
import hms.kite.util.KiteErrorBox;
import hms.kite.util.SdpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.*;

import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.KiteKeyBox.exclusiveK;
import static hms.kite.util.KiteKeyBox.keywordK;

/**
 * This query the routing-keys collection. routing-key collection will be as
 * follows.
 *
 * operator, shortcode, keyword = exclusive shortcode will have a keyword, sp-id
 * = if assigned to a sp that sp-id, app-id = if assigned to a application that
 * application id, status = active, cr, request, exclusive = true | false.
 *
 * When create a shortcode to list in the uis, keyword should leave as "".
 *
 */
public class RoutingKeyMongoDBRepository implements RoutingKeyRepository {

	private static final Logger logger = LoggerFactory.getLogger(RoutingKeyMongoDBRepository.class);

	public static final String RK_COLLECTION = "routing_keys";
	public static final String TERMINATED_RK_COLLECTION = "terminated_routing_keys";

	private MongoTemplate mongoTemplate;

	private String ncsType;

	public Map<String, Object> registerRoutingKey(String spId, String operatorName, String shortcode, String keyword) {

		logger.debug("Creating routing key [sp-id = [{}], operator-name = [{}], shortcode = [{}], keyword = [{}]",
				new Object[] { spId, operatorName, shortcode, keyword });

		Map<String, Object> routingKey = findRoutingKey(operatorName, shortcode, "");

		return createRoutingKey(spId, "", operatorName, shortcode, keyword, (Boolean) routingKey.get(exclusiveK));
	}

	@Override
	public Map<String, Object> registerRoutingKey(String spId, String appId, String operatorName, String shortcode,
			String keyword) {

		logger.debug(
				"Creating routing key [sp-id = [{}], app-id = [{}], operator-name = [{}], shortcode = [{}], keyword = [{}]",
				new Object[] { spId, appId, operatorName, shortcode, keyword });

		Map<String, Object> routingKey = findRoutingKey(operatorName, shortcode, "");

		return createRoutingKey(spId, appId, operatorName, shortcode, keyword, (Boolean) routingKey.get(exclusiveK));
	}

	private Map createRoutingKey(String spId, String appId, String operatorName, String shortcode, String keyword,
			boolean exclusive) {

		Map<String, Object> existingKey = findRoutingKey(operatorName, shortcode, keyword);

		if (existingKey == null) {
			BasicDBObject addKeyQuery = new BasicDBObject();
			addKeyQuery.put(spIdK, spId);
			addKeyQuery.put(operatorK, operatorName);
			addKeyQuery.put(shortcodeK, shortcode);
			addKeyQuery.put(keywordK, keyword);
			addKeyQuery.put(appIdK, appId);
			addKeyQuery.put(exclusiveK, exclusive);
			addKeyQuery.put(ncsTypeK, this.ncsType);
			addKeyQuery.put(lastUpdatedTimeK, new Date());

			saveAndEnsureIndex(addKeyQuery);
			return addKeyQuery.toMap();
		} else {

			boolean assignedToASp = !existingKey.get(spIdK).equals("");
			boolean assignedToThisSp = existingKey.get(spIdK).equals(spId);
			boolean assignedToApp = existingKey.get(appIdK) != null && !existingKey.get(appIdK).equals("");

			if (assignedToApp) {
				throw new SdpException("Routing key [" + existingKey.get(operatorK) + "/" + existingKey.get(shortcodeK)
						+ "/" + existingKey.get(keywordK) + " already assigned for " + existingKey.get(spIdK) + "/"
						+ existingKey.get(appIdK), KiteErrorBox.routingKeyAlreadyAssignedErrorCode);
			}
			if (!assignedToThisSp && assignedToASp) {
				throw new SdpException("Routing key [" + existingKey.get(operatorK) + "/" + existingKey.get(shortcodeK)
						+ "/" + existingKey.get(keywordK) + " already assigned for " + existingKey.get(spIdK) + "/"
						+ existingKey.get(appIdK), KiteErrorBox.routingKeyAlreadyAssignedErrorCode);
			}

			existingKey.put(appIdK, appId);
			existingKey.put(spIdK, spId);
			existingKey.put(ncsTypeK, this.ncsType);
            existingKey.put(lastUpdatedTimeK, new Date());

			saveAndEnsureIndex(new BasicDBObject(existingKey));
			return existingKey;
		}

	}

	private void saveAndEnsureIndex(BasicDBObject query) {
		BasicDBObject indexes = new BasicDBObject();
		indexes.put(ncsTypeK, 1);
		indexes.put(shortcodeK, 1);
		indexes.put(keywordK, 1);
		indexes.put(operatorK, 1);

		DBCollection collection = mongoTemplate.getCollection(RK_COLLECTION);
		collection.ensureIndex(indexes, "RK_INDEX", true);

		try {
			final WriteResult result = collection.save(query, WriteConcern.SAFE);
			result.getLastError().throwOnError();
		} catch (MongoException e) {
			throw new SdpException(KiteErrorBox.unknownErrorCode, e);
		}
	}

	@Override
	public boolean isRoutingKeyAvailableForSp(String operatorName, String shortcode, String keyword) {
		DBCollection dbCollection = mongoTemplate.getCollection(RK_COLLECTION);

		DBObject query = new QueryBuilder()
                .put(operatorK).is(operatorName)
                .put(shortcodeK).is(shortcode)
                .put(keywordK).is(keyword)
                .put(ncsTypeK).is(this.ncsType)
                .put(spIdK).notEquals("") // since sp id empty if it is not assigned
				.get();
        logger.debug("Query for SP available "+query);

		DBCursor dbCursor = dbCollection.find(query);
		return null == dbCursor || !dbCursor.hasNext();
	}

	@Override
	public boolean isRoutingKeyAvailableForApp(String spId, String operatorName, String shortcode, String keyword) {
		DBCollection dbCollection = mongoTemplate.getCollection(RK_COLLECTION);
		DBObject query = new QueryBuilder().put(operatorK).is(operatorName).put(shortcodeK).is(shortcode).put(keywordK)
				.is(keyword).put(spIdK).is(spId).put(appIdK).is("").put(ncsTypeK).is(this.ncsType).get();

		DBObject result = dbCollection.findOne(query);
		return result == null;
	}

	@Override
	public boolean isRoutingKeyAvailableForApp(String spId, String appId, String operatorName, String shortcode,
			String keyword) {

		DBCollection dbCollection = mongoTemplate.getCollection(RK_COLLECTION);
		DBObject query = new QueryBuilder().put(operatorK).is(operatorName).put(shortcodeK).is(shortcode).put(keywordK)
				.is(keyword).put(ncsTypeK).is(this.ncsType).get();
        logger.debug("Query for APP available "+query);

		DBObject result = dbCollection.findOne(query);
		Map<String, Object> routingKey = (result == null ? null : result.toMap());

		if (routingKey == null) {
			return true;
		}
		String keySpId = (String) routingKey.get(spIdK);
		String keyAppId = (String) routingKey.get(appIdK);

		if (keySpId == null || keySpId.isEmpty()) {
			return true;
		}

		if (!keySpId.equals(spId)) {
			return false;
		}

		if (keyAppId == null || keyAppId.isEmpty()) {
			return true;
		}

		if (keyAppId.equals(appId)) {
			return true;
		} else {
			return false;
		}

	}

	public Map<String, Map<String, List<String>>> retrieveAvailableRoutingKeys(String spId) {

		logger.debug("Find Available routing keys for sp {}", spId);

		Map<String, Map<String, List<String>>> rks = new HashMap<String, Map<String, List<String>>>();

		DBCollection dbCollection = mongoTemplate.getCollection(RK_COLLECTION);

		DBObject query = new QueryBuilder().put(spIdK).is(spId).put(appIdK).is("").get();

		DBCursor dbCursor = dbCollection.find(query);

		if (null != dbCursor) {
			createRkMap(rks, dbCursor);
		}

		logger.debug("Found these routing keys {}", rks);
		return rks;
	}

	@Override
	public Map<String, Map<String, List<String>>> retrieveAvailableRoutingKeys() {

		logger.debug("Find all available routing keys");
		return retrieveAvailableRoutingKeys("");
	}

	private void createRkMap(Map<String, Map<String, List<String>>> rks, DBCursor dbCursor) {

		while (dbCursor.hasNext()) {

			DBObject dbObject = dbCursor.next();
			String operator = (String) dbObject.get(operatorK);

			Map<String, List<String>> operatorRoutingKeys = rks.get(operator);

			if (operatorRoutingKeys == null) {
				operatorRoutingKeys = new HashMap<String, List<String>>();
				rks.put(operator, operatorRoutingKeys);
			}

			String shortcode = (String) dbObject.get(shortcodeK);
			List<String> keywordListForShortcode = operatorRoutingKeys.get(shortcode);
			if (keywordListForShortcode == null) {
				keywordListForShortcode = new LinkedList<String>();
				operatorRoutingKeys.put(shortcode, keywordListForShortcode);
			}
			keywordListForShortcode.add((String) dbObject.get(keywordK));
		}
	}

	@Override
	public Map<String, Set<String>> retrieveSharedShortcodes() {

		DBCollection collection = mongoTemplate.getCollection(RK_COLLECTION);

		BasicDBObject query = new BasicDBObject();
		query.put(exclusiveK, false);
		query.put(keywordK, ""); // since shared shortcodes will add with empty
									// keyword and exclusive false.
		query.put(ncsTypeK, this.ncsType);

		DBCursor dbCursor = collection.find(query);

		Map<String, Set<String>> result = new HashMap<String, Set<String>>();

		while (dbCursor.hasNext()) {

			DBObject next = dbCursor.next();

			String operator = (String) next.get(operatorK);
			Set<String> operatorShortcodeSet = result.get(operator);

			if (operatorShortcodeSet == null) {
				operatorShortcodeSet = new HashSet<String>();
				result.put(operator, operatorShortcodeSet);
			}
			operatorShortcodeSet.add((String) next.get(shortcodeK));
		}
		return result;
	}

    @Override
    public Map<String, Set<String>> retrieveDomainSpecificSharedShortCodes(Map<String, Object> reqObj) {
        DBCollection collection = mongoTemplate.getCollection(RK_COLLECTION);

        BasicDBObject query = new BasicDBObject(reqObj); // reqObject will have domain
        query.put(exclusiveK, false);
        query.put(keywordK, ""); // since shared shortcodes will add with empty
        // keyword and exclusive false.
        query.put(ncsTypeK, this.ncsType);

        DBCursor dbCursor = collection.find(query);

        Map<String, Set<String>> result = new HashMap<String, Set<String>>();

        while (dbCursor.hasNext()) {

            DBObject next = dbCursor.next();

            String operator = (String) next.get(operatorK);
            Set<String> operatorShortcodeSet = result.get(operator);

            if (operatorShortcodeSet == null) {
                operatorShortcodeSet = new HashSet<String>();
                result.put(operator, operatorShortcodeSet);
            }
            operatorShortcodeSet.add((String) next.get(shortcodeK));
        }
        return result;

    }

    public void createExclusiveShortcode(String operator, String shortcode) {
		doCreateShortcode(operator, shortcode, true);
	}

	public void createNonExclusiveShortcode(String operator, String shortcode) {
		doCreateShortcode(operator, shortcode, false);
	}

	private void doCreateShortcode(String operator, String shortcode, boolean exclusive) {
		if (isShortcodeExists(operator, shortcode)) {
			throw new IllegalStateException("Found already existing shortcode with same operator [" + operator
					+ "] shortcode [" + shortcode + "]");
		}

		BasicDBObject rk = new BasicDBObject();
		rk.put(operatorK, operator);
		rk.put(shortcodeK, shortcode);
		rk.put(keywordK, "");
		rk.put(exclusiveK, exclusive);
		rk.put(spIdK, "");
		rk.put(ncsTypeK, this.ncsType);

		// todo add indexes.

		mongoTemplate.getCollection(RK_COLLECTION).save(rk);
	}

	private boolean isShortcodeExists(String operator, String shortcode) {
		DBCollection collection = mongoTemplate.getCollection(RK_COLLECTION);

		BasicDBObject queryExistingKey = new BasicDBObject();

		queryExistingKey.put(operatorK, operator);
		queryExistingKey.put(shortcodeK, shortcode);

		DBObject one = collection.findOne(queryExistingKey);
		return one != null;
	}

	public void removeAllRks() {
		DBCollection dbCollection = mongoTemplate.getCollection(RK_COLLECTION);
		BasicDBObject document = new BasicDBObject();
		dbCollection.remove(document, WriteConcern.SAFE);
	}

	public Set<String> findExclusiveShortcodes(String operator) {
		return doFindShortcodes(operator, true);
	}

	public Set<String> findNonExclusiveShortcodes(String operator) {
		return doFindShortcodes(operator, false);
	}

	private Map<String, Object> findRoutingKey(String operator, String shortcode, String keyword) {
		DBCollection collection = mongoTemplate.getCollection(RK_COLLECTION);

		BasicDBObject query = new BasicDBObject();
		query.put(operatorK, operator);
		query.put(shortcodeK, shortcode);
		query.put(keywordK, keyword);
		query.put(ncsTypeK, this.ncsType);

		DBCursor dbCursor = collection.find(query);

		if (!dbCursor.hasNext()) {
			return null;
		}

		return (Map<String, Object>) dbCursor.next();
	}

	public List<Map<String, Object>> findRoutingKeys(String appId, String operator) {

		logger.debug("Find routing keys for app-id [{}], operator [{}]", appId, operator);
		DBCollection collection = mongoTemplate.getCollection(RK_COLLECTION);

		BasicDBObject query = new BasicDBObject();
		query.put(operatorK, operator);
		query.put(appIdK, appId);
		query.put(ncsTypeK, this.ncsType);

		List<Map<String, Object>> rks = doFind(collection, query);

		logger.info("Found Routing keys for app-id [{}] and operator [{}] is [{}]", new Object[] { appId, operator, rks });
		return rks;
	}

	@Override
	public List<Map<String, Object>> findRoutingKeys(String spId) {
		logger.debug("Find routing keys for sp-id [{}]", spId);
		DBCollection collection = mongoTemplate.getCollection(RK_COLLECTION);

		BasicDBObject query = new BasicDBObject();
		query.put(spIdK, spId);
		query.put(ncsTypeK, this.ncsType);

		List<Map<String, Object>> rks = doFind(collection, query);

		logger.info("Found Routing keys for sp-id [{}] is [{}]", spId, rks);
		return rks;
	}

	@Override
	public List<Map<String, Object>> findAllForOperator(String operator) {
		logger.debug("Find routing keys for operator [{}]", operator);
		DBCollection collection = mongoTemplate.getCollection(RK_COLLECTION);

		BasicDBObject query = new BasicDBObject();
		query.put(operatorK, operator);
		query.put(spIdK, "");
		query.put(keywordK, "");
		query.put(ncsTypeK, this.ncsType);

		List<Map<String, Object>> rks = doFind(collection, query);

		logger.info("Found Routing keys for operator [{}] is [{}]", operator, rks);
		return rks;
	}

	@Override
	public boolean isExclusive(String ncsType, String operator, String shortcode) {
		DBCollection collection = mongoTemplate.getCollection(RK_COLLECTION);

		BasicDBObject query = new BasicDBObject();
//		query.put(spIdK, "");    //Commented because of the shortcode didn't display issue with the SMS sla
		query.put(keywordK, "");
		query.put(shortcodeK, shortcode);
		query.put(ncsTypeK, ncsType);
		query.put(operatorK, operator);

		return (Boolean) collection.findOne(query).toMap().get(exclusiveK);
	}

	@Override
	public void terminate(String appId, String operator) {
		DBObject query = new QueryBuilder().put(appIdK).is(appId).put(ncsTypeK).is(ncsType).put(operatorK).is(operator)
				.get();
		DBCursor dbCursor = mongoTemplate.getCollection(RK_COLLECTION).find(query);
		while (dbCursor.hasNext()) {
			DBObject o = dbCursor.next();
			mongoTemplate.getCollection(TERMINATED_RK_COLLECTION).save(o);
			mongoTemplate.getCollection(RK_COLLECTION).remove(o);
		}
	}

	@Override
	public List<Map<String, Object>> findTerminatedRoutingKeys(String appId, String operator) {
		DBObject query = new QueryBuilder().put(appIdK).is(appId).put(ncsTypeK).is(ncsType).put(operatorK).is(operator)
				.get();
		return doFind(mongoTemplate.getCollection(TERMINATED_RK_COLLECTION), query);
	}

	@Override
	public void delete(String appId, String operator) {
		List<Map<String, Object>> routingKeys = findRoutingKeys(appId, operator);
		deleteRoutingKeys(routingKeys);
	}

	@Override
	public void delete(String appId) {
		List<Map<String, Object>> routingKeys = findRoutingKeysByAppId(appId);
		deleteRoutingKeys(routingKeys);
	}

	private List<Map<String, Object>> findRoutingKeysByAppId(String appId) {
		DBCollection collection = mongoTemplate.getCollection(RK_COLLECTION);

		DBObject query = new QueryBuilder().put(appIdK).is(appId).put(ncsTypeK).is(ncsType).get();
		doFind(collection, query);

		return doFind(collection, query);
	}

	private void deleteRoutingKeys(List<Map<String, Object>> routingKeys) {
		for (Map<String, Object> routingKey : routingKeys) {
			if ((Boolean) routingKey.get(exclusiveK)) {
				routingKey.put(spIdK, "");
				routingKey.put(appIdK, "");
				update(routingKey);
				logger.debug("update the routing key after removing sp-id and app-id. {}", routingKey);
			} else {
				logger.debug("Delete routing key {}", routingKey);
				delete(routingKey);
			}
		}
	}

	private void delete(Map<String, Object> routingKey) {
		mongoTemplate.getCollection(RK_COLLECTION).remove(new BasicDBObject(routingKey));
	}

	private void update(Map<String, Object> routingKey) {
		DBObject query = new QueryBuilder().put(idK).is(routingKey.remove(idK)).get();
		mongoTemplate.getCollection(RK_COLLECTION).update(query, new BasicDBObject(routingKey));
	}

	private List<Map<String, Object>> doFind(DBCollection collection, DBObject query) {

		DBCursor dbCursor = collection.find(query);

		List<Map<String, Object>> rks = new ArrayList<Map<String, Object>>();
		while (dbCursor.hasNext()) {
			rks.add(dbCursor.next().toMap());
		}
		return rks;
	}

	private Set<String> doFindShortcodes(String operator, boolean exclusive) {
		DBCollection collection = mongoTemplate.getCollection(RK_COLLECTION);

		BasicDBObject query = new BasicDBObject();
		query.put(operatorK, operator);
		query.put(exclusiveK, exclusive);
		query.put(ncsTypeK, this.ncsType);

		DBCursor dbCursor = collection.find(query);

		Set<String> shortcodes = new HashSet<String>();

		while (dbCursor.hasNext()) {
			DBObject next = dbCursor.next();
			shortcodes.add((String) next.get(shortcodeK));
		}

		return shortcodes;
	}

	public void setMongoTemplate(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}

	public void setNcsType(String ncsType) {
		this.ncsType = ncsType;
	}

	@Override
	public Map<String, Object> findSharedShortCodeRoutingKey(String operator, String shortcode, String keyword) {
		DBCollection collection = mongoTemplate.getCollection(RK_COLLECTION);

		BasicDBObject query = new BasicDBObject();
		query.put(operatorK, operator);
		query.put(shortcodeK, shortcode);
		query.put(keywordK, keyword);
		query.put(ncsTypeK, this.ncsType);
		query.put(exclusiveK, false);
		query.put(spIdK, new BasicDBObject("$ne", ""));
		return excuteQuery(collection, query);
	}

	@Override
	public Map<String, Object> findExclusiveShortCodeRoutingKey(String operator, String shortcode) {
		DBCollection collection = mongoTemplate.getCollection(RK_COLLECTION);

		BasicDBObject query = new BasicDBObject();
		query.put(operatorK, operator);
		query.put(shortcodeK, shortcode);
		query.put(ncsTypeK, this.ncsType);
		query.put(exclusiveK, true);
		query.put(spIdK, new BasicDBObject("$ne", ""));
		return excuteQuery(collection, query);
	}

	private Map<String, Object> excuteQuery(DBCollection collection, BasicDBObject query) {
		logger.trace("Query[{}]", query.toString());

		DBCursor dbCursor = collection.find(query);

		if (!dbCursor.hasNext()) {
			return null;
		}

		return (Map<String, Object>) dbCursor.next();
	}

    @Override
    public List<Map<String, Object>> findAllRoutingKeysForOperatorWithCategories(Map<String, Object> reqObj) {
        logger.debug("Find routing keys for request [{}]", reqObj);
        DBCollection collection = mongoTemplate.getCollection(RK_COLLECTION);

        BasicDBObject query = new BasicDBObject(reqObj);
        query.put(spIdK, "");
        query.put(keywordK, "");
        query.put(ncsTypeK, this.ncsType);

        List<Map<String, Object>> rks = doFind(collection, query);

        logger.debug("Routing keys for request [{}] is [{}]", reqObj, rks);
        return rks;
    }
}