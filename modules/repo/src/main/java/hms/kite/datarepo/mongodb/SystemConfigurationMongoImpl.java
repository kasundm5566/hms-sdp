/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo.mongodb;

import static hms.kite.util.KiteKeyBox.idK;
import hms.kite.datarepo.SystemConfiguration;

import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SystemConfigurationMongoImpl implements SystemConfiguration {

    private MongoTemplate template;
    private static final String uniqueIdCollectionName = "unique_id_collection";
    private static final String serviceKeywordsCollectionName = "service_keywords";
    private static final String systemConfigurationCollection = "system_configurations";
    private static final String SDP_UTIL_VALUE_KEY = "value";

    public SystemConfigurationMongoImpl () {}

    public SystemConfigurationMongoImpl (MongoTemplate template) {
        this.template = template;
    }

    @Override
    public boolean isServiceKeyword(String suspectedKeyword) {
        DBCollection dbCollection = template.getCollection(serviceKeywordsCollectionName);
        BasicDBObject query = new BasicDBObject();
        query.put("_id", suspectedKeyword.toUpperCase());
        DBCursor dbCursor = dbCollection.find(query);

        return dbCursor.hasNext();
    }

    /**
     * Generate a unique incremental Id.
     *
     * @param column
     * @return a next integer.
     */
    @Override
    public int incrementAndGetNextId(String column) {
        DBCollection seq = template.getCollection(uniqueIdCollectionName);

        DBObject query = new BasicDBObject();
        query.put("_id", uniqueIdCollectionName);

        DBObject change = new BasicDBObject(column, 1);
        DBObject update = new BasicDBObject("$inc", change);

        DBObject res = seq.findAndModify(query, new BasicDBObject(), new BasicDBObject(), false, update, true, true);
        return (Integer) res.get(column);
    }

    @Override
    public Object find(String key) {
        DBCollection collection = template.getCollection(systemConfigurationCollection);
        DBObject result = collection.findOne(new BasicDBObject(idK, key));
        return (result != null) ? result.get(SDP_UTIL_VALUE_KEY) : result;
    }

    public void setTemplate(MongoTemplate template) {
        this.template = template;
    }
}
