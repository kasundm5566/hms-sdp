/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo.mongodb;

import hms.kite.datarepo.BlacklistRepository;

import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.WriteConcern;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class BlacklistMongoDbRepository extends MongoDbUtils implements BlacklistRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(BlacklistMongoDbRepository.class);
    private static final String BL_COLLECTION = "blacklist";
    private static final String BL_COLUMN = "bl";
    private static final String BL_ID = "blid";

    @Override
    public boolean isExist(String blacklistId, String... numbers) {
        DBCollection dbCollection = mongoTemplate.getCollection(BL_COLLECTION);
        BasicDBObject query = new BasicDBObject();
        query.put(BL_ID, blacklistId);
        DBObject dbObject = dbCollection.findOne(query);
        if (dbObject == null) {
            return false;
        }

        Pattern blackListPattern = (Pattern) dbObject.get(BL_COLUMN);
        LOGGER.debug("Retrieved object : {}", blackListPattern);
        for (String number : numbers) {
            if (blackListPattern.matcher(number).matches()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void createBlacklist(String blackListId, String regex) {
        LOGGER.debug("Creating blacklist [id = " + blackListId + "]");
        BasicDBObject document = new BasicDBObject();
        document.put(BL_ID, blackListId);
        document.put(BL_COLUMN, Pattern.compile(regex, Pattern.CASE_INSENSITIVE));
        BasicDBObject indexes = new BasicDBObject();
        indexes.put(BL_ID, 1);
        persist(document, BL_COLLECTION, "BL_INDX", indexes);
    }

    public void removeAllBlacklists() {
        DBCollection dbCollection = mongoTemplate.getCollection(BL_COLLECTION);
        dbCollection.drop();
    }

}
