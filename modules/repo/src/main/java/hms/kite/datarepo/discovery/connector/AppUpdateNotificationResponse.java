package hms.kite.datarepo.discovery.connector;

import java.util.Map;

/**
 * Created by kasun on 7/4/18.
 */
public class AppUpdateNotificationResponse {
    private String statusCode;
    private String statusDescription;
    private Map<String, String> additionalParameters;

    public AppUpdateNotificationResponse() {
    }

    public AppUpdateNotificationResponse(String statusCode, String statusDescription, Map<String, String> additionalParameters) {
        this.statusCode = statusCode;
        this.statusDescription = statusDescription;
        this.additionalParameters = additionalParameters;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public Map<String, String> getAdditionalParameters() {
        return additionalParameters;
    }

    public void setAdditionalParameters(Map<String, String> additionalParameters) {
        this.additionalParameters = additionalParameters;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("AppUpdateNotificationResponse{");
        sb.append("statusCode='").append(statusCode).append('\'');
        sb.append(", statusDescription='").append(statusDescription).append('\'');
        sb.append(", additionalParameters=").append(additionalParameters);
        sb.append('}');
        return sb.toString();
    }
}
