/*
 * (C) Copyright 1996-2012 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.datarepo;

import com.mongodb.gridfs.GridFSDBFile;

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

public interface BuildFileRepository {

    Map<String, Object> getDevicesAndPlatformsByAppId(String appId);

    Map<String, Object> getDevicesAndPlatformsByAppIdList(List<String> appIdList, String platform, String version);

    List<Map<String, Object>> findByAppId(String appId);

    Map<String, Object> findByBuildFileId(String fileId);

    List<Map<String, Object>> search(Map<String, Object> query);

    int getBuildFilesCount(Map<String, Object> query);

    List<Map<String, Object>> findBuildFilesRange(Map<String, Object> queryCriteria, int skip, int limit);

    GridFSDBFile getBuildFile(String fileId);

    void closeGridFsFileOutputStream(String fileId);

    Map<String, Object> create(Map<String, Object> buildFileInfo);

    void update(Map<String, Object> buildFileInfo);

    void delete(Object id);

    void deleteByAppId(Object appId);

    OutputStream createGridFsFileOutputStream(String fileId, String filename);
}
