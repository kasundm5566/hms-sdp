/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.datarepo.mongodb;

import com.mongodb.*;
import hms.kite.datarepo.AppRepository;
import hms.kite.util.KiteErrorBox;
import hms.kite.util.KiteKeyBox;
import hms.kite.util.SdpException;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.*;
import java.util.regex.Pattern;

import static hms.kite.util.KiteErrorBox.appNameAlreadyAssignedErrorCode;
import static hms.kite.util.KiteErrorBox.appNotFoundErrorCode;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class AppMongoRepository implements AppRepository {

    public static final String appCollectionName = "app";

    private static MongoTemplate mongoTemplate;

    @Override
    public Map<String, Object> findById(String id) {
        HashMap<String, String> queryCriteria = new HashMap<String, String>();
        queryCriteria.put(KiteKeyBox.idK, id);
        return findOneApp(queryCriteria);
    }

    @Override
    public Map<String, Object> findByAppId(String appid) {
        HashMap<String, String> queryCriteria = new HashMap<String, String>();
        queryCriteria.put(KiteKeyBox.appIdK, appid);
        return findOneApp(queryCriteria);
    }

    @Override
    public Map<String, Object> findByAppName(String appName) {
        HashMap<String, String> queryCriteria = new HashMap<String, String>();
        queryCriteria.put(KiteKeyBox.nameK, appName);
        return findOneApp(queryCriteria);
    }

    @Override
    public int countApps(Map<String, Object> queryCriteria) {
        return mongoTemplate.getCollection(appCollectionName).find(new BasicDBObject(queryCriteria)).count();
    }

    @Override
    public List<Map<String, Object>> findAppsByStatusAndActiveProductionTime(String status, Date date) {
        return findAppsFromQuery(new QueryBuilder()
                .put(statusK).is(status)
                .put(activeProductionStartDateK).lessThanEquals(date).get());
    }

    @Override
    public List<Map<String, Object>> findBySpId(String spid) {
        HashMap<String, Object> queryCriteria = new HashMap<String, Object>();
        queryCriteria.put(KiteKeyBox.spIdK, spid);
        return findApps(queryCriteria);
    }

    private Map<String, Object> findOneApp(Map<String, String> queryCriteria) {
        DBCollection appCollection = mongoTemplate.getCollection(appCollectionName);
        BasicDBObject query = new BasicDBObject();
        query.putAll(queryCriteria);
        DBObject result = appCollection.findOne(query);
        if (result == null) {
            throw new SdpException(appNotFoundErrorCode);
        }
        return new HashMap<String, Object>(result.toMap());
    }

    private List<Map<String, Object>> findApps(Map<String, Object> queryCriteria) {
        BasicDBObject query = new BasicDBObject();
        query.putAll(queryCriteria);
        return findAppsFromQuery(query);
    }

    private List<Map<String, Object>> findAppsFromQuery(DBObject query) {
        DBCollection appCollection = mongoTemplate.getCollection(appCollectionName);
        DBCursor dbCursor = appCollection.find(query);
        List<Map<String, Object>> apps = new LinkedList<Map<String, Object>>();
        while (dbCursor.hasNext()) {
            apps.add((HashMap<String, Object>) dbCursor.next().toMap());
        }
        return apps;

    }

    @Override
    public List<Map<String, Object>> findAppsRange(Map<String, Object> queryCriteria, int skip, int limit) {
        DBCollection appCollection = mongoTemplate.getCollection(appCollectionName);
        BasicDBObject orderBy = new BasicDBObject();
        if (queryCriteria.containsKey(orderByK)) {
            orderBy.putAll((Map) queryCriteria.remove(orderByK));
        }
        BasicDBObject query = new BasicDBObject();
        query.putAll(queryCriteria);
        DBCursor dbCursor = appCollection.find(query).sort(orderBy).skip(skip).limit(limit);
        List<Map<String, Object>> apps = new LinkedList<Map<String, Object>>();
        while (dbCursor.hasNext()) {
            apps.add((HashMap<String, Object>) dbCursor.next().toMap());
        }
        return apps;
    }

    @Override
    public List<Map<String, Object>> findBySpId(String spid, String appType) {
        HashMap<String, Object> queryCriteria = new HashMap<String, Object>();
        queryCriteria.put(KiteKeyBox.spIdK, spid);
        queryCriteria.put(KiteKeyBox.appTypeK, appType);
        return findApps(queryCriteria);
    }

    @Override
    public List<Map<String, Object>> findByCoopUserId(String coopId, String appType) {
        HashMap<String, Object> queryCriteria = new HashMap<String, Object>();
        queryCriteria.put(KiteKeyBox.coopUserIdK, coopId);
        queryCriteria.put(KiteKeyBox.appTypeK, appType);
        return findApps(queryCriteria);
    }

    @Override
    public List<Map<String, Object>> search(String appName) {
        HashMap<String, Object> queryCriteria = new HashMap<String, Object>();
        queryCriteria.put(KiteKeyBox.nameK, Pattern.compile(appName, Pattern.CASE_INSENSITIVE));
        return findApps(queryCriteria);
    }

    @Override
    public void create(Map<String, Object> app) {
        if (app.get(appIdK) == null) {
            throw new IllegalStateException("App id required to save the app.");
        } else if (app.get(nameK) == null) {
            throw new IllegalStateException("App name required to save the app.");
        } else if (isAppIdExists((String) app.get(appIdK))) {
            throw new SdpException(appNameAlreadyAssignedErrorCode, "App ID already exist");
        } else if (isAppNameExists((String) app.get(appIdK), (String) app.get(idK))) {
            throw new SdpException(appNameAlreadyAssignedErrorCode, "App Name already exist");
        }

        app.put(idK, app.get(appIdK));

        mongoTemplate.getCollection(appCollectionName).save(new BasicDBObject(app));
    }

    @Override
    public void delete(String appId) {
        mongoTemplate.getCollection(appCollectionName).remove(new QueryBuilder().put(appIdK).is(appId).get());
    }

    @Override
    public void deleteAll() {
        mongoTemplate.getCollection(appCollectionName).drop();
    }

    @Override
    public void update(Map<String, Object> app) {
        mongoTemplate.getCollection(appCollectionName).save(new BasicDBObject(app));
    }

    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public List<Map<String, Object>> findAppsByStatus(String status) {
        List<Map<String, Object>> appList = new ArrayList<Map<String, Object>>();
        BasicDBObject query = new BasicDBObject();
        HashMap<String, String> queryCriteria = new HashMap<String, String>();
        queryCriteria.put(statusK, status);
        query.putAll(queryCriteria);
        DBCursor result = mongoTemplate.getCollection(appCollectionName).find(query);
        while (result.hasNext()) {
            appList.add(new HashMap<String, Object>(result.next().toMap()));
        }
        return appList;
    }

    @Override
    public String findAppStatus(String appId) {
        DBCollection appCollection = mongoTemplate.getCollection(appCollectionName);

        BasicDBObject query = new BasicDBObject();
        query.put(KiteKeyBox.appIdK, appId);

        DBObject fields = new BasicDBObject();
        fields.put(statusK, 1);

        DBObject result = appCollection.findOne(query, fields);
        if (result == null) {
            throw new SdpException(KiteErrorBox.appNotFoundErrorCode,
                    "Application for app-id[" + appId + "] not found.");
        }
        return result.get(statusK).toString();
    }

    @Override
    public boolean isAppNameExists(String appName, String appId) {
        DBObject query = new QueryBuilder().put(nameK).is(appName).put(appIdK).notEquals(appId).get();
        DBObject result = mongoTemplate.getCollection(appCollectionName).findOne(query);
        return result != null;
    }

    @Override
    public boolean isAppNameExists(String appName) {
        DBObject nameLikeQuery = new QueryBuilder().put("$regex").is("^" + appName + "$").put("$options").is("\\i").get();
        DBObject query = new QueryBuilder().put(nameK).is(nameLikeQuery).get();
        DBObject result = mongoTemplate.getCollection(appCollectionName).findOne(query);
        return result != null;
    }

    @Override
    public boolean isAppIdExists(String id) {
        DBObject query = new QueryBuilder().put(appIdK).is(id).get();
        DBObject result = mongoTemplate.getCollection(appCollectionName).findOne(query);
        return result != null;
    }
}
