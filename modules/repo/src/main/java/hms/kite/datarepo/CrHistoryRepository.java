/**
 * (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 * All Rights Reserved.
 * <p/>
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 * International (pvt) Limited.
 * <p/>
 * hSenid International (pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.kite.datarepo;

import java.util.Map;

/**
 * Repository interface for the Provisioning CR History Flow
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public interface CrHistoryRepository {

    void create(Map<String, Object> app);
}
