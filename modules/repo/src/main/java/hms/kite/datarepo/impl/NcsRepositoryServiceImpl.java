package hms.kite.datarepo.impl;

import com.mongodb.DBCursor;
import hms.kite.datarepo.NcsRepository;
import hms.kite.datarepo.NcsRepositoryService;
import hms.kite.datarepo.RoutingKeyRepository;
import hms.kite.datarepo.audit.AuditTrail;
import hms.kite.datarepo.mongodb.NcsMongoRepository;
import hms.kite.util.SystemUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static hms.kite.datarepo.RepositoryServiceRegistry.ncsRepositoryService;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class NcsRepositoryServiceImpl implements NcsRepositoryService {

    private static final Logger logger = LoggerFactory.getLogger(NcsRepositoryServiceImpl.class);

    private Map<String, RoutingKeyRepository> routingKeyRepositories;
    private NcsMongoRepository ncsMongoRepository;

    /**
     * Create routing key and ncs in db.
     *
     * @param sla  -
     * @param spId - id of service provider to be saved with routing key
     */
    public void createNcs(Map<String, Object> sla, String spId) {

        List<Map<String, Object>> routingKeys = (List<Map<String, Object>>) sla.remove(routingKeysK);
        List<Map<String, Object>> updatedRoutingKeys = new ArrayList<Map<String, Object>>();

        if (routingKeys == null || routingKeys.isEmpty()) {
            logger.info("No Routing key round in sla to create");
        } else {
            logger.info("Delete existing routing keys for this application");

            routingKeyRepositories.get(sla.get(ncsTypeK)).delete((String) sla.get(appIdK), (String) sla.get(operatorK));

            logger.info("Creating new {} routing keys {}.", routingKeys.size(), routingKeys);

            for (Map<String, Object> routingKey : routingKeys) {
                Map<String, Object> updatedRk = routingKeyRepositories.get(sla.get(ncsTypeK)).registerRoutingKey(spId, (String) sla.get(appIdK),
                        (String) sla.get(operatorK), (String) routingKey.get(shortcodeK), (String) routingKey.get(keywordK));
                updatedRoutingKeys.add(updatedRk);
            }
        }

        if (sla.get(operatorK) == null) {
            sla.put(operatorK, "");
        }

        Date currentDate = new Date();
        String appId = sla.get(appIdK).toString();
        String operator = "";
        String ncsType = sla.get(ncsTypeK).toString();
        Map<String, Object> isNcsExist = ncsRepositoryService().findByAppIdOperatorNcsType(appId, operator, ncsType);
        if (isNcsExist == null) {
            sla.put(createdDateK, currentDate);
            sla.put(createdByK, AuditTrail.getCurrentUsername());
        }
        sla.put(updatedDateK, currentDate);
        sla.put(updatedByK, AuditTrail.getCurrentUsername());

        ncsMongoRepository.create(sla);

        sla.put(routingKeysK, updatedRoutingKeys);
        AuditTrail.getInstance().auditNcs(sla, createK);
    }

    public void terminateNcs(Map<String, Object> sla) {
        if (SystemUtil.isRkAvailableForNcsType((String) sla.get(ncsTypeK))) {
            routingKeyRepositories.get(sla.get(ncsTypeK)).terminate((String) sla.get(appIdK), (String) sla.get(operatorK));
        }
        sla.put(statusK, terminateK);
        update(sla);
    }

    private Map<String, Map<String, Object>> attacheRksAndAggregateSlas(List<Map<String, Object>> slas) {

        Map<String, Map<String, Object>> result = new HashMap<String, Map<String, Object>>();

        for (Map<String, Object> sla : slas) {
            attacheRks(sla);
            String ncsSlaKey = "";
            if (sla.get(operatorK) != null && ((String) sla.get(operatorK)).trim().length() != 0) {
                ncsSlaKey = sla.get(operatorK) + "-";
            }
            ncsSlaKey += (String) sla.get(ncsTypeK);
            result.put(ncsSlaKey, sla);
        }

        return result;
    }

    private Map<String, Object> attacheRks(Map<String, Object> ncsSla) {
        if (ncsSla != null && SystemUtil.isRkAvailableForNcsType((String) ncsSla.get(ncsTypeK))) {
            List<Map<String, Object>> routingKeys;
            if (terminateK.equals(ncsSla.get(statusK))) {
                routingKeys = routingKeyRepositories.get(ncsSla.get(ncsTypeK)).findTerminatedRoutingKeys((String) ncsSla.get(appIdK), (String) ncsSla.get(operatorK));
            } else {
                routingKeys = routingKeyRepositories.get(ncsSla.get(ncsTypeK)).findRoutingKeys((String) ncsSla.get(appIdK), (String) ncsSla.get(operatorK));
            }
            ncsSla.put(routingKeysK, routingKeys);
        }

        return ncsSla;
    }

    private List<Map<String, Object>> attacheRks(List<Map<String, Object>> slas) {
        for (Map<String, Object> sla : slas) {
            attacheRks(sla);
        }
        return slas;
    }

    public Map<String, Object> findNcs(String appId, String ncsType, String operator) {
        return attacheRks(ncsMongoRepository.findByAppIdOperatorNcsType(appId, operator, ncsType));
    }

    public Map<String, Map<String, Object>> findByAppId(String appId) {
        return attacheRksAndAggregateSlas(ncsMongoRepository.findByAppId(appId));
    }

    @Override
    public List<Map<String, Object>> findByAppIdNcsType(String appid, String ncsType) {
        return attacheRks(ncsMongoRepository.findByAppIdNcsType(appid, ncsType));
    }

    @Override
    public List<Map<String, Object>> listSlas(DBCursor dbCursor) {
        return attacheRks(ncsMongoRepository.listSlas(dbCursor));
    }

    @Override
    public Map<String, Object> findOperatorSla(Map<String, Object> operator) {
        return attacheRks(ncsMongoRepository.findOperatorSla(operator));
    }

    @Override
    public Map<String, Object> findByAppIdOperatorNcsType(String appId, String operator, String ncsType) {
        return attacheRks(ncsMongoRepository.findByAppIdOperatorNcsType(appId, operator, ncsType));
    }

    @Override
    public boolean isTillNumberExists(String tillNumber) {
        return ncsMongoRepository.isTillNumberExists(tillNumber);
    }

    @Override
    public void update(String appId, String ncsType, String operator, String status) {
        Map<String, Object> sla = findByAppIdOperatorNcsType(appId, operator, ncsType);

        if (sla == null) {
            logger.debug("No Sla found to update with app-id[{}], ncs-type[{}], operator-[{}]. Discard updated call",
                    new Object[]{appId, ncsType, operator});
            return;
        }

        if (terminateK.equals(status)) {
            sla.put(statusK, status);
            terminateNcs(sla);
        } else if (suspendK.equals(status)) {
            sla.put(previousStateK, sla.remove(statusK));
            sla.put(statusK, suspendK);
            update(sla);
        } else if (restoreK.equals(status)) {
            sla.put(statusK, sla.remove(previousStateK));
            update(sla);
        } else if (deleteK.equals(status)) {
            delete(appId, ncsType, operator);
        } else {
            sla.put(statusK, status);
            update(sla);
        }
        AuditTrail.getInstance().auditNcs(sla, deleteK.equals(status) ? deleteK : updateK);
    }

    @Override
    public void delete(String appId, String ncsType, String operator) {
        //todo transaction.....
        if (SystemUtil.isRkAvailableForNcsType(ncsType)) {
            routingKeyRepositories.get(ncsType).delete(appId, operator);
        }
        ncsMongoRepository.delete(appId, ncsType, operator);
    }

    @Override
    public void delete(String appId) {
        ncsMongoRepository.delete(appId);
    }

    @Override
    public void update(Map<String, Object> sla) {
        Object rks = sla.remove(routingKeysK);//not my business
        Date updatedDate = new Date();
        sla.put(updatedDateK, updatedDate);
        sla.put(updatedByK, AuditTrail.getCurrentUsername());
        ncsMongoRepository.update(sla);
        sla.put(routingKeysK, rks);
    }

    @Override
    public void deleteAll() {
        ncsMongoRepository.deleteAll();
    }

    @Override
    public List<Map<String, Object>> filter(Map<String, Object> filterMap) {
        return ncsMongoRepository.filter(filterMap);
    }

    public void setRoutingKeyRepositories(Map<String, RoutingKeyRepository> routingKeyRepositories) {
        this.routingKeyRepositories = routingKeyRepositories;
    }

    public void setNcsMongoRepository(NcsMongoRepository ncsMongoRepository) {
        this.ncsMongoRepository = ncsMongoRepository;
    }
}
