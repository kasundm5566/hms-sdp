package hms.kite.datarepo.discovery.connector;

/**
 * Created by kasun on 7/4/18.
 */
public interface DiscoveryServiceConnector {
    AppUpdateNotificationResponse sendAppUpdateNotification(AppUpdateNotificationRequest request);
}
