package hms.kite.datarepo.discovery.connector;

/**
 * Created by kasun on 7/4/18.
 */
public class AppUpdateNotificationRequest {
    private String requestId;
    private String appId;
    private String appName;
    private String buildVersion;
    private String status;
    private String dispatchType;

    public AppUpdateNotificationRequest() {
    }

    public AppUpdateNotificationRequest(String requestId, String appId, String appName, String buildVersion, String status, String dispatchType) {
        this.requestId = requestId;
        this.appId = appId;
        this.appName = appName;
        this.buildVersion = buildVersion;
        this.status = status;
        this.dispatchType = dispatchType;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getBuildVersion() {
        return buildVersion;
    }

    public void setBuildVersion(String buildVersion) {
        this.buildVersion = buildVersion;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDispatchType() {
        return dispatchType;
    }

    public void setDispatchType(String dispatchType) {
        this.dispatchType = dispatchType;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("AppUpdateNotificationRequest{");
        sb.append("requestId='").append(requestId).append('\'');
        sb.append(", appId='").append(appId).append('\'');
        sb.append(", appName='").append(appName).append('\'');
        sb.append(", buildVersion='").append(buildVersion).append('\'');
        sb.append(", status='").append(status).append('\'');
        sb.append(", dispatchType='").append(dispatchType).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
