/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.datarepo.common.util;

import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CommonUtil {

    public static boolean isCollectionExists(MongoTemplate mongoTemplate, String collectionName) {
        return mongoTemplate.getCollectionNames().contains(collectionName);
    }
}
