/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo.impl;

import hms.kite.datarepo.DeviceAndPlatformRepositoryService;
import hms.kite.datarepo.DeviceRepository;
import hms.kite.datarepo.PlatformRepository;

import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: sanjeewa
 * Date: 3/25/12
 * Time: 6:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class DeviceAndPlatformServiceImpl implements DeviceAndPlatformRepositoryService {

    private DeviceRepository deviceRepository;
    private PlatformRepository platformRepository;

    @Override
    public List<Map<String, Object>> findAllDevices() {
        return deviceRepository.findAll();
    }

    @Override
    public List<Map<String, Object>> findAllPlatforms() {
        return platformRepository.findAll();
    }

    public void setDeviceRepository(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

    public void setPlatformRepository(PlatformRepository platformRepository) {
        this.platformRepository = platformRepository;
    }
}
