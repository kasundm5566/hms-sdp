/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.datarepo.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import hms.kite.datarepo.SpRepository;
import hms.kite.util.SdpException;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteErrorBox.spNotAvailableErrorCode;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class SpMongoRepository implements SpRepository {

    private MongoTemplate mongoTemplate;

    /**
     * Create the sp in db. Add the sp-id as the collection-entry id.
     *
     * @param sp - sp details
     * @throws IllegalStateException - if the sp-id is not found in the sp.
     */
    @Override
    public void create(Map<String, Object> sp) {
        if (sp.get(spIdK) == null) {
            throw new IllegalStateException("sp-id required in the sp request.");
        }
        sp.put(idK, sp.get(spIdK));
        mongoTemplate.getCollection(SP_COLLECTION_NAME).save(new BasicDBObject(sp));
    }

    @Override
    public List<Map<String, Object>> findAllSp(int skip, int limit) {
        List<Map<String, Object>> spList = new ArrayList<Map<String, Object>>();

        DBCursor result = mongoTemplate.getCollection(SP_COLLECTION_NAME).find().skip(skip).limit(limit);

        while (result.hasNext()) {
            spList.add(new HashMap<String, Object>(result.next().toMap()));
        }

        return spList;
    }

    @Override
    public Map<String, Object> findSpById(String id) {
        HashMap<String, Object> queryCriteria = new HashMap<String, Object>();
        queryCriteria.put(idK, id);
        return findOneSp(queryCriteria);
    }

    @Override
    public Map<String, Object> findSpByName(String name) {
        HashMap<String, Object> queryCriteria = new HashMap<String, Object>();
        queryCriteria.put(nameK, name);
        return findOneSp(queryCriteria);
    }

    @Override
    public Map<String, Object> findSpByCoopUserId(String coopUserId) {
        Map<String, Object> queryCriteria = new HashMap<String, Object>();
        queryCriteria.put(coopUserIdK, coopUserId);
        return findOneSp(queryCriteria);
    }

    @Override
    public Map<String, Object> findSpByCoopUserName(String name) {
        HashMap<String, Object> queryCriteria = new HashMap<String, Object>();
        queryCriteria.put(coopUserNameK, name);
        return findOneSp(queryCriteria);
    }

    private Map<String, Object> findOneSp(Map queryCriteria) {
        BasicDBObject query = new BasicDBObject();
        query.putAll(queryCriteria);

        DBObject result = mongoTemplate.getCollection(SP_COLLECTION_NAME).findOne(query);

        if (result == null) {
            throw new SdpException(spNotAvailableErrorCode);
        } else {
            return new HashMap<String, Object>(result.toMap());
        }
    }

    @Override
    public Map<String, Object> findSpBySpId(String spId) {
        Map<String, Object> queryCriteria = new HashMap<String, Object>();
        queryCriteria.put(spIdK, spId);
        return findOneSp(queryCriteria);
    }

    @Override
    public List<Map<String, Object>> findSpsByStatus(String status) {
        List<Map<String, Object>> spList = new ArrayList<Map<String, Object>>();

        BasicDBObject query = new BasicDBObject();
        query.put(statusK, status);

        DBCursor result = mongoTemplate.getCollection(SP_COLLECTION_NAME).find(query);

        while (result.hasNext()) {
            spList.add(new HashMap<String, Object>(result.next().toMap()));
        }

        return spList;
    }

    @Override
    public List<Map<String, Object>> findSpsByStatus(String status, int skip, int limit) {
        List<Map<String, Object>> spList = new ArrayList<Map<String, Object>>();

        BasicDBObject query = new BasicDBObject();
        query.put(statusK, status);

        DBCursor result = mongoTemplate.getCollection(SP_COLLECTION_NAME).find(query).skip(skip).limit(limit);

        while (result.hasNext()) {
            spList.add(new HashMap<String, Object>(result.next().toMap()));
        }

        return spList;
    }

    @Override
    public int countSpsByStatus(String status) {
        HashMap<String, Object> queryCriteria = new HashMap<String, Object>();
        queryCriteria.put(statusK, status);

        return mongoTemplate.getCollection(SP_COLLECTION_NAME).find(new BasicDBObject(queryCriteria)).count();
    }

    @Override
    public int countAllSps() {
        return mongoTemplate.getCollection(SP_COLLECTION_NAME).find().count();
    }

    @Override
    public void update(Map<String, Object> sp) {
        mongoTemplate.getCollection(SP_COLLECTION_NAME).save(new BasicDBObject(sp));
    }

    @Override
    public void deleteSpById(String id) {
        Map<String, Object> sp = findSpById(id);
        mongoTemplate.getCollection(SP_COLLECTION_NAME).remove(new BasicDBObject(sp));
    }

    @Override
    public void deleteAll() {
        mongoTemplate.getCollection(SP_COLLECTION_NAME).drop();
    }

    @Override
    public List<Map<String, Object>> findAllSpBySpQueryCriteria(Map<String, Object> queryCriteria, int skip, int limit) {
        BasicDBObject orderBy = new BasicDBObject();
        if (queryCriteria.containsKey(orderByK)) {
            orderBy.putAll((Map) queryCriteria.remove(orderByK));
        }

        BasicDBObject query = new BasicDBObject();
        query.putAll(queryCriteria);

        DBCursor result = mongoTemplate.getCollection(SP_COLLECTION_NAME).find(query).sort(orderBy).skip(skip).limit(limit);

        List<Map<String, Object>> spList = new ArrayList<Map<String, Object>>();

        while (result.hasNext()) {
            spList.add(new HashMap<String, Object>(result.next().toMap()));
        }

        return spList;
    }

    @Override
    public int countSpBySpQueryCriteria(Map<String, Object> query) {
        BasicDBObject dbQuery = new BasicDBObject();
        dbQuery.putAll(query);

        return mongoTemplate.getCollection(SP_COLLECTION_NAME).find(dbQuery).count();
    }

    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
}
