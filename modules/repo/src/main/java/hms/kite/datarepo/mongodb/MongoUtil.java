/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.datarepo.mongodb;

import java.util.HashMap;
import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class MongoUtil {

    public static Map<String, String> createLikeQuery(String string) {
        Map<String, String> query = new HashMap<String, String>();
        query.put("$regex", ".*" + string + ".*");
        query.put("$options", "\\i");
        return query;
    }
}
