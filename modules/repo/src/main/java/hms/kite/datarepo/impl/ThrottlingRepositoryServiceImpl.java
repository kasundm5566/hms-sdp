/*
*   (C) Copyright 2009-2012 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/

package hms.kite.datarepo.impl;

import hms.kite.datarepo.ThrottlingRepository;
import hms.kite.datarepo.ThrottlingRepositoryService;
import hms.kite.util.ThrottlingType;

/**
 * User: azeem
 * Date: 3/5/12
 * Time: 7:03 PM
 */
public class ThrottlingRepositoryServiceImpl implements ThrottlingRepositoryService {

    private ThrottlingRepository throttlingRepository;

    @Override
    public int increaseThrottling(String throttlingId, int delta, ThrottlingType type) {
        return throttlingRepository.increaseThrottling(throttlingId, delta, type.getValue());
    }

    @Override
    public void resetThrottling(ThrottlingType type) {
        throttlingRepository.resetThrottling(type);
    }

    @Override
    public boolean isNoMessagesInLastSec(String throttlingId, long currentTimeMills, String throttlingType) {
        return throttlingRepository.isNoMessagesInLastSec(throttlingId, currentTimeMills, throttlingType);
    }

    @Override
    public int findAndIncreaseCountForLastSec(String throttlingId, long currentTimeMills, String throttlingType) {
        return throttlingRepository.findAndIncreaseCountForLastSec(throttlingId, currentTimeMills, throttlingType);
    }

    @Override
    public void insertThrottlingEntity(String throttlingId, long lastUpdatedTimeMills, String throttlingType) {
        throttlingRepository.insertThrottlingEntity(throttlingId, lastUpdatedTimeMills, throttlingType);
    }

    public void setThrottlingRepository(ThrottlingRepository throttlingRepository) {
        this.throttlingRepository = throttlingRepository;
    }

}
