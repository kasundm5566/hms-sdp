/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import hms.kite.datarepo.PlatformRepository;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: sanjeewa
 * Date: 3/25/12
 * Time: 4:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class PlatformMongoRepositoryImpl implements PlatformRepository {

    private MongoTemplate mongoTemplate;
    private static final String COLLECTION_NAME = "platform";

    @Override
    public List<Map<String, Object>> findAll() {

        DBCursor dbCursor = mongoTemplate.getCollection(COLLECTION_NAME).find(new BasicDBObject());

        List<Map<String, Object>> platformList = getPlatformList(dbCursor);

        return platformList;
    }

    private List<Map<String, Object>> getPlatformList(DBCursor dbCursor) {
        List<Map<String, Object>> platformList = new LinkedList<Map<String, Object>>();

        while (dbCursor.hasNext()){
            platformList.add(dbCursor.next().toMap());
        }

        return platformList;
    }

    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
}
