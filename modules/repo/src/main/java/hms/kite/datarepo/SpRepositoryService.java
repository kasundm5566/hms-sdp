/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.datarepo;

import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public interface SpRepositoryService {

    void create(Map<String, Object> sp);

    List<Map<String, Object>> findAllSp(int skip, int limit);

    Map<String, Object> findSpById(String id);

    Map<String, Object> findSpByName(String name);

    Map<String, Object> findSpByCoopUserId(String coopUserId);

    Map<String, Object> findSpByCoopUserName(String coopUserName);

    List<Map<String, Object>> findAllSpBySpQueryCriteria(Map<String, Object> queryCriteria, int skip, int limit);

    int countSpBySpQueryCriteria(Map<String, Object> query);

    Map<String, Object> findSpBySpId(String spId);

    List<Map<String, Object>> findSpsByStatus(String status);

    List<Map<String, Object>> findSpsByStatus(String status, int skip, int limit);

    int countSpsByStatus(String status);

    int countAllSps();

    void update(Map<String, Object> sp);

    void deleteSpById(String id);
}
