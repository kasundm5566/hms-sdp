/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo;

import com.mongodb.DBCursor;

import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public interface NcsRepository {

    List<Map<String, Object>> findByAppId(String appid);

    List<Map<String, Object>> findByAppIdNcsType(String appid, String ncsType);

    List<Map<String, Object>> listSlas(DBCursor dbCursor);

    Map<String, Object> findOperatorSla(Map<String, Object> operator);

    Map<String, Object> findByAppIdOperatorNcsType(String appId, String operator, String ncsType);

    boolean isTillNumberExists(String tillNumber);

    void create(Map<String, Object> operatorSmsSla);

    void update(Map<String, Object> operatorSmsSla);

    void deleteAll();

    List<Map<String, Object>>  filter(Map<String, Object> filterMap);
}
