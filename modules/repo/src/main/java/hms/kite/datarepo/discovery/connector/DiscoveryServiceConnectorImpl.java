package hms.kite.datarepo.discovery.connector;

import hms.kite.util.GsonUtil;
import hms.kite.util.SdpException;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.io.InputStreamReader;

import static hms.kite.util.KiteErrorBox.systemErrorCode;

/**
 * Created by kasun on 7/4/18.
 */
public class DiscoveryServiceConnectorImpl implements DiscoveryServiceConnector {

    private static String appUpdateNotificationUrl;
    private static final Logger logger = LoggerFactory.getLogger(DiscoveryServiceConnectorImpl.class);

    @Override
    public AppUpdateNotificationResponse sendAppUpdateNotification(AppUpdateNotificationRequest request) {
        try {
            logger.debug("app update notification request: " + request);
            WebClient webClient = WebClient.create(appUpdateNotificationUrl);
            webClient.header("Content-Type", "application/json");
            webClient.accept("application/json");

            final Response response = webClient.post(GsonUtil.toJson(request));
            AppUpdateNotificationResponse registerAppResponse = readResponse((InputStream) response.getEntity(), AppUpdateNotificationResponse.class);
            logger.debug("app update notification response: " + registerAppResponse.toString());
            return registerAppResponse;
        } catch (Exception e) {
            logger.error("error sending app update notification: ", e);
            throw new SdpException(systemErrorCode, "error sending app update notification: ", e);
        }
    }

    public String getAppUpdateNotificationUrl() {
        return appUpdateNotificationUrl;
    }

    public void setAppUpdateNotificationUrl(String appUpdateNotificationUrl) {
        this.appUpdateNotificationUrl = appUpdateNotificationUrl;
    }

    private <T> T readResponse(InputStream is, Class<T> t) {
        return GsonUtil.getGson().fromJson(new InputStreamReader(is), t);
    }
}
