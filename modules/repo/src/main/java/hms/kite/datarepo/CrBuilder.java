/**
 * (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 * All Rights Reserved.
 * <p/>
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 * International (pvt) Limited.
 * <p/>
 * hSenid International (pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.kite.datarepo;

import java.util.Map;

/**
 * Builder interface for the Provisioning CR Flow
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public interface CrBuilder {

    /**
     * Initializes Service Provider Change Request
     *
     * @param spId
     * @return
     */
    Map<String, Object> createSpCr(String spId);

    /**
     * Initializes Application Change Request
     * @param spId
     * @return
     */
    Map<String, Object> createAppCr(String spId, String appId);

    /**
     * Initializes Ncs Sla Change Request
     * @param ncsType
     * @param operator
     * @param spId
     * @param appId
     * @return
     */
    Map<String, Object> createNcsCr(String ncsType, String operator, String spId, String appId);

}
