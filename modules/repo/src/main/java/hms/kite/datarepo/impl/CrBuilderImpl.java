/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo.impl;

import hms.kite.datarepo.AppRepositoryService;
import hms.kite.datarepo.CrBuilder;
import hms.kite.datarepo.NcsRepositoryService;
import hms.kite.datarepo.SpRepositoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static hms.kite.util.KiteKeyBox.*;

/**
 * Builder class which creates initial provisioning CR requests for the following types
 * 1) SP -CR
 * 2) App -CR
 * 3) NCS_CR
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CrBuilderImpl implements CrBuilder {

    private SpRepositoryService spRepositoryService;
    private AppRepositoryService appRepositoryService;
    private NcsRepositoryService ncsRepositoryService;

    private static final Logger LOGGER = LoggerFactory.getLogger(CrBuilderImpl.class);

    @Override
    public Map<String, Object> createSpCr(String spId) {
        LOGGER.info("Creating initial SP Change Request for sp-id [{}] ", spId);
        Map<String, Object> currentSp;
        Map<String, Object> spCr = new HashMap<String, Object>();
        try {
            currentSp = spRepositoryService.findSpBySpId(spId);
            spCr.put(crTypeK, crSpTypeK);
            setCommonCrReqParams(spId, currentSp, spCr);
            LOGGER.debug("Initial SP CR Request [{}] created successfully", spCr.toString());
        } catch (Exception e) {
            LOGGER.error("Error occurred while creating initial SP change request for sp-id [{}]", spId);
        }
        return spCr;
    }


    @Override
    public Map<String, Object> createAppCr(String spId, String appId) {
        LOGGER.info("Creating initial APP Change Request for sp-id [{}] and app-id [{}]", new Object[]{spId, appId});
        Map<String, Object> currentApp;
        Map<String, Object> appCr = new HashMap<String, Object>();
        try {
            currentApp = appRepositoryService.findByAppId(appId);
            appCr.put(crTypeK, crAppTypeK);
            setCommonCrReqParams(spId, currentApp, appCr);
            appCr.put(appIdK, appId);
            LOGGER.debug("Initial App CR Request [{}] created successfully", appCr.toString());
        } catch (Exception e) {
            LOGGER.error("Error occurred while creating initial App change request for app-id [{}]", appId);
        }
        return appCr;
    }

    @Override
    public Map<String, Object> createNcsCr(String ncsType, String operator, String spId, String appId) {
        LOGGER.info("Creating initial NCS Change Request for ncs-type [{}], operator [{}], sp-id [{}] and app-id [{}]",
                new Object[]{ncsType, operator, spId, appId});
        Map<String, Object> currentNcs;
        Map<String, Object> ncsCr = new HashMap<String, Object>();
        try {
            currentNcs = ncsRepositoryService.findNcs(appId, ncsType, operator);
            ncsCr.put(crTypeK, crNcsTypeK);
            setCommonCrReqParams(spId, currentNcs, ncsCr);
            ncsCr.put(appIdK, appId);
            ncsCr.put(operatorK, operator);
            ncsCr.put(ncsTypeK, ncsType);
            LOGGER.debug("Initial NCS CR Request [{}] created successfully", ncsCr.toString());
        } catch (Exception e) {
            LOGGER.error("Error occurred while creating initial NCS change request for app-id [{}]," +
                    " ncs-id [{}], and operator [{}]", new Object[]{appId, ncsType, operator});
        }
        return ncsCr;
    }


    private void setCommonCrReqParams(String spId, Map<String, Object> currentData, Map<String, Object> crReq) {
        //todo Add CR req ID generator here
        crReq.put(crIdK, String.valueOf(new Random().nextInt(1000)));
        crReq.put(spIdK, spId);
        crReq.put(statusK, initialK);
        crReq.put(startDateK, DateFormat.getDateInstance(DateFormat.SHORT).format(new Date()));
        crReq.put(currentDataK, currentData);
        HashMap<String, Object> updatedData = new HashMap<String, Object>();
        updatedData.putAll(currentData);
        crReq.put(updatedDataK, updatedData);
    }

    public void setSpRepositoryService(SpRepositoryService spRepositoryService) {
        this.spRepositoryService = spRepositoryService;
    }

    public void setAppRepositoryService(AppRepositoryService appRepositoryService) {
        this.appRepositoryService = appRepositoryService;
    }

    public void setNcsRepositoryService(NcsRepositoryService ncsRepositoryService) {
        this.ncsRepositoryService = ncsRepositoryService;
    }
}
