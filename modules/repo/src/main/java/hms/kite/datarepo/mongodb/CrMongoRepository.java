/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo.mongodb;

import static hms.kite.util.KiteErrorBox.crAlreadyAssignedErrorCode;
import static hms.kite.util.KiteErrorBox.crNotAvailableErrorCode;
import static hms.kite.util.KiteErrorBox.crSpAlreadyAssignedErrorCode;
import static hms.kite.util.KiteKeyBox.appIdK;
import static hms.kite.util.KiteKeyBox.crAppTypeK;
import static hms.kite.util.KiteKeyBox.crIdK;
import static hms.kite.util.KiteKeyBox.crNcsTypeK;
import static hms.kite.util.KiteKeyBox.crSpTypeK;
import static hms.kite.util.KiteKeyBox.crTypeK;
import static hms.kite.util.KiteKeyBox.createdByK;
import static hms.kite.util.KiteKeyBox.ncsTypeK;
import static hms.kite.util.KiteKeyBox.operatorK;
import static hms.kite.util.KiteKeyBox.spIdK;
import hms.kite.datarepo.CrRepository;
import hms.kite.datarepo.audit.AuditTrail;
import hms.kite.util.SdpException;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.QueryBuilder;

/**
 * Mongo Repository implementation for handling provisioning CR Requests
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CrMongoRepository implements CrRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(CrMongoRepository.class);
    private static final String COLLECTION_NAME = "cr";
    private MongoTemplate mongoTemplate;

    @Override
    public void create(final Map<String, Object> crReq) {
        if (crReq.get(crTypeK) == null) {
            throw new IllegalStateException("CR type is required to save the Change Request.");
        } else if (crReq.get(crIdK) == null) {
            throw new IllegalStateException("CR ID is required to save the Change Request.");
        } else if (isCrEntryExists(new HashMap<String, Object>(){{put(crIdK, crReq.get(crIdK));}})) {
            throw new SdpException(crAlreadyAssignedErrorCode, "CR ID already exist");
        } else if (((String) crReq.get(crTypeK)).equals(crSpTypeK)) {
            enforceSpCrConstraints(crReq);
        } else if (((String) crReq.get(crTypeK)).equals(crAppTypeK)) {
            enforceAppCrConstraints(crReq);
        } else if (((String) crReq.get(crTypeK)).equals(crNcsTypeK)) {
            enforceNcsCrConstraints(crReq);
        } else {
            throw new IllegalStateException("Unsupported CR Request Found [ " + crReq.toString() + " ]");
        }
        mongoTemplate.getCollection(COLLECTION_NAME).save(new BasicDBObject(crReq));
        //AuditLogTrail.getInstance().auditCr(createdByK, (String) crReq.get(spIdK), (String) crReq.get(crTypeK), createK, new HashMap<String, Object>());
    }

    private void enforceNcsCrConstraints(final Map<String, Object> crReq) {
        if(crReq.get(spIdK) == null) {
            throw new IllegalStateException("SP ID is required to save the NCS Change Request.");
        } else if(crReq.get(appIdK) == null) {
            throw new IllegalStateException("APP ID is required to save the NCS Change Request.");
        } else if(crReq.get(operatorK) == null) {
            throw new IllegalStateException("Operator is required to save the NCS Change Request.");
        } else if(crReq.get(ncsTypeK) == null) {
            throw new IllegalStateException("Ncs Type is required to save the NCS Change Request.");
        } else if(isCrEntryExists(new HashMap<String, Object>(){{put(spIdK, crReq.get(spIdK));
            put(appIdK, crReq.get(appIdK));
            put(operatorK, crReq.get(operatorK));
            put(ncsTypeK, crReq.get(ncsTypeK));
            put(crTypeK, crNcsTypeK);}})) {
            throw new SdpException(crSpAlreadyAssignedErrorCode, "Ongoing NCS CR found for APP ID [ "
                    + crReq.get(appIdK) + " ], NCS [ " + crReq.get(ncsTypeK) + " ] and Operator [ " +
                    crReq.get(operatorK) + " ]");
        }
        //AuditLogTrail.getInstance().auditCr(createdByK, (String) crReq.get(spIdK), (String) crReq.get(crTypeK), "enforce-ncs-cr-constraints", crReq);

    }

    private void enforceAppCrConstraints(final Map<String, Object> crReq) {
        if(crReq.get(spIdK) == null) {
            throw new IllegalStateException("SP ID is required to save the APP Change Request.");
        } else if(crReq.get(appIdK) == null) {
            throw new IllegalStateException("APP ID is required to save the App Change Request.");
        } else if(isCrEntryExists(new HashMap<String, Object>(){{put(spIdK, crReq.get(spIdK));
            put(appIdK, crReq.get(appIdK));
            put(crTypeK, crAppTypeK);}})) {
            throw new SdpException(crSpAlreadyAssignedErrorCode, "Ongoing APP CR found for APP ID [ "
                    + crReq.get(appIdK) + " ]");
        }
        //AuditLogTrail.getInstance().auditCr(createdByK, (String) crReq.get(spIdK), (String) crReq.get(crTypeK), "enforce-app-cr-constraints", crReq);

    }

    private void enforceSpCrConstraints(final Map<String, Object> crReq) {
        if(crReq.get(spIdK) == null) {
            throw new IllegalStateException("SP ID is required to save the SP Change Request.");
        } else if(isCrEntryExists(new HashMap<String, Object>(){{put(spIdK, crReq.get(spIdK));
            put(crTypeK, crSpTypeK);}})) {
            throw new SdpException(crSpAlreadyAssignedErrorCode, "Ongoing SP CR found for SP ID [ "
                    + crReq.get(spIdK) + " ]");
        }
        //AuditLogTrail.getInstance().auditCr(createdByK, (String) crReq.get(spIdK), (String) crReq.get(crTypeK), "enforce-sp-cr-constraints", crReq);
    }

    @Override
    public boolean isCrEntryExists(Map<String, Object> queryCriteria) {
        BasicDBObject query = new BasicDBObject();
        query.putAll(queryCriteria);
        DBObject result = mongoTemplate.getCollection(COLLECTION_NAME).findOne(query);
        return result != null;
    }

    @Override
    public void delete(String crId) {
        mongoTemplate.getCollection(COLLECTION_NAME).remove(new QueryBuilder().put(crIdK).is(crId).get());
        //auditPreviousState(crId, deleteK);
    }

    @Override
    public void deleteAll() {
        mongoTemplate.getCollection(COLLECTION_NAME).drop();
    }

    private void auditPreviousState(String crId, String auditOperation) {
        Map<String, Object> map = findCrByCrId(crId);
        if (map != null) {
            AuditTrail.getInstance().auditCr(createdByK, (String) map.get(spIdK), (String) map.get(crTypeK), auditOperation, map);
        }
    }

    @Override
    public void update(Map<String, Object> crReq) {
        mongoTemplate.getCollection(COLLECTION_NAME).save(new BasicDBObject(crReq));
        //auditPreviousState((String) crReq.get(crIdK), updateK);
    }

    @Override
    public Map<String, Object> findCrByCrId(String crId) {
        HashMap queryCriteria = new HashMap();
        queryCriteria.put(crIdK, crId);
        return findOneCr(queryCriteria);
    }

    @Override
    public List<Map<String, Object>> findCrBySpId(String spId) {
        HashMap<String, Object> queryCriteria = new HashMap<String, Object>();
        queryCriteria.put(spIdK, spId);
        return findCrs(queryCriteria);
    }

    private List<Map<String, Object>> findCrs(Map<String, Object> queryCriteria) {
        DBCollection appCollection = mongoTemplate.getCollection(COLLECTION_NAME);
        BasicDBObject query = new BasicDBObject();
        query.putAll(queryCriteria);
        DBCursor dbCursor = appCollection.find(query);
        List<Map<String, Object>> crs = new LinkedList<Map<String, Object>>();
        while (dbCursor.hasNext()) {
            crs.add((HashMap<String, Object>) dbCursor.next().toMap());
        }
        return crs;

    }

    private Map<String, Object> findOneCr(Map queryCriteria) {
        BasicDBObject query = new BasicDBObject();
        query.putAll(queryCriteria);
        DBObject result = mongoTemplate.getCollection(COLLECTION_NAME).findOne(query);
        if (result == null) {
            throw new SdpException(crNotAvailableErrorCode, "CR with the given ID [ " + queryCriteria.get(crIdK) + " ]" +
                    " is not available") ;
        } else {
            return new HashMap<String, Object>(result.toMap());
        }
    }

    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
}
