/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo.mongodb;

import com.mongodb.*;
import hms.kite.util.SdpException;
import org.springframework.data.mongodb.core.MongoTemplate;

import static hms.kite.util.KiteErrorBox.systemErrorCode;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class MongoDbUtils {

    protected MongoTemplate mongoTemplate;

    private void persist(BasicDBObject document, String collectionName) {
        DBCollection dbCollection = mongoTemplate.getCollection(collectionName);
        final WriteResult result = dbCollection.save(document);
        result.getLastError().throwOnError();
    }

    protected void persist(DBObject document, String collectionName, String identifier, BasicDBObject... indexes) {
        DBCollection dbCollection = mongoTemplate.getCollection(collectionName);
        for (int i = 0; i < indexes.length; i++) {
            BasicDBObject index = indexes[i];
            dbCollection.ensureIndex(index, identifier + i, true);
        }

        try {
            final WriteResult result = dbCollection.save(document, WriteConcern.SAFE);
            result.getLastError().throwOnError();
        } catch (MongoException e) {
            throw new SdpException(systemErrorCode, e);
        }
    }

    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
}
