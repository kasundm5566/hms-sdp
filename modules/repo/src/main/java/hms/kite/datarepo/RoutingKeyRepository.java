/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public interface RoutingKeyRepository {

    Map<String, Object> registerRoutingKey(String spId, String operatorName, String shortcode, String keyword);

    Map<String, Object> registerRoutingKey(String spId, String appId, String operatorName, String shortcode, String keyword);

    boolean isRoutingKeyAvailableForSp(String operatorName, String shortcode, String keyword);

    boolean isRoutingKeyAvailableForApp(String spId, String operatorName, String shortcode, String keyword);

    boolean isRoutingKeyAvailableForApp(String spId, String appId, String operatorName, String shortcode, String keyword);

    Map<String, Map<String, List<String>>> retrieveAvailableRoutingKeys(String spId);

    Map<String,Map<String, List<String>>> retrieveAvailableRoutingKeys();

    Map<String, Set<String>> retrieveSharedShortcodes();

    Map<String, Set<String>> retrieveDomainSpecificSharedShortCodes(Map<String, Object> reqObj);

//    Map<String, Object> findRoutingKey(String operator, String shortcode, String keyword);

    Map<String, Object> findSharedShortCodeRoutingKey(String operator, String shortcode, String keyword);

    Map<String, Object> findExclusiveShortCodeRoutingKey(String operator, String shortcode);

    /**
     * Find routing keys assigned to this app from this operator
     * @param appId
     * @param operator
     * @return
     */
    List<Map<String, Object>> findRoutingKeys(String appId, String operator);

    /**
     * Find routing keys assigned to this app from this operator
     * @param spId -
     * @return    -
     */
    List<Map<String, Object>> findRoutingKeys(String spId);

    List<Map<String, Object>> findAllForOperator(String operator);

    boolean isExclusive(String ncsType, String operator, String shortcode);

    void terminate(String appId, String operator);

    List<Map<String, Object>> findTerminatedRoutingKeys(String appId, String operator);

    void delete(String appId, String operator);

    void delete(String appid);

    List<Map<String, Object>> findAllRoutingKeysForOperatorWithCategories(Map<String, Object> reqObj);
}
