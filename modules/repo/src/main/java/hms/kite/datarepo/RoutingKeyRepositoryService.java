/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public interface RoutingKeyRepositoryService {

    void registerRoutingKey(String spId, String operatorName, String shortcode, String keyword);

    void registerRoutingKey(String spId, String appId, String operatorName, String shortcode, String keyword);

    /**
     * Check whether to routing key assigned for an sp. If the key already assigned to current sp, this vill return false.
     * @param operatorName
     * @param shortcode
     * @param keyword
     * @return
     */
    boolean isRoutingKeyAvailableForSp(String operatorName, String shortcode, String keyword);

    /**
     * if the key assigned to an application will return false.
     * @param spId
     * @param operatorName
     * @param shortcode
     * @param keyword
     * @return
     */
    boolean isRoutingKeyAvailableForApp(String spId, String operatorName, String shortcode, String keyword);

    /**
     * Check whether the key assigned to some other application instead of current application.
     * If the key assigned to some other sp or app will return false.
     * If the key assigned to current app will return true.
     *
     * @param spId
     * @param appId
     * @param operatorName
     * @param shortcode
     * @param keyword
     * @return
     */
    boolean isRoutingKeyAvailableForApp(String spId, String appId, String operatorName, String shortcode, String keyword);

    Map<String, Map<String, List<String>>> retrieveAvailableRoutingKeys(String spId);

    Map<String, Map<String, List<String>>> retrieveAvailableRoutingKeys();

    Map<String, Set<String>> retrieveSharedShortcodes();

    Map<String, Set<String>> retrieveDomainSpecificSharedShortCodes(Map<String, Object> reqObj);

//    Map<String, Object> findRoutingKey(String operator, String shortcode, String keyword);

    Map<String, Object> findSharedShortCodeRoutingKey(String operator, String shortcode, String keyword);

    Map<String, Object> findExclusiveShortCodeRoutingKey(String operator, String shortcode);

    /**
     * Find routing keys assigned to this app from this operator
     * @param appId
     * @param operator
     * @return
     */
    List<Map<String, Object>> findRoutingKeys(String appId, String operator);

    /**
     * Find routing keys assigned to this app from this operator
     * @param spId -
     * @return    -
     */
    List<Map<String, Object>> findRoutingKeys(String spId);

    List<Map<String, Object>> findAllForOperator(String operator);

    boolean isExclusive(String ncsType, String operator, String shortcode);

    List<Map<String, Object>> findAllRoutingKeysForOperatorWithCategories(Map<String, Object> reqObj);
}
