/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public interface AppRepositoryService {

    /**
     * @param app -
     */
    void create(Map<String, Object> app);

    /**
     * @param appId -
     */
    void delete(String appId);

    void update(Map<String, Object> app);

    List<Map<String, Object>> findAppsRange(Map<String, Object> queryCriteria, int skip, int limit);

    int countApps(Map<String, Object> queryCriteria);

    List<Map<String, Object>> findBySpId(String spid, String appType);

    List<Map<String, Object>> findBySpId(String spid);

    List<Map<String, Object>> findByCoopUserId(String coopId, String appType);

    List<Map<String, Object>> search(String appName);

    List<Map<String, Object>> findAppsByStatus(String status);

    String findAppStatus(String appId);

    /**
     * Check whether app name exist for any other application other than specified app
     * @param appName
     * @param appId
     * @return
     */
    boolean isAppNameExists(String appName, String appId);

    boolean isAppNameExists(String appName);

    boolean isAppIdExists(String id);

    Map<String, Object> findById(String id);

    Map<String, Object> findByAppId(String appid);

    Map<String, Object> findByAppName(String name);

    List<Map<String, Object>> findAppsByStatusAndActiveProductionTime(String status, Date date);
}
