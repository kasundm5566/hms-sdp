/**
 *   (C) Copyright 2011-2012 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo.mongodb;

import com.mongodb.*;
import hms.kite.datarepo.ThrottlingRepository;
import hms.kite.util.KiteErrorBox;
import hms.kite.util.SdpException;
import hms.kite.util.ThrottlingType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;

import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.KiteErrorBox.*;


/**
 * tpd data {
 *  _id :
 *  tpd : 
 *  last-updated-time : <long>
 * }
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ThrottlingMongoDbRepository extends MongoDbUtils implements ThrottlingRepository {

    private static final Logger logger = LoggerFactory.getLogger(ThrottlingMongoDbRepository.class);
    static final String throttlingCollectionName = "throttling";

    @Override
    public int increaseThrottling(String throttlingId, int delta, String throttlingType) {
        return changeAndUpdateThrottlingCount(throttlingId, delta, throttlingType);
    }

    @Override
    public void resetThrottling(ThrottlingType type) {
        DBCollection dbCollection = mongoTemplate.getCollection(throttlingCollectionName);
        DBObject update = new BasicDBObject("$set", new BasicDBObject(type.getValue(), 0));
        WriteResult writeResult = dbCollection.updateMulti(new BasicDBObject(), update);
        if (writeResult.getError() != null) {
            throw new SdpException(KiteErrorBox.temporarySystemErrorCode,
                    MessageFormat.format("An error occurred while resetting the throttling [{0}] count. Mongo Error Code [{1}] ",
                            type.getValue(), writeResult.getError()));
        }
    }

    @Override
    public boolean isNoMessagesInLastSec(String throttlingId, long currentTimeMills, String throttlingType) {
        final DBObject query = BasicDBObjectBuilder.start()
                .add(idK, throttlingId)
                .add(lastUpdatedTimeK, BasicDBObjectBuilder.start().add("$lte", currentTimeMills - 1000).get()).get();

        final DBObject update = BasicDBObjectBuilder.start()
                .add(throttlingType, 1)
                .add(lastUpdatedTimeK, currentTimeMills).get();

        final DBObject result = mongoTemplate.getCollection(throttlingCollectionName)
        .findAndModify(query, new BasicDBObject(), new BasicDBObject(), false, update, true, false);

        logger.trace("Result [{}]", result);

        return null != result;
    }

    @Override
    public int findAndIncreaseCountForLastSec(String throttlingId, long currentTimeMills, String throttlingType) {
        final DBObject query = BasicDBObjectBuilder.start()
                .add(idK, throttlingId)
                .add(lastUpdatedTimeK,  BasicDBObjectBuilder.start().add("$gt", currentTimeMills - 1000).get()).get();

        final DBObject update = BasicDBObjectBuilder.start()
                .add("$inc", BasicDBObjectBuilder.start().add(throttlingType, 1).get()).get();

        final DBObject result = mongoTemplate.getCollection(throttlingCollectionName)
                .findAndModify(query, new BasicDBObject(), new BasicDBObject(), false, update, true, false);

        logger.trace("Result [{}]", result);

        if (null == result) {
            throw new SdpException(unknownErrorCode, "[" + throttlingId + "] is not found in ["+ throttlingCollectionName +"], may be an new app/sp");
        }

        return (Integer) result.get(throttlingType);
    }

    @Override
    public void insertThrottlingEntity(String throttlingId, long lastUpdatedTimeMills, String throttlingType) {
        try {
            final DBObject record = BasicDBObjectBuilder.start()
                    .add(idK, throttlingId)
                    .add(throttlingType, 0)
                    .add(lastUpdatedTimeK, lastUpdatedTimeMills).get();

            final WriteResult result = mongoTemplate.getCollection(throttlingCollectionName).insert(record);

            if (null != result.getError()) {
                throw new SdpException(KiteErrorBox.temporarySystemErrorCode, "Error occurred while adding new " +
                        "throttling record for key[" + throttlingId + "] [" + result.getError() + "]");
            }
        } catch (MongoException e) {
            throw new SdpException(KiteErrorBox.temporarySystemErrorCode, "Error occurred while adding new throttling record for key[" +
                    throttlingId + "]", e);
        }

    }

    private int changeAndUpdateThrottlingCount(String throttlingId, int throttlingValue, String throttlingType) {
        DBCollection dbCollection = mongoTemplate.getCollection(throttlingCollectionName);
        DBObject query = new BasicDBObject();
        query.put(idK, throttlingId);
        DBObject update = new BasicDBObject("$inc", new BasicDBObject(throttlingType, throttlingValue));
        DBObject res = dbCollection.findAndModify(query,
                new BasicDBObject(), new BasicDBObject(), false, update, true, true);
        return (Integer) res.get(throttlingType);
    }

}
