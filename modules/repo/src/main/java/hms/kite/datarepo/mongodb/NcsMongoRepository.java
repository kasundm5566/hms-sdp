/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.datarepo.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.QueryBuilder;
import hms.kite.datarepo.NcsRepository;
import hms.kite.util.SdpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteErrorBox.systemErrorCode;
import static hms.kite.util.KiteErrorBox.ncsNotAvailableErrorCode;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class NcsMongoRepository implements NcsRepository {

    private static final Logger logger = LoggerFactory.getLogger(NcsMongoRepository.class);

    private static final String collectionName = "ncs_sla";

    private MongoTemplate mongoTemplate;

    /**
     * @param appid - of the sla.
     * @return - the map of slas with aggregating, 'operator'-'ncs-type' as key.
     */
    @Override
    public List<Map<String, Object>> findByAppId(String appid) {

        DBCursor dbCursor = mongoTemplate.getCollection(collectionName).find(new BasicDBObject(appIdK, appid));

        List<Map<String, Object>> slas = listSlas(dbCursor);

        logger.debug("Found {} slas for app {}", slas.size(), appid);

        return slas;
    }

    /**
     * @param appid   - of sla
     * @param ncsType - of sla
     * @return - - the map of slas with aggregating, 'operator'-'ncs-type' as key.
     */
    @Override
    public List<Map<String, Object>> findByAppIdNcsType(String appid, String ncsType) {

        BasicDBObject query = new BasicDBObject(appIdK, appid);
        query.put(ncsTypeK, ncsType);

        DBCursor dbCursor = mongoTemplate.getCollection(collectionName).find(query);

        List<Map<String, Object>> slas = listSlas(dbCursor);

        logger.debug("Found {} slas for app {} and ncs-type {}", new Object[]{slas.size(), appid, ncsType});
        return slas;
    }


    @Override
    public List<Map<String, Object>> listSlas(DBCursor dbCursor) {
        List<Map<String, Object>> slas = new ArrayList<Map<String, Object>>();

        while (dbCursor.hasNext()) {
            slas.add(dbCursor.next().toMap());
        }
        return slas;
    }


    @Override
    public Map<String, Object> findOperatorSla(Map<String, Object> query) {
        logger.debug("Find sla for {}", query);
        DBObject sla = mongoTemplate.getCollection(collectionName).findOne(new BasicDBObject(query));
        if (null == sla) {
            throw new SdpException(ncsNotAvailableErrorCode);
        }
        return (HashMap) sla.toMap();
    }

    @Override
    public void create(Map<String, Object> operatorSmsSla) {
        try {
            logger.info("Received Ncs Sla {}", operatorSmsSla);
            mongoTemplate.getCollection(collectionName).save(new BasicDBObject(operatorSmsSla));
            //AuditLogTrail.getInstance().auditNcs(createdByK, (String) operatorSmsSla.get(ncsTypeK), createK, new HashMap<String, Object>());
            //final String appid = (String) operatorSmsSla.get(KiteKeyBox.appIdK);
        } catch (Exception e) {
            logger.error("Unable to save operator sms sla", e);
            throw new SdpException(systemErrorCode, e);
        }
    }

    @Override
    public void update(Map<String, Object> operatorSmsSla) {
        mongoTemplate.getCollection(collectionName).save(new BasicDBObject(operatorSmsSla));
        //auditPreviousState((String) operatorSmsSla.get(appIdK), (String) operatorSmsSla.get(ncsTypeK), (String) operatorSmsSla.get(operatorK), deleteK);
    }

    @Override
    public void deleteAll() {
        mongoTemplate.getCollection(collectionName).drop();
    }

    @Override
    public Map<String, Object> findByAppIdOperatorNcsType(String appId, String operator, String ncsType) {
        DBObject query = new QueryBuilder()
                .put(appIdK).is(appId)
                .put(operatorK).is(operator)
                .put(ncsTypeK).is(ncsType).get();

        DBCursor dbCursor = mongoTemplate.getCollection(collectionName).find(query);

        Map<String, Object> result = null;
        while (dbCursor.hasNext()) {
            if (result != null) {
                throw new SdpException("Found more than one sla for app-id ["
                        + appId + "], operator [" + operator + "], ncs-type [" + ncsType + "]");
            }
            result = dbCursor.next().toMap();
        }
        return result;
    }

    @Override
    public boolean isTillNumberExists(String tillNumber) {
        logger.debug("Validating to check if till number [{}] already exists", new Object[]{tillNumber});
        DBObject tillNumLikeQuery = new QueryBuilder().put("$regex").is("^" + tillNumber + "$").put("$options").is("\\i").get();
        String itemPath = MessageFormat.format("{0}.{1}.{2}", chargingK, metaDataK, mpesaBuyGoodsTillNoK);
        DBObject query = new QueryBuilder().put(itemPath).is(tillNumLikeQuery).get();
        DBObject result = mongoTemplate.getCollection(collectionName).findOne(query);
        return result != null;
    }

    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public void delete(String appId, String ncsType, String operator) {
        logger.debug("Deleting ncs with appid = [{}] , ncsType=[{}], operator =[{}]", new Object[]{appId, ncsType, operator});
        DBObject query = new QueryBuilder()
                .put(appIdK).is(appId)
                .put(operatorK).is(operator)
                .put(ncsTypeK).is(ncsType).get();
        mongoTemplate.getCollection(collectionName).remove(query);
    }

    public void delete(String appId) {
        logger.debug("Deleting ncs with appid = [{}]", new Object[]{appId});
        DBObject query = new QueryBuilder()
                .put(appIdK).is(appId).get();
        mongoTemplate.getCollection(collectionName).remove(query);
    }

    public List<Map<String, Object>>  filter(Map<String, Object> filterMap) {
        logger.debug("Executing custom query on mongo database.");
        QueryBuilder queryBuilder = new QueryBuilder();
        for (String key : filterMap.keySet()) {
            queryBuilder.put(key).is(filterMap.get(key));
        }

        DBCursor dbCursor = mongoTemplate.getCollection(collectionName).find(queryBuilder.get());

        return listSlas(dbCursor);
    }

}
