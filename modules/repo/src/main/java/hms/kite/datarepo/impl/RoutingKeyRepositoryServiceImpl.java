/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo.impl;

import hms.kite.datarepo.RoutingKeyRepository;
import hms.kite.datarepo.RoutingKeyRepositoryService;
import hms.kite.datarepo.mongodb.RoutingKeyMongoDBRepository;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class RoutingKeyRepositoryServiceImpl implements RoutingKeyRepositoryService {

    private RoutingKeyRepository routingKeyRepository = new RoutingKeyMongoDBRepository();

    @Override
    public void registerRoutingKey(String spId, String operatorName, String shortcode, String keyword) {
        routingKeyRepository.registerRoutingKey(spId, operatorName, shortcode, keyword);
    }

    @Override
    public void registerRoutingKey(String spId, String appId, String operatorName, String shortcode, String keyword) {
        routingKeyRepository.registerRoutingKey(spId, appId, operatorName, shortcode, keyword);
    }

    @Override
    public boolean isRoutingKeyAvailableForSp(String operatorName, String shortcode, String keyword) {
        return routingKeyRepository.isRoutingKeyAvailableForSp(operatorName, shortcode, keyword);
    }

    @Override
    public boolean isRoutingKeyAvailableForApp(String spId, String operatorName, String shortcode, String keyword) {
        return routingKeyRepository.isRoutingKeyAvailableForApp(spId, operatorName, shortcode, keyword);
    }
    @Override
    public boolean isRoutingKeyAvailableForApp(String spId, String appId, String operatorName,
                                               String shortcode, String keyword) {
        return routingKeyRepository.isRoutingKeyAvailableForApp(spId, appId, operatorName, shortcode, keyword);
    }

    @Override
    public Map<String, Map<String, List<String>>> retrieveAvailableRoutingKeys(String spId) {
        return routingKeyRepository.retrieveAvailableRoutingKeys(spId);
    }

    @Override
    public Map<String, Map<String, List<String>>> retrieveAvailableRoutingKeys() {
        return routingKeyRepository.retrieveAvailableRoutingKeys();
    }

    @Override
    public Map<String, Set<String>> retrieveSharedShortcodes() {
        return routingKeyRepository.retrieveSharedShortcodes();
    }

    @Override
    public Map<String, Set<String>> retrieveDomainSpecificSharedShortCodes(Map<String, Object> reqObj) {
        return routingKeyRepository.retrieveDomainSpecificSharedShortCodes(reqObj);
    }

    //    @Override
//    public Map<String, Object> findRoutingKey(String operator, String shortcode, String keyword) {
//        return routingKeyRepository.findRoutingKey(operator, shortcode, keyword);
//    }

    @Override
    public List<Map<String, Object>> findRoutingKeys(String appId, String operator) {
        return routingKeyRepository.findRoutingKeys(appId, operator);
    }

    @Override
    public List<Map<String, Object>> findRoutingKeys(String spId) {
        return routingKeyRepository.findRoutingKeys(spId);
    }

    @Override
    public List<Map<String, Object>> findAllForOperator(String operator) {
        return routingKeyRepository.findAllForOperator(operator);
    }

    @Override
    public boolean isExclusive(String ncsType, String operator, String shortcode) {
        return this.routingKeyRepository.isExclusive(ncsType, operator, shortcode);
    }

    public void setRoutingKeyRepository(RoutingKeyRepository routingKeyRepository) {
        this.routingKeyRepository = routingKeyRepository;
    }

	@Override
	public Map<String, Object> findSharedShortCodeRoutingKey(String operator, String shortcode, String keyword) {
		 return routingKeyRepository.findSharedShortCodeRoutingKey(operator, shortcode, keyword);
	}

	@Override
	public Map<String, Object> findExclusiveShortCodeRoutingKey(String operator, String shortcode) {
		 return routingKeyRepository.findExclusiveShortCodeRoutingKey(operator, shortcode);
	}

    @Override
    public List<Map<String, Object>> findAllRoutingKeysForOperatorWithCategories(Map<String, Object> reqObj) {
        return routingKeyRepository.findAllRoutingKeysForOperatorWithCategories(reqObj);
    }
}
