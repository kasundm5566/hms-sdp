/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo.mongodb;

import hms.kite.datarepo.CrHistoryRepository;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.BasicDBObject;

/**
 * Mongo Repository implementation for maintaining provisioning CR History details
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class CrHistoryMongoRepository implements CrHistoryRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(CrHistoryMongoRepository.class);
    private static final String COLLECTION_NAME = "cr-history";
    private MongoTemplate mongoTemplate;

    @Override
    public void create(Map<String, Object> crReq) {
        mongoTemplate.getCollection(COLLECTION_NAME).save(new BasicDBObject(crReq));
        //AuditLogTrail.getInstance().auditCrHistory(createdByK, (String) crReq.get(spIdK), (String) crReq.get(crTypeK), createK, new HashMap<String, Object>());
    }

    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
}
