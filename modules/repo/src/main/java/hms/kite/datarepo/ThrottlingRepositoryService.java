/*
*   (C) Copyright 2009-2012 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/

package hms.kite.datarepo;

import hms.kite.util.ThrottlingType;

/**
 * User: azeem
 * Date: 3/5/12
 * Time: 6:28 PM
 */
public interface ThrottlingRepositoryService {

    int increaseThrottling(String throttlingId, int delta, ThrottlingType type);

    void resetThrottling(ThrottlingType type);

    boolean isNoMessagesInLastSec(String throttlingId, long currentTimeMills, String throttlingType);

    int findAndIncreaseCountForLastSec(String throttlingId, long currentTimeMills, String throttlingType);

    void insertThrottlingEntity(String throttlingId, long lastUpdatedTimeMills, String throttlingType);
}
