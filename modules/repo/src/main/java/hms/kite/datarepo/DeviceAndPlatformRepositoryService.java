/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo;

import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: sanjeewa
 * Date: 3/25/12
 * Time: 5:31 PM
 * To change this template use File | Settings | File Templates.
 */
public interface DeviceAndPlatformRepositoryService {

    List<Map<String, Object>> findAllDevices();

    List<Map<String, Object>> findAllPlatforms();

}
