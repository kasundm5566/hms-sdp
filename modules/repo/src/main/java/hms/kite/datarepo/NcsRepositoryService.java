/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo;

import com.mongodb.DBCursor;

import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public interface NcsRepositoryService {

    void createNcs(Map<String, Object> sla, String spId);

    Map<String, Object> findNcs(String appId, String ncsType, String operator);

    Map<String, Map<String, Object>> findByAppId(String appid);

    List<Map<String, Object>> findByAppIdNcsType(String appid, String ncsType);

    List<Map<String, Object>> listSlas(DBCursor dbCursor);

    Map<String, Object> findOperatorSla(Map<String, Object> operator);

    Map<String, Object> findByAppIdOperatorNcsType(String appId, String operator, String ncsType);

    /**
     * Checks if the Till number is already used in another appliaction
     * @param tillNumber
     * @return
     */
    boolean isTillNumberExists(String tillNumber);

    /**
     * Change the status of ncs to terminate, and move the routing keys to terminated collection.
     * @param sla -
     */
    public void terminateNcs(Map<String, Object> sla);

    /**
     * Just update the sla. No routing key modification will happen.
     * @param operatorSmsSla
     */
    void update(Map<String, Object> operatorSmsSla);

    void update(String appId, String ncsType, String operator, String status);

    /**
     * Delete the ncs and related routing keys.
     * @param appId
     * @param ncsType
     * @param operator
     */
    void delete(String appId, String ncsType, String operator);

    void delete(String appId);

    void deleteAll();

    List<Map<String, Object>> filter(Map<String, Object> filterMap);
}
