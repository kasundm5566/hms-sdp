/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo;

import hms.kite.util.NullObject;

/**
 * $LastChangedDate: 2011-06-29 13:05:28 +0530 (Wed, 29 Jun 2011) $
 * $LastChangedBy: romith $
 * $LastChangedRevision: 74555 $
 */
public final class RepositoryServiceRegistry {

    private static SpRepositoryService spRepositoryService;
    private static AppRepositoryService appRepositoryService;
    private static NcsRepositoryService ncsRepositoryService;
    private static RoutingKeyRepositoryService smsRoutingKeyRepositoryService;
    private static RoutingKeyRepositoryService ussdRoutingKeyRepositoryService;
    private static CrRepositoryService crRepositoryService;
    private static CrHistoryRepositoryService crHistoryRepositoryService;
    private static SystemConfiguration systemConfiguration;
    private static DeviceAndPlatformRepositoryService deviceAndPlatformRepositoryService;
    private static BlacklistRepository blacklistRepository;
    private static ThrottlingRepositoryService throttlingRepositoryService;
    private static AuditRepositoryService auditRepositoryService;

    private RepositoryServiceRegistry() {
    }

    static {
        setAppRepositoryService(new NullObject<AppRepositoryService>().get());
        setSpRepositoryService(new NullObject<SpRepositoryService>().get());
        setNcsRepositoryService(new NullObject<NcsRepositoryService>().get());
        setSmsRoutingKeyRepositoryService(new NullObject<RoutingKeyRepositoryService>().get());
        setUssdRoutingKeyRepositoryService(new NullObject<RoutingKeyRepositoryService>().get());
        setSystemConfiguration(new NullObject<SystemConfiguration>().get());
        setDeviceAndPlatformRepositoryService(new NullObject<DeviceAndPlatformRepositoryService>().get());
        setBlacklistRepository(new NullObject<BlacklistRepository>().get());
        setThrottlingRepository(new NullObject<ThrottlingRepositoryService>().get());
        setCrRepositoryService(new NullObject<CrRepositoryService>().get());
        setCrHistoryRepositoryService(new NullObject<CrHistoryRepositoryService>().get());
        setAuditRepositoryService(new NullObject<AuditRepositoryService>().get());
    }


    public static SpRepositoryService spRepositoryService() {
        return spRepositoryService;
    }

    private static void setSpRepositoryService(SpRepositoryService spRepositoryService) {
        RepositoryServiceRegistry.spRepositoryService = spRepositoryService;
    }

    public static AppRepositoryService appRepositoryService() {
        return appRepositoryService;
    }

    private static void setAppRepositoryService(AppRepositoryService appRepositoryService) {
        RepositoryServiceRegistry.appRepositoryService = appRepositoryService;
    }

    public static NcsRepositoryService ncsRepositoryService() {
        return ncsRepositoryService;
    }

    private static void setNcsRepositoryService(NcsRepositoryService ncsRepositoryService) {
        RepositoryServiceRegistry.ncsRepositoryService = ncsRepositoryService;
    }

    private static void setSmsRoutingKeyRepositoryService(RoutingKeyRepositoryService smsRoutingKeyRepositoryService) {
        RepositoryServiceRegistry.smsRoutingKeyRepositoryService = smsRoutingKeyRepositoryService;
    }

    public static RoutingKeyRepositoryService smsRoutingKeyRepositoryService() {
        return smsRoutingKeyRepositoryService;
    }

    private static void setUssdRoutingKeyRepositoryService(RoutingKeyRepositoryService ussdRoutingKeyRepositoryService) {
        RepositoryServiceRegistry.ussdRoutingKeyRepositoryService = ussdRoutingKeyRepositoryService;
    }

    public static RoutingKeyRepositoryService ussdRoutingKeyRepositoryService() {
        return ussdRoutingKeyRepositoryService;
    }

    public static SystemConfiguration systemConfiguration() {
        return systemConfiguration;
    }

    private static void setSystemConfiguration(SystemConfiguration systemConfiguration) {
        RepositoryServiceRegistry.systemConfiguration = systemConfiguration;
    }

    public static DeviceAndPlatformRepositoryService deviceAndPlatformRepositoryService() {
        return deviceAndPlatformRepositoryService;
    }

    private static void setDeviceAndPlatformRepositoryService(DeviceAndPlatformRepositoryService deviceAndPlatformRepositoryService) {
        RepositoryServiceRegistry.deviceAndPlatformRepositoryService = deviceAndPlatformRepositoryService;
    }

    public static BlacklistRepository blacklistRepository() {
        return blacklistRepository;
    }

    private static void setBlacklistRepository(BlacklistRepository blacklistRepository) {
        RepositoryServiceRegistry.blacklistRepository = blacklistRepository;
    }

    public static ThrottlingRepositoryService throttlingRepository() {
        return throttlingRepositoryService;
    }

    private static void setThrottlingRepository(ThrottlingRepositoryService tpdRepository) {
        RepositoryServiceRegistry.throttlingRepositoryService = tpdRepository;
    }

    public static CrRepositoryService crRepositoryService() {
        return crRepositoryService;
    }

    private static void setCrRepositoryService(CrRepositoryService crRepositoryService) {
        RepositoryServiceRegistry.crRepositoryService = crRepositoryService;
    }

    public static CrHistoryRepositoryService crHistoryRepositoryService() {
        return crHistoryRepositoryService;
    }

    private static void setCrHistoryRepositoryService(CrHistoryRepositoryService
                                                              crHistoryRepositoryService) {
        RepositoryServiceRegistry.crHistoryRepositoryService = crHistoryRepositoryService;
    }

    public static AuditRepositoryService auditRepositoryService() {
        return auditRepositoryService;
    }

    private static void setAuditRepositoryService(AuditRepositoryService auditRepositoryService) {
        RepositoryServiceRegistry.auditRepositoryService = auditRepositoryService;
    }
}
