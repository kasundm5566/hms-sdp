/**
 *   (C) Copyright 2011-2012 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo;

import hms.kite.util.ThrottlingType;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public interface ThrottlingRepository {

    int increaseThrottling(String throttlingId, int delta, String throttlingType);

    void resetThrottling(ThrottlingType type);

    boolean isNoMessagesInLastSec(String throttlingId, long currentTimeMills, String throttlingType);

    int findAndIncreaseCountForLastSec(String throttlingId, long currentTimeMills, String throttlingType);

    void insertThrottlingEntity(String throttlingId, long lastUpdatedTimeMills, String throttlingType);

}
