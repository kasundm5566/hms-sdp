/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.datarepo.mongodb;

import hms.kite.datarepo.AuditRepository;

import java.util.Map;

import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.BasicDBObject;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class AuditMongoRepository implements AuditRepository {

    public static final String auditCollectionName = "audit";

    private MongoTemplate mongoTemplate;

    @Override
    public void audit(Map<String, Object> data) {
        mongoTemplate.getCollection(auditCollectionName).save(new BasicDBObject(data));
    }

    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
}
