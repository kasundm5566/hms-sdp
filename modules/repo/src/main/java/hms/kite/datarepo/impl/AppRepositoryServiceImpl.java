/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.datarepo.impl;

import hms.kite.datarepo.AppRepository;
import hms.kite.datarepo.AppRepositoryService;
import hms.kite.datarepo.audit.AuditTrail;
import hms.kite.util.SdpException;
import hms.kite.util.logging.ProvReportingLog;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.util.KiteErrorBox.md5ExceptionErrorCode;
import static hms.kite.util.KiteErrorBox.systemErrorCode;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.SystemUtil.getPassword;
import static hms.kite.util.SystemUtil.md5Encryption;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class AppRepositoryServiceImpl implements AppRepositoryService {

    private AppRepository appRepository;
    private final int passwordLength = 15;

    @Override
    public void create(Map<String, Object> app) {
        Date createdDate = new Date();
        app.put(createdDateK, createdDate);
        app.put(updatedDateK, createdDate);
        app.put(createdByK, AuditTrail.getCurrentUsername());

        if (!app.containsKey(expireK)) {
            app.put(expireK, true);
        }

        String password = getPassword(passwordLength);
        String encryptedPassword = "";
        try {
            encryptedPassword = md5Encryption(password);
        } catch (NoSuchAlgorithmException e) {
            throw new SdpException(md5ExceptionErrorCode, e.getMessage());
        } catch (UnsupportedEncodingException e) {
            throw new SdpException(systemErrorCode, e.getMessage());
        }
        app.put(passwordK, encryptedPassword);

        Map<String, Object> loggingData = new HashMap<String, Object>();
        if(app.get(loggingDataK) != null) {
            loggingData = (Map<String, Object>)app.get(loggingDataK);
            app.remove(loggingDataK);
        }

        appRepository.create(app);
        AuditTrail.getInstance().auditApp(app, createK);
        String revenueType = "";
        String revenuePercentage = "0";
        if(app.get(revenueShareK) != null) {
            revenuePercentage=app.get(revenueShareK).toString();
        }
        if (!revenuePercentage.equals(null) && !revenuePercentage.equals("")) {
            revenueType = "percentageFromMonthlyRevenue";
        }

        Map<String, Object> sp = spRepositoryService().findSpBySpId(app.get(spIdK).toString());
        String coopUserId = sp.get(coopUserIdK).toString();

        ProvReportingLog.log(app.get(spIdK), getCorpUserNameForApp(app), coopUserId,
                app.get(appIdK), app.get(nameK), app.get(categoryK), revenueType,
                revenuePercentage, loggingData, app.get(statusK), app.get(createdDateK));
    }

    @Override
    public void delete(String appId) {
        Map<String, Object> app = appRepository.findByAppId(appId);
        app.put(statusK, deleteK);
        appRepository.delete(appId);
        AuditTrail.getInstance().auditApp(app, deleteK);
    }

    public void deleteAll() {
        appRepository.deleteAll();
    }

    @Override
    public void update(Map<String, Object> app) {
        app.put(updatedDateK, new Date());
        app.put(updatedByK, AuditTrail.getCurrentUsername());
        AuditTrail.getInstance().auditApp(app, updateK);
        String revenueType = "";
        String revenuePercentage = "0";
        if(app.get(revenueShareK) != null) {
            revenuePercentage=app.get(revenueShareK).toString();
        }
        if (!revenuePercentage.equals(null) && !revenuePercentage.equals("")) {
            revenueType = "percentageFromMonthlyRevenue";
        }

        Map<String, Object> loggingData = new HashMap<String, Object>();
        if(app.get(loggingDataK) != null) {
            loggingData = (Map<String, Object>)app.get(loggingDataK);
            app.remove(loggingDataK);
        }

        Map<String, Object> sp = spRepositoryService().findSpBySpId(app.get(spIdK).toString());
        String coopUserId = sp.get(coopUserIdK).toString();

        ProvReportingLog.log(app.get(spIdK), getCorpUserNameForApp(app), coopUserId,
                app.get(appIdK), app.get(nameK), app.get(categoryK), revenueType, revenuePercentage,
                loggingData, app.get(statusK), app.get(createdDateK));

        appRepository.update(app);
    }

    private Object getCorpUserNameForApp(Map<String, Object> app) {
        return spRepositoryService().findSpBySpId((String) app.get(spIdK)).get(coopUserNameK);
    }

    public void setAppRepository(AppRepository appRepository) {
        this.appRepository = appRepository;
    }

    @Override
    public List<Map<String, Object>> findAppsRange(Map<String, Object> queryCriteria, int skip, int limit) {
        return appRepository.findAppsRange(queryCriteria, skip, limit);
    }

    @Override
    public int countApps(Map<String, Object> queryCriteria) {
        return appRepository.countApps(queryCriteria);
    }

    @Override
    public List<Map<String, Object>> findBySpId(String spid, String appType) {
        return appRepository.findBySpId(spid, appType);
    }

    @Override
    public List<Map<String, Object>> findBySpId(String spid) {
        return appRepository.findBySpId(spid);
    }

    @Override
    public List<Map<String, Object>> findByCoopUserId(String coopId, String appType) {
        return appRepository.findByCoopUserId(coopId, appType);
    }

    @Override
    public List<Map<String, Object>> search(String appName) {
        return appRepository.search(appName);
    }

    @Override
    public List<Map<String, Object>> findAppsByStatus(String status) {
        return appRepository.findAppsByStatus(status);
    }

    @Override
    public String findAppStatus(String appId) {
        return appRepository.findAppStatus(appId);
    }

    @Override
    public boolean isAppNameExists(String appName, String appId) {
        return appRepository.isAppNameExists(appName, appId);
    }

    @Override
    public boolean isAppNameExists(String appName) {
        return appRepository.isAppNameExists(appName);
    }

    @Override
    public boolean isAppIdExists(String appName) {
        return appRepository.isAppIdExists(appName);
    }

    @Override
    public Map<String, Object> findById(String id) {
        return appRepository.findById(id);
    }

    @Override
    public Map<String, Object> findByAppId(String appid) {
        return appRepository.findByAppId(appid);
    }

    @Override
    public Map<String, Object> findByAppName(String name) {
        return appRepository.findByAppName(name);
    }

    @Override
    public List<Map<String, Object>> findAppsByStatusAndActiveProductionTime(String status, Date date) {
        return appRepository.findAppsByStatusAndActiveProductionTime(status, date);
    }
}
