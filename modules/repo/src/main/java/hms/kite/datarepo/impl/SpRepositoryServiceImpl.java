/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.datarepo.impl;

import hms.kite.datarepo.SpRepository;
import hms.kite.datarepo.SpRepositoryService;
import hms.kite.datarepo.audit.AuditTrail;
import hms.kite.datarepo.mongodb.SpMongoRepository;
import hms.kite.util.logging.ProvReportingLog;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class SpRepositoryServiceImpl implements SpRepositoryService {

    private SpRepository spRepository = new SpMongoRepository();

    @Override
    public void create(Map<String, Object> sp) {
        spRepository.create(sp);
        AuditTrail.getInstance().auditSp(sp, createK);
        ProvReportingLog.log(sp.get(spIdK), sp.get(coopUserNameK), sp.get(coopUserIdK), "", "", "", "", "", "", "",
                new Date());
    }

    @Override
    public List<Map<String, Object>> findAllSp(int skip, int limit) {
        return spRepository.findAllSp(skip, limit);
    }

    @Override
    public Map<String, Object> findSpById(String id) {
        return spRepository.findSpById(id);
    }

    @Override
    public Map<String, Object> findSpByName(String name) {
        return spRepository.findSpByName(name);
    }

    @Override
    public Map<String, Object> findSpByCoopUserId(String coopUserId) {
        return spRepository.findSpByCoopUserId(coopUserId);
    }

    @Override
    public Map<String, Object> findSpByCoopUserName(String coopUserName) {
        return spRepository.findSpByCoopUserName(coopUserName);
    }

    @Override
    public List<Map<String, Object>> findAllSpBySpQueryCriteria(Map<String, Object> queryCriteria, int skip, int limit) {
        return spRepository.findAllSpBySpQueryCriteria(queryCriteria, skip, limit);
    }

    @Override
    public int countSpBySpQueryCriteria(Map<String, Object> query) {
        return spRepository.countSpBySpQueryCriteria(query);
    }

    @Override
    public Map<String, Object> findSpBySpId(String spId) {
        return spRepository.findSpBySpId(spId);
    }

    @Override
    public List<Map<String, Object>> findSpsByStatus(String status) {
        return spRepository.findSpsByStatus(status);
    }

    @Override
    public List<Map<String, Object>> findSpsByStatus(String status, int skip, int limit) {
        return spRepository.findSpsByStatus(status, skip, limit);
    }

    @Override
    public int countSpsByStatus(String status) {
        return spRepository.countSpsByStatus(status);
    }

    @Override
    public int countAllSps() {
        return spRepository.countAllSps();
    }

    @Override
    public void update(Map<String, Object> sp) {
        spRepository.update(sp);
        AuditTrail.getInstance().auditSp(sp, updateK);
        ProvReportingLog.log(sp.get(spIdK), sp.get(coopUserNameK), sp.get(coopUserIdK), "", "", "", "", "", "", "",
                new Date());
    }

    @Override
    public void deleteSpById(String id) {
        Map<String, Object> sp = spRepository.findSpById(id);
        spRepository.deleteSpById(id);
        AuditTrail.getInstance().auditSp(sp, deleteK);
    }

    public void deleteAll() {
        spRepository.deleteAll();
    }

    public void setSpRepository(SpRepository spRepository) {
        this.spRepository = spRepository;
    }
}
