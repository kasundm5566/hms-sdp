/*
 * (C) Copyright 1996-2012 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.datarepo.mongodb;

import com.mongodb.*;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;
import hms.commons.IdGenerator;
import hms.kite.datarepo.BuildFileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.io.IOException;
import java.io.OutputStream;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static com.mongodb.MapReduceCommand.OutputType.REPLACE;
import static hms.kite.datarepo.common.util.CommonUtil.isCollectionExists;
import static hms.kite.datarepo.mongodb.MongoUtil.createLikeQuery;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class BuildFileMongoRepository implements BuildFileRepository {

    private static final Logger logger = LoggerFactory.getLogger(BuildFileMongoRepository.class);

    private static final String COLLECTION_NAME = "build_file";
    private static final String TEMP_1_COLLECTION_NAME = "temp1_%s";
    private static final String TEMP_2_COLLECTION_NAME = "temp2_%s";
    private static final String DEVICES_BRAND_MODEL_MAP_FUNCTION = "function(){var id=this[\"app-file-id\"];var version=this[\"version\"];var devices=this[\"supported-devices\"];devices.forEach(function(device){var brand=device.brand;device.models.forEach(function(model){emit({brand:brand,model:model},{content:[{" + contentIdK + ":id,version:version}]})})})}";
    private static final String DEVICES_BRAND_MAP_FUNCTION = "function(){var brand=this._id.brand;var model=this._id.model;var content=this.value.content;emit(brand,{models:[{model:model,content:content}]})}";
    private static final String DEVICES_REDUCE_CONTENT_FUNCTION = "function(key,values){var content=[];var maxIdIndex=0;var maxValuesIndex=0;var max=0;for(var i=0;i<values.length;i++){for(var j=0;j<values[i].content.length;j++){var current=parseFloat(values[i].content[j]." + contentIdK + ");if(max<current){maxValuesIndex=i;maxIdIndex=j;max=current}}}content.push({" + contentIdK + ":values[maxValuesIndex].content[maxIdIndex]." + contentIdK + ",version:values[maxValuesIndex].content[maxIdIndex].version});return{content:content}}";
    private static final String DEVICES_REDUCE_MODELS_FUNCTION = "function(key,values){var allModels=[];for(var i=0;i<values.length;i++)for(var j=0;j<values[i].models.length;j++)allModels.push({model:values[i].models[j].model,content:values[i].models[j].content});return{models:allModels}}";
    private static final String DEVICES_FINALIZE_FUNCTION = "function(key,value){var models={};for(var i=0;i<value.models.length;i++){models[value.models[i].model]=value.models[i].content}return models}";
    private static final String PLATFORMS_VERSION_MAP_FUNCTION = "function(){var id=this[\"app-file-id\"];var version=this[\"version\"];var platform=this[\"platform-name\"];var platformVersions=this[\"platform-versions\"];platformVersions.forEach(function(platformVersion){emit({platform:platform,platformVersion:platformVersion},{content:[{" + contentIdK + ":id,version:version}]})})}";
    private static final String PLATFORMS_MAP_FUNCTION = "function(){var platform=this._id.platform;var platformVersion=this._id.platformVersion;var content=this.value.content;emit(platform,{versions:[{version:platformVersion,content:content}]})}";
    private static final String PLATFORMS_REDUCE_CONTENT_FUNCTION = "function(key,values){var content=[];var maxIdIndex=0;var maxValuesIndex=0;var max=0;for(var i=0;i<values.length;i++){for(var j=0;j<values[i].content.length;j++){var current=parseFloat(values[i].content[j]." + contentIdK + ");if(max<current){maxValuesIndex=i;maxIdIndex=j;max=current}}}content.push({" + contentIdK + ":values[maxValuesIndex].content[maxIdIndex]." + contentIdK + ",version:values[maxValuesIndex].content[maxIdIndex].version});return{content:content}}";
    private static final String PLATFORMS_REDUCE_MODELS_FUNCTION = "function(key,values){var allVersions=[];for(var i=0;i<values.length;i++)for(var j=0;j<values[i].versions.length;j++)allVersions.push({version:values[i].versions[j].version,content:values[i].versions[j].content});return{versions:allVersions}}";
    private static final String PLATFORMS_FINALIZE_FUNCTION = "function(key,value){var versions={};for(var i=0;i<value.versions.length;i++){versions[value.versions[i].version]=value.versions[i].content}return versions}";

    private static final Map<String, GridFSInputFile> uploadFileList = new ConcurrentHashMap<String, GridFSInputFile>();

    private MongoTemplate mongoTemplate;

    private List<Map<String, Object>> getDBObjectList(DBCursor dbCursor) {
        List<Map<String, Object>> buildFileList = new ArrayList<Map<String, Object>>();

        while (dbCursor.hasNext()) {
            buildFileList.add(dbCursor.next().toMap());
        }

        return buildFileList;
    }

    private void saveBuildFile(Map<String, Object> buildFileInfo, String key) {
        if (buildFileInfo.containsKey(key)) {
            try {
                GridFSInputFile savedFile = uploadFileList.remove(buildFileInfo.get(key));
                if (savedFile != null) {
                    savedFile.getOutputStream().close();
                }
            } catch (Exception e) {
                logger.error("Something went wrong when saving the file in the db ", e);
            }
        }
    }

    private void deleteBuildFile(Object id) {
        GridFS gridFS = new GridFS(mongoTemplate.getDb(), COLLECTION_NAME);
        gridFS.remove(new BasicDBObject(idK, id));
    }

    private Map<String, Object> callMapReduce(String firstMapFunction, String firstReduceFunction, String secondMapFunction, String secondReduceFunction, String finalizeFunction, BasicDBObject query) {
        if (!isCollectionExists(mongoTemplate, COLLECTION_NAME)) {
            return new HashMap<String, Object>();
        }

        final String tempCollId = IdGenerator.generateId();
        final String tempCollection1 = String.format(TEMP_1_COLLECTION_NAME, tempCollId);
        final String tempCollection2 = String.format(TEMP_2_COLLECTION_NAME, tempCollId);

        Map<String, Object> resultMap = new HashMap<String, Object>();

        try {
            MapReduceOutput mapReduceOutput = mongoTemplate.getCollection(COLLECTION_NAME).mapReduce(firstMapFunction, firstReduceFunction, tempCollection1, query);

            MapReduceCommand mapReduceCommand = new MapReduceCommand(mongoTemplate.getCollection(tempCollection1), secondMapFunction, secondReduceFunction, tempCollection2, REPLACE, new BasicDBObject());
            mapReduceCommand.setFinalize(finalizeFunction);
            mapReduceOutput = mapReduceOutput.getOutputCollection().mapReduce(mapReduceCommand);

            DBCursor dbCursor = mapReduceOutput.getOutputCollection().find();

            while (dbCursor.hasNext()) {
                DBObject next = dbCursor.next();
                resultMap.put((String) next.get(idK), next.get(valueK));
            }
        } catch (Exception e) {
            logger.error("Error occurred while map reduce operation. [{}]", e);
        } finally {
            dropTemporaryCollections(tempCollection1, tempCollection2);
        }

        return resultMap;
    }

    private void dropTemporaryCollections(String... collectionNames) {
        for (String collectionName : collectionNames) {
            try {
                mongoTemplate.getCollection(collectionName).drop();
            } catch (Exception e) {
                logger.error("Error while dropping the temporary collections. []", e);
            }
        }

    }

    @Override
    public Map<String, Object> getDevicesAndPlatformsByAppId(String appId) {
        Map<String, Object> result = new HashMap<String, Object>();

        BasicDBObject query = new BasicDBObject();
        query.put(appIdK, appId);
        query.put(statusK, approvedK);

        Map<String, Object> devicesMap = callMapReduce(DEVICES_BRAND_MODEL_MAP_FUNCTION, DEVICES_REDUCE_CONTENT_FUNCTION, DEVICES_BRAND_MAP_FUNCTION, DEVICES_REDUCE_MODELS_FUNCTION, DEVICES_FINALIZE_FUNCTION, query);
        Map<String, Object> platformsMap = callMapReduce(PLATFORMS_VERSION_MAP_FUNCTION, PLATFORMS_REDUCE_CONTENT_FUNCTION, PLATFORMS_MAP_FUNCTION, PLATFORMS_REDUCE_MODELS_FUNCTION, PLATFORMS_FINALIZE_FUNCTION, query);

        result.put(cmsDevicesK, devicesMap);
        result.put(cmsPlatformsK, platformsMap);

        return result;
    }

    //These javascript functions were made single lined using http://jsbin.com/ujumus/11
    private static final String DEVICE_AND_PLATFORM_MAP1 = "function () {\n var id = this[\"app-file-id\"];\n var version = this[\"version\"];\n var devices = this[\"supported-devices\"];\n var appId = this[\"app-id\"];\n var platform = this[\"platform-name\"];\n //----- Extract devices and models\n devices.forEach(function (device) {\n var brand = device.brand;\n device.models.forEach(function (model) { \n emit({\n \"appId\" : appId,\n \"ctype\" : \"brand\",\n \"cdata\": {\n brand: brand,\n model: model }\n }, {\n content: [{\n \"content-id\": id,\n version: version\n }]\n })\n })\n })\n //------- Extract platforms\n var platform = this[\"platform-name\"];\n var platformVersions = this[\"platform-versions\"];\n platformVersions.forEach(function (platformVersion) {\n emit({\n \"appId\" : appId,\n \"ctype\" : \"platform\",\n \"cdata\": {\n platform: platform,\n platformVersion: platformVersion }\n }, {\n content: [{\n \"content-id\": id,\n version: version\n }]\n })\n })\n\n}";
    private static final String DEVICE_AND_PLATFORM_MAP2 = "function () {\n var ctype = this._id.ctype;\n var cdata = this._id.cdata;\n var appId = this._id.appId\n var content = this.value.content;\n\n emit(appId, {\n \"ctype\" : ctype, \n cdetails: [{\n cdata: cdata,\n content: content\n }]\n })\n}";
    private static final String DEVICE_AND_PLATFORM_REDUCE1 = "function (key, values) {\n var content = [];\n var maxIdIndex = 0;\n var maxValuesIndex = 0;\n var max = 0;\n for (var i = 0; i < values.length; i++) {\n for (var j = 0; j < values[i].content.length; j++) {\n var current = parseFloat(values[i].content[j].content-id);\n if (max < current) {\n maxValuesIndex = i;\n maxIdIndex = j;\n max = current\n }\n }\n\n }\n content.push({\n \"content-id\": values[maxValuesIndex].content[maxIdIndex].content-id,\n version: values[maxValuesIndex].content[maxIdIndex].version\n });\n return {\n content: content\n }\n\n}";
    private static final String DEVICE_AND_PLATFORM_REDUCE2 = "function (key, values) {\n var allModels = [];\n for (var i = 0; i < values.length; i++) for (var j = 0; j < values[i].cdetails.length; j++) allModels.push({\n ctype: values[i].ctype,\n cdata: values[i].cdetails[j].cdata,\n content: values[i].cdetails[j].content\n });\n return {\n models: allModels\n }\n\n}";
    private static final String DEVICE_AND_PLATFORM_FINALIZE = "function (key, value) {\n var models = {};\n var platforms = {};\n /* When there are no supported-devices for a app and only one platform version, map reducing is not executing(only one result returns from DEVICE_AND_PLATFORM_MAP2).\n It should have more than single elements to execute a map reducing.\n When the above scenario happens, value.models is undefined. This code block has changed to handle the issue. */\n if (value.models !== undefined) {\n for (var i = 0; i < value.models.length; i++) {\n var ctype = value.models[i].ctype;\n\n if (ctype == \"brand\") {\n var brand = value.models[i].cdata.brand\n if (models[brand] == undefined) {\n models[brand] = []\n }\n models[brand].push({ \"model\": value.models[i].cdata.model, \"content\": value.models[i].content });\n }\n\n if (ctype == \"platform\") {\n var platform = value.models[i].cdata.platform\n if (platforms[platform] == undefined) {\n platforms[platform] = []\n }\n\n platforms[platform].push({ \"version\": value.models[i].cdata.platformVersion, \"content\": value.models[i].content });\n }\n }\n } else {\n var allModels=[];\n for (var j = 0; j < value.cdetails.length; j++) allModels.push({\n ctype: value.ctype,\n cdata: value.cdetails[j].cdata,\n content: value.cdetails[j].content\n });\n for (var i = 0; i < allModels.length; i++) {\n var ctype = allModels[i].ctype;\n\n if (ctype == \"brand\") {\n var brand = allModels[i].cdata.brand\n if (models[brand] == undefined) {\n models[brand] = []\n }\n models[brand].push({ \"model\": allModels[i].cdata.model, \"content\": allModels[i].content });\n }\n\n if (ctype == \"platform\") {\n var platform = allModels[i].cdata.platform\n if (platforms[platform] == undefined) {\n platforms[platform] = []\n }\n\n platforms[platform].push({ \"version\": allModels[i].cdata.platformVersion, \"content\": allModels[i].content });\n }\n\n }\n }\n return {\"devices\": models, \"platforms\": platforms };\n}";

    @Override
    public Map<String, Object> getDevicesAndPlatformsByAppIdList(List<String> appIdList, String platform, String version) {
        BasicDBList appIdQ = new BasicDBList();

        for (String appId : appIdList) {
            BasicDBObject query = new BasicDBObject();
            query.put(appIdK, appId);
            query.put(statusK, approvedK);
            if(platform != null && !platform.trim().isEmpty()) {
                query.put(platformIdK, platform);
            }
            if(version != null && !version.trim().isEmpty()) {
                query.put(platformVersionsK, version);
            }
            appIdQ.add(query);
        }
        BasicDBObject query = new BasicDBObject();
        query.put("$or", appIdQ);
        logger.debug("Created mongo query[{}]", query);

        Map<String, Object> results = callMapReduce(DEVICE_AND_PLATFORM_MAP1, DEVICE_AND_PLATFORM_REDUCE1, DEVICE_AND_PLATFORM_MAP2, DEVICE_AND_PLATFORM_REDUCE2, DEVICE_AND_PLATFORM_FINALIZE, query);
        logger.debug("Received result from mongo[{}]", results);
        return results;
    }

    @Override
    public List<Map<String, Object>> findByAppId(String appId) {
        DBCursor dbCursor = mongoTemplate.getCollection(COLLECTION_NAME).find(new BasicDBObject(appIdK, appId));

        List<Map<String, Object>> buildFileList = getDBObjectList(dbCursor);

        return buildFileList;
    }

    @Override
    public Map<String, Object> findByBuildFileId(String fileId) {
        return (Map<String, Object>) mongoTemplate.getCollection(COLLECTION_NAME).findOne(new BasicDBObject(buildAppFileIdK, fileId));
    }

    @Override
    public List<Map<String, Object>> search(Map<String, Object> query) {
        Map<String, String> likeQuery = createLikeQuery((String) query.get(buildNameK));
        query.put(buildNameK, likeQuery);
        BasicDBObject dbQuery = new BasicDBObject();
        dbQuery.putAll(query);

        DBCursor dbCursor = mongoTemplate.getCollection(COLLECTION_NAME).find(dbQuery);

        List<Map<String, Object>> buildFileList = getDBObjectList(dbCursor);

        return buildFileList;
    }

    @Override
    public int getBuildFilesCount(Map<String, Object> queryCriteria) {
        return mongoTemplate.getCollection(COLLECTION_NAME).find(new BasicDBObject(queryCriteria)).count();
    }

    @Override
    public List<Map<String, Object>> findBuildFilesRange(Map<String, Object> queryCriteria, int skip, int limit) {
        BasicDBObject orderBy = new BasicDBObject();
        if (queryCriteria.containsKey(orderByK)) {
            orderBy.putAll((Map) queryCriteria.remove(orderByK));
        }
        BasicDBObject query = new BasicDBObject();
        query.putAll(queryCriteria);

        DBCursor dbCursor = mongoTemplate.getCollection(COLLECTION_NAME).find(query).sort(orderBy).skip(skip).limit(limit);

        List<Map<String, Object>> buildFiles = new LinkedList<Map<String, Object>>();

        while (dbCursor.hasNext()) {
            buildFiles.add((HashMap<String, Object>) dbCursor.next().toMap());
        }

        return buildFiles;
    }

    @Override
    public GridFSDBFile getBuildFile(String fileId) {
        GridFS gridFS = new GridFS(mongoTemplate.getDb(), COLLECTION_NAME);

        GridFSDBFile file = null;
        try {
            file = gridFS.findOne(new BasicDBObject(idK, fileId));
        } catch (Exception e) {
            logger.error("Error while reading the file ", e);
        }

        return file;
    }

    @Override
    public OutputStream createGridFsFileOutputStream(String fileId, String filename) {
        GridFS gridFS = new GridFS(mongoTemplate.getDb(), COLLECTION_NAME);
        GridFSInputFile gridFSInputFile = gridFS.createFile();
        gridFSInputFile.put(idK, fileId);
        gridFSInputFile.put(filenameK, filename);
        uploadFileList.put(fileId, gridFSInputFile);
        return gridFSInputFile.getOutputStream();
    }

    @Override
    public void closeGridFsFileOutputStream(String fileId) {
        try {
            GridFSInputFile gridFSInputFile = uploadFileList.get(fileId);
            if (gridFSInputFile != null) {
                gridFSInputFile.getOutputStream().close();
            }
            deleteBuildFile(fileId);
        } catch (IOException e) {
            logger.error("Something went wrong when deleting the file in the db ", e);
        }
    }

    @Override
    public Map<String, Object> create(Map<String, Object> buildFileInfo) {
        saveBuildFile(buildFileInfo, buildAppFileIdK);
        saveBuildFile(buildFileInfo, buildJadFileIdK);

        mongoTemplate.getCollection(COLLECTION_NAME).save(new BasicDBObject(buildFileInfo));

        return (Map<String, Object>) mongoTemplate.getCollection(COLLECTION_NAME).findOne(new BasicDBObject(buildFileInfo));
    }

    @Override
    public void update(Map<String, Object> buildFileInfo) {
        mongoTemplate.getCollection(COLLECTION_NAME).save(new BasicDBObject(buildFileInfo));
    }

    @Override
    public void delete(Object id) {
        Map<String, Object> buildFile = (Map<String, Object>) mongoTemplate.getCollection(COLLECTION_NAME).findOne(new BasicDBObject(idK, id));
        deleteBuildFile(buildFile.get(buildAppFileIdK));
        if (buildFile.containsKey(buildJadFileIdK)) {
            deleteBuildFile(buildFile.get(buildJadFileIdK));
        }
        mongoTemplate.getCollection(COLLECTION_NAME).remove(new BasicDBObject(idK, id));
    }

    @Override
    public void deleteByAppId(Object appId) {
        for (Map<String, Object> buildFile : findByAppId((String) appId)) {
            deleteBuildFile(buildFile.get(buildAppFileIdK));
            if (buildFile.containsKey(buildJadFileIdK)) {
                deleteBuildFile(buildFile.get(buildJadFileIdK));
            }
        }
        mongoTemplate.getCollection(COLLECTION_NAME).remove(new BasicDBObject(appIdK, appId));
    }

    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
}