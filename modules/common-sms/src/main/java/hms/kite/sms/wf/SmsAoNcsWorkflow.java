/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.sms.wf;

import hms.kite.util.KeyNameSpaceResolver;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.*;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.springframework.beans.factory.annotation.Autowired;

import static hms.kite.util.KiteErrorBox.unknownErrorCode;
import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteErrorBox.mtNotAllowedErrorCode;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SmsAoNcsWorkflow extends WrappedGeneratedWorkflow implements Workflow {
    
    @Autowired private Channel chargingService;
    @Autowired private Channel smsMtChannel;
    @Autowired private  Channel operatorChargingChannel;
    @Autowired private Channel blockedMsisdnValidationService;
    @Autowired private Channel advertisingChannel;
    @Autowired private Channel numberStatusValidationService;
    @Autowired private Channel whitelistService;
    @Autowired private Channel tpsThrottlingChannel;
    @Autowired private Channel tpdThrottlingChannel;
    @Autowired private Channel mainThrottlingChannel;
    @Autowired private Channel provNcsSlaChannel;
    @Autowired private Channel subscriptionCheckChannel;

    @Override
    protected Workflow generateWorkflow() {

        ServiceImpl cancel = new ServiceImpl("cancel-reservation");
        cancel.setChannel(chargingService);
        cancel.addServiceParameter("request-type", cancelCreditK);

        ServiceImpl commit = new ServiceImpl("commit-reservation");
        commit.addServiceParameter("request-type", "commit-credit");
        commit.setChannel(chargingService);

        ServiceImpl smsMt = new ServiceImpl("sms-mt-channel");
        smsMt.setChannel(smsMtChannel);
        smsMt.setOnSuccess(commit);
        smsMt.onDefaultError(cancel);

        ServiceImpl operatorCharging = new ServiceImpl("operator-charging");
        operatorCharging.setChannel(operatorChargingChannel);
        operatorCharging.setOnSuccess(smsMt);
        operatorCharging.onDefaultError(cancel);

        ServiceImpl reserve = new ServiceImpl("reserve-credit");
        reserve.setChannel(chargingService);
        reserve.addServiceParameter("request-type", "reserve-credit");
        reserve.setOnSuccess(operatorCharging);

        ServiceImpl blockedMsisdnChannel = new ServiceImpl("blocked-msisdn-channel");
        blockedMsisdnChannel.setChannel(blockedMsisdnValidationService);
        blockedMsisdnChannel.setOnSuccess(reserve);

        ServiceImpl advertisingChannelService = new ServiceImpl("advertising-channel");
        advertisingChannelService.setChannel(advertisingChannel);
        advertisingChannelService.setOnSuccess(blockedMsisdnChannel);

        EqualExpression advertiseEnabled = new EqualExpression(appK, advertiseK, true);
        Condition advertiseEnabledCondition = new Condition(advertiseEnabled, unknownErrorCode);

        ServiceImpl advertising = new ServiceImpl("advertising.eligibility.service", advertiseEnabledCondition);
        advertising.setOnSuccess(advertisingChannelService);
        advertising.onDefaultError(blockedMsisdnChannel);

        ServiceImpl numberStatusValidateion = new ServiceImpl("number-status-validation-channel");
        numberStatusValidateion.setChannel(numberStatusValidationService);
        numberStatusValidateion.setOnSuccess(advertising);

        ServiceImpl whitelist = new ServiceImpl("white-list");
        whitelist.setChannel(whitelistService);
        whitelist.setOnSuccess(numberStatusValidateion);

        EqualExpression appStatus = new EqualExpression(appK, statusK, limitedProductionK);
        Condition condition = new Condition(appStatus, unknownErrorCode);

        ServiceImpl whitelistApplied = new ServiceImpl("whitelist.eligibility.service", condition);
        whitelistApplied.setOnSuccess(whitelist);
        whitelistApplied.onDefaultError(numberStatusValidateion);

        ServiceImpl ncsTps = new ServiceImpl("tps-throttling-channel");
        ncsTps.setChannel(tpsThrottlingChannel);
        ncsTps.setOnSuccess(whitelistApplied);
        ncsTps.setServiceParameterKeys("ncs-sms-mt");

        ServiceImpl spTps = new ServiceImpl("tps-throttling-channel");
        spTps.setChannel(tpsThrottlingChannel);
        spTps.setOnSuccess(ncsTps);
        spTps.setServiceParameterKeys("sp-sms-mt");

        ServiceImpl ncsTpd = new ServiceImpl("tpd-throttling-channel");
        ncsTpd.setChannel(tpdThrottlingChannel);
        ncsTpd.setOnSuccess(spTps);
        ncsTpd.setServiceParameterKeys("ncs-sms-mt");


        ServiceImpl spTpd = new ServiceImpl("tpd-throttling-channel");
        spTpd.setChannel(tpdThrottlingChannel);
        spTpd.setOnSuccess(ncsTpd);
        spTpd.setServiceParameterKeys("sp-sms-mt");

        ServiceImpl systemTps = new ServiceImpl("main-throttling-channel");
        systemTps.setChannel(mainThrottlingChannel);
        systemTps.setOnSuccess(spTpd);
        systemTps.setServiceParameterKeys("system-tps");

        ServiceImpl subscriptionCheck = new ServiceImpl("subscription.check");
        subscriptionCheck.setChannel(subscriptionCheckChannel);
        subscriptionCheck.setOnSuccess(systemTps);

        ServiceImpl subscriptionRequiredCheck = createSubscriptionRequiredCheckService();
        subscriptionRequiredCheck.setOnSuccess(subscriptionCheck);
        subscriptionRequiredCheck.onDefaultError(systemTps);

        ServiceImpl senderAddressValidationService = createSenderAddressValidationService();
        senderAddressValidationService.setOnSuccess(subscriptionRequiredCheck);

        ServiceImpl ncsValidation = createNcsValidationService();
        ncsValidation.setOnSuccess(senderAddressValidationService);

        ServiceImpl ncsSlaChannel = new ServiceImpl("prov.ncs.sla.channel");
        ncsSlaChannel.setChannel(provNcsSlaChannel);
        ncsSlaChannel.setOnSuccess(ncsValidation);

        return new WorkflowImpl(ncsSlaChannel);
    }

    private ServiceImpl createSubscriptionRequiredCheckService() {
        EqualExpression subscriptionRequired = new EqualExpression(ncsK, subscriptionRequiredK, true);
        Condition subscriptionAppliedCondition = new Condition(subscriptionRequired, unknownErrorCode);
        return new ServiceImpl("subscription.check.validation", subscriptionAppliedCondition);
    }

    private static ServiceImpl createNcsValidationService() {
        ContainsExpression allowedExpression = new ContainsExpression(ncsK, mtK);
        Condition ncsAllowedCondition = new Condition(allowedExpression, mtNotAllowedErrorCode);
        return new ServiceImpl("ncs.state.validation", ncsAllowedCondition);
    }

    private static ServiceImpl createSenderAddressValidationService() {
        ContainsExpression senderAddressAvailable = new ContainsExpression(requestK, senderAddressK);
        NotExpression not = new NotExpression(senderAddressAvailable);
        InExpression alias = new InExpression(KeyNameSpaceResolver.nameSpacedKey(requestK, senderAddressK),
                KeyNameSpaceResolver.nameSpacedKey(ncsK, mtK, aliasingK));
        EqualExpression defaultSenderAddress = new EqualExpression(KeyNameSpaceResolver.nameSpacedKey(requestK, senderAddressK),
                KeyNameSpaceResolver.nameSpacedKey(ncsK, mtK, defaultSenderAddressK));

        OrExpression or = new OrExpression(not, alias, defaultSenderAddress);

        Condition condition = new Condition(or, invalidSourceAddressErrorCode);
        return new ServiceImpl("sender.address.validation", condition);
    }
}
