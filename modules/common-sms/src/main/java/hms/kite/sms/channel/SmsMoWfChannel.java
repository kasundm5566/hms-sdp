/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sms.channel;

import hms.kite.datarepo.RepositoryServiceRegistry;
import hms.kite.util.IdGenerator;
import hms.kite.util.SystemUtil;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;
import org.apache.log4j.NDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.logging.StatLogger.printStatLog;

/**
 * Responsibility of this class is find the suitable workflow for received
 * sms-mo and execute it. Response received from the workflow will be logged in.
 *
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 */
public class SmsMoWfChannel extends BaseChannel {

    private static final Logger logger = LoggerFactory.getLogger(SmsMoWfChannel.class);
    private static String keywordSeperateRegex = "\\s|\\|";

    private Map<String, Workflow> routingWorkflowRegistry;
    private Map<String, String> routingWorkflowNcsTypeMapping;

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        long start = System.currentTimeMillis();
        addParameters(requestContext);

        if (!requestContext.get(requestK).containsKey(correlationIdK)) {
            requestContext.get(requestK).put(correlationIdK, IdGenerator.generate());
        }
        try {
            NDC.push((String) requestContext.get(requestK).get(correlationIdK));
            String serviceKeyword = (String) requestContext.get(requestK).get(serviceKeywordK);
            String keyword = (String) requestContext.get(requestK).get(keywordK);
            String ncsType = (String) requestContext.get(requestK).get(ncsTypeK);
            String flowDirection = (String) requestContext.get(requestK).get(directionK);

            if (logger.isDebugEnabled()) {
                logger.debug("Resolving workflow for message with [{}], [{}] [{}], [{}]",
                        new Object[] { serviceKeyword, keyword, ncsType, flowDirection });
            }
            executeFlow(requestContext, serviceKeyword, ncsType, flowDirection);
            return ResponseBuilder.generateResponse(requestContext);

        } catch (Exception e) {
            logger.error("Exception occurred while executing message flow", e);
            return ResponseBuilder.generate(requestContext, e);
        } finally {
            printStatLog(start, "SMS-MO");
            NDC.pop();
        }
    }

    private void addParameters(Map<String, Map<String, Object>> requestContext) {
        requestContext.get(requestK).put(operatorK, requestContext.get(requestK).get("operator-id"));
        requestContext.get(requestK).put(shortcodeK, requestContext.get(requestK).get(recipientAddressK));
        requestContext.get(requestK).put(ncsTypeK, smsK);
        requestContext.get(requestK).put(directionK, moK);
        requestContext.get(requestK).put(receiveTimeK, Long.toString(System.currentTimeMillis()));
        requestContext.get(requestK).put(chargingTrxIdK, IdGenerator.generate());
        filterAppKeywordAndSystemKeyword(requestContext.get(requestK));

        List<Map<String, String>> recipientList = SystemUtil.generateRecipientList(requestContext.get(requestK));
        logger.debug("Recipient {}", recipientList);
        requestContext.get(requestK).put(recipientsK, recipientList);
    }

    private void executeFlow(Map<String, Map<String, Object>> requestContext, String serviceKeyword, String ncsType,
                             String flowDirection) {

        String workflowKey = generateWorkFlowKey(serviceKeyword, ncsType, flowDirection);
        logger.debug("Work flow key [{}]", workflowKey);
        Workflow workflow = routingWorkflowRegistry.get(workflowKey);
        updateNcsType(requestContext, workflowKey);

        if (null != workflow) {
            workflow.executeWorkflow(requestContext);
        }
    }

    private void updateNcsType(Map<String, Map<String, Object>> requestContext, String workflowKey) {
        String ncsType = routingWorkflowNcsTypeMapping.get(workflowKey);
        if (ncsType != null) {
            logger.debug("Updating ncs type from [{}] to [{}]", requestContext.get(requestK).get(ncsTypeK), ncsType);
            ((Map) requestContext.get(requestK)).put(ncsTypeK, ncsType);
        }
    }

    private String generateWorkFlowKey(String serviceKeyword, String ncsType, String flowDirection) {
        String workflowKey = null;
        if (null != serviceKeyword) {
            workflowKey = ncsType + "-" + flowDirection + "-" + serviceKeyword.toLowerCase();
        } else {
            workflowKey = ncsType + "-" + flowDirection;
        }
        return workflowKey;
    }

    static void filterAppKeywordAndSystemKeyword(Map<String, Object> msg) {
        String keyword = get1stKeyword((String) msg.get(messageK));
        if (RepositoryServiceRegistry.systemConfiguration().isServiceKeyword(keyword)) {
            msg.put(serviceKeywordK, keyword);
            msg.put(keywordK, get2ndKeyword(((String) msg.get(messageK)), keyword));
        } else {
            msg.put(keywordK, keyword);
        }
    }

    static String get1stKeyword(String message) {
        if (message == null) {
            return "";
        }

        String keyword;
        message = message.trim().toLowerCase();
        message = removeForAnyPipe(message);
        String[] splittedArray = message.split(keywordSeperateRegex);
        if(splittedArray == null || splittedArray.length < 1) {
            return message;
        } else {
            keyword = splittedArray[0];
            if(keyword.length() > 30) {
                logger.warn("Word identified for keyword is lengthier than 30 characters. So consider it as empty");
                keyword = "";
            }
            return keyword;
        }
    }

    private static String removeForAnyPipe(String message) {
        if(message.startsWith("|")){
            return removeForAnyPipe(message.substring(1));
        } else {
            return message;
        }
    }

    static String get2ndKeyword(String message, String keyword) {
        if (null == message) {
            return "";
        }
        message = message.trim().toLowerCase();
        return get1stKeyword(message.substring(message.indexOf(keyword) + keyword.length()));
    }

    public void setRoutingWorkflowRegistry(Map<String, Workflow> routingWorkflowRegistry) {
        this.routingWorkflowRegistry = routingWorkflowRegistry;
    }

    private static void setKeywordSeperateRegex(String keywordSeperateRegex) {
        SmsMoWfChannel.keywordSeperateRegex = keywordSeperateRegex;
    }

    public void setRoutingWorkflowNcsTypeMapping(Map<String, String> routingWorkflowNcsTypeMapping) {
        this.routingWorkflowNcsTypeMapping = routingWorkflowNcsTypeMapping;
    }
}
