/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.sms.service;

import hms.kite.util.SdpException;
import hms.kite.util.SystemUtil;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteErrorBox.invalidRequestErrorCode;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.KiteKeyBox.encodingK;
import static hms.kite.util.KiteKeyBox.requestK;
import static hms.kite.util.NblRequestValidator.*;
import static hms.kite.util.NblRequestValidator.isEncodingValid;
import static hms.kite.wfengine.impl.ResponseBuilder.generateSuccess;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class NblRequestValidatorService implements Channel {

    private static final Logger logger = LoggerFactory.getLogger(NblRequestValidatorService.class);

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        try {
            logger.debug("RequestContext: [{}]", requestContext);
            validate(requestContext);
        } catch (Exception ex) {
            logger.warn("Validation failed for the request [{}], [{}]", requestContext.get(requestK),
                    ex.getMessage());
            requestContext.get(requestK).put("validation-error", ex.getMessage());
            throw new SdpException(invalidRequestErrorCode);
        }
        convert2NblParameters(requestContext);
        return generateSuccess();
    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        return generateSuccess();
    }

    private void validate(Map<String, Map<String, Object>> requestContext) {
        validLoginDetails(requestContext.get(requestK));
        isMessageValid(requestContext.get(requestK).get(messageK));
        destinationsValid(requestContext.get(requestK).get(addressK));
        isValidStatusRequest(requestContext.get(requestK).get(statusRequestK));
        isEncodingValid(requestContext.get(requestK).get(encodingK));
    }

    private void convert2NblParameters(Map<String, Map<String, Object>> requestContext) {
        requestContext.get(requestK).put(directionK, mtK);
        addRecipients(requestContext.get(requestK));
        requestContext.get(requestK).put(appIdK, requestContext.get(requestK).get(applicationIdK));
        if (null != requestContext.get(requestK).get(sourceAddressNblK)
                && !((String) requestContext.get(requestK).get(sourceAddressNblK)).isEmpty()) {
            requestContext.get(requestK).put(senderAddressK, requestContext.get(requestK).get(sourceAddressNblK));
        }
        if (null != requestContext.get(requestK).get(chargingAmountNblK)
                && !((String) requestContext.get(requestK).get(chargingAmountNblK)).isEmpty()) {
            requestContext.get(requestK).put(chargingAmountK, requestContext.get(requestK).get(chargingAmountNblK));
        }
    }

    private void addRecipients(Map<String, Object> msg) {
        if (!msg.containsKey(recipientsK)) {
            msg.put(recipientsK, SystemUtil.convert2NblAddresses((List<String>) msg.get(addressK)));
        }
    }
}
