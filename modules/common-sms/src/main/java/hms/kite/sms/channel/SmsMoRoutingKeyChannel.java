/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sms.channel;

import hms.kite.datarepo.RepositoryServiceRegistry;
import hms.kite.util.SdpException;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.util.KiteErrorBox.invalidRoutingkeyErrorCode;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SmsMoRoutingKeyChannel implements Channel {
    private static final Logger logger = LoggerFactory.getLogger(SmsMoRoutingKeyChannel.class);

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        String operator = (String) requestContext.get(requestK).get(operatorK);
        String shortCode = (String) requestContext.get(requestK).get(shortcodeK);
        String keyword = (String) requestContext.get(requestK).get(keywordK);

        Map<String, Object> routingKey = findSharedShortCodeRoutingKey(operator, shortCode, keyword);

        if (null == routingKey) {
            routingKey = findExclusiveShortCodeRoutingKey(operator, shortCode);
        }

        if (null == routingKey || !isProvisioned(routingKey)) {
            logger.warn("Routing key found [{}] was empty or not provisioned", routingKey);
            return ResponseBuilder.generate(invalidRoutingkeyErrorCode, false, requestContext);
        }

        logger.debug("Routing key [{}] found", routingKey);

        addRoutingKeyToContext(requestContext, routingKey);

        return ResponseBuilder.generateSuccess();
    }

    private boolean isProvisioned(Map<String, Object> routingKey) {
        return (routingKey.containsKey(appIdK) && null != routingKey.get(appIdK));
    }

    private Map<String, Object> findSharedShortCodeRoutingKey(String operator, String shortCode, String keyword) {
        logger.debug("Trying to find shared routing key with operator[{}], short code[{}], keyword[{}]" ,
                new Object[]{operator, shortCode, keyword});
        return RepositoryServiceRegistry.smsRoutingKeyRepositoryService()
                .findSharedShortCodeRoutingKey(operator, shortCode, keyword);
    }

    private Map<String, Object> findExclusiveShortCodeRoutingKey(String operator, String shortCode) {
        logger.debug("Trying to find exclusive routing key with operator[{}], short code[{}]",
                new Object[]{operator, shortCode});
        Map<String, Object> routingKey = RepositoryServiceRegistry.smsRoutingKeyRepositoryService()
                    .findExclusiveShortCodeRoutingKey(operator, shortCode);

        if (null == routingKey || !isExclusiveShortCode(routingKey)) {
            throw new SdpException(invalidRoutingkeyErrorCode);
        }

        return routingKey;

    }

    private boolean isExclusiveShortCode(Map<String, Object> routingKey) {
        return (null != routingKey.get(exclusiveK) && ((Boolean) routingKey.get(exclusiveK)));
    }

    private void addRoutingKeyToContext(Map<String, Map<String, Object>> requestContext, Map<String, Object> routingKey) {
        requestContext.get(requestK).put(appIdK, routingKey.get(appIdK));
    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        //no impl needed
        return null;
    }

}
