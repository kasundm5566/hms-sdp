/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.sms.channel;

import hms.kite.util.*;
import hms.kite.util.logging.KiteLog.LogField;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KeyNameSpaceResolver.data;
import static hms.kite.util.KeyNameSpaceResolver.putData;
import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 */
public class SmsAoNcsWfChannel extends GenericWfChannel implements Channel {
	private static final Logger logger = LoggerFactory.getLogger(SmsAoNcsWfChannel.class);

	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
		List<String> operatorList = findOperatorList(requestContext.get(requestK));
		logger.info("Sending SMS AO message to operators [{}]", operatorList);

		for (String operator : operatorList) {
			Map<String, Map<String, Object>> operatorContext = filterRecipientList(operator, requestContext);
			try {

				workflow.executeWorkflow(operatorContext);
                logger.trace("[] Operators Context after executing ncs wf [{}]", operator, operatorContext.get(requestK));
                updateContext(requestContext, operatorContext);

			} catch (SdpException e) {
				logger.error("Sdp exception occurred while executing flow", e);
				updateContextForError(requestContext, operatorContext, e.getErrorCode());
			} catch (Exception e) {
				logger.error("Exception occurred while executing flow", e);
				updateContextForError(requestContext, operatorContext, systemErrorCode);
			}
		}

		updateOverallStatusCode(requestContext);
		return ResponseBuilder.generateResponse(requestContext);
	}

	private void updateContext(Map<String, Map<String, Object>> requestContext,
			Map<String, Map<String, Object>> optContext) {
		List<Map<String, String>> optRecipientList = (List<Map<String, String>>) optContext.get(requestK).get(
				recipientsK);
		List<Map<String, String>> recipientList = (List<Map<String, String>>) requestContext.get(requestK).get(
				recipientsK);
		Map<String, Object> request = requestContext.get(KiteKeyBox.requestK);
		Map<String, Object> ncs = optContext.get(KiteKeyBox.ncsK);

		updateAdvertisementKeys(requestContext, optContext);
        updateValidationErrors(requestContext, optContext);

		if (trueK.equals(optContext.get(requestK).get(statusK))) {
			for (final Map<String, String> optRecipient : optRecipientList) {
				for (Map<String, String> recipient : recipientList) {
					if (optRecipient.get(recipientAddressK).equals(recipient.get(recipientAddressK))) {
						recipient.put(recipientAddressStatusK, optRecipient.get(recipientAddressStatusK));
						updateRecipientChargingData(optRecipient, recipient);
						putChargingTypeDetailsToRecipients(request, ncs, recipient);
					}
				}
			}
        } else {
            for (final Map<String, String> optRecipient : optRecipientList) {
                for (Map<String, String> recipient : recipientList) {
                    if (optRecipient.get(recipientAddressK).equals(recipient.get(recipientAddressK))) {
                        if (!successCode.equals(optRecipient.get(recipientAddressStatusK))) {
                            recipient.put(recipientAddressStatusK, optRecipient.get(recipientAddressStatusK));
                            recipient.put(recipientAddressStatusDescriptionK, optRecipient.get(recipientAddressStatusDescriptionK));
                        } else {
                            recipient.put(recipientAddressStatusK, (String) optContext.get(requestK).get(statusCodeK));
                            recipient.put(recipientAddressStatusDescriptionK, (String) optContext.get(requestK).get(statusDescriptionK));
                        }
                        updateRecipientChargingData(optRecipient, recipient);
                        putChargingTypeDetailsToRecipients(request, ncs, recipient);
                    }
                }
            }
        }

	}

	private void updateRecipientChargingData(final Map<String, String> optRecipient, Map<String, String> recipient) {
		recipient.put(currencyCodeK, optRecipient.get(currencyCodeK));
		recipient.put(usedExchangeRatesK, refineRateCard(optRecipient.get(usedExchangeRatesK)));
		recipient.put(externalTransIdK, optRecipient.get(externalTransIdK));
		recipient.put(LogField.CHARGE_AMOUNT.getParameter(), optRecipient.get(chargedAmountK));
		recipient.put(LogField.CHARGED_CATEGORY.getParameter(), optRecipient.get(chargedCategoryK));
		recipient.put(LogField.OPERATOR_COST.getParameter(), optRecipient.get(operatorCostK));
		recipient.put(LogField.EVENT_TYPE.getParameter(), optRecipient.get(chargingStatusK));
        recipient.put(KiteKeyBox.paymentInstrumentNameK, optRecipient.get(PgwParam.FROM_PAYMENT_INS_NAME.getParameter()));
	}

	private String refineRateCard(String rateCard) {
		if (rateCard != null && rateCard.endsWith(",")) {
			return rateCard.substring(0, rateCard.lastIndexOf(","));
		} else {
			return rateCard;
		}
	}

	private void updateAdvertisementKeys(Map<String, Map<String, Object>> requestContext,
			Map<String, Map<String, Object>> optContext) {
		requestContext.get(KiteKeyBox.requestK).put(KiteKeyBox.adNameK,
				optContext.get(KiteKeyBox.requestK).get(KiteKeyBox.adNameK));
		requestContext.get(KiteKeyBox.requestK).put(KiteKeyBox.adTextK,
				optContext.get(KiteKeyBox.requestK).get(KiteKeyBox.adTextK));
	}

	private void putChargingTypeDetailsToRecipients(Map<String, Object> request, Map<String, Object> ncs,
			Map<String, String> recipient) {
		if (ncs != null && request != null) {
			Map<String, Object> flowDetails = (Map<String, Object>) ncs.get(request.get(directionK));
			if (flowDetails != null && flowDetails.containsKey(chargingK)) {
				Map<String, String> chargingDetails = (Map<String, String>) flowDetails.get(chargingK);
				if (chargingDetails != null) {
					recipient.put(LogField.CHARGING_TYPE.getParameter(), chargingDetails.get(typeK));
					recipient.put(LogField.CHARGE_PARTY_TYPE.getParameter(), chargingDetails.get(partyK));
				}
			}
		}
	}

	private void updateContextForError(Map<String, Map<String, Object>> requestContext,
			Map<String, Map<String, Object>> optContext, String errorCode) {
		List<Map<String, String>> optRecipientList = (List<Map<String, String>>) optContext.get(requestK).get(
				recipientsK);
		List<Map<String, String>> recipientList = (List<Map<String, String>>) requestContext.get(requestK).get(
				recipientsK);
		Map<String, Object> request = requestContext.get(KiteKeyBox.requestK);
		Map<String, Object> ncs = optContext.get(KiteKeyBox.ncsK);

        updateValidationErrors(requestContext, optContext);

		for (final Map<String, String> optRecipient : optRecipientList) {
			for (Map<String, String> recipient : recipientList) {
				if (optRecipient.get(recipientAddressK).equals(recipient.get(recipientAddressK))) {
					recipient.put(recipientAddressStatusK, errorCode);
					putChargingTypeDetailsToRecipients(request, ncs, recipient);
				}
			}
		}
	}

    private void updateValidationErrors(Map<String, Map<String, Object>> requestContext, Map optContext) {
        String validationError = (String) data((Map) optContext, requestK, validationErrorK);
        if (validationError != null) {
            putData(requestContext, validationError, requestK, validationErrorK);
        }
    }

    private List<String> findOperatorList(Map<String, Object> request) {
		List<String> operatorList = new ArrayList<String>();

		if (request.containsKey(recipientsK)) {

			List<Map<String, String>> recipientList = (List<Map<String, String>>) request.get(recipientsK);

			for (final Map<String, String> recipient : recipientList) {
				if (null != recipient.get(recipientAddressOptypeK)
						&& !unknownK.equals(recipient.get(recipientAddressOptypeK))
						&& !operatorList.contains(recipient.get(recipientAddressOptypeK))) {
					operatorList.add(recipient.get(recipientAddressOptypeK));
				}
			}
		}
		return operatorList;
	}

	private Map<String, Map<String, Object>> filterRecipientList(String operator,
			Map<String, Map<String, Object>> requestContext) {
		Map<String, Map<String, Object>> optContext = SystemUtil.copy(requestContext);

		List<Map<String, String>> recipientList = (List<Map<String, String>>) requestContext.get(requestK).get(
				recipientsK);

		List<Map<String, String>> optRecipientList = new ArrayList<Map<String, String>>();

		for (final Map<String, String> recipient : recipientList) {
			if (recipient.containsKey(recipientAddressOptypeK)
					&& operator.equals(recipient.get(recipientAddressOptypeK))) {
				optRecipientList.add(recipient);
			}
		}
		optContext.get(requestK).put(recipientsK, optRecipientList);
		optContext.get(requestK).put(operatorK, operator);
		return optContext;
	}

	@Override
	public Map<String, Object> send(Map<String, Object> request) {
		// no impl needed
		return null;
	}

	private void updateOverallStatusCode(Map<String, Map<String, Object>> requestContext) {
		if (isRequestFailed(requestContext)) {
			return;
		}

		String overallStatus = findOverallStatusCodeForDestinations(requestContext);
		requestContext.get(requestK).put(statusCodeK, overallStatus);
		requestContext.get(requestK).put(statusDescriptionK,
				ResponseBuilder.getErrorDescription(overallStatus, requestContext));
	}

	private boolean isRequestFailed(Map<String, Map<String, Object>> requestContext) {
		return !successCode.equals(requestContext.get(requestK).get(statusCodeK));
	}

	private String findOverallStatusCodeForDestinations(Map<String, Map<String, Object>> requestContext) {
		if (isAllDestinationsSuccess(requestContext)) {
			return successCode;
		} else {
			List<String> failureCodes = getAllDestinationErrorCodes(requestContext);
			if (isAllFailed(requestContext, failureCodes)) {
				if (isSameErrorCode(failureCodes)) {
					return failureCodes.get(0);
				} else {
					return unknownErrorCode;
				}
			} else {
				return partialSuccessCode;
			}
		}
	}

	private boolean isSameErrorCode(List<String> failureCodes) {
		String previousCode = null;
		for (String errorCode : failureCodes) {
			if (null == previousCode) {
				previousCode = errorCode;
			} else if (!previousCode.equals(errorCode)) {
				return false;
			}
		}
		return true;
	}

	private boolean isAllFailed(Map<String, Map<String, Object>> requestContext, List<String> failureCodes) {
		if (failureCodes.size() == ((List<Map<String, String>>) requestContext.get(requestK).get(recipientsK)).size()) {
			return true;
		}

		return false;
	}

	private List<String> getAllDestinationErrorCodes(Map<String, Map<String, Object>> requestContext) {
		List<String> errorCodes = new ArrayList<String>();

		for (Map<String, String> recipient : (List<Map<String, String>>) requestContext.get(requestK).get(recipientsK)) {
			if (!successCode.equals(recipient.get(recipientAddressStatusK))) {
				errorCodes.add(recipient.get(recipientAddressStatusK));
			}
		}

		return errorCodes;
	}

	private boolean isAllDestinationsSuccess(Map<String, Map<String, Object>> requestContext) {
		for (Map<String, String> recipient : (List<Map<String, String>>) requestContext.get(requestK).get(recipientsK)) {
			if (!successCode.equals(recipient.get(recipientAddressStatusK))) {
				return false;
			}
		}

		return true;
	}
}
