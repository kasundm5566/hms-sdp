/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sms.channel;

import hms.kite.wfengine.Workflow;

import java.util.Map;

import static hms.kite.util.KiteKeyBox.ncsTypeK;
import static hms.kite.util.KiteKeyBox.requestK;
import static hms.kite.util.KiteKeyBox.subscriptionK;

/**
 * Created by IntelliJ IDEA.
 * User: arosha
 * Date: 11/19/12
 * Time: 5:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class SmsBroadcastWfChannel extends GenericWfChannel {

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        requestContext.get(requestK).put(ncsTypeK, subscriptionK);
        workflow.executeWorkflow(requestContext);
        return generateResponse(requestContext);
    }
}
