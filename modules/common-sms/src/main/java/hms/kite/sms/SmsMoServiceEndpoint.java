package hms.kite.sms;

import hms.kite.util.KiteKeyBox;
import hms.kite.wfengine.ServiceEndpoint;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;

@Produces({"application/json", "application/xml"})
@Consumes({"application/json", "application/xml"})
@Path("/sms/")
public class SmsMoServiceEndpoint extends ServiceEndpoint {

    private static final Logger logger = LoggerFactory.getLogger(SmsMoServiceEndpoint.class);
    protected Channel smsMoChannel;
    protected Channel smsSrChannel;

    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    @Path("/receive")
    public Map<String, Object> onMoSmsMessageSimple(Map<String, Object> msg) {
        logger.info("Received sms mo from sbl [{}]", msg );
        Map<String, Object> response = (Map) smsMoChannel.send(msg);
        logger.info("Sending sms mo response to sbl [{}]", response);
        return response;
    }

    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    @Path("/report")
    public Map<String, Object> onSmsDr(Map<String, Object> msg) {
        logger.info("Received sms sr from sbl [" + msg + "]");
        msg.put(KiteKeyBox.deliveryStatusNblK, msg.get(KiteKeyBox.statusK));
        Map<String, Object> response = (Map) smsSrChannel.send(msg);
        logger.info("Sending sms sr response to sbl [{}]", response);
        return response;
    }

    public void setSmsMoChannel(Channel smsMoChannel) {
        this.smsMoChannel = smsMoChannel;
    }

    public void setSmsSrChannel(Channel smsSrChannel) {
        this.smsSrChannel = smsSrChannel;
    }
}
