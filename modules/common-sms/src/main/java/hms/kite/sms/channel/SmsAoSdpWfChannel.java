/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.sms.channel;

import hms.kite.util.SdpException;
import hms.kite.util.SystemUtil;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SmsAoSdpWfChannel extends BaseChannel {
    private static final Logger logger = LoggerFactory.getLogger(SmsAoSdpWfChannel.class);

    private Workflow broadcastRequestWf;
    private Workflow smsAoWf;

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        if (!isRecipientListAvailable(requestContext)) {
            createRecipientList(requestContext);
        }

        if (SystemUtil.isBroadcastRequest(requestContext)) {
            requestContext.get(requestK).put(ncsTypeK, smsK);
            return executeWf(requestContext, broadcastRequestWf);
        } else {
            return executeWf(requestContext, smsAoWf);
        }
    }

    private boolean isRecipientListAvailable(Map<String, Map<String, Object>> requestContext) {
        return requestContext.get(requestK).containsKey(recipientsK);
    }

    private void createRecipientList(Map<String, Map<String, Object>> requestContext) {
        requestContext.get(requestK).put(recipientsK,
                SystemUtil.convert2NblAddresses((List<String>) requestContext.get(requestK).get(addressK)));
    }

    private Map<String, Object> executeWf(Map<String, Map<String, Object>> requestContext, Workflow wf) {

        try {
            wf.executeWorkflow(requestContext);
            return ResponseBuilder.generateResponse(requestContext);

        } catch (SdpException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Exception occurred while executing flow", e);
            return ResponseBuilder.generate(requestContext, e);
        }
    }

    public void setBroadcastRequestWf(Workflow broadcastRequestWf) {
        this.broadcastRequestWf = broadcastRequestWf;
    }

    public void setSmsAoWf(Workflow smsAoWf) {
        this.smsAoWf = smsAoWf;
    }
}
