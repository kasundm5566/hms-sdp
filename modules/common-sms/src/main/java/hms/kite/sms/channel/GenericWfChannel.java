/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sms.channel;

import hms.kite.util.KiteErrorBox;
import hms.kite.util.KiteKeyBox;
import hms.kite.util.SdpException;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Generic channel which delegates the work to a workflow. The workflow name and the registry should be passed
 * <p/>
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class GenericWfChannel extends BaseChannel {

    private static final Logger logger = LoggerFactory.getLogger(GenericWfChannel.class);

    protected Workflow workflow;

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        return executeWf(requestContext);
    }

    /**
     * Executes the workflow
     *
     * @param requestContext
     * @return
     */
    protected Map<String, Object> executeWf(Map<String, Map<String, Object>> requestContext) {

        try {
            logger.debug("Attempting to execute the [{}] workflow", workflow);
            if(workflow == null) {
                logger.error("Workflow [{}] does not exist in the workflow registry", workflow);
                throw new SdpException(KiteErrorBox.systemErrorCode,
                        String.format("Workflow [%s] does not exist in the workflow registry", workflow));
            }
            workflow.executeWorkflow(requestContext);
            return generateResponse(requestContext);
        } catch (SdpException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Exception occurred while executing flow", e);
            return ResponseBuilder.generate(requestContext, e);
        }
    }

    /**
     * Generates the response from the request context.
     * Passes the original request back with the response.
     *
     * @param requestContext
     * @return
     */
    protected Map<String, Object> generateResponse(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> response = ResponseBuilder.generateResponse(requestContext);
        Object originalResponse = requestContext.get(KiteKeyBox.responseK);
        if (originalResponse != null)
            response.put(KiteKeyBox.responseK, originalResponse);

        return response;
    }

    public void setWorkflow(Workflow workflow) {
        this.workflow = workflow;
    }
}
