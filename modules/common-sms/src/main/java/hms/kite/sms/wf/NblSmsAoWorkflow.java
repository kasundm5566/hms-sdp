/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sms.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.control.InExpression;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class NblSmsAoWorkflow extends WrappedGeneratedWorkflow implements Workflow {

    private final String name = "sms-ao-nbl";

    @Autowired private Channel smsAoSdpChannel;
    @Autowired private Channel contentFilteringService;
    @Autowired private Channel messageHistoryChannel;
    @Autowired private Channel numberMaskingChannel;
    @Autowired private Channel provSpSlaChannel;
    @Autowired private Channel provAppSlaChannel;
    @Autowired private Channel internalHostResolverChannel;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void executeWorkflow(Map<String, Map<String, Object>> requestContext) {
        generateWorkflow().executeWorkflow(requestContext);
    }

    @Override
    protected Workflow generateWorkflow() {

        final ServiceImpl internalHostService = new ServiceImpl("internal.host.validation");
        internalHostService.setChannel(internalHostResolverChannel);
        internalHostService.setOnSuccess(internalWorkFlow());
        internalHostService.onDefaultError(externalWorkFlow());

        return new WorkflowImpl(internalHostService);
    }

    private ServiceImpl externalWorkFlow() {
        ServiceImpl sdpChannel = new ServiceImpl("sms-ao-sdp-channel");
        sdpChannel.setChannel(smsAoSdpChannel);

        ServiceImpl governanceChannel = new ServiceImpl("governance.channel");
        governanceChannel.setChannel(contentFilteringService);
        governanceChannel.setOnSuccess(sdpChannel);

        EqualExpression governanceEnabled = new EqualExpression(appK, governK, true);
        Condition governanceCondition = new Condition(governanceEnabled, unknownErrorCode);

        ServiceImpl governanceApplied = new ServiceImpl("governance.eligibility.validation", governanceCondition);
        governanceApplied.setOnSuccess(governanceChannel);
        governanceApplied.onDefaultError(sdpChannel);

        ServiceImpl messageHistoryChannelService = new ServiceImpl("message-history-channel", governanceApplied);
        messageHistoryChannelService.setChannel(messageHistoryChannel);
        messageHistoryChannelService.setOnSuccess(governanceApplied);

        ServiceImpl numberMaskingChannelService = new ServiceImpl("number-masking-channel", messageHistoryChannelService);
        numberMaskingChannelService.setChannel(numberMaskingChannel);
        numberMaskingChannelService.addServiceParameter(unmaskK, true);

        EqualExpression maskingApplied = new EqualExpression(appK, maskNumberK, true);
        Condition maskingCondition = new Condition(maskingApplied, unknownErrorCode);

        ServiceImpl numberMaskingApplied = new ServiceImpl("masking.eligibility.validation", maskingCondition);
        numberMaskingApplied.setOnSuccess(numberMaskingChannelService);
        numberMaskingApplied.onDefaultError(messageHistoryChannelService);

        ServiceImpl appStateValidationService = createAppStateValidationService();
        appStateValidationService.setOnSuccess(numberMaskingApplied);

        ServiceImpl spSlaValidation = createSpStateValidation();
        spSlaValidation.setOnSuccess(appStateValidationService);

        ServiceImpl authenticationService = createAuthenticationService();
        authenticationService.setOnSuccess(spSlaValidation);

        ServiceImpl hostValidation = createHostValidationService();
        hostValidation.setOnSuccess(authenticationService);

        ServiceImpl spSlaChannel = new ServiceImpl("prov.sp.sla.channel");
        spSlaChannel.setChannel(provSpSlaChannel);
        spSlaChannel.setOnSuccess(hostValidation);

        ServiceImpl appSlaChannel = new ServiceImpl("prov.app.sla.channel");
        appSlaChannel.setChannel(provAppSlaChannel);
        appSlaChannel.setOnSuccess(spSlaChannel);

        return appSlaChannel;
    }

    private ServiceImpl internalWorkFlow() {
        ServiceImpl sdpChannel = new ServiceImpl("sms-ao-sdp-channel");
        sdpChannel.setChannel(smsAoSdpChannel);

        ServiceImpl governanceChannel = new ServiceImpl("governance.channel");
        governanceChannel.setChannel(contentFilteringService);
        governanceChannel.setOnSuccess(sdpChannel);

        EqualExpression governanceEnabled = new EqualExpression(appK, governK, true);
        Condition governanceCondition = new Condition(governanceEnabled, unknownErrorCode);

        ServiceImpl governanceApplied = new ServiceImpl("governance.eligibility.validation", governanceCondition);
        governanceApplied.setOnSuccess(governanceChannel);
        governanceApplied.onDefaultError(sdpChannel);

        ServiceImpl messageHistoryChannelService = new ServiceImpl("message-history-channel", governanceApplied);
        messageHistoryChannelService.setChannel(messageHistoryChannel);
        messageHistoryChannelService.setOnSuccess(governanceApplied);

        ServiceImpl appStateValidationService = createAppStateValidationService();
        appStateValidationService.setOnSuccess(messageHistoryChannelService);

        ServiceImpl spSlaValidation = createSpStateValidation();
        spSlaValidation.setOnSuccess(appStateValidationService);

        ServiceImpl spSlaChannel = new ServiceImpl("prov.sp.sla.channel");
        spSlaChannel.setChannel(provSpSlaChannel);
        spSlaChannel.setOnSuccess(spSlaValidation);

        ServiceImpl appSlaChannel = new ServiceImpl("prov.app.sla.channel");
        appSlaChannel.setChannel(provAppSlaChannel);
        appSlaChannel.setOnSuccess(spSlaChannel);

        return appSlaChannel;
    }

    private static ServiceImpl createAppStateValidationService() {
        InExpression statusExpression = new InExpression(appK, statusK, new Object[]{limitedProductionK, activeProductionK});
        Condition statusCondition = new Condition(statusExpression, appNotAvailableErrorCode);

        return new ServiceImpl("app.state.validation.service", statusCondition);
    }

    private static ServiceImpl createAuthenticationService() {
        EqualExpression authenticationExpression = new EqualExpression(requestK, passwordK, appK, passwordK);
        Condition condition = new Condition(authenticationExpression, authenticationFailedErrorCode);

        return new ServiceImpl("authentication.service", condition);
    }

    private static ServiceImpl createHostValidationService() {
        InExpression hostExpression = new InExpression(requestK, remotehostK, appK, allowedHostsK);
        Condition condition = new Condition(hostExpression, invalidHostIpErrorCode);

        return new ServiceImpl("host.validation.service", condition);
    }

    private static ServiceImpl createSpStateValidation() {
        EqualExpression statusExpression = new EqualExpression(spK, statusK, approvedK);
        Condition statusCondition = new Condition(statusExpression, authenticationFailedErrorCode);

        InExpression ncsAllowedExpression = new InExpression(spK, spSelectedServicesK, new Object[]{smsK});
        Condition ncsAllowedCondition = new Condition(ncsAllowedExpression, ncsNotAllowedErrorCode);

        return new ServiceImpl("sp.state.validation.service", statusCondition, ncsAllowedCondition);
    }

}
