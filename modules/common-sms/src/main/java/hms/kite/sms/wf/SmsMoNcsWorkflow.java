/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sms.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.ContainsExpression;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.springframework.beans.factory.annotation.Autowired;

import static hms.kite.util.KiteErrorBox.unknownErrorCode;
import static hms.kite.util.KiteErrorBox.moNotAllowedErrorCode;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SmsMoNcsWorkflow extends WrappedGeneratedWorkflow implements Workflow {

    @Autowired private Channel chargingService;
    @Autowired private Channel smsAtChannel;
    @Autowired private Channel numberMaskingChannel;
    @Autowired private Channel tpdThrottlingChannel;
    @Autowired private Channel tpsThrottlingChannel;
    @Autowired private Channel mainThrottlingChannel;
    @Autowired private Channel numberStatusValidationService;
    @Autowired private Channel whitelistService;
    @Autowired private Channel blacklistingService;
    @Autowired private Channel mnpService;
    @Autowired private Channel provNcsSlaChannel;
    @Autowired private Channel subscriptionCheckChannel;

    @Override
    protected Workflow generateWorkflow() {

        ServiceImpl cancel = new ServiceImpl("commit-reservation");
        cancel.setChannel(chargingService);
        cancel.addServiceParameter(requestTypeK, "cancel-credit");

        ServiceImpl commit = new ServiceImpl("commit-reservation");
        commit.setChannel(chargingService);
        commit.addServiceParameter(requestTypeK, "commit-credit");

        ServiceImpl smsAt = new ServiceImpl("sms-at-channel");
        smsAt.setChannel(smsAtChannel);
        smsAt.setOnSuccess(commit);
        smsAt.onDefaultError(cancel);

        ServiceImpl reserve = new ServiceImpl("reserve-credit");
        reserve.setChannel(chargingService);
        reserve.addServiceParameter("request-type", "reserve-credit");
        reserve.setOnSuccess(smsAt);

        ServiceImpl numberMaskingChannelService = new ServiceImpl("number-masking-channel", reserve);
        numberMaskingChannelService.setChannel(numberMaskingChannel);
        numberMaskingChannelService.addServiceParameter(maskK, true);

        EqualExpression maskingApplied = new EqualExpression(appK, maskNumberK, true);
        Condition maskingCondition = new Condition(maskingApplied, unknownErrorCode);

        ServiceImpl numberMaskingApplied = new ServiceImpl("masking.eligibility.validation", maskingCondition);
        numberMaskingApplied.setOnSuccess(numberMaskingChannelService);
        numberMaskingApplied.onDefaultError(reserve);

        ServiceImpl ncsTpd = new ServiceImpl("tpd-throttling-channel");
        ncsTpd.setChannel(tpdThrottlingChannel);
        ncsTpd.setOnSuccess(numberMaskingApplied);
        ncsTpd.setServiceParameterKeys("ncs-sms-mo");

        ServiceImpl spTpd = new ServiceImpl("tpd-throttling-channel");
        spTpd.setChannel(tpdThrottlingChannel);
        spTpd.setOnSuccess(ncsTpd);
        spTpd.setServiceParameterKeys("sp-sms-mo");

        ServiceImpl ncsTps = new ServiceImpl("tps-throttling-channel");
        ncsTps.setChannel(tpsThrottlingChannel);
        ncsTps.setOnSuccess(spTpd);
        ncsTps.setServiceParameterKeys("ncs-sms-mo");

        ServiceImpl spTps = new ServiceImpl("tps-throttling-channel");
        spTps.setChannel(tpsThrottlingChannel);
        spTps.setOnSuccess(ncsTps);
        spTps.setServiceParameterKeys("sp-sms-mo");

        ServiceImpl systemTps = new ServiceImpl("main-throttling-channel");
        systemTps.setChannel(mainThrottlingChannel);
        systemTps.setOnSuccess(spTps);
        systemTps.setServiceParameterKeys("system-tps");

        ServiceImpl ncsSla = createNcsSlaValidationService();
        ncsSla.setOnSuccess(systemTps);

        ServiceImpl numberStatusValidation = new ServiceImpl("number-status-validation-channel");
        numberStatusValidation.setChannel(numberStatusValidationService);
        numberStatusValidation.setOnSuccess(ncsSla);

        ServiceImpl whitelist = new ServiceImpl("white-list");
        whitelist.setChannel(whitelistService);
        whitelist.setOnSuccess(numberStatusValidation);

        EqualExpression appStatus = new EqualExpression(appK, statusK, limitedProductionK);
        Condition condition = new Condition(appStatus, unknownErrorCode);

        ServiceImpl whitelistApplied = new ServiceImpl("whitelist.eligibility.validation", condition);
        whitelistApplied.setOnSuccess(whitelist);
        whitelistApplied.onDefaultError(numberStatusValidation);

        ServiceImpl blacklist = new ServiceImpl("black-list");
        blacklist.setChannel(blacklistingService);
        blacklist.setOnSuccess(whitelistApplied);

        ServiceImpl subscriptionCheck = new ServiceImpl("subscription.check");
        subscriptionCheck.setChannel(subscriptionCheckChannel);
        subscriptionCheck.setOnSuccess(blacklist);

        EqualExpression subscriptionRequired = new EqualExpression(ncsK, subscriptionRequiredK, true);
        Condition subscriptionAppliedCondition = new Condition(subscriptionRequired, unknownErrorCode);

        ServiceImpl subscriptionApplied = new ServiceImpl("subscription.check.validation", subscriptionAppliedCondition);
        subscriptionApplied.setOnSuccess(subscriptionCheck);
        subscriptionApplied.onDefaultError(blacklist);

        ServiceImpl numberChecking = new ServiceImpl("number-checking");
        numberChecking.setChannel(mnpService);
        numberChecking.setOnSuccess(subscriptionApplied);

        ServiceImpl ncsSlaChannel = new ServiceImpl("prov.ncs.sla.channel", numberChecking);
        ncsSlaChannel.setChannel(provNcsSlaChannel);

        return new WorkflowImpl(ncsSlaChannel);
    }

    private static ServiceImpl createNcsSlaValidationService() {

        ContainsExpression allowedExpression = new ContainsExpression(ncsK, moK);
        Condition ncsAllowedCondition = new Condition(allowedExpression, moNotAllowedErrorCode);

        return new ServiceImpl("ncs.state.validation.service", ncsAllowedCondition);
    }

}
