
/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sms.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.impl.BranchingService;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Map;

import static hms.kite.util.KiteErrorBox.unknownErrorCode;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 */
public class SdpSmsAoWorkflow extends WrappedGeneratedWorkflow implements Workflow {

    @Autowired private Channel smsAoNcsWfChannel;
    @Autowired private Channel numberMaskingChannel;
    @Autowired private Channel mnpService;
    @Autowired private Channel blacklistingService;
    @Autowired private Channel provSpSlaChannel;
    @Autowired private Channel provAppSlaChannel;

    Logger logger = LoggerFactory.getLogger(SdpSmsAoWorkflow.class);

    @Override
    protected Workflow generateWorkflow() {
        logger.debug("Generating work flow");
        ServiceImpl numberMaskingService = new ServiceImpl("number-masking-channel");
        numberMaskingService.setChannel(numberMaskingChannel);
        numberMaskingService.addServiceParameter(maskK, true);

        Condition maskingCondition = new Condition(new EqualExpression(appK, maskNumberK, true), unknownErrorCode);

        BranchingService numberMaskingOnSubmitRespService = new BranchingService("number-masking-on-submit-resp", numberMaskingService,
                null, maskingCondition);

        ServiceImpl smsAoNcsChannelService = new ServiceImpl("sms-ao-ncs");
        smsAoNcsChannelService.setChannel(smsAoNcsWfChannel);
        smsAoNcsChannelService.setOnSuccess(numberMaskingOnSubmitRespService);
        smsAoNcsChannelService.onDefaultError(numberMaskingOnSubmitRespService);

        ServiceImpl numberChecking = new ServiceImpl("number-checking");
        numberChecking.setChannel(mnpService);
        numberChecking.setOnSuccess(smsAoNcsChannelService);

        ServiceImpl blacklist = new ServiceImpl("black-list");
        blacklist.setChannel(blacklistingService);
        blacklist.setOnSuccess(numberChecking);

        ServiceImpl spSlaChannel = new ServiceImpl("prov.sp.sla.channel", blacklist);
        spSlaChannel.setChannel(provSpSlaChannel);

        ServiceImpl appSlaChannel = new ServiceImpl("prov.app.sla.channel", spSlaChannel);
        appSlaChannel.setChannel(provAppSlaChannel);

        return new WorkflowImpl(appSlaChannel);
    }

    @Override
    public void executeWorkflow(Map<String, Map<String, Object>> requestContext) {
        super.executeWorkflow(requestContext);
    }
}
