/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sms.channel;

import hms.common.rest.util.JsonBodyProvider;
import hms.common.rest.util.client.AbstractWebClient;
import hms.kite.util.NblKeyMapper;
import hms.kite.util.SdpException;
import hms.kite.util.SystemUtil;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.net.ConnectException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import static hms.kite.util.KeyNameSpaceResolver.data;
import static hms.kite.util.KeyNameSpaceResolver.nameSpacedKey;
import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.wfengine.impl.ResponseBuilder.generate;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SmsAtChannel extends AbstractWebClient implements Channel {

    private static final Logger logger = LoggerFactory.getLogger(SmsAtChannel.class);

    private Map<String, String> newNblSmsMoKeyMapper;

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        try {
            WebClient webClient = createWebclient(requestContext);
            Map<String, Object> atMessage = createMessage(requestContext);
            logger.info("Sending sms at message [{}] to url[{}]", atMessage, webClient.getBaseURI());
            Response response = webClient.post(atMessage);
            if (isSuccessHttpResponse(response)) {
                logger.info("Message sent successfully. response [{}]", response.getEntity());
                truncateResponse(response);
                return ResponseBuilder.generateSuccess();
            } else {
                logger.info("Message sending failed http status is [{}]", response.getStatus());
                return generate(applicationFailToProcessRequestErrorCode, false, requestContext);
            }

        } catch (ClientWebApplicationException e) {
            logger.error("Exception occurred while sending sms message", e);
            if (null != e.getCause() && e.getCause() instanceof Fault) {
                Fault f = (Fault) e.getCause();
                if (null != f.getCause() ) {
                    if (f.getCause() instanceof ConnectException) {
                        return generate(appConnectionRefusedErrorCode, false, requestContext);
                    }
                }
            }
            return generate(requestContext, e);
        } catch (SdpException e) {
            throw e;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return generate(requestContext, e);
        }
    }

    private WebClient createWebclient(Map<String, Map<String, Object>> requestContext) {
        String appUrl = findAppUrl(requestContext);
        if (null == appUrl) {
            logger.warn("App connection url is not available");
            throw new SdpException(systemErrorCode);
        }
        return createWebClient(appUrl);
    }

    private Map<String, Object> createMessage(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> params = new HashMap<String, Object>();

        if (!successCode.equals(requestContext.get(requestK).get(senderAddressStatusK))) {
            logger.warn("Sender address [{}] is not allowed and in [{}] status, Sms mo will not be sent",
                    requestContext.get(requestK).get(senderAddressK),
                    requestContext.get(requestK).get(senderAddressStatusK));
            throw new SdpException((String) requestContext.get(requestK).get(senderAddressStatusK));
        }

        params.put(versionK, "1.0");
        params.put(applicationIdK, requestContext.get(appK).get(appIdK));

        if (requestContext.get(requestK).containsKey(senderAddressMaskedK)){
            params.put(senderAddressNblK, SystemUtil.addAddressIdentifier((String) requestContext.get(requestK)
                    .get(senderAddressMaskedK)));
        } else {
            params.put(senderAddressNblK, SystemUtil.addAddressIdentifier((String) requestContext.get(requestK)
                    .get(senderAddressK)));
        }

        params.put(messageK, requestContext.get(requestK).get(messageK));
        params.put(messageIdNblK, requestContext.get(requestK).get(correlationIdK));
        if (requestContext.get(requestK).containsKey(encodingK)) {
            params.put(encodingK, requestContext.get(requestK).get(encodingK));
        } else {
            params.put(encodingK, encodingTextK);
        }

        NblKeyMapper nblKeyMapper = new NblKeyMapper();
        nblKeyMapper.setNewNblApiKeyMapper(newNblSmsMoKeyMapper);
        Map<String, Object> mappedParams = nblKeyMapper.convert(params, false);
        logger.debug("SMS At Message: [{}]", params);
        logger.debug("SMS At Converted Message: [{}]", mappedParams);
        return mappedParams;
    }

    private void truncateResponse(Response response) {
        try {
            readJsonResponse(response);
        } catch (Exception e) {

        }
    }

    private String findAppUrl(Map<String, Map<String, Object>> requestContext) {
        return (String) data((Map)requestContext, nameSpacedKey(ncsK, moK, connectionUrlK));
    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        return null;
    }

    public void setNewNblSmsMoKeyMapper(Map<String, String> newNblSmsMoKeyMapper) {
        this.newNblSmsMoKeyMapper = newNblSmsMoKeyMapper;
    }
}
