/**
 *   (C) Copyright 2011-2012 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sms.channel;

import static hms.kite.util.KiteKeyBox.ncsTypeK;
import static hms.kite.util.KiteKeyBox.recipientsK;
import static hms.kite.util.KiteKeyBox.requestK;
import static hms.kite.util.KiteKeyBox.smsK;

import hms.kite.util.KiteErrorBox;
import hms.kite.util.StatusDescriptionBuilder;
import hms.kite.util.SystemUtil;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 */
public class SmsSrWfChannel extends GenericWfChannel {
	private static final Logger logger = LoggerFactory.getLogger(SmsSrWfChannel.class);

    private Map<String, String> srStatusCodeMap;
    private StatusDescriptionBuilder descriptionBuilder;

    public void init(){
        descriptionBuilder = new StatusDescriptionBuilder("status-message");
        descriptionBuilder.init();
    }

	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
		addParameters(requestContext);
		return executeWf(requestContext);
	}

	private void addParameters(Map<String, Map<String, Object>> requestContext) {
		requestContext.get(requestK).put(ncsTypeK, smsK);
		requestContext.get(requestK).put(directionK, drK);
        addMappedErrorCode(requestContext);
		List<Map<String, String>> recipientList = SystemUtil.generateRecipientList(requestContext.get(requestK));
		logger.debug("Recipient {}", recipientList);
		requestContext.get(requestK).put(recipientsK, recipientList);
	}

    private void addMappedErrorCode(Map<String, Map<String, Object>> requestContext) {
        String status = (String) requestContext.get(requestK).get(statusK);
        String mappedStatus = srStatusCodeMap.get(status);
        if(mappedStatus == null ){
            mappedStatus = status;
        }
        requestContext.get(requestK).put(deliveryStatusK, mappedStatus);
        requestContext.get(requestK).put(deliveryStatusDescriptionK, descriptionBuilder.createStatusDescription(mappedStatus, requestContext));
    }

    public void setSrStatusCodeMap(Map<String, String> srStatusCodeMap) {
        this.srStatusCodeMap = srStatusCodeMap;
    }
}
