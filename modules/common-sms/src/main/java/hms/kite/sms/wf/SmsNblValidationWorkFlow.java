/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sms.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.InExpression;
import hms.kite.wfengine.impl.BranchingService;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;

import static hms.kite.util.KiteKeyBox.addressK;
import static hms.kite.util.KiteKeyBox.requestK;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: arosha
 * Date: 11/19/12
 * Time: 2:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class SmsNblValidationWorkFlow extends WrappedGeneratedWorkflow implements Workflow {

    private final String name = "sms-validation-nbl";

    @Autowired private Channel nblRequestValidatorService;
    @Autowired private Channel nblBroadcastWfChannel;
    @Autowired private Channel smsAoNblWfChannel;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void executeWorkflow(Map<String, Map<String, Object>> requestContext) {
        generateWorkflow().executeWorkflow(requestContext);
    }

    @Override
    protected Workflow generateWorkflow() {

        ServiceImpl smsBroadcastService = new ServiceImpl("sms-broadcast-channel");
        smsBroadcastService.setChannel(nblBroadcastWfChannel);

        ServiceImpl smsNormalService = new ServiceImpl("sms-normal-channel");
        smsNormalService.setChannel(smsAoNblWfChannel);

        Condition branchingCondition = new Condition(new InExpression(requestK, addressK, new String[] {"tel:all"}));

        BranchingService afterValidationSmsChannelService =
                new BranchingService("after-validation-sms-channel", smsBroadcastService, smsNormalService, branchingCondition);

        ServiceImpl nblRequestValidator = new ServiceImpl("nbl-request-validator-service");
        nblRequestValidator.setChannel(nblRequestValidatorService);
        nblRequestValidator.setOnSuccess(afterValidationSmsChannelService);

        return new WorkflowImpl(nblRequestValidator);
    }

    public void setNblRequestValidatorService(Channel nblRequestValidatorService) {
        this.nblRequestValidatorService = nblRequestValidatorService;
    }

    public void setNblBroadcastWfChannel(Channel nblBroadcastWfChannel) {
        this.nblBroadcastWfChannel = nblBroadcastWfChannel;
    }

    public void setSmsAoNblWfChannel(Channel smsAoNblWfChannel) {
        this.smsAoNblWfChannel = smsAoNblWfChannel;
    }
}
