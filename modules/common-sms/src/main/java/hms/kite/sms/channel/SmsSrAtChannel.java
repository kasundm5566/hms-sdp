package hms.kite.sms.channel;

import hms.common.rest.util.JsonBodyProvider;
import hms.common.rest.util.client.AbstractWebClient;
import hms.kite.util.DeliveryStatusKeyMapper;
import hms.kite.util.NblKeyMapper;
import hms.kite.util.SdpException;
import hms.kite.util.SystemUtil;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.net.ConnectException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KeyNameSpaceResolver.data;
import static hms.kite.util.KeyNameSpaceResolver.nameSpacedKey;
import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.wfengine.impl.ResponseBuilder.generate;
import static hms.kite.wfengine.impl.ResponseBuilder.generateSuccess;

public class SmsSrAtChannel extends AbstractWebClient implements Channel {
	private static final Logger logger = LoggerFactory.getLogger(SmsSrAtChannel.class);

    private Map<String, String> newNblSmsSrKeyMapper;

	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
		try {
            logger.debug("RequestContext [{}]", requestContext);
            if(isSrNotRequired(requestContext)) {//DR notification might not needed sometimes in delivery report based operator charging.
                return ResponseBuilder.generateSuccess();
            }
			WebClient webClient = createWebclient(requestContext);
			Map<String, Object> atMessage = createMessage(requestContext);
			logger.info("Sending sms sr message [{}] to url[{}]", atMessage, webClient.getBaseURI());
			Response response = webClient.post(atMessage);
			if (isSuccessHttpResponse(response)) {
                truncateResponse(response);
				logger.info("Message sent successfully. response [{}]", response.getEntity());
				return ResponseBuilder.generateSuccess();
			} else {
				logger.info("Message sending failed http status is [{}]", response.getStatus());
				return generate(applicationFailToProcessRequestErrorCode, false, requestContext);
			}

		} catch (ClientWebApplicationException e) {
			logger.error("Exception occurred while sending sms sr message", e);
			if (null != e.getCause() && e.getCause() instanceof Fault) {
				Fault f = (Fault) e.getCause();
				if (null != f.getCause()) {
					if (f.getCause() instanceof ConnectException) {
						return generate(appConnectionRefusedErrorCode, false, requestContext);
					}
				}
			}
			return generate(requestContext, e);
		} catch (SdpException e) {
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return generate(requestContext, e);
		}
	}

    private boolean isSrNotRequired(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> mtSla = (Map<String, Object>) requestContext.get(ncsK).get(mtK);
        if(mtSla.get(deliveryReportUrlK) == null) {
            return true;
        }
        return false;
    }

    private WebClient createWebclient(Map<String, Map<String, Object>> requestContext) {
        String appUrl = findAppUrl(requestContext);
        if (null == appUrl || appUrl.isEmpty()) {
            throw new SdpException(systemErrorCode);
        }
        return createWebClient(appUrl);
    }

	private Map<String, Object> createMessage(Map<String, Map<String, Object>> requestContext) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(addressK, SystemUtil.addAddressIdentifier(getRecipientAddress(requestContext)));
		params.put(timeStampK, requestContext.get(requestK).get(doneDateK));
		params.put(messageIdNblK, requestContext.get(requestK).get(correlationIdK));

        String smppStatusCode = (String) requestContext.get(requestK).get(deliveryStatusNblK);
        String mappedSdpStatusCode = DeliveryStatusKeyMapper.getSdpStatusCode(smppStatusCode);
        params.put(deliveryStatusNblK, mappedSdpStatusCode);

		/*if (requestContext.get(requestK).containsKey(deliveredK)
				&& Boolean.TRUE.toString().equals(requestContext.get(requestK).get(deliveredK))) {
			params.put(NblKeyMapper.statusCodeNblK, successCode);
			params.put(NblKeyMapper.statusDetailNblK, ResponseBuilder.getErrorDescription(successCode, requestContext));
		} else {
			params.put(NblKeyMapper.statusCodeNblK, requestDeliveryFailErrorCode);
			params.put(NblKeyMapper.statusDetailNblK,
					ResponseBuilder.getErrorDescription(requestDeliveryFailErrorCode, requestContext));
		}*/

        NblKeyMapper nblKeyMapper = new NblKeyMapper();
        nblKeyMapper.setNewNblApiKeyMapper(newNblSmsSrKeyMapper);
        Map<String, Object> mappedParams = nblKeyMapper.convert(params, false);
        logger.debug("SMS SR AT Message: [{}]", params);
        logger.debug("SMS SR AT Converted Message: [{}]", mappedParams);
        return mappedParams;

	}

	private String getRecipientAddress(Map<String, Map<String, Object>> requestContext) {
		List<Map<String, String>> recipients = (List<Map<String, String>>) requestContext.get(requestK).get(recipientsK);
		if (recipients.size() > 0) {
			Map<String, String> recipient = recipients.get(0);
            String recipientAddress = SystemUtil.isMaskedApp (requestContext) ? recipient.get(recipientAddressMaskedK) : recipient.get(recipientAddressK);
			return recipientAddress;
		} else {
			throw new RuntimeException("Invalid number[" + recipients.size() + "] of recipients found for sr request");
		}
	}

    private void truncateResponse(Response response) {
        try {
            readJsonResponse(response);
        } catch (Exception e) {

        }
    }

	private String findAppUrl(Map<String, Map<String, Object>> requestContext) {
		return (String) data((Map) requestContext, nameSpacedKey(ncsK, mtK, deliveryReportUrlK));
	}

	@Override
	public Map<String, Object> send(Map<String, Object> request) {
		return null;
	}

    public void setNewNblSmsSrKeyMapper(Map<String, String> newNblSmsSrKeyMapper) {
        this.newNblSmsSrKeyMapper = newNblSmsSrKeyMapper;
    }
}
