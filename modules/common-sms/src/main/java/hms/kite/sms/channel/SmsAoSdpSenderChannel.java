/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.sms.channel;

import hms.kite.sms.SmsAoServiceEndpoint;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static hms.kite.util.KiteKeyBox.requestK;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SmsAoSdpSenderChannel implements Channel {

    @Autowired private Channel sdpSmsAoChannel;
    private static final Logger LOGGER = LoggerFactory.getLogger(SmsAoSdpSenderChannel.class);

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
    	if(LOGGER.isDebugEnabled()) {
    		LOGGER.debug("Executing channel with [{}]", requestContext.get(requestK));
    	}
        Map<String, Object> response = sdpSmsAoChannel.execute(requestContext);
        //serviceEndpoint.sendServiceInternal(requestContext.get(requestK), requestContext);
        if (null != response ) {
            return response;
        }
        return ResponseBuilder.generateResponse(requestContext);
    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        //no impl needed
        return null;
    }

}
