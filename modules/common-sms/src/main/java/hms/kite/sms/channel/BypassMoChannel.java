/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sms.channel;

import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.KiteErrorBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class BypassMoChannel implements Channel {
    private static final Logger logger = LoggerFactory.getLogger(BypassMoChannel.class);
    private Map<String, String> serviceKeywordUrl = new HashMap<String, String>();

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        String serviceKeyword = (String) requestContext.get(requestK).get(serviceKeywordK);
        String url = serviceKeywordUrl.get(serviceKeyword);
        if (null != url || !url.isEmpty()) {
            logger.info("Bypass url found");
            requestContext.get(requestK).put(bypassUrlK, url);
            return ResponseBuilder.generateSuccess();
        }
        logger.info("Unable to find bypass url");
        return ResponseBuilder.generate(systemErrorCode, false, requestContext);
    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        //no impl needed
        return null;
    }

    public void setServiceKeywordUrl(Map<String, String> serviceKeywordUrl) {
        this.serviceKeywordUrl = serviceKeywordUrl;
    }
}
