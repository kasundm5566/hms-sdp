/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sms.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.control.InExpression;
import hms.kite.wfengine.impl.ErrorResponseRoutingService;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SmsMoWorkflow extends WrappedGeneratedWorkflow implements Workflow {

    @Autowired private Channel smsMoReplySenderChannel;
    @Autowired private Channel smsMoReplyMapperChannel;
    @Autowired private Channel smsMoNcsWfChannel;
    @Autowired private Channel provSpSlaChannel;
    @Autowired private Channel provAppSlaChannel;
    @Autowired private Channel smsMoRoutingKeyChannel;

    @Override
    protected Workflow generateWorkflow() {

    	ErrorResponseRoutingService replyChannel = new ErrorResponseRoutingService("sms-mo-reply-channel");
        replyChannel.setChannel(smsMoReplySenderChannel);

        ErrorResponseRoutingService replyMapperChannel = new ErrorResponseRoutingService("sms-mo-reply-mapper-channel", replyChannel);
        replyMapperChannel.setChannel(smsMoReplyMapperChannel);

        ServiceImpl moNcsChannel = new ServiceImpl("sms-mo-ncs-wf-channel");
        moNcsChannel.setChannel(smsMoNcsWfChannel);
        moNcsChannel.onDefaultError(replyMapperChannel);

        ServiceImpl appStateValidationService = createAppSlaValidationRule();
        appStateValidationService.setOnSuccess(moNcsChannel);
        appStateValidationService.onDefaultError(replyMapperChannel);

        ServiceImpl spSlaValidation = createSpSlaValidationService();
        spSlaValidation.setOnSuccess(appStateValidationService);
        spSlaValidation.onDefaultError(replyMapperChannel);

        ServiceImpl spSlaChannel = new ServiceImpl("prov.sp.sla.channel", spSlaValidation);
        spSlaChannel.setChannel(provSpSlaChannel);
        spSlaChannel.onDefaultError(replyMapperChannel);

        ServiceImpl appSlaChannel = new ServiceImpl("prov.app.sla.channel", spSlaChannel);
        appSlaChannel.setChannel(provAppSlaChannel);
        appSlaChannel.onDefaultError(replyMapperChannel);

        ServiceImpl routingKeyChannel = new ServiceImpl("sms-mo-routing-key-channel", appSlaChannel);
        routingKeyChannel.setChannel(smsMoRoutingKeyChannel);
        routingKeyChannel.onDefaultError(replyMapperChannel);

        return new WorkflowImpl(routingKeyChannel);
    }

    private static ServiceImpl createAppSlaValidationRule() {
        InExpression statusExpression = new InExpression(appK, statusK, new Object[]{limitedProductionK,
                activeProductionK});
        Condition statusCondition = new Condition(statusExpression, authenticationFailedErrorCode);

        return new ServiceImpl("app.state.validation.service", statusCondition);
    }

    private static ServiceImpl createSpSlaValidationService() {
        EqualExpression statusExpression = new EqualExpression(spK, statusK, approvedK);
        Condition statusCondition = new Condition(statusExpression, authenticationFailedErrorCode);

        InExpression ncsAllowedExpression = new InExpression(spK, spSelectedServicesK, new Object[]{smsK});
        Condition ncsAllowedCondition = new Condition(ncsAllowedExpression, ncsNotAllowedErrorCode);

        return new ServiceImpl("sp.state.validation.service", statusCondition, ncsAllowedCondition);
    }

    @Override
    public void executeWorkflow(Map<String, Map<String, Object>> requestContext) {
        super.executeWorkflow(requestContext);
    }
}
