/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sms.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class BypassMoWorkflow extends WrappedGeneratedWorkflow implements Workflow {

    @Autowired private Channel smsAtChannel;
    @Autowired private Channel bypassMoChannel;

    @Override
    protected Workflow generateWorkflow() {
        ServiceImpl smsAt = new ServiceImpl("sms-at-channel");
        smsAt.setChannel(smsAtChannel);

        ServiceImpl bypassChannel = new ServiceImpl("bypass-mo-channel", smsAt);
        bypassChannel.setChannel(bypassMoChannel);

        return new WorkflowImpl(bypassChannel);
    }

    @Override
    public void executeWorkflow(Map<String, Map<String, Object>> requestContext) {
        super.executeWorkflow(requestContext);
    }
}
