/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sms.channel;

import hms.common.registration.api.common.StatusCodes;
import hms.kite.util.SdpException;
import hms.kite.util.SystemUtil;
import hms.kite.util.TimeStampUtil;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static hms.kite.util.KiteErrorBox.invalidRequestErrorCode;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.NblRequestValidator.*;
import static hms.kite.util.logging.StatLogger.printStatLog;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 */
public class SmsAoNblWfChannel extends BaseChannel {
    private static final Logger logger = LoggerFactory.getLogger(SmsAoNblWfChannel.class);

    private Workflow validationWf;

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        addMandatoryParams(requestContext);
        return executeValidationSmsAoWf(requestContext);
    }

    private Map<String, Object> executeValidationSmsAoWf(Map<String, Map<String, Object>> requestContext) {
        long start = System.currentTimeMillis();
        try {
            validationWf.executeWorkflow(requestContext);
            return generateResponseForNbl(requestContext);
        } catch (SdpException e) {
            return convert2NblErrorResponse(ResponseBuilder.generate(e.getErrorCode(), false, requestContext),
                    requestContext);
        } catch (Exception e) {
            logger.error("Exception occurred while executing flow", e);
            return convert2NblErrorResponse(ResponseBuilder.generate(requestContext, e), requestContext);
        } finally {
            printStatLog(start, "SMS-VALIDATION");
        }
    }

    private Map<String, Object> generateResponseForNbl(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> resp = convert2NblResponse(requestContext);
        if (!SystemUtil.isBroadcastRequest(requestContext)) {
            List<Map<String, String>> destinations = new ArrayList<Map<String, String>>();
            for (Map<String, String> recipient : (List<Map<String, String>>) requestContext.get(requestK).get(recipientsK)) {
                Map<String, String> recipientResponseMap = new HashMap<String, String>();
                String recipientAddress = SystemUtil.isMaskedApp (requestContext) ? recipient.get(recipientAddressMaskedK) : recipient.get(recipientAddressK);
                recipientResponseMap.put(responseAddressK, telK + ":" + recipientAddress);
                recipientResponseMap.put(timestampK, TimeStampUtil.formatToNblTimeStamp(new Date()));
                recipientResponseMap.put(messageIdK, (String) requestContext.get(requestK).get(correlationIdK));
                if (recipient.containsKey(recipientAddressStatusK)) {
                    recipientResponseMap.put(statusCodeNblK, recipient.get(recipientAddressStatusK));
                    if (recipient.get(recipientAddressStatusDescriptionK) != null) {
                        recipientResponseMap.put(statusDescriptionNblK, recipient.get(recipientAddressStatusDescriptionK));
                    } else {
                        recipientResponseMap
                                .put(statusDescriptionNblK, ResponseBuilder.getErrorDescription(
                                        recipient.get(recipientAddressStatusK), requestContext));
                    }
                } else {
                    recipientResponseMap.put(statusCodeNblK, requestContext.get(requestK).get(statusCodeK).toString());
                    recipientResponseMap.put(statusDescriptionNblK, requestContext.get(requestK).get(statusDescriptionK).toString());
                }
                destinations.add(recipientResponseMap);
            }
            resp.put(destinationResponsesNblK, destinations);
        }
        return resp;
    }

    private Map<String, Object> convert2NblResponse(Map<String, Map<String, Object>> requestContext) {
        return convert2NblErrorResponse(ResponseBuilder.generateResponse(requestContext), requestContext);
    }

    private Map<String, Object> convert2NblErrorResponse(Map<String, Object> response,
                                                         Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> nblResp = ResponseBuilder.convert2NblErrorResponse(response);
        if (requestContext.get(requestK).containsKey(correlationIdK)) {
            nblResp.put(messageIdNblK, requestContext.get(requestK).get(correlationIdK));
        }
        return nblResp;
    }

    private void addMandatoryParams(Map<String, Map<String, Object>> requestContext) {
        requestContext.get(requestK).put(ncsTypeK, smsK);
        requestContext.get(requestK).put(receiveTimeK, Long.toString(System.currentTimeMillis()));
    }

    public void setValidationWf(Workflow validationWf) {
        this.validationWf = validationWf;
    }
}