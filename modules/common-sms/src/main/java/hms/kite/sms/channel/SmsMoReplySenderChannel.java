/**
 *   (C) Copyright 2011-2012 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sms.channel;

import hms.kite.util.SystemUtil;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteErrorBox.successCode;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 */
public class SmsMoReplySenderChannel implements Channel {

	private static final Logger logger = LoggerFactory.getLogger(SmsMoReplySenderChannel.class);

	private Channel smsMtChannel;
    private String systemSenderAddress;

	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
		String reply = (String) requestContext.get(requestK).get(replyK);
		logger.debug("Request Context [{}]", requestContext);
		if (null == reply) {
			logger.debug("No reply need to send to sender");
		} else {
			logger.debug("Sending reply [{}] to sender", reply);

			Map<String, Object> request = new HashMap<String, Object>();
			request.put(messageK, reply);
			addSenderAddress(requestContext, request);
			request.put(recipientsK, addRecipient(requestContext));
			request.put(operatorK, requestContext.get(requestK).get(operatorK));
			request.put(correlationIdK, Long.toString(SystemUtil.getCorrelationId()));
			request.put(srrK, Boolean.toString(false));

			smsMtChannel.send(request);
		}

		return ResponseBuilder.generateSuccess();
	}

	private void addSenderAddress(Map<String, Map<String, Object>> requestContext, Map<String, Object> request) {
        if (systemSenderAddress != null && systemSenderAddress.trim().length() != 0) {
            request.put(senderAddressK, systemSenderAddress);
        } else if (requestContext.get(requestK).containsKey(recipientAddressK)) {
			request.put(senderAddressK, requestContext.get(requestK).get(recipientAddressK));
		} else if (requestContext.get(requestK).containsKey(recipientsK)) {
			request.put(senderAddressK, ((List<Map<String, String>>) requestContext.get(requestK).get(recipientsK))
					.get(0).get(recipientAddressK));
		}
	}

    private List<Map<String, String>> addRecipient(Map<String, Map<String, Object>> requestContext) {
        List<Map<String, String>> recipients = new ArrayList<Map<String, String>>();
        Map<String, String> recipient = new HashMap<String, String>();
        String recipientAddr = "";
        String sender = (String) requestContext.get(requestK).get(senderAddressK);
        if (sender != null && !sender.trim().isEmpty()) {
            recipientAddr = sender;
        } else if (requestContext.get(requestK).containsKey(recipientsK)) {
            recipientAddr = ((List<Map<String, String>>) requestContext.get(requestK).get(recipientsK))
                    .get(0).get(recipientAddressK);
        }
        recipient.put(recipientAddressK, recipientAddr);
        recipient.put(recipientAddressOptypeK, (String) requestContext.get(requestK).get(operatorK));
        recipient.put(recipientAddressStatusK, successCode);
        recipient.put(recipientAddressSubtypeK, anyK);
        recipients.add(recipient);
        return recipients;
    }

	@Override
	public Map<String, Object> send(Map<String, Object> request) {
		// no impl needed
		return null;
	}

	public void setSmsMtChannel(Channel smsMtChannel) {
		this.smsMtChannel = smsMtChannel;
	}

    public void setSystemSenderAddress(String systemSenderAddress) {
        this.systemSenderAddress = systemSenderAddress;
	}
}
