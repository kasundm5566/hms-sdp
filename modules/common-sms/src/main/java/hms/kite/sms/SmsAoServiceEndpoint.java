package hms.kite.sms;

import hms.commons.IdGenerator;
import hms.kite.util.NblKeyMapper;
import hms.kite.util.SdpException;
import hms.kite.util.SystemUtil;
import hms.kite.wfengine.ServiceEndpoint;
import hms.kite.wfengine.transport.Channel;
import org.apache.log4j.NDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import java.util.HashMap;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.correlationIdK;
import static hms.kite.util.KiteKeyBox.remotehostK;
import static hms.kite.util.KiteKeyBox.requestK;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@Produces({"application/json"})
@Consumes({"application/json"})
@Path("/sms/")
public class SmsAoServiceEndpoint extends ServiceEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(SmsAoServiceEndpoint.class);

    private Channel smsAoChannel;
    private boolean override;
    private Map<String, String> newNblSmsKeyMapper;
    private NblKeyMapper nblKeyMapper;

    @Context
    private org.apache.cxf.jaxrs.ext.MessageContext mc;
    @Context
    private ServletContext sc;

    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    @Path("/send")
    public Map<String, Object> sendService(Map<String, Object> msg,
                                           @Context javax.servlet.http.HttpServletRequest request) {
        try {
            msg.put(correlationIdK, IdGenerator.generateId());
            NDC.push((String) msg.get(correlationIdK));
            return processRequest(msg, request);
        } finally {
            NDC.pop();
        }
    }

    public Map<String, Object> sendServiceInternal(Map<String, Object> msg, @Context javax.servlet.http.HttpServletRequest request) {
        return processRequest(msg, request);
    }

    private Map<String, Object> processRequest(Map<String, Object> msg, HttpServletRequest request) {
        Map<String, Object> respNbl;
        try {
            logger.info("Received Request [{}]", msg);

            Map<String, Object> newMsg = nblKeyMapper.convert(msg, true);
            logger.debug("Received Converted Request [{}]", newMsg);
            addHostDetails(newMsg, request);
            logger.debug("Received request from host [{}]", newMsg.get(remotehostK));
            Map<String, Object> resp = smsAoChannel.send(newMsg);
            respNbl = nblKeyMapper.convert(resp, false);
            logger.debug("Sending response [{}]", resp);

        } catch (SdpException ex) {
            logger.error("An exception occurred: [{}]", ex);
            respNbl = new HashMap<String, Object>();
            respNbl.put(NblKeyMapper.statusCode, ex.getErrorCode());
            respNbl.put(NblKeyMapper.statusDetail, ex.getErrorDescription());
        }
        logger.debug("Sending Converted response [{}]", respNbl);
        return respNbl;
    }

    private void addHostDetails(Map<String, Object> msg, HttpServletRequest request) {
        if (override || null == msg.get(remotehostK)) {
            String host = SystemUtil.findHostIp(request);
            if (null != host) {
                msg.put(remotehostK, host);
            }
        }
    }

    public void setSmsAoChannel(Channel smsAoChannel) {
        this.smsAoChannel = smsAoChannel;
    }

    public void setOverride(boolean override) {
        this.override = override;
    }

    public void setNewNblSmsKeyMapper(Map<String, String> newNblSmsKeyMapper) {
        this.newNblSmsKeyMapper = newNblSmsKeyMapper;
    }

    public void init() {
        nblKeyMapper = new NblKeyMapper();
        nblKeyMapper.setNewNblApiKeyMapper(newNblSmsKeyMapper);
    }
}
