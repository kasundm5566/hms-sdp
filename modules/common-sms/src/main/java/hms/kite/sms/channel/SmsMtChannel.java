/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sms.channel;

import hms.common.rest.util.Message;
import hms.common.rest.util.MessageBodyProvider;
import hms.common.rest.util.client.AbstractWebClient;
import hms.commons.SnmpLogUtil;
import hms.kite.datarepo.RepositoryServiceRegistry;
import hms.kite.util.*;
import hms.kite.wfengine.transport.BaseChannel;
import hms.kite.wfengine.transport.Channel;
import hms.kite.exceptions.NoRecipientFoundException;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.jaxrs.utils.HttpUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.reflect.TypeToken;

import javax.ws.rs.core.Response;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.*;

import static hms.kite.datarepo.RepositoryServiceRegistry.systemConfiguration;
import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.KeyNameSpaceResolver.*;
import static hms.kite.wfengine.impl.ResponseBuilder.generate;
import static hms.kite.wfengine.impl.ResponseBuilder.generateSuccess;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 */
public class SmsMtChannel extends AbstractWebClient implements Channel {

    private final static Logger logger = LoggerFactory.getLogger(SmsMtChannel.class);
    private final static String ACCEPTED = "accepted";
    private final static String QUEUE_FULL = "queue-full";
    private final static String INVALID_REQUEST = "invalid-request";
    private Map<String, WebClient> sblClientMap;
    private MessageCorrelator correlator;
    private Properties snmpMessges;
    private boolean alwaysSendServiceCode;
    private String defaultServiceCode;
    private boolean useServiceCodeShortCodeConvertion = false;

    public SmsMtChannel() {
    }

    public void init() {
        super.init();
        sblClientMap = new HashMap<String, WebClient>();
        Map<String, String> sblUrlMap = (Map<String, String>) RepositoryServiceRegistry.systemConfiguration().find(
                sblSmsReceiverAddressesK);
        logger.trace("SBL sms operator url map {}", sblUrlMap);
        for (Map.Entry<String, String> entry : sblUrlMap.entrySet()) {
            WebClient webClient = createWebClient(entry.getValue());
            sblClientMap.put(entry.getKey(), webClient);
        }
        logger.trace("SBL sms operator web client map {}", sblClientMap);

    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {

        Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String, Object>>();
        requestContext.put(requestK, request);

        return execute(requestContext);
    }

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        try {
            logger.trace("Request context [{}]", requestContext);
            Object operator = requestContext.get(requestK).get(operatorK);
            WebClient webClient = sblClientMap.get(operator);
            if (webClient == null) {
                throw new SdpException(systemErrorCode, "Mt sending failed. Couldn't find webClient for operator [" + operator + "]");
            }
            Map<String, Object> sms = createSblMessage(requestContext);
            logger.info("Sending sms message[{}] to sbl url [{}]", sms, webClient.getCurrentURI());
            correlator.addMessage(sms);
            Response response = sendMtRequest(webClient, sms);
            logger.info("Response received from sbl status[{}] message[{}]", response.getStatus(), response.getEntity());
            if (isStatusSuccess(response)) {
                Map<String, Object> resp = correlator.waitForResponse(sms);
                logger.info("SMS SubmitResponse received [{}]", resp);

                return processResponse(requestContext, resp);
            } else {
                return generateErrorResponse(requestContext, response);
            }

        } catch (NoRecipientFoundException e) {
            logger.info("No valid recipients found to send sms mt message [{}]", requestContext.get(requestK));
            return generateSuccess();
        } catch (EmptyMessageException e) {
            logger.info("Empty message text found to send sms mt message [{}]", requestContext.get(requestK));
            return generateSuccess();
        } catch (SdpException e) {
            throw e;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return generate(temporarySystemErrorCode, false, requestContext);
        }
    }

    private Response sendMtRequest(WebClient webClient, Map<String, Object> sms) {
        try {
            Response response = webClient.post(sms);
            SnmpLogUtil.clearTrap("sdpToSblConnection", (String) snmpMessges.get("sdp.server.to.sbl.server.connection.success.snmp.message"));
            return response;
        } catch (RuntimeException e) {
            logger.error("Couldn't send mt sm. SBL connection failed", e);
            SnmpLogUtil.timeTrap("sdpToSblConnection", Integer.parseInt((String) snmpMessges.get("sdp.server.to.sbl.server.connection.time.delay.in.seconds")), (String) snmpMessges.get("sdp.server.to.sbl.server.connection.fail.snmp.message"));
            throw e;
        }
    }

    private Map<String, Object> generateErrorResponse(Map<String, Map<String, Object>> requestContext, Response response) throws IOException {
        Map<String, Object> resp = readJsonResponse(response);
        if (resp != null) {
            String errorCode = temporarySystemErrorCode;
            if (QUEUE_FULL.equals(resp.get(statusK))) {
                errorCode = temporarySystemErrorCode;
            } else if (INVALID_REQUEST.equals(resp.get(statusK))) {
                errorCode = temporarySystemErrorCode;
            }
            Map<String, Object> errorResponse = generate(errorCode, false, requestContext);
            errorResponse.put(
                    statusDescriptionK,
                    MessageFormat.format("{0}({1})", errorResponse.get(statusDescriptionK),
                            resp.get(statusDescriptionK)));
            return errorResponse;
        } else {
            return generate(temporarySystemErrorCode, false, requestContext);
        }

    }

    private Map<String, Object> createSblMessage(Map<String, Map<String, Object>> requestContext) throws NoRecipientFoundException, EmptyMessageException {
        Map<String, Object> sms = new HashMap<String, Object>();

        createRecipientAddressList(requestContext, sms);
        addStatusRequest(requestContext, sms);
        addEncoding(requestContext, sms);
        addHeaderIfAvailable(requestContext, sms);
        addAppIdIfAvailable(requestContext, sms);
        addOperatorChargingDetailsIfRequired(requestContext, sms);
        addDefaultServiceCode(requestContext, sms);

        sms.put(messageK, getMessageText(requestContext));
        sms.put(correlationIdK, (String) requestContext.get(requestK).get(correlationIdK));
        sms.put(operatorK, (String) requestContext.get(requestK).get(operatorK));
        sms.put(senderAddressK, getSenderAddress(requestContext));

        return sms;
    }

    private String getMessageText(Map<String, Map<String, Object>> requestContext) throws EmptyMessageException {
        String message = (String)requestContext.get(requestK).get(messageK);
        if(message == null || message.isEmpty()){
            throw new EmptyMessageException();
        }
        return message;
    }

    private void addOperatorChargingDetailsIfRequired(Map<String, Map<String, Object>> requestContext, Map<String, Object> sms) {
        Object requestServiceCode = requestContext.get(requestK).get(serviceCodeK);
        if (null == requestServiceCode) {
            return;
        }

        String chargedAmount = (String) requestContext.get(requestK).get(chargedAmountK);
        if(chargedAmount != null){
            sms.put(createAdditionalParameter(chargedAmountK), chargedAmount);
        }

        sms.put(serviceCodeK, (String) requestServiceCode);

        Boolean srr = (Boolean) requestContext.get(requestK).get(srrK);
        if(srr != null && srr) {
            sms.put(srrK, Boolean.TRUE.toString());
        }

        if (!useServiceCodeShortCodeConvertion) {
            return;
        }

        String operator = (String) requestContext.get(requestK).get(operatorK);
        Map<String, String> serviceCodeShortCodeMap = (Map<String, String>) systemConfiguration().find(operator + "-sms-service-code-short-code-mapping");

        logger.debug("operator {0}, service-code {1}- map {2}", new Object[]{operator, requestServiceCode,
                serviceCodeShortCodeMap});

        if (serviceCodeShortCodeMap == null) {
            return;
        }

        String shortCode = serviceCodeShortCodeMap.get(requestServiceCode);
        if (shortCode == null) {
            logger.debug("There is no matching short code for the service code [{}]", requestServiceCode);
            return;
        }
        sms.put(shortcodeK, shortCode);
        requestContext.get(requestK).put(senderAddressK, shortCode);
        logger.debug("Short Code [{}] for Service Code [{}] ", sms.get(shortcodeK), sms.get(serviceCodeK));
    }

    private String createAdditionalParameter(String chargingAmount) {
        return "add_" + chargingAmount;
    }

    private void addAppIdIfAvailable(Map<String, Map<String, Object>> requestContext, Map<String, Object> sms) {
        if (null != requestContext.get(ncsK) && null != requestContext.get(ncsK).get(appIdK)) {
            sms.put(appIdK, (String) requestContext.get(ncsK).get(appIdK));
        }
    }

    private void addStatusRequest(Map<String, Map<String, Object>> requestContext, Map<String, Object> sms) {
        if (isSrrRequested(requestContext)) {
            if (isSrrAllowedBySla(requestContext)) {
                sms.put(srrK, Boolean.TRUE.toString());
            } else {
                logger.info("Status report is requested but not allowed by sla. Request will be rejected");
                String errorDescription = "Can not enable status report for this request. " +
                        "Status reports are not enabled in your sms sla. Please contact administrator to get this permission.";
                putData(requestContext, errorDescription, requestK, validationErrorK);
                throw new SdpException(invalidRequestErrorCode, errorDescription);
            }
        }
    }

    private void addDefaultServiceCode(Map<String, Map<String, Object>> requestContext, Map<String, Object> sms) {
        boolean aServiceNotCodeAvailable = sms.get(serviceCodeK) == null;
        if (alwaysSendServiceCode && aServiceNotCodeAvailable) {
            boolean defaultServiceCodeAvailable = defaultServiceCode != null && !defaultServiceCode.trim().isEmpty();
            if (defaultServiceCodeAvailable) {
                sms.put(serviceCodeK, defaultServiceCode);
            }
        }
    }

    private boolean isSrrAllowedBySla(Map<String, Map<String, Object>> requestContext) {
        return ((Map<String, Object>) requestContext.get(ncsK).get(mtK)).containsKey(deliveryReportUrlK)
                && null != ((Map<String, Object>) requestContext.get(ncsK).get(mtK)).get(deliveryReportUrlK);
    }

    private boolean isSrrRequested(Map<String, Map<String, Object>> requestContext) {
        return requestContext.get(requestK).containsKey(statusRequestK)
                && deliveryReportRequiredK.equals(requestContext.get(requestK).get(statusRequestK));
    }

    private void addHeaderIfAvailable(Map<String, Map<String, Object>> requestContext, Map<String, Object> sms) {
        if (requestContext.get(requestK).containsKey(binaryHeaderNblK)) {
            sms.put(binaryHeaderK, (String) requestContext.get(requestK).get(binaryHeaderNblK));
        }
    }

    private void addEncoding(Map<String, Map<String, Object>> requestContext, Map<String, Object> sms) {
        if (requestContext.get(requestK).containsKey(encodingK)) {
            sms.put(encodingK, (String) requestContext.get(requestK).get(encodingK));
        } else {
            sms.put(encodingK, encodingTextK);
        }
    }

    private String getSenderAddress(Map<String, Map<String, Object>> requestContext) {
        String senderAddress = (String) requestContext.get(requestK).get(senderAddressK);
        if (isNotEmpty(senderAddress)) {
            return senderAddress;
        } else {
            String defaultSenderAddress = (String) getDefaultSenderAddress(requestContext);
            if (isNotEmpty(defaultSenderAddress)) {
                logger.info("Sender address not provided, using default sender address[{}]", defaultSenderAddress);
                return defaultSenderAddress;
            }
        }

        logger.warn("Unable to send sms mt message. Sender address or default sender address not found");
        throw new SdpException(invalidSourceAddressErrorCode);
    }

    private boolean isNotEmpty(String str) {
        if (str != null && !str.trim().isEmpty()) {
            return true;
        } else {
            return false;
        }

    }

    private Object getDefaultSenderAddress(Map<String, Map<String, Object>> requestContext) {
        return (String) KeyNameSpaceResolver.data((Map) requestContext,
                KeyNameSpaceResolver.nameSpacedKey(ncsK, mtK, defaultSenderAddressK));
    }

    private void createRecipientAddressList(Map<String, Map<String, Object>> requestContext, Map<String, Object> sms)
            throws NoRecipientFoundException {
        List<Map<String, String>> recipients = (List<Map<String, String>>) requestContext.get(requestK)
                .get(recipientsK);
        StringBuilder sb = new StringBuilder();
        for (Map<String, String> recipient : recipients) {
            if (successCode.equals(recipient.get(recipientAddressStatusK))) {
                sb.append(",");
                sb.append(recipient.get(recipientAddressK));
            } else {
                logger.info("Recipient address [{}] is not in valid status so ignoring", recipient);
            }
        }
        String recepientAddressList = sb.toString();
        logger.debug("Created recepient address list[{}]", recepientAddressList);
        if (recepientAddressList.isEmpty()) {
            throw new NoRecipientFoundException();
        }

        if (recepientAddressList.startsWith(",")) {
            recepientAddressList = recepientAddressList.substring(1, recepientAddressList.length());
            logger.debug("Recepient address list after removing (,) [{}]", recepientAddressList);
        }
        sms.put(recipientAddressK, recepientAddressList);
    }

    private boolean isStatusSuccess(Response response) throws IOException {
        if (isSuccessHttpResponse(response)) {
            Map<String, Object> resp = readJsonResponse(response);
            if (resp != null && ACCEPTED.equals(resp.get(statusK))) {
                return true;
            }
        }
        return false;
    }

    private Map<String, Object> processResponse(Map<String, Map<String, Object>> requestContext, Map<String, Object> resp) {
        if (null == resp) {
            return generate(temporarySystemErrorCode, false, requestContext);
        } else if (!(successK).equals(resp.get(statusK))) {
            updateRecipientStatusesAsFailed(requestContext);
            return generate(temporarySystemErrorCode, false, requestContext);
        }

        if (resp.containsKey("unsuccessfull-destinations")) {
            List<Map<String, String>> failedAddresses = GsonUtil.getGson().fromJson(
                    (String) resp.get("unsuccessfull-destinations"),
                    new TypeToken<List<Map<String, String>>>() {
                    }.getType());
            List<Map<String, String>> recipients = (List<Map<String, String>>) requestContext.get(requestK).get(
                    recipientsK);

            if (failedAddresses != null) {
                for (Map<String, String> failedAddress : failedAddresses) {
                    for (Map<String, String> recipient : recipients) {
                        if (failedAddress.get(addressK).equals(recipient.get(recipientAddressK))) {
                            recipient.put(recipientAddressStatusK, failedAddress.get("errorCode"));
                        }
                    }
                }
            }
        }

        return generateSuccess();
    }

    private void updateRecipientStatusesAsFailed(Map<String, Map<String, Object>> requestContext) {
        for (Map<String, Object> recipient : (List<Map<String, Object>>) requestContext.get(requestK).get(recipientsK)) {
            if (!successCode.equals(recipient.get(recipientAddressStatusK))) {
                recipient.put(recipientAddressStatusK, temporarySystemErrorCode);
            }
        }
    }

    public void setCorrelator(MessageCorrelator correlator) {
        this.correlator = correlator;
    }

    public void setSnmpMessges(Properties snmpMessges) {
        this.snmpMessges = snmpMessges;
    }

    public void setDefaultServiceCode(String defaultServiceCode) {
        this.defaultServiceCode = defaultServiceCode;
    }

    public void setUseServiceCodeShortCodeConvertion(boolean useConvertion) {
        this.useServiceCodeShortCodeConvertion = useConvertion;
    }

    public void setAlwaysSendServiceCode(boolean alwaysSendServiceCode) {
        this.alwaysSendServiceCode = alwaysSendServiceCode;
    }
}
