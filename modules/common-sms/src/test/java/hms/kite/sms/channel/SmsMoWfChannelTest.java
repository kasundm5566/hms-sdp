/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sms.channel;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

/**
 * $LastChangedDate: 2011-06-10 06:46:30 +0530 (Fri, 10 Jun 2011) $
 * $LastChangedBy: romith $
 * $LastChangedRevision: 73975 $
 */
public class SmsMoWfChannelTest {

    @DataProvider(name = "message.list.for.keyword.extraction")
    public Object[][] possibleMessagesForKeywordExtraction() {
        return new Object[][]{
                {"test", "test"},
                {" test test", "test"},
                {" test", "test"},
                {" test\nmess", "test"},
                {" test\tmess", "test"},
                {"test|mess", "test"},
                {"|", ""},
                {"|test", "test"},
                {"test test2", "test"},
                {"keyword message", "keyword"},
                {"message keyword", "message"},
                {"BUS which bus to go to Colombo from Singapore", "bus"},
                {"BUS ", "bus"},
                {"BUS bus", "bus"},
                {"", ""},
                {null, ""},
                {" ", ""}
        };
    }

    @Test(dataProvider = "message.list.for.keyword.extraction")
    public void testKeywordExtraction(String message, String expectedKeyword) {
        String actualKeyword = SmsMoWfChannel.get1stKeyword(message);
        assertEquals("invalid keyword extraction", expectedKeyword, actualKeyword);
    }

}
