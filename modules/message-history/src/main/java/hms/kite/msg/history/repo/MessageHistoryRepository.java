/*
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.msg.history.repo;

import hms.common.rest.util.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import static hms.kite.util.KiteKeyBox.*;
/**
 * $LastChangedDate: 2011-07-20 14:01:33 +0530 (Wed, 20 Jul 2011) $
 * $LastChangedBy: ruwant $
 * $LastChangedRevision: 75106 $
 */
public class MessageHistoryRepository {

    private static Logger logger = LoggerFactory.getLogger(MessageHistoryRepository.class);

	private static final String APP_ID_COLUMN = "app_id";
	private static final String MESSAGE_COLUMN = "message";
	private static final String CORRELATION_ID_COLUMN = "correlation_id";
	private static final String RECEIVE_DATE_COLUMN = "receive_date";
	private static final String CREATE_MESSAGE_SQL = String.format("INSERT INTO message_history (%s, %s, %s, %s) VALUES (?, ?, ?, ?)", 
			new Object[]{APP_ID_COLUMN, MESSAGE_COLUMN, CORRELATION_ID_COLUMN, RECEIVE_DATE_COLUMN});
	private static final String FIND_MESSAGES_SQL = String.format("SELECT * FROM message_history order by %s desc limit ? offset ?",
			new Object[]{RECEIVE_DATE_COLUMN});
	private static final String FIND_MESSAGES_BY_APP_SQL = String.format("SELECT * FROM message_history WHERE %s = ? order by %s desc limit ?,?",
			new Object[]{APP_ID_COLUMN, RECEIVE_DATE_COLUMN});
	private static final String COUNT_MESSAGES_SQL = String.format("SELECT COUNT(*) FROM message_history WHERE %s = ?", 
			new Object[]{APP_ID_COLUMN});
	
	private JdbcTemplate jdbcTemplate;
	
	public void save(String appId, String message, String correlationId, long timeStamp) {
        logger.debug("Saving message");
		jdbcTemplate.update(CREATE_MESSAGE_SQL, new Object[]{appId, message, correlationId, new Timestamp(timeStamp)});
        logger.debug("Message saved successfully");
    }
	
	public List<Message> find(int limit, int offset) {
		return jdbcTemplate.query(FIND_MESSAGES_SQL, new Object[]{limit, offset}, 
				new RowMapper() {
			
					@Override
					public Message mapRow(ResultSet rs, int i) throws SQLException {
						return resultRow(rs);
					}
		});
	}
	
	public List<Message> findByAppId(String appId, int limit, int offset) {
		return jdbcTemplate.query(FIND_MESSAGES_BY_APP_SQL, new Object[]{appId, limit, offset}, 
				new RowMapper() {
			
					@Override
					public Message mapRow(ResultSet rs, int i) throws SQLException {
						return resultRow(rs);
					}
		});
	}
	
	public int count(String appId) {
		return jdbcTemplate.queryForInt(COUNT_MESSAGES_SQL, appId);
	}
	
	private Message resultRow(ResultSet rs) throws SQLException {
		Message result = new Message();
		result.put(appIdK, rs.getString(APP_ID_COLUMN));
		result.put(messageK, rs.getString(MESSAGE_COLUMN));
		result.put(correlationIdK, rs.getString(CORRELATION_ID_COLUMN));
		result.put(receiveTimeK, rs.getDate(RECEIVE_DATE_COLUMN).toString());
		return result;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
}
