/*
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.msg.history;

import static hms.kite.util.KiteKeyBox.appIdK;
import static hms.kite.util.KiteKeyBox.appK;
import static hms.kite.util.KiteKeyBox.messageK;
import static hms.kite.util.KiteKeyBox.receiveTimeK;
import static hms.kite.util.KiteKeyBox.requestK;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: 2011-07-20 14:01:33 +0530 (Wed, 20 Jul 2011) $
 * $LastChangedBy: danukas $ $LastChangedRevision: 75106 $
 */
public class MessageHistoryChannel extends BaseChannel {

	private MessageHistoryService messageHistoryService;
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageHistoryChannel.class);

	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
		LOGGER.debug("dumping message ...... ");
		long start = System.currentTimeMillis();
		try {
			try {
				messageHistoryService.dumpMessage((String) requestContext.get(appK).get(appIdK),
						(String) requestContext.get(requestK).get(messageK),
						(String) requestContext.get(requestK).get(receiveTimeK));
			} catch (Exception e) {
				LOGGER.error("Failed to persist message", e);
			}
			return ResponseBuilder.generateSuccess();
		} finally {
			LOGGER.debug("start[{}] time[{}] tag[MSG-HISTORY]", start, (System.currentTimeMillis() - start));
		}
	}

	public void setMessageHistoryService(MessageHistoryService messageHistoryService) {
		this.messageHistoryService = messageHistoryService;
	}

}
