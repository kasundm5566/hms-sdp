/*
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.msg.history;

import hms.kite.msg.history.repo.MessageHistoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * $LastChangedDate: 2011-07-20 14:01:33 +0530 (Wed, 20 Jul 2011) $
 * $LastChangedBy: ruwant $
 * $LastChangedRevision: 75106 $
 */
public class MessageHistoryService {

    private static Logger logger = LoggerFactory.getLogger(MessageHistoryService.class);

    private MessageHistoryRepository messageHistoryRepository;
    private ExecutorService executorService;
    private int threadPoolSize;

    public void init() {
        executorService = Executors.newFixedThreadPool(threadPoolSize);
    }

    public void dumpMessage(final String appId, final String message, final String receivedTime) {

        executorService.execute(new Runnable() {

            @Override
            public void run() {
                try {
                    messageHistoryRepository.save(appId, message, receivedTime, Long.parseLong(receivedTime));
                } catch (Exception e) {
                    logger.error("Unexpected error occurred while dumping messages to repository", e);
                }
            }
        });

    }

    public void destroy() {
        executorService.shutdown();
    }

    public MessageHistoryRepository getMessageHistoryRepository() {
        return messageHistoryRepository;
    }

    public void setMessageHistoryRepository(MessageHistoryRepository messageHistoryRepository) {
        this.messageHistoryRepository = messageHistoryRepository;
    }

    public int getThreadPoolSize() {
        return threadPoolSize;
    }

    public void setThreadPoolSize(int threadPoolSize) {
        this.threadPoolSize = threadPoolSize;
    }

    public void setExecutorService(ExecutorService executorService) {
        this.executorService = executorService;
    }

}
