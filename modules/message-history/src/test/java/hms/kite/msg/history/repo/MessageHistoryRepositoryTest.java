/*
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.msg.history.repo;

import hms.common.rest.util.Message;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static hms.kite.util.KiteKeyBox.appIdK;
import static org.testng.Assert.assertEquals;
/**
 * $LastChangedDate: 2011-07-20 14:01:33 +0530 (Wed, 20 Jul 2011) $
 * $LastChangedBy: ruwant $
 * $LastChangedRevision: 75106 $
 */

public class MessageHistoryRepositoryTest {

	private MessageHistoryRepository messageHistoryRepository;
	private JdbcTemplate jdbcTemplate;
	
	private static final String APP_ID_COLUMN = "app_id";
	private static final String MESSAGE_COLUMN = "message";
	private static final String CORRELATION_ID_COLUMN = "correlation_id";
	private static final String RECEIVE_DATE_COLUMN = "receive_date";
	
	@BeforeTest
	public void setUp() {
		ApplicationContext ac = new ClassPathXmlApplicationContext("beans-test.xml");
		messageHistoryRepository = (MessageHistoryRepository) ac.getBean("messageHistoryRepository");
		jdbcTemplate = (JdbcTemplate) ac.getBean("jdbcTemplate");
	}
	
	@Test(priority = 1, enabled = true)
	public void testSave() {
		messageHistoryRepository.save("app", "message .... ", "2341", 9838968234l);
		jdbcTemplate.queryForObject("select * from message_history", new RowMapper() {
			
			@Override
			public Object mapRow(ResultSet rs, int arg1) throws SQLException {
				assertEquals(rs.getString(APP_ID_COLUMN), "app");
				assertEquals(rs.getString(MESSAGE_COLUMN), "message .... ");
				assertEquals(rs.getString(CORRELATION_ID_COLUMN), "2341");
				return null;
			}
		});
	}
	
	@Test(priority = 2, enabled = true)
	public void testFind() {
		createTestData();
		List<Message> list = messageHistoryRepository.find(10, 0);
		assertEquals(list.size(), 7);
	}
	
	@Test(priority = 2, enabled = true)
	public void testFindByAppId() {
		List<Message> list = messageHistoryRepository.findByAppId("app 1", 10, 0);
		assertEquals(list.size(), 5);
		for(Message msg : list) {
			assertEquals(msg.get(appIdK), "app 1");
		}
		
		list = messageHistoryRepository.findByAppId("app 1", 4, 0);
		assertEquals(list.size(), 4);
		
		list = messageHistoryRepository.findByAppId("app 3", 4, 0);
		assertEquals(list.size(), 0);
	}
	
	private void createTestData() {
		jdbcTemplate.execute("delete from message_history");
		java.sql.Timestamp ts = new java.sql.Timestamp(22898892384l);
		jdbcTemplate.execute("insert into message_history("+APP_ID_COLUMN+", "+MESSAGE_COLUMN+", "+CORRELATION_ID_COLUMN+", "+RECEIVE_DATE_COLUMN+") values('app 1', 'xxxxxxx', '233523', '"+ts+"')");
		jdbcTemplate.execute("insert into message_history("+APP_ID_COLUMN+", "+MESSAGE_COLUMN+", "+CORRELATION_ID_COLUMN+", "+RECEIVE_DATE_COLUMN+") values('app 1', 'yyyyyyy', '233525', '"+ts+"')");
		jdbcTemplate.execute("insert into message_history("+APP_ID_COLUMN+", "+MESSAGE_COLUMN+", "+CORRELATION_ID_COLUMN+", "+RECEIVE_DATE_COLUMN+") values('app 2', 'zzzzzzz', '233527', '"+ts+"')");
		jdbcTemplate.execute("insert into message_history("+APP_ID_COLUMN+", "+MESSAGE_COLUMN+", "+CORRELATION_ID_COLUMN+", "+RECEIVE_DATE_COLUMN+") values('app 1', 'xxxxxxx', '233523', '"+ts+"')");
		jdbcTemplate.execute("insert into message_history("+APP_ID_COLUMN+", "+MESSAGE_COLUMN+", "+CORRELATION_ID_COLUMN+", "+RECEIVE_DATE_COLUMN+") values('app 1', 'yyyyyyy', '233525', '"+ts+"')");
		jdbcTemplate.execute("insert into message_history("+APP_ID_COLUMN+", "+MESSAGE_COLUMN+", "+CORRELATION_ID_COLUMN+", "+RECEIVE_DATE_COLUMN+") values('app 2', 'zzzzzzz', '233527', '"+ts+"')");
		jdbcTemplate.execute("insert into message_history("+APP_ID_COLUMN+", "+MESSAGE_COLUMN+", "+CORRELATION_ID_COLUMN+", "+RECEIVE_DATE_COLUMN+") values('app 1', 'xxxxxxx', '233523', '"+ts+"')");
	}
	
	@AfterTest
	public void tearDown() {
		jdbcTemplate.execute("delete from message_history");
	}
}
