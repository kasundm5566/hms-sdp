(ns hms.kite.test.charging
  (:use [clojure.test]
    [hms.common.clj-coll-util :only [to-clj-map]])
  (:import [java.util HashMap ArrayList]))

(defonce charging-service (hms.kite.Charging.))

(def app
  (doto (HashMap.)
    (.put "name" "PackMan")))

(def mo-recipient
  (doto (HashMap.)
    (.put "recipient-address" "13355544")
    (.put "recipient-address-status" "S1000")))

;============================= MO Request ===============================

(def mo-charging-sla
  (doto (HashMap.)
    (.put "party" "subscriber")
    (.put "type" "free")
    (.put "service-code" "456")))

(def mo-ncs
  (doto (HashMap.)
    (.put "app-id" "APP_00021")
    (.put "mo" (doto (HashMap.)
                  (.put "charging" mo-charging-sla)))))

(def mo-request
  (doto (HashMap.)
    (.put "request-type" "reserve-credit")
    (.put "direction" "mo")
    (.put "ncs-type"  "sms")
    (.put "correlation-id" "23543434")
    (.put "charging-trx-id" "2323")
    (.put "sender-address" "343535")
    (.put "sender-address-status" "S1000")
    (.put "recipients" (doto (ArrayList.) (.add mo-recipient)))))

(def mo-charging-request
  (doto (HashMap.)
    (.put "app" app)
    (.put "ncs" mo-ncs)
    (.put "request" mo-request)))

;================== MT Request ========================
(def mt-recipient
  (doto (HashMap.)
    (.put "recipient-address" "13355544")
    (.put "recipient-address-status" "S1000")
    (.put "charging-trx-id" "2323")))

(def mt-charging-sla
  (doto (HashMap.)
    (.put "party" "sp")
    (.put "type" "free")
    (.put "service-code" "456")))

(def mt-ncs
  (doto (HashMap.)
    (.put "app-id" "APP_00021")
    (.put "mt" (doto (HashMap.)
                  (.put "charging" mt-charging-sla)))))

(def mt-request
  (doto (HashMap.)
    (.put "request-type" "reserve-credit")
    (.put "direction" "mt")
    (.put "ncs-type"  "sms")
    (.put "correlation-id" "23543434")
    (.put "sender-address" "343535")
    (.put "sender-address-status" "S1000")
    (.put "recipients" (doto (ArrayList.) (.add mt-recipient)))))

(def mt-charging-request
  (doto (HashMap.)
    (.put "app" app)
    (.put "ncs" mt-ncs)
    (.put "request" mt-request)))


(deftest process-mo-req
  (let [response (.process charging-service mo-charging-request)]
    (is (= {"status-code" "S1000" "status-description" "Success"} response))))

(deftest process-mt-req
  (let [response (.process charging-service mt-charging-request)]
    (is (= {"status-code" "S1000" "status-description" "Success"} response))))

(deftest process-nil-req
  (let [response (.process charging-service nil)]
    (is (= "E1312" (get response "status-code")))))

(deftest process-empty-req
  (let [request (to-clj-map (java.util.HashMap.))
        response (.process charging-service request)]
    (is (= "E1312" (get response "status-code")))))
