(ns hms.kite.test.charging-util
  (:use [clojure.test]
    [hms.kite.charging]))

(def sample-req1
  {:app     {:name   "PackMan"}
   :ncs     {:app-id "APP_00021"
             :mo {:charging {:party        "subscriber"
                             :method       "default-payment-instrument"
                             :type         "flat"
                             :amount       "1"
                             :service-code "456"}}}
   :request {:request-type          "reserve-credit"
             :direction             "mo"
             :ncs-type              "sms"
             :correlation-id        "23543434"
             :charging-trx-id       "2323"
             :sender-address        "343535"
             :sender-address-status "S1000"
             :recipients            [{:recipient-address        "13355544"
                                      :recipient-address-status "S1000"}]}})

(def sample-req2 
  {:sp      {:coop-user-id "636636"}
   :app     {:name   "PackMan"}
   :ncs     {:app-id "APP_00021"
             :mt {:charging {:party        "sp"
                             :type         "flat"
                             :amount       "1"
                             :service-code "456"
                             :payment-instrument-name "ABC Bank"
                             :payment-account "11111"}}}
   :request {:request-type          "reserve-credit"
             :direction             "mt"
             :ncs-type              "sms"
             :correlation-id        "23543434"
             :sender-address        "343535"
             :sender-address-status "S1000"
             :recipients            [{:recipient-address        "13355544"
                                      :recipient-address-status "S1000"
                                      :charging-trx-id          "2323"}
                                     {:recipient-address        "13355545"
                                      :recipient-address-status "E1321"
                                      :charging-trx-id          "2324"}
                                     {:recipient-address        "13355546"
                                      :recipient-address-status "E1321"
                                      :charging-trx-id          "2325"}]}})

(def processed-req2
  {:sp      {:coop-user-id "636636"}
   :app     {:name   "PackMan"}
   :ncs     {:app-id "APP_00021"
             :mt {:charging {:party        "sp"
                             :type         "flat"
                             :amount       "1"
                             :service-code "456"
                             :payment-instrument-name "ABC Bank"
                             :payment-account "11111"}}}
   :request {:request-type          "reserve-credit"
             :direction             "mt"
             :ncs-type              "sms"
             :correlation-id        "23543434"
             :sender-address        "343535"
             :sender-address-status "S1000"
             :recipients            [{:currency-code            "KES"
                                      :recipient-address        "13355544"
                                      :recipient-address-status "S1000"
                                      :charging-trx-id          "2323"}
                                     {:recipient-address        "13355545"
                                      :recipient-address-status "E1321"
                                      :charging-trx-id          "2324"}
                                     {:recipient-address        "13355546"
                                      :recipient-address-status "E1321"
                                      :charging-trx-id          "2325"}]}})

(deftest charging-param-by-name-test
  (is (= "flat" (charging-param-by-name sample-req1 :type))))

(deftest filter-recipients-test
  (is (= sample-req1 (filter-request-by-status #{"S1000"} sample-req1))))

(deftest merge-charging-resp-with-request-fn-test
  (is (= processed-req2 (merge-charging-resp-with-request-fn #{"S1000"} sample-req2
    [{:recipient-address            "13355544"
      :recipient-address-status     "S1000"
      :charging-trx-id              "2323"}]))))

(deftest charging-amounts-test
  (is (= '({:amount {:amount-value  "1"
                     :currency-code "KES"}})
          (mk-charging-amounts sample-req1))))

(deftest update-service-code-test
  (is (=
    {:ncs     {:mt {:charging {:method       "operator-charging"
                               :service-code "456"}}}
     :request {:request-type    "reserve-credit"
               :direction       "mt"
               :service-code    "456"}}

    (update-service-code
     {:ncs     {:mt {:charging {
                                :method       "operator-charging"
                                :service-code "456"}}}
      :request {:request-type "reserve-credit"
                :direction    "mt"}}))))

(deftest mk-payer-details-test
  (is
    (=
      '({:payer-details {:msisdn "343535"}})
      (mk-payer-details sample-req1))))
