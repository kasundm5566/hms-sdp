(ns hms.kite.test.charging-validation
  (:use [clojure.test]
    [hms.kite.charging]))

(def sample-mo-flat-req
  {:app     {:name   "PackMan"}
   :ncs     {:app-id "APP_00021"
             :mo {:charging {:party        "subscriber"
                             :method       "default-payment-instrument"
                             :type         "flat"
                             :amount       "1"
                             :service-code "456"}}}
   :request {:request-type          "reserve-credit"
             :direction             "mo"
             :ncs-type              "sms"
             :correlation-id        "23543434"
             :charging-trx-id       "2323"
             :sender-address        "343535"
             :sender-address-status "S1000"
             :recipients            [{:recipient-address        "13355544"
                                      :recipient-address-status "S1000"}]}})

(def sample-mt-flat-req
  {:app     {:name   "PackMan"}
   :ncs     {:app-id "APP_00021"
             :mt {:charging {:party        "sp"
                             :type         "flat"
                             :amount       "1"
                             :service-code "456"}}}
   :request {:request-type          "reserve-credit"
             :direction             "mt"
             :ncs-type              "sms"
             :correlation-id        "23543434"
             :sender-address        "343535"
             :sender-address-status "S1000"
             :recipients            [{:recipient-address        "13355544"
                                      :recipient-address-status "S1000"
                                      :charging-trx-id          "2323"}
                                     {:recipient-address        "13355545"
                                      :recipient-address-status "ERROR_9003"
                                      :charging-trx-id          "2324"}
                                     {:recipient-address        "13355546"
                                      :recipient-address-status "ERROR_9003"
                                      :charging-trx-id          "2325"}]}})



(deftest validate-mo-flat-charging-req
  (is (= sample-mo-flat-req (validate-charging-request sample-mo-flat-req))))

(deftest validate-mt-flat-charging-req
  (is (= sample-mt-flat-req (validate-charging-request sample-mt-flat-req))))

(def sample-sp-charging-req
  {:app     {:name   "PackMan"}
   :ncs     {:app-id "APP_00021"
             :mt {:charging {:party        "sp"
                             :type         "flat"
                             :amount       "1"
                             :service-code "456"}}}
   :request {:request-type          "reserve-credit"
             :direction             "mt"
             :ncs-type              "sms"
             :correlation-id        "23543434"
             :sender-address        "343535"
             :sender-address-status "S1000"
             :recipients            [{:recipient-address        "13355544"
                                      :recipient-address-status "S1000"
                                      :charging-trx-id          "2323"}]}})


(deftest charging-process-dispatcher-fn-test1
  (is (= ["reserve-credit" "subscriber" "default-payment-instrument"]
        (request-type-party&method sample-mo-flat-req))))


(deftest charging-process-dispatcher-fn-test2
  (is (= ["reserve-credit" "subscriber" "default-payment-instrument"]
        (request-type-party&method sample-mt-flat-req))))

(deftest charging-process-dispatcher-fn-test3
  (is (= ["reserve-credit" "sp" "N/A"]
        (request-type-party&method sample-sp-charging-req))))

(def variable-charging-req1
  {:app     {:name   "PackMan"}
   :ncs     {:app-id "APP_00021"
             :mo {:charging {:party        "subscriber"
                             :method       "default-payment-instrument"
                             :type         "variable"
                             :min-amount   "1"
                             :max-amount   "2"
                             :service-code "456"}}}
   :request {:request-type          "reserve-credit"
             :direction             "mo"
             :ncs-type              "sms"
             :correlation-id        "23543434"
             :sender-address        "343535"
             :sender-address-status "S1000"
             :charging-trx-id       "2323"
             :charging-amount       "1"
             :recipients            [{:recipient-address        "13355544"
                                      :recipient-address-status "S1000"}]}})


(deftest charging-process-dispatcher-fn-test2
  (is (= ["reserve-credit" "subscriber" "default-payment-instrument"]
        (request-type-party&method variable-charging-req1))))

(deftest validate-variable-charging-amount-test
  (is (= true (validate-variable-charging-amount variable-charging-req1))))


(deftest validate-variable-charging-amount-test
  (is (= false (validate-variable-charging-amount (assoc-in variable-charging-req1 [:request :charging-amount] "10")))))

(deftest validate-variable-charging-amount-test2
  (is (= false (validate-variable-charging-amount (assoc-in variable-charging-req1 [:request :charging-amount] nil)))))