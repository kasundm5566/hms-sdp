package hms.kite.charging;

import hms.kite.datarepo.SystemConfiguration;

/**
 * Created by IntelliJ IDEA.
 * User: isuruanu
 * Date: 5/17/12
 * Time: 6:14 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ChargingConfiguration {
    
    public void setSystemCurrency(String string);

    public void setSystemConfiguration(SystemConfiguration systemConfiguration);
}
