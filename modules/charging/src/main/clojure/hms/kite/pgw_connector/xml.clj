(ns hms.kite.pgw-connector.xml
  (:use
    [clojure.tools.logging]
    ))

(defn- mk-common-charging-req-attrs [{:keys [client-trans-id merchant-id merchant-name system-id charged-category requested-app-id] :as req-map} & extra-keys]
  {:attrs
   (merge {:client-trans-id            client-trans-id
           :merchant-id                merchant-id
           :merchant-name              merchant-name
           :system-id                  system-id
           :charged-category           charged-category
           :requested-app-id           requested-app-id}
     (select-keys req-map (vec extra-keys)))})

(defn- mk-commit&rollback-attribs [{:keys [reservation-id] :as req-map} & extra-keys]
  {:attrs
   (merge
     {:reservation-id   reservation-id}
     (select-keys req-map (vec extra-keys)))})


(defn- mk-common-query-req-attrs [req-map & extra-keys]
  {:attrs
   (select-keys req-map (vec extra-keys))})

(defn- mk-allow-merchant-attrs [{:keys [client-trans-id merchant-id merchant-name] :as req-map} & extra-keys]
  {:attrs
   (merge
     {:client-trans-id   client-trans-id
      :merchant-id       merchant-id
      :merchant-name     merchant-name}
     (select-keys req-map (vec extra-keys)))})

(def empty-content {:content nil})

(defn- mk-tag-with-attrs [tag attrs]
	{:tag tag :attrs attrs})

(defn- mk-tag-with-content [aks aval]
  {:tag :entry :content [{:tag :key
                          :attrs {:xmlns:xsi "http://www.w3.org/2001/XMLSchema-instance", :xmlns:xs "http://www.w3.org/2001/XMLSchema" :xsi:type "xs:string"}
                          :content [ (if (nil? aks) "" (str (name aks)))]} {:tag :value :attrs {:xmlns:xsi "http://www.w3.org/2001/XMLSchema-instance", :xmlns:xs "http://www.w3.org/2001/XMLSchema" :xsi:type "xs:string"} :content [(if (nil? aval) "" (str (name aval)))]}]})

(defn- add-content[element]
  (let [ele-keys (vec (keys element))]
  (vec (map #(mk-tag-with-content % (get element %)) ele-keys))))

(defn- mk-tag-with-sub-elements[req-map ks]
  {:tag ks :content (add-content (ks req-map))})

(defn- mk-content [req-map & ks]
  {:content (vec (map #(mk-tag-with-attrs % (% req-map)) ks))})


(defn- add-content-to-dd-request[req-map & ks]
  {:content (vec (conj (vec (map #(mk-tag-with-attrs % (% req-map)) ks)) (mk-tag-with-sub-elements req-map :additionalParams)))})

(defn mk-credit-reserve [req-map]
  (-> {:tag :credit-reserve-request}
    (merge (mk-common-charging-req-attrs req-map :requested-timestamp :is-auth-reqd :merchant-should-be-allowed))
    (merge (mk-content req-map :payer-details :amount))))

(defn mk-credit-commit [req-map]
  (-> {:tag :credit-commit-request}
    (merge (mk-commit&rollback-attribs req-map :client-trans-id))
    (merge empty-content)))

(defn mk-credit-cancel [req-map]
  (-> {:tag :credit-cancel-request}
    (merge (mk-commit&rollback-attribs req-map :client-trans-id))
    (merge empty-content)))

(defn mk-bulk-req [req-keyword f requests]
  {:tag req-keyword
   :content [{:tag     :requests-list
              :content (vec (map f requests))}]})

(defn mk-bulk-credit-reserve [requests]
  (mk-bulk-req :bulk-credit-reserve-request mk-credit-reserve requests))

(defn mk-bulk-credit-commit [requests]
  (mk-bulk-req :bulk-credit-commit-request mk-credit-commit requests))

(defn mk-bulk-credit-cancel [requests]
  (mk-bulk-req :bulk-credit-cancel-request mk-credit-cancel requests))

(defn mk-request-credit [req-map]
  (-> {:tag :credit-request}
    (merge (mk-common-charging-req-attrs req-map :requested-timestamp :is-auth-reqd :merchant-should-be-allowed))
    (merge (mk-content req-map :payee-details :amount))))

(defn mk-request-debit [req-map]
  (debug (str "Request map given to create DD[" req-map "]"))
  (-> {:tag :directdebit-request}
    (merge (mk-common-charging-req-attrs req-map
             :requested-timestamp :is-auth-reqd :merchant-should-be-allowed :is-over-payment-allowed :is-partial-payment-allowed :timeout-period))
    (merge (add-content-to-dd-request req-map :payer-details :amount))))

(defn mk-request-transfer [req-map]
  (-> {:tag :transfer-request}
    (merge (mk-common-charging-req-attrs req-map :requested-timestamp :is-auth-reqd :merchant-should-be-allowed))
    (merge (mk-content req-map :payer-details :payee-details :amount))))

(defn mk-payment-ins-query [req-map]
  (-> {:tag :payins-query}
    (merge (mk-common-query-req-attrs req-map :user-id :msisdn :is-auth-reqd :merchant-should-be-allowed))
    (merge empty-content)))


(defn mk-allow-merchant [req-map]
  (-> {:tag :allow-request}
    (merge (mk-allow-merchant-attrs req-map :msisdn :userId))
    (merge empty-content)))

(defn mk-transaction-status-query [req-map]
  (-> {:tag :trx-query}
    (merge (mk-common-query-req-attrs req-map :transaction-id :requested-timestamp))
    (merge empty-content)))

(defn mk-request-query-balance [req-map]
  (-> {:tag :querybalance-request}
  (merge (mk-common-query-req-attrs req-map :requested-timestamp :is-auth-reqd :merchant-should-be-allowed ))
  (merge (mk-content req-map :payer-details))))
