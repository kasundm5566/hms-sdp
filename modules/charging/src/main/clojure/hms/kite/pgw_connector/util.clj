(ns hms.kite.pgw-connector.util
  (:use [clojure.xml :only [parse]]
    [hms.kite.pgw-connector.failure-monads]
    [clojure.tools.logging])
  (:import [java.io StringBufferInputStream]))

(def not-nil?
  (comp not nil?))

(defn emit-map-element [e]
  (if (instance? String e)
    (print e)
    (do
      (print (str "<" (name (:tag e))))
      (when (:attrs e)
        (doseq [attr (:attrs e)]
          (print (str " " (name (key attr)) "='" (val attr)"'"))))
      (if (:content e)
        (do
          (print ">")
          (doseq [c (:content e)]
            (emit-map-element c))
          (print (str "</" (name (:tag e)) ">")))
        (print "/>")))))

(defn emit-map [x]
  (println "<?xml version='1.0' encoding='UTF-8'?>")
  (emit-map-element x))

(defn to-xml-str [req-map]
  (with-out-str (emit-map req-map)))


(defn parse-bulk-resp [{content :content}]
  (let [[overall-status individual-response] content]
    {:overall-status (first (:content overall-status))
     :individual-response (vec (map #(:attrs %) (:content individual-response)))}))

(defn parse-payins-resp [{attrs :attrs
                          content :content}]
  (merge
      attrs
      {:payins-response (vec (map #(:attrs %) content))}))

(defn parse-query-balance-resp [{attrs :attrs content :content}]
  (merge attrs {:balance (into {} (map #(:attrs %) content))}))

(defn parse-resp [{body :body}]
  (try
    (if-let [pgw-resp (-> body StringBufferInputStream. parse)]
      (cond
        (= :response (:tag pgw-resp)) (:attrs pgw-resp)
        (= :bulk-pgw-response (:tag pgw-resp)) (parse-bulk-resp pgw-resp)
        (= :payins-response (:tag pgw-resp)) (parse-payins-resp pgw-resp)
        (= :querybalance-response (:tag pgw-resp)) (parse-query-balance-resp pgw-resp)
        :else (failure "E1308" (str "Payment Gateway Error, unsupported  response format [" body "]")))
      (failure "E1308" "Payment Gateway Error, empty response received"))
    (catch Throwable e
      e)))

(defn log-event
  [excep msg]
  "Log the event log messages to event logger"
  (log "snmp" :error nil msg))