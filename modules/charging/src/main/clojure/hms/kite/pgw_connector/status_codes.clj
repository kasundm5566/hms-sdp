  (ns hms.kite.pgw-connector.status-codes)

(defonce status-code-mapping {"SUCCESS"                           "S1000"
                              "RESERVE_SUCCESS"                   "S1000"
                              "RESERVE_COMMIT_SUCCESS"            "S1000"
                              "RESERVE_CANCEL_SUCCESS"            "S1000"
                              "INSUFFICIENT_CREDIT"               "E1326"
                              "PENDING_AUTH"                      "P1003"
                              "INVALID_REQUEST"                   "E1605"
                              "INVALID_AMOUNT"                    "E1606"
                              "CONNECTION_NOT_FOUND_TO_IN"        "E1603"
                              "INTERNAL_ERROR"                    "E1601"
                              "EXTERNAL_ERROR"                    "E1601"
                              "SUBSCRIBER_AUTHENTICATION_FAILED"  "E1337"})

(defn get-mapped-code [pgw-status-code]
  (get status-code-mapping pgw-status-code "E1308"))