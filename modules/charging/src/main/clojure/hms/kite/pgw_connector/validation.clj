(ns hms.kite.pgw-connector.validation
  (:use [hms.kite.pgw-connector.schema]
    [closchema.core :only [validate report-errors]]
    [hms.kite.pgw-connector.failure-monads :only [failure]]))

(defn- find-error-msg [{ref  :ref
                        key  :key
                        data :data}]
  (if ref
    (str "Parameter [" (name ref) "] " (name key))
    (str "Invalid parameter value [" (:value data) "] expected [" (apply str (interpose " " (:enum data))) "]")))

(defn- format-validation-errors [errors]
  (apply str (interpose "," (map find-error-msg errors))))

(defn validator-fn [schema req]
  (let [validation-results (report-errors (validate schema req))]
    (if (empty? validation-results)
      req
      (failure "E1312" (str "Invalid charging request : " (format-validation-errors validation-results))))))

(defmulti validate-request-params :req-path)

(defmethod validate-request-params "/reserveCredit" [{:keys [req]}]
  (validator-fn credit-reserve-schema req))

(defmethod validate-request-params "/commitCredit" [{:keys [req]}]
  (validator-fn credit-commit-schema req))


(defmethod validate-request-params "/cancelCredit" [{:keys [req]}]
  (validator-fn credit-cancel-schema req))

(defmethod validate-request-params "/credit" [req]
  (validator-fn credit-schema req))

(defmethod validate-request-params "/directDebit" [{:keys [req]}]
  (validator-fn direct-debit-schema req))

(defmethod validate-request-params "/transfer" [{:keys [req]}]
  (validator-fn transfer-schema req))

(defmethod validate-request-params "/allowMerchantQuery" [{:keys [req]}]
  (validator-fn allow-merchant-schema req))

(defmethod validate-request-params "/payInstrumentQuery" [{:keys [req]}]
  (validator-fn pay-ins-query-schema req))

(defmethod validate-request-params "/transactionQuery" [{:keys [req]}]
  (validator-fn transaction-query-schema req))

(defmethod validate-request-params "/queryBalance" [{:keys [req]}]
  (validator-fn query-balance-schema req))

(defmethod validate-request-params :default [{:keys [req]}]
  (failure "E1312" (str "Unkown payment gateway request [" (:req-path req) "]")))

