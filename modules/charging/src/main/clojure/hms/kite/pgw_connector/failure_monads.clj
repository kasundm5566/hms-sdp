(ns hms.kite.pgw-connector.failure-monads
  (:use [clojure.algo.monads :only [defmonad domonad with-monad m-chain]]))

(defrecord Failure [context])

(defn fail [context] (Failure. context))

(defn error-resp [code discription]
  {:status-code code
   :status-description discription})

(def failure
  (comp fail error-resp))

(defn success-resp []
  {:status-code "S1000"
   :status-description "Success"})

(defprotocol FailureLike
  (failed? [self])
  (value [self]))

(extend-protocol FailureLike
  Object
  (failed? [self] false)
  (value [self] self)
  Failure
  (failed? [self] true)
  (value [self]
    (:context self))
  Exception
  (failed? [self] true)
  (value [self]
    (error-resp "E1308" (.getMessage self)))
  nil
  (failed? [self] true)
  (value [self]
    (error-resp "E1312" "Null request is not allowed")))

(defmonad failure-m
  [m-result identity
   m-bind (fn [m f]
    (if (failed? m)
      m
      (f m)))])
