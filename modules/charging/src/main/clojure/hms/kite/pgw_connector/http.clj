(ns hms.kite.pgw-connector.http
  (:use [hms.kite.pgw-connector.failure-monads]
    [hms.kite.pgw-connector.util]
    [clojure.tools.logging :only [error]]
    [clojure.algo.monads :only [domonad with-monad m-chain]]
    [clj-http.client :only [with-connection-pool]])
  (:require [clj-http.client :as client])
  (:import [java.net URI])
  (:import [org.apache.http HttpHost])
  (:import [org.apache.http.params BasicHttpParams HttpConnectionParams])
  (:import [org.apache.http.conn.routing HttpRoute])
  (:import [org.apache.http.conn.params ConnManagerParams ConnPerRouteBean])
  (:import [org.apache.http.conn.scheme Scheme SchemeRegistry PlainSocketFactory])
  (:import [org.apache.http.impl DefaultConnectionReuseStrategy])
  (:import [org.apache.http.impl.client DefaultConnectionKeepAliveStrategy])
  (:import [org.apache.http.impl.client DefaultHttpRequestRetryHandler])
  (:import [org.apache.http.impl.conn.tsccm ThreadSafeClientConnManager])
  (:import [hms.commons SnmpLogUtil]))

(defonce pgw-connector-properties
  (into {} (doto (java.util.Properties.)
    (.load (-> (Thread/currentThread)
      (.getContextClassLoader)
      (.getResourceAsStream "charging-connector.properties"))))))

(defonce http-connection-timeout
  (-> (pgw-connector-properties "http.connection.timeout") read-string))

(defonce http-so-timeout
  (-> (pgw-connector-properties "http.connection.so.timeout") read-string))

(defonce http-pool-max-size
  (-> (pgw-connector-properties "http.connection.pool.max.size") read-string))

(defonce http-pool-min-size
  (-> (pgw-connector-properties "http.connection.pool.min.size") read-string))

(defonce http-pool-acquire-timeout
  (-> (pgw-connector-properties "http.connection.pool.checkout.timeout") read-string))

(defonce pgw-api-url
  (pgw-connector-properties "pgw.api.url"))

(defonce scheme-registry-details
  (let [pgw-uri (URI/create pgw-api-url)]
    {:host   (.getHost pgw-uri)
     :port   (.getPort pgw-uri)
     :scheme (.getScheme pgw-uri)}))

(defn- schme-registry-extractor []
  (-> scheme-registry-details (select-keys [:scheme :port :host]) vals))

(defn- mk-http-route []
  (let [[host port] (schme-registry-extractor)]
    (-> (HttpHost. host port) (HttpRoute. ))))

(defonce conn-params
  (let [http-params (BasicHttpParams.)]
    (do
      (HttpConnectionParams/setConnectionTimeout http-params http-connection-timeout)
      (HttpConnectionParams/setSoTimeout http-params http-so-timeout)
      (ConnManagerParams/setTimeout http-params http-pool-acquire-timeout)
        (ConnManagerParams/setMaxTotalConnections http-params http-pool-max-size)
      (ConnManagerParams/setMaxConnectionsPerRoute http-params
        (doto (ConnPerRouteBean. http-pool-min-size)
          (.setMaxForRoute (mk-http-route) http-pool-max-size)))
      http-params)))

(defn- mk-scheme-registry []
  (let [[_ port scheme] (schme-registry-extractor)
         sc-factory (PlainSocketFactory/getSocketFactory)]
    (doto (SchemeRegistry.)
      (.register (Scheme. scheme sc-factory port)))))

(defonce scheme-registry
  (mk-scheme-registry))

(defonce conn-mgr (ThreadSafeClientConnManager. conn-params scheme-registry))

(defonce pgw-http-pool-ctx
  {:manager             conn-mgr
   :params              conn-params
   :connection-header   "keep-alive"
   :reuse-strategy      (DefaultConnectionReuseStrategy.)
   :keep-alive-strategy (DefaultConnectionKeepAliveStrategy.)
   :retry-handler       (DefaultHttpRequestRetryHandler. 0 false)})

(defn mk-post-fn [req-path]
  (fn [body]
    (with-connection-pool pgw-http-pool-ctx
      (try
      (let [resp (client/request
        {:method :post
         :url (str pgw-api-url req-path)
         :content-type "application/xml"
         :accept "application/xml"
         :body body
         :throw-exceptions true})]
        ;(SnmpLogUtil/clearTrap "sdpToPgwConnection" (pgw-connector-properties "sdp.to.pgw.connection.success.snmp.message") (into-array []))
        resp
        )
      (catch Throwable e
        (log-event e (str "Error occurred while connecting to " pgw-api-url))
        ;(SnmpLogUtil/timeTrap "sdpToPgwConnection" (Integer/parseInt (pgw-connector-properties "sdp.to.pgw.connection.trap.connection.in.seconds")) (pgw-connector-properties "sdp.to.pgw.connection.failure.snmp.message") (into-array []))
        (Exception. (str "PGW Connection Error [" (.getMessage e) "]"))
        )))))

(defn- validate-response [resp]
  (if (= 200 (:status resp))
    resp
    (failure "E1308" (str "Payment Gateway Error , Http Status [" (:status resp) "]"))))

(defn send-xml-req [req-path req-as-xml]
  (with-monad failure-m
    ((m-chain
      [(mk-post-fn req-path) validate-response parse-resp])
      req-as-xml)))