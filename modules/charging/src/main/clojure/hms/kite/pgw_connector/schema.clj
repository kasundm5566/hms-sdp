(ns hms.kite.pgw-connector.schema)

; =========== Charging Request Schema ===============

(defonce features-properties
  (into {} (doto (java.util.Properties.)
             (.load (-> (Thread/currentThread)
                      (.getContextClassLoader)
                      (.getResourceAsStream "charging-connector-feature.properties"))))))

(defonce currency-code-optional
  (Boolean/parseBoolean (features-properties "is.currency.code.optional")))

(defonce common-charging-req
  {:client-trans-id            {:type "string"}
   :merchant-id                {:type "string"}
   :merchant-name              {:type "string"}
   :system-id                  {:type "string"}
   :requested-timestamp        {:type     "string"
                                :optional true}
   :is-auth-reqd               {:type     "string"
                                :enum     ["true" "false"]
                                :optional true}
   :merchant-should-be-allowed {:type     "string"
                                :enum     ["true" "false"]
                                :optional true}
   :charged-category           {:type "string"
                                :enum ["sms" "wap-push" "cas" "ussd"]}
   :is-over-payment-allowed    {:type "string"
                                :enum ["true" "false"]
                                :optional true}
   :is-partial-payment-allowed {:type "string"
                                :enum ["true" "false"]
                                :optional true}
   :timeout-period             {:type "string"
                                :optional true}})

(defonce payer-details
  {:payer-details {:type       "object"
                   :properties {:msisdn           {:optional true
                                                   :type     "string"}
                                :userId           {:optional true
                                                   :type     "string"}
                                :account-no       {:optional true
                                                   :type     "string"}
                                :payment-ins-name {:optional true
                                                   :type     "string"}}}})

(defonce payee-details
  {:payee-details {:type       "object"
                   :properties {:msisdn           {:optional true
                                                   :type     "string"}
                                :userId           {:optional true
                                                   :type     "string"}
                                :account-no       {:optional true
                                                   :type     "string"}
                                :payment-ins-name {:optional true
                                                   :requires "account-no"
                                                   :type     "string"}}}})


(defonce amount
  {:amount {:type       "object"
            :properties {:amount-value  {:type "string"}
                         :currency-code {:type "string"
                                         :optional currency-code-optional}}}})

(defonce addtional-params
  {:additionalParams {:type "object"
                     :optional true
                      :properties {
                                  :orderNo {:type "string"
                                               :optional true},
                                  :invoiceNo {:type "string"
                                              :optional true},
                                  :ezcashAgentAlias {:type "string"
                                                     :optional true},
                                  :ezcashAgentPin {:type "string"
                                                   :optional true},
                                  :reason-code {:type "string"
                                               :optional true}}}})


(defonce commit&cancel-required-ids
  {:client-trans-id {:type     "string"
                     :optional true}
   :reservation-id  {:type "string"}})

(defonce query-balance-reuired-ids
  {:client-trans-id {:type     "string"
                     :optional true}})

(defonce credit-reserve-schema
  {:type       "object"
   :properties (merge common-charging-req amount payer-details)})

(defonce credit-cancel-schema
  {:type       "object"
   :properties commit&cancel-required-ids})

(defonce credit-commit-schema
  {:type       "object"
   :properties commit&cancel-required-ids})

(defonce direct-debit-schema
  {:type       "object"
   :properties (merge common-charging-req payer-details amount addtional-params)})

(defonce query-balance-schema
  {:type       "object"
   :properties (merge common-charging-req query-balance-reuired-ids payer-details)})

(defonce transfer-schema
  {:type       "object"
   :properties (merge common-charging-req payee-details payer-details amount)})

(defonce credit-schema
  {:type       "object"
   :properties (merge common-charging-req payee-details amount)})

; =========== Query Request Schema ==============

(defonce common-query-req
  {:user-id                    {:type     "string"
                                :optional true}
   :msisdn                     {:type     "string"
                                :optional true}
   :req-sys-shortcode          {:type     "string"
                                :optional true}
   :is-auth-reqd               {:type     "string"
                                :optional true}
   :merchant-should-be-allowed {:type     "string"
                                :optional true}})


(defonce pay-ins-query-schema
  {:type       "object"
   :properties common-query-req})


(defonce allow-merchant-schema
  {:type       "object"
   :properties {:client-trans-id            {:type     "string"}
                :merchant-id                {:type     "string"}
                :merchant-name              {:type     "string"}
                :msisdn                     {:type     "string"
                                             :optional true}
                :userId                     {:type     "string"
                                             :optional true}}})

(defonce transaction-query-schema
  {:type       "object"
   :properties {:transaction-id             {:type     "string"
                                             :optional false}}
                :requested-timestamp        {:type     "string"
                                             :optional true}})
