(ns hms.kite.pgw-connector.connector
  (:use [clojure.algo.monads :only [domonad with-monad m-chain]]
    [clojure.tools.logging :only [info]]
    [hms.kite.pgw-connector.failure-monads]
    [hms.kite.pgw-connector.util]
    [hms.kite.pgw-connector.xml]
    [hms.kite.pgw-connector.http]
    [hms.kite.pgw-connector.validation]))

(defn- log-charging-req [req]
  (do
    (info (str "Sending charging req to payment gateway[" (seq req) "]"))
    req))

(defn- log-charging-resp [resp]
  (do
    (info (str "Received charging resp from payment gateway[" resp "]"))
    resp))

(defn execute-req-with-fns [{:keys [req-path] :as pgw-req-with-path} validator-fn add-system-setting-fn xml-mk-fn]
  (with-monad failure-m
    ((m-chain
      [validator-fn add-system-setting-fn log-charging-req xml-mk-fn to-xml-str (partial send-xml-req req-path) log-charging-resp])
      pgw-req-with-path)))
;
(defn add-currency-code [req]
  (info "Received request [" + req + "] to add system properties")
  (if (nil? (:currency-code (:amount req)))
    (assoc-in req [:amount :currency-code] (pgw-connector-properties "pgw.system.currency.code"))
    req))

(defn- add-bulk-req-currency-code[reqs]
  (map #(add-currency-code %) reqs))

(defn dummy-fn [req]
  req)

(defn- bulk-req-validator-fn [{:keys [req]}]
  req)

;TODO: refactor this duplicate code

(defn reserve-credit [reserve-req]
  (execute-req-with-fns
    {:req reserve-req
     :req-path "/reserveCredit"}
    validate-request-params
    add-currency-code
    mk-credit-reserve))

(defn bulk-reserve-credit [reserve-reqs]
  (execute-req-with-fns
    {:req reserve-reqs
     :req-path "/bulk/creditReserve"}
    bulk-req-validator-fn
    add-bulk-req-currency-code
    mk-bulk-credit-reserve))

(defn commit-credit [commit-req]
  (execute-req-with-fns
    {:req commit-req
     :req-path "/commitCredit"}
    validate-request-params
    dummy-fn
    mk-credit-commit))

(defn bulk-commit-credit [commit-reqs]
  (execute-req-with-fns
    {:req commit-reqs
     :req-path "/bulk/creditCommit"}
    bulk-req-validator-fn
    dummy-fn
    mk-bulk-credit-commit))

(defn cancel-credit [cancel-req]
  (execute-req-with-fns
    {:req cancel-req
     :req-path "/cancelCredit"}
    validate-request-params
    dummy-fn
    mk-credit-cancel))

(defn bulk-cancel-credit [cancel-reqs]
  (execute-req-with-fns
    {:req cancel-reqs
     :req-path "/bulk/creditCancel"}
    bulk-req-validator-fn
    dummy-fn
    mk-bulk-credit-cancel))

(defn request-credit [credit-req]
  (execute-req-with-fns
    {:req credit-req
     :req-path "/credit"}
    validate-request-params
    add-currency-code
    mk-request-credit))

(defn request-debit [debit-req]
  (execute-req-with-fns
    {:req debit-req
     :req-path "/directDebit"}
    validate-request-params
    add-currency-code
    mk-request-debit))

(defn request-transfer [transfer-req]
  (execute-req-with-fns
    {:req transfer-req
     :req-path "/transfer"}
    validate-request-params
    dummy-fn
    mk-request-transfer))

(defn allow-merchant [allow-req]
  (execute-req-with-fns
    {:req allow-req
     :req-path "/allowMerchantQuery"}
    validate-request-params
    dummy-fn
    mk-allow-merchant))

(defn query-payment-instruments [query-req]
  (execute-req-with-fns
    {:req query-req
     :req-path "/payInstrumentQuery"}
    validate-request-params
    dummy-fn
    mk-payment-ins-query))

(defn query-async-transaction-status [query-req]
  (execute-req-with-fns
    {:req query-req
     :req-path "/transactionQuery"}
    validate-request-params ;TODO What function should I have here and below
    dummy-fn
    mk-transaction-status-query))

(defn request-query-balance [query-balan-req]
  (execute-req-with-fns
    {:req query-balan-req
     :req-path "/queryBalance"}
    validate-request-params
    dummy-fn
    mk-request-query-balance))