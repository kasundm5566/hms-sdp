(ns hms.kite.charging
  (:gen-class
    :name hms.kite.Charging
    :main false
    :implements [hms.kite.wf.api.Service hms.kite.wf.api.ServiceLifeCycle hms.kite.charging.ChargingConfiguration]
    :method [[setSystemCurrency [java.lang.String] void]
             [setSystemConfiguration [hms.kite.datarepo.SystemConfiguration] void]
             ])
  (:import [java.util Collection Map HashMap ArrayList]
    [java.io StringBufferInputStream]
    [org.joda.time DateTime DateTimeZone]
    [hms.kite.util SystemUtil SdpException]
    [hms.kite.datarepo RepositoryServiceRegistry])
  (:use [clojure.set :only [rename-keys]]
    [clojure.tools.logging]
    [clojure.data.json   :only [json-str]]
    [clojure.algo.monads :only [domonad with-monad m-chain]]
    [clojure.walk :only [keywordize-keys stringify-keys]]
    [closchema.core :only [validate report-errors]]
    [hms.common.clj-coll-util :only [to-clj-map to-java-map]]
    [hms.kite.pgw-connector.failure-monads]
    [hms.kite.pgw-connector.connector]
    [hms.kite.pgw-connector.status-codes :only [get-mapped-code]]))

(defonce system-id "SDP")

(defonce system-currency "KES")

(def system-configuration)

(defonce system-exchange-rate
  [{:buyingRate 1 :sellingRate 1 :currencyCode system-currency}])

(defonce charging-types #{"free" "flat" "variable" "keyword"})

(defonce charging-methods #("default-payment-instrument" "payment-instrument" "operator-charging"))

(defonce request-type&status-codes {"reserve-credit"       #{"S1000"}
                                    "commit-credit"        {:delivery-success #{"S1000"}
                                                            :delivery-failure #{"E1321"}}
                                    "cancel-credit"        #{"S1000"}})

(defonce commit-status-codes (into #{} (reduce concat (-> request-type&status-codes (get "commit-credit") vals))))

(doseq [path ["charging_util" "charging_validation" "charging_request" "charging_response"]]
  (load path))

(defn request-type-party&method
  [{{direction :direction
     request-type :request-type} :request
    {{{type :type
       party :party
       allowed-pi :allowed-payment-instruments} :charging} (keyword direction)} :ncs}]
  (let [method (first allowed-pi)]
    (debug (str "Available allowed PI[" allowed-pi "], Selected[" method "]"))
    ;TODO: Handle for SP charging, waiting till NCS structure is finalized
    (cond
      (= "free" type) [request-type "free" "N/A" "N/A"]
      (= "operator-charging" method) [request-type "operator-charging" "N/A" "N/A"]
      (and (= "subscriber" party) (= false (nil? method))) [request-type party "default-payment-instrument"]
      :else [request-type party (if method
      method
      "N/AD")])))

(defmulti process-charging-request request-type-party&method)

(defmethod process-charging-request ["reserve-credit" "free" "N/A" "N/A"] [req]
  req)

(defmethod process-charging-request ["commit-credit" "free" "N/A" "N/A"] [req]
  req)

(defmethod process-charging-request ["cancel-credit" "free" "N/A" "N/A"] [req]
  req)

(defn gen-operator-charging-details [req]
  (if (not-empty (get-in req [:request :recipients]))
    (mk-operator-charging-details req)
    []))

(defmethod process-charging-request ["reserve-credit" "operator-charging" "N/A" "N/A"] [req]
  (let [status-codes (request-type&status-codes "reserve-credit")]
    (->> req
      (filter-request-by-status status-codes)
      gen-operator-charging-details
      (merge-charging-resp-with-request-fn status-codes req)
      set-operator-charging
      update-service-code
      update-charged-amount)))

(defmethod process-charging-request ["commit-credit" "operator-charging" "N/A" "N/A"] [req]
  req)

(defmethod process-charging-request ["cancel-credit" "operator-charging" "N/A" "N/A"] [req]
  req)

(defn charging-executor-fn [req req-builder-fn charging-fn]
  (if (not-empty (get-in req [:request :recipients]))
    (let [resp (with-monad failure-m
      ((m-chain
        [req-builder-fn
         charging-fn
         extract-response
         (map-response-paramters-fn req)
         (merge-request&response-fn req)])
        req))]
      (if (not (failed? resp))
        resp
        (fill-pgw-connector-error-code req (value resp))))
    []))

(defn reserve-credit-executor [req]
  (charging-executor-fn
    req
    mk-bulk-reserve-credit-req
    bulk-reserve-credit))

(defn commit-credit-executor [req]
  (charging-executor-fn
    req
    mk-bulk-commit-or-cancel-credit-req
    bulk-commit-credit))

(defn cancel-credit-executor [req]
  (charging-executor-fn
    req
    mk-bulk-commit-or-cancel-credit-req
    bulk-cancel-credit))


(defn execute-reserve-credit-flow [req]
  (let [status-codes (request-type&status-codes "reserve-credit")]
    (->> req
      (filter-request-by-status status-codes)
      reserve-credit-executor
      (merge-charging-resp-with-request-fn status-codes req))))

(defn- execute-commit-credit [req]
  (let [status-codes (get-in request-type&status-codes ["commit-credit" :delivery-success])]
    (->> req
      (filter-request-by-status status-codes)
      commit-credit-executor)))

(defn- execute-rolback-credit [req]
  (let [status-codes (get-in request-type&status-codes ["commit-credit" :delivery-failure])]
    (->> req
      (filter-request-by-status status-codes)
      cancel-credit-executor)))

(defn execute-commit-credit&cancel-flow [req]
  (let [f1 (future (execute-commit-credit req))
        f2 (future (execute-rolback-credit req))]
    (merge-charging-resp-with-request-fn
      commit-status-codes req (lazy-cat @f1 @f2))))

(defn execute-cancel-credit-flow [req]
  (let [status-codes (request-type&status-codes "cancel-credit")]
    (->> req
      (filter-request-by-status status-codes)
      cancel-credit-executor
      (merge-charging-resp-with-request-fn status-codes req))))

(defmethod process-charging-request ["reserve-credit" "subscriber" "default-payment-instrument"] [req]
  (execute-reserve-credit-flow req))

(defmethod process-charging-request ["commit-credit" "subscriber" "default-payment-instrument"] [req]
  (execute-commit-credit&cancel-flow req))

(defmethod process-charging-request ["cancel-credit" "subscriber" "default-payment-instrument"] [req]
  (execute-cancel-credit-flow req))

(defmethod process-charging-request ["reserve-credit" "sp" "N/A"] [req]
  (execute-reserve-credit-flow req))

(defmethod process-charging-request ["commit-credit" "sp" "N/A"] [req]
  (execute-commit-credit&cancel-flow req))

(defmethod process-charging-request ["cancel-credit" "sp" "N/A"] [req]
  (execute-cancel-credit-flow req))

(defmethod process-charging-request :default [req]
  (do (error
        (str
          "Could not find a charging flow to execute : "
          (into {} (map vector ["request-type" "party" "method"] (request-type-party&method req)))))
    (failure "E1312" "Invalid charging request")))

;update sdp request map with processed data
(defn update-request [^Map sdp-request charging-resp]
  (let [charging-resp-j-map (-> charging-resp :request to-java-map)]
    (do
      (.put sdp-request "request" charging-resp-j-map)
      (if (not= "cancel-credit"  (get charging-resp-j-map "request-type"))
        (success-resp)
        (failure (get charging-resp-j-map "status-code") (get charging-resp-j-map "status-description"))))))

(defn -process [this ^Map request]
(debug (str "Received context for charing[" request "]"))
  (let [transformed-req (to-clj-map request)
        response (with-monad failure-m
      ((m-chain
        [validate-charging-request
         process-charging-request
         (partial update-request request)])
        transformed-req))]
    (-> response value to-java-map)))

(defn -setSystemCurrency [_ currency]
(def system-currency currency))

(defn -setSystemConfiguration [_ configuration]
  (def system-configuration configuration))
