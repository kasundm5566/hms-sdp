(in-ns 'hms.kite.charging)

(defn- address-status-param [direction]
  (if (= "mt" direction)
    :recipient-address-status
    :sender-address-status))

(defn- map-parameters-in-dividual-resp-fn [req]
  (fn [{:keys [status-code] :as individual-resp}]
    (let [address-status-kw (-> req request-direction address-status-param)]
      (merge
        (rename-keys individual-resp {:client-transid :charging-trx-id, :status-code address-status-kw})
        {:charging-status   status-code
         :charged-category  (get-in req [:request :ncs-type])
         :charged-amount    (get-charging-amount req)
         address-status-kw  (get-mapped-code status-code)}))))

(defn map-response-paramters-fn [req]
  (fn [bulk-resp]
    (map (map-parameters-in-dividual-resp-fn req) bulk-resp)))

(defmulti fill-pgw-connector-error-code (fn [req _] (request-direction req)))

(defmethod fill-pgw-connector-error-code "mt" [{{recipients :recipients} :request} {status-code :status-code}]
  (map #(assoc % :recipient-address-status status-code) recipients))

(defmethod fill-pgw-connector-error-code "mo" [_ {status-code :status-code}]
  (list {:sender-address-status status-code}))

(defmethod fill-pgw-connector-error-code :default [req]
  (list))

(defn extract-response [{overall-status      :overall-status
                              individual-response :individual-response}]
  (if (not (nil? individual-response))
    individual-response
    (failure "E1308" (str "PGW Error [" overall-status "]"))))


(defmulti merge-request&response-fn request-direction)

(defmethod merge-request&response-fn "mt" [{{recipients :recipients} :request}]
  (fn [bulk-resp]
    (map #(merge (first %) (second %))
      (vals
        (group-by :charging-trx-id
          (concat
            recipients
            bulk-resp))))))

(defmethod merge-request&response-fn "mo" [_]
  identity)

(defmethod merge-request&response-fn :default [_]
  (failure "E1312" "Invalid request direction"))
