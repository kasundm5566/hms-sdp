(in-ns 'hms.kite.charging)

(defonce charging-request-schema
  {:type       "object"
   :properties {:sp      {:type       "object"
                          :optional   true
                          :properties {:coop-user-id {:type "string"}}}
                :ncs     {:type       "object"
                          :properties {:app-id {:type "string"}
                                       :mo {:type       "object"
                                            :optional   true
                                            :properties {:charging {:type "object"}}}
                                       :mt {:type       "object"
                                            :optional   true
                                            :properties {:charging {:type "object"}}}}}
                :request {:type           "object"
                          :properties     {:request-type          {:type "string"
                                                                   :enum ["reserve-credit" "commit-credit" "cancel-credit"]}
                                           :ncs-type              {:type "string"
                                                                   :enum ["sms" "wap-push" "ussd" "lbs"]}
                                           :direction             {:type "string"
                                                                   :enum ["mo" "mt"]}
                                           :correlation-id        {:type "string"}
                                           :sender-address-status {:type "string"}
                                           :sender-address        {:type "string" :optional true}
                                           :charging-trx-id       {:type "string" :optional true}
                                           :ext-transid           {:type "string" :optional true}
                                           :used-exchange-rates   {:type "string" :optional true}
                                           :recipients            {:type "array"
                                                                   :items {:type "object"
                                                                           :properties {:recipient-address              {:type "string"}
                                                                                        :recipient-address-status       {:type "string"}
                                                                                        :charging-trx-id                {:type "string" :optional true}
                                                                                        :ext-transid                    {:type "string" :optional true}
                                                                                        :used-exchange-rates            {:type "string" :optional true}}}}}}}})

(defn- find-error-msg [{ref  :ref
                        key  :key
                        data :data}]
  (if ref
    (str "Parameter [" (name ref) "] " (name key))
    (str "Invalid parameter value [" (:value data) "] expected [" (apply str (interpose " " (:enum data))) "]")))

(defn- format-validation-errors [errors]
  (apply str (interpose "," (map find-error-msg errors))))

(defn validate-charging-request [req]
  (let [validation-results (report-errors (validate charging-request-schema req))]
    (if (empty? validation-results)
      req
      (let [validation-error-description (format-validation-errors validation-results)]
        (do (error (str "Validation Error : " validation-error-description))
          (failure "E1312" (str "Invalid charging request : " validation-error-description)))))))

(defn- numeric? [s]
  (if-let [s (seq s)]
    (let [f #(Character/isDigit (int %))
          s (if (= (first s) \-) (next s) s)
          s (drop-while f s)
          s (if (= (first s) \.) (next s) s)
          s (drop-while f s)]
      (empty? s))
    false))

(defn- read-numeric [s]
  (if (numeric? s)
    (read-string s)
    0))

(defn validate-variable-charging-amount [req]
  (if-let [charging-amount (read-numeric (get-in req [:request :charging-amount]))]
    (and
      (<= (read-numeric (charging-param-by-name req :min-amount)) charging-amount)
      (>= (read-numeric (charging-param-by-name req :max-amount)) charging-amount))
    false))
