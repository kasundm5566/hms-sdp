(in-ns 'hms.kite.charging)

(defn- new-charging-tx-id []
  (hms.kite.util.SystemUtil/getCorrelationId))

(defn current-time-str []
  (str (DateTime. #^DateTimeZone (DateTimeZone/getDefault))))

(defn map-each-recipient-with [req f]
  (let [receipients (get-in req [:request :recipients ])]
    (map f receipients)))

(defn repeat-each-recipient-with [req m]
  (let [receipients (get-in req [:request :recipients ])]
    (repeat
      (count receipients)
      m)))

(defn mk-basic-reserve-credit-req [{{name :name} :app
                                    {app-id :app-id} :ncs
                                    {ncs-type :ncs-type} :request}]
  {:merchant-id app-id
   :merchant-name name
   :is-auth-reqd "false"
   :charged-category ncs-type
   :system-id system-id
   :merchant-should-be-allowed "true"
   :requested-timestamp (current-time-str)})

(defmulti mk-client-trans-ids request-direction)

(defmethod mk-client-trans-ids "mo" [req]
  {:client-trans-id (get-in req [:request :charging-trx-id ])})

(defmethod mk-client-trans-ids "mt" [req]
  (map-each-recipient-with
    req
    (fn [recipient]
      {:client-trans-id (recipient :charging-trx-id )})))

(defn payer-selectors [req]
;(charging-param-by-name req :method "N/A")
  [(request-direction req) (charging-param-by-name req :party ) (get-payment-instrument-name req)])

;Make Payer Details

(defmulti mk-payer-details payer-selectors)

(defmethod mk-payer-details ["mo" "subscriber" "default-payment-instrument"] [req]
  (repeat-each-recipient-with
    req
    {:payer-details {:msisdn (get-in req [:request :sender-address ])}}))

(defmethod mk-payer-details ["mo" "sp" "N/A"] [req]
  (repeat-each-recipient-with
    req
    {:payer-details
         {:payment-ins-name (charging-param-by-name req :payment-instrument-name)
          :account-no (charging-param-by-name req :payment-account)
          :userId (get-in req [:sp :coop-user-id])}}))

(defmethod mk-payer-details ["mt" "sp" "N/A"] [req]
  (repeat-each-recipient-with
    req
    {:payer-details
         {:payment-ins-name (charging-param-by-name req :payment-instrument-name)
          :account-no (charging-param-by-name req :payment-account)
          :userId (get-in req [:sp :coop-user-id])}}))

(defmethod mk-payer-details ["mt" "subscriber" "default-payment-instrument"] [req]
  (map-each-recipient-with
    req
    (fn [recipient]
        {:payer-details
         {:msisdn (recipient :recipient-address)}})))

(defmethod mk-payer-details :default [req]
  (do (error
        (str
          "Can not find required details to create payer details, check these parameters : "
          (into {} (map vector ["direction" "party" "method"] (payer-selectors req)))))
    (failure "E1312" "Invalid charging SLA parameters")))


;Make charging amount

(defn- mk-charging-details [amount]
  {:amount {:amount-value (str amount)
            :currency-code system-currency}})

(defmulti mk-charging-amounts #(charging-param-by-name % :type))

(defmethod mk-charging-amounts "flat" [req]
  (repeat-each-recipient-with
    req
    (mk-charging-details (charging-param-by-name req :amount))))

(defmethod mk-charging-amounts "variable" [req]
  (if (validate-variable-charging-amount req)
    (repeat-each-recipient-with
      req
      (mk-charging-details (get-in req [:request :charging-amount ])))
    (do
      (error
        (str "Invalid variable charging amount : "
          (get-in req [:request :charging-amount ] "Null")))
      (failure "E1312" "Invalid variable charging amount"))))

(defmethod mk-charging-amounts "keyword" [req]
  (repeat-each-recipient-with
    req
    (mk-charging-details
      ((charging-param-by-name req :keyword-charging-amounts )
        (keyword (get-in req [:request :keyword ]))))))

(defmethod mk-charging-amounts :default [req]
  (do
    (error
      (str "Invalid charging type : "
        (charging-param-by-name req :type "Null")))
    (failure "E1312" "Invalid charging type")))

(defn mk-basic-credit-reserve-reqs [req]
  (repeat-each-recipient-with
    req
    (mk-basic-reserve-credit-req req)))

(defn continue-mk-fn [req f]
  (fn [r1]
    (let [r2 (f req)]
      (if (failed? r2)
        r2
        (map merge r1 r2)))))

(defmulti empty-bulk-req request-direction)

(defmethod empty-bulk-req "mo" [req]
  (list {}))


(defmethod empty-bulk-req "mt" [req]
  (repeat-each-recipient-with
    req
    {}))

(defn mk-bulk-reserve-credit-req [bulk-req]
  (debug (str "----------------- " bulk-req))
  (let [intial-req (empty-bulk-req bulk-req)
        bulk-resp (with-monad failure-m
      ((m-chain
         [(continue-mk-fn bulk-req mk-basic-credit-reserve-reqs)
          (continue-mk-fn bulk-req mk-client-trans-ids)
          (continue-mk-fn bulk-req mk-charging-amounts)
          (continue-mk-fn bulk-req mk-payer-details)])
        intial-req))]
    bulk-resp))

(defmulti mk-bulk-commit-or-cancel-credit-req request-direction)

(defmethod mk-bulk-commit-or-cancel-credit-req "mt" [{{recipients :recipients} :request}]
  (map
    (fn [recipient]
      {:client-trans-id (recipient :charging-trx-id )
       :reservation-id (recipient :int-transid )})
    recipients))

(defmethod mk-bulk-commit-or-cancel-credit-req "mo" [{{charging-trx-id :charging-trx-id
                                                       int-transid :int-transid} :request}]
  (list
    {:client-trans-id charging-trx-id
     :reservation-id int-transid}))

(defn- operator-charging-details [req]
  (let [charging-type (get-in req [:ncs :mt :charging :type])
        charing-details {:currency-code system-currency
                   :operator-cost (get-charging-amount req)
                   :charged-amount (get-charging-amount req)
                   :charged-category (service-code-for-amount (charging-param-by-name-operator req :operator)
                          (cond
                            (= "flat" charging-type) (charging-param-by-name req :amount)
                            (= "variable" charging-type) (convert-to-amount (get-in req [:request :charging-amount ]))
                            )
                      )
                   :charging-status "operator-charging"
                   :used-exchange-rates (json-str system-exchange-rate)}]
    charing-details))

(defmulti mk-operator-charging-details request-direction)

(defmethod mk-operator-charging-details "mo" [req]
  (list (merge req (operator-charging-details req))))

(defmethod mk-operator-charging-details "mt" [req]
  (let [op-charging-detail (operator-charging-details req)]
    (map #(merge % op-charging-detail) (get-in req [:request :recipients]))))
