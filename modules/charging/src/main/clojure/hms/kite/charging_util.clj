(in-ns 'hms.kite.charging)

(defn request-direction [req]
  (get-in req [:request :direction]))

(defn app-id [req]
  (get-in req [:ncs :app-id]))

(defn app-name [req]
  (get-in [:app :name]))

(defn group-recipients-by-status [status-codes recipients]
  (group-by
    #(if (contains? status-codes (:recipient-address-status %)) :succeeded :failed)
    recipients))

(defn filter-request-by-status [status-codes req]
  (if (= "mt" (request-direction req))
    (update-in
      req
      [:request :recipients]
      #(->> % (group-recipients-by-status status-codes) :succeeded))
    (if (contains? status-codes (get-in req [:request :sender-address-status]))
      req
      {})))

(defn- fill-currency-code [{{request-type :request-type} :request} charging-details]
  (map #(assoc % :currency-code system-currency) charging-details))

(defn merge-charging-resp-with-request-fn [status-codes initial-req charging-details]
  (let [charging-details (fill-currency-code initial-req charging-details)]
    (if (= "mt" (request-direction initial-req))
      (update-in
        initial-req
        [:request :recipients]
        #(->> % (group-recipients-by-status status-codes) :failed (concat charging-details) vec))
      (if (contains? status-codes (get-in initial-req [:request :sender-address-status]))
        (assoc
          initial-req
          :request
          (merge
            (get initial-req :request)
            (first charging-details))) ; only one charging entry for mo
        initial-req))))

(defn charging-param-by-name [req param & default-value-seq]
  (get-in req [:ncs (keyword (request-direction req)) :charging param] (first default-value-seq)))

(defn get-payment-instrument-name [req]
  (let [direction (keyword (request-direction req))
        method (first (get-in req [:ncs direction :charging :allowed-payment-instruments ] []))
        party (get-in req [:ncs direction :charging :party ])]
    ;TODO: Handle for SP charging, waiting till NCS structure is finalized
    (cond
      (and (= "subscriber" party) (= false (nil? method))) "default-payment-instrument"
      :else "N/A")
    ))

(defn charging-param-by-name-operator [req param & default-value-seq]
  (get-in req [:ncs param] (first default-value-seq)))

(defn- charging-type&method
  [{{direction :direction}                             :request
    {{{type   :type
       method :method} :charging} (keyword direction)} :ncs}]
  (if (= "operator-charging" method)
    method
    type))

(defmulti get-charging-amount charging-type&method)

(defmethod get-charging-amount "flat" [req]
  (charging-param-by-name req :amount))

(defmethod get-charging-amount "variable" [req]
  (get-in req [:request :charging-amount]))

(defmethod get-charging-amount "keyword" [req]
  ((charging-param-by-name req :keyword-charging-amounts)
    (keyword (get-in req [:request :keyword]))))

(defmethod get-charging-amount "operator-charging" [req]
  (charging-param-by-name req :amount))


(defn update-charged-amount [req]
  (assoc-in req [:request :charged-amount] (get-charging-amount req)))

(defn set-operator-charging [req]
  (assoc-in req [:request :operator-charging] true))


(defn map-operator-service-code-amount [operator]
  (into {} (.find system-configuration (str operator "-sms-service-code-charging-amount-mapping"))))

(defn service-code-for-amount [operator amount]
  (let [service-code-amount-map (map-operator-service-code-amount operator)
        service-code (some #(if (= (val %) amount) (key %)) service-code-amount-map)]
    (if (nil? service-code) (throw (hms.kite.util.SdpException. "E1336" "No matching servie code found")) service-code)
    ))

(defn convert-to-amount [amount]
  (if (zero? (.compareTo (BigDecimal.  (format "%.0f" (BigDecimal. amount))) (BigDecimal. amount))) (format "%.0f" (BigDecimal. amount)) "-1"))

(defn get-service-code [req]
  (let [charging-type (get-in req [:ncs :mt :charging :type])
        service-code (service-code-for-amount (charging-param-by-name-operator req :operator)
          (cond
            (= "flat" charging-type) (charging-param-by-name req :amount)
            (= "variable" charging-type) (convert-to-amount (get-in req [:request :charging-amount ]))))]
  service-code))

(defn update-service-code [req]
  (assoc-in req [:request :service-code] (get-service-code req)))