(ns hms.kite.charging-as-service
  (:gen-class
    :name hms.kite.CasAdapter
    :methods [[reserveCredit [java.util.Map] java.util.Map]
              [commitCredit [java.util.Map] java.util.Map]
              [cancelCredit [java.util.Map] java.util.Map]
              [credit [java.util.Map] java.util.Map]
              [transfer [java.util.Map] java.util.Map]
              [directDebit [java.util.Map] java.util.Map]
              [allowMerchant [java.util.Map] java.util.Map]
              [queryPaymentInstruments [java.util.Map] java.util.Map]
              [asyncTransactionStatus [java.util.Map] java.util.Map]
              [queryUserBalance [java.util.Map] java.util.Map]]
    :main false)
  (:import [java.util HashMap]
    [java.io StringBufferInputStream]
    [hms.kite.util SystemUtil])
  (:use [clojure.tools.logging]
    [clojure.set :only [rename-keys]]
    [clojure.algo.monads :only [domonad with-monad m-chain]]
    [clojure.walk :only [keywordize-keys stringify-keys]]
    [hms.common.clj-coll-util :only [to-clj-map]]
    [hms.kite.pgw-connector.failure-monads]
    [hms.kite.pgw-connector.connector]
    [hms.kite.pgw-connector.status-codes :only [get-mapped-code]]
    [hms.kite.charging]))

(defn map-status-code [{status-code :status-code :as pgw-resp}]
  (info "Pgw response[" pgw-resp "]")
  (assoc pgw-resp :status-code (get-mapped-code status-code)))

(defn update-charging-req-amount[req]
  (if (nil? (:currency-code (:amount req)))
    (assoc-in req [:amount :currency-code] system-currency)
    req))

(defn add-currency-code-to-response[pgw-resp currency-code]
  (info "Received request [" + pgw-resp + "] currency in the requet [" + currency-code + "].")
  (if (nil? currency-code)                         ; currency-code is mandate for vcity nbl req, optional for dialog,
    (assoc pgw-resp :currency-code system-currency)
    (assoc pgw-resp :currency-code currency-code)))

(defn process [charging-fn req]
  (let [transformed-req  (to-clj-map req)
        request-currency (:currency-code (:amount req))
        response (add-currency-code-to-response (with-monad failure-m
      ((m-chain
        [charging-fn
         map-status-code])
        transformed-req)) request-currency)]
    (-> response value stringify-keys)))

(defn process-with-status [charging-fn req]
  (let [transformed-req (to-clj-map req)
        response (with-monad failure-m
      ((m-chain
        [charging-fn
         #(rename-keys % {:status :status-code})
         map-status-code])
        transformed-req))]
    (-> response value stringify-keys)))

(defn -reserveCredit [_ req]
  (->> req (process reserve-credit)))

(defn -commitCredit [_ req]
  (->> req (process commit-credit)))

(defn -cancelCredit [_ req]
  (->> req (process cancel-credit)))

(defn -credit [_ req]
  (->> req (process request-credit)))

(defn -transfer [_ req]
  (->> req (process request-transfer)))

(defn -directDebit [_ req]
  (->> req (process request-debit)))

(defn -allowMerchant [_ req]
  (->> req (process allow-merchant)))

(defn -queryPaymentInstruments [_ req]
  (->> req (process-with-status query-payment-instruments)))

(defn -asyncTransactionStatus [_ req]
  (->> req (process-with-status query-async-transaction-status)))

(defn -queryUserBalance[_ req]
  (->> req (process request-query-balance)))