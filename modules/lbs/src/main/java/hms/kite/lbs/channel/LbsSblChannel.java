/**
 *   (C) Copyright 2012-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.lbs.channel;

import hms.common.rest.util.client.AbstractWebClient;
import hms.kite.datarepo.RepositoryServiceRegistry;
import hms.kite.util.KeyNameSpaceResolver;
import hms.kite.util.SdpException;
import hms.kite.wfengine.transport.Channel;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.wfengine.impl.ResponseBuilder.generate;

public class LbsSblChannel extends AbstractWebClient implements Channel {

    private final static Logger logger = LoggerFactory.getLogger(LbsSblChannel.class);
    private Map<String, WebClient> sblClientMap;

    public void init() {
        super.init();
        sblClientMap = new HashMap<String, WebClient>();
        Map<String, String> sblUrlMap = (Map<String, String>) RepositoryServiceRegistry.systemConfiguration().find(
                sblLbsReceiverAddressesK);
        logger.debug("SBL lbs operator url map {}", sblUrlMap);
        for (Map.Entry<String, String> entry : sblUrlMap.entrySet()) {
            WebClient webClient = createWebClient(entry.getValue());
            sblClientMap.put(entry.getKey(), webClient);
        }
        logger.trace("SBL lbs operator web client map {}", sblClientMap);

    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {

        Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String, Object>>();
        requestContext.put(requestK, request);

        return execute(requestContext);
    }

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        try {
            logger.trace("Request context [{}]", requestContext);
            Map<String, String> recipient = getRecipient(requestContext);
            String operator = recipient.get(recipientAddressOptypeK);
            WebClient webClient = getWebClient(operator);
            Map<String, Object> lbsMsg = createSblMessage(requestContext, recipient);
            logger.info("Sending lbs message[{}] to sbl url [{}]", lbsMsg, webClient.getCurrentURI());
            Response response = sendMtRequest(webClient, lbsMsg);
            logger.info("Response received from sbl status[{}] message[{}]", response.getStatus(), response.getEntity());
            if (isSuccessHttpResponse(response)) {
                logger.info("LBS SubmitResponse received [{}]", response);
                return processResponse(requestContext, response);
            } else {
                return generateErrorResponse(requestContext, response);
            }
        } catch (SdpException e) {
            throw e;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return generate(temporarySystemErrorCode, false, requestContext);
        }
    }

    private WebClient getWebClient(String operator) {
        WebClient webClient = sblClientMap.get(operator);
        if (webClient == null) {
            throw new SdpException(systemErrorCode, "Sending to sbl failed. Couldn't find webClient for operator [" + operator + "]");
        }
        return webClient;
    }

    private Map<String, String> getRecipient(Map<String, Map<String, Object>> requestContext) {
        List<Map<String, String>> recipientList = (List<Map<String, String>>) requestContext.get(requestK).get(
                recipientsK);

        if (recipientList.size() == 1) {
            Map<String, String> recipient = recipientList.get(0);
            if (successCode.equals(recipient.get(recipientAddressStatusK))) {
                return recipient;
            } else {
                logger.error("Recipient address [{}] is not in valid state", recipient);
                throw new SdpException(recipient.get(recipientAddressStatusK), "Recipient address not in valid state");
            }
        } else {

            if (recipientList.isEmpty()) {
                throw new SdpException("No recipients found");
            } else {
                throw new SdpException("Multiple recipients found in the request. Current configuration only supports single recipient.");
            }
        }

    }

    private Map<String, Object> createSblMessage(Map<String, Map<String, Object>> requestContext, Map<String, String> recipient) {
        Map<String, Object> sblMsg = new HashMap<String, Object>();
        sblMsg.put(subscriberIdK, recipient.get(recipientAddressK));
        sblMsg.put("serviceType", KeyNameSpaceResolver.data(requestContext, requestK, serviceTypeK));
        sblMsg.put("responseTime", KeyNameSpaceResolver.data(requestContext, requestK, responseTimeK));
        sblMsg.put("horizontalAccuracy", KeyNameSpaceResolver.data(requestContext, requestK, horizontalAccuracyK) );
        sblMsg.put("freshness", KeyNameSpaceResolver.data(requestContext, requestK, freshnessOfLocationK));
        logger.debug("Created LBS Sbl message[{}]", sblMsg);
        return sblMsg;
    }

    private Response sendMtRequest(WebClient webClient, Map<String, Object> msg) {
        try {
            Response response = webClient.post(msg);
            return response;
        } catch (RuntimeException e) {
            logger.error("Couldn't send msg. SBL connection failed", e);
            throw e;
        }
    }

    private Map<String, Object> processResponse(Map<String, Map<String, Object>> requestContext, Response response) throws IOException {
        Map<String, Object> resp = readJsonResponse(response);
        logger.info("LBS response[{}]", resp);
        if (null == resp) {
            return generate(temporarySystemErrorCode, false, requestContext);
        } else if (successCode.equals(resp.get(statusK))) {
            resp.put(statusK, Boolean.toString(true));
            resp.put(statusCodeK, successCode);
            resp.put(statusDescriptionK, "Success");
            requestContext.put(responseK, resp);
            return resp;
        } else if(lbsInvalidPositioningMethodErrorCode.equals(resp.get(statusK))){
            resp.put(statusK, Boolean.toString(false));
            resp.put(statusCodeK, lbsInvalidPositioningMethodErrorCode);
            resp.put(statusDescriptionK, "Positioning method not supported by the system");
            requestContext.put(responseK, resp);
            return resp;
        } else {
            return generate(temporarySystemErrorCode, false, requestContext);
        }
    }

    private Map<String, Object> generateErrorResponse(Map<String, Map<String, Object>> requestContext, Response response) throws IOException {
        Map<String, Object> resp = readJsonResponse(response);
        if (resp != null) {
            String errorCode = temporarySystemErrorCode;
            Map<String, Object> errorResponse = generate(errorCode, false, requestContext);
            errorResponse.put(
                    statusDescriptionK,
                    MessageFormat.format("{0}({1})", errorResponse.get(statusDescriptionK),
                            resp.get(statusDescriptionK)));
            return errorResponse;
        } else {
            return generate(temporarySystemErrorCode, false, requestContext);
        }

    }

}
