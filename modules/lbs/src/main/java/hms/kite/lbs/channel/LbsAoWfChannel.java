/**
 *   (C) Copyright 2012-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.lbs.channel;

import hms.common.registration.api.common.StatusCodes;
import hms.kite.util.SdpException;
import hms.kite.util.SystemUtil;
import hms.kite.util.TimeStampUtil;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static hms.kite.util.KiteErrorBox.successCode;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.logging.StatLogger.printStatLog;

public class LbsAoWfChannel extends BaseChannel {
    private static final Logger logger = LoggerFactory.getLogger(LbsAoWfChannel .class);

    private Workflow lbsWf;
    private List<String> powerOnSubcriberStateList;

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        addMandatoryParams(requestContext);

        if (!isRecipientListAvailable(requestContext)) {
            createRecipientList(requestContext);
        }

        return executeWorkflow(requestContext);
    }

    private boolean isRecipientListAvailable(Map<String, Map<String, Object>> requestContext) {
        return requestContext.get(requestK).containsKey(recipientsK);
    }

    private void createRecipientList(Map<String, Map<String, Object>> requestContext) {
        requestContext.get(requestK).put(recipientsK,
                SystemUtil.convert2NblAddresses(Arrays.asList((String) requestContext.get(requestK).get(subscriberIdK))));
    }

    private void addMandatoryParams(Map<String, Map<String, Object>> requestContext) {
        requestContext.get(requestK).put(ncsTypeK, lbsK);
        requestContext.get(requestK).put(directionK, mtK);
        requestContext.get(requestK).put(receiveTimeK, Long.toString(System.currentTimeMillis()));
    }

    private Map<String, Object> executeWorkflow(Map<String, Map<String, Object>> requestContext) {
        long start = System.currentTimeMillis();
        try {
            lbsWf.executeWorkflow(requestContext);
            return generateResponseForNbl(requestContext);
        } catch (SdpException e) {
            return convert2NblResponse(ResponseBuilder.generate(e.getErrorCode(), false, requestContext),
                    requestContext);
        } catch (Exception e) {
            logger.error("Exception occurred while executing flow", e);
            return convert2NblResponse(ResponseBuilder.generate(requestContext, e), requestContext);
        } finally {
            printStatLog(start, "LBS-AO");
        }
    }

    private Map<String, Object> generateResponseForNbl(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> resp = convert2NblResponse(ResponseBuilder.generateResponse(requestContext), requestContext);
        if (successCode.equals(requestContext.get(requestK).get(statusCodeK))) {
            setLbsResponseParameters2NblResp(requestContext, resp);
        }
        return resp;
    }

    private void setLbsResponseParameters2NblResp(Map<String, Map<String, Object>> requestContext, Map<String, Object> resp) {
        Map<String, Object> resp2SentBack = requestContext.get(responseK);
        resp2SentBack.remove(statusCodeK);
        resp2SentBack.remove(statusDescriptionK);
        resp2SentBack.remove(statusK);
        for(Map.Entry<String, Object> item : resp2SentBack.entrySet()){
            resp.put(item.getKey(), item.getValue()) ;
        }

        String subscriberState = (String)resp.get(subscriberStateK);
        if(subscriberState != null){
               if(powerOnSubcriberStateList.contains(subscriberState)){
                   resp.put(subscriberStateK, true);
               } else {
                   resp.put(subscriberStateK, false);
               }
        }
    }


    private Map<String, Object> convert2NblResponse(Map<String, Object> response,
                                                         Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> nblResp = ResponseBuilder.convert2NblErrorResponse(response);
        if (requestContext.get(requestK).containsKey(correlationIdK)) {
            nblResp.put(messageIdNblK, requestContext.get(requestK).get(correlationIdK));
        }
        return nblResp;
    }


    public void setLbsWf(Workflow lbsWf) {
        this.lbsWf = lbsWf;
    }

    public void setPowerOnSubcriberStateList(List<String> powerOnSubcriberStateList) {
        this.powerOnSubcriberStateList = powerOnSubcriberStateList;
    }
}
