package hms.kite.lbs.channel;

import hms.kite.util.KeyNameSpaceResolver;
import hms.kite.util.SdpException;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;

public class QosValidationChannel extends BaseChannel {

    private static final Logger LOGGER = LoggerFactory.getLogger(QosValidationChannel.class);
    private Map<String, Integer> responseTimeMap;
    private Map<String, Integer> freshnessMap;
    private Map<String, Integer> horizontalAccMap;

    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> qos = (Map<String, Object>) KeyNameSpaceResolver.data(requestContext, ncsK, qosK);
        LOGGER.debug("Configured QOS[{}]", qos);
        if (qos == null || qos.isEmpty()) {
            return ResponseBuilder.generate(systemErrorCode, "Lbs QOS parameters are not configured.", false);
        } else {
            try {
                Map<String, Object> request = requestContext.get(requestK);

                boolean qosValid = isQosAllowed(responseTimeMap, convertNcsQosConfig((String) qos.get(responseTimeK)), request, responseTimeK) &&
                        isQosAllowed(freshnessMap, convertNcsQosConfig((String) qos.get(freshnessOfLocationK)), request, freshnessOfLocationK) &&
                        isQosAllowed(horizontalAccMap, qos.get(horizontalAccuracyK), request, horizontalAccuracyK);
                if (qosValid) {
                    return ResponseBuilder.generateSuccess();
                } else {
                    return ResponseBuilder.generate(lbsInvalidQosErrorCode, false, requestContext);
                }
            } catch (UnsupportedOperationException ex) {
                LOGGER.error("Error[{}]", ex.getMessage());
                return ResponseBuilder.generate(invalidRequestErrorCode, ex.getMessage(), false);
            }
        }
    }

    private boolean isQosAllowed(Map<String, Integer> data, Object allowed, Map<String, Object> request, String paramKey) {
        Object param = request.get(paramKey);
        LOGGER.trace("Received QOS Parameter[{}] to validate[{}]", paramKey, param);
        if(param == null){
            request.put(paramKey, allowed);
            LOGGER.debug("QOS Parameter[{}] is not provided with request, using default[{}]", paramKey, allowed);
            return true;
        } else {
            return getParamValue(data, (String) allowed) <= getParamValue(data, (String) param);
        }
    }

    private String convertNcsQosConfig(String value) {
        if (value != null) {
            return value.toUpperCase().replace("-", "_");
        } else {
            return value;
        }
    }

    private int getParamValue(Map<String, Integer> data, String key) {
        Integer val = data.get(key);
        if (val == null) {
            throw new SdpException(lbsInvalidQosErrorCode, "Invalid QOS value[" + key + "]");
        } else {
            return val;
        }
    }

    public void setResponseTimeMap(Map<String, Integer> responseTimeMap) {
        this.responseTimeMap = responseTimeMap;
    }

    public void setFreshnessMap(Map<String, Integer> freshnessMap) {
        this.freshnessMap = freshnessMap;
    }

    public void setHorizontalAccMap(Map<String, Integer> horizontalAccMap) {
        this.horizontalAccMap = horizontalAccMap;
    }
}
