/**
 *   (C) Copyright 2012-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.lbs;

import com.sun.java.swing.plaf.gtk.GTKConstants;
import hms.commons.IdGenerator;
import hms.kite.util.NblKeyMapper;
import hms.kite.util.SdpException;
import hms.kite.util.SystemUtil;
import hms.kite.wfengine.ServiceEndpoint;
import hms.kite.wfengine.transport.Channel;
import org.apache.log4j.NDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.stringtemplate.v4.misc.Misc;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import java.util.HashMap;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

@Produces({"application/json"})
@Consumes({"application/json"})
@Path("/lbs/")
public class LbsAoEndpoint extends ServiceEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(LbsAoEndpoint.class);
    public static final boolean FROM_NBL_PRAM = true;
    public static final boolean TO_NBL_PARAM = false;

    private Channel lbsAoChannel;
    private Map<String, String> nblLbsKeyMapper;
    private NblKeyMapper nblKeyMapper;

    @Context
    private org.apache.cxf.jaxrs.ext.MessageContext mc;
    @Context
    private ServletContext sc;

    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    @Path("/locate")
    public Map<String, Object> sendService(Map<String, Object> msg,
                                           @Context javax.servlet.http.HttpServletRequest request) {
        try {
            msg.put(correlationIdK, IdGenerator.generateId());
            NDC.push((String) msg.get(correlationIdK));
            return processRequest(msg, request);
        } finally {
            NDC.pop();
        }
    }

    private Map<String, Object> processRequest(Map<String, Object> msg, HttpServletRequest request) {
        Map<String, Object> respNbl = null;
        try {

            logger.debug("Received Request [{}]", msg);
            Map<String, Object> newMsg = nblKeyMapper.convert(msg, FROM_NBL_PRAM);
            logger.debug("Received Converted Request [{}]", newMsg);
            addHostDetails(newMsg, request);
            logger.debug("Received request from host [{}]", newMsg.get(remotehostK));
            Map<String, Object> flowExeResult = lbsAoChannel.send(newMsg);
            logger.info("Response after executing LBS workflow[{}]", flowExeResult);
            respNbl = nblKeyMapper.convert(flowExeResult, TO_NBL_PARAM);

            logger.debug("Sending Converted response [{}]", respNbl);

        } catch (SdpException ex) {
            logger.error("An exception occurred: [{}]", ex);
            respNbl = new HashMap<String, Object>();
            respNbl.put(NblKeyMapper.statusCode, ex.getErrorCode());
            respNbl.put(NblKeyMapper.statusDetail, ex.getErrorDescription());
        }
        return respNbl;
    }

    private void addHostDetails(Map<String, Object> msg, HttpServletRequest request) {
        if (null == msg.get(remotehostK)) {
            String host = SystemUtil.findHostIp(request);
            if (null != host) {
                msg.put(remotehostK, host);
            }
        }
    }

    public void setLbsAoChannel(Channel lbsAoChannel) {
        this.lbsAoChannel = lbsAoChannel;
    }

    public void init() {
        nblKeyMapper = new NblKeyMapper();
        nblKeyMapper.setNewNblApiKeyMapper(nblLbsKeyMapper);
        logger.info("LBS endpoint started.");
    }

    public void setNblLbsKeyMapper(Map<String, String> newNblLbsKeyMapper) {
        this.nblLbsKeyMapper = newNblLbsKeyMapper;
    }
}
