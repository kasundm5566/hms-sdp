#!/bin/sh

curl -v -H 'Content-Type:application/json' -X POST -d \
'{"address": "tel:254709725895"}' http://core.cms:4287/cms/cms-service/query/download-requests

echo

### Response : {"statusCode":"S1000","result":[{"_id":"1312071712270040","status":"charging-requested","applicationId":"APP_000143","requestedDate":"Jul 17, 2012 12:27:01 PM","paymentInstructions":"Please access Equity Bank Easy Pay menu and enter following when prompted: Business no:111111, Reference ID:121339, Amount:Ksh11. Please pay within 2 days."},{"_id":"1312071712130028","status":"wap-push-delivered","applicationId":"APP_000142","requestedDate":"Jul 17, 2012 12:13:00 PM","wapUrl":"https://dev.sdp.hsenidmobile.com/cms/download-file?id=1312071712130028"}],"statusDescription":"Success"} 
