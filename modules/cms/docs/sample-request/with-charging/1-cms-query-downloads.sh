#!/bin/sh

curl -v -H 'Content-Type:application/json' -X POST -d \
'{"applicationId": "APP_000143"}' http://core.cms:4287/cms/cms-service/query/downloads

echo

### Response - {"statusCode":"S1000","result":{"platforms":{"Android":{"2.1":[{"contentId":"1312051410120002","version":""}],"2.2":[{"contentId":"1312051410120003","version":"v2.0"}],"3.0":[{"contentId":"1312051410120003","version":"v2.0"}],"3.1":[{"contentId":"1312051410120003","version":"v2.0"}],"4.0":[{"contentId":"1312051410120003","version":"v2.0"}]}},"devices":{"HTC":{"My Touch 3G":[{"contentId":"1312051410120003","version":"v2.0"}],"My Touch 4G":[{"contentId":"1312051410120002","version":""}]},"Samsung":{"Galaxy Nexus":[{"contentId":"1312051410120003","version":"v2.0"}],"Galaxy S":[{"contentId":"1312051410120003","version":"v2.0"}],"Galaxy S II":[{"contentId":"1312051410120002","version":""}],"Galaxy Y":[{"contentId":"1312051410120003","version":"v2.0"}]}}},"statusDescription":"Success"}
