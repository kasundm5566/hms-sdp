#!/bin/sh

curl -v -H 'Content-Type:application/json' -X POST -d \
'{"applicationId":"APP_000143",
"contentId":"1312051410120002",
"address":"tel:254709725895"}' http://core.cms:4287/cms/cms-service/download

echo

### Response - {"statusCode":"P1501","statusDescription":"Select a payment instrument to do the charging","paymentInstrumentList":[{"name":"Equity Bank","type":"ASYNC"},{"name":"M-Pesa","type":"ASYNC"},{"name":"Mobile Account","type":"ASYNC"}],"downloadRequestId":"1312071712270040"}
