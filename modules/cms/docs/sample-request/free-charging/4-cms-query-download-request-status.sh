#!/bin/sh

curl -v -H 'Content-Type:application/json' -X POST -d \
'{"applicationId": "APP_000142", "address": "tel:254709725895"}' http://core.cms:4287/cms/cms-service/query/download-request-status

echo

### Response : {"statusCode":"S1000","result":{"_id":"1312071712130028","status":"wap-push-delivered","applicationId":"APP_000142","requestedDate":"Jul 17, 2012 12:13:00 PM","wapUrl":"https://dev.sdp.hsenidmobile.com/cms/download-file?id=1312071712130028"},"statusDescription":"Success"}
