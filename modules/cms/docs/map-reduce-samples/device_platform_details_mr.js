// application content details map-reduce
use cms;

db.temp.drop();
db.temp2.drop();

m1 = function () {
    var id = this["app-file-id"];
    var version = this["version"];
    var devices = this["supported-devices"];
    var appId = this["app-id"];
    var platform = this["platform-name"];
    //----- Extract devices and models
    devices.forEach(function (device) {
        var brand = device.brand;
        device.models.forEach(function (model) {            
            emit({
                "appId" : appId,
                "ctype" : "brand",
                "cdata": {
                brand: brand,
                model: model }
            }, {
                content: [{
                    "content-id": id,
                    version: version
                }]
            })
        })
    })
  //------- Extract platforms
    var platform = this["platform-name"];
    var platformVersions = this["platform-versions"];
    platformVersions.forEach(function (platformVersion) {
        emit({
            "appId" : appId,
            "ctype" : "platform",
            "cdata": {
            platform: platform,
            platformVersion: platformVersion }
        }, {
            content: [{
                "content-id": id,
                version: version
            }]
        })
    })

};

r1 = function (key, values) {
    var content = [];
    var maxIdIndex = 0;
    var maxValuesIndex = 0;
    var max = 0;
    for (var i = 0; i < values.length; i++) {
        for (var j = 0; j < values[i].content.length; j++) {
            var current = parseFloat(values[i].content[j].content-id);
            if (max < current) {
                maxValuesIndex = i;
                maxIdIndex = j;
                max = current
            }
        }

    }
    content.push({
        "content-id": values[maxValuesIndex].content[maxIdIndex].content-id,
        version: values[maxValuesIndex].content[maxIdIndex].version
    });
    return {
        content: content
    }

};

m2 = function () {
    var ctype = this._id.ctype;
    var cdata = this._id.cdata;
    var appId = this._id.appId
    var content = this.value.content;

    emit(appId, {
        "ctype" : ctype, 
        cdetails: [{
            cdata: cdata,
            content: content
        }]
    })
};

r2 = function (key, values) {
    var allModels = [];
    for (var i = 0; i < values.length; i++) for (var j = 0; j < values[i].cdetails.length; j++) allModels.push({
        ctype: values[i].ctype,
        cdata: values[i].cdetails[j].cdata,
        content: values[i].cdetails[j].content
    });
    return {
        models: allModels
    }

};

f = function (key, value) {
    var models = {};
    var platforms = {};
    for (var i = 0; i < value.models.length; i++) {
        var ctype = value.models[i].ctype;
        
        if(ctype == "brand"){
          var brand = value.models[i].cdata.brand
          if(models[brand] == undefined){ 
	      models[brand] = []
          }
          models[brand].push( { "model": value.models[i].cdata.model , "content" : value.models[i].content });     
        } 

        if(ctype == "platform"){
          var platform = value.models[i].cdata.platform
          if(platforms[platform]  == undefined){
        	platforms[platform] = []
          }

          platforms[platform].push( { "version": value.models[i].cdata.platformVersion , "content" : value.models[i].content });  
        }
         
    }

    return  {"devices": models, "platforms": platforms } ;
};

res1 = db.build_file.mapReduce(m1, r1, { out : "temp", query : {$or:[{"app-id" : "APP_000918"}, {"app-id": "APP_000850"}]} } );

res2 = db.temp.mapReduce(m2, r2, { out : "temp2" , "finalize" : f } );

db.temp.find()

db.temp2.find()


// Use http://jsbin.com/ujumus/11 to made this to single line

