package hms.cms.service.impl;

import hms.cms.service.DownloadUrlSender;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static hms.cms.common.CmsKeyBox.*;
import static hms.cms.service.CmsServiceRegistry.cmsSdpService;
import static hms.kite.util.KiteKeyBox.addressK;

/**
 * <p>
 *     Sms Download URL Sender.
 * </p>
 *
 * @author Manuja
 */
public class SmsDownloadUrlSender implements DownloadUrlSender {

    private String message;

    @Override
    public Map<String, Object> sendDownloadUrl(Map<String, Object> request, String downloadUrl) {
        Map<String, Object> sdpRequest = new HashMap<String, Object>();
        sdpRequest.put(messageK, MessageFormat.format(message, downloadUrl));
        sdpRequest.put(destinationAddressesK, Arrays.asList(request.get(addressK)));
        return cmsSdpService().sendSms(sdpRequest);
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
