/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.repo.impl;

import hms.cms.repo.CmsInternalRepository;
import hms.cms.repo.CmsInternalRepositoryService;

import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CmsInternalRepositoryServiceImpl implements CmsInternalRepositoryService {

    private CmsInternalRepository cmsInternalRepository;

    @Override
    public Map<String, Object> findById(String id) {
        return cmsInternalRepository.findById(id);
    }

    @Override
    public List<Map<String, Object>> getExpiredLinks(Map<String, Object> query, int skip, int limit) {
        return cmsInternalRepository.getExpiredLinks(query, skip, limit);
    }

    @Override
    public void createDownload(Map<String, Object> download) {
        cmsInternalRepository.createDownload(download);
    }

    @Override
    public void updateDownload(Map<String, Object> download) {
        cmsInternalRepository.updateDownload(download);
    }

    public void setCmsInternalRepository(CmsInternalRepository cmsInternalRepository) {
        this.cmsInternalRepository = cmsInternalRepository;
    }
}
