/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.repo.impl;

import hms.cms.repo.CmsInfindoRepository;
import hms.cms.repo.CmsInfindoRepositoryService;

import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CmsInfindoRepositoryServiceImpl implements CmsInfindoRepositoryService {

    private CmsInfindoRepository cmsInfindoRepository;

    @Override
    public Map<String, Object> getDownloadSupportInfoByAppId(Map<String, Object> request) {
        return cmsInfindoRepository.getDownloadSupportInfoByAppId(request);
    }

    @Override
    public Map<String, Object> getDownloadDetailsByContentId(String contentId) {
        return cmsInfindoRepository.getDownloadDetailsByContentId(contentId);
    }

    public void setCmsInfindoRepository(CmsInfindoRepository cmsInfindoRepository) {
        this.cmsInfindoRepository = cmsInfindoRepository;
    }
}
