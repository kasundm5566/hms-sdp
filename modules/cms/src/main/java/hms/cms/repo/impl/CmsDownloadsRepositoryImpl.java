package hms.cms.repo.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import hms.cms.repo.CmsDownloadsRepository;
import hms.kite.util.KiteKeyBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kasun on 3/22/18.
 */
public class CmsDownloadsRepositoryImpl implements CmsDownloadsRepository {

    private static final Logger logger = LoggerFactory.getLogger(CmsDownloadsRepositoryImpl.class);

    private static final String COLLECTION_NAME = "downloads";
    private static final String DOWNLOAD_COUNT_FIELD = "downloads-count";

    private MongoTemplate mongoTemplate;

    /**
     * This method is to find the number of downloads of a particular application.
     *
     * @param appId Application id of the downloaded application.
     * @return Number of downloads for the given application.
     */
    @Override
    public int getDownloadsCount(String appId) {
        DBObject dbObject = mongoTemplate.getCollection(COLLECTION_NAME).findOne(new BasicDBObject(KiteKeyBox.idK, appId));
        int downloadsCount = dbObject != null ? Integer.parseInt(dbObject.get("downloads-count").toString()) : 0;
        logger.debug("downloads count for the app [{}]: {}", appId, downloadsCount);
        return downloadsCount;
    }

    /**
     * This method is to increment the downloads count after a successful app download.
     *
     * @param appId Application id of the downloaded application.
     */
    @Override
    public void incrementDownloadsCount(String appId) {
        DBObject dbObject = mongoTemplate.getCollection(COLLECTION_NAME).findOne(new BasicDBObject(KiteKeyBox.idK, appId));
        Map<String, Object> downloadsMap = new HashMap<>();

        int updatedDownloadsCount;
        updatedDownloadsCount = dbObject != null ? Integer.parseInt(dbObject.get("downloads-count").toString()) + 1 : 1;

        downloadsMap.put(KiteKeyBox.idK, appId);
        downloadsMap.put(DOWNLOAD_COUNT_FIELD, updatedDownloadsCount);
        mongoTemplate.getCollection(COLLECTION_NAME).save(new BasicDBObject(downloadsMap));
        logger.debug("incremented downloads count of the app [{}]. new downloads count: {}.", appId, updatedDownloadsCount);
    }

    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
}
