/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.repo;

import hms.kite.datarepo.BuildFileRepositoryService;
import hms.kite.datarepo.ThrottlingRepositoryService;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public final class CmsRepositoryServiceRegistry {

    private static CmsRequestRepositoryService cmsRequestRepositoryService;
    private static CmsInfindoRepositoryService cmsInfindoRepositoryService;
    private static CmsInternalRepositoryService cmsInternalRepositoryService;
    private static BuildFileRepositoryService buildFileRepositoryService;
    private static ThrottlingRepositoryService throttlingRepositoryService;
    private static CmsDownloadsRepositoryService cmsDownloadsRepositoryService;

    static {
        setCmsRequestRepositoryService(new NullObject<CmsRequestRepositoryService>().get());
        setCmsInfindoRepositoryService(new NullObject<CmsInfindoRepositoryService>().get());
        setCmsInternalRepositoryService(new NullObject<CmsInternalRepositoryService>().get());
        setBuildFileRepositoryService(new NullObject<BuildFileRepositoryService>().get());
        setThrottlingRepositoryService(new NullObject<ThrottlingRepositoryService>().get());
        setCmsDownloadsRepositoryService(new NullObject<CmsDownloadsRepositoryService>().get());
    }

    public static CmsRequestRepositoryService cmsRequestRepositoryService() {
        return cmsRequestRepositoryService;
    }

    private static void setCmsRequestRepositoryService(CmsRequestRepositoryService cmsRequestRepositoryService) {
        CmsRepositoryServiceRegistry.cmsRequestRepositoryService = cmsRequestRepositoryService;
    }

    public static CmsInfindoRepositoryService cmsInfindoRepositoryService() {
        return cmsInfindoRepositoryService;
    }

    private static void setCmsInfindoRepositoryService(CmsInfindoRepositoryService cmsInfindoRepositoryService) {
        CmsRepositoryServiceRegistry.cmsInfindoRepositoryService = cmsInfindoRepositoryService;
    }

    public static CmsInternalRepositoryService cmsInternalRepositoryService() {
        return cmsInternalRepositoryService;
    }

    private static void setCmsInternalRepositoryService(CmsInternalRepositoryService cmsInternalRepositoryService) {
        CmsRepositoryServiceRegistry.cmsInternalRepositoryService = cmsInternalRepositoryService;
    }

    public static BuildFileRepositoryService buildFileRepositoryService() {
        return buildFileRepositoryService;
    }

    private static void setBuildFileRepositoryService(BuildFileRepositoryService buildFileRepositoryService) {
        CmsRepositoryServiceRegistry.buildFileRepositoryService = buildFileRepositoryService;
    }

    public static ThrottlingRepositoryService throttlingRepositoryService() {
        return throttlingRepositoryService;
    }

    private static void setThrottlingRepositoryService(ThrottlingRepositoryService throttlingRepositoryService) {
        CmsRepositoryServiceRegistry.throttlingRepositoryService = throttlingRepositoryService;
    }

    public static CmsDownloadsRepositoryService cmsDownloadsRepositoryService() {
        return cmsDownloadsRepositoryService;
    }

    private static void setCmsDownloadsRepositoryService(CmsDownloadsRepositoryService cmsDownloadsRepositoryService) {
        CmsRepositoryServiceRegistry.cmsDownloadsRepositoryService = cmsDownloadsRepositoryService;
    }

    private static class NullObject<T> {

        public T get() {
            return null;
        }
    }
}
