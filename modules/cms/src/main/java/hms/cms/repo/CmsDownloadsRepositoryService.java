package hms.cms.repo;

/**
 * Created by kasun on 3/22/18.
 */
public interface CmsDownloadsRepositoryService {
    int getDownloadsCount(String appId);

    void incrementDownloadsCount(String appId);
}
