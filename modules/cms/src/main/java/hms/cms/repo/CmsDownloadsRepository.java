package hms.cms.repo;

/**
 * Created by kasun on 3/22/18.
 */
public interface CmsDownloadsRepository {
    int getDownloadsCount(String appId);

    void incrementDownloadsCount(String appId);
}
