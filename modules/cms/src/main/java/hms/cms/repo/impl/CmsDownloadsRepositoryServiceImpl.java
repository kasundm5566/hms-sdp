package hms.cms.repo.impl;

import hms.cms.repo.CmsDownloadsRepository;
import hms.cms.repo.CmsDownloadsRepositoryService;

/**
 * Created by kasun on 3/22/18.
 */
public class CmsDownloadsRepositoryServiceImpl implements CmsDownloadsRepositoryService {

    private CmsDownloadsRepository cmsDownloadsRepository;

    @Override
    public int getDownloadsCount(String appId) {
        return cmsDownloadsRepository.getDownloadsCount(appId);
    }

    @Override
    public void incrementDownloadsCount(String appId) {
        cmsDownloadsRepository.incrementDownloadsCount(appId);
    }

    public void setCmsDownloadsRepository(CmsDownloadsRepository cmsDownloadsRepository) {
        this.cmsDownloadsRepository = cmsDownloadsRepository;
    }
}
