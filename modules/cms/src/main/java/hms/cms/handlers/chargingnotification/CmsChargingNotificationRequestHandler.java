/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.handlers.chargingnotification;

import hms.cms.handlers.CmsAbstractRequestHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.cms.common.CmsKeyBox.*;
import static hms.cms.common.CmsKeyBox.statusCodeK;
import static hms.cms.repo.CmsRepositoryServiceRegistry.cmsRequestRepositoryService;
import static hms.cms.service.CmsServiceRegistry.getProperty;
import static hms.kite.util.KiteKeyBox.cmsContentInfoK;
import static hms.kite.util.KiteKeyBox.idK;
import static hms.kite.util.KiteKeyBox.statusK;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 * <p/>
 * Handles all the asynchronous charging notifications
 */
public class CmsChargingNotificationRequestHandler extends CmsAbstractRequestHandler {

    private static final Logger logger = LoggerFactory.getLogger(CmsChargingNotificationRequestHandler.class);

    private Map<String, Object> processChargingNotification(Map<String, Object> request) {
        Map<String, Object> downloadRequest = cmsRequestRepositoryService().findByRequestId((String) request.get(externalTrxIdK));

        logger.debug("Download request content for received charging notification [{}]", downloadRequest);

        Map<String, Object> response = new HashMap<String, Object>();

        if (paymentSuccessPartialSuccessCode.equals(request.get(statusCodeK))) {
            downloadRequest.put(statusK, eligibleK);
            downloadRequest.put(chargingStatusCodeK, request.get(statusCodeK));
            downloadRequest.put(chargingStatusDescriptionsK, request.get(statusDetailK));
            cmsRequestRepositoryService().updateRequest(request);

            response.put(statusCodeK, successCode);
            response.put(statusDetailK, "Success");

            createDownloadDetail(downloadRequest, (Map<String, Object>) downloadRequest.get(cmsContentInfoK));

            sendDownloadUrl(downloadRequest, createDownloadUrl(downloadRequest.get(idK)));

            return response;
        }

        response.put(statusCodeK, cmsSystemErrorErrorCode);
        response.put(statusDetailK, "Unable to process your request");

        return response;
    }

    private Map<String, Object> validateRequest(Map<String, Object> request) {
        Map<String, Object> response = new HashMap<String, Object>();

        if (!request.containsKey(externalTrxIdK)) {
            logger.error("CMS charging notification request doesn't contain {}... [{}]", externalTrxIdK, request);
            response.put(statusCodeK, cmsExternalTransactionIdErrorCode);
            response.put(statusDetailK, "External Transaction ID is not found withing the request");
        } else if (!request.containsKey(statusCodeK)) {
            logger.error("CMS charging notification request doesn't contain {}... [{}]", statusCodeK, request);
            response.put(statusCodeK, cmsStatusCodeErrorCode);
            response.put(statusDetailK, "Status Code is not found withing the request");
        } else if (!request.containsKey(referenceIdK)) {
            logger.error("CMS charging notification request doesn't contain {}... [{}]", referenceIdK, request);
            response.put(statusCodeK, cmsReferenceIdErrorCode);
            response.put(statusDetailK, "Reference ID is not found withing the request");
        } else {
            logger.info("Request is validated successfully {}", request);
            response.put(statusCodeK, cmsSuccessCode);
            response.put(statusDetailK, "Request is validated successfully");
        }

        return response;
    }

    @Override
    public Map<String, Object> handleRequest(Map<String, Object> request) {
        Map<String, Object> response = validateRequest(request);

        if (cmsSuccessCode.equals(response.get(statusCodeK))) {
            response = processChargingNotification(request);
        }

        return response;
    }
}
