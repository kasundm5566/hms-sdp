/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.handlers.charge;

import com.google.common.base.Optional;
import com.google.common.collect.Maps;
import hms.cms.common.CmsKeyBox;
import hms.cms.handlers.CmsAbstractRequestHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static hms.cms.common.CmsKeyBox.*;
import static hms.cms.repo.CmsRepositoryServiceRegistry.cmsRequestRepositoryService;
import static hms.cms.service.CmsServiceRegistry.getProperty;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 * <p/>
 * Handles all the download charge requests
 */
public class CmsDownloadChargeRequestHandler extends CmsAbstractRequestHandler {

    private static final Logger logger = LoggerFactory.getLogger(CmsDownloadChargeRequestHandler.class);

    private Map<String, String> additionalParamsForNbl = Maps.newHashMap();

    private Map<String, Object> processChargeRequest(Map<String, Object> request) {
        Map<String, Object> downloadRequest = cmsRequestRepositoryService().findByRequestId((String) request.get(downloadRequestIdK));

        if (downloadRequest == null) {
            Map<String, Object> response = new HashMap<String, Object>();
            response.put(statusCodeK, cmsDownloadRequestNotFoundErrorCode);
            response.put(statusDetailK, "No download request found for given Download Request ID");

            return response;
        }

        logger.debug("Download request content for received charge request [{}]", downloadRequest);

        if (!acceptedK.equals(downloadRequest.get(statusK))) {
            Map<String, Object> response = new HashMap<String, Object>();
            response.put(statusCodeK, cmsChargingRequestedErrorCode);
            response.put(statusDetailK, "Charging request is already received");

            return response;
        }

        final Object paymentInstrumentName = request.get(paymentInstrumentNameK);


        Map<String, Object> nblCasRequestAdditionalParams = Maps.newHashMap();

        final Optional<Object> additionalParamsOpt = Optional.fromNullable(request.get(downloadRequestAdditionalParamsK));
        if(additionalParamsOpt.isPresent()) {
            if(additionalParamsOpt.get() instanceof Map) {
                try {
                    final Map<String, Object> additionalParamsFromRequest = (Map<String, Object>) additionalParamsOpt.get();
                    nblCasRequestAdditionalParams.putAll(additionalParamsForNbl);
                    nblCasRequestAdditionalParams.putAll(additionalParamsFromRequest);
                } catch (Exception e) {
                    // skip adding additional params
                }
            }
        }

        downloadRequest.put(paymentInstrumentNameK, paymentInstrumentName);
        downloadRequest.put(currencyK, getProperty("cms.system.currency"));
        downloadRequest.put(statusK, chargingPendingK);
        cmsRequestRepositoryService().updateRequest(downloadRequest);

        return sendCaasRequest((String) paymentInstrumentName, downloadRequest, nblCasRequestAdditionalParams);
    }

    private Map<String, Object> validateRequest(Map<String, Object> request) {
        Map<String, Object> response = new HashMap<String, Object>();

        if (!request.containsKey(downloadRequestIdK)) {
            logger.error("CMS charge request doesn't contain {}... [{}]", downloadRequestIdK, request);
            response.put(statusCodeK, cmsDownloadRequestIdErrorCode);
            response.put(statusDetailK, "Download Request ID is not found withing the request");
        } else if (!request.containsKey(paymentInstrumentNameK)) {
            logger.error("CMS charge request doesn't contain {}... [{}]", paymentInstrumentNameK, request);
            response.put(statusCodeK, cmsPaymentInstrumentNameErrorCode);
            response.put(statusDetailK, "Selected PI is not found withing the request");
        } else {
            logger.info("Request is validated successfully {}", request);
            response.put(statusCodeK, cmsSuccessCode);
            response.put(statusDetailK, "Request is validated successfully");
        }

        return response;
    }

    @Override
    public Map<String, Object> handleRequest(Map<String, Object> request) {
        Map<String, Object> response = validateRequest(request);

        if (cmsSuccessCode.equals(response.get(statusCodeK))) {
            response = processChargeRequest(request);
        }

        return response;
    }

    public void setAdditionalParamsForNbl(Map<String, String> additionalParamsForNbl) {
        this.additionalParamsForNbl = additionalParamsForNbl;
    }
}
