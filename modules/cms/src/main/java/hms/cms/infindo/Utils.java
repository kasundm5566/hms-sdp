/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.infindo;

import hms.cms.common.Http;
import hms.commons.IdGenerator;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class Utils {

    public static final String APP                  = "APP_9";
    public static final String CON                  = "CON_9";
    public static final String SCREENSHOT_1         = "_screenshot_1.jpg";
    public static final String SCREENSHOT_2         = "_screenshot_2.jpg";
    public static final String JPG                  = ".jpg";
    public static final String BASE_IMEGE_URL       = "images/applications/icons/";
    public static final String FULL_IMEGE_URL       = "/home/mazeem/images/applications/icons/";

    public static final String CMS_DOWNLOAD         = "download_details.json";
    public static final String APPSTORE_APPS        = "apps.json";
    public static final String KITE_APPS            = "app.json";
    public static final String NCS_SLAS             = "ncs_sla.json";
    public static final String FILE_PATH            = "/home/mazeem/data/{0}";

    public static final String TYPE_SCREENSHOT      = "type-screenshot";
    public static final String TYPE_ICON            = "type-icon";

    public void downloadImages(String imageUrl, String appName, String type, int count) throws IOException {

        InputStream inputStream = Http.getInputStream(imageUrl);

        String imageFullPath;

        if (TYPE_ICON.equals(type)) {
            imageFullPath = createFullImageUrl(appName, JPG);
        } else if (TYPE_SCREENSHOT.equals(type) && count == 1) {
            imageFullPath = createFullImageUrl(appName, SCREENSHOT_1);
        } else {
            imageFullPath = createFullImageUrl(appName, SCREENSHOT_2);
        }

        System.out.println("Image Full Path: " + imageFullPath);

        File file = new File(imageFullPath);
        if (!file.exists()) {
            file.getParentFile().mkdir();
            file.createNewFile();
        }

        BufferedImage bufferedImage = ImageIO.read(inputStream);
        try {
            ImageIO.write(bufferedImage, "jpg", file);
        } catch (Exception ignored) {

        }
    }

    public void cmsDownloadDetails(InfindoApp infindoApp, List<String> downloadDetails) {
        Map<String, Map<String, String>> supportedDeviceMap = infindoApp.getSupportedDeviceMap();
        Set<String> keySet = supportedDeviceMap.keySet();
        for (String key : keySet) {
            Map<String, String> modelMap = supportedDeviceMap.get(key);
            Set<String> modelMapKeySet = modelMap.keySet();
            for (String modelKey : modelMapKeySet) {
                downloadDetails.add(cmsDownloadDetails(infindoApp.getId(), infindoApp.getName(), key, modelKey, modelMap.get(modelKey)));
            }
        }
    }

    public void cmsDownloadDetails(InfindoApp infindoApp) {
        Map<String, Map<String, String>> supportedDeviceMap = infindoApp.getSupportedDeviceMap();
        Set<String> keySet = supportedDeviceMap.keySet();
        for (String key : keySet) {
            Map<String, String> modelMap = supportedDeviceMap.get(key);
            Set<String> modelMapKeySet = modelMap.keySet();
            for (String modelKey : modelMapKeySet) {
                String line = cmsDownloadDetails(infindoApp.getId(), infindoApp.getName(), key, modelKey, modelMap.get(modelKey));
                writeToFile(line, CMS_DOWNLOAD);
            }
        }
    }

    private String cmsDownloadDetails(String itemId, String appName, String vendor, String model, String wapUrl) {
        String appId = createAppId(APP, itemId);

        String conId = IdGenerator.generateId();

        StringBuilder sb = new StringBuilder();

        sb.append("{\"_id\":\"").append(conId).append("\",")
                .append("\"app-id\" : \"").append(appId).append("\",")
                .append("\"content-id\" : \"").append(conId).append("\",")
                .append("\"description\" : \"").append("Virtual City Download Application").append("\",")
                .append("\"vendor\" : \"").append(vendor).append("\",")
                .append("\"model\" : \"").append(model).append("\",")
                .append("\"wapUrl\" : \"").append(wapUrl).append("\",")
                .append("\"message\" : \"").append(appName).append("\",")
                .append("\"sender-address\" : \"").append("hewani").append("\"}");

        return sb.toString();
    }

    public String kiteNcsSla(InfindoApp infindoApp) {
        String appId = createAppId(APP, infindoApp.getId());
        StringBuilder sb = new StringBuilder();

        sb.append("{\"status\":\"active-production\",")
                .append("\"updated-by\":\"hewani\",")
                .append("\"created-by\":\"hewani\",")
                .append("\"app-id\" : \"").append(appId).append("\",")
                .append("\"dl-app-mo-tps\":\"20\",")
                .append("\"updated-date\": {\"$date\" : 1322123935200},")
                .append("\"ncs-type\":\"downloadable\",")
                .append("\"dl-app-mo-tpd\":\"30\",")
                .append("\"created-date\": {\"$date\" : 1322123935200},")
                .append("\"operator\":\"\"}");

        return sb.toString();
    }

    public String kiteAppMap(InfindoApp infindoApp) {

        String appId = createAppId(APP, infindoApp.getId());
        StringBuilder sb = new StringBuilder();
        sb.append("{").append("\"_id\":").append("\"").append(appId).append("\",")
                .append("\"updated-date\": {\"$date\" : 1322123935200},")
                .append("\"govern\": false,")
                .append("\"mask-number\": false,")
                .append("\"advertise\": false,")
                .append("\"created-date\": {\"$date\" : 1322123935200},")
                .append("\"status\": \"active-production\",")
                .append("\"created-by\": \"hewani\",")
                .append("\"sp-id\": \"SPP_000022\",")
                .append("\"app-request-date\": {\"$date\" : 1322123935200},")
                .append("\"password\": \"password\",")
                .append("\"white-list\": [],")
                .append("\"revenue-share\": \"\",")
                .append("\"description\": \"Download Game Application\",")
                .append("\"name\": \"").append(infindoApp.getName()).append("\",")
                .append("\"ncses\":[{ \"ncs-type\" : \"").append("downloadable").append("\", \"status\" : \"ncs-configured\"}],")
                .append("\"app-id\": \"").append(appId).append("\", ")
                .append("\"allowed-hosts\":[\"127.0.0.1\", \"192.168.0.95\"],")
                .append("\"black-list\":[]}");


        return sb.toString();
    }

    public String appstoreMap(InfindoApp infindoApp) {

        String appId = createAppId(APP, infindoApp.getId());
        StringBuilder sb = new StringBuilder();

        sb.append("{").append("\"_id\":").append("\"").append(appId).append("\",")
                .append("\"labels\":[\"Games\"],")
                .append("\"app_type\":[\"downloadable\"],")
                .append("\"status\":\"Publish\",")
                .append("\"instructions\":{},")
                .append("\"black-list\":[],")
                .append("\"updated-by\":\"sdpadmin\",")
                .append("\"date_added\": 1322132985434,")
                .append("\"white-list\":[],")
                .append("\"app_screenshots\":[{ \"url\" : \"").append(createAppUrl(infindoApp.getName(), SCREENSHOT_1)).append("\", \"caption\" : \"Screen Shot 1\"},")
                .append("{ \"url\" : \"").append(createAppUrl(infindoApp.getName(), SCREENSHOT_2)).append("\", \"caption\" : \"Screen Shot 2\"}],")
                .append("\"created-by\":\"hewani\",")
                .append("\"currency\":\"KSh.\",")
                .append("\"govern\": false,")
                .append("\"sp-id\":\"SPP_000022\",")
                .append("\"ncses\":[{ \"ncs-type\" : \"").append("downloadable").append("\", \"status\" : \"ncs-configured\"}],")
                .append("\"name\":\"").append(infindoApp.getName()).append("\",")
                .append("\"default_cost\":\"0\",")
                .append("\"allowed-hosts\":[],")
                .append("\"ncs-slas\": {\"downloadable\" : {")
                .append("\"status\":\"active-production\",")
                .append("\"updated-by\":\"hewani\",")
                .append("\"created-by\":\"hewani\",")
                .append("\"app-id\" : \"").append(appId).append("\",")
                .append("\"dl-app-mo-tps\":\"20\",")
                .append("\"updated-date\":\"Nov 20, 2011 9:59:38 AM\",")
                .append("\"ncs-type\":\"downloadable\",")
                .append("\"dl-app-mo-tpd\":\"30\",")
                .append("\"created-date\":\"Nov 20, 2011 9:55:03 AM\",")
                .append("\"operator\":\"\"} },")
                .append("\"app-request-date\":\"Nov 20, 2011 9:55:03 AM\",")
                .append("\"app_icon\":\"").append(createAppUrl(infindoApp.getName(), JPG)).append("\",")
                .append("\"updated-date\":\"Nov 20, 2011 9:59:37 AM\",")
                .append("\"revenue-share\":\"\",")
                .append("\"subscription\":false,")
                .append("\"rating\":0,")
                .append("\"short_description\": \"Downloadable Application\",")
                .append("\"detailed_cost_description\": \"\",")
                .append("\"usage\": 0,")
                .append("\"developer\": \"hewani\",")
                .append("\"detailed_cost\": \"This is a Free Application\",")
                .append("\"password\": \"password\",")
                .append("\"description\": \"").append(escapeHtml(infindoApp.getDescription())).append("\",")
                .append("\"advertise\": false,")
                .append("\"created-date\": \"Nov 20, 2011 9:55:03 AM\",")
                .append("\"category\": \"Downloads\",")
                .append("\"remarks\": \"\"}");

        return sb.toString();
    }

    public String escapeHtml(String aString) {
        return aString.replace("\"", "\\\"");
    }

    public String createAppUrl(String appName, String fileName) {
        appName = appName.replace(" ", "_");
        return new StringBuilder().append(BASE_IMEGE_URL)
                .append(appName)
                .append("/")
                .append(appName)
                .append(fileName).toString();
    }

    public String createFullImageUrl(String appName, String fileName) {
        appName = appName.replace(" ", "_");
        return new StringBuilder().append(FULL_IMEGE_URL)
                .append(appName)
                .append("/")
                .append(appName)
                .append(fileName).toString();
    }

    public String createAppId(String prefix, String itemId) {
        int len = 5 - itemId.length();
        String appId = prefix;

        for (int i = 0; i < len; i++) {
            appId = new StringBuilder().append(appId).append("0").toString();
        }
        return new StringBuilder().append(appId).append(itemId).toString();
    }

    public void writeToFile(String line, String fileName) {
        try {
            FileWriter fileWriter = new FileWriter(MessageFormat.format(FILE_PATH, fileName), true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(line);
            bufferedWriter.newLine();
            bufferedWriter.close();
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
