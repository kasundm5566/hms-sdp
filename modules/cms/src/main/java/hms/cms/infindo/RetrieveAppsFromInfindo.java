/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.infindo;

import hms.cms.common.Http;
import org.apache.cxf.jaxrs.client.WebClient;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class RetrieveAppsFromInfindo {

    public static final String getAllItemIdsUrl = "http://m.infindo.com/polygon388/fleximarketDataProvide?channel_id=ids";
    public static final String getAppDetailsUrl = "http://m.infindo.com/polygon388/fleximarketDataProvide?channel_id=appDetail&item_id=";
    public static final String getItemDownloadUrl = "http://m.infindo.com/polygon388/indigoDataProvide?channel_id=getItemDownloadUrl&item_id={0}&device_mode={1}";

    public RetrieveAppsFromInfindo() {

    }

    public static void main(String[] args) throws IOException {

        RetrieveAppsFromInfindo infindo = new RetrieveAppsFromInfindo();
        List<InfindoApp> infindoAppList = new ArrayList<InfindoApp>();

        List<String> itemIdList = infindo.getItemsIds();

        List<String> appstoreApp = new ArrayList<String>();
        List<String> kiteAppMap = new ArrayList<String>();
        List<String> kiteNcsSla = new ArrayList<String>();
        List<String> downloadDetails = new ArrayList<String>();

        for (int i = 0; i < itemIdList.size(); i++) {
            System.out.println(i);
            infindoAppList.add(infindo.getAppDetails(itemIdList.get(i)));
        }

        /*Utils utils = new Utils();

        for(InfindoApp infindoApp : infindoAppList) {
            appstoreApp.add(utils.appstoreMap(infindoApp));
            kiteAppMap.add(utils.kiteAppMap(infindoApp));
            kiteNcsSla.add(utils.kiteNcsSla(infindoApp));
            utils.cmsDownloadDetails(infindoApp, downloadDetails);
        }

        System.out.println("\n\n########################### Appstore Document ###########################\n\n\n");
        for(String string: appstoreApp) {
            System.out.println(string);
        }

        System.out.println("\n\n########################### Kite App Document ###########################\n\n\n");
        for(String string: kiteAppMap) {
            System.out.println(string);
        }

        System.out.println("\n\n########################### Kite NCS SLA Document ###########################\n\n\n");
        for(String string: kiteNcsSla) {
            System.out.println(string);
        }

        System.out.println("\n\n########################### Download CMS ###########################\n\n\n");
        for(String string: downloadDetails) {
            System.out.println(string);
        }

        System.out.println("\n\n########################### InfindoAppDetails ###########################\n\n\n");
        for(InfindoApp infindoApp : infindoAppList) {
            System.out.println(infindoApp.toString());
        }*/

    }

    public List<String> getItemsIds() {
        WebClient webClient = Http.createWebClientPlainText(getAllItemIdsUrl);
        String response = webClient.get(String.class);
        return Arrays.asList(response.split(","));

    }

    public InfindoApp getAppDetails(String itemId) {

        String response;
        try {
            response = Http.get(getAppDetailsUrl + itemId);
        } catch (IOException e) {
            throw new IllegalStateException("IO Exception Occurred" + e);
        }

        InfindoApp infindoApp = new InfindoApp();
        try {
            System.out.println(response);
            Document document = parseXmlFromString(response);
            document.getDocumentElement().normalize();
            NodeList appList = document.getElementsByTagName("app");

            Node appNode = appList.item(0);
            if (appNode.getNodeType() == Node.ELEMENT_NODE) {

                Element appElement = (Element) appNode;
                Element element = (Element) appElement.getElementsByTagName("id").item(0);

                try {
                    infindoApp.setId(element.getChildNodes().item(0).getNodeValue());
                } catch (Exception ignored) {

                }

                try {
                    element = (Element) appElement.getElementsByTagName("name").item(0);
                    infindoApp.setName(element.getChildNodes().item(0).getNodeValue());
                } catch (Exception ignored) {

                }

                try {
                    element = (Element) appElement.getElementsByTagName("category").item(0);
                    infindoApp.setCategory(element.getChildNodes().item(0).getNodeValue());
                } catch (Exception ignored) {

                }

                try {
                    element = (Element) appElement.getElementsByTagName("description").item(0);
                    infindoApp.setDescription(element.getChildNodes().item(0).getNodeValue());
                } catch (Exception ignored) {

                }

                try {
                    element = (Element) appElement.getElementsByTagName("suportedDevice").item(0);
                    infindoApp.setSupportedDevices(element.getChildNodes().item(0).getNodeValue());

                } catch (Exception ignored) {

                }

                try {
                    element = (Element) appElement.getElementsByTagName("iconBig").item(0);
                    infindoApp.setIconBig(element.getChildNodes().item(0).getNodeValue());
                } catch (Exception ignored) {

                }

                try {
                    element = (Element) appElement.getElementsByTagName("screen1").item(0);
                    infindoApp.setScreen1(element.getChildNodes().item(0).getNodeValue());
                } catch (Exception ignored) {

                }

                try {
                    element = (Element) appElement.getElementsByTagName("screen2").item(0);
                    infindoApp.setScreen2(element.getChildNodes().item(0).getNodeValue());
                } catch (Exception ignored) {

                }

                populateDeviceModelMap(infindoApp);

                Utils utils = new Utils();
                utils.writeToFile(utils.appstoreMap(infindoApp), Utils.APPSTORE_APPS);
                utils.writeToFile(utils.kiteAppMap(infindoApp), Utils.KITE_APPS);
                utils.writeToFile(utils.kiteNcsSla(infindoApp), Utils.NCS_SLAS);
                utils.cmsDownloadDetails(infindoApp);
                getInfindoImages(infindoApp);
            }

        } catch (ParserConfigurationException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        } catch (SAXException e) {
            System.out.println(e);
        }
        return infindoApp;
    }

    public String retrieveDownloadUrl(String itemId, String deviceModel) {

        String encodedDeviceMode = "";
        try {
            encodedDeviceMode = URLEncoder.encode(deviceModel.trim(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            System.out.println(e);
        }

        String url = MessageFormat.format(getItemDownloadUrl, itemId, encodedDeviceMode);

        String response;
        try {
            response = Http.get(url);
        } catch (IOException e) {
            throw new IllegalStateException("IO Exception Occurred" + e);
        }
        String output = "";
        System.out.println(response);
        try {
            Document document = parseXmlFromString(response);
            document.getDocumentElement().normalize();
            NodeList downloadXml = document.getElementsByTagName("httpresult");
            Node downloadNode = downloadXml.item(0);
            if (downloadNode.getNodeType() == Node.ELEMENT_NODE) {
                Element appElement = (Element) downloadNode;
                Element element = (Element) appElement.getElementsByTagName("push").item(0);
                output = element.getChildNodes().item(0).getNodeValue();
            }
        } catch (ParserConfigurationException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        } catch (SAXException e) {
            System.out.println(e);
        } catch (Exception ignored) {

        }
        return output;
    }

    public Document parseXmlFromString(String doc) throws ParserConfigurationException, IOException, SAXException {
        return DocumentBuilderFactory
                .newInstance()
                .newDocumentBuilder()
                .parse(new ByteArrayInputStream(doc.getBytes()));
    }

    public void populateDeviceModelMap(InfindoApp infindoApp) {
        Map<String, Map<String, String>> supportedDeviceMap = new HashMap<String, Map<String, String>>();
        List<String> deviceSets = Arrays.asList(infindoApp.getSupportedDevices().split(";"));
        for (String deviceSet : deviceSets) {
            String[] deviceVendorList = deviceSet.split(":");

            Map<String, String> map = new HashMap<String, String>();
            List<String> models = Arrays.asList(deviceVendorList[1].split(","));

            for (String model : models) {
                String url = retrieveDownloadUrl(infindoApp.getId(), model);
                if (!url.equals("")) {
                    map.put(model, url);
                }
            }
            supportedDeviceMap.put(deviceVendorList[0], map);
        }
        infindoApp.setSupportedDeviceMap(supportedDeviceMap);
    }

    public void getInfindoImages(InfindoApp infindoApp) throws IOException {
        Utils utils = new Utils();

        String iconUrl = infindoApp.getIconBig();
        String screen1 = infindoApp.getScreen1();
        String screen2 = infindoApp.getScreen2();
        String appName = infindoApp.getName();
        if (iconUrl != null) {
            utils.downloadImages(iconUrl, appName, Utils.TYPE_ICON, 0);
        }
        if (screen1 != null) {
            utils.downloadImages(screen1, appName, Utils.TYPE_SCREENSHOT, 1);
        }
        if (screen2 != null) {
            utils.downloadImages(screen2, appName, Utils.TYPE_SCREENSHOT, 2);
        }
    }

}
