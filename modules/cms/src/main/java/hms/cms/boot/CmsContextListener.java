/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.boot;

import hms.commons.SnmpLogUtil;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import static hms.cms.service.CmsServiceRegistry.getProperty;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class CmsContextListener implements ServletContextListener {

    public void contextInitialized(ServletContextEvent sce) {
        SnmpLogUtil.log(getProperty("cms.start.snmp.message"));
    }

    public void contextDestroyed(ServletContextEvent sce) {
        SnmpLogUtil.log(getProperty("cms.stop.snmp.message"));
    }
}
