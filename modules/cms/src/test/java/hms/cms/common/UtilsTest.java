/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.common;

import java.util.Date;

import static hms.cms.common.Utils.getExpireDate;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class UtilsTest {

    public static void main(String[] args) {
        Date currentDate = new Date();

        System.out.println("Current time is " + currentDate);

        String minutes = "120";
        Date expireDate = getExpireDate(currentDate, minutesK, minutes);
        System.out.println(minutes + " minutes passed time is " + expireDate);

        String hours = "36";
        expireDate = getExpireDate(currentDate, hoursK, hours);
        System.out.println(hours + " hours passed time is " + expireDate);

        String days = "5";
        expireDate = getExpireDate(currentDate, daysK, days);
        System.out.println(days + " days passed time is " + expireDate);
    }
}
