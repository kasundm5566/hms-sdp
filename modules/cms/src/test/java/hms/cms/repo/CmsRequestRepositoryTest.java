/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.repo;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.cms.common.CmsKeyBox.*;
import static hms.cms.repo.CmsRepositoryServiceRegistry.cmsRequestRepositoryService;
import static org.testng.AssertJUnit.*;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
@ContextConfiguration(locations = {"classpath:cms-test-beans.xml"})
public class CmsRequestRepositoryTest extends AbstractTestNGSpringContextTests {

    @Resource
    @Qualifier("cms.mongo.template")
    MongoTemplate template;

    private final String COLLECTION_NAME = "request";

    @BeforeMethod
    public void setUp() throws Exception {
        template.getCollection(COLLECTION_NAME).drop();
    }

    @AfterMethod
    public void tearDown() {
        template.getCollection(COLLECTION_NAME).drop();
    }

    @Test
    public void testFindByRequestId() {
        Map<String, Object> request1 = new HashMap<String, Object>();
        request1.put(idK, "1312042514090012");
        request1.put(cmsRequestTypeK, "download");
        request1.put(cmsChargingTypeK, "free");
        request1.put(cmsRepositoryK, "internal");
        request1.put(statusK, "eligible");

        Map<String, Object> contentInfoMap1 = new HashMap<String, Object>();
        contentInfoMap1.put(idK, "1312042514090012");
        contentInfoMap1.put(appIdK, "APP_000000");
        contentInfoMap1.put(buildFileIdK, "112042514090012");

        request1.put(cmsContentInfoK, contentInfoMap1);

        Map<String, Object> request2 = new HashMap<String, Object>();
        request2.put(idK, "1312042514090013");
        request2.put(cmsRequestTypeK, "download");
        request2.put(cmsChargingTypeK, "free");
        request2.put(cmsRepositoryK, "internal");
        request2.put(statusK, "eligible");

        Map<String, Object> contentInfoMap2 = new HashMap<String, Object>();
        contentInfoMap2.put(idK, "1312042514090013");
        contentInfoMap2.put(appIdK, "APP_000000");
        contentInfoMap2.put(buildFileIdK, "1312042514090013");

        request2.put(cmsContentInfoK, contentInfoMap2);

        cmsRequestRepositoryService().createRequest(request1);
        cmsRequestRepositoryService().createRequest(request2);

        Map<String, Object> dbRequest = cmsRequestRepositoryService().findByRequestId("1312042514090013");

        assertNotNull(dbRequest);
        assertEquals(request2, dbRequest);
    }

    @Test
    public void testFindByAddress() {
        Map<String, Object> request1 = new HashMap<String, Object>();
        request1.put(idK, "1312042514090012");
        request1.put(applicationIdK, "APP_000000");
        request1.put(cmsRequestTypeK, "download");
        request1.put(cmsChargingTypeK, "free");
        request1.put(cmsRepositoryK, "internal");
        request1.put(statusK, wapPushDeliveredK);
        request1.put(wapUrlNblK, "");
        request1.put(addressK, "tel:254707894561");

        Map<String, Object> contentInfoMap1 = new HashMap<String, Object>();
        contentInfoMap1.put(idK, "1312042514090012");
        contentInfoMap1.put(appIdK, "APP_000000");
        contentInfoMap1.put(buildFileIdK, "112042514090012");

        request1.put(cmsContentInfoK, contentInfoMap1);

        Map<String, Object> request2 = new HashMap<String, Object>();
        request2.put(idK, "1312042514090013");
        request2.put(applicationIdK, "APP_000000");
        request2.put(cmsRequestTypeK, "download");
        request2.put(cmsChargingTypeK, "free");
        request2.put(cmsRepositoryK, "internal");
        request2.put(statusK, chargingPendingK);
        request2.put(addressK, "tel:254707894561");
        request2.put(paymentInstructionsK, "instructions");

        Map<String, Object> contentInfoMap2 = new HashMap<String, Object>();
        contentInfoMap2.put(idK, "1312042514090013");
        contentInfoMap2.put(appIdK, "APP_000000");
        contentInfoMap2.put(buildFileIdK, "1312042514090013");

        request2.put(cmsContentInfoK, contentInfoMap2);

        Map<String, Object> request3 = new HashMap<String, Object>();
        request3.put(idK, "1312042514090014");
        request3.put(applicationIdK, "APP_000000");
        request3.put(cmsRequestTypeK, "download");
        request3.put(cmsChargingTypeK, "free");
        request3.put(cmsRepositoryK, "internal");
        request3.put(statusK, chargingPendingK);
        request3.put(addressK, "tel:254707894563");

        Map<String, Object> contentInfoMap3 = new HashMap<String, Object>();
        contentInfoMap3.put(idK, "1312042514090014");
        contentInfoMap3.put(appIdK, "APP_000000");
        contentInfoMap3.put(buildFileIdK, "1312042514090014");

        request3.put(cmsContentInfoK, contentInfoMap3);

        cmsRequestRepositoryService().createRequest(request1);
        cmsRequestRepositoryService().createRequest(request2);
        cmsRequestRepositoryService().createRequest(request3);

        List<Map<String, Object>> downloadRequests = cmsRequestRepositoryService().findByAddress("tel:254707894561");

        assertNotNull(downloadRequests);
        assertEquals(2, downloadRequests.size());

        Map<String, Object> downloadRequest1 = downloadRequests.get(0);

        assertTrue(downloadRequest1.containsKey(idK));
        assertTrue(downloadRequest1.containsKey(applicationIdK));
        assertTrue(downloadRequest1.containsKey(requestedDateK));
        assertTrue(downloadRequest1.containsKey(statusK));
        assertTrue(downloadRequest1.containsKey(paymentInstructionsK));

        assertFalse(downloadRequest1.containsKey(cmsChargingTypeK));

        Map<String, Object> downloadRequest2 = downloadRequests.get(1);

        assertTrue(downloadRequest2.containsKey(idK));
        assertTrue(downloadRequest2.containsKey(applicationIdK));
        assertTrue(downloadRequest2.containsKey(requestedDateK));
        assertTrue(downloadRequest2.containsKey(statusK));
        assertTrue(downloadRequest2.containsKey(wapUrlNblK));

        assertFalse(downloadRequest2.containsKey(paymentInstructionsK));
        assertFalse(downloadRequest2.containsKey(cmsChargingTypeK));
    }

    @Test
    public void testFindByAppIdAndAddress() {
        Map<String, Object> request1 = new HashMap<String, Object>();
        request1.put(idK, "1312042514090012");
        request1.put(applicationIdK, "APP_000000");
        request1.put(cmsRequestTypeK, "download");
        request1.put(cmsChargingTypeK, "free");
        request1.put(cmsRepositoryK, "internal");
        request1.put(statusK, wapPushDeliveredK);
        request1.put(addressK, "tel:254707894561");

        Map<String, Object> contentInfoMap1 = new HashMap<String, Object>();
        contentInfoMap1.put(idK, "1312042514090012");
        contentInfoMap1.put(appIdK, "APP_000000");
        contentInfoMap1.put(buildFileIdK, "112042514090012");

        request1.put(cmsContentInfoK, contentInfoMap1);

        Map<String, Object> request2 = new HashMap<String, Object>();
        request2.put(idK, "1312042514090013");
        request2.put(applicationIdK, "APP_000000");
        request2.put(cmsRequestTypeK, "download");
        request2.put(cmsChargingTypeK, "free");
        request2.put(cmsRepositoryK, "internal");
        request2.put(statusK, chargingPendingK);
        request2.put(addressK, "tel:254707894561");

        Map<String, Object> contentInfoMap2 = new HashMap<String, Object>();
        contentInfoMap2.put(idK, "1312042514090013");
        contentInfoMap2.put(appIdK, "APP_000000");
        contentInfoMap2.put(buildFileIdK, "1312042514090013");

        request2.put(cmsContentInfoK, contentInfoMap2);

        Map<String, Object> request3 = new HashMap<String, Object>();
        request3.put(idK, "1312042514090014");
        request3.put(applicationIdK, "APP_000000");
        request3.put(cmsRequestTypeK, "download");
        request3.put(cmsChargingTypeK, "free");
        request3.put(cmsRepositoryK, "internal");
        request3.put(statusK, chargingPendingK);
        request3.put(addressK, "tel:254707894563");

        Map<String, Object> contentInfoMap3 = new HashMap<String, Object>();
        contentInfoMap3.put(idK, "1312042514090014");
        contentInfoMap3.put(appIdK, "APP_000000");
        contentInfoMap3.put(buildFileIdK, "1312042514090014");

        request3.put(cmsContentInfoK, contentInfoMap3);

        cmsRequestRepositoryService().createRequest(request1);
        cmsRequestRepositoryService().createRequest(request2);
        cmsRequestRepositoryService().createRequest(request3);

        Map<String, Object> downloadRequest = cmsRequestRepositoryService().findByAppIdAndAddress("APP_000000", "tel:254707894561");

        assertNotNull(downloadRequest);

        assertTrue(downloadRequest.containsKey(idK));
        assertTrue(downloadRequest.containsKey(applicationIdK));
        assertTrue(downloadRequest.containsKey(requestedDateK));
        assertTrue(downloadRequest.containsKey(statusK));

        assertFalse(downloadRequest.containsKey(cmsChargingTypeK));

        assertEquals(request2.get(idK), downloadRequest.get(idK));
    }

    @Test
    public void testCreateRequest() {
        Map<String, Object> request = new HashMap<String, Object>();
        request.put(idK, "1312042514090012");
        request.put(cmsRequestTypeK, "download");
        request.put(cmsChargingTypeK, "free");
        request.put(cmsRepositoryK, "internal");

        Map<String, Object> contentInfoMap = new HashMap<String, Object>();
        contentInfoMap.put(idK, "1312042514090012");
        contentInfoMap.put(appIdK, "APP_000000");
        contentInfoMap.put(buildFileIdK, "112042514090012");

        request.put(cmsContentInfoK, contentInfoMap);

        cmsRequestRepositoryService().createRequest(request);

        Map<String, Object> dbRequest = cmsRequestRepositoryService().findByRequestId("1312042514090012");

        assertNotNull(dbRequest);
        assertEquals(request, dbRequest);
    }

    @Test
    public void testUpdateRequest() {
        Map<String, Object> request = new HashMap<String, Object>();
        request.put(idK, "1312042514090012");
        request.put(cmsRequestTypeK, "download");
        request.put(cmsChargingTypeK, "free");
        request.put(cmsRepositoryK, "internal");
        request.put(statusK, "eligible");

        Map<String, Object> contentInfoMap = new HashMap<String, Object>();
        contentInfoMap.put(idK, "1312042514090012");
        contentInfoMap.put(appIdK, "APP_000000");
        contentInfoMap.put(buildFileIdK, "112042514090012");

        request.put(cmsContentInfoK, contentInfoMap);

        cmsRequestRepositoryService().createRequest(request);

        Map<String, Object> dbRequest = cmsRequestRepositoryService().findByRequestId("1312042514090012");

        assertNotNull(dbRequest);
        assertEquals(request, dbRequest);

        request.put(statusK, "accepted");

        cmsRequestRepositoryService().updateRequest(request);

        dbRequest = cmsRequestRepositoryService().findByRequestId("1312042514090012");

        assertNotNull(dbRequest);
        assertEquals(request, dbRequest);
    }
}
