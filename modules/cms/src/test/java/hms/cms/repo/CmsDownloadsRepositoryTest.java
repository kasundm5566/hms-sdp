package hms.cms.repo;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.annotation.Resource;

import static hms.cms.repo.CmsRepositoryServiceRegistry.cmsDownloadsRepositoryService;
import static org.testng.AssertJUnit.assertEquals;

/**
 * Created by kasun on 3/22/18.
 */
@ContextConfiguration(locations = {"classpath:cms-test-beans.xml"})
public class CmsDownloadsRepositoryTest extends AbstractTestNGSpringContextTests {

    @Resource
    @Qualifier("cms.mongo.template")
    MongoTemplate template;

    private final String COLLECTION_NAME = "downloads";

    @BeforeClass
    public void cleanCollection() {
        template.getCollection(COLLECTION_NAME).drop();
    }

    @AfterClass
    public void tearDown() {
        template.getCollection(COLLECTION_NAME).drop();
    }

    @Test
    public void testGetDownloadsCount() {
        int count = cmsDownloadsRepositoryService().getDownloadsCount("APP_0000001");
        assertEquals(-1, count);
    }

    @Test
    public void testIncrementDownloadsCount(){
        cmsDownloadsRepositoryService().incrementDownloadsCount("APP_0000001");
        int count = cmsDownloadsRepositoryService().getDownloadsCount("APP_0000001");
        assertEquals(1, count);
    }
}
