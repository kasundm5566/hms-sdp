/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.cms.service;

import com.mongodb.BasicDBObject;
import hms.cms.repo.CmsInfindoRepositoryService;
import hms.cms.repo.CmsInternalRepositoryService;
import hms.cms.repo.CmsRepositoryServiceRegistry;
import hms.cms.repo.CmsRequestRepositoryService;
import hms.kite.datarepo.BuildFileRepositoryService;
import hms.kite.datarepo.NcsRepositoryService;
import hms.kite.datarepo.RepositoryServiceRegistry;
import org.hamcrest.Matcher;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import static hms.cms.common.CmsKeyBox.*;
import static hms.cms.service.CmsServiceRegistry.cmsService;
import static hms.kite.util.KiteKeyBox.cmsDevicesK;
import static hms.kite.util.KiteKeyBox.cmsPlatformsK;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.core.AllOf.allOf;
import static org.testng.AssertJUnit.*;

/**
 * Created with IntelliJ IDEA.
 * User: sanjeewa
 * Date: 4/26/12
 * Time: 11:57 AM
 * This will test the CmsServices
 */
@ContextConfiguration(locations = {"classpath:cms-test-beans.xml"})
public class CmsServiceTest extends AbstractTestNGSpringContextTests {

    private CmsSdpService cmsSdpService;
    private CmsRequestRepositoryService cmsRequestRepositoryService;
    private CmsInfindoRepositoryService cmsInfindoRepositoryService;
    private CmsInternalRepositoryService cmsInternalRepositoryService;
    private BuildFileRepositoryService buildFileRepositoryService;
    private NcsRepositoryService ncsRepositoryService;
    private Mockery mockery;
    private String appId;
    private String password;
    private String contentId;
    private String address;

    private Map<String, Object> buildFileResponse() {
        final Map<String, Object> buildFileResponse = new HashMap<String, Object>();
        buildFileResponse.put(supportDevK, Arrays.asList("Galaxy S", "My Touch 3G"));
        buildFileResponse.put(statusK, "pending-approve");
        buildFileResponse.put(buildNameK, "Build1");
        buildFileResponse.put(createdByK, "testcorp");
        buildFileResponse.put(buildAppFileNameK, "test.apk");
        buildFileResponse.put(buildAppFileIdK, contentId);
        buildFileResponse.put(spIdK, "SPP_000001");
        buildFileResponse.put(buildDescK, "description");
        buildFileResponse.put(buildVersionK, "1.0");
        buildFileResponse.put(updatedByK, "testcorp");
        buildFileResponse.put(platformIdK, "android");
        buildFileResponse.put(platformVersionsK, Arrays.asList("2.2"));
        buildFileResponse.put(supportDevTypeK, "specific-devices");
        buildFileResponse.put(appIdK, appId);
        buildFileResponse.put(platformNameK, "Android");
        return buildFileResponse;
    }

    private Map<String, Object> ncsResponse(String chargingType) {
        final Map<String, Object> charging = new HashMap<String, Object>();
        charging.put(typeK, chargingType);
        charging.put(amountK, "10");

        final Map<String, Object> ncsResponse = new HashMap<String, Object>();
        ncsResponse.put(dlAtmptK, 3);
        ncsResponse.put(linkExpTimeK, "10");
        ncsResponse.put(linkExpTimeTypeK, "minutes");
        ncsResponse.put(statusK, "pending-approve");
        ncsResponse.put(createdByK, "testcorp");
        ncsResponse.put(ncsTypeK, "downloadable");
        ncsResponse.put(maxDlPerDayK, "1000");
        ncsResponse.put(operatorK, "");
        ncsResponse.put(maxConDlK, "4");
        ncsResponse.put(appIdK, appId);
        ncsResponse.put(dlRepoK, "internal");
        ncsResponse.put(chargingK, charging);
        return ncsResponse;
    }

    private Map<String, Object> makeContent(String contentId) {
        BasicDBObject content = new BasicDBObject();
        content.put(contentIdK, contentId);
        content.put(versionK, "");

        return content;
    }

    @BeforeMethod
    public void setUp() throws Exception {
        mockery = new Mockery();
        cmsSdpService = mockery.mock(CmsSdpService.class);
        cmsRequestRepositoryService = mockery.mock(CmsRequestRepositoryService.class);
        cmsInfindoRepositoryService = mockery.mock(CmsInfindoRepositoryService.class);
        cmsInternalRepositoryService = mockery.mock(CmsInternalRepositoryService.class);
        buildFileRepositoryService = mockery.mock(BuildFileRepositoryService.class);
        ncsRepositoryService = mockery.mock(NcsRepositoryService.class);

        Method privateVoidSetCmsSdpService = CmsServiceRegistry.class.getDeclaredMethod("setCmsSdpService", CmsSdpService.class);
        privateVoidSetCmsSdpService.setAccessible(true);
        privateVoidSetCmsSdpService.invoke(CmsServiceRegistry.class, cmsSdpService);

        Method privateVoidSetCmsRequestRepositoryService = CmsRepositoryServiceRegistry.class.getDeclaredMethod("setCmsRequestRepositoryService", CmsRequestRepositoryService.class);
        privateVoidSetCmsRequestRepositoryService.setAccessible(true);
        privateVoidSetCmsRequestRepositoryService.invoke(CmsRepositoryServiceRegistry.class, cmsRequestRepositoryService);

        Method privateVoidSetCmsInfindoRepositoryService = CmsRepositoryServiceRegistry.class.getDeclaredMethod("setCmsInfindoRepositoryService", CmsInfindoRepositoryService.class);
        privateVoidSetCmsInfindoRepositoryService.setAccessible(true);
        privateVoidSetCmsInfindoRepositoryService.invoke(CmsRepositoryServiceRegistry.class, cmsInfindoRepositoryService);

        Method privateVoidSetCmsInternalRepositoryService = CmsRepositoryServiceRegistry.class.getDeclaredMethod("setCmsInternalRepositoryService", CmsInternalRepositoryService.class);
        privateVoidSetCmsInternalRepositoryService.setAccessible(true);
        privateVoidSetCmsInternalRepositoryService.invoke(CmsRepositoryServiceRegistry.class, cmsInternalRepositoryService);

        Method privateVoidSetBuildFileRepositoryService = CmsRepositoryServiceRegistry.class.getDeclaredMethod("setBuildFileRepositoryService", BuildFileRepositoryService.class);
        privateVoidSetBuildFileRepositoryService.setAccessible(true);
        privateVoidSetBuildFileRepositoryService.invoke(RepositoryServiceRegistry.class, buildFileRepositoryService);

        Method privateVoidSetNcsRepositoryService = RepositoryServiceRegistry.class.getDeclaredMethod("setNcsRepositoryService", NcsRepositoryService.class);
        privateVoidSetNcsRepositoryService.setAccessible(true);
        privateVoidSetNcsRepositoryService.invoke(RepositoryServiceRegistry.class, ncsRepositoryService);

        appId = "APP_000000";
        password = "password";
        contentId = "132042514090012";
        address = "tel:254709725894";
    }

    @Test
    public void testInvalidDownloadRequestWithNoAppId() {
        Map<String, Object> downloadRequest = new HashMap<String, Object>();
        downloadRequest.put(contentIdK, contentId);
        downloadRequest.put(addressK, address);

        Map<String, Object> response = cmsService().download(downloadRequest);

        assertNotNull(response);
        assertEquals(cmsAppIdErrorCode, response.get(statusCodeK));
        assertEquals("Application ID is not found withing the request", response.get(statusDetailK));
    }

    @Test
    public void testInvalidDownloadRequestWithNoContentId() {
        Map<String, Object> downloadRequest = new HashMap<String, Object>();
        downloadRequest.put(applicationIdK, appId);
        downloadRequest.put(addressK, address);

        Map<String, Object> response = cmsService().download(downloadRequest);

        assertNotNull(response);
        assertEquals(cmsContentIdErrorCode, response.get(statusCodeK));
        assertEquals("Content ID is not found withing the request", response.get(statusDetailK));
    }

    @Test
    public void testInvalidDownloadRequestWithNoAddress() {
        Map<String, Object> downloadRequest = new HashMap<String, Object>();
        downloadRequest.put(applicationIdK, appId);
        downloadRequest.put(contentIdK, contentId);

        Map<String, Object> response = cmsService().download(downloadRequest);

        assertNotNull(response);
        assertEquals(cmsRecipientAddressErrorCode, response.get(statusCodeK));
        assertEquals("Receiver address is not found withing the request", response.get(statusDetailK));
    }

    @Test
    public void testContentNotFoundForInfindoDownload() {
        final Map<String, Object> downloadResponse = null;

        mockery.checking(new Expectations() {
            {
                one(cmsInfindoRepositoryService).getDownloadDetailsByContentId(contentId); will(returnValue(downloadResponse));
                one(buildFileRepositoryService).findByBuildFileId(contentId); will(returnValue(downloadResponse));
            }
        });

        Map<String, Object> downloadRequest = new HashMap<String, Object>();
        downloadRequest.put(applicationIdK, appId);
        downloadRequest.put(passwordK, password);
        downloadRequest.put(contentIdK, contentId);
        downloadRequest.put(addressK, address);

        Map<String, Object> response = cmsService().download(downloadRequest);

        assertNotNull(response);
        assertEquals(cmsContentNotFoundErrorCode, response.get(statusCodeK));
        assertEquals("No content found for given Content ID", response.get(statusDetailK));
        mockery.assertIsSatisfied();
    }

    @Test
    public void testSuccessDownloadRequestForInfindoContent() {
        final Map<String, Object> sdpRequest = new HashMap<String, Object>();
        sdpRequest.put(messageK, "Virtual City");
        sdpRequest.put(wapUrlNblK, "http://m.infindo.com/polygon388/d/0A07Pb");
        sdpRequest.put(destinationAddressesK, Arrays.asList(address));

        final Map<String, Object> sdpResponse = new HashMap<String, Object>();
        sdpResponse.put(statusCodeK, "S1000");

        final Map<String, Object> downloadResponse = new HashMap<String, Object>();
        downloadResponse.put(idK, contentId);
        downloadResponse.put(appIdK, "APP_912202");
        downloadResponse.put(contentIdK, contentId);
        downloadResponse.put(descriptionK, "Download Application");
        downloadResponse.put("vendor", "Huawei");
        downloadResponse.put("model", "U8230");
        downloadResponse.put(wapUrlNblK, "http://m.infindo.com/polygon388/d/0A07Pb");
        downloadResponse.put(messageK, "Virtual City");
        downloadResponse.put(senderAddressK, "hewani");

        mockery.checking(new Expectations() {
            {
                Matcher<Map<String, ?>> createRequest = allOf(
                        hasKey(idK),
                        hasEntry(applicationIdK, appId),
                        hasEntry(passwordK, password),
                        hasEntry(addressK, address),
                        hasEntry(cmsRequestTypeK, downloadRequestK),
                        hasEntry(cmsRepositoryK, infindoDownloadK),
                        hasEntry(cmsChargingTypeK, freeK),
                        hasEntry(statusK, eligibleK),
                        hasKey(cmsContentInfoK)
                );

                Matcher<Map<String, ?>> updateRequest = allOf(
                        hasKey(idK),
                        hasEntry(applicationIdK, appId),
                        hasEntry(passwordK, password),
                        hasEntry(addressK, address),
                        hasEntry(cmsRequestTypeK, downloadRequestK),
                        hasEntry(cmsRepositoryK, infindoDownloadK),
                        hasEntry(cmsChargingTypeK, freeK),
                        hasEntry(statusK, wapPushDeliveredK),
                        hasKey(cmsContentInfoK)
                );

                one(cmsSdpService).sendWapPush(sdpRequest); will(returnValue(sdpResponse));
                one(cmsRequestRepositoryService).createRequest((Map<String, Object>) with(createRequest));
                one(cmsRequestRepositoryService).updateRequest((Map<String, Object>) with(updateRequest));
                one(cmsInfindoRepositoryService).getDownloadDetailsByContentId(contentId); will(returnValue(downloadResponse));
            }
        });

        Map<String, Object> downloadRequest = new HashMap<String, Object>();
        downloadRequest.put(applicationIdK, appId);
        downloadRequest.put(passwordK, password);
        downloadRequest.put(contentIdK, contentId);
        downloadRequest.put(addressK, address);

        Map<String, Object> response = cmsService().download(downloadRequest);

        assertNotNull(response);
        assertEquals(cmsPartialSuccessCode, response.get(statusCodeK));
        assertEquals("You will receive a message shortly", response.get(statusDetailK));
        assertTrue(response.containsKey(wapUrlNblK));
        mockery.assertIsSatisfied();
    }

    @Test
    public void testContentNotFoundForInternalDownload() {
        final Map<String, Object> downloadResponse = null;

        mockery.checking(new Expectations() {
            {
                one(cmsInfindoRepositoryService).getDownloadDetailsByContentId(contentId); will(returnValue(downloadResponse));
                one(buildFileRepositoryService).findByBuildFileId(contentId); will(returnValue(downloadResponse));
            }
        });

        Map<String, Object> downloadRequest = new HashMap<String, Object>();
        downloadRequest.put(applicationIdK, appId);
        downloadRequest.put(passwordK, password);
        downloadRequest.put(contentIdK, contentId);
        downloadRequest.put(addressK, address);

        Map<String, Object> response = cmsService().download(downloadRequest);

        assertNotNull(response);
        assertEquals(cmsContentNotFoundErrorCode, response.get(statusCodeK));
        assertEquals("No content found for given Content ID", response.get(statusDetailK));
        mockery.assertIsSatisfied();
    }

    @Test
    public void testSuccessDownloadRequestForInternalContentFreeCharging() {
        final Map<String, Object> sdpResponse = new HashMap<String, Object>();
        sdpResponse.put(statusCodeK, "S1000");

        final Map<String, Object> downloadResponse = null;

        final Map<String, Object> buildFileResponse = buildFileResponse();

        final Map<String, Object> ncsResponse = ncsResponse(freeK);

        mockery.checking(new Expectations() {
            {
                Matcher<Map<String, ?>> sdpRequest = allOf(
                        hasEntry(messageK, "Virtual City"),
                        hasEntry(destinationAddressesK, Arrays.asList(address)),
                        hasKey(wapUrlNblK)
                );

                Matcher<Map<String, ?>> download = allOf(
                        hasEntry(appIdK, appId),
                        hasEntry(buildFileIdK, contentId),
                        hasKey(idK),
                        hasKey(expireDateK)
                );

                Matcher<Map<String, ?>> request = allOf(
                        hasKey(idK),
                        hasEntry(applicationIdK, appId),
                        hasEntry(passwordK, password),
                        hasEntry(addressK, address),
                        hasEntry(cmsRequestTypeK, downloadRequestK),
                        hasEntry(cmsRepositoryK, internalDownloadK),
                        hasEntry(cmsChargingTypeK, freeK),
                        hasEntry(statusK, eligibleK),
                        hasKey(cmsContentInfoK)
                );

                one(cmsSdpService).sendWapPush((Map<String, Object>) with(sdpRequest)); will(returnValue(sdpResponse));
                one(cmsRequestRepositoryService).createRequest((Map<String, Object>) with(request));
                one(cmsRequestRepositoryService).updateRequest(with(any(HashMap.class)));
                one(cmsInfindoRepositoryService).getDownloadDetailsByContentId(contentId); will(returnValue(downloadResponse));
                one(buildFileRepositoryService).findByBuildFileId(contentId); will(returnValue(buildFileResponse));
                exactly(2).of(ncsRepositoryService).findByAppIdOperatorNcsType(appId, "", downloadableK); will(returnValue(ncsResponse));
                one(cmsInternalRepositoryService).createDownload((Map<String, Object>) with(download));
            }
        });

        Map<String, Object> downloadRequest = new HashMap<String, Object>();
        downloadRequest.put(applicationIdK, appId);
        downloadRequest.put(passwordK, password);
        downloadRequest.put(contentIdK, contentId);
        downloadRequest.put(addressK, address);

        Map<String, Object> response = cmsService().download(downloadRequest);

        assertNotNull(response);
        assertEquals(cmsPartialSuccessCode, response.get(statusCodeK));
        assertEquals("You will receive a message shortly", response.get(statusDetailK));
        assertTrue(response.containsKey(wapUrlNblK));
        mockery.assertIsSatisfied();
    }

    @Test
    public void testUnsuccessfulDownloadRequestForInternalContentFreeCharging() {
        final Map<String, Object> sdpResponse = new HashMap<String, Object>();
        sdpResponse.put(statusCodeK, internalErrorErrorCode);
        sdpResponse.put(statusDetailK, "Internal error occurred while processing request");

        final Map<String, Object> downloadResponse = null;

        final Map<String, Object> buildFileResponse = buildFileResponse();

        final Map<String, Object> ncsResponse = ncsResponse(freeK);

        mockery.checking(new Expectations() {
            {
                Matcher<Map<String, ?>> sdpRequest = allOf(
                        hasEntry(messageK, "Virtual City"),
                        hasEntry(destinationAddressesK, Arrays.asList(address)),
                        hasKey(wapUrlNblK)
                );

                Matcher<Map<String, ?>> download = allOf(
                        hasEntry(appIdK, appId),
                        hasEntry(buildFileIdK, contentId),
                        hasKey(idK),
                        hasKey(expireDateK)
                );

                one(cmsSdpService).sendWapPush((Map<String, Object>) with(sdpRequest)); will(returnValue(sdpResponse));
                one(cmsRequestRepositoryService).createRequest(with(any(HashMap.class)));
                one(cmsInfindoRepositoryService).getDownloadDetailsByContentId(contentId); will(returnValue(downloadResponse));
                one(buildFileRepositoryService).findByBuildFileId(contentId); will(returnValue(buildFileResponse));
                exactly(2).of(ncsRepositoryService).findByAppIdOperatorNcsType(appId, "", downloadableK); will(returnValue(ncsResponse));
                one(cmsInternalRepositoryService).createDownload((Map<String, Object>) with(download));
            }
        });

        Map<String, Object> downloadRequest = new HashMap<String, Object>();
        downloadRequest.put(applicationIdK, appId);
        downloadRequest.put(passwordK, password);
        downloadRequest.put(contentIdK, contentId);
        downloadRequest.put(addressK, address);

        Map<String, Object> response = cmsService().download(downloadRequest);

        assertNotNull(response);
        assertEquals("E", ((String) response.get(statusCodeK)).split("")[1]);
        assertTrue(response.containsKey(statusDetailK));
        mockery.assertIsSatisfied();
    }

    @Test
    public void testSuccessDownloadRequestForInternalContentWithCharging() {
        final Map<String, Object> sdpRequest = new HashMap<String, Object>();
        sdpRequest.put(subscriberIdK, address);
        sdpRequest.put(typeK, allK);

        final Map<String, Object> sdpResponse = new HashMap<String, Object>();
        final Map<String, Object> equity = new HashMap<String, Object>();
        equity.put(nameK, "Equity Bank");
        equity.put(typeK, "async");
        final Map<String, Object> mpesa = new HashMap<String, Object>();
        mpesa.put(nameK, "M-Pesa");
        mpesa.put(typeK, "async");
        sdpResponse.put(statusCodeK, "S1000");
        sdpResponse.put(paymentInstrumentListK, Arrays.asList(equity, mpesa));

        final Map<String, Object> downloadResponse = null;

        final Map<String, Object> buildFileResponse = buildFileResponse();

        final Map<String, Object> ncsResponse = ncsResponse(flatK);

        mockery.checking(new Expectations() {
            {
                Matcher<Map<String, ?>> request = allOf(
                        hasKey(idK),
                        hasEntry(applicationIdK, appId),
                        hasEntry(passwordK, password),
                        hasEntry(addressK, address),
                        hasEntry(cmsRequestTypeK, downloadRequestK),
                        hasEntry(cmsRepositoryK, internalDownloadK),
                        hasEntry(cmsChargingTypeK, flatK),
                        hasEntry(cmsChargingAmountK, "10"),
                        hasEntry(statusK, acceptedK),
                        hasKey(cmsContentInfoK)
                );

                one(cmsInfindoRepositoryService).getDownloadDetailsByContentId(contentId); will(returnValue(downloadResponse));
                one(buildFileRepositoryService).findByBuildFileId(contentId); will(returnValue(buildFileResponse));
                one(ncsRepositoryService).findByAppIdOperatorNcsType(appId, "", downloadableK); will(returnValue(ncsResponse));
                one(cmsRequestRepositoryService).createRequest((Map<String, Object>) with(request));
                one(cmsSdpService).getPiList(with(sdpRequest)); will(returnValue(sdpResponse));
            }
        });

        Map<String, Object> downloadRequest = new HashMap<String, Object>();
        downloadRequest.put(applicationIdK, appId);
        downloadRequest.put(passwordK, password);
        downloadRequest.put(contentIdK, contentId);
        downloadRequest.put(addressK, address);

        Map<String, Object> response = cmsService().download(downloadRequest);

        assertNotNull(response);
        assertEquals(cmsWithPiListPartialSuccessCode, response.get(statusCodeK));
        assertEquals("Select a payment instrument to do the charging", response.get(statusDetailK));
        assertTrue(response.containsKey(paymentInstrumentListK));
        assertTrue(response.containsKey(downloadRequestIdK));
        mockery.assertIsSatisfied();
    }

    @Test
    public void testSuccessDownloadRequestForInternalContentWithChargingWithSinglePayIns() {
        final Map<String, Object> sdpRequest = new HashMap<String, Object>();
        sdpRequest.put(subscriberIdK, address);
        sdpRequest.put(typeK, allK);

        final Map<String, Object> sdpResponse = new HashMap<String, Object>();
        final Map<String, Object> equity = new HashMap<String, Object>();
        equity.put(nameK, "Equity Bank");
        equity.put(typeK, "async");
        sdpResponse.put(statusCodeK, "S1000");
        sdpResponse.put(paymentInstrumentListK, Arrays.asList(equity));

        final Map<String, Object> downloadResponse = null;

        final Map<String, Object> buildFileResponse = buildFileResponse();

        final Map<String, Object> ncsResponse = ncsResponse(flatK);

        mockery.checking(new Expectations() {
            {
                Matcher<Map<String, ?>> request = allOf(
                        hasKey(idK),
                        hasEntry(applicationIdK, appId),
                        hasEntry(passwordK, password),
                        hasEntry(addressK, address),
                        hasEntry(cmsRequestTypeK, downloadRequestK),
                        hasEntry(cmsRepositoryK, internalDownloadK),
                        hasEntry(cmsChargingTypeK, flatK),
                        hasEntry(cmsChargingAmountK, "10"),
                        hasEntry(statusK, chargingPendingK),
                        hasKey(cmsContentInfoK)
                );

                Matcher<Map<String, ?>> sdpCaasRequest = allOf(
                        hasKey(externalTrxIdK),
                        hasEntry(amountK, "10"),
                        hasEntry(paymentInstrumentNameK, "Equity Bank"),
                        hasEntry(subscriberIdK, address),
                        hasEntry(currencyK, "KES")
                );

                Matcher<Map<String, ?>> sdpWapPushRequest = allOf(
                        hasEntry(messageK, "Virtual City"),
                        hasEntry(destinationAddressesK, Arrays.asList(address)),
                        hasKey(wapUrlNblK)
                );

                Matcher<Map<String, ?>> download = allOf(
                        hasEntry(appIdK, appId),
                        hasEntry(buildFileIdK, contentId),
                        hasKey(idK),
                        hasKey(expireDateK)
                );

                one(cmsInfindoRepositoryService).getDownloadDetailsByContentId(contentId); will(returnValue(downloadResponse));
                one(buildFileRepositoryService).findByBuildFileId(contentId); will(returnValue(buildFileResponse));
                exactly(2).of(ncsRepositoryService).findByAppIdOperatorNcsType(appId, "", downloadableK); will(returnValue(ncsResponse));
                one(cmsRequestRepositoryService).createRequest((Map<String, Object>) with(request));
                one(cmsSdpService).getPiList(with(sdpRequest)); will(returnValue(sdpResponse));
                exactly(2).of(cmsRequestRepositoryService).updateRequest((with(any(Map.class))));
                one(cmsSdpService).sendCaasRequest((Map<String, Object>) with(sdpCaasRequest)); will(returnValue(sdpResponse));
                one(cmsInternalRepositoryService).createDownload((Map<String, Object>) with(download));
                one(cmsSdpService).sendWapPush((Map<String, Object>) with(sdpWapPushRequest)); will(returnValue(sdpResponse));
            }
        });

        Map<String, Object> downloadRequest = new HashMap<String, Object>();
        downloadRequest.put(applicationIdK, appId);
        downloadRequest.put(passwordK, password);
        downloadRequest.put(contentIdK, contentId);
        downloadRequest.put(addressK, address);

        Map<String, Object> response = cmsService().download(downloadRequest);

        assertNotNull(response);
        assertEquals(cmsPartialSuccessCode, response.get(statusCodeK));
        assertEquals("You will receive a message shortly", response.get(statusDetailK));
        assertTrue(response.containsKey(wapUrlNblK));
        mockery.assertIsSatisfied();
    }


    @Test
    public void testUnsuccessfulDownloadRequestForInternalContentWithCharging() {
        final Map<String, Object> sdpPiListRequest = new HashMap<String, Object>();
        sdpPiListRequest.put(subscriberIdK, address);
        sdpPiListRequest.put(typeK, allK);

        final Map<String, Object> sdpPiListResponse = new HashMap<String, Object>();
        sdpPiListResponse.put(statusCodeK, internalErrorErrorCode);
        sdpPiListResponse.put(statusDetailK, "Internal error occurred while processing request");

        final Map<String, Object> downloadResponse = null;

        final Map<String, Object> buildFileResponse = buildFileResponse();

        final Map<String, Object> ncsResponse = ncsResponse(flatK);

        mockery.checking(new Expectations() {
            {
                one(cmsInfindoRepositoryService).getDownloadDetailsByContentId(contentId); will(returnValue(downloadResponse));
                one(buildFileRepositoryService).findByBuildFileId(contentId); will(returnValue(buildFileResponse));
                one(ncsRepositoryService).findByAppIdOperatorNcsType(appId, "", downloadableK); will(returnValue(ncsResponse));
                one(cmsSdpService).getPiList(with(sdpPiListRequest)); will(returnValue(sdpPiListResponse));
            }
        });

        Map<String, Object> downloadRequest = new HashMap<String, Object>();
        downloadRequest.put(applicationIdK, appId);
        downloadRequest.put(passwordK, password);
        downloadRequest.put(contentIdK, contentId);
        downloadRequest.put(addressK, address);

        Map<String, Object> response = cmsService().download(downloadRequest);

        assertNotNull(response);
        assertEquals("E", ((String) response.get(statusCodeK)).split("")[1]);
        assertTrue(response.containsKey(statusDetailK));
        mockery.assertIsSatisfied();
    }

    @Test
    public void testInvalidChargeRequestWithNoDownloadRequestId() {
        Map<String, Object> chargeRequest = new HashMap<String, Object>();
        chargeRequest.put(paymentInstrumentNameK, "Equity Bank");

        Map<String, Object> response = cmsService().chargeForDownload(chargeRequest);

        assertNotNull(response);
        assertEquals(cmsDownloadRequestIdErrorCode, response.get(statusCodeK));
        assertEquals("Download Request ID is not found withing the request", response.get(statusDetailK));
    }

    @Test
    public void testInvalidChargeRequestWithNoPaymentInstrumentName() {
        Map<String, Object> chargeRequest = new HashMap<String, Object>();
        chargeRequest.put(downloadRequestIdK, "1312042514090012");

        Map<String, Object> response = cmsService().chargeForDownload(chargeRequest);

        assertNotNull(response);
        assertEquals(cmsPaymentInstrumentNameErrorCode, response.get(statusCodeK));
        assertEquals("Selected PI is not found withing the request", response.get(statusDetailK));
    }

    @Test
    public void testDuplicateChargeRequest() {
        final Map<String, Object> downloadRequest = new HashMap<String, Object>();
        downloadRequest.put(idK, "1312042514090012");
        downloadRequest.put(applicationIdK, appId);
        downloadRequest.put(passwordK, password);
        downloadRequest.put(addressK, address);
        downloadRequest.put(cmsRequestTypeK, downloadRequestK);
        downloadRequest.put(cmsRepositoryK, internalDownloadK);
        downloadRequest.put(cmsChargingTypeK, flatK);
        downloadRequest.put(cmsChargingAmountK, "10");
        downloadRequest.put(statusK, chargingPendingK);
        downloadRequest.put(cmsContentInfoK, buildFileResponse());

        mockery.checking(new Expectations() {
            {
                one(cmsRequestRepositoryService).findByRequestId(with(any(String.class))); will(returnValue(downloadRequest));
            }
        });

        Map<String, Object> chargeRequest = new HashMap<String, Object>();
        chargeRequest.put(downloadRequestIdK, "1312042514090012");
        chargeRequest.put(paymentInstrumentNameK, "Equity Bank");

        Map<String, Object> response = cmsService().chargeForDownload(chargeRequest);

        assertNotNull(response);
        assertEquals(cmsChargingRequestedErrorCode, response.get(statusCodeK));
        assertEquals("Charging request is already received", response.get(statusDetailK));
        mockery.assertIsSatisfied();
    }

    @Test
    public void testSuccessChargeRequestForInternalContentDownload() {
        final Map<String, Object> sdpResponse = new HashMap<String, Object>();
        sdpResponse.put(statusCodeK, successCode);

        final Map<String, Object> downloadRequest = new HashMap<String, Object>();
        downloadRequest.put(idK, "1312042514090012");
        downloadRequest.put(applicationIdK, appId);
        downloadRequest.put(passwordK, password);
        downloadRequest.put(addressK, address);
        downloadRequest.put(cmsRequestTypeK, downloadRequestK);
        downloadRequest.put(cmsRepositoryK, internalDownloadK);
        downloadRequest.put(cmsChargingTypeK, flatK);
        downloadRequest.put(cmsChargingAmountK, "10");
        downloadRequest.put(statusK, acceptedK);
        downloadRequest.put(cmsContentInfoK, buildFileResponse());

        final Map<String, Object> ncsResponse = ncsResponse(flatK);

        mockery.checking(new Expectations() {
            {
                Matcher<Map<String, ?>> sdpCaasRequest = allOf(
                        hasKey(externalTrxIdK),
                        hasEntry(amountK, "10"),
                        hasEntry(paymentInstrumentNameK, "Equity Bank"),
                        hasEntry(subscriberIdK, address),
                        hasEntry(currencyK, "KES")
                );

                Matcher<Map<String, ?>> sdpWapPushRequest = allOf(
                        hasEntry(messageK, "Virtual City"),
                        hasEntry(destinationAddressesK, Arrays.asList(address)),
                        hasKey(wapUrlNblK)
                );

                Matcher<Map<String, ?>> download = allOf(
                        hasEntry(appIdK, appId),
                        hasEntry(buildFileIdK, contentId),
                        hasKey(idK),
                        hasKey(expireDateK)
                );

                one(cmsRequestRepositoryService).findByRequestId(with(any(String.class))); will(returnValue(downloadRequest));
                one(ncsRepositoryService).findByAppIdOperatorNcsType(appId, "", downloadableK); will(returnValue(ncsResponse));
                exactly(3).of(cmsRequestRepositoryService).updateRequest((with(any(Map.class))));
                one(cmsSdpService).sendCaasRequest((Map<String, Object>) with(sdpCaasRequest)); will(returnValue(sdpResponse));
                one(cmsInternalRepositoryService).createDownload((Map<String, Object>) with(download));
                one(cmsSdpService).sendWapPush((Map<String, Object>) with(sdpWapPushRequest)); will(returnValue(sdpResponse));
            }
        });

        Map<String, Object> chargeRequest = new HashMap<String, Object>();
        chargeRequest.put(downloadRequestIdK, "1312042514090012");
        chargeRequest.put(paymentInstrumentNameK, "Equity Bank");

        Map<String, Object> response = cmsService().chargeForDownload(chargeRequest);

        assertNotNull(response);
        assertEquals(cmsPartialSuccessCode, response.get(statusCodeK));
        assertEquals("You will receive a message shortly", response.get(statusDetailK));
        assertTrue(response.containsKey(wapUrlNblK));
        mockery.assertIsSatisfied();
    }

    @Test
    public void testPartiallySuccessChargeRequestForInternalContentDownload() {
        final Map<String, Object> sdpCaasResponse = new HashMap<String, Object>();
        sdpCaasResponse.put(timestampK, "2012-04-03T12:57:11.346+05:30");
        sdpCaasResponse.put(statusCodeK, paymentPendingPartialSuccessCode);
        sdpCaasResponse.put(shortDescriptionK, "Please access Equity Bank Easy Pay; enter following: Business no:equityBusinessNo, Reference ID:180, Amount:KES 57.5");
        sdpCaasResponse.put(longDescriptionK, "Please access Equity Bank Easy Pay menu and enter following when prompted: Business no:equityBusinessNo, Reference ID:180, Amount:KES57.5. Please pay within 3 days.");
        sdpCaasResponse.put(externalTrxIdK, "1312042514090012");
        sdpCaasResponse.put(statusDetailK, "Requested was partially processed");
        sdpCaasResponse.put(referenceIdK, "180");
        sdpCaasResponse.put(internalTrxIdK, "112040312570004");

        final Map<String, Object> downloadRequest = new HashMap<String, Object>();
        downloadRequest.put(idK, "1312042514090012");
        downloadRequest.put(applicationIdK, appId);
        downloadRequest.put(passwordK, password);
        downloadRequest.put(addressK, address);
        downloadRequest.put(cmsRequestTypeK, downloadRequestK);
        downloadRequest.put(cmsRepositoryK, internalDownloadK);
        downloadRequest.put(cmsChargingTypeK, flatK);
        downloadRequest.put(cmsChargingAmountK, "10");
        downloadRequest.put(statusK, acceptedK);
        downloadRequest.put(cmsContentInfoK, buildFileResponse());

        mockery.checking(new Expectations() {
            {
                Matcher<Map<String, ?>> sdpCaasRequest = allOf(
                        hasKey(externalTrxIdK),
                        hasEntry(amountK, "10"),
                        hasEntry(paymentInstrumentNameK, "Equity Bank"),
                        hasEntry(subscriberIdK, address),
                        hasEntry(currencyK, "KES")
                );

                one(cmsRequestRepositoryService).findByRequestId(with(any(String.class))); will(returnValue(downloadRequest));
                exactly(2).of(cmsRequestRepositoryService).updateRequest((with(any(Map.class))));
                one(cmsSdpService).sendCaasRequest((Map<String, Object>) with(sdpCaasRequest)); will(returnValue(sdpCaasResponse));
            }
        });

        Map<String, Object> chargeRequest = new HashMap<String, Object>();
        chargeRequest.put(downloadRequestIdK, "1312042514090012");
        chargeRequest.put(paymentInstrumentNameK, "Equity Bank");

        Map<String, Object> response = cmsService().chargeForDownload(chargeRequest);

        assertNotNull(response);
        assertEquals(cmsWithInstructionPartialSuccessCode, response.get(statusCodeK));
        assertEquals("Requested was partially processed", response.get(statusDetailK));
        assertTrue(response.containsKey(shortDescriptionK));
        assertTrue(response.containsKey(longDescriptionK));
        mockery.assertIsSatisfied();
    }

    @Test
    public void testChargingFailedChargeRequestForInternalDownload() {
        final Map<String, Object> sdpCaasResponse = new HashMap<String, Object>();
        sdpCaasResponse.put(statusCodeK, internalErrorErrorCode);
        sdpCaasResponse.put(statusDetailK, "Internal error occurred while processing request");

        final Map<String, Object> downloadRequest = new HashMap<String, Object>();
        downloadRequest.put(idK, "1312042514090012");
        downloadRequest.put(applicationIdK, appId);
        downloadRequest.put(passwordK, password);
        downloadRequest.put(addressK, address);
        downloadRequest.put(cmsRequestTypeK, downloadRequestK);
        downloadRequest.put(cmsRepositoryK, internalDownloadK);
        downloadRequest.put(cmsChargingTypeK, flatK);
        downloadRequest.put(cmsChargingAmountK, "10");
        downloadRequest.put(statusK, acceptedK);
        downloadRequest.put(cmsContentInfoK, buildFileResponse());

        mockery.checking(new Expectations() {
            {
                Matcher<Map<String, ?>> sdpCaasRequest = allOf(
                        hasKey(externalTrxIdK),
                        hasEntry(amountK, "10"),
                        hasEntry(paymentInstrumentNameK, "Equity Bank"),
                        hasEntry(subscriberIdK, address),
                        hasEntry(currencyK, "KES")
                );

                one(cmsRequestRepositoryService).findByRequestId(with(any(String.class))); will(returnValue(downloadRequest));
                exactly(2).of(cmsRequestRepositoryService).updateRequest((with(any(Map.class))));
                one(cmsSdpService).sendCaasRequest((Map<String, Object>) with(sdpCaasRequest)); will(returnValue(sdpCaasResponse));
            }
        });

        Map<String, Object> chargeRequest = new HashMap<String, Object>();
        chargeRequest.put(downloadRequestIdK, "1312042514090012");
        chargeRequest.put(paymentInstrumentNameK, "Equity Bank");

        Map<String, Object> response = cmsService().chargeForDownload(chargeRequest);

        assertNotNull(response);
        assertEquals("E", ((String) response.get(statusCodeK)).split("")[1]);
        assertTrue(response.containsKey(statusDetailK));
        mockery.assertIsSatisfied();
    }

    @Test
    public void testInvalidChargingNotificationRequestWithNoExternalTransactionId() {
        Map<String, Object> chargingNotificationRequest = new HashMap<String, Object>();
        chargingNotificationRequest.put(timestampK, "2012-04-03T12:57:11.346+05:30");
        chargingNotificationRequest.put(paidAmountK, "10");
        chargingNotificationRequest.put(totalAmountK, "10");
        chargingNotificationRequest.put(balanceDueK, "0");
        chargingNotificationRequest.put(statusCodeK, successCode);
        chargingNotificationRequest.put(statusDetailK, "Success");
        chargingNotificationRequest.put(referenceIdK, "123");
        chargingNotificationRequest.put(currencyK, "KES");
        chargingNotificationRequest.put(internalTrxIdK, "1112042514090012");

        Map<String, Object> response = cmsService().processChargingNotificationForDownload(chargingNotificationRequest);

        assertNotNull(response);
        assertEquals(cmsExternalTransactionIdErrorCode, response.get(statusCodeK));
        assertEquals("External Transaction ID is not found withing the request", response.get(statusDetailK));
    }

    @Test
    public void testInvalidChargingNotificationRequestWithNoStatusCode() {
        Map<String, Object> chargingNotificationRequest = new HashMap<String, Object>();
        chargingNotificationRequest.put(timestampK, "2012-04-03T12:57:11.346+05:30");
        chargingNotificationRequest.put(paidAmountK, "10");
        chargingNotificationRequest.put(totalAmountK, "10");
        chargingNotificationRequest.put(balanceDueK, "0");
        chargingNotificationRequest.put(externalTrxIdK, "1312042514090012");
        chargingNotificationRequest.put(statusDetailK, "Success");
        chargingNotificationRequest.put(referenceIdK, "123");
        chargingNotificationRequest.put(currencyK, "KES");
        chargingNotificationRequest.put(internalTrxIdK, "1112042514090012");

        Map<String, Object> response = cmsService().processChargingNotificationForDownload(chargingNotificationRequest);

        assertNotNull(response);
        assertEquals(cmsStatusCodeErrorCode, response.get(statusCodeK));
        assertEquals("Status Code is not found withing the request", response.get(statusDetailK));
    }

    @Test
    public void testInvalidChargingNotificationRequestWithNoReferenceId() {
        Map<String, Object> chargingNotificationRequest = new HashMap<String, Object>();
        chargingNotificationRequest.put(timestampK, "2012-04-03T12:57:11.346+05:30");
        chargingNotificationRequest.put(paidAmountK, "10");
        chargingNotificationRequest.put(totalAmountK, "10");
        chargingNotificationRequest.put(balanceDueK, "0");
        chargingNotificationRequest.put(externalTrxIdK, "1312042514090012");
        chargingNotificationRequest.put(statusCodeK, successCode);
        chargingNotificationRequest.put(statusDetailK, "Success");
        chargingNotificationRequest.put(currencyK, "KES");
        chargingNotificationRequest.put(internalTrxIdK, "1112042514090012");

        Map<String, Object> response = cmsService().processChargingNotificationForDownload(chargingNotificationRequest);

        assertNotNull(response);
        assertEquals(cmsReferenceIdErrorCode, response.get(statusCodeK));
        assertEquals("Reference ID is not found withing the request", response.get(statusDetailK));
    }

    @Test
    public void testSuccessChargingNotificationRequestForInternalContentDownload() {
        final Map<String, Object> sdpCaasRequest = new HashMap<String, Object>();
        sdpCaasRequest.put(amountK, "5");
        sdpCaasRequest.put(paymentInstrumentNameK, "Equity Bank");
        sdpCaasRequest.put(subscriberIdK, address);
        sdpCaasRequest.put(currencyK, "KES");

        final Map<String, Object> sdpResponse = new HashMap<String, Object>();
        sdpResponse.put(statusCodeK, successCode);

        final Map<String, Object> downloadRequest = new HashMap<String, Object>();
        downloadRequest.put(idK, "1312042514090012");
        downloadRequest.put(applicationIdK, appId);
        downloadRequest.put(passwordK, password);
        downloadRequest.put(addressK, address);
        downloadRequest.put(cmsRequestTypeK, downloadRequestK);
        downloadRequest.put(cmsRepositoryK, internalDownloadK);
        downloadRequest.put(cmsChargingTypeK, flatK);
        downloadRequest.put(cmsChargingAmountK, "10");
        downloadRequest.put(statusK, acceptedK);
        downloadRequest.put(cmsContentInfoK, buildFileResponse());

        final Map<String, Object> ncsResponse = ncsResponse(flatK);

        mockery.checking(new Expectations() {
            {
                Matcher<Map<String, ?>> sdpWapPushRequest = allOf(
                        hasEntry(messageK, "Virtual City"),
                        hasEntry(destinationAddressesK, Arrays.asList(address)),
                        hasKey(wapUrlNblK)
                );

                Matcher<Map<String, ?>> download = allOf(
                        hasEntry(appIdK, appId),
                        hasEntry(buildFileIdK, contentId),
                        hasKey(idK),
                        hasKey(expireDateK)
                );

                one(cmsRequestRepositoryService).findByRequestId(with(any(String.class))); will(returnValue(downloadRequest));
                one(ncsRepositoryService).findByAppIdOperatorNcsType(appId, "", downloadableK); will(returnValue(ncsResponse));
                exactly(2).of(cmsRequestRepositoryService).updateRequest((with(any(Map.class))));
                one(cmsInternalRepositoryService).createDownload((Map<String, Object>) with(download));
                one(cmsSdpService).sendWapPush((Map<String, Object>) with(sdpWapPushRequest)); will(returnValue(sdpResponse));
            }
        });

        Map<String, Object> chargingNotificationRequest = new HashMap<String, Object>();
        chargingNotificationRequest.put(timestampK, "2012-04-03T12:57:11.346+05:30");
        chargingNotificationRequest.put(paidAmountK, "10");
        chargingNotificationRequest.put(totalAmountK, "10");
        chargingNotificationRequest.put(balanceDueK, "0");
        chargingNotificationRequest.put(externalTrxIdK, "1312042514090012");
        chargingNotificationRequest.put(statusCodeK, paymentSuccessPartialSuccessCode);
        chargingNotificationRequest.put(statusDetailK, "Success");
        chargingNotificationRequest.put(referenceIdK, "123");
        chargingNotificationRequest.put(currencyK, "KES");
        chargingNotificationRequest.put(internalTrxIdK, "1112042514090012");

        Map<String, Object> response = cmsService().processChargingNotificationForDownload(chargingNotificationRequest);

        assertNotNull(response);
        assertEquals(successCode, response.get(statusCodeK));
        assertEquals("Success", response.get(statusDetailK));
        mockery.assertIsSatisfied();
    }

    @Test
    public void testInvalidDownloadsQueryRequestForInfindoContentsWithNoAppId() {
        Map<String, Object> queryRequest = new HashMap<String, Object>();

        Map<String, Object> response = cmsService().queryForDownloads(queryRequest);

        assertNotNull(response);
        assertEquals(cmsAppIdErrorCode, response.get(statusCodeK));
        assertEquals("Application ID is not found withing the request", response.get(statusDetailK));

        mockery.assertIsSatisfied();
    }

    @Test
    public void testSuccessDownloadsQueryRequestForNoDownloadableContents() {
        final Map<String, Object> emptyMap = new HashMap<String, Object>();
        emptyMap.put(cmsDevicesK, new HashMap<String, Object>());
        emptyMap.put(cmsPlatformsK, new HashMap<String, Object>());

        mockery.checking(new Expectations() {
            {
                one(cmsInfindoRepositoryService).getDownloadSupportInfoByAppId(with(any(HashMap.class))); will(returnValue(emptyMap));
                one(buildFileRepositoryService).getDevicesAndPlatformsByAppId(appId); will(returnValue(emptyMap));
            }
        });

        Map<String, Object> queryRequest = new HashMap<String, Object>();
        queryRequest.put(applicationIdK, appId);

        Map<String, Object> response = cmsService().queryForDownloads(queryRequest);

        assertNotNull(response);
        assertEquals(cmsContentNotFoundErrorCode, response.get(statusCodeK));
        assertEquals("There are no approved downloadable files for given Application ID", response.get(statusDetailK));
        mockery.assertIsSatisfied();
    }

    @Test
    public void testSuccessDownloadsQueryRequestForInfindoContents() {
        Map<String, Object> devices = new HashMap<String, Object>();
        BasicDBObject models1 = new BasicDBObject();
        models1.put("U8230", Arrays.asList(makeContent("132042514090012")));
        devices.put("Huawei", models1);

        BasicDBObject models2 = new BasicDBObject();
        models2.put("BACKFLIP", Arrays.asList(makeContent("132042514090012")));
        models2.put("XT810", Arrays.asList(makeContent("132042514090012")));
        models2.put("DEFY", Arrays.asList(makeContent("132042514090012")));
        devices.put("Motorola", models2);

        final Map<String, Object> appDevicesAndPlatforms = new HashMap<String, Object>();
        appDevicesAndPlatforms.put(cmsDevicesK, devices);
        appDevicesAndPlatforms.put(cmsPlatformsK, new HashMap<String, Object>());

        mockery.checking(new Expectations() {
            {
                one(cmsInfindoRepositoryService).getDownloadSupportInfoByAppId(with(any(HashMap.class))); will(returnValue(appDevicesAndPlatforms));
            }
        });

        Map<String, Object> queryRequest = new HashMap<String, Object>();
        queryRequest.put(applicationIdK, appId);

        Map<String, Object> response = cmsService().queryForDownloads(queryRequest);

        assertNotNull(response);
        assertEquals(successCode, response.get(statusCodeK));
        assertEquals(appDevicesAndPlatforms, response.get(resultK));
        mockery.assertIsSatisfied();
    }

    @Test
    public void testSuccessDownloadsQueryRequestForInternalContents() {
        final Map<String, Object> devices = new HashMap<String, Object>();
        BasicDBObject models1 = new BasicDBObject();
        models1.put("Galaxy Y", Arrays.asList(makeContent("132042514090012")));
        models1.put("Galaxy Nexus", Arrays.asList(makeContent("132042514090012")));
        models1.put("Galaxy S I", Arrays.asList(makeContent("132042514090012")));
        devices.put("Samsung", models1);

        BasicDBObject models2 = new BasicDBObject();
        models2.put("My Touch 3G", Arrays.asList(makeContent("132042514090012")));
        devices.put("HTC", models2);

        BasicDBObject models3 = new BasicDBObject();
        models3.put("6120", Arrays.asList(makeContent("132042514090013")));
        devices.put("Nokia", models3);

        final Map<String, Object> platforms = new HashMap<String, Object>();
        BasicDBObject versions1 = new BasicDBObject();
        versions1.put("3.0", Arrays.asList(makeContent("132042514090012")));
        versions1.put("3.1", Arrays.asList(makeContent("132042514090012")));
        versions1.put("4.0", Arrays.asList(makeContent("132042514090012")));
        versions1.put("2.2", Arrays.asList(makeContent("132042514090012")));
        platforms.put("Android", versions1);

        BasicDBObject versions2 = new BasicDBObject();
        versions2.put("S60", Arrays.asList(makeContent("132042514090013")));
        platforms.put("Symbian", versions2);

        final Map<String, Object> appDevicesAndPlatforms = new HashMap<String, Object>();
        appDevicesAndPlatforms.put(cmsDevicesK, devices);
        appDevicesAndPlatforms.put(cmsPlatformsK, platforms);

        final Map<String, Object> emptyMap = new HashMap<String, Object>();
        emptyMap.put(cmsDevicesK, new HashMap<String, Object>());
        emptyMap.put(cmsPlatformsK, new HashMap<String, Object>());

        mockery.checking(new Expectations() {
            {
                one(cmsInfindoRepositoryService).getDownloadSupportInfoByAppId(with(any(HashMap.class))); will(returnValue(emptyMap));
                one(buildFileRepositoryService).getDevicesAndPlatformsByAppId(appId); will(returnValue(appDevicesAndPlatforms));
            }
        });

        Map<String, Object> queryRequest = new HashMap<String, Object>();
        queryRequest.put(applicationIdK, appId);

        Map<String, Object> response = cmsService().queryForDownloads(queryRequest);

        assertNotNull(response);
        assertEquals(successCode, response.get(statusCodeK));
        assertEquals(appDevicesAndPlatforms, response.get(resultK));
        mockery.assertIsSatisfied();
    }

    @Test
    public void testInvalidDownloadRequestsQueryRequestWithNoAddress() {
        Map<String, Object> queryRequest = new HashMap<String, Object>();

        Map<String, Object> response = cmsService().queryForDownloadRequests(queryRequest);

        assertNotNull(response);
        assertEquals(cmsAddressErrorCode, response.get(statusCodeK));

        mockery.assertIsSatisfied();
    }

    @Test
    public void testSuccessDownloadRequestsQueryRequestForNoDownloadRequest() {
        mockery.checking(new Expectations() {
            {
                one(cmsRequestRepositoryService).findByAddress(with(any(String.class))); will(returnValue(new LinkedList<Map<String, Object>>()));
            }
        });

        Map<String, Object> queryRequest = new HashMap<String, Object>();
        queryRequest.put(addressK, address);

        Map<String, Object> response = cmsService().queryForDownloadRequests(queryRequest);

        assertNotNull(response);
        assertEquals(cmsDownloadRequestNotFoundErrorCode, response.get(statusCodeK));
        assertEquals("No download request found for given address", response.get(statusDetailK));
        mockery.assertIsSatisfied();
    }

    @Test
    public void testSuccessDownloadRequestsQueryRequest() {
        final Map<String, Object> downloadRequest1 = new HashMap<String, Object>();
        downloadRequest1.put(idK, "1312042514090012");
        downloadRequest1.put(applicationIdK, appId);
        downloadRequest1.put(passwordK, password);
        downloadRequest1.put(addressK, address);
        downloadRequest1.put(cmsRequestTypeK, downloadRequestK);
        downloadRequest1.put(cmsRepositoryK, internalDownloadK);
        downloadRequest1.put(cmsChargingTypeK, flatK);
        downloadRequest1.put(cmsChargingAmountK, "10");
        downloadRequest1.put(statusK, acceptedK);
        downloadRequest1.put(cmsContentInfoK, buildFileResponse());

        final Map<String, Object> downloadRequest2 = new HashMap<String, Object>();
        downloadRequest2.put(idK, "1312042514090013");
        downloadRequest2.put(applicationIdK, appId);
        downloadRequest2.put(passwordK, password);
        downloadRequest2.put(addressK, address);
        downloadRequest2.put(cmsRequestTypeK, downloadRequestK);
        downloadRequest2.put(cmsRepositoryK, internalDownloadK);
        downloadRequest2.put(cmsChargingTypeK, flatK);
        downloadRequest2.put(cmsChargingAmountK, "10");
        downloadRequest2.put(statusK, acceptedK);
        downloadRequest2.put(cmsContentInfoK, buildFileResponse());

        mockery.checking(new Expectations() {
            {
                one(cmsRequestRepositoryService).findByAddress(with(any(String.class))); will(returnValue(Arrays.asList(downloadRequest1, downloadRequest2)));
            }
        });

        Map<String, Object> queryRequest = new HashMap<String, Object>();
        queryRequest.put(addressK, address);

        Map<String, Object> response = cmsService().queryForDownloadRequests(queryRequest);

        assertNotNull(response);
        assertEquals(successCode, response.get(statusCodeK));
        assertEquals(Arrays.asList(downloadRequest1, downloadRequest2), response.get(resultK));
        mockery.assertIsSatisfied();
    }

    @Test
    public void testInvalidDownloadRequestStatusQueryRequestWithNoAppId() {
        Map<String, Object> queryRequest = new HashMap<String, Object>();
        queryRequest.put(addressK, address);

        Map<String, Object> response = cmsService().queryForDownloadRequestStatus(queryRequest);

        assertNotNull(response);
        assertEquals(cmsAppIdErrorCode, response.get(statusCodeK));

        mockery.assertIsSatisfied();
    }

    @Test
    public void testInvalidDownloadRequestStatusQueryRequestWithNoAddress() {
        Map<String, Object> queryRequest = new HashMap<String, Object>();
        queryRequest.put(applicationIdK, appId);

        Map<String, Object> response = cmsService().queryForDownloadRequestStatus(queryRequest);

        assertNotNull(response);
        assertEquals(cmsAddressErrorCode, response.get(statusCodeK));
        mockery.assertIsSatisfied();
    }

    @Test
    public void testSuccessDownloadRequestStatusQueryRequestForNoDownloadRequest() {
        mockery.checking(new Expectations() {
            {
                one(cmsRequestRepositoryService).findByAppIdAndAddress(with(any(String.class)), with(any(String.class))); will(returnValue(new HashMap<String, Object>()));
            }
        });

        Map<String, Object> queryRequest = new HashMap<String, Object>();
        queryRequest.put(applicationIdK, appId);
        queryRequest.put(addressK, address);

        Map<String, Object> response = cmsService().queryForDownloadRequestStatus(queryRequest);

        assertNotNull(response);
        assertEquals(cmsDownloadRequestNotFoundErrorCode, response.get(statusCodeK));
        assertEquals("No download request found for given application Id and address", response.get(statusDetailK));
        mockery.assertIsSatisfied();
    }

    @Test
    public void testSuccessDownloadRequestStatusQueryRequest() {
        final Map<String, Object> downloadRequest = new HashMap<String, Object>();
        downloadRequest.put(idK, "1312042514090012");
        downloadRequest.put(applicationIdK, appId);
        downloadRequest.put(passwordK, password);
        downloadRequest.put(addressK, address);
        downloadRequest.put(cmsRequestTypeK, downloadRequestK);
        downloadRequest.put(cmsRepositoryK, internalDownloadK);
        downloadRequest.put(cmsChargingTypeK, flatK);
        downloadRequest.put(cmsChargingAmountK, "10");
        downloadRequest.put(statusK, acceptedK);
        downloadRequest.put(cmsContentInfoK, buildFileResponse());

        mockery.checking(new Expectations() {
            {
                one(cmsRequestRepositoryService).findByAppIdAndAddress(with(any(String.class)), with(any(String.class))); will(returnValue(downloadRequest));
            }
        });

        Map<String, Object> queryRequest = new HashMap<String, Object>();
        queryRequest.put(applicationIdK, appId);
        queryRequest.put(addressK, address);

        Map<String, Object> response = cmsService().queryForDownloadRequestStatus(queryRequest);

        assertNotNull(response);
        assertEquals(successCode, response.get(statusCodeK));
        assertEquals(downloadRequest, response.get(resultK));
        mockery.assertIsSatisfied();
    }

    @Test
    public void testInvalidResendWapPushRequestWithNoDownloadRequestId() {
        Map<String, Object> response = cmsService().resendWapPush(new HashMap<String, Object>());

        assertNotNull(response);
        assertEquals(cmsDownloadRequestIdErrorCode, response.get(statusCodeK));
        assertEquals("Download Request ID is not found withing the request", response.get(statusDetailK));
    }

    @Test
    public void testUnsuccessfulResendWapPushRequest() {
        mockery.checking(new Expectations() {
            {
                one(cmsRequestRepositoryService).findByRequestId(with(any(String.class))); will(returnValue(null));
            }
        });

        Map<String, Object> controlRequest = new HashMap<String, Object>();
        controlRequest.put(downloadRequestIdK, "1312042514090012");

        Map<String, Object> response = cmsService().resendWapPush(controlRequest);

        assertNotNull(response);
        assertEquals(cmsDownloadRequestNotFoundErrorCode, response.get(statusCodeK));
        assertEquals("No download request found for given Download Request ID", response.get(statusDetailK));
        mockery.assertIsSatisfied();
    }

    @Test
    public void testSuccessResendWapPushRequestForInfindoDownload() {
        final Map<String, Object> sdpResponse = new HashMap<String, Object>();
        sdpResponse.put(statusCodeK, successCode);

        final Map<String, Object> downloadRequest = new HashMap<String, Object>();
        downloadRequest.put(idK, "1312042514090012");
        downloadRequest.put(applicationIdK, appId);
        downloadRequest.put(passwordK, password);
        downloadRequest.put(addressK, address);
        downloadRequest.put(cmsRequestTypeK, downloadRequestK);
        downloadRequest.put(cmsRepositoryK, infindoDownloadK);
        downloadRequest.put(cmsChargingTypeK, flatK);
        downloadRequest.put(cmsChargingAmountK, "10");
        downloadRequest.put(statusK, acceptedK);
        downloadRequest.put(cmsContentInfoK, buildFileResponse());

        Map<String, Object> contentInfo = new HashMap<String, Object>();
        contentInfo.put(wapUrlNblK, "http://dev.sdp.hsenidmobile.com/download-file");
        contentInfo.put(messageK, "Virtual City");
        contentInfo.put(appIdK, appId);

        downloadRequest.put(cmsContentInfoK, contentInfo);

        mockery.checking(new Expectations() {
            {
                Matcher<Map<String, ?>> sdpWapPushRequest = allOf(
                        hasEntry(messageK, "Virtual City"),
                        hasEntry(destinationAddressesK, Arrays.asList(address)),
                        hasEntry(wapUrlNblK, "http://dev.sdp.hsenidmobile.com/download-file")
                );

                one(cmsRequestRepositoryService).findByRequestId(with(any(String.class))); will(returnValue(downloadRequest));
                one(cmsSdpService).sendWapPush((Map<String, Object>) with(sdpWapPushRequest)); will(returnValue(sdpResponse));
                one(cmsRequestRepositoryService).createRequest((with(any(Map.class))));
                exactly(2).of(cmsRequestRepositoryService).updateRequest((with(any(Map.class))));
            }
        });

        Map<String, Object> controlRequest = new HashMap<String, Object>();
        controlRequest.put(downloadRequestIdK, "1312042514090012");

        Map<String, Object> response = cmsService().resendWapPush(controlRequest);

        assertNotNull(response);
        assertEquals(cmsPartialSuccessCode, response.get(statusCodeK));
        assertEquals("You will receive a message shortly", response.get(statusDetailK));
        assertTrue(response.containsKey(wapUrlNblK));
        mockery.assertIsSatisfied();
    }

    @Test
    public void testSuccessResendWapPushRequestForInternalContentDownload() {
        final Map<String, Object> sdpResponse = new HashMap<String, Object>();
        sdpResponse.put(statusCodeK, successCode);

        final Map<String, Object> downloadRequest = new HashMap<String, Object>();
        downloadRequest.put(idK, "1312042514090012");
        downloadRequest.put(applicationIdK, appId);
        downloadRequest.put(passwordK, password);
        downloadRequest.put(addressK, address);
        downloadRequest.put(cmsRequestTypeK, downloadRequestK);
        downloadRequest.put(cmsRepositoryK, internalDownloadK);
        downloadRequest.put(cmsChargingTypeK, flatK);
        downloadRequest.put(cmsChargingAmountK, "10");
        downloadRequest.put(statusK, acceptedK);
        downloadRequest.put(cmsContentInfoK, buildFileResponse());

        Map<String, Object> contentInfo = new HashMap<String, Object>();
        contentInfo.put(wapUrlNblK, "http://dev.sdp.hsenidmobile.com/download-file");
        contentInfo.put(messageK, "Virtual City");
        contentInfo.put(appIdK, appId);
        contentInfo.put(buildAppFileIdK, contentId);

        downloadRequest.put(cmsContentInfoK, contentInfo);

        final Map<String, Object> ncsResponse = ncsResponse(freeK);

        mockery.checking(new Expectations() {
            {
                Matcher<Map<String, ?>> sdpWapPushRequest = allOf(
                        hasEntry(messageK, "Virtual City"),
                        hasEntry(destinationAddressesK, Arrays.asList(address)),
                        hasKey(wapUrlNblK)
                );

                Matcher<Map<String, ?>> download = allOf(
                        hasEntry(appIdK, appId),
                        hasEntry(buildFileIdK, contentId),
                        hasKey(idK),
                        hasKey(expireDateK)
                );

                one(cmsRequestRepositoryService).findByRequestId(with(any(String.class))); will(returnValue(downloadRequest));
                one(ncsRepositoryService).findByAppIdOperatorNcsType(appId, "", downloadableK); will(returnValue(ncsResponse));
                one(cmsSdpService).sendWapPush((Map<String, Object>) with(sdpWapPushRequest)); will(returnValue(sdpResponse));
                one(cmsRequestRepositoryService).createRequest((with(any(Map.class))));
                exactly(2).of(cmsRequestRepositoryService).updateRequest((with(any(Map.class))));
                one(cmsInternalRepositoryService).createDownload((Map<String, Object>) with(download));
            }
        });

        Map<String, Object> controlRequest = new HashMap<String, Object>();
        controlRequest.put(downloadRequestIdK, "1312042514090012");

        Map<String, Object> response = cmsService().resendWapPush(controlRequest);

        assertNotNull(response);
        assertEquals(cmsPartialSuccessCode, response.get(statusCodeK));
        assertEquals("You will receive a message shortly", response.get(statusDetailK));
        assertTrue(response.containsKey(wapUrlNblK));
        mockery.assertIsSatisfied();
    }
}
