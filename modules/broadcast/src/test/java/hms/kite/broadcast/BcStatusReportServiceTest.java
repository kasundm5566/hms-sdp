/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.broadcast;

import static hms.kite.util.KiteKeyBox.broadcastStatusK;
import static hms.kite.util.KiteKeyBox.correlationIdK;
import static hms.kite.util.KiteKeyBox.messageIdK;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.fail;

import hms.kite.broadcast.domain.BroadcastStatus;
import hms.kite.broadcast.domain.Task;
import hms.kite.broadcast.repo.BroadcastMessageRepository;
import hms.kite.broadcast.repo.DbException;
import hms.kite.broadcast.repo.TaskRepository;

import java.util.HashMap;
import java.util.Map;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */
@ContextConfiguration(locations={"classpath:beans-test.xml"})
public class BcStatusReportServiceTest {

	private BcStatusReportService service;
	
	@BeforeTest
	public void setUp() throws DbException {
		ApplicationContext ac = new ClassPathXmlApplicationContext("beans-test.xml");
		Mockery mockery = new Mockery();
		service = (BcStatusReportService) ac.getBean("bcStatusReportSerivice");
		final TaskRepository taskRepository = mockery.mock(TaskRepository.class);
		final BroadcastMessageRepository broadcastMessageRepository = mockery.mock(BroadcastMessageRepository.class);
		final Task task = new Task();
		task.setTotalErrors(3);
		task.setTotalSent(9);
		Expectations expectations = new Expectations(){
			{
				allowing(taskRepository).findByCorrelationId("xx"); will(returnValue(task));
				allowing(taskRepository).findByCorrelationId("X"); will(throwException(new DbException()));
				allowing(broadcastMessageRepository).findStatusByCorrelationId("xx"); will(returnValue(BroadcastStatus.PENDING));
				allowing(broadcastMessageRepository).findStatusByCorrelationId("X"); will(throwException(new Throwable()));
			}
		};
		mockery.checking(expectations);
		service.setBroadcastMessageRepository(broadcastMessageRepository);
		service.setTaskRepository(taskRepository);
	}
	
	@Test
	public void testSend() {
		try {
			Map<String, Object> request = new HashMap<String, Object>();
			request.put(correlationIdK, "xy");
			request.put(messageIdK, "xx");
			Map<String, Object> response = service.send(request);
			assertEquals(3, Integer.parseInt((String) response.get("total-errors")));
			assertEquals(9, Integer.parseInt((String) response.get("total-sent")));
			assertEquals("PENDING", response.get(broadcastStatusK));
			request.put(messageIdK, "X");
			response = service.send(request);
			assertEquals(-1, Integer.parseInt((String) response.get("total-errors")));
			assertEquals(-1, Integer.parseInt((String) response.get("total-sent")));
			assertNull(response.get(broadcastStatusK));
		} catch(Exception ex) {
			fail(ex.getMessage());
		}
	}
	
	@AfterTest
	public void tearDown() {
		
	}
	
	
}
