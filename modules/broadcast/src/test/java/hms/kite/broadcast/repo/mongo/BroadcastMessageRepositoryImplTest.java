/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.broadcast.repo.mongo;

import static hms.kite.util.KiteKeyBox.correlationIdK;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import hms.kite.broadcast.domain.BroadcastStatus;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

/**
 * $LastChangedDate: 2011-05-24 10:33:14 +0530 (Tue, 24 May 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73286 $
 */

@ContextConfiguration(locations = { "classpath:beans-test.xml" })
public class BroadcastMessageRepositoryImplTest extends AbstractTestNGSpringContextTests {

    private static final Logger logger = LoggerFactory.getLogger(BroadcastMessageRepositoryImplTest.class);

	private static final String MESSAGE_COLLECTION = "broadcast_message";

    @Autowired
    private BroadcastMessageRepositoryImpl repository;

    @Autowired
    private MongoTemplate template;

	@BeforeTest
	public void setUp() {
//        template.dropCollection(MESSAGE_COLLECTION);
	}

	@Test(enabled=true)
	public void testCreate() {
		try {
			repository.create(getParameters());
			DBCollection col = template.getCollection(MESSAGE_COLLECTION);
			Map<String, String> result = (Map<String, String>) col.findOne().toMap();
			assertEquals(result, result);
		} catch (Exception ex) {
			fail(ex.getMessage());
		}
	}

//    @Ignore
	@Test(enabled=true)
	public void testFindPendingMessages() {
		try {
			populateCollection();
            List<Map<String, Object>> list = repository.findPendingMessages(3);
            assertEquals(list.size(), 3);

            list = repository.findPendingMessages(10);
			assertEquals(list.size(), 6);
		} catch (Exception e) {
            logger.error(e.getMessage(), e);
			fail(e.getMessage());
		}
	}

	@Test(enabled=true)
	public void testFindExpiredMessages() {
		try {
			populateCollection();
			List<Map<String, Object>> list = repository.findExpiredMessages();
			assertEquals(list.size(), 2);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            fail(e.getMessage());
		}
	}

	@Test(enabled=true)
	public void testUpdate() {
		try {
			Map<String, Map<String, Object>> map = getParameters();
			repository.create(map);
			map.get("request").put("message", "updated message");
			map.get("request").put("sender-address", "updated address");
			repository.update(map.get("request"));
			DBObject obj = template.getCollection(MESSAGE_COLLECTION).findOne();
			assertNotNull(obj);
			Map<String, String> parameters = (Map<String, String>)obj.get("message");
			assertEquals("updated message", parameters.get("message"));
			assertEquals("updated address", parameters.get("sender-address"));
		} catch(Exception ex) {
			fail(ex.getMessage());
		}
	}

	@Test(enabled=true)
	public void testUpdateExpired() {
		try {
			Map<String, Map<String, Object>> map = getParameters();
			repository.create(map);
			Map<String, Object> updateMap = new HashMap<String, Object>();
			updateMap.put("correlation-id", "53");
			repository.updateExpired(updateMap);
			DBObject obj = template.getCollection(MESSAGE_COLLECTION).findOne();
			assertEquals((String) obj.get("correlation-id"), "53");
			assertEquals((String) obj.get("status"), "EXPIRED");
			assertEquals((String) obj.get("appid"), "app_id");
			assertEquals((String) ((Map<String, String>) obj.get("message")).get("message"), "message");
			assertEquals((String) ((Map<String, String>) obj.get("message")).get("tps"), "tps");
		} catch(Exception ex) {
			fail(ex.getMessage());
		}
	}

	@Test(enabled=true)
	public void testFindAlreadyStarted() {
		try {
			Map<String, Map<String, Object>> parameters = getParameters();
			repository.create(parameters);
			parameters.get("request").put("status", BroadcastStatus.STARTED.name());
			parameters.get("request").put("correlation-id", "58");
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("message", parameters);
			map.put("status", BroadcastStatus.STARTED.name());
			map.put("start-date", new Date());
			map.put("end-date", new Date());
			map.put("appid", "_id");
			map.put("correlation-id", "58");
			template.getCollection(MESSAGE_COLLECTION).save(new BasicDBObject(map));
            List<Map<String, Object>> list = repository.findAlreadyStarted(10);
			assertEquals(list.size(), 1);
			assertEquals(((Map<String, Object>)list.get(0)).get("status"), BroadcastStatus.STARTED.name());
		} catch(Exception ex) {
			ex.printStackTrace();
			fail(ex.getMessage());
		}
	}

	@Test(enabled=true)
	public void testUpdateStatus() {
		try {
			Map<String, Map<String, Object>> requestContext = getParameters();
			repository.create(getParameters());
			DBObject ob = template.getCollection(MESSAGE_COLLECTION).findOne();
			assertEquals((Map<String, Object>) ob.get("message"), getParameters());
			assertEquals((String) ob.get("status"), BroadcastStatus.PENDING.name());
			repository.updateStatus((String)requestContext.get("request").get(correlationIdK), BroadcastStatus.COMPLETE);
			ob = template.getCollection(MESSAGE_COLLECTION).findOne();
			assertEquals((Map<String, String>) ob.get("message"), getParameters());
			assertEquals((String) ob.get("status"), BroadcastStatus.COMPLETE.name());
		} catch(Exception ex) {
			fail(ex.getMessage());
		}
	}

	@Test
	public void testFindStatusByCorrelationId() {
		try {
			repository.create(getParameters());
			assertEquals(BroadcastStatus.PENDING, repository.findStatusByCorrelationId("53"));
			assertEquals(null, repository.findStatusByCorrelationId("57"));
		} catch(Exception ex) {
			fail(ex.getMessage());
		}
	}

	private void populateCollection() throws ParseException {
		Map<String, Map<String, Object>> requestContext = getParameters();
		Map<String, Object> request = requestContext.get("request");
		repository.create(requestContext);
		request.put("start-date", "2011-05-20 15:45");
		request.put("end-date", "2020-05-09 14:42");
		request.put("correlation-id", "54");
		repository.create(requestContext);
		request.put("start-date", "2011-04-15 15:45");
		request.put("end-date", "2020-05-01 14:42");
		request.put("correlation-id", "55");
		repository.create(requestContext);
		request.put("start-date", null);
		request.put("end-date", null);
		request.put("correlation-id", "56");
		repository.create(requestContext);
		request.put("start-date", "2011-02-15 15:45:22");
		request.put("end-date", "2011-04-01 14:42:42");
		request.put("correlation-id", "57");
		repository.create(requestContext);
		request.put("start-date", "2011-02-15 15:45:22");
		request.put("end-date", null);
		request.put("correlation-id", "59");
		repository.create(requestContext);
		request.put("start-date", null);
		request.put("end-date", "2011-04-01 14:42:42");
		request.put("correlation-id", "60");
		repository.create(requestContext);
		request.put("start-date", null);
		request.put("end-date", "2020-05-01 14:42:22");
		request.put("correlation-id", "61");
		repository.create(requestContext);
		request.put("start-date", "2011-04-15 15:45");
		request.put("end-date", "2020-05-01 14:42");
		request.put("correlation-id", "55");
		request.put("app-id", "app_id_");
		repository.create(requestContext);
	}

	private Map<String, Map<String, Object>> getParameters() {
		Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String,Object>>();
		Map<String, Object> request = new HashMap<String, Object>();
		request.put("status", BroadcastStatus.PENDING.name());
		request.put("start-date", "2011-04-20 15:45");
		request.put("end-date", "2020-05-29 14:42");
		request.put("app-id", "app_id");
		request.put("sender-address", "from_address");
		request.put("ncs-types", "ncs-types");
		request.put("message", "message");
		request.put("tps", "tps");
		request.put("category", "category");
		request.put("correlation-id", "53");
		Map<String, Object> app = new HashMap<String, Object>();
		app.put("name", "Test App");
		Map<String, Object> sp = new HashMap<String, Object>();
		sp.put("sp-id", "SP000001");
		sp.put("coop-user-name", "spuser");
		requestContext.put("request", request);
		requestContext.put("app", app);
		requestContext.put("sp", app);
		return requestContext;
	}

	@AfterMethod(enabled=true)
	public void tearDown() {
		template.getCollection(MESSAGE_COLLECTION).drop();
	}
}
