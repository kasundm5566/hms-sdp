package hms.kite.broadcast.message;

import hms.kite.subscription.core.service.repo.Subscription;
import hms.kite.subscription.core.service.repo.SubscriptionRepoService;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.testng.annotations.Test;
import org.springframework.data.mongodb.core.query.Query;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import static hms.kite.subscription.core.service.repo.Subscription.*;
import static org.testng.Assert.*;

public class MessageSenderTaskTest {

    @Test
    public void testGetNextSubscribersBatch() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        final Mockery context = new Mockery();
        final SubscriptionRepoService repoService = context.mock(SubscriptionRepoService.class, "subscription-repo-service");

        MessageSenderTask messageSenderTask = new MessageSenderTask();
        messageSenderTask.setSubscriptionRepoService(repoService);

        final List<Subscription> subscriptions = Arrays.asList(
                new SubscriptionBuilder().withMsisdn("94775038417").build(),
                new SubscriptionBuilder().withMsisdn("94775038410").build(),
                new SubscriptionBuilder().withMsisdn("94775038419").build(),
                new SubscriptionBuilder().withMsisdn("94775038410").build()
        );

        context.checking(new Expectations() {
            {
                oneOf(repoService).query(with(any(Query.class)));
                will(returnValue(subscriptions));
            }
        });

        final Method getNextSubscribersBatchMethod =
                messageSenderTask.getClass().getDeclaredMethod("getNextSubscribersBatch", String.class, int.class);
        getNextSubscribersBatchMethod.setAccessible(true);

        final List<String> msisdnList = (List<String>) getNextSubscribersBatchMethod.invoke(messageSenderTask, "APP_09879", 10);

        assertEquals(msisdnList.get(0), "94775038417");
    }
}