/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.broadcast;

import static hms.kite.util.KiteKeyBox.endDateK;
import static hms.kite.util.KiteKeyBox.requestK;
import static hms.kite.util.KiteKeyBox.startDateK;
import static hms.kite.util.KiteKeyBox.statusK;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import hms.kite.broadcast.repo.BroadcastMessageRepository;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */
@ContextConfiguration(locations={"classpath:beans-test.xml"})
public class BoadcastServiceTest {
	
	private BroadcastService broadcastService;
	private Mockery mockery;
	
	@BeforeTest
	public void setUp() throws ParseException {
		ApplicationContext ac = new ClassPathXmlApplicationContext("beans-test.xml");
		broadcastService = (BroadcastService) ac.getBean("broadcastService");
		mockery = new Mockery();
		final BroadcastMessageRepository bmr = mockery.mock(BroadcastMessageRepository.class);
		Expectations expectations = new Expectations() {
			{
				allowing(bmr).create(with(any(java.util.Map.class)));
			}
		};
		broadcastService.setBroadcastMessageRepository(bmr);
		mockery.checking(expectations);
	}
	
	@Test(enabled = true, dataProvider = "success-data")
	public void testExecuteSuccess(String startDate, String endDate) {
		Map<String, Object> response =  getResponse(startDate, endDate);
		assertTrue(Boolean.parseBoolean(response.get(statusK).toString()), "status of the response should be true");
	}
	
	@Test(enabled = true, dataProvider = "failure-data")
	public void testExecuteFailure(String startDate, String endDate) {
		Map<String, Object> response =  getResponse(startDate, endDate);
		assertFalse(Boolean.parseBoolean(response.get(statusK).toString()), "status of the response should be false");
	}
	
	@DataProvider(name = "success-data")
	private Object[][] successData() {
		return new Object[][] {
				{"2020-04-01 14:42:23" , "2020-05-01 14:42:45"},
				{null , "2020-05-01 14:42:45"},
				{"2020-04-01 14:42:23" , null},
				{null, null}
		};
	}
	
	@DataProvider(name = "failure-data")
	private Object[][] failureData() {
		return new Object[][]{
				{"2010-04-01 14:42:23" , "2020-05-01 14:42:45"},
				{"2020-04-01 14:42:23" , "2020-02-01 14:42:45"},
				{"2020-04-01 14:42:23" , "2010-02-01 14:42:45"},
				{null , "2010-05-01 14:42:45"},
				{"2010-04-01 14:42:23" , null}	
		};
	}
	
	private Map<String, Object> getResponse(String startDate, String endDate) {
		Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String,Object>>();
		Map<String, Object> request = new HashMap<String, Object>();
		request.put(startDateK, startDate);
		request.put(endDateK, endDate);
		requestContext.put(requestK, request);
		return broadcastService.execute(requestContext);
	}

}
