/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.broadcast.repo.mongo;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import com.google.common.base.Optional;
import hms.kite.broadcast.repo.SubscriptionRepository;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mongodb.BasicDBObject;

@ContextConfiguration(locations={"classpath:beans-test.xml"})
public class SubscriptionRepositoryImplTest {

	private SubscriptionRepository repository;
	private MongoTemplate template;
	private static final String SUBSCRIPTION = "subscription";

	@BeforeTest
	public void setUp() {
		ApplicationContext ac = new ClassPathXmlApplicationContext("beans-test.xml");
		repository = (SubscriptionRepository) ac.getBean("subscriptionRepository");
		template = (MongoTemplate) ac.getBean("subscription.mongo.template");
	}

	@Test
	public void testFindSubscribers() {
        insertDummyData();
        List<String> list = repository.findSubscribers("app-id_1", Optional.of("dialog"));
        assertTrue(list.contains("+94778966584"));
        assertTrue(list.contains("+94778944925"));
        assertFalse(list.contains("+94728777884"));
        assertFalse(list.contains("+94777774184"));
	}

	@Test
	public void testFindSubscribersWithBatchSize() {
        insertDummyData();
        short i = 0;
        int skip = 0;
        String msisdn1 = null;
        String msisdn2 = null;
        while(true) {
            List<String> list = repository.findSubscribers("app-id_2", Optional.of("tigo"), 2, skip);
            if(list.size() == 0) {
                break;
            }
            skip += 2;
            i++;
            assertEquals(list.size(), 2);
            assertFalse(list.contains(msisdn1), "same msisdn can not be queried twice");
            assertFalse(list.contains(msisdn2), "same msisdn can not be queried twice");
            msisdn1 = list.get(0);
            msisdn2 = list.get(1);
        }
        assertEquals(i, 3);
	}

	private void insertDummyData() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("unique_id", "1234");
		map.put("created_date", new Date());
		map.put("last_modified_date", new Date());
		map.put("keyword", "keyword");
		map.put("short_code", "6666");
		map.put("msisdn", "+94778966584");
		map.put("operator", "dialog");
		map.put("mandate_id", "xx_id");
		map.put("app-id", "app-id_1");
		map.put("charge_amount", new Float(23855.50));
		map.put("current-status", "REGISTERED");
		template.getCollection(SUBSCRIPTION).save(new BasicDBObject(map));
		map.put("short_code", "8866");
		map.put("msisdn", "+94778944925");
		template.getCollection(SUBSCRIPTION).save(new BasicDBObject(map));
		map.put("msisdn", "+94728777884");
		map.put("operator", "tigo");
		map.put("app-id", "app-id_1");
		template.getCollection(SUBSCRIPTION).save(new BasicDBObject(map));
		map.put("msisdn", "+94777774184");
		map.put("operator", "dialog");
		map.put("app-id", "app-id_2");
		template.getCollection(SUBSCRIPTION).save(new BasicDBObject(map));

		map.put("msisdn", "+94725874101");
		map.put("operator", "tigo");
		map.put("app-id", "app-id_2");
		template.getCollection(SUBSCRIPTION).save(new BasicDBObject(map));
		map.put("msisdn", "+94728474102");
		template.getCollection(SUBSCRIPTION).save(new BasicDBObject(map));
		map.put("msisdn", "+94726374103");
		template.getCollection(SUBSCRIPTION).save(new BasicDBObject(map));
		map.put("msisdn", "+94723374104");
		template.getCollection(SUBSCRIPTION).save(new BasicDBObject(map));
		map.put("msisdn", "+94724174105");
		template.getCollection(SUBSCRIPTION).save(new BasicDBObject(map));
		map.put("msisdn", "+94724154106");
		template.getCollection(SUBSCRIPTION).save(new BasicDBObject(map));
	}

	@AfterMethod
	public void tearDown() {
		template.dropCollection(SUBSCRIPTION);
	}
}
