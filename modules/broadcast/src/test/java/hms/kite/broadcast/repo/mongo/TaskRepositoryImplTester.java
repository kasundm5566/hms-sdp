/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.broadcast.repo.mongo;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import hms.kite.broadcast.domain.BroadcastStatus;
import hms.kite.broadcast.domain.Task;
import hms.kite.broadcast.repo.TaskRepository;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * $LastChangedDate: 2011-05-24 10:33:14 +0530 (Tue, 24 May 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73286 $
 */
@ContextConfiguration(locations={"classpath:beans-test.xml"})
public class TaskRepositoryImplTester {

	private TaskRepository repository;
	private MongoTemplate template;

	private static final String TASK_COLLECTION = "task";

	@BeforeTest
	public void setUp() {
		ApplicationContext ac = new ClassPathXmlApplicationContext("beans-test.xml");
		repository = (TaskRepository) ac.getBean("taskRepository");
		template = (MongoTemplate) ac.getBean("template");
	}

	@Test
	public void testFindByCorrelationId() {
		try {
			insertDummyData();
			Task task = repository.findByCorrelationId("0001");
			assertNotNull(task);
			assertEquals(task.getAppId(), "app1");
			assertEquals(task.getStatus().name(), "PENDING");
			assertEquals(task.getTotalSent(), 5);
			assertEquals(task.getTotalErrors(), 2);
		} catch(Exception ex) {
			fail(ex.getMessage());
		}
	}

	@Test
	public void testCreate() {
		Task task = getDomainInstance();
		try {
			repository.create(task);
			DBObject obj = template.getCollection(TASK_COLLECTION).findOne(new BasicDBObject("_id", "7773"));
			assertEquals((String)obj.get("app_id"), "app_id");
			assertEquals((String) obj.get("broadcast_status"), "ERROR");
			assertEquals((Integer) obj.get("total_sent"), new Integer(8));
			assertEquals((Integer) obj.get("total_errors"), new Integer(7));
		} catch(Exception ex) {
			fail(ex.getMessage());
		}
	}

	@Test(dependsOnMethods={"testCreate"})
	public void testUpdate() {
		try {
			Task task = getDomainInstance();
			task.setStatus(BroadcastStatus.EXPIRED);
			task.setAppId("new_app_id");
			repository.update(task);
			DBObject obj = template.getCollection(TASK_COLLECTION).findOne(new BasicDBObject("_id", "7773"));
			assertEquals((String)obj.get("app_id"), "new_app_id");
			assertEquals((String) obj.get("broadcast_status"), "EXPIRED");
		} catch(Exception ex) {
			fail(ex.getMessage());
		}
	}

	private void insertDummyData() {
		Map<String, Object> map = getParameterMap();
		template.getCollection(TASK_COLLECTION).save(new BasicDBObject(map));
		map.put("_id", "0002");
		map.put("app_id", "app2");
		map.put("broadcast_status", "STARTED");
		map.put("total_sent", new Integer(10));
		map.put("total_errors", new Integer(4));
		template.getCollection(TASK_COLLECTION).save(new BasicDBObject(map));
	}

	private Map<String, Object> getParameterMap() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("_id", "0001");
		map.put("app_id", "app1");
		map.put("broadcast_status", "PENDING");
		map.put("last_subscriber", "last subscriber");
		map.put("total_sent", new Integer(5));
		map.put("total_errors", new Integer(2));
		map.put("start_time", new Date());
		map.put("end_time", new Date());
		return map;
	}

	private Task getDomainInstance() {
		Task task = new Task();
		task.setAppId("app_id");
		task.setCorrelationId("7773");
		task.setEndTime(new Date());
		task.setLastSentSubscriber("last subscribed");
		task.setStartedTime(new Date());
		task.setStatus(BroadcastStatus.ERROR);
		task.setTotalErrors(7);
		task.setTotalSent(8);
		return task;
	}

	@AfterTest
	public void tearDown() {
		template.dropCollection(TASK_COLLECTION);
	}
}
