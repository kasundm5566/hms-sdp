package hms.kite.broadcast.integration.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * SDP workflow for wap-push broadcast messages
 * <p/>
 * $LastChangedDate: 2011-07-28 13:35:31 +0530 (Thu, 28 Jul 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 75342 $
 */
public class SdpBroadcastWapPushWf extends WrappedGeneratedWorkflow implements Workflow {

    @Autowired private Channel provAppSlaChannel;
    @Autowired private Channel provSpSlaChannel;
    @Autowired private Channel mnpService;
    @Autowired private Channel broadcastService;

	@Override
	protected Workflow generateWorkflow() {
		ServiceImpl appSlaChannel = new ServiceImpl("prov.app.sla.channel").attachChannel(provAppSlaChannel);
        appSlaChannel.chain("prov.sp.sla.channel").attachChannel(provSpSlaChannel)
		    .chain("number-checking").attachChannel(mnpService)
            .chain().attachChannel(broadcastService);

		return new WorkflowImpl(appSlaChannel);
	}

}
