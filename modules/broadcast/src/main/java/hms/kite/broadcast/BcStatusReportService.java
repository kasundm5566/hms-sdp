/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.broadcast;

import hms.kite.broadcast.domain.BroadcastStatus;
import hms.kite.broadcast.message.BcStatusReportBuilder;
import hms.kite.broadcast.repo.BroadcastMessageRepository;
import hms.kite.broadcast.repo.TaskRepository;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.util.KiteErrorBox.invalidRequestErrorCode;
import static hms.kite.util.KiteKeyBox.*;

/**
 * The only parameter required to contain in the message passed in to the
 * send(Message) is message-id, which is the correlation-id of the last message
 * passed in by the same subscriber.
 * <p/>
 * $LastChangedDate$
 * $LastChangedBy$ $LastChangedRevision$
 */
public class BcStatusReportService extends BaseChannel {

	private TaskRepository taskRepository;
	private BroadcastMessageRepository broadcastMessageRepository;
	
	private static final Logger logger = LoggerFactory.getLogger(BcStatusReportService.class);

	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
		if (logger.isDebugEnabled()) {
			logger.debug("Status report request received [{}]", requestContext.get(requestK));
		}
		try {
			BroadcastStatus status = broadcastMessageRepository.findStatusByCorrelationId((String) requestContext.get(requestK).get(messageIdK));
			if(status == null) {
				return ResponseBuilder.generate(invalidRequestErrorCode, false, requestContext);
			}
			Map<String, Object> resp = BcStatusReportBuilder.createSuccess(ResponseBuilder.generateSuccess(requestContext),
					taskRepository.findByCorrelationId((String) requestContext.get(requestK).get(messageIdK)), status.name());
			addStatusParamsToContext(requestContext, resp);
			return resp;
		} catch (Exception t) {
			logger.error("Error processing request: ", t);
            //todo shd not send total send etc... in case of error
			return BcStatusReportBuilder.createError(
					ResponseBuilder.generate(requestContext, t), null, null);
		}
	}

	private void addStatusParamsToContext(Map<String, Map<String, Object>> requestContext, Map<String, Object> resp) {
		requestContext.get(requestK).put(broadcastStatusK, resp.get(statusK));
		requestContext.get(requestK).put(broadcastTotalSentK, resp.get(broadcastTotalSentK));
		requestContext.get(requestK).put(broadcastTotalErrorsK, resp.get(broadcastTotalErrorsK));
	}
	
	public void setTaskRepository(TaskRepository taskRepository) {
		this.taskRepository = taskRepository;
	}

	public void setBroadcastMessageRepository(
			BroadcastMessageRepository broadcastMessageRepository) {
		this.broadcastMessageRepository = broadcastMessageRepository;
	}

}
