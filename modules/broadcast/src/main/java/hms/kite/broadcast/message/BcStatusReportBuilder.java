/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.broadcast.message;

import static hms.kite.util.KiteKeyBox.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hms.common.rest.util.Message;
import hms.kite.broadcast.domain.Task;

import java.util.Map;

/**
 * $LastChangedDate: 2011-07-27 19:03:23 +0530 (Wed, 27 Jul 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 75318 $
 */
public class BcStatusReportBuilder {

	private static final Logger logger = LoggerFactory.getLogger(BcStatusReportBuilder.class);
	
	public static Map<String, Object> createSuccess(Map<String, Object> request, Task task, String status) {
		request.put(broadcastTotalSentK, Integer.toString(task.getTotalSent()));
		request.put(broadcastTotalErrorsK, Integer.toString(task.getTotalErrors()));
		request.put(broadcastStatusK, status);
		logger.info("Sending response [{}]", request);
		return request;
	}
	
	public static Map<String, Object> createError(Map<String, Object> request, Task task, String status) {
		request.put(broadcastTotalSentK, Integer.toString(-1));
		request.put(broadcastTotalErrorsK, Integer.toString(-1));
		logger.info("Sending response [{}]", request);
		return request;
	}
}
