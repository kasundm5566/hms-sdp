/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.broadcast.integration;

import hms.kite.util.SystemUtil;
import hms.kite.wfengine.ServiceEndpoint;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: 2011-07-28 13:35:00 +0530 (Thu, 28 Jul 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 75341 $
 */
@Produces({"application/json"})
@Consumes({"application/json"})
@Path("/broadcast/")
public class BcStatusReportEndpoint extends ServiceEndpoint {

    private static final Logger logger = LoggerFactory.getLogger(BcStatusReportEndpoint.class);
    private Channel broadcastChannel;

    @Produces({"application/json"})
    @Consumes({"application/json"})
    @POST
    @Path("/status")
    public Map<String, Object> sendService(Map<String, Object> msg, @Context javax.servlet.http.HttpServletRequest request) {
        addHostDetails(msg, request);
        logger.debug("Broadcast status report request received request from host [{}] port [{}]", msg.get(remotehostK),
                msg.get(portK));
        Map<String, Object> resp = broadcastChannel.send(msg);
        logger.info("Sending response [{}]", resp);
        return resp;
    }

    private void addHostDetails(Map<String, Object> msg, HttpServletRequest request) {
        String host = SystemUtil.findHostIp(request);
        if (null != host)
            msg.put(remotehostK, host);
    }

    public void setBroadcastChannel(Channel broadcastChannel) {
        this.broadcastChannel = broadcastChannel;
    }

}
