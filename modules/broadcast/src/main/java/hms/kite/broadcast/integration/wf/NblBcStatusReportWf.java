/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.broadcast.integration.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.control.InExpression;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static hms.kite.util.KiteErrorBox.authenticationFailedErrorCode;
import static hms.kite.util.KiteErrorBox.invalidHostIpErrorCode;
import static hms.kite.util.KiteKeyBox.*;

/**
 * SDP workflow for status reporting on broadcast messages
 * <p/>
 * $LastChangedDate: 2011-07-28 13:35:31 +0530 (Thu, 28 Jul 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 75342 $
 */
public class NblBcStatusReportWf extends WrappedGeneratedWorkflow implements Workflow {

    private static final Logger LOGGER = LoggerFactory.getLogger(NblBcStatusReportWf.class);

    @Autowired private Channel provAppSlaChannel;
    @Autowired private Channel provSpSlaChannel;
    @Autowired private Channel sdpBcStatusReportChannel;

    protected Workflow generateWorkflow() {

        ServiceImpl appSlaChannel = new ServiceImpl("prov.app.sla.channel").attachChannel(provAppSlaChannel);

        appSlaChannel.chain("prov.sp.sla.channel").attachChannel(provSpSlaChannel)
                .chain(new Condition(new InExpression(requestK, remotehostK, appK, allowedHostsK),
                        invalidHostIpErrorCode))
                .chain(new Condition(
                        new EqualExpression(requestK, passwordK, appK, passwordK), authenticationFailedErrorCode))
                .chain(new Condition(new EqualExpression(spK, statusK, approvedK)),
                    new Condition(new InExpression(spK, spSelectedServicesK, new Object[]{smsK})))
                .chain(new Condition(new InExpression(appK, statusK, new Object[]{limitedProductionK,
                        activeProductionK})))
                .chain("broadcast-status-sdp").attachChannel(sdpBcStatusReportChannel);

        return new WorkflowImpl(appSlaChannel);
    }
}
