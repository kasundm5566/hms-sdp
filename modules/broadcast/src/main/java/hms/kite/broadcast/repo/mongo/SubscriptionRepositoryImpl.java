/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.broadcast.repo.mongo;

import java.util.Arrays;
import java.util.List;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import hms.kite.broadcast.repo.SubscriptionRepository;
import hms.kite.datarepo.mongodb.MongoDbUtils;
import hms.kite.subscription.core.domain.SubscriptionStatus;
import org.springframework.data.mongodb.core.query.Query;

import static hms.kite.util.KiteKeyBox.*;
import static org.springframework.data.mongodb.core.query.Criteria.*;
import static org.springframework.data.mongodb.core.query.Query.*;

public class SubscriptionRepositoryImpl extends MongoDbUtils implements SubscriptionRepository {

	private static final String subscriptionCollectionName = "subscription";
    private static final List<String> broadCastEnabledSubscriptionStatus;

    static {
        broadCastEnabledSubscriptionStatus = Arrays.asList(SubscriptionStatus.REGISTERED.name(),
                                                            SubscriptionStatus.TRIAL.name());
    }

	@Override
	public List<String> findSubscribers(String appId, Optional<String> operator) {
        return querySubscribers(appId, operator, Optional.<Query>absent());
	}

    @Override
    public List<String> findSubscribers(String appId, Optional<String> operator, int batchSize, int skip) {
        return querySubscribers(appId, operator, Optional.of(new Query().skip(skip).limit(batchSize)));
    }

    private List<String> querySubscribers(String appId, Optional<String> operator, Optional<Query> limitAndSkipOpt) {
        final Query query = appIdOperatorStatusQuery(appId, operator);

        if(limitAndSkipOpt.isPresent()) {
            final Query limitAndSkip = limitAndSkipOpt.get();
            query.skip(limitAndSkip.getSkip()).limit(limitAndSkip.getLimit());
        }

        List<BasicDBObject> dbObjectList = mongoTemplate.find(query, BasicDBObject.class, subscriptionCollectionName);

        return convertToMsisdnList(dbObjectList);
    }

    private static final Query appIdOperatorStatusQuery(String appId, Optional<String> operator) {
        final Query query = query(where(appIdK).is(appId)).
                            addCriteria(where(currentStatusK).in(broadCastEnabledSubscriptionStatus));

        if(operator.isPresent()) {
            query.addCriteria(where(operatorK).is(operator.get()));
        }

        return query;
    }

    private static final List<String> convertToMsisdnList(List<BasicDBObject> dbObjectList) {
        return Lists.transform(dbObjectList, new Function<DBObject, String>() {
            @Override
            public String apply(DBObject input) {
                return (String) input.get(msisdnK);
            }
        });
    }
}