/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.broadcast.repo.mongo;

import hms.kite.broadcast.repo.DbException;
import hms.kite.broadcast.repo.OperatorListRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class OperatorListRepositoryImpl implements OperatorListRepository {

    private List<String> availableOperators = new ArrayList<String>();

    @Override
    public List<String> getAvailableOperators() throws DbException {
        return availableOperators;
    }

    public void setAvailableOperators(List<String> availableOperators) {
        this.availableOperators = availableOperators;
    }
}
