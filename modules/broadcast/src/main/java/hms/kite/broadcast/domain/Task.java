/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.broadcast.domain;

import java.util.Date;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class Task {

    private String correlationId;
    private String appId;
    private BroadcastStatus status;
    private int totalSent;
    private int totalErrors;
    private String lastSentSubscriber;
    private Date startedTime;
    private Date endTime;

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public BroadcastStatus getStatus() {
        return status;
    }

    public void setStatus(BroadcastStatus status) {
        this.status = status;
    }

    public int getTotalSent() {
        return totalSent;
    }

    public int markSentSuccess(int batchSize) {
        return totalSent += batchSize;
    }

    public int markSentFailed(int batchSize) {
        return totalErrors += batchSize;
    }

    public void setTotalSent(int totalSent) {
        this.totalSent = totalSent;
    }

    public int getTotalErrors() {
        return totalErrors;
    }

    public void setTotalErrors(int totalErrors) {
        this.totalErrors = totalErrors;
    }

    public String getLastSentSubscriber() {
        return lastSentSubscriber;
    }

    public void setLastSentSubscriber(String lastSentSubscriber) {
        this.lastSentSubscriber = lastSentSubscriber;
    }

    public Date getStartedTime() {
        return startedTime;
    }

    public void setStartedTime(Date startedTime) {
        this.startedTime = startedTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
    
    public String toString() {
    	return String.format("Task correlation-id [%s], app-id [%s], status [%s], total-sent [%s], total-errors [%s], last-subscriber[%s]", 
    			correlationId, appId, status, Integer.toString(totalSent), Integer.toString(totalErrors), lastSentSubscriber);
    }

    public int nextSubscriberIndex() {
        return totalErrors + totalSent;
    }
}
