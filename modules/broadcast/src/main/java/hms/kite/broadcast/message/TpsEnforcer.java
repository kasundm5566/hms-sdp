/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.broadcast.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: 2011-07-27 19:03:23 +0530 (Wed, 27 Jul 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 75318 $
 */
public class TpsEnforcer {

    private int tps;
    private int halfTps;
    private long lastMessageTime;
    private int count;
    private final static long second = 1000;
    private final static long halfSecond = 500;

    private static final Logger LOGGER = LoggerFactory.getLogger(TpsEnforcer.class);
    
    public TpsEnforcer(int tps) {
        this.tps = tps;
        if (1 < tps) {
            this.halfTps = tps / 2;
        } else {
            this.halfTps = tps;
        }
    }

    public void enforce() {
        final long timeDifference = System.currentTimeMillis() - lastMessageTime;
        if(timeDifference >= halfSecond) {
            initializeAgain();
        } else {
            count++;
            if(count > halfTps) {
                try {
                    long millis = halfSecond - timeDifference;
                    LOGGER.debug("Exceeded max tps >> [{}], tps [{}], wait for {}",
                            new Object[]{Integer.toString(tps), Integer.toString(count), millis});
                    Thread.sleep(millis);
                    initializeAgain();
                } catch (InterruptedException e) {
                    LOGGER.error("Sleep interrupted!", e);
                }
            }
        }
    }

    private void initializeAgain() {
        count = 1;
        lastMessageTime = System.currentTimeMillis();
    }
}
