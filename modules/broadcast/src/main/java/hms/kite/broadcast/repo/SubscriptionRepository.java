/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.broadcast.repo;

import com.google.common.base.Optional;

import java.util.List;

/**
 * Connect to subscription module to get list of subscribers
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public interface SubscriptionRepository {

    /**
     * Get list of valid subscribers from subscription module
     * Only REGISTERED subscribers shd be returned
     *
     * @param appId -
     * @param operator -
     * @return -
     */
    List<String> findSubscribers(String appId, Optional<String> operator);

    /**
     * @param appId
     * @param operator
     * @param batchSize
     * @param count TODO
     * @return
     */
    List<String> findSubscribers(String appId, Optional<String> operator, int batchSize, int skip);

}
