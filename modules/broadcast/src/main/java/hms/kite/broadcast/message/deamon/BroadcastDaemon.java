/*
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.broadcast.message.deamon;

import hms.kite.broadcast.domain.BroadcastStatus;
import hms.kite.broadcast.message.MessageSenderTask;
import hms.kite.broadcast.repo.BroadcastMessageRepository;
import hms.kite.broadcast.repo.TaskRepository;
import org.apache.log4j.NDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ApplicationContextEvent;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.ContextStoppedEvent;

import java.text.ParseException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: 2011-07-27 19:03:23 +0530 (Wed, 27 Jul 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 75318 $
 */
public class BroadcastDaemon implements ApplicationListener<ApplicationContextEvent> {

    private static final Logger logger = LoggerFactory.getLogger(BroadcastDaemon.class);

    private long invokeIntervalMills;
    private ExecutorService executorService;
    private BroadcastMessageRepository broadcastMessageRepository;
    private MessageSenderTask messageSenderTask;
    private int noOfConcurrentBCRequests = 5;
    private int maxConcurrentSpRequests;
    private Thread daemonThread;
    private long startedRequestTouchFrequency = 6;
    private long executionCount = 0;

    private final Map<String, Map<String, Object>> apps = new ConcurrentHashMap<String, Map<String, Object>>();
    private int shutdownTimeout = 100;

    private void init() {
        messageSenderTask.start();

        daemonThread = new Thread(new Runnable() {
            @Override
            public void run() {
                execute();
            }
        });

        daemonThread.start();
    }

    private void stop() {
        daemonThread.interrupt();
        executorService.shutdown();
        messageSenderTask.stop();
        try {
            executorService.awaitTermination(shutdownTimeout, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }

    private void execute() {
        while (!Thread.interrupted()) {
            try {
                long startedTime = System.currentTimeMillis();
                if (apps.size() >= noOfConcurrentBCRequests) {
                    logger.error("Will not start to send more messages since there are [{}] requests active", apps.size());
                } else {

                    executionCount++;

                    processHaltedMessages();
                    processExpiredMessages();
                    processPendingMessages();
                }
                waitForNextExecution(startedTime);
            } catch (InterruptedException t) {
                logger.info("Shutdown broadcast daemon boss thread.");
                return;//this exception happens in shutdown
            } catch (Throwable t) {
                logger.error("Unexpected error occurred in broadcast daemon", t);
            }
        }
    }

    private void waitForNextExecution(long startedTime) throws InterruptedException {
        try {
            long waitingTime = invokeIntervalMills - (System.currentTimeMillis() - startedTime);
            if (waitingTime > 0) {
                logger.trace("Broadcast demon waiting for [{}]", waitingTime);
                Thread.sleep(waitingTime);
            }
        } catch (InterruptedException e) {
            throw e;
        } catch (Throwable e) {
            logger.error("Error while waiting for next execution", e);
        }
    }

    private void processHaltedMessages() {

        if (executionCount % startedRequestTouchFrequency != 1) {
            logger.trace("Execution count {}%{} = {} will not consider for halted message processing {}",
                    new Object[]{executionCount, startedRequestTouchFrequency, executionCount % startedRequestTouchFrequency});
            return;
        }

        int batchSize = noOfConcurrentBCRequests - apps.size();
        if (batchSize <= 0) {
            logger.info("There are [{}] apps already in active state.", apps.size());
            return;
                        }
        final List<Map<String, Object>> requests = broadcastMessageRepository.findAlreadyStarted(batchSize);

        logger.info("Found [{}] halted messages to broadcast", requests.size());

        doSendMessages(requests);
                    }

    private boolean isRequestProcessing(Map<String, Object> request) {

        if (apps.containsKey(request.get(appIdK))) return true;

        int spRequestCount = 0;
        for (Map<String, Object> currentRequest : apps.values()) {
            if (currentRequest.get(spIdK).equals(request.get(spIdK))) {
                spRequestCount++;
                }
        }
        logger.debug("Currently executing sp request count is {}. ", spRequestCount);
        return spRequestCount >= maxConcurrentSpRequests;
    }

    private void processExpiredMessages() {
        final List<Map<String, Object>> requests = broadcastMessageRepository.findExpiredMessages();
        logger.trace("Found [{}] expired broadcast messages", requests.size());

        for (final Map<String, Object> request : requests) {
            try {
                NDC.push((String) request.get(correlationIdK));
                if (isRequestProcessing(request)) {
                    logger.debug("Request is already active, will not consider for expiration");
                } else {
                broadcastMessageRepository.updateStatus((String) request.get(correlationIdK), BroadcastStatus.EXPIRED);
                }
            } catch (Exception e) {
                request.put(statusK, BroadcastStatus.ERROR.name());
                logger.error("Unable to update broadcast message as expired", e);
            } finally {
                NDC.pop();
            }
        }
    }

    private void processPendingMessages() {

        int batchSize = noOfConcurrentBCRequests - apps.size();
        if (batchSize <= 0) {
            logger.info("There are [{}] apps already in active state.", apps.size());
            return;
        }

        final List<Map<String, Object>> requests = broadcastMessageRepository.findPendingMessages(noOfConcurrentBCRequests);

        logger.debug("Found {} pending requests to broadcast", requests.size());

        doSendMessages(requests);
    }

    private void doSendMessages(List<Map<String, Object>> requests) {
        logger.trace("Found [{}] broadcast messages", requests.size());

        try {
            for (final Map<String, Object> request : requests) {
                try {
                    if (apps.size() > noOfConcurrentBCRequests) {
                        logger.debug("Already max number of {} requests processing");
                        return;
                    }
                    NDC.push((String) request.get(correlationIdK));

                    logger.trace("Sending broadcast message[{}]", request);
                    if (isRequestProcessing(request)) {
                        logger.debug("SP/App [{}/{}] already has maximum active messages.",
                                request.get(spIdK), request.get(appIdK));
                        continue;
                    }
                    doBroadcastOneMessage(request);

                } finally {
                    NDC.pop();
                }
            }
        } catch (Exception e) {
            logger.error("Error while processing pending messages", e);
        }
    }

    private void doBroadcastOneMessage(final Map<String, Object> request) throws ParseException {

        apps.put((String) request.get(appIdK), request);
        final String broadcastRequestCorrelationId = (String) request.get(correlationIdK);
        broadcastMessageRepository.updateStatus(broadcastRequestCorrelationId, BroadcastStatus.STARTED);

        executorService.submit(
                new Runnable() {
                    @Override
                    public void run() {
                        try {

                            NDC.push(broadcastRequestCorrelationId);
                            try {
                                logger.debug("Start sending broadcast message [{}]", request);
                                messageSenderTask.send(request);
                                broadcastMessageRepository.updateStatus(broadcastRequestCorrelationId, BroadcastStatus.COMPLETE);
                                logger.debug("Finished sending broadcast message [{}]", request);
                            } catch (Throwable e) {
                                broadcastMessageRepository.updateStatus(broadcastRequestCorrelationId, BroadcastStatus.STARTED);
                                logger.error("Unable to broadcast message [" + request + "]", e);
                            }
                        } catch (Exception e) {
                            logger.error("Failed to update message correlation_id: [{}], appIdK: [{}], error [{}]",
                                    new Object[]{request.get(correlationIdK), request.get(appIdK), e});
                        } finally {
                            NDC.pop();
                            apps.remove(request.get(appIdK));
                        }
                    }
                }
        );
    }

    public void setInvokeIntervalMills(long invokeIntervalMills) {
        this.invokeIntervalMills = invokeIntervalMills;
    }

    public void setExecutorService(ExecutorService executorService) {
        this.executorService = executorService;
    }

    public void setBroadcastMessageRepository(BroadcastMessageRepository broadcastMessageRepository) {
        this.broadcastMessageRepository = broadcastMessageRepository;
    }

    public void setMessageSenderTask(MessageSenderTask messageSenderTask) {
        this.messageSenderTask = messageSenderTask;
    }

    public void setNoOfConcurrentBCRequests(int noOfConcurrentBCRequests) {
        this.noOfConcurrentBCRequests = noOfConcurrentBCRequests;
    }

    public void setStartedRequestTouchFrequency(long startedRequestTouchFrequency) {
        this.startedRequestTouchFrequency = startedRequestTouchFrequency;
    }

    public void setShutdownTimeout(int shutdownTimeout) {
        this.shutdownTimeout = shutdownTimeout;
    }

    public void setMaxConcurrentSpRequests(int maxConcurrentSpRequests) {
        this.maxConcurrentSpRequests = maxConcurrentSpRequests;
    }

    @Override
    public void onApplicationEvent(ApplicationContextEvent event) {
        logger.debug("Context event {} received", event);
        try {
            if (event instanceof ContextStartedEvent) {
        logger.debug("Context Start Event received to Broadcast daemon");
            init();
            } else if (event instanceof ContextStoppedEvent || event instanceof ContextClosedEvent) {
                logger.info("Shutting down broadcast service");
                stop();
            }
        } catch (Exception e) {
            logger.error("Unexpected error occurred while initializing the broadcast daemon", e);
        }
    }
}
