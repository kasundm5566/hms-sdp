/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.broadcast.integration.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.control.InExpression;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: 2011-07-28 13:35:31 +0530 (Thu, 28 Jul 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 75342 $
 */
public class NblBroadcastSmsWf extends WrappedGeneratedWorkflow implements Workflow {

    @Autowired private Channel smsAoSdpChannel;
    @Autowired private Channel messageHistoryChannel;
    @Autowired private Channel contentFilteringService;
    @Autowired private Channel tpdThrottlingChannel;
    @Autowired private Channel provSpSlaChannel;
    @Autowired private Channel provAppSlaChannel;
    @Autowired private Channel provNcsSlaChannel;
    @Autowired private Channel internalHostResolverChannel;

	private static final Logger LOGGER = LoggerFactory.getLogger(NblBroadcastSmsWf.class);

	protected Workflow generateWorkflow() {

		ServiceImpl sdpChannel = new ServiceImpl("sms-ao-sdp-channel");
        sdpChannel.setChannel(smsAoSdpChannel);


        ServiceImpl subscriptionTpd = new ServiceImpl("tpd-throttling-channel");
        subscriptionTpd.setChannel(tpdThrottlingChannel);
        subscriptionTpd.setOnSuccess(sdpChannel);
        subscriptionTpd.setServiceParameterKeys("subscription");

        ServiceImpl governanceChannel = new ServiceImpl("governance.channel");
        governanceChannel.setChannel(contentFilteringService);
        governanceChannel.setOnSuccess(subscriptionTpd);

        ServiceImpl governanceCheck = new ServiceImpl(
                new Condition(new EqualExpression(appK,  governK, true))
        );
        governanceCheck.setOnSuccess(governanceChannel);
        governanceCheck.onDefaultError(subscriptionTpd);

        ServiceImpl messageHistoryChannelService = new ServiceImpl("message-history-channel");
        messageHistoryChannelService.setChannel(messageHistoryChannel);
        messageHistoryChannelService.setOnSuccess(governanceCheck);

        ServiceImpl appStateValidation = new ServiceImpl(
                new Condition(new InExpression(appK, statusK, new Object[]{limitedProductionK, activeProductionK})));
        appStateValidation.setOnSuccess(messageHistoryChannelService);

        ServiceImpl spStateValidation = new ServiceImpl(
                new Condition(new EqualExpression(spK, statusK, approvedK)),
                new Condition(new InExpression(spK, spSelectedServicesK, new Object[]{smsK}))
        );
        spStateValidation.setOnSuccess(appStateValidation);

        ServiceImpl authenticationService = new ServiceImpl(
                new Condition(
                        new EqualExpression(requestK, passwordK, appK, passwordK), authenticationFailedErrorCode));
        authenticationService.setOnSuccess(spStateValidation);

        ServiceImpl hostValidation = createHostValidationService();
        hostValidation.setOnSuccess(authenticationService);

        ServiceImpl internalHostValidationService = new ServiceImpl("internal.host.validation");
        internalHostValidationService.setChannel(internalHostResolverChannel);
        internalHostValidationService.setOnSuccess(spStateValidation);
        internalHostValidationService.onDefaultError(hostValidation);

        ServiceImpl ncsSlaChannel = new ServiceImpl("prov.ncs.sla.channel");
        ncsSlaChannel.setChannel(provNcsSlaChannel);
        ncsSlaChannel.setOnSuccess(internalHostValidationService);

		ServiceImpl spSlaChannel = new ServiceImpl("prov.sp.sla.channel",
                ncsSlaChannel);
		spSlaChannel.setChannel(provSpSlaChannel);

		ServiceImpl appSlaChannel = new ServiceImpl("prov.app.sla.channel",
				spSlaChannel);
		appSlaChannel.setChannel(provAppSlaChannel);

		return new WorkflowImpl(appSlaChannel);
	}

	private static ServiceImpl createHostValidationService() {
        InExpression hostExpression = new InExpression(requestK, remotehostK, appK, allowedHostsK);
        Condition condition = new Condition(hostExpression, invalidHostIpErrorCode);

        return new ServiceImpl(condition);
    }
}
