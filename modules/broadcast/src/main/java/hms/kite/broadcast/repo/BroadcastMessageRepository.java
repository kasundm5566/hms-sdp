/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.broadcast.repo;

import hms.common.rest.util.Message;
import hms.kite.broadcast.domain.BroadcastStatus;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

public interface BroadcastMessageRepository {

	void create(Map<String, Map<String, Object>> requestContext) throws ParseException;

	void update(Map<String, Object> request) throws ParseException;

	void updateExpired(Map<String, Object> request) throws ParseException; // will be removed later ...

    List<Map<String, Object>> findPendingMessages(int batchSize);

	List<Map<String, Object>> findExpiredMessages();

    List<Map<String, Object>> findAlreadyStarted(int batchSize);

    BroadcastStatus findStatusByCorrelationId(String correlationId);

    void updateStatus(String requestCorrelationId, BroadcastStatus status) throws ParseException;
}
