package hms.kite.broadcast.integration.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * SDP workflow for SMS broadcast messages
 * <p/>
 * $LastChangedDate: 2011-07-28 13:35:31 +0530 (Thu, 28 Jul 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 75342 $
 */
public class SdpBroadcastSmsWf extends WrappedGeneratedWorkflow implements Workflow {

	private static final Logger LOGGER = LoggerFactory.getLogger(SdpBroadcastSmsWf.class);

    @Autowired private Channel provAppSlaChannel;
    @Autowired private Channel provSpSlaChannel;
    @Autowired private Channel mnpService;
    @Autowired private Channel broadcastService;


	protected Workflow generateWorkflow() {
        LOGGER.debug("Creating SdpBroadcastSmsWf workflow");
		ServiceImpl appSlaChannel = new ServiceImpl("prov.app.sla.channel").attachChannel(provAppSlaChannel);
        appSlaChannel.chain("prov.sp.sla.channel").attachChannel(provSpSlaChannel)
		    .chain("number-checking").attachChannel(mnpService)
            .chain().attachChannel(broadcastService);

		return new WorkflowImpl(appSlaChannel);
	}
}
