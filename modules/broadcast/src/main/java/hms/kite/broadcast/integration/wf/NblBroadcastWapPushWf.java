package hms.kite.broadcast.integration.wf;

import static hms.kite.util.KiteErrorBox.authenticationFailedErrorCode;
import static hms.kite.util.KiteErrorBox.invalidHostIpErrorCode;
import static hms.kite.util.KiteKeyBox.activeProductionK;
import static hms.kite.util.KiteKeyBox.allowedHostsK;
import static hms.kite.util.KiteKeyBox.appK;
import static hms.kite.util.KiteKeyBox.approvedK;
import static hms.kite.util.KiteKeyBox.governK;
import static hms.kite.util.KiteKeyBox.limitedProductionK;
import static hms.kite.util.KiteKeyBox.maskNumberK;
import static hms.kite.util.KiteKeyBox.passwordK;
import static hms.kite.util.KiteKeyBox.remotehostK;
import static hms.kite.util.KiteKeyBox.requestK;
import static hms.kite.util.KiteKeyBox.smsK;
import static hms.kite.util.KiteKeyBox.spK;
import static hms.kite.util.KiteKeyBox.spSelectedServicesK;
import static hms.kite.util.KiteKeyBox.statusK;
import static hms.kite.util.KiteKeyBox.unmaskK;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.control.InExpression;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * NBL workflow for wap-push broadcast messages
 * <p/>
 * $LastChangedDate: 2011-07-28 13:35:31 +0530 (Thu, 28 Jul 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 75342 $
 */
public class NblBroadcastWapPushWf extends WrappedGeneratedWorkflow implements Workflow {

    @Autowired private Channel wapAoSdpChannel;
    @Autowired private Channel contentFilteringService;
    @Autowired private Channel numberMaskingChannel;
    @Autowired private Channel provSpSlaChannel;
    @Autowired private Channel provAppSlaChannel;
    @Autowired private Channel internalHostResolverChannel;

	@Override
	protected Workflow generateWorkflow() {
		ServiceImpl sdpChannel = new ServiceImpl("wap-ao-sdp");  
        sdpChannel.setChannel(wapAoSdpChannel);

        ServiceImpl governanceChannel = new ServiceImpl("governance.channel");
        governanceChannel.setChannel(contentFilteringService);
        governanceChannel.setOnSuccess(sdpChannel);

        ServiceImpl governanceCheck = new ServiceImpl(
                new Condition(new EqualExpression(appK,  governK, true))
        );
        governanceCheck.setOnSuccess(governanceChannel);
        governanceCheck.onDefaultError(sdpChannel);

        ServiceImpl numberMaskingChannelService = new ServiceImpl("number-masking-channel");
        numberMaskingChannelService.setChannel(numberMaskingChannel);
        numberMaskingChannelService.addServiceParameter(unmaskK, true);
        numberMaskingChannelService.setOnSuccess(governanceCheck);

        ServiceImpl numberMaskingCheck = new ServiceImpl(
                new Condition(new EqualExpression(appK, maskNumberK, true))
        );
        numberMaskingCheck.setOnSuccess(numberMaskingChannelService);
        numberMaskingCheck.onDefaultError(governanceCheck);

        ServiceImpl appStateValidation = new ServiceImpl(
                new Condition(new InExpression(appK, statusK, new Object[]{limitedProductionK, activeProductionK})));
        appStateValidation.setOnSuccess(numberMaskingCheck);

        ServiceImpl spStateValidation = new ServiceImpl(
                new Condition(new EqualExpression(spK, statusK, approvedK)),
                new Condition(new InExpression(spK, spSelectedServicesK, new Object[]{smsK}))
        );
        spStateValidation.setOnSuccess(appStateValidation);

        ServiceImpl authenticationService = new ServiceImpl(
                new Condition(
                        new EqualExpression(requestK, passwordK, appK, passwordK), authenticationFailedErrorCode));
        authenticationService.setOnSuccess(spStateValidation);

        ServiceImpl hostValidation = createHostValidationService();
        hostValidation.setOnSuccess(authenticationService);

        ServiceImpl internalHostValidationService = new ServiceImpl("internal.host.validation");
        internalHostValidationService.setChannel(internalHostResolverChannel);
        internalHostValidationService.setOnSuccess(spStateValidation);
        internalHostValidationService.onDefaultError(hostValidation);

        ServiceImpl spSlaChannel = new ServiceImpl("prov.sp.sla.channel");
		spSlaChannel.setChannel(provSpSlaChannel);
        spSlaChannel.setOnSuccess(internalHostValidationService);

		ServiceImpl appSlaChannel = new ServiceImpl("prov.app.sla.channel", spSlaChannel);
		appSlaChannel.setChannel(provAppSlaChannel);

		return new WorkflowImpl(appSlaChannel);
    }

    private static ServiceImpl createHostValidationService() {
        InExpression hostExpression = new InExpression(requestK, remotehostK, appK, allowedHostsK);
        Condition condition = new Condition(hostExpression, invalidHostIpErrorCode);

        return new ServiceImpl(condition);
    }
}
