/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.broadcast.message;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.mongodb.DBObject;
import hms.kite.broadcast.domain.BroadcastStatus;
import hms.kite.broadcast.domain.Task;
import hms.kite.broadcast.repo.DbException;
import hms.kite.broadcast.repo.TaskRepository;
import hms.kite.broadcast.util.DateUtil;

import java.text.ParseException;
import java.util.*;

import hms.kite.subscription.core.domain.SubscriptionStatus;
import hms.kite.subscription.core.service.repo.Subscription;
import hms.kite.subscription.core.service.repo.SubscriptionRepoService;
import org.apache.log4j.NDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.query.Query;

import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.logging.StatLogger.printStatLog;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

public class MessageSenderTask {

    private BroadcastMessageBuilder builder;
    private TaskRepository taskRepository;
    private MessageSender sender;
    private int batchSize = 10;
    private int broadcastingTps;
    private volatile boolean stop = false;
    private boolean useSubsmitMulti = true;
    SubscriptionRepoService subscriptionRepoService;


    private static final List<String> broadCastEnabledSubscriptionStatus;
    static {
        broadCastEnabledSubscriptionStatus = Arrays.asList(SubscriptionStatus.REGISTERED.name(),
                SubscriptionStatus.TRIAL.name());
    }

    private final Logger logger = LoggerFactory.getLogger(MessageSenderTask.class);

    public void send(Map<String, Object> request) throws DbException {
        Task task = taskRepository.findByCorrelationId((String) request.get(correlationIdK));
        logger.debug("Task found in repository for id {} is {}", request.get(correlationIdK), task);
        if (task == null) {
            task = initTask(request);
        }
        send(request, task);
    }

    private void send(Map<String, Object> request, Task task) throws DbException {

        long dispatchStart = System.currentTimeMillis();
        String dispatchCorrelationId = (String) request.get(correlationIdK);
        String appid = (String) request.get(appIdK);
//		TpsEnforcer tpsEnforcer = new TpsEnforcer(Integer.parseInt((String) request.get(tpsK)));
        TpsEnforcer tpsEnforcer = new TpsEnforcer(broadcastingTps);//This is a temporary fix for live

        taskRepository.update(task);

        try {
            logger.info("Sending broadcast message[{}] ", request);
            logger.info("SubmitMulti dispatch mode enabled[{}]", useSubsmitMulti);
            while (!stop) {

                final int skipIndex = task.nextSubscriberIndex();

                final List<String> msisdnList = getNextSubscribersBatch(appid, skipIndex);

                logger.debug("Found [{}] size msisdn-list for query app-id = [{}] next-subscriber-index = [{}] batch-size = [{}]",
                        new Object[]{msisdnList.size(), appid, skipIndex, batchSize});

                if (msisdnList.isEmpty()) {
                    task.setEndTime(new Date());
                    task.setStatus(BroadcastStatus.COMPLETE);
                    taskRepository.update(task);
                    break;
                }

                if (useSubsmitMulti) {
                    if (!stop) {
                        sendToSubscriber(request, task, tpsEnforcer, msisdnList);
                    } else {
                        logger.info("Thread has interrupted. Stop processing messages");
                    }
                } else {
                    for (String msisdn : msisdnList) {
                        if (!stop) {
                            sendToSubscriber(request, task, tpsEnforcer, Arrays.asList(msisdn));
                        } else {
                            logger.info("Thread has interrupted. Stop processing messages");
                            break;
                        }
                    }
                }
                taskRepository.update(task);
            }
            logger.info("Stopped the task {}", task);
        } catch (Exception e) {
            logger.error("Unable to broadcast message : " + request, e);
            task.setStatus(BroadcastStatus.ERROR);
            taskRepository.update(task);
        } finally {
            printStatLog(dispatchStart, "SMS-BROADCAST-DISPATCH", dispatchCorrelationId);
        }
    }

    private List<String> getNextSubscribersBatch(String appId, int skipIndex) {
        final Query nextSubscribersBatchQuery = query(where(appIdK).
                                                        is(appId)).
                                                        addCriteria(where(currentStatusK).in(broadCastEnabledSubscriptionStatus)).
                                                        skip(skipIndex).
                                                        limit(batchSize);

        final List<DBObject> subscriptions = subscriptionRepoService.queryDbObjects(nextSubscribersBatchQuery);

        return Lists.transform(subscriptions, new Function<DBObject, String>() {
            @Override
            public String apply(DBObject input) {
                return (String) input.get(msisdnK);
            }
        });
    }

    private void sendToSubscriber(Map<String, Object> request, Task task, TpsEnforcer tpsEnforcer, List<String> subscribersList) throws ParseException {
        long start = System.currentTimeMillis();
        try {
            List<String> recipientList = new ArrayList<String>();
            for (String msisdn : subscribersList) {
                recipientList.add(msisdn);
            }

            Map<String, Object> broadcastMessage = builder.createMessage(request, recipientList);

            NDC.push((String) broadcastMessage.get(correlationIdK));
            logger.debug("Started sending broadcast message to subscriber list[{}]", recipientList);

            if (endDateValid(request)
                    && sender.sendMessage(broadcastMessage, tpsEnforcer)) {
                task.markSentSuccess(recipientList.size());
            } else {
                task.markSentFailed(recipientList.size());
            }
        } finally {
            NDC.pop();
            printStatLog(start, "SMS-BROADCAST-SINGLE-REQ");
        }
    }

    private Task initTask(Map<String, Object> request) {
        Task task = new Task();
        task.setCorrelationId((String) request.get(correlationIdK));
        task.setAppId((String) request.get(appIdK));
        task.setStartedTime(new Date());
        task.setStatus(BroadcastStatus.STARTED);
        return task;
    }

    private boolean endDateValid(Map<String, Object> request) throws ParseException {
        return ((request.get(endDateK) == null) || DateUtil.parseDate((String) request.get(endDateK)).after(new Date()));
    }

    public void setBuilder(BroadcastMessageBuilder builder) {
        this.builder = builder;
    }

    public void setSender(MessageSender sender) {
        this.sender = sender;
    }

    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }

    public void setBroadcastingTps(int broadcastingTps) {
        this.broadcastingTps = broadcastingTps;
    }

    public void setSubscriptionRepoService(SubscriptionRepoService subscriptionRepoService) {
        this.subscriptionRepoService = subscriptionRepoService;
    }

    public void stop() {
        stop = true;
    }

    public void start() {
        stop = false;
    }

    public void setUseSubsmitMulti(boolean useSubsmitMulti) {
        this.useSubsmitMulti = useSubsmitMulti;
    }
}
