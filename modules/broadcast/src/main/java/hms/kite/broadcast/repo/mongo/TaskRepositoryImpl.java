/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.broadcast.repo.mongo;

import hms.kite.broadcast.domain.BroadcastStatus;
import hms.kite.broadcast.domain.Task;
import hms.kite.broadcast.repo.DbException;
import hms.kite.broadcast.repo.TaskRepository;
import hms.kite.datarepo.mongodb.MongoDbUtils;

import java.util.Date;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

public class TaskRepositoryImpl extends MongoDbUtils implements TaskRepository {

	private static final String TASK_COLLECTION = "task";
	private static final String LAST_SUBSCRIBER = "last_subscriber";

	private DBCollection collection;

	@Override
	public Task findByCorrelationId(String correlationId) throws DbException {
		BasicDBObject query = new BasicDBObject();
		query.put(correlationIdK, correlationId);
		DBObject ob = getTaskCollection().findOne(query);
        if (ob != null) {
		Task task = new Task();
			task.setCorrelationId((String) ob.get(correlationIdK));
			task.setAppId((String) ob.get(appIdK)); 
			task.setStatus(BroadcastStatus.valueOf((String) ob.get(statusK)));
			task.setLastSentSubscriber((String) ob.get(LAST_SUBSCRIBER));
			task.setTotalSent((Integer) ob.get(broadcastTotalSentK));
			task.setTotalErrors((Integer) ob.get(broadcastTotalErrorsK));
			task.setStartedTime((Date) ob.get(startDateK));
			task.setEndTime((Date) ob.get(endDateK));
            return task;
		}
        return null;
	}

	@Override
	public void create(Task task) throws DbException {
		BasicDBObject query = getPersistenceObject(task);
		getTaskCollection().save(query);
	}

	@Override
	public void update(Task task) throws DbException {
		BasicDBObject query = new BasicDBObject();
		query.put(correlationIdK, task.getCorrelationId());
		BasicDBObject update = getPersistenceObject(task);
//		getTaskCollection().findAndModify(query, update);
		getTaskCollection().update(query, update, true, false);
	}

	private BasicDBObject getPersistenceObject(Task task) {
		BasicDBObject obj = new BasicDBObject();
		obj.put(correlationIdK, task.getCorrelationId());
		obj.put(appIdK, task.getAppId());
		obj.put(statusK, task.getStatus().name());
		obj.put(LAST_SUBSCRIBER, task.getLastSentSubscriber());
		obj.put(broadcastTotalSentK, task.getTotalSent());
		obj.put(broadcastTotalErrorsK, task.getTotalErrors());
		obj.put(startDateK, task.getStartedTime());
		obj.put(endDateK, task.getEndTime());
		return obj;
	}
	
	private DBCollection getTaskCollection() {
		if (collection == null) {
			collection = mongoTemplate.getCollection(TASK_COLLECTION);
			collection.ensureIndex(appIdK);
		}
		return collection;
	}
}
