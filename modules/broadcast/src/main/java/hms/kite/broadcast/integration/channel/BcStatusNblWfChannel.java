/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.broadcast.integration.channel;

import hms.kite.sms.channel.GenericWfChannel;
import hms.kite.util.IdGenerator;
import org.apache.log4j.NDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.util.KiteErrorBox.invalidRequestErrorCode;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.wfengine.impl.ResponseBuilder.*;

/**
 * $LastChangedDate: 2011-08-04 14:54:36 +0530 (Thu, 04 Aug 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 75577 $
 */
public class BcStatusNblWfChannel extends GenericWfChannel {

	private static final Logger logger = LoggerFactory.getLogger(BcStatusNblWfChannel.class);
	
	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        try {
            if(!validate(requestContext)) {
                logger.warn("Validation failed for the request [{}]", requestContext.get(requestK));
                return convert2NblErrorResponse(generate(invalidRequestErrorCode,
                        false, requestContext));
            }
            addParameters(requestContext);
            NDC.push((String) requestContext.get(requestK).get(correlationIdK));
            workflow.executeWorkflow(requestContext);
            return generateBcSRNblResponse(requestContext);
        } catch (Exception e) {
            logger.error("Exception occurred while executing flow", e);
            return convert2NblErrorResponse(generate(requestContext, e));
        } finally {
            NDC.pop();
        }
	}

	private void addParameters(Map<String, Map<String, Object>> requestContext) {
		requestContext.get(requestK).put(receiveTimeK, Long.toString(System.currentTimeMillis()));
		requestContext.get(requestK).put(correlationIdK, IdGenerator.generate());
		requestContext.get(requestK).put(ncsTypeK, smsK);
		requestContext.get(requestK).put(appIdK, requestContext.get(requestK).get(applicationIdK));
		requestContext.get(requestK).put(messageIdK, requestContext.get(requestK).get(messageIdNblK));
	}

	private boolean validate(Map<String, Map<String, Object>> requestContext) {
		return (isValidParameterValue(requestContext, applicationIdK) 
				&& isValidParameterValue(requestContext, passwordK) 
				&& isValidParameterValue(requestContext, messageIdNblK));
	}
	
	private boolean isValidParameterValue(Map<String, Map<String, Object>> requestContext, String key) {
		if (requestContext.get(requestK).containsKey(key) 
				&& requestContext.get(requestK).get(key) != null
				&& !requestContext.get(requestK).get(key).toString().isEmpty()) {
			return true;
		}
		logger.warn("Invalid value [{}] for request parameter [{}]", requestContext.get(requestK).get(key), key);
		return false;
	}
	
	private Map<String, Object> generateBcSRNblResponse(Map<String, Map<String, Object>> requestContext) {
		Map<String, Object> resp = convert2NblErrorResponse(generateResponse(requestContext));

		resp.put(broadcastStatusNblK, requestContext.get(requestK).get(broadcastStatusK));
		resp.put(broadcastTotalSentK, requestContext.get(requestK).get(broadcastTotalSentK));
		resp.put(broadcastTotalErrorsK, requestContext.get(requestK).get(broadcastTotalErrorsK));
		return resp;
	}
	
}
