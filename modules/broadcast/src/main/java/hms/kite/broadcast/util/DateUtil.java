/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.broadcast.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

public final class DateUtil {

	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    private DateUtil() {
        
    }
	
	public static Date parseDate(String dateString) throws ParseException {
		Date dt = null;
		if(dateString != null) {
			dt = DATE_FORMAT.parse(dateString);
		}
		return dt;
	}
	
	public static String formatDate(Date date) {
		String str = null;
		if(date != null) {
			str = DATE_FORMAT.format(date);
		}
		return str;
	}
	
	public static Date parseDate(String dateString, String pattern) throws ParseException {
		Date dt = null;
		if(dateString != null) {
			SimpleDateFormat dateFormat = simpleDateFormatFromPattern(pattern);
			dt = dateFormat.parse(dateString);
		}
		return dt;
	}
	
	public static String formatDate(Date date, String pattern) {
		String str = null;
		if(date != null) {
			SimpleDateFormat dateFormat = simpleDateFormatFromPattern(pattern);
			str = dateFormat.format(date);
		}
		return str;
	}
	
	private static SimpleDateFormat simpleDateFormatFromPattern(String pattern) {
		return new SimpleDateFormat(pattern);
	}
}
