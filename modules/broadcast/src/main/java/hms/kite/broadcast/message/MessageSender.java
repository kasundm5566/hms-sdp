/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.broadcast.message;

import hms.kite.util.SdpException;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate: 2011-07-27 19:03:23 +0530 (Wed, 27 Jul 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 75318 $
 */
public class MessageSender {

    private final static Logger logger = LoggerFactory.getLogger(MessageSender.class);
    private List<String> retriableStatusCodes;
    private long retryDelay;
    private int maxRetryLimit;
    private Channel smsChannel;
    private Channel wapChannel;


    public boolean sendMessage(Map<String, Object> request, TpsEnforcer tpsEnforcer) {

        tpsEnforcer.enforce();

        logger.trace("Broadcast message is [{}]", request);

        Map<String, Object> resp = dispatchToEndPoint(request);

        logResponse(request, resp);

        boolean status = Boolean.parseBoolean((String) resp.get(statusK));
         //TODO: Handle partial failure of submit-multi message
        if (!status) {
            String statusCode = (String) resp.get(statusCodeK);
            logger.info("Fail to send broadcast message [{}] status code [{}]", request, statusCode);
            int retryCount = getRetryCount(request);

            if(retryNeeded(statusCode) && retryCountNotExceeded(retryCount)) {
                logger.info("Retry sending message [{}], retry count[{}]", request, retryCount);
                delayBeforeRetry();
                request.put(retryContK, Integer.toString(++retryCount));
                return sendMessage(request, tpsEnforcer);
            } else {
                logger.info("Ignore sending broadcast message [{}]. try count exceeded", request, retryCount);
                return false;
            }
        } else {
            logger.info("Message successfully sent");
        }
        
        return true;
    }

    private void logResponse(Map<String, Object> request, Map<String, Object> resp) {
        logger.trace("Response [{}] for broadcast request [{}]", resp, request);

        List<Map<String, Object>> recipientResponses = (List<Map<String, Object>>) request.get(recipientsK);
        StringBuilder sb = new StringBuilder(300);
        if (recipientResponses != null) {
            for (Map<String, Object> r : recipientResponses) {
                sb.append("[msisdn[").append(r.get(recipientAddressK)).append("], ");
                sb.append("status[").append(r.get(recipientAddressStatusK)).append("]]");
            }
        }
        logger.info("Status of individual message dispatches[{}]", sb.toString());
    }

    private int getRetryCount(Map<String, Object> request) {
        String retryCount = (String) request.get(retryContK);
        if (null != retryCount) {
            return Integer.parseInt(retryCount);
        }

        return 0;
    }

    private boolean retryCountNotExceeded(int retryCount) {
        return maxRetryLimit >= retryCount;

    }

    private boolean retryNeeded(String statusCode) {
        return  retriableStatusCodes.contains(statusCode);
    }

    private Map<String, Object> dispatchToEndPoint(Map<String, Object> request) {
        try {
    	Map<String, Object> resp = null;
    	if(smsK.equals(request.get(ncsTypeK))) {
    		resp = smsChannel.send(request);
    	} else if(wapPushK.equals(request.get(ncsTypeK))) {
    		resp = wapChannel.send(request);
    	} else {
                logger.error("Unknown ncs-type [{}] found in broadcast message", request.get(ncsTypeK));
    	}
    	return resp;
        } catch (SdpException e) {
            logger.error("Exception occurred while broadcasting - " + e.getErrorCode() + e.getErrorDescription(), e);
            return Collections.singletonMap(statusCodeK, (Object) e.getErrorCode());
        }
    }

    private void delayBeforeRetry() {
        try {
            Thread.sleep(retryDelay);
        } catch (InterruptedException e) {
            //do nothing
        }
    }
    
    public void setRetriableStatusCodes(List<String> retriableStatusCodes) {
        this.retriableStatusCodes = retriableStatusCodes;
    }

    public void setMaxRetryLimit(int maxRetryLimit) {
        this.maxRetryLimit = maxRetryLimit;
    }

    public void setSmsChannel(Channel smsChannel) {
        this.smsChannel = smsChannel;
    }

    public void setWapChannel(Channel wapChannel) {
        this.wapChannel = wapChannel;
    }

    public void setRetryDelay(long retryDelay) {
        this.retryDelay = retryDelay;
    }
}
