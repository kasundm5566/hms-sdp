/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.broadcast.repo.mongo;

import com.mongodb.*;

import hms.kite.broadcast.domain.BroadcastStatus;
import hms.kite.broadcast.repo.BroadcastMessageRepository;
import hms.kite.datarepo.mongodb.MongoDbUtils;
import hms.kite.util.KiteKeyBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.util.*;

import static hms.kite.broadcast.util.DateUtil.parseDate;
import static hms.kite.util.KiteKeyBox.*;

public class BroadcastMessageRepositoryImpl extends MongoDbUtils implements BroadcastMessageRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(BroadcastMessageRepositoryImpl.class);
    private static final String MESSAGE_COLLECTION = "broadcast_message";
	private static final String APP_ID = "appid";
	private static final String RECEIVED_TIME = "received_time";
    private DBCollection collection;

    private int expiredBCMessageBatchSize = 5;

	@Override
	public void create(Map<String, Map<String, Object>> requestContext) throws ParseException {
		Map<String, Object> request = requestContext.get(requestK);
		Map<String, Object> app = requestContext.get(KiteKeyBox.appK);
		Map<String, Object> sp = requestContext.get(KiteKeyBox.spK);
		BasicDBObject query = new BasicDBObject();
		request.put(nameK, app.get(nameK));
		request.put(spIdK, app.get(spIdK));
		request.put(spNameK, sp.get(coopUserNameK));
		query.put(messageK, request);
		query.put(statusK, BroadcastStatus.PENDING.name());
		query.put(startDateK, parseDate((String) request.get(startDateK)));
		query.put(endDateK, parseDate((String) request.get(endDateK)));
		query.put(APP_ID, request.get(appIdK));
		query.put(correlationIdK, request.get(correlationIdK));
		query.put(RECEIVED_TIME, new Date());

		getMessageCollection().save(query);
	}

	@Override
	public void update(Map<String, Object> request) throws ParseException {
		BasicDBObject query = new BasicDBObject();
		query.put(correlationIdK, request.get(correlationIdK));

		BasicDBObject update = new BasicDBObject();
		update.put(startDateK, parseDate((String) request.get(startDateK)));
		update.put(endDateK, parseDate((String) request.get(endDateK)));
		update.put(statusK, request.get(statusK));
		update.put(correlationIdK, request.get(correlationIdK));
		update.put(messageK, request);

		getMessageCollection().findAndModify(query, update);
	}

    @Override
	public void updateStatus(String requestCorrelationId, BroadcastStatus status) throws ParseException {
        LOGGER.debug("Updating status of broadcast message[{}] to [{}]", requestCorrelationId, status);
        getMessageCollection().update(
                new BasicDBObject(correlationIdK, requestCorrelationId),
                new BasicDBObject("$set", new BasicDBObject(statusK, status.name())),
                false, false, WriteConcern.SAFE);
	}

	@Override
    public List<Map<String, Object>> findPendingMessages(int batchSize) {

		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		BasicDBObject query = new BasicDBObject();
		Calendar cal = Calendar.getInstance();
		Date now = cal.getTime();
		QueryBuilder qb = new QueryBuilder();

		qb.or(new QueryBuilder().put(startDateK).is(null).put(endDateK).is(null).get(),
				new QueryBuilder().put(startDateK).lessThanEquals(now).put(endDateK).greaterThanEquals(now).get(),
				new QueryBuilder().put(startDateK).notEquals(null).put(endDateK).is(null).get(),
				new QueryBuilder().put(startDateK).is(null).put(endDateK).greaterThanEquals(now).get());
		query.putAll(qb.get());

		query.put(statusK, BroadcastStatus.PENDING.name());

        DBCursor cursor = getMessageCollection()
                .find(query)
                .sort(new BasicDBObject(RECEIVED_TIME, 1))
                .limit(batchSize);

		while(cursor.hasNext()) {
			list.add((Map<String, Object>) cursor.next().toMap().get(messageK));
		}
		return list;
	}

	@Override
	public List<Map<String, Object>> findExpiredMessages() {
        List<Map<String, Object>> expiredList = new ArrayList<Map<String, Object>>();

		List<String> statusList = new ArrayList<String>();
		statusList.add(BroadcastStatus.PENDING.name());
		statusList.add(BroadcastStatus.STARTED.name());

		Calendar cal = Calendar.getInstance();
		Date now = cal.getTime();
		BasicDBObject query = new BasicDBObject();
		query.put(statusK, new BasicDBObject("$in", statusList));
		query.put(endDateK, new BasicDBObject("$lt", now));


        DBCursor cursor = getMessageCollection().find(query).limit(expiredBCMessageBatchSize);
		while(cursor.hasNext()) {
            expiredList.add((Map<String, Object>) cursor.next().toMap().get(messageK));
		}
        return expiredList;
	}

	@Override
    public List<Map<String, Object>> findAlreadyStarted(int batchSize) {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

        BasicDBObject query = new BasicDBObject();
        query.put(statusK, BroadcastStatus.STARTED.name());

        DBCursor cursor = getMessageCollection().find(query).limit(batchSize);

        while(cursor.hasNext()) {
            list.add((Map<String, Object>)cursor.next().toMap().get(messageK));
        }
        return list;
    }

    @Override
    public void updateExpired(Map<String, Object> request) throws ParseException {
        BasicDBObject query = new BasicDBObject(correlationIdK, request.get(correlationIdK));
        BasicDBObject update = new BasicDBObject(statusK, BroadcastStatus.EXPIRED.name());
        getMessageCollection().update(query, new BasicDBObject("$set", update));
    }

	@Override
	public BroadcastStatus findStatusByCorrelationId(String correlationId) {
		BasicDBObject query = new BasicDBObject(correlationIdK, correlationId);
		BasicDBObject fields = new BasicDBObject(statusK, 1);
		DBObject result = getMessageCollection().findOne(query, fields);
		BroadcastStatus status = null;
		if(result != null) {
			status = BroadcastStatus.valueOf((String) result.get(statusK));
		}
		return status;
	}

	private DBCollection getMessageCollection() {
		if(collection == null) {
			collection = mongoTemplate.getCollection(MESSAGE_COLLECTION);
			BasicDBObject keys = new BasicDBObject();
			keys.append(correlationIdK, 1);
			keys.append(statusK, 1);
			keys.append(startDateK, 1);
			keys.append(endDateK, 1);
			collection.ensureIndex(keys);
		}
		return collection;
	}

    public void setExpiredBCMessageBatchSize(int expiredBCMessageBatchSize) {
        this.expiredBCMessageBatchSize = expiredBCMessageBatchSize;
    }
}
