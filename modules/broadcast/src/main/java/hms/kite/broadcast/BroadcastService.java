/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.broadcast;

import hms.kite.broadcast.repo.BroadcastMessageRepository;
import hms.kite.broadcast.util.DateUtil;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;

import static hms.kite.util.KiteErrorBox.systemErrorCode;
import static hms.kite.util.KiteErrorBox.invalidRequestErrorCode;
import static hms.kite.util.KiteKeyBox.*;

/**
 *
 * Parameters contained in the message passed into send(Message) are
 * correlation-id, start-date, end-date, sender-address,
 *  tps, app-id and default-operator.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

public class BroadcastService extends BaseChannel {

	private BroadcastMessageRepository broadcastMessageRepository;
	private final static Logger logger = LoggerFactory.getLogger(BroadcastService.class);


    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
			logger.debug("Broadcast message received [{}] ", requestContext.get(requestK));
		try {
			if(isDateRangeValid(requestContext)) {
				broadcastMessageRepository.create(requestContext);
				if(logger.isTraceEnabled()) {
					logger.trace("Broadcast message stored successfully");
				}
				return ResponseBuilder.generateSuccess(requestContext);
			} else {
				return ResponseBuilder.generate(invalidRequestErrorCode, false, requestContext);
			}
		} catch(Throwable ex) {
			logger.error("Failed to persist message: ", ex);
			return ResponseBuilder.generate(systemErrorCode, false, requestContext);
		}
    }

    private boolean isDateRangeValid(Map<String, Map<String, Object>> requestContext) throws ParseException {
    	Date now = new Date();
    	Date startDate = null;
    	Date endDate = null;
    	boolean valid = false;
    	if(requestContext.get(requestK).get(startDateK) != null
    			&& requestContext.get(requestK).get(endDateK) != null) {
    		startDate = DateUtil.parseDate(requestContext.get(requestK).get(startDateK).toString());
        	endDate = DateUtil.parseDate(requestContext.get(requestK).get(endDateK).toString());
        	if(endDate.after(now) && startDate.after(now) && startDate.before(endDate)) {
        		valid = true;
        	}
    	} else if(requestContext.get(requestK).get(startDateK) == null
    			&& requestContext.get(requestK).get(endDateK) != null) {
    		endDate = DateUtil.parseDate(requestContext.get(requestK).get(endDateK).toString());
    		if(endDate.after(now)) {
    			valid = true;
    		}
    	} else if(requestContext.get(requestK).get(endDateK) == null
    			&& requestContext.get(requestK).get(startDateK) != null) {
    		startDate = DateUtil.parseDate(requestContext.get(requestK).get(startDateK).toString());
    		if(startDate.after(now)) {
    			valid = true;
    		}
    	} else {
    		valid = true;
    	}
    	return valid;
    }

	public void setBroadcastMessageRepository(BroadcastMessageRepository broadcastMessageRepository) {
		this.broadcastMessageRepository = broadcastMessageRepository;
	}

}
