/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.broadcast.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;

import hms.commons.IdGenerator;

/**
 * $LastChangedDate: 2011-07-20 14:01:33 +0530 (Wed, 20 Jul 2011) $
 * $LastChangedBy: ruwant $ $LastChangedRevision: 75106 $
 */
public class BroadcastMessageBuilder {

    private static final Logger logger = LoggerFactory.getLogger(BroadcastMessageBuilder.class);

    /**
     * This method adds the recipient address to the method
     *
     * @param broadcastRequest : the message to be sent (request of the request context)
     * @param subscriberList   : this is actually the msisdn of the subscriber
     * @return : new message with the recipient address list.
     */
    public Map<String, Object> createMessage(Map<String, Object> broadcastRequest, List<String> subscriberList) {
        String requestCorrelationId = IdGenerator.generateId();
        List<Recipient> recipientsList = new ArrayList<Recipient>();
        for (String subscriber : subscriberList) {
            String correlationId = IdGenerator.generateId();
            Recipient r = new Recipient(subscriber, correlationId);
            recipientsList.add(r);
        }

        broadcastRequest.remove(addressK);
        broadcastRequest.put(recipientsK, getRecipients(recipientsList));
        broadcastRequest.put(correlationIdK, requestCorrelationId);
        broadcastRequest.put(retryContK, Integer.toString(0));

        logger.debug("Created broadcast request[{}]", broadcastRequest);
        return broadcastRequest;
    }

    private List<Map<String, String>> getRecipients(List<Recipient> recipientsList) {
        List<Map<String, String>> recipients = new ArrayList<Map<String, String>>();
        for (Recipient r : recipientsList) {
            Map<String, String> recipient = new HashMap<String, String>();
            recipient.put(recipientAddressK, r.getMsisdn());
            recipient.put(chargingTrxIdK, r.getCorrelationId());
            recipients.add(recipient);
        }

        return recipients;
    }


    private class Recipient {
        private final String msisdn;
        private final String correlationId;

        public Recipient(String msisdn, String correlationId) {
            this.msisdn = msisdn;
            this.correlationId = correlationId;
        }

        public String getMsisdn() {
            return msisdn;
        }

        public String getCorrelationId() {
            return correlationId;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Recepient");
            sb.append("[msisdn='").append(msisdn).append('\'');
            sb.append(", correlationId='").append(correlationId).append('\'');
            sb.append(']');
            return sb.toString();
        }
    }
}
