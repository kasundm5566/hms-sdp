SBL-SDP Ussd MO Request/Response Formats
========================================
ussd-mo
-------------

	message= sports
	recipient-address=141
	ncs-id=ussd
	operator-id=safaricom
	encoding=0
	correlation-id=111080212300001
	sender-address=337788665522
	app-id=APP_00001 (app-id is only available with MO-CONT messages)
	ussd-op=MO-INIT, MO-CONT
	session-id=1234567890

ussd-mo-resp
-------------

	correlation-id=111080212300001
	session-id=1234567890
	app-id=APP_00001
	status-code=SUCCESS, FAILURE
	status-description=

ussd-mt
-------------

	message= your menu is here 1).Option 1 2).Option 2 3).Exit
	recipient-address=337788665522
	ncs-id=ussd
	operator-id=safaricom
	encoding=0
	correlation-id=111080212300001
	sender-address=141
	app-id=APP_00001
	ussd-op=MT-CONT, MT-END
	session-id=1234567890


ussd-mt-resp
-------------

	correlation-id=111080212300001
	session-id=1234567890
	app-id=APP_00001
	status-code=SUCCESS, FAILURE, TIMEOUT
	status-description=


Sample MO Requests
========================================
MO-INIT

	wget --post-data="ncs-type=ussd&operator-id=safaricom&message=test message&recipient-address=8546&correlation-id=1311576676710&sender-address=254701234567&encoding=0&ussd-op=mo-init&session-id=12345678" http://core.sdp:8500/ussd/receive
	
MO-CONT

	wget --post-data="ncs-type=ussd&operator-id=safaricom&message=test message&recipient-address=8546&correlation-id=1311576676710&sender-address=254701234567&encoding=0&ussd-op=mo-cont&session-id=12345678&app-id=APP_000001" http://core.sdp:8500/ussd/receive


SDP-NBL Ussd MO Request/Response formats
========================================

ussd-mo
------------

	message= your menu is here 1).Option 1 2).Option 2 3).Exit
	recipient-address=337788665522
	correlation-id=111080212300001
	sender-address=337788665522
	app-id=APP_00001
	ussd-op=MO-INIT, MO-CONT
	session-id=1234567890
	encoding=0
	
	
ussd-mt
------------

	message= sports
	recipient-address=141
	correlation-id=111080212300001
	sender-address=337788665522
	app-id=APP_00001
	ussd-op=MT-INIT, MT-CONT, MT-END
	session-id=1234567890
	encoding=0
	
SBL URL configuration for USSD connecor
=======================================	
Execute following on kite mondo db.	
	db.system_configurations.save({ "_id" : "sbl-ussd-receiver-addresses", "value" : { "safaricom" : "http://sbl.core.sdp:8000/ussd/send" } });
	
Ussd Ncs_Sla Structure
========================================


{
  "app-id": "APP_000001",
  "ncs-type": "ussd",
  "operator": "safaricom",
  "mo": {
    "tpd": "100",
    "tps": "10"
    "charging": {
      "type": "free"
    },
    "connection-url": "http://192.168.0.70:17000/ussd/receive",
  },
  "mt": {
    "tpd": "100",
    "tps": "10"
    "charging": {
      "type": "free"
    },
  },
  "session": {
    "mt-allowed": true,
    "mo-allowed": true
    "charging": {
      "amount": "10",
      "method": "operator-charging",
      "service-code": "SVC-345",
      "party": "subscriber",
      "type": "flat"
    },
  },
  "updated-date": "Wed Nov 09 2011 13:30:09 GMT+0530 (IST)",
  "created-by": "hsenid",
  "created-date": "Wed Nov 09 2011 13:30:09 GMT+0530 (IST)",
  "status": "pending-approve",
}