/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hms.kite.ussd.channel;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
public class UssdMoNcsWfChannel extends BaseChannel {

    private static final Logger LOGGER = LoggerFactory.getLogger(UssdMoNcsWfChannel.class);
    @Autowired private Workflow ussdMoNcsWf;

	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
		LOGGER.debug("request-context received: [{}]", requestContext);
		return executeWf(requestContext);
	}

	private Map<String, Object> executeWf(Map<String, Map<String, Object>> requestContext) {
		try {
			ussdMoNcsWf.executeWorkflow(requestContext);
			return ResponseBuilder.generateResponse(requestContext);
		} catch(Exception e) {
			LOGGER.error("Error occured while executing the workflow!", e);
			return ResponseBuilder.generate(requestContext, e);
		}
	}

}
