/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.ussd.channel;

import static hms.kite.util.KiteKeyBox.operatorK;
import static hms.kite.util.KiteKeyBox.recipientAddressOptypeK;
import static hms.kite.util.KiteKeyBox.recipientsK;
import static hms.kite.util.KiteKeyBox.requestK;
import static hms.kite.util.KiteKeyBox.unknownK;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 *
 */
public class UssdAoNcsWfChannel extends BaseChannel {

    @Autowired private Workflow ussdAoNcsWf;

	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
		setOperator(requestContext.get(requestK));
        ussdAoNcsWf.executeWorkflow(requestContext);
		return ResponseBuilder.generateResponse(requestContext);
	}


	private void setOperator(Map<String, Object> request) {

		if (request.containsKey(recipientsK)) {

			List<Map<String, String>> recipientList = (List<Map<String, String>>) request.get(recipientsK);
			final Map<String, String> recipient = recipientList.get(0);

			if (null != recipient.get(recipientAddressOptypeK)
					&& !unknownK.equals(recipient.get(recipientAddressOptypeK))) {
				request.put(operatorK,recipient.get(recipientAddressOptypeK));
			}
		}
	}

}
