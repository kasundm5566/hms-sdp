/*
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.ussd.wf;

import hms.kite.util.KeyNameSpaceResolver;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.*;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 */
public class NcsUssdAoWorkflow extends WrappedGeneratedWorkflow implements Workflow {


    @Autowired private Channel ussdMtChannel;
    @Autowired private Channel chargingService;
    @Autowired private Channel blockedMsisdnValidationService;
    @Autowired private Channel whitelistService;
    @Autowired private Channel tpsThrottlingChannel;
    @Autowired private Channel tpdThrottlingChannel;
    @Autowired private Channel mainThrottlingChannel;
    @Autowired private Channel provNcsSlaChannel;
    @Autowired private Channel subscriptionCheckChannel;

    private boolean aoChargingEnabled;

    @Override
	protected Workflow generateWorkflow() {
		ServiceImpl cancel = new ServiceImpl("commit-reservation");
        cancel.setChannel(chargingService);
		cancel.addServiceParameter(requestTypeK, cancelCreditK);

		ServiceImpl commit = new ServiceImpl("commit-reservation");
        commit.setChannel(chargingService);
		commit.addServiceParameter(requestTypeK, commitK);

		ServiceImpl ussdMt = new ServiceImpl("ussd-mt-channel");
		ussdMt.setChannel(ussdMtChannel);
		ussdMt.setOnSuccess(commit);
		ussdMt.onDefaultError(cancel);

		ServiceImpl reserve = new ServiceImpl("reserve-credit");
        reserve.setChannel(chargingService);
		reserve.addServiceParameter("request-type", "reserve-credit");
		reserve.setOnSuccess(ussdMt);

		ServiceImpl blockedMsisdnChannel = new ServiceImpl("blocked-msisdn-channel");
		blockedMsisdnChannel.setChannel(blockedMsisdnValidationService);
        if (aoChargingEnabled) {
            blockedMsisdnChannel.setOnSuccess(reserve);
        } else {
            blockedMsisdnChannel.setOnSuccess(ussdMt);
        }

		ServiceImpl whitelist = new ServiceImpl("white-list");
		whitelist.setChannel(whitelistService);
		whitelist.setOnSuccess(blockedMsisdnChannel);

		EqualExpression appStatus = new EqualExpression(appK, statusK, limitedProductionK);
		Condition condition = new Condition(appStatus, unknownErrorCode);

		ServiceImpl whitelistApplied = new ServiceImpl("whitelist.eligibility.service", condition);
		whitelistApplied.setOnSuccess(whitelist);
		whitelistApplied.onDefaultError(blockedMsisdnChannel);

		ServiceImpl ncsTps = new ServiceImpl("tps-throttling-channel");
		ncsTps.setChannel(tpsThrottlingChannel);
		ncsTps.setOnSuccess(whitelistApplied);
		ncsTps.setServiceParameterKeys("ncs-ussd-mt");

		ServiceImpl spTps = new ServiceImpl("tps-throttling-channel");
		spTps.setChannel(tpsThrottlingChannel);
		spTps.setOnSuccess(ncsTps);
		spTps.setServiceParameterKeys("sp-ussd-mt");

		ServiceImpl ncsTpd = new ServiceImpl("tpd-throttling-channel");
		ncsTpd.setChannel(tpdThrottlingChannel);
		ncsTpd.setOnSuccess(spTps);
		ncsTpd.setServiceParameterKeys("ncs-ussd-mt");

		ServiceImpl spTpd = new ServiceImpl("tpd-throttling-channel");
		spTpd.setChannel(tpdThrottlingChannel);
		spTpd.setOnSuccess(ncsTpd);
		spTpd.setServiceParameterKeys("sp-ussd-mt");

        ServiceImpl systemTps = new ServiceImpl("main-throttling-channel");
        systemTps.setChannel(mainThrottlingChannel);
        systemTps.setOnSuccess(spTpd);
        systemTps.setServiceParameterKeys("system-tps");

        ServiceImpl subscriptionCheck = new ServiceImpl("subscription.check");
        subscriptionCheck.setChannel(subscriptionCheckChannel);
        subscriptionCheck.setOnSuccess(systemTps);

        EqualExpression subscriptionRequired = new EqualExpression(ncsK, subscriptionRequiredK, true);
        Condition subscriptionAppliedCondition = new Condition(subscriptionRequired, unknownErrorCode);

        ServiceImpl subscriptionApplied = new ServiceImpl("subscription.check.validation", subscriptionAppliedCondition);
        subscriptionApplied.setOnSuccess(subscriptionCheck);
        subscriptionApplied.onDefaultError(systemTps);

        ServiceImpl senderAddressValidationService = createSenderAddressValidationService();
        senderAddressValidationService.setOnSuccess(subscriptionApplied);

        ServiceImpl ncsValidation = createNcsValidationService();
        ncsValidation.setOnSuccess(senderAddressValidationService);

		ServiceImpl ncsSlaChannel = new ServiceImpl("prov.ncs.sla.channel");
		ncsSlaChannel.setChannel(provNcsSlaChannel);
		ncsSlaChannel.setOnSuccess(ncsValidation);


        return new WorkflowImpl(ncsSlaChannel);
    }

    private static ServiceImpl createNcsValidationService() {
        EqualExpression reqMtInit = new EqualExpression(requestK, ussdOpK, mtInitK);

        EqualExpression ncsMtInitAllowed = new EqualExpression(true, ncsK, mtAllowedK);

        AndExpression mtInit = new AndExpression(reqMtInit, ncsMtInitAllowed);

        NotExpression notMtInit = new NotExpression(reqMtInit);

        OrExpression validateMtFlow = new OrExpression(mtInit, notMtInit);

        Condition ncsAllowedCondition = new Condition(validateMtFlow, mtNotAllowedErrorCode);
        return new ServiceImpl("ncs.state.validation", ncsAllowedCondition);
    }

    private static ServiceImpl createSenderAddressValidationService() {
        ContainsExpression senderAddressAvailable = new ContainsExpression(requestK, senderAddressK);
        NotExpression not = new NotExpression(senderAddressAvailable);
        InExpression alias = new InExpression(KeyNameSpaceResolver.nameSpacedKey(requestK, senderAddressK),
                KeyNameSpaceResolver.nameSpacedKey(ncsK, mtK, aliasingK));
        EqualExpression defaultSenderAddress = new EqualExpression(KeyNameSpaceResolver.nameSpacedKey(requestK,
                senderAddressK), KeyNameSpaceResolver.nameSpacedKey(ncsK, mtK, defaultSenderAddressK));

        OrExpression or = new OrExpression(not, alias, defaultSenderAddress);

        Condition condition = new Condition(or, invalidSourceAddressErrorCode);
        return new ServiceImpl("sender.address.validation", condition);
	}

    @Override
    public void executeWorkflow(Map<String, Map<String, Object>> requestContext) {
        super.executeWorkflow(requestContext);
    }

    public void setAoChargingEnabled(boolean aoChargingEnabled) {
        this.aoChargingEnabled = aoChargingEnabled;
    }
}
