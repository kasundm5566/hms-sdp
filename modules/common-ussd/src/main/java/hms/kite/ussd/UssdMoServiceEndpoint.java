/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hms.kite.ussd;

import hms.kite.ussd.channel.UssdMoWfChannel;
import hms.kite.wfengine.ServiceEndpoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.appIdK;
import static hms.kite.util.KiteKeyBox.replyK;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
@Produces({ "application/json", "application/xml" })
@Consumes({ "application/json", "application/xml" })
@Path("/ussd/")
public class UssdMoServiceEndpoint extends ServiceEndpoint {
	private static final Logger logger = LoggerFactory.getLogger(UssdMoServiceEndpoint.class);

	private UssdMoWfChannel moWfChannel;

	@Produces({ "application/json" })
	@Consumes({ "application/json" })
	@POST
	@Path("/receive")
	public Map<String, Object> onMoSmsMessageSimple(Map<String, Object> msg) {
		logger.info("Received ussd mo from sbl [" + msg + "]");
		Map<String, Object> resp = moWfChannel.send(msg);
        logger.debug("Request after process[{}]", msg);
        logger.debug("Resp from core[{}]", resp);
//		resp.put(replyK, msg.get(replyK));
		resp.put(appIdK, msg.get(appIdK));
        logger.info("Sending ussd mo response to sbl [{}]", resp);
		return resp;
	}

	public void setMoWfChannel(UssdMoWfChannel moWfChannel) {
		this.moWfChannel = moWfChannel;
	}

}
