/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hms.kite.ussd.channel;

import static hms.kite.util.KiteKeyBox.replyK;
import static hms.kite.util.KiteKeyBox.requestK;
import static hms.kite.util.KiteKeyBox.statusCodeK;
import hms.kite.util.StatusDescriptionBuilder;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 *
 */
public class UssdMoReplyMapperChannel implements Channel {
    private static final Logger LOGGER = LoggerFactory.getLogger(UssdMoReplyMapperChannel.class);

    private StatusDescriptionBuilder descriptionBuilder;

    public void init() {
        descriptionBuilder = new StatusDescriptionBuilder("ussd-mo-reply");
        descriptionBuilder.init();
    }

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        String statusCode = (String) requestContext.get(requestK).get(statusCodeK);
        LOGGER.debug("Mapping status code[{}] for reply message [{}]", statusCode, requestContext);
        String reply = descriptionBuilder.createStatusDescription(statusCode, requestContext);

        if (null != reply) {
            requestContext.get(requestK).put(replyK, reply);
        }

        return ResponseBuilder.generateSuccess();
    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        //no impl needed
        return null;
    }

}