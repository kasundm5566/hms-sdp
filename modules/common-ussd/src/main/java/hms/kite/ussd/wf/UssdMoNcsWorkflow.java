/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hms.kite.ussd.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.*;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static hms.kite.util.KiteErrorBox.moNotAllowedErrorCode;
import static hms.kite.util.KiteErrorBox.unknownErrorCode;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public class UssdMoNcsWorkflow extends WrappedGeneratedWorkflow implements Workflow {

    @Autowired private Channel ussdAtChannel;
    @Autowired private Channel chargingService;
    @Autowired private Channel numberMaskingChannel;
    @Autowired private Channel tpdThrottlingChannel;
    @Autowired private Channel tpsThrottlingChannel;
    @Autowired private Channel mainThrottlingChannel;
    @Autowired private Channel whitelistService;
    @Autowired private Channel blacklistingService;
    @Autowired private Channel mnpService;
    @Autowired private Channel provNcsSlaChannel;
    @Autowired private Channel subscriptionCheckChannel;

    private boolean moChargingEnabled;

    @Override
	protected Workflow generateWorkflow() {
		ServiceImpl cancel = new ServiceImpl("commit-reservation");
        cancel.setChannel(chargingService);
		cancel.addServiceParameter(requestTypeK, "cancel-credit");

		ServiceImpl commit = new ServiceImpl("commit-reservation");
        commit.setChannel(chargingService);
		commit.addServiceParameter(requestTypeK, "commit-credit");

		ServiceImpl ussdAt = new ServiceImpl("ussd-at-channel");
		ussdAt.setChannel(ussdAtChannel);
		ussdAt.setOnSuccess(commit);
		ussdAt.onDefaultError(cancel);

		ServiceImpl reserve = new ServiceImpl("reserve-credit");
        reserve.setChannel(chargingService);
		reserve.addServiceParameter("request-type", "reserve-credit");
		reserve.setOnSuccess(ussdAt);

		ServiceImpl numberMaskingChannelService = new ServiceImpl("number-masking-channel");
		numberMaskingChannelService.setChannel(numberMaskingChannel);
		numberMaskingChannelService.addServiceParameter(maskK, true);
        if (moChargingEnabled) {
            numberMaskingChannelService.setOnSuccess(reserve);
        } else {
            numberMaskingChannelService.setOnSuccess(ussdAt);
        }

		EqualExpression maskingApplied = new EqualExpression(appK, maskNumberK, true);
		Condition maskingCondition = new Condition(maskingApplied, unknownErrorCode);

		ServiceImpl numberMaskingApplied = new ServiceImpl("masking.eligibility.validation", maskingCondition);
		numberMaskingApplied.setOnSuccess(numberMaskingChannelService);
        if (moChargingEnabled) {
            numberMaskingApplied.onDefaultError(reserve);
        } else {
            numberMaskingApplied.onDefaultError(ussdAt);
        }

		ServiceImpl ncsTpd = new ServiceImpl("tpd-throttling-channel");
		ncsTpd.setChannel(tpdThrottlingChannel);
		ncsTpd.setOnSuccess(numberMaskingApplied);
		ncsTpd.setServiceParameterKeys("ncs-ussd-mo");

		ServiceImpl spTpd = new ServiceImpl("tpd-throttling-channel");
		spTpd.setChannel(tpdThrottlingChannel);
		spTpd.setOnSuccess(ncsTpd);
		spTpd.setServiceParameterKeys("sp-ussd-mo");

		ServiceImpl ncsTps = new ServiceImpl("tps-throttling-channel");
		ncsTps.setChannel(tpsThrottlingChannel);
		ncsTps.setOnSuccess(spTpd);
		ncsTps.setServiceParameterKeys("ncs-ussd-mo");

		ServiceImpl spTps = new ServiceImpl("tps-throttling-channel");
		spTps.setChannel(tpsThrottlingChannel);
		spTps.setOnSuccess(ncsTps);
		spTps.setServiceParameterKeys("sp-ussd-mo");

        ServiceImpl systemTps = new ServiceImpl("main-throttling-channel");
        systemTps.setChannel(mainThrottlingChannel);
        systemTps.setOnSuccess(spTps);
        systemTps.setServiceParameterKeys("system-tps");

		ServiceImpl ncsSla = createNcsSlaValidationService();
		ncsSla.setOnSuccess(systemTps);

		ServiceImpl whitelist = new ServiceImpl("white-list");
		whitelist.setChannel(whitelistService);
		whitelist.setOnSuccess(ncsSla);

		EqualExpression appStatus = new EqualExpression(appK, statusK, limitedProductionK);
		Condition condition = new Condition(appStatus, unknownErrorCode);

		ServiceImpl whitelistApplied = new ServiceImpl("whitelist.eligibility.validation", condition);
		whitelistApplied.setOnSuccess(whitelist);
		whitelistApplied.onDefaultError(ncsSla);

		ServiceImpl blacklist = new ServiceImpl("black-list");
		blacklist.setChannel(blacklistingService);
		blacklist.setOnSuccess(whitelistApplied);

        ServiceImpl subscriptionCheck = new ServiceImpl("subscription.check");
        subscriptionCheck.setChannel(subscriptionCheckChannel);
        subscriptionCheck.setOnSuccess(blacklist);

        EqualExpression subscriptionRequired = new EqualExpression(ncsK, subscriptionRequiredK, true);
        Condition subscriptionAppliedCondition = new Condition(subscriptionRequired, unknownErrorCode);

        ServiceImpl subscriptionApplied = new ServiceImpl("subscription.check.validation", subscriptionAppliedCondition);
        subscriptionApplied.setOnSuccess(subscriptionCheck);
        subscriptionApplied.onDefaultError(blacklist);

		ServiceImpl numberChecking = new ServiceImpl("number-checking");
		numberChecking.setChannel(mnpService);
		numberChecking.setOnSuccess(subscriptionApplied);

		ServiceImpl ncsSlaChannel = new ServiceImpl("prov.ncs.sla.channel", numberChecking);
		ncsSlaChannel.setChannel(provNcsSlaChannel);

		return new WorkflowImpl(ncsSlaChannel);
	}

	private static ServiceImpl createNcsSlaValidationService() {
		EqualExpression reqMoInit = new EqualExpression(requestK, ussdOpK, moInitK);

		EqualExpression ncsMoInitAllowed = new EqualExpression(true, ncsK, moAllowedK);

		AndExpression moInit = new AndExpression(reqMoInit, ncsMoInitAllowed);

		NotExpression notMoInit = new NotExpression(reqMoInit);

		OrExpression validateMoFlow = new OrExpression(moInit, notMoInit);

		Condition ncsAllowedCondition = new Condition(validateMoFlow, moNotAllowedErrorCode);
		return new ServiceImpl("ncs.state.validation.service", ncsAllowedCondition);
	}

    @Override
    public void executeWorkflow(Map<String, Map<String, Object>> requestContext) {
        super.executeWorkflow(requestContext);
    }

    public void setMoChargingEnabled(boolean moChargingEnabled) {
        this.moChargingEnabled = moChargingEnabled;
    }
}
