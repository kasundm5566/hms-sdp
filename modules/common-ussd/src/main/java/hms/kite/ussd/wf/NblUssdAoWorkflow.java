/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.ussd.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.control.InExpression;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;
/**
 *
 * The only parameter required to contain in the message passed in to the send(Message) is
 * message-id, which is the correlation-id of the last message passed in by the same subscriber.
 *
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */
public class NblUssdAoWorkflow extends WrappedGeneratedWorkflow implements Workflow {

	private final String name = "ussd-ao-nbl-wf";
	private static final Logger LOGGER = LoggerFactory.getLogger(NblUssdAoWorkflow.class);

    @Autowired private Channel ussdAoNcsWfChannel;
    @Autowired private Channel mnpService;
    @Autowired private Channel numberMaskingChannel;
    @Autowired private Channel provSpSlaChannel;
    @Autowired private Channel provAppSlaChannel;
    @Autowired private Channel internalHostResolverChannel;

	@Override
	public String getName() {
		return name;
	}

	@Override
	protected Workflow generateWorkflow() {

        final ServiceImpl internalHostService = new ServiceImpl("internal.host.validation");
        internalHostService.setChannel(internalHostResolverChannel);
        internalHostService.setOnSuccess(internalWorkFlow());
        internalHostService.onDefaultError(externalWorkFlow());

        return new WorkflowImpl(internalHostService);
	}

    private ServiceImpl externalWorkFlow() {
        ServiceImpl ncsChannel = new ServiceImpl("ussd-ao-ncs-channel");
        ncsChannel.setChannel(ussdAoNcsWfChannel);

        ServiceImpl numberChecking = new ServiceImpl("number-checking");
        numberChecking.setChannel(mnpService);
        numberChecking.setOnSuccess(ncsChannel);

        ServiceImpl numberMaskingChannelService = new ServiceImpl("number-masking-channel", numberChecking);
        numberMaskingChannelService.setChannel(numberMaskingChannel);
        numberMaskingChannelService.addServiceParameter(unmaskK, true);

        EqualExpression maskingApplied = new EqualExpression(appK, maskNumberK, true);
        Condition maskingCondition = new Condition(maskingApplied, unknownErrorCode);

        ServiceImpl numberMaskingApplied = new ServiceImpl("masking.eligibility.validation", maskingCondition);
        numberMaskingApplied.setOnSuccess(numberMaskingChannelService);
        numberMaskingApplied.onDefaultError(numberChecking);

        ServiceImpl appStateValidationService = createAppStateValidationService();
        appStateValidationService.setOnSuccess(numberMaskingApplied);

        ServiceImpl spSlaValidation = createSpStateValidation();
        spSlaValidation.setOnSuccess(appStateValidationService);

        ServiceImpl authenticationService = createAuthenticationService();
        authenticationService.setOnSuccess(spSlaValidation);

        ServiceImpl hostValidation = createHostValidationService();
        hostValidation.setOnSuccess(authenticationService);

        ServiceImpl spSlaChannel = new ServiceImpl("prov.sp.sla.channel");
        spSlaChannel.setChannel(provSpSlaChannel);
        spSlaChannel.setOnSuccess(hostValidation);

        ServiceImpl appSlaChannel = new ServiceImpl("prov.app.sla.channel");
        appSlaChannel.setChannel(provAppSlaChannel);
        appSlaChannel.setOnSuccess(spSlaChannel);

        return appSlaChannel;
    }

    private ServiceImpl internalWorkFlow() {
        ServiceImpl ncsChannel = new ServiceImpl("ussd-ao-ncs-channel");
        ncsChannel.setChannel(ussdAoNcsWfChannel);

        ServiceImpl numberChecking = new ServiceImpl("number-checking");
        numberChecking.setChannel(mnpService);
        numberChecking.setOnSuccess(ncsChannel);

        ServiceImpl appStateValidationService = createAppStateValidationService();
        appStateValidationService.setOnSuccess(numberChecking);

        ServiceImpl spSlaValidation = createSpStateValidation();
        spSlaValidation.setOnSuccess(appStateValidationService);

        ServiceImpl spSlaChannel = new ServiceImpl("prov.sp.sla.channel");
        spSlaChannel.setChannel(provSpSlaChannel);
        spSlaChannel.setOnSuccess(spSlaValidation);

        ServiceImpl appSlaChannel = new ServiceImpl("prov.app.sla.channel");
        appSlaChannel.setChannel(provAppSlaChannel);
        appSlaChannel.setOnSuccess(spSlaChannel);

        return appSlaChannel;
    }

    private static ServiceImpl createAppStateValidationService() {
        InExpression statusExpression = new InExpression(appK, statusK, new Object[]{limitedProductionK, activeProductionK});
        Condition statusCondition = new Condition(statusExpression, authenticationFailedErrorCode);

        return new ServiceImpl("app.state.validation.service", statusCondition);
    }

    private static ServiceImpl createAuthenticationService() {
        EqualExpression authenticationExpression = new EqualExpression(requestK, passwordK, appK, passwordK);
        Condition condition = new Condition(authenticationExpression, authenticationFailedErrorCode);

        return new ServiceImpl("authentication.service", condition);
    }

    private static ServiceImpl createHostValidationService() {
        InExpression hostExpression = new InExpression(requestK, remotehostK, appK, allowedHostsK);
        Condition condition = new Condition(hostExpression, invalidHostIpErrorCode);

        return new ServiceImpl("host.validation.service", condition);
    }

    private static ServiceImpl createSpStateValidation() {
        EqualExpression statusExpression = new EqualExpression(spK, statusK, approvedK);
        Condition statusCondition = new Condition(statusExpression, authenticationFailedErrorCode);

        InExpression ncsAllowedExpression = new InExpression(spK, spSelectedServicesK, new Object[]{smsK});
        Condition ncsAllowedCondition = new Condition(ncsAllowedExpression, ncsNotAllowedErrorCode);

        return new ServiceImpl("sp.state.validation.service", statusCondition, ncsAllowedCondition);
    }

    @Override
    public void executeWorkflow(Map<String, Map<String, Object>> requestContext) {
        super.executeWorkflow(requestContext);
    }
}
