/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.ussd;

import hms.kite.util.NblKeyMapper;
import hms.kite.util.SdpException;
import hms.kite.util.SystemUtil;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import java.util.HashMap;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.remotehostK;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
@Produces({ "application/json" })
@Consumes({ "application/json" })
@Path("/ussd/")
public class UssdAoServiceEndpoint {

    private static final Logger logger = LoggerFactory.getLogger(UssdAoServiceEndpoint.class);

    private Channel ussdAoWfChannel;
    private boolean override;
    private Map<String, String> newNblUssdMtKeyMapper;
    private NblKeyMapper nblKeyMapper;

    @Context
    private org.apache.cxf.jaxrs.ext.MessageContext mc;
    @Context
    private ServletContext sc;

    @Produces({ "application/json" })
    @Consumes({ "application/json" })
    @POST
    @Path("/send")
    public Map<String, Object> sendService(Map<String, Object> msg,
                                           @Context javax.servlet.http.HttpServletRequest request) {
        logger.info("Received Request [{}]", msg);
        Map<String, Object> respNbl;
        try {
            addHostDetails(msg, request);
            logger.debug("Received Request [{}] from host[{}]", msg, msg.get(remotehostK));
            Map<String, Object> newMsg = nblKeyMapper.convert(msg, true);
            logger.debug("Received Converted Request [{}]", newMsg);
            Map<String, Object> resp = ussdAoWfChannel.send(newMsg);
            respNbl = nblKeyMapper.convert(resp, false);
            logger.debug("Sending response [{}]", resp);
        } catch (SdpException ex) {
            logger.error("An exception occurred: [{}]", ex);
            respNbl = new HashMap<String, Object>();
            respNbl.put(NblKeyMapper.statusCode, ex.getErrorCode());
            respNbl.put(NblKeyMapper.statusDetail, ex.getErrorDescription());
        }
        logger.info("Sending Converted response [{}]", respNbl);
        return respNbl;
    }

    private void addHostDetails(Map<String, Object> msg, HttpServletRequest request) {
        if (override || null == msg.get(remotehostK)) {
            String host = SystemUtil.findHostIp(request);
            if (null != host)
                msg.put(remotehostK, host);
        }
    }

    public void setOverride(boolean override) {
        this.override = override;
    }

    public void setUssdAoWfChannel(Channel ussdAoWfChannel) {
        this.ussdAoWfChannel = ussdAoWfChannel;
    }

    public void setNewNblUssdMtKeyMapper(Map<String, String> newNblUssdMtKeyMapper) {
        this.newNblUssdMtKeyMapper = newNblUssdMtKeyMapper;
    }

    public void init() {
        nblKeyMapper = new NblKeyMapper();
        nblKeyMapper.setNewNblApiKeyMapper(newNblUssdMtKeyMapper);
    }
}
