/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hms.kite.ussd.wf;

import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.control.Condition;
import hms.kite.wfengine.control.EqualExpression;
import hms.kite.wfengine.control.InExpression;
import hms.kite.wfengine.impl.ErrorResponseRoutingService;
import hms.kite.wfengine.impl.ServiceImpl;
import hms.kite.wfengine.impl.WorkflowImpl;
import hms.kite.wfengine.impl.WrappedGeneratedWorkflow;
import hms.kite.wfengine.transport.Channel;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public class UssdMoWorkflow extends WrappedGeneratedWorkflow implements Workflow {

    @Autowired private Channel usddMoReplyMapperChannel;
    @Autowired private Channel usddMoNcsWfChannel;
    @Autowired private Channel provSpSlaChannel;
    @Autowired private Channel provAppSlaChannel;
    @Autowired private Channel ussdMoRoutingKeyChannel;

	@Override
	protected Workflow generateWorkflow() {

		ErrorResponseRoutingService replyMapperChannel = new ErrorResponseRoutingService("ussd-mo-reply-mapper-channel");
		replyMapperChannel.setChannel(usddMoReplyMapperChannel);

		ServiceImpl moNcsChannel = new ServiceImpl("ussd-mo-ncs-wf-channel");
		moNcsChannel.setChannel(usddMoNcsWfChannel);
		moNcsChannel.onDefaultError(replyMapperChannel);

		ServiceImpl appStateValidationService = createAppSlaValidationRule();
		appStateValidationService.setOnSuccess(moNcsChannel);
		appStateValidationService.onDefaultError(replyMapperChannel);

		ServiceImpl spSlaValidation = createSpSlaValidationService();
		spSlaValidation.setOnSuccess(appStateValidationService);
		spSlaValidation.onDefaultError(replyMapperChannel);

		ServiceImpl spSlaChannel = new ServiceImpl("prov.sp.sla.channel", spSlaValidation);
		spSlaChannel.setChannel(provSpSlaChannel);
		spSlaChannel.onDefaultError(replyMapperChannel);

		ServiceImpl appSlaChannel = new ServiceImpl("prov.app.sla.channel", spSlaChannel);
		appSlaChannel.setChannel(provAppSlaChannel);
		appSlaChannel.onDefaultError(replyMapperChannel);

		ServiceImpl routingKeyChannel = new ServiceImpl("ussd-mo-routing-key-channel", appSlaChannel);
		routingKeyChannel.setChannel(ussdMoRoutingKeyChannel);
		routingKeyChannel.onDefaultError(replyMapperChannel);

		return new WorkflowImpl(routingKeyChannel);
	}

	private static ServiceImpl createAppSlaValidationRule() {
		InExpression statusExpression = new InExpression(appK, statusK, new Object[] { limitedProductionK,
				activeProductionK });
		Condition statusCondition = new Condition(statusExpression, authenticationFailedErrorCode);

		return new ServiceImpl("app.state.validation.service", statusCondition);
	}

	private static ServiceImpl createSpSlaValidationService() {
		EqualExpression statusExpression = new EqualExpression(spK, statusK, approvedK);
		Condition statusCondition = new Condition(statusExpression, authenticationFailedErrorCode);

		InExpression ncsAllowedExpression = new InExpression(spK, spSelectedServicesK, new Object[] { ussdK });
		Condition ncsAllowedCondition = new Condition(ncsAllowedExpression, ncsNotAllowedErrorCode);

		return new ServiceImpl("sp.state.validation.service", statusCondition, ncsAllowedCondition);
	}

    @Override
    public void executeWorkflow(Map<String, Map<String, Object>> requestContext) {
        super.executeWorkflow(requestContext);
    }
}
