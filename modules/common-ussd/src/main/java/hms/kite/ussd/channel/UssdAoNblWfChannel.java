/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.ussd.channel;

import hms.kite.util.IdGenerator;
import hms.kite.util.SdpException;
import hms.kite.util.SystemUtil;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;
import org.apache.log4j.NDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Map;

import static hms.kite.util.KiteErrorBox.invalidRequestErrorCode;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.NblRequestValidator.isMessageValid;
import static hms.kite.util.NblRequestValidator.validLoginDetails;
import static hms.kite.util.logging.StatLogger.printStatLog;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class UssdAoNblWfChannel extends BaseChannel {
	private static final Logger logger = LoggerFactory.getLogger(UssdAoNblWfChannel.class);

    @Autowired private Workflow ussdAoNblWf;

	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
		NDC.push((String) requestContext.get(requestK).get(correlationIdK));
		try {
			try {
				validate(requestContext);
			} catch (Exception ex) {
				logger.warn("Validation failed for the request [{}], [{}]", requestContext.get(requestK),
						ex.getMessage());
				requestContext.get(requestK).put("validation-error", ex.getMessage());
				return convert2NblErrorResponse(
						ResponseBuilder.generate(invalidRequestErrorCode, false, requestContext), requestContext);
			}

			convert2NblParameters(requestContext);

			return executeUssdAoWf(requestContext);

		} finally {
			NDC.pop();
		}
	}

	private void convert2NblParameters(Map<String, Map<String, Object>> requestContext) {
		addCorrelationId(requestContext.get(requestK));
		requestContext.get(requestK).put(ncsTypeK, ussdK);
		requestContext.get(requestK).put(directionK, mtK);
		requestContext.get(requestK).put(receiveTimeK, Long.toString(System.currentTimeMillis()));
		requestContext.get(requestK).put(appIdK, requestContext.get(requestK).get(applicationIdK));
		requestContext.get(requestK).put(sessionIdK, requestContext.get(requestK).get(sessionIdNblK));
		requestContext.get(requestK).put(ussdOpK, requestContext.get(requestK).get(ussdOpNblK));
		addRecipients(requestContext.get(requestK));
		logger.trace("SDP Request created[{}]", requestContext);

	}

	private void addRecipients(Map<String, Object> msg) {
		if (!msg.containsKey(recipientsK)) {
			msg.put(recipientsK, SystemUtil.convert2NblAddresses(Arrays.asList((String) msg.get(senderAddressNblK))));
		}
	}

	private Map<String, Object> executeUssdAoWf(Map<String, Map<String, Object>> requestContext) {
		logger.trace("Ussd MT received[{}]", requestContext);
		long start = System.currentTimeMillis();
		try {
            ussdAoNblWf.executeWorkflow(requestContext);
			return convert2NblResponse(requestContext);
		} catch (SdpException e) {
			return convert2NblErrorResponse(ResponseBuilder.generate(e.getErrorCode(), false, requestContext),
					requestContext);
		} catch (Exception e) {
			logger.error("Exception occurred while executing flow", e);
			return convert2NblErrorResponse(ResponseBuilder.generate(requestContext, e), requestContext);
		} finally {
			printStatLog(start, "USSD-MT");
		}
	}

	private Map<String, Object> convert2NblResponse(Map<String, Map<String, Object>> requestContext) {
		return convert2NblErrorResponse(ResponseBuilder.generateResponse(requestContext), requestContext);
	}

	private Map<String, Object> convert2NblErrorResponse(Map<String, Object> response,
			Map<String, Map<String, Object>> requestContext) {
		Map<String, Object> nblResp = ResponseBuilder.convert2NblErrorResponse(response);
		if (requestContext.get(requestK).containsKey(correlationIdK)) {
			nblResp.put(messageIdNblK, requestContext.get(requestK).get(correlationIdK));
		}
		return nblResp;
	}

	private void addCorrelationId(Map<String, Object> msg) {
		msg.put(correlationIdK, IdGenerator.generate());
	}

	private void validate(Map<String, Map<String, Object>> requestContext) {
		validLoginDetails(requestContext.get(requestK));
		isMessageValid(requestContext.get(requestK).get(messageK));
		isDestinationValid(requestContext.get(requestK).get(senderAddressNblK));
		isSessionValid(requestContext.get(requestK).get(sessionIdNblK));
		isUssdOpValid((String)requestContext.get(requestK).get(ussdOpNblK));
	}

	private void isUssdOpValid(String ussdOp) {
		if (isEmpty(ussdOp)) {
			throw new IllegalArgumentException("ussdOp cannot be empty");
		}

		if(!(moContK.equals(ussdOp) || mtContK.equals(ussdOp) || mtInitK.equals(ussdOp) || mtFinK.equals(ussdOp))){
			throw new IllegalArgumentException("Invalid ussdOp value [" + ussdOp +"]");
		}
	}

	private void isDestinationValid(Object destination) {
		if (destination == null || !(destination instanceof String) || ((String) destination).trim().isEmpty()) {
			throw new IllegalArgumentException("Invalid destination [" + destination + "]");
		}
	}

	private void isSessionValid(Object session) {
		if (isEmpty(session)) {
			throw new IllegalArgumentException("SessionId cannot be empty");
		}
	}

	private boolean isEmpty(Object session) {
		return session == null || session.toString().trim().isEmpty();
	}

}
