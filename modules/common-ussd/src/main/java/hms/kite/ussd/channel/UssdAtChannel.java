/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hms.kite.ussd.channel;

import hms.common.rest.util.client.AbstractWebClient;
import hms.kite.util.NblKeyMapper;
import hms.kite.util.SdpException;
import hms.kite.util.SystemUtil;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.jaxrs.client.ClientWebApplicationException;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import static hms.kite.util.KeyNameSpaceResolver.data;
import static hms.kite.util.KeyNameSpaceResolver.nameSpacedKey;
import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.wfengine.impl.ResponseBuilder.generate;

/**
 * $LastChangedDate$
 * $LastChangedBy$ $LastChangedRevision$
 *
 */
public class UssdAtChannel extends AbstractWebClient implements Channel {

	private static final Logger logger = LoggerFactory.getLogger(UssdAtChannel.class);

    private Map<String, String> newNblUssdMoKeyMapper;

	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
		try {
			WebClient webClient = createWebclient(requestContext);
			Map<String, Object> atMessage = createMessage(requestContext);
            logger.info("Sending ussd at message [{}] to url[{}]", atMessage, webClient.getBaseURI());
			Response response = webClient.post(atMessage);
			if (isSuccessHttpResponse(response)) {
                truncateResponse(response);
                logger.info("Message sent successfully. response [{}]", response.getEntity());
				return ResponseBuilder.generateSuccess();
            } else {
                logger.info("Message sending failed http status is [{}]", response.getStatus());
				return generate(atMessageFailedErrorCode, false, requestContext);
			}

		} catch (ClientWebApplicationException e) {
            logger.error("Exception occurred while sending sms message", e);
			if (null != e.getCause() && e.getCause() instanceof Fault) {
				Fault f = (Fault) e.getCause();
				if (null != f.getCause()) {
					if (f.getCause() instanceof ConnectException) {
						return generate(appConnectionRefusedErrorCode, false, requestContext);
					}
				}
			}
			return generate(requestContext, e);
		} catch (SdpException e) {
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return generate(requestContext, e);
		}
	}

	private WebClient createWebclient(Map<String, Map<String, Object>> requestContext) {
		String appUrl = findAppUrl(requestContext);
        if (null == appUrl) {
            logger.warn("App connection url is not available");
			throw new SdpException(systemErrorCode);
		}
		return createWebClient(appUrl);
	}

	private Map<String, Object> createMessage(Map<String, Map<String, Object>> requestContext) {
		Map<String, Object> params = new HashMap<String, Object>();

		if (!successCode.equals(requestContext.get(requestK).get(senderAddressStatusK))) {
            logger.warn("Sender address [{}] is not allowed and in [{}] status, Ussd mo will not be sent",
                    requestContext.get(requestK).get(senderAddressK),
                    requestContext.get(requestK).get(senderAddressStatusK));
			throw new SdpException((String) requestContext.get(requestK).get(senderAddressStatusK));
		}

		params.put(versionK, "1.0");
        params.put(applicationIdK, requestContext.get(appK).get(appIdK));
        if (requestContext.get(requestK).containsKey(senderAddressMaskedK)) {
            params.put(senderAddressNblK,
                    SystemUtil.addAddressIdentifier((String) requestContext.get(requestK).get(senderAddressMaskedK)));
        } else {
            params.put(senderAddressNblK,
                    SystemUtil.addAddressIdentifier((String) requestContext.get(requestK).get(senderAddressK)));
        }

        try {
            logger.info("Original Message: [{}]", requestContext.get(requestK).get(messageK));
            String result = URLDecoder.decode((String)requestContext.get(requestK).get(messageK), "UTF-8");
            params.put(messageK, result);
            logger.info("Ussd message successfully url-decoded: [{}]", result);
        } catch (UnsupportedEncodingException e) {
            logger.info("Exception occurred while decoding ussd message: [{}]", e);
        }

		params.put(messageIdNblK, requestContext.get(requestK).get(correlationIdK));
		if (requestContext.get(requestK).containsKey(encodingK)) {
			params.put(encodingK, requestContext.get(requestK).get(encodingK));
		} else {
			params.put(encodingK, encodingTextK);
		}
		params.put(sessionIdNblK, requestContext.get(requestK).get(sessionIdK));
		params.put(ussdOpNblK, requestContext.get(requestK).get(ussdOpK));

        NblKeyMapper nblKeyMapper = new NblKeyMapper();
        nblKeyMapper.setNewNblApiKeyMapper(newNblUssdMoKeyMapper);
        Map<String, Object> mappedParams = nblKeyMapper.convert(params, false);
        logger.debug("USSD AT Message: [{}]", params);
        logger.debug("USSD AT Converted Message: [{}]", mappedParams);
        return mappedParams;
	}

	private String findAppUrl(Map<String, Map<String, Object>> requestContext) {
		// TODO: have to get app url from mo ncs
		// return (String) data((Map)requestContext, nameSpacedKey(ncsK, moK,
		// connectionUrlK));
		return (String) data((Map) requestContext, nameSpacedKey(ncsK, moK, connectionUrlK));
	}

    private void truncateResponse(Response response) {
        try {
            readJsonResponse(response);
        } catch (Exception e) {

        }
    }

	@Override
	public Map<String, Object> send(Map<String, Object> request) {
		return null;
	}

    public void setNewNblUssdMoKeyMapper(Map<String, String> newNblUssdMoKeyMapper) {
        this.newNblUssdMoKeyMapper = newNblUssdMoKeyMapper;
    }
}
