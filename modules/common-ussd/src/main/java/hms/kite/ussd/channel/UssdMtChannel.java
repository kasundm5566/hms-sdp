/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.ussd.channel;

import hms.common.rest.util.client.AbstractWebClient;
import hms.kite.datarepo.RepositoryServiceRegistry;
import hms.kite.util.SdpException;
import hms.kite.wfengine.transport.Channel;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.jaxrs.utils.HttpUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.wfengine.impl.ResponseBuilder.generate;
import static hms.kite.wfengine.impl.ResponseBuilder.generateSuccess;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 */
public class UssdMtChannel extends AbstractWebClient implements Channel {

	private final static Logger logger = LoggerFactory.getLogger(UssdMtChannel.class);
	private Map<String, WebClient> sblClientMap;
    private Map<String, String> sblErrorCodeMapper = new HashMap<String, String>();

	public UssdMtChannel() {
	}

	public void init() {
        super.init();
		Map<String, String> sblUrlMap = (Map<String, String>) RepositoryServiceRegistry.systemConfiguration().find(
				sblUssdReceiverAddressesK);
        sblClientMap = new HashMap<String, WebClient>();
		for (Map.Entry<String, String> entry : sblUrlMap.entrySet()) {
			WebClient webClient = createWebClient(entry.getValue());
			sblClientMap.put(entry.getKey(), webClient);
		}
	}

    @Override
    public Map<String, Object> send(Map<String, Object> request) {

        Map<String, Map<String, Object>> requestContext = new HashMap<String, Map<String, Object>>();
        requestContext.put(requestK, request);

        return execute(requestContext);
    }

	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
		try {
			WebClient webClient = sblClientMap.get(requestContext.get(requestK).get(operatorK));
            Map<String, Object> sblMessage = createSblMessage(requestContext);
			logger.info("Sending ussd-mt to sbl [{}]", sblMessage);
			Response response = webClient.post(sblMessage);
			logger.debug("Response form sbl[{}]", response.getEntity());
            Map<String, Object> resp = readJsonResponse(response);
            if (isStatusSuccess(response, resp)) {
				return generateSuccess();
			} else {
				return generateErrorResponse(requestContext, response, resp);
			}

		} catch (SdpException e) {
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return generate(temporarySystemErrorCode, false, requestContext);
		}
	}

	private Map<String, Object> createSblMessage(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> ussdMt = new HashMap<String, Object>();

		createRecipientAddressList(requestContext, ussdMt);
		// sms.put(senderAddressK, getSenderAddress(requestContext));//TODO:
		// Have add the service code

		ussdMt.put(messageK, (String) requestContext.get(requestK).get(messageK));
		ussdMt.put(correlationIdK, (String) requestContext.get(requestK).get(correlationIdK));
		ussdMt.put(operatorK, (String) requestContext.get(requestK).get(operatorK));
		ussdMt.put(sessionIdK, (String) requestContext.get(requestK).get(sessionIdK));
		ussdMt.put(ussdOpK, (String) requestContext.get(requestK).get(ussdOpK));

		return ussdMt;
	}

	private void createRecipientAddressList(Map<String, Map<String, Object>> requestContext, Map<String, Object> ussdMt) {
		List<Map<String, String>> recipients = (List<Map<String, String>>) requestContext.get(requestK)
				.get(recipientsK);
		if (recipients.size() != 1) {
			throw new SdpException(invalidRequestErrorCode, "Invalid recepient count, only one recepient supported.");
		} else {
			Map<String, String> recipient = recipients.get(0);
			if (successCode.equals(recipient.get(recipientAddressStatusK))) {
				ussdMt.put(recipientAddressK, recipient.get(recipientAddressK));
			} else {
				logger.info("Recipient address [{}] is not in valid status so ignoring", recipient);
				throw new SdpException(recipientNotAllowedErrorCode, "Recipient is not in allowed state.");
			}
		}

	}

	private boolean isStatusSuccess(Response response, Map<String, Object> resp) throws IOException {
		if (isSuccessHttpResponse(response)) {
			if (resp != null && "SUCCESS".equals(resp.get(statusK))) {
				return true;
			}
		}
		return false;
	}

    private Map<String, Object> generateErrorResponse(Map<String, Map<String, Object>> requestContext,
                                                      Response response, Map<String, Object> resp) throws IOException {
        logger.debug("Response from sbl - {}", resp);
        if (resp != null) {
            String errorCode = mapSblErrorCode(resp);
            Map<String, Object> errorResponse = generate(errorCode, false, requestContext);
            String string = (String) resp.get(statusDescriptionK);
            errorResponse.put(statusDescriptionK,
                    MessageFormat.format("{0}({1})", errorResponse.get(statusDescriptionK), string));
            return errorResponse;
        } else {
            return generate(temporarySystemErrorCode, false, requestContext);
        }
    }

    private String mapSblErrorCode(Map<String, Object> resp) {
        if (null != resp && resp.containsKey(statusK)) {
            String sblErrorCode = (String) resp.get(statusK);
            logger.debug("Sbl error code {}", sblErrorCode);
            if (sblErrorCodeMapper.containsKey(sblErrorCode)) {
                return sblErrorCodeMapper.get(sblErrorCode);
            }
        }

        return temporarySystemErrorCode;
    }

    public void setSblErrorCodeMapper(Map<String, String> sblErrorCodeMapper) {
        this.sblErrorCodeMapper = sblErrorCodeMapper;
    }
}
