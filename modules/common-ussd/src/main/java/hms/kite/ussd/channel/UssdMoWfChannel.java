/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hms.kite.ussd.channel;

import static hms.kite.util.KiteKeyBox.ncsTypeK;
import static hms.kite.util.KiteKeyBox.requestK;
import static hms.kite.util.KiteKeyBox.*;
import hms.kite.datarepo.RepositoryServiceRegistry;
import hms.kite.util.IdGenerator;
import hms.kite.util.SystemUtil;
import hms.kite.wfengine.Workflow;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public class UssdMoWfChannel extends BaseChannel {
	private static final Logger LOGGER = LoggerFactory.getLogger(UssdMoWfChannel.class);

    @Autowired private Workflow ussdMoWf;

	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
		LOGGER.debug("request-context received: [{}]", requestContext);
		addParameters(requestContext);
		return executeWf(requestContext);
	}

	private Map<String, Object> executeWf(Map<String, Map<String, Object>> requestContext) {
		try {
			ussdMoWf.executeWorkflow(requestContext);
			Map<String, Object> resp = ResponseBuilder.generateResponse(requestContext);
			LOGGER.debug("Responce after channel execution[{}]", resp);
			Map<String, Object> request = requestContext.get(requestK);
			String reply = "";
			if (request != null) {
				reply = (String) request.get(replyK);
			}
			resp.put(replyK, reply);

			return resp;
		} catch (Exception e) {
			LOGGER.error("Error occured while executing the workflow!", e);
			return ResponseBuilder.generate(requestContext, e);
		}
	}

	private void addParameters(Map<String, Map<String, Object>> requestContext) {
        requestContext.get(requestK).put(recipientAddressK, requestContext.get(requestK).get(serviceCodeK));
        requestContext.get(requestK).put(ncsTypeK, ussdK);
		requestContext.get(requestK).put(operatorK, requestContext.get(requestK).get("operator-id"));
		requestContext.get(requestK).put(shortcodeK, requestContext.get(requestK).get(recipientAddressK));
		requestContext.get(requestK).put(directionK, moK);
		requestContext.get(requestK).put(receiveTimeK, Long.toString(System.currentTimeMillis()));
		requestContext.get(requestK).put(chargingTrxIdK, IdGenerator.generate());

		filterAppKeyword(requestContext.get(requestK));

		List<Map<String, String>> recipientList = SystemUtil.generateRecipientList(requestContext.get(requestK));
		LOGGER.debug("Recipient {}", recipientList);
		requestContext.get(requestK).put(recipientsK, recipientList);
	}

	static void filterAppKeyword(Map<String, Object> msg) {
		String keyword = get1stKeyword((String) msg.get(messageK));
		msg.put(keywordK, keyword);

	}

	static String get1stKeyword(String message) {
		if (message == null) {
			return "";
		}

		String keyword;
		message = message.trim().toLowerCase();
		int indexOfHash = message.indexOf("#");
		int indexOfStar = message.indexOf("*");

		if (indexOfHash > -1 && indexOfStar > -1) {
			int separatorIndex = indexOfHash > indexOfStar ? indexOfStar : indexOfHash;
			keyword = message.substring(0, separatorIndex);
		} else if (indexOfHash > -1) {
			keyword = message.substring(0, indexOfHash);
		} else if (indexOfStar > -1) {
			keyword = message.substring(0, indexOfStar);
		} else {
			keyword = message;
		}
		return keyword;
	}

}
