/*
 *   (C) Copyright 2008-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate$
 *   $LastChangedBy$
 *   $LastChangedRevision$
 *
 */
package hms.kite.ussd.channel;

import static hms.kite.util.KiteErrorBox.invalidRoutingkeyErrorCode;
import static hms.kite.util.KiteKeyBox.appIdK;
import static hms.kite.util.KiteKeyBox.exclusiveK;
import static hms.kite.util.KiteKeyBox.keywordK;
import static hms.kite.util.KiteKeyBox.operatorK;
import static hms.kite.util.KiteKeyBox.requestK;
import static hms.kite.util.KiteKeyBox.shortcodeK;
import static hms.kite.util.KiteKeyBox.*;
import hms.kite.datarepo.RepositoryServiceRegistry;
import hms.kite.util.SdpException;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 *
 */
public class UssdMoRoutingKeyChannel implements Channel {
	private static final Logger logger = LoggerFactory.getLogger(UssdMoRoutingKeyChannel.class);

	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
		String operator = (String) requestContext.get(requestK).get(operatorK);
		String serviceCode = (String) requestContext.get(requestK).get(serviceCodeK);
		String keyword = (String) requestContext.get(requestK).get(keywordK);
		String ussdOp = (String) requestContext.get(requestK).get(ussdOpK);
		Map<String, Object> routingKey = null;
		// keyword is only available with MO-INIT, MO-CONT sends the app-id
		if (moInitK.equals(ussdOp)) {
			logger.info("Ussd MO-INIT message received.");
			routingKey = findSharedShortCodeRoutingKey(operator, serviceCode, keyword);
			if (null == routingKey) {
				routingKey = findExclusiveShortCodeRoutingKey(operator, serviceCode);
			}

		} else if (moContK.equals(ussdOp)) {
			logger.info("Ussd MO-CONT message received.");
			String appId = (String) requestContext.get(requestK).get(appIdK);
			routingKey = findRoutingKeyForApp(operator, appId);
		}

		if (null == routingKey || !isProvisioned(routingKey)) {
			logger.warn("Routing key found [{}] was empty or not provisioned", routingKey);
			return ResponseBuilder.generate(invalidRoutingkeyErrorCode, false, requestContext);
		}
		logger.debug("Routing key [{}] found", routingKey);

		addRoutingKeyToContext(requestContext, routingKey);

		return ResponseBuilder.generateSuccess();
	}

	private Map<String, Object> findRoutingKeyForApp(String operator, String appId) {
		logger.debug("Trying to find routing key with operator[{}] appId[{}]", operator, appId);
		List<Map<String, Object>> routingKeysForApp = RepositoryServiceRegistry.ussdRoutingKeyRepositoryService()
				.findRoutingKeys(appId, operator);
		if (routingKeysForApp != null) {
			for (Map<String, Object> routingKey : routingKeysForApp) {
				if (ussdK.equals(routingKey.get(ncsTypeK))) {
					return routingKey;
				}
			}
		}
		return null;
	}

	private boolean isProvisioned(Map<String, Object> routingKey) {
		return (routingKey.containsKey(appIdK) && null != routingKey.get(appIdK));
	}

	private Map<String, Object> findSharedShortCodeRoutingKey(String operator, String serviceCode, String keyword) {
		logger.debug("Trying to find shared routing key with operator[{}], service code[{}], keyword[{}]",
				new Object[] { operator, serviceCode, keyword });
		return RepositoryServiceRegistry.ussdRoutingKeyRepositoryService().findSharedShortCodeRoutingKey(operator,
				serviceCode, keyword);
	}

	private Map<String, Object> findExclusiveShortCodeRoutingKey(String operator, String serviceCode) {
		logger.debug("Trying to find exclusive routing key with operator[{}], service code[{}]", new Object[] {
				operator, serviceCode });
		Map<String, Object> routingKey = RepositoryServiceRegistry.ussdRoutingKeyRepositoryService()
				.findExclusiveShortCodeRoutingKey(operator, serviceCode);

		if (null == routingKey || !isExclusiveShortCode(routingKey)) {
			throw new SdpException(invalidRoutingkeyErrorCode);
		}

		return routingKey;

	}

	private boolean isExclusiveShortCode(Map<String, Object> routingKey) {
		return (null != routingKey.get(exclusiveK) && ((Boolean) routingKey.get(exclusiveK)));
	}

	private void addRoutingKeyToContext(Map<String, Map<String, Object>> requestContext, Map<String, Object> routingKey) {
		requestContext.get(requestK).put(appIdK, routingKey.get(appIdK));
	}

	@Override
	public Map<String, Object> send(Map<String, Object> request) {
		// no impl needed
		return null;
	}

}
