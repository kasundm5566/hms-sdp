# SDP SBL-Server Developer Guide

## How send USSD MO request from browser as a user.
Please use the following url for sending USSD MO from browser.
Please do change the parameters

For Dialog      : http://127.0.0.1:3873/dialog-ussd/?MSISDN=94772334476&SC=462&SESSIONID=1209992331266121&DATE=2012-03-20&TIME=11%3A57%3A42&msg=hnb
For Etisalat    : http://127.0.0.1:3874/etisalat-ussd/?txtSender=94723743056&txtMessage=%23462*1%23&txtTime=2007-12-28\
For Mobitel     : http://127.0.0.1:8875/?source_addr=94711234561&dest_addr=462&message=3&session_id=123654789&%20ussd_op=%20init


## Modifications done for Vcity profile

Some modifications has being done to the sbl server in vcity and vcity-live profiles. This was done to support multiple
types(protocols) of sms connectors.

Virtual city has SMPP and Parlayx sms connectors. Two additional dependencies have being used for this.

    <dependency>
        <groupId>hms.commons</groupId>
        <artifactId>parlayx-sms-connector</artifactId>
        <version>0.0.4</version>
    </dependency>
    <dependency>
        <groupId>hms.commons</groupId>
        <artifactId>common-sms-connector</artifactId>
        <version>1.0.1</version>
    </dependency>

For more information on these please refer to readme files in those projects.

*  Parlayx Connector - http://gitweb.hsenidmobile.com/?p=hsenid-component/parlayx-connector.git;a=blob_plain;f=readme.md;h=c2462f22cc68e3e3a2a425430fdd48e527344a46;hb=refs/heads/master
*  Common Sms Connector - http://gitweb.hsenidmobile.com/?p=hsenid-component/common-sms-connector.git;a=blob_plain;f=readme.md;h=82f64dd10d3956cba5391161bd8cc9da7f5498ac;hb=refs/heads/master

How to test Sms flow with parlayx is described in the parlayx connectors readme file.