/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sbl.boot;

import hms.commons.SnmpLogUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Properties;


public class SblStarter {

    public static void main(String[] args) throws Exception {
        final ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("sbl-context.xml");
        context.registerShutdownHook();

        final Properties snmpMessages = (Properties) context.getBean("snmpMessages");
        logStartedMessage(snmpMessages);

        Runtime.getRuntime().addShutdownHook(new Thread(){
            @Override
            public void run() {
                logStoppedMessage(snmpMessages);
            }
        });

        context.start();
        Thread.currentThread().join();
    }

    private static void logStartedMessage(Properties snmpMessages) {
        System.out.println("#############################################################");
        System.out.println("##             SBL Server Started Successfully             ##");
        System.out.println("#############################################################");
        SnmpLogUtil.log(snmpMessages.getProperty("sbl.server.start.snmp.message"));
    }

    private static void logStoppedMessage(Properties snmpMessages) {
        System.out.println("#############################################################");
        System.out.println("##             SBL Server Stopped Successfully             ##");
        System.out.println("#############################################################");
        SnmpLogUtil.log(snmpMessages.getProperty("sbl.server.stop.snmp.message"));
    }

}
