/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.util;

import hms.commons.CollectionUtil;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static hms.kite.util.KeyNameSpaceResolver.*;
import static org.testng.Assert.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class KeyNameSpaceResolverTest {

    @Test
    public void separateKeyData() {

        Object data = data(CollectionUtil.<String, Object>newHashMap(), "0");

        assertEquals(data, null);

        Object firstObject = new Object();
        Map<String, Object> firstMap = new HashMap<String, Object>();
        Object secondObject = new Object();
        Map<String, Object> secondMap = new HashMap<String, Object>();
        Object thirdObject = new Object();

        Map<String, Object> realData = CollectionUtil.newHashMap("1", firstObject);

        assertEquals(data(realData, "1"), firstObject);

        realData.put("1", firstMap);
        firstMap.put("2", secondObject);

        assertEquals(data(realData, "1", "2"), secondObject);

        firstMap.put("2", secondMap);
        secondMap.put("3", thirdObject);

        assertEquals(data(realData, "1", "2", "3"), thirdObject);

    }

    @Test
    public void nameSpacedKeyData() {

        Object data = data(CollectionUtil.<String, Object>newHashMap(), "0");

        assertEquals(data, null);

        Object firstObject = new Object();
        Map<String, Object> firstMap = new HashMap<String, Object>();
        Object secondObject = new Object();
        Map<String, Object> secondMap = new HashMap<String, Object>();
        Object thirdObject = new Object();

        Map<String, Object> realData = CollectionUtil.newHashMap("1", firstObject);

        assertEquals(data(realData, "1"), firstObject);

        realData.put("1", firstMap);
        firstMap.put("2", secondObject);

        assertEquals(data(realData, "1.2"), secondObject);

        firstMap.put("2", secondMap);
        secondMap.put("3", thirdObject);

        assertEquals(data(realData, "1.2.3"), thirdObject);
    }

    @Test
    public void testKey() {
        assertEquals(key("test"), "test");
        assertEquals(key("test", "test2"), "test-test2");
    }

    @Test
    public void testNameSpacedKey() {
        assertEquals(key("test"), "test");
        assertEquals(nameSpacedKey("test", "test2"), "test.test2");
    }


    @Test
    public void putDataTest() {
        HashMap<String, Object> data = new HashMap<String, Object>();
        putData(data, "value", "key");
        assertEquals("value", data(data, "key"));

        try {
            data = new HashMap<String, Object>();
            putData(data, "value", "key1", "key2");
            assertEquals("value", data(data, "key1", "key2"));
            fail();
        } catch (IllegalStateException e) {
            assertNotNull(e);
        }

        data = new HashMap<String, Object>();
        putData(data, new HashMap<String, Object>(), "key1");
        putData(data, "value", "key1", "key2");
        assertEquals("value", data(data, "key1", "key2"));

        try {
            data = new HashMap<String, Object>();
            putData(data, "value", new String[]{});
            fail();
        } catch (IllegalStateException e) {
            assertNotNull(e);
        }

        data = new HashMap<String, Object>();
        putData(data, new HashMap<String, Object>(), "key1");
        putData(data, new ArrayList(), "key1", "key2");
        putData(data, "value", "key1", "key2");
        assertTrue(data(data, "key1", "key2") instanceof ArrayList);
        assertEquals(1, ((ArrayList) data(data, "key1", "key2")).size());
        assertEquals("value", ((ArrayList) data(data, "key1", "key2")).get(0));

    }
}
