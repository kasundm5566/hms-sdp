package hms.kite.util;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class NblKeyMapperTest {

    private Map<String, Object> sampleRequestNew;
    private Map<String, Object> sampleRequestOld;
    private Map<String, String> keyMapper;

    @BeforeClass
    public void setUpBeforeClass() throws Exception {
        sampleRequestNew = new HashMap<String, Object>() {{
            put("applicationId", "APP_000017");
            put("password", "9f342b92e037ca0618809fcd68b733b1");
            put("message", "This is Service Delivery Platform");
            put("sourceAddress", "383");
            put("deliveryStatusRequest", 0);
            put("encoding", 0);
            put("destinationAddresses", Arrays.asList("tel:94777777777,tel:94777777778,tel:94726699335"));
            put("recursiveTest", Arrays.asList(Collections.singletonMap("applicationId", "Application id"),
                    Collections.singletonMap("destinationAddresses", "This is destination address")));
        }};

        sampleRequestOld = new HashMap<String, Object>() {{
            put("statusDescription", "Description 1");
            put("applicationID", "Application id 1");
        }};

        keyMapper = new HashMap<String, String>() {{
            put("applicationId", "applicationID");
            put("password", "password");
            put("version", "version");
            put("destinationAddresses", "address");
            put("message", "message");
            put("sourceAddress", "sourceAddress");
            put("deliveryStatusRequest", "statusRequest");
            put("encoding", "encoding");
            put("chargingAmount", "chargingAmount");
            put("binaryHeader", "binaryHeader");
            put("requestId", "messageId");
            put("statusCode", "statusCode");
            put("statusDetail", "statusDescription");
            put("timeStamp", "timestamp");
            put("destinationResponses", "destinationResponses");
        }};

        System.out.println("Text Fixture: " + sampleRequestNew);
    }

    @Test(enabled = false)
    public void testConvert() {
        NblKeyMapper nblKeyMapper = new NblKeyMapper();
        nblKeyMapper.setNewNblApiKeyMapper(keyMapper);
        System.out.println("Output: " + nblKeyMapper.convert(sampleRequestNew, true));
        System.out.println("Output: " + nblKeyMapper.convert(sampleRequestOld, false));
    }

    @Test
    public void testPrecision() {
        BigDecimal bigDecimal = new BigDecimal(32310.2222);
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        decimalFormat.setRoundingMode(RoundingMode.UP);
        String s = decimalFormat.format(bigDecimal);
        BigDecimal decimal = new BigDecimal(s, MathContext.UNLIMITED);
        System.out.println(decimal);
    }

}
