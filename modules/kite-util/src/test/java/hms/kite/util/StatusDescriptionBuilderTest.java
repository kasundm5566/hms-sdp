/**
 *   (C) Copyright 2011-2012 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.util;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class StatusDescriptionBuilderTest {

    private StatusDescriptionBuilder builder;
    private Map<String, Map<String, Object>> context1 = new HashMap<String, Map<String, Object>>();

    @BeforeTest
    public void setup() {
        builder = new StatusDescriptionBuilder("test-status-description");
        builder.init();
        Map<String, Object> request = new HashMap<String, Object>();
        request.put("app-id", "app001");

        Map<String, Object> ncs = new HashMap<String, Object>();
        ncs.put("ncs-type", "sms");

        context1.put("request", request);
        context1.put("ncs", ncs);
    }

    @Test
    public void createStatusDescriptionSuccess() {
        String desc1 = builder.createStatusDescription("E1001", context1);
        Assert.assertEquals(desc1, "my app app001 is ok");

        desc1 = builder.createStatusDescription("E1002", context1);
        Assert.assertEquals(desc1, "my app is ok");

        desc1 = builder.createStatusDescription("E1003", context1);
        Assert.assertEquals(desc1, "my app app001 is ok for ncs sms");
    }

    @Test
    public void createStatusDescriptionFail() {
        String desc1 = builder.createStatusDescription("E1004", context1);
        Assert.assertNull(desc1);
    }
}
