package hms.kite.util;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * User: azeem
 * Date: 2/23/12
 * Time: 12:48 PM
 */
public class DeliveryStatusKeyMapperTest {

    @BeforeMethod
    public void setUp() throws Exception {

    }

    @AfterMethod
    public void tearDown() throws Exception {

    }

    @Test(dependsOnMethods = "testGetSmppStatusCode")
    public void testGetSdpStatusCode() throws Exception {
        String smppStatusCode = "DELIVRD";
        String mappedStatusCode = DeliveryStatusKeyMapper.getSdpStatusCode(smppStatusCode);
        System.out.println("SMPP Status Code: " + smppStatusCode);
        System.out.println("Mapped SDP Status Code: " + mappedStatusCode);
    }

    @Test
    public void testGetSmppStatusCode() throws Exception {
        String sdpStatusCode = "DELIVERED";
        String mappedStatusCode = DeliveryStatusKeyMapper.getSmppStatusCode(sdpStatusCode);
        System.out.println("SDP Status Code: " + sdpStatusCode);
        System.out.println("Mapped SMPP Status Code: " + mappedStatusCode);
    }
}
