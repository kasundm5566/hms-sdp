package hms.kite.util;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * User: azeem
 * Date: 3/8/12
 * Time: 6:02 PM
 */
public class ThrottlingTypeTest {

    @Test
    public void testType() {
        String value = ThrottlingType.TPD.getValue();
        String expectedValue = KiteKeyBox.tpdK;
        Assert.assertEquals(value, expectedValue);
    }
}
