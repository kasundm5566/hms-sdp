/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.util.logging;

import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ProvReportingLog {

    private static final Logger transLogger = LoggerFactory.getLogger("reporting.log");
    private static final String DATE_TIME_FORMAT = "YYYY:MM:dd HH:mm";

    public static void log(Object spId, Object spName, Object coopId, Object appId, Object appName,
                           Object appCategory, Object revenueType, Object revenuePercentage, Object ncsDetails,
                           Object status, Object dateTime) {
        transLogger.info("{}|{}|{}|{}|{}|{}|{}|{}|{}|{}|{}", new Object[]{spId, spName, coopId, appId, appName,
                appCategory, revenueType, revenuePercentage, createChargingDetails(ncsDetails),
                status, formatDate(dateTime)});

    }
    // TODO : refactor code and add as a feature
    private static String createChargingDetails(Object ncsDetails) {
        /*{cas={party=subscriber, allowed-payment-instruments=[M-Pesa], meta-data={mpesa-paybill-number=101010}}}*/
        if(ncsDetails == null) {
              return new JSONObject().toString();
        }

        try {
            JSONObject jsonObject = new JSONObject();
            Map<String, Object> ncsDetailsMap = (Map<String, Object>)ncsDetails;

            for (String ncs : ncsDetailsMap.keySet()) {
                Map<String, Object> chargingDetailsMap = (Map<String, Object>) ncsDetailsMap.get(ncs);


                Object allowedPaymentInstruments = chargingDetailsMap.get("allowed-payment-instruments");
                if(allowedPaymentInstruments == null) {
                    continue;
                }

                List<String> paymentInsList = (List<String>) allowedPaymentInstruments;
                Map<String, Object> metaDataMap = new HashMap<String, Object>();

                Object metaData = chargingDetailsMap.get("meta-data");

                if(metaData != null) {
                    metaDataMap = (Map<String, Object>) metaData;
                }

                List<String> paymentInsLoggingList = new ArrayList<String>();

                for (String paymentIns : paymentInsList) {
                    if(metaDataMap.containsKey("mpesa-paybill-number") && "M-Pesa".equals(paymentIns)) {
                        paymentInsLoggingList.add(paymentIns + ":" + metaDataMap.get("mpesa-paybill-number"));
                    } else {
                        paymentInsLoggingList.add(paymentIns);
                    }
                }
                jsonObject.put(ncs, paymentInsLoggingList);

            }
            return jsonObject.toString();

        } catch (Exception e) {
            return new JSONObject().toString();
        }
    }

    private static String formatDate(Object dateTime)
    {
        String formattedDate =  new SimpleDateFormat(DATE_TIME_FORMAT).format(dateTime);
        return formattedDate;
    }


}
