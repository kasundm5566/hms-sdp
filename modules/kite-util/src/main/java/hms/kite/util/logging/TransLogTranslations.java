/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.util.logging;

import hms.kite.util.KiteErrorBox;
import hms.kite.util.KiteKeyBox;

/**
 * Created by IntelliJ IDEA. $LastChangedDate 9/5/11 3:50 PM$ $LastChangedBy
 * ruwan$ $LastChangedRevision$ To change this template use File | Settings |
 * File Templates.
 */
/* package private */class TransLogTranslations {

	public static final String APP_STATE_LIMITED_LIVE = "limitedLive";
	public static final String APP_STATE_LIVE = "live";
	public static final String APP_STATE_INTERMEDIATE = "intermediate";

	/**
	 * Translation from SDP application state to log application states
	 *
	 * @param appState
	 * @return
	 */
	public static String translateAppState(String appState) {
		if (appState == null)
			return APP_STATE_INTERMEDIATE;
		if (KiteKeyBox.activeProductionK.equals(appState)) {
			return APP_STATE_LIVE;
		}
		if (KiteKeyBox.limitedProductionK.equals(appState)) {
			return APP_STATE_LIMITED_LIVE;
		}
		return APP_STATE_INTERMEDIATE;
	}
    
    public static String getCaasStatusCode(String pgwStatuscode)    {
        if(pgwStatuscode != null && !pgwStatuscode.isEmpty())   {
            if("PARTIAL_OR_OVER_PAYMENT_NOT_ALLOWED".equals(pgwStatuscode)) {
                return "E1401";
            } else if("PARTIAL_PAYMENT_SUCCESS".equals(pgwStatuscode))  {
                return "P1004";
            } else if("AMOUNT_FULLY_PAID".equals(pgwStatuscode))    {
                return "S1000";
            } else if("TIMEOUTED".equals(pgwStatuscode))    {
                return "E1405";
            } else if("PARTIAL_TIMEOUTED".equals(pgwStatuscode))    {
                return "P1005";
            } else {
                return "";
            }
        } else {
            return "";
        }
    }

	public static String getEventType(String eventType) {
		if (eventType != null) {
			if ("RESERVE_CANCEL_SUCCESS".equals(eventType)) {
				return "cancel";
			} else if ("RESERVE_COMMIT_SUCCESS".equals(eventType)) {
				return "commitSuccess";
			} else if ("operator-charging".equals(eventType)) {
				return "operatorCharging";
			} else {
				return "commitFail";
			}
		} else {
			return "";
		}
	}

	public static String getChargingPartyType(String chargingParty, String chargingType) {
		if (KiteKeyBox.freeK.equals(chargingType)) {
			return "subscriber";
		} else if (KiteKeyBox.spK.equals(chargingParty)) {
			return "application";
		} else {
			return "subscriber";
		}
	}

	public static String refineRateCard(String rateCard) {
		if (rateCard != null && rateCard.endsWith(",")) {
			return rateCard.substring(0, rateCard.lastIndexOf(","));
		} else {
			return rateCard;
		}
	}

	public static String getTransacationStatus(String statusCode) {
		if (KiteErrorBox.successCode.equals(statusCode)) {
			return "success";
		} else if (KiteErrorBox.partialPaymentsSuccessCode.equals(statusCode)) {
			return "partial_success";
		} else if(KiteErrorBox.paymentPendingNotificationCode.equals(statusCode)) {
            return "payment_pending";
        } else {
			return "failure";
		}
	}
}
