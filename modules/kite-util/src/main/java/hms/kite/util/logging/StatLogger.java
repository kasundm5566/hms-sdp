/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.util.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class StatLogger {
	private static final Logger LOGGER = LoggerFactory.getLogger(StatLogger.class);

	public static void printStatLog(long startTime, String tag) {
		LOGGER.info("start[{}] time[{}] tag[{}]", new Object[] { startTime, (System.currentTimeMillis() - startTime),
				tag });
	}

	public static void printStatLog(long startTime, String tag, String message) {
		LOGGER.info("start[{}] time[{}] tag[{}] message[{}]", new Object[] { startTime,
				(System.currentTimeMillis() - startTime), tag, message });
	}
}
