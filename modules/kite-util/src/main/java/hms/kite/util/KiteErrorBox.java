/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.util;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class KiteErrorBox {

    /**
     *  Status Codes
     */
    public static final String successCode = "S1000";

    public static final String partialSuccessCode = "P1001";
    
    public static final String paymentPendingNotificationCode = "P1003";
    
    public static final String partialPaymentsSuccessCode = "P1004";

    public static final String messagePendingAdminApproval = "P1002";

    public static final String applicationFailToProcessRequestErrorCode = "E1000";

    /**
     * Sdp message flow validation errors
     */
    public static final String unknownErrorCode = "E1300";

    /**
     * Can not find the application with the given identities in the system.
     */
    public static final String appNotFoundErrorCode = "E1304";

    /**
     * Application is not available now for usage. It may be blocked, suspended or even terminated.
     */
    public static final String appNotAvailableErrorCode = "E1301";

    /**
     * Can not find the Service Provider (sp) with the given identities in the system.
     */
    public static final String spNotFoundErrorCode = "E1307";

    /**
     * Service Provider is not available now for usage. It may be suspended.
     */
    public static final String spNotAvailableErrorCode = "E1302";

    public static final String invalidHostIpErrorCode = "E1303";

    @Deprecated
    public static final String invalidAppIdErrorCode = "E1305";

    public static final String invalidRoutingkeyErrorCode = "E1306";

    /**
     * Use E1344 for temporary charging error scenario
     */
    public static final String chargingErrorCode = "E1308";

    public static final String ncsNotAllowedErrorCode = "E1309";

    public static final String moNotAllowedErrorCode = "E1310";

    public static final String mtNotAllowedErrorCode = "E1311";

    public static final String invalidRequestErrorCode = "E1312";

    public static final String authenticationFailedErrorCode = "E1313";

    public static final String ncsNotAvailableErrorCode = "E1315";

    public static final String appConnectionRefusedErrorCode = "E1316";

    /**
     * If the msisdn blocked, use E1342 and if the msisdn is not white listed use E1343.
     */
    @Deprecated
    public static final String msisdnNotAllowedErrorCode = "E1317";

    public static final String tpsExceededErrorCode = "E1318";

    public static final String tpdExceededErrorCode = "E1319";

    public static final String atMessageFailedErrorCode = "E1320";

    public static final String senderNotAllowedErrorCode = "E1322";

    public static final String recipientNotAllowedErrorCode = "E1323";

    public static final String subscriptionViaHttpNotAllowedErrorCode = "E1324";

    public static final String invalidMsisdnErrorCode = "E1325";

    public static final String insufficientFundErrorCode = "E1326";

    public static final String chargingNotAllowedErrorCode = "E1327";

    public static final String chargingOperationNotAllowedErrorCode = "E1328";

    public static final String chargingAmountTooHighErrorCode = "E1329";

    public static final String chargingAmountTooLowErrorCode = "E1330";

    public static final String invalidSourceAddressErrorCode = "E1331";

    @Deprecated
    public static final String deliveryFailedErrorCode = "E1332";

    public static final String messageTooLongErrorCode = "E1334";

    public static final String messageTooLongForAdvertiseErrorCode = "E1335";
    
    public static final String unsupportedChargingAmount = "E1336";

    public static final String subscriberAuthenticationFailed = "E1337";

    /**
     * Use E1312 instead of this error code.
     */
    //@Deprecated
    //public static final String invalidNblRequestErrorCode = "E1340";

    /**
     * This is an overall status for AO requests. If all the destinations has failure code,
     * use this error code as the overall error code.
     */
    public static final String requestDeliveryFailErrorCode = "E1341";

    public static final String msisdnIsBlacklistedErrorCode = "E1342";

    public static final String msisdnIsNotInWhiteListErrorCode = "E1343";

    public static final String temporaryChargingErrorCode = "E1344";

    public static final String subscriptionRegBlockedErrorCode = "E1350";

    public static final String subscriptionRegAlreadyRegErrorCode = "E1351";

    public static final String subscriptionRegSlaErrorCode = "E1352";

    public static final String subscriptionRegChargingErrorCode = "E1353";

    public static final String subscriptionUnregSlaErrorCode = "E1354";

    public static final String subscriptionUnregBlockedErrorCode = "E1355";

    public static final String subscriptionUnregNotRegErrorCode = "E1356";

    public static final String onDemandBlockedErrorCode = "E1357";

    public static final String pendingPaymentErrorCode = "E1358";

    public static final String invalidUserAccountErrorCode = "E1359";


    public static final String sblFailErrorCode = "E1360";

    public static final String sblMsgRegectedErrorCode = "E1361";

    public static final String sblInvalidRequestErrorCode = "E1362";

    public static final String sblNoResponseErrorCode = "E1363";

    public static final String cannotSentToSblErrorCode = "E1364";

    public static final String subscriberNotRegisteredCode = "E1365";

    public static final String mtDeliveryFailed = "E1366";

    public static final String caasPiNotAllowedErrorCode = "E1367";

    public static final String lbsInvalidQosErrorCode = "E1367";

    public static final String lbsInvalidServiceTypeErrorCode = "E1368";

    public static final String lbsInvalidPositioningMethodErrorCode = "E1369";

    /**
     * Unexpected errors, use systemErrorCode instead of this
     */
    @Deprecated
    public static final String internalErrorCode = "E1600";

    /**
     * This error code is used for permanent, internal, unexpected system errors.
     * Description should have enough information regarding the failure.
     */
    public static final String systemErrorCode = "E1601";

    public static final String temporarySystemErrorCode = "E1603";

    public static final String externalErrorCode = "E1602";


    /**
     * Sdp non-message-flow validation-errors
     */
    public static final String duplicateRoutingKeyErrorCode = "E1801";

    public static final String keywordIsServiceKeywordErrorCode = "E1802";

    public static final String routingKeyAlreadyAssignedErrorCode = "E1803";

    public static final String appNameAlreadyAssignedErrorCode = "E1804";

    public static final String emailNotificationParserErrorCode = "E1805";

    public static final String emailNotificationSendErrorCode = "E1806";

    public static final String fileNotFoundErrorCode = "E1807";

    public static final String emailTemplateNotFoundErrorCode = "E1808";

    public static final String emailTemplateAlreadyExistErrorCode = "E1809";

    public static final String sendEmailEventAlreadyExistErrorCode = "E1810";

    public static final String sendEmailEventNotFoundErrorCode = "E18112";

    public static final String spNotApprovedErrorCode = "E1811";

    public static final String spIsSuspendedErrorCode = "E1812";

    public static final String crAlreadyAssignedErrorCode = "E1813";

    public static final String crSpAlreadyAssignedErrorCode = "E1814";

    public static final String crAppAlreadyAssignedErrorCode = "E1815";

    public static final String crNcsAlreadyAssignedErrorCode = "E1816";

    public static final String crNotAvailableErrorCode = "E1817";

    public static final String md5ExceptionErrorCode = "E1818";

    public static final String spIsASolturaUserErrorCode = "E1819";

    public static final String notSupportedOperationErrorCode = "E1825";

    public static final String messageContainAbusiveContentErrorCode = "E1826";

    public static final String vdfApiUnexpectedError = "E91000";
    public static final String vdfApiInsufficientBalance = "E91001"; //Not sufficient balance.
    public static final String vdfApiSubscriberNotExists = "E91002"; //The subscriber does not exist or is not active.
    public static final String vdfApiPlanDoNotExists = "E91003"; //Plan does not exist
    public static final String vdfApiSubscriberAlreadyHasPlan = "E91004"; //The subscriber has already has the plan.
    public static final String vdfApiCannotBuyPlan = "E91005"; //Cannot buy this plan
    public static final String vdfApiTimeout = "E91008"; //Processing timout.



}
