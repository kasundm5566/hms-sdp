/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.util.logging;

import java.lang.annotation.*;

/**
 * Add this annotation to any method in order to provide trans-log writing capability
 *
 * $LastChangedDate 8/31/11 6:29 PM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 * To change this template use File | Settings | File Templates.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy .RUNTIME)
@Inherited
public @interface TransLogWriteCandidate {
}
