/**
 * (C) Copyright 2011-2012 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 * <p/>
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 * <p/>
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.kite.util.logging.service;

import hms.kite.util.KiteErrorBox;
import hms.kite.util.KiteKeyBox;
import hms.kite.util.logging.KiteLog;
import hms.kite.util.logging.TransLogUtil;

import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.logging.TransLogUtil.*;
import static hms.kite.util.logging.TransLogUtil.getIsMasked;

public class LbsTranslogService extends AbstractTransLogService {

    @Override
    public void log(Map<String, Map<String, Object>> requestContext) {
        super.log(requestContext);
        addAdditionalParameters(requestContext);
        TransLogUtil.log(requestContext);
    }

    public void addAdditionalParameters(Map<String, Map<String, Object>> requestContext) {

        Map<String, Object> request = requestContext.get(KiteKeyBox.requestK);
        Map<String, Object> sp = requestContext.get(KiteKeyBox.spK);
        Map<String, Object> app = requestContext.get(KiteKeyBox.appK);
        Map<String, Object> ncs = requestContext.get(KiteKeyBox.ncsK);


        if (KiteKeyBox.mtK.equals(request.get(KiteKeyBox.directionK))) {
            putLogElement(KiteLog.LogField.SOURCE_CHANNEL_TYPE.getParameter(), "lbs", requestContext);
            putLogElement(KiteLog.LogField.SOURCE_CHANNEL_PROTOCOL.getParameter(), "http", requestContext);
            putLogElement(KiteLog.LogField.DESTINATION_CHANNEL_TYPE.getParameter(), "lbs", requestContext);
            putLogElement(KiteLog.LogField.DESTINATION_CHANNEL_PROTOCOL.getParameter(), "mlp", requestContext);

            List<Map<String, Object>> recipients = (List) request.get(KiteKeyBox.recipientsK);
            if (recipients != null && recipients.size() > 0) {
                Map<String, Object> recipient = recipients.get(0);
                putLogElement(KiteLog.LogField.DESTINATION_ADDRESS.getParameter(), recipient.get(KiteKeyBox.recipientAddressK), requestContext);
                putLogElement(KiteLog.LogField.OPERATOR.getParameter(), recipient.get(KiteKeyBox.recipientAddressOptypeK), requestContext);

                if (getIsMasked(app)) {
                    putLogElement(KiteLog.LogField.MASKED_DESTINATION_ADDRESS.getParameter(), recipient.get(KiteKeyBox.recipientAddressMaskedK), requestContext);
                }

                putLogElement(KiteLog.LogField.CHARGE_PARTY_TYPE.getParameter(),
                        getChargingPartyType((String) recipient.get(KiteLog.LogField.CHARGE_PARTY_TYPE.getParameter()),
                                (String) recipient.get(KiteLog.LogField.CHARGING_TYPE.getParameter())), requestContext);
            }

            putLogElement(KiteLog.LogField.EVENT_TYPE.getParameter(), "locate", requestContext);
            putLogElement(KiteLog.LogField.CHARGE_AMOUNT.getParameter(), getChargeAmount(ncs), requestContext);
            putLogElement(KiteLog.LogField.CHARGING_TYPE.getParameter(), getLogElement(ncs, mtK, chargingK, typeK), requestContext);


            String requestStatus = (String) request.get(statusCodeK);
            putLogElement(KiteLog.LogField.RESPONSE_CODE.getParameter(), requestStatus, requestContext);
            putLogElement(KiteLog.LogField.RESPONSE_DESCRIPTION.getParameter(), buildStatusDescriptionIfNotAvailable(requestStatus, null, requestContext), requestContext);


            fetchLogParameter(KiteKeyBox.statusCodeK, KiteLog.LogField.OVERALL_RESPONSE_CODE.getParameter(), request,
                    requestContext);
            fetchLogParameter(KiteKeyBox.statusDescriptionK, KiteLog.LogField.OVERALL_RESPONSE_DESCRIPTION.getParameter(),
                    request, requestContext);
            putLogElement(KiteLog.LogField.TRANSACTION_STATUS.getParameter(), getTransactionStatus((String) request.get(KiteKeyBox.statusCodeK)), requestContext);


            putLogElement(KiteLog.LogField.SERVICE_TYPE.getParameter(), request.get(serviceTypeK), requestContext);
            putLogElement(KiteLog.LogField.RESPONSE_TIME.getParameter(), request.get(responseTimeK), requestContext);
            putLogElement(KiteLog.LogField.FRESHNESS.getParameter(), request.get(freshnessOfLocationK), requestContext);
            putLogElement(KiteLog.LogField.HORIZONTAL_ACCURACY.getParameter(), request.get(horizontalAccuracyK), requestContext);

        }
    }

    private String getChargeAmount(Map<String, Object> ncs) {
        String amount = (String) getLogElement(ncs, mtK, chargingK, amountK);
        return amount == null ? "0" : amount;
    }
}