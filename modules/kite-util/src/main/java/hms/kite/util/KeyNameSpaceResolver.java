/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.util;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * This class use to resolve hierarchical data and related utilities.
 *
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

public class KeyNameSpaceResolver {

    /**
     * Use this separator to create a combined key. Note that this separator ins not the nameSpace Separator.
     */
    private static final String keywordSeparator = "-";

    /**
     * Use this spacer to create combined names paced key.
     */
    public static final String keyNameSpacer = ".";

    /**
     * Create a combined key by adding keyword separator #keywordSeparator.
     * @param individualKeys -
     * @return add #keywordSeparator in between keywords;
     */
    public static String key(String... individualKeys) {
        StringBuilder sb = new StringBuilder();
        for (String individualKey : individualKeys) {
            sb.append(individualKey);
            sb.append(keywordSeparator);
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    /**
     * Create a name spaced key by adding name space separator in between.
     * @param individualKeys -
     * @return combined key by adding #keyNameSpacer
     */
    public static String nameSpacedKey(String... individualKeys) {
        StringBuilder sb = new StringBuilder();
        for (String individualKey : individualKeys) {
            sb.append(individualKey);
            sb.append(keyNameSpacer);
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    public static Object data(Map data, String... keys) {
        Object innerData = data;
        for (String key : keys) {
            if(innerData != null){
                innerData = ((Map<String, Object>) innerData).get(key);
            } else {
                return null;
            }

        }
        return innerData;
    }

    public static boolean contains(Map data, String... keys) {
        Object innerData = data;
        for (String key : keys) {
            if (!((Map<String, Object>) innerData).containsKey(key)) {
                return false;
            }
            innerData = ((Map<String, Object>) innerData).get(key);
        }

        return true;
    }

    public static Object data(Map<String, Object> data, String nameSpacedKey) {
        String[] keys = nameSpacedKey.split("\\.");
        return data(data, keys);
    }

    /**
     * If the entry doesn't exist, put the value in the map.
     * If the entry is a Map, and if it is last entry
     * @param src
     * @param value
     * @param keys
     */
    public static void putData(Map src, Object value, String... keys) {
        Map<String, Object> target = src;

        String lastKey = null;

        if (keys.length == 1) {
            target.put(keys[0], value);
            return;
        }

        for (int i = 0; i < keys.length; i++) {

            String key = keys[i];
            boolean isLastKey = i == keys.length - 1;

            Object possibleTarget = target.get(key);

            if (possibleTarget == null) {
                if (isLastKey) {
                    lastKey = key;
                    break;
                } else {
                    throw new IllegalStateException("No value found for key [" + key + "] at " + i);
                }
            }

            if (possibleTarget instanceof Map) {
                target = (Map<String, Object>) possibleTarget;
                continue;
            }

            if (List.class.isAssignableFrom(possibleTarget.getClass()) && isLastKey) {
                ((List) possibleTarget).add(value);
                return;
            }
        }
        if (lastKey == null) {
            throw new IllegalStateException("Can not find any data at path : "
                    + Arrays.toString(keys) + "in request : " + src);
        }
        target.put(lastKey, value);
    }

}
