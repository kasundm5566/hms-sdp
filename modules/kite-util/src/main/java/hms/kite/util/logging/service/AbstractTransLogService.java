package hms.kite.util.logging.service;

import hms.kite.util.KeyNameSpaceResolver;
import hms.kite.util.KiteErrorBox;
import hms.kite.util.KiteKeyBox;
import hms.kite.util.logging.KiteLog;
import hms.kite.util.logging.TransLogUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.logging.TransLogUtil.*;

/**
 * (C) Copyright 2011-2012 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 * <p/>
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 * <p/>
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
public abstract class AbstractTransLogService {

    Logger logger = LoggerFactory.getLogger(AbstractTransLogService.class);

    public void log(Map<String, Map<String, Object>> requestContext) {
        TransLogUtil.addMainParameters(requestContext);
    }

    public final void serve(Map<String, Map<String, Object>> requestContext) {
        Boolean trxLogPrinted = KeyNameSpaceResolver.data(requestContext, "trx-log-printed") == null ? false
                                    : (Boolean) KeyNameSpaceResolver.data(requestContext, "trx-log-printed");
        if (!trxLogPrinted) {
            this.log(requestContext);
            KeyNameSpaceResolver.putData(requestContext, true, "trx-log-printed");
            logger.trace("RequestContext for Translog[{}]", requestContext);
            logger.debug("Trans log has being printed for the workflow");
        } else {
            logger.debug("Trans log is already printed for the workflow [{}]", requestContext);
        }
    }


}
