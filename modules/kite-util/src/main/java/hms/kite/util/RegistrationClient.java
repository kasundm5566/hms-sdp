/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.util;

import hms.common.registration.api.common.RequestType;
import hms.common.registration.api.request.UserDetailByMsisdnRequestMessage;
import hms.common.registration.api.request.UserDetailRequestMessage;
import hms.common.registration.api.response.BasicUserResponseMessage;
import hms.common.rest.util.client.AbstractWebClient;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.Date;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */
public class RegistrationClient extends AbstractWebClient {

    private String registrationHost;
	private final String pathForMsisdn = "/registrationservice/user/detail/msisdn";
	private final String pathForUserId = "/registrationservice/user/detail";
	private WebClient webClientForMsisdn;
	private WebClient webClientForUserId;
	private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationClient.class);
	
	public BasicUserResponseMessage getUserInfoByMsisdn(String msisdn) {
		UserDetailByMsisdnRequestMessage reqMessage = new UserDetailByMsisdnRequestMessage();
        reqMessage.setRequestType(RequestType.USER_BASIC_DETAILS);
        reqMessage.setRequestedTimeStamp(new Date());
        reqMessage.setMsisdn(msisdn);
        if(LOGGER.isDebugEnabled()) {
        	LOGGER.debug("Sending Get User Additional Details Request " + reqMessage.toString());
        }
        return generateUserResponseMessage(getWebClient(registrationHost + pathForMsisdn).post(reqMessage.convertToMap()));
	}
	
	public BasicUserResponseMessage getUserInfoByUserId(String userId) {
		UserDetailRequestMessage reqMessage = new UserDetailRequestMessage();
        reqMessage.setRequestType(RequestType.USER_BASIC_DETAILS);
        reqMessage.setRequestedTimeStamp(new Date());
        reqMessage.setUserId(userId);
        if(LOGGER.isDebugEnabled()) {
        	LOGGER.debug("Sending Get User Basic Details Request [{}]", reqMessage.toString());
        }
		return generateUserResponseMessage(getWebClient(registrationHost + pathForUserId).post(reqMessage.convertToMap()));
	}
	
	private WebClient getWebClient(String url) {
		if(webClientForMsisdn == null) {
			webClientForMsisdn = createWebClient(url);
		}
		return webClientForMsisdn;
	}
	
	private BasicUserResponseMessage generateUserResponseMessage(Response response) {
        try {
            return BasicUserResponseMessage.convertFromMap(readJsonResponse(response));
        } catch (ParseException e) {
            LOGGER.warn("Error while getting basic user ", e);
            return null;
        } catch (IOException e) {
            LOGGER.warn("Error while getting basic user ", e);
            return null;
        }
	}

    public void setRegistrationHost(String registrationHost) {
        this.registrationHost = registrationHost;
    }
}
