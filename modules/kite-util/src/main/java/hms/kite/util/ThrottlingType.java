/*
*   (C) Copyright 2009-2012 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/

package hms.kite.util;

import static hms.kite.util.KiteKeyBox.*;

/**
 * User: azeem
 * Date: 3/8/12
 * Time: 6:00 PM
 */
public enum ThrottlingType {

    TPS(tpsK),
    TPD(tpdK),
    MCT(mctK),
    DUMMY("Used for testing purposes");

    private String value;

    private ThrottlingType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
