/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.util;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class GsonUtil {

    private static final Gson gson;
    private static final Logger logger = LoggerFactory.getLogger(GsonUtil.class);

    static {
        gson = new Gson();
    }

    public static String toJson(Object src) {
        return gson.toJson(src);
    }

    public static <T> T fromJson(String json, Class<T> classOfT) throws JsonSyntaxException {
        return gson.fromJson(json, classOfT);
    }

    public static Gson getGson(){
        return gson;
    }

    public static Object parseElement(JsonElement element) {
        Object result = null;

        logger.trace("element [{}]", element);

        if (element.isJsonArray()) {

            ArrayList list = new ArrayList();
            JsonArray jsonArray = element.getAsJsonArray();
            for (JsonElement jsonElement : jsonArray) {
                list.add(parseElement(jsonElement));
            }
            result = list;

        } else if (element.isJsonNull()) {
            logger.warn("Null value received no way to parse this.");
        } else if (element.isJsonPrimitive()) {
            result = element.getAsString();
        } else if (element.isJsonObject()) {

            Map map = new HashMap();
            JsonObject jsonObject = element.getAsJsonObject();
            for (Map.Entry<String, JsonElement> entry : jsonObject.entrySet()) {
                map.put(entry.getKey(), parseElement(entry.getValue()));
            }
            result = map;
        }
        logger.debug("Result is - {}", result);
        return result;
    }

}
