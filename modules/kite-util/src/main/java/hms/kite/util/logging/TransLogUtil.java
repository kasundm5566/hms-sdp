package hms.kite.util.logging;

import hms.kite.util.KiteErrorBox;
import hms.kite.util.KiteKeyBox;
import hms.kite.util.PgwParam;
import hms.kite.util.StatusDescriptionBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static hms.kite.util.KeyNameSpaceResolver.data;
import static hms.kite.util.KiteKeyBox.*;

/**
 * Created by IntelliJ IDEA.
 * User: isuruanu
 * Date: 7/2/12
 * Time: 1:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class TransLogUtil {

    public static final Logger LOGGER = LoggerFactory.getLogger(TransLogUtil.class);
    public static final String loggingContextK = "logging_context";
    public static final String contextLoggedK = "context_logged";
    public static final String recipientsStatusK = "recipients";
    public static final String PERCENTAGE_FROM_MONTHLY_REVENUE = "percentageFromMonthlyRevenue";
    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS");
    public static final String logFromInternalWorkFlow = "log-from-internal-workflow";

    private static StatusDescriptionBuilder statusDescriptionBuilder;

    static {
        loadProperties();
    }

    private static void loadProperties() {
        statusDescriptionBuilder = new StatusDescriptionBuilder("status-message");
        statusDescriptionBuilder.init();
    }

    public static void putLogElement(String name, Object value, Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> loggingContext = requestContext.get(loggingContextK);
        if (loggingContext == null) {
            loggingContext = new HashMap<String, Object>();
            requestContext.put(loggingContextK, loggingContext);
        }

        loggingContext.put(name, value);
    }

    public static Object getLogElement(Map requestContext, String... keys) {
        return data((Map)requestContext, keys);
    }

    public static String getMaskedStatus(boolean isMasked) {
        if (isMasked) {
            return "1";
        } else {
            return "0";
        }
    }

    public static void fetchLogParameter(String sourceKeyName, String logKeyName, Map<String, Object> source,
                                          Map<String, Map<String, Object>> requestContext) {
        if (source == null) {
            return;
        }
        Object value = source.get(sourceKeyName);
        if (value != null) {
            putLogElement(logKeyName, value, requestContext);
        }
    }

    public static void log(Map<String, Map<String, Object>> requestContext) {
        TransLogWriter.log(requestContext.get(loggingContextK));
    }

    public static String translateAppState(Map<String, Object> app) {
        return TransLogTranslations.translateAppState((String) app.get(KiteKeyBox.statusK));
    }

    public static void fetchDateTimeLogParameter(String sourceKeyName, String logKeyName, Map<String, Object> source,
                                                  Map<String, Map<String, Object>> requestContext) {
        Object value = source.get(sourceKeyName);
        if (value != null) {
            Date d = new Date(Long.parseLong((String) value));
            putLogElement(logKeyName, DATE_FORMAT.format(d), requestContext);
        }
    }

    public static String[] getChargedAddress(String chargingType, String chargePartyType, String subscriberMsisdn,
                                              String subscriberMsisdnPlainText, Map<String, Object> sp) {
        String spId = "";
        if (sp != null) {
            spId = (String) sp.get(KiteKeyBox.spIdK);
        }
        return getChargedAddress(chargingType, chargePartyType, subscriberMsisdn, subscriberMsisdnPlainText, spId);

    }

    public static String[] getChargedAddress(String chargingType, String chargePartyType, String subscriberMsisdn,
                                              String subscriberMsisdnPlainText, String spId) {
        String chargeAddress = "";
        String plainTextChargedAddress = "";
        if (!KiteKeyBox.freeK.equals(chargingType)) {
            if (KiteKeyBox.spK.equals(chargePartyType)) {
                chargeAddress = spId;
            } else if (KiteKeyBox.subscriberK.equals(chargePartyType)) {
                chargeAddress = subscriberMsisdn;
                plainTextChargedAddress = subscriberMsisdnPlainText;
            }
        }

        return new String[] { chargeAddress, plainTextChargedAddress };

    }

    public static String getChargingPartyType(String chargingParty, String chargingType) {
        return TransLogTranslations.getChargingPartyType(chargingParty, chargingType);
    }

    public static String refineRateCard(String rateCard) {
        return TransLogTranslations.refineRateCard(rateCard);
    }

    public static String getEventType(String eventType) {
        return TransLogTranslations.getEventType(eventType);
    }

    public static String getTransactionStatus(String statusCode) {
        return TransLogTranslations.getTransacationStatus(statusCode);
    }

    public static String buildStatusDescriptionIfNotAvailable(String statusCode, String statusDescription,
                                                               Map<String, Map<String, Object>> requestContext) {
        if ((statusDescription == null || statusDescription.trim().isEmpty()) && statusCode != null) {
            return statusDescriptionBuilder.createStatusDescription(statusCode, requestContext);
        } else {
            return statusDescription;
        }
    }

    public static String getCaasStatusCode(String pgwStatuscode)    {
        if(pgwStatuscode != null && !pgwStatuscode.isEmpty())   {
            if("PARTIAL_OR_OVER_PAYMENT_NOT_ALLOWED".equals(pgwStatuscode)) {
                return "E1401";
            } else if("PARTIAL_PAYMENT_SUCCESS".equals(pgwStatuscode))  {
                return "P1004";
            } else if("AMOUNT_FULLY_PAID".equals(pgwStatuscode))    {
                return "S1000";
            } else if("TIMEOUTED".equals(pgwStatuscode))    {
                return "E1405";
            } else if("PARTIAL_TIMEOUTED".equals(pgwStatuscode))    {
                return "P1005";
            } else {
                return "";
            }
        } else {
            return "";
        }
    }

    public static void putRecipientsDetails(Map<String, Map<String, Object>> requestContext,
                                             Map<String, Object> request, Map<String, Object> sp, boolean isMaskedApp) {
        if (request != null) {
            if (KiteKeyBox.mtK.equals(request.get(KiteKeyBox.directionK))) {

                List<Map<String, Object>> recipientsStatus = new ArrayList<Map<String, Object>>();
                List<Map<String, Object>> recipients = (List) request.get(KiteKeyBox.recipientsK);
                for (Map<String, Object> recipient : recipients) {
                    Map<String, Object> recipientTransLog = new HashMap<String, Object>();
                    if (isMaskedApp) {
                        recipientTransLog.put(KiteKeyBox.recipientAddressK,
                                recipient.get(KiteKeyBox.recipientAddressPlainTextK));
                        recipientTransLog.put(KiteLog.LogField.MASKED_DESTINATION_ADDRESS.getParameter(),
                                recipient.get(KiteKeyBox.recipientAddressK));
                    } else {
                        recipientTransLog
                                .put(KiteKeyBox.recipientAddressK, recipient.get(KiteKeyBox.recipientAddressK));
                    }
                    recipientTransLog.put(KiteKeyBox.chargingTrxIdK, recipient.get(KiteKeyBox.chargingTrxIdK));
                    recipientTransLog.put(KiteLog.LogField.RESPONSE_CODE.getParameter(),
                            recipient.get(KiteKeyBox.recipientAddressStatusK));

                    recipientTransLog.put(
                            KiteLog.LogField.RESPONSE_DESCRIPTION.getParameter(),
                            buildStatusDescriptionIfNotAvailable(
                                    (String) recipient.get(KiteKeyBox.recipientAddressStatusK),
                                    (String) recipient.get(KiteKeyBox.statusDescriptionK), requestContext));

                    recipientTransLog.put(KiteLog.LogField.CHARGING_TYPE.getParameter(),
                            recipient.get(KiteLog.LogField.CHARGING_TYPE.getParameter()));
                    recipientTransLog.put(
                            KiteLog.LogField.CHARGE_PARTY_TYPE.getParameter(),
                            getChargingPartyType((String) recipient.get(KiteLog.LogField.CHARGE_PARTY_TYPE.getParameter()),
                                    (String) recipient.get(KiteLog.LogField.CHARGING_TYPE.getParameter())));
                    recipientTransLog.put(KiteLog.LogField.OPERATOR.getParameter(),
                            recipient.get(KiteKeyBox.recipientAddressOptypeK));
                    recipientTransLog.put(KiteLog.LogField.RATE_CARD.getParameter(),
                            recipient.get(KiteKeyBox.usedExchangeRatesK));
                    recipientTransLog.put(KiteLog.LogField.CHARGE_AMOUNT_CURRENCY.getParameter(),
                            recipient.get(KiteKeyBox.currencyCodeK));

                    String[] chargedAddress = getChargedAddress(
                            (String) recipient.get(KiteLog.LogField.CHARGING_TYPE.getParameter()),
                            (String) recipient.get(KiteLog.LogField.CHARGE_PARTY_TYPE.getParameter()),
                            (String) recipient.get(KiteKeyBox.recipientAddressK),
                            (String) recipient.get(KiteKeyBox.recipientAddressPlainTextK), sp);
                    if (isMaskedApp) {
                        recipientTransLog.put(KiteLog.LogField.CHARGE_ADDRESS.getParameter(), chargedAddress[1]);
                        recipientTransLog.put(KiteLog.LogField.MASKED_CHARGE_ADDRESS.getParameter(), chargedAddress[0]);
                    } else {
                        recipientTransLog.put(KiteLog.LogField.CHARGE_ADDRESS.getParameter(), chargedAddress[0]);
                    }

                    recipientTransLog.put(KiteLog.LogField.CHARGED_CATEGORY.getParameter(),
                            recipient.get(KiteLog.LogField.CHARGED_CATEGORY.getParameter()));
                    recipientTransLog.put(KiteLog.LogField.OPERATOR_COST.getParameter(),
                            recipient.get(KiteLog.LogField.OPERATOR_COST.getParameter()));
                    recipientTransLog.put(KiteLog.LogField.CHARGE_AMOUNT.getParameter(),
                            recipient.get(KiteLog.LogField.CHARGE_AMOUNT.getParameter()));
                    recipientTransLog.put(KiteLog.LogField.EVENT_TYPE.getParameter(),
                            getEventType((String) recipient.get(KiteLog.LogField.EVENT_TYPE.getParameter())));
                    recipientTransLog.put(KiteLog.LogField.PGW_TRANSACTION_ID.getParameter(),
                            recipient.get(KiteKeyBox.externalTransIdK));

                    if(recipient.get(KiteKeyBox.paymentInstrumentNameK) != null) {
                        recipientTransLog.put(KiteLog.LogField.FROM_PAYMENT_INSTRUMENT_NAME.getParameter(), recipient.get(KiteKeyBox.paymentInstrumentNameK));
                    }

                    recipientsStatus.add(recipientTransLog);
                }

                putLogElement(recipientsStatusK, recipientsStatus, requestContext);
            }
        }

    }

    public static Boolean getIsMasked(Map<String, Object> app) {
        if(app == null) {
            return false;
        } else {
            return app.get(KiteKeyBox.maskNumberK) != null ? (Boolean)app.get(KiteKeyBox.maskNumberK) : false;
        }        
    }
    
    public static void addMainParameters(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> app = requestContext.get(KiteKeyBox.appK);
        Map<String, Object> request = requestContext.get(KiteKeyBox.requestK);
        Map<String, Object> sp = requestContext.get(KiteKeyBox.spK);
        Map<String, Object> response = requestContext.get(KiteKeyBox.responseK);

        if (app != null) {
            addApplicationRelatedParameters(app, requestContext);            
        }
        if (request != null) {
            addRequestRelatedParameters(request, app, requestContext);
        }
        if (sp != null) {
            fetchLogParameter(KiteKeyBox.coopUserNameK, KiteLog.LogField.SP_NAME.getParameter(), sp, requestContext);
        }
        if(response != null) {
            fetchLogParameter(PgwParam.FROM_PAYMENT_INS_NAME.getParameter(), KiteLog.LogField.FROM_PAYMENT_INSTRUMENT_NAME.getParameter(), response, requestContext);
        }

        putLogElement(KiteLog.LogField.BILLING_TYPE.getParameter(), "unknown", requestContext);

    }

    private static void addApplicationRelatedParameters(Map<String, Object> app, Map<String, Map<String, Object>> requestContext) {
        fetchLogParameter(KiteKeyBox.spIdK, KiteLog.LogField.SP_ID.getParameter(), app, requestContext);
        fetchLogParameter(KiteKeyBox.spNameK, KiteLog.LogField.SP_NAME.getParameter(), app, requestContext);
        fetchLogParameter(KiteKeyBox.appIdK, KiteLog.LogField.APP_ID.getParameter(), app, requestContext);
        fetchLogParameter(KiteKeyBox.nameK, KiteLog.LogField.APP_NAME.getParameter(), app, requestContext);
        putLogElement(KiteLog.LogField.APP_STATE.getParameter(), translateAppState(app), requestContext);

        putLogElement(KiteLog.LogField.IS_NUMBER_MASKED.getParameter(), getMaskedStatus(getIsMasked(app)), requestContext);

        putLogElement(KiteLog.LogField.REVENUE_SHARE_TYPE.getParameter(), PERCENTAGE_FROM_MONTHLY_REVENUE, requestContext);
        fetchLogParameter(KiteKeyBox.revenueShareK, KiteLog.LogField.REVENUE_SHARE_PERCENTAGE.getParameter(), app, requestContext);
    }

    private static void addRequestRelatedParameters(Map<String, Object> request, Map<String, Object> app, Map<String, Map<String, Object>> requestContext) {

        fetchLogParameter(KiteKeyBox.correlationIdK, KiteLog.LogField.CORRELATION_ID.getParameter(), request, requestContext);
        fetchLogParameter(KiteKeyBox.directionK, KiteLog.LogField.DIRECTION.getParameter(), request, requestContext);
        fetchLogParameter(KiteKeyBox.keywordK, KiteLog.LogField.KEYWORD.getParameter(), request, requestContext);
        fetchLogParameter(KiteKeyBox.shortcodeK, KiteLog.LogField.SHORT_CODE.getParameter(), request, requestContext);
        fetchLogParameter(KiteKeyBox.operatorK, KiteLog.LogField.OPERATOR.getParameter(), request, requestContext);
        fetchLogParameter(KiteKeyBox.ncsTypeK, KiteLog.LogField.NCS.getParameter(), request, requestContext);
        fetchLogParameter(KiteKeyBox.adNameK, KiteLog.LogField.ADVERTISEMENT_ID.getParameter(), request, requestContext);
        fetchLogParameter(KiteKeyBox.adTextK, KiteLog.LogField.ADVERTISEMENT_NAME.getParameter(), request, requestContext);
        fetchLogParameter(KiteKeyBox.invoiceK, KiteLog.LogField.INVOICE_NO.getParameter(), request, requestContext);
        fetchLogParameter(KiteKeyBox.orderNoK, KiteLog.LogField.ORDER_NO.getParameter(), request, requestContext);
        fetchLogParameter(KiteKeyBox.externalTransIdNblK, KiteLog.LogField.EXTERNAL_TRX_ID.getParameter(), request, requestContext);
        fetchLogParameter(KiteKeyBox.balanceDueK, KiteLog.LogField.BALANCE_DUE.getParameter(), request, requestContext);
        fetchLogParameter(KiteKeyBox.totalAmountK, KiteLog.LogField.TOTAL_AMOUNT.getParameter(), request, requestContext);
        fetchLogParameter(KiteKeyBox.ussdOpK, KiteLog.LogField.USSD_OPERATION.getParameter(), request, requestContext);
        fetchLogParameter(KiteKeyBox.sessionIdK, KiteLog.LogField.SESSION_ID.getParameter(), request, requestContext);
        fetchLogParameter(KiteKeyBox.msisdnK, KiteLog.LogField.SOURCE_ADDRESS.getParameter(), request, requestContext);
        fetchLogParameter(KiteKeyBox.statusCodeK, KiteLog.LogField.OVERALL_RESPONSE_CODE.getParameter(), request, requestContext);
        fetchLogParameter(KiteKeyBox.statusDescriptionK, KiteLog.LogField.OVERALL_RESPONSE_DESCRIPTION.getParameter(), request, requestContext);
        fetchLogParameter(PgwParam.FROM_PAYMENT_INS_NAME.getParameter(), KiteLog.LogField.FROM_PAYMENT_INSTRUMENT_NAME.getParameter(), request, requestContext);
        fetchLogParameter(KiteKeyBox.businessIdK, KiteLog.LogField.PAYMENT_INSTRUMENT_BUSINESS_ID.getParameter(), request, requestContext);

        putLogElement(KiteLog.LogField.TRANSACTION_STATUS.getParameter(), getTransactionStatus((String) request.get(KiteKeyBox.statusCodeK)), requestContext);

        if (KiteKeyBox.moK.equals(request.get(KiteKeyBox.directionK))) {
            String addressStatus = (String) request.get(KiteKeyBox.senderAddressStatusK);
            String statusCode = (String) request.get(KiteKeyBox.statusCodeK);
            String requestStatus = KiteErrorBox.successCode;
            if (KiteErrorBox.successCode.equals(addressStatus)) {
                requestStatus = statusCode;
            } else if (KiteErrorBox.successCode.equals(statusCode)) {
                requestStatus = addressStatus;
            } else {
                requestStatus = statusCode;
            }
            putLogElement(KiteLog.LogField.RESPONSE_CODE.getParameter(), requestStatus, requestContext);
            putLogElement(KiteLog.LogField.RESPONSE_DESCRIPTION.getParameter(), buildStatusDescriptionIfNotAvailable(requestStatus, null, requestContext), requestContext);
            putLogElement(KiteLog.LogField.TRANSACTION_STATUS.getParameter(), getTransactionStatus(requestStatus), requestContext);

        } else if (KiteKeyBox.mtK.equals(request.get(KiteKeyBox.directionK))) {
            fetchLogParameter(KiteKeyBox.statusCodeK, KiteLog.LogField.OVERALL_RESPONSE_CODE.getParameter(), request, requestContext);
            fetchLogParameter(KiteKeyBox.statusDescriptionK, KiteLog.LogField.OVERALL_RESPONSE_DESCRIPTION.getParameter(), request, requestContext);
            putLogElement(KiteLog.LogField.TRANSACTION_STATUS.getParameter(), getTransactionStatus((String) request.get(KiteKeyBox.statusCodeK)), requestContext);
        } else if (KiteKeyBox.drK.equals(request.get(KiteKeyBox.directionK))) {
            putLogElement(KiteLog.LogField.CHARGE_AMOUNT.getParameter(), request.get("add_charged-amount"), requestContext);
            putLogElement(KiteLog.LogField.OVERALL_RESPONSE_CODE.getParameter(), request.get(deliveryStatusK), requestContext);
            putLogElement(KiteLog.LogField.OVERALL_RESPONSE_DESCRIPTION.getParameter(), request.get(deliveryStatusDescriptionK), requestContext);
        } else if (KiteKeyBox.srrK.equals(request.get(KiteKeyBox.directionK))) {
            putLogElement(KiteLog.LogField.OVERALL_RESPONSE_CODE.getParameter(), request.get(deliveryStatusK), requestContext);
            putLogElement(KiteLog.LogField.OVERALL_RESPONSE_DESCRIPTION.getParameter(), request.get(deliveryStatusDescriptionK), requestContext);
        }

        String receiveTime = (String) request.get(KiteKeyBox.receiveTimeK);
        

        if (receiveTime != null) {
            fetchDateTimeLogParameter(KiteKeyBox.receiveTimeK, KiteLog.LogField.TIME_STAMP.getParameter(), request,
                    requestContext);
        } else {
            putLogElement(KiteLog.LogField.TIME_STAMP.getParameter(), DATE_FORMAT.format(new Date()), requestContext);
        }

        if (getIsMasked(app)) {
            fetchLogParameter(KiteKeyBox.senderAddressK, KiteLog.LogField.MASKED_SOURCE_ADDRESS.getParameter(), request,
                    requestContext);
            fetchLogParameter(KiteKeyBox.senderAddressPlainTextK, KiteLog.LogField.SOURCE_ADDRESS.getParameter(), request,
                    requestContext);
        } else {
            fetchLogParameter(KiteKeyBox.senderAddressK, KiteLog.LogField.SOURCE_ADDRESS.getParameter(), request,
                    requestContext);
        }
    }

    public static void putChargingDetailsForMo(Map<String, Map<String, Object>> requestContext,
                                                Map<String, Object> request, Map<String, Object> ncs, Map<String, Object> sp) {
        if (ncs != null && request != null) {
            if (KiteKeyBox.moK.equals(request.get(KiteKeyBox.directionK))) {

                Map<String, Object> flowDetails = (Map<String, Object>) ncs.get(KiteKeyBox.moK);
                if (flowDetails != null && flowDetails.containsKey(KiteKeyBox.chargingK)) {
                    Map<String, String> chargingDetails = (Map<String, String>) flowDetails.get(KiteKeyBox.chargingK);
                    if (chargingDetails != null) {
                        putLogElement(KiteLog.LogField.CHARGING_TYPE.getParameter(), chargingDetails.get(KiteKeyBox.typeK),
                                requestContext);
                        putLogElement(
                                KiteLog.LogField.CHARGE_PARTY_TYPE.getParameter(),
                                getChargingPartyType(chargingDetails.get(KiteKeyBox.partyK),
                                        chargingDetails.get(KiteKeyBox.typeK)), requestContext);

                        String[] chargedAddress = getChargedAddress(chargingDetails.get(KiteKeyBox.typeK),
                                chargingDetails.get(KiteKeyBox.partyK),
                                (String) request.get(KiteKeyBox.senderAddressK),
                                (String) request.get(KiteKeyBox.senderAddressPlainTextK), sp);
                        putLogElement(KiteLog.LogField.CHARGE_ADDRESS.getParameter(), chargedAddress[0], requestContext);
                        putLogElement(KiteLog.LogField.MASKED_CHARGE_ADDRESS.getParameter(), chargedAddress[1], requestContext);

                        putLogElement(KiteLog.LogField.CHARGED_CATEGORY.getParameter(),
                                request.get(KiteKeyBox.chargedCategoryK), requestContext);
                        putLogElement(KiteLog.LogField.CHARGE_AMOUNT.getParameter(), request.get(KiteKeyBox.chargedAmountK),
                                requestContext);
                        putLogElement(KiteLog.LogField.RATE_CARD.getParameter(),
                                refineRateCard((String) request.get(KiteKeyBox.usedExchangeRatesK)), requestContext);
                        putLogElement(KiteLog.LogField.CHARGE_AMOUNT_CURRENCY.getParameter(),
                                request.get(KiteKeyBox.currencyCodeK), requestContext);
                        putLogElement(KiteLog.LogField.PGW_TRANSACTION_ID.getParameter(),
                                (String) request.get(KiteKeyBox.externalTransIdK), requestContext);
                        if (KiteKeyBox.operatorChargingK.equals(chargingDetails.get(KiteKeyBox.methodK))) {
                            putLogElement(KiteLog.LogField.OPERATOR_COST.getParameter(), request.get(KiteKeyBox.operatorCostK),
                                    requestContext);
                            putLogElement(KiteLog.LogField.CHARGE_AMOUNT.getParameter(), request.get(KiteKeyBox.operatorCostK),
                                    requestContext);
                        }
                        putLogElement(KiteLog.LogField.EVENT_TYPE.getParameter(),
                                getEventType((String) request.get(KiteKeyBox.chargingStatusK)), requestContext);
                    }
                }
            }

        }

    }
}
