/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class NblKeyMapper {
    private static final Logger logger = LoggerFactory.getLogger(NblKeyMapper.class);
    public static final String statusCode = "statusCode";
    public static final String statusDetail = "statusDetail";

    private Map<String, String> newNblApiKeyMapper;
    private Map<String, String> oldNblApiKeyMapper;
    private List<String> unwantedKeysInAppResponse;

    /**
     * @param request
     * @param convertToInternalNblApi
     * @return map
     */
    public Map<String, Object> convert(Map<String, Object> request, boolean convertToInternalNblApi) {

        Map<String, Object> mappedRequest = new HashMap<String, Object>();

        for (Map.Entry entry : request.entrySet()) {
            String requestKey = (String) entry.getKey();
            String mappedKey = convertToInternalNblApi ? getInternalNblKey(requestKey) : getExternalNblKey(requestKey);
            logger.trace("Request Key: [{}] mapped to New key: [{}]", requestKey, mappedKey);
            Object mappedValues = entry.getValue() instanceof Map ? convert((Map<String, Object>) entry.getValue(), convertToInternalNblApi)
                    : (entry.getValue() instanceof List ? convert((Collection<Object>) entry.getValue(), convertToInternalNblApi) : entry.getValue());
            if(mappedKey != null)
                mappedRequest.put(mappedKey, mappedValues);
            /*mappedRequest.put(mappedKey, entry.getValue());*/
        }
        return mappedRequest;
    }

    public Collection<Object> convert(Collection<Object> list, boolean convertToInternalNblApi) {
        List<Object> newList = new ArrayList<Object>();
        for (Object item : list) {
            if (item instanceof Map) {
                newList.add(convert((Map<String, Object>) item, convertToInternalNblApi));
            } else if (item instanceof List) {
                newList.addAll(convert((Collection<Object>) item, convertToInternalNblApi));
            } else {
                newList.add(item);
            }
        }
        return newList;
    }

    /**
     * When an external nbl key is password, the corresponding and supported internal nbl key is returned.
     * @param newNblKey
     * @return string
     */
    public String getInternalNblKey(String newNblKey) {

        if (newNblApiKeyMapper.containsKey(newNblKey)) {
            return newNblApiKeyMapper.get(newNblKey);
        }
//        throw new SdpException(invalidRequestErrorCode, "New NBL Key [" + newNblKey + "] is not recognizable");
        return newNblKey;
    }

    /**
     * When an internal nbl key is password, the corresponding and supported external nbl key is returned.
     * @param oldNblKey
     * @return string
     */
    public String getExternalNblKey(String oldNblKey) {

        if (oldNblApiKeyMapper.containsKey(oldNblKey)) {
            return filterUnwantedKey(oldNblApiKeyMapper.get(oldNblKey));
        }
//        throw new SdpException(invalidRequestErrorCode, "Old NBL Key [" + oldNblKey + "] is not recognizable");
        return filterUnwantedKey(oldNblKey);
    }
    
    private String filterUnwantedKey(String key) {
        if((unwantedKeysInAppResponse == null) || (!unwantedKeysInAppResponse.contains(key))) {
            return key;
        } else {
            logger.trace("Unwanted key found: [{}]", key);
            return null;
        }
    }

    public Map<String, String> getNewNblApiKeyMapper() {
        return newNblApiKeyMapper;
    }

    public void setNewNblApiKeyMapper(Map<String, String> newNblApiKeyMapper) {
        this.newNblApiKeyMapper = newNblApiKeyMapper;
        setOldNblApiKeyMapper(newNblApiKeyMapper);
    }

    public Map<String, String> getOldNblApiKeyMapper() {
        return oldNblApiKeyMapper;
    }

    private void setOldNblApiKeyMapper(Map<String, String> newNblApiKeyMapper) {
        oldNblApiKeyMapper = new HashMap<String, String>();
        for (Map.Entry<String, String> entry : newNblApiKeyMapper.entrySet()) {
            oldNblApiKeyMapper.put(entry.getValue(), entry.getKey());
        }
    }

    public void setUnwantedKeysInAppResponse(List<String> unwantedKeysInAppResponse) {
        this.unwantedKeysInAppResponse = unwantedKeysInAppResponse;
    }
}
