/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.util;

import static hms.kite.util.KiteKeyBox.applicationIdK;
import static hms.kite.util.KiteKeyBox.deliveryReportNotRequiredK;
import static hms.kite.util.KiteKeyBox.deliveryReportRequiredK;
import static hms.kite.util.KiteKeyBox.encodingBinaryK;
import static hms.kite.util.KiteKeyBox.encodingFlashK;
import static hms.kite.util.KiteKeyBox.encodingTextK;
import static hms.kite.util.KiteKeyBox.requestK;
import static hms.kite.util.KiteKeyBox.serviceIndicatorK;
import static hms.kite.util.KiteKeyBox.serviceLocatorK;
import static hms.kite.util.KiteKeyBox.wapPushTypeNblK;

import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class NblRequestValidator {
	public static void isValidStatusRequest(Object statusRequest) {
		if (!(statusRequest == null || (statusRequest.equals(deliveryReportNotRequiredK) || statusRequest
				.equals(deliveryReportRequiredK)))) {
			throw new IllegalArgumentException("Status request is present and invalid [" + statusRequest + "]");
		}
	}

	public static void isEncodingValid(Object encoding) {
		if (!(encoding == null || (encoding.equals(encodingTextK) || encoding.equals(encodingFlashK) || encoding
				.equals(encodingBinaryK)))) {
			throw new IllegalArgumentException("Encoding is present and invalid [" + encoding + "]");
		}
	}

	public static void destinationsValid(Object destinations) {
		if (destinations == null || !(destinations instanceof List) || ((List<String>) destinations).isEmpty()) {
			throw new IllegalArgumentException("Invalid destinations [" + destinations + "]");
		}
        //todo enhance the descriptions.
	}

	public static void validLoginDetails(Map<String, Object> request) {
		if (!request.containsKey(applicationIdK) || request.get(applicationIdK) == null
				|| request.get(applicationIdK).toString().isEmpty()) {
			throw new IllegalArgumentException("ApplicationId cannot be empty");
		}
	}

	public static void isMessageValid(Object message) {
		if (message == null || message.toString().trim().isEmpty()) {
			throw new IllegalArgumentException("Message cannot be empty");
		}
	}

	public static void isWapPushUrlValid(Object url) {
		if (url == null || url.toString().isEmpty()) {
			throw new IllegalArgumentException("Wap-push url cannot be empty");
		}
	}

	public static void isWappushTypeValid(Map<String, Map<String, Object>> requestContext) {
		if (!(requestContext.get(requestK).get(wapPushTypeNblK) == null
				|| requestContext.get(requestK).get(wapPushTypeNblK).toString().equals(serviceIndicatorK) || requestContext
				.get(requestK).get(wapPushTypeNblK).toString().equals(serviceLocatorK))) {
			throw new IllegalArgumentException("Wap-push type is present and invalid ["
					+ requestContext.get(requestK).get(wapPushTypeNblK) + "]");
		}
	}
}
