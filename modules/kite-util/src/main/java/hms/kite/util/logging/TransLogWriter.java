/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.util.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hms.kite.util.KiteKeyBox;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static hms.kite.util.logging.KiteLog.LogField;

/**
 * Writes the trans-log into the standard logging. $LastChangedDate 8/31/11 9:35
 * AM$ $LastChangedBy ruwan$ $LastChangedRevision$
 */
/* package private */class TransLogWriter {
	private static final Logger logger = LoggerFactory.getLogger(TransLogWriter.class);
	private static final Logger transLogLogger = LoggerFactory.getLogger("SDP.TransLog");

	private static final String logFormat;

	static {
		StringBuffer stringBuffer = new StringBuffer();
		int fieldsCount = LogField.values().length;
		int i = 0;
		while (i < fieldsCount) {
			i++;
            stringBuffer.append("{}");
            if (i < fieldsCount) {
                stringBuffer.append("|");
            }
		}

		logFormat = stringBuffer.toString();
	}

	public static void log(Map<String, Object> loggingContext) {

		if (loggingContext == null) {
			return;
		}

		List<Map<String, Object>> recipientsStatus = (List) loggingContext.get(TransLog.recipientsStatusK);
        List<String> values = new ArrayList<String>();
		if (recipientsStatus != null) {
			String isMultipleRecipeint = recipientsStatus.size() > 1 ? "1" : "0";
			for (Map<String, Object> recipientData : recipientsStatus) {
				for (LogField logField : LogField.values()) {
					if (logField == LogField.DESTINATION_ADDRESS) {
						Object value = recipientData.get(KiteKeyBox.recipientAddressK);
						values.add(getNotNullStr(value));
					} else if (logField == LogField.MASKED_DESTINATION_ADDRESS) {
						Object value = recipientData.get(LogField.MASKED_DESTINATION_ADDRESS.getParameter());
						values.add(getNotNullStr(value));
					} else if (logField == LogField.RESPONSE_CODE) {
						Object value = recipientData.get(LogField.RESPONSE_CODE.getParameter());
						values.add(getNotNullStr(value));
					} else if (logField == LogField.RESPONSE_DESCRIPTION) {
						Object value = recipientData.get(LogField.RESPONSE_DESCRIPTION.getParameter());
						values.add(getNotNullStr(value));
					} else if (logField == LogField.CHARGING_TYPE) {
						Object value = recipientData.get(LogField.CHARGING_TYPE.getParameter());
						values.add(getNotNullStr(value));
					} else if (logField == LogField.CHARGE_PARTY_TYPE) {
						Object value = recipientData.get(LogField.CHARGE_PARTY_TYPE.getParameter());
						values.add(getNotNullStr(value));
					} else if (logField == LogField.IS_MULTIPLE_RECEIPIENTS) {
						values.add(isMultipleRecipeint);
					} else if (logField == LogField.OPERATOR) {
						Object value = recipientData.get(LogField.OPERATOR.getParameter());
						values.add(getNotNullStr(value));
					} else if (logField == LogField.RATE_CARD) {
						Object value = recipientData.get(LogField.RATE_CARD.getParameter());
						values.add(getNotNullStr(value));
					} else if (logField == LogField.CHARGE_AMOUNT_CURRENCY) {
						Object value = recipientData.get(LogField.CHARGE_AMOUNT_CURRENCY.getParameter());
						values.add(getNotNullStr(value));
					} else if (logField == LogField.CHARGE_ADDRESS) {
						Object value = recipientData.get(LogField.CHARGE_ADDRESS.getParameter());
						values.add(getNotNullStr(value));
					} else if (logField == LogField.MASKED_CHARGE_ADDRESS) {
						Object value = recipientData.get(LogField.MASKED_CHARGE_ADDRESS.getParameter());
						values.add(getNotNullStr(value));
					} else if (logField == LogField.CHARGED_CATEGORY) {
						Object value = recipientData.get(LogField.CHARGED_CATEGORY.getParameter());
						values.add(getNotNullStr(value));
					} else if (logField == LogField.OPERATOR_COST) {
						Object value = recipientData.get(LogField.OPERATOR_COST.getParameter());
						values.add(getNotNullStr(value));
					} else if (logField == LogField.CHARGE_AMOUNT) {
						Object value = recipientData.get(LogField.CHARGE_AMOUNT.getParameter());
						values.add(getNotNullStr(value));
					} else if (logField == LogField.PGW_TRANSACTION_ID) {
						Object value = recipientData.get(LogField.PGW_TRANSACTION_ID.getParameter());
						values.add(getNotNullStr(value));
					} else if (logField == LogField.EVENT_TYPE) {
						Object value = recipientData.get(LogField.EVENT_TYPE.getParameter());
						values.add(getNotNullStr(value));
					} else if(logField == LogField.FROM_PAYMENT_INSTRUMENT_NAME) {
                        Object value = recipientData.get(LogField.FROM_PAYMENT_INSTRUMENT_NAME.getParameter());
                        values.add(getNotNullStr(value));
                    } else {
						Object value = loggingContext.get(logField.getParameter());
						values.add(getNotNullStr(value));
					}
				}
            transLogLogger.info(logFormat, values.toArray());
            values.clear();
            }
		} else {
			for (LogField logField : LogField.values()) {
				Object value = loggingContext.get(logField.getParameter());
				values.add(getNotNullStr(value));
			}
            transLogLogger.info(logFormat, values.toArray());
		}

		loggingContext.put(TransLog.contextLoggedK, Boolean.TRUE.toString());
	}

	private static String getNotNullStr(Object value) {
        String field = value != null ? String.valueOf(value) : "";
        return field.replace("|", "_");
	}

}
