/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.util;

import hms.common.rest.util.Message;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 */
@Consumes({ "application/json" })
@Path("/notify/")
public class SblNotifyEndpoint {
	private static final Logger LOGGER = LoggerFactory.getLogger(SblNotifyEndpoint.class);
	private MessageCorrelator correlator;

	@Consumes({ "application/json" })
	@POST
	@Path("/receive")
	public Map<String, Object> onSmsNotifySimple(Map<String, Object> response) {
		long start = System.currentTimeMillis();
		try {
			LOGGER.info("Sms SubmitResponse received from SBL[{}]", response);
			correlator.notify(response);
            Map<String, Object> resp = new HashMap<String, Object>();
			resp.put("status", "notified");
			return resp;
		} finally {
			LOGGER.debug("start[{}] time[{}] tag[MT-RESP-PROCESS]", start, (System.currentTimeMillis() - start));
		}
	}

	public void setCorrelator(MessageCorrelator correlator) {
		this.correlator = correlator;
	}
}
