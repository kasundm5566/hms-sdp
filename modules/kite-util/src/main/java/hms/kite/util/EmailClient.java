/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.util;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

/**
 * $LastChangedDate: 2011-06-05 09:31:14 +0530 (Sun, 05 Jun 2011) $
 * $LastChangedBy: danukas $
 * $LastChangedRevision: 73804 $
 */
public class EmailClient {

	private transient MailSender mailSender;
	private static final Logger LOGGER = LoggerFactory.getLogger(EmailClient.class);


	public boolean send(String to, String from, String[] ccList, String subject, String text) {
		try {
			LOGGER.debug("email parameters [{}]", new Object[]{to, from, ccList, subject, text});
			SimpleMailMessage mailMessage = new SimpleMailMessage();
			mailMessage.setSubject(subject);
			mailMessage.setText(text);
			mailMessage.setFrom(from);
			mailMessage.setTo(to);
			if(ccList != null && !(ccList.length == 0)) {
				mailMessage.setCc(ccList);
			}
			mailSender.send(mailMessage);
			LOGGER.info("email was sent to [{}] & [{}] successfully.", to, ccList);
			return true;
		} catch(Throwable t) {
			LOGGER.error(String.format("Message sending failed for [%s,  %s, %s, %s, %s]", 
					new Object[]{to, from, ccList, subject, text}), t);
			return false;
		}
	}

	public void setMailSender(MailSender mailSender) {
		this.mailSender = mailSender;
	}

}
