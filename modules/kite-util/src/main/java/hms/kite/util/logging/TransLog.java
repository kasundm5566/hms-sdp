/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.util.logging;

import hms.kite.util.KiteErrorBox;
import hms.kite.util.KiteKeyBox;
import hms.kite.util.StatusDescriptionBuilder;
import hms.kite.util.logging.KiteLog.LogField;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.KeyNameSpaceResolver.data;
import static hms.kite.util.logging.TransLogTranslations.*;

/**
 * Trans-Log for the SDP
 *
 * $LastChangedDate 8/31/11 11:26 AM$ $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
public class TransLog {
	private static final Logger LOGGER = LoggerFactory.getLogger(TransLog.class);
	public static final String loggingContextK = "logging_context";
	public static final String contextLoggedK = "context_logged";
	public static final String recipientsStatusK = "recipients";
	private static final String PERCENTAGE_FROM_MONTHLY_REVENUE = "percentageFromMonthlyRevenue";
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS");
	public static final String logFromInternalWorkFlow = "log-from-internal-workflow";

	private static StatusDescriptionBuilder statusDescriptionBuilder;

	static {
		loadProperties();
	}

	private static void loadProperties() {
		statusDescriptionBuilder = new StatusDescriptionBuilder("status-message");
		statusDescriptionBuilder.init();
	}

	public static void log(Map<String, Map<String, Object>> requestContext) {
		LOGGER.trace("Context before printing translog[{}]", requestContext);
		TransLogWriter.log(requestContext.get(TransLog.loggingContextK));
	}

	public static void logBroadCastRequest(Map<String, Object> broadcastContext) {
		LOGGER.trace("Broadcast Context before printing translog[{}]", broadcastContext);
		TransLogWriter.log((Map<String, Object>) broadcastContext.get(TransLog.loggingContextK));
	}

	/**
	 * Puts the logging element onto the request context. Creates new logging
	 * context in the request context if not exists. Put the logging value in
	 * the logging context keyed by the name.
	 *
	 * @param name
	 * @param value
	 * @param requestContext
	 */
	public static void putLogElement(String name, Object value, Map<String, Map<String, Object>> requestContext) {
		Map<String, Object> loggingContext = requestContext.get(loggingContextK);
		if (loggingContext == null) {
			loggingContext = new HashMap<String, Object>();
			requestContext.put(loggingContextK, loggingContext);
		}

		loggingContext.put(name, value);
	}

//	public static void gatherLogParametersFromBroadcast(Map<String, Object> broadcastContext) {
//		fetchBroadcastRequestLogElement(LogField.CORRELATION_ID, KiteKeyBox.correlationIdK, broadcastContext);
//		String receiveTime = (String) broadcastContext.get(KiteKeyBox.receiveTimeK);
//		if (receiveTime != null) {
//			Date d = new Date(Long.parseLong(receiveTime));
//			putBroadcastRequestLogElement(LogField.TIME_STAMP, DATE_FORMAT.format(d), broadcastContext);
//		} else {
//			putBroadcastRequestLogElement(LogField.TIME_STAMP, DATE_FORMAT.format(new Date()), broadcastContext);
//		}
//
//		boolean isMaskedApp = false;
//		// if(broadcastContext.get(key))/
//
//		fetchBroadcastRequestLogElement(LogField.DIRECTION, KiteKeyBox.directionK, broadcastContext);
//		fetchBroadcastRequestLogElement(LogField.NCS, KiteKeyBox.ncsTypeK, broadcastContext);
//		fetchBroadcastRequestLogElement(LogField.ADVERTISEMENT_ID, KiteKeyBox.adNameK, broadcastContext);
//		fetchBroadcastRequestLogElement(LogField.ADVERTISEMENT_NAME, KiteKeyBox.adTextK, broadcastContext);
//		fetchBroadcastRequestLogElement(LogField.APP_ID, KiteKeyBox.appIdK, broadcastContext);
//		fetchBroadcastRequestLogElement(LogField.APP_NAME, KiteKeyBox.nameK, broadcastContext);
//		fetchBroadcastRequestLogElement(LogField.SP_ID, KiteKeyBox.spIdK, broadcastContext);
//		fetchBroadcastRequestLogElement(LogField.SP_NAME, KiteKeyBox.spNameK, broadcastContext);
//		putBroadcastRequestLogElement(LogField.BILLING_TYPE, "unknown", broadcastContext);
//		putBroadcastRequestLogElement(LogField.SOURCE_CHANNEL_TYPE, "sms", broadcastContext);
//		putBroadcastRequestLogElement(LogField.SOURCE_CHANNEL_PROTOCOL, "http", broadcastContext);
//		putBroadcastRequestLogElement(LogField.DESTINATION_CHANNEL_TYPE, "sms", broadcastContext);
//		putBroadcastRequestLogElement(LogField.DESTINATION_CHANNEL_PROTOCOL, "smpp", broadcastContext);
//		putBroadcastRequestLogElement(LogField.REVENUE_SHARE_TYPE, PERCENTAGE_FROM_MONTHLY_REVENUE, broadcastContext);
//
//		List<Map<String, Object>> recipients = (List) broadcastContext.get(KiteKeyBox.recipientsK);
//		Map<String, Object> recipient = recipients.get(0);
//
//		if (recipient.containsKey(KiteKeyBox.recipientAddressPlainTextK)) {
//			isMaskedApp = true;
//		}
//
//		if (isMaskedApp) {
//			putBroadcastRequestLogElement(LogField.DESTINATION_ADDRESS,
//					(String) recipient.get(KiteKeyBox.recipientAddressPlainTextK), broadcastContext);
//			putBroadcastRequestLogElement(LogField.MASKED_DESTINATION_ADDRESS,
//					(String) recipient.get(KiteKeyBox.recipientAddressK), broadcastContext);
//		} else {
//			putBroadcastRequestLogElement(LogField.DESTINATION_ADDRESS,
//					(String) recipient.get(KiteKeyBox.recipientAddressK), broadcastContext);
//		}
//		putBroadcastRequestLogElement(LogField.RESPONSE_CODE,
//				(String) recipient.get(KiteKeyBox.recipientAddressStatusK), broadcastContext);
//		putBroadcastRequestLogElement(LogField.RESPONSE_DESCRIPTION,
//				(String) broadcastContext.get(KiteKeyBox.statusDescriptionK), broadcastContext);
//		putBroadcastRequestLogElement(LogField.TRANSACTION_STATUS,
//				getTransacationStatus((String) recipient.get(KiteKeyBox.recipientAddressStatusK)), broadcastContext);
//		putBroadcastRequestLogElement(LogField.OPERATOR, (String) recipient.get(KiteKeyBox.recipientAddressOptypeK),
//				broadcastContext);
//		putBroadcastRequestLogElement(LogField.RATE_CARD,
//				refineRateCard((String) recipient.get(KiteKeyBox.usedExchangeRatesK)), broadcastContext);
//		putBroadcastRequestLogElement(LogField.CHARGE_AMOUNT_CURRENCY,
//				(String) recipient.get(KiteKeyBox.currencyCodeK), broadcastContext);
//		putBroadcastRequestLogElement(LogField.CHARGING_TYPE, (String) recipient.get("charging_type"), broadcastContext);
//		putBroadcastRequestLogElement(
//				LogField.CHARGE_PARTY_TYPE,
//				getChargingPartyType((String) recipient.get(KiteKeyBox.chargedPartyTypeK),
//						(String) recipient.get("charging_type")), broadcastContext);
//		String[] chargedAddress = getChargedAddress((String) recipient.get("charging_type"),
//				(String) recipient.get(KiteKeyBox.chargedPartyTypeK),
//				(String) recipient.get(KiteKeyBox.recipientAddressK),
//				(String) recipient.get(KiteKeyBox.recipientAddressPlainTextK),
//				(String) broadcastContext.get(KiteKeyBox.spIdK));
//		putBroadcastRequestLogElement(LogField.CHARGE_ADDRESS, chargedAddress[0], broadcastContext);
//		putBroadcastRequestLogElement(LogField.MASKED_CHARGE_ADDRESS, chargedAddress[1], broadcastContext);
//
//		putBroadcastRequestLogElement(LogField.CHARGED_CATEGORY,
//				(String) recipient.get(LogField.CHARGED_CATEGORY.getParameter()), broadcastContext);
//		putBroadcastRequestLogElement(LogField.OPERATOR_COST,
//				(String) recipient.get(LogField.OPERATOR_COST.getParameter()), broadcastContext);
//		putBroadcastRequestLogElement(LogField.CHARGE_AMOUNT,
//				(String) recipient.get(LogField.CHARGE_AMOUNT.getParameter()), broadcastContext);
//		putBroadcastRequestLogElement(LogField.PGW_TRANSACTION_ID, (String) recipient.get(KiteKeyBox.externalTransIdK),
//				broadcastContext);
//
//		if (isMaskedApp) {
//			fetchBroadcastRequestLogElement(LogField.SOURCE_ADDRESS, KiteKeyBox.senderAddressPlainTextK,
//					broadcastContext);
//			fetchBroadcastRequestLogElement(LogField.MASKED_SOURCE_ADDRESS, KiteKeyBox.senderAddressK, broadcastContext);
//		} else {
//			fetchBroadcastRequestLogElement(LogField.SOURCE_ADDRESS, KiteKeyBox.senderAddressK, broadcastContext);
//		}
//
//		putBroadcastRequestLogElement(LogField.IS_NUMBER_MASKED, getMaskedStatus(isMaskedApp), broadcastContext);
//
//	}

	/**
	 * Gather the log parameters from the request context. This may not gather
	 * all the required parameters. However maximum effort is made to gather all
	 * the parameters from the request context.
	 *
	 * User code of TransLog has option to set the log parameter using
	 * #putLogElement(String, Object, Map) individually at the relevant code
	 * segments or add them in the #gatherLogParameters(Map) method.
	 *
	 * @param requestContext
	 */
	public static void gatherLogParameters(Map<String, Map<String, Object>> requestContext) {
		Map<String, Object> app = requestContext.get(KiteKeyBox.appK);
		Map<String, Object> ncs = requestContext.get(KiteKeyBox.ncsK);
		Map<String, Object> request = requestContext.get(KiteKeyBox.requestK);
		Map<String, Object> sp = requestContext.get(KiteKeyBox.spK);

		boolean isMaskedApp = false;
		if (app != null) {
			fetchLogParameter(KiteKeyBox.spIdK, LogField.SP_ID.getParameter(), app, requestContext);
			fetchLogParameter(KiteKeyBox.spNameK, LogField.SP_NAME.getParameter(), app, requestContext);
			fetchLogParameter(KiteKeyBox.appIdK, LogField.APP_ID.getParameter(), app, requestContext);
			fetchLogParameter(KiteKeyBox.nameK, LogField.APP_NAME.getParameter(), app, requestContext);
			putLogElement(LogField.APP_STATE.getParameter(),
					TransLogTranslations.translateAppState((String) app.get(KiteKeyBox.statusK)), requestContext);

			isMaskedApp = (Boolean) app.get(KiteKeyBox.maskNumberK);
			putLogElement(LogField.IS_NUMBER_MASKED.getParameter(), getMaskedStatus(isMaskedApp), requestContext);
		}

		if (request != null) {
            String ncsType = (String) request.get(ncsTypeK);

			fetchLogParameter(KiteKeyBox.correlationIdK, LogField.CORRELATION_ID.getParameter(), request,
					requestContext);

            // TODO : Fix this status code mapping after pgw send the status code in string+numbers than plain text.
            if(KiteKeyBox.casK.equals(ncsType)) {
                putLogElement(LogField.OVERALL_RESPONSE_CODE.getParameter(), getCaasStatusCode((String) request.get(KiteKeyBox.pgwStatusCodeK)), requestContext);
                putLogElement(LogField.OVERALL_RESPONSE_DESCRIPTION.getParameter(), getTransacationStatus(getCaasStatusCode((String) request.get(KiteKeyBox.pgwStatusCodeK))), requestContext);
            } else {
                fetchLogParameter(KiteKeyBox.statusCodeK, LogField.OVERALL_RESPONSE_CODE.getParameter(), request,
                    requestContext);
                fetchLogParameter(KiteKeyBox.statusDescriptionK, LogField.OVERALL_RESPONSE_DESCRIPTION.getParameter(),
                    request, requestContext);
            }

            String receiveTime = (String) request.get(KiteKeyBox.receiveTimeK);
			if (receiveTime != null) {
				fetchDateTimeLogParameter(KiteKeyBox.receiveTimeK, LogField.TIME_STAMP.getParameter(), request,
						requestContext);
			} else {
				putLogElement(LogField.TIME_STAMP.getParameter(), DATE_FORMAT.format(new Date()), requestContext);
			}

			if (isMaskedApp) {
				fetchLogParameter(KiteKeyBox.senderAddressK, LogField.MASKED_SOURCE_ADDRESS.getParameter(), request,
						requestContext);
				fetchLogParameter(KiteKeyBox.senderAddressPlainTextK, LogField.SOURCE_ADDRESS.getParameter(), request,
						requestContext);
			} else {
				fetchLogParameter(KiteKeyBox.senderAddressK, LogField.SOURCE_ADDRESS.getParameter(), request,
						requestContext);
			}

			fetchLogParameter(KiteKeyBox.directionK, LogField.DIRECTION.getParameter(), request, requestContext);

			if (KiteKeyBox.mtK.equals(request.get(KiteKeyBox.directionK))) {
                if(KiteKeyBox.casK.equals(request.get(KiteKeyBox.ncsK)))    {
                    putLogElement(LogField.OVERALL_RESPONSE_CODE.getParameter(), request.get(KiteKeyBox.statusCodeK), requestContext);
                    putLogElement(LogField.OVERALL_RESPONSE_DESCRIPTION.getParameter(), getTransacationStatus((String) request.get(KiteKeyBox.statusCodeK)), requestContext);
                } else {
				    fetchLogParameter(KiteKeyBox.statusCodeK, LogField.OVERALL_RESPONSE_CODE.getParameter(), request,
						requestContext);
				    fetchLogParameter(KiteKeyBox.statusDescriptionK, LogField.OVERALL_RESPONSE_DESCRIPTION.getParameter(),
						request, requestContext);
                }
				putLogElement(LogField.TRANSACTION_STATUS.getParameter(), getTransacationStatus((String) request.get(KiteKeyBox.statusCodeK)), requestContext);
			} else if (KiteKeyBox.moK.equals(request.get(KiteKeyBox.directionK))) {

				/*
				 * Use either 'sender-address-status' or 'status' because for
				 * some scenarios we get one in success state and other in error
				 * state. If one of them has an error code use that. If both has
				 * error code user error code in 'status' field.
				 */
				String addressStatus = (String) request.get(KiteKeyBox.senderAddressStatusK);
				String statusCode = (String) request.get(KiteKeyBox.statusCodeK);
				String requestStatus = KiteErrorBox.successCode;
				if (KiteErrorBox.successCode.equals(addressStatus)) {
					requestStatus = statusCode;
				} else if (KiteErrorBox.successCode.equals(statusCode)) {
					requestStatus = addressStatus;
				} else {
					requestStatus = statusCode;
				}

                if(KiteKeyBox.casK.equals(ncsType)) {
                    putLogElement(LogField.RESPONSE_CODE.getParameter(), getCaasStatusCode((String) request.get(KiteKeyBox.pgwStatusCodeK)), requestContext);
                    putLogElement(LogField.RESPONSE_DESCRIPTION.getParameter(), getTransacationStatus(getCaasStatusCode((String) request.get(KiteKeyBox.pgwStatusCodeK))), requestContext);
                    putLogElement(LogField.TRANSACTION_STATUS.getParameter(), getTransacationStatus(getCaasStatusCode((String) request.get(KiteKeyBox.pgwStatusCodeK))), requestContext);

                } else {
				    putLogElement(LogField.RESPONSE_CODE.getParameter(), requestStatus, requestContext);
				    putLogElement(LogField.RESPONSE_DESCRIPTION.getParameter(),
						buildStatusDescriptionIfNotAvailable(requestStatus, null, requestContext), requestContext);
				    putLogElement(LogField.TRANSACTION_STATUS.getParameter(), getTransacationStatus(requestStatus),
						requestContext);
                }
			} else if(KiteKeyBox.drK.equals(request.get(KiteKeyBox.directionK))){
                putLogElement(LogField.CHARGE_AMOUNT.getParameter(), request.get("add_charged-amount"), requestContext);
                putLogElement(LogField.OVERALL_RESPONSE_CODE.getParameter(), request.get(deliveryStatusK), requestContext);
                putLogElement(LogField.OVERALL_RESPONSE_DESCRIPTION.getParameter(), request.get(deliveryStatusDescriptionK), requestContext);
            }

			fetchLogParameter(KiteKeyBox.keywordK, LogField.KEYWORD.getParameter(), request, requestContext);
			fetchLogParameter(KiteKeyBox.shortcodeK, LogField.SHORT_CODE.getParameter(), request, requestContext);
			fetchLogParameter(KiteKeyBox.operatorK, LogField.OPERATOR.getParameter(), request, requestContext);
			fetchLogParameter(KiteKeyBox.ncsTypeK, LogField.NCS.getParameter(), request, requestContext);
			fetchLogParameter(KiteKeyBox.adNameK, LogField.ADVERTISEMENT_ID.getParameter(), request, requestContext);
			fetchLogParameter(KiteKeyBox.adTextK, LogField.ADVERTISEMENT_NAME.getParameter(), request, requestContext);

            if(ncsType != null && ncsType.equals(ussdK)) {
                fetchLogParameter(KiteKeyBox.serviceCodeK, LogField.SHORT_CODE.getParameter(), request, requestContext);
            }

            fetchLogParameter(KiteKeyBox.invoiceK, LogField.INVOICE_NO.getParameter(), request, requestContext);
            fetchLogParameter(KiteKeyBox.orderNoK, LogField.ORDER_NO.getParameter(), request, requestContext);
            fetchLogParameter(KiteKeyBox.externalTransIdNblK, LogField.EXTERNAL_TRX_ID.getParameter(), request, requestContext);
            fetchLogParameter(KiteKeyBox.balanceDueK, LogField.BALANCE_DUE.getParameter(), request, requestContext);
            fetchLogParameter(KiteKeyBox.totalAmountK, LogField.TOTAL_AMOUNT.getParameter(), request, requestContext);
            fetchLogParameter(KiteKeyBox.ussdOpK, LogField.USSD_OPERATION.getParameter(), request, requestContext);
            fetchLogParameter(KiteKeyBox.sessionIdK, LogField.SESSION_ID.getParameter(), request, requestContext);
		}

		if (sp != null) {
			fetchLogParameter(KiteKeyBox.coopUserNameK, LogField.SP_NAME.getParameter(), sp, requestContext);
		}

		putLogElement(LogField.BILLING_TYPE.getParameter(), "unknown", requestContext);

		putRevenueShareDetails(requestContext, request, app);

		if (isCaasRequest(ncs)) {
			putCaasSpecificDetails(requestContext, request, isMaskedApp);
		} else {
			putRecipientsDetails(requestContext, request, sp, isMaskedApp);
		}

		putChannelDetails(requestContext, request);

		putChargingDetailsForMo(requestContext, request, ncs, sp);

	}

    private static Object d(Map requestContext, String... keys) {
        return data((Map)requestContext, keys);
    }

    private static void putCaasSpecificDetails(Map<String, Map<String, Object>> requestContext,
			Map<String, Object> request, boolean isMaskedApp) {
		if (request != null) {

			//List<Map<String, Object>> recipientsStatus = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> recipients = (List) request.get(KiteKeyBox.recipientsK);
			if (recipients != null && recipients.size() > 0) {
				Map<String, Object> recipient = recipients.get(0);
				//Map<String, Object> recipientTransLog = new HashMap<String, Object>();*/
				if (isMaskedApp) {
                    putLogElement(KiteKeyBox.recipientAddressK,
							recipient.get(KiteKeyBox.recipientAddressPlainTextK), requestContext);
                    putLogElement(LogField.MASKED_DESTINATION_ADDRESS.getParameter(),
							recipient.get(KiteKeyBox.recipientAddressK), requestContext);
				} else {
                    putLogElement(KiteKeyBox.recipientAddressK, recipient.get(KiteKeyBox.recipientAddressK), requestContext);
				}
                putLogElement(LogField.EVENT_TYPE.getParameter(), request.get(KiteKeyBox.requestTypeK), requestContext);
                //putLogElement(LogField.CHARGE_AMOUNT.getParameter(), request.get(KiteKeyBox.amountK), requestContext);
                putLogElement(LogField.CHARGE_AMOUNT_CURRENCY.getParameter(), request.get(KiteKeyBox.currencyK), requestContext);



				//recipientsStatus.add(recipientTransLog);
			}

            if(KiteKeyBox.mtK.equals((String)request.get(KiteKeyBox.directionK)))   {
                putLogElement(LogField.PGW_TRANSACTION_ID.getParameter(), request.get(KiteKeyBox.internalTrxIdCaasReq), requestContext);
                putLogElement(LogField.RESPONSE_CODE.getParameter(), request.get(KiteKeyBox.statusCodeK), requestContext);
                putLogElement(LogField.RESPONSE_DESCRIPTION.getParameter(), getTransacationStatus((String)request.get(KiteKeyBox.statusCodeK)), requestContext);
                putLogElement(LogField.OVERALL_RESPONSE_CODE.getParameter(), request.get(KiteKeyBox.statusCodeK), requestContext);
                putLogElement(LogField.OVERALL_RESPONSE_DESCRIPTION.getParameter(), getTransacationStatus((String) request.get(KiteKeyBox.statusCodeK)), requestContext);
                putLogElement(LogField.CHARGE_AMOUNT.getParameter(), request.get(KiteKeyBox.paidAmountForSpK), requestContext);
            } else if(KiteKeyBox.moK.equals((String)request.get(KiteKeyBox.directionK)))    {
                putLogElement(LogField.PGW_TRANSACTION_ID.getParameter(), request.get(KiteKeyBox.internalTransIdK), requestContext);
                putLogElement(LogField.RESPONSE_CODE.getParameter(), getCaasStatusCode((String) request.get(KiteKeyBox.pgwStatusCodeK)), requestContext);
                putLogElement(LogField.RESPONSE_DESCRIPTION.getParameter(), getTransacationStatus(getCaasStatusCode((String) request.get(KiteKeyBox.pgwStatusCodeK))), requestContext);
                putLogElement(LogField.OVERALL_RESPONSE_CODE.getParameter(), getCaasStatusCode((String) request.get(KiteKeyBox.pgwStatusCodeK)), requestContext);
                putLogElement(LogField.OVERALL_RESPONSE_DESCRIPTION.getParameter(), getTransacationStatus(getCaasStatusCode((String) request.get(KiteKeyBox.pgwStatusCodeK))), requestContext);
                putLogElement(LogField.CHARGE_AMOUNT.getParameter(), request.get(KiteKeyBox.paidAmountK), requestContext);
            }

            if("direct-debit".equals(request.get(requestTypeK))) {
                putLogElement(LogField.CHARGE_AMOUNT.getParameter(), 0, requestContext);
                putLogElement(LogField.BALANCE_DUE.getParameter(), request.get(KiteKeyBox.amountK), requestContext);
                putLogElement(LogField.TOTAL_AMOUNT.getParameter(), 0 , requestContext);
            }

			//putLogElement(recipientsStatusK, recipientsStatus, requestContext);

		}
	}

	private static boolean isCaasRequest(Map<String, Object> ncs) {
		return ncs != null && KiteKeyBox.casK.equals(ncs.get(KiteKeyBox.ncsTypeK));
	}

	private static String getMaskedStatus(boolean isMasked) {
		if (isMasked) {
			return "1";
		} else {
			return "0";
		}
	}

	private static void putChargingDetailsForMo(Map<String, Map<String, Object>> requestContext,
			Map<String, Object> request, Map<String, Object> ncs, Map<String, Object> sp) {
		if (ncs != null && request != null) {
			if (KiteKeyBox.moK.equals(request.get(KiteKeyBox.directionK))) {

				Map<String, Object> flowDetails = (Map<String, Object>) ncs.get(KiteKeyBox.moK);
				if (flowDetails != null && flowDetails.containsKey(KiteKeyBox.chargingK)) {
					Map<String, String> chargingDetails = (Map<String, String>) flowDetails.get(KiteKeyBox.chargingK);
					if (chargingDetails != null) {
						putLogElement(LogField.CHARGING_TYPE.getParameter(), chargingDetails.get(KiteKeyBox.typeK),
								requestContext);
						putLogElement(
								LogField.CHARGE_PARTY_TYPE.getParameter(),
								getChargingPartyType(chargingDetails.get(KiteKeyBox.partyK),
										chargingDetails.get(KiteKeyBox.typeK)), requestContext);

						String[] chargedAddress = getChargedAddress(chargingDetails.get(KiteKeyBox.typeK),
								chargingDetails.get(KiteKeyBox.partyK),
								(String) request.get(KiteKeyBox.senderAddressK),
								(String) request.get(KiteKeyBox.senderAddressPlainTextK), sp);
						putLogElement(LogField.CHARGE_ADDRESS.getParameter(), chargedAddress[0], requestContext);
						putLogElement(LogField.MASKED_CHARGE_ADDRESS.getParameter(), chargedAddress[1], requestContext);

						putLogElement(LogField.CHARGED_CATEGORY.getParameter(),
								request.get(KiteKeyBox.chargedCategoryK), requestContext);
						putLogElement(LogField.CHARGE_AMOUNT.getParameter(), request.get(KiteKeyBox.chargedAmountK),
								requestContext);
						putLogElement(LogField.RATE_CARD.getParameter(),
								refineRateCard((String) request.get(KiteKeyBox.usedExchangeRatesK)), requestContext);
						putLogElement(LogField.CHARGE_AMOUNT_CURRENCY.getParameter(),
								request.get(KiteKeyBox.currencyCodeK), requestContext);
						putLogElement(LogField.PGW_TRANSACTION_ID.getParameter(),
								(String) request.get(KiteKeyBox.externalTransIdK), requestContext);
						if (KiteKeyBox.operatorChargingK.equals(chargingDetails.get(KiteKeyBox.methodK))) {
							putLogElement(LogField.OPERATOR_COST.getParameter(), request.get(KiteKeyBox.operatorCostK),
									requestContext);
							putLogElement(LogField.CHARGE_AMOUNT.getParameter(), request.get(KiteKeyBox.operatorCostK),
									requestContext);
						}
						putLogElement(LogField.EVENT_TYPE.getParameter(),
								getEventType((String) request.get(KiteKeyBox.chargingStatusK)), requestContext);
					}
				}
			}

//            if (subscriptionK.equals(d(ncs, ncsTypeK))) {
//                String serviceKeyword = (String) d(request, serviceKeywordK);
//                if (regK.equalsIgnoreCase(serviceKeyword)) {
//                    putLogElement(LogField.EVENT_TYPE.getParameter(), "registration", requestContext);
//                } else if (unregK.equalsIgnoreCase(serviceKeyword)) {
//                    putLogElement(LogField.EVENT_TYPE.getParameter(), "unregistration", requestContext);
//                }
//            }
		}

	}

	/**
	 * For Masked Application <br/>
	 * 0 - Masked Address 1 - PlainText Address <br/>
	 * For Plain Application <br/>
	 * 0 - PlainText Address
	 *
	 * @param chargingType
	 * @param chargePartyType
	 * @param subscriberMsisdn
	 * @param subscriberMsisdnPlainText
	 * @param sp
	 * @return
	 */
	private static String[] getChargedAddress(String chargingType, String chargePartyType, String subscriberMsisdn,
			String subscriberMsisdnPlainText, Map<String, Object> sp) {
		String spId = "";
		if (sp != null) {
			spId = (String) sp.get(KiteKeyBox.spIdK);
		}
		return getChargedAddress(chargingType, chargePartyType, subscriberMsisdn, subscriberMsisdnPlainText, spId);

	}

	/**
	 * For Masked Application <br/>
	 * 0 - Masked Address 1 - PlainText Address <br/>
	 * For Plain Application <br/>
	 * 0 - PlainText Address
	 *
	 * @param chargingType
	 * @param chargePartyType
	 * @param subscriberMsisdn
	 * @param subscriberMsisdnPlainText
	 * @param spId
	 * @return
	 */
	private static String[] getChargedAddress(String chargingType, String chargePartyType, String subscriberMsisdn,
			String subscriberMsisdnPlainText, String spId) {
		String chargeAddress = "";
		String plainTextChargedAddress = "";
		if (!KiteKeyBox.freeK.equals(chargingType)) {
			if (KiteKeyBox.spK.equals(chargePartyType)) {
				chargeAddress = spId;
			} else if (KiteKeyBox.subscriberK.equals(chargePartyType)) {
				chargeAddress = subscriberMsisdn;
				plainTextChargedAddress = subscriberMsisdnPlainText;
			}
		}

		return new String[] { chargeAddress, plainTextChargedAddress };

	}

	private static void putChannelDetails(Map<String, Map<String, Object>> requestContext, Map<String, Object> request) {
		if (request != null) {

            String ncsType = getNcsType(requestContext, request);
            
			if (KiteKeyBox.mtK.equals(request.get(KiteKeyBox.directionK))) {
				if (KiteKeyBox.smsK.equals(ncsType)) {
					putLogElement(LogField.SOURCE_CHANNEL_TYPE.getParameter(), "sms", requestContext);
					putLogElement(LogField.SOURCE_CHANNEL_PROTOCOL.getParameter(), "http", requestContext);
					putLogElement(LogField.DESTINATION_CHANNEL_TYPE.getParameter(), "sms", requestContext);
					putLogElement(LogField.DESTINATION_CHANNEL_PROTOCOL.getParameter(), "smpp", requestContext);
				} else if(KiteKeyBox.ussdK.equals(ncsType)){
                    putLogElement(LogField.SOURCE_CHANNEL_TYPE.getParameter(), "ussd", requestContext);
                    putLogElement(LogField.SOURCE_CHANNEL_PROTOCOL.getParameter(), "http", requestContext);
                    putLogElement(LogField.DESTINATION_CHANNEL_TYPE.getParameter(), "ussd", requestContext);
                    putLogElement(LogField.DESTINATION_CHANNEL_PROTOCOL.getParameter(), "smpp", requestContext);
                } else if (KiteKeyBox.wapPushK.equals(ncsType)) {
					putLogElement(LogField.SOURCE_CHANNEL_TYPE.getParameter(), "push", requestContext);
					putLogElement(LogField.SOURCE_CHANNEL_PROTOCOL.getParameter(), "http", requestContext);
					putLogElement(LogField.DESTINATION_CHANNEL_TYPE.getParameter(), "push", requestContext);
					putLogElement(LogField.DESTINATION_CHANNEL_PROTOCOL.getParameter(), "smpp", requestContext);
				} else if (KiteKeyBox.casK.equals(ncsType)) {
					putLogElement(LogField.SOURCE_CHANNEL_TYPE.getParameter(), "cas", requestContext);
					putLogElement(LogField.SOURCE_CHANNEL_PROTOCOL.getParameter(), "http", requestContext);
					putLogElement(LogField.DESTINATION_CHANNEL_TYPE.getParameter(), "cas", requestContext);
					putLogElement(LogField.DESTINATION_CHANNEL_PROTOCOL.getParameter(), "", requestContext);
				} else if (KiteKeyBox.lbsK.equals(ncsType)) {
                    putLogElement(LogField.SOURCE_CHANNEL_TYPE.getParameter(), "lbs", requestContext);
                    putLogElement(LogField.SOURCE_CHANNEL_PROTOCOL.getParameter(), "http", requestContext);
                    putLogElement(LogField.DESTINATION_CHANNEL_TYPE.getParameter(), "lbs", requestContext);
                    putLogElement(LogField.DESTINATION_CHANNEL_PROTOCOL.getParameter(), "mlp", requestContext);
                }

			} else if (KiteKeyBox.moK.equals(request.get(KiteKeyBox.directionK))) {
				if (KiteKeyBox.smsK.equals(ncsType)) {
					putLogElement(LogField.SOURCE_CHANNEL_TYPE.getParameter(), "sms", requestContext);
					putLogElement(LogField.SOURCE_CHANNEL_PROTOCOL.getParameter(), "smpp", requestContext);
					putLogElement(LogField.DESTINATION_CHANNEL_TYPE.getParameter(), "sms", requestContext);
					putLogElement(LogField.DESTINATION_CHANNEL_PROTOCOL.getParameter(), "http", requestContext);
				} else if (KiteKeyBox.ussdK.equals(ncsType)) {
					putLogElement(LogField.SOURCE_CHANNEL_TYPE.getParameter(), "ussd", requestContext);
					putLogElement(LogField.SOURCE_CHANNEL_PROTOCOL.getParameter(), "smpp", requestContext);
					putLogElement(LogField.DESTINATION_CHANNEL_TYPE.getParameter(), "ussd", requestContext);
					putLogElement(LogField.DESTINATION_CHANNEL_PROTOCOL.getParameter(), "http", requestContext);
				} else if (KiteKeyBox.subscriptionK.equals(ncsType)) {
                    putLogElement(LogField.SOURCE_CHANNEL_TYPE.getParameter(), "sms", requestContext);
                    putLogElement(LogField.SOURCE_CHANNEL_PROTOCOL.getParameter(), "smpp", requestContext);
                }
			}

            fetchLogParameter(KiteKeyBox.msisdnK, LogField.SOURCE_ADDRESS.getParameter(), request,
                    requestContext);
		}

	}

    private static String getNcsType(Map<String, Map<String, Object>> requestContext, Map<String, Object> request) {
        String ncsType = (String) request.get(ncsTypeK);
        if(ncsType == null) {
            Map<String, Object>  ncs = requestContext.get(ncsK);
            if(ncs != null){
                ncsType = (String) ncs.get(ncsTypeK);
            }
        }
        return ncsType;
    }

    private static void putRecipientsDetails(Map<String, Map<String, Object>> requestContext,
			Map<String, Object> request, Map<String, Object> sp, boolean isMaksedApp) {
		if (request != null) {
			if (KiteKeyBox.mtK.equals(request.get(KiteKeyBox.directionK))) {

				List<Map<String, Object>> recipientsStatus = new ArrayList<Map<String, Object>>();
				List<Map<String, Object>> recipients = (List) request.get(KiteKeyBox.recipientsK);
				for (Map<String, Object> recipient : recipients) {
					Map<String, Object> recipientTransLog = new HashMap<String, Object>();
					if (isMaksedApp) {
						recipientTransLog.put(KiteKeyBox.recipientAddressK,
								recipient.get(KiteKeyBox.recipientAddressPlainTextK));
						recipientTransLog.put(LogField.MASKED_DESTINATION_ADDRESS.getParameter(),
								recipient.get(KiteKeyBox.recipientAddressK));
					} else {
						recipientTransLog
								.put(KiteKeyBox.recipientAddressK, recipient.get(KiteKeyBox.recipientAddressK));
					}
					recipientTransLog.put(KiteKeyBox.chargingTrxIdK, recipient.get(KiteKeyBox.chargingTrxIdK));
					recipientTransLog.put(LogField.RESPONSE_CODE.getParameter(),
							recipient.get(KiteKeyBox.recipientAddressStatusK));

					recipientTransLog.put(
							LogField.RESPONSE_DESCRIPTION.getParameter(),
							buildStatusDescriptionIfNotAvailable(
									(String) recipient.get(KiteKeyBox.recipientAddressStatusK),
									(String) recipient.get(KiteKeyBox.statusDescriptionK), requestContext));

					recipientTransLog.put(LogField.CHARGING_TYPE.getParameter(),
							recipient.get(LogField.CHARGING_TYPE.getParameter()));
					recipientTransLog.put(
							LogField.CHARGE_PARTY_TYPE.getParameter(),
							getChargingPartyType((String) recipient.get(LogField.CHARGE_PARTY_TYPE.getParameter()),
									(String) recipient.get(LogField.CHARGING_TYPE.getParameter())));
					recipientTransLog.put(LogField.OPERATOR.getParameter(),
							recipient.get(KiteKeyBox.recipientAddressOptypeK));
					recipientTransLog.put(LogField.RATE_CARD.getParameter(),
							recipient.get(KiteKeyBox.usedExchangeRatesK));
					recipientTransLog.put(LogField.CHARGE_AMOUNT_CURRENCY.getParameter(),
							recipient.get(KiteKeyBox.currencyCodeK));

					String[] chargedAddress = getChargedAddress(
							(String) recipient.get(LogField.CHARGING_TYPE.getParameter()),
							(String) recipient.get(LogField.CHARGE_PARTY_TYPE.getParameter()),
							(String) recipient.get(KiteKeyBox.recipientAddressK),
							(String) recipient.get(KiteKeyBox.recipientAddressPlainTextK), sp);
					if (isMaksedApp) {
						recipientTransLog.put(LogField.CHARGE_ADDRESS.getParameter(), chargedAddress[1]);
						recipientTransLog.put(LogField.MASKED_CHARGE_ADDRESS.getParameter(), chargedAddress[0]);
					} else {
						recipientTransLog.put(LogField.CHARGE_ADDRESS.getParameter(), chargedAddress[0]);
					}

					recipientTransLog.put(LogField.CHARGED_CATEGORY.getParameter(),
							recipient.get(LogField.CHARGED_CATEGORY.getParameter()));
					recipientTransLog.put(LogField.OPERATOR_COST.getParameter(),
							recipient.get(LogField.OPERATOR_COST.getParameter()));
					recipientTransLog.put(LogField.CHARGE_AMOUNT.getParameter(),
							recipient.get(LogField.CHARGE_AMOUNT.getParameter()));
					recipientTransLog.put(LogField.EVENT_TYPE.getParameter(),
							getEventType((String) recipient.get(LogField.EVENT_TYPE.getParameter())));
					recipientTransLog.put(LogField.PGW_TRANSACTION_ID.getParameter(),
							recipient.get(KiteKeyBox.externalTransIdK));

					recipientsStatus.add(recipientTransLog);
				}

				putLogElement(recipientsStatusK, recipientsStatus, requestContext);
			}
		}

	}

	private static String buildStatusDescriptionIfNotAvailable(String statusCode, String statusDescription,
			Map<String, Map<String, Object>> requestContext) {
		if ((statusDescription == null || statusDescription.trim().isEmpty()) && statusCode != null) {
			return statusDescriptionBuilder.createStatusDescription(statusCode, requestContext);
		} else {
			return statusDescription;
		}
	}

	private static void putRevenueShareDetails(Map<String, Map<String, Object>> requestContext,
			Map<String, Object> request, Map<String, Object> app) {

		boolean isCasRequest = false;
		if (request != null) {
			if (KiteKeyBox.casK.equals(request.get(KiteKeyBox.ncsTypeK))) {
				isCasRequest = true;
			}
		}
		if (isCasRequest) {
			putLogElement(LogField.REVENUE_SHARE_TYPE.getParameter(), PERCENTAGE_FROM_MONTHLY_REVENUE, requestContext);
			putLogElement(LogField.REVENUE_SHARE_PERCENTAGE.getParameter(), "100", requestContext);
		} else {
			putLogElement(LogField.REVENUE_SHARE_TYPE.getParameter(), PERCENTAGE_FROM_MONTHLY_REVENUE, requestContext);
			if (app != null) {
				fetchLogParameter(KiteKeyBox.revenueShareK, LogField.REVENUE_SHARE_PERCENTAGE.getParameter(), app,
						requestContext);
			}
		}

	}

	/**
	 * Convenient method to fetch and add given log parameter form the given
	 * request key.
	 *
	 * @param sourceKeyName
	 *            key of the source Map
	 * @param logKeyName
	 *            key of the logging context
	 * @param source
	 *            the Map from which the values are looked upon
	 * @param requestContext
	 *            the request context to which the logging context information
	 *            be applied
	 */
	private static void fetchLogParameter(String sourceKeyName, String logKeyName, Map<String, Object> source,
			Map<String, Map<String, Object>> requestContext) {
        if (source == null) {
            return;
        }
		Object value = source.get(sourceKeyName);
		if (value != null) {
			putLogElement(logKeyName, value, requestContext);
		}

	}

	private static void fetchDateTimeLogParameter(String sourceKeyName, String logKeyName, Map<String, Object> source,
			Map<String, Map<String, Object>> requestContext) {
		Object value = source.get(sourceKeyName);
		if (value != null) {
			Date d = new Date(Long.parseLong((String) value));
			putLogElement(logKeyName, DATE_FORMAT.format(d), requestContext);
		}

	}

	private static void fetchBroadcastRequestLogElement(LogField logFiled, String valueKey,
			Map<String, Object> broadcastContext) {
		String value = (String) broadcastContext.get(valueKey);
		if (value != null) {
			putBroadcastRequestLogElement(logFiled, value, broadcastContext);
		}
	}

	private static void putBroadcastRequestLogElement(LogField logFiled, String value,
			Map<String, Object> broadcastContext) {
		Map<String, Object> loggingContext = (Map<String, Object>) broadcastContext.get(loggingContextK);
		if (loggingContext == null) {
			loggingContext = new HashMap<String, Object>();
			broadcastContext.put(loggingContextK, loggingContext);
		}
		loggingContext.put(logFiled.getParameter(), value);

	}
}
