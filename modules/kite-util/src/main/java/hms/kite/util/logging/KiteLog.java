/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.util.logging;

import java.util.Map;

/**
 * Convenience class in placing the log fields
 *
 * $LastChangedDate 9/5/11 4:19 PM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
public class KiteLog {

    /**
     * Enum for the Trans-log. The order of the CSV file is determined by the order of the enumeration
     */

    //101206081435340001|2012-06-08 14:35:35 002|SPP_000011|testcorp|APP_000098|casTest|live|||||254711234567||||mo|cas|unknown|||KES||||||S1000|Success|failure|||||||||0|PARTIAL_OR_OVER_PAYMENT_NOT_ALLOWED|failure|||0||||||40.50|10.00

    public enum LogField {
        CORRELATION_ID("correlation_id"),
        TIME_STAMP("time_stamp"),
        SP_ID("sp_id"),
        SP_NAME("sp_name"),
        APP_ID("app_id"),
        APP_NAME("app_name"),
        APP_STATE("app_state"),
        SOURCE_ADDRESS("source_address"),
        MASKED_SOURCE_ADDRESS("masked_source_address "),
        SOURCE_CHANNEL_TYPE("source_channel_type"),
        SOURCE_CHANNEL_PROTOCOL("source_channel_protocol"),
        DESTINATION_ADDRESS("destination_address"),
        MASKED_DESTINATION_ADDRESS("masked_destination_address"),
        DESTINATION_CHANNEL_TYPE("destination_channel_type"),
        DESTINATION_CHANNEL_PROTOCOL("destination_channel_protocol"),
        DIRECTION("direction"),
        NCS("ncs"),
        BILLING_TYPE("billing_type"),
        CHARGE_PARTY_TYPE("charge_party_type"),
        CHARGE_AMOUNT("charge_amount"),
        CHARGE_AMOUNT_CURRENCY("charge_amount_currency"),  //
        RATE_CARD("rate_card"),
        CHARGED_CATEGORY("charged_category"),
        CHARGE_ADDRESS("charge_address"),
        MASKED_CHARGE_ADDRESS("masked_charge_address"),
        EVENT_TYPE("event_type"),
        RESPONSE_CODE("response_code"),
        RESPONSE_DESCRIPTION("response_description"),
        TRANSACTION_STATUS("transaction_status"),
        KEYWORD("keyword"),
        SHORT_CODE("short_code"),
        OPERATOR("operator"),
        OPERATOR_COST("operator_cost"),
        REVENUE_SHARE_TYPE("revenue_share_type"),
        REVENUE_SHARE_PERCENTAGE("revenue_share_percentage"),
        ADVERTISEMENT_ID("advertisement_id"),
        ADVERTISEMENT_NAME("advertisement_name"),
        IS_MULTIPLE_RECEIPIENTS("is_multiple_recipients"),
        OVERALL_RESPONSE_CODE("overall_response_code"),
        OVERALL_RESPONSE_DESCRIPTION("overall_response_description"),
        PGW_TRANSACTION_ID("pgw_transaction_id"),
        CHARGING_TYPE("charging_type"),
        IS_NUMBER_MASKED("is_number_masked"),
        INVOICE_NO("invoice_no"),
        ORDER_NO("order_no"),
        EXTERNAL_TRX_ID("external_trx_id"),
        SESSION_ID("session_id"),
        USSD_OPERATION("ussd_operation"),
        BALANCE_DUE("balance_due"),
        TOTAL_AMOUNT("total_amount"),
        ACCOUNT_TYPE("account_type"),
        ACCOUNT_STATUS("account_status"),
        CHARGEABLE_BALANCE("chargeable_balance"),
        SERVICE_TYPE("service_type"),
        RESPONSE_TIME("response_time"),
        FRESHNESS("freshness"),
        HORIZONTAL_ACCURACY("horizontal_accuracy"),
        FROM_PAYMENT_INSTRUMENT_NAME("from_payment_instrument_name"),
        PAYMENT_INSTRUMENT_BUSINESS_ID("business_id");

        private String parameter;

        LogField(String parameter) {
            this.parameter = parameter;
        }

        public String getParameter() {
            return parameter;
        }
    }

    /**
     * Puts the logging element onto the request context.
     * Creates new logging context in the request context if not exists.
     * Put the logging value in the logging context keyed by the name.
     *
     * @param name
     * @param value
     * @param requestContext
     */
    public static void log(String name, Object value, Map<String, Map<String, Object>> requestContext) {
       TransLog.putLogElement(name, value, requestContext);
    }
}
