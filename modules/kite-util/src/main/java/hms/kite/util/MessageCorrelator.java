/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class MessageCorrelator {
    private Map<String, Session> messageMap = new HashMap<String, Session>();
    private long timeoutPeriod;

    private static final Logger logger = LoggerFactory.getLogger(MessageCorrelator.class);

    public void addMessage(Map<String, Object> message) {
        Session session = new Session(message);
        messageMap.put((String) message.get(correlationIdK), session);
    }

    public Map<String, Object> waitForResponse(Map<String, Object> message) throws InterruptedException {
        Session session = messageMap.get(message.get(correlationIdK));
        synchronized (session) {
            if (null == session.getResponse()) {
                session.wait(timeoutPeriod);
            }
            messageMap.remove(message.get(correlationIdK));
        }
        return session.getResponse();
    }

    public void notify(Map<String, Object> resp) {
        final Session session = messageMap.get(resp.get(correlationIdK));

        if (null != session) {
            session.setResponse(resp);

            synchronized (session) {
                session.notify();
            }
        } else {
            logger.warn("Message is already timeout for sbl response {}", resp);
        }
    }

    public void setTimeoutPeriod(long timeoutPeriod) {
        this.timeoutPeriod = timeoutPeriod;
    }
}
