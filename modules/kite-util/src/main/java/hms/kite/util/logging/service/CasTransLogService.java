package hms.kite.util.logging.service;

import hms.kite.util.KeyNameSpaceResolver;
import hms.kite.util.KiteKeyBox;
import hms.kite.util.PgwParam;
import hms.kite.util.logging.KiteLog;
import hms.kite.util.logging.TransLogUtil;

import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.logging.TransLogUtil.*;

/**
 * (C) Copyright 2011-2012 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 * <p/>
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 * <p/>
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
public class CasTransLogService extends AbstractTransLogService{

    @Override
    public void log(Map<String, Map<String, Object>> requestContext) {
        super.log(requestContext);
        addAdditionalParameters(requestContext);
        TransLogUtil.log(requestContext);
    }

    public void addAdditionalParameters(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> request = requestContext.get(KiteKeyBox.requestK);
        Map<String, Object> app = requestContext.get(KiteKeyBox.appK);

        putLogElement(KiteLog.LogField.OVERALL_RESPONSE_CODE.getParameter(), getCaasStatusCode((String) request.get(KiteKeyBox.pgwStatusCodeK)), requestContext);
        putLogElement(KiteLog.LogField.OVERALL_RESPONSE_DESCRIPTION.getParameter(), getTransactionStatus(getCaasStatusCode((String) request.get(KiteKeyBox.pgwStatusCodeK))), requestContext);
        putLogElement(KiteLog.LogField.REVENUE_SHARE_TYPE.getParameter(), PERCENTAGE_FROM_MONTHLY_REVENUE, requestContext);
        putLogElement(KiteLog.LogField.REVENUE_SHARE_PERCENTAGE.getParameter(), findRevenueSharePercentage(requestContext), requestContext);

        if (KiteKeyBox.mtK.equals(request.get(KiteKeyBox.directionK))) {
            putLogElement(KiteLog.LogField.OVERALL_RESPONSE_CODE.getParameter(), request.get(KiteKeyBox.statusCodeK), requestContext);
            putLogElement(KiteLog.LogField.OVERALL_RESPONSE_DESCRIPTION.getParameter(), getTransactionStatus((String) request.get(KiteKeyBox.statusCodeK)), requestContext);

            putLogElement(KiteLog.LogField.SOURCE_CHANNEL_TYPE.getParameter(), "cas", requestContext);
            putLogElement(KiteLog.LogField.SOURCE_CHANNEL_PROTOCOL.getParameter(), "http", requestContext);
            putLogElement(KiteLog.LogField.DESTINATION_CHANNEL_TYPE.getParameter(), "cas", requestContext);
            putLogElement(KiteLog.LogField.DESTINATION_CHANNEL_PROTOCOL.getParameter(), "", requestContext);

        } else if(KiteKeyBox.moK.equals(request.get(KiteKeyBox.directionK))) {
            putLogElement(KiteLog.LogField.RESPONSE_CODE.getParameter(), getCaasStatusCode((String) request.get(KiteKeyBox.pgwStatusCodeK)), requestContext);
            putLogElement(KiteLog.LogField.RESPONSE_DESCRIPTION.getParameter(), getTransactionStatus(getCaasStatusCode((String) request.get(KiteKeyBox.pgwStatusCodeK))), requestContext);
            putLogElement(KiteLog.LogField.TRANSACTION_STATUS.getParameter(), getTransactionStatus(getCaasStatusCode((String) request.get(KiteKeyBox.pgwStatusCodeK))), requestContext);
        }

        putCaasSpecificDetails(requestContext, request, getIsMasked(app));
    }

    private static void putCaasSpecificDetails(Map<String, Map<String, Object>> requestContext,
                                               Map<String, Object> request, boolean isMaskedApp) {
        if (request != null) {
            List<Map<String, Object>> recipients = (List) request.get(KiteKeyBox.recipientsK);
            if (recipients != null && recipients.size() > 0) {
                Map<String, Object> recipient = recipients.get(0);
                if (isMaskedApp) {
                    putLogElement(KiteLog.LogField.DESTINATION_ADDRESS.getParameter(),
                            recipient.get(KiteKeyBox.recipientAddressK), requestContext);
                    putLogElement(KiteKeyBox.recipientAddressK,
                            recipient.get(KiteKeyBox.recipientAddressPlainTextK), requestContext);
                    putLogElement(KiteLog.LogField.MASKED_DESTINATION_ADDRESS.getParameter(),
                            recipient.get(KiteKeyBox.recipientAddressMaskedK), requestContext);
                } else {
                    putLogElement(KiteKeyBox.recipientAddressK, recipient.get(KiteKeyBox.recipientAddressK), requestContext);
                    putLogElement(KiteLog.LogField.DESTINATION_ADDRESS.getParameter(), recipient.get(KiteKeyBox.recipientAddressK), requestContext);
                }
                putLogElement(KiteLog.LogField.SOURCE_ADDRESS.getParameter(), "", requestContext);
                putLogElement(KiteLog.LogField.MASKED_SOURCE_ADDRESS.getParameter(), "", requestContext);
                putLogElement(KiteLog.LogField.EVENT_TYPE.getParameter(), request.get(KiteKeyBox.requestTypeK), requestContext);
                putLogElement(KiteLog.LogField.CHARGE_AMOUNT_CURRENCY.getParameter(), request.get(KiteKeyBox.currencyK), requestContext);
                putLogElement(KiteLog.LogField.RATE_CARD.getParameter(), request.get(KiteKeyBox.usedExchangeRatesK), requestContext);
            }

            if(KiteKeyBox.mtK.equals((String)request.get(KiteKeyBox.directionK)))   {
                putLogElement(KiteLog.LogField.PGW_TRANSACTION_ID.getParameter(), request.get(KiteKeyBox.internalTrxIdCaasReq), requestContext);
                putLogElement(KiteLog.LogField.RESPONSE_CODE.getParameter(), request.get(KiteKeyBox.statusCodeK), requestContext);
                putLogElement(KiteLog.LogField.RESPONSE_DESCRIPTION.getParameter(), getTransactionStatus((String) request.get(KiteKeyBox.statusCodeK)), requestContext);
                putLogElement(KiteLog.LogField.OVERALL_RESPONSE_CODE.getParameter(), request.get(KiteKeyBox.statusCodeK), requestContext);
                putLogElement(KiteLog.LogField.OVERALL_RESPONSE_DESCRIPTION.getParameter(), getTransactionStatus((String) request.get(KiteKeyBox.statusCodeK)), requestContext);
                putLogElement(KiteLog.LogField.CHARGE_AMOUNT.getParameter(), request.get(KiteKeyBox.paidAmountForSpK), requestContext);
            } else if(KiteKeyBox.moK.equals((String)request.get(KiteKeyBox.directionK)))    {
                putLogElement(KiteLog.LogField.PGW_TRANSACTION_ID.getParameter(), request.get(KiteKeyBox.internalTransIdK), requestContext);
                putLogElement(KiteLog.LogField.RESPONSE_CODE.getParameter(), getCaasStatusCode((String) request.get(KiteKeyBox.pgwStatusCodeK)), requestContext);
                putLogElement(KiteLog.LogField.RESPONSE_DESCRIPTION.getParameter(), getTransactionStatus(getCaasStatusCode((String) request.get(KiteKeyBox.pgwStatusCodeK))), requestContext);
                putLogElement(KiteLog.LogField.OVERALL_RESPONSE_CODE.getParameter(), getCaasStatusCode((String) request.get(KiteKeyBox.pgwStatusCodeK)), requestContext);
                putLogElement(KiteLog.LogField.OVERALL_RESPONSE_DESCRIPTION.getParameter(), getTransactionStatus(getCaasStatusCode((String) request.get(KiteKeyBox.pgwStatusCodeK))), requestContext);
                putLogElement(KiteLog.LogField.CHARGE_AMOUNT.getParameter(), request.get(KiteKeyBox.paidAmountK), requestContext);
            }
            if("direct-debit".equals(request.get(requestTypeK))) {
                putLogElement(KiteLog.LogField.CHARGE_AMOUNT.getParameter(), request.get(KiteKeyBox.amountK), requestContext);
                putLogElement(KiteLog.LogField.BALANCE_DUE.getParameter(), request.get(KiteKeyBox.amountK), requestContext);
                putLogElement(KiteLog.LogField.TOTAL_AMOUNT.getParameter(), 0 , requestContext);
                putLogElement(KiteLog.LogField.CHARGING_TYPE.getParameter(), KiteKeyBox.variableK, requestContext);
            } else if(queryBalanceK.equals(request.get(requestTypeK))) {
                putLogElement(KiteLog.LogField.ACCOUNT_TYPE.getParameter(), request.get(KiteKeyBox.accountTypeNblK) , requestContext);
                putLogElement(KiteLog.LogField.ACCOUNT_STATUS.getParameter(), request.get(KiteKeyBox.accountStatusNblK) , requestContext);
                putLogElement(KiteLog.LogField.CHARGEABLE_BALANCE.getParameter(), request.get(KiteKeyBox.chargeableBalanceNblK) , requestContext);
                putLogElement(KiteLog.LogField.CHARGING_TYPE.getParameter(), KiteKeyBox.freeK, requestContext);
            }
        }
    }

    private String findRevenueSharePercentage(Map<String, Map<String, Object>> requestContext) {
        double serviceChargePercentage = 0;
        if (KeyNameSpaceResolver.contains(requestContext, ncsK, serviceChargePercentageK)) {
            serviceChargePercentage =  Double.parseDouble(KeyNameSpaceResolver.data(requestContext, ncsK, serviceChargePercentageK).toString());
        }

        return String.valueOf((100 - serviceChargePercentage));

    }
}
