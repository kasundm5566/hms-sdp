/*
 * (C) Copyright 2010- 2017 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.kite.util.logging.service;

import hms.kite.util.KiteKeyBox;
import hms.kite.util.logging.KiteLog;
import hms.kite.util.logging.TransLogUtil;

import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.logging.TransLogUtil.*;

public class VodafoneApisTranslogService extends AbstractTransLogService {

    @Override
    public void log(Map<String, Map<String, Object>> requestContext) {
        super.log(requestContext);
        addAdditionalParameters(requestContext);
        TransLogUtil.log(requestContext);
    }

    public void addAdditionalParameters(Map<String, Map<String, Object>> requestContext) {

        Map<String, Object> request = requestContext.get(KiteKeyBox.requestK);
        Map<String, Object> sp = requestContext.get(KiteKeyBox.spK);
        Map<String, Object> app = requestContext.get(KiteKeyBox.appK);
        Map<String, Object> ncs = requestContext.get(KiteKeyBox.ncsK);


        if (KiteKeyBox.mtK.equals(request.get(KiteKeyBox.directionK))) {
            putLogElement(KiteLog.LogField.SOURCE_CHANNEL_TYPE.getParameter(), "vdf-apis", requestContext);
            putLogElement(KiteLog.LogField.SOURCE_CHANNEL_PROTOCOL.getParameter(), "http", requestContext);
            putLogElement(KiteLog.LogField.DESTINATION_CHANNEL_TYPE.getParameter(), "vdf-apis", requestContext);
            putLogElement(KiteLog.LogField.DESTINATION_CHANNEL_PROTOCOL.getParameter(), "http", requestContext);
            //This is hard coded for VDF API apps
            putLogElement(KiteLog.LogField.REVENUE_SHARE_PERCENTAGE.getParameter(), "100", requestContext);

            List<Map<String, Object>> recipients = (List) request.get(KiteKeyBox.recipientsK);
            if (recipients != null && recipients.size() > 0) {
                Map<String, Object> recipient = recipients.get(0);
                putLogElement(KiteLog.LogField.DESTINATION_ADDRESS.getParameter(), recipient.get(KiteKeyBox.recipientAddressK), requestContext);
                putLogElement(KiteLog.LogField.OPERATOR.getParameter(), recipient.get(KiteKeyBox.recipientAddressOptypeK), requestContext);

                if (getIsMasked(app)) {
                    putLogElement(KiteLog.LogField.MASKED_DESTINATION_ADDRESS.getParameter(), recipient.get(KiteKeyBox.recipientAddressMaskedK), requestContext);
                }

                putLogElement(KiteLog.LogField.CHARGE_PARTY_TYPE.getParameter(),
                        getChargingPartyType((String) recipient.get(KiteLog.LogField.CHARGE_PARTY_TYPE.getParameter()),
                                (String) recipient.get(KiteLog.LogField.CHARGING_TYPE.getParameter())), requestContext);
            }

            putLogElement(KiteLog.LogField.EVENT_TYPE.getParameter(), getLogElement(request, requestTypeK), requestContext);
            putLogElement(KiteLog.LogField.CHARGE_AMOUNT.getParameter(), getLogElement(request, amountK), requestContext);
            putLogElement(KiteLog.LogField.CHARGE_AMOUNT_CURRENCY.getParameter(), "FJD", requestContext);
            putLogElement(KiteLog.LogField.RATE_CARD.getParameter(), "[{\"currencyCode\":\"FJD\", \"buyingRate\": 1, \"sellingRate\": 1}]", requestContext);
//            putLogElement(KiteLog.LogField.CHARGING_TYPE.getParameter(), getLogElement(ncs, mtK, chargingK, typeK), requestContext);


            String requestStatus = (String) request.get(statusCodeK);
            putLogElement(KiteLog.LogField.RESPONSE_CODE.getParameter(), requestStatus, requestContext);
            putLogElement(KiteLog.LogField.RESPONSE_DESCRIPTION.getParameter(), buildStatusDescriptionIfNotAvailable(requestStatus, null, requestContext), requestContext);


            fetchLogParameter(KiteKeyBox.statusCodeK, KiteLog.LogField.OVERALL_RESPONSE_CODE.getParameter(), request,
                    requestContext);
            fetchLogParameter(KiteKeyBox.statusDescriptionK, KiteLog.LogField.OVERALL_RESPONSE_DESCRIPTION.getParameter(),
                    request, requestContext);
            putLogElement(KiteLog.LogField.TRANSACTION_STATUS.getParameter(), getTransactionStatus((String) request.get(KiteKeyBox.statusCodeK)), requestContext);



        }
    }

}