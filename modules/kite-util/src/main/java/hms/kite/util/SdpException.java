/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.util;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SdpException extends RuntimeException {
    private String errorCode;
    private String errorDescription;

    public SdpException(String errorCode) {
        this.errorCode = errorCode;
    }

    public SdpException(String errorCode, String errorDescription) {
        super(errorDescription);
        this.errorCode = errorCode;
        this.errorDescription = errorDescription;
    }

    public SdpException(String errorCode, Throwable throwable) {
        super(throwable);
        this.errorCode = errorCode;
    }

    public SdpException(String errorCode, String errorDescription, Throwable throwable) {
        super(errorDescription, throwable);
        this.errorCode = errorCode;
        this.errorDescription = errorDescription;
    }



    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("SdpException{")
                .append("errorCode='").append(errorCode).append('\'')
                .append(", errorDescription='").append(errorDescription)
                .append('\'').append('}').toString();
    }
}
