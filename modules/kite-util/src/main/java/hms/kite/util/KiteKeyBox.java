/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.util;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class KiteKeyBox {



    protected KiteKeyBox() {
    }

    public static final String abuseWordsK			 = "abusive-words";
    public static final String accountIdK			 = "account-id";
    public static final String accountIdNblK		 = "accountId";
    public static final String accountStatusNblK	 = "accountStatus";
    public static final String accountTypeNblK		 = "accountType";
    public static final String actionK				 = "action";
    public static final String activeK               = "active";
    public static final String activeProductionK     = "active-production";
    public static final String activeProductionStartDateK     = "active-production-start-date";
    public static final String addressK              = "address";
    public static final String responseAddressK      = "response-address";
    public static final String adminK                = "admin";
    public static final String advertiseK            = "advertise";
    public static final String allK                  = "all";
    public static final String allowedHostsK         = "allowed-hosts";
    public static final String allowedK              = "allowed";
    public static final String allowedPaymentInstrumentsK = "allowed-payment-instruments";
    public static final String aliasingK             = "aliasing";
    public static final String aoK					 = "ao";
    public static final String appK                  = "app";
    public static final String appsK                 = "apps";
    public static final String appIdK                = "app-id";
    public static final String appIdsK               = "app-ids";
    public static final String applicationIdK        = "applicationID";
    public static final String applicationSubIdK     = "applicationId";
    public static final String appStatusK            = "app-status";
    public static final String applyTaxK             = "tax";

    public static final String adNameK               = "advertisement-name";
    public static final String adTextK				 = "advertisement-text";
    public static final String createdTimeK          = "created-time";
    public static final String lastModifiedTimeK     = "last-modified-time";

    @Deprecated
    public static final String appNameK				 = "app-name";

    public static final String approveK              = "approve";
    public static final String approvedK             = "approved";
    public static final String approvedActiveProductionK = "approved-active-production";
    public static final String appTypeK              = "app-type";
    public static final String appTypeCurrentK       = "current";
    public static final String appTypeCrK            = "cr";
    public static final String anyK                  = "any";
    public static final String availableK            = "available";
    public static final String auditK                = "audit";
    public static final String operationK = "audit-operation";
    public static final String baseSizeK             = "base-size";
    public static final String baseSizeNblK          = "baseSize";

    public static final String blackListK            = "black-list";
    public static final String blackListIdK            = "black-list-id";
    public static final String broadcastAddressK     = "broadcast-address";
    public static final String broadcastStatusK		 = "broadcast-status";
    public static final String broadcastStatusNblK	 = "broadcastStatus";
    public static final String broadcastTotalSentK   = "total-sent";
    public static final String broadcastTotalErrorsK = "total-errors";
    public static final String bypassUrlK		     = "bypass-url";
    public static final String blockK		         = "block";
    public static final String blockedK		         = "blocked";
    public static final String binaryHeaderNblK      = "binaryHeader";
    public static final String binaryHeaderK         = "binary-header";
    public static final String categoryK             = "category";
    public static final String cancelCreditK         = "cancel-credit";
    public static final String chargingK             = "charging";
    public static final String chargingAmountK       = "charging-amount";
    public static final String chargedAmountK        = "charged-amount";
    public static final String chargingAmountNblK    = "chargingAmount"; //todo use one
    public static final String chargingMethodK       = "charging-method";
    public static final String chargedPartyK         = "charged-party";
    public static final String chargedCategoryK      = "charged-category";
    public static final String chargeableBalanceNblK  = "chargeableBalance";
    public static final String chargedPartyTypeK     = "charge_party_type";
    public static final String chargingTrxIdK        = "charging-trx-id";
    public static final String chargingStatusK       = "charging-status";
    public static final String configuredK			 = "configured";
    public static final String chargingTypeK         = "charging-type";
    public static final String connectionUrlK        = "connection-url";
    public static final String coopUserIdK           = "coop-user-id";
    public static final String coopUserNameK         = "coop-user-name";
    public static final String commitK                = "commit-credit";
    public static final String correlationIdK        = "correlation-id";
    public static final String createdByK            = "created-by";
    public static final String createdDateK          = "created-date";
    public static final String createdUserTypeK      = "created-user-type";
    public static final String createK               = "create";
    public static final String crK                   = "cr";
    public static final String crIdK                 = "cr-id";
    public static final String crTypeK               = "cr-type";
    public static final String crSpTypeK             = "cr-sp";
    public static final String crAppTypeK            = "cr-app";
    public static final String crNcsTypeK            = "cr-ncs";
    public static final String currencyK		     = "currency";
    public static final String currencyCodeK		 = "currency-code";
    public static final String currentDataK          = "current-data";
    public static final String currentUsernameK      = "current-username";
    public static final String currentUserTypeK      = "current-user-type";
    public static final String dailyK			     = "daily";
    public static final String dataCodingK			 = "data-coding";
    public static final String defaultPaymentInstrumentK = "default-payment-instrument";
    public static final String defaultSenderAddressK = "default-sender-address";
    public static final String defaultVersionK       = "1.0";
    public static final String deleteK               = "delete";
    public static final String deliveryReportUrlK    = "delivery-report-url";
    public static final String deliveryReportRequiredK    = "1";
    public static final String deliveryReportNotRequiredK = "0";
    public static final String deliveredK = "delivered";
    public static final String destinationResponsesNblK    = "destinationResponses";
    public static final String descriptionK          = "description";
    public static final String directionK            = "direction";
    public static final String doneDateK            = "done-date";
    public static final String draftK                = "draft";
    public static final String duplicateKeywordK     = "duplicate-keyword";
    public static final String exclusiveK            = "exclusive";
    public static final String enableK               = "enable";
    public static final String encodingK             = "encoding";
    public static final String encodingTextK         = "0";
    public static final String encodingFlashK        = "240";
    public static final String encodingBinaryK       = "245";
    public static final String endDateK				 = "end-date";
    public static final String eventK                = "event";
    public static final String externalTransIdK      = "ext-transid";
    public static final String internalTransIdK      = "int-transid";
    public static final String externalTransIdNblK	 = "externalTrxId";
    public static final String expireK               = "expire";
    public static final String disableK              = "disable";
    public static final String falseK                = "false";
    public static final String chargingPartyK        = "charging-party";
    public static final String financialInstrumentK  = "financialInstrument";
    public static final String freeK                 = "free";
    public static final String frequencyK 			 = "frequency";
    public static final String frequencyIdK 		 = "frequency";
    public static final String flatK                 = "flat";
    public static final String governK               = "govern";
    public static final String governDaemonDelayK	 = "governDaemonDelay";
    public static final String governDaemonIntervalK = "governDaemonInterval";
    public static final String halfMonthlyK			 = "fifteen-days";
    public static final String idK                   = "_id";
    public static final String initialK              = "initial";
    public static final String internalHostsK		 = "internal-hosts";
    public static final String internalHostK		 = "internal-host";
    public static final String lastUpdatedTimeK      = "last-updated-time";
    public static final String latestVersionK		 = "1.0";
    public static final String legacyNumberMaskAllowedK		 = "legacy-number-mask-allowed";
    public static final String legacyNumberMaskingUsedK		 = "legacy-masking-used";
    public static final String lbsK		             = "lbs";
    public static final String limitedProductionK    = "limited-production";
    public static final String keywordK              = "keyword";
    public static final String keywordChargingAmountK   = "keyword-charging-amounts";
    public static final String maskNumberK           = "mask-number";
    public static final String maskK                 = "mask";
    public static final String maxChargingAmountK    = "max-charging-amount";
    public static final String minChargingAmountK    = "min-charging-amount";
    public static final String maxTpsK               = "maxTps";
    public static final String maxTpdK               = "maxTpd";
    public static final String mctK				     = "mct";
    public static final String mediumK				 = "medium";
    public static final String messageK              = "message";
    public static final String messageIdK			 = "message-id";
    public static final String messageIdNblK		 = "messageId";
    public static final String messageReceivingUrlK  = "message-receiving-url";
    public static final String metaDataK             = "meta-data";
    public static final String methodK               = "method";
    public static final String minAmountK			 = "min-amount";
    public static final String maxAmountK			 = "max-amount";
    public static final String moK			         = "mo";
    public static final String moAllowedK            = "mo-allowed";
    public static final String moChargingAllowedK    = "mo-charging-allowed";
    public static final String moChargedPartyK       = "mo-charged-party";
    public static final String moInitK				 = "mo-init";
    public static final String moContK				 = "mo-cont";
    public static final String monthlyK				 = "monthly";
    public static final String msisdnK               = "msisdn";
    public static final String msisdnOptypeK         = "msisdn-optype";
    public static final String msisdnSubypeK         = "msisdn-subtype";
    public static final String msisdnStatusK         = "msisdn-status";
    public static final String drK                   = "dr";
    public static final String mtK                   = "mt";
    public static final String mtInitK               = "mt-init";
    public static final String mtFinK                = "mt-fin";
    public static final String mtContK               = "mt-cont";
    public static final String mtAllowedK            = "mt-allowed";
    public static final String mtChargingAllowedK    = "mt-charging-allowed";
    public static final String mtAliasingK           = mtK + "-" + aliasingK;
    public static final String mtChargingPartyK      = mtK + "-" + chargedPartyK;
    public static final String nameK                 = "name";
    public static final String ncsK                  = "ncs";
    public static final String ncsesK                = "ncses";
    public static final String ncsTypeK              = "ncs-type";
    public static final String ncsSlasK              = "ncs-slas";
    public static final String noteK                 = "note";
    public static final String notificationUrlK      = "notification-url";
    public static final String orderByK              = "order-by";
    public static final String operatorK             = "operator";
    public static final String operatorChargingK     = "operator-charging";
    public static final String operatorCostK	     = "operator-cost";
    public static final String optInK				 = "1";				// TODO: change value to opt-out. change references too
    public static final String optOutK				 = "0";				// TODO: change value to opt-in. change references too
    public static final String partyK                = "party";
    public static final String passwordK             = "password";
    public static final String paymentInstrumentIdK   = "payment-instrument-id";
    public static final String paymentInstrumentNameK = "payment-instrument-name";
    public static final String paymentAccountK       = "payment-account";
    public static final String pendingApproveK       = "pending-approve";
    public static final String scheduledActiveProductionK = "scheduled-active-production";
    public static final String pendingApproveForActiveProductionK = "pending-approve-for-active-production";
    public static final String portK                 = "port";
    public static final String productionK           = "production";
    public static final String previousStateK        = "previous-state";
    public static final String queryBalanceK  = "query-balance";
    public static final String queryBalanceAllowedK  = "query-balance-allowed";
    public static final String reasonCodeK           = "reason-code";
    public static final String receiveTimeK			 = "receiveTime";
    public static final String recipientsK           = "recipients";
    public static final String recipientAddressK     = "recipient-address";
    public static final String recipientAddressNblK     = "recipientAddress";
    public static final String recipientAddressMaskedK     = "recipient-address-masked";
    public static final String recipientAddressPlainTextK     = "recipient-address-plaintext";
    public static final String recipientAddressOptypeK= "recipient-address-optype";
    public static final String recipientAddressSubtypeK= "recipient-address-subtype";
    public static final String recipientAddressStatusK= "recipient-address-status";
    public static final String recipientAddressStatusDescriptionK= "recipient-address-description";
    public static final String recipientAddressRegExK= "recipient-address[0-9]*";
    public static final String recipientCountK		 = "recipient-count";
    public static final String regK                  = "reg";
    public static final String rejectK               = "reject";
    public static final  String retryContK           = "retry-count";

    public static final String remarksK           = "remarks";
    public static final String remotehostK           = "remote-host";
    public static final String replyK			     = "reply";
    public static final String reserveK			     = "reserve-credit";
    public static final String responseK			 = "response";
    public static final String terminateK            = "terminate";
    public static final String restoreK              = "restore";
    public static final String requestK              = "request";
    public static final String requestIdK              = "requestId";
    public static final String requestTypeK          = "request-type";
    public static final String revenueShareK         = "revenue-share";
    public static final String rollbackK           = "rollback-credit";
    public static final String routingKeyK           = "routing-key";
    public static final String routingKeysK          = "routing-keys";
    public static final String requestForActiveProductionk = "request-for-active-production";
    public static final String sblSmsReceiverAddressesK = "sbl-sms-receiver-addresses";
    public static final String sblLbsReceiverAddressesK = "sbl-lbs-receiver-addresses";
    public static final String sblWappushReceiverAddressesK = "sbl-wappush-receiver-addresses";
    public static final String sblUssdReceiverAddressesK = "sbl-ussd-receiver-addresses";
    public static final String sblVodafoneApiReceiverAddressesK = "sbl-vodafone-api-receiver-addresses";
    public static final String scheduledTimeK        = "scheduled-time";
    public static final String senderAddressK        = "sender-address";
    public static final String senderAddressMaskedK = "sender-address-masked";
    public static final String senderAddressPlainTextK = "sender-address-plaintext";
    public static final String senderAddressNblK     = "senderAddress";
    public static final String senderAddressOptypeK  = "sender-address-optype";
    public static final String senderAddressSubtypeK = "sender-address-subtype";
    public static final String senderAddressStatusK  = "sender-address-status";
    public static final String sendSubsNotifyK       = "send-subs-notify";
    public static final String sendChargeMinAmountK       = "min-amount";
    public static final String sendChargeMaxAmountK       = "max-amount";
    public static final String serviceCodeK          = "service-code";
    public static final String serviceChargePercentageK = "service-charge-percentage";
    public static final String serviceIndicatorK	 = "si";
    public static final String serviceKeywordK       = "service-keyword";
    public static final String serviceLocatorK		 = "sl";
    public static final String serviceParameterKeysK = "service-parameter-keys";
    public static final String serviceTypeK          = "service-type";
    public static final String sessionIdK			 = "session-id";
    public static final String sessionIdNblK		 = "sessionID";
    public static final String sessionBasedK		 = "session-based";
    public static final String sessionK		         = "session";
    public static final String sessionAllowedK		 = "session-allowed";
    public static final String sessionChargingAllowedK = "session-charging-allowed";
    public static final String shortcodeK            = "shortcode";
    public static final String shareShortCodeK       = "share-short-code";
    public static final String shareServiceCodeK     = "share-service-code";
    public static final String smsK                  = "sms";
    public static final String sourceAddressNblK     = "sourceAddress";
    public static final String solturaK              = "soltura";
    public static final String solturaUserK          = "soltura-user";
    public static final String sdpUserK              = "sdp-user";
    public static final String dlAppUserK            = "dl-app-user";
    public static final String spK                   = "sp";
    public static final String spIdK                 = "sp-id";
    public static final String spNameK               = "sp-name";
    public static final String spSelectedServicesK   = "sp-selected-services";
    public static final String srUrlK                = "sr-url";
    public static final String srrK                  = "srr";
    public static final String startDateK			 = "start-date";
    public static final String statusK               = "status";
    public static final String statusCodeK           = "status-code";
    public static final String pgwStatusCodeK        = "pgw-status-code";
    public static final String statusCodeNblK           = "statusCode";
    public static final String statusRequestK        = "statusRequest";
    public static final String statusDescriptionK    = "status-description";
    public static final String statusDescriptionNblK    = "statusDescription";
    public static final String submitMultiK			 = "submit-multi";
    public static final String subscribedAppsK		 = "subscribed-apps";
    public static final String subscriberK           = "subscriber";
    public static final String subscriberIdNblK         = "subscriberID";
    public static final String subscriberSubIdNblK   = "subscriberId";
    public static final String subscriberIdK         = "subscriberId";
    public static final String subscriptionStatusK	 = "subscription-status";
    public static final String subscriptionStatusNblK = "subscriptionStatus";
    public static final String subscriberStateK     = "subscriberState";
    public static final String subscriptionRequestTypeK = "subscription-request-type";
    public static final String subscriptionRequiredK = "subscription-required";
    public static final String subscriptionRecursiveChargingK		 = "subs-rec-charge";
    public static final String subscriptionSelectedK = "subscription-selected";
    public static final String subscriptionRecursiveChargingNotifyK = "subs-rec-charg-notify";
    public static final String subscriptionNotificationK = "subscription-notification";
    public static final String suspendK              = "suspend";
    public static final String suspendedK            = "suspended";
    public static final String supportNcsAndOperatorsK = "supported-ncses-and-operators";
    public static final String trueK                 = "true";
    public static final String trustedK              = "trusted";
    public static final String tpsK                  = "tps";
    public static final String tpdK                  = "tpd";
    public static final String telK                  = "tel";
    public static final String typeK                 = "type";
    public static final String timeStampK            = "timeStamp";
    public static final String ussdK                 = "ussd";
    public static final String unregK                = "unreg";
    public static final String unknownK              = "unknown";
    public static final String updateK               = "update";
    public static final String updatedDataK          = "updated-data";
    public static final String loggingDataK          = "logging-data";
    public static final String updatedByK            = "updated-by";
    public static final String updatedDateK          = "updated-date";
    public static final String usedExchangeRatesK    = "used-exchange-rates";
    public static final String ussdOpK				 = "ussd-op";
    public static final String ussdOpNblK			 = "ussdOperation";
    public static final String variableK             = "variable";
    public static final String valueK                = "value";
    public static final String vdfApiK               = "vdf-apis";
    public static final String vdfApiCheckMsisdnK    = "check-msisdn";
    public static final String vdfApiSendChargeK     = "send-charge";
    public static final String vdfApiSendSubscriptionK  = "send-subscription";
    public static final String wapPushK              = "wap-push";
    public static final String wapPushTypeK			 = "wap-push-type";
    public static final String wapPushTypeNblK		 = "wapPushType";
    public static final String wapUrlK				 = "wap-url";
    public static final String wapUrlNblK			 = "wapUrl";
    public static final String webK					 = "web";
    public static final String aasK                  = "aas";
    public static final String casK                  = "cas";
    public static final String downloadableK         = "downloadable";
    public static final String subscriptionK         = "subscription";
    public static final String unmaskK               = "unmask";
    public static final String versionK              = "version";
    public static final String whiteListK            = "white-list";
    public static final String weeklyK               = "weekly";
    public static final String vlrAddressNblK        = "vlrAddress";
    
    public static final String deliveryStatusK        = "deliveryStatus";
    public static final String deliveryStatusDescriptionK        = "deliveryStatusDescription";
    public static final String deliveryStatusNblK    = "deliveryStatus";

    public static final String qosK                  = "qos";
    public static final String responseTimeK         = "response-time";
    public static final String horizontalAccuracyK   = "horizontal-accuracy";
    public static final String freshnessOfLocationK  = "freshness-of-location";

    /* v For Downloadable Applications v */
    public static final String expireDateK           = "expire-date";
    public static final String dlAppMcdK             = "downloadable-mcd";
    public static final String dlAppMdpdK            = "downloadable-mdpd";
    public static final String buildFileK            = "build-file";
    public static final String buildFileIdK          = buildFileK+"-id";
    public static final String buildNameK            = "name";
    public static final String buildVersionK         = "version";
    public static final String buildDescK            = "description";
    public static final String buildAppFileK         = "app-file";
    public static final String buildJadFileK         = "jad-file";
    public static final String buildAppFileIdK       = buildAppFileK + "-id";
    public static final String buildJadFileIdK       = buildJadFileK + "-id";
    public static final String buildAppFileNameK     = buildAppFileK + "-" + nameK;
    public static final String buildJadFileNameK     = buildJadFileK + "-" + nameK;
    public static final String dlAtmptK              = "dl-atmpt";//Download Attempts
    public static final String dlChargingTypeK       = "charging-type";
    public static final String dlChargingMethodK     = "charging-method";
    public static final String dlOperatorChargingK   = "operator-charging";
    public static final String dlFinInsChargingK     = "financial-instrument-charging";
    public static final String dlChargingAmountK     = "charging-amount";
    public static final String dlRepoK               = "dl-repo";//Downloadable repository
    public static final String dlSlaStatusK          = "dl-sla-status";
    public static final String filenameK             = "filename";
    public static final String linkExpTimeK          = "link-exp-time";//Link Expiration Time
    public static final String linkExpTimeTypeK      = "link-exp-time-type";//Link Expiration Time Type
    public static final String minutesK              = "minutes";
    public static final String hoursK                = "hours";
    public static final String daysK                 = "days";
    public static final String maxConDlK             = "max-con-dl";//Maximum Concurrent Downloads
    public static final String maxDlPerDayK          = "max-dl-per-day";//Maximum Downloads Per Day
    public static final String platformK             = "platform";
    public static final String platformIdK           = "platform-id";
    public static final String platformNameK         = "platform-name";
    public static final String platformVersionsK     = "platform-versions";
    public static final String supportDevK           = "supported-devices";
    public static final String supportDevModelsK     = "models"; // Models
    public static final String supportDevTypeK       = "supported-devices-type";
    public static final String supportSpecDevTypeK   = "specific-devices";//Supported Specific Devices Type
    public static final String supportAnyDevTypeK    = "any-supported-devices";//Supported Any Devices Type
    public static final String supportDevBrandK      = "brand";//Supported Device Brand
    public static final String supportDevModelK      = "model";//Supported Device Model
    public static final String cmsRequestTypeK       = "request-type";
    public static final String cmsRepositoryK        = "repository";
    public static final String cmsContentInfoK       = "content-info";
    public static final String contentIdK            = "contentId";
    public static final String cmsDevicesK           = "devices";
    public static final String cmsPlatformsK         = "platforms";
    /* ^ For Downloadable Applications ^ */

    //todo order by alphabet
    public static final String successK                    = "success";
    public static final String failedK                     = "failed";
    public static final String maxNoOfSubscribersK         = "max-no-of-subscribers";
    public static final String maxNoOfBcMsgsPerDayK        = "max-no-of-bc-msgs-per-day";
    public static final String subscriptionRespK           = "subscription-response";
    public static final String sendSubNotificationK        = "notification-url";
    public static final String subscriptionRespMessageK    = "subscription-response-message";
    public static final String unsubscriptionRespMessageK  = "unsubscription-response-message";
    public static final String allowHttpSubRequestsK       = "allow-http-subscription-requests";
    public static final String serviceCodeChargingAmountMappingK = "service-code-charging-amount-mapping";
    public static final String serviceCodeChargingAmountDefaultK = "service-code-charging-amount-default";

    // Subscription Authorization related Keys
    public static final String subscriptionConfirmationRequiredK = "confirmation-required";
    public static final String subscriptionConfirmedK = "confirmed";

    //todo order by alphabet
    public static final String eventDataK               = "event-data";
    public static final String eventNameK               = "event-name";
    public static final String emailBodyK               = "email-body";
    public static final String toAddressListK           = "to-address-list";
    public static final String ccAddressListK           = "cc-address-list";
    public static final String bccAddressListK           = "bcc-address-list";
    public static final String emailSubjectK            = "subject";
    public static final String emailAdditionalContentK  = "additionalContent";
    public static final String fromAddressK             = "from-address";
    public static final String emailFooterK             = "footer";
    public static final String firstNameK               = "first-name";
    public static final String reasonK                  = "reason";
    public static final String mpesaPayBillDuplicatedSpsK = "mpesa-paybill-duplicate-sps";

    //TODO: Order by alphabet
    public static final String spRegistrationRequestEvent               = "sp-registration-request-event";
//    public static final String spEditByAdminRequestEvent                = "sp-save-edit-request-event";
    public static final String spRegistrationApprovedEvent              = "sp-registration-approved-event";
    public static final String spRegistrationApprovedWithChangesEvent   = "sp-registration-approved-with-changes-event";
    public static final String spRegistrationRejectedEvent              = "sp-registration-rejected-event";
    public static final String spSuspendEvent                           = "sp-suspend-event";
    public static final String spRestoreEvent                           = "sp-restore-event";
    public static final String spCrRequestEvent                         = "sp-cr-request-event";
    public static final String spCrApprovedEvent                        = "sp-cr-approved-event";
    public static final String spCrApprovedWithChangesEvent             = "sp-cr-approved-with-changes-event";
    public static final String spCrRejectedEvent                        = "sp-cr-rejected-event";
    public static final String adminSpRequestsSummaryEvent              = "admin-sp-requests-summary-event";
    public static final String spEditByAdminEvent                       = "sp-edit-by-admin-event";
    public static final String appRegistrationRequestEvent              = "app-registration-request-event";
    public static final String appRegistrationWithCaasRequestEvent      = "app-registration-with-caas-request-event";
    public static final String appRegistrationApprovedEvent             = "app-registration-approved-event";
    public static final String appRegistrationApprovedToActiveEvent     = "app-registration-approved-to-active-event";
    public static final String appRegistrationApprovedWithChangesEvent  = "app-registration-approved-with-changes-event";
    public static final String appRegistrationApprovedWithChangesToActiveEvent      = "app-registration-approved-with-changes-to-active-event";
    public static final String appRegistrationRejectedEvent             = "app-registration-rejected-event";
    public static final String appCrRequestEvent                        = "app-cr-request-even";
    public static final String appCrApprovedEvent                       = "app-cr-approved-event";
    public static final String appCrApprovedWithChangesEvent            = "app-cr-approved-with-changes-event";
    public static final String appCrRejectedEvent                       = "app-cr-rejected-event";
    public static final String appCrAppliedToAppEvent                   = "app-cr-applied-to-app-event";
    public static final String appCrResetPasswordEvent                  = "app-cr-reset-password-event";
    public static final String appSpSendPasswordEvent                   = "app-sp-send-password-event";
    public static final String appAdminRequestsSummaryEvent             = "app-admin-requests-summary-event";
    public static final String appSuspendByAdminEvent                   = "app-suspend-by-admin-event";
    public static final String appTerminateByAdminEvent                 = "app-terminate-by-admin-event";
    public static final String appTerminateAutomaticEvent               = "app-terminate-automatic-event";
    public static final String appDeleteByAdminEvent                    = "app-delete-by-admin-event";
    public static final String appEditByAdminEvent                      = "app-edit-by-admin-event";
    public static final String appRestoreFromSuspendEvent               = "app-restore-from-suspend-event";
    public static final String provCrEditNcsEvent                       = "prov-cr-edit-ncs-event";
    public static final String crEditNcsEvent                           = "cr-edit-ncs-event";
    public static final String duplicateMpesaPayBillNoEvent             = "app-registration-duplicate-mpesa-paybill-no-event";


    //TODO: Order by alphabet
    public static final String amountK                  = "amount";
    public static final String paidAmountK              = "paid-amount";
    public static final String paidAmountForSpK         = "paid-amount-for-sp";
    public static final String totalAmountK             = "total-amount";
    public static final String balanceDueK              = "balance-due";
    public static final String creditAllowedK           = "credit-allowed";
    public static final String creditMaxAmountK         = "credit-max-amount";
    public static final String creditMinAmountK         = "credit-min-amount";
    public static final String debitAllowedK            = "debit-allowed";
    public static final String debitMaxAmount           = "debit-max-amount";
    public static final String debitMinAmount           = "debit-min-amount";
    public static final String reserveAllowedK          = "reserve-allowed";
    public static final String reserveMaxAmountK        = "reserve-max-amount";
    public static final String reserveMinAmountK        = "reserve-min-amount";
    public static final String systemIdSdpK             = "SDP";
    public static final String timestampK               = "timestamp";
    public static final String validationErrorK         = "validation-error";
    public static final String asyncK                   = "async";
    public static final String syncK                    = "sync";
    public static final String orderNoK                 = "orderNo";
    public static final String invoiceK                 = "invoiceNo";
    public static final String internalTrxIdCaasReq     = "internalTrxId";
    public static final String partialMsisdnAllowedK    = "partial-msisdn-allowed";

    public static final String chargingTypeFlatK        = "flat";
    public static final String mpesaPayBillNoK          = "mpesa-paybill-number";
    public static final String mpesaBuyGoodsTillNoK     = "mpesa-buygoods-till-number";

    public static final String chargingTrialK           = "trials";
    public static final String chargingTrialAllowedK    = "allowed";
    public static final String chargingTrialMaxCountK   = "max-count";
    public static final String chargingTrialPeriodK     = "period";
    public static final String chargingTrialPeriodValueK= "value";
    public static final String chargingTrialPeriodUnitK = "unit";

    // Authorization Threshold and Medium keys saved in Database
    public static final String mobileAccAuthThresholdK  = "mobile-account-auth-threshold";
    public static final String mobileAccAuthMediumK     = "mobile-account-auth-medium";

    // Authorization Threshold and Medium keys filtered from DialogMobileAccountMetaDataHandler
    public static final String authThresholdK           = "auth-threshold";
    public static final String authMediumK              = "auth-medium";

    public static final String notificationStatusCodeK = "notification-status-code";
    public static final String executionCountK = "execution_count";
    public static final String currentStatusK = "current-status";

    public static final String additionalParamsK = "additionalParams";

    public static final String inAppPurchasingAllowedK = "in-app-purchasing-allowed";
    public static final String inAppApiKeyK = "in-app-api-key";

    public static final String paymentInstrumentAdditionalDataK = "payment-instrument-additional-data";
    public static final String realTimeChargingEnabledK= "real-time-charging-enabled";
    public static final String businessIdK = "business-id";

    /*Vodafone API Application*/
    public static final String vdfapiK = "vdf-apis";
    public static final String checkMsisdnk = "check-msisdn";
    public static final String sendChargek = "send-charge";
    public static final String sendSubsk = "send-subscription";
    public static final String chargeMink = "min-amount";
    public static final String chargeMaxk = "max-amount";
    public static final String vdfApiTpsk = "tps";
    public static final String vdfApiTpdk = "tpd";
    public static final String vdfApiAllowedk = "allowed";
}
