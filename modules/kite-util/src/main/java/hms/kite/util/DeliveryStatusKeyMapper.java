package hms.kite.util;

import java.util.HashMap;
import java.util.Map;

/**
 * User: azeem
 * Date: 2/23/12
 * Time: 12:09 PM
 */
public class DeliveryStatusKeyMapper {

    public static final String DELIVERED            = "DELIVERED";
    public static final String EXPIRED              = "EXPIRED";
    public static final String DELETED              = "DELETED";
    public static final String UNDELIVERABLE        = "UNDELIVERABLE";
    public static final String ACCEPTED             = "ACCEPTED";
    public static final String UNKNOWN              = "UNKNOWN";
    public static final String REJECTED             = "REJECTED";

    public static final String smppDELIVERED        = "DELIVRD";
    public static final String smppEXPIRED          = "EXPIRED";
    public static final String smppDELETED          = "DELETED";
    public static final String smppUNDELIVERABLE    = "UNDELIV";
    public static final String smppACCEPTED         = "ACCEPTD";
    public static final String smppUNKNOWN          = "UNKNOWN";
    public static final String smppREJECTED         = "REJECTD";

    private static Map<String, String> keyMapper;

    static {
        keyMapper = new HashMap<String, String>() {{
            put(smppDELIVERED, DELIVERED);
            put(smppEXPIRED, EXPIRED);
            put(smppDELETED, DELETED);
            put(smppUNDELIVERABLE, UNDELIVERABLE);
            put(smppACCEPTED, ACCEPTED);
            put(smppUNKNOWN, UNKNOWN);
            put(smppREJECTED, REJECTED);
        }};
    }

    public static String getSdpStatusCode(String smppStatusCode) {
        return keyMapper.containsKey(smppStatusCode) ? keyMapper.get(smppStatusCode) : smppStatusCode;
    }
    
    public static String getSmppStatusCode(String sdpStatusCode) {
        for (Map.Entry<String, String> entry : keyMapper.entrySet()) {
            if (entry.getValue().equals(sdpStatusCode)) {
                return entry.getKey();
            }
        }
        return  sdpStatusCode;
    }
}
