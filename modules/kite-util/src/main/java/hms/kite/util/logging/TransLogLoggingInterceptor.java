/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.util.logging;

import hms.kite.util.GsonUtil;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.ReflectiveMethodInvocation;

import hms.kite.util.KiteKeyBox;

import java.util.Map;

/**
 * Created by IntelliJ IDEA. $LastChangedDate 8/30/11 11:45 AM$ $LastChangedBy
 * ruwan$ $LastChangedRevision$ To change this template use File | Settings |
 * File Templates.
 */
public class TransLogLoggingInterceptor implements MethodInterceptor {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransLogLoggingInterceptor.class);

    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {
        Object result = methodInvocation.proceed();
        TransLogWriteCandidate translogDefault = methodInvocation.getMethod().getAnnotation(
                TransLogWriteCandidate.class);

        if (translogDefault == null) {
            return result;
        }

        LOGGER.trace("Trans log Intercepted {}, {}", methodInvocation.getThis().getClass().getName()
                , methodInvocation.getMethod().getName());

        Object mapCandidate = null;
        if (methodInvocation.getArguments() != null && methodInvocation.getArguments().length == 1) {
            mapCandidate = methodInvocation.getArguments()[0];
        }
        LOGGER.trace("First argument is {}", GsonUtil.toJson(mapCandidate));
        if (translogDefault != null && mapCandidate != null && mapCandidate instanceof Map) {

            Map<String, Map<String, Object>> requestContext = (Map<String, Map<String, Object>>) mapCandidate;
//			if ("hms.kite.sms.wf.SdpSmsAoWorkflow".equals(methodInvocation.getThis().getClass().getName())) {
//				//This is used to log transactions from governance
//				LOGGER.trace("Context ================[{}]", requestContext);
//				Map<String, Object> request = requestContext.get(KiteKeyBox.requestK);
//				if (request != null) {
//					if (KiteKeyBox.trueK.equals(request.get(TransLog.logFromInternalWorkFlow))) {
//						LOGGER.debug("Printing translog from SdpSmsAoWorkflow");
//						// Gather the trans-log one last time.
//						TransLog.gatherLogParameters(requestContext);
//						// Log it
//						TransLog.log(requestContext);
//					}
//				}
//
//			} else {

            // Gather the trans-log one last time.
            TransLog.gatherLogParameters(requestContext);

            // Log it
            TransLog.log(requestContext);
//			}

        }

        return result;
    }
}
