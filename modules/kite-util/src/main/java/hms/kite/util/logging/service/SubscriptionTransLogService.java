package hms.kite.util.logging.service;

import hms.kite.util.KiteKeyBox;
import hms.kite.util.logging.KiteLog;
import hms.kite.util.logging.TransLogUtil;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.KiteKeyBox.unregK;
import static hms.kite.util.logging.TransLogUtil.*;

/**
 * (C) Copyright 2011-2012 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 * <p/>
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 * <p/>
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
public class SubscriptionTransLogService extends AbstractTransLogService{

    public static final String REGISTRATION = "registration";
    public static final String FREE_REGISTRATION = "freeRegistration";
    public static final String UNREGISTRATION = "unregistration";
    public static final String REG_STATUS = "reg-status";
    public static final String BASE_SIZE = "base-size";
    public static final String SUBSCRIBED_APPS = "subscribed-apps";
    private String defaultRateCard = "['{'\"currencyCode\":\"{0}\", \"buyingRate\": 1, \"sellingRate\": 1'}']";

    @Override
    public void log(Map<String, Map<String, Object>> requestContext) {
        super.log(requestContext);
        addAdditionalParameters(requestContext);
        TransLogUtil.log(requestContext);
    }

    public void addAdditionalParameters(Map<String, Map<String, Object>> requestContext) {

        Map<String, Object> request = requestContext.get(KiteKeyBox.requestK);
        Map<String, Object> sp = requestContext.get(KiteKeyBox.spK);
        Map<String, Object> app = requestContext.get(KiteKeyBox.appK);
        Map<String, Object> ncs = requestContext.get(KiteKeyBox.ncsK);

        putLogElement(KiteLog.LogField.SOURCE_ADDRESS.getParameter(), request.get(KiteKeyBox.senderAddressK), requestContext);

        if (KiteKeyBox.moK.equals(request.get(KiteKeyBox.directionK))) {
            putLogElement(KiteLog.LogField.SOURCE_CHANNEL_TYPE.getParameter(), "sms", requestContext);
            putLogElement(KiteLog.LogField.SOURCE_CHANNEL_PROTOCOL.getParameter(), "smpp", requestContext);

            TransLogUtil.putChargingDetailsForMo(requestContext, request, ncs, sp);
        }

        String direction = (String)getLogElement(request, directionK);
        if (aoK.equals(direction) || subscriptionRecursiveChargingNotifyK.equals(direction)) {
            if (regK.equals(getLogElement(request, subscriptionRequestTypeK))) {
                if ("1".equals(getLogElement(request, actionK))) {
                    putLogElement(KiteLog.LogField.EVENT_TYPE.getParameter(), getRegistrationType(ncs), requestContext);
                } else if ("0".equals(getLogElement(request, actionK))) {
                    putLogElement(KiteLog.LogField.EVENT_TYPE.getParameter(), UNREGISTRATION, requestContext);
                }
            } else if (subscriptionRecursiveChargingK.equals(getLogElement(request, subscriptionRequestTypeK))) {
                putLogElement(KiteLog.LogField.EVENT_TYPE.getParameter(), getEventType(request), requestContext);
                putLogElement(KiteLog.LogField.CHARGE_AMOUNT.getParameter(), getLogElement(request, chargedAmountK), requestContext);
                putLogElement(KiteLog.LogField.CHARGE_AMOUNT_CURRENCY.getParameter(), getLogElement(request, currencyCodeK), requestContext);
                putLogElement(KiteLog.LogField.RATE_CARD.getParameter(), MessageFormat.format(defaultRateCard, request.get(currencyCodeK)), requestContext);
                putLogElement(KiteLog.LogField.CHARGING_TYPE.getParameter(), getLogElement(request, chargingTypeK), requestContext);
                putLogElement(KiteLog.LogField.CHARGE_ADDRESS.getParameter(), getLogElement(request, msisdnK), requestContext);
                putLogElement(KiteLog.LogField.CHARGE_PARTY_TYPE.getParameter(), "subscriber", requestContext);

            } else if (subscriptionStatusK.equals(getLogElement(request, subscriptionRequestTypeK))) {
                putLogElement(KiteLog.LogField.EVENT_TYPE.getParameter(), REG_STATUS, requestContext);
            } else if (baseSizeK.equals(getLogElement(request, subscriptionRequestTypeK))) {
                putLogElement(KiteLog.LogField.EVENT_TYPE.getParameter(), BASE_SIZE, requestContext);
            } else if (subscribedAppsK.equals(getLogElement(request, subscriptionRequestTypeK))) {
                putLogElement(KiteLog.LogField.EVENT_TYPE.getParameter(), SUBSCRIBED_APPS, requestContext);
            }

            putLogElement(KiteLog.LogField.CHARGED_CATEGORY.getParameter(), "subscription", requestContext);
            putLogElement(KiteLog.LogField.SOURCE_CHANNEL_TYPE.getParameter(), "subscription", requestContext);
            putLogElement(KiteLog.LogField.SOURCE_CHANNEL_PROTOCOL.getParameter(), "http", requestContext);

            List<Map<String, Object>> recipients = (List) request.get(KiteKeyBox.recipientsK);
            if (recipients != null && recipients.size() > 0) {
                Map<String, Object> recipient = recipients.get(0);
                putLogElement(KiteLog.LogField.DESTINATION_ADDRESS.getParameter(), recipient.get(KiteKeyBox.recipientAddressK), requestContext);
            }

            putLogElement(KiteLog.LogField.RESPONSE_CODE.getParameter(), request.get(KiteKeyBox.statusCodeK), requestContext);
            putLogElement(KiteLog.LogField.RESPONSE_DESCRIPTION.getParameter(), getTransactionStatus((String) request.get(KiteKeyBox.statusCodeK)), requestContext);
            putLogElement(KiteLog.LogField.OVERALL_RESPONSE_CODE.getParameter(), request.get(KiteKeyBox.statusCodeK), requestContext);
            putLogElement(KiteLog.LogField.OVERALL_RESPONSE_DESCRIPTION.getParameter(), request.get(KiteKeyBox.statusDescriptionK), requestContext);

        } else {
            String serviceKeyword = (String) getLogElement(request, serviceKeywordK);

            if (regK.equalsIgnoreCase(serviceKeyword)) {
                putLogElement(KiteLog.LogField.EVENT_TYPE.getParameter(), getRegistrationType(ncs), requestContext);
            } else if (unregK.equalsIgnoreCase(serviceKeyword)) {
                putLogElement(KiteLog.LogField.EVENT_TYPE.getParameter(), UNREGISTRATION, requestContext);
            }
        }

        /* As of now only payment instrument allowed for subscription is mobile account.*/
        if(subscriptionRecursiveChargingNotifyK.equals(direction)) {
            putLogElement(KiteLog.LogField.FROM_PAYMENT_INSTRUMENT_NAME.getParameter(), "Mobile Account", requestContext);
        }

        putRecipientsDetails(requestContext, request, sp, getIsMasked(app));
    }


    private String getEventType(Map<String, Object> request) {
        String subscriptionResponse = String.valueOf(request.get(subscriptionRespK));
        if("RECURSIVE_SUCCESS" == subscriptionResponse){
            return "registrationRecursiveCharging";
        } else   if("SUCCESS" == subscriptionResponse){
            return "registrationCharging";
        }  else {
            return "registrationRecursiveCharging";
        }
    }


    private String getRegistrationType(Map<String, Object> ncs) {
        if (ncs != null) {
            Map<String, Object> chargeDetails = (Map<String, Object>) ncs.get(chargingK);
            if (chargeDetails != null && freeK.equalsIgnoreCase(String.valueOf(chargeDetails.get(typeK)))) {
                return FREE_REGISTRATION;
            } else {
                return REGISTRATION;
            }
        } else {
            return "";
        }
    }

    public void setDefaultRateCard(String defaultRateCard) {
        this.defaultRateCard = defaultRateCard;
    }
}
