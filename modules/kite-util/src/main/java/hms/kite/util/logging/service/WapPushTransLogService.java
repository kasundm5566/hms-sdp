package hms.kite.util.logging.service;

import hms.kite.util.KiteKeyBox;
import hms.kite.util.logging.KiteLog;
import hms.kite.util.logging.TransLogUtil;

import java.util.Map;

import static hms.kite.util.logging.TransLogUtil.getIsMasked;
import static hms.kite.util.logging.TransLogUtil.putLogElement;
import static hms.kite.util.logging.TransLogUtil.putRecipientsDetails;

/**
 * (C) Copyright 2011-2012 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 * <p/>
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 * <p/>
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
public class WapPushTransLogService  extends AbstractTransLogService{

    @Override
    public void log(Map<String, Map<String, Object>> requestContext) {
        super.log(requestContext);
        addAdditionalParameters(requestContext);
        TransLogUtil.log(requestContext);
    }

    public void addAdditionalParameters(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> request = requestContext.get(KiteKeyBox.requestK);
        Map<String, Object> sp = requestContext.get(KiteKeyBox.spK);
        Map<String, Object> app = requestContext.get(KiteKeyBox.appK);

        if (KiteKeyBox.mtK.equals(request.get(KiteKeyBox.directionK))) {
            putLogElement(KiteLog.LogField.SOURCE_CHANNEL_TYPE.getParameter(), "push", requestContext);
            putLogElement(KiteLog.LogField.SOURCE_CHANNEL_PROTOCOL.getParameter(), "http", requestContext);
            putLogElement(KiteLog.LogField.DESTINATION_CHANNEL_TYPE.getParameter(), "push", requestContext);
            putLogElement(KiteLog.LogField.DESTINATION_CHANNEL_PROTOCOL.getParameter(), "smpp", requestContext);

        }

        putRecipientsDetails(requestContext, request, sp, getIsMasked(app));
    }
}
