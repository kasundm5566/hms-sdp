package hms.kite.util;

/**
 * Payment Gateway parameters.
 * <p/>
 * $LastChangedDate 7/27/11 2:36 PM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
public enum PgwParam {

    ACCOUNT_NO("account-no"),
    ACC_CURRENCY("acc_currency"),
    ADDITIONAL_PARAMS("additionalParams"),
    AMOUNT("amount"),
    AMOUNT_VALUE("amount-value"),
    CANCEL_REASON("cancel-reason"),
    CURRENCY_CODE("currency-code"),
    EXT_TRANSID("ext-transid"),
    INT_TRANSID("int-transid"),
    IS_AUTH_REQUIRED("is-auth-reqd"),
    IS_DEFAULT("is-default"),
    MERCHANT_SHOULD_BE_ALLOWED("merchant-should-be-allowed"),
    CHARGED_CATEGORY("charged-category"),
    CLIENT_TRANS_ID("client-trans-id"),
    CLIENT_TRANS_ID_FOR_RESPONSE("client-transid"),
    REQUESTED_TIMESTAMP("requested-timestamp"),
    MERCHANT_ID("merchant-id"),
    MERCHANT_NAME("merchant-name"),
    MSISDN("msisdn"),
    PAYER_DETAILS("payer-details"),
    PAYMENT_INS_NAME("payment-ins-name"),
    FROM_PAYMENT_INS_NAME("from-payment-ins-name"),
    PAYINS_ACC_ID("payins-acc-id"),
    PAYINS_NAME("payins_name"),
    PAYINS_TYPE("payins_type"),
    PAYINS_ID("payins_id"),
    SYSTEM_ID("system-id"),
    RESERVATION_ID("reservation-id"),
    STATUS_CODE("status-code"),
    STATUS_TEXT("status-text"),
    USER_ID("userId"),
    USER_ID_DASHED("user-id"),
    REQUIESTED_APP_ID("requested-app-id"),
    CHARGING_INSTRUCTION("charging-instructions"),
    TRANSACTION_ID("transaction-id"),
    BUSINESS_NO("business-no"),
    REFERENCE_ID("reference-id"),
    AMOUNT_DUE("amount-due"),
    CHARGING_SHORT_INSTRUCTIONS("charging-short-instructions"),
    IS_OVERPAYMENT_ALLOWED("is-over-payment-allowed"),
    IS_PARTIAL_PAYMENT_ALLOWED("is-partial-payment-allowed"),
    TIMEOUT_PERIOD("timeout-period"),
    ACCOUNT_TYPE("account-type"),
    ACCOUNT_STATUS("account-status"),
    CHARGEABLE_BALANCE("balance"),
    CHARGEABLE_BALANCE_AMOUNT("amount-value");

    private String parameter;

    PgwParam(String value){
        this.parameter = value;
    }

    public String getParameter() {
        return parameter;
    }
}
