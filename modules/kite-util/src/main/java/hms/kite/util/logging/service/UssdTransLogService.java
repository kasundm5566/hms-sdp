package hms.kite.util.logging.service;

import hms.kite.util.KiteErrorBox;
import hms.kite.util.KiteKeyBox;
import hms.kite.util.logging.KiteLog;
import hms.kite.util.logging.TransLogUtil;

import java.util.Map;
import static hms.kite.util.logging.TransLogUtil.*;

/**
 * (C) Copyright 2011-2012 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 * <p/>
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 * <p/>
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
public class UssdTransLogService extends AbstractTransLogService{

    @Override
    public void log(Map<String, Map<String, Object>> requestContext) {
        super.log(requestContext);
        addAdditionalParameters(requestContext);
        TransLogUtil.log(requestContext);
    }

    public void addAdditionalParameters(Map<String, Map<String, Object>> requestContext) {

        Map<String, Object> request = requestContext.get(KiteKeyBox.requestK);
        Map<String, Object> sp = requestContext.get(KiteKeyBox.spK);
        Map<String, Object> app = requestContext.get(KiteKeyBox.appK);
        Map<String, Object> ncs = requestContext.get(KiteKeyBox.ncsK);

        if(request != null) {
            fetchLogParameter(KiteKeyBox.serviceCodeK, KiteLog.LogField.SHORT_CODE.getParameter(), request, requestContext);
        }

        if (KiteKeyBox.mtK.equals(request.get(KiteKeyBox.directionK))) {
            putLogElement(KiteLog.LogField.SOURCE_CHANNEL_TYPE.getParameter(), "ussd", requestContext);
            putLogElement(KiteLog.LogField.SOURCE_CHANNEL_PROTOCOL.getParameter(), "http", requestContext);
            putLogElement(KiteLog.LogField.DESTINATION_CHANNEL_TYPE.getParameter(), "ussd", requestContext);
            putLogElement(KiteLog.LogField.DESTINATION_CHANNEL_PROTOCOL.getParameter(), "smpp", requestContext);

            fetchLogParameter(KiteKeyBox.statusCodeK, KiteLog.LogField.OVERALL_RESPONSE_CODE.getParameter(), request,
                    requestContext);
            fetchLogParameter(KiteKeyBox.statusDescriptionK, KiteLog.LogField.OVERALL_RESPONSE_DESCRIPTION.getParameter(),
                    request, requestContext);
            putLogElement(KiteLog.LogField.TRANSACTION_STATUS.getParameter(), getTransactionStatus((String) request.get(KiteKeyBox.statusCodeK)), requestContext);

        } else if(KiteKeyBox.moK.equals(request.get(KiteKeyBox.directionK))) {
            putLogElement(KiteLog.LogField.SOURCE_CHANNEL_TYPE.getParameter(), "ussd", requestContext);
            putLogElement(KiteLog.LogField.SOURCE_CHANNEL_PROTOCOL.getParameter(), "smpp", requestContext);
            putLogElement(KiteLog.LogField.DESTINATION_CHANNEL_TYPE.getParameter(), "ussd", requestContext);
            putLogElement(KiteLog.LogField.DESTINATION_CHANNEL_PROTOCOL.getParameter(), "http", requestContext);

            String addressStatus = (String) request.get(KiteKeyBox.senderAddressStatusK);
            String statusCode = (String) request.get(KiteKeyBox.statusCodeK);
            String requestStatus = KiteErrorBox.successCode;
            if (KiteErrorBox.successCode.equals(addressStatus)) {
                requestStatus = statusCode;
            } else if (KiteErrorBox.successCode.equals(statusCode)) {
                requestStatus = addressStatus;
            } else {
                requestStatus = statusCode;
            }

            putLogElement(KiteLog.LogField.RESPONSE_CODE.getParameter(), requestStatus, requestContext);
            putLogElement(KiteLog.LogField.RESPONSE_DESCRIPTION.getParameter(),
                    buildStatusDescriptionIfNotAvailable(requestStatus, null, requestContext), requestContext);
            putLogElement(KiteLog.LogField.TRANSACTION_STATUS.getParameter(), getTransactionStatus(requestStatus),
                    requestContext);

            TransLogUtil.putChargingDetailsForMo(requestContext, request, ncs, sp);
        }

        putRecipientsDetails(requestContext, request, sp, getIsMasked(app));
    }
}