/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.util;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static hms.kite.util.KiteErrorBox.invalidMsisdnErrorCode;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SystemUtil {

    private static int txIdIncrmentator = 0;
    private static final String DATE_FORMAT_CORRELATION_ID = "yyMMddHHmm";
    private static final String DECIMAL_FORMAT_CORRELATION_ID = "0000";
    private static short systemId;
    private static final String DATE_FORMAT_WS_BAS = "yyyyMMddHHmmss";
    private static final String XForwardedFor = "X-Forwarded-For";
    private static final String colon = ":";
    private static final Logger logger = LoggerFactory.getLogger(SystemUtil.class);

    /**
     * Creates a long value prefixed by systemId. Value is unique
     * within a single jvm.
     *
     * @param systemId
     * @return
     */
    public static long getCorrelationId(short systemId) {
        int localIndex;
        synchronized (SystemUtil.class) {
            if (txIdIncrmentator >= 9999) {
                txIdIncrmentator = 0;
            }
            localIndex = ++txIdIncrmentator;
        }
        SimpleDateFormat formatter =
                new SimpleDateFormat(DATE_FORMAT_CORRELATION_ID);
        Date date = new Date();
        StringBuilder sb = new StringBuilder();
        sb.append(systemId);
        sb.append(formatter.format(date));

        DecimalFormat df = new DecimalFormat(DECIMAL_FORMAT_CORRELATION_ID);
        sb.append(df.format(localIndex));

        return Long.parseLong(sb.toString());
    }

    /**
     * Creates a long value prefixed by systemId. Value is unique
     * within a single jvm.
     * <p/>
     * CoreUtil of the corelogic have the same method with systemId hardcoded as 9, so better not to use 9 for system Id
     *
     * @return
     */
    public static long getCorrelationId() {
        return getCorrelationId(systemId);
    }

    /**
     * Create a random password which is "n" lengths
     * @param n
     * @return
     */
    public static String getPassword(int n) {
        char[] pw = new char[n];
        int c  = 'A';
        int  r1 = 0;
        for (int i=0; i < n; i++)
        {
            r1 = (int)(Math.random() * 3);
            switch(r1) {
                case 0: c = '0' +  (int)(Math.random() * 10); break;
                case 1: c = 'a' +  (int)(Math.random() * 26); break;
                case 2: c = 'A' +  (int)(Math.random() * 26); break;
            }
            pw[i] = (char)c;
        }
        return new String(pw);
    }

    /**
     * The input string will be encrypted using MD5 algorithm
     * @param value
     * @return
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    public static String md5Encryption(String value) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        return new String(Hex.encodeHex(MessageDigest.getInstance("MD5").digest(value.getBytes("UTF-8"))));
    }

    public void setSystemId(short systemId) {
        SystemUtil.systemId = systemId;
    }

    public static String findHostIp(HttpServletRequest request) {
        if (null != request) {
            String xForwardedFor = request.getHeader(XForwardedFor);
            if (isAvailable(xForwardedFor)) {
                return getRemoteIpFromXForwardedFor(xForwardedFor);
            } else {
                return getRemoteHostIp(request);
            }
        }
        return null;
    }


    private static String getRemoteHostIp(HttpServletRequest request) {
        String host = request.getRemoteHost();
        if ( null != host && !host.isEmpty()) {
            if (host.contains(":")) {
                final String[] hostdetails = host.split(":");
                return hostdetails[0];
            } else {
                return host;
            }
        }
        return null;
    }

    private static String getRemoteIpFromXForwardedFor(String xForwardedFor) {
        int idx = xForwardedFor.indexOf(',');
        if (idx > -1) {
            xForwardedFor = xForwardedFor.substring(0, idx);
        }
        return xForwardedFor;
    }

    private static boolean isAvailable(String xForwardedFor) {
        return xForwardedFor != null && !xForwardedFor.trim().equals("");
    }

    public static List<Map<String, String>> convert2NblAddresses(List<String> addresses) {
        List<Map<String, String>> interpretedAddresses = new ArrayList<Map<String, String>>();
        for (String address : addresses) {
            interpretedAddresses.add(convert2NblAddresses(address));
        }

        return interpretedAddresses;
    }

    public static Map<String, String> convert2NblAddresses(String address) {
        Map<String, String> addressMap = new HashMap<String, String>();
        if (null != address && address.startsWith(telK) && address.contains(colon)) {
            address = address.substring(address.indexOf(colon) + 1, address.length()).trim();
            addressMap.put(recipientAddressK, address);
            addressMap.put(chargingTrxIdK, IdGenerator.generate());
        } else {
            logger.info("Invalid msisdn found[{}]", address);
            addressMap.put(recipientAddressK, address.trim());
            addressMap.put(recipientAddressStatusK, invalidMsisdnErrorCode);
        }
        return addressMap;
    }

    public static String addAddressIdentifier(String msisdn) {
        return new StringBuilder(telK).append(":").append(msisdn).toString();
    }

    public static boolean isBroadcastRequest(Map<String, Map<String, Object>> requestContext) {
        for (Map<String, String> recipient : (List<Map<String, String>>) requestContext.get(requestK).get(recipientsK)) {
            if (recipient.get(recipientAddressK).trim().equals(allK)) {
                logger.info("Message will be considered as a broadcast message");
                return true;
            }
        }
        return requestContext.get(requestK).containsKey(broadcastAddressK);
    }

    public static boolean isMaskedApp(Map<String, Map<String, Object>> requestContext) {
        Map<String, Object> app = requestContext.get(appK);
        if (app != null && app.containsKey(maskNumberK)) {
            return (Boolean) app.get(maskNumberK);
        } else {
            return false;
        }
    }

    public static List<Map<String, String>> generateRecipientList(Map<String, Object> request) {
        List<Map<String, String>> recipientList = new ArrayList<Map<String, String>>();
        String recipientAddress = (String) request.get(recipientAddressK);
        if (null != recipientAddress) {
            Map<String, String> recipient = new HashMap<String, String>();
            recipient.put(recipientAddressK, recipientAddress);
            recipient.put(chargingTrxIdK, new Long(System.currentTimeMillis()).toString());
            recipientList.add(recipient);
            request.remove(recipientAddressK);
        }

        List<String> keysToRemove = new ArrayList<String>();
        for (final Map.Entry<String, Object> reqParam : request.entrySet()) {
            String key = reqParam.getKey();
            if (key.matches(recipientAddressRegExK)) {
                Map<String, String> recipient = new HashMap<String, String>();
                recipient.put(recipientAddressK, (String) reqParam.getValue());
                recipient.put(chargingTrxIdK, new Long(System.currentTimeMillis()).toString());
                recipientList.add(recipient);
                keysToRemove.add(reqParam.getKey());
            }
        }

        for (String key : keysToRemove) {
            request.remove(key);
        }

        return recipientList;
    }

    public static Map<String, Map<String, Object>> copy(Map<String, Map<String, Object>> requestContext) {
        Map<String, Map<String, Object>> copy = new HashMap<String, Map<String, Object>>();

        for (final Map.Entry<String, Map<String, Object>> reqParam : requestContext.entrySet()) {
            copy.put(reqParam.getKey(), new HashMap<String, Object>(reqParam.getValue()));
        }

        return copy;
    }

    private static Set<String> routingKeyAvailableNcses;
    static {
        routingKeyAvailableNcses = new HashSet<String>();
        routingKeyAvailableNcses.add(smsK);
        routingKeyAvailableNcses.add(ussdK);
    }
    /**
     * Check whether the given ncs type has routing keys. For example, wappush, cas, subscription slas will
     * not have routing keys.
     * @param ncsType
     * @return
     */
    public static boolean isRkAvailableForNcsType(String ncsType) {
        return routingKeyAvailableNcses.contains(ncsType);
    }

}
