/**
 *   (C) Copyright 2011-2012 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.util;


/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 */
public class IdGenerator {

	/**
	 * Creates a long value prefixed by serverId. Value is unique within a
	 * single jvm.
	 *
	 * @return
	 */
    @Deprecated
	public static String generate() {
		return hms.commons.IdGenerator.generateId();
	}
}
