/**
 *   (C) Copyright 2011-2012 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class StatusDescriptionBuilder {

    private static final Logger logger = LoggerFactory.getLogger(StatusDescriptionBuilder.class);

    private String statusDescriptionResourceName;

    private Properties statusDescriptions;

    public StatusDescriptionBuilder() {

    }

    public StatusDescriptionBuilder(String statusDescriptionResourceName) {
        this.statusDescriptionResourceName = statusDescriptionResourceName;
    }

    public void init() {
        loadProperties();
    }

    public String createStatusDescription(String statusCode, Map<String, Map<String, Object>> requestContext) {
        try {
            String[] formatKeys = getFormatKeys(statusCode);
            String[] formatParts = getFormatParts(formatKeys, requestContext);

            return getFormattedDescription(statusCode, formatParts);
        } catch (Exception e) {
            logger.warn("Unable to get status description for status code [" + statusCode + "]", e);
            return null;
        }
    }

    private String[] getFormatParts(String[] formatKeys, Map<String, Map<String, Object>> requestContext) {
        List<String> parts = new ArrayList<String>();
        for(String key : formatKeys) {
            Object formatObject = KeyNameSpaceResolver.data((Map) requestContext, key);
            if (null == formatObject || !(formatObject instanceof String)) {
                throw new NoSuchElementException(key);
            }
            parts.add((String) formatObject);
        }
        String[] partsArray = new String[parts.size()];
        partsArray = parts.toArray(partsArray);
        return partsArray;
    }

    private String[] getFormatKeys(String statusCode) {
        List<String> props = new ArrayList<String>();
        for (String prop : statusDescriptions.stringPropertyNames()) {
            if (prop.startsWith(statusCode) && !prop.endsWith("description")) {
                props.add(prop);
            }
        }
        String[] keys = new String[props.size()];
        for (String prop : props) {
            StringTokenizer st = new StringTokenizer(prop, ".");
            st.nextToken();
            String index = st.nextToken();
            keys[new Integer(index)] = statusDescriptions.getProperty(prop);
        }
        return keys;
    }

    private String getFormattedDescription(String statusCode, String[] formatParts) {
        String message = statusDescriptions.getProperty(new StringBuilder(statusCode).append(".description").toString());

        if (null != formatParts && 0 < formatParts.length) {
            MessageFormat format = new MessageFormat(message);
            return format.format(formatParts);
        }

        return message;
    }

    private void loadProperties() {
        try {
            ResourceBundle rb = ResourceBundle.getBundle(statusDescriptionResourceName);
            statusDescriptions = new Properties();
            Set<String> keySet = rb.keySet();
            for (String key : keySet) {
                statusDescriptions.put(key, rb.getString(key));
            }
        } catch (Throwable e) {
            logger.error("Unable to get load status descriptions", e);
            statusDescriptions = new Properties();
        }
    }

    public void setStatusDescriptionResourceName(String statusDescriptionResourceName) {
        this.statusDescriptionResourceName = statusDescriptionResourceName;
    }
}
