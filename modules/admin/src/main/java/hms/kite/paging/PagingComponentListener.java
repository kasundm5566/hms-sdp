/*
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.paging;

public interface PagingComponentListener {

    /**
     * This Method will be executed when page is changed by clicking a page index
     * or next, previous button
     * @param start start of the batch need to be loaded
     * @param batchSize  Size of batch need to be loaded
     */
    public void onReload(int start, int batchSize);

}