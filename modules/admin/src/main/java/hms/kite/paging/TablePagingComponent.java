/*
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.paging;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.themes.BaseTheme;

public class TablePagingComponent extends HorizontalLayout {


    private PagingComponentListener pagingComponentListener;
    private int recordsPerPage;
    private int totalNumberOfPages;
    private int maxPagesDisplayed = 5;
    private HorizontalLayout pageIndexLayout = new HorizontalLayout();
    private int displayStartIndex = 1;
    private int displayLastIndex;
    private int displaySelectedIndex = 1;
    private String nextButtonText = "Next >>";
    private String prevButtonText = "<< Prev";

    public TablePagingComponent(PagingComponentListener pagingComponentListener, int totalRecordCount, int recordsPerPage) {
        this.pagingComponentListener = pagingComponentListener;
        this.recordsPerPage = recordsPerPage;
        totalNumberOfPages = (int) Math.ceil((double )totalRecordCount / recordsPerPage);
        addComponent(pageIndexLayout);
    }

    public void init() {
        initializeData();
        reloadIndexBar();
    }

    private void initializeData() {
        if (totalNumberOfPages < maxPagesDisplayed) {
            maxPagesDisplayed = totalNumberOfPages;
        }
        displayLastIndex = maxPagesDisplayed;
    }

    private void reloadIndexBar() {
        pagingComponentListener.onReload((displaySelectedIndex - 1) * recordsPerPage, recordsPerPage);
        pageIndexLayout.removeAllComponents();
        if(totalNumberOfPages <= 1) {
            return;
        }
        Button previousBtn = new Button(prevButtonText);
        previousBtn.setStyleName(BaseTheme.BUTTON_LINK);
        previousBtn.addStyleName("page-link");

        previousBtn.addListener(new Button.ClickListener() {

            public void buttonClick(Button.ClickEvent clickEvent) {
                if (displayStartIndex <= 1) {
                    if (displaySelectedIndex > displayStartIndex) {
                        displaySelectedIndex--;
                        reloadIndexBar();
                    }
                    return;
                }
                displayStartIndex--;
                displayLastIndex--;
                displaySelectedIndex--;
                reloadIndexBar();
            }
        });
        pageIndexLayout.addComponent(previousBtn);

        reloadPageIndexes();

        Button nextBtn = new Button(nextButtonText);
        nextBtn.setStyleName(BaseTheme.BUTTON_LINK);
        nextBtn.addStyleName("page-link");
        nextBtn.addListener(new Button.ClickListener() {

            public void buttonClick(Button.ClickEvent clickEvent) {
                if (displayLastIndex >= totalNumberOfPages) {
                    if (displaySelectedIndex < displayLastIndex) {
                        displaySelectedIndex++;
                        reloadIndexBar();
                    }
                    return;
                }
                displayStartIndex++;
                displayLastIndex++;
                displaySelectedIndex++;
                reloadIndexBar();
            }
        });
        pageIndexLayout.addComponent(nextBtn);
    }

    private void reloadPageIndexes() {
        for (int i = displayStartIndex; i <= displayLastIndex; i++) {
            Button indexBtn = new Button("" + i);
            indexBtn.setData(new Integer(i));
            indexBtn.setStyleName(BaseTheme.BUTTON_LINK);
            if (i == displaySelectedIndex) {
                indexBtn.addStyleName("page-selected-link");
            } else {
                indexBtn.addStyleName("page-link");
            }
            indexBtn.addListener(new Button.ClickListener() {


                public void buttonClick(Button.ClickEvent clickEvent) {
                    Integer pageIndex = (Integer) clickEvent.getButton().getData() - 1;
                    displaySelectedIndex = pageIndex + 1;
                    reloadIndexBar();
                }
            });
            pageIndexLayout.addComponent(indexBtn);

        }
    }

    public String getPrevButtonText() {
        return prevButtonText;
    }

    public void setPrevButtonText(String prevButtonText) {
        this.prevButtonText = prevButtonText;
    }

    public String getNextButtonText() {
        return nextButtonText;
    }

    public void setNextButtonText(String nextButtonText) {
        this.nextButtonText = nextButtonText;
    }

    public int getMaxPagesDisplayed() {
        return maxPagesDisplayed;
    }

    public void setMaxPagesDisplayed(int maxPagesDisplayed) {
        this.maxPagesDisplayed = maxPagesDisplayed;
    }
}