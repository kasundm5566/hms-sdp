/*
*   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*
*/

package hms.kite;

import org.jasig.cas.client.authentication.DefaultGatewayResolverImpl;
import org.jasig.cas.client.authentication.GatewayResolver;
import org.jasig.cas.client.util.CommonUtils;
import org.jasig.cas.client.validation.Assertion;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CasAuthenticationFilter extends org.jasig.cas.client.util.AbstractCasFilter {

    private static final String ADMINSTORE_USER_LOGGED_IN = "admin-user-logged-in";
    private String casServerLoginUrl;
    private boolean renew = false;

    private boolean gateway = false;

    private GatewayResolver gatewayStorage = new DefaultGatewayResolverImpl();
    private String casLogoutUrl;
    private String adminLogoutPattern;

    public static final Logger LOGGER = LoggerFactory.getLogger(CasAuthenticationFilter.class);

    protected void initInternal(FilterConfig filterConfig) throws ServletException {
        if (!isIgnoreInitConfiguration()) {
            super.initInternal(filterConfig);
            setCasServerLoginUrl(getPropertyFromInitParams(filterConfig, "casServerLoginUrl", null));
            setRenew(parseBoolean(getPropertyFromInitParams(filterConfig, "renew", "false")));
            setGateway(parseBoolean(getPropertyFromInitParams(filterConfig, "gateway", "false")));

            String gatewayStorageClass = getPropertyFromInitParams(filterConfig, "gatewayStorageClass", null);
            casLogoutUrl = filterConfig.getInitParameter("casLogoutUrl");
            adminLogoutPattern = filterConfig.getInitParameter("adminLogoutPattern");


            if (gatewayStorageClass != null)
                try {
                    this.gatewayStorage = ((GatewayResolver) Class.forName(gatewayStorageClass).newInstance());
                } catch (Exception e) {
                    this.log.error(e, e);
                    throw new ServletException(e);
                }
        }
    }

    public void init() {
        super.init();
        CommonUtils.assertNotNull(this.casServerLoginUrl, "casServerLoginUrl cannot be null.");
    }

    public final void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);
        String serviceUrl = constructServiceUrl(request, response);

        final String url = ((HttpServletRequest) servletRequest).getRequestURL().toString();
        if (url != null && url.endsWith(adminLogoutPattern)) {
            session.invalidate();
            response.sendRedirect(casLogoutUrl);
            return;
        } else {
            log.info(url);
        }
        Assertion assertion = session != null ? (Assertion) session.getAttribute(CONST_CAS_ASSERTION) : null;

        if (assertion != null) {
            filterChain.doFilter(request, response);
            return;
        }

        String ticket = CommonUtils.safeGetParameter(request, getArtifactParameterName());
        boolean wasGatewayed = this.gatewayStorage.hasGatewayedAlready(request, serviceUrl);
        if ((CommonUtils.isNotBlank(ticket)) || (wasGatewayed)) {
            filterChain.doFilter(request, response);
            return;
        }
        String modifiedServiceUrl;
        if (this.gateway) {
            modifiedServiceUrl = this.gatewayStorage.storeGatewayInformation(request, serviceUrl);
        } else {
            modifiedServiceUrl = serviceUrl;
        }

        String urlToRedirectTo = CommonUtils.constructRedirectUrl(this.casServerLoginUrl, getServiceParameterName(), modifiedServiceUrl, this.renew, this.gateway);
        response.sendRedirect(urlToRedirectTo);
    }

    public final void setRenew(boolean renew) {
        this.renew = renew;
    }

    public final void setGateway(boolean gateway) {
        this.gateway = gateway;
    }

    public final void setCasServerLoginUrl(String casServerLoginUrl) {
        this.casServerLoginUrl = casServerLoginUrl;
    }

    public final void setGatewayStorage(GatewayResolver gatewayStorage) {
        this.gatewayStorage = gatewayStorage;
    }

    public void setCasLogoutUrl(String casLogoutUrl) {
        this.casLogoutUrl = casLogoutUrl;
    }

    public void setAdminLogoutPattern(String adminLogoutPattern) {
        this.adminLogoutPattern = adminLogoutPattern;
    }

}