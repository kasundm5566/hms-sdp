(ns hms.kite.advertisement.manage-advertisement
  (:import (com.vaadin.terminal ExternalResource Sizeable))
  (:import (com.vaadin.ui GridLayout Table))
  (:import (com.vaadin.data Validator))
  (:import (hms.kite.paging TablePagingComponent))
  (:use hms.kite.admin.vaadin)
  (:use hms.kite.repo.advertisement-repo)
  (:use clojure.tools.logging)
  (:use hms.kite.common-util))

(declare fill-table create-ad-edit-window create-ad-delete-window create-button)

(def manage-panel-width "850px")
(def manage-t-width "690px")
(def manage-t-hight "400px")

(def manage-t-headers
  [["manage.t.adname" String] ["manage.t.content" String] ["manage.t.action" GridLayout]])

(def manage-t-header-size
  [["manage.t.adname" 200] ["manage.t.content" 500] ["manage.t.action" 150]])

(defn reload-manage-ad-table [list table window paging panel]
  (doseq [con list]
    (.addItem table (into-array Object [(get con :_id) (get con :message) (create-button window con table paging panel)]) (get con :_id))))

(defn summery-manage-listener-fn [table window paging panel]
  (proxy [hms.kite.paging.PagingComponentListener] []
    (onReload [start batch]
      (.removeAllItems table)
      (reload-manage-ad-table (fetch-all-ad start batch) table window paging panel))))

(defn page-summery-manage [total rec-per-page table window paging panel]
  (let [table-pag (TablePagingComponent. (summery-manage-listener-fn table window paging panel) total rec-per-page)]
    (.init table-pag)
    table-pag))

(defn fill-table [table paging-layout window panel]
  (.removeAllComponents paging-layout)
  (.addComponent paging-layout (page-summery-manage (get-total-number-of-ads) 10 table window paging-layout panel)))

(defn create-ad [id message]
  {:_id id :last-modified-time (System/currentTimeMillis) :created-time (System/currentTimeMillis) :status "enable" :message message})

(defn validate-add [ad-name ad-content window]
  (if (= (fetch-ad-by-name ad-name) ())
    (do
      (save-ad (create-ad ad-name ad-content))
      (log "admin-event" :info nil (str ad-name "|" ad-content)))
    (display-message-humanized-message window (i18n "add.already.exist"))))

(defn create-advertisment-comp [window table paging-layout panel]
  (let [add-window (new-sub-window "panel.ad.create") form (new-form) h-layout (new-h-layout) ad-name (new-text-with-name "create.ad.name")
        ad-content (new-text-area-with-name "create.ad.content") add-button (new-button "button.create.submit") cancel-button (new-button "ad.cancel.button")
        error-h (new-h-layout) error-l (new-label "error.label.add")]
    (.addWindow window add-window)
    (doto add-window
      (.setReadOnly true)
      (.setResizable false)
      (.setModal true)
      (.setHeight "270px")
      (.setWidth "550px")
      (.addComponent form)
      (.addComponent error-h)
      (.addComponent h-layout))
    (doto error-h
      (.addStyleName "error-h-add"))
    (doto error-l
      (.addStyleName "error-label-add"))
    (doto form
      (.addStyleName "create-add-form")
      (.addField "ad-n" ad-name)
      (.addField "ad-c" ad-content))
    (doto h-layout
      (.setSpacing true)
      (.addStyleName "create-add-h-layout")
      (.addComponent add-button)
      (.addComponent cancel-button))
    (doto ad-name
      (.setWidth "250px")
      (.setRequired true)
      (.setRequiredError "This is mandatory"))
    (doto ad-content
      (.setWidth "250px")
      (.setMaxLength (Integer/parseInt (i18n "create.add.conten.max.length")))
      (.setRequired true)
      (.setRequiredError "This is mandatory"))
    (.addListener add-button (proxy [com.vaadin.ui.Button$ClickListener] []
                               (buttonClick [event]
                                 (if (or (not (is-nil-empty-string? (.getValue ad-name))) (not (is-nil-empty-string? (.getValue ad-content))))
                                    (.addComponent error-h error-l)
                                   (do
                                     (try [(.validate form)
                                           (validate-add (.trim (.getValue ad-name)) (.trim (.getValue ad-content)) window)
                                           (fill-table table paging-layout window panel)
                                           (.removeWindow window add-window)]
                                       (catch Exception e)))))))
    (.addListener cancel-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                  (buttonClick [event]
                                    (.removeWindow window add-window))))))

(defn create-search-content [h-layout table panel paging-layout window h-bu]
  (let [form (new-form) text (new-text-field) search-button (new-small-button "manage.search.button") h (new-h-layout) h-layout-1 (new-h-layout)
        add-adve-button (new-link-button "child.add.advertisement")]
    (doto h-bu
      (.addStyleName "h-button")
      (.addComponent add-adve-button))
    (.addComponent h-layout form)
    (.addComponent h-layout h-layout-1)
    (doto form
      (.setLayout h)
      (.addStyleName "manage-ad-form"))
    (doto h
      (.setSpacing true)
      (.addComponent (new-label "manage.search.label"))
      (.addComponent text)
      (.addComponent search-button))
    (.addStyleName search-button "manage-ad-search")
    (.addListener search-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                  (buttonClick [event]
                                    (.removeAllItems table)
                                    (.removeComponent panel paging-layout)
                                    (if (= (.getValue text) "")
                                      (do
                                        (fill-table table paging-layout window panel))
                                      (do
                                        (let [search-pattern (re-pattern (str "(?i)" (.trim (.getValue text))))
                                              addvertisements (fetch-ad-by-name search-pattern)]
                                          (if (not-nil-and-empty? addvertisements)
                                            (doseq [con addvertisements]
                                            (.addItem table (into-array Object [(get con :_id) (get con :message) (create-button window con table paging-layout panel)]) (get con :_id)))
                                            (display-message-humanized-message window (i18n "no.results.found")))))))))
    (.addListener add-adve-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                    (buttonClick [event]
                                      (create-advertisment-comp window table paging-layout panel))))))


(defn create-manage-table [h-layout paging-layout manage-t window panel]
  (.addComponent h-layout manage-t)
  (doto manage-t
    (.setWidth manage-t-width)
    (.setHeight manage-t-hight))
  (doseq [con manage-t-headers]
    (.addContainerProperty manage-t (i18n (first con)) (second con) nil)
    (.setColumnAlignment manage-t (i18n (first con)) (Table/ALIGN_LEFT)))
  (doseq [con manage-t-header-size]
    (.setColumnWidth manage-t (first con) (second con)))
  (fill-table manage-t paging-layout window panel))

(defn create-manage-ad [body-v window]
  (let [manage-panel (new-panel "manage.panel" "manage-panel") h-layout-1 (new-h-layout) h-layout-2 (new-h-layout) h-layout-3 (new-h-layout) manage-t (new-table "") h-button (new-h-layout)]
    (.addComponent body-v manage-panel)
    (doto manage-panel
      (.setHeight (Sizeable/SIZE_UNDEFINED) 0)
      (.addComponent h-button)
      (.addComponent h-layout-1)
      (.addComponent h-layout-2))
    (.setSizeUndefined (.getContent manage-panel))
    (.addStyleName manage-t "manage-table")
    (create-search-content h-layout-1 manage-t manage-panel h-layout-3 window h-button)
    (create-manage-table h-layout-2 h-layout-3 manage-t window manage-panel)
    (.addComponent manage-panel h-layout-3)))

(defn create-ad-edit-window [window ad-map paging table panel]
  (let [edit-window (new-sub-window "ad.edit.window") form (new-form) edit-button (new-button "ad.edit.button") name-text (new-text-with-name "create.ad.name")
        content-text (new-text-area-with-name "create.ad.content") cancel-button (new-button "ad.cancel.button")
        h-layout-1 (new-h-layout) h-layout-2 (new-h-layout)]
    (.addWindow window edit-window)
    (doto edit-window
      (.setReadOnly true)
      (.setResizable false)
      (.setModal true)
      (.setHeight "255px")
      (.setWidth "550px")
      (.addComponent h-layout-1)
      (.addComponent h-layout-2))
    (doto h-layout-1
      (.setSpacing true)
      (.addComponent form))
    (doto form
      (.addStyleName "manage-form-add")
      (.addField "ad-name" name-text)
      (.addField "ad-content" content-text))
    (doto h-layout-2
      (.setSpacing true)
      (.addComponent edit-button)
      (.addComponent cancel-button)
      (.addStyleName "manage-edit-add-h"))
    (doto name-text
      (.setWidth "250px")
      (.setValue (get ad-map :_id)))
    (doto content-text
      (.setWidth "250px")
      (.setValue (get ad-map :message))
      (.setMaxLength (Integer/parseInt (i18n "create.add.conten.max.length"))))
    (.addListener edit-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                (buttonClick [event]
                                  (update-ad ad-map (.getValue name-text) (.getValue content-text))
                                  (log "admin-event" :info nil (str (.getValue name-text) "|" (.getValue content-text)))
                                  (if (not= (get ad-map :_id) (.getValue name-text))
                                    (delete-app-ad (get ad-map :_id)))
                                  (.removeWindow window edit-window)
                                  (fill-table table paging window panel))))
    (.addListener cancel-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                  (buttonClick [event]
                                    (.removeWindow window edit-window))))))

(defn create-ad-delete-window [window ad-map table paging panel]
  (let [del-window (new-sub-window "ad.del.window") ad-name (new-label "") del-button (new-button "ad.del.button") h-layout-1 (new-h-layout) h-layout-2 (new-h-layout)
        h-layout-3 (new-h-layout) cancel-button (new-button "ad.cancel.button") v (new-v-layout) h-4 (new-h-layout)]
    (.addWindow window del-window)
    (doto del-window
      (.setReadOnly true)
      (.setResizable false)
      (.setModal true)
      (.setHeight "235px")
      (.setWidth "520px")
      (.addComponent h-layout-1)
      (.addComponent h-layout-2)
      (.addComponent h-4)
      (.addComponent h-layout-3))
    (doto h-layout-1
      (.addStyleName "manage-add-delete-h-1")
      (.setSpacing true)
      (.addComponent (new-label (str (i18n "del.ad.msg") " " (get ad-map :_id) " ?"))))
    (doto h-layout-2
      (.addStyleName "manage-add-delete-h-2")
      (.setSpacing true)
      (.addComponent (new-label "del.ad.remove.msg"))
      (.setVisible false))
    (doto h-layout-3
      (.addStyleName "manage-add-delete-h-3")
      (.setSpacing true)
      (.addComponent del-button)
      (.addComponent cancel-button))
    (doto h-4
      (.addStyleName "manage-add-delete-h-4")
      (.setSpacing true)
      (.addComponent v))
    (doseq [con (get-apps-by-ad-name (get ad-map :_id))]
      (.setVisible h-layout-2 true)
      (.addComponent v (new-label (get con :_id))))
    (.addListener cancel-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                  (buttonClick [event]
                                    (.removeWindow window del-window))))
    (.addListener del-button (proxy [com.vaadin.ui.Button$ClickListener] []
                               (buttonClick [event]
                                 (delete-add (get ad-map :_id))
                                 (delete-app-ad (get ad-map :_id))
                                 (.removeWindow window del-window)
                                 (fill-table table paging window panel))))))

(defn create-button [window ad-map table paging panel]
  (let [h (new-grid-layout 2 1) edit-button (new-link-button "manage.edit.button") delete-button (new-link-button "manage.delete.button")]
    (doto h
      (.addStyleName "manage-button-h")
      (.setSpacing true)
      (.addComponent edit-button 0 0)
      (.addComponent delete-button 1 0))
    (.addListener edit-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                (buttonClick [event]
                                  (create-ad-edit-window window ad-map paging table panel))))
    (.addListener delete-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                  (buttonClick [event]
                                    (create-ad-delete-window window ad-map table paging panel))))
    h))
