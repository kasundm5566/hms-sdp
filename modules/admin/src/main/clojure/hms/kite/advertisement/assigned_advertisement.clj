(ns hms.kite.advertisement.assigned-advertisement
  (:import (com.vaadin.terminal Sizeable UserError))
  (:import (com.vaadin.ui Button GridLayout))
  (:import (hms.kite.paging TablePagingComponent))
  (:use hms.kite.admin.vaadin)
  (:use hms.kite.repo.advertisement-repo))

(declare fill-manage-ad-table create-buttons create-search-component create-assign-component)

(def manage-table-headers
  [["manage.table.application" String] ["manage.table.adname" String] ["manage.table.button" GridLayout]])

(def manage-table-header-sizes
  [["manage.table.application" 200] ["manage.table.adname" 400] ["manage.table.button" 150]])

(def combo-names
  [["combo.appname" "assigned.combo.appname"] ["combo.adname" "assigned.combo.adname"]])

(def manage-table-width "690px")
(def manage-table-height "400px")
(def panel-width "858px")

(def change-height "200px")

(defn display-error-change-message [window msg]
  (.showNotification window (i18n msg) (com.vaadin.ui.Window$Notification/TYPE_HUMANIZED_MESSAGE)))

(defn create-manage-ad-table [body-v urifu roles-list window]
  (.removeAllComponents body-v)
  (let [manage-ad-grid (new-grid-layout 3 3) manage-panel (new-panel "panel.ad.assigned") h-layout (new-h-layout) manage-table (new-table "")
        paging-h (new-h-layout) h-layout-assign-ad (new-h-layout) assign-button (new-link-button "ad.assign.ad.button")]
    (.addComponent body-v manage-ad-grid)
    (doto manage-ad-grid
      (.addStyleName "manage-grid")
      (.setSpacing true)
      (.addComponent manage-panel 0 0 2 2))
    (doto manage-panel
      (.addStyleName "advertisement-manage-panel")
      (.setHeight (Sizeable/SIZE_UNDEFINED) 0)
      (.addComponent h-layout-assign-ad)
      (.addComponent h-layout)
      (.addComponent manage-table)
      (.addComponent paging-h))
    (.setSizeUndefined (.getContent manage-panel))
    (doto h-layout-assign-ad
      (.setSpacing true)
      (.addStyleName "h-layout-assign-ad")
      (.addComponent assign-button))
    (.addListener assign-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                  (buttonClick [event]
                                    (create-assign-component body-v urifu roles-list window)

                                    )))
    (doto manage-table
      (.addStyleName "assigned-table")
      (.setWidth manage-table-width)
      (.setHeight manage-table-height))
    (.setSpacing h-layout true)
    (create-search-component h-layout manage-table paging-h manage-panel window roles-list urifu body-v)
    (doseq [con manage-table-headers]
      (.addContainerProperty manage-table (i18n (first con)) (second con) nil))
    (doseq [con manage-table-header-sizes]
      (.setColumnWidth manage-table (first con) (second con)))
    (fill-manage-ad-table manage-table paging-h window manage-panel body-v urifu roles-list)))

(defn reload-assigned-ad-table [list table window panel body-v urifu roles-list]
  (doseq [con list]
    (doseq [app (get-app-info-by-app-id (get con :_id))]
      (.addItem table (into-array Object [(get app :name) (get con :advertisement-name) (create-buttons window app con table panel body-v urifu roles-list)]) (get con :_id)))))

(defn summery-assigned-listener-fn [table window panel body-v urifu roles-list]
  (proxy [hms.kite.paging.PagingComponentListener] []
    (onReload [start batch]
      (.removeAllItems table)
      (reload-assigned-ad-table (get-all-app-advertisement start batch) table window panel body-v urifu roles-list))))

(defn page-summery-assigned [total rec-per-page table window panel body-v urifu roles-list]
  (let [table-pag (TablePagingComponent. (summery-assigned-listener-fn table window panel body-v urifu roles-list) total rec-per-page)]
    (.init table-pag)
    table-pag))

(defn fill-manage-ad-table [table paging window panel body-v urifu roles-list]
  (.addComponent paging (page-summery-assigned (get-total-number-of-app-ad) 10 table window panel body-v urifu roles-list)))


(defn display-sucessfull-msg [window]
  (.showNotification window (i18n "sucess.chage.combo") (com.vaadin.ui.Window$Notification/TYPE_HUMANIZED_MESSAGE)))

(defn create-map [id ad-name]
  {:_id id :advertisement-name ad-name})

(defn create-change-advertisement [app-name panel app-id window ad-name body-v urifu roles-list]
  (let [change-window (new-sub-window "ad.change.panel.name") h-layout-1 (new-h-layout) h-layout-2 (new-h-layout) h-layout-3 (new-h-layout)
        ad-drop (new-combo-box " ") change-button (new-button "manage.change.submit.button") app-id-text (new-text-field) cancel-button (new-button "child.window.button.cancel")]
    (.addWindow window change-window)
    (doto change-window
      (.setReadOnly true)
      (.setModal true)
      (.setHeight "250px")
      (.setWidth "500px")
      (.setResizable false)
      (.addComponent h-layout-1)
      (.addComponent h-layout-2)
      (.addComponent h-layout-3))
    (doto h-layout-1
      (.setSpacing true)
      (.addStyleName "add-change-h-layout-1")
      (.addComponent (new-label "manage.change.label.app"))
      (.addComponent app-id-text))
    (doto h-layout-2
      (.setSpacing true)
      (.addStyleName "add-change-h-layout-2")
      (.addComponent (new-label "manage.change.label.ad"))
      (.addComponent ad-drop))
    (doto h-layout-3
      (.setSpacing true)
      (.addStyleName "add-change-h-layout-3")
      (.addComponent change-button)
      (.addComponent cancel-button))
    (.setValue app-id-text app-name)
    (.setReadOnly app-id-text true)
    (doto ad-drop
      (.addStyleName "ad-drop-combo")
      (.setImmediate true)
      (.setNullSelectionAllowed false))
    (doseq [con (fetch-all-ad 0 0)]
      (.addItem ad-drop (get con :_id)))
    (.setValue ad-drop ad-name)
    (.addListener change-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                  (buttonClick [event]
                                    (update-ad-app-change (create-map app-id (.getValue ad-drop)))
                                    (create-manage-ad-table body-v urifu roles-list window)
                                    (.removeWindow window change-window))))
    (.addListener cancel-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                  (buttonClick [event]
                                    (.removeWindow window change-window))))))

(defn create-remove-advertisement [window app-map ad-map table]
  (let [remove-window (new-sub-window "ad.remove.window") del-button (new-button "ad.del.button") cancel-button (new-button "ad.cancel.button")
        h (new-h-layout) h-layout-1 (new-h-layout) h-layout-2 (new-h-layout)]
    (.addWindow window remove-window)
    (.addStyleName del-button "add-del-button")
    (doto remove-window
      (.setReadOnly true)
      (.setModal true)
      (.setHeight "200px")
      (.setWidth "565px")
      (.setResizable false)
      (.addComponent h-layout-1)
      (.addComponent h-layout-2))
    (doto h-layout-1
      (.addStyleName "remove-h-layout")
      (.setSpacing true)
      (.addComponent (new-label (str (i18n "ad.remove.msg") " " (get ad-map :advertisement-name) " " (i18n "ad.to") " " (get app-map :name) " ?"))))
    (doto h-layout-2
      (.addStyleName "remove-h-layout-2")
      (.setSpacing true)
      (.addComponent del-button)
      (.addComponent cancel-button))
    (.addListener cancel-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                  (buttonClick [event]
                                    (.removeWindow window remove-window))))
    (.addListener del-button (proxy [com.vaadin.ui.Button$ClickListener] []
                               (buttonClick [event]
                                 (.removeItem table (get ad-map :_id))
                                 (delete-app-add-by-app-id (get ad-map :_id))
                                 (.removeWindow window remove-window))))))

(defn create-buttons [window app-map ad-map table panel body-v urifu roles-list]
  (let [grid (new-grid-layout 2 1) change-button (new-link-button "assigned.change.button") remove-button (new-link-button "assigned.remove.button")]
    (doto grid
      (.addStyleName "manage-button-h")
      (.setSpacing true)
      (.addComponent change-button 0 0)
      (.addComponent remove-button 1 0))
    (.addListener change-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                  (buttonClick [event]
                                    (create-change-advertisement (get app-map :name) panel (get app-map :app-id) window (get ad-map :advertisement-name) body-v urifu roles-list))))
    (.addListener remove-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                  (buttonClick [event]
                                    (create-remove-advertisement window app-map ad-map table))))
    grid))

(defn fill-assigned-table-by-app-name [window table app-name panel body-v urifu roles-list]
  (let [search-pattern (re-pattern (str "(?i)" app-name))]
    (doseq [con (get-app-info-by-app-name search-pattern)]
      (doseq [add (get-app-ad-by-app-id (get con :app-id))]
        (.addItem table (into-array Object [(get con :name) (get add :advertisement-name) (create-buttons window con add table panel body-v urifu roles-list)]) (get con :_id))))))

(defn fill-assigned-table-by-ad-name [window table ad-name panel body-v urifu roles-list]
  (let [search-pattern (re-pattern (str "(?i)" ad-name))]
    (doseq [con (get-apps-by-ad-name search-pattern)]
      (doseq [app (get-app-info-by-app-id (get con :_id))]
        (.addItem table (into-array Object [(get app :name) (get con :advertisement-name) (create-buttons window app con table panel body-v urifu roles-list)]) (get app :_id))))))

(defn create-search-component [h-layout table paging-h panel window roles-list urifu body-v]
  (let [form (new-form) text (new-text-field) search-button (new-small-button "manage.search.button") h (new-h-layout) combo (new-combo-box "")]
    (.addComponent h-layout form)
    (doto combo
      (.addStyleName "assigned-combo")
      (.setImmediate true)
      (.setNullSelectionAllowed false)
      (.setWidth "190px"))
    (.addStyleName search-button "assigned-button")
    (doto form
      (.setLayout h)
      (.addStyleName "assigned-ad-form"))
    (doto h
      (.setSpacing true)
      (.addComponent (new-label "subscription.search.label"))
      (.addComponent combo)
      (.addComponent text)
      (.addComponent search-button))
    (doseq [con combo-names]
      (.addItem combo (i18n (second con))))
    (.setValue combo (i18n "assigned.combo.appname"))
    (.addListener search-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                  (buttonClick [event]
                                    (.removeAllItems table)
                                    (.removeComponent panel paging-h)
                                    (if (= (.getValue text) "")
                                      (create-manage-ad-table body-v urifu roles-list window)
                                      (if (= (.getValue combo) (i18n "assigned.combo.appname"))
                                        (do
                                          (fill-assigned-table-by-app-name window table (.trim (.getValue text)) panel body-v urifu roles-list))
                                        (if (= (.getValue combo) (i18n "assigned.combo.adname"))
                                          (do
                                            (fill-assigned-table-by-ad-name window table (.trim (.getValue text)) panel body-v urifu roles-list))))))))))

(defn display-error-msg-ad [window message]
  (.showNotification window (i18n message) (com.vaadin.ui.Window$Notification/TYPE_WARNING_MESSAGE)))

(defn app-ad-map [app-id ad-name]
  {:_id app-id :advertisement-name ad-name})

(defn create-assign-component [body-v urifu roles-list window]
  (let [assign-window (new-sub-window "ad.assign.window") h-layout (new-h-layout) form (new-form) assign-button (new-button "ad.assign.button") h-error (new-h-layout) error-label (new-label "advertisement.error.message")
        app-name-combo (new-combo-with-name "ad.assign.app.combo") ad-name-combo (new-combo-with-name "ad.assign.ad.combo") cancel-button (new-button "ad.cancel.button")]
    (.addWindow window assign-window)
    (doto assign-window
      (.setReadOnly true)
      (.setModal true)
      (.setHeight "230px")
      (.setWidth "470px")
      (.setResizable false)
      (.addComponent form)
      (.addComponent h-layout))
    (doto form
      (.addStyleName "ad-assign-form")
      (.addField "ad-name" ad-name-combo)
      (.addField "app-name" app-name-combo))
    (doto h-layout
      (.setSpacing true)
      (.addStyleName "ad-assign-h-layout")
      (.addComponent assign-button)
      (.addComponent cancel-button))
    (doto error-label
      (.addStyleName "ad-assign-error-label"))
    (doto h-error
      (.setSpacing true)
      (.addStyleName "ad-assign-h-layout-error")
      (.addComponent error-label))
    (doto ad-name-combo
      (.setImmediate true)
      (.setNullSelectionAllowed false))
    (doto app-name-combo
      (.setImmediate true)
      (.setNullSelectionAllowed false))
    (doseq [ad (get-all-add)]
      (.addItem ad-name-combo (get ad :_id))
      (doseq [app (get-all-app)]
        (.addItem app-name-combo (get app :name))
        (.addListener assign-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                      (buttonClick [event]
                                        (if (or (= nil (.getValue app-name-combo)) (= nil (.getValue ad-name-combo)))
                                          (.addComponent assign-window h-error)
                                          (do
                                            (.removeComponent assign-window h-error)
                                            (doseq [con (get-app-info-by-app-name (.getValue app-name-combo))]
                                              (update-ad-app-change (app-ad-map (get con :app-id) (.getValue ad-name-combo))))
                                            (create-manage-ad-table body-v urifu roles-list window)
                                            (.removeWindow window assign-window))))))))
    (.addListener cancel-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                      (buttonClick [event]
                                        (.removeWindow window assign-window))))))
