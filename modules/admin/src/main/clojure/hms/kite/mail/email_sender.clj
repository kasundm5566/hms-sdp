(ns hms.kite.mail.email-sender
  (:use hms.kite.governance.read-property)
  (:use clojure.tools.logging)
  (:use hms.kite.admin.vaadin)
  (:import (org.apache.commons.mail SimpleEmail))
  (:import (hms.commons SnmpLogUtil)))

(def ^{:private true} host (load-property "mail.sender.host"))
(def ^{:private true} port (load-property "mail.sender.port"))
(def ^{:private true} username (load-property "mail.sender.username"))
(def ^{:private true} password (load-property "mail.sender.password"))
(def ^{:private true} auth (load-property "mail.smtp.auth"))
(def ^{:private true} tls-enabled (load-property "mail.smtp.starttls.enable"))
(def ^{:private true} from (load-property "gov.email.send.from.address"))

(defonce email-connection "email-connection")

(defn- email-connection-success []
  (log "hms.kite" :info nil ( str "Sending Email sending success snmp notification"))
  (SnmpLogUtil/clearTrap email-connection (str (load-property "snmp.email.sent.successed")) (into-array String ["bar" "baz"]))
  1)

(defn- email-connection-failed[]
  (log "hms.kite" :info nil ( str "Sending Email sending failed snmp notification"))
  (SnmpLogUtil/trap email-connection (str (load-property "snmp.email.sent.failed")) (into-array String ["bar" "baz"]))
  -1)

(defn print-email-send-failed-logs[to subject message & cc]
  (log "email_failed_logger" :info nil (str "\n"))
  (log "email_failed_logger" :info nil (str "to:" to))
  (log "email_failed_logger" :info nil (str "from:"from))
  (log "email_failed_logger" :info nil (str "cc:" (if (or (nil? (first cc)) (empty? (first cc))) "-" (clojure.string/split (first cc) #","))))
  (log "email_failed_logger" :info nil (str "bcc:-"))
  (log "email_failed_logger" :info nil (str "subject:" subject))
  (log "email_failed_logger" :info nil (str "content:" message))
  (log "email_failed_logger" :info nil (str "\n")))

(defn- get-simple-email []
(log "hms.kite" :info nil (str "Mail sending property values host [" host "], port [" port "], username [" username "],
                            password [" password "], auth? [" auth "], TLS enabled? [" tls-enabled "], from [" from "]."))
  (let [mail (SimpleEmail.)]
      (.setHostName mail host)
      (.setSslSmtpPort mail port)
      (.setFrom mail from)
      (if (= auth "true")
        (.setAuthentication mail username password))
      (.setTLS mail (= tls-enabled "true"))
    mail))

(defn send-mail
  ([to subject message cc-list]
  (log :info (str "Sending email with cc list to : " to ", subject : " subject ", message : " message ", cc-list : " cc-list))
  (if (or (nil? cc-list) (empty? cc-list))
    (send-mail to subject message)
    (let [mail (cast SimpleEmail (get-simple-email ))]
      (doseq [c (into-array (clojure.string/split cc-list #","))]
        (.addCc mail c))
      (doto mail
        (.addTo to)
        (.setSubject subject)
        (.setMsg message))
        (try (.send mail)
          (email-connection-success)
          (catch Exception e (print-email-send-failed-logs to subject message cc-list)
                            (log "hms.kite" :error nil (str "An Email sending exception thrown " e))
                            (email-connection-failed)))      )))
  ([to subject message]
  (log :info (str "Sending email without cc list " to ", " subject ", " message))
  (let [mail (cast SimpleEmail (get-simple-email))]
    (doto mail
      (.addTo to)
      (.setSubject subject)
      (.setMsg message))
      (try (.send mail)
        (email-connection-success)
        (catch Exception e (print-email-send-failed-logs to subject message) (email-connection-failed))))))
