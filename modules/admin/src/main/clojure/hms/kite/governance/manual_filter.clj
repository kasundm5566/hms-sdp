(ns hms.kite.governance.manual-filter
  (:use hms.kite.admin.vaadin)
  (:use hms.kite.repo.manual-filter-repo)
  (:use hms.kite.repo.advertisement-repo)
  (:use clojure.tools.logging)
  (:use hms.kite.user.create-user)
  (:use hms.kite.mail.email-sender)
  (:import (com.vaadin.ui GridLayout CheckBox Table))
  (:import (com.vaadin.terminal Sizeable))
  (:import (java.util Date))
  (:import (hms.kite.paging TablePagingComponent))
  (:import (java.text SimpleDateFormat))
  (:import (java.util Calendar)))

(declare create-child-window create-sub-window-components-manual create-email-sub-window-manual)

(def manual-filter-table-headers
  [["table.application" String] ["table.msg" String] ["table.time" String] ["table.no" Integer]
   ["table.suspend" GridLayout]])

(def manual-filter-table-header-sizes
  [["table.application" 70] ["table.msg" 250] ["table.time" 70] ["table.no" 10] ["table.suspend" 120]])

(def manual-filter-table-width "690px")
(def manual-filter-table-height "400px")

(defn create-allow-reject-button [role-list window msg-msp table username paging-h]
  (let [child-win (new-sub-window "child.confirm.window") grid (new-grid-layout 2 1) allow-button (new-link-button "allow") rej-button (new-link-button "reject")]
    (doto grid
      (.setSpacing true)
      (.addStyleName "allow-reject-grid"))
    (if (.contains role-list "ROLE_ADM_ALLOW_MT")
      (.addComponent grid allow-button 0 0))
    (if (.contains role-list "ROLE_ADM_REJECT_MT")
      (.addComponent grid rej-button 1 0))
    (.addListener allow-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                 (buttonClick [event]
                                   (create-child-window child-win msg-msp window table "ALLOWED" username role-list paging-h))))
    (.addListener rej-button (proxy [com.vaadin.ui.Button$ClickListener] []
                               (buttonClick [event]
                                 (create-child-window child-win msg-msp window table "REJECTED" username role-list paging-h))))
    grid))

(defn reload-manual-ad-table [list table window role-list username paging-h]
  (log :info (str "reloading manual ad table with " list))
  (doseq [msg list]
    (log :info (str "Retrieved data : " msg))
    (doseq [app (get-app-info-by-app-id (get msg :app-id))]
      (.addItem table (into-array Object [(get app :name) (get msg :message) (.toString (Date. (Long/parseLong (get msg :receiveTime)))) (get msg :recipient-count)
                                          (create-allow-reject-button role-list window msg table username paging-h)])
        (get msg :_id)))))

(defn summery-manual-listener-fn [table window role-list username paging-h]
  (proxy [hms.kite.paging.PagingComponentListener] []
    (onReload [start batch]
      (.removeAllItems table)
      (reload-manual-ad-table (fetch-all-manual-filter-message start batch) table window role-list username paging-h))))

(defn page-summery-manual [total rec-per-page table window role-list username paging-h]
  (let [table-pag (TablePagingComponent. (summery-manual-listener-fn table window role-list username paging-h) total rec-per-page)]
    (.init table-pag)
    table-pag))

(defn fill-filter-table [window role-list table paging-h username]
  (.removeAllComponents paging-h)
  (.addComponent paging-h (page-summery-manual (get-total-number-of-manual-filter-message) 10 table window role-list username paging-h)))

(defn create-manual-filter-table [body-v window role-list username]
  (log :info (str "creating manula filter table with role [" role-list "], username : " username))
  (let [filter-grid (new-grid-layout 3 2) filter-panel (new-panel "panel.manual.filter" "manual-filter-panel") filter-table (new-table "") paging-h (new-h-layout)]
    (.setSpacing filter-grid true)
    (.addComponent body-v filter-grid)
    (.addComponent filter-grid filter-panel 0 1 2 1)
    (doto filter-panel
      (.addComponent filter-table)
      (.addComponent paging-h))
    (.setSizeUndefined (.getContent filter-panel))
    (doto filter-table
      (.addStyleName "manual-filter-table")
      (.setWidth manual-filter-table-width)
      (.setHeight manual-filter-table-height)
      (.setPageLength 10))
    (.addStyleName paging-h "abuse-paging")
    (doseq [con manual-filter-table-headers]
      (.addContainerProperty filter-table (i18n (first con)) (second con) nil))
    (doseq [con manual-filter-table-header-sizes]
      (.setColumnWidth filter-table (first con) (second con)))
    (.setColumnAlignment filter-table (i18n "table.no") (Table/ALIGN_RIGHT))
    (fill-filter-table window role-list filter-table paging-h username)))

(defn date-now
  ([]
    (.format (SimpleDateFormat. "yyyy-MM-dd hh:mm:ss") (.getTime (Calendar/getInstance))))
  ([time-in-milli]
    (let [cal (Calendar/getInstance)]
      (.setTimeInMillis cal time-in-milli)
      (.format (SimpleDateFormat. "yyyy-MM-dd hh:mm:ss") (.getTime cal)))))

(defn create-manual-filter-report [status ms-map username]
  (doseq [con (get-app-info-by-app-id (get ms-map :app-id))]
    (if (= status "ALLOWED")
      (log "admin-manual-filter" :info nil (str (get ms-map :app-id) "|" (get con :name) "|" (get ms-map :message) "|"
                                             (date-now (Long/parseLong (get ms-map :receiveTime))) "|" "APPROVED" "|" (date-now) "|" username "|" (get ms-map :recipient-count)))
      (log "admin-manual-filter" :info nil (str (get ms-map :app-id) "|" (get con :name) "|" (get ms-map :message) "|"
                                             (date-now (Long/parseLong (get ms-map :receiveTime))) "|" "REJECTED" "|" (date-now) "|" username "|" (get ms-map :recipient-count))))))

(defn create-child-window [child-win msg-map window table status username role-list paging-h]
  (.addWindow window child-win)
  (.removeAllComponents child-win)
  (doto child-win
    (.setResizable false)
    (.setReadOnly true)
    (.setHeight "150px")
    (.setWidth "265px")
    (.setModal true))
  (let [ok (new-button "child.window.button.ok") cancel (new-button "child.window.button.cancel") h-layout-1 (new-h-layout)
        h-layout-2 (new-h-layout)]
    (doto child-win
      (.addComponent h-layout-1)
      (.addComponent h-layout-2))
    (doto h-layout-1
      (.addStyleName "manual-filter-message-layout-1")
      (.setSpacing true))
    (if (= status "ALLOWED")
      (.addComponent h-layout-1 (new-label "child.window.allow.label"))
      (.addComponent h-layout-1 (new-label "child.window.reject.label")))
    (doto h-layout-2
      (.addStyleName "manual-filter-message-layout-2")
      (.setSpacing true)
      (.addComponent ok)
      (.addComponent cancel))
    (.addListener ok (proxy [com.vaadin.ui.Button$ClickListener] []
                       (buttonClick [event]
                         (if (= status "REJECTED")
                             (create-email-sub-window-manual window (get (first (get-app-info-by-app-id (get msg-map :app-id))) :name) (get msg-map :coop-user-id) msg-map username role-list paging-h table)
                           (do
                             (update-manual-filter-message msg-map "ALLOWED")
                             (create-manual-filter-report "ALLOWED" msg-map username)
                             (fill-filter-table window role-list table paging-h username)))
                         (.removeWindow window child-win))))
    (.addListener cancel (proxy [com.vaadin.ui.Button$ClickListener] []
                           (buttonClick [event]
                             (.removeWindow window child-win))))))

(defn create-readonly [type value]
  (.setWidth type "400px")
  (.setValue type value)
  (.setReadOnly type true))

(defn create-email-window-manual [window app-name coop-id sp-email sp-first-name msg-map username role-list paging-h table]
  (log :info (str "creating email window manual with appname : " app-name ", sp-email : " sp-first-name ", username : " username))
  (let [email-window (new-sub-window "email.sub.window")]
    (.addWindow window email-window)
    (doto email-window
      (.setReadOnly true)
      (.setResizable false)
      (.setHeight "350px")
      (.setWidth "650px")
      (.setModal true))
    (create-sub-window-components-manual window email-window app-name sp-email sp-first-name msg-map username role-list paging-h table)))

(defn display-error-notification-manual [window message]
  (.showNotification window (i18n message) (com.vaadin.ui.Window$Notification/TYPE_ERROR_MESSAGE)))

(defn create-email-sub-window-manual [window app-name coop-id msg-map username role-list paging-h table]
  (let [coop-user (user-coop-id coop-id)]
    (if (not (or (nil? coop-user) (empty? coop-user)))
      (create-email-window-manual window app-name coop-id (get coop-user :email) (get coop-user :username) msg-map username role-list paging-h table)
      (display-error-notification-manual window "sp.not.found"))))

(defn create-email-sucessfull-window-component-manual [window email mail-status]
  (log "hms.kite" :info nil (str "Received mail status " mail-status))
  (let [sucessfull-window (new-sub-window "email.sub.sucessfull.window")]
    (.addWindow window sucessfull-window)
    (doto sucessfull-window
      (.setModal true)
      (.setReadOnly true)
      (.setHeight "350px")
      (.setWidth "650px"))
    (let [grid (new-grid-layout 1 2)]
      (.setSpacing grid true)
      (.addComponent sucessfull-window grid)
      (.addStyleName grid "sucessfull-grid")
      (let [h (new-h-layout)]
        (.addComponent grid h 0 0)
        (.addStyleName h "sucessfull-window-h")
        (.addComponent h (new-label (if (> mail-status 0) (str (i18n "message.successfull") " " email " " (i18n "success")) (str (i18n "internal.error"))))))
      (let [ok-button (new-button "ok.button")]
        (.addComponent grid ok-button 0 1)
        (.addListener ok-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                  (buttonClick [event]
                                    (.removeWindow window sucessfull-window))))))))

(defn create-sub-window-components-manual [window email-window app-name sp-email sp-first-name msg-map username role-list paging-h table]
  (log :info (str "Creating abuse report table with appname : " app-name ", ep-email : " sp-email ", sp-first-name : " sp-first-name ", username : " username))
  (let [email-grid (new-grid-layout 2 6) h-layout (new-h-layout) label (new-label (str (i18n "email.header") " " sp-first-name))
        to-text-field (new-text-field) cc-text-field (new-text-field) subject-text-field (new-text-field) msg-textarea (new-text-area)
        send-button (new-button "send") close-button (new-button "close")]
    (.addComponent email-window email-grid)
    (doto email-grid
      (.addStyleName "email-main-grid")
      (.setSpacing true)
      (.addComponent h-layout 0 0 1 0)
      (.addComponent (new-label "email.to") 0 1)
      (.addComponent to-text-field 1 1)
      (.addComponent (new-label "email.cc") 0 2)
      (.addComponent cc-text-field 1 2)
      (.addComponent (new-label "email.subject") 0 3)
      (.addComponent subject-text-field 1 3)
      (.addComponent (new-label "message") 0 4)
      (.addComponent msg-textarea 1 4)
      (.addComponent send-button 0 5)
      (.addComponent close-button 1 5))
    (.addComponent h-layout label)
    (.addStyleName label "email-label")
    (create-readonly to-text-field sp-email)
    (.setWidth cc-text-field "400px")
    (.setValue subject-text-field (str (i18n "subject.text.manual.filter") " " app-name))
    (.setReadOnly subject-text-field true)
    (.setWidth subject-text-field "400px")
    (.setWidth msg-textarea "400px")
    (.addListener send-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                (buttonClick [event]
                                  (if (= "" (.getValue msg-textarea))
                                    (display-error-notification-manual window "error.email.content")
                                    (do
                                      (let [mail-status (send-mail (.getValue to-text-field) (.getValue subject-text-field) (.getValue msg-textarea) (.getValue cc-text-field))]
                                      (.removeWindow window email-window)
                                      (update-manual-filter-message msg-map "REJECTED")
                                      (fill-filter-table window role-list table paging-h username)
                                      (create-manual-filter-report "REJECTED" msg-map username)
                                      (create-email-sucessfull-window-component-manual window sp-email mail-status)))))))
    (.addListener close-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                 (buttonClick [event]
                                   (.removeAllComponents email-grid)
                                   (.removeWindow window email-window))))))
