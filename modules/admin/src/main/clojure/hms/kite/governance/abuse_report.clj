(ns hms.kite.governance.abuse-report
  (:use hms.kite.admin.vaadin)
  (:use hms.kite.repo.governance-repo)
  (:use clojure.tools.logging)
  (:use hms.kite.repo.message-history-repo)
  (:use hms.kite.user.create-user)
  (:use hms.kite.mail.email-sender)
  (:use hms.kite.api.provision)
  (:use hms.kite.governance.read-property)
  (:import (com.vaadin.ui HorizontalLayout Button CheckBox Window Table))
  (:import (com.vaadin.terminal ExternalResource Sizeable))
  (:import (java.util HashMap ArrayList))
  (:import (hms.kite.paging TablePagingComponent)))

(declare create-abuse-report create-detail-app-info)

(def app-list (ArrayList.))

(def prov-error-codes {"E1302" "sp.not.found"})

(def abuse-report-table-headers
  [["table.application" Button] ["table.sp" String] ["table.pending" Integer] ["table.valid.reports" Integer] ["table.invalid.reports" Integer]
   ["table.reported" Integer] ["table.sub.info" HorizontalLayout]])

(def abuse-report-table-heder-sizes
  [["table.application" 100] ["table.sp" 100] ["table.pending" 50] ["table.valid.reports" 50] ["table.invalid.reports" 50] ["table.reported" 50]
   ["table.sub" 250]])

(def app-info-table-headers
  [["table.sub" CheckBox] ["table.message" String] ["table.date" String] ["table.medium" String] ["table.reported.user" String]])

(def app-info-table-header-size
  [["table.sub" 10] ["table.message" 640] ["table.date" 50] ["table.medium" 50] ["table.reported.user" 50]])

(def message-table-headers
  [["message.history" String] ["message.date" String]])

(def message-table-header-sizes
  [["message.history" 790] ["message.date" 60]])

(def abuse-report-table-width "690px")
(def abuse-report-table-hight "400px")

(def app-info-table-width "690px")
(def app-info-table-height "170px")

(def email-component-width "400px")

(def abuse-report-status "report_status")

(def abuse-report-status-valid "VALID")
(def abuse-report-status-invalid "INVALID")

(def message-table-width "690px")
(def message-table-height "150px")

(def string-template {:email ""})

(def suspend-sp-url (load-property "suspend.sp.url"))
(def suspend-app-url (load-property "suspend.app.url"))

(defn checkbox-listener [event app-detail]
  (let [enable (.booleanValue (.getButton event))]
    (if enable
      (.add app-list app-detail)
      (.remove app-list app-detail))))

(defn reload-message-history [message-list message-table]
  (.removeAllItems message-table)
  (doseq [msg-map message-list]
    (.addItem message-table (into-array Object [(get msg-map :message) (.toString (get msg-map :receive_date))])
      (get msg-map :correlation_id))))

(defn message-listener-fn [message-table role-list application-id]
  (proxy [hms.kite.paging.PagingComponentListener] []
    (onReload [start batch]
      (reload-message-history (fetch-recs-by-app-id application-id start batch) message-table))))

(defn message-history-page [total rec-per-page message-table role-list application-id]
  (let [table-pag (TablePagingComponent. (message-listener-fn message-table role-list application-id) total rec-per-page)]
    (.init table-pag)
    table-pag))

(defn create-message-history-table [grid application-id role-list app-info-panel]
  (let [message-table (new-table "")]
    (.addComponent grid message-table 0 4 1 5)
    (.addStyleName message-table "message-table")
    (.setWidth message-table message-table-width)
    (.setHeight message-table message-table-height)
    (.setPageLength message-table 3)
    (doseq [con message-table-headers]
      (.addContainerProperty message-table (i18n (first con)) (second con) nil))
    (doseq [con message-table-header-sizes]
      (.setColumnWidth message-table (first con) (second con)))
    (.addComponent app-info-panel (message-history-page (fetch-count-by-app-id application-id) 3 message-table role-list application-id))))

(defn create-checkbox [app-detail]
  (let [checkbox (new-checkbox "table.sub")]
    (.setImmediate checkbox true)
    (.addListener checkbox (proxy [com.vaadin.ui.Button$ClickListener] []
                             (buttonClick [event]
                               (checkbox-listener event app-detail))))
    checkbox))

(defn create-appinfo-table [app-info-grid application-id role-list app-info-panel]
  (let [grid (new-grid-layout 2 7) app-table (new-table "")]
    (.setSpacing grid true)
    (.addComponent app-info-panel grid)
    (.addComponent grid app-table 0 1 1 2)
    (doto app-table
      (.addStyleName "app-info-table")
      (.setWidth app-info-table-width)
      (.setHeight app-info-table-height)
      (.setPageLength 4))
    (doseq [con app-info-table-headers]
      (.addContainerProperty app-table (i18n (first con)) (second con) nil))
    (doseq [con app-info-table-header-size]
      (.setColumnWidth app-table (first con) (second con)))
    (.setColumnAlignment app-table (i18n "table.reported.user") (Table/ALIGN_RIGHT))
    (doseq [app-detail (find-abuse-report-by-app-id application-id "PENDING")]
      (.setCaption app-info-panel (str (i18n "app.info.panel.name") " " (get app-detail :name)))
      (.addItem app-table (into-array Object [(create-checkbox app-detail) (get app-detail :message) (.toString (get app-detail :received_time))
                                              (get app-detail :ncs-type) (get app-detail :sender-address)]) (get app-detail :_id)))
    (if (.contains role-list "ROLE_ADM_MSG_HISTORY_VIEW")
      (create-message-history-table grid application-id role-list app-info-panel))))

(defn create-back-button [app-info-grid window body-v urifu]
  (let [back-button (new-button "back")]
    (.addComponent app-info-grid back-button 0 2)
    (.addListener back-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                (buttonClick [event]
                                  (.removeAll app-list app-list)
                                  (.setFragment urifu "abusereport"))))))

(defn update-count [app-id status]
  (doseq [con (find-application-info-by-appid app-id)]
    (def pending (get con :pending))
    (def valid (get con :valid))
    (def invalid (get con :invalid))
    (if (= status "VALID")
      (update-abuse-report-count con (- pending (.size app-list)) (+ valid (.size app-list)) status)
      (update-abuse-report-count con (- pending (.size app-list)) (+ invalid (.size app-list)) status))))

(defn display-error-notification [window message]
  (.showNotification window (i18n message) (com.vaadin.ui.Window$Notification/TYPE_ERROR_MESSAGE)))

(defn valid-button-listner [window body-v application-id urifu role-list]
  (if (.isEmpty app-list)
    (display-error-notification window "error.valid.invalid")
    (do
      (doseq [app app-list]
        (update-abuse-repot-status "VALID" app))
      (update-count application-id "VALID")
      (.removeAll app-list app-list)
      (.removeAllComponents body-v)
      (create-detail-app-info window body-v application-id urifu role-list))))

(defn invalid-button-listner [window body-v application-id urifu role-list]
  (if (.isEmpty app-list)
    (display-error-notification window "error.valid.invalid")
    (do
      (doseq [app app-list]
        (update-abuse-repot-status "INVALID" app))
      (update-count application-id "INVALID")
      (.removeAll app-list app-list)
      (.removeAllComponents body-v)
      (create-detail-app-info window body-v application-id urifu role-list))))

(defn create-appinfo-buttons [app-info-panel window body-v application-id urifu role-list]
  (let [sub-grid (new-grid-layout 6 1)]
    (.addStyleName sub-grid "app-info-button-sub-grid")
    (.addComponent app-info-panel sub-grid)
    (.setSpacing sub-grid true)
    (if
      (.contains role-list "ROLE_ADM_MARK_MSG_STATUS")
      (do
        (let [valid-button (new-link-button "valid")]
          (.addComponent sub-grid valid-button 0 0)
          (.addListener valid-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                       (buttonClick [event]
                                         (valid-button-listner window body-v application-id urifu role-list)))))
        (let [invalid-button (new-link-button "invalid")]
          (.addComponent sub-grid invalid-button 2 0)
          (.addListener invalid-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                         (buttonClick [event]
                                           (invalid-button-listner window body-v application-id urifu role-list)))))))))


(defn create-detail-app-info [window body-v application-id urifu role-list]
  (let [app-info-grid (new-grid-layout 2 3) app-info-panel (new-panel (str (i18n "app.info.panel.name") " " (get (find-application-info-by-appid application-id) :name)) "app-info-panel")]
    (.addComponent body-v app-info-grid)
    (.setSizeUndefined (.getContent app-info-panel))
    (.addComponent app-info-grid app-info-panel 0 1 1 1)
    (.setWidth app-info-grid "740px")
    (.addStyleName app-info-grid "app-info-grid")
    (create-appinfo-buttons app-info-panel window body-v application-id urifu role-list)
    (create-appinfo-table app-info-grid application-id role-list app-info-panel)
    (create-back-button app-info-grid window body-v urifu)))

(defn create-email-sucessfull-window-component [window email status]
  (log "hms.kite" :info nil (str "Received mail status " status))
  (let [sucessfull-window (new-sub-window "email.sub.sucessfull.window")]
    (.addWindow window sucessfull-window)
    (doto sucessfull-window
      (.setModal true)
      (.setReadOnly true)
      (.setHeight "350px")
      (.setWidth "650px"))
    (let [grid (new-grid-layout 1 2)]
      (.setSpacing grid true)
      (.addComponent sucessfull-window grid)
      (.addStyleName grid "sucessfull-grid")
      (let [h (new-h-layout)]
        (.addComponent grid h 0 0)
        (.addStyleName h "sucessfull-window-h")
        (.addComponent h (new-label (if (> status 0) (str (i18n "message.successfull") " " email " " (i18n "success")) (str (i18n "internal.error"))))))
      (let [ok-button (new-button "ok.button")]
        (.addComponent grid ok-button 0 1)
        (.addListener ok-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                  (buttonClick [event]
                                    (.removeWindow window sucessfull-window))))))))

(defn create-submission-successfull-window-component [window status-code email sp-first-name app-name stat mail-status]
  (log "hms.kite" :info nil (str "Received mail status " mail-status))
  (let [successfull-window (new-sub-window "email.sub.sucessfull.window")]
    (.addWindow window successfull-window)
    (doto successfull-window
      (.setModal true)
      (.setReadOnly true)
      (.setHeight "350px")
      (.setWidth "650px"))
    (let [grid (new-grid-layout 1 2)]
      (.setSpacing grid true)
      (.addComponent successfull-window grid)
      (.addStyleName grid "sucessfull-grid")
      (let [h (new-h-layout) sucess-text-area (new-text-area) success-msg (str (if (= "suspendsp" stat) sp-first-name app-name ) " " (i18n "sp-app-suspension-message-successful") " " email " " (i18n "success"))
            failed-text-area (new-text-area) failed-msg (str (i18n "internal.error"))]
        (doto sucess-text-area
          (.setValue  success-msg)
          (.setReadOnly true)
          (.setWidth email-component-width))
        (doto failed-text-area
          (.setValue failed-msg)
          (.setReadOnly true)
          (.setWidth email-component-width))
        (.addComponent grid h 0 0)
        (.addStyleName h "sucessfull-window-h")
        (.addComponent h (if (and (= "S1000" status-code) (> mail-status 0)) sucess-text-area failed-text-area)))
      (let [ok-button (new-button "ok.button")]
        (.addComponent grid ok-button 0 1)
        (.addListener ok-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                  (buttonClick [event]
                                    (.removeWindow window successfull-window))))))))

(defn create-readonly-text-fields [type value]
  (.setWidth type email-component-width)
  (.setValue type value)
  (.setReadOnly type true))

(defn create-sub-window-components [window email-window app-name sp-email sp-first-name stat]
  (log :info (str "Creating email popup window for " app-name ", " sp-first-name ", " sp-email))
  (let [email-grid (new-grid-layout 2 6) h-layout (new-h-layout) label (new-label (str (i18n "email.header") " " sp-first-name))
        to-text-field (new-text-field) cc-text-field (new-text-field) subject-text-field (new-text-field) msg-textarea (new-text-area)
        send-button (new-button "send") close-button (new-button "close")]
    (.addComponent email-window email-grid)
    (doto email-grid
      (.addStyleName "email-main-grid")
      (.setSpacing true)
      (.addComponent h-layout 0 0 1 0)
      (.addComponent (new-label "email.to") 0 1)
      (.addComponent to-text-field 1 1)
      (.addComponent (new-label "email.cc") 0 2)
      (.addComponent cc-text-field 1 2)
      (.addComponent (new-label "email.subject") 0 3)
      (.addComponent subject-text-field 1 3)
      (.addComponent (new-label "message") 0 4)
      (.addComponent msg-textarea 1 4)
      (.addComponent send-button 0 5)
      (.addComponent close-button 1 5))
    (.addComponent h-layout label)
    (.addStyleName label "email-label")
    (doto to-text-field
      (.setWidth email-component-width)
      (.setValue sp-email)
      (.setReadOnly true))
    (.setWidth cc-text-field email-component-width)
    (.setValue subject-text-field (i18n "subject.text.abuse.report.warning"))
    (.setReadOnly subject-text-field true)
    (.setWidth subject-text-field email-component-width)
    (.setWidth msg-textarea email-component-width)
    (.addListener send-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                (buttonClick [event]
                                  (if (= "" (.getValue msg-textarea))
                                    (display-error-notification window "error.email.content")
                                    (do
                                      (let [mail-status (send-mail (.getValue to-text-field) (.getValue subject-text-field) (.getValue msg-textarea) (.getValue cc-text-field))]
                                      (.removeWindow window email-window)
                                      (create-email-sucessfull-window-component window sp-email mail-status)))))))
    (.addListener close-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                 (buttonClick [event]
                                   (.removeAllComponents email-grid)
                                   (.removeWindow window email-window))))))

(defn create-text-sub-window-components [window text-window app-name coop-id sp-id sp-email sp-first-name app-id stat username]
  (let [text-grid (new-grid-layout 2 4) h-layout (new-h-layout) label (new-label (str (i18n "text.header") " " sp-first-name))
        subject-text-field (new-text-field) msg-textarea (new-text-area)
        send-button (new-button "send") close-button (new-button "close")]
    (.addComponent text-window text-grid)
    (doto text-grid
      (.addStyleName "email-main-grid")
      (.setSpacing true)
      (.addComponent h-layout 0 0 1 0)
      (.addComponent (new-label "email.subject") 0 1)
      (.addComponent subject-text-field 1 1)
      (.addComponent (new-label "message") 0 2)
      (.addComponent msg-textarea 1 2)
      (.addComponent send-button 0 3)
      (.addComponent close-button 1 3))
    (.addComponent h-layout label)
    (.addStyleName label "email-label")
    (cond
      (= stat "suspendapp") (.setValue subject-text-field (str (i18n "subject.text.suspend.app") " " app-name))
      (= stat "suspendsp") (.setValue subject-text-field (i18n "subject.text.suspend.sp")))
    (.setReadOnly subject-text-field true)
    (.setWidth subject-text-field email-component-width)
    (.setReadOnly subject-text-field true)
    (.setWidth msg-textarea email-component-width)
    (.addListener send-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                (buttonClick [event]
                                  (if (= "" (.getValue msg-textarea))
                                    (display-error-notification window "error.email.content")
                                    (do
                                      (let [response-status (:status-code (send-suspend-request
                                                                            (if (= stat "suspendsp") "sp-id" "app-id")
                                                                            (if (= stat "suspendsp") sp-id app-id)
                                                                            (.getValue msg-textarea)
                                                                            (if (= stat "suspendsp") suspend-sp-url suspend-app-url) username))]
                                      (if (= "S1000" response-status)
                                              (do
                                                (let [mail-status (send-mail sp-email (.getValue subject-text-field) (.getValue msg-textarea))]
                                                (.removeWindow window text-window)
                                                (create-submission-successfull-window-component window response-status sp-email sp-first-name app-name stat mail-status)))
                                              (do (.removeWindow window text-window)
                                              (create-submission-successfull-window-component window response-status sp-email sp-first-name app-name stat -1)))))))))
    (.addListener close-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                 (buttonClick [event]
                                   (.removeAllComponents text-grid)
                                   (.removeWindow window text-window))))))

(defn create-email-window [window app-name coop-id sp-email sp-first-name stat]
  (let [email-window (new-sub-window "email.sub.window")]
    (.addWindow window email-window)
    (doto email-window
      (.setReadOnly true)
      (.setResizable false)
      (.setHeight "350px")
      (.setWidth "650px")
      (.setModal true))
    (create-sub-window-components window email-window app-name sp-email sp-first-name stat)))

(defn create-text-window [window app-name coop-id sp-id sp-email sp-first-name app-id stat username ]
  (let [text-window (new-sub-window "text.message.sub.window")]
    (.addWindow window text-window)
    (doto text-window
      (.setReadOnly true)
      (.setResizable false)
      (.setHeight "350px")
      (.setWidth "650px")
      (.setModal true))
    (create-text-sub-window-components window text-window app-name coop-id sp-id sp-email sp-first-name app-id stat username)))

(defn create-email-sub-window [window app-name coop-id stat]
(let [coop-user (user-coop-id coop-id)]
  (if (not (or (nil? coop-user) (empty? coop-user)))
    (create-email-window window app-name coop-id (get coop-user :email) (get coop-user :username) stat)
    (display-error-notification window "sp.not.found"))))

(defn create-text-sub-window [window app-name coop-id sp-id app-id stat username]
  (let [coop-user (user-coop-id coop-id)]
    (if (not (or (nil? coop-user) (empty? coop-user)))
      (create-text-window window app-name coop-id sp-id (get coop-user :email) (get coop-user :username) app-id stat username)
      (display-error-notification window "sp.not.found"))))

(defn reload-summery-table [list window abuse-repo-table urifu role-list username]
  (doseq [con list]
    (log :info (str "Loading Abuse report list " con))
    (let [app-button (new-link-button (get con :name)) h-layout (new-h-layout) email-sp (new-link-button "email.sp") suspend-app (new-link-button "suspend.app")
          suspend-sp (new-link-button "suspend.sp")]
      (doto h-layout
        (.setSpacing true)
        (.addComponent email-sp))
      (.addListener email-sp (proxy [com.vaadin.ui.Button$ClickListener] []
                               (buttonClick [event]
                                 (create-email-sub-window window (get con :name) (get con :coop-user-id) "manual"))))
      (.addListener suspend-app (proxy [com.vaadin.ui.Button$ClickListener] []
                                  (buttonClick [event]
                                    (create-text-sub-window window (get con :name) (get con :coop-user-id) (get con :sp-id) (get con :app-id) "suspendapp" username))))
      (.addListener suspend-sp (proxy [com.vaadin.ui.Button$ClickListener] []
                                 (buttonClick [event]
                                   (create-text-sub-window window (get con :name) (get con :coop-user-id) (get con :sp-id) (get con :app-id) "suspendsp" username))))

      (if (.contains role-list "ROLE_ADM_SUSPEND_APP")
        (.addComponent h-layout suspend-app))
      (if (.contains role-list "ROLE_ADM_SUSPEND_COOPERATE_USER")
        (.addComponent h-layout suspend-sp))
      (.addListener app-button (reify com.vaadin.ui.Button$ClickListener
                                 (buttonClick [this event]
                                   (.setFragment urifu (str "app_details_" (get con :app-id))))))
      (.addItem abuse-repo-table (into-array Object [app-button (get con :sp-name) (get con :pending) (get con :valid)
                                                     (get con :invalid) (get con :reported) h-layout])
        (get con :app-id)))))

(defn summery-listener-fn [window abuse-repo-table urifu role-list username]
  (proxy [hms.kite.paging.PagingComponentListener] []
    (onReload [start batch]
      (.removeAllItems abuse-repo-table)
      (reload-summery-table (fetch-all-abuse-reports nil start batch) window abuse-repo-table urifu role-list username))))

(defn page-summery [total rec-per-page window abuse-repo-table urifu role-list username]
  (log :info nil (str "Total Number of Records " total))
  (let [table-pag (TablePagingComponent. (summery-listener-fn window abuse-repo-table urifu role-list username) total rec-per-page)]
    (.init table-pag)
    table-pag))

(defn fill-abuse-report [abuse-repo-table body-v window urifu abuse-repo-panel role-list h username]
  (.addComponent h (page-summery (get-total-number-of-abuse-reports nil) 10 window abuse-repo-table urifu role-list username)))

(defn create-abuse-report [body-v window urifu role-list username]
  (let [abuse-grid (new-grid-layout 2 2)
        abuse-repo-panel (new-panel "abused.reports" "abuse-repo-panel")
        paging-h (new-h-layout "abuse-paging")
        abuse-repo-table (new-table "")]
    (.addStyleName abuse-grid "abuse-grid")
    (.setSpacing abuse-grid true)
    (.addComponent body-v abuse-grid)
    (doto abuse-repo-panel
      (.setHeight (Sizeable/SIZE_UNDEFINED) 0)
      (.addComponent abuse-repo-table)
      (.addComponent paging-h))
    (.addComponent abuse-grid abuse-repo-panel)
    (.setSizeUndefined (.getContent abuse-repo-panel))
    (doto abuse-repo-table
      (.addStyleName "abuse-report-table")
      (.setWidth abuse-report-table-width)
      (.setHeight abuse-report-table-hight)
      (.setPageLength 10))
    (doseq [con abuse-report-table-headers]
      (.addContainerProperty abuse-repo-table (i18n (first con)) (second con) nil))
    (doseq [con abuse-report-table-heder-sizes]
      (.setColumnWidth abuse-repo-table (first con) (second con)))
    (doto abuse-repo-table
      (.setColumnAlignment (i18n "table.pending") (Table/ALIGN_RIGHT))
      (.setColumnAlignment (i18n "table.valid.reports") (Table/ALIGN_RIGHT))
      (.setColumnAlignment (i18n "table.invalid.reports") (Table/ALIGN_RIGHT))
      (.setColumnAlignment (i18n "table.reported") (Table/ALIGN_RIGHT)))
    (fill-abuse-report abuse-repo-table body-v window urifu abuse-repo-panel role-list paging-h username)))
