(ns hms.kite.governance.read-property
  (:import (java.io File InputStream))
  (:import (java.util Properties)))

(def prop (Properties. ))
(def quick-link-prob (Properties.))

(defn load-property [property]
   (.load prop (.getResourceAsStream (clojure.lang.RT/baseLoader) "admin.properties"))
    (.getProperty prop property))

(defn load-quick-links[]
  (into {} (doto quick-link-prob
             (.load (.getResourceAsStream (clojure.lang.RT/baseLoader) "quick-links.properties")))))




