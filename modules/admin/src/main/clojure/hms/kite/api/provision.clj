(ns hms.kite.api.provision
  (:use clojure.tools.logging)
  (:use clojure.data.json)
  (:use hms.kite.admin.vaadin)
  (:use hms.kite.governance.read-property)
  (:require [clj-http.client :as client])
  (:import [hms.commons SnmpLogUtil]))


(defn- prov-connection-failed[]
  (SnmpLogUtil/timeTrap "adminToPro" (Integer/parseInt (load-property "snmp.prov.connection.time.delay.in.seconds")) (load-property "connection.to.prov.failed") (into-array [])))

(defn- send-post-req-to-prov [uri body]
  (try
    (let [resp (client/post uri
      {:headers {"Content-Type" "application/json" "Accept" "application/json"}
       :body (json-str body)})]
   (SnmpLogUtil/clearTrap "adminToPro" (load-property "connection.to.prov.successed") (into-array []))
    resp)
    (catch Exception e (prov-connection-failed))))

(defn send-suspend-request [catagory-key catagory-id reason uri username]
  (let
    [body {(str catagory-key) (str catagory-id)
           "username" username
           "reason" (str reason)}
     response (send-post-req-to-prov uri body)]
  (when (not (nil? response))
    (log :info (str "parameter sent to " uri " with " body))
    (log :info (str "parameter received from " uri " : " (:body response)))
    (if (or (nil? response) (empty? response)) {:status-code "E1300"} (read-json (:body response))))))