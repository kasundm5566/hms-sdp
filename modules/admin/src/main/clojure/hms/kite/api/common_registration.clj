(ns hms.kite.api.common-registration
  (:use clojure.tools.logging)
  (:use clojure.data.json)
  (:use hms.kite.admin.vaadin)
  (:use hms.kite.governance.read-property)
  (:require [clj-http.client :as client])
  (:import [hms.commons SnmpLogUtil]))

(defn- common-reg-connection-failed []
  (SnmpLogUtil/timeTrap "adminToCommonRegistration" (Integer/parseInt (load-property "snmp.common.reg.connection.time.delay.in.seconds")) (load-property "connection.to.common.reg.failed") (into-array [])))

(defn- send-post-req-to-common-reg [uri body]
  (try
    (let [resp (client/post uri
                 {:headers {"Content-Type" "application/json" "Accept" "application/json"}
                  :body (json-str body)})]
      (SnmpLogUtil/clearTrap "adminToCommonRegistration" (load-property "connection.to.common.reg.success") (into-array []))
      resp)
    (catch Exception e (common-reg-connection-failed))))

(defn- send-get-req-to-common-reg [uri]
  (try
    (let [resp (client/get uri
                 {:headers {"Content-Type" "application/json" "Accept" "application/json"}})]
      (SnmpLogUtil/clearTrap "adminToCommonRegistration" (load-property "connection.to.common.reg.success") (into-array []))
      resp)
    (catch Exception e (common-reg-connection-failed))))

(defn send-password-reset-request [uri msisdn]
  (let
    [body {"msisdn" msisdn}
     response (send-post-req-to-common-reg uri body)]
    (when (not (nil? response))
      (log :info (str "parameter sent to " uri " with " body))
      (log :info (str "parameter received from " uri " : " (:body response)))
      (if (or (nil? response) (empty? response)) {:status-code "E1300"} (read-json (:body response))))))

(defn find-all-users-details-request [uri]
  (let
    [response (send-get-req-to-common-reg uri)]
    (when (not (nil? response))
      (log :info (str "parameter sent to " uri))
      (log :info (str "parameter received from " uri " : " (:body response)))
      (if (or (nil? response) (empty? response)) {:status-code "E1300"} (read-json (:body response))))))

(defn find-all-users-count-request [uri]
  (let
    [response (send-get-req-to-common-reg uri)]
    (when (not (nil? response))
      (log :info (str "parameter sent to " uri))
      (log :info (str "parameter received from " uri " : " (:body response)))
      (if (or (nil? response) (empty? response)) {:status-code "E1300"} (read-json (:body response))))))

(defn search-users-request [uri]
  (let
    [response (send-get-req-to-common-reg uri)]
    (when (not (nil? response))
      (log :info (str "parameter sent to " uri))
      (log :info (str "parameter received from " uri " : " (:body response)))
      (if (or (nil? response) (empty? response)) {:status-code "E1300"} (read-json (:body response))))))