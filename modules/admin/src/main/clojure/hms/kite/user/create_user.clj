(ns hms.kite.user.create-user
  (:use clojure.data.json)
  (:import [hms.common.registration.api.request UserDetailByUsernameRequestMessage UserDetailRequestMessage])
  (:import [hms.common.registration.api.response BasicUserResponseMessage])
  (:import [hms.common.registration.api.common RequestType])
  (:import [hms.common.registration.api.util RestApiKeys])
  (:import [java.util ArrayList Date])
  (:import hms.kite.client.CreateWebClient)
  (:use hms.kite.governance.read-property))

(defn- get-web-client[url]
  (let [web-client (CreateWebClient.)]
  (.createWebClient web-client url)))

(defn- create-web-client []
  (get-web-client (.toString (load-property "registration.role.url"))))

(defn- read-json-response [stream]
  (let [web-client (CreateWebClient.)
        resp (.readJsonResponse web-client stream)]
  (when (or (= (:status-code resp) "SUCCESS") (= (.get resp "status-code") "SUCCESS"))
        (BasicUserResponseMessage/convertFromMap resp))))

(defn retrieve-data [username]
  (let [request (UserDetailByUsernameRequestMessage.)]
    (.setUserName request username)
    (.setModuleName request "admin")
    (.setRequestType request RequestType/USER_BASIC_DETAILS)
    (.setRequestedTimeStamp request (Date.))
    (let [resp-stream (-> (create-web-client) (.post (.convertToMap request)))
          response (read-json-response resp-stream)]
      (.getRoles response))))


(defn- create-web-client-user []
  (get-web-client (.toString (load-property "gov.reg.client.req.path.user.id"))))

(defn- retrieve-data-by-id [userid]
  (let [request (UserDetailRequestMessage.)]
    (.setUserId request userid)
    (.setRequestType request RequestType/USER_BASIC_DETAILS)
    (.setRequestedTimeStamp request (Date.))
    (let [resp-stream (-> (create-web-client-user) (.post (.convertToMap request)))
          response (read-json-response resp-stream)]
    (when (and (not (nil? response)) (not (nil? (.getAdditionalDataMap  response))))
      {:username (.getAdditionalData response RestApiKeys/USERNAME) :email (.getAdditionalData response RestApiKeys/EMAIL)}))))

(defn user-coop-id [coop-id]
  (retrieve-data-by-id coop-id))

