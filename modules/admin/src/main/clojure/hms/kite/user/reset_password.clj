(ns hms.kite.user.reset-password
  (:import (com.vaadin.terminal Sizeable UserError))
  (:import (com.vaadin.ui Button GridLayout))
  (:import (hms.kite.paging TablePagingComponent))
  (:use hms.kite.admin.vaadin)
  (:use hms.kite.governance.read-property)
  (:use hms.kite.api.common-registration)
  (:require [clojure.tools.logging :as log])
  (:require [clojure.string :as str])
  (:use hms.kite.repo.advertisement-repo))

(declare fill-password-reset-table fill-password-reset-table-search create-password-reset-button create-search-component2)

(def users-details-url1 (load-property "all.users.details.url1"))
(def users-details-url2 (load-property "all.users.details.url2"))
(def all-users-count-url (load-property "all.users.count.url"))
(def reset-password-url (load-property "password.reset.url"))
(def search-users-url1 (load-property "search.users.url1"))
(def search-users-url2 (load-property "search.users.url2"))
(def search-users-url3 (load-property "search.users.url3"))

(defn create-url-to-get-all-users-with-paging [start limit]
  (str(load-property "all.users.details.url1") start (load-property "all.users.details.url2") limit ))

(defn create-url-to-search-users-with-paging [field value start limit]
  (str(load-property "search.users.url1") field "/" value (load-property "search.users.url2") start (load-property "search.users.url3") limit ))

(defn display-password-reset-error-message [window message]
  (.showNotification window message (com.vaadin.ui.Window$Notification/TYPE_WARNING_MESSAGE)))

(def manage-users-table-headers
  [["user.table.col1.header" String] ["user.table.col2.header" String] ["user.table.col3.header" String] ["user.table.col4.header" GridLayout]])

(def manage-users-table-header-sizes
  [["user.table.col1.header" 200] ["user.table.col2.header" 200] ["user.table.col3.header" 200] ["user.table.col4.header" 150]])

(def combo-user-search-options
  [["combo.appname" "combo.search.user.full.name.option"] ["combo.adname" "combo.search.user.email.option"] ["combo.adname" "combo.search.user.mobile.number.option"]])

(def manage-users-table-width "690px")
(def manage-users-table-height "400px")
(def panel-width2 "858px")

(def change-height2 "200px")

(defn display-error-change-message2 [window msg]
  (.showNotification window (i18n msg) (com.vaadin.ui.Window$Notification/TYPE_HUMANIZED_MESSAGE)))

(defn create-reset-password-table [body-v urifu roles-list window]
  (.removeAllComponents body-v)
  (let [manage-ad-grid (new-grid-layout 3 3) manage-panel (new-panel "reset.password.panel.header") h-layout (new-h-layout) manage-table (new-table "")
        paging-h (new-h-layout) h-layout-assign-ad (new-h-layout)]
    (.addComponent body-v manage-ad-grid)
    (doto manage-ad-grid
      (.addStyleName "manage-grid")
      (.setSpacing true)
      (.addComponent manage-panel 0 0 2 2))
    (doto manage-panel
      (.addStyleName "advertisement-manage-panel")
      (.setHeight (Sizeable/SIZE_UNDEFINED) 0)
      (.addComponent h-layout-assign-ad)
      (.addComponent h-layout)
      (.addComponent manage-table)
      (.addComponent paging-h))
    (.setSizeUndefined (.getContent manage-panel))
    (doto h-layout-assign-ad
      (.setSpacing true)
      (.addStyleName "h-layout-assign-ad"))
    (doto manage-table
      (.addStyleName "assigned-table")
      (.setWidth manage-users-table-width)
      (.setHeight manage-users-table-height))
    (.setSpacing h-layout true)
    (create-search-component2 h-layout manage-table paging-h manage-panel window roles-list urifu body-v)
    (doseq [con manage-users-table-headers]
      (.addContainerProperty manage-table (i18n (first con)) (second con) nil))
    (doseq [con manage-users-table-header-sizes]
      (.setColumnWidth manage-table (first con) (second con)))
    (fill-password-reset-table manage-table paging-h window manage-panel body-v urifu roles-list)))

(defn create-search-results-table [body-v urifu roles-list window field text]
  (.removeAllComponents body-v)
  (let [manage-ad-grid (new-grid-layout 3 3) manage-panel (new-panel "reset.password.panel.header") h-layout (new-h-layout) manage-table (new-table "")
        paging-h (new-h-layout) h-layout-assign-ad (new-h-layout)]
    (.addComponent body-v manage-ad-grid)
    (doto manage-ad-grid
      (.addStyleName "manage-grid")
      (.setSpacing true)
      (.addComponent manage-panel 0 0 2 2))
    (doto manage-panel
      (.addStyleName "advertisement-manage-panel")
      (.setHeight (Sizeable/SIZE_UNDEFINED) 0)
      (.addComponent h-layout-assign-ad)
      (.addComponent h-layout)
      (.addComponent manage-table)
      (.addComponent paging-h))
    (.setSizeUndefined (.getContent manage-panel))
    (doto h-layout-assign-ad
      (.setSpacing true)
      (.addStyleName "h-layout-assign-ad"))
    (doto manage-table
      (.addStyleName "assigned-table")
      (.setWidth manage-users-table-width)
      (.setHeight manage-users-table-height))
    (.setSpacing h-layout true)
    (create-search-component2 h-layout manage-table paging-h manage-panel window roles-list urifu body-v)
    (doseq [con manage-users-table-headers]
      (.addContainerProperty manage-table (i18n (first con)) (second con) nil))
    (doseq [con manage-users-table-header-sizes]
      (.setColumnWidth manage-table (first con) (second con)))
    (fill-password-reset-table-search manage-table paging-h window manage-panel body-v urifu roles-list field text)))

(defn reload-password-reset-table [list table window panel body-v urifu roles-list]
  (doseq [con list]
    (.addItem table (into-array Object [(con :fullName) (con :email) (con :mobileNumber) (create-password-reset-button window con table panel body-v urifu roles-list)]) (con :mobileNumber))))

(defn paging-panel-listener-fn [table window panel body-v urifu roles-list]
  (proxy [hms.kite.paging.PagingComponentListener] []
    (onReload [start batch]
      (.removeAllItems table)
      (reload-password-reset-table (get (find-all-users-details-request (create-url-to-get-all-users-with-paging start batch)) :users) table window panel body-v urifu roles-list))))

(defn paging-panel-init [total rec-per-page table window panel body-v urifu roles-list]
  (let [table-pag (TablePagingComponent. (paging-panel-listener-fn table window panel body-v urifu roles-list) total rec-per-page)]
    (.init table-pag)
    table-pag))

(defn fill-password-reset-table [table paging window panel body-v urifu roles-list]
  (.addComponent paging (paging-panel-init (get (find-all-users-count-request all-users-count-url) :count) 10 table window panel body-v urifu roles-list)))

(defn paging-panel-listener-search-fn [table window panel body-v urifu roles-list field text]
  (proxy [hms.kite.paging.PagingComponentListener] []
    (onReload [start batch]
      (.removeAllItems table)
      (reload-password-reset-table (get (search-users-request (create-url-to-search-users-with-paging field (.getValue text) start batch)) :users) table window panel body-v urifu roles-list))))


(defn paging-panel-init-search [total rec-per-page table window panel body-v urifu roles-list field text]
  (let [table-pag2 (TablePagingComponent. (paging-panel-listener-search-fn table window panel body-v urifu roles-list field text) total rec-per-page)]
    (.init table-pag2)
    table-pag2))

(defn fill-password-reset-table-search [table paging window panel body-v urifu roles-list field text]
  (.addComponent paging (paging-panel-init-search (get (search-users-request (create-url-to-search-users-with-paging field (.getValue text) 0 2)) :count) 10 table window panel body-v urifu roles-list field text)))


(defn display-sucessfull-msg2 [window]
  (.showNotification window (i18n "sucess.chage.combo") (com.vaadin.ui.Window$Notification/TYPE_HUMANIZED_MESSAGE)))

(defn create-reset-password-confirmation [mobileNumber window table]
  (let [remove-window (new-sub-window "ad.remove.window") reset-button (new-button "reset.password.confirmation.button.yes") cancel-button (new-button "reset.password.confirmation.button.no")
        h (new-h-layout) h-layout-1 (new-h-layout) h-layout-2 (new-h-layout)]
    (.addWindow window remove-window)
    (.addStyleName reset-button "add-del-button")
    (doto remove-window
      (.setReadOnly true)
      (.setModal true)
      (.setHeight "200px")
      (.setWidth "500px")
      (.setResizable false)
      (.addComponent h-layout-1)
      (.addComponent h-layout-2))
    (doto h-layout-1
      (.addStyleName "remove-h-layout")
      (.setSpacing true)
      (.addComponent (new-label (str (i18n "reset.password.confirmation.message") " " mobileNumber " ?"))))
    (doto h-layout-2
      (.addStyleName "remove-h-layout-2")
      (.setSpacing true)
      (.addComponent reset-button)
      (.addComponent cancel-button))
    (.addListener cancel-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                  (buttonClick [event]
                                    (.removeWindow window remove-window))))
    (.addListener reset-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                 (buttonClick [event]
                                   (let [response (send-password-reset-request reset-password-url mobileNumber)]
                                     (if (= "S1000" (:status-code response))
                                       (do
                                         (display-password-reset-error-message window (i18n "reset.password.success.message")))
                                       (do
                                         (display-password-reset-error-message window (i18n "reset.password.error.message"))))
                                     (.removeWindow window remove-window)))))
    ))

(defn create-password-reset-button [window users table panel body-v urifu roles-list]
  (let [grid (new-grid-layout 2 1) password-reset-button (new-link-button "reset.password.button.text")]
    (doto grid
      (.addStyleName "manage-button-h")
      (.setSpacing true)
      (.addComponent password-reset-button 0 0))
    (.addListener password-reset-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                          (buttonClick [event]
                                            (create-reset-password-confirmation (get users :mobileNumber) window table)
                                            )))
    grid))

(defn fill-table-by-mobile-no [window table list panel body-v urifu roles-list]
  (doseq [con list]
    (.addItem table (into-array Object [(con :fullName) (con :email) (con :mobileNumber) (create-password-reset-button window con table panel body-v urifu roles-list)]) (con :mobileNumber))))

(defn fill-assigned-table-by-ad-name2 [window table ad-name panel body-v urifu roles-list]
  (doseq [con list]
    (.addItem table (into-array Object [(con :fullName) (con :email) (con :mobileNumber) (create-password-reset-button window con table panel body-v urifu roles-list)]) (con :mobileNumber))))

(defn create-search-component2 [h-layout table paging-h panel window roles-list urifu body-v]
  (let [form (new-form) text (new-text-field) search-button (new-small-button "manage.search.button") h (new-h-layout) combo (new-combo-box "")]
    (.addComponent h-layout form)
    (doto combo
      (.addStyleName "assigned-combo")
      (.setImmediate true)
      (.setNullSelectionAllowed false)
      (.setWidth "190px"))
    (.addStyleName search-button "assigned-button")
    (doto form
      (.setLayout h)
      (.addStyleName "assigned-ad-form"))
    (doto h
      (.setSpacing true)
      (.addComponent (new-label "subscription.search.label"))
      (.addComponent combo)
      (.addComponent text)
      (.addComponent search-button))
    (doseq [con combo-user-search-options]
      (.addItem combo (i18n (second con))))
    (.setValue combo (i18n "combo.search.user.mobile.number.option"))
    (.addListener search-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                  (buttonClick [event]
                                    (.removeAllItems table)
                                    (.removeComponent panel paging-h)
                                    (if (= (.getValue text) "")
                                      (create-reset-password-table body-v urifu roles-list window)
                                      (if (= (.getValue combo) (i18n "combo.search.user.mobile.number.option"))
                                        (do
                                          (create-search-results-table body-v urifu roles-list window "mobileNo" text))
                                        (if (= (.getValue combo) (i18n "combo.search.user.full.name.option"))
                                          (do
                                            (create-search-results-table body-v urifu roles-list window "name" text))
                                          (if (= (.getValue combo) (i18n "combo.search.user.email.option"))
                                            (do
                                              (create-search-results-table body-v urifu roles-list window "email" text)))))))))))

(defn display-error-msg-ad2 [window message]
  (.showNotification window (i18n message) (com.vaadin.ui.Window$Notification/TYPE_WARNING_MESSAGE)))