(ns hms.kite.repo.governance-repo
  (:use karras.sugar)
  (:use karras.core)
  (:use karras.collection))

(defn get-total-number-of-abuse-reports [coll-name]
  (if (= coll-name nil)
    (count-docs (collection :abuse_report_summary))
    (count-docs coll-name)))

(defn fetch-all-abuse-reports [coll-name skip limit]
  (if (= coll-name nil)
    (fetch-all (collection :abuse_report_summary)
      :skip skip
      :limit limit)
    (fetch-all coll-name)))

(defn find-abuse-report-by-app-id [app-id status]
  (fetch (collection :abuse_report)
    (where (eq :report_status status) (eq :app-id app-id))))

(defn find-application-info-by-appid [app-id]
  (fetch (collection :abuse_report_summary)
    (where (eq :app-id app-id))))

(defn update-abuse-repot-status [status abuse-map]
  (if (= status "VALID")
    (save (collection :abuse_report) (assoc abuse-map :report_status status))
    (save (collection :abuse_report) (assoc abuse-map :report_status status))))

(defn update-abuse-report-count[abuse-repo-map pending count status]
  (if (= status "VALID")
    (save (collection :abuse_report_summary) (assoc abuse-repo-map :pending pending :valid count ))
    (save (collection :abuse_report_summary) (assoc abuse-repo-map :pending pending :invalid count ))))




