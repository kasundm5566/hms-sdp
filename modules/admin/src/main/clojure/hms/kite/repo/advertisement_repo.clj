(ns hms.kite.repo.advertisement-repo
  (:use karras.sugar)
  (:use karras.core)
  (:use karras.collection)
  (:use [hms.kite.admin.mongo-connection-filter :only [*mongo-dbs*]]))

(defn fetch-all-ad [skip limit]
  (fetch-all (collection :advertisements)
    :skip skip
    :limit limit))

(defn get-total-number-of-ads []
  (count-docs (collection :advertisements) ))

(defn fetch-ad-by-name [ad-name]
  (fetch (collection :advertisements)
    (where (eq :_id ad-name))))

(defn delete-add [ad-name]
  (delete (collection :advertisements) (where (eq :_id ad-name))))

(defn update-ad [ad-map ad-name content]
  (delete-add (get ad-map :_id))
  (save (collection :advertisements) (assoc ad-map :_id ad-name :message content)))

(defn get-apps-by-ad-name [ad-name]
  (fetch (collection :app_advertisements)
    (where (eq :advertisement-name ad-name))))

(defn delete-app-ad [ad-name]
  (delete (collection :app_advertisements) (where (eq :advertisement-name ad-name))))

(defn delete-app-add-by-app-id[app-id]
  (delete (collection :app_advertisements) (where (eq :_id app-id))))

(defn get-all-app-advertisement [skip limit]
  (fetch-all (collection :app_advertisements)
    :skip skip
    :limit limit))

(defn get-total-number-of-app-ad[]
  (count-docs (collection :app_advertisements) ))

(defn get-app-info-by-app-name [app-name]
  (fetch (collection :app)
    (where (eq :name app-name))))

(defn get-app-ad-by-app-id [app-id]
  (fetch (collection :app_advertisements)
    (where (eq :_id app-id))))

(defn get-app-info-by-app-id [app-id]
  (fetch (collection :app)
    (where (eq :app-id app-id))))

(defn update-ad-app-change [ad-app-map]
  (save (collection :app_advertisements) ad-app-map))

(defn save-ad [ad-map]
  (save (collection :advertisements) ad-map))

(defn get-all-add[]
  (fetch-all (collection :advertisements)))

(defn get-all-app[]
  (fetch (collection :app) (where (eq :advertise true)) ))

(defn get-all-subscription-apps []
  (fetch (collection :app) (where (eq :ncses.ncs-type "subscription")) ))
