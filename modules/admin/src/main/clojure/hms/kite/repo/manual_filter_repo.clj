(ns hms.kite.repo.manual-filter-repo
  (:use karras.sugar)
  (:use karras.core)
  (:use karras.collection))

(defn get-total-number-of-manual-filter-message []
  (count-docs (collection :manual_filter_message) (where (eq :filter-status "PENDING")) ))

(defn fetch-all-manual-filter-message [skip limit]
  (fetch (collection :manual_filter_message)
    (where (eq :filter-status "PENDING"))
    :skip skip
    :limit limit))

(defn update-manual-filter-message [msg-map status]
  (save (collection :manual_filter_message) (assoc msg-map :filter-status status)))