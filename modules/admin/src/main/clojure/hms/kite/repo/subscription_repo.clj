(ns hms.kite.repo.subscription-repo
  (:use karras.sugar)
  (:use karras.core)
  (:use karras.collection)
  (:use [hms.kite.admin.mongo-connection-filter :only [*mongo-dbs*]]))

(defn fetch-all-reg-subs-collection [skip limit]
  (with-mongo-request (:subscription-db *mongo-dbs*)
    (fetch (collection :subscription)
      (where (in :current-status ["REGISTERED" "TRIAL" "TEMPORARY_BLOCKED"]))
      :skip skip
      :limit limit)))

(defn fetch-all-subs-collection [skip limit]
  (with-mongo-request (:subscription-db *mongo-dbs*)
    (fetch-all (collection :subscription)
      :skip skip
      :limit limit)))

(defn fetch-all-reg-subs-msisdn [coll-name msisdn skip limit]
  (if (= coll-name nil)
    (with-mongo-request (:subscription-db *mongo-dbs*)
      (fetch (collection :subscription)
        (where (eq :msisdn msisdn) (in :current-status ["REGISTERED" "TRIAL" "TEMPORARY_BLOCKED"]))
        :skip skip
        :limit limit))
    (fetch coll-name (where (eq :msisdn msisdn) (in :current-status ["REGISTERED" "TRIAL" "TEMPORARY_BLOCKED"])))))

(defn fetch-all-subs-msisdn [coll-name msisdn skip limit]
  (if (= coll-name nil)
    (with-mongo-request (:subscription-db *mongo-dbs*)
      (fetch (collection :subscription)
        (where (eq :msisdn msisdn))
        :skip skip
        :limit limit))
    (fetch coll-name (where (eq :msisdn msisdn)))))

(defn fetch-all-reg-subs-app-name [coll-name app-name skip limit]
  (if (= coll-name nil)
    (with-mongo-request (:subscription-db *mongo-dbs*)
      (fetch (collection :subscription)
        (where (eq :app-name app-name) (in :current-status ["REGISTERED" "TRIAL" "TEMPORARY_BLOCKED"]))
        :skip skip
        :limit limit))
    (fetch coll-name (where (eq :app-name app-name) (in :current-status ["REGISTERED" "TRIAL" "TEMPORARY_BLOCKED"])))))

(defn fetch-all-subs-app-name [coll-name app-name skip limit]
  (if (= coll-name nil)
    (with-mongo-request (:subscription-db *mongo-dbs*)
      (fetch (collection :subscription)
        (where (eq :app-name app-name))
        :skip skip
        :limit limit))
    (fetch coll-name (where (eq :app-name app-name)))))

(defn fetch-all-reg-subs-sp-name [coll-name sp-name skip limit]
  (if (= coll-name nil)
    (with-mongo-request (:subscription-db *mongo-dbs*)
      (fetch (collection :subscription)
        (where (eq :coop-user-name sp-name) (in :current-status ["REGISTERED" "TRIAL" "TEMPORARY_BLOCKED"]))
        :skip skip
        :limit limit))
    (fetch coll-name (where (eq :coop-user-name sp-name) (in :current-status ["REGISTERED" "TRIAL" "TEMPORARY_BLOCKED"])))))

(defn fetch-all-subs-sp-name [coll-name sp-name skip limit]
  (if (= coll-name nil)
    (with-mongo-request (:subscription-db *mongo-dbs*)
      (fetch (collection :subscription)
        (where (eq :coop-user-name sp-name))
        :skip skip
        :limit limit))
    (fetch coll-name (where (eq :coop-user-name sp-name)))))

(defn get-total-number-of-reg-subs [coll-name]
  (if (= coll-name nil)
    (with-mongo-request (:subscription-db *mongo-dbs*)
      (count-docs (collection :subscription) (where (in :current-status ["REGISTERED" "TRIAL" "TEMPORARY_BLOCKED"]))))
    (count-docs coll-name)))

(defn get-total-number-of-subs [coll-name]
  (if (= coll-name nil)
    (with-mongo-request (:subscription-db *mongo-dbs*)
      (count-docs (collection :subscription)))
    (count-docs coll-name)))

(defn get-total-number-of-reg-subs-by-msisdn [coll-name msisdn]
  (if (= coll-name nil)
    (with-mongo-request (:subscription-db *mongo-dbs*)
      (count-docs (collection :subscription)
        (where (eq :msisdn msisdn) (in :current-status ["REGISTERED" "TRIAL" "TEMPORARY_BLOCKED"]))))
    (count-docs coll-name (where (eq :msisdn msisdn) (in :current-status ["REGISTERED" "TRIAL" "TEMPORARY_BLOCKED"])))))

(defn get-total-number-of-subs-by-msisdn [coll-name msisdn]
  (if (= coll-name nil)
    (with-mongo-request (:subscription-db *mongo-dbs*)
      (count-docs (collection :subscription)
        (where (eq :msisdn msisdn))))
    (count-docs coll-name (where (eq :msisdn msisdn)))))

(defn get-total-number-of-reg-subs-by-appname [coll-name app-name]
  (if (= coll-name nil)
    (with-mongo-request (:subscription-db *mongo-dbs*)
      (count-docs (collection :subscription)
        (where (eq :app-name app-name) (in :current-status ["REGISTERED" "TRIAL" "TEMPORARY_BLOCKED"]))))
    (count-docs coll-name (where (eq :app-name app-name) (in :current-status ["REGISTERED" "TRIAL" "TEMPORARY_BLOCKED"])))))

(defn get-total-number-of-subs-by-appname [coll-name app-name]
  (if (= coll-name nil)
    (with-mongo-request (:subscription-db *mongo-dbs*)
      (count-docs (collection :subscription)
        (where (eq :app-name app-name))))
    (count-docs coll-name (where (eq :app-name app-name)))))

(defn get-total-number-of-reg-subs-by-spname [coll-name sp-name]
  (if (= coll-name nil)
    (with-mongo-request (:subscription-db *mongo-dbs*)
      (count-docs (collection :subscription)
        (where (eq :coop-user-name sp-name) (in :current-status ["REGISTERED" "TRIAL" "TEMPORARY_BLOCKED"]))))
    (count-docs coll-name (where (eq :coop-user-name sp-name) (in :current-status ["REGISTERED" "TRIAL" "TEMPORARY_BLOCKED"])))))

(defn get-total-number-of-subs-by-spname [coll-name sp-name]
  (if (= coll-name nil)
    (with-mongo-request (:subscription-db *mongo-dbs*)
      (count-docs (collection :subscription)
        (where (eq :coop-user-name sp-name))))
    (count-docs coll-name (where (eq :coop-user-name sp-name)))))

;(defn get-subs-by-id [id]
;  (with-mongo-request (:subscription-db *mongo-dbs*)
;    (fetch (collection :subscription)
;      (where (eq :_id id) (eq :current-status "REGISTERED")))))


