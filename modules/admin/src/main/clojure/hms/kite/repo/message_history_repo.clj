(ns hms.kite.repo.message-history-repo
  (use [clojure.java.jdbc :only [with-connection with-query-results]])
  (:use hms.kite.governance.read-property))


(def classname (load-property "jdbc.driver"))
(def username (load-property "jdbc.username"))
(def m-password (load-property "jdbc.password"))
(def url (load-property "jdbc.url"))

(def db
  {:classname   (str classname)
   :subprotocol "mysql"
   :user        (str username)
   :password    (str m-password)
   :subname     (str url )})

(defn fetch-recs-by-app-id [app-id start batch]
  (with-connection db
     (with-query-results rs ["select * from message_history where app_id = ? limit ? offset ?" app-id batch start]
       (doall rs))))

(defn fetch-count-by-app-id [app-id]
  (with-connection db
     (with-query-results rs ["select * from message_history where app_id = ?" app-id ] (count rs))))


