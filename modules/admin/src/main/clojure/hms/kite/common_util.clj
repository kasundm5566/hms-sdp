(ns hms.kite.common-util)

(def not-nil? (complement nil?))

(def not-empty? (complement empty?))

(defn not-nil-and-empty?[col]
  (and (not-nil? col) (not-empty? col)))

(defn is-nil-empty-string?[text]
  (and (not-nil-and-empty? text) (not-empty? (.trim text))))