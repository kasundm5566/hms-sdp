(ns hms.kite.admin.vcontext-listener
  (:use karras.core)
  (:use hms.kite.governance.read-property)
  (:import [com.mongodb Mongo])
  (:import (hms.commons SnmpLogUtil))
  (:gen-class
    :implements [javax.servlet.ServletContextListener]
    :name hms.kite.admin.VContextListener))

(defonce admin-start-stop-snmp-id "sdp-admin")

(defn -contextInitialized [this ^javax.servlet.ServletContextEvent sce]
  (let [mongo-connection (connect (load-property "mongo.kite.host") (Integer/parseInt (load-property "mongo.kite.port")))
        kite-db (mongo-db mongo-connection (load-property "mongo.kite.db"))
        subscription-mongo-connection (connect (load-property "mongo.subscription.host") (Integer/parseInt (load-property "mongo.subscription.port")))
        subscription-db (mongo-db subscription-mongo-connection (load-property "mongo.subscription.db"))]
    (doto (.getServletContext sce)
      (.setAttribute "mongo-connection" mongo-connection)
      (.setAttribute "kite-db" kite-db)
      (.setAttribute "subscription-mongo-connection" subscription-mongo-connection)
      (.setAttribute "subscription-db" subscription-db)))
  (SnmpLogUtil/clearTrap admin-start-stop-snmp-id (str (load-property "sdp.admin.module.started")) (into-array String ["bar" "baz"])))

(defn -contextDestroyed [this ^javax.servlet.ServletContextEvent sce]
  (when-let [mongo-connection (.getAttribute (.getServletContext sce)  "mongo-connection")]
       (.close mongo-connection))
  (SnmpLogUtil/trap admin-start-stop-snmp-id (str (load-property "sdp.admin.module.stopped")) (into-array String ["bar" "baz"])))