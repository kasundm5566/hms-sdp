(ns hms.kite.admin.admin-servlet
  (:gen-class
    :extends com.vaadin.terminal.gwt.server.AbstractApplicationServlet
    :name hms.kite.admin.AdminServlet)
  (:use hms.kite.admin.admin-application))

(defn ^Class -getApplicationClass [this]
  hms.kite.admin.AdminApplication)

(defn ^hms.kite.admin.AdminApplication -getNewApplication [this request]
  (hms.kite.admin.AdminApplication.))

