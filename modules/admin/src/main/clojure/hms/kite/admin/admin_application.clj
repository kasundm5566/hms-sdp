(ns hms.kite.admin.admin-application
  (:gen-class :extends com.vaadin.Application
              :name hms.kite.admin.AdminApplication
              :implements [com.vaadin.terminal.gwt.server.HttpServletRequestListener]
              :init cjinit)
  (:use hms.kite.governance.abuse-report)
  (:use hms.kite.admin.layout)
  (:use hms.kite.admin.vaadin)
  (:use hms.kite.governance.manual-filter)
  (:use hms.kite.governance.read-property)
  (:use hms.kite.subscription.search-app)
  (:use hms.kite.user.create-user)
  (:use hms.kite.repo.governance-repo)
  (:use hms.kite.advertisement.assigned-advertisement)
  (:use hms.kite.advertisement.manage-advertisement)
  (:use hms.kite.subscription.customer-care-search-app)
  (:use hms.kite.user.reset-password)
  (:use clojure.tools.logging)
  (:use clojure.set)
  (:import (com.vaadin.ui
             Window
             UriFragmentUtility
             UriFragmentUtility$FragmentChangedListener
             Alignment))
  (:import (com.vaadin.terminal
             Sizeable
             ExternalResource))
  (:import (java.util ArrayList)))

(defonce columns 4)
(defonce rows 6)
(def role-list (ArrayList.))

(defn- get-all-admin-roles []
  (let [roles (ArrayList.)]
    (doseq [[menu sub-menus] (:children side-menu)]
      (doseq [[sub-menu role-uri] (:children sub-menus)]
        (.addAll roles (into [] (:roles role-uri)))))
    roles))

(def admin-roles (into #{} (get-all-admin-roles)))

(defn -cjinit []
  [[] (ref {})])

(defn -onRequestStart [this request response]
  (if (not (nil? (.getAttribute (.getSession request) "session-time-out")))
      (if (not (nil? (.getMainWindow this)))
        (.open (.getMainWindow this) (ExternalResource. cas-logout))))
  (.removeAll role-list role-list)
  (let [principal (.getUserPrincipal request)]
    (def username (.getName principal))
    (.addAll role-list (retrieve-data (.getName principal)))))

(defn- check-equals-or-start-with [^String value ^String fragment]
  (cond (nil? fragment) false
        (= value fragment) true
        (.startsWith fragment value) true
        :esle false))

(defn get-default-view-for-user-role []
  (let [uri-values (ArrayList.)]
    (doseq [[menu sub-menus] (:children side-menu)]
      (doseq [[sub-menu role-uri] (:children sub-menus)]
      (when (some (:roles role-uri) role-list)
        (.add uri-values (:uri role-uri)))))
    uri-values))

(defn- get-app-id-from-fragment[fragment]
  (.substring fragment 12))

(defn- get-uri-frag-window [body-v window urifu fragment]
  (condp check-equals-or-start-with (if (empty? fragment) (first (get-default-view-for-user-role)) fragment)
    "abusereport" (if (.contains role-list "ROLE_ADM_ABUSE_RPT")
                        (create-abuse-report body-v window urifu role-list username)
                        (get-uri-frag-window body-v window urifu (first (get-default-view-for-user-role))))
    "manualfiltering" (if (.contains role-list "ROLE_ADM_MANUAL_FILTERING_RPT")
                        (create-manual-filter-table body-v window role-list username)
                        (get-uri-frag-window body-v window urifu (first (get-default-view-for-user-role))))
    "search" (if
               (and (.contains role-list "ROLE_ADM_SEARCH_SUBSCRIPTION") (.contains role-list "ROLE_ADM_VIEW_SUBSCRIPTION") (.contains role-list "ROLE_ADM_UNSUBSCRIBE_SUBSCRIPTION"))
                    (create-search body-v window role-list)
                    (get-uri-frag-window body-v window urifu (first (get-default-view-for-user-role))))
    "cc.search" (if (and (.contains role-list "ROLE_ADM_CC_SEARCH_SUBSCRIPTION") (.contains role-list "ROLE_ADM_CC_VIEW_SUBSCRIPTION"))
                        (create-cc-search body-v window role-list)
                        (get-uri-frag-window body-v window urifu (first (get-default-view-for-user-role))))
    "assigned.advertisement" (if (.contains role-list "ROLE_ADM_CREATE_ADVERTISING")
                               (create-manage-ad-table body-v urifu role-list window)
                               (get-uri-frag-window body-v window urifu (first (get-default-view-for-user-role))))
    "manage.advertisement" (if (.contains role-list "ROLE_ADM_LIST_ADVERTISING")
                                (create-manage-ad body-v window)
                                (get-uri-frag-window body-v window urifu (first (get-default-view-for-user-role))))
    "resetpassword" (if (.contains role-list "ROLE_ADM_USER_RESET_PASSWORD")
                             (create-reset-password-table body-v urifu role-list window)
                             (get-uri-frag-window body-v window urifu (first (get-default-view-for-user-role))))
    "app_details_" (if (.contains role-list "ROLE_ADM_ABUSE_RPT")
                     (let [app-id (get-app-id-from-fragment fragment)]
                      (create-detail-app-info window body-v app-id urifu role-list))
                     (get-uri-frag-window body-v window urifu (first (get-default-view-for-user-role))))

    (get-uri-frag-window body-v window urifu (first (get-default-view-for-user-role)))))

(defn urifu-listener [body-v window urifu]
  (log :info (str "Roles for the loged in user" role-list))
  (reify UriFragmentUtility$FragmentChangedListener
    (fragmentChanged [this source]
      (.removeAllComponents body-v)
      (let [fragment (.getFragment (.getUriFragmentUtility source))]
      (get-uri-frag-window body-v window urifu fragment)))))

(defn -onRequestEnd [this request response])

(defn -init [this]
  (let [window (Window. (i18n "title"))
        urifu (UriFragmentUtility.)
        main-v-layout (new-v-layout)
        body-v-layout (new-v-layout "main-layout")
        quick-link-layout (new-h-layout "main-link-layout")
        image-header-layout (new-v-layout "img-layout")
        content-h-layout (new-h-layout)
        nav-panel-v-layout (new-v-layout)
        right-body-layout (new-v-layout)
        h-layout (new-h-layout)]
    (.setMainWindow this window)
    (.setTheme window "admin")
    (.addComponent window main-v-layout)
    (.addComponent main-v-layout quick-link-layout)
    (.addComponent main-v-layout body-v-layout)
    (.setComponentAlignment main-v-layout quick-link-layout Alignment/TOP_CENTER)
    (.setComponentAlignment main-v-layout body-v-layout Alignment/TOP_CENTER)
    (.addComponent window urifu)
    (doto body-v-layout
      (.setSpacing true)
      (.addComponent image-header-layout)
      (.addComponent content-h-layout))
    (doto image-header-layout
      (.setSpacing true))
    (doto content-h-layout
      (.setSpacing true)
      (.addComponent nav-panel-v-layout)
      (.addComponent right-body-layout))

    (create-quick-link-panel quick-link-layout window)
    (create-header-image image-header-layout)

    (if (some admin-roles role-list)
      (let [nav-v-panel (new-v-layout)
            body-v (new-v-layout "body-v-layout")]
        (.addListener urifu (urifu-listener right-body-layout window urifu))
        (create-nav-panel nav-panel-v-layout window urifu role-list)
        (.setFragment urifu (first (get-default-view-for-user-role)))
        (create-copywright-info main-v-layout))
      (do
        (create-non-role-ui nav-panel-v-layout window)
        (create-copywright-info main-v-layout)))))
