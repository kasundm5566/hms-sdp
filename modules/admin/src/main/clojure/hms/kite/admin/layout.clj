(ns hms.kite.admin.layout
  (:use hms.kite.governance.manual-filter)
  (:use hms.kite.governance.abuse-report)
  (:use hms.kite.admin.vaadin)
  (:use hms.kite.governance.read-property)
  (:use clojure.tools.logging)
  (:import (com.vaadin.ui Embedded Alignment))
  (:import (com.vaadin.data Property))
  (:import (com.vaadin.terminal ThemeResource Sizeable ExternalResource)))

(defn create-and-add-quick-link-button[quick-link-left-layout window]
  (let [quick-links (into [] (keys (load-quick-links)))]
    (doseq [one-link quick-links]
      (let [one-link-text ((load-quick-links) one-link)
            one-link-url (load-property one-link)
            one-link-button (new-link-button one-link-text "single-link-layout")]
        (.addListener one-link-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                            (buttonClick [event]
                                              (.open window (ExternalResource. one-link-url)))))
        (.addComponent quick-link-left-layout one-link-button)))))



(def cas-logout (load-property "cas.logout"))
(def home-link (load-property "home.url"))


(defn create-quick-link-panel [toplayout window]
  (let [quick-link-right (new-h-layout "right-link-layout")
        quick-link-left (new-h-layout "left-link-layout")
        home-button (new-link-button "home.button" "single-link-layout")
        logout-button (new-link-button "logout.button" "single-link-layout")]

    (create-and-add-quick-link-button quick-link-left window)

    (.addListener home-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                 (buttonClick [event]
                                   (.open window (ExternalResource. home-link)))))
    (.addListener logout-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                  (buttonClick [event]
                                    (.open window (ExternalResource. cas-logout )))))

    (.addComponent quick-link-right home-button)
    (.addComponent quick-link-right logout-button)
    (.addComponent toplayout quick-link-left)
    (.addComponent toplayout quick-link-right)))

(defn create-header-image [img-h-header]
  (let [header-img (Embedded. nil (ThemeResource. "img/top-banner.jpg"))]
    (.addComponent img-h-header header-img)
    (.setComponentAlignment img-h-header header-img Alignment/MIDDLE_CENTER)))

(defonce main-menu-map
  (let [fragments {:menu.governance "abusereport"
                   :menu.subscription "search"
                   :menu.advertising "assigned.advertisement"
                   :menu.user.management "resetpassword"
                   :child.abusereport "abusereport"
                   :child.resetpassword "resetpassword"
                   :child.manualfiltering "manualfiltering"
                   :child.search "search"
                   :child.cc.search "cc.search"
                   :child.assigned.advertisement "assigned.advertisement"
                   :child.add.advertisement "add.advertisement"
                   :child.manage.advertisement "manage.advertisement"}]
    (into {} (map #(vector (i18n (name (first %))) (second %)) fragments))))


(defn tree-listener [main-menu urifu]
  (.addListener main-menu
    (reify com.vaadin.data.Property$ValueChangeListener
      (valueChange [this event]
        (let [key (i18n (.getValue (.getProperty event)))
              val (main-menu-map key)]
          (debug "Selected tree item: " val)
          (if-not (nil? val) (.setFragment urifu val)))))))

(defonce side-menu
  {:children
   (array-map
     "menu.subscription"
     {:expand true
      :children
      (array-map
                  "child.search" {:roles #{"ROLE_ADM_SEARCH_SUBSCRIPTION" "ROLE_ADM_VIEW_SUBSCRIPTION" "ROLE_ADM_UNSUBSCRIBE_SUBSCRIPTION"}
                                  :uri "search"}
                  "child.cc.search" {:roles #{"ROLE_ADM_CC_SEARCH_SUBSCRIPTION" "ROLE_ADM_CC_VIEW_SUBSCRIPTION"}
                                     :uri "cc.search"})}

     "menu.governance"
     {:expand true
      :children
      (array-map
                  "child.abusereport" {:roles #{"ROLE_ADM_ABUSE_RPT"} :uri "abusereport"}
                  "child.manualfiltering" {:roles #{"ROLE_ADM_MANUAL_FILTERING_RPT"} :uri "manualfiltering"})}

     "menu.advertising"
     {:expand true
      :children
      (array-map
                  "child.assigned.advertisement" {:roles #{"ROLE_ADM_CREATE_ADVERTISING"} :uri "assigned.advertisement"}
                  "child.manage.advertisement" {:roles #{"ROLE_ADM_LIST_ADVERTISING"} :uri "manage.advertisement"})}

     "menu.user.management"
     {:expand true
      :children (array-map
                  "child.resetpassword" {:roles #{"ROLE_ADM_LIST_ADVERTISING"} :uri "resetpassword"})})})

(defn get-roles [menus]
  (into #{} (concat (:roles (first (vals menus))) (:roles (second (vals menus))))))

(defn create-nav-panel [nav-v-panel window urifu user-roles]
  (let [nav-panel (new-panel "user.management")
        v (new-h-layout)
        main-menu (new-tree "")]
    (letfn [(menu-builder [root-menu-item menu-config]
              (let [{:keys [children roles expand]
                     :or {:roles #{} :children {} :expand false}} menu-config]
                (when expand
                  (.expandItemsRecursively main-menu root-menu-item))
                (.setChildrenAllowed main-menu root-menu-item (boolean (not-empty children)))
                  (doseq [[menu sub-menus] children]
                    (let [node (i18n menu)
                          parent-menu-roles (if (.startsWith menu "menu") (get-roles (:children sub-menus)) #{})
                          child-menu-roles (if (.startsWith menu "child") (:roles sub-menus) #{})]
                        (when (or
                                (and (.startsWith menu "menu") (some parent-menu-roles user-roles))
                                (and (.startsWith menu "child") (some child-menu-roles user-roles)))
                          (doto main-menu
                            (.addItem node)
                            (.setParent node root-menu-item)))
                      (menu-builder node sub-menus)))))]
      (doto nav-panel
        (.setWidth (Sizeable/SIZE_UNDEFINED) 0)
        (.setHeight (Sizeable/SIZE_UNDEFINED) 0)
        (.addStyleName "nav-panel"))
      (.addComponent nav-v-panel nav-panel)
      (.addStyleName v "nav-v-layout")
      (.addComponent nav-panel v)
      (.setImmediate main-menu true)
      (menu-builder "" side-menu)

      (.addComponent v main-menu)
      (tree-listener main-menu urifu))))

(defn create-copywright-info [main-v-layout]
  (let [v (new-v-layout)]
    (.addComponent main-v-layout v)
    (doto v
      (.addComponent (new-label "copyright"))
      (.addStyleName "copywight-layout"))))

(defn create-non-role-ui[v-layout window]
  (let [h-layout (new-h-layout) panel (new-panel "non.user.panel") label (new-label "non.user.label") log-out (new-link-button "non.user.link")]
    (.addComponent v-layout h-layout)
    (doto h-layout
      (.setSpacing true)
      (.addComponent panel)
      (.addStyleName "non-user-h"))
    (doto panel
      (.addStyleName "non-user-panel")
      (.setWidth "1120px")
      (.setHeight (Sizeable/SIZE_UNDEFINED) 0)
      (.addComponent label)
      (.addComponent log-out))
    (doto label
      (.addStyleName "non-user-label"))
    (.addListener log-out (reify com.vaadin.ui.Button$ClickListener
                         (buttonClick [this event]
                          (.open window (ExternalResource. cas-logout )))))))
