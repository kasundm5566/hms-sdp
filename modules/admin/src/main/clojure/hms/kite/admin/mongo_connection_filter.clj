(ns hms.kite.admin.mongo-connection-filter
  (:use karras.core)
  (:import [javax.servlet ServletRequest ServletResponse FilterChain])
  (:gen-class
    :implements [javax.servlet.Filter]
    :name hms.kite.admin.MongoConnectionFilter
    :init clojure-init
    :state mongodbs))

(def ^{:dynamic true
       :doc "Var to bind mongo dbs used"}
  *mongo-dbs* {})

(defn -clojure-init []
  [[] (atom {})])

(defn- get-attribute [fc attrib]
  (-> (.getServletContext fc) (.getAttribute attrib)))

(defn -init [this ^javax.servlet.FilterConfig fc]
  (swap! (.mongodbs this) assoc :kite-db (get-attribute fc "kite-db"))
  (swap! (.mongodbs this) assoc :subscription-db (get-attribute fc "subscription-db")))

(defn -doFilter [this ^ServletRequest request ^ServletResponse response ^FilterChain chain]
  (do (binding [*mongo-dbs* @(.mongodbs this)]
        (with-mongo-request
          (:kite-db @(.mongodbs this))
          (.doFilter chain request response)))))

(defn -destroy [this])
