(ns hms.kite.admin.vaadin
  (:use clojure.java.io)
  (:import (java.io File))
  (:import (com.vaadin.ui Window Embedded Alignment Button Label Table VerticalLayout HorizontalLayout GridLayout Panel
    FormLayout Button$ClickListener TextArea ComboBox Tree CheckBox TextField Form))
  (:import (com.vaadin.ui.themes Runo)))

(def ^:dynamic *i18n* true)
(def ^:dynamic *local* "en")


(defn load-probs [lang]
  (let [file-name (str "messages" (File/separator) "messages_" lang ".properties")]
    (into {} (doto (java.util.Properties.)
               (.load (-> (Thread/currentThread)
                        (.getContextClassLoader)
                        (.getResourceAsStream file-name)))))))

(defn- load-property-files [langs]
  (reduce
    (fn [lmap lang]
      (assoc lmap lang (load-probs lang)))
    {}
    langs))

(def ^:dynamic *loaded-property-files*
  (load-property-files ["en"]))

(defn i18n [code]
  (if *i18n*
    (if-let [result ((*loaded-property-files* *local*) code)]
      result
      code)
    code))

(defn ^VerticalLayout new-v-layout
  ([] (VerticalLayout.))
  ([style]
    (let [layout (VerticalLayout.)]
      (.setStyleName layout style)
      layout)))

(defn ^HorizontalLayout new-h-layout
  ([]  (HorizontalLayout.))
  ([style]
    (let [layout (HorizontalLayout.)]
      (.setStyleName layout style)
      layout)))

(defn ^GridLayout new-grid-layout [columns rows]
  (GridLayout. columns rows))

(defn ^Table new-table-layout []
  (Table.))

(defn ^Button new-button
  ([] (Button.))
  ([caption] (Button. (i18n caption))))

(defn ^Button new-link-button
  ([caption]
    (let [button (Button. (i18n caption))]
      (.setStyleName button Runo/BUTTON_LINK)
      button))
  ([caption style]
    (let [button (Button. (i18n caption))]
      (.setStyleName button Runo/BUTTON_LINK)
      (.addStyleName button style)
      button)))

(defn ^Panel new-panel
  ([] (Panel. ))
  ([caption] (Panel. (i18n caption)))
  ([caption style]
    (let [panel (Panel. (i18n caption))]
      (.setStyleName panel style)
      panel)))

(defn ^FormLayout new-f-layout []
  (FormLayout. ))

(defn ^Table new-table
  ([caption] (Table. caption)))

(defn ^Label new-label
  [caption] (Label. (i18n caption)))

(defn ^Window new-sub-window
  [caption] (Window. (i18n caption)))

(defn ^TextArea new-text-area []
  (TextArea.))

(defn ^ComboBox new-combo-box
  [caption] (ComboBox. caption))

(defn ^Tree new-tree
  [caption] (Tree. caption))

(defn ^CheckBox new-checkbox
  [caption] (CheckBox. (i18n caption)))

(defn ^TextField new-text-field
  ([] (TextField.))
  ([style]
    (let [text-field (TextField.)]
      (.setStyleName text-field style)
      text-field)))

(defn ^Form new-form[]
  (Form.))

(defn ^Button new-small-button
  [caption]
  (let [button (Button. (i18n caption))]
      (.setStyleName button Runo/BUTTON_SMALL)
      button))

(defn ^TextField new-text-with-name
  [caption] (TextField. (i18n caption)))

(defn ^TextArea new-text-area-with-name
  [caption] (TextArea. (i18n caption)))

(defn ^ComboBox new-combo-with-name
  [caption] (ComboBox. (i18n caption)))

(defn display-message-humanized-message [window message]
  (.showNotification window message (com.vaadin.ui.Window$Notification/TYPE_HUMANIZED_MESSAGE)))

(defn display-message-error [window message]
  (.showNotification window message (com.vaadin.ui.Window$Notification/TYPE_ERROR_MESSAGE)))

(defn display-message-as-warning[window message]
  (.showNotification window message (com.vaadin.ui.Window$Notification/TYPE_WARNING_MESSAGE)))

