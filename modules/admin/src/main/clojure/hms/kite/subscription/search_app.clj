(ns hms.kite.subscription.search-app
  (:use hms.kite.admin.vaadin)
  (:use hms.kite.common-util)
  (:use hms.kite.repo.subscription-repo)
  (:use hms.kite.governance.read-property)
  (:use clojure.data.json)
  (:use clojure.tools.logging)
  (:use hms.kite.repo.advertisement-repo)
  (:require [clj-http.client :as client])
  (:import (com.vaadin.ui Button Window Table))
  (:import (com.vaadin.terminal ExternalResource Sizeable))
  (:import (hms.kite.paging TablePagingComponent))
  (:import (hms.commons SnmpLogUtil))
  (:import (java.text SimpleDateFormat))
  (:import (java.util Date)))


(def ^{:private true} search-grid-cols 4)
(def ^{:private true} search-grid-rows 6)

(def ^{:private true} search-table-width "690px")
(def ^{:private true} search-table-hight "350px")
(def ^{:private true} search-panel-width "870px")


(defn- subscription-connection-failed[]
  (SnmpLogUtil/timeTrap "adminToSubscription" (Integer/parseInt (load-property "snmp.subscription.connection.time.delay.in.seconds")) (load-property "connection.to.subscription.failed") (into-array String ["bar" "baz"])))

(def combo-search-names
  [["combo.appname" "subs.appname"] ["combo.msisdn" "subs.msisdn"] ["combo.spname" "subs.spname"]])

(def search-table-headers
  [["table.appname" String] ["table.msisdn" String] ["table.spname" String] ["subscription.start.date" String] ["table.status" Button]])

(def search-table-sizes
  [["table.appname" 100] ["table.msisdn" 70] ["table.spname" 70] ["subscription.start.date" 70] ["table.status" 70]])

(defn url-fn []
  (str (load-property "subscription.provisioning.url")))

(defn display-error-message [window message]
  (.showNotification window message (com.vaadin.ui.Window$Notification/TYPE_WARNING_MESSAGE)))

(defn- send-post-req-to-subscription [uri body window error-key]
  (try
    (let [resp (client/post (uri)
                  {:headers {"Content-Type" "application/json" "Accept" "application/json"}
                   :body (json-str body)})]
      (SnmpLogUtil/clearTrap "adminToSubscription" (load-property "connection.to.prov.successed") (into-array []))
      resp)
    (catch Exception e (subscription-connection-failed) (display-error-message window (i18n error-key)))))

(defn send-subscription-request-to-api [appid msisdn action uri-fn window error-key]
    (let [app (first (get-app-info-by-app-id appid))
          body { "applicationId" appid
                  "subscriberId" (str "tel:" msisdn)
                  "action" action
                  "password" (when (not (nil? app)) (get app :password))} ; for subscription action 1, for un subscription 0
          response (send-post-req-to-subscription uri-fn body window error-key)]
        (read-json (:body response))))


(defn- create-child-window-sub [window con table]
  (let [child (new-sub-window "child.confirm.window") cancel (new-button "child.window.button.cancel")
        ok (new-button "child.window.button.ok") upper (new-h-layout) lower (new-h-layout)]
    (.addWindow window child)
    (doto child
      (.removeAllComponents)
      (.setHeight "200px")
      (.setWidth "315px")
      (.setResizable false)
      (.setReadOnly true)
      (.setModal true)
      (.addComponent upper)
      (.addComponent lower))
    (doto upper
      (.setSpacing true)
      (.addComponent (new-label "child.window.label.subs")))
    (doto lower
      (.setSpacing true)
      (.addStyleName "subs-child-window-h-layout")
      (.addComponent ok)
      (.addComponent cancel))
    (doto ok
      (.addStyleName "subs-child-button-ok"))
    (.addListener ok (proxy [com.vaadin.ui.Button$ClickListener] []
                       (buttonClick [event]
                         (send-subscription-request-to-api (get con :app-id) (get con :msisdn) 0 url-fn window "error.unsub.message")
                         (.removeItem table (get con :_id))
                         (.removeWindow window child))))
    (.addListener cancel (proxy [com.vaadin.ui.Button$ClickListener] []
                           (buttonClick [event]
                             (.removeWindow window child))))))

(defn- create-action-button [window con table]
  (let [button (new-link-button "button.block")]
    (.addListener button (proxy [com.vaadin.ui.Button$ClickListener] []
                           (buttonClick [event]
                             (create-child-window-sub window con table))))
    button))

(defn- convert-time-to-string [date]
  (if (nil? date)
      (str (i18n "data.not.available"))
      (.format (SimpleDateFormat. "yyyy-MM-dd HH:mm:ss") (cast Date date))))

(defn- reload-subs-ad-table [list table window]
  (doseq [con list]
    (.addItem table (into-array Object
                      [(get con :app-name) (get con :msisdn) (get con :coop-user-name) (convert-time-to-string (get con :created-date)) (create-action-button window con table)])
                    (get con :_id))))

(defn- summery-subs-listener-fn [table window]
  (proxy [hms.kite.paging.PagingComponentListener] []
    (onReload [start batch]
      (.removeAllItems table)
      (reload-subs-ad-table (fetch-all-reg-subs-collection start batch) table window))))

(defn- page-summery-subs [total rec-per-page table window]
  (let [table-pag (TablePagingComponent. (summery-subs-listener-fn table window) total rec-per-page)]
    (.init table-pag)
    table-pag))

(defn- fill-table-initial [search-table paging-h window]
  (.removeAllComponents paging-h)
  (.addComponent paging-h (page-summery-subs (get-total-number-of-reg-subs nil) 10 search-table window)))

(defn- create-table [h-layout search-table paging-h window]
  (.addComponent h-layout search-table)
  (doto search-table
    (.addStyleName "search-table")
    (.setWidth search-table-width)
    (.setHeight search-table-hight)
    (.setPageLength 10))
  (doseq [con search-table-headers]
    (.addContainerProperty search-table (i18n (first con)) (second con) nil))
  (doseq [con search-table-sizes]
    (.setColumnWidth search-table (first con) (second con)))
  (fill-table-initial search-table paging-h window))

(defn- reload-subs-ad-table-msisdn [list table window]
  (doseq [con list]
    (.addItem table (into-array Object
                      [(get con :app-name) (get con :msisdn) (get con :coop-user-name) (convert-time-to-string (get con :created-date)) (create-action-button window con table)])
                    (get con :_id))))

(defn- summery-subs-listener-fn-msisdn [table pattern window]
  (proxy [hms.kite.paging.PagingComponentListener] []
    (onReload [start batch]
      (.removeAllItems table)
      (reload-subs-ad-table-msisdn (fetch-all-reg-subs-msisdn nil pattern start batch) table window))))

(defn- page-summery-subs-msisdn [total rec-per-page table pattern window]
  (let [table-pag (TablePagingComponent. (summery-subs-listener-fn-msisdn table pattern window) total rec-per-page)]
    (.init table-pag)
    table-pag))

(defn- fill-table-by-msisdn [search-table search-text window roles-list page-h]
  (.removeAllComponents page-h)
  (let [search-patten (re-pattern (str "(?i)" (.trim (.getValue search-text))))]
    (.addComponent page-h (page-summery-subs-msisdn
                            (get-total-number-of-reg-subs-by-msisdn nil search-patten) 10 search-table search-patten window))))

(defn- reload-subs-ad-table-app [list table window]
  (doseq [con list]
    (.addItem table (into-array Object
                        [(get con :app-name) (get con :msisdn) (get con :coop-user-name) (convert-time-to-string (get con :created-date)) (create-action-button window con table)])
                    (get con :_id))))

(defn- summery-subs-listener-fn-app [table pattern window]
  (proxy [hms.kite.paging.PagingComponentListener] []
    (onReload [start batch]
      (.removeAllItems table)
      (reload-subs-ad-table-app (fetch-all-reg-subs-app-name nil pattern start batch) table window))))

(defn- page-summery-subs-app [total rec-per-page table pattern window]
  (let [table-pag (TablePagingComponent. (summery-subs-listener-fn-app table pattern window) total rec-per-page)]
    (.init table-pag)
    table-pag))

(defn- fill-table-by-app-name [search-table search-text window roles-list page-h]
  (.removeAllComponents page-h)
  (let [search-patten (re-pattern (str "(?i)" (.trim (.getValue search-text))))]
    (.addComponent page-h (page-summery-subs-app
                            (get-total-number-of-reg-subs-by-appname nil search-patten) 10 search-table search-patten window))))

(defn- reload-subs-ad-table-sp [list table window]
  (doseq [con list]
    (.addItem table
      (into-array Object
                      [(get con :app-name) (get con :msisdn) (get con :coop-user-name) (convert-time-to-string (get con :created-date)) (create-action-button window con table)])
      (get con :_id))))

(defn- summery-subs-listener-fn-sp [table pattern window]
  (proxy [hms.kite.paging.PagingComponentListener] []
    (onReload [start batch]
      (.removeAllItems table)
      (reload-subs-ad-table-sp (fetch-all-reg-subs-sp-name nil pattern start batch) table window))))

(defn- page-summery-subs-sp [total rec-per-page table pattern window]
  (let [table-pag (TablePagingComponent. (summery-subs-listener-fn-sp table pattern window) total rec-per-page)]
    (.init table-pag)
    table-pag))

(defn- fill-table-by-sp-name [search-table search-text window roles-list page-h]
  (.removeAllComponents page-h)
  (let [search-patten (re-pattern (str "(?i)" (.trim (.getValue search-text))))]
    (.addComponent page-h (page-summery-subs-sp
                            (get-total-number-of-reg-subs-by-spname nil search-patten) 10 search-table search-patten window))))

(defn- create-search-com [form combo-select search-text search-button]
  (let [h (new-h-layout) label (new-label "subscription.search.label")]
    (doto form
      (.addStyleName "subs-search")
      (.setLayout h))
    (doto combo-select
      (.addStyleName "search-combo")
      (.setImmediate true)
      (.setNullSelectionAllowed false))
    (doseq [con combo-search-names]
      (.addItem combo-select (i18n (second con))))
    (.setValue combo-select (i18n "subs.appname"))
    (.addStyleName label "subs-search-label")
    (doto h
      (.setSpacing true)
      (.addComponent label)
      (.addComponent combo-select)
      (.addComponent search-text)
      (.addComponent search-button))))

(defn- create-register-component [body-v roles-list window]
  (let [register-window (new-sub-window "subscribe.window.title") h-layout (new-h-layout) form (new-form)
        subscribe-button (new-button "subscribe.window.button.subscribe") cancel-button (new-button "subscribe.window.button.cancel")
        h-error-common (new-h-layout) h-error-msisdn (new-h-layout) error-label-common (new-label "subscribe.window.error.message.common")
        error-label-msisdn (new-label "subscribe.window.error.message.msisdn")
        app-name-combo (new-combo-with-name "subscribe.window.app.name") msisdn-text (new-text-with-name "subscribe.window.msisdn")
        regex (load-property "subscription.msisdn.regex")]
    (.addWindow window register-window)
    (doto register-window
          (.setReadOnly true)
          (.setModal true)
          (.setHeight "200px")
          (.setWidth "470px")
          (.setResizable false)
          (.addComponent form)
          (.addComponent h-layout))
    (doto form
          (.addStyleName "subscribe-window-form")
          (.addField "subscribe.window.app.name" app-name-combo)
          (.addField "subscribe.window.msisdn" msisdn-text)
          )
    (doto h-layout
          (.setSpacing true)
          (.addStyleName "subscribe-window-h-layout")
          (.addComponent subscribe-button)
          (.addComponent cancel-button))
    (doto error-label-common
          (.addStyleName "subscribe-window-error-label"))
    (doto error-label-msisdn
          (.addStyleName "subscribe-window-error-label"))
    (doto h-error-common
          (.setSpacing true)
          (.addStyleName "subscribe-window-h-layout-error")
          (.addComponent error-label-common))
    (doto h-error-msisdn
          (.setSpacing true)
          (.addStyleName "subscribe-window-h-layout-error")
          (.addComponent error-label-msisdn))
    (doto msisdn-text
          (.setWidth "199px")
          (.setRequired true))
    (doto app-name-combo
          (.setImmediate true)
          (.setNullSelectionAllowed false)
          (.setRequired true))
    (doseq [app (get-all-subscription-apps)]
      (.addItem app-name-combo (get app :name)))
    (.addListener subscribe-button (proxy [com.vaadin.ui.Button$ClickListener] []
                            (buttonClick [event]
                                         (if (and (not-nil-and-empty? (.getValue app-name-combo)) (not-nil-and-empty? (.getValue msisdn-text)))
                                             (if (re-matches (re-pattern regex) (.getValue msisdn-text))
                                                 (let [con (first (get-app-info-by-app-name (.getValue app-name-combo)))
                                                       resp (send-subscription-request-to-api (get con :app-id) (.getValue msisdn-text) 1 url-fn window "error.sub.message")]
                                                   (if (= "S1000" (:statusCode resp))
                                                       (do
                                                         (.removeComponent register-window h-error-common)
                                                         (.removeComponent register-window h-error-msisdn)
                                                         (.removeWindow window register-window))
                                                       (display-error-message window (:statusDetail resp))))
                                                 (do
                                                   (.removeComponent register-window h-error-common)
                                                   (.addComponent register-window h-error-msisdn)))
                                             (.addComponent register-window h-error-common)))))
    (.addListener cancel-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                       (buttonClick [event]
                                                    (.removeWindow window register-window))))))

(defn create-search [body-v window roles-list]
  (let [v-layout (new-v-layout) search-panel (new-panel (i18n "search.panel") "search-panel-admin") search-table (new-table "")
        search-grid (new-grid-layout search-grid-cols search-grid-rows) h-layout (new-h-layout)
        combo-select (new-combo-box "") search-text (new-text-field)
        search-button (new-small-button "subs.search.button") page-h (new-h-layout) form (new-form)
        h-layout-1 (new-h-layout "subs-search-h-layout-1") h-layout-2 (new-h-layout "subs-search-h-layout-2")
        h-layout-3 (new-h-layout "subs-search-h-layout-3") register-button (new-link-button "button.register")]
    (.removeAllComponents search-panel)
    (.addComponent body-v search-panel)
    (doto search-panel
      (.setHeight (Sizeable/SIZE_UNDEFINED) 0))
    (if (and (.contains roles-list "ROLE_ADM_SEARCH_SUBSCRIPTION")
            (.contains roles-list "ROLE_ADM_VIEW_SUBSCRIPTION")
            (.contains roles-list "ROLE_ADM_UNSUBSCRIBE_SUBSCRIPTION"))
      (do
        (.addComponent search-panel h-layout-3)
        (.addComponent search-panel h-layout-1)
        (.addComponent search-panel h-layout-2)))
    (if (.contains roles-list "ROLE_ADM_SUBSCRIBE_SUBSCRIPTION")
      (do (.setSpacing h-layout-3 true)
         (.addComponent h-layout-3 register-button)
         (.addListener register-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                         (buttonClick [event]
                                                      (create-register-component body-v roles-list window)
                                                      )))))
    (.addComponent h-layout-1 form)
    (doto search-text
      (.addStyleName "search-text"))
    (.addStyleName search-button "search-button")
    (if (.contains roles-list "ROLE_ADM_SEARCH_SUBSCRIPTION")
      (create-search-com form combo-select search-text search-button))
    (if (.contains roles-list "ROLE_ADM_VIEW_SUBSCRIPTION")
      (create-table h-layout-2 search-table page-h window))
    (.addComponent search-panel page-h)
    (.addComponent search-panel v-layout)
    (.addListener search-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                  (buttonClick [event]
                                    (.removeAllItems search-table)
                                    (if (= (.getValue search-text) "")
                                      (fill-table-initial search-table page-h window)
                                      (if (= (.getValue combo-select) (i18n "subs.msisdn"))
                                        (do
                                          (fill-table-by-msisdn search-table search-text window roles-list page-h))
                                        (if (= (.getValue combo-select) (i18n "subs.appname"))
                                          (do
                                            (fill-table-by-app-name search-table search-text window roles-list page-h))
                                          (if (= (.getValue combo-select) (i18n "subs.spname"))
                                            (fill-table-by-sp-name search-table search-text window roles-list page-h))))))))))
