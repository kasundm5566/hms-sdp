(ns hms.kite.subscription.customer-care-search-app
  (:use hms.kite.admin.vaadin)
  (:use hms.kite.repo.subscription-repo)
  (:use hms.kite.governance.read-property)
  (:use clojure.tools.logging)
  (:import (com.vaadin.ui Button Window Table))
  (:import (com.vaadin.terminal ExternalResource Sizeable))
  (:import (hms.kite.paging TablePagingComponent))
  (:import (java.text SimpleDateFormat))
  (:import (java.util Date)))

(def cc-search-table-headers
  [["table.appname" String] ["table.msisdn" String] ["table.spname" String] ["cc.table.status" String] ["subscription.start.date" String] ["charging.amount" String] ["charging.frequency" String]
   ["last.charged.amount" String] ["last.charged.date" String] ["instruction.to.unsubscribe" String] ["instruction.to.report.abuse" String]])

(def cc-search-table-sizes
  [["table.appname" 50] ["table.msisdn" 50] ["table.spname" 25] ["cc.table.status" 50] ["subscription.start.date" 50] ["charging.amount" 25] ["charging.frequency" 50] ["last.charged.amount" 25] ["last.charged.date" 30]
   ["instruction.to.unsubscribe" 100] ["instruction.to.report.abuse" 100]])

(def cc-combo-search-names
  [["combo.msisdn" "subs.msisdn"] ["combo.appname" "subs.appname"] ["combo.spname" "subs.spname"]])

(def ^{:private true} search-grid-cols 4)
(def ^{:private true} search-grid-rows 6)

(def ^{:private true} search-table-width "690px")
(def ^{:private true} search-table-hight "350px")
(def ^{:private true} search-panel-width "870px")

;------------------------------------

;(defn concat-app-routing-keys [con]
;  (map #(assoc % :keyword (:keyword (get-app-by-id (get % :app-id)))) con))

(defn- populate-ins-to-unreg [keyword shortcode]
  (str "Send 'unreg " keyword "' to " shortcode))

(defn- populate-ins-to-report-abuse [keyword shortcode]
  (str "Send 'abuse " keyword " [complain]' to " shortcode))

(defn- convert-time-to-string [date]
  (if (nil? date)
      (str (i18n "data.not.available"))
      (.format (SimpleDateFormat. "yyyy-MM-dd HH:mm:ss") (cast Date date))))

(defn- check-for-empty? [resp]
  (if
    (or (nil? resp) (empty? (.toString resp)))
      (str (i18n "data.not.available"))
      resp))

(defn- reload-subs-ad-table [list table window]
  (doseq [con list]
    (.addItem table (into-array Object
                      [(get con :app-name) (get con :msisdn) (get con :coop-user-name) (get con :current-status) (convert-time-to-string (get con :created-date)) (.toString (check-for-empty? (get con :charging-amount))) (check-for-empty? (get con :frequency)) (check-for-empty? (get con :last-charged-amount)) (convert-time-to-string (get con :last-charged-time)) (populate-ins-to-unreg (get con :keyword) (get con :shortcode)) (populate-ins-to-report-abuse (get con :keyword) (get con :shortcode))])
      (get con :_id))))

(defn- summery-subs-listener-fn [table window]
  (proxy [hms.kite.paging.PagingComponentListener] []
    (onReload [start batch]
      (.removeAllItems table)
      (reload-subs-ad-table (fetch-all-subs-collection start batch) table window))))

(defn- page-summery-subs [total rec-per-page table window]
  (let [table-pag (TablePagingComponent. (summery-subs-listener-fn table window) total rec-per-page)]
    (.init table-pag)
    table-pag))

(defn- fill-table-initial [search-table paging-h window]
  (.removeAllComponents paging-h)
  (.addComponent paging-h (page-summery-subs (get-total-number-of-subs nil) 10 search-table window)))

(defn- create-table [h-layout search-table paging-h window]
  (.addComponent h-layout search-table)
  (doto search-table
    (.addStyleName "search-table")
    (.setWidth search-table-width)
    (.setHeight search-table-hight)
    (.setPageLength 10))
  (doseq [con cc-search-table-headers]
    (.addContainerProperty search-table (i18n (first con)) (second con) nil))
  (doseq [con cc-search-table-sizes]
    (.setColumnWidth search-table (first con) (second con)))
  (fill-table-initial search-table paging-h window))

(defn- reload-subs-ad-table-msisdn [list table window]
  (doseq [con list]
    (.addItem table (into-array Object
                      [(get con :app-name) (get con :msisdn) (get con :coop-user-name) (get con :current-status) (convert-time-to-string (get con :created-date)) (.toString (check-for-empty? (get con :charging-amount))) (check-for-empty? (get con :frequency)) (check-for-empty? (get con :last-charged-amount)) (convert-time-to-string (get con :last-charged-time)) (populate-ins-to-unreg (get con :keyword) (get con :shortcode)) (populate-ins-to-report-abuse (get con :keyword) (get con :shortcode))])
      (get con :_id))))

(defn- summery-subs-listener-fn-msisdn [table pattern window]
  (proxy [hms.kite.paging.PagingComponentListener] []
    (onReload [start batch]
      (.removeAllItems table)
      (reload-subs-ad-table-msisdn (fetch-all-subs-msisdn nil pattern start batch) table window))))

(defn- page-summery-subs-msisdn [total rec-per-page table pattern window]
  (let [table-pag (TablePagingComponent. (summery-subs-listener-fn-msisdn table pattern window) total rec-per-page)]
    (.init table-pag)
    table-pag))

(defn- fill-table-by-msisdn [search-table search-text window roles-list page-h]
  (.removeAllComponents page-h)
  (let [search-patten (re-pattern (str "(?i)" (.trim (.getValue search-text))))]
    (.addComponent page-h (page-summery-subs-msisdn
                            (get-total-number-of-subs-by-msisdn nil search-patten) 10 search-table search-patten window))))

(defn- reload-subs-ad-table-app [list table window]
  (doseq [con list]
    (.addItem table (into-array Object
                      [(get con :app-name) (get con :msisdn) (get con :coop-user-name) (get con :current-status) (convert-time-to-string (get con :created-date)) (.toString (check-for-empty? (get con :charging-amount))) (check-for-empty? (get con :frequency)) (check-for-empty? (get con :last-charged-amount)) (convert-time-to-string (get con :last-charged-time)) (populate-ins-to-unreg (get con :keyword) (get con :shortcode)) (populate-ins-to-report-abuse (get con :keyword) (get con :shortcode))])
      (get con :_id))))

(defn- summery-subs-listener-fn-app [table pattern window]
  (proxy [hms.kite.paging.PagingComponentListener] []
    (onReload [start batch]
      (.removeAllItems table)
      (reload-subs-ad-table-app (fetch-all-subs-app-name nil pattern start batch) table window))))

(defn- page-summery-subs-app [total rec-per-page table pattern window]
  (let [table-pag (TablePagingComponent. (summery-subs-listener-fn-app table pattern window) total rec-per-page)]
    (.init table-pag)
    table-pag))

(defn- fill-table-by-app-name [search-table search-text window roles-list page-h]
  (.removeAllComponents page-h)
  (let [search-patten (re-pattern (str "(?i)" (.trim (.getValue search-text))))]
    (.addComponent page-h (page-summery-subs-app
                            (get-total-number-of-subs-by-appname nil search-patten) 10 search-table search-patten window))))

(defn- reload-subs-ad-table-sp [list table window]
  (doseq [con list]
    (.addItem table
      (into-array Object
        [(get con :app-name) (get con :msisdn) (get con :coop-user-name) (get con :current-status) (convert-time-to-string (get con :created-date)) (.toString (check-for-empty? (get con :charging-amount))) (check-for-empty? (get con :frequency)) (check-for-empty? (get con :last-charged-amount)) (convert-time-to-string (get con :last-charged-time)) (populate-ins-to-unreg (get con :keyword) (get con :shortcode)) (populate-ins-to-report-abuse (get con :keyword) (get con :shortcode))])
      (get con :_id))))

(defn- summery-subs-listener-fn-sp [table pattern window]
  (proxy [hms.kite.paging.PagingComponentListener] []
    (onReload [start batch]
      (.removeAllItems table)
      (reload-subs-ad-table-sp (fetch-all-subs-sp-name nil pattern start batch) table window))))

(defn- page-summery-subs-sp [total rec-per-page table pattern window]
  (let [table-pag (TablePagingComponent. (summery-subs-listener-fn-sp table pattern window) total rec-per-page)]
    (.init table-pag)
    table-pag))

(defn- fill-table-by-sp-name [search-table search-text window roles-list page-h]
  (.removeAllComponents page-h)
  (let [search-patten (re-pattern (str "(?i)" (.trim (.getValue search-text))))]
    (.addComponent page-h (page-summery-subs-sp
                            (get-total-number-of-subs-by-spname nil search-patten) 10 search-table search-patten window))))

(defn- create-search-com [form combo-select search-text search-button]
  (let [h (new-h-layout) label (new-label "subscription.search.label")]
    (doto form
      (.addStyleName "subs-search")
      (.setLayout h))
    (doto combo-select
      (.addStyleName "search-combo")
      (.setImmediate true)
      (.setNullSelectionAllowed false)
      (.setWidth "190px"))
    (doseq [con cc-combo-search-names]
      (.addItem combo-select (i18n (second con))))
    (.setValue combo-select (i18n "subs.msisdn"))
    (.addStyleName label "subs-search-label")
    (doto h
      (.setSpacing true)
      (.addComponent label)
      (.addComponent combo-select)
      (.addComponent search-text)
      (.addComponent search-button))))


(defn create-cc-search [body-v window roles-list]
  (log :info (str "Creating admin CC sreach window with" roles-list))
  (let [v-layout (new-v-layout) search-panel (new-panel (i18n "cc.search.panel") "search-panel") search-table (new-table "")
        search-grid (new-grid-layout search-grid-cols search-grid-rows) h-layout (new-h-layout)
        combo-select (new-combo-box "") search-text (new-text-field)
        search-button (new-small-button "subs.search.button") page-h (new-h-layout) form (new-form)
        h-layout-1 (new-h-layout "subs-search-h-layout-1") h-layout-2 (new-h-layout "subs-search-h-layout-2")]
    (.removeAllComponents search-panel)
    (.addComponent body-v search-panel)
    (doto search-panel
      (.setHeight (Sizeable/SIZE_UNDEFINED) 0))
    (.setSizeUndefined (.getContent search-panel))
    (if (and (.contains roles-list "ROLE_ADM_CC_SEARCH_SUBSCRIPTION")
          (.contains roles-list "ROLE_ADM_CC_VIEW_SUBSCRIPTION"))
      (do
        (.addComponent search-panel h-layout-1)
        (.addComponent search-panel h-layout-2)))
    (.addComponent h-layout-1 form)
    (doto search-text
      (.addStyleName "search-text"))
    (.addStyleName search-button "search-button")
    (if (.contains roles-list "ROLE_ADM_CC_SEARCH_SUBSCRIPTION")
      (create-search-com form combo-select search-text search-button))
    (if (.contains roles-list "ROLE_ADM_CC_VIEW_SUBSCRIPTION")
      (create-table h-layout-2 search-table page-h window))
    (.addComponent search-panel page-h)
    (.addComponent search-panel v-layout)
    (.addListener search-button (proxy [com.vaadin.ui.Button$ClickListener] []
                                  (buttonClick [event]
                                    (.removeAllItems search-table)
                                    (if (= (.getValue search-text) "")
                                      (fill-table-initial search-table page-h window)
                                      (if (= (.getValue combo-select) (i18n "subs.msisdn"))
                                        (do
                                          (fill-table-by-msisdn search-table search-text window roles-list page-h))
                                        (if (= (.getValue combo-select) (i18n "subs.appname"))
                                          (do
                                            (fill-table-by-app-name search-table search-text window roles-list page-h))
                                          (if (= (.getValue combo-select) (i18n "subs.spname"))
                                            (fill-table-by-sp-name search-table search-text window roles-list page-h))))))))))