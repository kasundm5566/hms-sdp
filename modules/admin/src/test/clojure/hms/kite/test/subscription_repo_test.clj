(ns hms.kite.test.subscription-repo-test
  (:use [clojure.test] [midje.sweet])
  (:use [karras.core] [karras.collection])
  (:use [hms.kite.repo.subscription-repo])
  (:import (java.util ArrayList)))

(def subscription-test (collection (connect) :subscription :sub_test))

(def s-list (ArrayList.))

(defn- drop-collection-test []
  (drop-collection subscription-test))

(defn- insert-data []
  (insert subscription-test
    {:chargeAmount 0, :mandateId 1, :msisdn "123456", :keyword nil, :shortcode nil,
     :appId "APP1", :currentStatus "REGISTERED", :operator nil, :coop-user-name "sp1", :name "votingtest"}
    {:chargeAmount 0, :mandateId 1, :msisdn "123456", :keyword nil, :shortcode nil,
     :appId "APP2", :currentStatus "REGISTERED", :operator nil, :coop-user-name "sp2", :name "votingtest1"}
    {:chargeAmount 0, :mandateId 1, :msisdn "456789", :keyword nil, :shortcode nil,
     :appId "APP3", :currentStatus "UNREGISTERED",:operator nil, :coop-user-name "sp3", :name "votingtest2"}))

(against-background [(before :contents (drop-collection-test))]
  (fact "add data"
    (insert-data)))

(deftest count-all-sub-collection-test
  (is (= 3 (get-total-number-of-reg-subs subscription-test))))

(deftest count-given-msisdn-test
  (is (= 1 (get-total-number-of-reg-subs-by-msisdn subscription-test "456789"))))

(deftest count-given-appname-test
  (is (= 1 (get-total-number-of-reg-subs-by-appname subscription-test "votingtest"))))

(deftest count-given-spname-test
  (is (= 1 (get-total-number-of-reg-subs-by-spname subscription-test "sp2"))))

(deftest fetch-all-subs-msisdn-test
  (.clear s-list)
  (doseq [con (fetch-all-reg-subs-msisdn subscription-test "123456" 0 0)]
    (.add s-list con))
  (is (= 2 (.size s-list))))

(deftest fetch-all-subs-appname-test
  (.clear s-list)
  (doseq [con (fetch-all-reg-subs-app-name subscription-test "votingtest" 0 0)]
    (.add s-list con))
  (is (= 1 (.size s-list))))

(deftest fetch-all-subs-spname-test
  (.clear s-list)
  (let [search-patten (re-pattern (str "(?i)" "sp") )]
    (doseq [con (fetch-all-subs-sp-name subscription-test search-patten 0 0)]
    (.add s-list con)))
  (is (= 3 (.size s-list))))





