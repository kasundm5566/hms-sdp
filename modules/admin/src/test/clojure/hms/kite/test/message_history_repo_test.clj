(ns hms.kite.test.message-history-repo-test
  (:use [clojure.test] [midje.sweet] [hms.kite.repo.message-history-repo] [clojure.java.jdbc :as sql]))

(def mysql-db {:classname "com.mysql.jdbc.Driver"
               :subprotocol "mysql"
               :subname "//127.0.0.1:3306/kite"
               :user "root"
               :password "123"})

(defn create-table-message-history-test
  []
  (clojure.java.jdbc/create-table
    :message_history_test
    [:app_id "varchar(255)"]
    [:message "varchar(255)"]
    [:correlation_id "varchar(255)"]))

(defn drop-db
  []
  (try
    (clojure.java.jdbc/drop-table :message_history_test)
    (catch Exception _)))

(deftest test-create-table
  (sql/with-connection mysql-db
    (clojure.java.jdbc/with-connection
      mysql-db
      (clojure.java.jdbc/transaction
        (drop-db)
        (create-table-message-history-test)))
          (is (= 0 (sql/with-query-results res ["SELECT * FROM message_history_test"] (count res))))))