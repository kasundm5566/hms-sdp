(ns hms.kite.test.governance-repo-test
  (:use [clojure.test] [midje.sweet])
  (:use [karras.core] [karras.collection])
  (:use [hms.kite.repo.governance-repo])
  (:import (java.util ArrayList)))

(def governance-test (collection (connect) :kite :abuse_report_test))

(def g-list (ArrayList.))

(defn- drop-collection-test []
  (drop-collection governance-test))

(defn- insert-data []
  (insert governance-test
    { :app-id "app1", :coop-user-id  "123", :invalid  0, :name  "test1", :pending 3, :reported 3,
      :sp-id  "SP001", :sp-name "sp1", :valid  0 }
    { :app-id "app2", :coop-user-id  "456", :invalid  0, :name  "test2", :pending 3, :reported 3,
      :sp-id  "SP001", :sp-name "sp2", :valid  0 }
    { :app-id "app3", :coop-user-id  "789", :invalid  0, :name  "test3", :pending 3, :reported 3,
      :sp-id  "SP001", :sp-name "sp3", :valid  0 }))

(against-background [(before :contents (drop-collection-test))]
  (fact "add data"
    (insert-data)))

(deftest count-all-abuse-report-collection-test
  (is (= 3 (get-total-number-of-abuse-reports governance-test))))

(deftest fetch-all-abuse-reports-test
  (.clear g-list)
  (doseq [con (fetch-all-abuse-reports governance-test 0 0)]
    (.add g-list con))
  (is (= 3 (.size g-list))))