(defproject admin "1.0.0-SNAPSHOT"
  :description "Admin - UI"
  :dependencies [[org.clojure/clojure "1.3.0"]
                 [org.clojure/data.json "0.1.1"]
                 [org.mongodb/mongo-java-driver "2.1"]
                 [clj-http "0.3.2"]
                 [org.clojure/tools.logging "0.2.3"]
                 [com.vaadin/vaadin "6.6.0"]
                 [mysql/mysql-connector-java "5.1.18"]
                 [hms.common/rest-util "1.0.6"
                  :exclusions [org.slf4j/slf4j-api]]
                 [org.clojure/java.jdbc "0.0.5"]
                 [javax.servlet/servlet-api "2.5"]
                 [org.apache.cxf/cxf-rt-frontend-jaxrs "2.6.1"
                  :exclusions [org.apache.abdera/abdera-core org.apache.abdera/abdera-extensions-json org.apache.abdera/abdera-parser]]
                 [hms/registration-api "1.1.3"]
                 [org.clojars.hms/karras "0.7.1"]
                 [org.jasig.cas.client/cas-client-core "3.2.2.hms"]
                 [org.apache.commons/commons-email "1.2"]
                 [org.slf4j/slf4j-log4j12 "1.6.1"]
                 [hms.common/hms-common-util "1.0.8"]]
  :dev-dependencies [[uk.org.alienscience/leiningen-war "0.0.13"]]
  :repositories {"sonatype" "https://oss.sonatype.org/content/repositories/snapshots"
                 "snapshots" "http://archiva.hsenidmobile.com/repository/snapshots"
                 "releases" "http://archiva.hsenidmobile.com/repository/internal"}

  :java-source-path "src/main/java"
  :source-path "src/main/clojure"
  :compile-path  "target/classes"
  :library-path  "lib/"
  :resources-path "src/main/resources"

  :aot [hms.kite.admin.admin-application
        hms.kite.admin.admin-servlet
        hms.kite.admin.vaadin
        hms.kite.governance.read-property
        hms.kite.governance.abuse-report
        hms.kite.admin.vcontext-listener
        hms.kite.admin.mongo-connection-filter]

  :war {:name "admin.war"
        :webxml-path "src/main/webapp/WEB-INF/web.xml"
        :web-content "src/main/webapp/html"})
