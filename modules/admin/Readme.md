#Admin UI Installation Instructions

##Required Software
    1. Java 1.7
    2. Maven  3.2.3
    3. Mongodb 2.4.3
    4. Tomcat 6.0.39
    5. MySQL 5.5.17

##Setup Admin UI
    * Add following host name in your /etc/hosts
        db.mongo.admin.sdp
        db.mongo.subscription
        web.cur
        db.mysql.sdp
        core.sdp
        prov.sdp

##Configurations
* Use `sdp/modules/admin/src/main/profile/<profile>/resources/admin.properties` file to edit the properties.

##Deploy Admin UI
    1. Use 'mvn clean install -DskipTests -P<profile>'
       Ex: mvn clean install -DskipTests -Pvodafone
    2. Copy the war file into '<tomcat>/webapps/'
    3. Start the tomcat

##For Developers
*   Please don't use clojure.contrib or any other clojure 1.2 libraries. We have moved every thing into clojure 1.3