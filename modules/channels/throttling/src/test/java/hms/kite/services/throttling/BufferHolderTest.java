/**
 * (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 * All Rights Reserved.
 * <p/>
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 * International (pvt) Limited.
 * <p/>
 * hSenid International (pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.kite.services.throttling;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.annotations.AfterTest;
import static org.testng.Assert.assertEquals;

import java.util.*;

public class BufferHolderTest {

    private BufferHolder bufferHolder;
    private BufferHolder.IterativeConcurrentHashMap itrMap;

    private Map<String, Map<String, Object>> requestContext1;
    private Map<String, Map<String, Object>> requestContext2;
    private Map<String, Map<String, Object>> requestContext3;
    private Map<String, Map<String, Object>> requestContext4;
    private Map<String, Map<String, Object>> requestContext5;
    private Map<String, Map<String, Object>> requestContext6;
    private Map<String, Map<String, Object>> requestContext7;
    private Map<String, Map<String, Object>> requestContext8;

    @BeforeMethod
    public void setup() throws BufferLimitExceededException {
        requestContext1 = new HashMap<String, Map<String, Object>>();
        requestContext1.put("requestK1", Collections.<String, Object>singletonMap("correlationIdK1", "101"));
        requestContext2 = new HashMap<String, Map<String, Object>>();
        requestContext2.put("requestK2", Collections.<String, Object>singletonMap("correlationIdK2", "102"));
        requestContext3 = new HashMap<String, Map<String, Object>>();
        requestContext3.put("requestK3", Collections.<String, Object>singletonMap("correlationIdK3", "103"));
        requestContext4 = new HashMap<String, Map<String, Object>>();
        requestContext4.put("requestK4", Collections.<String, Object>singletonMap("correlationIdK4", "104"));
        requestContext5 = new HashMap<String, Map<String, Object>>();
        requestContext5.put("requestK5", Collections.<String, Object>singletonMap("correlationIdK5", "105"));
        requestContext6 = new HashMap<String, Map<String, Object>>();
        requestContext6.put("requestK6", Collections.<String, Object>singletonMap("correlationIdK6", "106"));
        requestContext7 = new HashMap<String, Map<String, Object>>();
        requestContext7.put("requestK7", Collections.<String, Object>singletonMap("correlationIdK7", "107"));
        requestContext8 = new HashMap<String, Map<String, Object>>();
        requestContext8.put("requestK8", Collections.<String, Object>singletonMap("correlationIdK8", "108"));

        bufferHolder = new BufferHolder();
        itrMap = bufferHolder.new IterativeConcurrentHashMap();
        bufferHolder.setBufferLimit(5);

        bufferHolder.createBuffer("Buffer1", requestContext1, 10);
        bufferHolder.addMessage("Buffer1", requestContext2);
        bufferHolder.addMessage("Buffer1", requestContext5);
        bufferHolder.addMessage("Buffer1", requestContext6);

        bufferHolder.createBuffer("Buffer2", requestContext3, 10);
        bufferHolder.addMessage("Buffer2", requestContext4);
    }

    //Testing addMessage() method with different circumstances.
    @Test
    public void testAddMessage() throws BufferLimitExceededException {
        assertEquals(bufferHolder.addMessage("Buffer2", requestContext5), true);
        assertEquals(bufferHolder.addMessage("Buffer3", requestContext6), false);

        assertEquals(bufferHolder.getBuffers().get("Buffer2").getMessages().get(2), requestContext5);
        assertEquals(bufferHolder.getBuffers().containsKey("Buffer3"), false);
    }

    @Test(expectedExceptions = BufferLimitExceededException.class)
    public void testExceptionAddMessage() throws BufferLimitExceededException {
        bufferHolder.addMessage("Buffer1", requestContext7);
        bufferHolder.addMessage("Buffer1", requestContext8);
    }

    //Testing createBuffer() method with different circumstances.
    @Test
    public void testCreateBuffer() throws BufferLimitExceededException {
        bufferHolder.createBuffer("Buffer3", requestContext5, 10);
        bufferHolder.createBuffer("Buffer3", requestContext6, 10);

        assertEquals(bufferHolder.getBuffers().get("Buffer3").getMessages().get(0), requestContext5);
        assertEquals(bufferHolder.getBuffers().get("Buffer3").getMessages().get(1), requestContext6);
    }

    @Test(expectedExceptions = BufferLimitExceededException.class)
    public void testExceptionCreateBuffer() throws BufferLimitExceededException {
        bufferHolder.createBuffer("Buffer1", requestContext7, 10);
        bufferHolder.createBuffer("Buffer1", requestContext8, 10);
    }

    // Testing popMessage() method with different circumstances.
    @Test(expectedExceptions = NullPointerException.class)
    public void testPopMessageWithoutBuffer() throws RequestedAmountIsTooLargeException {
        List<Map<String, Map<String, Object>>> messageList = bufferHolder.popMessage("Buffer3", 4);
        Map m = messageList.get(0);
    }

    @Test(expectedExceptions = RequestedAmountIsTooLargeException.class)
    public void testPopMessageWithoutMessages() throws RequestedAmountIsTooLargeException {
        Buffer buffer = new Buffer("Buffer4", requestContext1, 10, 5);
        bufferHolder.getBuffers().put("Buffer4", buffer);
        bufferHolder.getBuffers().get("Buffer4").getMessages().clear();

        List<Map<String, Map<String, Object>>> messageList = bufferHolder.popMessage("Buffer4", 4);
    }

    @Test(expectedExceptions = RequestedAmountIsTooLargeException.class)
    public void testPopMessageWithLesserMessages() throws RequestedAmountIsTooLargeException, BufferLimitExceededException {
        List messageList = bufferHolder.popMessage("Buffer1", 5);
    }

    @Test
    public void testPopMessageWithMoreMessages() throws RequestedAmountIsTooLargeException {
        List messageList = bufferHolder.popMessage("Buffer1", 2);

        assertEquals(messageList.get(0), requestContext1);
        assertEquals(messageList.get(1), requestContext2);
        assertEquals(bufferHolder.getBuffers().containsKey("Buffer1"), true);
    }

    @Test
    public void testPopMessageSuccess() throws RequestedAmountIsTooLargeException {
        List messageList = bufferHolder.popMessage("Buffer1", 4);

        assertEquals(messageList.get(0), requestContext1);
        assertEquals(messageList.get(1), requestContext2);
        assertEquals(messageList.get(2), requestContext5);
        assertEquals(messageList.get(3), requestContext6);
        assertEquals(bufferHolder.getBuffers().containsKey("Buffer1"), false);
    }

    // Testing getNextBuffer() method with different circumstances.
    @Test
    public void testGetNextBuffer() throws BufferLimitExceededException {
        bufferHolder.createBuffer("Buffer3", requestContext3, 10);
        bufferHolder.createBuffer("Buffer4", requestContext4, 10);
        bufferHolder.createBuffer("Buffer5", requestContext5, 10);

        assertEquals(itrMap.getNextBuffer(), bufferHolder.getBuffers().get("Buffer1"));
        assertEquals(itrMap.getNextBuffer(), bufferHolder.getBuffers().get("Buffer2"));
        assertEquals(itrMap.getNextBuffer(), bufferHolder.getBuffers().get("Buffer3"));
        assertEquals(itrMap.getNextBuffer(), bufferHolder.getBuffers().get("Buffer4"));
        assertEquals(itrMap.getNextBuffer(), bufferHolder.getBuffers().get("Buffer5"));

        //Checking again whether Round Robin principle is happening.
        assertEquals(itrMap.getNextBuffer(), bufferHolder.getBuffers().get("Buffer1"));
        assertEquals(itrMap.getNextBuffer(), bufferHolder.getBuffers().get("Buffer2"));
        assertEquals(itrMap.getNextBuffer(), bufferHolder.getBuffers().get("Buffer3"));
        assertEquals(itrMap.getNextBuffer(), bufferHolder.getBuffers().get("Buffer4"));
        assertEquals(itrMap.getNextBuffer(), bufferHolder.getBuffers().get("Buffer5"));

        //Remove is done during the call of getNextBuffer method.
        bufferHolder.getBuffers().remove("Buffer3");
        bufferHolder.getBuffers().remove("Buffer4");
        assertEquals(itrMap.getNextBuffer(), bufferHolder.getBuffers().get("Buffer1"));
        assertEquals(itrMap.getNextBuffer(), bufferHolder.getBuffers().get("Buffer2"));
        assertEquals(itrMap.getNextBuffer(), bufferHolder.getBuffers().get("Buffer5"));
    }

    @AfterTest
    public void tearDown() {
        requestContext1 = null;
        requestContext2 = null;
        requestContext3 = null;
        requestContext4 = null;
        requestContext5 = null;
        requestContext6 = null;
        requestContext7 = null;
        requestContext8 = null;

        bufferHolder = null;
        itrMap = null;
    }

}
