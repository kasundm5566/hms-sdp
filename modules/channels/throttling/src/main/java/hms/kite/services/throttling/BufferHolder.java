/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.services.throttling;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class BufferHolder {

    private IterativeConcurrentHashMap<String, Buffer> buffers =
            new IterativeConcurrentHashMap<String, Buffer>();
    private int bufferLimit;
    private List<String> bufferList = new ArrayList<String>();

    /**
     * If there exists a buffer belong to a specific message type(value), this method add that message to that
     * buffer and returns true. If not returns false.
     *
     * @param key
     * @param message
     * @return boolean
     * @throws BufferLimitExceededException
     */
    public boolean addMessage(String key, Map<String, Map<String, Object>> message) throws BufferLimitExceededException {
        if(buffers.containsKey(key)) {
            Buffer buffer = buffers.get(key);
            //Check whether buffer is null or not
            if(buffer != null) {
                synchronized (buffer) {
                    //Verify whether there is a buffer for the key at this stage.
                    //The buffer might have been removed from the buffers by another thread => buffer is empty.
                    if(buffers.containsKey(key)) {
                        if(buffer.getBufferSize() < buffer.getBufferLimit()) {
                            buffer.addMessage(message);
                            return true;
                        } else {
                            throw new BufferLimitExceededException();
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * This will create a new buffer for a specific application if there isn't any. When storing this buffer
     * to buffers some concurrent behaviour may lead to return not null buffer from the put method. In that case
     * put method handle that situation if there does not occur BufferLimitExceededException. If that occurs
     * method will again call createBuffer method.
     * @param key
     * @param message
     * @param maxTps
     * @throws BufferLimitExceededException
     */
    public void createBuffer(String key, Map<String, Map<String, Object>> message, int maxTps)
            throws BufferLimitExceededException {
        Buffer buffer;
        if(!buffers.containsKey(key)) {
            buffer = new Buffer(key, message, maxTps, bufferLimit);
            if(buffers.put(key, buffer) != null){
                throw new BufferLimitExceededException();
            }
        } else {
            buffer = buffers.get(key);
            if(buffer != null) {
                synchronized (buffer) {
                    if(buffers.containsKey(key)) {
                        if(buffer.getBufferSize() < buffer.getBufferLimit()) {
                            buffer.addMessage(message);
                        } else {
                            throw new BufferLimitExceededException();
                        }
                    } else {
                        createBuffer(key, message, maxTps);
                    }
                }
            } else {
                createBuffer(key, message, maxTps);
            }
        }
    }

    /**
     * This method pop-up given amount of messages from given buffer and return those messages as a list.
     * If the amount is too large this will throws RequestedAmountIsTooLargeException.
     * @param key
     * @param noOfMessages
     * @return List
     * @throws RequestedAmountIsTooLargeException
     */
    public List<Map<String, Map<String, Object>>> popMessage(String key, int noOfMessages) throws RequestedAmountIsTooLargeException {
        if(buffers.containsKey(key)) {
            List<Map<String, Map<String, Object>>> messageList = new ArrayList<Map<String, Map<String, Object>>>();
            Buffer buffer = buffers.get(key);
            CopyOnWriteArrayList<Map<String, Map<String, Object>>> messages = buffer.getMessages();
            int count = 0;
            while(!messages.isEmpty() && (count < noOfMessages)) {
                messageList.add(messages.remove(0));
                count++;
                if(messages.isEmpty()) {
                    synchronized (buffer) {
                        if(buffer.isBufferEmpty()) {
                            buffers.remove(key);
                        }
                    }
                }
            }
            if (count < noOfMessages) {
                count = 0;
                throw new RequestedAmountIsTooLargeException();
            }
            count = 0;
            return messageList;
        }
        return null;
    }

    /**
     * This inner class override put and remove method of ConcurrentHashMap class in order to maintain a list
     * of current buffers. This also has getNextBuffer method to select a buffer to continue messages
     * according to Round Robin algorithm.
     * @param <K>
     * @param <V>
     */
    class IterativeConcurrentHashMap<K, V> extends ConcurrentHashMap<K, V> {

        private AtomicInteger bufferListIndex = new AtomicInteger(0);  // Index is starting from 1

        public Buffer getNextBuffer() {
            if(bufferList.size() > 0) {
                bufferListIndex.set((bufferList.size() > bufferListIndex.get()) ? bufferListIndex.get() + 1: 1);
                int index = bufferListIndex.get();
                String bufferName = bufferList.get(index - 1);    // unit test
                return buffers.get(bufferName);
            }
            else {
                return null;
            }
        }


        /**
         * This method overrides the put method of ConcurrentHashMap.
         * If there are multiple buffer creations, some buffers may get lost. In order to avoid that we
         * check for previous buffer and if there exists any we add previous buffer messages to the current buffer.
         * If the buffer limit exceeded this method retain previous buffer and return new buffer.
         * @param key
         * @param value
         * @return V
         */
        public V put(K key, V value) {
            //todo
            //check the synchronization with hashcode or ..?
            bufferList.add((String)key);
            Buffer buffer = (Buffer) value;
            Buffer bufferPrevious = (Buffer) super.put(key, value);
            if(bufferPrevious != null) {
                // Newer buffer should have only one message object. Therefore previous buffer should have at least
                // one place to add it. If there is no space this method retain previous buffer and return new buffer.
                if(bufferPrevious.getBufferSize() < bufferPrevious.getBufferLimit() - 1) {
                    bufferPrevious.getMessages().addAll(buffer.getMessages());
                }
                return super.put(key, (V) bufferPrevious);
            }
            return (V) bufferPrevious;
        }

        /**
         * This method overrides the "public V remove(Object key)" method of ConcurrentHashMap and maintain
         * buffer list's index when removing buffers from buffer holder. If removing item is before current
         * index it will decrement by one. Else nothing happens.
         * @param key
         * @return V
         */
        public V remove(Object key) {
            int index = bufferList.indexOf(key);
            if (index != -1) {
                if(bufferList.remove(key)) {
                    bufferListIndex.set(bufferListIndex.get() >= index ?
                            bufferListIndex.get() - 1 : bufferListIndex.get());
                    return super.remove(key);
                }
            }
            return null;
        }
    }

    public void setBufferLimit(int bufferLimit) {
        this.bufferLimit = bufferLimit;
    }

    public IterativeConcurrentHashMap<String, Buffer> getBuffers() {
        return buffers;
    }

    public void setBuffers(IterativeConcurrentHashMap<String, Buffer> buffers) {
        this.buffers = buffers;
    }
}