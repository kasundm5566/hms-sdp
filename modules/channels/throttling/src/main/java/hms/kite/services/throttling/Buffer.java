/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.services.throttling;

import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public class Buffer {

    private int maxTps;
    private String bufferName;
    private int bufferLimit;
    private CopyOnWriteArrayList<Map<String, Map<String, Object>>> messages;

    public Buffer(String bufferName, Map<String, Map<String, Object>> value, int maxTps, int bufferLimit) {
        this.bufferLimit = bufferLimit;
        this.maxTps = maxTps;
        this.bufferName = bufferName;
        messages = new CopyOnWriteArrayList<Map<String, Map<String, Object>>>();
        messages.add(value);
    }

    public boolean addMessage(Map<String, Map<String, Object>> message) {
        return messages.add(message);
    }

    public int getBufferSize() {
        return messages.size();
    }

    public boolean isBufferEmpty() {
        return (messages.size() == 0);
    }

    // Getter setter methods
    public int getBufferLimit() {
        return bufferLimit;
    }

    public CopyOnWriteArrayList<Map<String, Map<String, Object>>> getMessages() {
        return messages;
    }

    public int getMaxTps() {
        return maxTps;
    }

    public String getBufferName() {
        return bufferName;
    }

    public void setBufferLimit(int bufferLimit) {
        this.bufferLimit = bufferLimit;
    }

    public void setMessages(CopyOnWriteArrayList<Map<String, Map<String, Object>>> messages) {
        this.messages = messages;
    }

    public void setMaxTps(int maxTps) {
        this.maxTps = maxTps;
    }

    public void setBufferName(String bufferName) {
        this.bufferName = bufferName;
    }
}