package hms.kite.services.throttling;

/**
 * (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 * All Rights Reserved.
 * <p/>
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 * International (pvt) Limited.
 * <p/>
 * hSenid International (pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
public class RequestedAmountIsTooLargeException extends Exception {
    public RequestedAmountIsTooLargeException() {
        super("Requested amount is too large");
    }
}