/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.services.mnp;

import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class NumberCheckingService extends BaseChannel {

    private static final Logger logger = LoggerFactory.getLogger(NumberCheckingService.class);

    private Properties properties = null;
    private int[] localNumberLengths;
    private int[] intlNumberLengths;


    public NumberCheckingService(String propertyFile) {
        init(propertyFile);
    }

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        logger.trace("Received number checking request [{}]", requestContext.get(requestK));
        Map<String, Object> res = ResponseBuilder.generateSuccess();

        checkRecipientAddressList(requestContext);
        checkSenderAddress(requestContext);
        checkMsisdn(requestContext);

        logger.trace("Sent number checking response [{}] for request [{}]", res, requestContext.get(requestK));
        return res;
    }

    public String findOperator(String msisdn) throws NumberCheckingException {
        if (null == msisdn || msisdn.isEmpty() || !(msisdn.matches("\\+?([0-9]+)"))) {
            throw new NumberCheckingException("The Number Format Error");
        }
        String number = convertToLocalNumber(msisdn);
        for (Map.Entry entry : properties.entrySet()) {
            if (number.matches((String) entry.getKey())) {
                return (String) entry.getValue();
            }
        }
        return unknownK;
    }

    public String findSubscriberType(String msisdn) throws NumberCheckingException {
        //TODO right now the logic not done yet
        return anyK;
    }

    public void init(String fileName) {
        try {
            properties = new Properties();
            final InputStream inStream = NumberCheckingService.class.getClassLoader().getResourceAsStream(fileName);
            properties.load(inStream);
        } catch (Throwable e) {
            logger.error("Unable to initialize number checking props", e);
            throw new RuntimeException("Unable to load number ranges information from [" + fileName + "]", e);
        }

    }

    public String convertToLocalNumber(String number) {
        //Check whether the number is international.
        final int length = number.length();
        if (isLocalLength(length)) {
            return number;
        }
        //Check whether the number is local.
        if (isIntlLength(length)) {
            //TODO
        }

        return number;

    }

    public boolean isLocalLength(int length) {
        if (localNumberLengths == null) {
            return false;
        }
        for (int len : localNumberLengths) {
            if (length == len) {
                return true;
            }
        }
        return false;
    }

    public boolean isIntlLength(int length) {
        for (int len : intlNumberLengths) {
            if (length == len) {
                return true;
            }
        }
        return false;
    }

    private void checkSenderAddress(Map<String, Map<String, Object>> requestContext) {
        final String senderNumber = (String) requestContext.get(requestK).get(senderAddressK);
        Map<String, String> parameters = new HashMap<String, String>();

        try {
            parameters.put(senderAddressOptypeK, findOperator(senderNumber));
            parameters.put(senderAddressSubtypeK, findSubscriberType(senderNumber));
            if (!unknownK.equals(parameters.get(senderAddressOptypeK))) {
                parameters.put(senderAddressStatusK, successCode);
            } else {
                parameters.put(senderAddressStatusK, invalidMsisdnErrorCode);
            }
        } catch (NumberCheckingException e) {
            parameters.put(senderAddressOptypeK, unknownK);
            parameters.put(senderAddressSubtypeK, anyK);
            parameters.put(senderAddressStatusK, invalidMsisdnErrorCode);
        }

        requestContext.get(requestK).putAll(parameters);
    }

    private void checkMsisdn(Map<String, Map<String, Object>> requestContext) {
        Map<String, String> parameters = new HashMap<String, String>();

        try {
            final String msisdn = (String) requestContext.get(requestK).get(msisdnK);
            if (null != msisdn && !msisdn.isEmpty()) {
                parameters.put(msisdnOptypeK, findOperator(msisdn));
                parameters.put(msisdnSubypeK, findSubscriberType(msisdn));
                if (!unknownK.equals(parameters.get(msisdnOptypeK))) {
                    parameters.put(msisdnStatusK, successCode);
                } else {
                    parameters.put(msisdnStatusK, invalidMsisdnErrorCode);
                }
            }
        } catch (NumberCheckingException e ) {
            parameters.put(msisdnOptypeK, unknownK);
            parameters.put(msisdnSubypeK, anyK);
            parameters.put(senderAddressStatusK, invalidMsisdnErrorCode);
        }
    }

    private void checkRecipientAddressList(Map<String, Map<String, Object>> requestContext) {
        if (requestContext.get(requestK).containsKey(recipientsK)) {

            List<Map<String, String>> recipientList = (List<Map<String, String>>) requestContext.get(requestK).get(recipientsK);

            for (final Map<String, String> recipient : recipientList) {
                if (!recipient.containsKey(recipientAddressStatusK)
                        || successCode.equals(recipient.get(recipientAddressStatusK))) {
                    try {
                        recipient.put(recipientAddressOptypeK, findOperator(recipient.get(recipientAddressK)));
                        recipient.put(recipientAddressSubtypeK, findSubscriberType(recipient.get(recipientAddressK)));
                        if (!unknownK.equals(recipient.get(recipientAddressOptypeK))) {
                            recipient.put(recipientAddressStatusK, successCode);
                        } else {
                            recipient.put(recipientAddressStatusK, invalidMsisdnErrorCode);
                        }
                    } catch (NumberCheckingException e ) {
                        recipient.put(recipientAddressOptypeK, unknownK);
                        recipient.put(recipientAddressSubtypeK, anyK);
                        recipient.put(recipientAddressStatusK, invalidMsisdnErrorCode);
                    }
                }
            }
        }
    }

    public int[] getLocalNumberLengths() {
        return localNumberLengths;
    }

    public void setLocalNumberLengths(int... localNumberLengths) {
        this.localNumberLengths = localNumberLengths;
    }

    public int[] getIntlNumberLengths() {
        return intlNumberLengths;
    }

    public void setIntlNumberLengths(int... intlNumberLengths) {
        this.intlNumberLengths = intlNumberLengths;
    }
}
