/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.services.mnp;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class NumberCheckingServiceTest {
    private NumberCheckingService numberCheckingService;

    @BeforeTest
    public void setup() {
        numberCheckingService = new NumberCheckingService("operator_types.properties");
        numberCheckingService.setIntlNumberLengths(10);
        numberCheckingService.setLocalNumberLengths(5);
    }

    @Test
    public void testSend() {
        final Map<String, Object> request = new HashMap<String, Object>();
        request.put(senderAddressK, "07099992");
        List<Map<String, String>> recipients = new ArrayList<Map<String, String>>();
        Map<String, String> recipient1 = new HashMap<String, String>();
        recipient1.put(recipientAddressK, "07599992");
        recipients.add(recipient1);
        request.put(recipientsK, recipients);

        Map<String, Object> response = numberCheckingService.send(request);
        assertEquals(request.get(senderAddressOptypeK), "safaricom");
        assertEquals(recipient1.get(recipientAddressOptypeK), "yu");

        recipient1 = new HashMap<String, String>();
        recipient1.put(recipientAddressK, "07099999");
        Map<String, String> recipient2 = new HashMap<String, String>();
        recipient2.put(recipientAddressK, "07599999");
        Map<String, String> recipient3 = new HashMap<String, String>();
        recipient3.put(recipientAddressK, "07399999");
        Map<String, String> recipient4 = new HashMap<String, String>();
        recipient4.put(recipientAddressK, "07699992");
        recipients.add(recipient1);
        recipients.add(recipient2);
        recipients.add(recipient3);
        recipients.add(recipient4);
        request.put(recipientsK, recipients);

        response = numberCheckingService.send(request);
        assertEquals(recipient1.get(recipientAddressOptypeK), "safaricom");
        assertEquals(recipient2.get(recipientAddressOptypeK), "yu");
        assertEquals(recipient3.get(recipientAddressOptypeK), "airtel");
        assertEquals(recipient4.get(recipientAddressOptypeK), unknownK);
    }

    @Test
    public void testFindOperator() {
        try {
            String[] numbers = {"07099999", "07099992", "07019999", "07119999", "07219999", "07209999"};
            for (String number : numbers) {
                assertEquals(numberCheckingService.findOperator(number), "safaricom");
            }
        } catch (NumberCheckingException unexpected) {
            assertNull(unexpected);
        }

        try {
            String[] numbers = {"07399999", "07399992", "07319999"};
            for (String number : numbers) {
                assertEquals(numberCheckingService.findOperator(number), "airtel");
            }
        } catch (NumberCheckingException unexpected) {
            assertNull(unexpected);
        }

        try {
            String[] numbers = {"07599999", "07599992", "07519999"};
            for (String number : numbers) {
                assertEquals(numberCheckingService.findOperator(number), "yu");
            }
        } catch (NumberCheckingException unexpected) {
            assertNull(unexpected);
        }

        try {
            String[] numbers = {"07799999", "07799992", "07719999"};
            for (String number : numbers) {
                assertEquals(numberCheckingService.findOperator(number), "orange");
            }
        } catch (NumberCheckingException unexpected) {
            assertNull(unexpected);
        }
        try {
            String[] numbers = {"07499999", "07699992", "07819999"};
            for (String number : numbers) {
                assertEquals(numberCheckingService.findOperator(number), unknownK);
            }
        } catch (NumberCheckingException unexpected) {
            assertNull(unexpected);
        }
        try {
            String[] numbers = {"+947499999", "07699992", "07819999"};
            for (String number : numbers) {
                assertEquals(numberCheckingService.findOperator(number), unknownK);
            }
        } catch (NumberCheckingException unexpected) {
            assertNull(unexpected);
        }
        // Add your code here
    }
}
