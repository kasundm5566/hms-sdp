/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.services.standard;

import hms.kite.datarepo.RepositoryServiceRegistry;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteErrorBox.msisdnIsBlacklistedErrorCode;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.wfengine.impl.ResponseBuilder.generateSuccess;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */

public class BlacklistingService implements Channel {

    private static final Logger logger = LoggerFactory.getLogger(BlacklistingService.class);

    private String systemBlacklistId = "system-blacklist";

    public Map<String, Object> send(Map<String, Object> request) {
        //no impl needed
        return null;
    }

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        if (!senderAddressValidated(requestContext)) {
            return ResponseBuilder.generate(msisdnIsBlacklistedErrorCode, false, requestContext);
        }

        validateRecipientAddresses(requestContext);
        return generateSuccess();
    }

    private void validateRecipientAddresses(Map<String, Map<String, Object>> requestContext) {
        if (!requestContext.get(requestK).containsKey(recipientsK)) {
            return;
        }

        List<Map<String, String>> recipientList = (List<Map<String, String>>) requestContext.get(requestK).get(recipientsK);
        List<String> blackList = (List<String>) requestContext.get(appK).get(blackListK);

        for (Map<String, String> recipient : recipientList) {
            String recipientAddress = recipient.get(recipientAddressK);
            if (null != recipientAddress &&
                    RepositoryServiceRegistry.blacklistRepository().isExist(systemBlacklistId, recipientAddress)) {
                recipient.put(recipientAddressStatusK, msisdnIsBlacklistedErrorCode);
                logger.info("Recipient Address {} blocked from system blacklist",recipientAddress);
            }
            for (String prefix : blackList) {
                if (!prefix.isEmpty() && recipientAddress.startsWith(prefix)) {
                    recipient.put(recipientAddressStatusK, msisdnIsBlacklistedErrorCode);
                    logger.info("Recipient Address {} blocked in app blacklist",recipientAddress);
                }
            }
        }
    }

    private boolean senderAddressValidated(Map<String, Map<String, Object>> requestContext) {
        if (null != requestContext.get(requestK).get(senderAddressK) &&
                (!sourceAddressValidatedInSystemBlackList(requestContext) ||
                !senderAddressValidatedInAppBlackList(requestContext))) {
            return false;
        }

        return true;
    }

    private boolean senderAddressValidatedInAppBlackList(Map<String, Map<String, Object>> requestContext) {
        final String senderAddress = (String) requestContext.get(requestK).get(senderAddressK);
        List<String> blackList = (List<String>) requestContext.get(appK).get(blackListK);
        for (String prefix : blackList) {
            if (!prefix.isEmpty() && senderAddress.startsWith(prefix)) {
                logger.info("Sender Address {} blocked in app blacklist",senderAddress);
                return false;
            }
        }
        return true;
    }

    private boolean sourceAddressValidatedInSystemBlackList(Map<String, Map<String, Object>> requestContext) {
        final String senderAddress = (String) requestContext.get(requestK).get(senderAddressK);
        if (RepositoryServiceRegistry.blacklistRepository().isExist(systemBlacklistId, senderAddress)) {
            logger.info("Source Address {} blocked from system blacklist", senderAddress);
            return false;
        }

        return true;
    }

    private boolean isEmpty(String string) {
        return null == string || string.isEmpty();
    }

    public void setSystemBlacklistId(String systemBlacklistId) {
        this.systemBlacklistId = systemBlacklistId;
    }
}
