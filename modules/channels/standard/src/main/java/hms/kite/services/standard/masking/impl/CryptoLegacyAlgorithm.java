/*
 * (C) Copyright 2010-2014. hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.kite.services.standard.masking.impl;

import hms.kite.services.standard.masking.CryptoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CryptoLegacyAlgorithm implements CryptoService {

    private static final Logger logger = LoggerFactory.getLogger(CryptoLegacyAlgorithm.class);

    private Map<String, String> encryptionKeys;
    private Map<String, List<Cipher>> ciphers;
    private static final String UTF = "UTF8";
    private static final int KEY_LENGTH = 5;
    private static final int CIPHER_INDEX = 0;
    private static final int DECIPHER_INDEX = 1;

    public void init() {
        ciphers = new HashMap<String, List<Cipher>>();
        for (Map.Entry<String, String> entry : encryptionKeys.entrySet()) {
            ciphers.put(entry.getKey(), init(new SecretKeySpec(entry.getValue().getBytes(), "DESede")));
        }
    }

    private List<Cipher> init(SecretKey key) {
        ArrayList<Cipher> list = new ArrayList<Cipher>();
        try {
            Cipher ecipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
            Cipher dcipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
            ecipher.init(Cipher.ENCRYPT_MODE, key);
            dcipher.init(Cipher.DECRYPT_MODE, key);
            list.add(ecipher);
            list.add(dcipher);
        } catch (Exception e) {
            logger.error("Error in initialising encryption stranded", e);
            throw new RuntimeException("Error in initialising encryption stranded", e);
        }
        return list;
    }

    public String encrypt(String msisdn, String appId) {
        logger.debug("MSISDN value for Encryption [{}]", msisdn);
        if (msisdn != null) {
            StringBuilder encryptStr = appendSecretKey(msisdn);
            try {
                for (Map.Entry<String, List<Cipher>> entry : ciphers.entrySet()) {
                    byte[] enc = entry.getValue().get(CIPHER_INDEX).doFinal(encryptStr.toString().getBytes(UTF));
                    String encryptValue = entry.getKey() + (new BASE64Encoder().encode(enc));
                    logger.debug("Encrypted msisdn [{}] to [{}]", msisdn, encryptValue);
                    return encryptValue.replace("+", "_");
                }
                throw new RuntimeException("No encryption format found..");
            } catch (Exception e) {
                logger.error("Error encrypting [" + msisdn + "]", e);
                throw new RuntimeException("Error encrypting [" + msisdn + "]", e);
            }
        } else {
            return "";
        }
    }

    /**
     * Decrypts a value which was encrypted using the encrypt method.
     *
     * @param msisdn
     *            value to be encrypted.
     * @return decypted value.
     * @throws Exception
     */
    public String decrypt(String msisdn, String appId) {
        logger.debug("Encrypted value for decryption [{}]", msisdn);
        msisdn = msisdn.replace("_", "+");
        String key = msisdn.substring(0, KEY_LENGTH);
        msisdn = msisdn.substring(KEY_LENGTH, msisdn.length());
        String decodedString;
        try {
            byte[] dec = new BASE64Decoder().decodeBuffer(msisdn);
            if (ciphers.containsKey(key)) {
                decodedString = new String(ciphers.get(key).get(DECIPHER_INDEX).doFinal(dec), UTF);
            } else {
                throw new RuntimeException("Invalid Encryption format, key not recognised [" + key + "]");
            }
        } catch (Exception e) {
            logger.error("Error encrypting [" + msisdn + "]", e);
            throw new RuntimeException("Error encrypting [" + msisdn + "]", e);
        }
        StringBuilder returnValue = removeKeys(decodedString);
        logger.debug("Decrypted msisdn [{}] to [{}]", msisdn, returnValue);
        return returnValue.toString();

    }

    private StringBuilder appendTime(String msisdn) {
        StringBuilder encryptStr = new StringBuilder("");
        final String[] currentTime = new StringBuilder(String.valueOf(System.currentTimeMillis())).reverse().toString()
                .split("|");
        String[] split = msisdn.split("|");

        for (int i = 0; i < split.length; i++) {
            String character = split[i];
            if (currentTime.length > i) {
                encryptStr.append(currentTime[i]).append(character);
            } else {
                encryptStr.append(currentTime[1]).append(character);
            }
        }
        return encryptStr;
    }

    private StringBuilder appendSecretKey(String msisdn) {
        StringBuilder encryptStr = new StringBuilder("");
        final String[] key = "yu$*(&u@#$(%76(&#867!~#&JGH*(h}{*".split("|");
        String[] split = msisdn.split("|");

        for (int i = 0; i < split.length; i++) {
            String character = split[i];
            if (key.length - 1 > i) {
                if (i % 3 == 0) {
                    encryptStr.append(key[i]).append(character);
                } else {
                    encryptStr.append(key[i + 2]).append(character);
                }
            } else {
                encryptStr.append("G").append(character);
            }
        }
        return encryptStr;
    }

    private StringBuilder removeKeys(String decodedString) {
        StringBuilder returnValue = new StringBuilder("");
        String[] split = decodedString.split("|");
        for (int i = 0; i < split.length; i++) {
            if (i % 2 == 0) {
                returnValue.append(split[i]);
            }
        }
        return returnValue;
    }

    public void setEncryptionKeys(Map<String, String> encryptionKeys) {
        this.encryptionKeys = encryptionKeys;
    }

}
