/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.services.standard;

import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteErrorBox.msisdnIsNotInWhiteListErrorCode;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.wfengine.impl.ResponseBuilder.generateSuccess;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class WhitelistService implements Channel {

    private static final Logger logger = LoggerFactory.getLogger(WhitelistService.class);


    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        Object whiteList = requestContext.get(appK).get(whiteListK);
        if (whiteList instanceof List) {
            return validateWhiteList((List<String>)whiteList, requestContext);
        }
        return generateSuccess();
    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        //no impl needed
        return generateSuccess();
    }

    private Map<String, Object> validateWhiteList(List<String> list, Map<String, Map<String, Object>> requestContext) {
        final String senderAddress = (String) requestContext.get(requestK).get(senderAddressK);
        List<Map<String, String>> recipientList = (List<Map<String, String>>) requestContext.get(requestK).get(recipientsK);

        if (!checkAddress(senderAddress, list)) {
            logger.info("Sender address {} is not in white list",senderAddress);
            requestContext.get(requestK).put(senderAddressStatusK, msisdnIsNotInWhiteListErrorCode);
        }

        if (null == recipientList ) {
            return ResponseBuilder.generateSuccess();
        }

        for (Map<String, String> recipient : recipientList) {
            String recipientAddress = recipient.get(recipientAddressK);
            if (!checkAddress(recipientAddress, list)) {
                recipient.put(recipientAddressStatusK, msisdnIsNotInWhiteListErrorCode);
                logger.info("Recipient Address {} is not in white list",recipientAddress);
            }
        }
        return generateSuccess();
    }

    private boolean checkAddress(String address, List<String> list) {
        if (list.contains(address)) {
            return true;
        }
        return false;
    }
}
