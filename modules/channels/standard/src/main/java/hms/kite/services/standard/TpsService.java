/**
 *   (C) Copyright 2012-2013 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.services.standard;

import hms.kite.datarepo.ThrottlingRepositoryService;
import hms.kite.util.SdpException;
import hms.kite.util.ThrottlingType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TpsService {

    protected ThrottlingRepositoryService throttlingRepositoryService;

    private static final Logger logger = LoggerFactory.getLogger(TpsService.class);

    public boolean enforceTps(String throttlingKey, int threshold, ThrottlingType throttlingType) {

        final long currentTimeMills = System.currentTimeMillis();

        logger.debug("Apply threshold for [{}] [{}] [{}] ", new Object[]{throttlingKey, threshold, throttlingType});

        if (throttlingRepositoryService.isNoMessagesInLastSec(throttlingKey, currentTimeMills, throttlingType.getValue())) {
            return true;

        } else {
            try {
                final int count = throttlingRepositoryService.findAndIncreaseCountForLastSec(throttlingKey, currentTimeMills, throttlingType.getValue());
                return count < threshold;
            } catch (SdpException e) {
                logger.debug("Unable to increase tps count", e.getMessage());
                createThrottlingEntryFor(throttlingKey, currentTimeMills, throttlingType.getValue());
                return true;
            }
        }
    }

    private void createThrottlingEntryFor(String throttlingKey, long lastUpdatedTimeMills, String throttlingType) {
        try {
            throttlingRepositoryService.insertThrottlingEntity(throttlingKey, lastUpdatedTimeMills, throttlingType);
        } catch (Exception e) {
            logger.error("Unable to insert record for throttling", e);
            //can happen if two threads trying to insert same record - so ignoring
        }
    }

    public void setThrottlingRepositoryService(ThrottlingRepositoryService throttlingRepositoryService) {
        this.throttlingRepositoryService = throttlingRepositoryService;
    }
}
