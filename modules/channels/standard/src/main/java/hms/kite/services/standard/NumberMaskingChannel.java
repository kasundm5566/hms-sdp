/**
 *   (C) Copyright 2011-2012 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.services.standard;

import com.google.common.base.Optional;
import hms.kite.services.standard.masking.CryptoService;
import hms.kite.subscription.SubscriptionAdapter;
import hms.kite.subscription.core.service.repo.Subscription;
import hms.kite.util.KeyNameSpaceResolver;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteErrorBox.invalidMsisdnErrorCode;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.wfengine.impl.ResponseBuilder.generate;


public class NumberMaskingChannel implements Channel {
    private static final Logger logger = LoggerFactory.getLogger(NumberMaskingChannel.class);

    public enum EncryptionType {LEGACY, NEW}

    private CryptoService legacyAlgorithm;
    private CryptoService newAlgorithm;
    private SubscriptionAdapter subscriptionAdapter;

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {

        try {
            if (requestContext.get(requestK).containsKey(maskK)) {
                maskNumbers(requestContext);
            } else if (requestContext.get(requestK).containsKey(unmaskK)) {
                unmaskNumbers(requestContext);
            }
            return ResponseBuilder.generateSuccess();
        } catch (Exception ex) {
            logger.error("Exception in number mask/unmasking.", ex);
            return ResponseBuilder.generate(invalidMsisdnErrorCode, false, requestContext);
        }
    }

    private void unmaskNumbers(Map<String, Map<String, Object>> requestContext) throws Exception {
        logger.trace("Request context at unmask[{}]", requestContext);

        String appId = (String) KeyNameSpaceResolver.data(requestContext, appK, appIdK);
        boolean legacyNumberMaskAllowed = legacyAllowed4App(requestContext);
        logger.debug("Using legacy number mask algorithm is allowed [{}] for app[{}]", legacyNumberMaskAllowed, appId);


        if (isRegistrationNblCall(requestContext)) {
            legacyNumberMaskAllowed = false;
            logger.info("Legacy support disabled for subscription Reg/Unreg calls.");
        }

        unmaskSender(requestContext, appId, legacyNumberMaskAllowed);

        unmaskRecipients(requestContext, appId, legacyNumberMaskAllowed);
    }

    private void unmaskSender(Map<String, Map<String, Object>> requestContext, String appId, boolean legacyNumberMaskAllowed) throws Exception {
        Object direction = requestContext.get(requestK).get(directionK);
        if (!mtK.equals(direction)) {
            String senderAddress = (String) requestContext.get(requestK).get(senderAddressK);
            if (senderAddress != null) {
                requestContext.get(requestK).put(senderAddressMaskedK, senderAddress);
                DecryptResp decryptResp = decrypt(senderAddress, appId, legacyNumberMaskAllowed);
                if (!decryptResp.plainTxt.trim().isEmpty()) {
                    requestContext.get(requestK).put(senderAddressK, decryptResp.plainTxt);
                    requestContext.get(requestK).put(legacyNumberMaskingUsedK, String.valueOf(decryptResp.type == EncryptionType.LEGACY));
                }
            }
        } else {
            logger.debug("Not unmasking sender address for MT flow");
        }
    }

    private void unmaskRecipients(Map<String, Map<String, Object>> requestContext, String appId, boolean legacyNumberMaskAllowed) throws Exception {
        List<Map<String, String>> recipients = (List<Map<String, String>>) requestContext.get(requestK)
                .get(recipientsK);
        if (recipients != null) {
            for (Map<String, String> recipient : recipients) {
                DecryptResp decryptResp = decrypt(recipient.get(recipientAddressK), appId, legacyNumberMaskAllowed);
                recipient.put(recipientAddressMaskedK, recipient.get(recipientAddressK));
                recipient.put(recipientAddressK, decryptResp.plainTxt);
                recipient.put(recipientAddressPlainTextK, recipient.get(recipientAddressK));
                recipient.put(legacyNumberMaskingUsedK, String.valueOf(decryptResp.type == EncryptionType.LEGACY));
            }
        }
    }

    private boolean isRegistrationNblCall(Map<String, Map<String, Object>> requestContext) {
        String ncsType = (String) KeyNameSpaceResolver.data(requestContext, requestK, ncsTypeK);
        Object direction = requestContext.get(requestK).get(directionK);
        String subsReqType = (String) KeyNameSpaceResolver.data(requestContext, requestK, subscriptionRequestTypeK);
        return aoK.equals(direction) && subscriptionK.equals(ncsType) && regK.equals(subsReqType);
    }

    private boolean legacyAllowed4App(Map<String, Map<String, Object>> requestContext) {
        Object allowed = KeyNameSpaceResolver.data(requestContext, appK, legacyNumberMaskAllowedK);
        return obj2Bool(allowed);
    }

    private boolean requestingLegacyMasking(Map<String, Map<String, Object>> requestContext) {
        Object used = KeyNameSpaceResolver.data(requestContext, requestK, legacyNumberMaskingUsedK);
        return obj2Bool(used);
    }

    private boolean obj2Bool(Object obj) {
        if (obj != null) {
            if (obj instanceof Boolean) {
                return (Boolean) obj;
            } else {
                return Boolean.parseBoolean(obj.toString());
            }
        } else {
            return false;
        }
    }

    private void maskNumbers(Map<String, Map<String, Object>> requestContext) throws Exception {
        logger.trace("Masking numbers [{}]", requestContext);

        String appId = (String) KeyNameSpaceResolver.data(requestContext, appK, appIdK);
        boolean legacyAllowed4App = legacyAllowed4App(requestContext);
        logger.debug("Using legacy number mask algorithm is allowed [{}] for app[{}]", legacyAllowed4App, appId);

        maskSender(requestContext, appId, legacyAllowed4App);

        maskRecepients(requestContext, appId, legacyAllowed4App);
    }

    private void maskSender(Map<String, Map<String, Object>> requestContext, String appId, boolean legacyAllowed4App) throws Exception {
        boolean requestLegacyMask = requestingLegacyMasking(requestContext);
        Object ncsType = KeyNameSpaceResolver.data(requestContext, requestK, ncsTypeK);
        Object direction = KeyNameSpaceResolver.data(requestContext, requestK, directionK);
        String senderAddressPlainText = (String) requestContext.get(requestK).get(senderAddressK);

        if (legacyAllowed4App) {
            boolean appHasSubscriptionForSubsNotRequiredSla = isAppHasSubscriptionSlaButSubscriptionNotAllowedInSla(requestContext);
            if (moK.equals(direction) && (smsK.equals(ncsType) || ussdK.equals(ncsType)) && appHasSubscriptionForSubsNotRequiredSla) {
                Optional<Subscription> subscriber = subscriptionAdapter.getSubscriber(senderAddressPlainText, appId);
                logger.debug("Subscriber found[{}]", subscriber);
                if (subscriber.isPresent()) {
                    requestLegacyMask = subscriber.get().isUseLegacyMask();
                }
            }
        }

        requestContext.get(requestK).put(senderAddressMaskedK,
                encrypt((String) requestContext.get(requestK).get(senderAddressK), appId, (legacyAllowed4App && requestLegacyMask)));
        requestContext.get(requestK).put(senderAddressPlainTextK, senderAddressPlainText);
    }

    private void maskRecepients(Map<String, Map<String, Object>> requestContext, String appId, boolean legacyAllowed4App) throws Exception {
        List<Map<String, String>> recipients = (List<Map<String, String>>) requestContext.get(requestK)
                .get(recipientsK);

        if (recipients != null) {
            for (Map<String, String> recipient : recipients) {
                boolean legacyMasked = obj2Bool(recipient.get(legacyNumberMaskingUsedK));
                String plainTextNumber = recipient.get(recipientAddressK);
                recipient.put(recipientAddressMaskedK, encrypt(recipient.get(recipientAddressK), appId, (legacyAllowed4App && legacyMasked)));
                recipient.put(recipientAddressPlainTextK, plainTextNumber);
            }
        }
    }

    private boolean isAppHasSubscriptionSlaButSubscriptionNotAllowedInSla(Map<String, Map<String, Object>> requestContext) {
        List<Map<String, String>> configuredSla = (List<Map<String, String>>) KeyNameSpaceResolver.data(requestContext, appK, ncsesK);
        logger.debug("Configured SLAs[{}]", configuredSla);
        boolean subsSlaAvailable = false;
        for (Map<String, String> sla : configuredSla) {
            if (subscriptionK.equals(sla.get(ncsTypeK))) {
                subsSlaAvailable = true;
                break;
            }
        }

        boolean slaSubsRequired = obj2Bool(KeyNameSpaceResolver.data(requestContext, ncsSlasK, subscriptionRequiredK));

        if (subsSlaAvailable && !slaSubsRequired) {
            logger.debug("Subscription is not required for SLA, but subscription NCS available for the app[{}]", KeyNameSpaceResolver.data(requestContext, appK, appIdK));
            return true;
        } else {
            return false;
        }

    }

    private String encrypt(String msisdn, String appId, boolean legacyUsed) throws Exception {
        if (legacyUsed) {
            return legacyAlgorithm.encrypt(msisdn, appId);
        } else {
            return newAlgorithm.encrypt(msisdn, appId);
        }
    }

    private DecryptResp decrypt(String encryptedTxt, String appId, boolean legacyAllowed) throws Exception {
        boolean isNewFormat = isInNewFormat(encryptedTxt);

        if (isNewFormat) {
            return new DecryptResp(newAlgorithm.decrypt(encryptedTxt, appId), EncryptionType.NEW);
        } else if (legacyAllowed) {
            return new DecryptResp(legacyAlgorithm.decrypt(encryptedTxt, appId), EncryptionType.LEGACY);
        } else {
            throw new RuntimeException(String.format("Trying to use legacy masked number in legacy not allowed app[%s]", appId));
        }
    }

    private boolean isInNewFormat(String encryptedTxt) {
        return encryptedTxt.length() == 69;
    }


    private class DecryptResp {
        private String plainTxt;
        private EncryptionType type;

        private DecryptResp(String plainTxt, EncryptionType type) {
            this.plainTxt = plainTxt;
            this.type = type;
        }
    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        // no impl needed
        return null;
    }

    public void setLegacyAlgorithm(CryptoService legacyAlgorithm) {
        this.legacyAlgorithm = legacyAlgorithm;
    }

    public void setNewAlgorithm(CryptoService newAlgorithm) {
        this.newAlgorithm = newAlgorithm;
    }

    public void setSubscriptionAdapter(SubscriptionAdapter subscriptionAdapter) {
        this.subscriptionAdapter = subscriptionAdapter;
    }
}