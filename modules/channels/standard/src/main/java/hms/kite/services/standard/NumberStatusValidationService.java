/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.services.standard;

import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.KiteErrorBox.successCode;
import static hms.kite.util.KiteErrorBox.invalidMsisdnErrorCode;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class NumberStatusValidationService implements Channel {

	private static final Logger LOGGER = LoggerFactory.getLogger(NumberStatusValidationService.class);

	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
		Map<String, Object> request = requestContext.get(requestK);
		if (request != null) {
			if (mtK.equals(request.get(directionK))) {
				LOGGER.debug("Validating recipients msisdn status for MT flow.");
				List<Map<String, Object>> recipients = (List) request.get(recipientsK);
				for (Map<String, Object> recipient : recipients) {
					if (successCode.equals(recipient.get(recipientAddressStatusK))) {
						return ResponseBuilder.generateSuccess();
					}
				}
				LOGGER.info("All of the msisdns in recipients list are in error state.");
				return ResponseBuilder.generate(invalidMsisdnErrorCode, false, requestContext);
			} else if (moK.equals(request.get(directionK))) {
				LOGGER.debug("Validating sender msisdn status for MO flow.");
				if (successCode.equals(request.get(senderAddressStatusK))) {
					return ResponseBuilder.generateSuccess();
				} else {
					LOGGER.info("Sender address is in error status.");
					return ResponseBuilder.generate((String)request.get(senderAddressStatusK), false, requestContext);
				}
			}
		}

		return ResponseBuilder.generateSuccess();
	}

	@Override
	public Map<String, Object> send(Map<String, Object> request) {
		return null;
	}

}
