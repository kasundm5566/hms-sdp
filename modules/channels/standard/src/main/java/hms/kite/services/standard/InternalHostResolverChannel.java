/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.services.standard;

import hms.kite.util.SdpException;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.BaseChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.systemConfiguration;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.KiteErrorBox.*;

/**
 * Check host ip is an internal host and return success response if matched
 * Return default error if not matched
 *
 * $LastChangedDate: 2011-06-18 16:23:28 +0530 (Sat, 18 Jun 2011) $
 * $LastChangedBy: danukas $ $LastChangedRevision: 74190 $
 */
public class InternalHostResolverChannel extends BaseChannel {

	private static final Logger logger = LoggerFactory.getLogger(InternalHostResolverChannel.class);

	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        try {
            logger.debug("Executing channel with [{}] ", requestContext);
            String host = (String) requestContext.get(requestK).get(remotehostK);
            List<String> internalIps = (List<String>) systemConfiguration().find(internalHostsK);

            if (host == null || internalIps.contains(host)) {
                logger.info("Request received from internal host");
                requestContext.get(requestK).put(internalHostK, Boolean.TRUE);
                return ResponseBuilder.generateSuccess();
            } else {
                requestContext.get(requestK).put(internalHostK, Boolean.FALSE);
                return ResponseBuilder.generate(unknownErrorCode, false, requestContext);
            }

        } catch (Exception e) {
            logger.error("Channel failed ... ", e);
            throw new SdpException(systemErrorCode, e);
        }
    }

}
