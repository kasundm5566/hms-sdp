/*
 * (C) Copyright 2010-2014. hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.kite.services.standard.masking.impl;

import hms.kite.services.standard.masking.CryptoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class CryptoAppwiseAlgorithm implements CryptoService {
    private static final Logger logger = LoggerFactory.getLogger(CryptoAppwiseAlgorithm.class);
    private static final int KEY_IDENTIFIER_LENGTH = 5;
    private Map<String, String> encryptionKeys;
    private Map<String, CipherSet> ciphers;

    public void init() throws Exception {
        ciphers = new ConcurrentHashMap<String, CipherSet>();
        int index = 0;
        for (Map.Entry<String, String> keyEntry : encryptionKeys.entrySet()) {
            String keyIdentifier = keyEntry.getKey();
            if (keyIdentifier.length() != KEY_IDENTIFIER_LENGTH) {
                throw new Exception("Key Identifier should be " + KEY_IDENTIFIER_LENGTH + " chars long");
            }

            CipherSet cs = new CipherSet(keyIdentifier, getCypher(keyEntry.getValue(), Cipher.ENCRYPT_MODE), getCypher(keyEntry.getValue(), Cipher.DECRYPT_MODE));
            ciphers.put(String.valueOf(index), cs);
            ciphers.put(keyIdentifier, cs);
            index++;
        }
    }

    @Override
    public String encrypt(String msisdn, String appId) throws Exception {
        logger.debug("Received msisdn[{}] to encrypt for app[{}]", msisdn, appId);
        if (msisdn != null) {
            CipherSet cs = getCipherSet4Msisdn(msisdn);
            String plainTxt = scramble(msisdn, appId);
            byte[] encrypted = cs.encryptCipher.doFinal(plainTxt.getBytes("UTF-8"));
            String encryptValue = cs.key + (new BASE64Encoder().encode(encrypted));
            logger.debug("Encrypted msisdn [{}] to [{}]", msisdn, encryptValue);
            return encryptValue;
        } else {
            return "";
        }
    }

    @Override
    public String decrypt(String encryptedTxt, String appId) throws Exception {
        logger.debug("Received msisdn to decrypt [{}] for app[{}]", encryptedTxt, appId);
        String keyIdentifier = encryptedTxt.substring(0, KEY_IDENTIFIER_LENGTH);
        String encryptedValue = encryptedTxt.substring(KEY_IDENTIFIER_LENGTH, encryptedTxt.length());
        CipherSet cs = getCipherSet4Key(keyIdentifier);

        String dcrp = new String(cs.decryptCipher.doFinal(new BASE64Decoder().decodeBuffer(encryptedValue)), "UTF-8");
        String plainTxt = unScramble(dcrp);
        String msidn = plainTxt.substring(0, plainTxt.indexOf("-"));
        String extractedAppId = plainTxt.substring(plainTxt.indexOf("-") + 1, plainTxt.length());
        if (!appId.equals(extractedAppId)) {
            throw new RuntimeException(String.format("AppId in decrypted masked number[%s] do not match with requesting appId[%s]", extractedAppId, appId));
        }
        logger.debug("Decrypted msisdn [{}] to [{}]", encryptedTxt, msidn);
        return msidn;
    }

    public void setEncryptionKeys(Map<String, String> encryptionKeys) {
        this.encryptionKeys = encryptionKeys;
    }

    protected String scramble(String msisdn, String appId) {
        StringBuilder scrambledTxt = new StringBuilder("");
        final String[] key = "yu$*(&u@#$(%76(&#867!~#&JGH*(h}{*".split("|");
        String[] split = String.format("%s-%s", msisdn, appId).split("|");

        for (int i = 0; i < split.length; i++) {
            String character = split[i];
            if (key.length - 1 > i) {
                if (i % 3 == 0) {
                    scrambledTxt.append(key[i]).append(character);
                } else {
                    scrambledTxt.append(key[i + 2]).append(character);
                }
            } else {
                scrambledTxt.append("G").append(character);
            }
        }
        return scrambledTxt.toString();
    }

    protected String unScramble(String scrambled) {
        StringBuilder returnValue = new StringBuilder("");
        String[] split = scrambled.split("|");
        for (int i = 0; i < split.length; i++) {
            if (i % 2 == 0) {
                returnValue.append(split[i]);
            }
        }
        return returnValue.toString();
    }


    private CipherSet getCipherSet4Msisdn(String msisdn) {
        long l = Long.parseLong(msisdn);
        String index = String.valueOf(l % encryptionKeys.size());
        CipherSet cs = ciphers.get(index);
        return cs;
    }

    private CipherSet getCipherSet4Key(String key) {
        CipherSet cs = ciphers.get(key);
        return cs;
    }

    private Cipher getCypher(String key, int mode) throws Exception {
        String IV = "AAAAAAAAAAAAAAAA";
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        md5.update(key.getBytes("UTF-8"));
        SecretKeySpec keySpec = new SecretKeySpec(md5.digest(), "AES");

        Cipher encryptCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        encryptCipher.init(mode, keySpec, new IvParameterSpec(IV.getBytes("UTF-8")));
        return encryptCipher;
    }

    private class CipherSet {

        private final String key;
        private final Cipher encryptCipher;
        private final Cipher decryptCipher;

        public CipherSet(String key, Cipher encryptCipher, Cipher decryptCipher) {
            this.key = key;
            this.encryptCipher = encryptCipher;
            this.decryptCipher = decryptCipher;
        }
    }


}


