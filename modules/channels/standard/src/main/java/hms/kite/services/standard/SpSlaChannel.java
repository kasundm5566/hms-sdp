/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.services.standard;

import hms.kite.util.SdpException;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.spRepositoryService;
import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.wfengine.impl.ResponseBuilder.generate;
import static hms.kite.wfengine.impl.ResponseBuilder.generateSuccess;


public class SpSlaChannel implements Channel {
    private static final Logger logger = LoggerFactory.getLogger(SpSlaChannel.class);

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        if(requestContext.containsKey(spK)) {
            logger.debug("Sp sla is already available in request context");
            return generateSuccess(requestContext);
        }

        String spId = null;
        try {
            spId = (String) requestContext.get(appK).get(spIdK);
            logger.debug("Finding sp sla for sp id[{}]", spId);
            Map<String, Object> sp = spRepositoryService().findSpBySpId(spId);
            logger.info("Found SP sla [{}]", sp);

            if(null == sp) {
                logger.warn("Sp for spId[{}] not found", spId);
                return generate(systemErrorCode, false, requestContext);
            }

            requestContext.put(spK, sp);
        } catch (SdpException e) {
            logger.warn("Sdp error when getting sp sla for spId[" + spId + "]", e);
            if (e.getErrorCode().equals(spNotAvailableErrorCode) || e.getErrorCode().equals(spNotFoundErrorCode)) {
                e.setErrorCode(authenticationFailedErrorCode);
            }
            return generate(requestContext, e);
        } catch (Exception e) {
            logger.warn("Unable to get sp sla for spId[" + spId + "]", e);
            return generate(systemErrorCode, false, requestContext);
        }

        return generateSuccess(requestContext);
    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        //No implementation needed
        return null;
    }
}
