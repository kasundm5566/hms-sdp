/*
 * (C) Copyright 2010-2014. hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.kite.services.standard.masking;


public interface CryptoService {

    String encrypt(String msisdn, String appId) throws Exception;

    String decrypt(String encryptedTxt, String appId) throws Exception;
}
