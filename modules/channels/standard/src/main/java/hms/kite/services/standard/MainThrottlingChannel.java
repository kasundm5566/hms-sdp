/*
*   (C) Copyright 2009-2012 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/

package hms.kite.services.standard;

import hms.commons.SnmpLogUtil;
import hms.kite.datarepo.ThrottlingRepositoryService;
import hms.kite.util.SdpException;
import hms.kite.util.ThrottlingType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.util.KiteErrorBox.temporarySystemErrorCode;
import static hms.kite.wfengine.impl.ResponseBuilder.generate;
import static hms.kite.wfengine.impl.ResponseBuilder.generateSuccess;

/**
 * This class enforces the system tps throttling for the message flow. the threshold values are hardcoded.
 * The class name is changed from SystemTpsChannel to MainThrottlingChannel because to hide the purpose of this class
 * for some extent.
 */
public class MainThrottlingChannel extends ThrottlingChannel {

    private static final Logger logger = LoggerFactory.getLogger("hms.kite.licence");
    private static final String repositoryKey = "system.tps";

    private int maximumThreshold;
    private int threshold;
    private int marginalThreshold;
    private int trapRaisingIntervalInSeconds;

    private boolean isSnmpTrapEnabled;

    private String throttlingAboutToExceed;
    private String throttlingReached;
    private String throttlingExceeded;
    private TpsService tpsService;

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {

        try {
            if (!tpsService.enforceTps(repositoryKey, maximumThreshold, ThrottlingType.TPS)) {
                logger.debug("System TPS threshold [{}] is exceeded", maximumThreshold);
                return generate(temporarySystemErrorCode, false, requestContext);
            }

//            int latestThrottlingValue = throttlingRepositoryService
//                    .increaseThrottling(repositoryKey, 1, ThrottlingType.TPS);
//            snmpTrapLogs(latestThrottlingValue);
//            if (maximumThreshold < latestThrottlingValue) {
//                logger.debug("System TPS threshold [{}] is exceeded [{}]", maximumThreshold, latestThrottlingValue);
//                return generate(temporarySystemErrorCode, false, requestContext);
//            }
        } catch (SdpException ex) {
            logger.debug("An Error occurred [{}] for [{}] at system tps channel", ex.getErrorCode(), ex.getErrorDescription());
            return generate(ex.getErrorCode(), false, requestContext);
        }
        return generateSuccess();
    }

    public void snmpTrapLogs(int latestThrottlingValue) {
        if (!isSnmpTrapEnabled) {
            return;
        }
        if (maximumThreshold < latestThrottlingValue) {
            SnmpLogUtil.timeLog("system-tps-cap", trapRaisingIntervalInSeconds, throttlingExceeded, latestThrottlingValue, System.currentTimeMillis());
        } else if (threshold < latestThrottlingValue) {
            logger.debug("System TPS threshold [{}] reached [{}]", threshold, latestThrottlingValue);
            SnmpLogUtil.timeLog("system-tps", trapRaisingIntervalInSeconds, throttlingReached, latestThrottlingValue, System.currentTimeMillis());
        } else if (marginalThreshold < latestThrottlingValue) {
            logger.debug("System TPS marginal threshold [{}] reached [{}]", marginalThreshold, latestThrottlingValue);
            SnmpLogUtil.timeLog("system-tps-marginal", trapRaisingIntervalInSeconds, throttlingAboutToExceed, latestThrottlingValue, System.currentTimeMillis());
        }
    }

    public void setThrottlingRepositoryService(ThrottlingRepositoryService throttlingRepositoryService) {
        this.throttlingRepositoryService = throttlingRepositoryService;
    }

    public void init() {
        setThrottlingType(ThrottlingType.TPS);
    }

    public void setThrottlingAboutToExceed(String throttlingAboutToExceed) {
        this.throttlingAboutToExceed = throttlingAboutToExceed;
    }

    public void setThrottlingReached(String throttlingReached) {
        this.throttlingReached = throttlingReached;
    }

    public void setThrottlingExceeded(String throttlingExceeded) {
        this.throttlingExceeded = throttlingExceeded;
    }

    public void setSnmpTrapEnabled(boolean snmpTrapEnabled) {
        isSnmpTrapEnabled = snmpTrapEnabled;
    }

    public void setTrapRaisingIntervalInSeconds(int trapRaisingIntervalInSeconds) {
        this.trapRaisingIntervalInSeconds = trapRaisingIntervalInSeconds;
    }

    public void setTpsService(TpsService tpsService) {
        this.tpsService = tpsService;
    }

    public void setMaximumThreshold(int maximumThreshold) {
        this.maximumThreshold = maximumThreshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    public void setMarginalThreshold(int marginalThreshold) {
        this.marginalThreshold = marginalThreshold;
    }
}
