/*
*   (C) Copyright 2009-2012 hSenid Software International (Pvt) Limited.
*   All Rights Reserved.
*
*   These materials are unpublished, proprietary, confidential source code of
*   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
*   of hSenid Software International (Pvt) Limited.
*
*   hSenid Software International (Pvt) Limited retains all title to and intellectual
*   property rights in these materials.
*/

package hms.kite.services.standard;

import hms.kite.datarepo.ThrottlingRepositoryService;
import hms.kite.util.KeyNameSpaceResolver;
import hms.kite.util.SdpException;
import hms.kite.util.ThrottlingType;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.Map;

import static hms.kite.util.KeyNameSpaceResolver.data;
import static hms.kite.util.KeyNameSpaceResolver.nameSpacedKey;
import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.wfengine.impl.ResponseBuilder.generate;
import static hms.kite.wfengine.impl.ResponseBuilder.generateSuccess;


public class ThrottlingChannel implements Channel {

    private static final Logger logger = LoggerFactory.getLogger(ThrottlingChannel.class);

    protected ThrottlingType throttlingType;
    protected ThrottlingRepositoryService throttlingRepositoryService;
    protected TpsService tpsService;

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {

        int throttlingThreshold = findThreshold(requestContext);
        if (0 >= throttlingThreshold) {
            logger.debug("[{}] threshold not configured, so skipping", throttlingType.getValue());
            return generateSuccess();
        }

        String repositoryKey = generateRepoKey(requestContext);

        if (ThrottlingType.TPS == throttlingType) {
            if (!tpsService.enforceTps(repositoryKey, throttlingThreshold, throttlingType)) {
                logger.debug("{} threshold [{}] is exceeded",
                        new Object[] {repositoryKey, throttlingThreshold});
                return generate(tpsExceededErrorCode, false, requestContext);
            }

        } else {
            try {
                int latestThrottlingValue = throttlingRepositoryService.increaseThrottling(repositoryKey, 1, throttlingType);

                logger.debug("Apply threshold for [{}] [{}] [{}] ",
                        new Object[]{repositoryKey, throttlingThreshold, latestThrottlingValue});

                if (throttlingThreshold < latestThrottlingValue) {
                    logger.debug("{} threshold [{}] is exceeded [{}]",
                            new Object[] {repositoryKey, throttlingThreshold, latestThrottlingValue});
                    return generate(throttlingType == ThrottlingType.TPS ? tpsExceededErrorCode : tpsExceededErrorCode,
                            false, requestContext);
                }
            } catch (SdpException ex) {
                logger.debug("An Error occurred [{}] for [{}]", ex.getErrorCode(), ex.getErrorDescription());
                return generate(ex.getErrorCode(), false, requestContext);
            }
        }

        return generateSuccess();

    }

    private int findThreshold(Map<String, Map<String, Object>> requestContext) {
        String key = ((String[]) requestContext.get(requestK).get(serviceParameterKeysK))[0];

        if ("sp-sms-mt".equals(key)) {
            return findSpSmsMtThreshold(requestContext);
        } else if ("ncs-sms-mt".equals(key)) {
            return findNcsSmsMtThreshold(requestContext);
        } else if ("sp-sms-mo".equals(key)) {
            return findSpSmsMoThreshold(requestContext);
        } else if ("ncs-sms-mo".equals(key)) {
            return findNcsSmsMoThreshold(requestContext);
        } else if ("ncs-caas".equals(key)) {
            return findNcsCaasThreshold(requestContext);
        } else if ("sp-wap-push-mt".equals(key)) {
            return findSpWapMtThreshold(requestContext);
        } else if ("ncs-wap-push-mt".equals(key)) {
            return findNcsWapMtThreshold(requestContext);
        } else if ("ncs-ussd-mt".equals(key)) {
            return findNcsUssdMtThreshold(requestContext);
        } else if ("sp-ussd-mt".equals(key)) {
            return findSpUssdMtThreshold(requestContext);
        } else if ("ncs-ussd-mo".equals(key)) {
            return findNcsUssdMoThreshold(requestContext);
        } else if ("sp-ussd-mo".equals(key)) {
            return findSpUssdMoThreshold(requestContext);
        } else if (subscriptionK.equals(key)) {
            return findSubscriptionThreshold(requestContext);
        } else if ("sp-lbs".equals(key)) {
            return findSpLbsThreshold(requestContext);
        } else if ("ncs-lbs".equals(key)) {
            return findNcsLbsThreshold(requestContext);
        } else if ("sp-vdf-apis".equals(key)) {
            return findSpVdfApiThreshold(requestContext);
        } else if ("ncs-vdf-apis".equals(key)) {
            return findNcsVdfApiThreshold(requestContext);
        }

        logger.error("Unable to validate throttling for key[{}]", key);
        throw new SdpException(systemErrorCode, "Unable to validate throttling for key[" + key + "]");
    }

    private int findSpVdfApiThreshold(Map<String, Map<String, Object>> requestContext) {
        String requestType = (String)KeyNameSpaceResolver.data(requestContext, requestK, requestTypeK);
        String thresholdKey = nameSpacedKey(spK, requestType + "-" + throttlingType.getValue());
        return findThreshold(requestContext, thresholdKey);
    }

    private int findNcsVdfApiThreshold(Map<String, Map<String, Object>> requestContext) {
        String requestType = (String)KeyNameSpaceResolver.data(requestContext, requestK, requestTypeK);
        String thresholdKey = nameSpacedKey(ncsK, requestType, throttlingType.getValue());
        return findThreshold(requestContext, thresholdKey);
    }

    private int findSpLbsThreshold(Map<String, Map<String, Object>> requestContext) {
        String thresholdKey = nameSpacedKey(spK, lbsK + "-" + throttlingType.getValue());
        return findThreshold(requestContext, thresholdKey);
    }

    private int findNcsLbsThreshold(Map<String, Map<String, Object>> requestContext) {
        String thresholdKey = nameSpacedKey(ncsK, throttlingType.getValue());
        return findThreshold(requestContext, thresholdKey);
    }

    private int findSpUssdMoThreshold(Map<String, Map<String, Object>> requestContext) {
        String thresholdKey = nameSpacedKey(spK, ussdK + "-" + moK + "-" + throttlingType.getValue());
        return findThreshold(requestContext, thresholdKey);
    }

    private int findNcsUssdMoThreshold(Map<String, Map<String, Object>> requestContext) {
        String thresholdKey = nameSpacedKey(ncsK, moK, throttlingType.getValue());
        return findThreshold(requestContext, thresholdKey);
    }

    private int findSpUssdMtThreshold(Map<String, Map<String, Object>> requestContext) {
        String thresholdKey = nameSpacedKey(spK, ussdK + "-" + mtK + "-" + throttlingType.getValue());
        return findThreshold(requestContext, thresholdKey);
    }

    private int findNcsUssdMtThreshold(Map<String, Map<String, Object>> requestContext) {
        String thresholdKey = nameSpacedKey(ncsK, mtK, throttlingType.getValue());
        return findThreshold(requestContext, thresholdKey);
    }

    private int findNcsSmsMtThreshold(Map<String, Map<String, Object>> requestContext) {
        String thresholdKey = nameSpacedKey(ncsK, mtK, throttlingType.getValue());
        return findThreshold(requestContext, thresholdKey);
    }

    private int findSpSmsMtThreshold(Map<String, Map<String, Object>> requestContext) {
        String thresholdKey = nameSpacedKey(spK, smsK + "-" + mtK + "-" + throttlingType.getValue());
        return findThreshold(requestContext, thresholdKey);
    }

    private int findNcsSmsMoThreshold(Map<String, Map<String, Object>> requestContext) {
        String thresholdKey = nameSpacedKey(ncsK, moK, throttlingType.getValue());
        return findThreshold(requestContext, thresholdKey);
    }

    private int findSpSmsMoThreshold(Map<String, Map<String, Object>> requestContext) {
        String thresholdKey = nameSpacedKey(spK, smsK + "-" + moK + "-" + throttlingType.getValue());
        return findThreshold(requestContext, thresholdKey);
    }

    private int findNcsCaasThreshold(Map<String, Map<String, Object>> requestContext) {
        String thresholdKey = nameSpacedKey(ncsK, throttlingType.getValue());
        return findThreshold(requestContext, thresholdKey);
    }

    private int findSubscriptionThreshold(Map<String, Map<String, Object>> requestContext) {
        String thresholdKey = nameSpacedKey(ncsK, maxNoOfBcMsgsPerDayK);
        return findThreshold(requestContext, thresholdKey);
    }

    private int findSpWapMtThreshold(Map<String, Map<String, Object>> requestContext) {
        String thresholdKey = nameSpacedKey(spK, wapPushK + "-" + mtK + "-" + throttlingType.getValue());
        return findThreshold(requestContext, thresholdKey);
    }

    private int findNcsWapMtThreshold(Map<String, Map<String, Object>> requestContext) {
        String thresholdKey = nameSpacedKey(ncsK, mtK, throttlingType.getValue());
        return findThreshold(requestContext, thresholdKey);
    }

    private int findThreshold(Map<String, Map<String, Object>> requestContext, String thresholdKey) {
        String thresholdString = (String) data((Map) requestContext, thresholdKey);

        logger.debug("Enforcing throttling threshold for [{}] is [{}]", thresholdKey, thresholdString);
        try {
            if (null != thresholdString && !thresholdString.isEmpty()) {
                return Integer.parseInt(thresholdString);
            }

            return 0;
        } catch (NumberFormatException e) {
            throw new SdpException(systemErrorCode, String.format(
                    "Could not parse throttling threshold [%s] from the request context ", thresholdString));
        }
    }

    private String generateRepoKey(Map<String, Map<String, Object>> requestContext) {
        String key = ((String[]) requestContext.get(requestK).get(serviceParameterKeysK))[0];

        if (key != null) {
            if (key.startsWith("sp")) {
                return createSpRelatedRepoKeys(key, requestContext);
            } else if (key.startsWith("ncs")) {
                return createNcsRelatedRepoKeys(key, requestContext);
            } else if (key.startsWith(subscriptionK)) {
                return createNcsRelatedRepoKeys(key, requestContext);
            }
        }

        return null;
    }

    private String createNcsRelatedRepoKeys(String key, Map<String, Map<String, Object>> requestContext) {
        String appId = (String) requestContext.get(appK).get(appIdK);
        String ncsType = (String) requestContext.get(ncsK).get(ncsTypeK);
        String operator = (String) requestContext.get(ncsK).get(operatorK);
        if ("ncs-sms-mt".equals(key)) {
            return nameSpacedKey(throttlingType.getValue(), appId, ncsType, operator, nameSpacedKey(ncsK, mtK, throttlingType.getValue()));
        } else if ("ncs-sms-mo".equals(key)) {
            return nameSpacedKey(throttlingType.getValue(), appId, ncsType, operator, nameSpacedKey(ncsK, moK, throttlingType.getValue()));
        } else if ("ncs-cas".equals(key)) {
            return nameSpacedKey(throttlingType.getValue(), appId, ncsType, nameSpacedKey(ncsK, throttlingType.getValue()));
        } else if ("ncs-wap-push-mt".equals(key)) {
            return nameSpacedKey(throttlingType.getValue(), appId, ncsType, operator, nameSpacedKey(ncsK, mtK, throttlingType.getValue()));
        } else if ("ncs-ussd-mt".equals(key)) {
            return nameSpacedKey(throttlingType.getValue(), appId, ncsType, operator, nameSpacedKey(ncsK, mtK, throttlingType.getValue()));
        } else if ("ncs-ussd-mo".equals(key)) {
            return nameSpacedKey(throttlingType.getValue(), appId, ncsType, operator, nameSpacedKey(ncsK, moK, throttlingType.getValue()));
        } else if (("ncs-" + maxNoOfBcMsgsPerDayK).equals(key)) {
            return nameSpacedKey(throttlingType.getValue(), appId, ncsType, nameSpacedKey(ncsK, throttlingType.getValue()));
        } else if (subscriptionK.equals(key)) {
            return nameSpacedKey(throttlingType.getValue(), appId, subscriptionK, nameSpacedKey(ncsK, throttlingType.getValue()));
        } else if ("ncs-caas".equals(key)) {
            return nameSpacedKey(throttlingType.getValue(), appId, ncsType, operator, nameSpacedKey(ncsK, throttlingType.getValue()));
        } else if ("ncs-lbs".equals(key)) {
            return nameSpacedKey(throttlingType.getValue(), appId, ncsType, operator, nameSpacedKey(ncsK, throttlingType.getValue()));
        }else if("ncs-vdf-apis".equals(key)){
            String requestType = (String)KeyNameSpaceResolver.data(requestContext, requestK, requestTypeK);
            return nameSpacedKey(throttlingType.getValue(), appId, ncsType, operator, nameSpacedKey(ncsK, requestType, throttlingType.getValue()));
        }
        return null;
    }

    private String createSpRelatedRepoKeys(String key, Map<String, Map<String, Object>> requestContext) {
        String spId = (String) requestContext.get(spK).get(spIdK);
        if ("sp-sms-mt".equals(key)) {
            return nameSpacedKey(throttlingType.getValue(), spId, nameSpacedKey(smsK, mtK));
        } else if ("sp-sms-mo".equals(key)) {
            return nameSpacedKey(throttlingType.getValue(), spId, nameSpacedKey(smsK, moK));
        } else if ("sp-wap-push-mt".equals(key)) {
            return nameSpacedKey(throttlingType.getValue(), spId, nameSpacedKey(wapPushK, mtK));
        } else if ("sp-ussd-mt".equals(key)) {
            return nameSpacedKey(throttlingType.getValue(), spId, nameSpacedKey(ussdK, mtK));
        } else if ("sp-ussd-mo".equals(key)) {
            return nameSpacedKey(throttlingType.getValue(), spId, nameSpacedKey(ussdK, moK));
        } else if("sp-lbs".equals(key)){
            return nameSpacedKey(throttlingType.getValue(), spId, nameSpacedKey(lbsK));
        } else if("sp-vdf-apis".equals(key)){
            String requestType = (String)KeyNameSpaceResolver.data(requestContext, requestK, requestTypeK);
            return nameSpacedKey(throttlingType.getValue(), spId, nameSpacedKey(vdfApiK, requestType));
        }
        return null;
    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        return null;
    }

    public void setThrottlingType(ThrottlingType throttlingType) {
        this.throttlingType = throttlingType;
    }

    public void setThrottlingType(String type) {
        try {
            this.throttlingType = ThrottlingType.valueOf(type);
        } catch (IllegalArgumentException ex) {
            throw new SdpException(temporarySystemErrorCode,
                    MessageFormat.format("Unrecognized throttling type injected: [{0}]", type));
        }
    }

    public void setThrottlingRepositoryService(ThrottlingRepositoryService throttlingRepositoryService) {
        this.throttlingRepositoryService = throttlingRepositoryService;
    }

    public void setTpsService(TpsService tpsService) {
        this.tpsService = tpsService;
    }
}
