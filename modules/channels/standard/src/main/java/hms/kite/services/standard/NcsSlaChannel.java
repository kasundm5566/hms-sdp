/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.services.standard;

import hms.kite.util.SdpException;
import hms.kite.wfengine.impl.ResponseBuilder;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.ncsRepositoryService;
import static hms.kite.util.KiteErrorBox.systemErrorCode;
import static hms.kite.util.KiteKeyBox.*;


public class NcsSlaChannel implements Channel {
    private static final Logger logger = LoggerFactory.getLogger(NcsSlaChannel.class);

    private List<String> operatorSpecificNcss;

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        if(requestContext.containsKey(ncsK)) {
            return ResponseBuilder.generateSuccess();
        }

        String appId = null;
        String ncsType = null;
        String recipientOperator = null;
        try {
            appId = (String) requestContext.get(requestK).get(appIdK);
            ncsType = (String) requestContext.get(requestK).get(ncsTypeK);

            if (isOperatorSpecificNcs(ncsType)) {
                recipientOperator = (String) requestContext.get(requestK).get(operatorK);

                logger.debug("Finding ncs sla for app-id[{}] ncs-type[{}] operator[{}]", new Object[]{appId,
                        ncsType, recipientOperator});

                Map<String, Object> ncs = findNcs(appId, ncsType, recipientOperator);
                logger.info("Found ncs [{}]" , ncs);
                requestContext.put(ncsK, ncs);
                return ResponseBuilder.generateSuccess();
            } else {
                logger.debug("Finding ncs sla for app-id[{}] ncs-type[{}]", new Object[]{appId, ncsType});

                Map<String, Object> ncs = findNcs(appId, ncsType);
                logger.info("Found ncs [{}]" , ncs);
                requestContext.put(ncsK, ncs);
                return ResponseBuilder.generateSuccess();
            }

        } catch (SdpException e) {
            logger.warn("Sdp exception occurred", e);
            return ResponseBuilder.generate(requestContext, e);
        } catch (Exception e) {
            logger.error("Unexpected error occurred", e);
            return ResponseBuilder.generate(systemErrorCode, false, requestContext);
        } finally {
            logger.debug("Request Context after executing NCS SLA CHANEL {}", requestContext);
        }
    }

    private boolean isOperatorSpecificNcs(String ncsType) {
        return (operatorSpecificNcss != null) && operatorSpecificNcss.contains(ncsType);
    }

    private Map<String, Object> findNcs(String appId, String ncsType, String recipientOperator) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put(appIdK, appId);
        map.put(ncsTypeK, ncsType);
        map.put(operatorK, recipientOperator);

        return ncsRepositoryService().findOperatorSla(map);
    }

    private Map<String, Object> findNcs(String appId, String ncsType) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put(appIdK, appId);
        map.put(ncsTypeK, ncsType);

        return ncsRepositoryService().findOperatorSla(map);
    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        //no impl needed
        return null;
    }

    public void setOperatorSpecificNcss(List<String> operatorSpecificNcss) {
        this.operatorSpecificNcss = operatorSpecificNcss;
    }
}
