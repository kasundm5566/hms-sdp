/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.services.standard;

import hms.kite.datarepo.RepositoryServiceRegistry;
import hms.kite.util.SdpException;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.util.KiteErrorBox.*;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.wfengine.impl.ResponseBuilder.generate;
import static hms.kite.wfengine.impl.ResponseBuilder.generateSuccess;


public class AppSlaChannel implements Channel {
    private static final Logger logger = LoggerFactory.getLogger(AppSlaChannel.class);

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
        if(requestContext.containsKey(appK)) {
            logger.debug("App sla is already available in request context");
            return generateSuccess(requestContext);
        }

        if (!requestContext.get(requestK).containsKey(appIdK)) {
            logger.error("App id not available in request");
            return generate(invalidRequestErrorCode, "App ID not provided", false);

        } else {
            String appId = null;
            try {
                appId = (String) requestContext.get(requestK).get(appIdK);
                logger.debug("Finding app sla for app id[{}]", appId);
                Map<String, Object> app = RepositoryServiceRegistry.appRepositoryService().findByAppId(appId);
                logger.info("Found App sla [{}]", app);
                requestContext.put(appK, app);
            } catch (SdpException e) {
                logger.error("Sdp error when getting app sla for App Id [{}], [{}]", appId, e);
                if (e.getErrorCode().equals(appNotAvailableErrorCode) || e.getErrorCode().equals(appNotFoundErrorCode)) {
                    e.setErrorCode(authenticationFailedErrorCode);
                }
                return generate(requestContext, e);
            } catch (Exception e) {
                logger.error("Unable to get app sla for App Id [{}], [{}]", appId, e);
                return generate(systemErrorCode, false, requestContext);
            }
        }

        return generateSuccess(requestContext);
    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        //No implementation needed
        return null;
    }
}
