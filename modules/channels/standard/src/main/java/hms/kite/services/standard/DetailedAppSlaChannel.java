package hms.kite.services.standard;

import hms.kite.util.SdpException;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.Map;

import static hms.kite.datarepo.RepositoryServiceRegistry.appRepositoryService;
import static hms.kite.datarepo.RepositoryServiceRegistry.ncsRepositoryService;
import static hms.kite.util.KiteErrorBox.invalidRequestErrorCode;
import static hms.kite.util.KiteErrorBox.systemErrorCode;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.wfengine.impl.ResponseBuilder.generate;
import static hms.kite.wfengine.impl.ResponseBuilder.generateSuccess;

/**
 * User: azeem
 * Date: 3/29/12
 * Time: 1:34 PM
 */
public class DetailedAppSlaChannel implements Channel {

    private static final Logger logger = LoggerFactory.getLogger(DetailedAppSlaChannel.class);

    @Override
    public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {

        if (requestContext.containsKey(appK)) {
            logger.debug("App SLA is already available in the request context");
            return generateSuccess(requestContext);
        }

        if (!requestContext.get(requestK).containsKey(appIdK)) {
            logger.debug("Request context does not contain app id");
            return generate(invalidRequestErrorCode, "App id is not provided with the request context", false);
        } else {
            String appId = (String) requestContext.get(requestK).get(appIdK);
            try {
                logger.debug("Finding app sla for app-id [{}]", appId);
                Map<String, Object> app = appRepositoryService().findByAppId(appId);
                logger.debug("Finding ncs slas for app-id [{}]", appId);
                Map<String, Map<String, Object>> ncsSlas = ncsRepositoryService().findByAppId(appId);
                String operatorSmsNcsKey = MessageFormat.format("{0}-{1}",
                        requestContext.get(requestK).get(operatorK), smsK);
                if (ncsSlas.containsKey(operatorSmsNcsKey)) {
                    app.put(smsK, ncsSlas.get(operatorSmsNcsKey));
                }
                requestContext.put(appK, app);
            } catch (SdpException e) {
                logger.error(e.getErrorDescription(), e);
                return generate(requestContext, e);
            } catch (Exception e) {
                logger.error("Unable to get app sla and ncs slas for app-id [{}], [{}]", appId, e);
                return generate(systemErrorCode, false, requestContext);
            }
        }
        return generateSuccess(requestContext);
    }

    @Override
    public Map<String, Object> send(Map<String, Object> request) {
        return null;
    }
}
