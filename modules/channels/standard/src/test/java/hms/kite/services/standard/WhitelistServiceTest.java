/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hms.kite.services.standard;


import hms.kite.util.KiteErrorBox;
import hms.kite.util.KiteKeyBox;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.lang.Boolean;
import java.lang.Object;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;


public class WhitelistServiceTest {

    private WhitelistService whitelistService;
    private Map<String, Object> resp = null;
    private Map<String, Map<String, Object>> requestContext = null;
    private Map<String, Object> app = null;
    private Map<String, Object> request = null;
    private List<Map<String, String>> recipientList = null;
    private Map<String, String> recipientMap = null;


    @BeforeTest
    public void setup() {
        whitelistService = new WhitelistService();
        List<String> whiteList = new ArrayList<String>();
        whiteList.add("0711234567");
        whiteList.add("0711234568");
        requestContext = new HashMap<String, Map<String, Object>>();
        recipientList = new ArrayList<Map<String, String>>();
        app = new HashMap<String, Object>();
        request = new HashMap<String, Object>();
        recipientMap = new HashMap<String, String>();
        app.put(whiteListK, whiteList);
        requestContext.put(appK, app);
    }

    @Test
    public void testSuccess() {
        request.put(senderAddressK, "0711234567");
        recipientMap.put(recipientAddressK, "0711234568");
        recipientList.add(recipientMap);
        request.put(recipientsK, recipientList);
        requestContext.put(requestK, request);
        resp = whitelistService.execute(requestContext);
        assertEquals((String) resp.get(statusK), Boolean.toString(true));

        request.put(senderAddressK, "0711234567");
        recipientMap.put(recipientAddressK, "0710000");
        recipientMap.put(recipientAddressK, "0710001");
        recipientList.add(recipientMap);
        request.put(recipientsK, recipientList);
        requestContext.put(requestK, request);
        resp = whitelistService.execute(requestContext);
        List<Map<String, String>> recipientList = (List<Map<String, String>>) requestContext.get(requestK).get(recipientsK);
        for (Map<String,String> recipient : recipientList) {
            assertEquals(recipient.get(recipientAddressStatusK), KiteErrorBox.msisdnIsNotInWhiteListErrorCode);
        }
    }

    @Test
    public void testFailures() {
        request.put(senderAddressK, "071123456");
        requestContext.put(requestK, request);
        resp = whitelistService.execute(requestContext);
        assertEquals((String) resp.get(statusK), Boolean.toString(true));
    }

    @AfterTest
    public void tearDown() {
        whitelistService = null;
    }
}