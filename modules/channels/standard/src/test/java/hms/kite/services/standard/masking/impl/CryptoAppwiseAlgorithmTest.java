/*
 * (C) Copyright 2010-2014. hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.kite.services.standard.masking.impl;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;


public class CryptoAppwiseAlgorithmTest {

    @Test
    public void testInit() {
        CryptoAppwiseAlgorithm csi = new CryptoAppwiseAlgorithm();
        Map<String, String> keySet = new HashMap<String, String>();
        keySet.put("0001", "Secretkey1");
        csi.setEncryptionKeys(keySet);
        try {
            csi.init();
            fail("Should have thrown exception with invalid key identifier length");
        } catch (Exception e) {
            assertEquals("Key Identifier should be 5 chars long", e.getMessage());
        }
    }

    @Test
    public void testEncrypt() throws Exception {
        CryptoAppwiseAlgorithm csi = new CryptoAppwiseAlgorithm();
        Map<String, String> keySet = new HashMap<String, String>();
        keySet.put("00001", "key1");
        keySet.put("00002", "key2");
        keySet.put("00003", "key3");
        csi.setEncryptionKeys(keySet);
        csi.init();

        String s1 = csi.encrypt("94776177393", "APP_000123");
        System.out.println(s1);
        assertEquals("00001cT/EghmkKE1QFXVtfaAtWT8jS99hG+C/w3BMcGOIHAildBU3/glvuy4Wa0pjNwYK", s1);
        assertTrue("should have used key1", s1.startsWith("00001"));

        String s2 = csi.encrypt("94776177394", "APP_000123");
        System.out.println(s2);
        assertEquals("00002qedBCItm94NZ19TggQ6B/JNYdRIyIArzNKS9mZ1POZQgj8VWDl2NaUdsb5ep3fMH", s2);
        assertTrue("should have used key2", s2.startsWith("00002"));

        String s3 = csi.encrypt("94776177395", "APP_000123");
        System.out.println(s3);
        assertEquals("00003b/QwRKouhmicSc6+QEFj1k23vOvq0hij913E0FQnp4YMGAM5O+TOj1tCKr23jp4b", s3);
        assertTrue("should have used key3", s3.startsWith("00003"));
    }

    @Test
    public void testEncrypSameNumberDifferentApps() throws Exception {
        CryptoAppwiseAlgorithm csi = new CryptoAppwiseAlgorithm();
        Map<String, String> keySet = new HashMap<String, String>();
        keySet.put("00001", "key1");
        keySet.put("00002", "key2");
        keySet.put("00003", "key3");
        csi.setEncryptionKeys(keySet);
        csi.init();

        String s1 = csi.encrypt("94776177393", "APP_000123");
        System.out.println(s1);

        String s2 = csi.encrypt("94776177393", "APP_000456");
        System.out.println(s2);
        assertFalse("encrypted should be different", s1.equalsIgnoreCase(s2));

    }

    @Test
    public void testDecrpt() throws Exception {
        CryptoAppwiseAlgorithm csi = new CryptoAppwiseAlgorithm();
        Map<String, String> keySet = new HashMap<String, String>();
        keySet.put("00001", "key1");
        keySet.put("00002", "key2");
        keySet.put("00003", "key3");
        csi.setEncryptionKeys(keySet);
        csi.init();

        String s1 = csi.decrypt("00001cT/EghmkKE1QFXVtfaAtWT8jS99hG+C/w3BMcGOIHAildBU3/glvuy4Wa0pjNwYK", "APP_000123");
        System.out.println(s1);
        assertEquals("94776177393", s1);

        String s2 = csi.decrypt("00002qedBCItm94NZ19TggQ6B/JNYdRIyIArzNKS9mZ1POZQgj8VWDl2NaUdsb5ep3fMH", "APP_000123");
        System.out.println(s2);
        assertEquals("94776177394", s2);

        String s3 = csi.decrypt("00003b/QwRKouhmicSc6+QEFj1k23vOvq0hij913E0FQnp4YMGAM5O+TOj1tCKr23jp4b", "APP_000123");
        System.out.println(s3);
        assertEquals("94776177395", s3);

    }

    @Test
    public void testDecryptSameNumberDifferentApps() throws Exception {
        CryptoAppwiseAlgorithm csi = new CryptoAppwiseAlgorithm();
        Map<String, String> keySet = new HashMap<String, String>();
        keySet.put("00001", "key1");
        keySet.put("00002", "key2");
        keySet.put("00003", "key3");
        csi.setEncryptionKeys(keySet);
        csi.init();

        String s1 = csi.decrypt("00001cT/EghmkKE1QFXVtfaAtWT8jS99hG+C/w3BMcGOIHAildBU3/glvuy4Wa0pjNwYK", "APP_000123");
        System.out.println(s1);
        assertEquals("94776177393", s1);

        String s2 = csi.decrypt("00001cT/EghmkKE1QFXVtfaAtWT8jS99hG+C/w3BMcGOIHAgDuZHtFRyMCsO9tWAxtnn1", "APP_000456");
        System.out.println(s2);
        assertEquals("94776177393", s2);

    }

    @Test
    public void testDecrpt_EncryptedForDifferentApp() throws Exception {
        CryptoAppwiseAlgorithm csi = new CryptoAppwiseAlgorithm();
        Map<String, String> keySet = new HashMap<String, String>();
        keySet.put("00001", "key1");
        keySet.put("00002", "key2");
        keySet.put("00003", "key3");
        csi.setEncryptionKeys(keySet);
        csi.init();

        try {
            String s1 = csi.decrypt("00001cT/EghmkKE1QFXVtfaAtWT8jS99hG+C/w3BMcGOIHAildBU3/glvuy4Wa0pjNwYK", "APP_000456");
            fail("Should throw exception when decrpting number masked for different app");
        } catch (Exception e) {
            assertEquals("AppId in decrypted masked number[APP_000123] do not match with requesting appId[APP_000456]", e.getMessage());
        }


    }

    @Test
    public void testScramble() {
        CryptoAppwiseAlgorithm csi = new CryptoAppwiseAlgorithm();
        String s = csi.scramble("94776177393", "APP_000123");
        assertEquals("scrambled msisdn", "$9*4$7&7u6&1#7$7#3%973%-(A&P(P8_6080!0~1!2&3", s);

    }

    @Test
    public void testUnscrable() {
        CryptoAppwiseAlgorithm csi = new CryptoAppwiseAlgorithm();
        String s = csi.unScramble("$9*4$7&7u6&1#7$7#3%973%-(A&P(P8_6080!0~1!2&3");
        assertEquals("un-scrambled msisdn with appId", "94776177393-APP_000123", s);
    }


}
