package hms.kite.services.standard;

import hms.kite.services.standard.masking.impl.CryptoLegacyAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;

@ContextConfiguration(locations={"classpath:beans-test.xml"})
public class NumberMaskingChannelTest extends AbstractTestNGSpringContextTests {

    @Autowired
    CryptoLegacyAlgorithm numberMaskingChannel;

    private String plainMsisdn = "94775038419";
    private String encryptedMsisdn = "";

    @BeforeClass
    public void setUp() {
    }

    @Test
    public void testEncrypt() throws Exception {
        encryptedMsisdn = numberMaskingChannel.encrypt(plainMsisdn, "");
    }

    @Test(dependsOnMethods = "testEncrypt")
    public void testDecrypt() throws Exception {
        String decryptedMsisdn = numberMaskingChannel.decrypt(encryptedMsisdn, "");
        assertEquals(decryptedMsisdn, plainMsisdn);
    }
}
