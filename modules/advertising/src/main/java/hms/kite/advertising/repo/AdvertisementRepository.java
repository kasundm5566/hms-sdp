/**
 * (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 * All Rights Reserved.
 * <p/>
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 * International (pvt) Limited.
 * <p/>
 * hSenid International (pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.kite.advertising.repo;

import hms.kite.advertising.repo.mongo.AdvertisementAlreadyCreatedException;

import java.util.List;
import java.util.Map;

public interface AdvertisementRepository {

    String findAdNameByAppId(String appId);

    Map<String, String> findAdByAdName(String adName);

    Map<String, String> findAdByAppId(String appId);

    List<Map<String, String>> getAllAds();

    void updateApp(Map<String, String> appAdvertisement);

    void createAd(Map<String, String> advertisement) throws AdvertisementAlreadyCreatedException;

    void editAd(Map<String, String> advertisement);

    void deleteAd(String adName);

    //void getAppsForAdd(String adName);
}
