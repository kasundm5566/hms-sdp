/**
 * (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 * All Rights Reserved.
 * <p/>
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 * International (pvt) Limited.
 * <p/>
 * hSenid International (pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.kite.advertising.repo.mongo;

import static hms.kite.util.KiteKeyBox.adNameK;
import static hms.kite.util.KiteKeyBox.createdTimeK;
import static hms.kite.util.KiteKeyBox.idK;
import static hms.kite.util.KiteKeyBox.lastModifiedTimeK;
import hms.kite.advertising.repo.AdvertisementRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.QueryBuilder;

public class AdvertisementMongoRepository implements AdvertisementRepository {

    /*
        Collection Name = app_Advertisements(Map<String, String>)
            - This collection holds advertisements and their appIds
            - Unique key will be appId
        {
            _id                 : app-id
            advertisement-name  : advertisement-name
        }
     */
    public static final String appAdvertisementsCollectionName = "app_advertisements";

    /*
        Collection Name = advertisements(Map<String, String>)
            - This collection holds all available advertisements and their details
            - Unique key will be ad_name
        {
            _id                 : advertisement-name
            message             : advertisement-text
            status              : (enable or disable)
            created-time        : created-time
            last-modified-time  : last-modified-time
        }
     */
    public static final String advertisementsCollectionName = "advertisements";

    private MongoTemplate mongoTemplate;

    @Override
    public String findAdNameByAppId(String appId) {
        DBObject dbObject = mongoTemplate.getCollection(appAdvertisementsCollectionName)
                .findOne(new QueryBuilder().put(idK).is(appId).get());
        if (null != dbObject) {
            return ((Map<String, String>) dbObject).get(adNameK);
        }
        return null;
    }

    @Override
    public Map<String, String> findAdByAdName(String adName) {
        DBObject dbObject = mongoTemplate.getCollection(advertisementsCollectionName)
               .findOne(new QueryBuilder().put(idK).is(adName).get());
        if (null != dbObject) {
            return (Map<String, String>) dbObject;
        }
        return null;
    }

    public Map<String, String> findAdByAppId(String appId) {
        return findAdByAdName(findAdNameByAppId(appId));
    }

    @Override
    public List<Map<String, String>> getAllAds() {
        List<Map<String, String>> allAds = new ArrayList<Map<String, String>>();
        DBCollection appCollection = mongoTemplate.getCollection(advertisementsCollectionName);
        DBCursor dbCursor = appCollection.find();
        while (dbCursor.hasNext()) {
            allAds.add((Map<String, String>) dbCursor.next().toMap());
        }
        return allAds;
    }

    @Override
    public void updateApp(Map<String, String> appAdvertisement) {
        mongoTemplate.getCollection(appAdvertisementsCollectionName).save(new BasicDBObject(appAdvertisement));
    }

    /**
     * This method validate the newly created advertisement by,
     *      - searching is this advertisement is already created or not
     *      - checking whether advertisement-length is below than max-advertisement-length
     * @param advertisement
     * @throws AdvertisementAlreadyCreatedException
     */
    @Override
    public void createAd(Map<String, String> advertisement) throws AdvertisementAlreadyCreatedException {
        boolean isCreatedBefore = (Map<String, String>)mongoTemplate.getCollection(advertisementsCollectionName)
                .findOne(new QueryBuilder().put(idK).is(advertisement.get(idK)).get()) != null;
        if(isCreatedBefore) {
            throw new AdvertisementAlreadyCreatedException();
        }
        String currentTime = Long.toString(System.currentTimeMillis());
        advertisement.put(createdTimeK, currentTime);
        advertisement.put(lastModifiedTimeK, currentTime);
        mongoTemplate.getCollection(advertisementsCollectionName).save(new BasicDBObject(advertisement));
    }

    @Override
    public void editAd(Map<String, String> advertisement) {
        String currentTime = Long.toString(System.currentTimeMillis());
        advertisement.put(lastModifiedTimeK, currentTime);
        mongoTemplate.getCollection(advertisementsCollectionName).save(new BasicDBObject(advertisement));
    }

    /**
     * This method delete advertisement data from both collections
      * @param adName
     */
    @Override
    public void deleteAd(String adName) {
        mongoTemplate.getCollection(advertisementsCollectionName)
                .remove(new QueryBuilder().put(idK).is(adName).get());

        HashMap<String, Object> queryCriteria = new HashMap<String, Object>();
        queryCriteria.put(adNameK, Pattern.compile(adName, Pattern.CASE_INSENSITIVE));
        DBCollection appCollection = mongoTemplate.getCollection(appAdvertisementsCollectionName);
        BasicDBObject query = new BasicDBObject();
        query.putAll(queryCriteria);
        appCollection.remove(query);
    }

    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public MongoTemplate getMongoTemplate() {
        return mongoTemplate;
    }
}
