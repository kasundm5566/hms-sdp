/**
 *   (C) Copyright 2011-2012 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.advertising;

import hms.kite.advertising.repo.mongo.AdvertisementMongoRepository;
import hms.kite.wfengine.transport.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static hms.kite.wfengine.impl.ResponseBuilder.generate;
import static hms.kite.wfengine.impl.ResponseBuilder.generateSuccess;
import static hms.kite.util.KiteKeyBox.*;
import static hms.kite.util.KiteErrorBox.*;

/**
 * $LastChangedDate$ $LastChangedBy$ $LastChangedRevision$
 */
public class AdvertisingChannel implements Channel {

	private static final Logger logger = LoggerFactory.getLogger(AdvertisingChannel.class);
	private AdvertisementMongoRepository advertisementMongoRepository;
	private int maxMessageSize;
	private int maxAdvertisementLength;
	private String separator;
    private boolean enableMessageLengthValidation;

	@Override
	public Map<String, Object> execute(Map<String, Map<String, Object>> requestContext) {
		String appId = (String) requestContext.get(requestK).get(appIdK);
		String message = (String) requestContext.get(requestK).get(messageK);
		logger.debug("Message Length[{}]", message.length());
        if (validateMessageLength(message)) return generate(messageTooLongErrorCode, false, requestContext);

        Map<String, String> advertisement = advertisementMongoRepository.findAdByAppId(appId);
        appendAdvertisement(requestContext, appId, message, advertisement);
        return generateSuccess();
	}

    private boolean validateMessageLength(String message) {
        if (enableMessageLengthValidation && message.length() >= (maxMessageSize - maxAdvertisementLength)) {
            logger.info("Required message length is exceeded and cannot append the advertisement.");
            return true;
        }
        return false;
    }

    private void appendAdvertisement(Map<String, Map<String, Object>> requestContext, String appId, String message, Map<String, String> advertisement) {
        if (advertisement != null) {
            if (advertisement.get(statusK).equalsIgnoreCase(enableK)) {
                logger.info("Advertisement found[{}]", advertisement);
                message = message + separator + advertisement.get(messageK);
                requestContext.get(requestK).put(messageK, message);
                requestContext.get(requestK).put(adNameK, advertisement.get("_id"));
                requestContext.get(requestK).put(adTextK, advertisement.get("message"));
            } else {
                logger.info("Advertisement is disabled.");
            }
        } else {
            logger.info("No Advertisement configured for appId[{}].", appId);
        }
    }

    @Override
	public Map<String, Object> send(Map<String, Object> request) {
		return generateSuccess();
	}

	public void setMaxMessageSize(int maxMessageSize) {
		this.maxMessageSize = maxMessageSize;
	}

	public void setMaxAdvertisementLength(int maxAdvertisementLength) {
		this.maxAdvertisementLength = maxAdvertisementLength;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}

	public void setAdvertisementMongoRepository(AdvertisementMongoRepository advertisementMongoRepository) {
		this.advertisementMongoRepository = advertisementMongoRepository;
	}

    public void setEnableMessageLengthValidation(boolean enableMessageLengthValidation) {
        this.enableMessageLengthValidation = enableMessageLengthValidation;
    }
}
