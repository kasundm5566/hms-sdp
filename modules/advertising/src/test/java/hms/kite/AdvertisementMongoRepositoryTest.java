/**
 * (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 * All Rights Reserved.
 * <p/>
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 * International (pvt) Limited.
 * <p/>
 * hSenid International (pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.kite;

import static hms.kite.util.KiteKeyBox.adNameK;
import static hms.kite.util.KiteKeyBox.disableK;
import static hms.kite.util.KiteKeyBox.enableK;
import static hms.kite.util.KiteKeyBox.idK;
import static hms.kite.util.KiteKeyBox.messageK;
import static hms.kite.util.KiteKeyBox.statusK;
import static org.testng.Assert.assertEquals;
import hms.kite.advertising.repo.mongo.AdvertisementAlreadyCreatedException;
import hms.kite.advertising.repo.mongo.AdvertisementMongoRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.mongodb.BasicDBObject;
import com.mongodb.QueryBuilder;

@ContextConfiguration(locations = {"classpath:beans-test.xml"})
public class AdvertisementMongoRepositoryTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private AdvertisementMongoRepository advertisementMongoRepository;

    public static final String appAdvertisementsCollectionName = "app_Advertisements";
    public static final String advertisementsCollectionName = "advertisements";

    @Resource(name = "repo.mongo.template")
    private MongoTemplate mongoTemplate;

    @BeforeMethod
    public void setUp() throws AdvertisementAlreadyCreatedException {
        //Delete previous data.
        mongoTemplate.dropCollection(appAdvertisementsCollectionName);
        mongoTemplate.dropCollection(advertisementsCollectionName);

        //Creating two new advertisements
        Map<String, String> advertisement1 = new HashMap<String, String>();
        advertisement1.put(idK, "Ad_Initial1");
        advertisement1.put(messageK, "Ad_Text_Initial1");
        advertisement1.put(statusK, enableK);
        advertisementMongoRepository.createAd(advertisement1);

        Map<String, String> advertisement2 = new HashMap<String, String>();
        advertisement2.put(idK, "Ad_Initial2");
        advertisement2.put(messageK, "Ad_Text_Initial2");
        advertisement2.put(statusK, disableK);
        advertisementMongoRepository.createAd(advertisement2);

        //Linking ads and apps
        Map<String, String> appAdvertisement1 = new HashMap<String, String>();
        appAdvertisement1.put(idK, "App_Id1");
        appAdvertisement1.put(adNameK, "Ad_Initial1");

        Map<String, String> appAdvertisement2 = new HashMap<String, String>();
        appAdvertisement2.put(idK, "App_Id2");
        appAdvertisement2.put(adNameK, "Ad_Initial2");

        //Adding again an app for advertisement "Ad_Initial1" in order to sure multiple deletes in deleteAd method.
        Map<String, String> appAdvertisement3 = new HashMap<String, String>();
        appAdvertisement2.put(idK, "App_Id3");
        appAdvertisement2.put(adNameK, "Ad_Initial1");

        mongoTemplate.getCollection(appAdvertisementsCollectionName).save(new BasicDBObject(appAdvertisement1));
        mongoTemplate.getCollection(appAdvertisementsCollectionName).save(new BasicDBObject(appAdvertisement2));
    }

    @Test
    public void testFindAdNameByAppId() {
        assertEquals(advertisementMongoRepository.findAdNameByAppId("App_Id1"), "Ad_Initial1");
    }

    @Test
    public void testFindAdByAdName() {
       Map<String, String> advertisement = advertisementMongoRepository.findAdByAdName("Ad_Initial1");
       assertEquals(advertisement.get(messageK), "Ad_Text_Initial1");
    }

    @Test
    public void testFindAdByAppId() {
        Map<String, String> advertisement = advertisementMongoRepository.findAdByAppId("App_Id1");
        assertEquals(advertisement.get(messageK), "Ad_Text_Initial1");
    }

    @Test
    public void testGetAllAds() {
        List<Map<String, String>> allAds = new ArrayList<Map<String, String>>();
        allAds = advertisementMongoRepository.getAllAds();

        Map<String, String> advertisement1 = (Map<String, String>)mongoTemplate
                .getCollection(advertisementsCollectionName)
                .findOne(new QueryBuilder().put(idK).is("Ad_Initial1").get());
        Map<String, String> advertisement2 = (Map<String, String>)mongoTemplate
                .getCollection(advertisementsCollectionName)
                .findOne(new QueryBuilder().put(idK).is("Ad_Initial2").get());
        assertEquals(allAds.get(0), advertisement1);
        assertEquals(allAds.get(1), advertisement2);
    }

    @Test
    public void testUpdateApp() {
        Map<String, String> appAdvertisement = new HashMap<String, String>();
        appAdvertisement.put(idK, "App_Id1");
        appAdvertisement.put(adNameK, "Ad_Initial2");
        advertisementMongoRepository.updateApp(appAdvertisement);
        appAdvertisement = (Map<String, String>)mongoTemplate.getCollection(appAdvertisementsCollectionName)
                .findOne(new QueryBuilder().put(idK).is("App_Id1").get());

        assertEquals(appAdvertisement.get(adNameK), "Ad_Initial2");
    }

    @Test
    public void testCreateAd() throws AdvertisementAlreadyCreatedException {
        Map<String, String> advertisement = new HashMap<String, String>();
        advertisement.put(idK, "Ad_1");
        advertisement.put(messageK, "Ad_Text1");
        advertisement.put(statusK, enableK);
        advertisementMongoRepository.createAd(advertisement);
        advertisement = (Map<String, String>)mongoTemplate.getCollection(advertisementsCollectionName)
                .findOne(new QueryBuilder().put(idK).is("Ad_1").get());

        assertEquals(advertisement.get(messageK), "Ad_Text1");
    }

    @Test(expectedExceptions = AdvertisementAlreadyCreatedException.class)
    public void testCreateAdAlreadyCreatedException() throws AdvertisementAlreadyCreatedException {
        Map<String, String> advertisement = new HashMap<String, String>();
        advertisement.put(idK, "Ad_Initial1");
        advertisement.put(messageK, "Ad_Text_Initial2");
        advertisement.put(statusK, enableK);
        advertisementMongoRepository.createAd(advertisement);
    }

    @Test
    public void testEditAd() {
        Map<String, String> advertisement = new HashMap<String, String>();
        advertisement.put(idK, "Ad_Initial1");
        advertisement.put(messageK, "Ad_Text_Initial_Edited");
        advertisementMongoRepository.editAd(advertisement);
        advertisement = (Map<String, String>)mongoTemplate.getCollection(advertisementsCollectionName)
                .findOne(new QueryBuilder().put(idK).is("Ad_Initial1").get());

        assertEquals(advertisement.get(messageK), "Ad_Text_Initial_Edited");
    }

    @Test
    public void testDeleteAd() {
        advertisementMongoRepository.deleteAd("Ad_Initial1");
        Map<String, String> advertisement1 = (Map<String, String>)mongoTemplate
                .getCollection(advertisementsCollectionName)
                .findOne(new QueryBuilder().put(idK).is("Ad_Initial1").get());

        Map<String, String> advertisement2 = (Map<String, String>)mongoTemplate
                .getCollection(appAdvertisementsCollectionName)
                .findOne(new QueryBuilder().put(adNameK).is("Ad_Initial1").get());

        assertEquals(advertisement1, null);
        assertEquals(advertisement2, null);
    }

    @AfterTest
    public void tearDown() {
        advertisementMongoRepository = null;
        mongoTemplate = null;
    }
}