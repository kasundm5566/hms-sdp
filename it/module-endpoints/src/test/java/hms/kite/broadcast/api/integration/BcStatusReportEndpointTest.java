package hms.kite.broadcast.api.integration;

import hms.common.rest.util.Message;
import hms.kite.api.BaseAppAuthenticationEndpointTest;
import hms.kite.broadcast.integration.BcStatusReportEndpoint;
import hms.kite.sms.SmsAoServiceEndpoint;
import hms.kite.util.KiteErrorBox;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.Map;

import static hms.kite.util.KiteKeyBox.*;
import static org.testng.Assert.fail;

/**
 * Tests the Charging as a service for correct results and validations
 *
 * $LastChangedDate 7/18/11 9:51 AM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class BcStatusReportEndpointTest extends BaseAppAuthenticationEndpointTest {

    SmsAoServiceEndpoint smsAoServiceEndpoint;
    BcStatusReportEndpoint  bcStatusReportEndpoint;

    @BeforeTest
    public void setUp() {
        ApplicationContext ac = new ClassPathXmlApplicationContext(new String[]{"test-context.xml"});
        Mockery mockery = new Mockery();
        bcStatusReportEndpoint = (BcStatusReportEndpoint) ac.getBean("nblBcStatusReportEndpoint");
        smsAoServiceEndpoint = (SmsAoServiceEndpoint)ac.getBean("nblAoSmsEndpoint");
        Expectations expectations = new Expectations() {
            {
            }
        };
        mockery.checking(expectations);
    }

    @Test
    public void testSend() {
        try {
            Map<String, Object> smsMsg = populateTestBroadcastMessage(generateTestMessage());
            Map<String, Object> msg = populateValidRequest(generateTestMessage());

            MockHttpServletRequest servletRequest = generateServletRequest();
            Message smsBcResponse = null;//smsAoServiceEndpoint.sendService(null, servletRequest);
            Assert.assertEquals(smsBcResponse.get(statusCodeK), KiteErrorBox.successCode,
                    "Broadcast request should have been a success but got: "+smsBcResponse.get(statusDescriptionK));

            msg.put(messageIdK, smsBcResponse.get(correlationIdK));
            Map<String, Object> result = bcStatusReportEndpoint.sendService(msg, servletRequest);

            Assert.assertEquals(result.get(statusCodeK), KiteErrorBox.successCode,
                    "Broadcast status request should have been a success but got: "+result.get(statusDescriptionK));

            msg.put(messageIdK, "INVALID_CORRELATION_ID");
            result = bcStatusReportEndpoint.sendService(msg, servletRequest);
            Assert.assertEquals(result.get(statusCodeK), KiteErrorBox.invalidRequestErrorCode,
                    "Invalid BC message ID should return an invalid request but got: "+result.get(statusDescriptionK));
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test
    public void testSend_InvalidAppId() {
        try {
            Map<String, Object> msg = generateTestMessage();
            MockHttpServletRequest servletRequest = generateServletRequest();
            msg.put(appIdK, "NOAPP");
            Map<String, Object> result = bcStatusReportEndpoint.sendService(msg, servletRequest);

            Assert.assertEquals(result.get(statusCodeK), KiteErrorBox.appNotFoundErrorCode);
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    @Test
    public void testSend_InvalidPassword() {
        try {
            Map<String, Object> msg = generateTestMessage();
            MockHttpServletRequest servletRequest = generateServletRequest();

            msg.put(passwordK, "WRONG_P");
            Map<String, Object> result = bcStatusReportEndpoint.sendService(msg, servletRequest);

            Assert.assertEquals(result.get(statusCodeK), KiteErrorBox.authenticationFailedErrorCode);
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    @AfterTest
    public void tearDown() {

    }

    protected Map<String, Object> populateValidRequest(Map<String, Object> msg) {
        msg.put(broadcastAddressK, "ALL");
        return msg;
    }

    protected Map<String, Object> populateTestBroadcastMessage(Map<String, Object> msg) {
        msg.put(messageK, "Test Message");
        //msg.put(senderAddressK, "0717475488");
        msg.put(recipientAddressK, "0716212871");
        msg.put(broadcastAddressK, "ALL");

        return msg;
    }

}
