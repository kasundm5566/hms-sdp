package hms.kite.caas.http;

import hms.kite.caas.ListInstrumentsTest;

import java.util.Map;

/**
 * Web Test for /cas/list/pi
 *
 * $LastChangedDate 7/26/11 8:52 AM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
public class ListInstrumentsHTTPTest extends ListInstrumentsTest {

    @Override
    protected Map callListPIService(Map message) {
        return SimulateTxFlowHTTPTest.doCallListPIService(message);
    }
}
