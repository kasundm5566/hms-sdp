package hms.kite.caas;

import hms.kite.caas.api.NblParameter;
import hms.kite.services.standard.NumberMaskingChannel;
import hms.kite.util.KiteErrorBox;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Test for access request cas call
 * $LastChangedDate 7/26/11 8:52 AM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class AccessRequestTest extends CasRequestEndpointTest {

    @Test
    public void testAccessRequest_With_MSISDN() {

        Map msgReserve = generateTestMessage();
        populateRequestData(msgReserve, "tel:07012312");

        Map result = callAccessRequestService(msgReserve);

        assertResponseStatus(result, KiteErrorBox.successCode);

        Assert.assertNotNull(result.get(NblParameter.SUBSCRIBER_ID.getName()), "Subscriber Id is mandatory in response");
        Assert.assertNotNull(result.get(NblParameter.TIMESTAMP.getName()), "Timestamp is required in response");

    }

    @Test
    public void testAccessRequest_With_MSISDN_MaskedNumber() {

        String maskedMsisdn = maskMsisdn("07012312");
        Map msgReserve = generateTestMessage();
        msgReserve.put(NblParameter.APPLICATION_ID.getName(), "APP_T_00020");
        populateRequestData(msgReserve, "tel:"+maskedMsisdn);

        Map result = callAccessRequestService(msgReserve);

        assertResponseStatus(result, KiteErrorBox.successCode);

        Assert.assertNotNull(result.get(NblParameter.SUBSCRIBER_ID.getName()), "Subscriber Id is mandatory in response");
        Assert.assertNotNull(result.get(NblParameter.TIMESTAMP.getName()), "Timestamp is required in response");

    }

    @Test
    public void testAccessRequest_With_WrongCredentials() {
        Map msgReserve = generateTestMessage();
        //Wrong Password
        msgReserve.put(NblParameter.PASSWORD.getName(), "WRONG_PASSWORD");

        populateRequestData(msgReserve, "tel:07012312");

        Map result = callAccessRequestService(msgReserve);

        assertResponseStatus(result, KiteErrorBox.authenticationFailedErrorCode);

        //Put the correct password for invalid app
        msgReserve.put(NblParameter.PASSWORD.getName(), "123");
        msgReserve.put(NblParameter.APPLICATION_ID.getName(), "WRONG_APP_ID");

        result = callAccessRequestService(msgReserve);

        assertResponseStatus(result, KiteErrorBox.appNotFoundErrorCode);

    }

    @Test
    public void testAccessRequest_With_InvalidSubscriberId() {

        Map msgReserve = generateTestMessage();
        populateRequestData(msgReserve, "07012312");

        Map result = callAccessRequestService(msgReserve);

        assertResponseStatus(result, KiteErrorBox.invalidRequestErrorCode);

        populateRequestData(msgReserve, null);

        result = callAccessRequestService(msgReserve);

        assertResponseStatus(result, KiteErrorBox.invalidRequestErrorCode);

    }

    /**
     * Populates the reservation data to the supplied map
     *
     * @param msgReserve
     * @param subscriberId
     */
    protected static void populateRequestData(Map msgReserve, String subscriberId) {

        if(subscriberId != null)
            msgReserve.put(NblParameter.SUBSCRIBER_ID.getName(), subscriberId);

    }

    protected Map callAccessRequestService(Map message) {
        MockHttpServletRequest servletRequest = generateServletRequest();
        Map result = casRequestEndpoint.accessRequestService(message, servletRequest);
        return result;
    }

    protected Map<String, Object> generateTestMessage() {
        Map<String, Object> msg = super.generateTestMessage();
        msg.put(NblParameter.APPLICATION_ID.getName(), "APP_T_00021");
        return msg;
    }
}
