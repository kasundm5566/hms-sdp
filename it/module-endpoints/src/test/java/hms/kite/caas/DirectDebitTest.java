package hms.kite.caas;

import hms.kite.caas.api.NblParameter;
import hms.kite.services.standard.NumberMaskingChannel;
import hms.kite.util.KiteErrorBox;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Test for direct debit cas call
 * $LastChangedDate 7/26/11 8:52 AM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class DirectDebitTest extends CasRequestEndpointTest {

    /**
     * Tests the direct debit request for given payment instrument
     */
    @Test
    public void testDirectDebit_With_MSISDN() {

        Map msgReserve = generateTestMessage();
        String myTxId = "TX0000001";
        CreditReserveTest.populateReservationData(msgReserve, myTxId, "tel:07012312",
                "ABC Bank", "123123123",
                "5", "USD",
                "T/INV/00001", "T/PO/2011/07/00001");

        Map result = callTransferService(msgReserve);

        assertResponseStatus(result, KiteErrorBox.successCode);

        Assert.assertNotNull(result.get(NblParameter.INTERNAL_TRX_ID.getName()), "Internal Tx Id is mandatory in response");
        Assert.assertEquals(result.get(NblParameter.EXTERNAL_TRX_ID.getName()), myTxId,  "External Tx Id should be the one originally supplied");
        Assert.assertNotNull(result.get(NblParameter.TIMESTAMP.getName()), "Timestamp is required in response");

    }

    /**
     * Tests the direct debit request for default payment instrument
     */
    @Test
    public void testDirectDebit_With_MSISDN_DefaultPaymentInstrument() {

        Map msgReserve = generateTestMessage();
        String myTxId = "TX0000001";
        CreditReserveTest.populateReservationData(msgReserve, myTxId, "tel:07012312",
                null, null,
                "5", "USD",
                "T/INV/00001", "T/PO/2011/07/00001");

        Map result = callTransferService(msgReserve);

        assertResponseStatus(result, KiteErrorBox.successCode);

        Assert.assertNotNull(result.get(NblParameter.INTERNAL_TRX_ID.getName()), "Internal Tx Id is mandatory in response");
        Assert.assertEquals(result.get(NblParameter.EXTERNAL_TRX_ID.getName()), myTxId,  "External Tx Id should be the one originally supplied");
        Assert.assertNotNull(result.get(NblParameter.TIMESTAMP.getName()), "Timestamp is required in response");

    }

    /**
     * Tests the direct debit request for default payment instrument
     */
    @Test
    public void testDirectDebit_With_MSISDN_DefaultPaymentInstrument_MaskedNumber() {

        String maskedMsisdn = maskMsisdn("07012312");
        Map msgReserve = generateTestMessage();
        msgReserve.put(NblParameter.APPLICATION_ID.getName(), "APP_T_00023");
        String myTxId = "TX0000001";
        CreditReserveTest.populateReservationData(msgReserve, myTxId, "tel:"+maskedMsisdn,
                null, null,
                "5", "USD",
                "T/INV/00001", "T/PO/2011/07/00001");

        Map result = callTransferService(msgReserve);

        assertResponseStatus(result, KiteErrorBox.successCode);

        Assert.assertNotNull(result.get(NblParameter.INTERNAL_TRX_ID.getName()), "Internal Tx Id is mandatory in response");
        Assert.assertEquals(result.get(NblParameter.EXTERNAL_TRX_ID.getName()), myTxId,  "External Tx Id should be the one originally supplied");
        Assert.assertNotNull(result.get(NblParameter.TIMESTAMP.getName()), "Timestamp is required in response");

    }

    protected Map callTransferService(Map message) {
        MockHttpServletRequest servletRequest = generateServletRequest();
        Map result = casRequestEndpoint.directDebitService(message, servletRequest);
        return result;
    }
}
