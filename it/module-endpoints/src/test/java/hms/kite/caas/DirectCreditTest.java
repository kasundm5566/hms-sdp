package hms.kite.caas;

import hms.kite.caas.api.NblParameter;
import hms.kite.util.KiteErrorBox;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Map;

/**
 * Test for direct debit cas call
 * $LastChangedDate 7/26/11 8:52 AM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class DirectCreditTest extends DirectDebitTest {


    @Override
    protected Map callTransferService(Map message) {
        MockHttpServletRequest servletRequest = generateServletRequest();
        Map result = casRequestEndpoint.directCreditService(message, servletRequest);
        return result;
    }
}
