package hms.kite.caas;

import hms.kite.api.BaseAppAuthenticationEndpointTest;
import hms.kite.caas.api.CasRequestEndpoint;
import hms.kite.caas.api.NblParameter;
import hms.kite.services.standard.NumberMaskingChannel;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Tests the Charging as a service for correct results and validations
 *
 * $LastChangedDate 7/18/11 9:51 AM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class CasRequestEndpointTest extends BaseAppAuthenticationEndpointTest {


    CasRequestEndpoint casRequestEndpoint;

    @BeforeTest
    public void setUp() {
        ApplicationContext ac = new ClassPathXmlApplicationContext(new String[]{"test-context.xml"});
        Mockery mockery = new Mockery();
        casRequestEndpoint = (hms.kite.caas.api.CasRequestEndpoint) ac.getBean("casRequestEndpoint");
        Expectations expectations = new Expectations() {
            {
            }
        };
        mockery.checking(expectations);
    }

    @Test
    public void testSend() {
//        try {
//            Map msg = generateTestMessage();
//            MockHttpServletRequest servletRequest = generateServletRequest();
//            Map result = casRequestEndpoint.accessRequestService(msg, servletRequest);
//
//            Assert.assertEquals(result.get(statusCodeK), KiteErrorBox.successCode);
//        } catch (Exception ex) {
//            fail(ex.getMessage());
//        }
    }



    @AfterTest
    public void tearDown() {

    }

    /**
     * Shortcut method to assert the response status
     *
     * @param statusCode
     */
    protected void assertResponseStatus(Map result, String statusCode) {
        Assert.assertEquals(result.get(NblParameter.STATUS_CODE.getName()), statusCode);
        Assert.assertTrue(result.get(NblParameter.STATUS_TEXT.getName()) != null, "Response should have status message");
    }

    /**
     * Masks the given MSISDN for testing purposes
     *
     * @param msisdn
     * @return
     */
    protected String maskMsisdn(String msisdn) {
        NumberMaskingChannel numberMaskingChannel =  new NumberMaskingChannel();
        Map<String, String> encKeys = new HashMap<String, String>();
        encKeys.put("AZ110", "63pq%n89y(*6HK%$^@JK%^!@");
        numberMaskingChannel.setEncryptionKeys(encKeys);
        numberMaskingChannel.init();

        String maskedMsisdn =numberMaskingChannel.encrypt(msisdn);
        return maskedMsisdn;
    }

}
