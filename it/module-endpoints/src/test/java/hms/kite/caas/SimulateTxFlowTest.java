package hms.kite.caas;

import hms.kite.broadcast.integration.wf.NblBcStatusReportWf;
import hms.kite.caas.api.CasRequestEndpoint;
import hms.kite.caas.api.NblParameter;
import hms.kite.services.standard.NumberMaskingChannel;
import hms.kite.util.KiteErrorBox;
import hms.kite.util.KiteKeyBox;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.testng.Assert.fail;

/**
 * Test for Charging As a Service NBL flow
 * $LastChangedDate 7/26/11 8:52 AM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class SimulateTxFlowTest extends CasRequestEndpointTest {


    /**
     * Tests whether the PGW allows credit reserve and commit the reserved credit.
     * This does not verify the commit has actually does the financial transaction.
     * It only checks for the messages from PGW.
     */
    @Test
    public void testSimulateSuccess_With_MSISDN() {
        try {
            Map listPiMsg = generateTestMessage();
            listPiMsg.put(NblParameter.SUBSCRIBER_ID.getName(), "tel:07012312");
            Map listPiResult = callListPIService(listPiMsg);

            List<Map<String, String>> piList = (List<Map<String, String>> )listPiResult.get(NblParameter.PAYMENT_INSTRUMENT.getName());

            Assert.assertTrue(piList != null, "Payment instruments should be in the response");
            Assert.assertTrue(piList.size() >0 , "There should be at least one Payment instrument");

            //Get the first Payment instrument and charge USD 1 and commit
            Map<String, String>  piSelected =  piList.get(0);
            String piName = piSelected.get(NblParameter.PAYMENT_INSTRUMENT_NAME);
            String piAccountId = piSelected.get(NblParameter.PAYMENT_INSTRUMENT_ID);

            Map reserveCreditMsg = generateTestMessage();
            CreditReserveTest.populateReservationData(reserveCreditMsg, "TX0000002", "tel:07012312",
                    piName,  piAccountId,
                    "100", "USD",
                    "T/INV/00002", "T/PO/2011/07/00002");

            Map reserveCreditResult = callReserveService(reserveCreditMsg);

            Assert.assertEquals(reserveCreditResult.get(NblParameter.STATUS_CODE.getName()), KiteErrorBox.successCode);

            String reservationId = (String) reserveCreditResult.get(NblParameter.RESERVATION_ID.getName());

            //Now commit the Transaction
            Map commitCreditMsg = generateTestMessage();
            commitCreditMsg.put(NblParameter.RESERVATION_ID.getName(), reservationId);
            Map commitCreditResult = callCommitService(commitCreditMsg);

            Assert.assertEquals(commitCreditResult.get(NblParameter.STATUS_CODE.getName()), KiteErrorBox.successCode);
            Assert.assertTrue(commitCreditResult.get(NblParameter.STATUS_TEXT.getName()) != null, "Response should have status message");

         } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    /**
     * Tests whether the PGW allows credit reserve and commit the reserved credit with default instrument.
     * The "Payment Instrument Name" and "Account ID" are not sent so that Payment gateway will treat it as
     * a request for default instrument.
     * This does not verify the commit has actually does the financial transaction.
     * It only checks for the messages from PGW.
     */
    @Test
    public void testSimulateSuccess_With_MSISDN_DefaultPaymentInstrument() {
        try {

            Map reserveCreditMsg = generateTestMessage();
            CreditReserveTest.populateReservationData(reserveCreditMsg, "TX0000002", "tel:07012312",
                    null,  null,
                    "100", "USD",
                    "T/INV/00002", "T/PO/2011/07/00002");

            Map reserveCreditResult = callReserveService(reserveCreditMsg);

            Assert.assertEquals(reserveCreditResult.get(NblParameter.STATUS_CODE.getName()), KiteErrorBox.successCode);

            String reservationId = (String) reserveCreditResult.get(NblParameter.RESERVATION_ID.getName());

            //Now commit the Transaction
            Map commitCreditMsg = generateTestMessage();
            commitCreditMsg.put(NblParameter.RESERVATION_ID.getName(), reservationId);
            Map commitCreditResult = callCommitService(commitCreditMsg);

            Assert.assertEquals(commitCreditResult.get(NblParameter.STATUS_CODE.getName()), KiteErrorBox.successCode);
            Assert.assertTrue(commitCreditResult.get(NblParameter.STATUS_TEXT.getName()) != null, "Response should have status message");

         } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }


        /**
     * Tests whether the PGW allows credit reserve and commit the reserved credit with default instrument.
     * The "Payment Instrument Name" and "Account ID" are not sent so that Payment gateway will treat it as
     * a request for default instrument.
     * This does not verify the commit has actually does the financial transaction.
     * It only checks for the messages from PGW.
     */
    @Test
    public void testSimulateSuccess_With_MSISDN_DefaultPaymentInstrument_MaskedNumber() {
        try {
            String maskedMsisdn = maskMsisdn("07012312");
            Map reserveCreditMsg = generateTestMessage();
            reserveCreditMsg.put(NblParameter.APPLICATION_ID.getName(), "APP_T_00023");
            CreditReserveTest.populateReservationData(reserveCreditMsg, "TX0000002", "tel:"+maskedMsisdn,
                    null,  null,
                    "100", "USD",
                    "T/INV/00002", "T/PO/2011/07/00002");

            Map reserveCreditResult = callReserveService(reserveCreditMsg);

            Assert.assertEquals(reserveCreditResult.get(NblParameter.STATUS_CODE.getName()), KiteErrorBox.successCode);

            String reservationId = (String) reserveCreditResult.get(NblParameter.RESERVATION_ID.getName());

            //Now commit the Transaction
            Map commitCreditMsg = generateTestMessage();
            commitCreditMsg.put(NblParameter.RESERVATION_ID.getName(), reservationId);
            Map commitCreditResult = callCommitService(commitCreditMsg);

            Assert.assertEquals(commitCreditResult.get(NblParameter.STATUS_CODE.getName()), KiteErrorBox.successCode);
            Assert.assertTrue(commitCreditResult.get(NblParameter.STATUS_TEXT.getName()) != null, "Response should have status message");

         } catch (Exception ex) {
            ex.printStackTrace();
            fail(ex.getMessage());
        }
    }

    /**
     * Tests whether the PGW allows credit reserve and cancel the reserved credit.
     * This does not verify the commit has actually does the financial transaction.
     * It only checks for the messages from PGW.
     */
    @Test
    public void testSimulateCancel_With_MSISDN() {
        try {
            Map listPiMsg = generateTestMessage();
            listPiMsg.put(NblParameter.SUBSCRIBER_ID.getName(), "tel:07012312");
            Map listPiResult = callListPIService(listPiMsg);

            List<Map<String, String>> piList = (List<Map<String, String>> )listPiResult.get(NblParameter.PAYMENT_INSTRUMENT.getName());

            Assert.assertTrue(piList != null, "Payment instruments should be in the response");
            Assert.assertTrue(piList.size() >0 , "There should be at least one Payment instrument");

            //Get the first Payment instrument and charge USD 1 and commit
            Map<String, String>  piSelected =  piList.get(0);
            String piName = piSelected.get(NblParameter.PAYMENT_INSTRUMENT_NAME);
            String piAccountId = piSelected.get(NblParameter.PAYMENT_INSTRUMENT_ID);

            Map reserveCreditMsg = generateTestMessage();
            CreditReserveTest.populateReservationData(reserveCreditMsg, "TX0000002", "tel:07012312",
                    piName,  piAccountId,
                    "100", "USD",
                    "T/INV/00002", "T/PO/2011/07/00002");

            Map reserveCreditResult = callReserveService(reserveCreditMsg);

            Assert.assertEquals(reserveCreditResult.get(NblParameter.STATUS_CODE.getName()), KiteErrorBox.successCode);

            String reservationId = (String) reserveCreditResult.get(NblParameter.RESERVATION_ID.getName());

            //Now commit the Transaction
            Map commitCreditMsg = generateTestMessage();
            commitCreditMsg.put(NblParameter.RESERVATION_ID.getName(), reservationId);
            Map commitCreditResult = callCancelService(commitCreditMsg);

            Assert.assertEquals(commitCreditResult.get(NblParameter.STATUS_CODE.getName()), KiteErrorBox.successCode);
            Assert.assertTrue(commitCreditResult.get(NblParameter.STATUS_TEXT.getName()) != null, "Response should have status message");

         } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }



    protected Map callListPIService(Map message) {
        MockHttpServletRequest servletRequest = generateServletRequest();
        Map result = casRequestEndpoint.listPaymentInstrumentsService(message, servletRequest);
        return result;
    }

    protected Map callReserveService(Map message) {
        MockHttpServletRequest servletRequest = generateServletRequest();
        Map result = casRequestEndpoint.reserveService(message, servletRequest);
        return result;
    }

    protected Map callCommitService( Map message) {
        MockHttpServletRequest servletRequest = generateServletRequest();
        Map result = casRequestEndpoint.commitService(message, servletRequest);
        return result;
    }

    protected Map callCancelService(Map message) {
        MockHttpServletRequest servletRequest = generateServletRequest();
        Map result = casRequestEndpoint.cancelService(message, servletRequest);
        return result;
    }
}
