package hms.kite.caas;

import hms.kite.caas.api.NblParameter;
import hms.kite.util.KiteErrorBox;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Map;

import static hms.kite.util.KiteKeyBox.statusCodeK;
import static hms.kite.util.KiteKeyBox.statusDescriptionK;
import static org.testng.Assert.fail;

/**
 * Tests the charging commit message
 *
 * $LastChangedDate 7/26/11 8:52 AM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class CreditCommitTest extends CasRequestEndpointTest {

    @Test
    public void testCreditCommit_With_MSISDN() {
        try {
            Map msgReserve = generateTestMessage();
            msgReserve.put(NblParameter.EXTERNAL_TRX_ID.getName(), "TX0000001");
            msgReserve.put(NblParameter.SUBSCRIBER_ID.getName(), "tel:T123123123");
            msgReserve.put(NblParameter.PAYMENT_INSTRUMENT_NAME.getName(), "WALLET");
            msgReserve.put(NblParameter.AMOUNT.getName(), "100");
            msgReserve.put(NblParameter.CURRENCY.getName(), "USD");
            msgReserve.put(NblParameter.INVOICE_NO.getName(), "T/INV/00001");
            msgReserve.put(NblParameter.ORDER_NO.getName(), "T/PO/2011/07/00001");


            MockHttpServletRequest servletRequest = generateServletRequest();
            Map result = casRequestEndpoint.reserveService(msgReserve, servletRequest);

            Assert.assertEquals(result.get(statusCodeK), KiteErrorBox.successCode);
            Assert.assertTrue(result.get(statusDescriptionK) != null,  "Response should have status message");
//            Assert.assertTrue(result.get(correlationIdK) != null,  "Response should have correlation ID");
//            Assert.assertTrue(result.get("reservation-id") != null,  "Response should have reservation ID");  //TODO: KiteKeyBox <-
            String reservationId = (String)result.get(NblParameter.RESERVATION_ID.getName());
            servletRequest = generateServletRequest();
            Map msgCommit = generateTestMessage();
            msgReserve.put(NblParameter.RESERVATION_ID.getName(), reservationId);
            result = casRequestEndpoint.reserveService(msgCommit, servletRequest);
            Assert.assertEquals(result.get(statusCodeK), KiteErrorBox.successCode);
//            Assert.assertTrue(result.get(timestampK) != null, "Timestamp should exist in the commit response");
            Assert.assertTrue(result.get(statusDescriptionK)!= null, "Status Description should exist in the commit response");

        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }
}
