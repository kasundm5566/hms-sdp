package hms.kite.caas;

import hms.kite.caas.api.CasRequestEndpoint;
import hms.kite.caas.api.NblParameter;
import hms.kite.util.KiteErrorBox;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Map;

import static org.testng.Assert.fail;

/**
 * Test for credit reserve cas call
 * $LastChangedDate 7/26/11 8:52 AM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class CreditReserveTest extends CasRequestEndpointTest {

    @Test
    public void testCreditReserve_With_MSISDN() {

        Map msgReserve = generateTestMessage();
        populateReservationData(msgReserve, "TX0000001", "tel:07012312",
                "ABC Bank", "123123123",
                "100", "USD",
                "T/INV/00001", "T/PO/2011/07/00001");

        Map result = callReserveService(msgReserve);

        assertResponseStatus(result, KiteErrorBox.successCode);

        Assert.assertNotNull(result.get(NblParameter.RESERVATION_ID.getName()), "Reservation Id is mandatory in response");
        Assert.assertNotNull(result.get(NblParameter.TIMESTAMP.getName()), "Timestamp is required in response");

    }

    @Test
    public void testCreditReserve_With_UntrustedApplication() {

        Map msgReserve = generateTestMessage();
        populateReservationData(msgReserve, "TX0000001", "tel:07012312",
                "ABC Bank", "123123123",
                "100", "USD",
                "T/INV/00001", "T/PO/2011/07/00001");

        //Untrusted app id
        msgReserve.put(NblParameter.APPLICATION_ID.getName(), "APP_T_00021");

        Map result = callReserveService(msgReserve);

        assertResponseStatus(result, KiteErrorBox.chargingErrorCode);

    }


    @Test
    public void testCreditReserve_With_MSISDN_AmountTooHighOrLow() {

        Map msgReserve = generateTestMessage();
        populateReservationData(msgReserve, "TX0000002", "tel:07012312",
                "ABC Bank", "123123123",
                "120", "USD",
                "T/INV/00002", "T/PO/2011/07/00002");

        Map result = callReserveService(msgReserve);

        assertResponseStatus(result, KiteErrorBox.chargingAmountTooHighErrorCode);

        msgReserve.put(NblParameter.AMOUNT.getName(), "10");
        result = callReserveService(msgReserve);

        assertResponseStatus(result, KiteErrorBox.chargingAmountTooLowErrorCode);

    }

    @Test
    public void testCreditReserve_With_WrongCredentials() {
        Map msgReserve = generateTestMessage();
        //Wrong Password
        msgReserve.put(NblParameter.PASSWORD.getName(), "WRONG_PASSWORD");

        populateReservationData(msgReserve, "TX0000001", "tel:07012312",
                "ABC Bank", "123123123",
                "100", "USD",
                "T/INV/00001", "T/PO/2011/07/00001");

        Map result = callReserveService(msgReserve);

        assertResponseStatus(result, KiteErrorBox.authenticationFailedErrorCode);

        //Put the correct password for invalid app
        msgReserve.put(NblParameter.PASSWORD.getName(), "123");
        msgReserve.put(NblParameter.APPLICATION_ID.getName(), "WRONG_APP_ID");

        result = callReserveService(msgReserve);

        assertResponseStatus(result, KiteErrorBox.appNotFoundErrorCode);

    }

    @Test
    public void testCreditReserve_With_InvalidSubscriberId() {

        Map msgReserve = generateTestMessage();
        populateReservationData(msgReserve,  "TX0000001", "WRONG_SUBSCRIBER_ID",
                "ABC Bank", "123123123",
                "100", "USD",
                "T/INV/00001", "T/PO/2011/07/00001");

        Map result = callReserveService(msgReserve);

        assertResponseStatus(result, KiteErrorBox.invalidRequestErrorCode);

    }


    /**
     * Populates the reservation data to the supplied map
     *
     * @param msgReserve
     * @param externalTxId
     * @param subscriberId
     * @param paymentInstrumentName
     * @param accountId
     * @param amount
     * @param currency
     * @param invoiceNo
     * @param orderNo
     */
    protected static void populateReservationData(Map msgReserve, String externalTxId, String subscriberId,
                                         String paymentInstrumentName, String accountId,
                                         String amount, String currency, String invoiceNo, String orderNo) {

        if(externalTxId != null)
            msgReserve.put(NblParameter.EXTERNAL_TRX_ID.getName(), externalTxId);
        if(subscriberId != null)
            msgReserve.put(NblParameter.SUBSCRIBER_ID.getName(), subscriberId);
        if(paymentInstrumentName != null)
            msgReserve.put(NblParameter.PAYMENT_INSTRUMENT_NAME.getName(), paymentInstrumentName);
        if(accountId != null)
            msgReserve.put(NblParameter.ACCOUNT_ID.getName(), accountId);
        if(amount != null)
            msgReserve.put(NblParameter.AMOUNT.getName(),amount );
        if(currency != null)
            msgReserve.put(NblParameter.CURRENCY.getName(), currency);
        if(invoiceNo != null)
            msgReserve.put(NblParameter.INVOICE_NO.getName(), invoiceNo);
        if(orderNo != null)
            msgReserve.put(NblParameter.ORDER_NO.getName(), orderNo);
    }

    protected Map callReserveService(Map message) {
        MockHttpServletRequest servletRequest = generateServletRequest();
        Map result = casRequestEndpoint.reserveService(message, servletRequest);
        return result;
    }
}
