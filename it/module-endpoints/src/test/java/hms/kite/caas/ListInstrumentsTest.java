package hms.kite.caas;

import hms.kite.caas.api.CasRequestEndpoint;
import hms.kite.caas.api.NblParameter;
import hms.kite.services.standard.NumberMaskingChannel;
import hms.kite.util.KiteErrorBox;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.testng.Assert.fail;

/**
 * Test for list payment instruments
 * $LastChangedDate 7/26/11 8:52 AM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class ListInstrumentsTest extends CasRequestEndpointTest {

    @Test
    public void testListPI_With_UserId() {
        try {
            Map msgReserve = generateTestMessage();
            //msgReserve.put(NblParameter.SUBSCRIBER_ID.getName(), "tel:07012312");
            msgReserve.put(NblParameter.SUBSCRIBER_ID.getName(), "id:20110815164214165");

            doTestResponse(msgReserve);
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }


    @Test
    public void testListPI_With_MSISDN() {
        try {
            Map msgReserve = generateTestMessage();
            msgReserve.put(NblParameter.SUBSCRIBER_ID.getName(), "tel:07012312");

            doTestResponse(msgReserve);

        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }


        @Test
    public void testListPI_With_MSISDN_MaskedNumber() {

        try {
            String maskedMsisdn = maskMsisdn("07012312");
            Map msgReserve = generateTestMessage();
            msgReserve.put(NblParameter.APPLICATION_ID.getName(), "APP_T_00023");

            msgReserve.put(NblParameter.SUBSCRIBER_ID.getName(), "tel:"+maskedMsisdn);

            doTestResponse(msgReserve);

        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    /**
     * Do the test response
     *
     * @param msgReserve
     */
    private void doTestResponse(Map msgReserve) {
        Map result = callListPIService(msgReserve);

        assertResponseStatus(result, KiteErrorBox.successCode);
        Assert.assertTrue(result.get(NblParameter.PAYMENT_INSTRUMENT.getName()) != null, "Payment instruments should be in the response");
        Assert.assertTrue(result.get(NblParameter.PAYMENT_INSTRUMENT.getName()) instanceof List, "Payment instruments should be a List");
        Assert.assertTrue(((List)result.get(NblParameter.PAYMENT_INSTRUMENT.getName())).size() >0 , "There should be at least one Payment instrument");
    }

    protected Map callListPIService(Map message) {
        MockHttpServletRequest servletRequest = generateServletRequest();
        Map result = casRequestEndpoint.listPaymentInstrumentsService(message, servletRequest);
        return result;
    }
}
