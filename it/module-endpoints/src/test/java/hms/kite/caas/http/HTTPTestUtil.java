package hms.kite.caas.http;

import hms.common.rest.util.JsonBodyProvider;
import org.apache.cxf.jaxrs.client.WebClient;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple utility for web tests
 * $LastChangedDate 8/4/11 10:08 AM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 * To change this template use File | Settings | File Templates.
 */
public class HTTPTestUtil {
    public static WebClient createClient(String relativePath) {
        List<Object> bodyProviders = new ArrayList<Object>();
        bodyProviders.add(new JsonBodyProvider());
        WebClient webClient = WebClient.create("http://127.0.0.1:7000"+relativePath, bodyProviders);
        webClient.header("Content-Type", "application/json");
        webClient.accept("application/json");;
        return webClient;
    }
}
