package hms.kite.caas.http;

import hms.kite.caas.SimulateTxFlowTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.Map;

import static org.testng.Assert.fail;

/**
 * Test for Charging As a Service NBL flow
 * $LastChangedDate 7/26/11 8:52 AM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class SimulateTxFlowHTTPTest extends SimulateTxFlowTest {

    @Override
    protected Map callListPIService(Map message) {
        return doCallListPIService(message);
    }

    @Override
    protected Map callReserveService(Map message) {
        return doCallReserveService(message);
    }

    @Override
    protected Map callCommitService( Map message) {
        return doCallCommitService(message);
    }

    @Override
    protected Map callCancelService( Map message) {
        return doCallCancelService(message);
    }

    static Map doCallListPIService(Map message) {

        javax.ws.rs.core.Response response = HTTPTestUtil.createClient("/caas/list/pi").post(message);
        Map result = (Map) response.getEntity();

        return result;
    }

    static Map doCallReserveService(Map message) {

        javax.ws.rs.core.Response response = HTTPTestUtil.createClient("/caas/credit/reserve").post(message);
        Map result = (Map) response.getEntity();

        return result;
    }

    static Map doCallCommitService( Map message) {
        javax.ws.rs.core.Response response = HTTPTestUtil.createClient("/caas/credit/commit").post(message);
        Map result = (Map) response.getEntity();

        return result;
    }

    static Map doCallCancelService( Map message) {
        javax.ws.rs.core.Response response = HTTPTestUtil.createClient("/caas/credit/cancel").post(message);
        Map result = (Map) response.getEntity();

        return result;
    }
}
