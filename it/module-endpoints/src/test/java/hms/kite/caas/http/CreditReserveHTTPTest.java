package hms.kite.caas.http;

import hms.kite.caas.CreditReserveTest;

import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * $LastChangedDate 8/4/11 9:34 AM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 * To change this template use File | Settings | File Templates.
 */
public class CreditReserveHTTPTest extends CreditReserveTest {

    @Override
    protected Map callReserveService(Map message) {
        return SimulateTxFlowHTTPTest.doCallReserveService(message);
    }
}
