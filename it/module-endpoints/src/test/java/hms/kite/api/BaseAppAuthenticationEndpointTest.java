package hms.kite.api;

import hms.common.rest.util.Message;
import hms.kite.caas.api.NblParameter;
import hms.kite.util.KiteErrorBox;
import org.springframework.mock.web.MockHttpServletRequest;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static hms.kite.util.KiteKeyBox.appIdK;
import static hms.kite.util.KiteKeyBox.passwordK;
import static hms.kite.util.KiteKeyBox.statusCodeK;
import static org.testng.Assert.fail;

/**
 * Base test for module endpoints
 *
 * $LastChangedDate 7/19/11 9:01 AM$
 * $LastChangedBy ruwan$
 * $LastChangedRevision$
 */
public class BaseAppAuthenticationEndpointTest {


    protected Map<String, Object> generateTestMessage() {
        Map<String, Object> msg = new HashMap<String, Object> ();
        msg.put(NblParameter.PASSWORD.getName(), "123");
        msg.put(NblParameter.APPLICATION_ID.getName(), "APP_T_00022");
        return msg;
    }



    protected static MockHttpServletRequest generateServletRequest() {
        MockHttpServletRequest servletRequest = new MockHttpServletRequest("Post", "/cas/request/access");
        servletRequest.setRemoteHost("127.0.0.1");
        return servletRequest;
    }
}
