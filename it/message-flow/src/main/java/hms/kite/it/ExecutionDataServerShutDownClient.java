/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.it;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class ExecutionDataServerShutDownClient {

    private static byte[] stopOrder = new byte[]{1, 2, 3, 4};

    public static void main(String[] args) throws IOException, InterruptedException {
        Socket clientSocket = new Socket("it-report.sdp.virtualcity", 6301);
        OutputStream outputStream = clientSocket.getOutputStream();
        try {
            System.out.println("Start shutdown signal");
            for (byte b : stopOrder) {
                Thread.sleep(5);
                System.out.println("Sending signal....");
                outputStream.write(b);
                outputStream.flush();
                byte[] response = new byte[8];
                clientSocket.getInputStream().read(response);
                System.out.println(new String(response));
            }
        } finally {
            clientSocket.close();
        }
    }
}
