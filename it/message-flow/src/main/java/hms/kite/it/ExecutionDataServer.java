/*******************************************************************************
 * Copyright (c) 2009, 2011 Mountainminds GmbH & Co. KG and Contributors
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Marc R. Hoffmann - initial API and implementation
 *
 *******************************************************************************/
package hms.kite.it;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

import org.jacoco.core.data.ExecutionData;
import org.jacoco.core.data.IExecutionDataVisitor;
import org.jacoco.core.data.ISessionInfoVisitor;
import org.jacoco.core.data.SessionInfo;
import org.jacoco.core.runtime.RemoteControlReader;
import org.jacoco.core.runtime.RemoteControlWriter;

/**
 * This example starts a socket server to collect coverage from agents that run
 * in output mode <code>tcpclient</code>. The collected data is dumped to a
 * local file.
 */
public class ExecutionDataServer {

    private static final String DESTFILE = "target/jacoco-server.exec";

    private static final String ADDRESS = "0.0.0.0";

    private static final int PORT = 6300;

    /**
     * Start the server as a standalone program.
     *
     * @param args
     * @throws java.io.IOException
     */
    public static void main(final String[] args) throws IOException, InterruptedException {
        final ServerSocket serverSocket = new ServerSocket(PORT, 0,
                InetAddress.getByName(ADDRESS));

        System.out.println("=============================================================");
        System.out.println("===      Jacoco Coverage report collector Starting....    ===");
        System.out.println("=============================================================");
        Thread.sleep(6 * 1000);

        Runnable server = new Runnable() {
            public void run() {
                try {
                    final RemoteControlWriter fileWriter = new RemoteControlWriter(
                            new FileOutputStream(DESTFILE));

                    while (true) {
                        final Handler handler = new Handler(serverSocket.accept(), fileWriter);
                        new Thread(handler).start();
                        Thread.yield();
                    }
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            }
        };
        final Thread serverThread = new Thread(server);
        serverThread.start();

        System.out.println("=============================================================");
        System.out.println("===        Jacoco Coverage report collector Started       ===");
        System.out.println("=============================================================");
        byte[] stopOrder = new byte[]{1, 2, 3, 4};

        ServerSocket shutdownSocket = new ServerSocket(6301);

        System.out.println("=============================================================");
        System.out.println("===              Jacoco Shutdown Server  Started          ===");
        System.out.println("=============================================================");

        for (;;) {
            Socket socket = shutdownSocket.accept();
            InputStream inputStream = socket.getInputStream();
            int index = 0;
            int read;
            while ( (read = inputStream.read()) != -1) {

                if (stopOrder[index] == read && stopOrder.length -1 == index) {

                    //break work is done
                    System.out.println("[jacoco] ====================================================");
                    System.out.println("[jacoco] =====       Shutdown Signal Received.          =====");
                    System.out.println("[jacoco] =====   Stop the Coverage Report Collector     =====");
                    System.out.println("[jacoco] ====================================================");
                    System.out.println("[jacoco]   Stop the server thread");
                    serverSocket.close();
                    serverThread.interrupt();
                    socket.getOutputStream().write("Shutdown".getBytes());
                    socket.getOutputStream().flush();
                    inputStream.close();
                    shutdownSocket.close();

                    System.out.println("[jacoco] ====================================================");
                    System.out.println("[jacoco] ===        Shutdown completed Successfully       ===");
                    System.out.println("[jacoco] ====================================================");
                    System.exit(0);
                    return;
                } else if (stopOrder[index] == read) {
                    System.out.println("[jacoco] ====================================================");
                    System.out.println("[jacoco] ===        " + (index + 1) + " Signal Received.       ===");
                    System.out.println("[jacoco] ====================================================");
                    index++;
                    socket.getOutputStream().write("Accepted".getBytes());
                    socket.getOutputStream().flush();
                    //continue since it is matched....
                } else {
                    // not matched should be something else, waiting for the socket.
                    System.out.println("[jacoco] ==================================================================");
                    System.out.println("[jacoco] === Unexpected byte order, discard the shutdown request.       ===");
                    System.out.println("[jacoco] ==================================================================");
                    break;
                }
            }
        }
    }

    private static class Handler implements Runnable, ISessionInfoVisitor,
            IExecutionDataVisitor {

        private final Socket socket;

        private final RemoteControlReader reader;

        private final RemoteControlWriter fileWriter;

        Handler(final Socket socket, final RemoteControlWriter fileWriter)
                throws IOException {
            this.socket = socket;
            this.fileWriter = fileWriter;

            // Just send a valid header:
            new RemoteControlWriter(socket.getOutputStream());

            reader = new RemoteControlReader(socket.getInputStream());
            reader.setSessionInfoVisitor(this);
            reader.setExecutionDataVisitor(this);
        }

        public void run() {
            try {
                while (reader.read()) {
                }
                socket.close();
                synchronized (fileWriter) {
                    fileWriter.flush();
                }
            } catch (final IOException e) {
                e.printStackTrace();
            }
        }

        public void visitSessionInfo(final SessionInfo info) {
            System.out.printf("Retrieving execution Data for session: %s%n",
                    info.getId());
            synchronized (fileWriter) {
                fileWriter.visitSessionInfo(info);
            }
        }

        public void visitClassExecution(final ExecutionData data) {
            synchronized (fileWriter) {
                fileWriter.visitClassExecution(data);
            }
        }

    }
}
