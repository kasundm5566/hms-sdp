/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.integration;

import hms.common.rest.util.JsonBodyProvider;
import org.apache.cxf.jaxrs.client.WebClient;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SimpleSmsMtIT {

    private static final String url = "http://core.sdp:7000/sms/send";
    private WebClient webClient;

    @BeforeTest
    public void setup() {
        webClient = createWebClient();
    }

    @Test
    public void sendInvalidRequestEmptyParams() {
        Map<String, Object> smsMtRequest = createInvalidRequestEmptyParams();
        Response response = webClient.post(smsMtRequest);
        logResponse(response);
        Map<String, Object> resp = (Map<String, Object>) response.getEntity();

        Assert.assertEquals(resp.get("statusCode"), "E1312");
        Assert.assertNotNull(resp.get("statusDescription"));
    }

    @Test
    public void sendInvalidRequestAppIdEmpty() {
        Map<String, Object> smsMtRequest = createInvalidRequestAppIdEmpty();
        Response response = webClient.post(smsMtRequest);
        logResponse(response);
        Map<String, Object> resp = (Map<String, Object>) response.getEntity();

        Assert.assertEquals(resp.get("statusCode"), "E1312");
        Assert.assertNotNull(resp.get("statusDescription"));
    }

    @Test
    public void sendSuccess() {
        Map<String, Object> smsMtRequest = createRequestSuccess();
        Response response = webClient.post(smsMtRequest);
        logResponse(response);
        Map<String, Object> resp = (Map<String, Object>) response.getEntity();

        Assert.assertEquals(resp.get("statusCode"), "S1000");
    }

    private static HashMap<String, Object> createInvalidRequestEmptyParams() {
        HashMap<String, Object> request = new HashMap<String, Object>();
        return request;
    }

    private static HashMap<String, Object> createInvalidRequestAppIdEmpty() {
        HashMap<String, Object> request = new HashMap<String, Object>();
        request.put("password", "test");
        request.put("message", "sample sms mt");
        request.put("sourceAddress", "votingtest");
        List<String> addresses = new ArrayList<String>();
        addresses.add("tel:254712323654");
        request.put("address", addresses);
        return request;
    }

    private static HashMap<String, Object> createRequestSuccess() {
        HashMap<String, Object> request = new HashMap<String, Object>();
        request.put("applicationID", "APP_000008");
        request.put("password", "test");
        request.put("message", "sample sms mt");
        request.put("sourceAddress", "votingtest");
        List<String> addresses = new ArrayList<String>();
        addresses.add("tel:254712323654");
        request.put("address", addresses);
        return request;
    }


    private static void logResponse(javax.ws.rs.core.Response response) {
        System.out.println("=================== Response ==========================");
        System.out.println("Status:" + response.getStatus());
        System.out.println("Meta data:" + response.getMetadata());
        Map<String, Object> resp = (Map<String, Object>) response.getEntity();
        System.out.println("Response : " + resp);
        System.out.println("=======================================================");

    }

    private static WebClient createWebClient() {
        List<Object> bodyProviders = new ArrayList<Object>();
        bodyProviders.add(new JsonBodyProvider());
        WebClient webClient = WebClient.create(url, bodyProviders);
        webClient.header("Content-Type", "application/json");
        webClient.accept("application/json");
        return webClient;
    }


}
