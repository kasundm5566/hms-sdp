/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package com.logica.smscsim;


import org.testng.annotations.Test;

import java.util.Properties;

import static org.testng.Assert.assertEquals;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SimulatorPDUProcessorTest {

    @Test
    public void testForSuccess() {
        Properties props = new Properties();
        props.put("34", "5");
        SimulatorPDUProcessor.setResponseProperties(props);
        int statusForAddress = SimulatorPDUProcessor.findStatusForAddress("1234");
        assertEquals(5, statusForAddress);
    }

    @Test
    public void testForLessThanTwoDigit() {
        Properties props = new Properties();
        props.put("34", "5");
        SimulatorPDUProcessor.setResponseProperties(props);
        int statusForAddress = SimulatorPDUProcessor.findStatusForAddress("12");
        assertEquals(12, statusForAddress);
        statusForAddress = SimulatorPDUProcessor.findStatusForAddress("");
        assertEquals(12, statusForAddress);
        statusForAddress = SimulatorPDUProcessor.findStatusForAddress("1");
        assertEquals(12, statusForAddress);
    }
}
