/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sample.util;

import hms.commons.CollectionUtil;

import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class CasClient {

    private SdpClient commitClient;
    private SdpClient reserveClient;
    private SdpClient cancelClient;
    private SdpClient directDebitClient;
    private SdpClient directCreditClient;


    public CasClient(SdpClient commitClient, SdpClient reserveClient,
                     SdpClient cancelClient, SdpClient directDebitClient,
                     SdpClient directCreditClient) {

        this.commitClient = commitClient;
        this.reserveClient = reserveClient;
        this.cancelClient = cancelClient;
        this.directDebitClient = directDebitClient;
        this.directCreditClient = directCreditClient;
    }

    public Map commit(Map message){ return commitClient.send(message); }
    public Map reserve(Map message){ return reserveClient.send(message); }
    public Map cancel(Map message){ return cancelClient.send(message); }
    public Map directDebit(Map message){ return directDebitClient.send(message); }
    public Map directCredit(Map message){ return directCreditClient.send(message); }


}
