/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sample.util;

import hms.common.rest.util.Message;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.HashMap;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@Produces({"application/json"})
@Consumes({"application/json"})
@Path("/sms/")
public class SmsServer {

    private SmsMoListener smsMoListener;

    @POST
    @Path("/mo")
    public Map<String, Object> sendServiceSimple(Map<String, Object> request) {
        Map response = smsMoListener.onSms(request);
        return (response == null ? new HashMap() : response);
    }

    public void setSmsMoListener(SmsMoListener smsMoListener) {
        this.smsMoListener = smsMoListener;
    }
}
