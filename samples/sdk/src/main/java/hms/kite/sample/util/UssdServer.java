/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sample.util;

import hms.common.rest.util.Message;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.HashMap;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@Produces({"application/x-www-form-urlencoded"})
@Consumes({"application/x-www-form-urlencoded"})
@Path("/ussd/")
public class UssdServer {

    private UssdMoListener ussdMoListener;

    @POST
    @Path("/mo")
    public Message sendServiceSimple(Message request) {
        Map response = ussdMoListener.onUssd(request.getParameters());
        return new Message(response == null ? new HashMap() : response);
    }

    public void setUssdMoListener(UssdMoListener ussdMoListener) {
        this.ussdMoListener = ussdMoListener;
    }
}
