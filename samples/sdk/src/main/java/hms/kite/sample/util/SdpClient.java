/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sample.util;

import hms.common.rest.util.Message;
import hms.common.rest.util.MessageBodyProvider;
import org.apache.cxf.jaxrs.client.WebClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SdpClient {

    private final WebClient webClient;

    public SdpClient(String url) {
        ArrayList<Object> providers = new ArrayList<Object>();
        providers.add(new MessageBodyProvider());
        webClient = WebClient.create(url, providers);
        webClient.header("Content-Type", "application/x-www-form-urlencoded");
        webClient.accept("application/x-www-form-urlencoded");
    }

    public final Map send(Map<String, Object> message) {
        beforeSend(message);
        javax.ws.rs.core.Response response = webClient.post(new Message(message));
        return ((Message) response.getEntity()).getParameters();
    }

    protected void beforeSend(Map<String, Object> message) {
        //nothing
    }


}
