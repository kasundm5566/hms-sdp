* TODO status are
   1. Pending  2. Later   3. Done   4. NA(not assigned)

|  id    |  Responsible | Status   |  Item   |
| ------ | ------------ | -------  | ------- |                                                                                                                       |  1     |   Romith     |  NA      | For any exception, happned while processing the request, send E1601 error code. Check with sdp api and send a proper error code|
|  2     |   Malith     |  Done    | Convert existing credit request to direct debit request. Use the url similar to nbl url|
|  3     |   Malith     |  NA      | Define common configuration (ex : Sdp url,)|
|  4     |   Romith     |  NA      | Build file has my-project extension. Remove this and correct start scripts|
|  5     |   Malith     |  Done    | Update the readMe with markdown format . Include the configuration, software versions, for standalone port, sdp request receiving paths|
|  6     |   Malith     |  Pending | Use classpath wildcard(*) instead of java-ext-dir option in starut-app.sh|
|  7     |   Romith     |  NA      | Enhance the class level documentations|








