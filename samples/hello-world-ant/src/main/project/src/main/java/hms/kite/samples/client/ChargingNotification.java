/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.samples.client;

import hms.kite.samples.api.caas.CaasNotificationListener;
import hms.kite.samples.api.caas.messages.ChargingNotificationRequest;

import java.util.logging.Logger;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ChargingNotification implements CaasNotificationListener {

    private final static Logger LOGGER = Logger.getLogger(ChargingNotification.class.getName());

    @Override
    public void onReceiveCaasNotification(ChargingNotificationRequest chargingNotificationReq) {

        LOGGER.info("Charging notification received : "+chargingNotificationReq);
    }
}