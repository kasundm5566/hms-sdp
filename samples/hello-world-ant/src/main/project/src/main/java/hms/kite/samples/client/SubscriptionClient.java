/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.samples.client;

import hms.kite.samples.api.SdpException;
import hms.kite.samples.api.sms.MoSmsListener;
import hms.kite.samples.api.StatusCodes;
import hms.kite.samples.api.sms.messages.MoSmsReq;
import hms.kite.samples.api.subscription.SubscriptionRequestSender;
import hms.kite.samples.api.subscription.messages.*;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SubscriptionClient implements MoSmsListener {

    private final static Logger LOGGER = Logger.getLogger(SubscriptionClient.class.getName());

    @Override
    public void init(ServletConfig servletConfig) {

    }

    @Override
    public void onReceivedSms(MoSmsReq moSmsReq) {
        try {
            LOGGER.info("Sms Received for generate Subscription request " + moSmsReq);

            String message = moSmsReq.getMessage();
            if (!message.equals(null)) {
                if ("status".equals(message)) {
                    sendSubscriptionStatusRequest(moSmsReq);
                } else if ("query-base".equals(message)) {
                    sendQueryBaseRequest(moSmsReq);
                } else {
                    sendSubscriptionRequest(moSmsReq);
                }
            } else {
                LOGGER.info("Invalid action, request not sent to the SDP.\n1-For Subscribe\n0-Unsubscribe\nstatus-Request status");
            }
        } catch (Exception e) {
            LOGGER.log(Level.INFO,"Unexpected error occurred while handling subscription process", e);
        }
    }

    private void sendSubscriptionRequest(MoSmsReq moSmsReq) throws MalformedURLException, SdpException {
        SubscriptionRequestSender subscriptionSender;
        String statusCode;
        String description;
        subscriptionSender = new SubscriptionRequestSender(new URL("http://localhost:7000/subscription/send"));
        SubscriptionRequest subscriptionRequest = createSubscriptionRequest(moSmsReq);
        String action = subscriptionRequest.getAction();
        if (action.equals("0") || action.equals("1")) {
            SubscriptionResponse subscriptionResponse = subscriptionSender.sendSubscriptionRequest(subscriptionRequest);
            statusCode = subscriptionResponse.getStatusCode();
            description = subscriptionResponse.getStatusDetail();
            if (StatusCodes.SuccessK.equals(statusCode)) {
                LOGGER.info("Subscription message sent successfully and receive response status:[" + statusCode + "] " + description);
            } else {
                LOGGER.info("Subscription request failed with status code [" + statusCode + "] " + description);
            }
        } else {
            LOGGER.info("Invalid action, request not sent to the SDP.\n1-For Subscribe\n0-Unsubscribe\nstatus-Request status");
        }
    }

    private void sendSubscriptionStatusRequest(MoSmsReq moSmsReq) throws MalformedURLException, SdpException {
        SubscriptionRequestSender subscriptionSender;
        String statusCode;
        String description;
        subscriptionSender = new SubscriptionRequestSender(new URL("http://localhost:7000/subscription/getStatus"));
        SubscriptionStatusRequest subStatusRequest = createSubscriptionStatusRequest(moSmsReq);
        SubscriptionStatusResponse subscriptionStatusResponse = subscriptionSender.sendSubscriptionStatusRequest(subStatusRequest);

        statusCode = subscriptionStatusResponse.getStatusCode();
        description = subscriptionStatusResponse.getStatusDetail();
        String subscriptionStatus = subscriptionStatusResponse.getSubscriptionStatus();

        if (StatusCodes.SuccessK.equals(statusCode)) {
            LOGGER.info("Subscription status request sent successfully. Subscription status for "
                    + subStatusRequest.getSourceAddress() + "-" + subscriptionStatus);
        } else {
            LOGGER.info("Subscription status request failed with status code [" + statusCode + "] " + description);
        }
    }

    private void sendQueryBaseRequest(MoSmsReq moSmsReq) throws MalformedURLException, SdpException {
        SubscriptionRequestSender subscriptionSender;
        String statusCode;
        subscriptionSender = new SubscriptionRequestSender(new URL("http://localhost:7000/subscription/query-base"));
        SubQueryBaseRequest subscriptionRequest = new SubQueryBaseRequest();

        subscriptionRequest.setApplicationId(moSmsReq.getApplicationId());
        subscriptionRequest.setPassword("password");
        SubQueryBaseResponse subQueryBaseResponse = subscriptionSender.sendSubscriptionQueryBaseRequest(subscriptionRequest);

        statusCode = subQueryBaseResponse.getStatusCode();

        if (StatusCodes.SuccessK.equals(statusCode)) {
            LOGGER.info("Subscription query request sent successfully and receive response, " + subQueryBaseResponse );
        } else {
            LOGGER.info("Subscription query base request failed with status code [" + subQueryBaseResponse.getStatusCode()+"]: "+subQueryBaseResponse.getStatusDetail());
        }
    }

    private SubscriptionStatusRequest createSubscriptionStatusRequest(MoSmsReq moSmsReq) {
        SubscriptionStatusRequest subStatusRequest = new SubscriptionStatusRequest();
        subStatusRequest.setApplicationId(moSmsReq.getApplicationId());
        subStatusRequest.setSourceAddress(moSmsReq.getSourceAddress());
        subStatusRequest.setPassword("password");
        return subStatusRequest;
    }

    private SubscriptionRequest createSubscriptionRequest(MoSmsReq moSmsReq) {
        SubscriptionRequest subscriptionRequest = new SubscriptionRequest();
        subscriptionRequest.setSubscriberId(moSmsReq.getSourceAddress());
        subscriptionRequest.setApplicationId(moSmsReq.getApplicationId());
        subscriptionRequest.setAction(moSmsReq.getMessage());
        subscriptionRequest.setPassword("test");
        subscriptionRequest.setVersion("0.1");
        return subscriptionRequest;
    }
}