/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.samples.client;

import hms.kite.samples.api.StatusCodes;
import hms.kite.samples.api.sms.MoSmsListener;
import hms.kite.samples.api.sms.messages.MoSmsReq;
import hms.kite.samples.api.wp.WapPushRequestSender;
import hms.kite.samples.api.wp.messages.MtWpReq;
import hms.kite.samples.api.wp.messages.MtWpResp;

import javax.servlet.ServletConfig;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SimpleClientWP implements MoSmsListener {

    private final static Logger LOGGER = Logger.getLogger(SimpleClientWP.class.getName());

    @Override
    public void init(ServletConfig servletConfig) {

    }

    public void onReceivedSms(MoSmsReq moSmsReq) {
        try {
            LOGGER.info("Wap-push request Received " + moSmsReq);

            WapPushRequestSender mtWpSender = new WapPushRequestSender(new URL("http://localhost:7000/wappush/send"));
            MtWpReq  mtWpReq = createSubmitWapPush(moSmsReq);

            mtWpReq.setApplicationId(moSmsReq.getApplicationId());
            mtWpReq.setPassword("password");

            String deliveryReq = moSmsReq.getDeliveryStatusRequest();
            if (deliveryReq != null) {
                if (deliveryReq.equals("1")) {
                    mtWpReq.setDeliveryStatusRequest("1");
                }
            } else {
                mtWpReq.setDeliveryStatusRequest("0");
            }
            MtWpResp mtWpResp = mtWpSender.sendWpRequest(mtWpReq);
            String statusCode = mtWpResp.getStatusCode();
            String statusDetails = mtWpResp.getStatusDetail();
            if (StatusCodes.SuccessK.equals(statusCode)) {
                LOGGER.info("MT Wap-push message and URL sent successfully");
            } else {
                LOGGER.info("MT Wap-push message sending failed with status code [" + statusCode + "]"+statusDetails);
            }


        } catch (Exception e) {
            LOGGER.log(Level.INFO,"Unexpected error occurred ", e);
        }
    }

    private MtWpReq createSubmitWapPush(MoSmsReq moSmsReq) {
        MtWpReq mtWpReq = new MtWpReq();
        List<String> addressList = new ArrayList<String>();

        mtWpReq.setWapUrl("https://dev.sdp.hsenidmobile.com/");
        mtWpReq.setTitle("click the link to download app");
        addressList.add(moSmsReq.getSourceAddress());
        mtWpReq.setDestinationAddresses(addressList);

        return mtWpReq;
    }
}