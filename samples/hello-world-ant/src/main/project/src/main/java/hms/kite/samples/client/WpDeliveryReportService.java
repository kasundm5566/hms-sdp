/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.samples.client;

import hms.kite.samples.api.wp.MoWpDeliveryReportListener;
import hms.kite.samples.api.wp.messages.MoWpDeliveryReportReq;

import java.util.logging.Logger;

public class WpDeliveryReportService implements MoWpDeliveryReportListener {

    private final static Logger LOGGER = Logger.getLogger(WpDeliveryReportService.class.getName());

    @Override
    public void onReceivedDeliveryReport(MoWpDeliveryReportReq moWpDeliveryReportReq) {

        LOGGER.info("==> Wap-push delivery report received from SDP : "+ moWpDeliveryReportReq);
    }
}