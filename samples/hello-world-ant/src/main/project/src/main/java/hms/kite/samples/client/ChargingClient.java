package hms.kite.samples.client;

import hms.kite.samples.api.SdpException;
import hms.kite.samples.api.caas.ChargingException;
import hms.kite.samples.api.caas.ChargingRequestSender;
import hms.kite.samples.api.caas.messages.*;
import hms.kite.samples.api.sms.MoSmsListener;
import hms.kite.samples.api.sms.messages.MoSmsReq;

import javax.servlet.ServletConfig;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ChargingClient implements MoSmsListener {

    private final static Logger LOGGER = Logger.getLogger(ChargingClient.class.getName());

    private String receivedPaymentInstrument = null;

    @Override
    public void init(ServletConfig servletConfig) {

    }

    @Override
    public void onReceivedSms(MoSmsReq moSmsReq) {

        try {
            LOGGER.info("Sms Received" + moSmsReq);
            String message = moSmsReq.getMessage();
            if (!(message == null)) {
                if (message.equals("cr")) {
                    sendCreditReserveRequest(moSmsReq);
                } else if (message.equals("dc")) {
                    sendDirectCreditRequest(moSmsReq);
                } else if (message.equals("pil")) {
                    sendPaymentInstrumentListRequest(moSmsReq);
                } else if (message.equals("chr")) {
                    sendDirectDebitRequest(moSmsReq);
                } else {
                    LOGGER.info("Request not success, send message with followings\n" +
                            "pi-To get payment instrument list request\n" +
                            "dd-Credit direct debit request\n" +
                            "chr-Charging request");
                }
            } else {
                LOGGER.info("Request not success, send message with followings\n" +
                        "pi-To get payment instrument list request\n" +
                        "dd-Credit direct debit request\n" +
                        "chr-Charging request");
            }
        } catch (Exception e) {
            LOGGER.log(Level.INFO,"Unexpected error occurred", e);
        }
    }

    private void sendCreditReserveRequest(MoSmsReq moSmsReq) throws MalformedURLException, ChargingException, SdpException {
        LOGGER.info("Sending Credit Reserve Request");
        ChargingRequestSender chargingRequestSender = new ChargingRequestSender(new URL("http://localhost:7000/caas/credit/reserve"));

        CreditReserveRequest creditReserveRequest = new CreditReserveRequest();

        creditReserveRequest.setApplicationId(moSmsReq.getApplicationId());
        creditReserveRequest.setSubscriberId(moSmsReq.getSourceAddress());
        creditReserveRequest.setPassword("password");
        creditReserveRequest.setPaymentInstrumentName("ABC Bank");
        creditReserveRequest.setAccountId("11111111");
        creditReserveRequest.setAmount("50");
        creditReserveRequest.setCurrency("USD");
        creditReserveRequest.setInvoiceNo("SOME_INV_NO_00001");
        creditReserveRequest.setOrderNo("SOME_ORDER_NO_00001");

        DirectDebitResponse creditReserveResponse = chargingRequestSender.sendCreditReserveRequest(creditReserveRequest);
        LOGGER.info("Credit Reserve Request Sent with request response " + creditReserveResponse);

    }

    private void sendDirectCreditRequest(MoSmsReq moSmsReq) throws MalformedURLException, ChargingException, SdpException {
        LOGGER.info("Sending direct debit request");
        ChargingRequestSender chargingRequestSender = new ChargingRequestSender(new URL("http://localhost:7000/caas/direct/credit"));

        DirectCreditRequest directCreditReq = new DirectCreditRequest();

        directCreditReq.setApplicationId(moSmsReq.getApplicationId());
        directCreditReq.setSubscriberId(moSmsReq.getSourceAddress());
        directCreditReq.setPassword("password");
        directCreditReq.setPaymentInstrumentName("ABC Bank");
        directCreditReq.setAccountId("33333333");
        directCreditReq.setAmount("5");
        directCreditReq.setCurrency("USD");
        directCreditReq.setInvoiceNo("SOME_INV_NO_00001");
        directCreditReq.setOrderNo("SOME_ORDER_NO_00001");

        DirectDebitResponse creditReserveResponse = chargingRequestSender.sendDirectCreditRequest(directCreditReq);
        LOGGER.info("Direct credit request sent with request response " + creditReserveResponse);
    }

    private void sendPaymentInstrumentListRequest(MoSmsReq moSmsReq) throws MalformedURLException, ChargingException, SdpException {
        LOGGER.info("Sending payment instrument request");
        ChargingRequestSender paymentInReqSender = new ChargingRequestSender(new URL("http://localhost:7000/caas/list/pi"));
        PaymentInstrumentListRequest paymentInsListReq = new PaymentInstrumentListRequest();

        paymentInsListReq.setApplicationId("APP_000001");
        paymentInsListReq.setPassword("Password");
        paymentInsListReq.setSubscriberId(moSmsReq.getSourceAddress());
        paymentInsListReq.setType("all");

        PaymentInstrumentListResponse paymentInsListRes = paymentInReqSender.sendPaymentInstrumentListReq(paymentInsListReq);
        List<PaymentInstrument> pil = paymentInsListRes.getPaymentInstrumentList();
        if (!pil.isEmpty()) {
            receivedPaymentInstrument = pil.get(0).getName();
        } else {
            receivedPaymentInstrument = "Equity";
        }

        LOGGER.info("Payment instrument list request sent with request response " + paymentInsListRes);

        if (paymentInsListRes != null) {
            sendDirectDebitRequest(moSmsReq);
        }
    }

    private void sendDirectDebitRequest(MoSmsReq moSmsReq) throws MalformedURLException, SdpException, ChargingException {

        LOGGER.info("Sending charging request");
        ChargingRequestSender chargingRequestSender = new ChargingRequestSender(new URL("http://localhost:7000/caas/direct/debit"));
        DirectDebitRequest chargingReq = new DirectDebitRequest();

        chargingReq.setApplicationId(moSmsReq.getApplicationId());
        chargingReq.setSubscriberId(moSmsReq.getSourceAddress());
        chargingReq.setPassword("password");

        Random randomGenerator = new Random();

        chargingReq.setExternalTrxId(String.valueOf(randomGenerator.nextInt(100000)));
        chargingReq.setPaymentInstrumentName(receivedPaymentInstrument);
        chargingReq.setAccountId(String.valueOf(randomGenerator.nextInt(100000)));
        chargingReq.setAmount("50");
        chargingReq.setCurrency("KES");
        chargingReq.setInvoiceNo("SOME_INV_NO_00001");
        chargingReq.setOrderNo("SOME_ORDER_NO_00001");
//        chargingReq.setAllowPartialPayments("true");
//        chargingReq.setAllowOverPayments("true");

        ChargingRequestResponse chargingResponse = chargingRequestSender.sendChargingRequest(chargingReq);
        LOGGER.info("Charging request sent with request response " + chargingResponse);
    }
}
