
To run the application you have two options. \
    1. Running as a standalone application.
    2. Creating a web archive and deploying in a web container like tomcat.

##  Required software
- JDK 1.6 or upper

## Relieving URL from SDP to sample application

USSD
: http://{host}:{port}/mo-receiver

## Start-up port of sample app
- 5557

###  Create Standalone application

#### Note : Build the following api to download the dependencies.
samples/sdp-app-api

- for linux users  run following commands.
- cd bin
- sh create_standalone.sh
- cd ../target/stand-alone/bin/
- sh start-app.sh

#### NOTE : As a time consuming, developers can start the sample application by running one script.
    --> Go to samples/ussd-company-site/
    --> Run start.sh script (e.g. sh start.sh)


- for windows users  run following commands.
- cd bin
- create_standalone.bat
- cd ..\target\stand-alone\bin
- start-app.bat


#### Enabling Https Support:
   --> Follow the instructions given in https-configuration.md for create certificate key-store
   --> From sample-application side if you wish to support https requests, you need to set the property
       'enable.https' as true in ussdmenu.properties property file in ussd-company-site sample application.
   --> If you wish to send https request to sdp, then you need to set the https url configured in the simple-client.properties
       property file
       eg.
          #application sms-mt sending http url
          #url = http://localhost:7000/sms/send
          #application sms-mt sending https url
          url = https://localhost:7443/sms/send


###  Create a web archive to deploy in a web container
for linux users run following commands. ussd-company-site.war  will be created in target folder.
- cd bin
- sh create_war.sh

for windows users  run following commands.
- cd bin
- create_war.bat

## Service Codes.

Service starts with #678*

Follow screen numbers to navigate menus.

999 for back.
000 for exit.