/*
 *   (C) Copyright 1996-2012 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package ussd;

import org.eclipse.jetty.http.ssl.SslContextFactory;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.nio.SelectChannelConnector;
import org.eclipse.jetty.server.ssl.SslSelectChannelConnector;
import org.eclipse.jetty.webapp.WebAppContext;
import ussd.utils.PropertyLoader;

import java.util.logging.Logger;

public class MainApp {

    private final static Logger LOGGER = Logger.getLogger(MainApp.class.getName());

    public static void main(String[] args) throws Exception {

        LOGGER.info("Application starting ...");
        Server serverUssd = new Server();
        serverUssd.setConnectors(new Connector[]{getConnector()});

        WebAppContext webAppContext = new WebAppContext();
        webAppContext.setContextPath("/");
        webAppContext.setWar("../lib/sdp-ussd-company.war");

        serverUssd.setHandler(webAppContext);
        serverUssd.start();
        serverUssd.join();
    }

    private static Connector getConnector() {
        boolean useHttps = Boolean.parseBoolean(getPropertyValue("enable.https"));
        int port = Integer.parseInt(getPropertyValue("port"));
        if (useHttps) {
            LOGGER.info("Creating HTTPS Connector on port[" + port + "]");
            SslSelectChannelConnector sslConnector = new SslSelectChannelConnector();
            sslConnector.setPort(port);
            SslContextFactory cf = sslConnector.getSslContextFactory();
            cf.setKeyStore(getPropertyValue("keystore.path"));
            cf.setKeyStorePassword(getPropertyValue("keystore.password"));
            cf.setKeyManagerPassword(getPropertyValue("keymanager.password"));
            return sslConnector;
        } else {
            LOGGER.info("Creating HTTP Connector on port[" + port + "]");
            SelectChannelConnector connector = new SelectChannelConnector();
            connector.setPort(port);
            return connector;
        }
    }

    private static String getPropertyValue(String key) {
        return PropertyLoader.getInstance().getText(key);
    }
}
