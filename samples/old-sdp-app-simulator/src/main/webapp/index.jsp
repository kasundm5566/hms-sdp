<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Application Simulator</title>
</head>
<body>
<center>
    <h3>
        <u>Etisalat Appzone</u>
    </h3>
    <br> <br>
    <form name="input" action="SendBroadCastMsgServlet" method="get">
        <select name="applicationDetails">
            <option value="PD_ET_G0296">ET_Gym</option>
            <option value="PD_ET_w0845">ET_wishsms</option>
            <option value="PD_ET_m0651">ET_mpoll</option>
            <option value="PD_ET_s0885">ET_speed</option>
            <option value="PD_ET_B0980">ET_BusRoute</option>
            <option value="PD_ET_t0974">ET_timereader</option>
            <option value="PD_ET_r0987">ET_rahukala</option>
            <option value="PD_ET_s0184">ET_soccer</option>
            <option value="PD_ET_n0362">ET_news</option>
        </select>
        <br> <br>
        <textarea name="broadcastMessage" rows="4" cols="50">
        </textarea>
        <br> <br> <input type="submit" value="Submit">
    </form>
</center>
</body>
</html>