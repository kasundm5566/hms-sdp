/*
 *
 *  *
 *  *  *   (C) Copyright 2013-2014 hSenid Software International (Pvt) Limited.
 *  *  *   All Rights Reserved.
 *  *  *
 *  *  *   These materials are unpublished, proprietary, confidential source code of
 *  *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *  *   of hSenid Software International (Pvt) Limited.
 *  *  *
 *  *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *  *   property rights in these materials.
 *  *  *
 *  *
 *
 */
package hsenidmobile.old.sdp.app.simulator.util;

import hsenidmobile.old.sdp.app.simulator.Application;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.bytecode.opencsv.CSVParser;
import au.com.bytecode.opencsv.CSVReader;

/**
 * <p>
 * App simulator utils.
 * </p>
 * 
 * @author manuja
 */
public class AppSimulatorUtils {
	
	private static final Logger logger = LoggerFactory
			.getLogger(AppSimulatorUtils.class);

	public static final String CONFIGURATION_BUNDLE_NAME = "configuration";
    public static final String APPLICATION_PATH_NAME = "application.path";
	private static ResourceBundle resourceBundle = ResourceBundle
			.getBundle(CONFIGURATION_BUNDLE_NAME);

	public static Map<String, Application> applicationDetailsMap = new HashMap<String, Application>();
	
	/**
	 * <p>
	 * Resolve a message by a key and argument replacements.
	 * </p>
	 * 
	 * @see MessageFormat#format(String, Object...)
	 * @param key
	 *            the message to look up
	 * @param arguments
	 *            optional message arguments
	 * @return the resolved message
	 **/
	public static String get(final String key, final Object... arguments) {
		try {
			if (arguments != null) {
				return MessageFormat.format(resourceBundle.getString(key),
						arguments);
			}
			return resourceBundle.getString(key);
		} catch (MissingResourceException e) {
			logger.error("Message key not found: [{}]", key);
			return '!' + key + '!';
		}
	}

	/**
	 * <p>
	 * Get application details for given keyword.
	 * </p>
	 * 
	 * @param keyword
	 *            application keyword
	 * 
	 * @return application detail instance
	 */
	public static Application getApplication(final String keyword) {
	if (applicationDetailsMap.isEmpty()) {
			loadApplicationDetails();
		}
		return applicationDetailsMap.get(keyword);
	}

	
	/**
	 * <p>
	 * Get application details for given keyword.
	 * </p>
	 * 
	 * @param keyword
	 *            application keyword
	 * 
	 * @return application detail instance
	 */
	
	@SuppressWarnings("rawtypes")
	public static Map<String, Application> getApplicationMap() {
		
		if (applicationDetailsMap.isEmpty()) {
			loadApplicationDetails();
		}
		return applicationDetailsMap;
	}
	
	private static void loadApplicationDetails() {
		String[][] applicationDetails = loadFromFile(APPLICATION_PATH_NAME);
		logger.debug("Initiating application details map");
		for (String[] applicationDetail : applicationDetails) {
			String applicationId = applicationDetail[0];
			String password = applicationDetail[1];
			String keyword = applicationDetail[2];
			String applicationName = applicationDetail[3];
			logger.info(
					"Adding application detail for application [{}] keyword [{}], application Id [{}], password [{}]",
					applicationName, keyword, applicationId, password);
			applicationDetailsMap.put(keyword, new Application(applicationId,
					password, applicationName));
		}
	}

	@SuppressWarnings("resource")
	private static String[][] loadFromFile(final String fileName) {
		InputStream inputStream = AppSimulatorUtils.class
				.getResourceAsStream(fileName);
		InputStreamReader in = new InputStreamReader(inputStream);
		CSVReader reader = new CSVReader(in, CSVParser.DEFAULT_SEPARATOR,
				CSVParser.DEFAULT_QUOTE_CHARACTER,
				CSVParser.DEFAULT_ESCAPE_CHARACTER, 1);
		String[][] linesAndColumns = null;
		try {
			List<String[]> myEntries = reader.readAll();
			ListIterator<String[]> it = myEntries.listIterator();
			linesAndColumns = new String[myEntries.size()][myEntries.get(0).length];
			int j = 0;
			while (it.hasNext()) {
				String[] val = it.next();
				for (int i = 0; i < val.length; i++) {
					linesAndColumns[j][i] = val[i];
				}
				j++;
			}
		} catch (IOException e) {
			logger.error("Error Loading File [{}]", fileName, e);
		}
		return linesAndColumns;
	}
}
