/*
 *
 *  *
 *  *  *   (C) Copyright 2013-2014 hSenid Software International (Pvt) Limited.
 *  *  *   All Rights Reserved.
 *  *  *
 *  *  *   These materials are unpublished, proprietary, confidential source code of
 *  *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *  *   of hSenid Software International (Pvt) Limited.
 *  *  *
 *  *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *  *   property rights in these materials.
 *  *  *
 *  *
 *
 */
package hsenidmobile.old.sdp.app.simulator;

/**
 * <p>
 * Application details.
 * </p>
 * 
 * @author manuja
 */
public class Application {

	private String appId;
	private String password;
	private String appName;
	
	/**
	 * <p>
	 * Default constructor.
	 * </p>
	 *  
	 * @param inAppId application id
	 * @param inPassword application password
	 * @param inAppName application name
	 */
	public Application(final String inAppId, final String inPassword, final String inAppName)
	{
		this.appId = inAppId;
		this.password = inPassword;
		this.appName = inAppName;
	}

	/**
	 * @return the appId
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * @param appId
	 *            the appId to set
	 */
	public void setAppId(final String appId) {
		this.appId = appId;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(final String password) {
		this.password = password;
	}

	/**
	 * @return the appName
	 */
	public String getAppName() {
		return appName;
	}

	/**
	 * @param appName the appName to set
	 */
	public void setAppName(String appName) {
		this.appName = appName;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Application [appId=");
		builder.append(appId);
		builder.append(", password=");
		builder.append(password);
		builder.append(", appName=");
		builder.append(appName);
		builder.append("]");
		return builder.toString();
	}
}
