/*
 *
 *  *
 *  *  *   (C) Copyright 2013-2014 hSenid Software International (Pvt) Limited.
 *  *  *   All Rights Reserved.
 *  *  *
 *  *  *   These materials are unpublished, proprietary, confidential source code of
 *  *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *  *   of hSenid Software International (Pvt) Limited.
 *  *  *
 *  *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *  *   property rights in these materials.
 *  *  *
 *  *
 *
 */
package hsenidmobile.old.sdp.app.simulator;

import static hsenidmobile.old.sdp.app.simulator.util.AppSimulatorUtils.get;

import java.net.MalformedURLException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hsenidmobile.old.sdp.app.simulator.util.AppSimulatorUtils;
import hsenidmobile.sdp.rest.servletbase.MchoiceAventuraMessagingException;
import hsenidmobile.sdp.rest.servletbase.MchoiceAventuraSmsMessage;
import hsenidmobile.sdp.rest.servletbase.MchoiceAventuraSmsMoServlet;
import hsenidmobile.sdp.rest.servletbase.MchoiceAventuraSmsSender;

/**
 * <p>
 * Message receiver implementation.
 * </p>
 * 
 * @author manuja
 */
public class MessageReceiver extends MchoiceAventuraSmsMoServlet {

	private static final long serialVersionUID = -3334562189253442146L;
	private static final Logger logger = LoggerFactory
			.getLogger(MessageReceiver.class);

	@Override
	protected void onMessage(MchoiceAventuraSmsMessage msg) {
		String message = msg.getMessage();
		logger.info("MO Message received: [{}] from [{}]", message,
				msg.getAddress());

		String[] splittedMessage = message.split("\\s+");
		if (splittedMessage.length > 0) {
			try {
				Application application = AppSimulatorUtils
						.getApplication(splittedMessage[0]);

				if (null != application) {
					logger.info("Application details to send MT : [{}]", application);
					MchoiceAventuraSmsSender smsSender = new MchoiceAventuraSmsSender(
							new URL(get("new.sdp.sms.url")),
							application.getAppId(), application.getPassword());
					smsSender.sendMessage(get("response.message"),
							msg.getAddress());
				}
			} catch (MchoiceAventuraMessagingException ex) {
				logger.error("Messaging error while sending MT message [{}]",
						ex);
			} catch (MalformedURLException ex) {
				logger.error("SDP URL error while sending MT message [{}]", ex);
			}
		}
	}
}
