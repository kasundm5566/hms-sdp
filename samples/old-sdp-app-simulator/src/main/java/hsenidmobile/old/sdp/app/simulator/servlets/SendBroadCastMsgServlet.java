package hsenidmobile.old.sdp.app.simulator.servlets;

import hsenidmobile.sdp.rest.servletbase.MchoiceAventuraMessagingException;
import hsenidmobile.sdp.rest.servletbase.MchoiceAventuraSmsSender;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URL;

import static hsenidmobile.old.sdp.app.simulator.util.AppSimulatorUtils.get;

public class SendBroadCastMsgServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        String selectedItem = "default value";

        MchoiceAventuraSmsSender smsSender = new MchoiceAventuraSmsSender(
                new URL(get("new.sdp.sms.url")), get("application.id"),
                get("application.password"));

        try {
            String name = req.getParameter("applicationDetails");
            String broadcastMessageText = req.getParameter("broadcastMessage");
            selectedItem = req.getParameter("applicationDetails");

            smsSender.broadcastMessage(broadcastMessageText + "  " + selectedItem);
            resp.sendRedirect("index.jsp");

        } catch (MchoiceAventuraMessagingException e) {
            e.printStackTrace();
        }
    }
}
