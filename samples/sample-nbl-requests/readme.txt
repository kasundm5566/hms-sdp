Send sms mt
-----------
 mvn clean package exec:java -Dexec.mainClass="hms.kite.samples.client.SampleClient" -Dexec.args="sms-mt"

    or

 wget --post-data="{\"applicationId\":\"APP_000022\",\"password\":\"79af765ee935b36ec2ddd35d2f744967\",\"message\":\"This is Service Delivery Platform\",\"address\":\"tel:94777777777\"}" --header="Content-Type:application/json" http://sdp.dlg:7000/sms/send

Form zip file
-------------

 run mvn clean install 
 go to the target directory
 extract sample-requests.zip which is built in target
 go to sample-requests/bin
 run run-client providing the appropriate properties file in ../conf directory, as the argument (eg:- ./run-client sms-mt)
 observe the output


Testing a Flow
--------------
In order to test a flow (e.g. Charging as a Service) we may need to pass parameters from previous call
e.g.
  ./run_client.sh caas-credit-reserve

  note the response e.g. reservationId=xxxxxx and use the parameter value in the next command in command line.
  (The commandline parameters will override those from the property files.)

  ./run_client.sh caas-credit-commit reservationId=xxxxxx


