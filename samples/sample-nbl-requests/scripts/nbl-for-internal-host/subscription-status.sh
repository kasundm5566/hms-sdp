curl -v -H "Content-Type: application/json" -X POST -d \
'{"applicationId":"APP_000145",
"subscriberId":"tel:6798031380"}' http://core.sdp:7000/subscription/subscriptionstatus


# Sample Response 1
# {"statusCode":"E1325","statusDetail":"Format of the address is invalid","version":"1.0"}

#Sample Response 2
# {"statusCode":"S1000","statusDetail":"Request was successfully processed","subscriptionStatus":"UNREGISTERED","version":"1.0"}
