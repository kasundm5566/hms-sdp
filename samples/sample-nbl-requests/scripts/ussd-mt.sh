curl -v -H "Content-Type: application/json" -X POST -d \
'{"applicationId":"APP_000189",
"password":"b528e51b5d46c69c781feea972c907d0",
"message":"Welcome to ussd",
"sessionId":"94723743056",
"ussdOperation":"mt-cont",
"destinationAddress":"tel:254709725894"}' http://core.sdp:7000/ussd/send


#Optional Parameters
# "version":1.0,
# "encoding":0,
# "chargingAmount"=8.252342,

echo
echo
echo "#### Other Optional Parameters ####"
echo -------------------------------------
echo
echo version = 1.0
echo
echo sourceAddress = 383 
echo
echo "# 0 - Text"
echo "# 240 - flash"
echo "# 245 - binary sms"
echo encoding = 0
echo
echo "### EOF"