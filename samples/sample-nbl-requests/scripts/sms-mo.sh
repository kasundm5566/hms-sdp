curl -v -H "Content-Type: application/json" -X POST -d \
'{"applicationId":"APP_000057",
"password":"6d161e5ea6df56df9d18d8d20fea7c71",
"message":"bmi 75 1.9",
"destinationAddresses":["tel:94774545445"],
"version":1.0,
"deliveryStatusRequest":1}' http://192.168.0.125:7000/sms/receive

#Optional Parameters
# "version":1.0,
# "sourceAddress":383,
# "deliveryStatusRequest":0,
# "encoding":0,
# "binary-header":7663697479,
# "chargingAmount"=8.252342,

echo
echo
echo "#### Other Optional Parameters ####"
echo -------------------------------------
echo
echo version = 1.0
echo
echo sourceAddress = 383
echo
echo "# 0 - delivery report not required"
echo "# 1 - delivery report required"
echo deliveryStatusRequest = 0
echo
echo "# 0 - Text"
echo "# 240 - flash"
echo "# 245 - binary sms"
echo encoding = 0
echo
echo binary-header =
echo
echo "# For variable charging"
echo chargingAmount=8.252342
echo
echo "### EOF"
