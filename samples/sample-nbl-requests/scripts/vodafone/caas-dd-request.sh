curl -v -H "Content-Type: application/json" -X POST -d \
'{"externalTrxId":"25609",
 "amount":"5",
 "invoiceNo":"SOME_INV_NO_00001",
 "applicationId":"APP_1000002",
 "password":"b528e51b5d46c69c781feea972c907d0",
 "paymentInstrumentName":"Mpaisa",
 "subscriberId":"tel:6798031380",
 "additionalParams": {
    "pin": "4444", "imei": "0U09800980488049809", "payment-ref-id": "REF-9098"}
}'  http://127.0.0.1:7000/caas/direct/debit

# "orderNo":"SOME_ORDER_NO_00001"
 # Sample Response message

  #{"statusCode":"P1003",
  #"businessNumber":"mpesaBusinessNo",
  #"timeStamp":"2012-05-24T09:56:05.463+05:30",
  #"shortDescription":"Please access Pay Bill in M-PESA menu; enter following: Business no:mpesaBusinessNo, Account no:121390, Amount:Ksh5,000. ",
  #"externalTrxId":"25609",
  #"statusDetail":"Request successful. Payment notification pending from payment instrument",
  #"longDescription":"Please access Pay Bill in M-PESA menu and enter the following when prompted: Business no:mpesaBusinessNo, Account no:121390, Amount:KES5,000. Please pay within 2 minutes.",
  #"referenceId":"121390",
  #"amountDue":"5000.00",
  #"internalTrxId":"912060216280004"}


  # Sample Asynchronous notification

  #{"statusCode":"S1000",
  #"timeStamp":"08-Feb-2012 12:22",
  #"externalTrxId":"25609",
  #"statusDetail":"Request was Successfully processed, Due amount fully paid.",
  #"totalAmount":"5000.00",
  #"paidAmount":"5000.00",
  #"referenceId":"121390",
  #"balanceDue":"0.00",
  #"currency":"KES",
  #"internalTrxId":"912060216280004"}
  #


