curl -v -H "Content-Type: application/json" -X POST -d \
'{"senderAddress":"94774545445",
"applicationID":"APP_000000",
"password":"password"}' http://core.sdp:7000/subscription/applist

# TO SEND FROM INTERNAL HOST (e.g :- from app store)
#curl -v -H "Content-Type: application/json" -X POST -d \
#'{"senderAddress":"94774545445"}' http://core.sdp:7000/subscription/applist

# Sample response
# {"statusCode":"S1000","statusDescription":"Request was successfully processed","subscribed-apps":["APP_000126"],"version":"1.0"}

