curl -v -H "Content-Type: application/json" -X POST -d \
'{"applicationId":"APP_000002",
"password":"5f27720a6784532de8f98c9391130cda",
"message":"Wapush Test Message",
"wapUrl":"http://testingwappush.com",
"deliveryStatusRequest":"1",
"destinationAddresses":["tel:254709725894"]}' http://127.0.0.1:7000/wappush/send

#Optional Parameters
# "version":"1.0",
# "wapPushType":"sl",


echo
echo
echo "#### Other Optional Parameters ####"
echo -----------------------------------
echo
echo version = 1.0
echo
echo sourceAddress = 383
echo
echo "# 0 - delivery report not required"
echo "# 1 - delivery report required"
echo deliveryStatusRequest = 0
echo
echo encoding = 0
echo
echo binary-header =
echo
echo "# For variable charging"
echo chargingAmount=8.252342
echo # Example Delivery report"
echo "#{"statusCode":"S1000",
echo "#               "timeStamp":"1205301211",
echo "#               "requestId":"101205301211420096",
echo "#              "statusDetail":"Request was successfully processed",
echo "#               "destinationAddress":"tel:null"}"
echo
echo "### EOF"

#Response Message

#{"statusCode":"S1000",
#"wapUrl":"http://testingwappush.com","
#requestId":"101205291829310192",
#"statusDetail":"Request was successfully processed",
#"wapPushType":"si",
#"destinationResponses":[{"destinationAddresses":"tel:254709725894",
#        "statusCode":"S1000","requestId":"101205291829310192",
#        "statusDetail":"Request was successfully processed"}],
#        "version":"1.0"}