curl -v -H "Content-Type: application/json" -X POST -d \
'{"applicationId":"APP_002693",
"password":"e8a2b1aef246087f9930dcbf931d0d29",
"message":"hello world",
"destinationAddresses":["tel:all"],
"deliveryStatusRequest":0}' http://core.sdp:7000/sms/send

# Sample delivery deport.

#    {timeStamp=1205311655,
#     requestId=101205311655490068,
#     destinationAddress=tel:254716351234,
#     deliveryStatus=DELIVERED}


#Optional Parameters
# "version":1.0,
# "sourceAddress":383,
# "deliveryStatusRequest":0,
# "encoding":0,
# "binary-header":7663697479,
# "chargingAmount"=8.252342,

echo
echo
echo "#### Other Optional Parameters ####"
echo -------------------------------------
echo
echo version = 1.0
echo
echo sourceAddress = 383
echo
echo "# 0 - delivery report not required"
echo "# 1 - delivery report required"
echo deliveryStatusRequest = 0
echo
echo "# 0 - Text"
echo "# 240 - flash"
echo "# 245 - binary sms"
echo encoding = 0
echo
echo binary-header =
echo
echo "# For variable charging"
echo chargingAmount=8.252342
echo
echo "### EOF"

