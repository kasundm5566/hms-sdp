/*
 *
 *  *
 *  *  *   (C) Copyright 2013-2014 hSenid Software International (Pvt) Limited.
 *  *  *   All Rights Reserved.
 *  *  *
 *  *  *   These materials are unpublished, proprietary, confidential source code of
 *  *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *  *   of hSenid Software International (Pvt) Limited.
 *  *  *
 *  *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *  *   property rights in these materials.
 *  *  *
 *  *
 *
 */
package com.hms.appzone.migration;

import au.com.bytecode.opencsv.CSVParser;
import au.com.bytecode.opencsv.CSVReader;
import com.hms.appzone.migration.impl.*;
import com.hms.appzone.migration.util.PropertyLoader;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

/**
 * <p>
 * Migration main class.
 * </p>
 *
 * @author manuja
 */
public class MigrationMain {

    private static final String APPLICATION_DETAILS_FILE_PATH = "application.names.path";

    public static void main(String[] args) throws Exception {

        HashMap<String, String> applicationDetailsMap = new HashMap<String, String>();
        loadApplicationDetails(applicationDetailsMap);
        System.out.println(applicationDetailsMap.size() + " applications loaded.");

        MigrationMain migrationMain = new MigrationMain();

        // RoutingKeysMigration
        RoutingKeysMigration routingKeysMigration = new RoutingKeysMigration();
        migrationMain.migrate(routingKeysMigration, applicationDetailsMap);

        // CorporateUserInfo
        CorporateUserInfo corporateUserInfo = new CorporateUserInfo();
        migrationMain.migrate(corporateUserInfo, applicationDetailsMap);

        // SpMigration
        SpMigration spMigration = new SpMigration();
        migrationMain.migrate(spMigration, applicationDetailsMap);

        // AppMigration
        AppMigration appMigration = new AppMigration();
        migrationMain.migrate(appMigration, applicationDetailsMap);

        // NcsSlaMigration
        NcsSlaMigration ncsSlaMigration = new NcsSlaMigration();
        migrationMain.migrate(ncsSlaMigration, applicationDetailsMap);

        // CommonAdminsMigration
        CommonAdminsMigration commonAdminsMigration = new CommonAdminsMigration();
        migrationMain.migrate(commonAdminsMigration, applicationDetailsMap);

        // ReportingServiceProviderMigration
        ReportingServiceProviderMigration reportingServiceProviderMigration = new ReportingServiceProviderMigration();
        migrationMain.migrate(reportingServiceProviderMigration, applicationDetailsMap);

        // SubscriberCollectionMigration
        SubscriberCollectionMigration subscriberCollectionMigration = new SubscriberCollectionMigration();
        migrationMain.migrate(subscriberCollectionMigration, applicationDetailsMap);
    }

    private void migrate(final IMigration migration, HashMap<String, String> applicationDetailsMap) throws Exception {
        migration.convert(applicationDetailsMap);
    }


    private static void loadApplicationDetails(HashMap<String, String> applicationDetailsMap) throws FileNotFoundException {
        String[][] applicationDetails = loadFromFile(PropertyLoader.get(APPLICATION_DETAILS_FILE_PATH));
        for (String[] applicationDetail : applicationDetails) {
            String oldAppName = applicationDetail[0];
            String newAppName = applicationDetail[1];
            applicationDetailsMap.put(oldAppName, newAppName);
        }
    }

    @SuppressWarnings("resource")
    private static String[][] loadFromFile(final String fileName) throws FileNotFoundException {
        InputStream inputStream = new FileInputStream(fileName);
        InputStreamReader in = new InputStreamReader(inputStream);
        CSVReader reader = new CSVReader(in, CSVParser.DEFAULT_SEPARATOR,
                CSVParser.DEFAULT_QUOTE_CHARACTER,
                CSVParser.DEFAULT_ESCAPE_CHARACTER, 1);
        String[][] linesAndColumns = null;
        try {
            List<String[]> myEntries = reader.readAll();
            ListIterator<String[]> it = myEntries.listIterator();
            linesAndColumns = new String[myEntries.size()][myEntries.get(0).length];
            int j = 0;
            while (it.hasNext()) {
                String[] val = it.next();
                for (int i = 0; i < val.length; i++) {
                    linesAndColumns[j][i] = val[i];
                }
                j++;
            }
        } catch (IOException e) {
        }
        return linesAndColumns;
    }
}
