/*
 *
 *  *
 *  *  *   (C) Copyright 2013-2014 hSenid Software International (Pvt) Limited.
 *  *  *   All Rights Reserved.
 *  *  *
 *  *  *   These materials are unpublished, proprietary, confidential source code of
 *  *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *  *   of hSenid Software International (Pvt) Limited.
 *  *  *
 *  *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *  *   property rights in these materials.
 *  *  *
 *  *
 *
 */
package com.hms.appzone.migration.db;

import com.hms.appzone.migration.util.PropertyLoader;

import java.sql.*;

/**
 * <p>
 * MySQL data accesss
 * </p>
 * 
 * @author manuja
 */
public class MySqlDataAccess {

	Connection conn = null;

	/**
	 * <p>
	 * Execute given query.
	 * </p>
	 * 
	 * @param query
	 *            query string
	 * 
	 * @return result set
	 * 
	 * @throws Exception
	 *             throws when error occurs
	 */
	public ResultSet executeQuery(String query) throws Exception {
		try {
			conn = DriverManager.getConnection(PropertyLoader.get("mysql.url"),
					PropertyLoader.get("mysql.username"),
					PropertyLoader.get("mysql.password"));

			Statement stmt = conn.createStatement();
			return stmt.executeQuery(query);
		} catch (Exception ex) {
			throw ex;
		}
	}

	/**
	 * <p>
	 * Close mysql connection.
	 * </p>
	 * 
	 * @throws SQLException
	 *             throw when sql exception occurs
	 */
	public void closeConnection() throws SQLException {
		if (conn != null) {
			conn.close();
		}
	}
}
