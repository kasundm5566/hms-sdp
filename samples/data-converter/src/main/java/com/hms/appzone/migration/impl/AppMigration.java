package com.hms.appzone.migration.impl;

import com.hms.appzone.migration.IMigration;
import com.hms.appzone.migration.db.MySqlDataAccess;
import com.hms.appzone.migration.util.PropertyLoader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.util.HashMap;

/**
 * <p>
 * Application Migration Implementation.
 * </p>
 *
 * @author dulith
 */
public class AppMigration implements IMigration {

    // ArrayList used to read in non user applications

    // Mongo Collections
    // Mongo Collection name :  App
    private static final String MONGO_NAME = "name";
    private static final String MONGO_APP_ID = "app-id";
    private static final String MONGO_MASK_NUMBER = "mask-number";
    private static final String MONGO_GOVERN = "govern";
    private static final String MONGO_APPLY_TAX = "apply-tax";
    private static final String MONGO_ADVERTISE = "advertise";
    private static final String MONGO_STATUS = "status";
    private static final String MONGO_TYPE = "type";
    private static final String MONGO_APP_REQUEST_DATE = "app-request-date";
    private static final String MONGO_REVENUE_SHARE = "revenue-share";
    private static final String MONGO_DESCRIPTION = "description";
    private static final String MONGO_UPDATED_BY = "updated-by";
    private static final String MONGO_BLACKLISTED = "black-list";
    private static final String MONGO_WHITELISTED = "white-list";
    private static final String MONGO_PREVIOUS_STATE = "previous-state";
    private static final String MONGO_ALLOWED_HOSTS = "allowed-hosts";
    private static final String MONGO_UPDATED_DATE = "updated-date";
    private static final String MONGO_USER_TYPE = "user-type";
    private static final String MONGO_SP_ID = "sp-id";
    private static final String MONGO_CATEGORY = "category";
    private static final String MONGO_CREATED_BY = "created-by";
    private static final String MONGO_CREATED_DATE = "created-date";
    private static final String MONGO_EXPIRE = "expire";
    private static final String MONGO_REMARKS = "remarks";
    private static final String MONGO_PASSWORD = "password";
    private static final String MONGO_NCS_CONFIGURED = "ncs-configured";
    private static final String MONGO_NCS_TYPE = "ncs-type";
    private static final String MONGO_OPERATOR = "operator";
    private static final String MONGO_NCSES = "ncses";
    private static final String MONGO_ACTIVE_PRODUCTION_START_DATE = "active-production-start-date";


    // MySQL table names and their columns
    // Column Names
    private static final String APP_NAME = "app_name";
    private static final String APP_ID = "app_id";
    private static final String DESCRIPTION = "app_description";
    private static final String UPDATED_DATE = "last_modified_time";
    private static final String CREATED_BY = "sp_user_name.username";
    private static final String CREATED_DATE = "created_time";
    private static final String REMARKS = "remark_state";
    private static final String SUB_REG_SLA_TYPE = "subs_reg_sla_type";
    private static final String SP_ID = "sp_id";
    private static final String ACTIVE_PRODUCTION_START_DATE = "production_time";

    // Static values
    private static final boolean MASK_NUMBER = true;
    private static final boolean GOVERN = true;
    private static final boolean APPLY_TAX = false;
    private static final boolean ADVERTISE = false;
    private static final String STATUS = "active-production";
    private static final String RENEUE_SHARE = "70";
    private static final String ON_DEMAND_SUB_REG_SLA_TYPE = "NULL_SUBS_REG_SLA";
    private static final String PASSWORD = "ws_password";
    private static final String UPDATED_BY = "sdpadmin";
    private static final String USER_TYPE = "corporate";
    private static final String CATEGORY = "sdp";
    private static final boolean EXPIRE = false;
    private static final String PREVIOUS_STATE = "active-production";
    private static final String TYPE = "flat";
    private static final String MONGO_AUTO_INCREMENTER = "_id";
    private static final String OPERATOR = "etisalat";
    private static final String NCS_TYPE = "sms";
    private static final String SUBSCRIPTION = "subscription";
    private static final String ALLOWED_HOST_ADDRESS_VALUE = "allowed.host.address";
    public static final String WHITE_LISTED_USERS = "94722361735";
    private static final String OUTPUT_FILE_NAME = "app.json";

    /**
     * {@inheritDoc}
     */

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public void convert(HashMap<String, String> applicationDetailsMap) throws Exception {

        MySqlDataAccess mySqlDataAccess = new MySqlDataAccess();

        ResultSet resultSet = mySqlDataAccess.executeQuery(
                "SELECT distinct " +
                        "app.ws_password, " +
                        "app.remark_state, " +
                        "app.app_description, " +
                        "sp.sp_id as sp_id, " +
                        "production_time, " +
                        "req_sms_mo_sla.connection_url, " +
                        "app.subs_reg_sla_type, " +
                        "mo_allowed, " +
                        "app.last_modified_time, " +
                        "sp_user_name.username, " +
                        "ncs_sla.current_ncs_sla_state, " +
                        "subscriber_registration_sla.registration_success_msg, " +
                        "subscriber_registration_sla.unregistraiton_success_msg, " +
                        "ncs_sla.current_ncs_sla_state, " +
                        "app.app_id, " +
                        "app.created_time, " +
                        "app.app_name, " +
                        "prov_sms_mt_sla.mpd as sms_mt_sla_mpd, " +
                        "prov_sms_mt_sla.mps as sms_mt_sla_mps, " +
                        "prov_sms_mo_sla.mpd as sms_mo_sla_mpd, " +
                        "prov_sms_mo_sla.mps as sms_mo_sla_mps " +
                        "FROM     app app " +
                        "INNER JOIN ncs_sla ncs_sla ON ncs_sla.app_id = app.id " +
                        "LEFT OUTER JOIN prov_sms_mo_sla prov_sms_mo_sla ON prov_sms_mo_sla.id = ncs_sla.mo_sla " +
                        "LEFT OUTER JOIN prov_sms_mt_sla prov_sms_mt_sla ON prov_sms_mt_sla.id = ncs_sla.mt_sla " +
                        "LEFT OUTER JOIN req_sms_mo_sla ON req_sms_mo_sla.id = app.id " +
                        "LEFT OUTER JOIN subscriber_registration_sla ON subscriber_registration_sla.id = app.subs_reg_sla " +
                        "INNER JOIN sp_user_name ON sp_user_name.sp_id = app.sp " +
                        "INNER JOIN sp ON sp.id = app.sp " +
                        "WHERE " +
                        "app.app_id like 'PD%' and " +
                        "app.current_state = 'ACTIVE' and " +
                        "ncs_sla.current_ncs_sla_state = 'ACTIVE_PD';");

        File file = new File(PropertyLoader.get("output.dir") + OUTPUT_FILE_NAME);
        File fileAppNames = new File(PropertyLoader.get("output.dir") + "applications");
        FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
        FileWriter fileWriterApp = new FileWriter(fileAppNames.getAbsoluteFile());
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        BufferedWriter bufferedWriterAppNames = new BufferedWriter(fileWriterApp);
        System.out.println("\nMigrating Apps...");
        int counter = 0;
        while (resultSet.next()) {

            String spId = resultSet.getString(SP_ID);
            spId = isValidUser(spId);

            long activeProductionStartDate = resultSet.getTimestamp(ACTIVE_PRODUCTION_START_DATE).getTime();
            String subsRegSlaType = resultSet.getString(SUB_REG_SLA_TYPE);
            String mongoAutoIncrementer = resultSet.getString(APP_ID);
            String appName = resultSet.getString(APP_NAME);

            String appId = resultSet.getString(APP_ID);
            long appRequestDate = resultSet.getTimestamp(CREATED_DATE).getTime();
            String description = resultSet.getString(DESCRIPTION);

            JSONObject reqDate = new JSONObject();
            reqDate.put("$date", appRequestDate);

            JSONArray ncs = new JSONArray();

            HashMap ncsesMap = new HashMap();

            HashMap ncsesSubscription = new HashMap();

            if (subsRegSlaType.equals(ON_DEMAND_SUB_REG_SLA_TYPE)) {

                ncsesMap.put(MONGO_STATUS, MONGO_NCS_CONFIGURED);
                ncsesMap.put(MONGO_NCS_TYPE, NCS_TYPE);
                ncsesMap.put(MONGO_OPERATOR, OPERATOR);
                ncs.add(ncsesMap);

            } else {

                ncsesMap.put(MONGO_STATUS,MONGO_NCS_CONFIGURED );
                ncsesMap.put(MONGO_NCS_TYPE, NCS_TYPE);
                ncsesMap.put(MONGO_OPERATOR, OPERATOR);
                ncsesSubscription.put(MONGO_STATUS, MONGO_NCS_CONFIGURED);
                ncsesSubscription.put(MONGO_NCS_TYPE, SUBSCRIPTION);
                ncs.add(ncsesMap);
                ncs.add(ncsesSubscription);
            }

            JSONArray allowedHosts = new JSONArray();
            allowedHosts.add(PropertyLoader.get(ALLOWED_HOST_ADDRESS_VALUE));

            long updatedDate = resultSet.getTimestamp(UPDATED_DATE).getTime();

            JSONObject updatedDt = new JSONObject();
            updatedDt.put("$date", updatedDate);

            String createdBy = resultSet.getString(CREATED_BY);
            long createdDate = resultSet.getTimestamp(CREATED_DATE).getTime();

            JSONObject createdDt = new JSONObject();
            createdDt.put("$date", createdDate);

            String remarks = resultSet.getString(REMARKS);
            String password = resultSet.getString(PASSWORD);

            JSONObject productionStartDate = new JSONObject();
            productionStartDate.put("$date", activeProductionStartDate);

            JSONArray whiteListed = new JSONArray();
            whiteListed.add(WHITE_LISTED_USERS);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(MONGO_AUTO_INCREMENTER, mongoAutoIncrementer);

            if (applicationDetailsMap.containsKey(appName)) {
                appName = applicationDetailsMap.get(appName);
            } else {
                System.out.println("ERROR : Application name not found [" + appName + "]");
            }

            jsonObject.put(MONGO_NAME, appName);
            jsonObject.put(MONGO_PASSWORD,password);
            jsonObject.put(MONGO_APP_ID, appId);
            jsonObject.put(MONGO_MASK_NUMBER, MASK_NUMBER);
            jsonObject.put(MONGO_GOVERN, GOVERN);
            jsonObject.put(MONGO_APPLY_TAX, APPLY_TAX);
            jsonObject.put(MONGO_ADVERTISE, ADVERTISE);
            jsonObject.put(MONGO_STATUS, STATUS);
            jsonObject.put(MONGO_TYPE, USER_TYPE);
            jsonObject.put(MONGO_APP_REQUEST_DATE, reqDate);
            jsonObject.put(MONGO_TYPE, TYPE);
            jsonObject.put(MONGO_REVENUE_SHARE, RENEUE_SHARE);
            jsonObject.put(MONGO_DESCRIPTION, description);
            jsonObject.put(MONGO_BLACKLISTED, new JSONArray());
            jsonObject.put(MONGO_WHITELISTED, whiteListed);
            jsonObject.put(MONGO_ALLOWED_HOSTS, allowedHosts);
            jsonObject.put(MONGO_UPDATED_DATE, updatedDt);
            jsonObject.put(MONGO_UPDATED_BY, UPDATED_BY);
            jsonObject.put(MONGO_USER_TYPE, USER_TYPE);
            jsonObject.put(MONGO_SP_ID, spId);
            jsonObject.put(MONGO_CATEGORY, CATEGORY);
            jsonObject.put(MONGO_CREATED_BY, createdBy);
            jsonObject.put(MONGO_CREATED_DATE, createdDt);
            jsonObject.put(MONGO_EXPIRE, EXPIRE);
            jsonObject.put(MONGO_REMARKS, remarks);
            jsonObject.put(MONGO_NCSES, ncs);
            jsonObject.put(MONGO_PREVIOUS_STATE, PREVIOUS_STATE);
            jsonObject.put(MONGO_ACTIVE_PRODUCTION_START_DATE, productionStartDate);

            bufferedWriter.write(jsonObject.toJSONString());
            bufferedWriterAppNames.write(appName + "\n");
            bufferedWriterAppNames.flush();
            bufferedWriter.write("\n");
            counter++;
        }

        bufferedWriter.close();
        mySqlDataAccess.closeConnection();
        System.out.println("Successfully migrated [" + counter + "] Apps.");
    }

    private String isValidUser(final String id) {

        String updatedId = id;

        if (id.equals("10000077")) // sp table`s auto increment value :  id = 47
        {
            updatedId= "10000073";
        }
        else if (id.equals("10000074")) //  sp table`s auto increment value : id = 44
        {
            updatedId= "10000059";
        }
        else if (id.equals("10000110")) //  sp table`s auto increment value :  id = 80
        {
            updatedId= "10000009";
        }
        else if (id.equals("10000279")) //  sp table`s auto increment value : id = 136
        {   updatedId= "10000287";

        }
        else if (id.equals("10000285"))  //  sp table`s auto increment value : id = 139
        {   updatedId= "10000276";

        }

        return updatedId;
    }

}

