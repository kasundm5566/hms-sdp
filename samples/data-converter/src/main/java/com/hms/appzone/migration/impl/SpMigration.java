package com.hms.appzone.migration.impl;

import com.hms.appzone.migration.IMigration;
import com.hms.appzone.migration.db.MySqlDataAccess;
import com.hms.appzone.migration.util.PropertyLoader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.util.HashMap;

/**
 * <p>
 * Sp Migration Implementation.
 * </p>
 *
 * @author dulith
 */

public class SpMigration implements IMigration {

    // Mongo fields
    private static final String MONGO_SDP_USER_COLUMN = "sdp-user";
    private static final String MONGO_SMS_MO = "sms-mo";
    private static final String MONGO_SMS_MO_TPD = "sms-mo-tpd";
    private static final String MONGO_SMS_MO_TPS = "sms-mo-tps";
    private static final String MONGO_SMS_MT = "sms-mt";
    private static final String MONGO_SMS_MT_TPD = "sms-mt-tpd";
    private static final String MONGO_SMS_MT_TPS = "sms-mt-tps";
    private static final String MONGO_SP_SMS_SELECTED = "sms-selected";
    private static final String MONGO_SOLTURA_USER = "soltura-user";
    private static final String MONGO_SP_ID = "sp-id";
    private static final String MONGO_STATUS = "status";
    private static final String MONGO_SP_SUBSCRIPTION_SELECTED = "subscription-selected";
    private static final String MONGO_USSD_MO_TPD1 = "ussd-mo-tpd";
    private static final String MONGO_USSD_MO_TPS = "ussd-mo-tps";
    private static final String MONGO_USSD_SELECTED = "ussd-selected";
    private static final String MONGO_ID_COLUMN = "_id";
    private static final String MONGO_CAS_MO_TPD = "cas-mo-tpd";
    private static final String MONGO_CAS_MO_TPS = "cas-mo-tps";
    private static final String MONGO_CAS_SELECTED = "cas-selected";
    private static final String MONGO_COOP_USER_ID = "coop-user-id";
    private static final String MONGO_COOP_USER_NAME = "coop-user-name";
    private static final String MONGO_CREATED_BY = "created-by";
    private static final String MONGO_DOWNLOADABLE_MCD = "downloadable-mcd";
    private static final String MONGO_DOWNLOADABLE_MDPD = "downloadable-mdpd";
    private static final String MONGO_DOWNLOADABLE_SELECTED = "downloadable-selected";
    private static final String MONGO_LBS_SELECTED = "lbs-selected";
    private static final String MONGO_LBS_TPD = "lbs-tpd";
    private static final String MONGO_LBS_TPS = "lbs-tps";
    private static final String MONGO_SP_MAX_NO_OF_BC_MSGS_PER_DAY = "max-no-of-bc-msgs-per-day";
    private static final String MONGO_MAX_NO_OF_SUBSCRIBERS = "max-no-of-subscribers";
    private static final String MONGO_USSD = "ussd";
    private static final String MONGO_CAS = "cas";
    private static final String MONGO_SUBSCRIPTION = "subscription";
    private static final String MONGO_DOWNLOADABLE = "downloadable";
    private static final String MONGO_LBS = "lbs";
    private static final String MONGO_SMS = "sms";
    private static final String MONGO_REMARKS = "remarks";
    private static final String MONGO_SP_REQUEST_DATE = "sp-request-date";
    private static final String MONGO_SP_SELECTED_SERVICES = "sp-selected-services";
    private static final String MONGO_USSD_MT_TPD = "ussd-mt-tpd";
    private static final String MONGO_USSD_MT_TPS = "ussd-mt-tps";
    //MySQL fields
    private static final String MYSQL_SP_ID = "sp_id";
    private static final String MYSQL_SP_INT_ID = "id";
    private static final String MYSQL_USERNAME = "username";
    private static final String MYSQL_REMARKS = "remark_state";
    private static final String MYSQL_SP_REQUEST_DATE = "created_time";
    //Static fields
    private static final boolean SMS_MT = true;
    private static final boolean SMS_MO = true;
    private static final boolean LBS_SELECTED = true;
    private static final String SOLTURA_USER = "true";
    private static final String USSD_SELECTED = "true";
    private static final String USSD_MO_TPS = "10";
    private static final String USSD_MO_TPD = "10000";
    private static final String USSD_MT_TPS = "10";
    private static final String USSD_MT_TPD = "10000";
    private static final String STATUS = "approved";
    private static final String CAS_SELECTED = "true";
    private static final String SMS_MO_TPD = "30000";
    private static final String SMS_MT_TPD = "30000";
    private static final String SUBSCRIPTION_SELECTED = "true";
    private static final String SMS_SELECTED = "true";
    private static final String CAS_MO_TPD = "5000";
    private static final String DOWNLOADABLE_MCD = "10";
    private static final String DOWNLOADABLE_MDPD = "100";
    private static final String DOWNLOADABLE__SELECTED = "true";
    private static final String CAS_MO_TPS = "10";
    private static final String MAX_NUMBER_OF_SUBSCRIBERS = "100000";
    private static final String MAX_NUMBER_OF_BC_MESSAGES = "100000";
    private static final String SMS_MT_TPS = "30";
    private static final String SMS_MO_TPS = "10";
    private static final String LBS_TPD = "100";
    private static final String LBS_TPS = "10";
    private static final String SDP_USER = "true";
    private static final String OUTPUT_FILE_NAME = "sp.json";

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings({"unchecked"})
    @Override
    public void convert(HashMap<String, String> applicationDetailsMap) throws Exception {
        MySqlDataAccess mySqlDataAccess = new MySqlDataAccess();
        ResultSet resultSet = mySqlDataAccess.executeQuery(
                "SELECT DISTINCT   sp.id, sp.sp_id, " +
                                  "sp_user_name.username, " +
                                  "sp.remark_state, " +
                                  "app.created_time, " +
                                  "mo_sla.mpd as mo_sla_mpd, "+
                                  "mo_sla.mps as mo_sla_mps, "+
                                  "mt_sla.mpd as mt_sla_mpd, " +
                                  "mt_sla.mps as mt_sla_mps " +
                "FROM              sp " +
                                  "INNER JOIN app " +
                                  "ON app.sp = sp.id " +
                                  "INNER JOIN sp_user_name " +
                                  "ON sp.id = sp_user_name.sp_id " +
                                  "INNER JOIN sp_sms_mo_sla mo_sla "  +
                                  "ON mo_sla.id = sp.id " +
                                  "INNER JOIN sp_sms_mt_sla mt_sla " +
                                  "ON mt_sla.id = sp.id " +
                "WHERE             app_id LIKE 'PD%' " +
                                  "AND current_state LIKE '%ACTIVE%' " +
                "GROUP BY          sp_id;");

        File file1 = new File(PropertyLoader.get("output.dir") + OUTPUT_FILE_NAME);
        FileWriter fileWriter1 = new FileWriter(file1.getAbsoluteFile());
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter1);

        JSONArray sp_selected_services = new JSONArray();
        sp_selected_services.add(0, MONGO_USSD);
        sp_selected_services.add(1, MONGO_CAS);
        sp_selected_services.add(2, MONGO_SUBSCRIPTION);
        sp_selected_services.add(3, MONGO_DOWNLOADABLE);
        sp_selected_services.add(4, MONGO_LBS);
        sp_selected_services.add(5, MONGO_SMS);

        System.out.println("\nMigrating SP users . . .");
        int counter = 0;

        while (resultSet.next()) {
            String id = resultSet.getString(MYSQL_SP_INT_ID);
            String coopUserId = resultSet.getString(MYSQL_SP_ID);
            String spId = resultSet.getString(MYSQL_SP_ID);
            String username = resultSet.getString(MYSQL_USERNAME);
            String remarkState = resultSet.getString(MYSQL_REMARKS);
            String spRequestDate = resultSet.getString(MYSQL_SP_REQUEST_DATE);

            String moSlaMPS = resultSet.getString("mo_sla_mps");
            String moSlaMPD = resultSet.getString("mo_sla_mpd");
            String mtSlaMPS = resultSet.getString("mt_sla_mps");
            String mtSlaMPD = resultSet.getString("mt_sla_mpd");

            if (isValidUser(id)) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(MONGO_ID_COLUMN, spId);
                jsonObject.put(MONGO_COOP_USER_ID, coopUserId);
                jsonObject.put(MONGO_SP_ID, spId);
                jsonObject.put(MONGO_COOP_USER_ID, coopUserId);
                jsonObject.put(MONGO_COOP_USER_NAME, username);
                jsonObject.put(MONGO_CREATED_BY, username);
                jsonObject.put(MONGO_SDP_USER_COLUMN, SDP_USER);
                jsonObject.put(MONGO_SOLTURA_USER, SOLTURA_USER);
                jsonObject.put(MONGO_SMS_MT, SMS_MT);
                jsonObject.put(MONGO_SMS_MO, SMS_MO);
                jsonObject.put(MONGO_USSD_SELECTED, USSD_SELECTED);
                jsonObject.put(MONGO_USSD_MO_TPS, USSD_MO_TPS);
                jsonObject.put(MONGO_USSD_MO_TPD1, USSD_MO_TPD);
                jsonObject.put(MONGO_MAX_NO_OF_SUBSCRIBERS, MAX_NUMBER_OF_SUBSCRIBERS);
                jsonObject.put(MONGO_STATUS, STATUS);
                jsonObject.put(MONGO_CAS_SELECTED, CAS_SELECTED);
                jsonObject.put(MONGO_SMS_MO_TPD, moSlaMPD);
                jsonObject.put(MONGO_SMS_MT_TPD, mtSlaMPD);
                jsonObject.put(MONGO_SMS_MT_TPS, mtSlaMPS);
                jsonObject.put(MONGO_SMS_MO_TPS, moSlaMPS);
                jsonObject.put(MONGO_SP_SUBSCRIPTION_SELECTED, SUBSCRIPTION_SELECTED);
                jsonObject.put(MONGO_SP_SMS_SELECTED, SMS_SELECTED);
                jsonObject.put(MONGO_CAS_MO_TPD, CAS_MO_TPD);
                jsonObject.put(MONGO_CAS_MO_TPS, CAS_MO_TPS);
                jsonObject.put(MONGO_SP_MAX_NO_OF_BC_MSGS_PER_DAY, MAX_NUMBER_OF_BC_MESSAGES);
                jsonObject.put(MONGO_DOWNLOADABLE_MCD, DOWNLOADABLE_MCD);
                jsonObject.put(MONGO_DOWNLOADABLE_MDPD, DOWNLOADABLE_MDPD);
                jsonObject.put(MONGO_DOWNLOADABLE_SELECTED, DOWNLOADABLE__SELECTED);
                jsonObject.put(MONGO_LBS_SELECTED, LBS_SELECTED);
                jsonObject.put(MONGO_LBS_TPD, LBS_TPD);
                jsonObject.put(MONGO_LBS_TPS, LBS_TPS);
                jsonObject.put(MONGO_SP_SELECTED_SERVICES, sp_selected_services);
                jsonObject.put(MONGO_REMARKS, remarkState);
                jsonObject.put(MONGO_SP_REQUEST_DATE, spRequestDate);
                jsonObject.put(MONGO_USSD_MT_TPD, USSD_MT_TPD);
                jsonObject.put(MONGO_USSD_MT_TPS, USSD_MT_TPS);

                bufferedWriter.write(jsonObject.toJSONString() + "\n");
                counter++;
            }
        }

        bufferedWriter.close();
        mySqlDataAccess.closeConnection();
        System.out.println("Successfully migrated [" + counter + "] SP users.");
    }

    private boolean isValidUser(final String id) {

        if (id.equals("47") || id.equals("44") || id.equals("80") || id.equals("136") || id.equals("139")) {
            return false;
        }

        return true;
    }


}

