package com.hms.appzone.migration.impl;

import com.hms.appzone.migration.IMigration;
import com.hms.appzone.migration.db.MySqlDataAccess;
import com.hms.appzone.migration.util.PropertyLoader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.util.HashMap;

/**
 * <p>
 * Ncs Sla Migration Implementation.
 * </p>
 *
 * @author dulith
 */
public class NcsSlaMigration implements IMigration {

    // Mongo Collection
    private static final String MONGO_APP_ID = "app-id";
    private static final String MONGO_MT_ALLOWED = "mt-allowed";
    private static final String MONGO_ALIASING = "aliasing";
    private static final String MONGO_DEFAULT_SENDER_MARKET = "app-request-date";
    private static final String MONGO_CREATED_DATE = "created-date";
    private static final String MONGO_MO_ALLOWED = "mo-allowed";
    private static final String CREATED_DATE = "created-date";
    private static final String UPDATED_DATE = "updated-date";
    private static final String MONGO_DEFAULT_SENDER_ADDRESS = "default-sender-address";
    private static final String MONGO_AMOUNT = "amount";
    private static final String MONGO_FREQUENCY = "frequency";
    private static final String MONGO_PARTY = "party";
    private static final String MONGO_TYPE = "type";
    private static final String MONGO_CHARGING = "charging";
    private static final String MONGO_CREATED_BY = "created-by";
    private static final String MONGO_MAX_NO_OF_BC_MSGS_PER_DAY = "max-no-of-bc-msgs-per-day";
    private static final String MONGO_NCS_TYPE = "ncs-type";
    private static final String MONGO_OPERATOR = "operator";
    private static final String MONGO_STATUS = "status";
    private static final String MONGO_SUB_RESPONSE_MSG = "subscription-response-message";
    private static final String MONGO_UN_SUB_RESPONSE_MSG = "unsubscription-response-message";
    private static final String MONGO_UPDATED_BY = "updated-by";
    private static final String MONGO_ALLOW_HTTP_SUBSCRIPTION_REQUESTS = "allow-http-subscription-requests";
    private static final String MONGO_ALLOWED_PAYMENT_INSTRUMENTS = "allowed-payment-instruments";
    private static final String MONGO_TPD = "tpd";
    private static final String MONGO_TPS = "tps";
    private static final String MONGO_CONNECTION_URL = "connection-url";
    public final String SUBSCRIPTIONREQUIRED = "subscription-required";
    private static final String ALLOWED = "allowed";
    private static final String MAX_COUNT = "max-count";
    private static final String UNIT = "unit";
    private static final String VALUE = "value";
    private static final String TRIALS = "trials";
    private static final String PERIOD = "period";


    // MySQL table names and their columns
    private static final String STATUS = "current_ncs_sla_state";
    private static final String MO_ALLOWED = "mo_allowed";
    private static final String KEYWORD = "keyword";
    private static final String SMS_MO_MPD = "sms_mo_sla_mpd";
    private static final String SMS_MO_MPS = "sms_mo_sla_mps";
    private static final String SMS_MT_MPD = "sms_mt_sla_mpd";
    private static final String SMS_MT_MPS = "sms_mt_sla_mps";
    private static final String CREATED_TIME = "created_time";
    private static final String REAL_APP_ID = "app.app_id";
    private static final String SP_USER_NAME = "sp_user_name.username";
    private static final String LAST_MODIFIED_TIME = "last_modified_time";
    private static final String SUB_REG_SLA_TYPE = "subs_reg_sla_type";
    private static final String SUB_REG_SLA = "SUBS_REG_SLA";
    private static final String CONNECTION_URL = "connection_url";

    //Static values
    public boolean subscriptionRequiredBool = true;
    private static final String OPERATOR = "etisalat";
    private static final boolean MT_ALLOWED = true;
    private static final String NCS_TYPE = "sms";
    private static final String MOBILE_ACCOUNT = "Mobile Account";
    private static final String SHORT_CODE = "4499";
    private static final String SUBSCRIPTION_AMOUNT = "15";
    private static final String MT_AMOUNT = "5";
    private static final String FREQUENCY = "fifteen-days";
    private static final String PARTY = "subscriber";
    private static final String FLAT_TYPE = "flat";
    private static final String FREE_TYPE = "free";
    private static final String MAX_NO_OF_BC_MSGS_PER_DAY = "1000";
    private static final String NCS_TYPE_SUB = "subscription";
    private static final String STATUS_ACTIVE = "active-production";
    private static final String UPDATED_BY = "sdpadmin";
    private static String MONGO_MT = "mt";
    private static String MONGO_MO = "mo";
    private static final String LEGACY_CONVERTER_URL = "legacy.converter.url";
    private static final String STATUS_CURRENT = "ACTIVE_PD";
    private static final String ACTIVE_PRODUCTION = "active-production";
    private static final String LIMITED_PRODUCTION= "limited-production";
    private static String SUBSCRIPTION_MESSAGE = "";
    private static String UNSUBCRIPTION_MESSAGE = "";
    private static final int MAXCOUNT_VALUE = 1;
    private static final int DAYS = 15;
    private static final String VALUE_DAYS = "period";
    private static final boolean ALLOWED_VALUE = true;
    private static final  String UNIT_VALUE = "days";

    private static final String OUTPUT_FILE_NAME = "ncs_sla.json";

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings({ "unchecked"})
    @Override
    public void convert(HashMap<String, String> applicationDetailsMap) throws Exception {
        MySqlDataAccess mySqlDataAccess = new MySqlDataAccess();
        ResultSet resultSet = mySqlDataAccess.executeQuery(
        		"SELECT  subs_reg_sla_type, " +
	                    "ncs_sla.mo_allowed, " +
	                    "app.last_modified_time, "  +
	                    "keyword, " +
	                    "ncs_sla.current_ncs_sla_state, " +
	                    "prov_sms_mo_sla.connection_url, " +
	                    "sp_user_name.username, " +
	                    "app.app_name, " +
	                    "prov_sms_mt_sla.mpd as sms_mt_sla_mpd, " +
	                    "prov_sms_mt_sla.mps as sms_mt_sla_mps, " +
	                    "prov_sms_mo_sla.mpd as sms_mo_sla_mpd, " +
	                    "prov_sms_mo_sla.mps as sms_mo_sla_mps, " +
	                    "app.app_id, " +
	                    "app.created_time " +
	             "FROM   ncs_sla ncs_sla " +
	                    "INNER JOIN  app app ON ncs_sla.app_id = app.id " +
	                    "INNER JOIN routing_key rk ON rk.active_app_id = app.app_id " +
	                    "LEFT OUTER JOIN prov_sms_mo_sla prov_sms_mo_sla ON prov_sms_mo_sla.id = ncs_sla.mo_sla " +
	                    "LEFT OUTER JOIN prov_sms_mt_sla prov_sms_mt_sla ON prov_sms_mt_sla.id = ncs_sla.mt_sla " +
	                    "INNER JOIN sp_user_name ON sp_user_name.sp_id = app.sp " +
	             "WHERE  app.app_id LIKE 'PD%' " +
	                    "AND app.current_state LIKE '%ACTIVE%' " +
	                    "AND ncs_sla.current_ncs_sla_state ='ACTIVE_PD'; ");

        File file1 = new File(PropertyLoader.get("output.dir") + OUTPUT_FILE_NAME);
        FileWriter fileWriter1 = new FileWriter(file1.getAbsoluteFile());
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter1);

        JSONObject moCharging = new JSONObject();
        moCharging.put(MONGO_TYPE, FREE_TYPE);

        JSONArray aliasing = new JSONArray();
        aliasing.add(SHORT_CODE);

        System.out.println("\nMigrating ncs_sla . . .");

        int smsNceCount = 0;
        int subsNcesCount = 0;

        JSONArray allowedPaymentInstruments = new JSONArray();
        allowedPaymentInstruments.add(MOBILE_ACCOUNT);

        while (resultSet.next()) {

            JSONObject mt = new JSONObject();
            mt.put(MONGO_ALIASING, aliasing);
            mt.put(MONGO_DEFAULT_SENDER_ADDRESS, SHORT_CODE);

            JSONObject mtCharging = new JSONObject();
            JSONObject mo = new JSONObject();

        	String legacyConverterUrl = PropertyLoader.get(
					LEGACY_CONVERTER_URL, resultSet.getString(CONNECTION_URL));
        	
			mo.put(MONGO_TPD, resultSet.getString(SMS_MO_MPD));
			mo.put(MONGO_TPS, resultSet.getString(SMS_MO_MPS));
			mo.put(MONGO_CONNECTION_URL, legacyConverterUrl);
			mt.put(MONGO_TPD, resultSet.getString(SMS_MT_MPD));
			mt.put(MONGO_TPS, resultSet.getString(SMS_MT_MPS));
			mo.put(MONGO_CHARGING, moCharging);
			mt.put(MONGO_CHARGING, mtCharging);

			long createdDate = resultSet.getTimestamp(CREATED_TIME).getTime();
			String appId = resultSet.getString(REAL_APP_ID);
			String username = resultSet.getString(SP_USER_NAME);
			String keyword = resultSet.getString(KEYWORD);
			long defaultSenderMarket = resultSet.getTimestamp(CREATED_TIME).getTime();
            long updatedDate = resultSet.getTimestamp(LAST_MODIFIED_TIME).getTime();
            boolean moAllowed = resultSet.getBoolean(MO_ALLOWED);
            String createdBy = resultSet.getString(SP_USER_NAME);
            String status = resultSet.getString(STATUS);
            if (status.equals(STATUS_CURRENT)) {
                status = ACTIVE_PRODUCTION;

            } else {
                status = LIMITED_PRODUCTION;
            }

           
            JSONObject createdDate1 = new JSONObject();
            createdDate1.put("$date", createdDate);

            JSONObject defaultSenderMarktetDate = new JSONObject();
            defaultSenderMarktetDate.put("$date", defaultSenderMarket);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(MONGO_MT, mt);
            jsonObject.put(MONGO_MO, mo);
            jsonObject.put(MONGO_MT, mt);
            jsonObject.put(MONGO_MO, mo);
            jsonObject.put(MONGO_APP_ID, appId);
            jsonObject.put(MONGO_CREATED_BY, username);
            jsonObject.put(MONGO_NCS_TYPE, NCS_TYPE);
            jsonObject.put(MONGO_OPERATOR, OPERATOR);
            jsonObject.put(MONGO_MT_ALLOWED, MT_ALLOWED);
            jsonObject.put(MONGO_STATUS, status);
            jsonObject.put(MONGO_MO_ALLOWED, moAllowed);
            jsonObject.put(MONGO_ALIASING, aliasing);
            jsonObject.put(MONGO_DEFAULT_SENDER_MARKET, defaultSenderMarktetDate);
            jsonObject.put(MONGO_CREATED_DATE, createdDate1);

            String subscriptionRegistrationSla = resultSet.getString(SUB_REG_SLA_TYPE);
            if (subscriptionRegistrationSla.equals(SUB_REG_SLA)) {
            	
            	String subscriptionMessage = SUBSCRIPTION_MESSAGE;

                mtCharging.put(MONGO_TYPE, FREE_TYPE);

                JSONObject subscriptionNcs = new JSONObject();

                subscriptionNcs.put(CREATED_DATE, createdDate);

                JSONObject updatedDate1 = new JSONObject();
                updatedDate1.put("$date", updatedDate);

                subscriptionNcs.put(UPDATED_DATE, updatedDate1);

                JSONObject createdDate2 = new JSONObject();

                createdDate2.put("$date", createdDate);
                JSONObject updatedDate2 = new JSONObject();
                updatedDate2.put("$date", updatedDate);
                subscriptionNcs.put(CREATED_DATE, createdDate1);

                subscriptionNcs.put(MONGO_ALLOW_HTTP_SUBSCRIPTION_REQUESTS, true);
                subscriptionNcs.put(MONGO_APP_ID, appId);
                JSONObject chargingSubscriptionNcs = new JSONObject();


                JSONObject trials = new JSONObject();
                trials.put( ALLOWED, ALLOWED_VALUE);
                trials.put(MAX_COUNT , MAXCOUNT_VALUE);


                JSONObject period = new JSONObject();
                period.put(UNIT , UNIT_VALUE);
                period.put(VALUE , DAYS);


                chargingSubscriptionNcs.put(TRIALS , trials);

                trials.put(VALUE_DAYS , period);

                chargingSubscriptionNcs.put(MONGO_ALLOWED_PAYMENT_INSTRUMENTS, allowedPaymentInstruments);
                chargingSubscriptionNcs.put(MONGO_AMOUNT, SUBSCRIPTION_AMOUNT);
                chargingSubscriptionNcs.put(MONGO_FREQUENCY, FREQUENCY);
                chargingSubscriptionNcs.put(MONGO_PARTY, PARTY);
                chargingSubscriptionNcs.put(MONGO_TYPE, FLAT_TYPE);
                subscriptionNcs.put(MONGO_CHARGING, chargingSubscriptionNcs);
                subscriptionNcs.put(MONGO_CREATED_BY, createdBy);
                subscriptionNcs.put(MONGO_MAX_NO_OF_BC_MSGS_PER_DAY, MAX_NO_OF_BC_MSGS_PER_DAY);
                subscriptionNcs.put(MONGO_NCS_TYPE, NCS_TYPE_SUB);
                subscriptionNcs.put(MONGO_OPERATOR, OPERATOR);
                subscriptionNcs.put(MONGO_STATUS, STATUS_ACTIVE);
                subscriptionNcs.put(MONGO_SUB_RESPONSE_MSG, subscriptionMessage);
                subscriptionNcs.put(MONGO_UN_SUB_RESPONSE_MSG, UNSUBCRIPTION_MESSAGE);

                JSONObject updateDate3 = new JSONObject();
                updateDate3.put("$date", updatedDate);

                jsonObject.put(MONGO_UPDATED_BY, UPDATED_BY);
                jsonObject.put(UPDATED_DATE, updateDate3);
                jsonObject.put(SUBSCRIPTIONREQUIRED,subscriptionRequiredBool );



                bufferedWriter.write(subscriptionNcs.toJSONString() + "\n" + jsonObject.toJSONString() + "\n");
                subsNcesCount++;
            } else {
                mtCharging.put(MONGO_TYPE, FLAT_TYPE);
                mtCharging.put(MONGO_ALLOWED_PAYMENT_INSTRUMENTS, allowedPaymentInstruments);
                mtCharging.put(MONGO_AMOUNT, MT_AMOUNT);
                mtCharging.put(MONGO_PARTY, PARTY);

                bufferedWriter.write(jsonObject.toJSONString() + "\n");
            }

            smsNceCount++;

        }
        bufferedWriter.close();
        mySqlDataAccess.closeConnection();
        System.out.println("Successfully migrate [" + subsNcesCount + "] subscription ncs_sla");
        System.out.println("Successfully migrated [" + smsNceCount + "] sms ncs_sla.");

    }

}
