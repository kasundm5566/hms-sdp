package com.hms.appzone.migration.impl;

/**
 * <p>
 * Sp Migration Implementation.
 * </p>
 *
 * @author dulith
 */

import com.hms.appzone.migration.IMigration;
import com.hms.appzone.migration.db.MySqlDataAccess;
import com.hms.appzone.migration.util.PropertyLoader;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.util.HashMap;

public class CorporateUserInfo implements IMigration {

    // Static Values
    private static final String USER_ID = "sp_id";
    private static final String MYSQL_SP_INT_ID = "sp.id";
    private static final String USERNAME = "username";
    private static final String OUTPUT_FILE_NAME = "corporate_user_info.sql";
    public static int COUNTER = 0;
    private static String SP_BENEFICIARY = "null";
    private static String BANK_CODE = "null";
    private static String BRANCH_CODE = "null";
    private static String BRANCH_NAME = "null";
    private static String BANK_ACCOUNT_NUMBER = "null";

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings({"resource" })
    @Override
    public void convert(HashMap<String, String> applicationDetailsMap) throws Exception {

        FileWriter fileWriter = new FileWriter(PropertyLoader.get("output.dir")
                + OUTPUT_FILE_NAME);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        System.out.println("Migrating users for common admin . . .");
        MySqlDataAccess mySqlDataAccess = new MySqlDataAccess();
        String sql = "SELECT DISTINCT     sp.id, " +
                "sp.sp_id, " +
                "sp_user_name.username " +
                "FROM               sp " +
                "INNER JOIN app " +
                "ON app.sp = sp.id " +
                "INNER JOIN sp_user_name " +
                "ON sp.id = sp_user_name.sp_id " +
                "WHERE              app_id LIKE 'PD%'" +
                "AND current_state LIKE '%ACTIVE%' " +
                "GROUP BY           sp_id;";

        ResultSet rs = mySqlDataAccess.executeQuery(sql);
        bufferedWriter.write(
                "INSERT INTO  corporate_user_info " +
                        "(corporate_user_id, " +
                        "sp_beneficiary, " +
                        "sp_beneficiary_name, " +
                        "sp_beneficiary_bank_code, " +
                        "sp_beneficiary_branch_code, " +
                        "sp_beneficiary_branch_name, " +
                        "sp_beneficiary_bank_account_number ) " +

                        "VALUES    " );

        while (rs.next()) {
            String id = rs.getString(MYSQL_SP_INT_ID);
            String userId = rs.getString(USER_ID);
           String  username = rs.getString(USERNAME);

            if (isValidUser(id))

            {
                if (COUNTER == 0) {
                    bufferedWriter.write(
                            "('" + userId + "', " +
                                    "'" + SP_BENEFICIARY + "', " +
                                    "'" + username + "', " +
                                    "'" + BANK_CODE + "', " +
                                    "'" + BRANCH_CODE + "', " +
                                    "'" + BRANCH_NAME + "', " +
                                    "'" + BANK_ACCOUNT_NUMBER + "')  ");
                } else {
                    bufferedWriter.write(
                            " ,('" + userId + "', " +
                                    "'" + SP_BENEFICIARY + "', " +
                                    "'" + username + "', " +
                                    "'" + BANK_CODE + "', " +
                                    "'" + BRANCH_CODE + "', " +
                                    "'" + BRANCH_NAME + "', " +
                                    "'" + BANK_ACCOUNT_NUMBER + "' ) ");
                }
                bufferedWriter.write("\n");
                COUNTER++;
            }
        }

        bufferedWriter.write(" ;");

        bufferedWriter.flush();
        mySqlDataAccess.closeConnection();
        System.out.println("Successfully migrated [" + COUNTER + "] users for corporate_user_info;");
    }

    private boolean isValidUser(final String id) {

        return !(id.equals("47") || id.equals("44") || id.equals("80") || id.equals("136") || id.equals("139"));

    }
}


