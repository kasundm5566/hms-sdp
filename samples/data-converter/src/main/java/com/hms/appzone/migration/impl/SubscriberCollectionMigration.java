package com.hms.appzone.migration.impl;

import com.hms.appzone.migration.IMigration;
import com.hms.appzone.migration.db.MySqlDataAccess;
import com.hms.appzone.migration.util.PropertyLoader;
import hms.commons.IdGenerator;
import org.json.simple.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * Subscriber Collection Migration Implementation.
 * </p>
 *
 * @author dulith
 */

public class SubscriberCollectionMigration implements IMigration {

    // Required Static fields - Mongo Database
    public static final String MONGO_FREQUENCY = "frequency";
    public static final String MONGO_CURRENT_STATUS = "current-status";
    public static final String MONGO_APP_ID = "app-id";
    public static final String MONGO_SP_ID = "sp-id";
    public static final String MONGO_APP_NAME = "app-name";
    public static final String MONGO_CHARGING_AMOUNT = "charging-amount";
    public static final String MONGO_MSISDN = "msisdn";
    public static final String MONGO_MANDATE_ID = "mandate-id";
    public static final String MONGO_KEYWORD = "keyword";
    public static final String MONGO_SHORT_CODE = "shortcode";
    public static final String MONGO_APP_STATUS = "app-status";
    public static final String MONGO_COOP_USER_NAME = "coop-user-name";
    public static final String MONGO_OPERATOR = "operator";
    public static final String USERNAME = "username";
    public static final String MONGO_CREATED_DATE = "created-date";
    public static final String MONGO_LAST_MODIFIED_DATE = "last-modified-date";
    public static final String MONGO_LAST_CHARGED_TIME = "last-charged-time";
    public static final String MONGO_CONSUMED_TRIALS_COUNT = "consumed-trials-count";
    public static final String MONGO_WHITE_LISTED_USERS = "white-list";
    private static final String MONGO_STATUS = "status";
    private static final String MONGO_LAST_CHARGED_AMOUNT_Subscriber = "last-charged-amount";
    private static final String MONGO_LAST_CORRELATION_ID = "correlation-id";
    private static final String MONGO_END_DATE = "end-date";
    private static final String MONGO_EXPIREABLE_APP = "expirable-app";


    // mongo keys for mandate creation
    private static final String MONGO__ID_KEY = "_id";
    private static final String MONGO_META_DATA_KEY = "meta-data";
    private static final String MONGO_SCHEDULE_KEY = "schedule";
    private static final String MONGO_RETRY_ATTEMPT_KEY = "retry-attempt";
    private static final String MONGO_CHARGE_AMOUNT_KEY = "charge-amount";
    private static final String MONGO_PHASE_KEY = "phase";
    private static final String MONGO_CHARGING_SHORT_INSTRUCTIONS_KEY = "charging-short-instructions";
    private static final String MONGO_LOADING_STATE_KEY = "loading-state";
    private static final String MONGO_NEXT_DATE_KEY = "next-date";
    private static final String MONGO_EXECUTION_COUNT_KEY = "execution-count";
    private static final String MONGO_FREQUENCY_KEY = "frequency";
    private static final String MONGO_STATE_KEY = "state";
    private static final String MONGO_END_DATE_KEY = "end-date";
    private static final String MONGO_START_CRITERIA_KEY = "start-criteria";
    private static final String MONGO_HEADERS_KEY = "headers";
    private static final String MONGO_SYSTEM_ID_KEY = "system-id";
    private static final String MONGO_MANDATE_ID_KEY = "mandate-id";
    private static final String MONGO_CORRELATION_ID_KEY = "correlation-id";
    private static final String MONGO_START_DATE_KEY = "start-date";
    private static final String MONGO_CHARGING_KEY = "charging";
    private static final String MONGO_AMOUNT_KEY = "amount";
    private static final String MONGO_VALUE_KEY = "value";
    private static final String MONGO_CURRENCY_CODE_KEY = "currency-code";
    private static final String MONGO_CATEGORY_KEY = "category";
    private static final String MONGO_MODEL_KEY = "model";
    private static final String MONGO_MERCHANT_ID_KEY = "merchant-id";
    private static final String MONGO_MSISDN_KEY = "msisdn";
    private static final String MONGO_MERCHANT_NAME_KEY = "merchant-name";
    private static final String MONGO_TYPE_KEY = "type";
    private static final String MONGO_FROM_PAYMENT_INSTRUMENT_KEY = "from-payment-instrument";
    private static final String MONGO_DESCRIPTION_KEY = "description";
    private static final String MONGO_CONTROL_DATA_KEY = "control-data";
    private static final String MONGO_APP__KEY = "app";
    private static final String MONGO_ID_KEY = "id";
    private static final String MONGO_SP_KEY = "sp";

    // Required static fields in MySQL database
    public static final String MYSQL_APP_ID = "app_id";
    public static final String MYSQL_SP_ID = "sp_id";
    public static final String MYSQL_APP_NAME = "app_name";
    public static final String MYSQL_SUBSCRIBER_ID = "subscriber_id";
    public static final String MYSQL_KEYWORD = "keyword";
    public static final String REGISTRATION_STATUS = "registrationStatus";
    public static final String LAST_MODIFIED_DATE = "last_modified_time";
    public static final String CREATED_TIME = "created_time";
    public static final String MYSQL_LAST_CHARGED_DATE = "last_charged_date";
    public static final String MYSQL_NEXT_CHARGING_DATE = "next_charging_date";
    // Static fields
    public static final String OPERATOR = "etisalat";
    public static final String SHORTCODE = "4499";
    public static final String APP_ID_MATCHER = "PD";
    public static final String APP_STATUS_MATCHER = "ACTIVE";
    public static final String ACTIVE_PRODUCTION = "active-production";
    public static final int TRIAL_COUNT = 1;
    private static final String LAST_CHARGED_AMOUNT = "15";
    private static final String CORRELATION_ID = "1";
    private static final String END_DATE = null;
    private static final boolean EXPRIABLE_APP = false;
    private static final String STATUS = "S1000";

    private static final String REGISTRATION_STATUS_UNREGISTERED = "UNREGISTERED";
    private static final String REGISTRATION_STATUS_OLD_SDP_TEMPARARY_BLOCKED = "TEMPARARY_BLOCKED";
    private static final String REGISTRATION_STATUS_TEMPORARY_BLOCKED = "TEMPORARY_BLOCKED";
    private static final String REGISTRATION_STATUS_BLOCKED = "BLOCKED";

    // STATIC VALUES for ma0ndate creation

    private static final String CILENT_TRANSID = "client-transid";

    private static final String LAST_CYCLE_DATE = "last-cycle-date";
    private static final String LAST_EXECUTION_DATE = "last-execution-date";
    private static final String PGW_STATUS_CODE = "pgw-status-code";
    private static final String PGW_TX_ID = "pgw-tx-id";
    private static final Double CHARGE_AMOUNT = 15.0;
    private static final String PHASE = "initial";
    private static final String EXECUTION_STATE = "unloaded";
    private static final String FREQUENCY = "fifteen-days";
    private static final String START_CRITERIA = "given-date";
    private static final String SYSTEM_ID = "subscription";
    private static final String CURRENCY_CODE = "LKR";
    private static final String CATEGORY = "subscription";
    private static final String MODEL = "fixed";
    private static final String TYPE = "subscription";
    private static final String PAYMENT_INSTRUMENT = "Mobile Account";
    private static final String DESCRIPTION = "subscription charging mandate request";
    private static final long RETRY_ATTEMPT = 0;
    private static final long EXECUTION_COUNT = 0;
    private static final String CHARGING_AMOUNT_VALUE = "15";
    private static final String CURRENT_STATE = "current_state";

    private static final String OUTPUT_FILE_NAME = "subscription.json";
    public static final String OUTPUT_FILE_NAME_MANDATE = "mandate.json";

    @SuppressWarnings({"resource", "unchecked"})
    @Override
    public void convert(HashMap<String, String> applicationDetailsMap) throws Exception {
        System.out.println(" Migrating Subscribers . . .");
        MySqlDataAccess mySqlDataAccess = new MySqlDataAccess();
        ResultSet resultSet = mySqlDataAccess
                .executeQuery(
                        "SELECT     " +
                                "* " +
                                "FROM " +
                                "subscriber_registration " +
                                "inner join " +
                                "(select  " +
                                "app.app_id, " +
                                "app.app_name, " +
                                "sp.sp_id, " +
                                "routing_key.keyword,  " +
                                "duration_based_subs_charging_policy.charging_amount, " +
                                "duration_based_subs_charging_policy.duration, " +
                                "sp_user_name.username " +
                                "From " +
                                "app " +
                                "left join sp ON sp.id = app.sp " +
                                "left join routing_key ON routing_key.active_app_id = app.app_id " +
                                "left join duration_based_subs_charging_policy ON app.id = duration_based_subs_charging_policy.id " +
                                "left join sp_user_name ON sp_user_name.sp_id = sp.id where app.current_state = 'ACTIVE') apps ON subscriber_registration.app_id = apps.app_id"
                );

        File file = new File(PropertyLoader.get("output.dir") + OUTPUT_FILE_NAME);
        FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        File mandateFile = new File(PropertyLoader.get("output.dir") + OUTPUT_FILE_NAME_MANDATE);
        FileWriter mandateFileWriter = new FileWriter(mandateFile);
        BufferedWriter mandateBufferedWriter = new BufferedWriter(mandateFileWriter);

        Map<String, Boolean> mandateIdMap = new HashMap<String, Boolean>();

        int counter = 0;
        int duplicateCounter = 0;
        while (resultSet.next()) {

            String currentStatus = resultSet.getString(REGISTRATION_STATUS);
            if (currentStatus.equals(REGISTRATION_STATUS_BLOCKED) || currentStatus.equals(REGISTRATION_STATUS_OLD_SDP_TEMPARARY_BLOCKED))
                currentStatus = REGISTRATION_STATUS_TEMPORARY_BLOCKED;
            String spId = resultSet.getString(MYSQL_SP_ID);
            String appName = resultSet.getString(MYSQL_APP_NAME);
            if (applicationDetailsMap.containsKey(appName)) {
                appName = applicationDetailsMap.get(appName);
            } else {
                System.out.println("ERROR : Application name not found [" + appName + "]");
            }

            String msisdn = resultSet.getString(MYSQL_SUBSCRIBER_ID);
            String keyword = resultSet.getString(MYSQL_KEYWORD).toLowerCase();;
            String coopUserName = resultSet.getString(USERNAME);
            long lastModifiedDate2 = resultSet.getTimestamp(LAST_MODIFIED_DATE).getTime();
            long createdDate2 = resultSet.getTimestamp(CREATED_TIME).getTime();
            String appId = resultSet.getString(MYSQL_APP_ID);
            long lastChargedDate = resultSet.getLong(MYSQL_LAST_CHARGED_DATE);
            long nextChargingDate = resultSet.getLong(MYSQL_NEXT_CHARGING_DATE);

            JSONObject lastModifiedDate1 = new JSONObject();
            lastModifiedDate1.put("$date", lastModifiedDate2);

            JSONObject createdDate1 = new JSONObject();
            createdDate1.put("$date", createdDate2);

            JSONObject createdDate = new JSONObject();
            createdDate.put(MONGO_CREATED_DATE, createdDate2);

            JSONObject lastModifiedDate = new JSONObject();
            lastModifiedDate.put(MONGO_LAST_MODIFIED_DATE, lastModifiedDate2);

            JSONObject lastChargedTime = new JSONObject();
            lastChargedTime.put("$date", lastChargedDate);

            String mandateId = null;

            while (true) {
                mandateId = IdGenerator.generateId();
                if (!mandateIdMap.containsKey(mandateId)) {
                    mandateIdMap.put(mandateId, true);
                    break;
                } else {
                    duplicateCounter++;
                }
            }

            JSONObject jsonObject = new JSONObject();
            jsonObject.put(MONGO_CREATED_DATE, createdDate);
            jsonObject.put(MONGO_LAST_MODIFIED_DATE, lastModifiedDate);
            jsonObject.put(MONGO_LAST_CHARGED_TIME, lastChargedTime);
            jsonObject.put(MONGO_CURRENT_STATUS, currentStatus);
            jsonObject.put(MONGO_APP_ID, appId);
            jsonObject.put(MONGO_SP_ID, spId);
            jsonObject.put(MONGO_APP_NAME, appName);
            jsonObject.put(MONGO_CHARGING_AMOUNT, CHARGE_AMOUNT);
            jsonObject.put(MONGO_MSISDN, msisdn);
            jsonObject.put(MONGO_KEYWORD, keyword);
            jsonObject.put(MONGO_SHORT_CODE, SHORTCODE);
            jsonObject.put(MONGO_LAST_MODIFIED_DATE, lastModifiedDate1);
            jsonObject.put(MONGO_CREATED_DATE, createdDate1);
            jsonObject.put(MONGO_APP_STATUS, ACTIVE_PRODUCTION);
            jsonObject.put(MONGO_COOP_USER_NAME, coopUserName);
            jsonObject.put(MONGO_OPERATOR, OPERATOR);
            jsonObject.put(MONGO_FREQUENCY, FREQUENCY);
            jsonObject.put(MONGO_MANDATE_ID, mandateId);
            jsonObject.put(MONGO_CONSUMED_TRIALS_COUNT, TRIAL_COUNT);
            jsonObject.put(MONGO_STATUS, STATUS);
            jsonObject.put(MONGO_LAST_CHARGED_AMOUNT_Subscriber, LAST_CHARGED_AMOUNT);
            jsonObject.put(MONGO_CORRELATION_ID_KEY, mandateId);
            jsonObject.put(MONGO_END_DATE, END_DATE);
            jsonObject.put(MONGO_EXPIREABLE_APP, EXPRIABLE_APP);

            bufferedWriter.write(jsonObject.toJSONString() + "\n");

            if (!currentStatus.equals(REGISTRATION_STATUS_UNREGISTERED)) {
                mandateBufferedWriter.write(createMandate(mandateId, appId, appName,
                        spId, msisdn, currentStatus, createdDate2, lastChargedDate, nextChargingDate) + "\n");
            }

            counter++;
        }
        System.out.println("Avoided [" + duplicateCounter + "] duplicate entries");

        bufferedWriter.close();
        mandateBufferedWriter.close();
        mySqlDataAccess.closeConnection();
        System.out.println("Successfully migrated [" + counter
                + "] Subscribers\n\n");
    }

    @SuppressWarnings("unchecked")
    private String createMandate(String mandateId, String appId,
                                 String appName, String spId, String msisdn, String currentStatus, long createdDate, long lastChargedDate, long nextChargingDate) {


        JSONObject metaDataObject = new JSONObject();
        metaDataObject.put(MONGO_META_DATA_KEY, metaDataObject);

        JSONObject nextDateMap = new JSONObject();
        nextDateMap.put("$date", nextChargingDate);

        JSONObject scheduleObject = new JSONObject();
        //scheduleObject.put(CILENT_TRANSID, mandateId + "-1-0");
        //scheduleObject.put(PGW_STATUS_CODE, "success");
        //scheduleObject.put(PGW_TX_ID, null);

        //JSONObject lastCycleDate = new JSONObject();
        //lastCycleDate.put("$date", lastChargedDate);

        //JSONObject lastExecutionDate = new JSONObject();
        //lastExecutionDate.put("$date", lastChargedDate);


        //scheduleObject.put(LAST_CYCLE_DATE, lastCycleDate);
        //scheduleObject.put(LAST_EXECUTION_DATE, lastExecutionDate);
        scheduleObject.put(MONGO_RETRY_ATTEMPT_KEY, RETRY_ATTEMPT);
        //scheduleObject.put(MONGO_CHARGE_AMOUNT_KEY, CHARGE_AMOUNT);
        scheduleObject.put(MONGO_PHASE_KEY, PHASE);
        //scheduleObject.put(MONGO_CHARGING_SHORT_INSTRUCTIONS_KEY, null);
        scheduleObject.put(MONGO_LOADING_STATE_KEY, EXECUTION_STATE);
        scheduleObject.put(MONGO_NEXT_DATE_KEY, nextDateMap);
        //scheduleObject.put(MONGO_EXECUTION_COUNT_KEY, EXECUTION_COUNT);

        JSONObject headersObject = new JSONObject();
        headersObject.put(MONGO_SYSTEM_ID_KEY, SYSTEM_ID);
        headersObject.put(MONGO_MANDATE_ID_KEY, mandateId);
        headersObject.put(MONGO_CORRELATION_ID_KEY, mandateId);


        JSONObject startDateMap = new JSONObject();
        startDateMap.put("$date", createdDate);

        JSONObject amountObject = new JSONObject();
        amountObject.put(MONGO_VALUE_KEY, CHARGING_AMOUNT_VALUE);
        amountObject.put(MONGO_CURRENCY_CODE_KEY, CURRENCY_CODE);

        JSONObject chargingObject = new JSONObject();
        chargingObject.put(MONGO_AMOUNT_KEY, amountObject);
        chargingObject.put(MONGO_CATEGORY_KEY, CATEGORY);
        chargingObject.put(MONGO_MODEL_KEY, MODEL);
        chargingObject.put(MONGO_MERCHANT_ID_KEY, appId);
        chargingObject.put(MONGO_MSISDN_KEY, msisdn);
        chargingObject.put(MONGO_MERCHANT_NAME_KEY, appName);
        chargingObject.put(MONGO_TYPE_KEY, TYPE);
        chargingObject.put(MONGO_FROM_PAYMENT_INSTRUMENT_KEY,
                PAYMENT_INSTRUMENT);

        JSONObject appObject = new JSONObject();
        appObject.put(MONGO_ID_KEY, appId);
        appObject.put(MONGO_STATE_KEY, "enable");

        JSONObject spObject = new JSONObject();
        spObject.put(MONGO_ID_KEY, spId);
        spObject.put(MONGO_STATE_KEY, "enable");

        JSONObject controlDataObject = new JSONObject();
        controlDataObject.put(MONGO_APP__KEY, appObject);
        controlDataObject.put(MONGO_SP_KEY, spObject);

        JSONObject mandateObject = new JSONObject();
        mandateObject.put(MONGO__ID_KEY, mandateId);
        mandateObject.put(MONGO_SCHEDULE_KEY, scheduleObject);
        mandateObject.put(MONGO_FREQUENCY_KEY, FREQUENCY);
        mandateObject.put(MONGO_STATE_KEY, "fresh");
        //mandateObject.put(MONGO_END_DATE_KEY, null);
        mandateObject.put(MONGO_START_CRITERIA_KEY, START_CRITERIA);
        mandateObject.put(MONGO_HEADERS_KEY, headersObject);
        mandateObject.put(MONGO_START_DATE_KEY, startDateMap);
        mandateObject.put(MONGO_CHARGING_KEY, chargingObject);
        mandateObject.put(MONGO_DESCRIPTION_KEY, DESCRIPTION);
        mandateObject.put(MONGO_CONTROL_DATA_KEY, controlDataObject);
        return mandateObject.toJSONString();
    }
}