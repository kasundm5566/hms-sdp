package com.hms.appzone.migration.impl;

import com.hms.appzone.migration.IMigration;
import com.hms.appzone.migration.db.MySqlDataAccess;
import com.hms.appzone.migration.util.PropertyLoader;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;

/**
 * <p>
 * Reporting Service Provider Migration Implementation.
 * </p>
 * 
 * @author dulith
 */
public class ReportingServiceProviderMigration implements IMigration {

	// MySQL namings
	public static String SP_ID = "sp_id";
	public static String SP_NAME = "sp_name";
	public static String COOPERATE_USER_ID = "cooperate_user_id";
	public static String APP_ID = "app_id";
	public static String APP_NAME = "app_name";
	public static String APP_CATEGORY = "sdp";
	public static String REVENUE_SHARE_TYPE = "percentageFromMonthlyReven";
    private static final String MYSQL_SP_INT_ID = "sp.id";
	// static values
	public static final long REVENUE_SHARE_PERCENTAGE = 70;
	private static final String OUTPUT_FILE_NAME = "service_provider_info.sql";
	public static int COUNTER = 0;

	/**
     * {@inheritDoc}
     */
	@SuppressWarnings("resource")
	@Override
	public void convert(HashMap<String, String> applicationDetailsMap) throws Exception {
		FileWriter fileWriter = new FileWriter(PropertyLoader.get("output.dir")
				+ OUTPUT_FILE_NAME);
		MySqlDataAccess mySqlDataAccess = new MySqlDataAccess();

		  String sql =  "SELECT      sp.sp_id, " +
                                    "sp.sp_id AS cooperate_user_id, " +
                                    "sp_user_name.username AS sp_name, " +
                                    "app.app_id, " +
                                    "app.app_name, " + "app.current_state, " + "app.created_time, "+
                                    "sp.id " +
                        "FROM        sp " +
                                    "INNER JOIN sp_user_name " +
                                    "ON sp_user_name.sp_id = sp.id " +
                                    "INNER JOIN app " +
                                    "ON app.sp = sp.id " +
                        "WHERE       app.app_id LIKE 'PD%' " +
                                    "AND app.current_state LIKE '%ACTIVE%' ";

		ResultSet rs = mySqlDataAccess.executeQuery(sql);

		String app_category = APP_CATEGORY;
		String revenue_share_type = REVENUE_SHARE_TYPE;
		double revenue_share_percentage = REVENUE_SHARE_PERCENTAGE;

		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);


			bufferedWriter.write("INSERT INTO service_provider_info " +
                                                "(sp_id, " +
                                                "sp_name, " +
                                                "corporate_user_id, " +
                                                "app_id, " +
                                                "app_name, " +
                                                "app_catogory, " +
                                                "revenue_share_type, " +
                                                "revenue_share_percentage, " + "app_state, "+ "created_time " +") " +
                                        "VALUES " );
        while (rs.next()) {
            String id = rs.getString(MYSQL_SP_INT_ID);
            String sp_id = rs.getString(SP_ID);
            String sp_name = rs.getString(SP_NAME);
            String cooperate_user_id = rs.getString(COOPERATE_USER_ID);
            String app_id = rs.getString(APP_ID);
            String app_name = rs.getString(APP_NAME);
            String current_state = rs.getString("current_state");

            Date createdDate = new Date(rs.getTimestamp("created_time").getTime());
            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String formattedDate = sdf.format(createdDate);

            if (applicationDetailsMap.containsKey(app_name)) {
                app_name = applicationDetailsMap.get(app_name);
            } else {
                System.out.println("ERROR : Application name not found [" + app_name + "]");
            }

            current_state = "active-production";

           if (isValidUser(id , app_id)) {
                if (COUNTER == 0) {

                    bufferedWriter.write("('" + sp_id + "', " +

                            "'" + sp_name + "', " +
                            "'" + cooperate_user_id + "', " +
                            "'" + app_id + "', " +
                            "'" + app_name + "', " +
                            "'" + app_category + "', " +
                            "'" + revenue_share_type + "', " +
                            "" + revenue_share_percentage + ",'" + current_state + "', "  + " '" + formattedDate + "' " + "" + ") ");
                } else {
                    bufferedWriter.write(" ,('" + sp_id + "', " +

                            "'" + sp_name + "', " +
                            "'" + cooperate_user_id + "', " +
                            "'" + app_id + "', " +
                            "'" + app_name + "', " +
                            "'" + app_category + "', " +
                            "'" + revenue_share_type + "', " +
                            "" + revenue_share_percentage +  ",'" + current_state + "', "  + " '" + formattedDate + "' " + "" + ") ");

                }
                bufferedWriter.write("\n");

                COUNTER++;
           }
        }
        bufferedWriter.write(" ; ");
        bufferedWriter.flush();
        mySqlDataAccess.closeConnection();
        System.out.println("Reporting Service Providers " + COUNTER
                + " Migrated !");
    }

    private boolean isValidUser(final String id , String app_id) {

        if (app_id.equals("PD_ET_h0919") || id.equals("136") || id.equals("139")) {

            return false;
        }

        return true;
    }
}
