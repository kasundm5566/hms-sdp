package com.hms.appzone.migration.impl;

import com.hms.appzone.migration.IMigration;
import com.hms.appzone.migration.db.MySqlDataAccess;
import com.hms.appzone.migration.util.PropertyLoader;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

/**
 * <p>
 * Common Admins Migration Implementation.
 * </p>
 *
 * @author dulith
 */

public class CommonAdminsMigration implements IMigration {

    // Static Values
    private static final String USER_ID = "sp_id";
    private static final String MYSQL_SP_INT_ID = "sp.id";
    private static final String USERNAME = "username";
    private static final int USERGROUP = 3;
    private static final int EMAIL_VERIFIED = 1;
    private static final int ENABLED = 1;
    private static final int MSISDN_VERIFIED = 1;
    private static final String DTYPE = "CorporateUser";
    private static final int CORPORATE_USER = 1;
    private static final String TYPE = "CORPORATE";
    private static final String DOMAIN = "internal";
    private static final String STATUS = "ACTIVE";
    private static final String PASSWORD = "44bc7b417faef5c8125581ebaf8b8e1e";    //test123#
    private static final boolean passwordSelection = Boolean.parseBoolean(PropertyLoader.get("default.password.reader"));
    private static final String EMAIL = "email";
    private static final String MOBILE_NUMBER = "mobile.number";
    private static final String MSISDN_VERIFICATION_CODE = "msisdn.verification.code";
    private static final String CONTACT_PERSON_MAIL = "contact.person.email";
    private static final String CONTACT_PERSON_NAME = "contact.person.name";
    private static final String CONTACT_PERSON_PHONE = "contact.person.phone";
    private static final String OUTPUT_FILE_NAME = "user.sql";
    public static int COUNTER = 0;
    private static String ENCRYPTED_PASSWORD = null;
    private static String DYNAMIC_PASSWORD = null;
    private static String username = "";

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings({"resource" })
    @Override
    public void convert(HashMap<String, String> applicationDetailsMap) throws Exception {

        FileWriter fileWriter = new FileWriter(PropertyLoader.get("output.dir")
                + OUTPUT_FILE_NAME);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        FileWriter fileWriterUserDetails = new FileWriter(PropertyLoader.get("output.dir")
                + "UserDetailsAppStore");
        BufferedWriter bufferedWriterUserDetails = new BufferedWriter(fileWriterUserDetails);

        System.out.println("Migrating users for common admin . . .");
        MySqlDataAccess mySqlDataAccess = new MySqlDataAccess();
        PropertyLoader propertyLoader = new PropertyLoader();
        String sql = "SELECT DISTINCT     sp.id, " +
                "sp.sp_id, " +
                "sp_user_name.username " +
                "FROM               sp " +
                "INNER JOIN app " +
                "ON app.sp = sp.id " +
                "INNER JOIN sp_user_name " +
                "ON sp.id = sp_user_name.sp_id " +
                "WHERE              app_id LIKE 'PD%'" +
                "AND current_state LIKE '%ACTIVE%' " +
                "GROUP BY           sp_id;";

        ResultSet rs = mySqlDataAccess.executeQuery(sql);
        bufferedWriter.write(
                "INSERT INTO  user " +
                        "(user_id, " +
                        "username, " +
                        "password, " +
                        "user_group, " +
                        "email_verified, " +
                        "enabled, " +
                        "msisdn_verified, " +
                        "dtype, " +
                        "corporate_user, " +
                        "status, " +
                        "type, " +
                        "email, "+
                        "mobile_no, "+
                        "msisdn_verification_code, "+
                        "contact_person_email, "+
                        "contact_person_name, "+
                        "contact_person_phone, "+
                        "domain) " +

                        "VALUES    " );

        while (rs.next()) {
            String id = rs.getString(MYSQL_SP_INT_ID);
            String userId = rs.getString(USER_ID);
            username = rs.getString(USERNAME);

            if(passwordSelection ==true) {

                ENCRYPTED_PASSWORD =  PASSWORD;

            }
            else {

                // Refer to method randomPasswordGenerator()
                CommonAdminsMigration commonAdminsMigration = new CommonAdminsMigration();
                DYNAMIC_PASSWORD = commonAdminsMigration.randomPasswordGenerator();
                ENCRYPTED_PASSWORD =  commonAdminsMigration.convertToMd5(DYNAMIC_PASSWORD);
            }

            if (isValidUser(id))

            {
                if (COUNTER == 0) {
                    bufferedWriter.write(
                            "('" + userId + "', " +
                                    "'" + username + "', " +
                                    "'" + ENCRYPTED_PASSWORD + "', " +
                                    "" + USERGROUP + ", " +
                                    "" + EMAIL_VERIFIED + ", " +
                                    "" + ENABLED + ", " +
                                    "" + MSISDN_VERIFIED + ", " +
                                    "'" + DTYPE + "', " +
                                    "" + CORPORATE_USER + ", " +
                                    "'" + STATUS + "', " +
                                    "'" + TYPE + "', " +
                                    "'" + propertyLoader.get(EMAIL) + "', " +
                                    "'" + propertyLoader.get(MOBILE_NUMBER) + "', " +
                                    "'" + propertyLoader.get(MSISDN_VERIFICATION_CODE) + "', " +
                                    "'" + propertyLoader.get(CONTACT_PERSON_MAIL) + "', " +
                                    "'" + propertyLoader.get(CONTACT_PERSON_NAME) + "', " +
                                    "'" + propertyLoader.get(CONTACT_PERSON_PHONE) + "', " +
                                    "'" + DOMAIN + "')  ");
                } else {
                    bufferedWriter.write(
                            " ,('" + userId + "', " +
                                    "'" + username + "', " +
                                    "'" + ENCRYPTED_PASSWORD + "', " +
                                    "" + USERGROUP + ", " +
                                    "" + EMAIL_VERIFIED + ", " +
                                    "" + ENABLED + ", " +
                                    "" + MSISDN_VERIFIED + ", " +
                                    "'" + DTYPE + "', " +
                                    "" + CORPORATE_USER + ", " +
                                    "'" + STATUS + "', " +
                                    "'" + TYPE + "', " +
                                    "'" + propertyLoader.get(EMAIL) + "', " +
                                    "'" + propertyLoader.get(MOBILE_NUMBER) + "', " +
                                    "'" + propertyLoader.get(MSISDN_VERIFICATION_CODE) + "', " +
                                    "'" + propertyLoader.get(CONTACT_PERSON_MAIL) + "', " +
                                    "'" + propertyLoader.get(CONTACT_PERSON_NAME) + "', " +
                                    "'" + propertyLoader.get(CONTACT_PERSON_PHONE) + "', " +
                                    "'" + DOMAIN + "' ) ");
                }
                bufferedWriterUserDetails.write(username+"\t"+DYNAMIC_PASSWORD);
                bufferedWriterUserDetails.write("\n");
                bufferedWriterUserDetails.flush();
                bufferedWriter.write("\n");
                COUNTER++;
            }
        }

        bufferedWriter.write(" ;");

        bufferedWriter.flush();
        mySqlDataAccess.closeConnection();
        System.out.println("Successfully migrated [" + COUNTER + "] users for common admin");
    }

    private boolean isValidUser(final String id) {

        return !(id.equals("47") || id.equals("44") || id.equals("80") || id.equals("136") || id.equals("139"));

    }

    private String randomPasswordGenerator() throws IOException, NoSuchAlgorithmException {
        Random randomGenerator = new Random();

        int randomInt1 = 0;
        int randomInt2 = 0;
        char randomChar1 = '0';
        char randomChar2 = '0';

        Scanner keyboard = new Scanner(System.in);

        Random genRandom = new Random();
        randomInt1 = genRandom.nextInt(10);
        randomInt2 = genRandom.nextInt(10);

        switch (randomInt2) {

            case 10 : randomChar2 = '!';  break;
            case 9 : randomChar2 = '@';  break;
            case 8 : randomChar2 = '#';  break;
            case 7 : randomChar2 = '$';  break;
            case 6 : randomChar2 = '%';  break;
            case 5 : randomChar2 = '&';  break;
            case 4 : randomChar2 = '*';  break;
            case 3 : randomChar2 = '(';  break;
            case 2 : randomChar2 = '+';  break;
            case 1 : randomChar2 = '-';  break;
            case 0 : randomChar2 = '/'; break;

        }
        switch (randomInt1) {

            case 10 : randomChar1 = '!';  break;
            case 9 : randomChar1 = '@';  break;
            case 8 : randomChar1 = '#';  break;
            case 7 : randomChar1 = '$';  break;
            case 6 : randomChar1 = '%';  break;
            case 5 : randomChar1 = '&';  break;
            case 4 : randomChar1 = '*';  break;
            case 3 : randomChar1 = '(';  break;
            case 2 : randomChar1 = '+';  break;
            case 1 : randomChar1 = '-';  break;
            case 0 : randomChar1 = '/'; break;

        }
        return (username+""+randomInt1+""+randomInt2+""+randomChar1+""+randomChar2);
    }

    public String convertToMd5(String password) throws NoSuchAlgorithmException {

        String md5 = null;
        if(null == password) return null;
        try {
            //Create MessageDigest object for MD5
            MessageDigest digest = MessageDigest.getInstance("MD5");
            //Update input string in message digest
            digest.update(password.getBytes(), 0, password.length());
            //Converts message digest value in base 16 (hex)
            md5 = new BigInteger(1, digest.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return md5;
    }
}
