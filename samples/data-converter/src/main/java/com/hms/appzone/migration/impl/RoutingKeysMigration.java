package com.hms.appzone.migration.impl;

import com.hms.appzone.migration.IMigration;
import com.hms.appzone.migration.db.MySqlDataAccess;
import com.hms.appzone.migration.util.PropertyLoader;
import org.json.simple.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.util.HashMap;

/**
 * <p>
 * Routing Keys Migration Implementation.
 * </p>
 * 
 * @author dulith
 */
public class RoutingKeysMigration implements IMigration {

	// Mongo Fields
	// Mongo Collection name : routing_keys
	public static final String MONGO_SP_ID = "sp-id";
	public static final String MONGO_OPERATOR = "operator";
	public static final String MONGO_SHORT_CODE = "shortcode";
	public static final String MONGO_KEYWORD = "keyword";
	public static final String MONGO_APP_ID = "app-id";
	public static final String MONGO_EXCLUSIVE = "exclusive";
	public static final String MONGO_NCS_TYPE = "ncs-type";
	public static final String MONGO_LAST_UPDATED_TIME = "last-updated-time";

	// MySQL table name = routing_keys
	// MySQL column names are mentioned below....
	public static final String NCS_TYPE = "ncs_type";
	public static final String KEYWORD = "keyword";
	public static final String SP_ID = "sp_id";
	public static final String APP_ID = "app_id";
	public static final String LAST_UPDATED_TIME = "last_modified_time";

	// Static fields
	public static final String OPERATOR = "etisalat";
	public static final String SHORT_CODE = "4499";
	public static final boolean EXCLUSIVE = false;
	private static final String OUTPUT_FILE_NAME = "routing_keys.json";

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings({ "unchecked" })
	@Override
	public void convert(HashMap<String, String> applicationDetailsMap) throws Exception {
		MySqlDataAccess mySqlDataAccess = new MySqlDataAccess();

		ResultSet resultSet = mySqlDataAccess.executeQuery(
				"SELECT routing_key.keyword, " +
                        "routing_key.last_modified_time, " +
                        "app.app_id, " +
                        "app.sp, " +
                        "sp.sp_id, " +
                        "ncs.ncs_type, " +
                        "app.current_state " +
                  "FROM routing_key routing_key " +
                        "INNER JOIN app app on routing_key.active_app_id=app.app_id " +
                        "INNER JOIN sp sp on app.sp=sp.id " +
                        "INNER JOIN ncs ncs on ncs.id=routing_key.ncs " +
                 "WHERE app.app_id LIKE 'PD%' " +
                        "AND app.current_state='ACTIVE';");
		 
		File file2 = new File(PropertyLoader.get("output.dir")
				+ OUTPUT_FILE_NAME);

		FileWriter fileWriter2 = new FileWriter(file2.getAbsoluteFile());
		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter2);
		System.out.println("\nMigrating routing keys . . .");

		int counter = 0;
		while (resultSet.next()) {

			String ncsType = resultSet.getString(NCS_TYPE);
			String keyword = resultSet.getString(KEYWORD).toLowerCase();
			String spId = resultSet.getString(SP_ID);
			String appId = resultSet.getString(APP_ID);
			long lastUpdatedTime = resultSet.getDate(LAST_UPDATED_TIME)
					.getTime();
			
			JSONObject lastUpdatedDate = new JSONObject();
			lastUpdatedDate.put("$date", lastUpdatedTime);

			JSONObject jsonObject = new JSONObject();
			jsonObject.put(MONGO_NCS_TYPE, ncsType.toLowerCase());
			jsonObject.put(MONGO_OPERATOR, OPERATOR);
			jsonObject.put(MONGO_SHORT_CODE, SHORT_CODE);
			jsonObject.put(MONGO_KEYWORD, keyword);
			jsonObject.put(MONGO_EXCLUSIVE, EXCLUSIVE);
			jsonObject.put(MONGO_SP_ID, spId);
			jsonObject.put(MONGO_APP_ID, appId);
			jsonObject.put(MONGO_LAST_UPDATED_TIME, lastUpdatedDate);

			bufferedWriter.write(jsonObject.toJSONString() + "\n");
			counter++;
		}

		bufferedWriter.close();
		mySqlDataAccess.closeConnection();
		System.out.println("Successfully migrated [" + counter
				+ "] routing keys.");

	}

}
