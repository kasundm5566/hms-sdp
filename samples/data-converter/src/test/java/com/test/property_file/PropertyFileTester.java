package com.test.property_file;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * Created with IntelliJ IDEA.
 * User: dulith
 * Date: 9/27/13
 * Time: 10:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class PropertyFileTester {

    public static void main(String[] args) throws IOException {

        Properties  properties = new Properties();


       properties.load(new FileInputStream("/home/dulith/sources/sdp/samples/db-converter/src/test/resources/DbConnection.properties"));
       System.out.println("Checking Property File Reader Class");
       System.out.println(properties.getProperty("DB_URL"));
       System.out.println("**************************");
       System.out.println(properties.getProperty("USER"));
       System.out.println("**************************");
       System.out.println(properties.getProperty("PASS"));
       System.out.println("**************************");
    }


}


