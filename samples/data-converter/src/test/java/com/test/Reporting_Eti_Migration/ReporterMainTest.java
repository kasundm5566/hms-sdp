package com.test.Reporting_Eti_Migration;

import com.hms.appzone.migration.util.PropertyLoader;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;

/**
 * Created with IntelliJ IDEA.
 * User: dulith
 * Date: 10/16/13
 * Time: 10:23 AM
 * To change this template use File | Settings | File Templates.
 */
public class ReporterMainTest {

    public static String SP_ID = "sp_id";
    public static String SP_NAME = "sp_name";
    public static String COOPERATE_USER_ID = "cooperate_user_id";
    public static String APP_ID = "app_id";
    public static String APP_NAME = "app_name";
    public static String APP_CATEGORY = "sdp";
    public static String REVENUE_SHARE_TYPE = "percentageFromMonthlyReven";
    public static long REVENUE_SHARE_PERCENTAGE = 70;

    public static int COUNTER = 0;

    public  void main(String[] args) throws IOException {

        Connection conn = null;
        Statement stmt = null;

        FileWriter fileWriter = new FileWriter(PropertyLoader.get("output.dir") + "ReportingServiceProviderMigration.sql");
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        try {

            Class.forName("com.mysql.jdbc.Driver");

            conn = DriverManager.getConnection(PropertyLoader.get("DB_URL"), PropertyLoader.get("USER"), PropertyLoader.get("PASS"));

            stmt = conn.createStatement();
            String sql;
            sql = "select sp.sp_id , sp.sp_id as cooperate_user_id , sp_user_name.username as sp_name , app.app_id, app.app_name from sp left outer join sp_user_name on sp_user_name.sp_id = sp.id left outer join app on app.sp = sp.id left outer join ncs_sla on ncs_sla.app_id = app.id where app.app_id like 'PD%' and app.current_state like '%ACTIVE%' and ncs_sla.current_ncs_sla_state = 'ACTIVE_PD';";
            ResultSet rs = stmt.executeQuery(sql);

            String app_category = APP_CATEGORY;
            String revenue_share_type =REVENUE_SHARE_TYPE;
            double revenue_share_percentage = REVENUE_SHARE_PERCENTAGE;


            while (rs.next()) {


                String sp_id = rs.getString(SP_ID);
                String sp_name = rs.getString(SP_NAME);
                String cooperate_user_id = rs.getString(COOPERATE_USER_ID);
                String app_id = rs.getString(APP_ID);
                String app_name = rs.getString(APP_NAME);


                bufferedWriter.write("INSERT INTO user (sp_id, sp_name , cooperate_user_id , app_id , app_name, app_catogory,revenue_share_type, revenue_share_percentage) VALUES( '" + sp_id + "'" + "," + " '" + sp_name + "'" + "," + " '" + cooperate_user_id + "', '" + app_id + "','" + app_name + "','" + app_category + "','" + revenue_share_type + "', " + revenue_share_percentage + ");" + "\n");

                bufferedWriter.flush();

                COUNTER++;

            }

            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {

            se.printStackTrace();
        } catch (Exception e) {

            e.printStackTrace();
        } finally {

            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }

        System.out.println("Reporting Service Providers " + COUNTER + " Migrated !");

    }

}
