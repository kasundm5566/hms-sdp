package com.test.AppMigrationCheckerTest;

import com.hms.appzone.migration.util.PropertyLoader;
import com.mongodb.*;

import java.io.*;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: dulith
 * Date: 10/7/13
 * Time: 4:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class App {

    Connection conn = null;
    Statement stmt = null;

    int counter=0;  int incrementer=1;  int mongo_counter=1;

    List <String> dataReadFromMysql = new ArrayList<String>();

    List<DBObject> dataReadFromMongo = new ArrayList<DBObject>();

    public static void main(String[] args) throws Exception {


        App app = new App();

        app.executeQuery("select app_id from app where app_id like 'PD%' and current_state like '%ACTIVE%'");
        app.readFileAndStoreInList();
        app.readDataFromMongo();
        app.getIntersection();
    }

         public ResultSet executeQuery(String queryStatement) throws Exception {

        ResultSet rs;
        try {
            conn = DriverManager.getConnection(PropertyLoader.get("DB_URL"), PropertyLoader.get("USER"), PropertyLoader.get("PASS"));


            stmt = conn.createStatement();
            String sql;

            sql = queryStatement;
            rs = stmt.executeQuery(sql);

            while(rs.next()) {

                counter++;

            }

            System.out.println("Total Active + PD Application in table : APP  is :- " + counter);

        } finally {


        }
        return rs;
    }


    public void readFileAndStoreInList() throws IOException {


        Scanner scanner = null;

        try {
            scanner = new Scanner(new BufferedReader(new FileReader("/home/dulith/Desktop/app_id_total_mysql.txt")));

            while (scanner.hasNext()) {

                dataReadFromMysql.add(scanner.next());
                incrementer++;

            }
        } finally {
            if (scanner != null) {
                scanner.close();
            }

            System.out.println("Successfully added to the list");
        }


    }

    public void readDataFromMongo() throws UnknownHostException {

        MongoClient mongoClient = new MongoClient();

        BasicDBObject fields = new BasicDBObject();

        DB db = mongoClient.getDB("dulith_test_etisalat");

        DBCollection table = db.getCollection("app");

       BasicDBObject allQuery = new BasicDBObject();


        DBCursor cursor = table.find(allQuery , fields);

        while (cursor.hasNext()) {

            dataReadFromMongo.add(cursor.next());

            mongo_counter++;
        }

        System.out.println("Successfully added to the Mongo List ");
    }


    public void getIntersection() {

        boolean x = dataReadFromMysql.removeAll(dataReadFromMongo);

        System.out.println(x);

        System.out.println("Mongo List : " + dataReadFromMongo);

        for(int i =0; i < dataReadFromMongo.size(); i++) {

            System.out.println(dataReadFromMongo.get(i).get("app-id"));

        }


    }

}
