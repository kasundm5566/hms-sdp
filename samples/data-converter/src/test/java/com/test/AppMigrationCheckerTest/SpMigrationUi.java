package com.test.AppMigrationCheckerTest;

import com.hms.appzone.migration.util.PropertyLoader;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: dulith
 * Date: 9/16/13
 * Time: 10:58 AM
 * To change this template use File | Settings | File Templates.
 */
public class SpMigrationUi {

    // Static Values
    private static final String USER_ID = "sp_id";
    private static final String USERNAME = "username";
    private static final int USERGROUP = 3;
    private static final int EMAIL_VERIFIED = 1;
    private static final int ENABLED = 1;
    private static final int MSISDN_VERIFIED = 1;
    private static final String DTYPE = "CorporateUser";
    private static final int CORPORATE_USER = 1;
    private static final String TYPE = "CORPORATE";
    private static final String DOMAIN = "internal";
    private static final String STATUS = "ACTIVE";
    private static final String PASSWORD = "44bc7b417faef5c8125581ebaf8b8e1e";    //test123#
    public static int COUNTER = 0;

    public static void main(String[] args) throws IOException {

        SpMigrationUi spMigrationUi = new SpMigrationUi();
        execute();

    }

    public static void execute() throws IOException {

        FileWriter fileWriter = new FileWriter(PropertyLoader.get("output.dir") + "SpMigrationUi.sql");
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        Connection conn = null;
        java.sql.Statement stmt = null;

        try {

            Class.forName("com.mysql.jdbc.Driver");

          //  Properties properties = new Properties();
         //   properties.load(new FileInputStream("configuration.properties"));

            PropertyLoader propertyLoader = new PropertyLoader();

            conn = DriverManager.getConnection(propertyLoader.get("mysql.url"), propertyLoader.get("mysql.username"), propertyLoader.get("mysql.password"));

            stmt = conn.createStatement();
            String sql;

            sql = "select sp.sp_id, sp_user_name.username from sp join sp_user_name on sp.id = sp_user_name.sp_id;";

            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {

                String user_id = rs.getString(USER_ID);
                String username = rs.getString(USERNAME);
                String password = PASSWORD;
                int userGroup = USERGROUP;
                int emailVerified = EMAIL_VERIFIED;
                int enabled = ENABLED;
                int msisdnVerified = MSISDN_VERIFIED;
                String dtype = DTYPE;
                int corporateUser = CORPORATE_USER;
                String type = TYPE;
                String domain = DOMAIN;
                String status = STATUS;

                bufferedWriter.write("INSERT INTO user (user_id, username , password , user_group , email_verified, enabled,msisdn_verified,dtype,corporate_user, status , type, domain ) VALUES( '" + user_id + "'" + "," + " '" + username + "'" + "," + " '" + password + "', " + userGroup + "," + emailVerified + "," + enabled + "," + msisdnVerified + ", '" + dtype + "'," + corporateUser + ",'" + status + "' , '" + type + "','" + domain + "'); " + "\n");

                bufferedWriter.flush();

                System.out.println(COUNTER + " -- " + "INSERT INTO user (user_id, username , password , user_group , email_verified, enabled,msisdn_verified,dtype,corporate_user, status ) VALUES( ' " + user_id + " ' " + "," + " ' " + username + " '" + "," + " ' " + password + " ', " + userGroup + "," + emailVerified + "," + enabled + "," + msisdnVerified + ", '" + dtype + "','" + status + "'  );");

                COUNTER++;
            }

            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {

            se.printStackTrace();
        } catch (Exception e) {

            e.printStackTrace();
        } finally {

            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }

    }


}
