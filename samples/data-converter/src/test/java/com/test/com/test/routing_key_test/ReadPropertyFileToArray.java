package com.test.com.test.routing_key_test;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: dulith
 * Date: 9/5/13
 * Time: 9:36 AM
 * To change this template use File | Settings | File Templates.
 */
public class ReadPropertyFileToArray {

    static Properties properties = new Properties();

    public static List<List<String>> insertSplitWords = new ArrayList<List<String>>();

    static {

        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream stream = loader.getResourceAsStream("sp_table_mapping.properties");

        try {

            properties.load(stream);

        } catch (IOException e) {

        }
    }

    public static void main(String[] args) {

        String wordsSplitted[] = null;

        System.out.println("Read Property File Started !");

        Set<String> propertyNames = properties.stringPropertyNames();

        // To iterate through the Set which holds the keys.

        Iterator<String> iterateThePropertyFile = propertyNames.iterator();

        for (int i = 0; i < properties.size(); i++) {


            List<String> list = Arrays.asList(properties.getProperty(iterateThePropertyFile.next()).split("\\."));


            System.out.println(list.get(1));

        }

    }
}
