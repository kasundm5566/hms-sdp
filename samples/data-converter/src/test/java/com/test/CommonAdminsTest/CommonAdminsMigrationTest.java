package com.test.CommonAdminsTest;

import com.hms.appzone.migration.IMigration;
import com.hms.appzone.migration.db.MySqlDataAccess;
import com.hms.appzone.migration.util.PropertyLoader;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.util.HashMap;

/**
 * <p>
 * Common Admins Migration Implementation.
 * </p>
 *
 * @author dulith
 */

public class CommonAdminsMigrationTest implements IMigration {

    // Static Values
    private static final String USER_ID = "sp_id";
    private static final String USERNAME = "username";
    private static final String WS_PASSWORD = "ws_password";
    private static final int USERGROUP = 3;
    private static final int EMAIL_VERIFIED = 1;
    private static final int ENABLED = 1;
    private static final int MSISDN_VERIFIED = 1;
    private static final String DTYPE = "CorporateUser";
    private static final int CORPORATE_USER = 1;
    private static final String TYPE = "CORPORATE";
    private static final String DOMAIN = "internal";
    private static final String STATUS = "ACTIVE";
    private static final String PASSWORD = "44bc7b417faef5c8125581ebaf8b8e1e";    //test123#
    private static  String DYNAMIC_PASSWORD= null;
    private static final boolean passwordSelection = Boolean.parseBoolean(PropertyLoader.get("default.password.reader"));
    public static int COUNTER = 0;
    private static final String EMAIL = "email";
    private static final String MOBILE_NUMBER = "mobile.number";
    private static final String MSISDN_VERIFICATION_CODE = "msisdn.verification.code";
    private static final String CONTACT_PERSON_MAIL = "contact.person.email";
    private static final String CONTACT_PERSON_NAME = "contact.person.name";
    private static final String CONTACT_PERSON_PHONE= "contact.person.phone";

    private static final String OUTPUT_FILE_NAME = "CommonAdminsMigration.sql";

    /**
     * {@inheritDoc}
     */
    public static void main(String[] args) throws Exception {
        CommonAdminsMigrationTest commonAdminsMigrationTest = new CommonAdminsMigrationTest();
        commonAdminsMigrationTest.convert(new HashMap<String, String>());
    }

    @SuppressWarnings({"resource" })
    @Override
    public void convert(HashMap<String, String> applicationDetailsMap) throws Exception {

        FileWriter fileWriter = new FileWriter(PropertyLoader.get("output.dir")
                + OUTPUT_FILE_NAME);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        System.out.println("Migrating users for common admin . . .");
        MySqlDataAccess mySqlDataAccess = new MySqlDataAccess();
        PropertyLoader propertyLoader = new PropertyLoader();
        String sql = "";

        ResultSet rs = mySqlDataAccess.executeQuery(sql);
        while (rs.next()) {
            String userId = rs.getString(USER_ID);
            String username = rs.getString(USERNAME);

            if (passwordSelection == true) {

                DYNAMIC_PASSWORD = PASSWORD;
            } else {
                DYNAMIC_PASSWORD = rs.getString(WS_PASSWORD);
            }

            bufferedWriter.write(
                    "INSERT INTO user " +
                            "(user_id, " +
                            "username, " +
                            "password, " +
                            "user_group, " +
                            "email_verified, " +
                            "enabled, " +
                            "msisdn_verified, " +
                            "dtype, " +
                            "corporate_user, " +
                            "status, " +
                            "type, " +
                            "email, "+
                            "mobile_no, "+
                            "msisdn_verification_code, "+
                            "contact_person_email, "+
                            "contact_person_name, "+
                            "contact_person_phone, "+
                            "domain) " +
                            "VALUES " +
                            "('"+userId+"', " +
                            "'"+username+"', " +
                            "'"+DYNAMIC_PASSWORD+"', " +
                            ""+USERGROUP+", " +
                            ""+EMAIL_VERIFIED+", " +
                            ""+ENABLED+", " +
                            ""+MSISDN_VERIFIED+", " +
                            "'"+DTYPE+" ', " +
                            ""+CORPORATE_USER+", " +
                            "'"+STATUS+"', " +
                            "'"+TYPE+"', " +
                            "'"+propertyLoader.get(EMAIL)+"', "+
                            "'"+propertyLoader.get(MOBILE_NUMBER)+"', "+
                            "'"+propertyLoader.get(MSISDN_VERIFICATION_CODE)+"', "+
                            "'"+propertyLoader.get(CONTACT_PERSON_MAIL)+"', "+
                            "'"+propertyLoader.get(CONTACT_PERSON_NAME)+"', "+
                            "'"+propertyLoader.get(CONTACT_PERSON_PHONE)+"', "+
                            "'"+DOMAIN+"'); ");


            bufferedWriter.write("\n");
            bufferedWriter.flush();
            COUNTER++;
        }

        mySqlDataAccess.closeConnection();
        System.out.println("Successfully migrated [" + COUNTER + "] users for common admin");
    }

}
