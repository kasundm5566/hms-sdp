package com.test.mongo;

import java.net.UnknownHostException;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.Mongo;
import com.mongodb.MongoException;

/**
 * ShowResults class, helps user to check values that are inserted into their
 * mentioned database. In order to get use of this class, DB and Collection
 * should be changed according to your database and collection.
 * 
 * @author dulith
 * @version 1.0
 */

public class ShowResults {

	public static void main(String[] args) {
		try {

			Mongo mongo = new Mongo("localhost", 27017);
			DB db = mongo.getDB("Dulith_Test");
			DBCollection collection = db.getCollection("user");

			// convert JSON to DBObject directly

			DBCursor cursorDoc = collection.find();

			System.out.println("-*-*-*-*-*-*-*-*-*-*-*-*");
			System.out.println("Mongo Resutls :");

			while (cursorDoc.hasNext()) {
				System.out.println(cursorDoc.next());
			}
			System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*");

		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (MongoException e) {
			e.printStackTrace();
		}
	}

}
