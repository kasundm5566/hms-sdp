#! /bin/bash
# MigrationScript.sh file manipulates the count of MySQL and Mongodb data entries.
# App Migration - MySQL Verification

database_connection="mysql --user=user --pass=password --database=aventura_20100907 --host=$db_ip --skip-column-names  -e"

subscription_based_applications=`$database_connection "select count(*) from app  join sp on sp.id=app.sp join subscriber_registration_sla on  app.subs_reg_sla = subscriber_registration_sla.id join routing_key on app.app_id = routing_key.active_app_id   WHERE app.app_id LIKE 'PD%' AND app.current_state='ACTIVE' AND subs_reg_sla_type='SUBS_REG_SLA';"`

on_demand_based_applications=`$database_connection "select count(*) from app  join sp on sp.id=app.sp join subscriber_registration_sla on  app.subs_reg_sla = subscriber_registration_sla.id join routing_key on app.app_id = routing_key.active_app_id   WHERE app.app_id LIKE 'PD%' AND app.current_state='ACTIVE' AND subs_reg_sla_type='NULL_SUBS_REG_SLA';"`

echo "-----------------------"
echo "App Migration Details - MySQL :"
echo "------------------------------------------"
echo "Subscription Applications = "  $subscription_based_applications
echo "Non - Subscription Applications = " $on_demand_based_applications
echo "------------------------------------------"
echo

# Sp Migration - MySQL Verification

sp_migration_query=`$database_connection "SELECT COUNT(*) FROM sp"`

echo "-----------------------"
echo "Sp Migration Details - MySQL :"
echo "------------------------------------------"
echo "Migrated SPs = "  $sp_migration_query
echo "------------------------------------------"
echo 

# Ncs_sla Migration - MySQL Verification

ncs_sla_migration_query_subs_reg_sla=`$database_connection "select count(*) from ncs_sla left join app on ncs_sla.app_id = app.id left join sp_user_name on sp_user_name.sp_id = ncs_sla.sp_id left join subscriber_registration_sla on  app.subs_reg_sla = subscriber_registration_sla.id left join routing_key on app.app_id = routing_key.active_app_id   where app.app_id like '%PD%' AND app.current_state='ACTIVE' AND app.subs_reg_sla_type = 'SUBS_REG_SLA';"`

ncs_sla_migration_query_null_subs_reg_sla=`$database_connection "select count(*) from ncs_sla left join app on ncs_sla.app_id = app.id left join sp_user_name on sp_user_name.sp_id = ncs_sla.sp_id left join subscriber_registration_sla on  app.subs_reg_sla = subscriber_registration_sla.id left join routing_key on app.app_id = routing_key.active_app_id   where app.app_id like '%PD%' AND app.current_state='ACTIVE' AND app.subs_reg_sla_type = 'NULL_SUBS_REG_SLA';"`

echo "-----------------------"
echo "Ncs_sla Migration Details - MySQL :"
echo "------------------------------------------"
echo "ncs_sla amount (Subscription)  : = "  $ncs_sla_migration_query_subs_reg_sla
echo "ncs_sla amount (Non - Subscription)  : = "  $ncs_sla_migration_query_null_subs_reg_sla
echo "------------------------------------------"
echo 

# Routing_key Migration - MySQL Verification

routing_key_query=`$database_connection "select count(*) from routing_key a inner join  app c on a.active_app_id=c.app_id inner join sp d  on c.sp=d.id inner join ncs e on e.id=a.ncs  where c.app_id like 'PD%' AND c.current_state='ACTIVE';"`

echo "-----------------------"
echo "Routing_key Migration Details - MySQL :"
echo "------------------------------------------"
echo "Migrated Routing_key applications  : = " $routing_key_query
echo "------------------------------------------"
echo

#Subscriber Migration - MySQL Verification

subscribers_query=`$database_connection "select count(*) from subscriber_registration left join sp on subscriber_registration.id = sp.id left join app on subscriber_registration.id = app.id  left join duration_based_subs_charging_policy on app.id = duration_based_subs_charging_policy.id left join routing_key on duration_based_subs_charging_policy.id = routing_key.id left join sp_user_name on routing_key.id = sp_user_name.sp_id;"`

echo "-----------------------"
echo "Subscriber Migration Details - MySQL :"
echo "------------------------------------------"
echo "Total Subscribers : = " $subscribers_query
echo "------------------------------------------"
echo


#Reporting Count Verification

query=`$database_connection "SELECT count(*)
                        FROM        sp 
                                    INNER JOIN sp_user_name 
                                    ON sp_user_name.sp_id = sp.id 
                                    INNER JOIN app 
                                    ON app.sp = sp.id 
                        WHERE       app.app_id LIKE 'PD%' 
                                    AND app.current_state LIKE '%ACTIVE%';"`

echo "---------------------------"
echo "Reporting Migrated count"
echo "Migrated Count : "$query
echo "---------------------------"

# Mongodb Verification Scripts

echo "Mongodb Verification :"
echo "----------------------"
echo 

# Read Mongo file : VerifyMongo.js

mongo --quiet dulith_test_etisalat VerifyMongo.js

echo "End of the Scipt"
echo "----------------------"

# End of the Migration Script....