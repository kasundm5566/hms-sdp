#! /bin/bash
# VerificationScriptMigrationDataSdpEtisalat.sh file manipulates the count of MySQL and Mongodb data entries.

database_connection="mysql --user=user --pass=password --database=aventura_20100907 --host=$db_ip --skip-column-names  -e"

# Sp Migration - MySQL Verification

full_sp_count=`$database_connection "select count(*) from sp;"`
sp_with_issues=`$database_connection ""`
migrated_sp_count=`$database_connection ""`

echo "\t\t\t*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*"
echo "\t\t\t\t\t\t\t\tSP MIGRATION SUMMARY - LOGIC"
echo "\t\t\t*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*"
echo "
\t\t\t\t\t\t\t\tTotal sp count : 281

\t\t\t\t\t\t\tTotal count of sp with applications : 263

\t\t\t\t\tTotal count of sp with issues (sp duplication issues) : 12

\t\t\t\t\t\t\tLegible sp count will be : (263 - 12)

\t\t\t\t\t\t\t\tMigrated SP count : 251"
echo ""
echo "\t\t\t*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*"
echo "\t\t\t\t\t\t\tSP MIGRATION SUMMARY - EXECUTION OF QUERIES "
echo "\t\t\t*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*"
echo ""
echo "\t\t\t\t\t\t (Total sp Count) : SELECT COUNT(*) FROM sp; = "  $full_sp_count
echo ""
# Insert rest of the queries here .....
echo "\t\t\t\t\t\t\t\tEND OF SP SUMMARY REPORT"
echo "\t\t\t\t\t\t\t\t------------------------"
echo ""

# Confirm rest of the queries from Dulika ....

# App Migration - MySQL Verification

echo ""
echo "\t\t\t*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*"
echo "\t\t\t\t\t\t\t\tAPP MIGRATION SUMMARY - LOGIC"
echo "\t\t\t*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*"
echo "
\t\t\t\t\t\t\t\tTotal app count : 1253

\t\t\t\t\t\t\tTotal count of ( ACTIVE + PD ) applications: 577

\t\t\t\t\t\tTotal count of app with issues (ncs || routing keys) : 3

\t\t\t\t\t\t\tLegible app count will be : (577 - 3)

\t\t\t\t\t\t\t\tMigrated APP count : 574"

subscription_based_applications=`$database_connection "select count(*) from app  join sp on sp.id=app.sp join subscriber_registration_sla on  app.subs_reg_sla = subscriber_registration_sla.id join routing_key on app.app_id = routing_key.active_app_id   WHERE app.app_id LIKE 'PD%' AND app.current_state='ACTIVE' AND subs_reg_sla_type='SUBS_REG_SLA';"`

on_demand_based_applications=`$database_connection "select count(*) from app  join sp on sp.id=app.sp join subscriber_registration_sla on  app.subs_reg_sla = subscriber_registration_sla.id join routing_key on app.app_id = routing_key.active_app_id   WHERE app.app_id LIKE 'PD%' AND app.current_state='ACTIVE' AND subs_reg_sla_type='NULL_SUBS_REG_SLA';"`

Totalapplications=`$database_connection "select count(*) from app;"`

Migratedapplications=`$database_connection "select count(*) from app where app_id like 'PD%' and current_state like 'ACTIVE';"`

echo ""
echo "\t\t\t*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*"
echo "\t\t\t\t\t\t\tAPP MIGRATION SUMMARY - EXECUTION OF QUERIES "
echo "\t\t\t*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*"
echo ""
echo "\t\t\t\t\t\t(Total app count) : SELECT COUNT(*) FROM app; = " $Totalapplications
echo ""
echo "\t\t(Total [ ACTIVE + PD ] app count) : SELECT COUNT(*) FROM app WHERE app_id LIKE 'PD%' AND current_state LIKE 'ACTIVE'; = " $Migratedapplications
echo ""
echo "\t\t\t\t\t\t\t\tEND OF APP SUMMARY REPORT"
echo "\t\t\t\t\t\t\t\t-------------------------"
echo ""
echo ""


# Ncs_sla Migration - MySQL Verification

ncs_sla_migration_query_subs_reg_sla=`$database_connection "select count(*) from ncs_sla left join app on ncs_sla.app_id = app.id left join sp_user_name on sp_user_name.sp_id = ncs_sla.sp_id left join subscriber_registration_sla on  app.subs_reg_sla = subscriber_registration_sla.id left join routing_key on app.app_id = routing_key.active_app_id   where app.app_id like '%PD%' AND app.current_state='ACTIVE' AND app.subs_reg_sla_type = 'SUBS_REG_SLA';"`

ncs_sla_migration_query_null_subs_reg_sla=`$database_connection "select count(*) from ncs_sla left join app on ncs_sla.app_id = app.id left join sp_user_name on sp_user_name.sp_id = ncs_sla.sp_id left join subscriber_registration_sla on  app.subs_reg_sla = subscriber_registration_sla.id left join routing_key on app.app_id = routing_key.active_app_id   where app.app_id like '%PD%' AND app.current_state='ACTIVE' AND app.subs_reg_sla_type = 'NULL_SUBS_REG_SLA';"`

echo "\t\t\t*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*"
echo "\t\t\t\t\t\t\t\tNCS MIGRATION SUMMARY - LOGIC"
echo "\t\t\t*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*"
echo "
\t\t\t\t\t\t\t\tTotal ncs count : 1253

\t\t\t\t\t\t\tTotal count of ( ACTIVE + PD ) applications: 577

\t\t\t\t\t\tTotal count of app with issues (ncs || routing keys) : 3

\t\t\t\t\t\t\tLegible app count will be : (577 - 3)

\t\t\t\t\t\t\t\tMigrated APP count : 574"
echo "ncs_sla amount (Subscription)  : = "  $ncs_sla_migration_query_subs_reg_sla
echo "ncs_sla amount (Non - Subscription)  : = "  $ncs_sla_migration_query_null_subs_reg_sla
echo "------------------------------------------"
echo


















# Routing_key Migration - MySQL Verification

routing_key_query=`$database_connection "select count(*) from routing_key a inner join  app c on a.active_app_id=c.app_id inner join sp d  on c.sp=d.id inner join ncs e on e.id=a.ncs  where c.app_id like 'PD%' AND c.current_state='ACTIVE';"`
full_routing_keys_count=`$database_connection "select count(*) from routing_key;"`

echo "\t\t\t*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*"
echo "\t\t\t\t\t\t\t ROUTING KEYS MIGRATION SUMMARY - LOGIC"
echo "\t\t\t*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*"
echo "
\t\t\t\t\t\t\t\tTotal routing keys count : 1887
\t\t\t\t\t\t\tTotal count of ( ACTIVE + PD ) routing keys: 575
\t\t\t\t\t\t\tMigrated routing keys counts : 575"
echo ""
echo "\t\t\t*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*"
echo "\t\t\t\t\t\tROUTING KEYS MIGRATION SUMMARY - EXECUTION OF QUERIES "
echo "\t\t\t*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*"
echo ""
echo "\t\t\t\t\t\t (Total routing keys Count) : SELECT COUNT(*) FROM routing_key; = "  $full_routing_keys_count
echo ""
echo "(Total [ ACTIVE + PD ] routing keys Count) : select count(*) from routing_key a inner join  app c on a.active_app_id=c.app_id inner join sp d  on c.sp=d.id inner join ncs e on e.id=a.ncs  where c.app_id like 'PD%' AND c.current_state='ACTIVE';  = "  $routing_key_query
echo ""
echo "\t\t\t\t\t\t\t\tEND OF ROUTING KEYS SUMMARY REPORT"
echo "\t\t\t\t\t\t\t\t----------------------------------"
echo ""



#Subscriber Migration - MySQL Verification

echo "\t\t\t*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*"
echo "\t\t\t\t\t\t\t SUBSCRIBER MIGRATION SUMMARY - LOGIC"
echo "\t\t\t*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*"
echo "
\t\t\t\t\t\t\t\tTotal Subscribers : 204932"

subscribers_query=`$database_connection "SELECT  count(*)
                       		FROM subscriber_registration
                               LEFT JOIN app ON subscriber_registration.app_id = app.app_id
                               LEFT JOIN sp ON sp.id = app.sp
                               LEFT JOIN duration_based_subs_charging_policy ON app.id = duration_based_subs_charging_policy.id
                               LEFT JOIN routing_key ON routing_key.active_app_id = app.app_id
                               LEFT JOIN sp_user_name ON sp_user_name.sp_id = sp.id;"`

echo ""
echo "\t\t\t*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*"
echo "\t\t\t\t\t\tSUBSCRIBER MIGRATION SUMMARY - EXECUTION OF QUERIES "
echo "\t\t\t*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*"
echo ""
echo "\t\t\t\t\t\t (Total Subscribers Migrated) : SELECT  count(*)
                       FROM subscriber_registration
                               left JOIN app ON subscriber_registration.app_id = app.app_id
                               LEFT JOIN sp ON sp.id = app.sp
                               LEFT JOIN duration_based_subs_charging_policy ON app.id = duration_based_subs_charging_policy.id
                               LEFT JOIN routing_key ON routing_key.active_app_id = app.app_id
                               LEFT JOIN sp_user_name ON sp_user_name.sp_id = sp.id; = "  $subscribers_querys


echo ""
echo "\t\t\t\t\t\t\t\tEND OF SUBSCRIBER SUMMARY REPORT"
echo "\t\t\t\t\t\t\t\t----------------------------------"
echo ""

#Common Admins Migration - MySQL Verification

echo "\t\t\t*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*"
echo "\t\t\t\t\t\t\t COMMON ADMINS MIGRATION SUMMARY - LOGIC"
echo "\t\t\t*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*"
echo "
\t\t\t\t\t\t\t\tTotal Common Admins : 251"

subscribers_query=`$database_connection "select count(*) from sp"`

echo ""
echo "\t\t\t*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*"
echo "\t\t\t\t\t\tCOMMON ADMINS MIGRATION SUMMARY - EXECUTION OF QUERIES "
echo "\t\t\t*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*"
echo ""
echo "\t\t\t\t\t\t (Total Common Admins Migrated) : SELECT  count(*) from sp; = "  $subscribers_querys


echo ""
echo "\t\t\t\t\t\t\t\tEND OF COMMON ADMINS SUMMARY REPORT"
echo "\t\t\t\t\t\t\t\t----------------------------------"
echo ""


#Reporting Count Verification

echo "\t\t\t*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*"
echo "\t\t\t\t\t\t\t REPORTING MIGRATION SUMMARY - LOGIC"
echo "\t\t\t*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*"
echo "
\t\t\t\t\t\t\t\tTotal Reportings : 574"


query=`$database_connection "SELECT count(*)
                        FROM        app
                        WHERE       app.app_id LIKE 'PD%'
                                    AND app.current_state LIKE '%ACTIVE%';"`

echo "---------------------------"
echo "Reporting Migrated count"
echo "Migrated Count : "$query
echo "---------------------------"

# Mongodb Verification Scripts

echo "Mongodb Verification :"
echo "----------------------"
echo

# Read Mongo file : VerifyMongo.js

mongo --quiet dulith_test_etisalat VerifyMongo.js

echo "End of the Scipt"
echo "----------------------"

# End of the Migration Script....