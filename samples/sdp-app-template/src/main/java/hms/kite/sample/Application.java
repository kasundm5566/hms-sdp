/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sample;

import hms.common.rest.util.Message;
import hms.kite.sample.util.*;

import java.util.HashMap;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class Application implements SmsMoListener, UssdMoListener {

    private SdpClient smsClient;
    private SdpClient ussdClient;
    private SdpClient wappushClient;
    private CasClient casClient;

    @Override
    public Map onSms(Map request) {
        System.out.println("request = " + request);
        System.out.println("request = " + request);
        System.out.println("request = " + request);
        System.out.println("request = " + request);
        System.out.println("request = " + request);
        System.out.println("request = " + request);
        System.out.println("request = " + request);
        System.out.println("request = " + request);
        System.out.println("request = " + request);
        System.out.println("request = " + request);
        System.out.println("request = " + request);
        System.out.println("request = " + request);
        System.out.println("request = " + request);


        String message = (String) request.get("message");

        String[] split = message.split(" ");


        Map response = new HashMap();

        response.put("message", "thank you for voting us.");

//        smsClient.send(response);


        return null;
    }

    @Override
    public Map onUssd(Map request) {

        //here u get the ussd request. Serve it.
        return null;
    }

    public void setSmsClient(SdpClient sdpClient) {
        this.smsClient = sdpClient;
    }

    public void setUssdClient(SdpClient ussdClient) {
        this.ussdClient = ussdClient;
    }

    public void setWappushClient(SdpClient wappushClient) {
        this.wappushClient = wappushClient;
    }

    public void setCasClient(CasClient casClient) {
        this.casClient = casClient;
    }
}
