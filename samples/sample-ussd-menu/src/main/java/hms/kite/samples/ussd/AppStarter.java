/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.samples.ussd;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class AppStarter {
	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-context.xml");
		System.out.println(" __   __          __                     __           __   __       ___  ___     ___ ");
		System.out
				.println(" |  \\ /  \\    \\ / /  \\ |  |    |__/ |\\ | /  \\ |  |    /  ` /  \\ |\\ |  |  |__  \\_/  |  ");
		System.out
				.println(" |__/ \\__/     |  \\__/ \\__/    |  \\ | \\| \\__/ |/\\|    \\__, \\__/ | \\|  |  |___ / \\  | ");
		System.out.println("\n**********  $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$   ************");
	}
}
