/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.samples.ussd;

import hms.kite.samples.ussd.data.QuestionsAndAnswersProcessor;

import java.util.HashMap;
import java.util.Map;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 */
public class ResponseMessageCreator {
    private static final String MO_CONT = "mo-cont";
    private static final String MO_INIT = "mo-init";
    private static final String MT_CONT = "mt-cont";
    private static final String MT_FIN = "mt-fin";
    private static final String USSD_DATA_EXIT = "99";
    private QuestionsAndAnswersProcessor questionsAndAnswersProcessor;
    private String appPassword;

    /**
     * Create the response message need to send back to the mobile user based on
     * the message send by him
     *
     * @param deliverRequest parameters Received USSD message
     * @return Generated Ussd message needed to send back to mobile user
     */
    public Map<String, Object> createResponse(Map<String, Object> moMessage) {
        String data = (String) moMessage.get("message");
        String ussdOp = (String) moMessage.get("ussdOperation");
        Map<String, Object> replyMessage = null;
        String replyToUser = null;

        // Initial message from the mobile user
        if (MO_INIT.equals(ussdOp)) {

            replyToUser = questionsAndAnswersProcessor.startQuiz(); // Start
            // quiz
            replyMessage = createRespMessage(moMessage, MT_CONT, replyToUser);

        } else if (MO_CONT.equals(ussdOp) && (data.equalsIgnoreCase(USSD_DATA_EXIT))) {// Intermediate
            // message
            // from
            // mobile
            // user

            replyToUser = questionsAndAnswersProcessor.exitTheQuiz(); // Exit
            // from
            // quiz
            replyMessage = createRespMessage(moMessage, MT_FIN, replyToUser);

        } else if (MO_CONT.equals(ussdOp)) { // Last message from the Mobile
            // user

            replyToUser = questionsAndAnswersProcessor.checkUserAnswer(data); // Check
            // user
            // answers

            if (questionsAndAnswersProcessor.isFinalResult()) { // If it was
                // final result
                // user should
                // be exit from
                // quiz.

                questionsAndAnswersProcessor.setFinalResult(Boolean.FALSE);
                replyMessage = createRespMessage(moMessage, MT_FIN, replyToUser);

            } else {
                replyMessage = createRespMessage(moMessage, MT_CONT, replyToUser);
            }

            if (replyMessage == null) {

                System.out.println("No reply message is configured for user input[" + data
                        + "], sending main menu again.");
                replyMessage = createRespMessage(moMessage, MT_CONT, questionsAndAnswersProcessor.stopQuizInError());
            }

        } else {
            System.out.println("Ussd_op was [" + ussdOp + "], No reply sent.");
        }

        return replyMessage;
    }

    private Map<String, Object> createRespMessage(final Map<String, Object> moMessage, String ussdOp, String respToUser) {
        Map<String, Object> mtRequest = new HashMap<String, Object>();
        mtRequest.put("applicationId", moMessage.get("applicationId"));
        mtRequest.put("sessionId", moMessage.get("sessionId"));
        mtRequest.put("destinationAddress", moMessage.get("sourceAddress"));
        mtRequest.put("password", appPassword);
        mtRequest.put("ussdOperation", ussdOp);
        mtRequest.put("message", respToUser);
        return mtRequest;
    }

    public void setQuestionsAndAnswersProcessor(QuestionsAndAnswersProcessor questionsAndAnswersProcessor) {
        this.questionsAndAnswersProcessor = questionsAndAnswersProcessor;
    }

    public void setAppPassword(String appPassword) {
        this.appPassword = appPassword;
    }

}
