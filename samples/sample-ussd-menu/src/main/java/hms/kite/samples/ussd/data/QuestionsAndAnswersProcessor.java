/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.samples.ussd.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Processes questions, check user answer and notify result.
 *
 */
public class QuestionsAndAnswersProcessor {

	private List<Question> questionsList = null;
	public static int numberofQuestionsSend;
	private boolean isInitialRequestSend;
	private boolean isFinalResult;

	/**
	 * When user send his initial request, ussd_op = 1.
	 */
	public String startQuiz() {

		isInitialRequestSend = Boolean.TRUE;

		String startingMessage = "\n ==== USSD quiz show going to start ! === \n"
				+ "\n #) Quiz contains almost 3 questions. " + "\n #) Each question has four answers. "
				+ "\n #) User should select one correct answer. " + "\n #) At the end result will be notify. "
				+ "\n\n  *** Now you will getting your fist question."
				+ "\n\n ========================================\n\n ";

		// Set number of questions send to user 0
		numberofQuestionsSend = 0;

		// Set number of questions contains empty
		questionsList = null;

		setAllQuestions();
		return sendQuestion();
	}

	/**
	 * Set all questions with answer, before start quiz.
	 */
	private void setAllQuestions() {

		// id, question, answers, correctAnswerIndex
		Question questionsOne = new Question(1, "Question-One", "a,b,c,d", "1");
		Question questionsTwo = new Question(2, "Question-Two", "a,b,c,d", "2");
		Question questionsThree = new Question(3, "Question-Three", "a,b,c,d", "3");

		questionsList = new ArrayList<Question>();
		// add questions to list
		questionsList.add(questionsOne);
		questionsList.add(questionsTwo);
		questionsList.add(questionsThree);
	}

	/**
	 * Send one question to user.
	 */
	private String sendQuestion() {

		String inforQuestionsSend = null;
		if (questionsList.size() > numberofQuestionsSend) {

			Question questionsSend = questionsList.get(numberofQuestionsSend);
			inforQuestionsSend = questionToString(questionsSend);
			numberofQuestionsSend++;
		} else {
			inforQuestionsSend = stopTheQuiz();
		}

		return inforQuestionsSend;
	}

	/**
	 * Check whether answer given by user correct or not.
	 *
	 * @param userSelection
	 *            String
	 */
	public String checkUserAnswer(String userSelection) {

		String inforQuestionsSend = null;

		if (isInitialRequestSend) { // Check whether user send initial request
									// for start the quiz.

			int questionsNoSend = numberofQuestionsSend - 1;
			Question questionSend = questionsList.get(questionsNoSend);

			if (questionSend.getCorrectAnswerIndex().equalsIgnoreCase(userSelection)) {
				System.out.println("\n ---- Answer is correct --- \n");
				questionSend.setUserAnswerCorrectly(Boolean.TRUE);
				questionsList.set(questionsNoSend, questionSend);
			} else {
				System.out.println("\n --- Answer is Incorrect --- \n");
			}

			// Send next question
			inforQuestionsSend = sendQuestion();
		} else {

			String initialMessage = "Please send initial request for start the quiz.";
			System.out.println("\n===================================\n");
			System.out.println(initialMessage);
			System.out.println("\n===================================\n");
			stopQuizInError();
			inforQuestionsSend = initialMessage;
		}

		return inforQuestionsSend;
	}

	/**
	 * Notify final result of quiz.
	 */
	private String notifyResult() {

		int numberofQuestionsAnsweredCorrectly = 0;
		StringBuffer questionsAnsweredCorrectly = new StringBuffer("");

		for (int j = 0; j < questionsList.size(); j++) {

			Question questionSend = questionsList.get(j);

			if (questionSend.isUserAnswerCorrectly()) {
				numberofQuestionsAnsweredCorrectly++;
				questionsAnsweredCorrectly.append("Question ID: " + questionSend.getId() + "\n" + "Question: "
						+ questionSend.getQuestion() + "\n" + "Answers: \n" + questionSend.appendAnswers()
						+ "Correct Answer Index:" + questionSend.getCorrectAnswerIndex() + "\n\n");
			}
		}

		System.out.println("\n***************** Final result of quiz *****************\n");

		String numofQuestionsAnsweredCorrectly = "Questions answered correctly: " + numberofQuestionsAnsweredCorrectly
				+ "\n\n";
		System.out.println(numofQuestionsAnsweredCorrectly);
		System.out.println("\n --- Questions --- " + "\n\n" + questionsAnsweredCorrectly);

		System.out.println("\n********************************************************\n");

		// Set number of questions contains empty
		questionsList = null;

		return numofQuestionsAnsweredCorrectly + questionsAnsweredCorrectly.toString();
	}

	/**
	 * Send result when user request exit from quiz
	 */
	public String exitTheQuiz() {

		System.out.println("\n***************** Exit from quiz *****************\n");

		String inforQuestionsSend = stopTheQuiz();

		if (inforQuestionsSend == null) {
			inforQuestionsSend = "You have not answer to any question";
		}

		return inforQuestionsSend;
	}

	/**
	 * When the quiz is end.
	 */
	private String stopTheQuiz() {

		numberofQuestionsSend = 0;
		isInitialRequestSend = Boolean.FALSE;
		isFinalResult = Boolean.TRUE;
		return notifyResult();
	}

	/**
	 * Stop the quiz when error occurred .
	 */
	public String stopQuizInError() {

		String errorMessage = "Sorry due to some error quiz will stop.";
		numberofQuestionsSend = 0;
		questionsList = null;
		isInitialRequestSend = Boolean.FALSE;

		return errorMessage;
	}

	/**
	 * Get content of the question to one string
	 *
	 * @param questionSend
	 * @return
	 */
	private String questionToString(Question questionSend) {

		StringBuffer questionsBuff = new StringBuffer("");
		questionsBuff.append("Question ID: " + questionSend.getId() + "\n" + "Question: " + questionSend.getQuestion()
				+ "\n" + "Answers: \n" + questionSend.appendAnswers() + "\n");

		return questionsBuff.toString();
	}

	public boolean isFinalResult() {
		return isFinalResult;
	}

	public void setFinalResult(boolean finalResult) {
		isFinalResult = finalResult;
	}
}
