/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.samples.ussd;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */

@Produces({ "application/json", "application/xml" })
@Consumes({ "application/json", "application/xml" })
@Path("/ussd/")
public class UssdMoReceiver {
	private static final Logger LOGGER = LoggerFactory.getLogger(UssdMoReceiver.class);

	private MoMessageProcessor moMessageProcessor;

	@Produces({ "application/json", "application/xml" })
	@Consumes({ "application/json", "application/xml" })
	@POST
	@Path("/receive")
	public Map<String, Object> sendService(Map<String, Object> msg) {
		LOGGER.info("======================================");
		LOGGER.info("Message received[" + msg + "]");
		LOGGER.info("======================================");

		moMessageProcessor.processMessage(msg);

		Map<String, Object> resp = new HashMap<String, Object>();
		resp.put("statusCode", "S1000");
		resp.put("statusDescription", "Success");
		return resp;
	}

	public void setMoMessageProcessor(MoMessageProcessor moMessageProcessor) {
		this.moMessageProcessor = moMessageProcessor;
	}

}
