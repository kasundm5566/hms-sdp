/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.samples.ussd.data;

/**
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class Question {
	private int id;
	private String question;
	private String answers;
	private String[] arrayAnswers;
	private String correctAnswerIndex;
	private boolean isUserAnswerCorrectly;

	public Question(int id, String question, String answers, String correctAnswerIndex) {
		this.id = id;
		this.question = question;
		this.answers = answers;
		this.correctAnswerIndex = correctAnswerIndex;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getCorrectAnswerIndex() {
		return correctAnswerIndex;
	}

	public void setCorrectAnswerIndex(String correctAnswerIndex) {
		this.correctAnswerIndex = correctAnswerIndex;
	}

	public String getAnswers() {
		return answers;
	}

	public void setAnswers(String answers) {
		this.answers = answers;
		splitAnswers();
	}

	private void splitAnswers() {
		arrayAnswers = answers.split(",");
	}

	/**
	 * Append one by one from array of answers.
	 *
	 * @return
	 */
	public String appendAnswers() {

		splitAnswers();

		StringBuffer answers = new StringBuffer("");

		if (arrayAnswers.length > 0) {
			for (int i = 0; i < arrayAnswers.length; i++) {
				answers.append((1 + i) + "). " + arrayAnswers[i] + "\n");
			}

		} else {
			System.out.println("Please set the answers");
		}

		return answers.toString();
	}

	public boolean isUserAnswerCorrectly() {
		return isUserAnswerCorrectly;
	}

	public void setUserAnswerCorrectly(boolean userAnswerCorrectly) {
		isUserAnswerCorrectly = userAnswerCorrectly;
	}
}
