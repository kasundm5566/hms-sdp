/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 *   $LastChangedDate: $
 *   $LastChangedBy: $
 *   $LastChangedRevision: $
 *
 */
package hms.kite.samples.ussd;

import hms.common.rest.util.JsonBodyProvider;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * $LastChangedDate: $ $LastChangedBy: $ $LastChangedRevision: $
 *
 */
public class MoMessageProcessor {
	private static final Logger LOGGER = LoggerFactory.getLogger(MoMessageProcessor.class);
	/*
	 * { message=111, senderAddress=tel: 254701234567, applicationID=APP_000001,
	 * encoding=0, ussd-op=mo-init, version=1.0, session-id=1320389366689 }
	 */

	private ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 8);
	private ResponseMessageCreator responseMessageCreator;

	public void processMessage(Map<String, Object> msg) {
		executor.execute(new Processor(msg));
	}

	private LinkedList<Object> providers;

	public void init() {
		providers = new LinkedList<Object>();
		providers.add(new JsonBodyProvider());
	}



	public void setResponseMessageCreator(ResponseMessageCreator responseMessageCreator) {
		this.responseMessageCreator = responseMessageCreator;
	}



	private WebClient createWebclient() {
		String appUrl = "http://core.sdp:7000/ussd/send";
		WebClient webClient = WebClient.create(appUrl, providers);
		webClient.header("Content-Type", "application/json");
		webClient.accept("application/json");
		return webClient;
	}

	private class Processor implements Runnable {

		private final Map<String, Object> moMessage;

		public Processor(Map<String, Object> moMessage) {
			super();
			this.moMessage = moMessage;
		}

		@Override
		public void run() {
			try {
				WebClient webClient = createWebclient();
				Map<String, Object> atMessage = createMessage(moMessage);
				LOGGER.info("Sending ussd at message [{}] to url[{}]", atMessage, webClient.getBaseURI());
				Response response = webClient.post(atMessage);
				if (response.getStatus() >= 200 && response.getStatus() <= 205) {
					LOGGER.info("Message sent successfully. response [{}]", response.getEntity());
				} else {
					LOGGER.info("Message sending failed http status is [{}]", response.getStatus());
				}

			} catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
			}

		}

		private Map<String, Object> createMessage(final Map<String, Object> moMessage) {
			/*
			nbl-url=http://core.sdp:7000/ussd/send

				############ Message parameters ############

				#M
				applicationID=APP_000001

				#M
				password = test

				#O
				version = 1.0

				#M
				message =my testing message

				#M
				ussdOp = mt-cont

				#M
				sessionId = 12345678

				#M
				address= tel:254701234563
				*/
//			Map<String, Object> mtRequest = new HashMap<String, Object>();
//			mtRequest.put("applicationID", moMessage.get("applicationID"));
//			mtRequest.put("password", "test");
//			mtRequest.put("version", "1.0");
//			mtRequest.put("message", "my testing message");
//			mtRequest.put("ussdOp", "mt-cont");
//			mtRequest.put("sessionId", moMessage.get("session-id"));
//			mtRequest.put("address", moMessage.get("senderAddress"));
//			return mtRequest;

			return responseMessageCreator.createResponse(moMessage);

		}

	}

}
