/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.samples.api.caas.messages;

public class DirectCreditRequest extends ChargingRequestCommonParameters {

    private String externalTrxId;
    private String paymentInstrumentName;
    private String accountId;
    private String amount;
    private String currency;
    private String orderNo;
    private String invoiceNo;

    public String getExternalTrxId() {
        return externalTrxId;
    }

    public void setExternalTrxId(String externalTrxId) {
        this.externalTrxId = externalTrxId;
    }

    public String getPaymentInstrumentName() {
        return paymentInstrumentName;
    }

    public void setPaymentInstrumentName(String paymentInstrumentName) {
        this.paymentInstrumentName = paymentInstrumentName;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("DirectCreditRequest{")
                .append("accountId='").append(accountId).append('\'')
                .append(", externalTrxId='").append(externalTrxId).append('\'')
                .append(", paymentInstrumentName='").append(paymentInstrumentName).append('\'')
                .append(", amount='").append(amount).append('\'')
                .append(", currency='").append(currency).append('\'')
                .append(", orderNo='").append(orderNo).append('\'')
                .append(", invoiceNo='").append(invoiceNo).append('\'')
                .append('}').toString();
    }
}