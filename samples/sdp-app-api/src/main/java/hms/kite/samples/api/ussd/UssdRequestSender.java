/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.samples.api.ussd;

import hms.kite.samples.api.GenericSender;
import hms.kite.samples.api.SdpException;
import hms.kite.samples.api.ussd.messages.MtUssdReq;
import hms.kite.samples.api.ussd.messages.MtUssdResp;

import java.net.URL;

/**
 * This class is use for sending the USSD requests from the sample app to SDP by using GenericSender class
 */
public class UssdRequestSender {
    private URL sdpUrl;

    public UssdRequestSender(URL sdpUrl) {
        this.sdpUrl = sdpUrl;
    }

    public MtUssdResp sendUssdRequest(MtUssdReq smsReq) throws SdpException {
        try {
            GenericSender<MtUssdReq, MtUssdResp> ussdRequestSender = new GenericSender<MtUssdReq, MtUssdResp>(sdpUrl);
            return ussdRequestSender.sendRequest(smsReq, MtUssdResp.class);
        } catch (Exception e) {
            throw new SdpException(e);
        }
    }
}