/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.samples.api.caas;

import com.google.gson.Gson;
import hms.kite.samples.api.StatusCodes;
import hms.kite.samples.api.caas.messages.ChargingNotificationRequest;
import hms.kite.samples.api.caas.messages.ChargingNotificationResponse;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ChargingNotificationReceiver extends HttpServlet {

    private final static Logger LOGGER = Logger.getLogger(ChargingNotificationReceiver.class.getName());

    private List<CaasNotificationListener> chargingNotificationListenerList = new ArrayList<CaasNotificationListener>();
    private ExecutorService executorService;
    private static int sdpCaasNoteReceiverThreadCount;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        String receiverClassName = config.getInitParameter("chargingNotificationReceive");
        initializeListeners(receiverClassName);
        initializeReceivingThreadPool();
    }

    private void initializeReceivingThreadPool() {
        executorService = Executors.newCachedThreadPool(new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r, "sdp-caas-notification-receiver-thread-" + ++sdpCaasNoteReceiverThreadCount);
            }
        });
    }

    private void initializeListeners(String receiverClassName) {
        try {
            if (receiverClassName != null) {
                Class listener = Class.forName(receiverClassName);
                Constructor constructor = listener.getConstructor(new Class[]{});
                Object object = constructor.newInstance();
                if (object instanceof CaasNotificationListener) {
                    chargingNotificationListenerList.add((CaasNotificationListener) object);
                }
            }
        } catch (Exception e) {
            LOGGER.log(Level.INFO,"Exception occurred while initializing listener", e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String contentType = req.getContentType();
        if (contentType == null || !contentType.equals("application/json")) {
            resp.setStatus(415);
            resp.getWriter().println("Only application/json is supporting");
            return;
        }
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse resp) {
        Gson gson = new Gson();
        try {
            String readContent = readStringContent(req);
            ChargingNotificationRequest noteReq = gson.fromJson(readContent, ChargingNotificationRequest.class);
            for (CaasNotificationListener notificationListener : chargingNotificationListenerList) {
                fireMoEvent(notificationListener, noteReq);
            }

            ChargingNotificationResponse chargingNoteRes = createResponse(StatusCodes.SuccessK, "Success");
            resp.getWriter().print(gson.toJson(chargingNoteRes));

        } catch (Exception e) {
            ChargingNotificationResponse chargingNoteRes = createResponse(StatusCodes.SystemErrorK, "System Error Occurred");
            try {
                resp.getWriter().print(gson.toJson(chargingNoteRes));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    private void fireMoEvent(final CaasNotificationListener noteListener, final ChargingNotificationRequest noteReq) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    noteListener.onReceiveCaasNotification(noteReq);
                } catch (Exception e) {
                    LOGGER.log(Level.INFO, "Unexpected error occurred", e);
                }
            }
        });
    }

    private String readStringContent(HttpServletRequest req) throws IOException {
        InputStream is = req.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));

        String line;
        StringBuilder content = new StringBuilder();

        while ((line = bufferedReader.readLine()) != null) {
            content.append(line);
        }

        return content.toString();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().println("SDP Application is Running");
    }

    private ChargingNotificationResponse createResponse(String statusCode, String statusDetail) {
        ChargingNotificationResponse chargingNoteRes = new ChargingNotificationResponse();
        chargingNoteRes.setStatusCode(statusCode);
        chargingNoteRes.setStatusDetail(statusDetail);
        return chargingNoteRes;
    }
}