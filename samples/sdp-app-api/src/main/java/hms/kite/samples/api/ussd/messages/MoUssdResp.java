/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.samples.api.ussd.messages;

public class MoUssdResp {

    private String statusCode;
    private String statusDetail;

    /**
     * Get the status code for the USSD request
     * @return
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * Set the statud code for the USSD request
     * @param statusCode
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * Get the description of the status code
     * @return
     */
    public String getStatusDetail() {
        return statusDetail;
    }

    /**
     * Set the description of the status code
     * @param statusDetail
     */
    public void setStatusDetail(String statusDetail) {
        this.statusDetail = statusDetail;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("MoUssdResp{")
                .append("statusCode='").append(statusCode).append('\'')
                .append(", statusDetail='").append(statusDetail).append('\'')
                .append('}').toString();
    }
}