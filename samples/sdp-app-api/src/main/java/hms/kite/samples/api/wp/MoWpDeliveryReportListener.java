package hms.kite.samples.api.wp;

import hms.kite.samples.api.wp.messages.MoWpDeliveryReportReq;

public interface MoWpDeliveryReportListener {

    /**
     * Overide this method for receive a MoWpDeliveryReportRequest
     * @param moWpDeliveryReportReq
     */
    public void onReceivedDeliveryReport(MoWpDeliveryReportReq moWpDeliveryReportReq);
}
