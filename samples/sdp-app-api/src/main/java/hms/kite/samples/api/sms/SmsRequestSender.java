/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.samples.api.sms;

import hms.kite.samples.api.GenericSender;
import hms.kite.samples.api.SdpException;
import hms.kite.samples.api.sms.messages.MtSmsReq;
import hms.kite.samples.api.sms.messages.MtSmsResp;

import java.net.URL;

/**
 * This class is use for sending the SMS requests from the sample app to SDP by using GenericSender class
 */
public class SmsRequestSender {

    private URL sdpUrl;

    public SmsRequestSender(URL sdpUrl) {
        this.sdpUrl = sdpUrl;
    }

    public MtSmsResp sendSmsRequest(MtSmsReq smsReq) throws SdpException {
        try {
            GenericSender<MtSmsReq, MtSmsResp> smsRequestSender = new GenericSender<MtSmsReq, MtSmsResp>(sdpUrl);
            return smsRequestSender.sendRequest(smsReq, MtSmsResp.class);
        } catch (Exception e) {
            throw new SdpException(e);
        }
    }

}