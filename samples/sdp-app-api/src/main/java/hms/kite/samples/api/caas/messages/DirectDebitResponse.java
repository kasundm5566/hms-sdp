/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.samples.api.caas.messages;

/**
 * This class is use for take responses for the charging request from SDP
 */
public class DirectDebitResponse {

    private String statusCode;
    private String statusDetail;

    /**
     * Get the request status code
     *
     * @return
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * Set the request status code
     *
     * @param statusCode
     */
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * Get the status code description
     *
     * @return
     */
    public String getStatusDetail() {
        return statusDetail;
    }

    /**
     * Set the status code description
     *
     * @param statusDetail
     */
    public void setStatusDetail(String statusDetail) {
        this.statusDetail = statusDetail;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("CreditReserveResponse{")
                .append("statusCode='").append(statusCode).append('\'')
                .append(", statusDetail='").append(statusDetail).append('\'')
                .append('}').toString();
    }
}
