/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.samples.api.subscription;

import hms.kite.samples.api.GenericSender;
import hms.kite.samples.api.SdpException;
import hms.kite.samples.api.subscription.messages.*;

import java.net.URL;

/**
 * This class is use for sending the Subscription requests from the sample app to SDP by using GenericSender class
 */
public class SubscriptionRequestSender {
    private URL sdpUrl;

    public SubscriptionRequestSender(URL sdpUrl) {
        this.sdpUrl = sdpUrl;
    }

    public SubscriptionResponse sendSubscriptionRequest(SubscriptionRequest subscriptionRequest) throws SdpException {
        try {
            GenericSender<SubscriptionRequest, SubscriptionResponse> subscriptionRequestSender = new GenericSender<SubscriptionRequest, SubscriptionResponse>(sdpUrl);
            return subscriptionRequestSender.sendRequest(subscriptionRequest, SubscriptionResponse.class);
        } catch (Exception e) {
            throw new SdpException(e);
        }
    }

    public SubscriptionStatusResponse sendSubscriptionStatusRequest(SubscriptionStatusRequest subscritptionStatusRequest) throws SdpException {
        try {
            GenericSender<SubscriptionStatusRequest, SubscriptionStatusResponse> subscriptionRequestSender = new GenericSender<SubscriptionStatusRequest, SubscriptionStatusResponse>(sdpUrl);
            return subscriptionRequestSender.sendRequest(subscritptionStatusRequest, SubscriptionStatusResponse.class);
        } catch (Exception e) {
            throw new SdpException(e);
        }
    }

    public SubQueryBaseResponse sendSubscriptionQueryBaseRequest(SubQueryBaseRequest subscriptionRequest) throws SdpException {
        try {
            GenericSender<SubQueryBaseRequest, SubQueryBaseResponse> subscriptionRequestSender = new GenericSender<SubQueryBaseRequest, SubQueryBaseResponse>(sdpUrl);
            return subscriptionRequestSender.sendRequest(subscriptionRequest, SubQueryBaseResponse.class);
        } catch (Exception e) {
            throw new SdpException(e);
        }
    }

}