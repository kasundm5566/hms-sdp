/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.samples.api.ussd;

public enum OperationType {
    
    MT_INIT("mt-init"),
    MT_CONT("mt-cont"),
    MT_FIN("mt-fin"),
    MO_INIT("mo-init"),
    MO_CONT("mo-cont");

    private String name;

    private OperationType(String name){
        this.name=name;
    }
    
    public String getName(){
        return name;
    }
}
