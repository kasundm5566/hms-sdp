/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.samples.api.caas;

public class ChargingException extends Exception{
    public ChargingException() {
        super();
    }

    public ChargingException(String message) {
        super(message);
    }

    public ChargingException(String message, Throwable cause) {
        super(message, cause);
    }

    public ChargingException(Throwable cause) {
        super(cause);
    }
}
