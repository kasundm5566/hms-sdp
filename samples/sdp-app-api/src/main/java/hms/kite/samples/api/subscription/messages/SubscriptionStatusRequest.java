/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.samples.api.subscription.messages;

/**
 * This class use as to set parameters to request for the subscription status
 */
public class SubscriptionStatusRequest {

    private String applicationId;
    private String sourceAddress;
    private String password;

    /**
     * Get the application Id of application
     * @return
     */
    public String getApplicationId() {
        return applicationId;
    }

    /**
     * Set the application Id for the application
     * @param applicationId
     */
    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    /**
     * Get the sender address or subscriber user Id
     * @return
     */
    public String getSourceAddress() {
        return sourceAddress;
    }

    /**
     * Set the sender address or subscriber user Id
     * @param sourceAddress
     */
    public void setSourceAddress(String sourceAddress) {
        this.sourceAddress = sourceAddress;
    }

    public String getPassword() {
        return password;
    }

    /**
     * Set the application password
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("SubscriptionStatusRequest{")
                .append("applicationId='")
                .append(applicationId).append('\'')
                .append(", sourceAddress='").append(sourceAddress).append('\'')
                .append(", password='").append(password).append('\'')
                .append('}').toString();
    }
}