/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.samples.api.caas.messages;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ChargingRequestResponse {

    private String externalTrxId;
    private String internalTrxId;
    private String referenceId;
    private String businessNumber;
    private String timeStamp;
    private String statusCode;
    private String statusDetail;
    private String longDescription;
    private String shortDescription;

    public String getBusinessNumber() {
        return businessNumber;
    }

    public void setBusinessNumber(String businessNumber) {
        this.businessNumber = businessNumber;
    }

    public String getExternalTrxId() {
        return externalTrxId;
    }

    public void setExternalTrxId(String externalTrxId) {
        this.externalTrxId = externalTrxId;
    }

    public String getInternalTrxId() {
        return internalTrxId;
    }

    public void setInternalTrxId(String internalTrxId) {
        this.internalTrxId = internalTrxId;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusDetail() {
        return statusDetail;
    }

    public void setStatusDetail(String statusDetail) {
        this.statusDetail = statusDetail;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("ChargingRequestResponse{")
                .append("businessNumber='").append(businessNumber).append('\'')
                .append(", externalTrxId='").append(externalTrxId).append('\'')
                .append(", internalTrxId='").append(internalTrxId).append('\'')
                .append(", referenceId='").append(referenceId).append('\'')
                .append(", timeStamp='").append(timeStamp).append('\'')
                .append(", statusCode='").append(statusCode).append('\'')
                .append(", statusDetail='").append(statusDetail).append('\'')
                .append(", longDescription='").append(longDescription).append('\'')
                .append(", shortDescription='").append(shortDescription).append('\'')
                .append('}').toString();
    }
}