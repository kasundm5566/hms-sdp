/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.samples.api.sms;

import hms.kite.samples.api.sms.messages.MoSmsDeliveryReportReq;

public interface MoSmsDeliveryReportListener {

    /**
     * Overide this method for receive a MoDeliveryReportRequest
     * @param moDeliveryReportReq  Mobile Originate (MO), delivery report received from SDP
     */
    public void onReceivedDeliveryReport(MoSmsDeliveryReportReq moDeliveryReportReq);
}
