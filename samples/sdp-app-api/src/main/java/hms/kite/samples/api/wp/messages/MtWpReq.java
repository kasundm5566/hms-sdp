/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.samples.api.wp.messages;

import java.util.List;

public class MtWpReq {
    private String applicationId;
    private String password;
    private String version;
    private String wapPushType;
    private String wapUrl;
    private List<String> destinationAddresses;
    private String title;
    private String sourceAddress;
    private String deliveryStatusRequest;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getDestinationAddresses() {
        return destinationAddresses;
    }

    public void setDestinationAddresses(List<String> destinationAddresses) {
        this.destinationAddresses = destinationAddresses;
    }

    public String getDeliveryStatusRequest() {
        return deliveryStatusRequest;
    }

    public void setDeliveryStatusRequest(String deliveryStatusRequest) {
        this.deliveryStatusRequest = deliveryStatusRequest;
    }

    public String getSourceAddress() {
        return sourceAddress;
    }

    public void setSourceAddress(String sourceAddress) {
        this.sourceAddress = sourceAddress;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getWapUrl() {
        return wapUrl;
    }

    public void setWapUrl(String wapUrl) {
        this.wapUrl = wapUrl;
    }

    public String getWapPushType() {
        return wapPushType;
    }

    public void setWapPushType(String wapPushType) {
        this.wapPushType = wapPushType;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("MtWpReq{")
                .append("applicationId='")
                .append(applicationId).append('\'')
                .append(", password='").append(password).append('\'')
                .append(", version='").append(version).append('\'')
                .append(", wapPushType='").append(wapPushType).append('\'')
                .append(", wapUrl='").append(wapUrl).append('\'')
                .append(", destinationAddresses=").append(destinationAddresses)
                .append(", title='").append(title).append('\'')
                .append(", sourceAddress='").append(sourceAddress).append('\'')
                .append(", deliveryStatusRequest='").append(deliveryStatusRequest)
                .append('\'').append('}').toString();
    }
}