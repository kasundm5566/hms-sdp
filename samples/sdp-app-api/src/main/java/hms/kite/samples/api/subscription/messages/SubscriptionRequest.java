/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.samples.api.subscription.messages;

/**
 * This class use as to set the parameters for the subscription opt-in and opt-out requests
 */
public class SubscriptionRequest {

    private String applicationId;
    private String password;
    private String version;
    private String action;
    private String subscriberId;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSubscriberId() {
        return subscriberId;
    }

    public void setSubscriberId(String subscriberId) {
        this.subscriberId = subscriberId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("SubscriptionRequest{")
                .append("action='").append(action).append('\'')
                .append(", applicationId='").append(applicationId).append('\'')
                .append(", password='").append(password).append('\'')
                .append(", version='").append(version).append('\'')
                .append(", subscriberId='").append(subscriberId).append('\'')
                .append('}').toString();
    }
}