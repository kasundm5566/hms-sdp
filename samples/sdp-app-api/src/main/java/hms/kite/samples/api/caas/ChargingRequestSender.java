/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.samples.api.caas;

import hms.kite.samples.api.GenericSender;
import hms.kite.samples.api.SdpException;
import hms.kite.samples.api.caas.messages.*;

import java.net.URL;

/**
 * This class is use for sending the CAAS requests from the sample app to SDP by using GenericSender class
 */
public class ChargingRequestSender {

    private URL sdpUrl;

    public ChargingRequestSender(URL sdpUrl) {
        this.sdpUrl = sdpUrl;
    }

    public DirectDebitResponse sendCreditReserveRequest(CreditReserveRequest smsReq) throws SdpException {
        try {
            //todo : this is a prototype implementation. Complete this implementation after checking with new api.
            GenericSender<CreditReserveRequest, DirectDebitResponse> caasRequestSender = new GenericSender<CreditReserveRequest, DirectDebitResponse>(sdpUrl);
            return caasRequestSender.sendRequest(smsReq, DirectDebitResponse.class);
        } catch (Exception e) {
            throw new SdpException(e);
        }
    }

    public DirectDebitResponse sendDirectDebitRequest(DirectDebitRequest directDebitReq) throws SdpException {
        try {
            GenericSender<DirectDebitRequest, DirectDebitResponse> caasRequestSender = new GenericSender<DirectDebitRequest, DirectDebitResponse>(sdpUrl);
            return caasRequestSender.sendRequest(directDebitReq, DirectDebitResponse.class);
        } catch (Exception e) {
            throw new SdpException(e);
        }
    }

    public DirectDebitResponse sendDirectCreditRequest(DirectCreditRequest smsReq) throws SdpException {
        try {
            GenericSender<DirectCreditRequest, DirectDebitResponse> caasRequestSender = new GenericSender<DirectCreditRequest, DirectDebitResponse>(sdpUrl);
            return caasRequestSender.sendRequest(smsReq, DirectDebitResponse.class);
        } catch (Exception e) {
            throw new SdpException(e);
        }
    }

    public PaymentInstrumentListResponse sendPaymentInstrumentListReq(PaymentInstrumentListRequest smsReq) throws SdpException {
        try {
            GenericSender<PaymentInstrumentListRequest, PaymentInstrumentListResponse> caasRequestSender =
                    new GenericSender<PaymentInstrumentListRequest, PaymentInstrumentListResponse>(sdpUrl);
            return caasRequestSender.sendRequest(smsReq, PaymentInstrumentListResponse.class);
        } catch (Exception e) {
            throw new SdpException(e);
        }
    }

    public ChargingRequestResponse sendChargingRequest(DirectDebitRequest chargingReq) throws SdpException {
        try {
            GenericSender<DirectDebitRequest, ChargingRequestResponse> caasRequestSender = new GenericSender<DirectDebitRequest, ChargingRequestResponse>(sdpUrl);
            return caasRequestSender.sendRequest(chargingReq, ChargingRequestResponse.class);
        } catch (Exception e) {
            throw new SdpException(e);
        }
    }
}
