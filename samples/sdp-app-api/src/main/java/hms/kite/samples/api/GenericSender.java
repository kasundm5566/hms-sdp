/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.samples.api;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

/**
 * Use this class to send MtRequests to SDP.
 */
public class GenericSender<Req, Resp> {

    private URL sdpUrl;

    public GenericSender(URL sdpUrl) {
        this.sdpUrl = sdpUrl;
    }

    public Resp sendRequest(Req req, Class<Resp> resp) throws SdpException {
        try {
            return internalSendRequest(req, resp);
        } catch (Exception e) {
            throw new SdpException(e);
        }
    }

    private Resp internalSendRequest(Req req, Class<Resp> resp) throws IOException {
        URLConnection conn = sdpUrl.openConnection();
        Gson gson = new Gson();
        conn.setDoOutput(true);
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Accept", "application/json");
        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

        wr.write(gson.toJson(req));
        wr.flush();

        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        StringBuilder content = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            content.append(line);
            content.append("\n");
        }
        Resp response = gson.fromJson(content.toString(), resp);
        wr.close();
        rd.close();
        return response;
    }

    public URL getSdpUrl() {
        return sdpUrl;
    }

    /**
     * Set SDP MT Receiving URL
     *
     * @param sdpUrl
     */
    public void setSdpUrl(URL sdpUrl) {
        this.sdpUrl = sdpUrl;
    }

}
