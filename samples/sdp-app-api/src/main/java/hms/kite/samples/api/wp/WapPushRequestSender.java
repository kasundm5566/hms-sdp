/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.samples.api.wp;

import hms.kite.samples.api.GenericSender;
import hms.kite.samples.api.SdpException;
import hms.kite.samples.api.wp.messages.MtWpReq;
import hms.kite.samples.api.wp.messages.MtWpResp;

import java.net.URL;

/**
 * This class is use for sending the wap-push requests from the sample app to SDP by using GenericSender class
 */
public class WapPushRequestSender {

    private URL sdpUrl;

    public WapPushRequestSender(URL sdpUrl) {
        this.sdpUrl = sdpUrl;
    }

    public MtWpResp sendWpRequest(MtWpReq smsReq) throws SdpException {
        try {
            GenericSender<MtWpReq, MtWpResp> wpRequestSender = new GenericSender<MtWpReq, MtWpResp>(sdpUrl);
            return wpRequestSender.sendRequest(smsReq, MtWpResp.class);
        } catch (Exception e) {
            throw new SdpException(e);
        }
    }
}