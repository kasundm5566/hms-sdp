/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.samples.api.caas.messages;

import java.util.List;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class PaymentInstrumentListResponse {

    private List<PaymentInstrument> paymentInstrumentList;
    private String statusCode;
    private String statusDetail;

    public List<PaymentInstrument> getPaymentInstrumentList() {
        return paymentInstrumentList;
    }

    public void setPaymentInstrumentList(List<PaymentInstrument> paymentInstrumentList) {
        this.paymentInstrumentList = paymentInstrumentList;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusDetail() {
        return statusDetail;
    }

    public void setStatusDetail(String statusDetail) {
        this.statusDetail = statusDetail;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("PaymentInstrumentListResponse{")
                .append("paymentInstrumentList=").append(paymentInstrumentList)
                .append(", statusCode='").append(statusCode).append('\'')
                .append(", statusDetail='").append(statusDetail).append('\'')
                .append('}').toString();
    }
}