/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.samples.api.ussd;

import hms.kite.samples.api.ussd.messages.MoUssdReq;

public interface MoUssdListener {

    /**
     * Override this for initializing purposes if any
     */

    public void init();

    /**
     * Override this method to Receive MoUssdRequests.
     * @param moUssdReq Ussd request received from SDP to the application
     */
    public void onReceivedUssd(MoUssdReq moUssdReq);
}
