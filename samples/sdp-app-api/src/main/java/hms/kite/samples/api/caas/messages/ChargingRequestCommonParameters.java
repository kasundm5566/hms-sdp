/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.samples.api.caas.messages;

/**
 * This class contains the common characteristics for all charging requests
 */
public class ChargingRequestCommonParameters {

    private String applicationId;
    private String password;
    private String subscriberId;

    /**
     * Get the application Id
     * @return
     */
    public String getApplicationId() {
        return applicationId;
    }

    /**
     * set the application Id
     * @param applicationId
     */
    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    /**
     * get the application password
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set the application password
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * get the charging occurrence subscriber user id
     * @return
     */
    public String getSubscriberId() {
        return subscriberId;
    }

    /**
     * Set the charging occurrence subscriber user id
     * @param subscriberId
     */
    public void setSubscriberId(String subscriberId) {
        this.subscriberId = subscriberId;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("ChargingRequestCommonParameters{")
                .append("applicationId='").append(applicationId).append('\'')
                .append(", password='").append(password).append('\'')
                .append(", subscriberId='").append(subscriberId).append('\'')
                .append('}').toString();
    }
}
