/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.samples.api.caas.messages;

/**
 * $LastChangedDate: $
 * $LastChangedBy: $
 * $LastChangedRevision: $
 */
public class ChargingNotificationRequest {

    private String externalTrxId;
    private String internalTrxId;
    private String referenceId;
    private String paidAmount;
    private String totalAmount;
    private String balanceDue;
    private String currency;
    private String statusCode;
    private String statusDetail;
    private String timeStamp;

    public String getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(String paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getExternalTrxId() {
        return externalTrxId;
    }

    public void setExternalTrxId(String externalTrxId) {
        this.externalTrxId = externalTrxId;
    }

    public String getInternalTrxId() {
        return internalTrxId;
    }

    public void setInternalTrxId(String internalTrxId) {
        this.internalTrxId = internalTrxId;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getStatusDetail() {
        return statusDetail;
    }

    public void setStatusDetail(String statusDetail) {
        this.statusDetail = statusDetail;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getBalanceDue() {
        return balanceDue;
    }

    public void setBalanceDue(String balanceDue) {
        this.balanceDue = balanceDue;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("ChargingNotificationRequest{")
                .append("paidAmount='").append(paidAmount).append('\'')
                .append(", externalTrxId='").append(externalTrxId).append('\'')
                .append(", internalTrxId='").append(internalTrxId).append('\'')
                .append(", referenceId='").append(referenceId).append('\'')
                .append(", totalAmount='").append(totalAmount).append('\'')
                .append(", balanceDue='").append(balanceDue).append('\'')
                .append(", currency='").append(currency).append('\'')
                .append(", statusCode='").append(statusCode).append('\'')
                .append(", statusDetail='").append(statusDetail).append('\'')
                .append(", timeStamp='").append(timeStamp).append('\'')
                .append('}').toString();
    }
}