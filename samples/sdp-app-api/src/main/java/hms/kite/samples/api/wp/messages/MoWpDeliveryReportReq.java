/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.samples.api.wp.messages;

public class MoWpDeliveryReportReq {

    private String destinationAddress;
    private String requestId;
    private String timeStamp;
    private String deliverStatus;

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getDeliverStatus() {
        return deliverStatus;
    }

    public void setDeliverStatus(String deliverStatus) {
        this.deliverStatus = deliverStatus;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("MoWpDeliveryReportReq{")
                .append("deliverStatus='").append(deliverStatus).append('\'')
                .append(", destinationAddress='").append(destinationAddress).append('\'')
                .append(", requestId='").append(requestId).append('\'')
                .append(", timeStamp='").append(timeStamp).append('\'')
                .append('}').toString();
    }
}