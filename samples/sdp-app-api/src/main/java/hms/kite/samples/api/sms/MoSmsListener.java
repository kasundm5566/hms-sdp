/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.samples.api.sms;

import hms.kite.samples.api.sms.messages.MoSmsReq;

import javax.servlet.ServletConfig;

/**
 * Implement this interface to receive MoSmRequests and mention the implemented fully qualified class name in web xml
 * as bellow. Here it is assumed that implemented class name is hms.kite.samples.client.SimpleClient.
 * <servlet>
 * <servlet-name>moReceiver</servlet-name>
 * <servlet-class>hms.kite.samples.api.sms.MoSMsReceiver</servlet-class>
 * <init-param>
 * <param-name>smsReceiver</param-name>
 * <param-value>hms.kite.samples.client.SimpleClient</param-value>
 * </init-param>
 * </servlet>
 *
 * <servlet-mapping>
 * <servlet-name>moReceiver</servlet-name>
 * <url-pattern>/mo-receiver</url-pattern>
 * </servlet-mapping>
 */
public interface MoSmsListener {

    /**
     * Override this for initializing purposes if any
     */
    public void init(ServletConfig servletConfig);

    /**
     * Override this method to Receive MoSmsRequests.
     * @param moSmsReq  Mobile Originated (MO), sms received from the SDP to application
     */
    public void onReceivedSms(MoSmsReq moSmsReq);
}
