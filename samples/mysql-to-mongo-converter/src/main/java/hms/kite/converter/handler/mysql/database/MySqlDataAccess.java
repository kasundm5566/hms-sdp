package hms.kite.converter.handler.mysql.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * 
 * DataManipulatorMySQL class simply connects to MySQL database and it retrieves
 * required data.
 * 
 * @author dulith
 * @version 1.0
 */

public class MySqlDataAccess {

	/**
	 * Calls readDataMySql method.
	 * 
	 * @param args
	 */


	public static Statement stmt = null;

	/**
	 * Gets a Query, is executed by this method and result set is returned !
	 * 
	 * @param queryStatement
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */

	public ResultSet executeQuery(String queryStatement)
			throws ClassNotFoundException, SQLException {

		// Make a connection to JDBC Driver

		// not a good practice ! Connection leak can happen.

		ConnectionProvider connectionProvider = new ConnectionProvider();
		 Connection conn = connectionProvider.conn;

		stmt = conn.createStatement();

		ResultSet rs = stmt.executeQuery(queryStatement);

		while(rs.next()) {
			System.out.println(rs.getString("app_name") + "\t"  + rs.getString("keyword") );
		}
		
		return rs;

	}

}