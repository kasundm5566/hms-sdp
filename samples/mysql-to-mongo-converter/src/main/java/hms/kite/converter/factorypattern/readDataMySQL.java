package hms.kite.converter.factorypattern;

public interface readDataMySQL {
	
	public abstract void readDataMySQL();
	
	public abstract void convertToJSONObject();

}
