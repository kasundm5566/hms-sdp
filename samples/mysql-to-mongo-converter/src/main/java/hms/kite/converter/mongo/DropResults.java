package hms.kite.converter.mongo;

import java.net.UnknownHostException;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;
import com.mongodb.MongoException;

/**
 * DropResults class, manipulates deletion of data. This is designed for testing
 * purposes. Database and Collection names should be changed according to your
 * aspect.
 * 
 * @author dulith
 * @version 1.0
 */

public class DropResults {

	public static void main(String[] args) {

		try {

			Mongo mongo = new Mongo("localhost", 27017);
			DB db = mongo.getDB("EtisalatApplicationUser");

			DBCollection collection = db.getCollection("user");

			collection.drop();

			System.out.println("Done");

		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (MongoException e) {
			e.printStackTrace();
		}

	}

}
