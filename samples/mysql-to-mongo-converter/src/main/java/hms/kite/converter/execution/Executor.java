package hms.kite.converter.execution;

import java.net.UnknownHostException;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.simple.JSONObject;

import hms.kite.converter.handler.mongo.database.MongoDataAccess;
import hms.kite.converter.handler.mysql.database.MySqlDataAccess;

/**
 * 
 * @author dulith
 * @version 1.0
 * 
 *          Executor class manipulates the execution of this application.
 */
public class Executor {

	int id, develop_id = 0;
	String app_id, password, created_date, app_name, keyword = "";

	/**
	 * Main method, Invokes accessMongo, convertToJsonObject and accessMySQL
	 * methods. SQL query string is passed to accessMySQL method.
	 * 
	 * @param args
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws UnknownHostException
	 */

	public static void main(String[] args) throws ClassNotFoundException,
			SQLException, UnknownHostException {

		MySqlDataAccess accessMySQL = new MySqlDataAccess();
		Executor execute = new Executor();
		MongoDataAccess accessMongo = new MongoDataAccess();

//		accessMongo.insert(execute.convertToJsonObject(accessMySQL
//				.executeQuery("SELECT * FROM ApplicationUser;")));
		
		accessMySQL.executeQuery("SELECT * FROM ApplicationUser;");
		
		// accessMySQL.executeQuery("SELECT * FROM Customer;");

	}

	/**
	 * convertToJsonObject method converts resultSet objects to JSON Objects.
	 * 
	 * @param resultSet
	 * @return
	 * @throws SQLException
	 */

	public JSONObject convertToJsonObject(ResultSet resultSet)
			throws SQLException {

		// Values at ResultSet...
		JSONObject jsonObject = new JSONObject();

		while (resultSet.next()) {
			id = resultSet.getInt("id");
			app_id = resultSet.getString("app_id");
			password = resultSet.getString("password");
			develop_id = resultSet.getInt("develop_id");
			created_date = resultSet.getString("created_date");
			app_name = resultSet.getString("app_name");
			keyword = resultSet.getString("keyword");

			// Convert to jsonObject type..

			jsonObject.put("id", id);

			jsonObject.put("app_id", app_id);

			jsonObject.put("password", password);

			jsonObject.put("develop_id", develop_id);

			jsonObject.put("created_date", created_date);

			jsonObject.put("app_name", app_name);

			jsonObject.put("keyword", keyword);

		}
		return jsonObject;

	}

}
