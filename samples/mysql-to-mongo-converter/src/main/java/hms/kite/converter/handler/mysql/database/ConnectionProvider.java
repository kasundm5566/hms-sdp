package hms.kite.converter.handler.mysql.database;

import java.io.IOException;
import java.io.InputStream;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ResourceBundle;

import com.mysql.jdbc.Connection;

public class ConnectionProvider {

	public static Connection conn = null;

    static Properties prop = new Properties();
    static {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream stream = loader.getResourceAsStream("db.properties");
        try {
            prop.load(stream);
        } catch (IOException e) {

        }
    }

	public ConnectionProvider() throws ClassNotFoundException,
			SQLException {
		Class.forName(prop.getProperty("JDBC_DRIVER"));

		conn = (Connection) DriverManager.getConnection(
                prop.getProperty("DB_URL"),
                prop.getProperty("DB_USERNAME"),
                prop.getProperty("DB_PASSWORD"));
	}

}
