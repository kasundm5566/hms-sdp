package hms.kite.converter.handler.mongo.database;

import java.io.BufferedWriter;
import java.io.IOException;
import java.net.UnknownHostException;

import org.json.simple.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.util.JSON;

/**
 * 
 * @author dulith
 * @version 1.0
 * 
 *          MongoDataAccess class, manipulates receiving required data types.
 *          Namely id, app_id, password, develop_id, created_date, app_name and
 *          keyword. Finally read data is written to Mongo Database.
 * 
 */

public class MongoDataAccess {
	BufferedWriter bw = null;

	/**
	 * 
	 * getValuesFromMySql method accepts values from the MySQL database Accepted
	 * values will be sent to Mongo Database
	 * 
	 * @param id
	 *            receives id value from MySQL database
	 * @param app_id
	 *            receives app_id value from MySQL database
	 * @param password
	 *            receives password value from MySQL database
	 * @param develop_id
	 *            receives develop_id value from MySQL database
	 * @param created_date
	 *            receives created_date value from MySQL database
	 * @param app_name
	 *            receives app_name value from MySQL database
	 * @param keyword
	 *            receives keyword value from MySQL database
	 * @throws IOException
	 *             receives id value from MySQL database
	 */

	public void insert(JSONObject getJsonObject) throws UnknownHostException {

		Mongo mongo = new Mongo("localhost", 27017);
		DB db = mongo.getDB("EtisalatApplicationUser");
		DBCollection collection = db.getCollection("user");

		DBObject dbObject = (DBObject) JSON.parse(getJsonObject.toJSONString());

		collection.insert(dbObject);

		DBCursor cursorDoc = collection.find();

		while (cursorDoc.hasNext()) {
			System.out.println(cursorDoc.next());
			System.out.println("\n");
		}

	}

	public void update(String keyword, String newValue)
			throws UnknownHostException {

		// Locate the existing value using the id and will be replaced by the
		// newValue;

		Mongo mongo = new Mongo("localhost", 27017);
		DB db = mongo.getDB("EtisalatApplicationUser");
		DBCollection collection = db.getCollection("user");

		// According to the Parameters !

		BasicDBObject updateQuery = new BasicDBObject();
		updateQuery
				.append("$set", new BasicDBObject().append("clients", "888"));

		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.append("type", "vps");

		collection.updateMulti(searchQuery, updateQuery);

	}

}
