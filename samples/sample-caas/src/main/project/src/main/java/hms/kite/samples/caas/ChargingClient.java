/*
 *   (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.samples.caas;

import hms.kite.samples.api.SdpException;
import hms.kite.samples.api.caas.ChargingRequestSender;
import hms.kite.samples.api.caas.messages.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Charging Client for send direct-debit request/ get payment-instrument-list
 */
public class ChargingClient {

    private static final String PAYMENT_INS_LIST_KEYWORD = "pay-ins-list";
    private static final String DIRECT_DEBIT_KEYWORD = "direct-debit";

    private static String requestUrl;
    private static String appId;
    private static String password;
    private static String subscriberId;
    private static String type;
    private static String payInsName;
    private static String amount;
    private static String currency;
    private static String extTrxId;
    private static String allowPartialPayments;
    private static String allowOverPayments;
    private static String extra;
    private static String tillNo;

    public static void main(String[] args) {
        ChargingClient chargingClient = new ChargingClient();

        try {
            if (DIRECT_DEBIT_KEYWORD.equals(args[0])) {
                initializeDirectDebitRequestParams(args);
                chargingClient.sendDirectDebitRequest(requestUrl, appId, password, subscriberId, payInsName,
                        allowPartialPayments, allowOverPayments, currency, amount, extTrxId, tillNo);

            } else if (PAYMENT_INS_LIST_KEYWORD.equals(args[0])) {
                initializePaymentInsListRequestParams(args);
                chargingClient.sendPaymentInstrumentListRequest(requestUrl, appId, password, subscriberId, type);

            }

        } catch (MalformedURLException e){
            System.out.println("Error Occurred due to :" + e);

        } catch (SdpException e){
            System.out.println("Error Occurred due to :" + e);

        } catch (Exception e){
            System.out.println("Error Occurred due to :" + e);
            e.printStackTrace();

        }
    }


    /**
     * Send Direct Debit Request
     * @param requestUrl - direct debit request URL
     * @param appId - application id
     * @param password - Password of the application
     * @param subscriberId - subscriber id
     * @param payInsName - payment instrument name
     * @param allowOverPayments - is enable over payment
     * @param allowPartialPayments - is enable partial payment
     * @param currency - specific currency
     * @param amount - request amount
     * @param extTrxId - external transaction id
     * @param tillNo - Till no as extra for buy goods
     * @throws java.net.MalformedURLException
     * @throws hms.kite.samples.api.SdpException
     */
    private void sendDirectDebitRequest(String requestUrl, String appId, String password, String subscriberId, String payInsName,
                                        String allowPartialPayments, String allowOverPayments, String currency, String amount,
                                        String extTrxId, String tillNo)
            throws MalformedURLException, SdpException {


        ChargingRequestSender chargingRequestSender = new ChargingRequestSender(new URL(requestUrl));

        // Create Direct Debit Request.
        DirectDebitRequest debitRequest = new DirectDebitRequest();
        debitRequest.setApplicationId(appId);
        debitRequest.setPassword(password);
        debitRequest.setSubscriberId(subscriberId);
        debitRequest.setPaymentInstrumentName(payInsName);
        debitRequest.setAllowPartialPayments(allowPartialPayments);
        debitRequest.setAllowOverPayments(allowOverPayments);
        debitRequest.setAmount(amount);
        debitRequest.setExternalTrxId(extTrxId);
        debitRequest.setCurrency(currency);
        debitRequest.setCurrency(currency);

        // Add Till No for Extra only for Buy Goods Payment Instrument
        if (tillNo != null) {
            Map<String, String> extraMap = new HashMap<String, String>();
            extraMap.put("tillNo", tillNo);
            debitRequest.setExtra(extraMap);
        }

        // Send Charging Request.
        System.out.println("\n\nSending Direct Debit Request : " + printDirectDebitReqParams());
        ChargingRequestResponse chargingRequestResponse = chargingRequestSender.sendChargingRequest(debitRequest);
        System.out.println("\n\nReceived Response : " + chargingRequestResponse.toString());
    }


    /**
     * Send Payment Instrument List with following parameters.
     * @param url payment instrument list url
     * @param appId - application id
     * @param password - Password of the application
     * @param subscriberId - subscriber id
     * @param type - payment instrument query type (possible values- All/Individual)
     * @throws java.net.MalformedURLException
     * @throws hms.kite.samples.api.SdpException
     */
    private void sendPaymentInstrumentListRequest(String url, String appId, String password, String subscriberId, String type)
            throws MalformedURLException, SdpException {

        ChargingRequestSender paymentInReqSender = new ChargingRequestSender(new URL(url));

        // Create Payment Instrument Request
        PaymentInstrumentListRequest paymentInsListReq = new PaymentInstrumentListRequest();
        paymentInsListReq.setApplicationId(appId);
        paymentInsListReq.setPassword(password);
        paymentInsListReq.setSubscriberId(subscriberId);
        paymentInsListReq.setType(type);

        // Send Payment Instrument List Request
        System.out.println("\n\nSending Payment Instrument Request : " + printPayInsListReqParams());
        PaymentInstrumentListResponse paymentInsListRes = paymentInReqSender.sendPaymentInstrumentListReq(paymentInsListReq);
        System.out.println("\n\nReceived Response  : " + paymentInsListRes);

    }

    /**
     * Initialization of Direct Debit Request Parameters.
     * @param args
     */
    private static void initializeDirectDebitRequestParams(String [] args) {
        requestUrl = args[1];
        appId = args[2];
        password = args[3];
        subscriberId = args[4];
        payInsName = args[5];
        allowPartialPayments =  args[6];
        allowOverPayments =  args[7];
        currency = args[8];
        amount = args[9];
        extTrxId = args[10];

        if (args.length > 11) {
            extra = args[11];
            tillNo = extra.split(":")[1];
        }
    }

    /**
     * Initialization of Payment Instrument List Request Parameters.
     * @param args
     */
    private static void initializePaymentInsListRequestParams(String[] args) {
        requestUrl = args[1];
        appId = args[2];
        password = args[3];
        subscriberId = args[4];
        type = args[5];
    }

    private String printDirectDebitReqParams() {
        return new StringBuilder()
                .append("ChargingRequest{")
                .append("applicationId='").append(appId).append('\'')
                .append(", password='").append(password).append('\'')
                .append(", subscriberId='").append(subscriberId).append('\'')
                .append(", externalTrxId='").append(extTrxId).append('\'')
                .append(", paymentInstrumentName='").append(payInsName).append('\'')
                .append(", amount='").append(amount).append('\'')
                .append(", allowOverPayments='").append(allowOverPayments).append('\'')
                .append(", allowPartialPayments='").append(allowPartialPayments).append('\'')
                .append(", Till No='").append(tillNo).append('\'')
                .append(", currency='").append(currency).append('\'')
                .append('}').toString();
    }


    private String printPayInsListReqParams() {
        return new StringBuilder()
                .append("PaymentInstrumentListRequest{")
                .append("applicationId='").append(appId).append('\'')
                .append(", password='").append(password).append('\'')
                .append(", subscriberId='").append(subscriberId).append('\'')
                .append(", type='").append(type).append('\'')
                .append('}').toString();
    }

}
