/*
 *   (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */

package hms.kite.samples.caas;


import hms.kite.samples.api.caas.CaasNotificationListener;
import hms.kite.samples.api.caas.messages.ChargingNotificationRequest;

/**
 * Handle Charging Notification
 */
public class ChargingNotificationClient implements CaasNotificationListener {

    /**
     * Process Receive Chraging Notification.
     * @param chargingNotificationReq
     */
    @Override
    public void onReceiveCaasNotification(ChargingNotificationRequest chargingNotificationReq) {
        System.out.println("\nCaas Charging Notification Received for generate request ====>>: " + chargingNotificationReq.toString() );
    }

}
