#########################################################################################
#   (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited
#   All Rights Reserved.
#
#   These materials are unpublished, proprietary, confidential source code of
#   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
#   of hSenid Mobile Solutions (Pvt) Limited.
#
#   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
#   property rights in these materials.
#
#########################################################################################
#   Description  : Send Direct Debit Request (M-Pesa-Buygoods).
#########################################################################################
#!/bin/bash
DD_URL="http://localhost:7000/caas/direct/debit"
APP_ID="APP_000001"
PASSWORD="8edb0c038b3295fdd271de"
SUBSCRIBER_ID="tel:254712345600"
PAYMENT_INS_NAME="M-Pesa-Buygoods"
ALLOW_PARTIAL_PAYMENTS="allow"
ALLOW_OVER_PAYMENTS="allow"
AMOUNT="25.00"
CURRENCY="KES"
EXTERNAL_TRX_ID="456123"
EXTRA="tillNo:200206"

# Send Direct Debit request with above parameters
cd ../../target
echo "########################### Sending Direct Debit request ###################################"
java -jar sample-caas.jar direct-debit $DD_URL $APP_ID $PASSWORD $SUBSCRIBER_ID $PAYMENT_INS_NAME $ALLOW_PARTIAL_PAYMENTS $ALLOW_OVER_PAYMENTS $CURRENCY $AMOUNT $EXTERNAL_TRX_ID $EXTRA

# Start Charging Notification Listener
cd ../
echo ""
echo "########################### Starting Charging Notification Receiver ######################################"
echo ""
mvn jetty:run