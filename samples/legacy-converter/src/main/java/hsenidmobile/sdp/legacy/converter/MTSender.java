/*
 *
 *  *
 *  *  *   (C) Copyright 2013-2014 hSenid Software International (Pvt) Limited.
 *  *  *   All Rights Reserved.
 *  *  *
 *  *  *   These materials are unpublished, proprietary, confidential source code of
 *  *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *  *   of hSenid Software International (Pvt) Limited.
 *  *  *
 *  *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *  *   property rights in these materials.
 *  *  *
 *  *
 *
 */
package hsenidmobile.sdp.legacy.converter;

import hms.kite.samples.api.SdpException;
import hms.kite.samples.api.sms.SmsRequestSender;
import hms.kite.samples.api.sms.messages.MtSmsReq;
import hms.kite.samples.api.sms.messages.MtSmsResp;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * MT message sender.
 * </p>
 * 
 * @author manuja
 */
public class MTSender {

	private static final Logger logger = LoggerFactory.getLogger(MTSender.class);
	
	private SmsRequestSender[] requestSender;
	AtomicInteger counter = new AtomicInteger(0);
	private final int poolSize;

	/**
	 * <p>
	 * Constructor of MT Sender.
	 * </p>
	 * 
	 * @param url receiver url
	 * @param inPoolSize sender pool size
	 * 
	 * @throws MalformedURLException throws when malformed error occurred
	 */
	public MTSender(final String url, final int inPoolSize) throws MalformedURLException {
		poolSize = inPoolSize;
		requestSender = new SmsRequestSender[poolSize];

		for (int i = 0; i < poolSize; i++) {
			requestSender[i] = new SmsRequestSender(new URL(url));
		}
	}

	/**
	 * <p>
	 * Send MT Message.
	 * <p>
	 * 
	 * @param address destination address
	 * @param applicationId application id
	 * @param password application password
	 * @param message message content
	 * @return mt sms response instance
	 * 
	 * @throws IOException throws when IO error occurred
	 * @throws SdpException throws when SDP error occurred
	 */
	public MtSmsResp sendMessage(final String address, final String applicationId, final String password,
			final String message) throws IOException, SdpException {

		MtSmsReq mtSmsReq = new MtSmsReq();
		mtSmsReq.setMessage(message);
		mtSmsReq.setApplicationId(applicationId);
		mtSmsReq.setPassword(password);
		mtSmsReq.setDestinationAddresses(Arrays.asList(address));

		logger.debug("Sending response to new SDP [ {} ]", mtSmsReq);
		int currentCount = counter.getAndIncrement();
		if (currentCount >= poolSize) {
			counter.set(0);
			currentCount = 0;
		}
		return requestSender[currentCount].sendSmsRequest(mtSmsReq);
	}
}
