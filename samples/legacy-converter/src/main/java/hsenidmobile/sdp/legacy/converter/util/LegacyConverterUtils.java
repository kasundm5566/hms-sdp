/*
 *
 *  *
 *  *  *   (C) Copyright 2013-2014 hSenid Software International (Pvt) Limited.
 *  *  *   All Rights Reserved.
 *  *  *
 *  *  *   These materials are unpublished, proprietary, confidential source code of
 *  *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *  *   of hSenid Software International (Pvt) Limited.
 *  *  *
 *  *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *  *   property rights in these materials.
 *  *  *
 *  *
 *
 */
package hsenidmobile.sdp.legacy.converter.util;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Legacy converter utils.
 * </p>
 * 
 * @author manuja
 */
public class LegacyConverterUtils {

	private static final Logger logger = LoggerFactory.getLogger(LegacyConverterUtils.class);

	public static final String CONFIGURATION_BUNDLE_NAME = "configuration";
	private static ResourceBundle resourceBundle = ResourceBundle.getBundle(CONFIGURATION_BUNDLE_NAME);

	public static final String NEW_SDP_URL = get("new.sdp.url");
	public static final int MT_SENDER_POOL_SIZE = Integer.parseInt(get("mt.sender.pool.size"));
	public static final String SUCCESS_CODE = get("success.response.code");

	/**
	 * <p>
	 * Resolve a message by a key and argument replacements.
	 * </p>
	 * 
	 * @see MessageFormat#format(String, Object...)
	 * @param key
	 *            the message to look up
	 * @param arguments
	 *            optional message arguments
	 * @return the resolved message
	 **/
	public static String get(final String key, final Object... arguments) {
		try {
			if (arguments != null) {
				return MessageFormat.format(resourceBundle.getString(key), arguments);
			}
			return resourceBundle.getString(key);
		} catch (MissingResourceException e) {
			logger.error("Message key not found: " + key);
			return '!' + key + '!';
		}
	}
}
