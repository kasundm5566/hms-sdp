/*
 *
 *  *
 *  *  *   (C) Copyright 2013-2014 hSenid Software International (Pvt) Limited.
 *  *  *   All Rights Reserved.
 *  *  *
 *  *  *   These materials are unpublished, proprietary, confidential source code of
 *  *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *  *   of hSenid Software International (Pvt) Limited.
 *  *  *
 *  *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *  *   property rights in these materials.
 *  *  *
 *  *
 *
 */
package hsenidmobile.sdp.legacy.converter;

import static hsenidmobile.sdp.legacy.converter.util.LegacyConverterUtils.get;

import hms.kite.samples.api.SdpException;
import hms.kite.samples.api.sms.messages.MtSmsResp;
import hsenidmobile.sdp.legacy.converter.messages.SmsAoRequestMessage;
import hsenidmobile.sdp.legacy.converter.util.LegacyConverterUtils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.text.MessageFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * AO message sender.
 * </p>
 * 
 * @author manuja
 */
public class AOReceiver extends HttpServlet {

	private static final long serialVersionUID = -352501753708119559L;
	private static final Logger logger = LoggerFactory.getLogger(AOReceiver.class);
	private static final String ADDRESS_KEY = "address";
	private static final String MESSAGE_KEY = "message";
	private static final String AUTHORIZATION_KEY = "Authorization";
	private static final String ERROR_RESPONSE = "UNKNOWN";
	private static final String SDP_V1_0_BROADCAST_MESSAGE_KEY = "list:all_registered";
	private static final String SDP_V2_0_BROADCAST_MESSAGE_KEY = "tel:all";
	private MTSender requestSender;
	
	@Override
	public void init() throws ServletException {
		super.init();
		try {
			requestSender = new MTSender(LegacyConverterUtils.NEW_SDP_URL,
					LegacyConverterUtils.MT_SENDER_POOL_SIZE);
		} catch (MalformedURLException e) {
			logger.error("Error while creating request sender", e);
		}
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			handleSmsRequest(req, resp);
		} catch (Exception e) {
			writeResponse(resp, ERROR_RESPONSE, ERROR_RESPONSE);
			logger.error("Error while processing MT request", e);
		}
		resp.getWriter().flush();
	}

	private void handleSmsRequest(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		String address = req.getParameter(ADDRESS_KEY);
		String message = req.getParameter(MESSAGE_KEY);

		SmsAoRequestMessage smsAoRequestMessage = new SmsAoRequestMessage();
		smsAoRequestMessage.setMessage(message);

		validateAORequest(req, resp, address, message, smsAoRequestMessage);
		logger.debug("New SMS Received [{}]", smsAoRequestMessage);
		sendSuccessMessage(resp, message, smsAoRequestMessage);
	}

	private void validateAORequest(HttpServletRequest req, HttpServletResponse resp, String address, String message,
			SmsAoRequestMessage smsAoRequestMessage) throws Exception {
		logger.debug("MT Response received : destination address - [{}], response message - [{}]", address, message);
		try {
			validateAndSetAuthentication(req, resp, smsAoRequestMessage);
			logger.debug("Authentication validation success");
			validateAndSetAddress(resp, address, smsAoRequestMessage);
			logger.debug("Address validation success success");
			validateAndSetMessage(resp, message, smsAoRequestMessage);
			logger.debug("Messag validation success");
		} catch (Exception e) {
			logger.error("Error while validating received message : [{}]", smsAoRequestMessage);
			throw e;
		}
	}

	private void sendSuccessMessage(HttpServletResponse resp, String message, SmsAoRequestMessage smsAoRequestMessage)
			throws MalformedURLException, SdpException, IOException {
		MtSmsResp mtSmsResp = sendMTRequest(message, smsAoRequestMessage);
		if (mtSmsResp.getStatusCode().equals(LegacyConverterUtils.SUCCESS_CODE)) {
			writeResponse(resp, get("ao.receiver.mt.send.success.response.code"),
					get("ao.receiver.mt.send.success.response.detail"));
		} else {
			writeResponse(resp, mtSmsResp.getStatusCode(), mtSmsResp.getStatusDetail());
		}
	}

	private MtSmsResp sendMTRequest(String message, SmsAoRequestMessage smsAoRequestMessage) throws IOException,
			SdpException {
		logger.debug("Sending response to [{}]", LegacyConverterUtils.NEW_SDP_URL);
		String addresses = smsAoRequestMessage.getAddress();
		addresses = addresses.replace(SDP_V1_0_BROADCAST_MESSAGE_KEY, SDP_V2_0_BROADCAST_MESSAGE_KEY);
		return requestSender.sendMessage(addresses, smsAoRequestMessage.getAppId(), smsAoRequestMessage.getPassword(),
				message);
	}

	private void validateAndSetMessage(HttpServletResponse resp, String message, SmsAoRequestMessage smsAoRequestMessage)
			throws Exception {
		try {
			if ("".equals(message)) {
				throw new Exception();
			}
			smsAoRequestMessage.setMessage(message);
		} catch (Exception e) {
			writeResponse(resp, get("ao.receiver.message.validation.error.response.code"),
					get("ao.receiver.message.validation.error.response.detail"));
			throw e;
		}
	}

	private void validateAndSetAddress(HttpServletResponse resp, String address, SmsAoRequestMessage smsAoRequestMessage)
			throws Exception {
		try {
			smsAoRequestMessage.setAddress(address);
		} catch (Exception e) {
			writeResponse(resp, get("ao.receiver.address.validation.error.response.code"),
					get("ao.receiver.address.validation.error.response.detail"));

			throw e;
		}
	}

	private void validateAndSetAuthentication(HttpServletRequest req, HttpServletResponse resp,
			SmsAoRequestMessage smsAoRequestMessage) throws Exception {
		try {
			String[] authentication = getAppIdPassword(req.getHeader(AUTHORIZATION_KEY));
			for (String s : authentication) {
				if ("".equals(s)) {
					throw new Exception();
				}
			}
			smsAoRequestMessage.setAppId(authentication[0]);
			smsAoRequestMessage.setPassword(authentication[1]);
		} catch (Exception e) {
			writeResponse(resp, get("ao.receiver.authorization.error.response.code"),
					get("ao.receiver.authorization.error.response.detail"));

			throw e;
		}
	}

	private void writeResponse(HttpServletResponse resp, String statusCode, String statusMessage) throws IOException {
		final String AO_MSG_RESPONSE_STRING = "<mchoice_sdp_sms_response>\n   "
													+ "<version>1.0</version>\n   "
													+ "<correlator>{0}</correlator>\n   "
													+ "<status_code>{1}</status_code>\n   "
													+ "<status_message>{2}</status_message>\n"
											 + "</mchoice_sdp_sms_response>";
		long correlationId = (long) Math.random() * 1000000000L;
		resp.getWriter().write(
				MessageFormat.format(AO_MSG_RESPONSE_STRING, new Object[] { Long.valueOf(correlationId), statusCode,
						statusMessage }));
	}

	public String[] getAppIdPassword(String basicAuthHeader) {
		basicAuthHeader = basicAuthHeader.substring(6);
		String decoded = new String(Base64.decodeBase64(basicAuthHeader.getBytes()));
		return decoded.split(":");
	}
}
