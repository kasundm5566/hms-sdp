/*
 *
 *  *
 *  *  *   (C) Copyright 2013-2014 hSenid Software International (Pvt) Limited.
 *  *  *   All Rights Reserved.
 *  *  *
 *  *  *   These materials are unpublished, proprietary, confidential source code of
 *  *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *  *   of hSenid Software International (Pvt) Limited.
 *  *  *
 *  *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *  *   property rights in these materials.
 *  *  *
 *  *
 *
 */
package hsenidmobile.sdp.legacy.converter;

import static hsenidmobile.sdp.legacy.converter.util.LegacyConverterUtils.get;

import java.io.IOException;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * AT message sender.
 * </p>
 * 
 * @author manuja
 * 
 */
public class ATSender {

	private static final Logger logger = LoggerFactory.getLogger(ATSender.class);
	private static final String CONTENT_TYPE_KEY = "CONTENT_TYPE";
	private static final String ACCEPT_KEY = "ACCEPT";
	private static final String VERSION_KEY = "version";
	private static final String ADDRESS_KEY = "address";
	private static final String MESSAGE_KEY = "message";
	private static final String CORRELATOR_KEY = "correlator";
	private static final String CONTENT_TYPE = "application/x-www-form-urlencoded";
	private static final String ACCEPT = "text/xml";
    private static final String VERSION = "version";
    private static final String MESSAGE_PREFIX = "tel:";
	private static ATSender smsMessageSender = new ATSender();
	private final HttpClient httpClient;

	private ATSender() {
		this.httpClient = new HttpClient(createHttpConnectionManager());
	}

	private MultiThreadedHttpConnectionManager createHttpConnectionManager() {
		MultiThreadedHttpConnectionManager httpConnectionManager = new MultiThreadedHttpConnectionManager();
		HttpConnectionManagerParams params = new HttpConnectionManagerParams();
		params.setTcpNoDelay(Boolean.parseBoolean(get("http.client.tcp.no.delay")));
		params.setDefaultMaxConnectionsPerHost(Integer.parseInt(get("http.client.max.connections.per.host")));
		params.setMaxTotalConnections(Integer.parseInt(get("http.client.max.total.connections")));
		params.setConnectionTimeout(Integer.parseInt(get("http.client.connection.timeout")));
		httpConnectionManager.setParams(params);
		return httpConnectionManager;
	}

	/**
	 * Send AT Message.
	 * 
	 * @param url application url
	 * @param address source address
	 * @param message message content
	 *
	 * @throws IOException throws when IO Error occurred
	 */
	public void sendMessage(final String url, final String address, final String message) throws IOException {
		
		String addressWOPrefix = address.replace(MESSAGE_PREFIX, "");
		String correlator = String.valueOf(System.currentTimeMillis());

		PostMethod postMethod = new PostMethod(url);
		postMethod.addRequestHeader(CONTENT_TYPE_KEY, CONTENT_TYPE);
		postMethod.addRequestHeader(ACCEPT_KEY, ACCEPT);
		postMethod.addParameter(VERSION_KEY, VERSION);
		postMethod.addParameter(ADDRESS_KEY, addressWOPrefix);
		postMethod.addParameter(MESSAGE_KEY, message);
		postMethod.addParameter(CORRELATOR_KEY, correlator);

		logger.debug("Sending SMS to application [ address: {} ,message: {} ,correlator: {} ]", addressWOPrefix,
				message, correlator);

		this.httpClient.executeMethod(postMethod);
	}

	/**
	 * <p>
	 * Get AT Message Sender instance.
	 * </p>
	 * 
	 * @return ATSender instance
	 */
	public static ATSender getInstance() {
		if (smsMessageSender == null) {
			smsMessageSender = new ATSender();
		}
		return smsMessageSender;
	}
}
