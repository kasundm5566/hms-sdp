/*
 *
 *  *
 *  *  *   (C) Copyright 2013-2014 hSenid Software International (Pvt) Limited.
 *  *  *   All Rights Reserved.
 *  *  *
 *  *  *   These materials are unpublished, proprietary, confidential source code of
 *  *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *  *   of hSenid Software International (Pvt) Limited.
 *  *  *
 *  *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *  *   property rights in these materials.
 *  *  *
 *  *
 *
 */
package hsenidmobile.sdp.legacy.converter.messages;

/**
 * <p>
 * SMS AO Request Message.
 * </p>
 * 
 * @author manuja
 */
public class SmsAoRequestMessage {
	private String address;
	private String message;
	private String appId;
	private String password;

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getAppId() {
		return this.appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("SmsAoRequestMessage");
		sb.append("{address='").append(this.address).append('\'');
		sb.append(", message='").append(this.message).append('\'');
		sb.append(", appId='").append(this.appId).append('\'');
		sb.append(", password='").append(this.password).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
