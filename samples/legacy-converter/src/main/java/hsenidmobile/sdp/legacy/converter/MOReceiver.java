/*
 *
 *  *
 *  *  *   (C) Copyright 2013-2014 hSenid Software International (Pvt) Limited.
 *  *  *   All Rights Reserved.
 *  *  *
 *  *  *   These materials are unpublished, proprietary, confidential source code of
 *  *  *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *  *  *   of hSenid Software International (Pvt) Limited.
 *  *  *
 *  *  *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *  *  *   property rights in these materials.
 *  *  *
 *  *
 *
 */
package hsenidmobile.sdp.legacy.converter;

import static hsenidmobile.sdp.legacy.converter.util.LegacyConverterUtils.get;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import hms.kite.samples.api.sms.messages.MoSmsReq;
import hms.kite.samples.api.sms.messages.MoSmsResp;
import hsenidmobile.sdp.legacy.converter.util.LegacyConverterUtils;

/**
 * <p>
 * MO message receiver
 * </p>
 * 
 * @author manuja
 */
public class MOReceiver extends HttpServlet {

	private static final long serialVersionUID = 5715300350972295982L;
	private static final Logger logger = LoggerFactory.getLogger(MOReceiver.class);
	private static final String CONTENT_TYPE = "application/json";
	private static final String APP_URL_KEY = "appUrl";
	private final Gson gson = new Gson();
	private ExecutorService executorService;
	private static int sdpMoDeReportReceiverThreadCount;

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		initializeReceivingThreadPool();
	}

	private void initializeReceivingThreadPool() {
		this.executorService = Executors.newCachedThreadPool(new ThreadFactory() {
			public Thread newThread(Runnable r) {
				return new Thread(r, "sdp-mo-receiver-thread-" + ++sdpMoDeReportReceiverThreadCount);
			}
		});
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		logger.debug("Post request received");
		String contentType = req.getContentType();
		if ((contentType == null) || (!contentType.equals(CONTENT_TYPE))) {
			resp.setStatus(Integer.parseInt(get("mo.receiver.invalid.content.type.received.response.code")));
			resp.getWriter().println(get("mo.receiver.invalid.content.type.received.response.detail"));
			return;
		}
		processRequest(req, resp);
	}

	private void processRequest(HttpServletRequest req, HttpServletResponse resp) {
		try {
			String appUrl = req.getParameter(APP_URL_KEY);
			logger.debug("MO message receive for application url [{}]", appUrl);

			if (checkIfURLExists(appUrl)) {
				MoSmsReq moSmsReq = gson.fromJson(readStringContent(req), MoSmsReq.class);
				fireMoEvent(moSmsReq, appUrl);
				sendMOResponse(resp, LegacyConverterUtils.SUCCESS_CODE, get("mo.receiver.mo.sms.resp.success.detail"));
			} else {
				resp.sendError(HttpServletResponse.SC_NOT_FOUND);
			}
		} catch (Exception e) {
			logger.error("Error while processing MO request");
			sendMOResponse(resp, get("mo.receiver.system.error.occured.response.code"),
					get("mo.receiver.system.error.occured.response.detail"));
		}
	}

	private void sendMOResponse(final HttpServletResponse resp, final String statusCode, final String statusDetail) {
		MoSmsResp moSmsResp = new MoSmsResp();
		moSmsResp.setStatusCode(statusCode);
		moSmsResp.setStatusDetail(statusDetail);
		logger.debug("Sending response for MO [{}]", moSmsResp);
		try {
			resp.getWriter().print(gson.toJson(moSmsResp));
		} catch (IOException e) {
			logger.error("Error while sending response message to MO", e);
		}
	}

	private void fireMoEvent(final MoSmsReq moSmsReq, final String appUrl) {
		this.executorService.submit(new Runnable() {
			public void run() {
				try {
					logger.debug("Routing MO request [{}]", moSmsReq.toString());
					ATSender.getInstance().sendMessage(appUrl, moSmsReq.getSourceAddress(), moSmsReq.getMessage());
				} catch (Exception e) {
					logger.error("Error while sending message to application : ", e);
				}
			}
		});
	}

	private String readStringContent(HttpServletRequest req) throws IOException {
		InputStream is = req.getInputStream();
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));

		StringBuilder content = new StringBuilder();
		String line;
		while ((line = bufferedReader.readLine()) != null) {
			content.append(line);
		}

		return content.toString();
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.getWriter().println(get("mo.receiver.get.request.response"));
	}

	private boolean checkIfURLExists(String targetUrl) {
		HttpURLConnection httpUrlConn;
		try {
			httpUrlConn = (HttpURLConnection) new URL(targetUrl).openConnection();
			httpUrlConn.setRequestMethod("HEAD");
			httpUrlConn.setConnectTimeout(30000);
			httpUrlConn.setReadTimeout(30000);
			int responseCode = httpUrlConn.getResponseCode();
			return (responseCode == HttpURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_BAD_METHOD);
		} catch (Exception e) {
			return false;
		}
	}
}
