/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.samples.client;

import hms.common.rest.util.JsonBodyProvider;
import org.apache.cxf.jaxrs.client.WebClient;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.*;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class SampleClient {

    private static String nblUrlK = "nbl-url";

    public static void main(String[] args) throws IOException {
        Properties props = loadProperties(args[0]);
        overridePropertiesWithCommandline(props, args);

        WebClient webClient = createWebClient(props);

        HashMap<String, Object> request = createRequest(props);

        Map response = webClient.post(request, Map.class);

        logResponse(response);
    }

    private static void overridePropertiesWithCommandline(Properties props, String[] args) {
        if (args.length < 2)
            return;

        System.out.println("Overriding Properties ================================");
        for (String arg : args) {
            if (arg.contains("=")) {
                String[] overridingProps = arg.split("=");
                if (overridingProps.length > 1) {
                    System.out.printf("{%s : %s }\n", overridingProps[0], overridingProps[1]);
                    props.put(overridingProps[0], overridingProps[1]);
                }
            }
        }
        System.out.println("Overriding Properties ==========================Done");
    }

    private static HashMap<String, Object> createRequest(Properties props) throws IOException {
        HashMap<String, Object> request = new HashMap<String, Object>();
        for (Map.Entry<Object, Object> entry : props.entrySet()) {
            if (!entry.getKey().equals(nblUrlK)
                    && !entry.getKey().equals("address")) {
                request.put((String) entry.getKey(), entry.getValue());
            }
        }
        List<String> addressList = createAddressList(props);
        if (0 < addressList.size()) {
            request.put("address", addressList);
        }
        return request;
    }

    private static void logResponse(Map response) {
        System.out.println("=================== Response ==========================");
        System.out.println("Response : " + response);
        System.out.println("=======================================================");

    }

    private static List<String> createAddressList(Properties props) throws IOException {
        List<String> address = new ArrayList<String>();
        for (Map.Entry<Object, Object> entry : props.entrySet()) {
            if (entry.getKey().equals("address")) {
                StringTokenizer st = new StringTokenizer((String) entry.getValue(), ",");
                while (st.hasMoreTokens()) {
                    address.add(st.nextToken().trim());
                }
            }
        }
        return address;
    }

    private static WebClient createWebClient(Properties props) {
        List<Object> bodyProviders = new ArrayList<Object>();
        bodyProviders.add(new JsonBodyProvider());
        WebClient webClient = WebClient.create(props.getProperty(nblUrlK), bodyProviders);
        webClient.header("Content-Type", "application/json");
        webClient.accept("application/json");
        return webClient;
    }

    private static Properties loadProperties(String resourceName) {
        ResourceBundle rb = ResourceBundle.getBundle(resourceName);
        Properties properties = new Properties();
        Set<String> keySet = rb.keySet();
        for (String key : keySet) {
            properties.put(key, rb.getString(key));
        }
        return properties;
    }
}
