/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.appzone.db.comparison;

import com.mongodb.BasicDBList;
import com.mongodb.DBObject;
import hms.appzone.db.util.ComparisonResult;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * User: Pasan Buddhika
 * Date: 10/3/13
 */
public abstract class Comparator {
    protected DBObject mongoData = null;
    protected ResultSet mysqlData = null;

    public abstract ComparisonResult compare(DBObject mongoData, ResultSet mysqlData);

    protected void compareStringField(String mongoKey, String mySqlKey, ComparisonResult comparisonResult) throws SQLException {
        if (!mongoData.get(mongoKey).equals(mysqlData.getString(mySqlKey))) {
            updateMismatchResult(mongoKey, mongoData.get(mongoKey).toString(), mysqlData.getString(mySqlKey), comparisonResult);
        }
    }

    protected void compareStaticStringValue(String mongoKey, String value, ComparisonResult comparisonResult) {
        if (!mongoData.get(mongoKey).equals(value)) {
            updateMismatchResult(mongoKey, mongoData.get(mongoKey).toString(), value, comparisonResult);
        }
    }

    protected void compareBooleanField(String mongoKey, String mysqlKey, ComparisonResult comparisonResult) throws SQLException {
        if (!mongoData.get(mongoKey).equals(mysqlData.getBoolean(mysqlKey))) {
            updateMismatchResult(mongoKey, mongoData.get(mongoKey).toString(), String.valueOf(mysqlData.getBoolean(mysqlKey)), comparisonResult);
        }
    }

    protected void checkStaticBooleanValue(String mongoKey, boolean value, ComparisonResult comparisonResult) {
        if (!mongoData.get(mongoKey).equals(value)) {
            updateMismatchResult(mongoKey, mongoData.get(mongoKey).toString(), String.valueOf(value), comparisonResult);
        }
    }

    protected void compareDates(String mongoKey, String mysqlKey, ComparisonResult comparisonResult) throws SQLException {
        if (!mongoData.get(mongoKey).equals(mysqlData.getDate(mysqlKey))) {
            updateMismatchResult(mongoKey, mongoData.get(mongoKey).toString(), mysqlData.getDate(mysqlKey).toString(), comparisonResult);
        }
    }

    protected void checkValueInArray(String mongoKey, String value, ComparisonResult comparisonResult) {
        BasicDBList values = (BasicDBList) mongoData.get(mongoKey);
        Object[] valueList =  values.toArray();
        for (int i=0; i < valueList.length; i++) {
            if (valueList[i].equals(value)) {
                return;
            }
        }
        comparisonResult.addMismatchResult(mongoKey, "["+value+"] not in "+mongoKey+" list");
    }

    protected void updateMismatchResult(String fieldName, String mongoVal, String mysqlVal, ComparisonResult comparisonResult) {
        comparisonResult.addMismatchResult(fieldName, "Kite : [" + mongoVal + "] Aventura : [" + mysqlVal + "]");
    }
}
