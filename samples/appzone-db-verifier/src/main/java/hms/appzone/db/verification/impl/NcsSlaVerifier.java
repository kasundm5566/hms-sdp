/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.appzone.db.verification.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import hms.appzone.db.comparison.Comparator;
import hms.appzone.db.comparison.impl.NcsSlaComparator;
import hms.appzone.db.comparison.impl.RoutingKeyComparator;
import hms.appzone.db.connection.MongoConnector;
import hms.appzone.db.connection.MysqlConnector;
import hms.appzone.db.util.ComparisonResult;
import hms.appzone.db.util.HtmlGenerator;
import hms.appzone.db.verification.Verifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * User: Pasan Buddhika
 * Date: 10/7/13
 */
public class NcsSlaVerifier implements Verifier {
    private static final Logger logger = LoggerFactory.getLogger("ncs_sla_verification_result");
    public void verify() throws SQLException, IOException {
        MysqlConnector mysqlConnector = new MysqlConnector();
        logger.info("Verifying ncs-sla Migration . . .");
        int count = 0;

        MongoConnector mongoConnector = new MongoConnector();
        DBCursor res = mongoConnector.getResults(new BasicDBObject(), "ncs_sla");
        HtmlGenerator htmlGenerator = new HtmlGenerator("ncs-sla");

        while (res.hasNext()) {
            DBObject dbObject = res.next();

            String appId = dbObject.get("app-id").toString();
            ResultSet resultSet = mysqlConnector.getResult("SELECT ncs_sla.*, rk.keyword, app.app_id as active_app_id, app.created_time, app.app_name , app.subs_reg_sla_type, prov_sms_mt_sla.mpd as sms_mt_tpd, prov_sms_mt_sla.mps as sms_mt_tps ,prov_sms_mo_sla.mpd as sms_mo_tpd , \n" +
                    "prov_sms_mo_sla.mps as sms_mo_tps, prov_sms_mo_sla.connection_url  " +
                    "FROM ncs_sla ncs_sla " +
                    "INNER JOIN app app ON ncs_sla.app_id = app.id " +
                    "INNER JOIN prov_sms_mo_sla prov_sms_mo_sla  ON prov_sms_mo_sla.id = ncs_sla.mo_sla " +
                    "INNER JOIN prov_sms_mt_sla prov_sms_mt_sla ON prov_sms_mt_sla.id = ncs_sla.mt_sla " +
                    "INNER JOIN routing_key rk ON rk.active_app_id = app.app_id " +
                    "where app.app_id like 'PD%' and app.current_state = 'ACTIVE' and ncs_sla.current_ncs_sla_state = 'ACTIVE_PD' and app.app_id='"+appId+"'");

            if (resultSet.next()) {
                String ncs_type = dbObject.get("ncs-type").toString();
                Comparator comparator = new NcsSlaComparator();
                ComparisonResult comparisonResult = comparator.compare(dbObject, resultSet);
//                logger.info("App-id : [{}], mismatch count : [{}], details : [{}]", new Object[]{appId, comparisonResult.getMismatchCount(), comparisonResult.getMisMatchResults()});
                htmlGenerator.appendResult(appId + " - " + ncs_type + " ncs-sla" , comparisonResult);
            }
            else {
                System.out.println(appId);
            }
            resultSet.close();
            count++;
        }
        htmlGenerator.generateFile();
        res.close();
        mysqlConnector.closeConnection();
        logger.info("ncs-sla count in Kite : [{}]",  count);
    }
}
