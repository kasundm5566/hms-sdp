/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.appzone.db.comparison.impl;

import com.mongodb.DBObject;
import hms.appzone.db.comparison.Comparator;
import hms.appzone.db.util.ComparisonResult;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * User: Pasan Buddhika
 * Date: 10/4/13
 */
public class RoutingKeyComparator extends Comparator {

    public static String MONGO_ID = "_id";
    public static String MONGO_SP_ID = "sp-id";
    public static String MONGO_OPERATOR = "operator";
    public static String MONGO_SHORT_CODE = "shortcode";
    public static String MONGO_KEYWORD = "keyword";
    public static String MONGO_APP_ID = "app-id";
    public static String MONGO_EXCLUSIVE = "exclusive";
    public static String MONGO_NCS_TYPE = "ncs-type";
    public static String MONGO_LAST_UPDATED_TIME = "last-updated-time";


    public static String NCS_TYPE = "ncs_type";
    public static String KEYWORD = "keyword";
    public static String SP_ID = "sp_id";
    public static String ID = "id";
    public static String APP_ID = "app_id";
    public static String LAST_UPDATED_TIME = "last_modified_time";


    public static String OPERATOR = "etisalat";
    public static String SHORT_CODE = "4499";
    public static boolean EXCLUSIVE = false;

    @Override
    public ComparisonResult compare(DBObject mongoData, ResultSet mysqlData) {
        this.mongoData = mongoData;
        this.mysqlData = mysqlData;

        ComparisonResult comparisonResult = new ComparisonResult();

        try {
            doComparison(comparisonResult);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return comparisonResult;
    }

    private void doComparison(ComparisonResult comparisonResult) throws SQLException {
        compareStringField(MONGO_KEYWORD, KEYWORD, comparisonResult);
        compareStaticStringValue(MONGO_SHORT_CODE, SHORT_CODE, comparisonResult);
        compareStaticStringValue(MONGO_OPERATOR, OPERATOR, comparisonResult);
        compareNcsType(MONGO_NCS_TYPE, NCS_TYPE, comparisonResult);
        compareStringField(MONGO_APP_ID, APP_ID, comparisonResult);
        compareStringField(MONGO_SP_ID, SP_ID, comparisonResult);
        checkStaticBooleanValue(MONGO_EXCLUSIVE, EXCLUSIVE, comparisonResult);
        compareDates(MONGO_LAST_UPDATED_TIME, LAST_UPDATED_TIME, comparisonResult);
    }

    private void compareNcsType(String mongoKey, String mySqlKey, ComparisonResult comparisonResult) throws SQLException {
        if (!mongoData.get(mongoKey).equals(mysqlData.getString(mySqlKey).toLowerCase())) {
            updateMismatchResult(mongoKey, mongoData.get(mongoKey).toString(), mysqlData.getString(mySqlKey).toLowerCase(), comparisonResult);
        }
    }
}
