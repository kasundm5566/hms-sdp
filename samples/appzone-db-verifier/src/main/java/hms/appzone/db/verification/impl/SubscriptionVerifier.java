/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.appzone.db.verification.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import hms.appzone.db.comparison.Comparator;
import hms.appzone.db.comparison.impl.SpComparator;
import hms.appzone.db.comparison.impl.SubscriptionComparator;
import hms.appzone.db.connection.MongoConnector;
import hms.appzone.db.connection.MysqlConnector;
import hms.appzone.db.util.HtmlGenerator;
import hms.appzone.db.util.PropertyLoader;
import hms.appzone.db.verification.Verifier;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * User: Pasan Buddhika
 * Date: 11/14/13
 */
public class SubscriptionVerifier implements Verifier {
    @Override
    public void verify() throws SQLException, IOException {
        MysqlConnector mysqlConnector = new MysqlConnector();
        System.out.println("Verifying Subscription Migration . . .");
        int count = 0;

        MongoConnector mongoConnector = new MongoConnector();
        DBCursor res = mongoConnector.getResults(new BasicDBObject(), "subscription", PropertyLoader.get("MONGO_DB_SUBSCRIPTION"));
        HtmlGenerator htmlGenerator = new HtmlGenerator("Subscription");

        while (res.hasNext()) {
            DBObject dbObject = res.next();

            String app_id = dbObject.get("app-id").toString();
            String msisdn = dbObject.get("msisdn").toString();

            String query = "SELECT     " +
                    "* " +
                    "FROM " +
                    "subscriber_registration " +
                    "inner join " +
                    "(select  " +
                    "app.app_id, " +
                    "app.app_name, " +
                    "app.current_state, " +
                    "sp.sp_id, " +
                    "routing_key.keyword, " +
                    "duration_based_subs_charging_policy.charging_amount, " +
                    "duration_based_subs_charging_policy.duration, " +
                    "sp_user_name.username " +
                    "From " +
                    "app " +
                    "left join sp ON sp.id = app.sp " +
                    "left join routing_key ON routing_key.active_app_id = app.app_id " +
                    "left join duration_based_subs_charging_policy ON app.id = duration_based_subs_charging_policy.id " +
                    "left join sp_user_name ON sp_user_name.sp_id = sp.id where app_id= '"+app_id+"') apps ON subscriber_registration.app_id = apps.app_id " +
                    "where subscriber_registration.subscriber_id = '" + msisdn + "'";
//            System.out.println(query);
            ResultSet resultSet = mysqlConnector.getResult(query);

            if (resultSet.next()) {
                Comparator comparator = new SubscriptionComparator();
                htmlGenerator.appendResult(app_id + "-" + msisdn, comparator.compare(dbObject, resultSet));
            }
            else {
                System.out.println(app_id);
            }
            resultSet.close();
            count++;
        }
    }
}
