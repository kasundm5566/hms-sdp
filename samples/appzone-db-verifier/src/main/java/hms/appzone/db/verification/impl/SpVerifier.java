/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.appzone.db.verification.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import hms.appzone.db.comparison.Comparator;
import hms.appzone.db.comparison.impl.AppComparator;
import hms.appzone.db.comparison.impl.SpComparator;
import hms.appzone.db.connection.MongoConnector;
import hms.appzone.db.connection.MysqlConnector;
import hms.appzone.db.util.HtmlGenerator;
import hms.appzone.db.verification.Verifier;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * User: Pasan Buddhika
 * Date: 10/7/13
 */
public class SpVerifier implements Verifier {
    @Override
    public void verify() throws SQLException, IOException {
        MysqlConnector mysqlConnector = new MysqlConnector();
        System.out.println("Verifying SP Migration . . .");
        int count = 0;

        MongoConnector mongoConnector = new MongoConnector();
        DBCursor res = mongoConnector.getResults(new BasicDBObject(), "sp");
        HtmlGenerator htmlGenerator = new HtmlGenerator("SP");

        while (res.hasNext()) {
            DBObject dbObject = res.next();

            String sp_id = dbObject.get("_id").toString();
            ResultSet resultSet = mysqlConnector.getResult("SELECT sp_user_name.username, sp.* FROM sp LEFT JOIN sp_user_name ON sp.id=sp_user_name.sp_id where sp.sp_id='" + sp_id + "'");

            if (resultSet.next()) {
                Comparator comparator = new SpComparator();
                htmlGenerator.appendResult(sp_id, comparator.compare(dbObject, resultSet));
            }
            else {
                System.out.println(sp_id);
            }
            resultSet.close();
            count++;
        }
        htmlGenerator.generateFile();
        res.close();
        mysqlConnector.closeConnection();
        System.out.println("SP count in Kite : " + count + "\n\n");
    }
}
