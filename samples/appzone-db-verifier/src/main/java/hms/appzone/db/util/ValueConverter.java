/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.appzone.db.util;

/**
 * User: Pasan Buddhika
 * Date: 10/4/13
 */
public class ValueConverter {
    public static String getReleventAppStatus(String mysqlAppStatus, String appId) {
        if (mysqlAppStatus.equals("ACTIVE") && appId.matches("^(PD).*")) {
            return "active-production";
        }
        else {
            return "suspended";
        }
    }

    public static String getRelevantNcsSlaStatus(String mysqlNcsStatus) {
        if (mysqlNcsStatus.equals("ACTIVE_PD")) {
            return  "active-production";
        } else if (mysqlNcsStatus.equals("LIMITED_PD")){
            return "limited-production";
        } else if (mysqlNcsStatus.equals("PREACTIVE_PD")) {
            return ""; // todo: need to find ther relevant value
        } else {
            return null;
        }
    }
}
