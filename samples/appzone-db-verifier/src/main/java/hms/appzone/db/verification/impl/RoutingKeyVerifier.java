/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.appzone.db.verification.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import hms.appzone.db.comparison.Comparator;
import hms.appzone.db.comparison.impl.AppComparator;
import hms.appzone.db.comparison.impl.RoutingKeyComparator;
import hms.appzone.db.connection.MongoConnector;
import hms.appzone.db.connection.MysqlConnector;
import hms.appzone.db.util.HtmlGenerator;
import hms.appzone.db.verification.Verifier;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * User: Pasan Buddhika
 * Date: 10/4/13
 */
public class RoutingKeyVerifier implements Verifier {
    @Override
    public void verify() throws SQLException, IOException {
        MysqlConnector mysqlConnector = new MysqlConnector();
        System.out.println("Verifying Routing Key Migration . . .");
        int count = 0;

        MongoConnector mongoConnector = new MongoConnector();
        DBCursor res = mongoConnector.getResults(new BasicDBObject("app-id", new BasicDBObject("$ne", null)), "routing_keys");
        HtmlGenerator htmlGenerator = new HtmlGenerator("Routing Keys");

        while (res.hasNext()) {
            DBObject dbObject = res.next();

            String appId = dbObject.get("app-id").toString();
            ResultSet resultSet = mysqlConnector.getResult("select a.keyword, a.last_modified_time, c.app_id, c.sp, d.sp_id, e.ncs_type , c.current_state  from routing_key a inner join  app c on a.active_app_id=c.app_id inner join sp d  on c.sp=d.id inner join ncs e on e.id=a.ncs  where c.app_id='"+appId+"'");

            if (resultSet.next()) {
                Comparator comparator = new RoutingKeyComparator();
                htmlGenerator.appendResult(appId, comparator.compare(dbObject, resultSet));
            }
            else {
                System.out.println(appId);
            }
            resultSet.close();
             count++;
        }
        htmlGenerator.generateFile();
        res.close();
        mysqlConnector.closeConnection();
        System.out.println("Routing Key count in Kite : " + count + "\n\n");
    }
}
