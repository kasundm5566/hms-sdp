/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.appzone.db.verification.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import hms.appzone.db.comparison.Comparator;
import hms.appzone.db.comparison.impl.AppComparator;
import hms.appzone.db.connection.MongoConnector;
import hms.appzone.db.connection.MysqlConnector;
import hms.appzone.db.util.ComparisonResult;
import hms.appzone.db.util.HtmlGenerator;
import hms.appzone.db.verification.Verifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.UnknownHostException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * User: Pasan Buddhika
 * Date: 10/3/13
 */
public class AppVerifier implements Verifier {
    private static final Logger logger = LoggerFactory.getLogger("app-verification-result");
    public void verify() throws SQLException, IOException {
        MysqlConnector mysqlConnector = new MysqlConnector();
        System.out.println("Verifying App Migration . . .");
        int count = 0;

        MongoConnector mongoConnector = new MongoConnector();
        DBCursor res = mongoConnector.getResults(new BasicDBObject(), "app");
        HtmlGenerator htmlGenerator = new HtmlGenerator("App");

        while (res.hasNext()) {
            DBObject dbObject = res.next();

            String appid = dbObject.get("app-id").toString();
            ResultSet resultSet = mysqlConnector.getResult("select * from app join sp on sp.id=app.sp INNER JOIN sp_user_name ON sp_user_name.sp_id = app.sp where app_id='" + appid + "'");

            if (resultSet.next()) {
                Comparator comparator = new AppComparator();
                ComparisonResult comparisonResult = comparator.compare(dbObject, resultSet);
                logger.debug("App-id : {}, Status : [{}] mismatches found, Details : {}", new Object[] {appid, comparisonResult.getMismatchCount(), comparisonResult.getMisMatchResults().toString().replaceAll("\\{", "").replaceAll("}", "").replaceAll("=", " in ")});
                htmlGenerator.appendResult(appid, comparisonResult);
            }
            else {
                System.out.println(appid);
            }
            resultSet.close();
            count++;
        }
        htmlGenerator.generateFile();
        res.close();
        mysqlConnector.closeConnection();
        System.out.println("App count in Kite : " + count + "\n\n");
    }
}
