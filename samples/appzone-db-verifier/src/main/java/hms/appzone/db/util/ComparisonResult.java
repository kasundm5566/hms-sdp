/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.appzone.db.util;

import java.util.HashMap;
import java.util.Map;

/**
 * User: Pasan Buddhika
 * Date: 10/3/13
 */
public class ComparisonResult {
    private int mismatchCount;
    private Map<String, String> misMatchResults;

    public ComparisonResult() {
        mismatchCount = 0;
        misMatchResults = new HashMap<String, String>();
    }

    public int getMismatchCount() {
        return mismatchCount;
    }

    public Map<String, String> getMisMatchResults() {
        return misMatchResults;
    }

    private void incrementUnmatchCount() {
        mismatchCount++;
    }

    public void addMismatchResult(String field, String description) {
        incrementUnmatchCount();
        misMatchResults.put(field, description);
    }

    @Override
    public String toString() {
        return "ComparisonResult{" +
                "mismatchCount=" + mismatchCount +
                ", misMatchResults=" + misMatchResults +
                '}';
    }
}
