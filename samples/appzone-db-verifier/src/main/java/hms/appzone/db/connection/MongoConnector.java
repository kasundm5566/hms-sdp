/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.appzone.db.connection;

import com.mongodb.*;
import hms.appzone.db.util.PropertyLoader;

import java.net.UnknownHostException;

/**
 * User: Pasan Buddhika
 * Date: 10/3/13
 */
public class MongoConnector {
    public DBCursor getResults(BasicDBObject query, String collectionName) throws UnknownHostException {
        return findResults(query, collectionName, PropertyLoader.get("MONGO_DB_KITE"));
    }

    public DBCursor getResults(BasicDBObject query, String collectionName, String dbName) throws UnknownHostException {
        return findResults(query, collectionName, dbName);
    }

    private DBCursor findResults(BasicDBObject query, String collectionName, String dbName) throws UnknownHostException {
        MongoClient mongoClient = new MongoClient(PropertyLoader.get("MONGO_HOST"), Integer.parseInt(PropertyLoader.get("MONGO_PORT")));
        DB db = mongoClient.getDB(dbName);
        DBCollection collection = db.getCollection(collectionName);
        return collection.find(query);
    }
}
