/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.appzone.db;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import hms.appzone.db.comparison.impl.AppComparator;
import hms.appzone.db.comparison.Comparator;
import hms.appzone.db.connection.MongoConnector;
import hms.appzone.db.connection.MysqlConnector;
import hms.appzone.db.util.HtmlGenerator;
import hms.appzone.db.verification.Verifier;
import hms.appzone.db.verification.impl.*;

import java.io.IOException;
import java.net.UnknownHostException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * User: Pasan Buddhika
 * Date: 10/3/13
 */
public class VerifierMain {
    public static void main(String[] args) throws SQLException, IOException {
        new AppVerifier().verify();
        new RoutingKeyVerifier().verify();
        new SpVerifier().verify();
        new NcsSlaVerifier().verify();
        new SubscriptionVerifier().verify();
    }
}
