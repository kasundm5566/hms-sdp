/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.appzone.db.util;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * User: Pasan Buddhika
 * Date: 9/30/13
 */
public class PropertyLoader {
    public static final String CONFIGURATION_BUNDLE_NAME = "configuration";
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle(CONFIGURATION_BUNDLE_NAME);

    public static String get(final String key, final Object... arguments)
    {
        try
        {
            if (arguments != null)
            {
                return MessageFormat.format(resourceBundle.getString(key), arguments);
            }
            return resourceBundle.getString(key);
        }
        catch (MissingResourceException e)
        {
            return '!' + key + '!';
        }
    }
}
