/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.appzone.db.verification;

import java.io.IOException;
import java.net.UnknownHostException;
import java.sql.SQLException;

/**
 * User: Pasan Buddhika
 * Date: 10/3/13
 */
public interface Verifier {
    void verify() throws SQLException, IOException;
}
