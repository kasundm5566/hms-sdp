/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.appzone.db.comparison.impl;

import com.mongodb.DBObject;
import hms.appzone.db.comparison.Comparator;
import hms.appzone.db.util.ComparisonResult;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * User: Pasan Buddhika
 * Date: 10/7/13
 */
public class SpComparator extends Comparator {
    private static final String MONGO_SMS_MT_KEY = "sms-mt";
    private static final String MONGO_CAS_MO_TPS_KEY = "cas-mo-tps";
    private static final String MONGO_SP_ID_KEY = "sp-id";
    private static final String MONGO_COOP_USER_ID_KEY = "coop-user-id";
    private static final String MONGO_SMS_MO_KEY = "sms-mo";
    private static final String MONGO_SOLTURA_USER_KEY = "soltura-user";
    private static final String MONGO_MAX_NO_OF_SUBSCRIBERS_KEY = "max-no-of-subscribers";
    private static final String MONGO_USSD_SELECTED_KEY = "ussd-selected";
    private static final String MONGO_ID_KEY = "_id";
    private static final String MONGO_LBS_TPD_KEY = "lbs-tpd";
    private static final String MONGO_USSD_MO_TPD_KEY = "ussd-mo-tpd";
    private static final String MONGO_SMS_MT_TPS_KEY = "sms-mt-tps";
    private static final String MONGO_SMS_MO_TPS_KEY = "sms-mo-tps";
    private static final String MONGO_SDP_USER_KEY = "sdp-user";
    private static final String MONGO_COOP_USER_NAME_KEY = "coop-user-name";
    private static final String MONGO_STATUS_KEY = "status";
    private static final String MONGO_USSD_MO_TPS_KEY = "ussd-mo-tps";
    private static final String MONGO_CAS_SELECTED_KEY = "cas-selected";
    private static final String MONGO_SMS_MO_TPD_KEY = "sms-mo-tpd";
    private static final String MONGO_SMS_MT_TPD_KEY = "sms-mt-tpd";
    private static final String MONGO_SP_SELECTED_SERVICES_KEY = "sp-selected-services";
    private static final String MONGO_LBS_TPS_KEY = "lbs-tps";
    private static final String MONGO_DOWNLOADABLE_MDPD_KEY = "downloadable-mdpd";
    private static final String MONGO_SUBSCRIPTION_SELECTED_KEY = "subscription-selected";
    private static final String MONGO_LBS_SELECTED_KEY = "lbs-selected";
    private static final String MONGO_DOWNLODABLE_SELECTED_KEY = "downloadable-selected";
    private static final String MONGO_DOWNLODABLE_MCD_KEY = "downloadable-mcd";
    private static final String MONGO_SMS_SELECTED_KEY = "sms-selected";
    private static final String MONGO_CAS_MO_TPD__KEY = "cas-mo-tpd";
    private static final String MONGO_MAX_NO_OF_BC_MSGS_PER_DAY_KEY = "max-no-of-bc-msgs-per-day";

    private static final String MYSQL_SP_ID = "sp_id";
    private static final String MYSQL_COMPANY_NAME = "company_name";
    private static final String MYSQL_DESCRIPTION = "description";
    private static final String MYSQL_SP_USERNAME_TABLE_NAME = "sp_username";
    private static final String MYSQL_USERNAME = "username";
    private static final String MYSQL_SP_SMS_MT_SLA_TABLE_NAME = "sp_sms_mt_sla";
    private static final String MYSQL_SP_SMS_MT_SLA_MPS = "mps";
    private static final String MYSQL_SP_SMS_MO_SLA_TABLE_NAME = "sp_sms_mo_sla";
    private static final String MYSQL_SP_SMS_MO_SLA_MPS = "mps";


    private static final String SOLTURA_USER = "true";
    private static final String SDP_USER = "true";
    private static final boolean SMS_MT = true;
    private static final boolean SMS_MO = true;
    private static final String USSD_SELECTED = "true";
    private static final String USSD_MO_TPS = "10";
    private static final String USSD_MO_TPD = "10000";
    private static final String STATUS = "approved";
    private static final String CAS_SELECTED = "true";
    private static final String SMS_MO_TPD = "30000";
    private static final String SMS_MT_TPD = "30000";
    private static final String SUBSCRIPTION_SELECTED = "true";
    private static final String SMS_SELECTED = "true";
    private static final String CAS_MO_TPD = "5000";
    private static final String MAX_NO_OF_BC_MSGS_PER_DAY = "10000";
    private static final String DOWNLOADABLE_MCD = "10";
    private static final String DOWNLOADABLE_MDPD = "100";
    private static final String DOWNLOADABLE__SELECTED = "true";
    private static final boolean LBS_SELECTED = true;
    private static final String CAS_MO_TPS = "10";
    private static String SMS_MT_TPS = "30";
    private static String SMS_MO_TPS = "10";
    private static String LBS_TPD = "100";
    private static String LBS_TPS = "10";

   @Override
    public ComparisonResult compare(DBObject mongoData, ResultSet mysqlData) {
       this.mongoData = mongoData;
       this.mysqlData = mysqlData;

       ComparisonResult comparisonResult = new ComparisonResult();

       try {
           doComparison(comparisonResult);
       } catch (SQLException e) {
           e.printStackTrace();
       }

       return comparisonResult;
    }

    private void doComparison(ComparisonResult comparisonResult) throws SQLException {
        compareStringField(MONGO_ID_KEY, MYSQL_SP_ID, comparisonResult);
        compareStringField(MONGO_SP_ID_KEY, MYSQL_SP_ID, comparisonResult);
        compareStringField(MONGO_COOP_USER_ID_KEY, MYSQL_SP_ID, comparisonResult);
        compareStringField(MONGO_COOP_USER_NAME_KEY, MYSQL_USERNAME, comparisonResult);
        compareStaticStringValue(MONGO_SMS_MO_TPD_KEY, SMS_MO_TPD, comparisonResult);
        compareStaticStringValue(MONGO_SMS_MO_TPS_KEY, SMS_MO_TPS, comparisonResult);
        compareStaticStringValue(MONGO_SMS_MT_TPD_KEY, SMS_MT_TPD, comparisonResult);
        compareStaticStringValue(MONGO_SMS_MT_TPS_KEY, SMS_MT_TPS, comparisonResult);
        compareStaticStringValue(MONGO_USSD_MO_TPS_KEY, USSD_MO_TPS, comparisonResult);
        compareStaticStringValue(MONGO_USSD_MO_TPD_KEY, USSD_MO_TPD, comparisonResult);
        compareStaticStringValue(MONGO_CAS_MO_TPS_KEY, CAS_MO_TPS, comparisonResult);
        compareStaticStringValue(MONGO_CAS_MO_TPD__KEY, CAS_MO_TPD, comparisonResult);
        compareStaticStringValue(MONGO_LBS_TPD_KEY, LBS_TPD, comparisonResult);
        compareStaticStringValue(MONGO_LBS_TPS_KEY, LBS_TPS, comparisonResult);
        checkStaticBooleanValue(MONGO_SMS_MO_KEY, SMS_MO, comparisonResult);
        checkStaticBooleanValue(MONGO_SMS_MT_KEY, SMS_MT, comparisonResult);
        compareStaticStringValue(MONGO_USSD_SELECTED_KEY, USSD_SELECTED, comparisonResult);
        compareStaticStringValue(MONGO_CAS_SELECTED_KEY, CAS_SELECTED, comparisonResult);
        checkStaticBooleanValue(MONGO_LBS_SELECTED_KEY, LBS_SELECTED, comparisonResult);
        compareStaticStringValue(MONGO_SMS_SELECTED_KEY, SMS_SELECTED, comparisonResult);
        compareStaticStringValue(MONGO_SUBSCRIPTION_SELECTED_KEY, SUBSCRIPTION_SELECTED, comparisonResult);
        compareStaticStringValue(MONGO_SOLTURA_USER_KEY, SOLTURA_USER, comparisonResult);
        compareStaticStringValue(MONGO_STATUS_KEY, STATUS, comparisonResult);
        compareStaticStringValue(MONGO_SDP_USER_KEY, SDP_USER, comparisonResult);
        compareStaticStringValue(MONGO_DOWNLODABLE_SELECTED_KEY, DOWNLOADABLE__SELECTED, comparisonResult);
        compareStaticStringValue(MONGO_MAX_NO_OF_BC_MSGS_PER_DAY_KEY, MAX_NO_OF_BC_MSGS_PER_DAY, comparisonResult);
        compareStaticStringValue(MONGO_DOWNLOADABLE_MDPD_KEY, DOWNLOADABLE_MDPD, comparisonResult);
        compareStaticStringValue(MONGO_DOWNLODABLE_MCD_KEY, DOWNLOADABLE_MCD, comparisonResult);
    }
}
