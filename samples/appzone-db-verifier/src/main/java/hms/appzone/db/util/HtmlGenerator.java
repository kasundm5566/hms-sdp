/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.appzone.db.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * User: Pasan Buddhika
 * Date: 10/4/13
 */
public class HtmlGenerator {
    private String name;
    private BufferedWriter bufferedWriter;
    StringBuilder stringBuilder = new StringBuilder();
    private int totalCount;
    private int errorCount;

    public HtmlGenerator(String name) throws IOException {
        this.name = name;
        init();
    }

    public void appendResult(String key, ComparisonResult comparisonResult) throws IOException {
        totalCount++;
        if (comparisonResult.getMismatchCount() > 0) {
            errorCount++;
            stringBuilder.append("<p class='error'>" + key + "</p>");
            stringBuilder.append("Status : " + comparisonResult.getMismatchCount() + " mismatches found<br/>");
            stringBuilder.append("Details : " + comparisonResult.getMisMatchResults().toString().replaceAll("\\{", "").replaceAll("}", "").replaceAll("=", " in "));
        }
        else {
            stringBuilder.append("<p class='success'>" + key + "</p>");
            stringBuilder.append("Status : " + "success");
        }
        stringBuilder.append("<br/><br/>");
    }

    public void generateFile() throws IOException {
        bufferedWriter.append(generateHeader());
        bufferedWriter.append(stringBuilder.toString());
        bufferedWriter.append(generateFooter());
        bufferedWriter.flush();
        bufferedWriter.close();
    }

    private void init() throws IOException {
    	File file = new File(PropertyLoader.get("RESULT_FILE_OUTPUT_DIR") + name + ".html");
    	file.getParentFile().mkdirs();    	
        bufferedWriter = new BufferedWriter(new FileWriter(file));

    }

    private String generateHeader() {
        return "<!DOCTYPE html><html><head><title>" + name + "Migration Verification Results</title></head><style type=\"text/css\">html {margin :0; padding : 0; font-size : 13px; } p {margin : 0; padding : 0} .error {color: #ff0000; font-weight : bold} .success {color: green; font-weight : bold}</style> <body>" +
                "<h3>" + name + " Migration Verification Results</h3>" +
                "<p>Checked : " + String.valueOf(totalCount) +  " Success : " + String.valueOf(totalCount - errorCount) + " Errors : " + String.valueOf(errorCount) + "</p><br/><br/>";
    }

    private String generateFooter() {
        return "</body></html>";
    }
}
