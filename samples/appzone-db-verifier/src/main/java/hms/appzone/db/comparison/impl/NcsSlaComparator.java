/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.appzone.db.comparison.impl;

import com.mongodb.DBObject;
import hms.appzone.db.comparison.Comparator;
import hms.appzone.db.util.ComparisonResult;
import hms.appzone.db.util.PropertyLoader;
import hms.appzone.db.util.ValueConverter;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * User: Pasan Buddhika
 * Date: 10/7/13
 */
public class NcsSlaComparator extends Comparator {

    private static final String MONGO_SUBSCRIPTION_RESPONSE_MESSSAGE_KEY = "subscription-response-message";
    private static final String MONGO_STATUS_KEY = "status";
    private static final String MONGO_CHARGING_KEY = "charging";
    private static final String MONGO_AMOUNT_KEY = "amount";
    private static final String MONGO_FREQUENCY_KEY  = "frequency";
    private static final String MONGO_TYPE_KEY = "type";
    private static final String MONGO_NCS_TYPE_KEY = "ncs-type";
    private static final String MONGO_PARTY_KEY = "party";
    private static final String MONGO_OPERATOR_KEY = "operator";
    private static final String MONGO_APP_ID_KEY = "app-id";
    private static final String MONGO_MO_ALLOWED_KEY = "mo-allowed";
    private static final String MONGO_MT_ALLOWED_KEY = "mt-allowed";
    private static final String MONGO_MO_KEY = "mo";
    private static final String MONGO_MT_KEY = "mt";
    private static final String MONGO_TPS_KEY = "tps";
    private static final String MONGO_TPD_KEY = "tpd";
    private static final String MONGO_DEFAULT_SENDER_ADDRESS_KEY = "default-sender-address";
    private static final String MONGO_CONNECTION_URL_KEY = "connection-url";


    private static String APP_ID = "active_app_id";
    private static String STATUS = "current_ncs_sla_state";
    private static String MO_ALLOWED = "mo_allowed";
    private static String MYSQL_MO_TPS = "sms_mo_tps";
    private static String MYSQL_MO_TPD = "sms_mo_tpd";
    private static String MYSQL_MT_TPS = "sms_mt_tps";
    private static String MYSQL_MT_TPD = "sms_mt_tpd";
    private static String SUBS_REG_SLA_TYPE = "subs_reg_sla_type";
    private static final String KEYWORD = "keyword";
    private static final String CONNECTION_URL = "connection_url";


    private static String OPERATOR = "etisalat";
    private static boolean MT_ALLOWED = true;
    private static final String FLAT = "flat";
    private static final String FREE = "free";
    private static String DEFAULT_SENDER_ADDRESS = "4499";
    private static final String MT_CHARGING_AMOUNT  = PropertyLoader.get("MT_CHARGING_AMOUNT");
    private static final String PARTY = "subscriber";
    private static final String FREQUENCY  = PropertyLoader.get("SUBSCRIPTION_CHARGING_FREQUENCY");
    private static final String SUBSCRIPTION_CHARGING_AMOUNT = PropertyLoader.get("SUBSCRIPTION_CHARGING_AMOUNT");


    @Override
    public ComparisonResult compare(DBObject mongoData, ResultSet mysqlData) {
        this.mongoData = mongoData;
        this.mysqlData = mysqlData;

        ComparisonResult comparisonResult = new ComparisonResult();

        try {
            doComparison(comparisonResult);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return comparisonResult;
    }

    private void doComparison(ComparisonResult comparisonResult) throws SQLException{
        compareNcsStatue(mongoData.get(MONGO_STATUS_KEY).toString(), mysqlData.getString(STATUS), comparisonResult);
        compareStringField(MONGO_APP_ID_KEY, APP_ID, comparisonResult);
        compareStaticStringValue(MONGO_OPERATOR_KEY, OPERATOR, comparisonResult);
        if (mongoData.get(MONGO_NCS_TYPE_KEY).equals("sms")) {
            compareBooleanField(MONGO_MO_ALLOWED_KEY, MO_ALLOWED, comparisonResult);
            checkStaticBooleanValue(MONGO_MT_ALLOWED_KEY, MT_ALLOWED, comparisonResult);
            compareStaticStringValue(MONGO_OPERATOR_KEY, OPERATOR, comparisonResult);
            compareMoMap((DBObject) mongoData.get(MONGO_MO_KEY), comparisonResult);
            compareMtMap((DBObject) mongoData.get(MONGO_MT_KEY), comparisonResult);
        } else if (mongoData.get(MONGO_NCS_TYPE_KEY).equals("subscription")) {
            compareSubscriptionChargingMap((DBObject) mongoData.get(MONGO_CHARGING_KEY), comparisonResult);
            String msg =  PropertyLoader.get("SUBSCRIPTION_SUCCESS_MESSAGE", mysqlData.getString(KEYWORD));

            compareStaticStringValue(MONGO_SUBSCRIPTION_RESPONSE_MESSSAGE_KEY, msg, comparisonResult );
        }
    }

    private void compareNcsStatue(String mongoValue, String mysqlValue, ComparisonResult comparisonResult) {
        if (!mongoValue.equals(ValueConverter.getRelevantNcsSlaStatus(mysqlValue))) {
            updateMismatchResult("status", mongoValue, mysqlValue, comparisonResult);
        }
    }

    private void compareMoMap(DBObject moObject, ComparisonResult comparisonResult) throws SQLException {
        if (!moObject.get(MONGO_TPD_KEY).equals(mysqlData.getString(MYSQL_MO_TPD))) {
            updateMismatchResult(MONGO_MO_KEY + " " + MONGO_TPD_KEY, moObject.get(MONGO_TPD_KEY).toString(), mysqlData.getString(MYSQL_MO_TPD), comparisonResult);
        }
        if (!moObject.get(MONGO_TPS_KEY).equals(mysqlData.getString(MYSQL_MO_TPS))) {
            updateMismatchResult(MONGO_MO_KEY + " " + MONGO_TPS_KEY, moObject.get(MONGO_TPS_KEY).toString(), mysqlData.getString(MYSQL_MO_TPS), comparisonResult);
        }
        if (!((DBObject)moObject.get(MONGO_CHARGING_KEY)).get(MONGO_TYPE_KEY).equals(FREE)) {
            updateMismatchResult(MONGO_MO_KEY + " " + MONGO_CHARGING_KEY, ((DBObject)moObject.get(MONGO_CHARGING_KEY)).get(MONGO_TYPE_KEY).toString(), FREE, comparisonResult);
        }
        String url = PropertyLoader.get("LEGACY_CONVERTER_URL_FORMAT", mysqlData.getString(CONNECTION_URL));
        if (!moObject.get(MONGO_CONNECTION_URL_KEY).equals(url)){
            updateMismatchResult(MONGO_MO_KEY + " " + MONGO_CONNECTION_URL_KEY, moObject.get(MONGO_CONNECTION_URL_KEY).toString(), url, comparisonResult);
        }
    }

    private void compareSubscriptionChargingMap(DBObject dbObject, ComparisonResult comparisonResult) {
        if (!dbObject.get(MONGO_TYPE_KEY).equals(FLAT)) {
            updateMismatchResult(MONGO_CHARGING_KEY + " " + MONGO_TYPE_KEY, dbObject.get(MONGO_TYPE_KEY).toString(), FREE, comparisonResult);
        }
        if (!dbObject.containsField(MONGO_AMOUNT_KEY)) {
            updateMismatchResult(MONGO_CHARGING_KEY + " " + MONGO_AMOUNT_KEY, "null", MT_CHARGING_AMOUNT, comparisonResult);
        }
        else {
            if (!dbObject.get(MONGO_AMOUNT_KEY).equals(SUBSCRIPTION_CHARGING_AMOUNT)) {
                updateMismatchResult( MONGO_CHARGING_KEY + " " + MONGO_AMOUNT_KEY, dbObject.get(MONGO_AMOUNT_KEY).toString(), SUBSCRIPTION_CHARGING_AMOUNT, comparisonResult);
            }
            if (!dbObject.get(MONGO_PARTY_KEY).equals(PARTY)) {
                updateMismatchResult(MONGO_CHARGING_KEY + " " + MONGO_PARTY_KEY, dbObject.get(MONGO_PARTY_KEY).toString(), PARTY, comparisonResult);
            }
            if (!dbObject.get(MONGO_FREQUENCY_KEY).equals(FREQUENCY)) {
                updateMismatchResult(MONGO_CHARGING_KEY + " " + MONGO_FREQUENCY_KEY, dbObject.get(MONGO_FREQUENCY_KEY).toString(), FREQUENCY, comparisonResult);
            }
        }
    }

    private void compareMtMap(DBObject mtObject, ComparisonResult comparisonResult) throws SQLException {
        DBObject mtChargingMap = (DBObject)mtObject.get(MONGO_CHARGING_KEY);

        if (!mtObject.get(MONGO_TPD_KEY).equals(mysqlData.getString(MYSQL_MT_TPD))) {
            updateMismatchResult(MONGO_MT_KEY + MONGO_TPD_KEY, mtObject.get(MONGO_TPD_KEY).toString(), mysqlData.getString(MYSQL_MT_TPD), comparisonResult);
        }
        if (!mtObject.get(MONGO_TPS_KEY).equals(mysqlData.getString(MYSQL_MT_TPS))) {
            updateMismatchResult(MONGO_MT_KEY + MONGO_TPS_KEY, mtObject.get(MONGO_TPS_KEY).toString(), mysqlData.getString(MYSQL_MT_TPS), comparisonResult);
        }
        if (!mtObject.get(MONGO_DEFAULT_SENDER_ADDRESS_KEY).equals(DEFAULT_SENDER_ADDRESS)) {
            updateMismatchResult(MONGO_MT_KEY + MONGO_DEFAULT_SENDER_ADDRESS_KEY, mtObject.get(MONGO_DEFAULT_SENDER_ADDRESS_KEY).toString(), DEFAULT_SENDER_ADDRESS, comparisonResult);
        }
        if (mysqlData.getString(SUBS_REG_SLA_TYPE).equals("SUBS_REG_SLA")) {
            if (!mtChargingMap.get(MONGO_TYPE_KEY).equals(FREE)) {
                updateMismatchResult(MONGO_MT_KEY + " " + MONGO_CHARGING_KEY + " " + MONGO_TYPE_KEY, mtChargingMap.get(MONGO_TYPE_KEY).toString(), FREE, comparisonResult);
            }
        }
        else {
            if (!mtChargingMap.get(MONGO_TYPE_KEY).equals(FLAT)) {
                updateMismatchResult(MONGO_MT_KEY + " " + MONGO_CHARGING_KEY + " " + MONGO_TYPE_KEY, mtChargingMap.get(MONGO_TYPE_KEY).toString(), FREE, comparisonResult);
            }
            if (!mtChargingMap.containsField(MONGO_AMOUNT_KEY)) {
                updateMismatchResult(MONGO_MT_KEY + " " + MONGO_CHARGING_KEY + " " + MONGO_AMOUNT_KEY, "null", MT_CHARGING_AMOUNT, comparisonResult);
            }
            else {
                if (!mtChargingMap.get(MONGO_AMOUNT_KEY).equals(MT_CHARGING_AMOUNT)) {
                    updateMismatchResult(MONGO_MT_KEY + " " + MONGO_CHARGING_KEY + " " + MONGO_AMOUNT_KEY, mtChargingMap.get(MONGO_AMOUNT_KEY).toString(), MT_CHARGING_AMOUNT, comparisonResult);
                }
                if (!mtChargingMap.get(MONGO_PARTY_KEY).equals(PARTY)) {
                    updateMismatchResult(MONGO_MT_KEY + " " + MONGO_CHARGING_KEY + " " + MONGO_PARTY_KEY, mtChargingMap.get(MONGO_PARTY_KEY).toString(), PARTY, comparisonResult);
                }
            }
        }
    }
}
