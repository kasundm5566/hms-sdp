/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.appzone.db.comparison.impl;

import com.mongodb.DBObject;
import hms.appzone.db.comparison.Comparator;
import hms.appzone.db.util.ComparisonResult;
import hms.appzone.db.util.PropertyLoader;
import hms.appzone.db.util.ValueConverter;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * User: Pasan Buddhika
 * Date: 10/3/13
 */
public class AppComparator extends Comparator {

    // Mongo Collections and their fields are mentioned below
    // Mongo Collection name :  App
    private final String MONGO_NAME = "name";
    private final String MONGO_APP_ID = "app-id";
    private final String MONGO_MASK_NUMBER = "mask-number";
    private final String MONGO_GOVERN = "govern";
    private final String MONGO_APPLY_TAX = "apply-tax";
    private final String MONGO_ADVERTISE = "advertise";
    private final String MONGO_STATUS = "status";
    private final String MONGO_TYPE = "type";
    private final String MONGO_PASSWORD = "password";
    private final String MONGO_REVENUE_SHARE = "revenue-share";
    private final String MONGO_DESCRIPTION = "description";
    private final String MONGO_UPDATED_BY = "updated-by";
    private final String MONGO_ALLOWED_HOSTS = "allowed-hosts";
    private final String MONGO_USER_TYPE = "user-type";
    private final String MONGO_SP_ID = "sp-id";
    private final String MONGO_CATEGORY = "category";
    private final String MONGO_CREATED_BY = "created-by";
    private final String MONGO_EXPIRE = "expire";
    private final String MONGO_REMARKS = "remarks";

    // MySQL table names and their columns
    // App table:
    private final String APP_NAME = "app_name";
    private final String APP_ID = "app_id";
    private final String STATUS = "current_state";
    private final String PASSWORD = "ws_password";
    private final String DESCRIPTION = "app_description";
    private final String SP_ID = "sp_id";
    private final String CREATED_BY = "sp_user_name.username";
    private final String REMARKS = "remark_state";


    private final boolean EXPIRE = false;
    private final boolean MASK_NUMBER = true;
    private final boolean GOVERN = true;
    private final boolean APPLY_TAX = false;
    private final boolean ADVERTISE = false;
    private final String CATEGORY = "sdp";
    private final String UPDATED_BY = "sdpadmin";
    private final String USER_TYPE = "corporate";
    private final String ALLOWED_HOSTS = PropertyLoader.get("ALLOWED_HOST");
    private final String REVENUE_SHARE = "70";
    private final String TYPE = "flat";


    @Override
    public ComparisonResult compare(DBObject mongoData, ResultSet mysqlData) {
        this.mongoData = mongoData;
        this.mysqlData = mysqlData;

        ComparisonResult comparisonResult = new ComparisonResult();

        try {
            doComparison(comparisonResult);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return comparisonResult;
    }

    private void doComparison(ComparisonResult comparisonResult) throws SQLException {
        compareStringField(MONGO_APP_ID, APP_ID, comparisonResult);
        compareStringField(MONGO_NAME, APP_NAME, comparisonResult);
        compareStringField(MONGO_DESCRIPTION, DESCRIPTION, comparisonResult);
        checkStaticBooleanValue(MONGO_APPLY_TAX, APPLY_TAX, comparisonResult);
        checkStaticBooleanValue(MONGO_GOVERN, GOVERN, comparisonResult);
        checkStaticBooleanValue(MONGO_ADVERTISE, ADVERTISE, comparisonResult);
        checkStaticBooleanValue(MONGO_MASK_NUMBER, MASK_NUMBER, comparisonResult);
        checkStaticBooleanValue(MONGO_EXPIRE, EXPIRE, comparisonResult);
        checkAppStatus(mongoData.get(MONGO_STATUS).toString(), mysqlData.getString(STATUS), comparisonResult);
        compareStringField(MONGO_SP_ID, SP_ID, comparisonResult);
        compareStringField(MONGO_PASSWORD, PASSWORD, comparisonResult);
        compareStringField(MONGO_REMARKS, REMARKS, comparisonResult);
        compareStaticStringValue(MONGO_REVENUE_SHARE, REVENUE_SHARE, comparisonResult);
        compareStaticStringValue(MONGO_TYPE, TYPE, comparisonResult);
        compareStringField(MONGO_CREATED_BY, CREATED_BY, comparisonResult);
        compareStaticStringValue(MONGO_UPDATED_BY, UPDATED_BY, comparisonResult);
        compareStaticStringValue(MONGO_USER_TYPE, USER_TYPE, comparisonResult);
        checkValueInArray(MONGO_ALLOWED_HOSTS, ALLOWED_HOSTS, comparisonResult);
        compareStaticStringValue(MONGO_CATEGORY, CATEGORY, comparisonResult);
    }


    private void checkAppStatus(String mongoStatus, String mysqlStatus, ComparisonResult comparisonResult) throws SQLException {
        if (!mongoStatus.equals(ValueConverter.getReleventAppStatus(mysqlStatus, mysqlData.getString(APP_ID)))) {
            updateMismatchResult(MONGO_STATUS, mongoStatus, ValueConverter.getReleventAppStatus(mysqlStatus, mysqlData.getString(APP_ID)), comparisonResult);
        }
    }
}
