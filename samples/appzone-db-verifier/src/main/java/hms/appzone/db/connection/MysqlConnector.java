/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.appzone.db.connection;

import hms.appzone.db.util.PropertyLoader;

import java.sql.*;

/**
 * User: Pasan Buddhika
 * Date: 10/3/13
 */
public class MysqlConnector {
    Connection conn;
    Statement statement;

    public MysqlConnector() throws SQLException {
        conn = DriverManager.getConnection(PropertyLoader.get("MYSQL_DB_URL"), PropertyLoader.get("MYSQL_USER"), PropertyLoader.get("MYSQL_PASSWORD"));
        statement = conn.createStatement();
    }

    public ResultSet getResult(String query) throws SQLException {
        ResultSet res = statement.executeQuery(query);

        return res;
    }

    public void closeConnection() throws SQLException {
        if (conn != null) {
            conn.close();
        }
    }
}
