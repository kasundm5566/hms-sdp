/*
 *   (C) Copyright 2008-2013 hSenid Mobile Solutions (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Mobile Solutions (Pvt) Limited.
 *
 *   hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.appzone.db.comparison.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import hms.appzone.db.comparison.Comparator;
import hms.appzone.db.connection.MongoConnector;
import hms.appzone.db.util.ComparisonResult;
import hms.appzone.db.util.PropertyLoader;

import java.net.UnknownHostException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * User: Pasan Buddhika
 * Date: 11/14/13
 */
public class SubscriptionComparator extends Comparator {
    // Required Static fields - Mongo Database
    public static final String MONGO_FREQUENCY = "frequency";
    public static final String MONGO_CURRENT_STATUS = "current-status";
    public static final String MONGO_APP_ID = "app-id";
    public static final String MONGO_SP_ID = "sp-id";
    public static final String MONGO_APP_NAME = "app-name";
    public static final String MONGO_CHARGING_AMOUNT = "charging-amount";
    public static final String MONGO_MSISDN = "msisdn";
    public static final String MONGO_MANDATE_ID = "mandate-id";
    public static final String MONGO_KEYWORD = "keyword";
    public static final String MONGO_SHORT_CODE = "short-code";
    public static final String MONGO_APP_STATUS = "app-status";
    public static final String MONGO_COOP_USER_NAME = "coop-user-name";
    public static final String MONGO_OPERATOR = "operator";
    public static final String USERNAME = "username";
    public static final String MONGO_CREATED_DATE = "created-date";
    public static final String MONGO_LAST_MODIFIED_DATE = "last-modified-date";
    public static final String MONGO_LAST_CHARGED_TIME = "last-charged-time";

    // mongo keys for mandate creation
    private static final String MONGO__ID_KEY = "_id";
    private static final String MONGO_META_DATA_KEY = "meta-data";
    private static final String MONGO_SCHEDULE_KEY = "schedule";
    private static final String MONGO_RETRY_ATTEMPT_KEY = "retry-attempt";
    private static final String MONGO_CHARGE_AMOUNT_KEY = "charge-amount";
    private static final String MONGO_PHASE_KEY = "phase";
    private static final String MONGO_CHARGING_SHORT_INSTRUCTIONS_KEY = "charging-short-instructions";
    private static final String MONGO_EXECUTION_STATE_KEY = "execution-state";
    private static final String MONGO_LAST_DATE_KEY = "last-date";
    private static final String MONGO_NEXT_DATE_KEY = "next-date";
    private static final String MONGO_EXECUTION_COUNT_KEY = "execution-count";
    private static final String MONGO_FREQUENCY_KEY = "frequency";
    private static final String MONGO_STATE_KEY = "state";
    private static final String MONGO_END_DATE_KEY = "end-date";
    private static final String MONGO_START_CRITERIA_KEY = "start-criteria";
    private static final String MONGO_HEADERS_KEY = "headers";
    private static final String MONGO_SYSTEM_ID_KEY = "system-id";
    private static final String MONGO_MANDATE_ID_KEY = "mandate-id";
    private static final String MONGO_CORRELATION_ID_KEY = "correlation-id";
    private static final String MONGO_START_DATE_KEY = "start-date";
    private static final String MONGO_CHARGING_KEY = "charging";
    private static final String MONGO_AMOUNT_KEY = "amount";
    private static final String MONGO_VALUE_KEY = "value";
    private static final String MONGO_CURRENCY_CODE_KEY = "currency-code";
    private static final String MONGO_CATEGORY_KEY = "category";
    private static final String MONGO_MODEL_KEY = "model";
    private static final String MONGO_MERCHANT_ID_KEY = "merchant-id";
    private static final String MONGO_MSISDN_KEY = "msisdn";
    private static final String MONGO_MERCHANT_NAME_KEY = "merchant-name";
    private static final String MONGO_TYPE_KEY = "type";
    private static final String MONGO_FROM_PAYMENT_INSTRUMENT_KEY = "from-payment-instrument";
    private static final String MONGO_DESCRIPTION_KEY = "description";
    private static final String MONGO_CONTROL_DATA_KEY = "control-data";
    private static final String MONGO_APP__KEY = "app";
    private static final String MONGO_ID_KEY = "id";
    private static final String MONGO_SP_KEY = "sp";

    // Required static fields in MySQL database
    public static final String MYSQL_APP_ID = "app_id";
    public static final String MYSQL_SP_ID = "sp_id";
    public static final String MYSQL_APP_NAME = "app_name";
    public static final String MYSQL_SUBSCRIBER_ID = "subscriber_id";
    public static final String MYSQL_KEYWORD = "keyword";
    public static final String REGISTRATION_STATUS = "registrationStatus";
    public static final String LAST_MODIFIED_DATE = "last_modified_time";
    public static final String CREATED_TIME = "created_time";


    // Static fields
    public static final String OPERATOR = "etisalat";
    public static final String SHORTCODE = "4499";
    public static final String APP_ID_MATCHER = "PD";
    public static final String APP_STATUS_MATCHER = "ACTIVE";
    public static final String ACTIVE_PRODUCTION = "active-production";

    @Override
    public ComparisonResult compare(DBObject mongoData, ResultSet mysqlData) {
        this.mongoData = mongoData;
        this.mysqlData = mysqlData;

        ComparisonResult comparisonResult = new ComparisonResult();

        try {
            doComparison(comparisonResult);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return comparisonResult;

    }

    private void doComparison(ComparisonResult comparisonResult) throws SQLException {
        try {
            compareStringField(MONGO_APP_ID, MYSQL_APP_ID, comparisonResult);
            compareStringField(MONGO_APP_NAME, MYSQL_APP_NAME, comparisonResult);
            compareStringField(MONGO_MSISDN, MYSQL_SUBSCRIBER_ID, comparisonResult);
            compareStringField(MONGO_SP_ID, MYSQL_SP_ID, comparisonResult);
            compareStaticStringValue(MONGO_APP_STATUS, APP_STATUS_MATCHER, comparisonResult);
            compareStringField(MONGO_KEYWORD, MYSQL_KEYWORD, comparisonResult);
            compareStringField(MONGO_CURRENT_STATUS, REGISTRATION_STATUS, comparisonResult);
            compareStaticStringValue(MONGO_SHORT_CODE, SHORTCODE, comparisonResult);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
