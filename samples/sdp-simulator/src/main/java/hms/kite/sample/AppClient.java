/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sample;

import hms.common.rest.util.Message;
import hms.common.rest.util.MessageBodyProvider;
import org.apache.cxf.jaxrs.client.WebClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
public class AppClient {

    public static void main(String[] args) {
        ArrayList<Object> providers = new ArrayList<Object>();
        providers.add(new MessageBodyProvider());
        WebClient webClient = WebClient.create("http://localhost:9070/sms/mo", providers);
        webClient.header("Content-Type", "application/x-www-form-urlencoded");
        webClient.accept("application/x-www-form-urlencoded");

        Message message = new Message();
        message.put("message", "sample message");
        message.put("from-address", "0772916996");
        message.put("receiver-address", "0772916996");

        javax.ws.rs.core.Response response = webClient.post(message);
        Message responseEntity = (Message) response.getEntity();


        System.out.println("responseEntity = " + responseEntity);
    }
}
