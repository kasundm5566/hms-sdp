/**
 *   (C) Copyright 2010-2011 hSenid International (pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid International (pvt) Limited and constitute a TRADE SECRET of hSenid
 *   International (pvt) Limited.
 *
 *   hSenid International (pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */
package hms.kite.sample;

import hms.common.rest.util.Message;
import hms.commons.CollectionUtil;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@Produces({"application/x-www-form-urlencoded"})
@Consumes({"application/x-www-form-urlencoded"})
@Path("/")
public class Sdp {

    @POST
    @Path("sms/send")
    public Message sms(Message request) {
        System.out.println("===================    Received SMS Request is ===============");
        System.out.println(request);
        System.out.println("==============================================================");
        return new Message();
    }

    @POST
    @Path("ussd/send")
    public Message ussd(Message request) {
        System.out.println("===================    Received USSD Request is ===============");
        System.out.println(request);
        System.out.println("===============================================================");
        return new Message();
    }

    @POST
    @Path("wappush/send")
    public Message wappush(Message request) {
        System.out.println("===================    Received WAPPUSH Request is ===============");
        System.out.println(request);
        System.out.println("==================================================================");
        return new Message();
    }

    @POST
    @Path("cas/access")
    public Message casAccess(Message request) {
        System.out.println("===================    Received CAS Access Request is ===============");
        System.out.println(request);
        System.out.println("=====================================================================");
        return new Message();
    }

    @POST
    @Path("cas/reserve")
    public Message casReserve(Message request) {
        System.out.println("===================    Received CAS Reserve Request is ===============");
        System.out.println(request);
        System.out.println("======================================================================");
        return new Message();
    }

    @POST
    @Path("cas/commit")
    public Message casCommit(Message request) {
        System.out.println("===================    Received CAS Commit Request is ===============");
        System.out.println(request);
        System.out.println("=====================================================================");
        return new Message();
    }

    @POST
    @Path("cas/cancel")
    public Message casCancel(Message request) {
        System.out.println("===================    Received CAS Cancel Request is ===============");
        System.out.println(request);
        System.out.println("=====================================================================");
        return new Message();
    }

    @POST
    @Path("cas/direct-debit")
    public Message casDirectDebit(Message request) {
        System.out.println("===================    Received CAS Direct Debit Request is ===============");
        System.out.println(request);
        System.out.println("===========================================================================");
        return new Message();
    }

    @POST
    @Path("cas/direct-credit")
    public Message casCredit(Message request) {
        System.out.println("===================    Received CAS Direct Credit Request is ===============");
        System.out.println(request);
        System.out.println("============================================================================");
        return new Message();
    }
}
