package hms.kite.vinca;

import javax.servlet.ServletInputStream;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import java.io.IOException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Map;

/**
 * $LastChangedDate$
 * $LastChangedBy$
 * $LastChangedRevision$
 */
@Produces({"*/*", "*/*"})
@Consumes({"*/*", "*/*"})
@Path("/")
public class CommonServer {

    @POST
    @Path("/")
    public Map any(@Context javax.servlet.http.HttpServletRequest request) throws IOException {
        System.out.println("Received request is - " + request);
        System.out.println("request.getPathInfo() = " + request.getPathInfo());
        System.out.println("request.getHeaderNames() = " + request.getHeaderNames());
        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = (String) headerNames.nextElement();
            System.out.println("Header [" + headerName + "][" +  request.getHeader(headerName)  + "]");
        }
        System.out.println("Query String " + request.getQueryString());

        ServletInputStream inputStream = request.getInputStream();

        int available = inputStream.available();
        byte[] data = new byte[available];
        inputStream.read(data);
        System.out.println("data = \n" + new String(data));
        return Collections.singletonMap("statusCode", "S1000");
    }
}
