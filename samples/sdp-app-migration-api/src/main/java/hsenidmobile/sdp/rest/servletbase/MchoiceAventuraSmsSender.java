/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 */

package hsenidmobile.sdp.rest.servletbase;


import hms.kite.samples.api.GenericSender;
import hms.kite.samples.api.SdpException;
import hms.kite.samples.api.sms.messages.MtSmsReq;
import hms.kite.samples.api.sms.messages.MtSmsResp;

import java.net.URL;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A utility class to facilitate message sending through mChoice SDP. This class hides the http messaging part from
 * the user and allows to send the request and receive response.
 *
 *  $LastChangedDate: 2011-01-07 14:24:33 +0530 (Fri, 07 Jan 2011) $
 *  $LastChangedBy: jasond $
 *  $LastChangedRevision: 69721 $
 */
public class MchoiceAventuraSmsSender {

    private final String BROADCAST_ADDRESS = "tel:all";
    private static final String BINARY_HEADER = "";
    private static final String CHARGING_AMOUNT = "0";
    private static final String DELIVERY_STATUS_REQUEST = "0";
    private static final String ENCODING = "0";
    private static final String SOURCE_ADDRESS = "4499";
    private static final String VERSION = "1.0";
    
    /**
     * mChoice SDP sms mt message endpoint url.
     */
    private String url = "http://127.0.0.1:7000/sms/send";
    /**
     * Application id provided by the mChoice SDP.
     */
    private String appId;
    /**
     * Password issued for the application.
     */
    private String password;
    /**
     * mChoice SDP http api version.
     */
    @SuppressWarnings("unused")
	private String version = VERSION;

    @SuppressWarnings("unused")
	private String senderAddress;
    
    private static final Set<String> successResponseCodes;
    static {
        successResponseCodes = new HashSet<String>();
        successResponseCodes.add("S1000");
    }

    /**
     * Set the sender address witch would be shown on the phone of the person who is receiving the message.
     * @param senderAddress
     */
    public void setSenderAddress(String senderAddress) {
        this.senderAddress = senderAddress;
    }

    /**
     * This constructor used only for testing perposes. Don't use this to instantiate the sender.
     */
    protected MchoiceAventuraSmsSender() {
    }

    /**
     * Constructor for this util class. Use this constructor to instantiate an object.
     *
     * @param url - mChoice SDP sms mt endpoint url.
     * @param appId - application id provided by the mChoice SDP.
     * @param password - password id provided for your application.
     */
    public MchoiceAventuraSmsSender(final URL url, final String appId, final String password) {
        //this.url = url.toString();
        this.appId = appId;
        this.password = password;

    }/**
     * Constructor for this util class. Use this constructor to instantiate an object.
     *
     * @param url - mChoice SDP sms mt endpoint url.
     * @param appId - application id provided by the mChoice SDP.
     * @param password - password id provided for your application.
     */
    public MchoiceAventuraSmsSender(final String url, final String appId, final String password) {
        //this.url = url;
        this.appId = appId;
        this.password = password;

    }

    /**
     * Constructor for this util class. Use this constructor to instantiate an object.
     *
     * @param version  - REST API version
     * @param url - mChoice SDP sms mt endpoint url.
     * @param appId - application id provided by the mChoice SDP.
     * @param password - password id provided for your application.
     */
    public MchoiceAventuraSmsSender(String version, String url, String appId, String password) {
        //this.url = url;
        this.appId = appId;
        this.password = password;
        this.version = version;
    }

    /**
     * Send the message to list of people.
     *
     * @param message - string message that required to send.
     * @param addresses - lists of addresses with country code and subscriber number. Example :+94969696969
     * @return response received from SDP.
     * @throws MchoiceAventuraMessagingException - In case of illegal formatted message or connection
     * error this exception will throws.
     */
    public MchoiceAventuraResponse sendMessage(String message, List<String> addresses) throws MchoiceAventuraMessagingException {
    	return sendMessage(-1, message, addresses);
    }

    /**
     * Send the message to list of people.
     *
     * @param correlationId - CorrelationId for the message
     * @param message - string message that required to send.
     * @param addresses - lists of addresses with country code and subscriber number. Example :+94969696969
     * @return response received from SDP.
     * @throws MchoiceAventuraMessagingException - In case of illegal formatted message or connection
     * error this exception will throws.
     */
    public MchoiceAventuraResponse sendMessage(long correlationId, String message, List<String> addresses) throws MchoiceAventuraMessagingException {
    	MtSmsReq mtSmsReq = createMTRequest(message, addresses);
    	sendMtMessage(mtSmsReq);
		return null;
    }

    /**
     * Send the message to set of people. You can provide addresses in comma separated string.
     *
     * @param message - string message that required to send.
     * @param addresses - lists of addresses with country code and subscriber number. Example :+94969696969
     * @return response received from SDP.
     * @throws MchoiceAventuraMessagingException - In case of illegal formatted message or connection
     * error this exception will throws.
     */
    public MchoiceAventuraResponse sendMessage(String message, String ... addresses) throws MchoiceAventuraMessagingException {
        return sendMessage(-1, message, addresses);
    }


    /**
     * Send the message to set of people. You can provide addresses in comma separated string.
     *
     * @param correlationId - CorrelationId for the message
     * @param message - string message that required to send.
     * @param addresses - lists of addresses with country code and subscriber number. Example :+94969696969
     * @return response received from SDP.
     * @throws MchoiceAventuraMessagingException - In case of illegal formatted message or connection
     * error this exception will throws.
     */
    public MchoiceAventuraResponse sendMessage(long correlationId, String message, String ... addresses) throws MchoiceAventuraMessagingException {
    	return sendMtMessage(createMTRequest(message, Arrays.asList(addresses)));
    }

	private MchoiceAventuraResponse sendMtMessage(MtSmsReq mtSmsReq) {
		try {
			return createMchoiceAventuraResponse(sendSmsRequest(mtSmsReq));

		} catch (SdpException e) {
			new MchoiceAventuraMessagingException(e);
		}

		return null;
	}

    /**
     * Send the message to all the registered users those who has registered with this application.
     *
     * @param message - string message that required to send.
     * @return response received from SDP.
     * @throws MchoiceAventuraMessagingException - In case of illegal formatted message or connection
     * error this exception will throws.
     */
    public MchoiceAventuraResponse broadcastMessage(String message) throws MchoiceAventuraMessagingException {
    	return broadcastMessage(-1, message, "");
    }

    /**
     * Send the message to all the registered users those who has registered with this application.
     *
     * @param message - string message that required to send.
     * @param addressListName - Name of a address list. For Example `all_registered` .
     * @return response received from SDP.
     * @throws MchoiceAventuraMessagingException - In case of illegal formatted message or connection
     * error this exception will throws.
     */
    public MchoiceAventuraResponse broadcastMessage(String message, String addressListName) throws MchoiceAventuraMessagingException {
    	return broadcastMessage(-1, message, "");
    }

    /**
     * Send the message to all the registered users those who has registered with this application.
     *
     * @param message - string message that required to send.
     * @param addressListName - Name of a address list. For Example `all_registered` .
     * @param correlationId - CorrelationId for the message
     * @return response received from SDP.
     * @throws MchoiceAventuraMessagingException - In case of illegal formatted message or connection
     * error this exception will throws.
     */
    public MchoiceAventuraResponse broadcastMessage(long correlationId, String message, String addressListName) throws MchoiceAventuraMessagingException {
    	return sendMtMessage(createMTRequest(message, Arrays.asList(new String [] {BROADCAST_ADDRESS})));
    }
    
    private MtSmsReq createMTRequest(String message, List<String> addressList)
    {
    	MtSmsReq mtSmsReq = new MtSmsReq();
    	mtSmsReq.setApplicationId(this.appId);
    	mtSmsReq.setBinaryHeader(BINARY_HEADER);
    	mtSmsReq.setChargingAmount(CHARGING_AMOUNT);
    	mtSmsReq.setDeliveryStatusRequest(DELIVERY_STATUS_REQUEST);
    	mtSmsReq.setDestinationAddresses(addressList);
    	mtSmsReq.setEncoding(ENCODING);
    	mtSmsReq.setMessage(message);
    	mtSmsReq.setPassword(this.password);
    	mtSmsReq.setSourceAddress(SOURCE_ADDRESS);
    	mtSmsReq.setVersion(VERSION);
    	
    	return mtSmsReq;
    }


    protected MchoiceAventuraResponse createMchoiceAventuraResponse(final MtSmsResp mtSmsResp) {
        final String version = mtSmsResp.getVersion();
        final String statusCode = mtSmsResp.getStatusCode();
        final String statusMessage = mtSmsResp.getStatusDetail();
        return new MchoiceAventuraResponse(version, null, statusCode, statusMessage, successResponseCodes.contains(statusCode));
    }
    
    public MtSmsResp sendSmsRequest(MtSmsReq smsReq) throws SdpException {
        try {
            GenericSender<MtSmsReq, MtSmsResp> smsRequestSender = new GenericSender<MtSmsReq, MtSmsResp>(new URL(url));
            
            System.out.println("Sending SMS Request");
            return smsRequestSender.sendRequest(smsReq, MtSmsResp.class);
        } catch (Exception e) {
        	e.printStackTrace();
            throw new SdpException(e);
        }
    }
}