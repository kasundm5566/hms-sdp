/*
 *   (C) Copyright 2009-2010 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.sdp.rest.servletbase;

/**
 * Response send by the mChoice SDP for sent message. For more information read the mChoice_SDP_HTTP_REST_Developer_Guide.
 * 
 * $LastChangedDate: 2010-05-25 11:44:24 +0530 (Tue, 25 May 2010) $
 * $LastChangedBy: kasunl $
 * $LastChangedRevision: 61197 $
 */
public class MchoiceAventuraResponse {

    /**
     * Version number from which the sms response send. Read the 
     */
    private String version;

    /**
     * A unique id assigned for the submitted request.
     */
    private String correlator;

    /**
     *  A code to identify the message status.
     */
    private String statusCode;

    /**
     * A description message regarding the message status.
     */
    private String statusMessage;

    /**
     * Indicate whether the request send successfully
     */
    private boolean success;

    public MchoiceAventuraResponse() {
    }

    public MchoiceAventuraResponse(final String version, final String correlator,
                              final String statusCode, final String statusMessage, final boolean success) {
        this.version = version;
        this.correlator = correlator;
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        this.success = success;
    }

    public String getVersion() {
        return version;
    }

    public String getCorrelator() {
        return correlator;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public boolean isSuccess() {
        return success;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final MchoiceAventuraResponse that = (MchoiceAventuraResponse) o;

        if (success != that.success) return false;
        if (correlator != null ? !correlator.equals(that.correlator) : that.correlator != null) return false;
        if (statusCode != null ? !statusCode.equals(that.statusCode) : that.statusCode != null) return false;
        if (statusMessage != null ? !statusMessage.equals(that.statusMessage) : that.statusMessage != null)
            return false;
        if (version != null ? !version.equals(that.version) : that.version != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = version != null ? version.hashCode() : 0;
        result = 31 * result + (correlator != null ? correlator.hashCode() : 0);
        result = 31 * result + (statusCode != null ? statusCode.hashCode() : 0);
        result = 31 * result + (statusMessage != null ? statusMessage.hashCode() : 0);
        result = 31 * result + (success ? 1 : 0);
        return result;
    }
}
