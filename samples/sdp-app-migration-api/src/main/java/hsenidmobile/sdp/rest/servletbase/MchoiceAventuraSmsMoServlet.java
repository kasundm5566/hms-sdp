/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.sdp.rest.servletbase;

import com.google.gson.Gson;

import hms.kite.samples.api.StatusCodes;
import hms.kite.samples.api.sms.messages.MoSmsReq;
import hms.kite.samples.api.sms.messages.MoSmsResp;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * <p>
 * Abstract implementation of message receiving servlet. Developer just need to
 * extend this servlet and implement onMessage method with application logic.
 * </p>
 * 
 * @version $Id$
 * 
 * @author manuja
 */
public abstract class MchoiceAventuraSmsMoServlet extends HttpServlet {

	private static final long serialVersionUID = 5841829010470560473L;
	private ExecutorService executorService;
    private static int sdpMoReceiverThreadCount;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void init(final ServletConfig config) throws ServletException {
		super.init(config);
		
        initializeReceivingThreadPool();
	}
	
	private void initializeReceivingThreadPool() {
        executorService = Executors.newCachedThreadPool(new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r, "sdp-mo-receiver-thread-" + ++sdpMoReceiverThreadCount);
            }
        });
    }

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String contentType = req.getContentType();
		if (contentType == null || !contentType.equals("application/json")) {
			resp.setStatus(415);
			resp.getWriter().println("Only application/json is supporting");
			return;
		}
		processRequest(req, resp);
	}

	private void processRequest(HttpServletRequest req, HttpServletResponse resp) {
		Gson gson = new Gson();
		try {
			String readContent = readStringContent(req);
			MoSmsReq moSmsReq = gson.fromJson(readContent, MoSmsReq.class);

			fireMoEvent(new MchoiceAventuraSmsMessage(moSmsReq.getVersion(),
					null, moSmsReq.getSourceAddress(), moSmsReq.getMessage()));

			MoSmsResp moSmsResp = new MoSmsResp();
			moSmsResp.setStatusCode(StatusCodes.SuccessK);
			moSmsResp.setStatusDetail("Success");
			resp.getWriter().print(gson.toJson(moSmsResp));
		} catch (Exception e) {
			MoSmsResp moSmsResp = new MoSmsResp();
			moSmsResp.setStatusCode(StatusCodes.SystemErrorK);
			moSmsResp.setStatusDetail("System Error occurred");
			try {
				resp.getWriter().print(gson.toJson(moSmsResp));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	private String readStringContent(HttpServletRequest req) throws IOException {
		InputStream is = req.getInputStream();
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(is));

		String line;
		StringBuilder content = new StringBuilder();

		while ((line = bufferedReader.readLine()) != null) {
			content.append(line);
		}

		return content.toString();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.getWriter().println("SDP Application is Running");
	}
	
	private void fireMoEvent(final MchoiceAventuraSmsMessage message) {
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    onMessage(message);
                } catch (Exception e) {
                    //System.err.println("Unexpected error occurred");
                    //LOGGER.log(Level.INFO, "Unexpected error occurred", e);
                }
            }
        });
    }

	/**
	 * This method will be called every time a Mobile-Originated message is
	 * received. Override this method to have your own logic within.
	 * 
	 * @param message
	 *            message content
	 */
	protected abstract void onMessage(MchoiceAventuraSmsMessage message);
}
