/*
 *   (C) Copyright 2010-2011 hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hsenidmobile.sdp.rest.servletbase;

/**
 * Contains the parameter sent by a Mobile-Originated message.
 *
 * $LastChangedDate: 2010-12-19 09:32:52 +0530 (Sun, 19 Dec 2010) $
 * $LastChangedBy: jasond $
 * $LastChangedRevision: 68991 $
 */
public class MchoiceAventuraSmsMessage {

    /**
     * Http Rest api version.
     */
    private String version;

    /**
     * An unique ID sent with every request.
     */
    private String correlator;

    /**
     * Phone number which the message originated from.
     */
    private String address;
    /**
     * Message sent by the mobile user.
     */
    private String message;

    private String chargedPartyAddress;

    /**
     * Create a Mobile Originated SMS message.
     * @param version Version of the requested message
     * @param correlator corerelatorID for the message
     * @param address Address which the message originated from
     * @param message Message content
     */
    public MchoiceAventuraSmsMessage(String version, String correlator, String address, String message) {
        this.version = version;
        this.correlator = correlator;
        this.address = address;
        this.message = message;
    }

    public String getVersion() {
        return version;
    }

    public String getCorrelator() {
        return correlator;
    }

    public String getAddress() {
        return address;
    }

    public String getMessage() {
        return message;
    }

    public String getChargedPartyAddress() {
        return chargedPartyAddress;
    }

    public void setChargedPartyAddress(String chargedPartyAddress) {
        this.chargedPartyAddress = chargedPartyAddress;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("SmsMessage{")
                .append("version='").append(version).append('\'')
                .append(", correlator='").append(correlator).append('\'')
                .append(", address='").append(address).append('\'')
                .append(", chargedPartyAddress='").append(chargedPartyAddress).append('\'')
                .append(", message='").append(message).append('\'')
                .append('}').toString();
    }
}
