/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.samples.api;

import com.google.gson.Gson;

import javax.net.ssl.*;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * Use this class to send MtRequests to SDP.
 */
public class GenericSender<Req, Resp> {

    private URL sdpUrl;

    public GenericSender(URL sdpUrl) {
        this.sdpUrl = sdpUrl;
    }

    public Resp sendRequest(Req req, Class<Resp> resp) throws SdpException {
        try {
            return internalSendRequest(req, resp);
        } catch (Exception e) {
            throw new SdpException(e);
        }
    }

    private Resp internalSendRequest(Req req, Class<Resp> resp) throws IOException {

        String[] urlType = sdpUrl.toString().split(":");
        Gson gson = new Gson();
        Resp response = null;

        if (urlType[0].equals("http")) {

            URLConnection conn = sdpUrl.openConnection();

            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

            wr.write(gson.toJson(req));
            wr.flush();

            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder content = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                content.append(line);
                content.append("\n");
            }
            response = gson.fromJson(content.toString(), resp);
            wr.close();
            rd.close();
            return response;

        } else if (urlType[0].equals("https")) {

            try{
                SSLContext ctx = SSLContext.getInstance("TLS");
                ctx.init(new KeyManager[0], new TrustManager[] {new DefaultTrustManager()}, new SecureRandom());
                SSLContext.setDefault(ctx);

                HttpsURLConnection connection = (HttpsURLConnection) sdpUrl.openConnection();

                connection.setHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String arg0, SSLSession arg1) {
                        return true;
                    }
                });

                connection.setDoInput(true);
                connection.setDoOutput(true);

                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestMethod("POST");

                //Send request
                DataOutputStream wr = new DataOutputStream (
                        connection.getOutputStream ());
                wr.writeBytes(gson.toJson(req));
                wr.flush ();
                wr.close ();

                //Get response
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder content = new StringBuilder();
                String line;
                while ((line = rd.readLine()) != null) {
                    content.append(line);
                    content.append("\n");
                }

                response = gson.fromJson(content.toString(), resp);
                wr.close();
                rd.close();

                System.out.println("Response : " + response);

            } catch (Exception e) {
                System.out.println("unexpected error occurred while sending sms-mt " + e);
            }
        }

        return response;
    }

    public URL getSdpUrl() {
        return sdpUrl;
    }

    /**
     * Set SDP MT Receiving URL
     *
     * @param sdpUrl
     */
    public void setSdpUrl(URL sdpUrl) {
        this.sdpUrl = sdpUrl;
    }

    private static class DefaultTrustManager implements X509TrustManager {

        @Override
        public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}

        @Override
        public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }

}
