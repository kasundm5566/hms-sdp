/*
 *   (C) Copyright 1996-${year} hSenid Software International (Pvt) Limited.
 *   All Rights Reserved.
 *
 *   These materials are unpublished, proprietary, confidential source code of
 *   hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 *   of hSenid Software International (Pvt) Limited.
 *
 *   hSenid Software International (Pvt) Limited retains all title to and intellectual
 *   property rights in these materials.
 *
 */
package hms.kite.samples.api.sms.messages;

import java.util.List;

public class MtSmsReq {

    private String applicationId;
    private String password;
    private String version;
    private List<String> destinationAddresses;
    private String message;
    private String sourceAddress;
    private String deliveryStatusRequest;
    private String encoding;
    private String chargingAmount;
    private String binaryHeader;


    public String getMessage() {
        return message;
    }

    /**
     * Set the message of mt request.
     *
     * @param message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    public String getApplicationId() {
        return applicationId;
    }

    /**
     * Set the application id
     *
     * @param applicationId
     */
    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getPassword() {
        return password;
    }

    /**
     * Set the password
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getDestinationAddresses() {
        return destinationAddresses;
    }

    /**
     * Set the recipient address list.
     *
     * @param destinationAddresses
     */
    public void setDestinationAddresses(List<String> destinationAddresses) {
        this.destinationAddresses = destinationAddresses;
    }

    public String getChargingAmount() {
        return chargingAmount;
    }

    /**
     * Set the charging amount in sdp system currency. This should be a numeric value.
     *
     * @param chargingAmount
     */
    public void setChargingAmount(String chargingAmount) {
        this.chargingAmount = chargingAmount;
    }

    public String getEncoding() {
        return encoding;
    }

    /**
     * Encoding type of the message.
     *
     * @param encoding
     */
    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public String getDeliveryStatusRequest() {
        return deliveryStatusRequest;
    }

    /**
     * Specify status Reports required or not. Values should be true of false.
     *
     * @param deliveryStatusRequest
     */
    public void setDeliveryStatusRequest(String deliveryStatusRequest) {
        this.deliveryStatusRequest = deliveryStatusRequest;
    }

    public String getSourceAddress() {
        return sourceAddress;
    }

    /**
     * Set Source address.
     *
     * @param sourceAddress
     */
    public void setSourceAddress(String sourceAddress) {
        this.sourceAddress = sourceAddress;
    }

    public String getVersion() {
        return version;
    }

    /**
     * Set message version.
     *
     * @return
     */
    public void setVersion(String version) {
        this.version = version;
    }

    public String getBinaryHeader() {
        return binaryHeader;
    }

    /**
     * Set binary header
     *
     * @param binaryHeader
     */
    public void setBinaryHeader(String binaryHeader) {
        this.binaryHeader = binaryHeader;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("MtSmsReq{")
                .append("applicationId='").append(applicationId).append('\'')
                .append(", password='").append(password).append('\'')
                .append(", version='").append(version).append('\'')
                .append(", destinationAddresses=").append(destinationAddresses)
                .append(", message='").append(message).append('\'')
                .append(", sourceAddress='").append(sourceAddress).append('\'')
                .append(", deliveryStatusRequest='").append(deliveryStatusRequest).append('\'')
                .append(", encoding='").append(encoding).append('\'')
                .append(", chargingAmount='").append(chargingAmount).append('\'')
                .append(", binaryHeader='").append(binaryHeader).append('\'')
                .append('}').toString();
    }
}
